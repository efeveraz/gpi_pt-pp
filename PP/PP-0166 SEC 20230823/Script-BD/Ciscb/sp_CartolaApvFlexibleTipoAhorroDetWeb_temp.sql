IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[sp_CartolaApvFlexibleTipoAhorroDetWeb_temp]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [DBO].[sp_CartolaApvFlexibleTipoAhorroDetWeb_temp]
GO

CREATE PROCEDURE [DBO].[sp_CartolaApvFlexibleTipoAhorroDetWeb_temp]
  @RutCliente  VARCHAR(15)
 ,@FechaDesde  VARCHAR(8)
 ,@FechaHasta  VARCHAR(8)
 ,@CodErr   INT      OUTPUT
 ,@MsgErr   VARCHAR(4000)  OUTPUT

 -- set @RutCliente='12063716-9'
 --set  @FechaDesde='20170430'
 --set  @FechaHasta='20170507'
 --set @CodErr=0
 --set  @MsgErr=''
AS
BEGIN

 SET NOCOUNT ON;
  --***********************************************************************************************
 -- DECLARACION DE VARIABLES
 --***********************************************************************************************
 DECLARE @UF1      NUMERIC(10,2)
 DECLARE @UF2      NUMERIC(10,2)
 DECLARE @DOLAR1      NUMERIC(10,2)
 DECLARE @DOLAR2      NUMERIC(10,2)
 DECLARE @ID_CLIENTE     INT
 DECLARE @ID_CUENTA     INT
 DECLARE @NUM_CUENTA     INT
 DECLARE @DSC_TIPOAHORRO    VARCHAR(100)
 DECLARE @DSC_CUENTA     VARCHAR(100)
 DECLARE @ID_EMPRESA     INT
 DECLARE @ID_MONEDA     INT
 DECLARE @ID_TIPOCUENTA    INT
 DECLARE @CONT      INT
 DECLARE @CANTIDAD     INT
 DECLARE @CONT2      INT
 DECLARE @CANTIDAD2     INT
 DECLARE @CONT3      INT
 DECLARE @CANTIDAD3     INT
 DECLARE @ID_ARBOL_CLASE_INST_HOJA INT
 DECLARE @DSC_ARBOL_CLASE_INST  VARCHAR(100)
 DECLARE @DSC_ARBOL_CLASE_INST_HOJA VARCHAR(100)
 DECLARE @SALDO_ANTERIOR    FLOAT
 DECLARE @SALDO      FLOAT
 DECLARE @VECNOM      VARCHAR(MAX)
 DECLARE @VECMTO      VARCHAR(MAX)
 DECLARE @ID_CAJA_CUENTA    INT
        DECLARE @ID_ARBOL_SEC_ECONOMICO INT
        DECLARE @fecha_desde_caja varchar(8)
        DECLARE @DSC_CAJA_CUENTA VARCHAR(50)
        DECLARE @Fecha_liq varchar(50)


 --***********************************************************************************************
 -- DECLARACION DE TABLAS
 --***********************************************************************************************
 DECLARE @INDICADORES TABLE (
   ID_TIPO_CAMBIO  INT
   ,DSC_MONEDA   VARCHAR(50)
   ,VALOR    NUMERIC(18,2)
   ,DSC_TIPO_CAMBIO VARCHAR(100)
   ,ABR_TIPO_CAMBIO VARCHAR(30) )

 DECLARE @INDICADORES_SALIDA TABLE (
   ID_TABLA   INT  IDENTITY(1,1)
   ,Descripcion  VARCHAR(30)
   ,VALOR1    VARCHAR(30)
   ,VALOR2    VARCHAR(30) )

 DECLARE @Cuentas TABLE (
   ID_TABLA     INT IDENTITY(1,1),
   id_cuenta     INT,
   id_contrato_cuenta   INT,
   num_cuenta     INT,
   ABR_CUENTA     VARCHAR(30),
   DSC_CUENTA     VARCHAR(100),
   ID_MONEDA     INT,
   COD_MONEDA     VARCHAR(10),
   DSC_MONEDA     VARCHAR(30),
   FLG_ES_MONEDA_PAGO   VARCHAR(10),
   OBSERVACION     VARCHAR(100),
   FLG_BLOQUEADO    VARCHAR(1),
   OBS_BLOQUEO     VARCHAR(100),
   FLG_IMP_INSTRUCCIONES  VARCHAR(1),
   id_cliente     INT,
   rut_cliente     VARCHAR(15),
   nombre_cliente    VARCHAR(100),
   ID_TIPO_ESTADO    INT,
   COD_ESTADO     VARCHAR(1),
   DSC_ESTADO     VARCHAR(30),
   COD_TIPO_ADMINISTRACION  VARCHAR(10),
   DSC_TIPO_ADMINISTRACION  VARCHAR(30),
   ID_EMPRESA     INT,
   DSC_EMPRESA     VARCHAR(30),
   ID_PERFIL_RIESGO   INT,
   DSC_PERFIL_RIESGO   VARCHAR(30),
   FECHA_OPERATIVA    DATETIME,
   FLG_MOV_DESCUBIERTOS  VARCHAR(1),
   ID_ASESOR     INT,
   Fecha_Cierre_Cuenta   DATETIME,
   DECIMALES_CUENTA   INT,
   Simbolo_Moneda    VARCHAR(10),
   FECHA_CONTRATO    DATETIME,
   ID_TIPOCUENTA    INT,
   NUMERO_FOLIO    INT,
   TIPO_AHORRO     INT,
   RUT_ASESOR     VARCHAR(15),
   NOMBRE_ASESOR    VARCHAR(100),
   Flg_Considera_Com_VC  VARCHAR(1),
   FECHA_CIERRE_OPERATIVA DATETIME,
   DSC_TIPOAHORRO    VARCHAR(250) )

 CREATE TABLE #Arbol (
   ID_TABLA     INT    IDENTITY(1,1),
   ID_ARBOL_CLASE_INST   INT,
   ID_ARBOL_CLASE_INST_HOJA INT,
   DSC_ARBOL_CLASE_INST  VARCHAR(100),
   DSC_ARBOL_CLASE_INST_HOJA VARCHAR(100),
   MONTO_MON_CTA_INST   BIGINT,
   MONTO_MON_CTA_HOJA   BIGINT,
   CODIGO      VARCHAR(10),
   NIVEL      INT,
   CODIGO_PADRE                VARCHAR(20) )



DECLARE   @SaldosActivos TABLE(
 origen                       VARCHAR(50)
  ,ID_CUENTA        NUMERIC
  , FECHA_CIERRE                      DATETIME
  , ID_NEMOTECNICO                    NUMERIC
  , NEMOTECNICO                       VARCHAR(50)
  , EMISOR                            VARCHAR(100)
  , COD_EMISOR                        VARCHAR(10)
  , DSC_NEMOTECNICO                   VARCHAR(120)
  , TASA_EMISION_2                    NUMERIC(18,4)
  , CANTIDAD                          NUMERIC(18,4)
  , PRECIO                            NUMERIC(18,6)
  , TASA_EMISION                      NUMERIC(18,4)
  , FECHA_VENCIMIENTO                 DATETIME
  , PRECIO_COMPRA                     NUMERIC(18,4)
  , TASA                              NUMERIC(18,4)
  , TASA_COMPRA                       NUMERIC(18,4)
  , MONTO_VALOR_COMPRA                NUMERIC(18,4)
  , MONTO_MON_CTA                     NUMERIC(18,4)
  , ID_MONEDA_CTA                     NUMERIC
  , ID_MONEDA_NEMOTECNICO             NUMERIC
  , SIMBOLO_MONEDA                    VARCHAR(3)
  , MONTO_MON_NEMOTECNICO             NUMERIC(18,4)
  , MONTO_MON_ORIGEN                  NUMERIC(18,4)
  , ID_EMPRESA                        NUMERIC
  , ID_ARBOL_CLASE_INST               NUMERIC
  , COD_INSTRUMENTO                   VARCHAR(15)
  , DSC_ARBOL_CLASE_INST              VARCHAR(100)
  , PORCENTAJE_RAMA                   NUMERIC(18,4)
  , PRECIO_PROMEDIO_COMPRA            NUMERIC(18,4)
  , MONTO_PROMEDIO_COMPRA             NUMERIC(18,4)
  , DSC_PADRE_ARBOL_CLASE_INST        VARCHAR(100)
  , RENTABILIDAD                      FLOAT
  , DIAS                              NUMERIC
  , COD_PRODUCTO                      VARCHAR(10)
  , ID_PADRE_ARBOL_CLASE_INST         NUMERIC
  , DURACION                          NUMERIC(18,6)
  , DECIMALES_MOSTRAR                 NUMERIC
  , DSC_CLASIFICADOR_RIESGO           VARCHAR(50)
  , ID_SUBFAMILIA                     NUMERIC
  , COD_SUBFAMILIA                    VARCHAR(50)
  , CODIGO                            VARCHAR(20)
  , MONTO_INVERTIDO                   NUMERIC(18,6)
  , VALOR_MERCADO                     NUMERIC(18,6)
  , UTILIDAD_PERDIDA                  NUMERIC(18,6)
  , NUMERO_CONTRATO                   VARCHAR(50)
  , TIPO_FWD                          VARCHAR(10)
  , MONEDA_EMISION                    VARCHAR(10)
  , MODALIDAD                         VARCHAR(15)
  , UNIDAD_ACTIVO_SUBYACENTE          VARCHAR(10)
  , DECIMALES_ACTIVO_SUBYACENTE       NUMERIC
  , VALOR_NOMINAL                     NUMERIC(18,4)
  , FECHA_INICIO                      DATETIME
  , PRECIO_PACTADO                    NUMERIC(18,6)
  , MONTO_MON_USD                     NUMERIC(18,6)
  , VALOR_PRECIO_PACTADO              NUMERIC(18,4)
  )

 DECLARE @Instrumentos TABLE (
   ID_TABLA     INT    IDENTITY(1,1),
   TipoInstrumento    VARCHAR(50),
   SubTipo      VARCHAR(50),
   NumCuenta     INT,
   DscCuenta     VARCHAR(100),
   Nemotecnico     VARCHAR(50),
   Emisor      VARCHAR(50),
   Moneda      VARCHAR(10),
   PorcentajeInversion   NUMERIC(6,2),
   Cantidad     NUMERIC(18,4),
   PrecioCompra    NUMERIC(18,4),
   MontoCompra     BIGINT,
   PrecioActual    NUMERIC(18,4),
   ValorMercado    BIGINT,
   Rentabilidad    NUMERIC(6,2),
   TasaCupon     NUMERIC(18,4),
   FechaVencimiento   VARCHAR(8),
   TasaCompra     NUMERIC(6,2),
   TasaMercado     NUMERIC(6,2),
   Valorizacion    BIGINT,
            duration     NUMERIC (18,6))

 DECLARE @RvSector TABLE (
   ID_TABLA     INT    IDENTITY(1,1),
   NumCuenta     INT,
   ID_SECTOR     INT,
   DSC_SECTOR     VARCHAR(50),
   MONTO_MON_CTA    BIGINT,
   PORC_SECTOR     NUMERIC(10,6) )

 DECLARE @Graficos TABLE (
   ID_TABLA     INT    IDENTITY(1,1),
   NumCuenta     INT,
   VECNOM      VARCHAR(MAX),
   VECMTO      VARCHAR(MAX) )

 DECLARE @CajasCuentas TABLE (
   ID_TABLA     INT    IDENTITY(1,1),
   ID_CAJA_CUENTA    INT,
   DSC_CAJA_CUENTA    VARCHAR(30),
   ID_CUENTA     INT,
   NUM_CUENTA     VARCHAR(10),
   ID_MONEDA     INT,
   COD_MONEDA     VARCHAR(10),
   DSC_MONEDA     VARCHAR(30),
   COD_MERCADO     VARCHAR(1),
   DESC_MERCADO    VARCHAR(30),
   Decimales     INT )

  DECLARE @DescCuentas TABLE (
   ID_TABLA     INT    IDENTITY(1,1),
  ID_CAJA_CUENTA    INT,
   DSC_CAJA_CUENTA    VARCHAR(30),
   ID_CUENTA     INT,
   NUM_CUENTA     VARCHAR(10))



 DECLARE @InformesCajas TABLE (
   ID_TABLA     INT    IDENTITY(1,1),
   ID_CUENTA     INT,
   NUM_CUENTA     INT,
   DSC_CUENTA     VARCHAR(100),
   TIPO_CAJA     VARCHAR(50),
   ID_CIERRE INT,
   ID_OPERACION    INT,
   FECHA_LIQUIDACION   DATETIME,
   DESCRIPCION     VARCHAR(100),
   DESCRIPCION_CAJA   VARCHAR(100),
   MONEDA      VARCHAR(10),
   MONTO_MOVTO_ABONO   NUMERIC(18,4),
   MONTO_MOVTO_CARGO   NUMERIC(18,4),
   SALDO      NUMERIC(18,4) )

 DECLARE @InformeTransacciones TABLE (
 FECHA_OPERACION datetime
 ,FECHA_CORTE datetime
 ,TIPO_MOVIMIENTO varchar(max)
 ,DSC_TIPO_MOVIMIENTO varchar(max)
 ,NUM_CUENTA varchar(max)
 ,ID_OPERACION int
 ,FACTURA int
 ,NEMOTECNICO varchar(max)
 ,CANTIDAD numeric(18,4)
 ,MONEDA varchar(max)
 ,DECIMALES_MOSTRAR numeric(2,0)
 ,PRECIO numeric(18,10)
 ,COMISION numeric(18,4)
 ,DERECHOS numeric(18,4)
 ,GASTOS numeric(18,4)
 ,IVA numeric(18,4)
 ,MONTO numeric(18,4)
 ,MONTO_TOTAL_OPERACION  numeric(18,4)
 ,DSC_TIPO_OPERACION varchar(max)
 ,CONTRAPARTE VARCHAR(100)
 --,DSC_CUENTA varchar(100)
  )
  create TABLE #InformeTransaccionesFinal(
  --DECLARE @InformeTransaccionesFinal TABLE (
 FECHA_OPERACION datetime
 ,FECHA_CORTE datetime
 ,TIPO_MOVIMIENTO varchar(max)
 ,DSC_TIPO_MOVIMIENTO varchar(max)
 ,NUM_CUENTA varchar(max)
 ,ID_OPERACION int
 ,FACTURA int
 ,NEMOTECNICO varchar(max)
 ,CANTIDAD numeric(18,4)
 ,MONEDA varchar(max)
 ,DECIMALES_MOSTRAR numeric(2,0)
 ,PRECIO numeric(18,10)
 ,COMISION numeric(18,4)
 ,DERECHOS numeric(18,4)
 ,GASTOS numeric(18,4)
 ,IVA numeric(18,4)
 ,MONTO numeric(18,4)
 ,MONTO_TOTAL_OPERACION  numeric(18,4)
 ,DSC_TIPO_OPERACION varchar(max)
 ,DSC_CUENTA varchar(max)
 ,CONTRAPARTE VARCHAR(100)
  )

  DECLARE @TMP TABLE (
      ID_ARBOL_CLASE_INST                        NUMERIC,
                       ID_ARBOL_CLASE_INST_HOJA                  NUMERIC,
                       DSC_ARBOL_CLASE_INST VARCHAR(100),
                       DSC_ARBOL_CLASE_INST_HOJA VARCHAR(100),
                       MONTO_MON_CTA_INST        NUMERIC(18,2),
                       MONTO_MON_CTA_HOJA        NUMERIC(18,2),
                       CODIGO                    VARCHAR(20),
                       NIVEL                     INT ,
                       CODIGO_PADRE              VARCHAR(20)  )



 BEGIN TRY

  SET @CodErr = 0
  SET @MsgErr = ''

  --***********************************************************************************************
  -- INDICADORES
  --***********************************************************************************************
  print('1')
  INSERT INTO @INDICADORES EXEC CSGPI.dbo.PKG_VALOR_TIPO_CAMBIO$BuscarIndicadores @FechaHasta
  print('2')
  INSERT INTO @INDICADORES_SALIDA
  SELECT 'Valores al'
    ,@FechaHasta
    ,@FechaDesde

  SELECT @DOLAR1 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 1
  SELECT @UF1 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 2
  print('3')
  INSERT INTO @INDICADORES EXEC CSGPI.dbo.PKG_VALOR_TIPO_CAMBIO$BuscarIndicadores @FechaDesde
  print('4')

  SELECT @DOLAR2 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 1
  SELECT @UF2 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 2

  INSERT INTO @INDICADORES_SALIDA
  SELECT 'UF'
    ,@UF1
    ,@UF2

  INSERT INTO @INDICADORES_SALIDA
  SELECT 'Dolar OBS.'
    ,@DOLAR1
    ,@DOLAR2

  --***********************************************************************************************
  -- CUENTAS APV
  --***********************************************************************************************
  SELECT TOP 1 @ID_CLIENTE = id_cliente
  FROM CSGPI.dbo.VIEW_CUENTAS_VIGENTES
  WHERE rut_cliente = @RutCliente
  print('5')
  INSERT INTO @Cuentas EXEC CSGPI.dbo.PKG_CUENTAS$BuscarCuentasAPV @ID_CLIENTE, NULL
  print('6')
  SET @CANTIDAD = ISNULL((SELECT COUNT(ID_TABLA) FROM @Cuentas), 0)
  SET @CONT = 1

  WHILE @CONT <= @CANTIDAD
  BEGIN

   -- LIMPIA TABLAS
   TRUNCATE TABLE #Arbol

   -- OBTIENE IDENTIFICADORES
   SELECT @ID_CUENTA = ID_CUENTA,
     @NUM_CUENTA = num_cuenta,
     @ID_EMPRESA = ID_EMPRESA,
     @ID_MONEDA = ID_MONEDA,
     @ID_TIPOCUENTA = ID_TIPOCUENTA,
     @DSC_TIPOAHORRO = DSC_TIPOAHORRO
   FROM @Cuentas
   WHERE ID_TABLA = @CONT

   SET @DSC_CUENTA = 'Cuenta: ' + right('0000' +  CAST(@NUM_CUENTA AS VARCHAR(10)),4) + ' - ' + @DSC_TIPOAHORRO

   INSERT INTO #Arbol
    EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA @ID_CUENTA,@ID_EMPRESA,@FechaHasta,NULL,NULL,NULL

   SET @CANTIDAD2 = ISNULL((SELECT COUNT(ID_TABLA) FROM #Arbol), 0)
   SET @CONT2 = 1

   WHILE @CONT2 <= @CANTIDAD2
   BEGIN

    SELECT  @ID_ARBOL_CLASE_INST_HOJA = ID_ARBOL_CLASE_INST_HOJA
      ,@DSC_ARBOL_CLASE_INST = DSC_ARBOL_CLASE_INST
      ,@DSC_ARBOL_CLASE_INST_HOJA = DSC_ARBOL_CLASE_INST_HOJA
    FROM #Arbol
    WHERE ID_TABLA = @CONT2

    DELETE FROM @SaldosActivos



    INSERT INTO @SaldosActivos
    EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS_WEB$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO @ID_CUENTA,@FechaHasta,@ID_ARBOL_CLASE_INST_HOJA,@ID_MONEDA,NULL,NULL,NULL

    --***********************************************************************************************
    -- RENTA VARIABLE NACIONAL
    --***********************************************************************************************


     INSERT INTO @Instrumentos (
      TipoInstrumento,
      SubTipo,
      NumCuenta,
      DscCuenta,
      Nemotecnico,
      Moneda,
      PorcentajeInversion,
      Cantidad,
      PrecioCompra,
      MontoCompra,
      PrecioActual,
      ValorMercado,
      Rentabilidad
     )
     SELECT
      'RENTA VARIABLE',
      dsc_arbol_clase_inst,
      @NUM_CUENTA,
      @DSC_CUENTA,
      nemotecnico,
      SIMBOLO_MONEDA,
      porcentaje_rama,
      cantidad,
      precio_promedio_compra,
      ROUND(monto_promedio_compra,0),
      precio,
      monto_mon_cta,
      rentabilidad
     FROM @SaldosActivos
     WHERE dsc_padre_arbol_clase_inst ='Renta Variable Nacional' --06-03-2014 --'RENTA VARIABLE'



     INSERT INTO @Instrumentos (
      TipoInstrumento,
      SubTipo,
      NumCuenta,
      DscCuenta,
      PorcentajeInversion,
      MontoCompra,
      ValorMercado
     )
     SELECT
      'RENTA VARIABLE',
      dsc_arbol_clase_inst,
      @NUM_CUENTA,
      @DSC_CUENTA,
      100,
      SUM(ROUND(monto_promedio_compra,0)),
      SUM(monto_mon_cta)
     FROM @SaldosActivos
     WHERE dsc_padre_arbol_clase_inst ='Renta Variable Nacional' --06-03-2014--'RENTA VARIABLE'
     GROUP BY dsc_arbol_clase_inst

     --***********************************************************************************************
     -- RENTA VARIABLE POR SECTORES ECONOMICOS
     --***********************************************************************************************
     IF @ID_ARBOL_CLASE_INST_HOJA = 39
     BEGIN


      INSERT INTO @TMP
      EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA @ID_CUENTA, @ID_EMPRESA, @FechaHasta

      SELECT @ID_ARBOL_SEC_ECONOMICO = ID_ARBOL_CLASE_INST_HOJA from  @TMP
      WHERE DSC_ARBOL_CLASE_INST= 'Renta Variable Nacional' AND DSC_ARBOL_CLASE_INST_HOJA='Acciones'

      INSERT INTO @RvSector (
       ID_SECTOR,
       DSC_SECTOR,
       MONTO_MON_CTA,
       PORC_SECTOR)
      EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$BUSCAR_RV_SECTOR @ID_CUENTA,@FechaHasta,'39',@ID_MONEDA

      UPDATE @RvSector
      SET NumCuenta = @NUM_CUENTA
      WHERE NumCuenta IS NULL

      -- Genera parametros del gr�fico
      SET @VECNOM = ''
      SET @VECMTO = ''
      SET @CANTIDAD3 = ISNULL((SELECT COUNT(ID_TABLA) FROM @RvSector),0)
      SET @CONT3 = ISNULL((SELECT TOP 1 ID_TABLA FROM @RvSector WHERE NumCuenta = @NUM_CUENTA ORDER BY ID_TABLA),0)

      WHILE @CONT3 <= @CANTIDAD3
      BEGIN

       SET @VECNOM = @VECNOM + ISNULL((SELECT DSC_SECTOR FROM @RvSector WHERE ID_TABLA = @CONT3),'') + ' ' +  REPLACE(ISNULL(CAST(CAST(((SELECT PORC_SECTOR FROM @RvSector WHERE ID_TABLA = @CONT3)*100)as decimal(6,2)) AS VARCHAR(MAX)),''),'.',',')  + '%;'






       SET @VECMTO = @VECMTO + REPLACE(ISNULL(CAST(((SELECT PORC_SECTOR FROM @RvSector WHERE ID_TABLA = @CONT3)*100) AS VARCHAR(MAX)),''),'.',',') + ';'


       SET @CONT3 = @CONT3 + 1
      END

      -- Elimina el �ltimo ;
      IF LEN(@VECNOM) > 0
      BEGIN

       SET @VECNOM = SUBSTRING(@VECNOM,1,LEN(@VECNOM)-1)
       SET @VECMTO = SUBSTRING(@VECMTO,1,LEN(@VECMTO)-1)
      END

      INSERT INTO @Graficos (
       NumCuenta,
       VECNOM,
       VECMTO )
      SELECT @NUM_CUENTA, @VECNOM, @VECMTO

      -- Inserta Total
      IF EXISTS(SELECT * FROM @RvSector)
       INSERT INTO @RvSector (
        NumCuenta,
        DSC_SECTOR,
        MONTO_MON_CTA
       )
       SELECT  @NUM_CUENTA
         ,'Total'
         ,SUM(MONTO_MON_CTA)
       FROM @RvSector
       WHERE NumCuenta = @NUM_CUENTA

     END --END @ID_ARBOL_CLASE_INST_HOJA = 39

    --END

    --***********************************************************************************************
    -- RENTA FIJA NACIONAL
    --***********************************************************************************************


    IF @ID_ARBOL_CLASE_INST_HOJA = 41 OR @ID_ARBOL_CLASE_INST_HOJA = 42 OR @ID_ARBOL_CLASE_INST_HOJA = 40 OR @ID_ARBOL_CLASE_INST_HOJA = 244
    BEGIN

     INSERT INTO @Instrumentos (
      TipoInstrumento,
      SubTipo,
      NumCuenta,
      DscCuenta,
      Nemotecnico,
      Emisor,
      Moneda,
      PorcentajeInversion,
      Cantidad,
      TasaCupon,
      FechaVencimiento,
      TasaCompra,
      TasaMercado,
      Valorizacion,
      duration)
 SELECT
      'RENTA FIJA',
      dsc_arbol_clase_inst,
      @NUM_CUENTA,
      @DSC_CUENTA,
      nemotecnico,
      cod_emisor,
      SIMBOLO_MONEDA,
      CAST(ROUND(porcentaje_rama,2) AS NUMERIC(6,2)),
      cantidad,
      tasa_emision,
      CONVERT(VARCHAR(8),FECHA_VENCIMIENTO,112),
      CAST(tasa_compra AS NUMERIC(6,2)),
      CAST(tasa AS NUMERIC(6,2)),
      CAST(monto_mon_cta AS BIGINT),
      DURACION
     FROM @SaldosActivos
     WHERE dsc_padre_arbol_clase_inst ='Renta Fija Nacional' --06-03-2014-- 'RENTA FIJA'


     INSERT INTO @Instrumentos (
      TipoInstrumento,
      SubTipo,
      NumCuenta,
      DscCuenta,
      PorcentajeInversion,
      MontoCompra,
      ValorMercado)
     SELECT
      'RENTA FIJA',
      dsc_arbol_clase_inst,
      @NUM_CUENTA,
      @DSC_CUENTA,
      100,
      SUM(CAST(monto_promedio_compra AS BIGINT)),
      SUM(CAST(monto_mon_cta AS BIGINT))
     FROM @SaldosActivos
     WHERE dsc_padre_arbol_clase_inst ='Renta Fija Nacional' --06-03-2014 -- 'RENTA FIJA'
     GROUP BY dsc_arbol_clase_inst

    END

    SET @CONT2 = @CONT2 + 1
   END


-----------------------


   --***********************************************************************************************
   -- INFORMES DE CAJA
   --***********************************************************************************************


   INSERT INTO @CajasCuentas
   EXEC CSGPI.dbo.PKG_CAJAS_CUENTA$Buscarview NULL, @ID_CUENTA



   SET @CANTIDAD2 = ISNULL((SELECT COUNT(*) FROM @CajasCuentas),0)
   SET @CONT2 = 1
   SET @CONT2 = ISNULL((SELECT min(ID_TABLA) FROM @CajasCuentas WHERE ID_CUENTA=@ID_CUENTA),0)

   WHILE @CONT2 <= @CANTIDAD2
   BEGIN

    SET @ID_CAJA_CUENTA = (SELECT ID_CAJA_CUENTA FROM @CajasCuentas WHERE ID_TABLA = @CONT2)

    SET @DSC_CAJA_CUENTA = (SELECT DSC_CAJA_CUENTA  FROM @CajasCuentas WHERE ID_TABLA = @CONT2)
    select @fecha_desde_caja=CONVERT(VARCHAR(8), DATEADD(DAY,1,@FechaDesde), 112)


    EXEC CSGPI.dbo.PKG_SALDOS_CAJA$Buscar_Saldos_Cuenta @PSaldo=@SALDO OUTPUT,@Pid_Cuenta=@ID_CUENTA,@PId_Caja_Cuenta=@ID_CAJA_CUENTA,@PFecha_Cierre=@fecha_desde_caja--@FechaDesde

    SET @SALDO = ISNULL(@SALDO, 0)

    INSERT INTO @InformesCajas (
  FECHA_LIQUIDACION,
  DESCRIPCION,
  SALDO)
    VALUES( CONVERT(DATETIME, @FechaDesde, 112), 'SALDO ANTERIOR', @SALDO)


    INSERT INTO @InformesCajas (
    ID_CIERRE,
  ID_OPERACION,
  FECHA_LIQUIDACION,
  DESCRIPCION,
  DESCRIPCION_CAJA,
  MONEDA,
  MONTO_MOVTO_ABONO,
  MONTO_MOVTO_CARGO)
    EXEC CSGPI.dbo.PKG_MOVIMIENTOS_CAJA_SECURITY @ID_CAJA_CUENTA,@fecha_desde_caja,@FechaHasta


    SET @Fecha_liq = (select top 1 fecha_liquidacion from @InformesCajas order by ID_TABLA desc)
    SET  @Fecha_liq=CONVERT(VARCHAR(8), DATEADD(DAY,1,@Fecha_liq), 112)

    EXEC CSGPI.dbo.PKG_SALDOS_CAJA$Buscar_Saldos_Cuenta @PSaldo=@SALDO OUTPUT,@Pid_Cuenta=@ID_CUENTA,@PId_Caja_Cuenta=@ID_CAJA_CUENTA,@PFecha_Cierre=@Fecha_liq
    SET @SALDO_ANTERIOR =  @SALDO


    UPDATE @InformesCajas
    SET SALDO = @SALDO_ANTERIOR-- MONTO_MOVTO_ABONO - MONTO_MOVTO_CARGO + @SALDO_ANTERIOR
    ,TIPO_CAJA= @DSC_CAJA_CUENTA
    WHERE /*ID_TABLA = @CONT2 AND*/ SALDO IS NULL


  EXEC CSGPI.dbo.PKG_SALDOS_CAJA$Buscar_Saldos_Cuenta @PSaldo=@SALDO OUTPUT,@Pid_Cuenta=@ID_CUENTA,@PId_Caja_Cuenta=@ID_CAJA_CUENTA,@PFecha_Cierre=@FechaHasta--@FechaDesde

    SET @SALDO = ISNULL(@SALDO, 0)



    UPDATE @InformesCajas
    SET ID_CUENTA = @ID_CUENTA
  ,NUM_CUENTA = @NUM_CUENTA
  ,DSC_CUENTA = @DSC_CUENTA
  ,TIPO_CAJA= @DSC_CAJA_CUENTA
    WHERE ID_CUENTA IS NULL
    -- INSERT TOTALES
    INSERT INTO @InformesCajas (
  ID_CUENTA,
  NUM_CUENTA,
  DSC_CUENTA,
  DESCRIPCION,
  MONTO_MOVTO_ABONO,
  MONTO_MOVTO_CARGO,
  SALDO,
  TIPO_CAJA )
    SELECT
  @ID_CUENTA,
  @NUM_CUENTA,
  @DSC_CUENTA,
  'TOTALES',
  SUM(MONTO_MOVTO_ABONO),
  SUM(MONTO_MOVTO_CARGO),
  @SALDO,
  @DSC_CAJA_CUENTA
    FROM @InformesCajas
    WHERE ID_CUENTA = @ID_CUENTA




          SET @CONT2 = @CONT2 + 1
   END

   SET @CONT = @CONT + 1  --end while inicial
  END

  --***********************************************************************************************
  -- IMPRIME RESULTADOS
  --***********************************************************************************************

  SELECT 'Indicadores'   AS InicioBloque
    ,Descripcion   AS Descripcion
    ,ISNULL(Valor1, '')  AS Valor1
    ,ISNULL(Valor2, '')  AS Valor2
  FROM @INDICADORES_SALIDA

  SELECT 'Instrumento'  AS InicioBloque,
    NumCuenta,
    DscCuenta,
    TipoInstrumento,
    SubTipo,
    Nemotecnico = ISNULL(Nemotecnico,''),
    Emisor = ISNULL(Emisor,''),
    Moneda = ISNULL(Moneda,''),
    PorcentajeInversion,
    Cantidad,
    PrecioCompra,
    MontoCompra,
    PrecioActual,
    ValorMercado,
    Rentabilidad,
    TasaCupon,
    FechaVencimiento,
    TasaCompra,
    TasaMercado,
    Valorizacion,
    duration as duracion

  FROM @Instrumentos
  ORDER BY NumCuenta, TipoInstrumento DESC, SubTipo, ID_TABLA

  SELECT 'SectoresEconomicos'  AS InicioBloque,
    NumCuenta     AS NumCuenta,
    DSC_SECTOR     AS SectoresEconomicos,
    MONTO_MON_CTA    AS Monto
  FROM @RvSector

  SELECT 'Graficos'     AS InicioBloque,
    NumCuenta,
    VECNOM,
    VECMTO
  FROM @Graficos

  SELECT 'InformesCajas'           AS InicioBloque,
    NUM_CUENTA            AS NumCuenta,
    DSC_CUENTA            AS DscCuenta,
    TIPO_CAJA           AS TipoCaja,
    ID_OPERACION           AS NumeroOperacion,
    ISNULL(CONVERT(VARCHAR(8), FECHA_LIQUIDACION, 112), '') AS FechaLiquidacion,
    DESCRIPCION            AS Detalle,
    CAST(MONTO_MOVTO_ABONO AS BIGINT)      AS Ingreso,
    CAST(MONTO_MOVTO_CARGO AS BIGINT)      AS Egreso,
    CAST(SALDO AS BIGINT)         AS Saldo
  FROM @InformesCajas
   ORDER BY ID_TABLA

   --***********************************************************************************************
   -- INFORME DE TRANSACCIONES
   --***********************************************************************************************
  INSERT INTO @InformeTransacciones
    EXEC CSGPI.dbo.PKG_CARTOLA$DetalleMovimientos
    @PCONSOLIDADO  = 'CLT'
, @PID_ENTIDAD   = @ID_CLIENTE --898
, @PFECHA_INICIO = @FechaDesde
, @PFECHA_FIN    = @FechaHasta
, @PID_EMPRESA   = @ID_EMPRESA --4


INSERT INTO #InformeTransaccionesFinal (
     FECHA_OPERACION
 ,FECHA_CORTE
 ,TIPO_MOVIMIENTO
 ,DSC_TIPO_MOVIMIENTO
 ,NUM_CUENTA
 ,ID_OPERACION
 ,FACTURA
 ,NEMOTECNICO
 ,CANTIDAD
 ,MONEDA
 ,DECIMALES_MOSTRAR
 ,PRECIO
 ,COMISION
 ,DERECHOS
 ,GASTOS
 ,IVA
 ,MONTO
 ,MONTO_TOTAL_OPERACION
 ,DSC_TIPO_OPERACION
 )SELECT
      FECHA_OPERACION
 ,FECHA_CORTE
 ,TIPO_MOVIMIENTO
 ,DSC_TIPO_MOVIMIENTO
 ,NUM_CUENTA
 ,ID_OPERACION
 ,FACTURA
 ,NEMOTECNICO
 ,CANTIDAD
 ,MONEDA
 ,DECIMALES_MOSTRAR
 ,PRECIO
 ,COMISION
 ,DERECHOS
 ,GASTOS
 ,IVA
 ,MONTO
 ,MONTO_TOTAL_OPERACION
 ,DSC_TIPO_OPERACION
 from @InformeTransacciones

 --select '@InformeTransaccionesFinal',* from #InformeTransaccionesFinal



UPDATE #InformeTransaccionesFinal
SET DSC_CUENTA = i.DSC_CUENTA
FROM (SELECT NUM_CUENTA, DSC_CUENTA
      FROM @InformesCajas) i
WHERE  i.NUM_CUENTA = #InformeTransaccionesFinal.NUM_CUENTA



SELECT
'InformeTransacciones' AS InicioBloque
,DSC_CUENTA AS DescripcionCaja
,ISNULL(CONVERT(VARCHAR(8), FECHA_OPERACION, 112), '') AS FechaOperacion
,ISNULL(CONVERT(VARCHAR(8), FECHA_CORTE, 112), '') AS FechaCorte
,TIPO_MOVIMIENTO AS TipoMovimiento
,DSC_TIPO_MOVIMIENTO AS DscTipoMovimiento
,NUM_CUENTA AS NumCuenta
,ID_OPERACION AS IdOperacion
,FACTURA AS Factura
,NEMOTECNICO AS Nemotecnico
,CANTIDAD AS Cantidad
,MONEDA As Moneda
,DECIMALES_MOSTRAR AS DecimalesMostrar
,PRECIO AS Precio
,COMISION AS Comision
,DERECHOS AS Derecho
,GASTOS AS Gastos
,IVA AS Iva
,MONTO AS Monto
,MONTO_TOTAL_OPERACION  AS MontoTotalOperacion
,DSC_TIPO_OPERACION AS DscTipoOperacion
,CONTRAPARTE
 FROM #InformeTransaccionesFinal
 ORDER BY NUM_CUENTA, FECHA_OPERACION
  --***********************************************************************************************
  DROP TABLE #Arbol
   DROP TABLE #InformeTransaccionesFinal

 END TRY
 BEGIN CATCH
  SET @CodErr = @@ERROR
  SET @MsgErr = 'Error en el Procedimiento sp_CartolaApvFlexibleTipoAhorroDetWeb_temp:' + ERROR_MESSAGE()
 END CATCH
END
GO

GRANT EXECUTE ON [sp_CartolaApvFlexibleTipoAhorroDetWeb_temp] TO DB_EXECUTESP
GO
