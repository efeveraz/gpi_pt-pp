IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[sp_CartolaApvFlexibleConsDetalleWeb_temp]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [DBO].[sp_CartolaApvFlexibleConsDetalleWeb_temp]
GO

CREATE PROCEDURE [DBO].[sp_CartolaApvFlexibleConsDetalleWeb_temp]
 @RutCliente      VARCHAR(15)
,@FechaDesde      VARCHAR(8)
,@FechaHasta      VARCHAR(8)
,@CodErr          INT         OUTPUT
,@MsgErr          VARCHAR(4000)   OUTPUT
AS
BEGIN

   SET NOCOUNT ON;


   --***********************************************************************************************
   -- DECLARACION DE VARIABLES
   DECLARE @UF1                  NUMERIC(10,2)
   DECLARE @UF2                  NUMERIC(10,2)
   DECLARE @DOLAR1                  NUMERIC(10,2)
   DECLARE @DOLAR2                  NUMERIC(10,2)
   DECLARE @Consolidado            VARCHAR(10)
   DECLARE @ID_CLIENTE               INT
   DECLARE @ID_EMPRESA               INT
   DECLARE @ID_MONEDA               INT
   DECLARE @ID_TIPOCUENTA            INT
   DECLARE @CONT                  INT
   DECLARE @CANTIDAD               INT
   DECLARE @ID_ARBOL_CLASE_INST_HOJA   INT
   DECLARE @DSC_ARBOL_CLASE_INST      VARCHAR(100)
   DECLARE @DSC_ARBOL_CLASE_INST_HOJA   VARCHAR(100)
   DECLARE @SALDO_ANTERIOR            FLOAT
   DECLARE @SALDO                  FLOAT
   DECLARE @VECNOM                  VARCHAR(MAX)
   DECLARE @VECMTO                  VARCHAR(MAX)
   DECLARE @fecha_desde_caja varchar(8)
   DECLARE @DESC_CAJA_CUENTA varchar(100)
   DECLARE @ID_CAJA_CUENTA  INT
   DECLARE @CANTIDAD2   INT
   DECLARE @CONT2       INT


   --***********************************************************************************************
   -- DECLARACION DE TABLAS
   DECLARE @INDICADORES TABLE (ID_TIPO_CAMBIO    INT
                              ,DSC_MONEDA        VARCHAR(50)
                              ,VALOR             NUMERIC(18,2)
                              ,DSC_TIPO_CAMBIO   VARCHAR(100)
                              ,ABR_TIPO_CAMBIO   VARCHAR(30) )

   DECLARE @INDICADORES_SALIDA TABLE (ID_TABLA               INT      IDENTITY(1,1)
                                     ,Descripcion            VARCHAR(30)
                                     ,VALOR1                  VARCHAR(30)
          ,VALOR2                  VARCHAR(30) )

   DECLARE @Cuentas TABLE (ID                      INT IDENTITY(1,1),
                           id_cuenta               INT,
                           id_contrato_cuenta      INT,
                           num_cuenta              INT,
                           ABR_CUENTA              VARCHAR(30),
                           DSC_CUENTA              VARCHAR(100),
                           ID_MONEDA          INT,
                           COD_MONEDA              VARCHAR(10),
                           DSC_MONEDA              VARCHAR(30),
         FLG_ES_MONEDA_PAGO      VARCHAR(10),
                           OBSERVACION             VARCHAR(100),
                           FLG_BLOQUEADO           VARCHAR(1),
                           OBS_BLOQUEO             VARCHAR(100),
                           FLG_IMP_INSTRUCCIONES   VARCHAR(1),
                           id_cliente              INT,
                           rut_cliente             VARCHAR(15),
                           nombre_cliente          VARCHAR(100),
                           ID_TIPO_ESTADO          INT,
                           COD_ESTADO              VARCHAR(1),
                           DSC_ESTADO              VARCHAR(30),
                           COD_TIPO_ADMINISTRACION VARCHAR(10),
                           DSC_TIPO_ADMINISTRACION VARCHAR(30),
                           ID_EMPRESA              INT,
                           DSC_EMPRESA             VARCHAR(30),
                           ID_PERFIL_RIESGO        INT,
                           DSC_PERFIL_RIESGO       VARCHAR(30),
                           FECHA_OPERATIVA         DATETIME,
                           FLG_MOV_DESCUBIERTOS    VARCHAR(1),
                           ID_ASESOR               INT,
                           Fecha_Cierre_Cuenta     DATETIME,
                           DECIMALES_CUENTA        INT,
                           Simbolo_Moneda          VARCHAR(10),
                           FECHA_CONTRATO          DATETIME,
                           ID_TIPOCUENTA           INT,
                           NUMERO_FOLIO            INT,
                           TIPO_AHORRO             INT,
                           RUT_ASESOR              VARCHAR(15),
                           NOMBRE_ASESOR           VARCHAR(100),
                           Flg_Considera_Com_VC    VARCHAR(1),
                           FECHA_CIERRE_OPERATIVA DATETIME,----01-06-2014
                           DSC_TIPOAHORRO          VARCHAR(250) )

   DECLARE @Arbol   TABLE (ID_TABLA                  INT            IDENTITY(1,1),
                           ID_ARBOL_CLASE_INST       INT,
                           ID_ARBOL_CLASE_INST_HOJA  INT,
                           DSC_ARBOL_CLASE_INST      VARCHAR(100),
                           DSC_ARBOL_CLASE_INST_HOJA VARCHAR(100),
                           MONTO_MON_CTA_INST        BIGINT,
                           MONTO_MON_CTA_HOJA        BIGINT,
                           CODIGO                    VARCHAR(10),
                           NIVEL                     INT,
         CODIGO_PADRE              VARCHAR(30))



DECLARE   @SaldosActivos TABLE(
   ID_TABLA                    INT            IDENTITY(1,1),
   fecha_cierre          DATETIME      ,
   id_nemotecnico              NUMERIC          ,
   nemotecnico      VARCHAR(50)      ,
   EMISOR                      VARCHAR(100)     ,
   cod_emisor                  VARCHAR(10)      ,
   dsc_nemotecnico             VARCHAR(120)     ,
   precio                      NUMERIC(18,6)    ,
   tasa_emision                NUMERIC(18,4)    ,
   FECHA_VENCIMIENTO           DATETIME         ,
   tasa                        NUMERIC(18,4)    ,
   tasa_compra                 NUMERIC(18,4)    ,
   precio_compra               NUMERIC(18,4)    ,
   id_moneda_cta               NUMERIC          ,
   id_moneda_nemotecnico       NUMERIC          ,
   SIMBOLO_MONEDA              VARCHAR(3)       ,
   id_empresa                  NUMERIC          ,
   id_arbol_clase_inst         NUMERIC          ,
   cod_instrumento             VARCHAR(15)      ,
   dsc_arbol_clase_inst        VARCHAR(100)     ,
   precio_promedio_compra      NUMERIC(18,4)    ,
   dsc_padre_arbol_clase_inst  VARCHAR(100)     ,
   duration                    FLOAT            ,
   cod_producto                varchar(10)      ,
   id_padre_arbol_clase_inst   NUMERIC(10,0)    ,
   DSC_CLASIFICADOR_RIESGO     VARCHAR(50)      ,
   COD_SUBFAMILIA              varchar(10)      ,
   CODIGO                      VARCHAR(20)      ,
   cantidad                    NUMERIC(18,4)    ,
   monto_valor_compra          NUMERIC(18,4)    ,
   MONTO                       NUMERIC(18,4)    ,
   monto_mon_nemotecnico       NUMERIC(18,4)    ,
   monto_mon_origen            NUMERIC(18,4)    ,
   monto_mon_cta               NUMERIC(18,4)    ,
   porcentaje_rama             numeric(18,4)    ,
   monto_promedio_compra       NUMERIC(18,4)    ,
   rentabilidad                FLOAT

   )


   DECLARE @RentaVariable TABLE (ID_TABLA               INT            IDENTITY(1,1),
                                 TipoInstrumento      VARCHAR(50),
                                 Nemotecnico            VARCHAR(50),
                                 Moneda                 VARCHAR(10),
                                 PorcentajeInversion    NUMERIC(6,2),
                                 Cantidad               NUMERIC(18,4),
                                 PrecioCompra           NUMERIC(18,4),
                                 MontoCompra            NUMERIC(18,4),
                                 PrecioActual           NUMERIC(18,4),
                                 ValorMercado           NUMERIC(18,4),
                                 MontoMonCta            NUMERIC(18,4),
                                 Rentabilidad           NUMERIC(6,2) )

   DECLARE @RentaFija TABLE (ID_TABLA               INT            IDENTITY(1,1),
                             TipoInstrumento        VARCHAR(50),
                             Nemotecnico            VARCHAR(50),
                             Emisor                 VARCHAR(50),
                             Moneda                 VARCHAR(10),
                             PorcentajeInversion    NUMERIC(6,2),
                             Nominales              NUMERIC(18,4),
                             TasaCupon              VARCHAR(10),
               FechaVencimiento       VARCHAR(8),
                             TasaCompra             NUMERIC(6,2),
                             TasaMercado            NUMERIC(6,2),
                             Valorizacion           BIGINT,
        duration               NUMERIC(18,6) )

   DECLARE @CajasCuentas TABLE (ID_TABLA            INT            IDENTITY(1,1),
                                ID_CAJA_CUENTA      INT,
                                DSC_CAJA_CUENTA     VARCHAR(30),
                                ID_CUENTA           INT,
                                NUM_CUENTA          VARCHAR(10),
                                ID_MONEDA           INT,
                                COD_MONEDA          VARCHAR(10),
                                DSC_MONEDA          VARCHAR(30),
                                COD_MERCADO         VARCHAR(1),
                                DESC_MERCADO        VARCHAR(30),
                                Decimales           INT )

   DECLARE @SaldoInicial TABLE (id_Moneda           INT,
                                SaldoCaja           NUMERIC(18,4) )

   DECLARE @SaldoFinal TABLE (id_Moneda           INT,
                                SaldoCaja           NUMERIC(18,4) )

   DECLARE @InformesCajas   TABLE (ID_TABLA           INT            IDENTITY(1,1),
                                   ID_CUENTA          INT,
                                   ID_OPERACION       INT,
                                   FECHA_LIQUIDACION  DATETIME,
                                   DESCRIPCION        VARCHAR(100),
                                   MONEDA             VARCHAR(10),
                                   MONTO_MOVTO_ABONO  NUMERIC(18,4),
                                   MONTO_MOVTO_CARGO  NUMERIC(18,4),
                                   SALDO              NUMERIC(18,4),
           ID_MONEDA    INT ,
                                   DSC_CAJA_CUENTA VARCHAR(100),
                                   ID_CAJA_CUENTA INT )

   DECLARE @RvSector TABLE (ID_TABLA        INT            IDENTITY(1,1),
                            ID_SECTOR       INT,
                            DSC_SECTOR      VARCHAR(50),
                            MONTO_MON_CTA   BIGINT,
                            PORC_SECTOR     NUMERIC(10,6) )

   DECLARE @InformeTransacciones TABLE (
    FECHA_OPERACION datetime
    ,FECHA_CORTE datetime
    ,TIPO_MOVIMIENTO varchar(max)
    ,DSC_TIPO_MOVIMIENTO varchar(max)
    ,NUM_CUENTA varchar(max)
    ,ID_OPERACION int
    ,FACTURA int
    ,NEMOTECNICO varchar(max)
    ,CANTIDAD numeric(18,4)
    ,MONEDA varchar(max)
    ,DECIMALES_MOSTRAR numeric(2,0)
    ,PRECIO numeric(18,10)
    ,COMISION numeric(18,4)
    ,DERECHOS numeric(18,4)
    ,GASTOS numeric(18,4)
    ,IVA numeric(18,4)
    ,MONTO numeric(18,4)
    ,MONTO_TOTAL_OPERACION  numeric(18,4)
    ,DSC_TIPO_OPERACION varchar(max)
 ,CONTRAPARTE VARCHAR(10))

   CREATE TABLE #InformeTransaccionesFinal(
    FECHA_OPERACION datetime
    ,FECHA_CORTE datetime
    ,TIPO_MOVIMIENTO varchar(max)
    ,DSC_TIPO_MOVIMIENTO varchar(max)
    ,NUM_CUENTA varchar(max)
    ,ID_OPERACION int
    ,FACTURA int
    ,NEMOTECNICO varchar(max)
    ,CANTIDAD numeric(18,4)
    ,MONEDA varchar(max)
    ,DECIMALES_MOSTRAR numeric(2,0)
    ,PRECIO numeric(18,10)
    ,COMISION numeric(18,4)
    ,DERECHOS numeric(18,4)
    ,GASTOS numeric(18,4)
    ,IVA numeric(18,4)
    ,MONTO numeric(18,4)
    ,MONTO_TOTAL_OPERACION  numeric(18,4)
    ,DSC_TIPO_OPERACION varchar(max)
    ,DSC_CUENTA varchar(max)
    ,CONTRAPARTE VARCHAR(10)
     )

     --***********************************************************************************************

   BEGIN TRY

      SET @CodErr = 0
      SET @MsgErr = ''
      SET @Consolidado = 'CLT'

      --***********************************************************************************************
      -- INDICADORES
      --***********************************************************************************************

      INSERT INTO @INDICADORES EXEC CSGPI.dbo.PKG_VALOR_TIPO_CAMBIO$BuscarIndicadores @FechaHasta

      INSERT INTO @INDICADORES_SALIDA
      SELECT 'Valores al'
            ,substring(@FechaHasta,7,2) + '-'+ substring(@FechaHasta,5,2)+'-'+ substring(@FechaHasta,1,4)--@FechaHasta
            ,substring(@FechaDesde,7,2) + '-'+ substring(@FechaDesde,5,2)+'-'+ substring(@FechaDesde,1,4)--@FechaDesde

      SELECT @DOLAR1 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 1
      SELECT @UF1    = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 2

      INSERT INTO @INDICADORES EXEC CSGPI.dbo.PKG_VALOR_TIPO_CAMBIO$BuscarIndicadores @FechaDesde

      SELECT @DOLAR2 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 1
      SELECT @UF2    = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 2

      INSERT INTO @INDICADORES_SALIDA
      SELECT   'UF',REPLACE(@UF1,'.',','),REPLACE(@UF2,'.',',')

      INSERT INTO @INDICADORES_SALIDA
      SELECT   'Dolar OBS.',REPLACE(@DOLAR1,'.',','),REPLACE(@DOLAR2,'.',',')

      SELECT 'Indicadores'       AS InicioBloque
            ,Descripcion         AS Descripcion
            ,ISNULL(Valor1, '')  AS Valor1
            ,ISNULL(Valor2, '')  AS Valor2
      FROM   @INDICADORES_SALIDA

      --***********************************************************************************************


       SELECT   @ID_CLIENTE = Cu.ID_CLIENTE
       FROM     CSGPI.dbo.CUENTAS AS Cu with (NOLOCK)  INNER JOIN CSGPI.dbo.CLIENTES AS Cl with (NOLOCK)
                                              ON Cu.ID_CLIENTE = Cl.id_cliente
       WHERE    Cl.RUT_CLIENTE = @RutCliente
       AND      Cu.COD_ESTADO = 'H'

       INSERT INTO @Cuentas EXEC CSGPI.dbo.PKG_CUENTAS$BuscarCuentasAPV @ID_CLIENTE, NULL

       SELECT   TOP 1                  @ID_EMPRESA = ID_EMPRESA,
                @ID_MONEDA = ID_MONEDA,
                @ID_TIPOCUENTA = ID_TIPOCUENTA
       FROM     @Cuentas

      INSERT INTO @Arbol
         EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA_Consolidado NULL,@ID_EMPRESA,@FechaHasta,@ID_CLIENTE,NULL,@ID_MONEDA,@Consolidado

      SET @Cantidad = ISNULL((SELECT COUNT(ID_ARBOL_CLASE_INST_HOJA) FROM @Arbol), 0)
      SET @Cont = 1

      WHILE @Cont <= @Cantidad
      BEGIN

          SELECT @ID_ARBOL_CLASE_INST_HOJA = ID_ARBOL_CLASE_INST_HOJA
                ,@DSC_ARBOL_CLASE_INST = DSC_ARBOL_CLASE_INST
                ,@DSC_ARBOL_CLASE_INST_HOJA = DSC_ARBOL_CLASE_INST_HOJA
          FROM   @Arbol
          WHERE  ID_TABLA = @Cont


         INSERT INTO @SaldosActivos
            EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO_Consolidado NULL,@FechaHasta,@ID_ARBOL_CLASE_INST_HOJA,@ID_MONEDA,NULL,NULL,NULL,@ID_EMPRESA,@Consolidado,@ID_CLIENTE,NULL,@ID_TIPOCUENTA

         SET @Cont = @Cont + 1
      END

      --***********************************************************************************************
      -- RENTA VARIABLE NACIONAL
      --***********************************************************************************************

      INSERT INTO @RentaVariable (TipoInstrumento,
          Nemotecnico,
       Moneda,
                                  PorcentajeInversion,
          Cantidad,
          PrecioCompra,
                                  MontoCompra,
          PrecioActual,
          ValorMercado,
          MontoMonCta,
                                  Rentabilidad )
                           SELECT dsc_arbol_clase_inst,
          nemotecnico,
          SIMBOLO_MONEDA,
                                  porcentaje_rama,
          cantidad   ,
          precio_promedio_compra,
                                 ROUND(monto_promedio_compra,0),
          precio     ,
          MONTO,
          monto_mon_cta,
                                  ROUND( CAST(rentabilidad AS numeric(18,4)),2 ) as rentabilidad
                           FROM   @SaldosActivos
                           WHERE  dsc_padre_arbol_clase_inst = 'Renta Variable Nacional'--'RENTA VARIABLE'



      INSERT INTO @RentaVariable (TipoInstrumento,
                                  PorcentajeInversion,
                                  MontoCompra,
                                  ValorMercado,
                                  MontoMonCta)
                          SELECT  dsc_arbol_clase_inst,
                                  100,
                                  SUM(ROUND(monto_promedio_compra,0)),
                                  SUM(monto),
                                  SUM(monto_mon_cta)
                          FROM    @SaldosActivos
                          WHERE   dsc_padre_arbol_clase_inst = 'Renta Variable Nacional'--'RENTA VARIABLE'
                          GROUP   BY dsc_arbol_clase_inst

       SELECT   'RentaVariable'      AS InicioBloque,
               TipoInstrumento,
               Nemotecnico = ISNULL(Nemotecnico, ''),
               Moneda = ISNULL(Moneda,''),
               PorcentajeInversion,
               Cantidad,
               PrecioCompra,
               MontoCompra,
               PrecioActual,
               CAST(ROUND(ValorMercado,0) AS BIGINT) AS ValorMercado,
               CAST(ROUND(MontoMonCta,0) AS BIGINT) AS MontoMonCta,
               Rentabilidad
           FROM @RentaVariable
         ORDER BY TipoInstrumento, ID_TABLA

      --***********************************************************************************************
      -- RENTA VARIABLE POR SECTORES ECONOMICOS
      --***********************************************************************************************

      INSERT INTO @RvSector
         EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$BUSCAR_RV_SECTOR_consolidado NULL,@FechaHasta,'39',@ID_MONEDA,@ID_CLIENTE,@Consolidado,@ID_EMPRESA

      -- Genera parametros del gr�fico
      SET @VECNOM = ''
      SET @VECMTO = ''
      SET @CANTIDAD = ISNULL((SELECT COUNT(ID_TABLA) FROM @RvSector),0)
      SET @CONT = 1

      WHILE @CONT <= @CANTIDAD
      BEGIN
         SET @VECNOM = @VECNOM + ISNULL((SELECT DSC_SECTOR FROM @RvSector WHERE ID_TABLA = @CONT),'')+ ' ' +  REPLACE(ISNULL(CAST(CAST(((SELECT PORC_SECTOR FROM @RvSector WHERE ID_TABLA = @CONT)*100)as decimal(6,2)) AS VARCHAR(MAX)),''),'.',',') + '%' +





';'
         SET @VECMTO = @VECMTO + REPLACE(ISNULL(CAST(((SELECT PORC_SECTOR FROM @RvSector WHERE ID_TABLA = @CONT)*100) AS VARCHAR(MAX)),''),'.',',') + ';'

         SET @CONT = @CONT + 1
      END

      -- Elimina el �ltimo ;
      IF LEN(@VECNOM) > 1
      BEGIN
         SET @VECNOM = SUBSTRING(@VECNOM,1,LEN(@VECNOM)-1)
         SET @VECMTO = SUBSTRING(@VECMTO,1,LEN(@VECMTO)-1)
      END

      -- Inserta Total
      IF EXISTS(SELECT * FROM @RvSector)
         INSERT INTO @RvSector (
            DSC_SECTOR,
            MONTO_MON_CTA
         )
         SELECT   'Total',SUM(MONTO_MON_CTA) FROM @RvSector

      -- Imprime resultados
       SELECT   'SectoresEconomicos'      AS InicioBloque,
            DSC_SECTOR  AS SectoresEconomicos,
            MONTO_MON_CTA            AS Monto
         FROM @RvSector

      IF @VECNOM <> ''
          SELECT   'Grafico'               AS InicioBloque,
               @VECNOM                  AS VECNOM,
               @VECMTO                  AS VECMTO
      ELSE
         SELECT   ''               AS InicioBloque,
         @VECNOM                  AS VECNOM,
         @VECMTO                  AS VECMTO

      --***********************************************************************************************
      -- RENTA FIJA NACIONAL
      --***********************************************************************************************

      INSERT INTO @RentaFija (
            TipoInstrumento,
            Nemotecnico,
            Emisor,
            Moneda,
            PorcentajeInversion,
            Nominales,
            TasaCupon,
            FechaVencimiento,
            TasaCompra,
            TasaMercado,
            Valorizacion,
   duration)
       SELECT
            dsc_arbol_clase_inst,
   nemotecnico,
            cod_emisor,--EMISOR,
            SIMBOLO_MONEDA,
            CAST(ROUND(porcentaje_rama,2) AS NUMERIC(6,2)),
            cantidad,
            CAST(tasa_emision AS VARCHAR(10)),
            CONVERT(VARCHAR(8),FECHA_VENCIMIENTO,112),
            CAST(tasa_compra AS NUMERIC(6,2)),
            CAST(tasa AS NUMERIC(6,2)),
            CAST(ROUND(monto_mon_cta,0) AS BIGINT),
            duration
         FROM @SaldosActivos
        WHERE dsc_padre_arbol_clase_inst = 'Renta Fija Nacional'--'RENTA FIJA'

      INSERT INTO @RentaFija (
            TipoInstrumento,
            PorcentajeInversion,
            TasaCompra,
            Valorizacion)
       SELECT
            dsc_arbol_clase_inst,
            100,
            SUM(CAST(tasa_compra AS NUMERIC(6,2))),
            SUM(CAST(ROUND(monto_mon_cta,0) AS BIGINT))
         FROM @SaldosActivos
        WHERE dsc_padre_arbol_clase_inst ='Renta Fija Nacional' -- 'RENTA FIJA'
      GROUP BY dsc_arbol_clase_inst


       SELECT   'RentaFija'      AS InicioBloque,
            TipoInstrumento,
            Nemotecnico = ISNULL(Nemotecnico, ''),
            Emisor = ISNULL(Emisor, ''),
            Moneda = ISNULL(Moneda, ''),
            PorcentajeInversion,
            Nominales,
            TasaCupon,
            FechaVencimiento,
            TasaCompra,
  TasaMercado,
            Valorizacion,
            duration as duracion
         FROM @RentaFija
      ORDER BY TipoInstrumento, ID_TABLA

      --***********************************************************************************************
      -- INFORMES DE CAJA
      --***********************************************************************************************



 INSERT INTO @SaldoInicial
  EXEC CSGPI.dbo.PKG_SALDOS_CAJA$Buscar_Saldos_Consolidado @ID_EMPRESA, @Consolidado,@ID_TIPOCUENTA,NULL,@ID_CLIENTE,NULL,@FechaDesde


 INSERT INTO @SaldoFinal
  EXEC CSGPI.dbo.PKG_SALDOS_CAJA$Buscar_Saldos_Consolidado @ID_EMPRESA, @Consolidado,@ID_TIPOCUENTA,NULL,@ID_CLIENTE,NULL,@FechaHasta

 DECLARE @id_Moneda_c           INT,@SaldoCaja_c           NUMERIC(18,4)

 DECLARE cSaldosCajas CURSOR FOR
 SELECT * FROM  @SaldoInicial

 OPEN    cSaldosCajas

 FETCH cSaldosCajas INTO    @id_Moneda_c,@SaldoCaja_c

  WHILE (@@FETCH_STATUS = 0 )
  BEGIN
  --**********************************************************
   --SELECT 'cursor ', @id_Moneda_c , '_' , @SaldoCaja_c
    SET @SALDO = ISNULL((SELECT top 1 SaldoCaja FROM @SaldoInicial where id_Moneda =@id_Moneda_c),0)
    --print 'print 2'
    INSERT INTO @InformesCajas (
    FECHA_LIQUIDACION,
    DESCRIPCION,
    SALDO)
 VALUES( CONVERT(DATETIME, @FechaDesde, 112), 'SALDO ANTERIOR', @SALDO)


   select @fecha_desde_caja=CONVERT(VARCHAR(8), DATEADD(DAY,1,@FechaDesde), 112)
    INSERT INTO @InformesCajas (
    ID_CUENTA,
    ID_OPERACION,
    FECHA_LIQUIDACION,
    DESCRIPCION,
    MONEDA,
    MONTO_MOVTO_ABONO,
    MONTO_MOVTO_CARGO)
    EXEC CSGPI.dbo.PKG_CARTOLA$EntregaMovimientosCaja_Consolidado @ID_EMPRESA,@id_Moneda_c,@fecha_desde_caja,@FechaHasta,@Consolidado,@ID_TIPOCUENTA,@ID_CLIENTE,NULL

   UPDATE @InformesCajas
  SET DSC_CAJA_CUENTA = @id_Moneda_c
  WHERE DSC_CAJA_CUENTA IS NULL
  --FIN

    UPDATE @InformesCajas
    SET ID_MONEDA = @id_Moneda_c
    WHERE ID_MONEDA IS NULL

--**

    SET @SALDO = ISNULL((SELECT   top 1 SaldoCaja FROM @SaldoFinal where id_Moneda=@id_Moneda_c), 0)

     INSERT INTO @InformesCajas (
      DESCRIPCION,
      MONTO_MOVTO_ABONO,
      MONTO_MOVTO_CARGO,
      SALDO,
      DSC_CAJA_CUENTA,
      ID_MONEDA )
       SELECT
      'TOTALES',
      SUM(MONTO_MOVTO_ABONO),
      SUM(MONTO_MOVTO_CARGO),
      @SALDO,
      @id_Moneda_c,
      @id_Moneda_c
      FROM @InformesCajas
      WHERE ID_MONEDA =  @id_Moneda_c
       AND DSC_CAJA_CUENTA= @id_Moneda_c --12-04-2012




    --**********************************************************
  FETCH cSaldosCajas INTO  @id_Moneda_c,@SaldoCaja_c
  END

 CLOSE cSaldosCajas
 DEALLOCATE cSaldosCajas

--****************************************************

 SET @CANTIDAD2 = ISNULL((SELECT COUNT(ID_TABLA) FROM @InformesCajas),0)
      SET @CONT2 = 1

      WHILE @CONT2 <= @CANTIDAD2
      BEGIN
      IF (SELECT MONTO_MOVTO_ABONO FROM @InformesCajas WHERE ID_TABLA = @CONT2) IS NOT NULL
      BEGIN
      SET @SALDO_ANTERIOR = ISNULL((SELECT SALDO FROM @InformesCajas WHERE ID_TABLA = (@CONT2 - 1)), 0)

      UPDATE @InformesCajas
         SET SALDO =  MONTO_MOVTO_ABONO - MONTO_MOVTO_CARGO + @SALDO_ANTERIOR
       WHERE ID_TABLA = @CONT2
      AND DESCRIPCION NOT IN ('TOTALES')

      END
      SET @CONT2 = @CONT2 + 1
      END

       SELECT   'InformesCajas'                                    AS InicioBloque,
      CASE DSC_CAJA_CUENTA
      WHEN '1'  THEN 'Caja Pesos'
      WHEN '2' THEN 'Caja Dolar'
      WHEN '3'      THEN 'Caja Dolar Internacional'
      ELSE 'Caja Otros'
     END           AS TipoCaja,
      ID_OPERACION                                    AS NumeroOperacion,
      ISNULL(CONVERT(VARCHAR(8), FECHA_LIQUIDACION, 112), '')     AS FechaLiquidacion,
      DESCRIPCION                                       AS Detalle,
      CAST(MONTO_MOVTO_ABONO AS NUMERIC(18,4))                     AS Ingreso,
      CAST(MONTO_MOVTO_CARGO AS NUMERIC(18,4))                     AS Egreso,
      CAST(SALDO AS NUMERIC(18,4))                              AS Saldo
      FROM @InformesCajas
      ORDER BY ID_TABLA
         --END

         select 'Cuentas'         AS InicioBloque,
             num_cuenta              AS NumCuenta
         from @Cuentas

   --***********************************************************************************************
   -- INFORME DE TRANSACCIONES
   --***********************************************************************************************
   INSERT INTO @InformeTransacciones
  EXEC CSGPI.dbo.PKG_CARTOLA$DetalleMovimientos
  @PCONSOLIDADO  = 'CLT'
 , @PID_ENTIDAD   = @ID_CLIENTE --898
 , @PFECHA_INICIO = @FechaDesde
 , @PFECHA_FIN    = @FechaHasta
 , @PID_EMPRESA   = @ID_EMPRESA --4


 INSERT INTO #InformeTransaccionesFinal (
   FECHA_OPERACION
  ,FECHA_CORTE
  ,TIPO_MOVIMIENTO
  ,DSC_TIPO_MOVIMIENTO
  ,NUM_CUENTA
  ,ID_OPERACION
  ,FACTURA
  ,NEMOTECNICO
  ,CANTIDAD
  ,MONEDA
  ,DECIMALES_MOSTRAR
  ,PRECIO
  ,COMISION
  ,DERECHOS
  ,GASTOS
  ,IVA
  ,MONTO
  ,MONTO_TOTAL_OPERACION
  ,DSC_TIPO_OPERACION
  ,CONTRAPARTE
  )SELECT
    FECHA_OPERACION
  ,FECHA_CORTE
  ,TIPO_MOVIMIENTO
  ,DSC_TIPO_MOVIMIENTO
  ,NUM_CUENTA
  ,ID_OPERACION
  ,FACTURA
  ,NEMOTECNICO
  ,CANTIDAD
  ,MONEDA
  ,DECIMALES_MOSTRAR
  ,PRECIO
  ,COMISION
  ,DERECHOS
  ,GASTOS
  ,IVA
  ,MONTO
  ,MONTO_TOTAL_OPERACION
  ,DSC_TIPO_OPERACION
  ,CONTRAPARTE
  from @InformeTransacciones



 SELECT
 'InformeTransacciones' AS InicioBloque
 ,DSC_CUENTA AS DescripcionCaja
 ,ISNULL(CONVERT(VARCHAR(8), FECHA_OPERACION, 112), '') AS FechaOperacion
 ,ISNULL(CONVERT(VARCHAR(8), FECHA_CORTE, 112), '') AS FechaCorte
 ,TIPO_MOVIMIENTO AS TipoMovimiento
 ,DSC_TIPO_MOVIMIENTO AS DscTipoMovimiento
 ,NUM_CUENTA AS NumCuenta
 ,ID_OPERACION AS IdOperacion
 ,FACTURA AS Factura
 ,NEMOTECNICO AS Nemotecnico
 ,CANTIDAD AS Cantidad
 ,MONEDA As Moneda
 ,DECIMALES_MOSTRAR AS DecimalesMostrar
 ,PRECIO AS Precio
 ,COMISION AS Comision
 ,DERECHOS AS Derecho
 ,GASTOS AS Gastos
 ,IVA AS Iva
 ,MONTO AS Monto
 ,MONTO_TOTAL_OPERACION  AS MontoTotalOperacion
 ,DSC_TIPO_OPERACION AS DscTipoOperacion
 ,CONTRAPARTE
  FROM #InformeTransaccionesFinal

  DROP TABLE #InformeTransaccionesFinal

   END TRY
   BEGIN CATCH
      SET @CodErr = @@ERROR
      SET @MsgErr = 'Error en el Procedimiento sp_CartolaApvFlexibleConsDetalleWeb_temp:' + ERROR_MESSAGE()
   END CATCH

END
GO

GRANT EXECUTE ON [sp_CartolaApvFlexibleConsDetalleWeb_temp] TO DB_EXECUTESP
GO
