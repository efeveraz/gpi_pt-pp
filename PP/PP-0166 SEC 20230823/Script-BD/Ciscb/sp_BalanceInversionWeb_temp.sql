IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[sp_BalanceInversionWeb_temp]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [DBO].[sp_BalanceInversionWeb_temp]
GO

CREATE PROCEDURE [DBO].[sp_BalanceInversionWeb_temp]
 @RutCliente   VARCHAR(15)
,@NroCuenta    INT             =NULL
,@FechaDesde   VARCHAR(8)
,@FechaHasta   VARCHAR(8)
,@CodErr       INT             OUTPUT
,@MsgErr       VARCHAR(4000)   OUTPUT
AS
BEGIN
   SET NOCOUNT ON;
--declare
-- @RutCliente   VARCHAR(15)
--,@NroCuenta    INT
--,@FechaDesde   VARCHAR(8)
--,@FechaHasta   VARCHAR(8)
--,@CodErr       INT
--,@MsgErr       VARCHAR(4000)
--select
-- @RutCliente = '78938830-K'
--,@NroCuenta  = '00209'
--,@FechaDesde = '20220201'
--,@FechaHasta = '20220228'
--,@CodErr     = null
--,@MsgErr     = null
 --***********************************************************************************************
 -- DECLARACION DE VARIABLES
 --***********************************************************************************************
 DECLARE @DATE_DESDE              DATETIME
 DECLARE @ID_CUENTA               INT
 DECLARE @PID_CAJA_CUENTA         INT
 DECLARE @pId_Moneda_Salida       INT
 DECLARE @TotalValorActual        FLOAT --BIGINT
 DECLARE @TotalValorAnterior      FLOAT-- BIGINT
 DECLARE @CajaActual              NUMERIC(18,4)--BIGINT
 DECLARE @CajaAnterior            NUMERIC(18,4)--BIGINT
 DECLARE @CobrarActual            NUMERIC(18,4) --BIGINT
 DECLARE @CobrarAnterior          NUMERIC(18,4) --BIGINT
 DECLARE @PagarActual             NUMERIC(18,4) --BIGINT
 DECLARE @PagarAnterior           NUMERIC(18,4) --BIGINT
 DECLARE @TotalPasivoActual       FLOAT--BIGINT
 DECLARE @TotalPasivoAnterior     FLOAT --BIGINT
 DECLARE @PagarAnteriorForwards   NUMERIC(18,4) --BIGINT
 DECLARE @PagarActualForwards     NUMERIC(18,4) --BIGINT

 DECLARE @PorcentajeCaja   NUMERIC(10,2)
 DECLARE @PorcentajeRV     NUMERIC(10,2)
 DECLARE @PorcentajeRF     NUMERIC(10,2)
 DECLARE @PorcentajeFFMM   NUMERIC(10,2)
 DECLARE @PorcentajeRVI    NUMERIC(10,2)
 DECLARE @PorcentajeRFI    NUMERIC(10,2)

 DECLARE @VECNOM     VARCHAR(MAX)
 DECLARE @VECMTO     VARCHAR(MAX)
 DECLARE @ID_CLIENTE INT
 DECLARE @DOLARDESDE FLOAT
 DECLARE @DOLAHASTA  FLOAT
 DECLARE @Decimales  INT

 DECLARE @SumaCaja NUMERIC(10,2)
 DECLARE @SumaVNVI NUMERIC(10,2)
 DECLARE @SumaFNFI NUMERIC(10,2)
 DECLARE @SumaOT   NUMERIC(10,2)

 DECLARE @ContGra         INT
 DECLARE @CantidadContGra INT
 DECLARE @VECNOM_1        VARCHAR(MAX)
 DECLARE @VECMTO_1        VARCHAR(MAX)
 declare @FECHA_OPERATIVA datetime
 declare @fechajj         datetime
 declare @fecha_final     datetime
 declare @INTERVALO       INT
 DECLARE @MIN_VECMTO      NUMERIC(18, 6)
 DECLARE @MAX_VECMTO      NUMERIC(18, 6)

 declare @comisioHasta NUMERIC(18,6)
 declare @comisioDesde NUMERIC(18,6)
 declare @cuentasHasta NUMERIC(18,6)
 declare @cuentasDesde NUMERIC(18,6)

 --***********************************************************************************************
 -- DECLARACION DE TABLAS
 --***********************************************************************************************
 DECLARE @Servicio TABLE (
  ID                  INT    IDENTITY(1,1)
, Nombre              VARCHAR(100)
, ValorActual         float--BIGINT
, PorcentajeActual    NUMERIC(10,2)
, FechaHasta          VARCHAR(8)
, ValorAnterior       float   --BIGINT
, PorcentajeAnterior  NUMERIC(10,2)
, FechaDesde          VARCHAR(8)
, TipoDato            VARCHAR(100)  --06-01-2014
, Bloque              INT )

 DECLARE @Renta TABLE (
  ID                          INT
, DSC_ARBOL                   VARCHAR(100)
, NIVEL                       INT
, MONTO_MON_CTA               float --BIGINT
, MONTO_MON_CTA_ANTERIOR      float --BIGINT
, VALOR_HOJA                  INT
, PADRE                       INT
, ID_ARBOL_CLASE_INST         INT
, ID_EMPRESA                  INT
, ID_PADRE_ARBOL_CLASE_INST   INT
, DSC_ARBOL_CLASE_INST        VARCHAR(100)
, ORDEN                       INT
, CODIGO                      VARCHAR(10)
, ID_ACI_TIPO                 INT
, MONTO_ACTUAL                float --BIGINT
, MONTO_ANTERIOR              float --BIGINT
, FECHA_ULTIMO_CIERRE         DATETIME
, FECHA_MES_ANTERIOR          DATETIME)

 DECLARE @Patrimonio TABLE (
  ID_SALDO_CAJA            NUMERIC
, ID_CAJA_CUENTA           NUMERIC
, FECHA_CIERRE             DATETIME
, ID_MONEDA_CAJA           NUMERIC
, MONTO_MON_CAJA           FLOAT
, ID_MONEDA_CTA            NUMERIC
, MONTO_MON_CTA            FLOAT
, ID_CIERRE                NUMERIC
, MONTO_X_COBRAR_MON_CTA   FLOAT
, MONTO_X_PAGAR_MON_CTA    FLOAT
, MONTO_X_COBRAR_MON_CAJA  FLOAT
, MONTO_X_PAGAR_MON_CAJA   FLOAT)

--***EN 203 D005
 DECLARE @Rentabilidades  TABLE(
  fecha_cierre                    DATETIME
, rentabilidad_mon_cuenta         NUMERIC(18,4)
, rentabilidad_mensual            NUMERIC(18,4)
, hay_valor_cuota_mes             VARCHAR(10)
, rentabilidad_anual              NUMERIC(18,4)
, hay_valor_cuota_ano_calendario  VARCHAR(10)
, rentabilidad_ult_12_meses       NUMERIC(18,4)
, hay_valor_cuota_ult_12_meses    VARCHAR(10)
, volatilidad_mensual             NUMERIC(18,4)
, volatilidad_anual               NUMERIC(18,4)
, volatilidad_ult_12_meses        NUMERIC(18,4)
, rentabilidad_inicio_cuenta      NUMERIC(18,2)
, rentabilidad_mensual_$$         NUMERIC(18,2)
, rentabilidad_anual_$$           NUMERIC(18,2)
, rentabilidad_ult_12_meses_$$    NUMERIC(18,2)
, rentabilidad_inicio_cuenta_$$   NUMERIC(18,2)
, rentabilidad_mensual_UF         NUMERIC(18,2)
, rentabilidad_anual_UF           NUMERIC(18,2)
, rentabilidad_ult_12_meses_UF    NUMERIC(18,2)
, rentabilidad_inicio_cuenta_UF   NUMERIC(18,2)
, rentabilidad_mensual_DO         NUMERIC(18,2)
, rentabilidad_anual_DO           NUMERIC(18,2)
, rentabilidad_ult_12_meses_DO    NUMERIC(18,2)
, rentabilidad_inicio_cuenta_DO   NUMERIC(18,2))

 DECLARE @Rentabilidades_OUT TABLE(
  Nombre  VARCHAR(50)
, Peso    NUMERIC(24,2)
, Uf      NUMERIC(24,2)
, Dolar   NUMERIC(24,2) )

 DECLARE @ActivosMoneda TABLE(
  MONEDA      VARCHAR(30)
, PORCENTAJE  FLOAT
, MONTO       FLOAT)

 DECLARE @TMP_SALIDA TABLE (
  MONEDA      VARCHAR(30)
, PORCENTAJE  FLOAT
, MONTO       FLOAT)

 DECLARE @ComCartera TABLE(
  SumaCaja  NUMERIC(10,2)
, SumaVNVI  NUMERIC(10,2)
, SumaFNFI  NUMERIC(10,2)
, SumaOT    NUMERIC(10,2))

 DECLARE @Flujo TABLE(
  FECHA_MOVIMIENTO         DATETIME
, FLG_TIPO_MOVIMIENTO      VARCHAR(100)
, MONTO                    FLOAT(53)
, MONTO_UF                 FLOAT(53)
, MONTO_TOTAL_RESCATES     FLOAT
, MONTO_TOTAL_RESCATES_UF  FLOAT
, MONTO_TOTAL_APORTES      FLOAT
, MONTO_TOTAL_APORTES_UF   FLOAT)

 DECLARE @FlujoPatrimonial TABLE(
  Nombre   VARCHAR(100)
, Aportes  NUMERIC(18,2)
, Retiros  NUMERIC(18,2))

 DECLARE @DolarObsDesde TABLE(
  DOLARDESDE  FLOAT)

 DECLARE @DolarObsHasta TABLE(
  DOLARHASTA  FLOAT)

 DECLARE @GlosaMoneda TABLE(
  ID_TABLA    INT IDENTITY(1,1)
, DSC_MONEDA  VARCHAR(50))

 DECLARE @SALIDA_GRAFICO TABLE(
  FECHA_CIERRE     DATETIME
, VALOR_CUOTA      NUMERIC(18, 6)
, RENTABILIDAD     NUMERIC(18, 6)
, VALOR_CUOTA_100  NUMERIC(18, 6)
, RENTAB_ACUM2     NUMERIC(18, 6)
, RENTAB_ACUM      NUMERIC(18, 6))

 DECLARE @PARAMETRO_GRAFICO TABLE(
  ID_TABLA         INT  IDENTITY(1,1)
, DIAS             INT
, FECHA_CIERRE     DATETIME
, VALOR_CUOTA      NUMERIC(18, 6)
, RENTABILIDAD     NUMERIC(18, 6)
, VALOR_CUOTA_100  NUMERIC(18, 6)
, RENTAB_ACUM      NUMERIC(18, 6))

 DECLARE @Cuentas TABLE (
  ID_TABLA                       INT IDENTITY(1,1)
, ID_CUENTA                      INT NULL
, ID_CONTRATO_CUENTA             INT NULL
, NUM_CUENTA                     INT NULL
, ABR_CUENTA                     VARCHAR(30) NULL
, DSC_CUENTA                     VARCHAR(100) NULL
, OBSERVACION                    VARCHAR(100) NULL
, FLG_BLOQUEADO                  VARCHAR(1) NULL
, OBS_BLOQUEO                    VARCHAR(100) NULL
, FLG_IMP_INSTRUCCIONES          VARCHAR(1) NULL
, id_cliente                     INT      NULL
, rut_cliente                    VARCHAR(15)  NULL
, razon_social                   VARCHAR(500) NULL
, nombre_cliente                 VARCHAR(100) NULL
, ID_TIPO_ESTADO                 INT         NULL
, COD_ESTADO                     VARCHAR(1)  NULL
, DSC_ESTADO                     VARCHAR(30) NULL
, COD_TIPO_ADMINISTRACION        VARCHAR(10) NULL
, DSC_TIPO_ADMINISTRACION        VARCHAR(30) NULL
, ID_EMPRESA                     INT         NULL
, DSC_EMPRESA                    VARCHAR(100) NULL
, ID_ASESOR                      INT        NULL
, DSC_ASESOR                     VARCHAR(100) NULL
, ID_PERFIL_RIESGO               INT         NULL
, DSC_PERFIL_RIESGO              VARCHAR(100)  NULL
, PORC_SALTO_CUOTA               NUMERIC(18,4) NULL
, ID_MONEDA                      INT         NULL
, FECHA_OPERATIVA                DATETIME NULL
, FLG_MOV_DESCUBIERTOS           VARCHAR(2)  NULL
, PORCEN_RF                      NUMERIC(18,4) NULL
, PORCEN_RV                      NUMERIC(18, 4) NULL
, EMAIL                          VARCHAR(60)  NULL
, FONO                           VARCHAR(20)  NULL
, ABR_NOMBRE                     VARCHAR(50)  NULL
, Rut_asesor                     VARCHAR(15)  NULL
, fecha_cierre_contrato          DATETIME   NULL
, Fecha_Cierre_Cuenta            DATETIME    NULL
, NUMERO_FOLIO                   NUMERIC(18,0) NULL
, ID_TIPOCUENTA                  INT         NULL
, FECHA_CONTRATO                 DATETIME  NULL
, DESCRIPCION_CORTA              VARCHAR(100) NULL
, DESCRIPCION_LARGA              VARCHAR(100) NULL
, TIPO_AHORRO                    INT NULL
, Fecha_Cierre_Virtual           DATETIME  NULL
, Flg_Considera_Com_VC           VARCHAR(1)  NULL
, COD_ESTADO_CLIENTE             VARCHAR(2)  NULL
, nombre_asesor                  VARCHAR(100) NULL
, FLG_COMISION_AFECTA_IMPUESTO   VARCHAR(2)  NULL
, Email_cliente                  VARCHAR(200)  NULL
, fecha_min_operativa            DATETIME NULL)


---
 DECLARE @patrimonioSalida TABLE (
  ID_PATRIMONIO_CUENTA               INT
, id_cuenta                          INT
, fecha_cierre                       datetime
, id_moneda_cuenta                   INT
, COMI_DEVENG_MON_CTA                numeric(18,4)
, saldo_caja_mon_cuenta              numeric(18,4)
, saldo_activo_mon_cuenta            numeric(18,4)
, patrimonio_uf                      numeric(18,4)
, patrimonio_usd                     numeric(18,4)
, patrimonio_mon_cuenta              numeric(18,4)
, patrimonio_$$                      numeric(18,4)
, patrimonio_mon_cuenta_mda_cta      numeric(18,4)
, id_moneda_empresa                  INT
, saldo_caja_mon_empresa             numeric(18,4)
, saldo_activo_mon_empresa           numeric(18,4)
, patrimonio_mon_empresa             numeric(18,4)
, valor_cuota_mon_cuenta             numeric(18,4)
, total_cuotas_mon_cuenta            numeric(18,4)
, rentabilidad_mon_cuenta            numeric(18,4)
, aporte_retiros_mon_cuenta          numeric(18,4)
, id_cierre                          INT
, monto_x_cobrar_mon_cta             numeric(18,4)
, monto_x_pagar_mon_cta              numeric(18,4)
, monto_x_cobrar_mon_empresa         numeric(18,4)
, monto_x_pagar_mon_empresa          numeric(18,4)
, patrimonio_mes_anterior            numeric(18,4)
, comisiones_devengadas              numeric(18,4)
, comision                           numeric(18,4)
, SIMULTANEAS                        numeric(18,4))

  SET @CodErr = 0
  SET @MsgErr = ''
  SET @CajaActual =0
  SET @CajaAnterior = 0
  SET @CobrarActual = 0
  SET @CobrarAnterior = 0
  SET @PagarActual = 0
  SET @PagarAnterior = 0
  SET @DOLARDESDE =0
  SET @DOLAHASTA =0

  SELECT @ID_CLIENTE = Cu.ID_CLIENTE
    FROM CSGPI.dbo.CUENTAS  AS Cu with (NOLOCK)
   INNER JOIN CSGPI.dbo.CLIENTES  AS Cl   with (NOLOCK) ON Cu.ID_CLIENTE = Cl.id_cliente
   WHERE Cl.RUT_CLIENTE = @RutCliente

 DECLARE @PRODUCTOS TABLE (
  TipoAhorro VARCHAR(100)
, Orden      SMALLINT
, Glosa      VARCHAR(100)
, Valor      SMALLINT
, CARTOLA    CHAR(1))

  INSERT INTO @PRODUCTOS
  exec [FFMM].mg_safmutnw.dbo.EC_sp_ListadoProductos @pCodErr = 0, @pMsgErr = '', @pBalanceInversion = 'I'

  IF @NroCuenta IS NULL OR @NroCuenta = ''
  BEGIN
     SELECT TOP 1 @ID_CUENTA = ID_CUENTA FROM CSGPI.dbo.VIEW_CUENTAS with (NOLOCK) WHERE rut_cliente = @RutCliente
  END
  ELSE
  BEGIN
     SELECT @ID_CUENTA =  ID_CUENTA FROM CSGPI.dbo.VIEW_CUENTAS with (NOLOCK) WHERE (NUM_CUENTA = @NroCuenta AND rut_cliente = @RutCliente)
  END

  INSERT INTO @Cuentas
  exec CSGPI.dbo.PKG_CUENTAS$BUSCAR @ID_CUENTA,null,null,NULL,null,null,null,NULL,null,'CLT'

  IF @FechaHasta IS NULL OR @FechaHasta = ''
     SET @FechaHasta = CONVERT(VARCHAR(8),(CSGPI.dbo.FNT_EntregaFechaCierreCuenta(@ID_CUENTA)),112)

  IF @FechaDesde IS NULL OR @FechaDesde = ''
  BEGIN
     SET @DATE_DESDE = CONVERT(DATETIME,@FechaHasta,112)
     SET @DATE_DESDE = CONVERT(DATETIME,'01/' + CAST(MONTH(@DATE_DESDE) AS VARCHAR(2)) + '/' + CAST(YEAR(@DATE_DESDE) AS VARCHAR(4)),103)
     SET @FechaDesde = CONVERT(VARCHAR(8),DATEADD(d,-1,(@DATE_DESDE)),112)
  END

  --***********************************************************************************************
  -- PATRIMONIO  MONTOS ANTERIORES
  --***********************************************************************************************

  INSERT  INTO @DolarObsDesde
  exec [FFMM].mg_safmutnw.dbo.EC_sp_ParidadForwards  @FechaDesde,'DO', @CodErr ,@MsgErr
  INSERT  INTO @DolarObsHasta
  exec [FFMM].mg_safmutnw.dbo.EC_sp_ParidadForwards  @FechaHasta,'DO', @CodErr ,@MsgErr

  SELECT @DOLARDESDE =DOLARDESDE FROM @DolarObsDesde
  SELECT @DOLAHASTA = DOLARHASTA FROM @DolarObsHasta

 --**** CURSOR
 DECLARE CursorCuenta CURSOR FOR
  SELECT ID_CAJA_CUENTA
       , ID_MONEDA
    FROM CSGPI.dbo.CAJAS_CUENTA with (NOLOCK)
   WHERE ID_CUENTA IN (@ID_CUENTA)

 OPEN CursorCuenta
 FETCH NEXT FROM CursorCuenta
 INTO
      @PID_CAJA_CUENTA
    , @pId_Moneda_Salida

 WHILE @@FETCH_STATUS = 0
 BEGIN

    select @pId_Moneda_Salida= ID_MONEDA from CSGPI.dbo.CUENTAS with (NOLOCK) where ID_CUENTA=@ID_CUENTA

  --****** Fecha Anterior
    INSERT INTO @Patrimonio
    exec CSGPI.dbo.PKG_SALDOS_CAJA$BUSCAR @PID_CAJA_CUENTA, @FechaDesde, NULL, @ID_CUENTA, @pId_Moneda_Salida

    IF EXISTS(SELECT ID_SALDO_CAJA FROM @Patrimonio)
    BEGIN
	   IF EXISTS(SELECT 1 FROM @Patrimonio WHERE ID_MONEDA_CTA=1)--ID_MONEDA_CAJA=1)
       BEGIN
          SET @CobrarAnterior = @CobrarAnterior + ISNULL((SELECT monto_x_cobrar_mon_cta FROM @Patrimonio), 0)
          SET @CajaAnterior = @CajaAnterior + ISNULL((SELECT MONTO_MON_CTA FROM @Patrimonio), 0)
          SET @PagarAnterior = @PagarAnterior + ISNULL((SELECT monto_x_pagar_mon_cta FROM @Patrimonio), 0)
       END

       IF EXISTS(SELECT 1 FROM @Patrimonio WHERE ID_MONEDA_CTA=2)--ID_MONEDA_CAJA=2)
       BEGIN
          SET @CobrarAnterior = @CobrarAnterior + (ISNULL((SELECT monto_x_cobrar_mon_cta FROM @Patrimonio), 0))
          SET @CajaAnterior   = @CajaAnterior + (ISNULL((SELECT MONTO_MON_CTA FROM @Patrimonio), 0))
          SET @PagarAnterior  = @PagarAnterior + ISNULL((SELECT monto_x_pagar_mon_cta FROM @Patrimonio), 0)
       END

       DELETE FROM @Patrimonio
    END
  -- **** fin nuevo Fecha Anterior

    set @PagarActualForwards = 0
    set @PagarAnteriorForwards = 0
  ----------------------------------------------------
    SELECT @PagarActualForwards = SUM(SD.VMM)
      FROM CSGPI.dbo.VIEW_SALDOS_DERIVADOS SD
     WHERE SD.ID_CUENTA = @ID_CUENTA
       AND SD.FECHA_CIERRE = @FechaHasta
       AND SD.COD_INSTRUMENTO = 'FWD_NAC'

    SELECT @PagarAnteriorForwards = SUM(SD.VMM)
      FROM CSGPI.dbo.VIEW_SALDOS_DERIVADOS SD
     WHERE SD.ID_CUENTA = @ID_CUENTA
       AND SD.FECHA_CIERRE = @FechaDesde
       AND SD.COD_INSTRUMENTO = 'FWD_NAC'
  ----------------------------------------------------

    INSERT INTO @Patrimonio
    exec CSGPI.dbo.PKG_SALDOS_CAJA$BUSCAR @PID_CAJA_CUENTA, @FechaHasta, NULL, @ID_CUENTA, @pId_Moneda_Salida

    IF EXISTS(SELECT ID_SALDO_CAJA FROM @Patrimonio)
    BEGIN
       IF EXISTS(SELECT 1 FROM @Patrimonio WHERE ID_MONEDA_CTA=1)--ID_MONEDA_CAJA=1)
       BEGIN
          SET @CobrarActual = @CobrarActual + ISNULL((SELECT monto_x_cobrar_mon_cta FROM @Patrimonio), 0)
          SET @CajaActual = @CajaActual + ISNULL((SELECT MONTO_MON_CTA FROM @Patrimonio), 0)
          SET @PagarActual = @PagarActual + ISNULL((SELECT monto_x_pagar_mon_cta FROM @Patrimonio), 0)
       END

       IF EXISTS(SELECT 1 FROM @Patrimonio WHERE ID_MONEDA_CTA=2) --ID_MONEDA_CAJA=2)
       BEGIN
          SET @CobrarActual = @CobrarActual + (ISNULL((SELECT monto_x_cobrar_mon_cta FROM @Patrimonio), 0) )--* @DOLAHASTA)
          SET @CajaActual = @CajaActual + (ISNULL((SELECT MONTO_MON_CTA FROM @Patrimonio), 0) )--* @DOLAHASTA),0)
          SET @PagarActual = @PagarActual + ISNULL((SELECT monto_x_pagar_mon_cta FROM @Patrimonio), 0)
       END
       DELETE FROM @Patrimonio
    END
  --******fin Fecha Actual

  FETCH NEXT FROM CursorCuenta
  INTO @PID_CAJA_CUENTA
     , @pId_Moneda_Salida
 END --fin while puntero CursorCuenta
 CLOSE CursorCuenta
 DEALLOCATE CursorCuenta

 --**** fin CURSOR
select @pId_Moneda_Salida= ID_MONEDA from CSGPI.dbo.CUENTAS with (NOLOCK) where ID_CUENTA=@ID_CUENTA
set @Decimales =(select dicimales_mostrar from CSGPI.dbo.monedas with (NOLOCK) where id_moneda =@pId_Moneda_Salida)

 IF @CobrarAnterior  IS NULL SET @CobrarAnterior = 0
 IF @CajaAnterior  IS NULL SET @CajaAnterior = 0
 IF @PagarAnterior  IS NULL SET @PagarAnterior = 0
 --IF @PatrimonioAnterior IS NULL SET @PatrimonioAnterior = 0
 IF @CobrarActual  IS NULL SET @CobrarActual = 0
 IF @CajaActual   IS NULL SET @CajaActual = 0
 IF @PagarActual   IS NULL SET @PagarActual = 0
 --IF @PatrimonioActual IS NULL SET @PatrimonioActual = 0
  --fin nuevo
  -- Inserta Registros
  INSERT INTO @Servicio (
   Nombre,
   valorActual,
   FechaHasta,
   valorAnterior,
   FechaDesde,
   TipoDato,
   Bloque
  )
  VALUES(
   'Caja'
   ,@CajaActual
   ,@FechaHasta
   ,@CajaAnterior
   ,@FechaDesde
   ,'ACTIVOS'
   ,0 )

  INSERT INTO @Servicio (
   Nombre,
   valorActual,
   FechaHasta,
   valorAnterior,
   FechaDesde,
   TipoDato,
   Bloque
  )
  VALUES(
   'Cuentas por Cobrar'
   ,@CobrarActual
   ,@FechaHasta
   ,@CobrarAnterior
   ,@FechaDesde
   ,'ACTIVOS'
   ,0 )

  --***********************************************************************************************
  -- Acciones, Renta Fija y Fondos Mutuos Mixtos
  --***********************************************************************************************

  INSERT INTO @Renta (
   ID,
   DSC_ARBOL,
   NIVEL,
   MONTO_MON_CTA,
   MONTO_MON_CTA_ANTERIOR,
   VALOR_HOJA,
   PADRE,
   ID_ARBOL_CLASE_INST,
   ID_EMPRESA,
   ID_PADRE_ARBOL_CLASE_INST,
   DSC_ARBOL_CLASE_INST,
   ORDEN,
   CODIGO,
   ID_ACI_TIPO,
   MONTO_ACTUAL,
   MONTO_ANTERIOR,
   FECHA_ULTIMO_CIERRE,
   FECHA_MES_ANTERIOR
  )
  EXEC CSGPI.dbo.PKG_BALANCE_INVERSIONES_II$BUSCAR_POR_CUENTA @ID_CUENTA, @FechaDesde, @FechaHasta


  INSERT INTO @Servicio (
   Nombre,
   valorActual,
   FechaHasta,
   valorAnterior,
   FechaDesde,
   TipoDato,
   Bloque
  )
  SELECT
    DSC_ARBOL_CLASE_INST
    ,MONTO_ACTUAL
    ,@FechaHasta
    ,MONTO_ANTERIOR
    ,@FechaDesde
    ,DSC_ARBOL
    ,1
  FROM @Renta

  INSERT INTO @Servicio (
   Nombre,
   valorActual,
   FechaHasta,
   valorAnterior,
   FechaDesde,
   TipoDato,
   Bloque
  )
  SELECT
   'Total'
   ,SUM(MONTO_ACTUAL)
   ,@FechaHasta
   ,SUM(MONTO_ANTERIOR)
   ,@FechaDesde
   ,DSC_ARBOL
   ,1
  FROM @Renta
  GROUP BY DSC_ARBOL

  SELECT
   @TotalValorActual = SUM(ValorActual)
   ,@TotalValorAnterior = SUM(valorAnterior)
  FROM @Servicio
  WHERE Nombre = 'Total' OR Bloque = 0

---------------------------------------------------------------------------------------------------------------------------------------------------

  update @Servicio
  SET valorActual=case when isnull(@PagarActualForwards ,0) = 0 THEN  0
                 when isnull(@PagarActualForwards ,0) > 0 THEN isnull(@PagarActualForwards ,0)
                 when isnull(@PagarActualForwards ,0) < 0 THEN 0
                 end
  ,valorAnterior =case when isnull(@PagarAnteriorForwards ,0) = 0 THEN  0
                 when isnull(@PagarAnteriorForwards ,0) > 0 THEN isnull(@PagarAnteriorForwards ,0)
                 when isnull(@PagarAnteriorForwards ,0) < 0 THEN 0
                 end
  where nombre ='Forwards'



  INSERT INTO @Servicio (
   Nombre,
   valorActual,
   PorcentajeActual,
   FechaHasta,
   valorAnterior,
   PorcentajeAnterior,
   FechaDesde,
   TipoDato,
   Bloque
  )
  VALUES(
   'TOTAL ACTIVOS'
   ,ROUND(@TotalValorActual,@Decimales)
   ,100
   ,@FechaHasta
   ,ROUND(@TotalValorAnterior,@Decimales)
   ,100
   ,@FechaDesde
   ,'TOTAL ACTIVOS'
   ,2 )

  IF @TotalValorActual > 0
   UPDATE @Servicio
   SET PorcentajeActual = (CAST(ValorActual AS DECIMAL) * 100) / CAST(@TotalValorActual AS DECIMAL (18,4))
   WHERE PorcentajeActual IS NULL
  ELSE
   UPDATE @Servicio
   SET PorcentajeActual = 0
   WHERE PorcentajeActual IS NULL

  --IF @TotalValorAnterior <> 0
  IF @TotalValorAnterior > 0
   UPDATE @Servicio
   SET PorcentajeAnterior = (CAST(valorAnterior AS DECIMAL) * 100) / CAST(@TotalValorAnterior AS DECIMAL (18,4))
   WHERE PorcentajeAnterior IS NULL
  ELSE
   UPDATE @Servicio
   SET PorcentajeAnterior = 0
   WHERE PorcentajeAnterior IS NULL

  --***********************************************************************************************
  -- PASIVOS
  --***********************************************************************************************
DECLARE @flagSalidaComision varchar(5)
select @flagSalidaComision=Flg_Considera_Com_VC from @Cuentas



INSERT INTO @patrimonioSalida
EXEC CSGPI.DBO.PKG_PATRIMONIO_CUENTAS_WEB$Buscar @ID_CUENTA,@FechaHasta,@pId_Moneda_Salida

select @comisioHasta = comision
    ,@cuentasHasta=monto_x_pagar_mon_cta
from @patrimonioSalida

IF ( @comisioHasta IS NULL)
BEGIN
 SET @comisioHasta=0
END
IF ( @cuentasHasta IS NULL)
BEGIN
 SET @cuentasHasta=0
END


delete @patrimonioSalida

INSERT INTO @patrimonioSalida
EXEC CSGPI.DBO.PKG_PATRIMONIO_CUENTAS_WEB$Buscar @ID_CUENTA,@FechaDesde,@pId_Moneda_Salida
select @comisioDesde = comision
      ,@cuentasDesde=monto_x_pagar_mon_cta
from @patrimonioSalida

IF ( @comisioDesde IS NULL)
BEGIN
 SET @comisioDesde=0
END
IF ( @cuentasDesde IS NULL)
BEGIN
 SET @cuentasDesde=0
END


IF (LTRIM(RTRIM(@flagSalidaComision))= 'S')
begin
  INSERT INTO @Servicio (
   Nombre,
   valorActual,
   FechaHasta,
   valorAnterior,
   FechaDesde,
   TipoDato,
   Bloque
  )
  VALUES(
   'Comisi�n por Administraci�n'
   ,@comisioHasta --0
   ,@FechaHasta
   ,@comisioDesde --0
   ,@FechaDesde
   ,'PASIVOS'
   ,3 )
end

  INSERT INTO @Servicio (
   Nombre,
   valorActual,
   FechaHasta,
   valorAnterior,
   FechaDesde,
   TipoDato,
   Bloque
  )
  VALUES(
   'Cuentas por Pagar'
   ,@cuentasHasta--@PagarActual
   ,@FechaHasta
   ,@cuentasDesde--@PagarAnterior
   ,@FechaDesde
   ,'PASIVOS'
   ,3 )

 INSERT INTO @Servicio (
   Nombre,
   valorActual,
   FechaHasta,
   valorAnterior,
   FechaDesde,
   TipoDato,
   Bloque
  )
  VALUES(
   'Forwards'
   ,case when isnull(@PagarActualForwards ,0) = 0 THEN  0
   when isnull(@PagarActualForwards ,0) > 0 THEN  0
   when isnull(@PagarActualForwards ,0) < 0 THEN  @PagarActualForwards * -1
 end
   ,@FechaHasta
   , case when isnull(@PagarAnteriorForwards ,0) = 0 THEN  0
   when isnull(@PagarAnteriorForwards ,0) > 0 THEN  0
   when isnull(@PagarAnteriorForwards ,0) < 0 THEN  @PagarAnteriorForwards * -1
 end
   ,@FechaDesde
   ,'PASIVOS'
   ,3 )

---

  SELECT
   @TotalPasivoActual = SUM(ValorActual)
   ,@TotalPasivoAnterior = SUM(valorAnterior)
  FROM @Servicio
  WHERE Bloque = 3

  INSERT INTO @Servicio (
   Nombre,
   valorActual,
   FechaHasta,
   valorAnterior,
   FechaDesde,
   TipoDato,
   Bloque
  )           VALUES(
   'TOTAL PASIVOS'
   ,@TotalPasivoActual
   ,@FechaHasta
   ,@TotalPasivoAnterior
   ,@FechaDesde
   ,'PASIVOS'
   ,3 )

  --***********************************************************************************************
  -- TOTAL PATRIMONIO
  --***********************************************************************************************
  INSERT INTO @Servicio (
   Nombre,
   valorActual,
   FechaHasta,
   valorAnterior,
   FechaDesde,
   TipoDato,
   Bloque
  )
  VALUES(
   'PATRIMONIO'
   ,round((@TotalValorActual - @TotalPasivoActual),@Decimales )
   ,@FechaHasta
   ,round((@TotalValorAnterior - @TotalPasivoAnterior),@Decimales)
   ,@FechaDesde
   ,'PATRIMONIO'
   ,4 )

----  --***********************************************************************************************
----  -- SALIDA DE DATOS : SERVICIOS
----  --***********************************************************************************************
  SELECT ID,
  'Servicio'  AS InicioBloque,
   TipoDato,
   Nombre,
   dbo.fn_CC_IncluirMilesCartola(ValorActual,@Decimales) as ValorActual,---convert(numeric(24,0),ValorActual) as ValorActual,
   isnull(PorcentajeActual,0) as PorcentajeActual,
   FechaHasta,
    dbo.fn_CC_IncluirMilesCartola(ValorAnterior,@Decimales) as ValorAnterior, --convert(numeric(24,0),ValorAnterior) as ValorAnterior,
   isnull(PorcentajeAnterior,0) as PorcentajeAnterior,
   FechaDesde
  FROM @Servicio
  --ORDER BY Bloque, TipoDato DESC, ID
 order BY (CASE TipoDato WHEN 'ACTIVOS' THEN 1
        WHEN 'Renta Fija Nacional' THEN 2
        WHEN 'Renta Fija Internacional' THEN 3
        WHEN 'Renta Variable Nacional' THEN 4
        WHEN 'Renta Variable Internacional' THEN 5
        WHEN 'Otros Activos' THEN 6
       WHEN 'TOTAL ACTIVOS' THEN 7
       WHEN 'PASIVOS' THEN 8
       WHEN 'PATRIMONIO' THEN 9
    ELSE 0
              END )
,ID
--end





  --***********************************************************************************************
  -- SALIDA DE DATOS : SERVICIOS
  --***********************************************************************************************

  --***********************************************************************************************
  -- RENTABILIDADES
  --***********************************************************************************************
  INSERT INTO @Rentabilidades
   EXEC CSGPI.dbo.PKG_PATRIMONIO_CUENTAS$RENTABILIDADES @ID_CUENTA, @FechaHasta

  INSERT INTO @Rentabilidades_OUT (
   Nombre,
   Peso,
   Uf,
   Dolar
  )
  SELECT
    'Rentabilidad Mensual'    AS Nombre
   ,rentabilidad_mensual_$$   AS Peso
   ,rentabilidad_mensual_UF   AS Uf
   ,rentabilidad_mensual_DO   AS Dolar
  FROM @Rentabilidades

  INSERT INTO @Rentabilidades_OUT (
   Nombre,
   Peso,
   Uf,
   Dolar
  )
  SELECT
    'Rentabilidad Acumulada Anual'  AS Nombre
   ,rentabilidad_anual_$$    AS Peso
   ,rentabilidad_anual_UF    AS Uf
   ,rentabilidad_anual_DO    AS Dolar
  FROM @Rentabilidades

  INSERT INTO @Rentabilidades_OUT (
   Nombre,
   Peso,
   Uf,
   Dolar
  )
  SELECT
    'Rentabilidad �ltimos 12 Meses' AS Nombre
   ,rentabilidad_ult_12_meses_$$  AS Peso
   ,rentabilidad_ult_12_meses_UF  AS Uf
   ,rentabilidad_ult_12_meses_DO  AS Dolar
  FROM @Rentabilidades

  SELECT
    'Rentabilidades'     AS InicioBloque,
    Nombre,
    Peso,
    Uf,
    Dolar
  FROM @Rentabilidades_OUT


  --***********************************************************************************************
  -- FLUJO PATRIMONIAL
  --***********************************************************************************************
  INSERT INTO @Flujo
   EXEC CSGPI.dbo.PKG_FLUJO_PATRIMONIAL$Buscar @ID_CUENTA, '20000101', @FechaHasta

  INSERT INTO @FlujoPatrimonial (
   Nombre,
   Aportes,
   Retiros
  )
  SELECT   TOP 6
    CONVERT(VARCHAR(8),FECHA_MOVIMIENTO,112) AS Nombre
   ,CASE FLG_TIPO_MOVIMIENTO
    WHEN 'APORTE' THEN MONTO
    ELSE 0
    END          AS aportes
   ,CASE FLG_TIPO_MOVIMIENTO
    WHEN 'RETIRO' THEN MONTO
    ELSE 0
    END          AS retiros
  FROM @Flujo

  -- TOTALES
  INSERT INTO @FlujoPatrimonial (
   Nombre,
   Aportes,
   Retiros
  )
  SELECT  TOP 1
     CASE @pId_Moneda_Salida when 1 then 'TOTAL EN PESOS'
        when 2 then 'TOTAL EN DOLAR OBS.' END AS Nombre  --14-04-2014
   ,MONTO_TOTAL_APORTES   AS aportes
   ,MONTO_TOTAL_RESCATES  AS retiros
  FROM @Flujo

  INSERT INTO @FlujoPatrimonial (
   Nombre,
   Aportes,
   Retiros
  )
  SELECT  TOP 1
    'TOTAL EN UF'    AS Nombre
,MONTO_TOTAL_APORTES_UF  AS aportes
   ,MONTO_TOTAL_RESCATES_UF AS retiros
  FROM @Flujo

  SELECT
   'FlujoPatrimonial'       AS InicioBloque,
   Nombre,
   Aportes,
   Retiros
  FROM @FlujoPatrimonial

  --***********************************************************************************************
  -- GENERA GRAFICO
  --***********************************************************************************************
  SELECT @PorcentajeCaja = sum(PorcentajeActual) FROM @Servicio WHERE Nombre IN ('Caja','Cuentas por Cobrar')
  SELECT @PorcentajeRV = PorcentajeActual FROM @Servicio WHERE (TipoDato = 'RENTA VARIABLE' OR TipoDato = 'Renta Variable Nacional') AND Nombre = 'Total'
  SELECT @PorcentajeRF = PorcentajeActual FROM @Servicio WHERE (TipoDato = 'RENTA FIJA' or TipoDato = 'Renta Fija Nacional') AND Nombre = 'Total'
  SELECT @PorcentajeFFMM = PorcentajeActual FROM @Servicio WHERE TipoDato = 'Otros Activos' AND (Nombre = 'Total')--TipoDato = 'FONDOS MUTUOS MIXTOS' AND Nombre = 'Total'

  SELECT @PorcentajeRVI = PorcentajeActual FROM @Servicio WHERE (TipoDato = 'Renta Variable Internacional') AND Nombre = 'Total'
  SELECT @PorcentajeRFI = PorcentajeActual FROM @Servicio WHERE (TipoDato = 'Renta Fija Internacional') AND Nombre = 'Total'

  SET @VECNOM = ''
  SET @VECMTO = ''

  IF @PorcentajeCaja > 0
  BEGIN
   SET @VECNOM = @VECNOM + 'Caja' + ';'
   SET @VECMTO = @VECMTO + REPLACE(CAST(@PorcentajeCaja AS  VARCHAR(10)),'.',',') + ';'
  END
  IF @PorcentajeRV > 0
  BEGIN
   SET @VECNOM = @VECNOM + 'Renta Variable Nacional' + ';'
   SET @VECMTO = @VECMTO + REPLACE(CAST(@PorcentajeRV AS  VARCHAR(10)),'.',',') + ';'
  END
  IF @PorcentajeRF > 0
  BEGIN
   SET @VECNOM = @VECNOM + 'Renta Fija Nacional' + ';'
   SET @VECMTO = @VECMTO + REPLACE(CAST(@PorcentajeRF AS  VARCHAR(10)),'.',',') + ';'
  END
  IF @PorcentajeFFMM > 0
  BEGIN
   SET @VECNOM = @VECNOM + 'Otros Activos' + ';'
   SET @VECMTO = @VECMTO + REPLACE(CAST(@PorcentajeFFMM AS  VARCHAR(10)),'.',',') + ';'
  END


  IF  @PorcentajeRVI > 0
  BEGIN
   SET @VECNOM = @VECNOM + 'Renta Variable Internacional' + ';'
   SET @VECMTO = @VECMTO + REPLACE(CAST(@PorcentajeRVI AS  VARCHAR(10)),'.',',') + ';'
  END

  IF  @PorcentajeRFI > 0
  BEGIN
   SET @VECNOM = @VECNOM + 'Renta Fija Internacional' + ';'
   SET @VECMTO = @VECMTO + REPLACE(CAST(@PorcentajeRFI AS  VARCHAR(10)),'.',',') + ';'
  END





  -- Elimina el �ltimo ;
if LEN(@VECNOM) > 1
BEGIN
  SET @VECNOM = SUBSTRING(@VECNOM,1,LEN(@VECNOM)-1)
  SET @VECMTO = SUBSTRING(@VECMTO,1,LEN(@VECMTO)-1)
END
  SELECT 'Grafico'     AS InicioBloque,
    @VECNOM      AS VECNOM,
    @VECMTO      AS VECMTO

  --***********************************************************************************************

--***********************************************************************************************
  -- ACTIVOS POR MONEDAS
  --***********************************************************************************************

--INSERT INTO @ActivosMoneda
--exec CSGPI.dbo.PKG_CARTOLA_CONS$BUSCAR_ACTIVOS_MONEDA 'CTA',@ID_CUENTA, @FechaHasta,@pId_Moneda_Salida

------------
INSERT INTO @ActivosMoneda VALUES ('Pesos',0,0)
INSERT INTO @ActivosMoneda VALUES ('UF',0,0)
INSERT INTO @ActivosMoneda VALUES ('D�lar',0,0)
INSERT INTO @ActivosMoneda VALUES ('Otro',0,0)
INSERT INTO @ActivosMoneda VALUES ('TOTAL',0,0)

insert into  @TMP_SALIDA exec CSGPI.dbo.PKG_CARTOLA_CONS$BUSCAR_ACTIVOS_MONEDA 'CTA',@ID_CUENTA, @FechaHasta,@pId_Moneda_Salida


UPDATE @ActivosMoneda
SET PORCENTAJE = t2.PORCENTAJE, MONTO = t2.MONTO
FROM @ActivosMoneda t1, @TMP_SALIDA t2
WHERE t1.MONEDA =t2.MONEDA

------------

SELECT
  CASE @pId_Moneda_Salida
   WHEN 1 THEN 'Monto en $'--'PESOS'
   WHEN 2 THEN 'Monto en USD'--'DOLAR'
  END       AS signoMoneda,
  MONEDA as moneda
 ,dbo.fn_CC_IncluirMilesCartola(PORCENTAJE,2) as porcentaje
 ,dbo.fn_CC_IncluirMilesCartola(MONTO,@Decimales) as monto
 FROM @ActivosMoneda

--***********************************************************************************************
  -- COMPOSICION CARTERA
  --***********************************************************************************************
SET @SumaCaja =@PorcentajeCaja
SET @SumaVNVI= (@PorcentajeRV + @PorcentajeRVI)
SET @SumaFNFI = (@PorcentajeRF + @PorcentajeRFI)
SET @SumaOT  = @PorcentajeFFMM


INSERT INTO @ComCartera VALUES (@SumaCaja,@SumaVNVI,@SumaFNFI,@SumaOT)

SELECT dbo.fn_CC_IncluirMilesCartola(SumaCaja,2) AS Caja,
    dbo.fn_CC_IncluirMilesCartola(SumaVNVI,2) as Rv,
    dbo.fn_CC_IncluirMilesCartola(SumaFNFI,2) as Rf,
    dbo.fn_CC_IncluirMilesCartola(SumaOT,2)   as Otra
FROM @ComCartera

--****Glosa Monedas

   INSERT INTO @GlosaMoneda
   select CASE @pId_Moneda_Salida
      WHEN 1 THEN 'MONEDA: PESOS'
      WHEN 2 THEN 'MONEDA: DOLAR OBS.'

     END

   select CASE isnull(@pId_Moneda_Salida,'')
      WHEN 1 THEN 'MONEDA: PESOS'
      WHEN 2 THEN 'MONEDA: DOLAR OBS.'
      ELSE ''
     END  AS GlosaMoneda
            from @GlosaMoneda


--***********************************************************************************************
  -- GENERA GRAFICO LINEA
  --***********************************************************************************************
SELECT @FECHA_OPERATIVA=FECHA_OPERATIVA
      FROM @Cuentas

 SET @ContGra = 1
    SET @VECNOM_1 = ''
    SET @VECMTO_1 = ''
    set @fecha_final=  convert(datetime,@FechaHasta)
   --***********************************************************************************************
   -- obtiene datos
   --***********************************************************************************************

 INSERT INTO @PARAMETRO_GRAFICO
      exec  [CSGPI].[dbo].[PKG_WEB$GRAFICO_RENTABILIDAD]
   @ID_CUENTA       --NUMERIC   = NULL
  ,null            -- @ID_CLIENTE      NUMERIC   = NULL
  , null                   --@PID_GRUPO
  ,@fecha_final        --DATETIME


SELECT TOP 1 @INTERVALO = DIAS   FROM @PARAMETRO_GRAFICO

  --select 'caida'
   INSERT INTO @SALIDA_GRAFICO
 exec CSGPI.dbo.PKG_PATRIMONIO_CUENTAS$RENTABILIDAD_PERIODOS_2
  @ID_CUENTA --<id_cuenta>
 ,NULL      --id_cliente
    ,null      --@PID_GRUPO
 ,@FECHA_OPERATIVA--<fecha_operativa>
 , @fecha_final --<fecha_final>*/

SELECT @MIN_VECMTO =  ROUND(MIN(RENTAB_ACUM),0) FROM @SALIDA_GRAFICO
SELECT @MAX_VECMTO = ROUND(MAX(RENTAB_ACUM),0,1) FROM @SALIDA_GRAFICO

IF @MIN_VECMTO>0
SELECT @MIN_VECMTO =  ROUND(MIN(RENTAB_ACUM),1)-1  FROM @SALIDA_GRAFICO
ELSE
SELECT @MIN_VECMTO =  ROUND(MIN(RENTAB_ACUM),0,1)-1  FROM @SALIDA_GRAFICO

IF @MAX_VECMTO>0
SELECT @MAX_VECMTO = ROUND(MAX(RENTAB_ACUM),0)+1  FROM @SALIDA_GRAFICO
ELSE
SELECT @MAX_VECMTO = ROUND(MAX(RENTAB_ACUM),0,1)+1  FROM @SALIDA_GRAFICO

SELECT  @VECNOM_1 = @VECNOM_1 + dbo.Fn_FechaAlfabeticaWeb(convert(datetime,(FECHA_CIERRE))) + ';'
      , @VECMTO_1 = @VECMTO_1 + CAST(dbo.fn_CC_IncluirMilesCartola(RENTAB_ACUM,2) AS  VARCHAR(20)) +';' FROM @SALIDA_GRAFICO

IF @VECNOM_1 !=''
BEGIN
SELECT InicioBloque = 'GraficoLine'
  , SUBSTRING(@VECNOM_1,1,len(@VECNOM_1)-1) as VECNOM_1
  , SUBSTRING(@VECMTO_1,1,len(@VECMTO_1)-1) as VECMTO_1
        ,@INTERVALO AS INTERVALO
, CAST(@MIN_VECMTO AS int) as MIN_VECMTO
 ,CAST(@MAX_VECMTO AS int) as MAX_VECMTO
END
ELSE
BEGIN
   SET @VECNOM_1 = ''
   SET @VECMTO_1 = ''
     SET @INTERVALO = 1
 SET @MIN_VECMTO = 0
 SET @MAX_VECMTO = 0

   SELECT InicioBloque = 'GraficoLine'
  , @VECNOM_1 as VECNOM_1
  , @VECMTO_1 as VECMTO_1
       ,@INTERVALO AS INTERVALO
 ,CAST(@MIN_VECMTO AS int) as MIN_VECMTO
 ,CAST(@MAX_VECMTO AS int) as MAX_VECMTO
END

/*
END TRY
 BEGIN CATCH
  SET @CodErr = @@ERROR
  SET @MsgErr = 'Error en el Procedimiento sp_BalanceInversionWeb:' + ERROR_MESSAGE()


 IF CURSOR_STATUS('global', 'CursorCuenta') > 0
 BEGIN
    CLOSE CursorCuenta
    DEALLOCATE CursorCuenta
 END
 END CATCH
*/
END
GO

GRANT EXECUTE ON [sp_BalanceInversionWeb_temp] TO DB_EXECUTESP
GO
