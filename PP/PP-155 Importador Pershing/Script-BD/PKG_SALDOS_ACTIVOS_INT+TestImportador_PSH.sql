USE [CSGPI]
GO

IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[PKG_SALDOS_ACTIVOS_INT$TestImportador_PSH]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
   DROP PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS_INT$TestImportador_PSH]
GO

CREATE PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS_INT$TestImportador_PSH]
( @PFECHA_CIERRE                 DATETIME
, @PCUENTA                       VARCHAR(50)
, @PCUSIP                        VARCHAR(30)
, @PFAMILIA_PRODUCTO             VARCHAR(50)
, @PNOMBRE                       VARCHAR(120)
, @PNOMBRE_CORTO                 VARCHAR(50)
, @PFECHA_POSICION               DATETIME
, @PMONEDA_POSICION              VARCHAR(10)
, @PCANTIDAD_ULTIMO_TRADE        FLOAT
, @PCOSTO_ORIGINAL               FLOAT
, @PPRECIOACTUAL                 FLOAT
, @PVALOR_MERCADO_USD            FLOAT
, @PRUT_CLIENTE                  VARCHAR(15)
, @PISIN                         VARCHAR(20) = NULL
) AS
BEGIN
	SET NOCOUNT ON

    DECLARE @LRUT_CLIENTE          VARCHAR(12)
          , @LID_CUENTA            NUMERIC
          , @LCODRESULTADO         VARCHAR(10)
          , @LMSGRESULTADO         VARCHAR(800)
          , @LCOD_ORIGEN           VARCHAR(20)
          , @LID_NEMOTECNICO       NUMERIC
          , @LNEMOTECNICO          VARCHAR(50)
          , @LCOD_INSTRUMENTO      VARCHAR(20)
          , @LID_MONEDA_USD        NUMERIC
          , @LID_MONEDA_EUR        NUMERIC
          , @LID_EMISOR_ESPECIFICO NUMERIC
          , @LVALORCUENTA          VARCHAR(50)
          , @LCOD_INST_SVS         VARCHAR(10)
          , @LID_INSTRUMENTO_SVS   NUMERIC

	SET @LMSGRESULTADO = ''
    SET @LCODRESULTADO = 'OK'
  BEGIN TRY
	---------------------------------------------------------------------------------
		SELECT @LID_MONEDA_USD = ID_MONEDA
		  FROM MONEDAS
		 WHERE COD_MONEDA='USD'
		SELECT @LID_MONEDA_EUR = ID_MONEDA
		  FROM MONEDAS
		 WHERE COD_MONEDA='EUR'
	---------------------------------------------------------------------------------
		SELECT @LCOD_ORIGEN = COD_ORIGEN
		  FROM ORIGENES
		 WHERE DSC_ORIGEN = 'PERSHING'

		SET @LVALORCUENTA = @PRUT_CLIENTE + '/' + @PCUENTA

		IF NOT @PRUT_CLIENTE IS NULL
			SET @PRUT_CLIENTE = RTRIM(@PRUT_CLIENTE)
		ELSE
			SET @PRUT_CLIENTE = ''

		IF ISNULL((SELECT COUNT(*)
					 FROM VIEW_ALIAS
					WHERE TABLA='CUENTAS'
					  AND COD_ORIGEN=@LCOD_ORIGEN
					  AND VALOR = @LVALORCUENTA),0) <> 0
		BEGIN
			SELECT @LID_CUENTA = ID_ENTIDAD
			  FROM VIEW_ALIAS
			 WHERE TABLA='CUENTAS'
			   AND COD_ORIGEN=@LCOD_ORIGEN
			   AND VALOR = @LVALORCUENTA
		END
		ELSE
		BEGIN
			SET @LCODRESULTADO = 'ERROR'
			SET @LMSGRESULTADO = 'ERROR: Cuenta Externa no tiene Alias en GPI: ' + @PRUT_CLIENTE
		END
	---------------------------------------------------------------------------------
		IF @LCODRESULTADO = 'OK'
		BEGIN
			IF NOT @PFAMILIA_PRODUCTO IS NULL
				SELECT @LCOD_INSTRUMENTO = CASE @PFAMILIA_PRODUCTO WHEN '(FIX INC)' THEN 'RF_INT_PSH'
																   WHEN '(EQUITY)'  THEN 'RV_INT_PSH'
																   WHEN '(MUT FND)' THEN 'FFMM_INT_PSH'
																   END
			ELSE
			BEGIN
				SET @LCODRESULTADO = 'ERROR'
				SET @LMSGRESULTADO = 'ERROR: CUSIP no posee familia asignada : ' + @PCUSIP
			END
		END
	---------------------------------------------------------------------------------
		IF @LCODRESULTADO = 'OK'
		BEGIN
	---------------------------------------------------------------------------------
			SELECT @LCOD_INST_SVS = CASE @LCOD_INSTRUMENTO WHEN 'RV_INT_PSH' THEN 'ACE'
														   WHEN 'RF_INT_PSH'   THEN 'BEE'
														   WHEN 'FFMM_INT_PSH' THEN 'CFME'  END

			SELECT @LID_INSTRUMENTO_SVS = ID_ENTIDAD
			  FROM VIEW_ALIAS
			 WHERE COD_ORIGEN = 'PERSHING'
			   AND TABLA = 'INSTRUMENTOS_SVS'
			   AND VALOR = @LCOD_INST_SVS

			IF ISNULL((SELECT COUNT(1) FROM REL_NEMOTECNICO_ATRIBUTOS WHERE CUSIP = @PCUSIP),0) <> 0
			BEGIN
				IF ((SELECT COUNT(1) FROM REL_NEMOTECNICO_ATRIBUTOS WHERE CUSIP = @PCUSIP) > 1)
				BEGIN
					SET @LCODRESULTADO = 'ERROR_OTRO'
					SET @LMSGRESULTADO = 'ERROR: CUSIP (' + @PCUSIP + ') se encuentra asociado a m�s de un nemot�cnico, por favor regularizar.'
				END
				ELSE
				BEGIN
					SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO
						 , @LNEMOTECNICO    = NEMOTECNICO
					  FROM NEMOTECNICOS
					 WHERE ID_NEMOTECNICO = (SELECT ID_NEMOTECNICO
											   FROM REL_NEMOTECNICO_ATRIBUTOS
											  WHERE CUSIP = @PCUSIP)

					UPDATE REL_NEMOTECNICO_ATRIBUTOS
					   SET ID_INSTRUMENTO_SVS = @LID_INSTRUMENTO_SVS,
						   ISIM = @PISIN
					 WHERE ID_NEMOTECNICO = @LID_NEMOTECNICO AND CUSIP = @PCUSIP
				END
			END
			ELSE
			BEGIN
				IF ISNULL((SELECT COUNT(1) FROM REL_NEMOTECNICO_ATRIBUTOS WHERE ISIM = @PISIN),0) <> 0
				BEGIN
					IF ((SELECT COUNT(1) FROM REL_NEMOTECNICO_ATRIBUTOS WHERE ISIM = @PISIN) > 1)
					BEGIN
						SET @LCODRESULTADO = 'ERROR_OTRO'
						SET @LMSGRESULTADO = 'ERROR: ISIN (' + @PISIN + ') se encuentra asociado a m�s de un nemot�cnico, por favor regularizar.'
					END
					ELSE
					BEGIN
						SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO
							 , @LNEMOTECNICO    = NEMOTECNICO
						  FROM NEMOTECNICOS
						 WHERE ID_NEMOTECNICO = (SELECT ID_NEMOTECNICO
												   FROM REL_NEMOTECNICO_ATRIBUTOS
												  WHERE ISIM = @PISIN)

						UPDATE REL_NEMOTECNICO_ATRIBUTOS
						   SET ID_INSTRUMENTO_SVS = @LID_INSTRUMENTO_SVS,
							   CUSIP = @PCUSIP
						 WHERE ID_NEMOTECNICO = @LID_NEMOTECNICO AND ISIM = @PISIN
					END
				END
				ELSE
				BEGIN
					IF ISNULL((SELECT COUNT(*) FROM NEMOTECNICOS WHERE DSC_NEMOTECNICO = @PNOMBRE),0) <> 0
					BEGIN
						SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO
							 , @LNEMOTECNICO    = NEMOTECNICO
						  FROM NEMOTECNICOS
						 WHERE DSC_NEMOTECNICO = @PNOMBRE

						IF ((SELECT COUNT(*) FROM REL_NEMOTECNICO_ATRIBUTOS WHERE ID_NEMOTECNICO = @LID_NEMOTECNICO) = 0)
						BEGIN
							INSERT INTO REL_NEMOTECNICO_ATRIBUTOS (ID_NEMOTECNICO, ISIM, CUSIP)
							VALUES (@LID_NEMOTECNICO, @PISIN, @PCUSIP)
						END
						ELSE
						BEGIN
							UPDATE REL_NEMOTECNICO_ATRIBUTOS
							   SET CUSIP = @PCUSIP,
								   ISIM = @PISIN
							 WHERE ID_NEMOTECNICO = @LID_NEMOTECNICO
						END
					END
					ELSE
					BEGIN
						SELECT @LID_EMISOR_ESPECIFICO = ID_EMISOR_ESPECIFICO
						  FROM EMISORES_ESPECIFICO
						 WHERE COD_EMISOR_ESPECIFICO = CASE @LCOD_INSTRUMENTO WHEN 'RV_INT_PSH'   THEN 'ERVINT'
																			  WHEN 'RF_INT_PSH'   THEN 'ERFINT'
																			  WHEN 'FFMM_INT_PSH' THEN 'EFMINT'
													   END

						EXEC PKG_NEMOTECNICOS$Guardar  @PID_NEMOTECNICO               = @LID_NEMOTECNICO OUTPUT
													 , @PCOD_INSTRUMENTO              = @LCOD_INSTRUMENTO
													 , @PNEMOTECNICO                  = @PNOMBRE_CORTO
													 , @PID_MERCADO_TRANSACCION       = 22
													 , @PID_EMISOR_ESPECIFICO         = @LID_EMISOR_ESPECIFICO
													 , @PID_MONEDA                    = @LID_MONEDA_USD
													 , @PID_TIPO_ESTADO               = 3
													 , @PCOD_ESTADO                   = 'V'
													 , @PDSC_NEMOTECNICO              = @PNOMBRE
													 , @PTASA_EMISION                 = NULL
													 , @PTIPO_TASA                    = NULL
													 , @PPERIODICIDAD                 = NULL
													 , @PFECHA_VENCIMIENTO            = @PFECHA_POSICION
													 , @PCORTE_MINIMO_PAPEL           = 0
													 , @PMONTO_EMISION                = 0
													 , @PLIQUIDEZ                     = NULL
													 , @PBASE                         = NULL
													 , @PCOD_PAIS                     = 'OTR'
													 , @PID_EMISOR_ESPECIFICO_ORIGEN  = @LID_EMISOR_ESPECIFICO
													 , @PID_MONEDA_TRANSACCION        = @LID_MONEDA_USD
													 , @PID_SUBFAMILIA                = NULL
													 , @PFLG_FUNGIBLE                 = NULL
													 , @PFECHA_EMISION                = @PFECHA_CIERRE
													 , @PFLG_TIPO_CUOTA_INGRESO       = NULL
													 , @PFLG_TIPO_CUOTA_EGRESO        = NULL
													 , @PID_USUARIO_INSERT            = 1
													 , @PID_USUARIO_UPDATE            = 1

						IF @LID_NEMOTECNICO IS NULL
						BEGIN
							SET @LMSGRESULTADO = 'ERROR: Nemot�cnico no pudo ser creado (' + @PNOMBRE_CORTO + ').'
							SET @LCODRESULTADO = 'ERROR_OTRO'
						END
						ELSE
						BEGIN
							INSERT INTO REL_NEMOTECNICO_ATRIBUTOS  (ID_NEMOTECNICO, ISIM, CUSIP, ID_INSTRUMENTO_SVS)
							VALUES (@LID_NEMOTECNICO, @PISIN, @PCUSIP, @LID_INSTRUMENTO_SVS)
						END
					END
				END
			END
	---------------------------------------------------------------------------------
		END
	---------------------------------------------------------------------------------
		IF @LCODRESULTADO = 'OK'
		BEGIN
			SELECT EM.DSC_EMPRESA
				 , 'PSH'                                      'ORIGEN'
				 , @LID_CUENTA                                'ID_CUENTA'
				 , CT.NUM_CUENTA                              'NUM_CUENTA'
				 , CL.RUT_CLIENTE                             'RUT_CLIENTE'
				 , CASE WHEN (CL.TIPO_ENTIDAD = 'J') THEN
							CL.RAZON_SOCIAL
						ELSE
							(ISNULL(CL.NOMBRES, '') + ' ' +
							 ISNULL(CL.PATERNO, '') + ' ' +
							 ISNULL(CL.MATERNO, '')) END      'NOMBRE_CLIENTE'
				 , @PCUSIP                                    'CUSIP'
				 , @LID_NEMOTECNICO                           'ID_NEMOTECNICO'
				 , NE.NEMOTECNICO                             'NEMOTECNICO'
				 , @LCOD_INSTRUMENTO                          'COD_INSTRUMENTO'
				 , NE.DSC_INSTRUMENTO                         'DSC_INSTRUMENTO'
				 , NE.DSC_PRODUCTO                            'DSC_PRODUCTO'
				 , @PMONEDA_POSICION                          'MONEDA'
				 , @PCANTIDAD_ULTIMO_TRADE                    'CANTIDAD'
				 , @PCOSTO_ORIGINAL                           'PRECIO_PROMEDIO_COMPRA'
				 , @PPRECIOACTUAL                             'PRECIO_MERCADO'
				 , @PCOSTO_ORIGINAL                           'PRECIO_COMPRA'
				 , @PFECHA_POSICION                           'FECHA_VENCIMIENTO'
				 , @PCOSTO_ORIGINAL * @PCANTIDAD_ULTIMO_TRADE 'MONTO_INVERTIDO'
				 , @PVALOR_MERCADO_USD                        'VALOR_MERCADO_MON_ORIGEN'
				 , @PVALOR_MERCADO_USD                        'VALOR_MERCADO_MON_USD'
				 , @LCODRESULTADO                             'CODRESULTADO'
				 , @LMSGRESULTADO                             'MSGRESULTADO'
			  FROM CUENTAS CT
				 , CLIENTES CL
				 , EMPRESAS EM
				, (SELECT N.NEMOTECNICO
						, I.DSC_INTRUMENTO DSC_INSTRUMENTO
						, P.DSC_PRODUCTO
					 FROM NEMOTECNICOS N
						, INSTRUMENTOS I
						, PRODUCTOS P
					WHERE I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
					  AND P.COD_PRODUCTO = I.COD_PRODUCTO
					  AND N.ID_NEMOTECNICO = @LID_NEMOTECNICO) NE
			 WHERE CT.ID_CUENTA  = @LID_CUENTA
			   AND CL.ID_CLIENTE = CT.ID_CLIENTE
			   AND EM.ID_EMPRESA = CT.ID_EMPRESA
		END
		ELSE
		BEGIN
			SELECT ''                                         'DSC_EMPRESA'
				 , 'PSH'                                      'ORIGEN'
				 , 0                                          'ID_CUENTA'
				 , ''                                         'NUM_CUENTA'
				 , ''                                         'RUT_CLIENTE'
				 , ''                                         'NOMBRE_CLIENTE'
				 , @PCUSIP                                    'CUSIP'
				 , 0                                          'ID_NEMOTECNICO'
				 , ''                                         'NEMOTECNICO'
				 , ''                                         'COD_INSTRUMENTO'
				 , ''                                         'DSC_INSTRUMENTO'
				 , ''                                         'DSC_PRODUCTO'
				 , @PMONEDA_POSICION                          'MONEDA'
				 , @PCANTIDAD_ULTIMO_TRADE                    'CANTIDAD'
				 , @PCOSTO_ORIGINAL                           'PRECIO_PROMEDIO_COMPRA'
				 , @PPRECIOACTUAL                             'PRECIO_MERCADO'
				 , @PCOSTO_ORIGINAL                           'PRECIO_COMPRA'
				 , @PFECHA_POSICION                           'FECHA_VENCIMIENTO'
				 , @PCOSTO_ORIGINAL * @PCANTIDAD_ULTIMO_TRADE 'MONTO_INVERTIDO'
				 , @PVALOR_MERCADO_USD                        'VALOR_MERCADO_MON_ORIGEN'
				 , @PVALOR_MERCADO_USD                        'VALOR_MERCADO_MON_USD'
				 , @LCODRESULTADO                             'CODRESULTADO'
				 , @LMSGRESULTADO                             'MSGRESULTADO'
		END
----------------------------------------------------------------
  END TRY
  BEGIN CATCH
     SET @LCODRESULTADO = @@ERROR
     SET @LMSGRESULTADO = 'Error en el procedimiento PKG_SALDOS_ACTIVOS_INT$TestImportador_PSH: ' + ERROR_MESSAGE()
  END CATCH
----------------------------------------------------------------
  SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_SALDOS_ACTIVOS_INT$TestImportador_PSH] TO DB_EXECUTESP
GO