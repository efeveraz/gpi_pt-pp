USE [CSGPI]
GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_MOVIMIENTOS_TOTALES$Buscar_AD5]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_MOVIMIENTOS_TOTALES$Buscar_AD5]
GO
CREATE  PROCEDURE [dbo].[PKG_MOVIMIENTOS_TOTALES$Buscar_AD5]
(@PID_CUENTA NUMERIC,
 @PFECHA     DATETIME)
AS
SET NOCOUNT ON
BEGIN

  CREATE TABLE #TBLTEMP (INSTRUMENTO NUMERIC, DESCRIPCION_EMISOR VARCHAR(100), VALOR_CARTERA NUMERIC)

  INSERT INTO #TBLTEMP
  SELECT 1 AS INSTRUMENTO,
   EE.DSC_EMISOR_ESPECIFICO,
         CASE
            WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra' )) THEN
               SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_VALOR_COMPRA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
            WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado' )) THEN
               SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_MON_CTA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
         END AS VALOR_CARTERA
    FROM SALDOS_ACTIVOS SA WITH (NOLOCK),
         NEMOTECNICOS N    WITH (NOLOCK),
         EMISORES_ESPECIFICO EE WITH (NOLOCK)
   WHERE SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
     AND N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
     AND EE.COD_CLASIFICADOR    = 'M'
     AND SA.FECHA_CIERRE        = @PFECHA
     AND SA.ID_CUENTA           = @PID_CUENTA
GROUP BY SA.ID_CUENTA, EE.DSC_EMISOR_ESPECIFICO

  INSERT INTO #TBLTEMP
  SELECT 2 AS INSTRUMENTO,
   EE.DSC_EMISOR_ESPECIFICO,
         CASE
            WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra' )) THEN
               SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_VALOR_COMPRA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
            WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado' )) THEN
               SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_MON_CTA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
         END AS VALOR_CARTERA
    FROM SALDOS_ACTIVOS SA WITH (NOLOCK),
         NEMOTECNICOS N WITH (NOLOCK),
         EMISORES_ESPECIFICO EE WITH (NOLOCK)
   WHERE SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
     AND N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
     AND EE.COD_CLASIFICADOR    = 'I'
     AND SA.FECHA_CIERRE        = @PFECHA
     AND SA.ID_CUENTA           = @PID_CUENTA
GROUP BY SA.ID_CUENTA, EE.DSC_EMISOR_ESPECIFICO

  INSERT INTO #TBLTEMP
  SELECT 3 AS INSTRUMENTO,
   EE.DSC_EMISOR_ESPECIFICO,
         CASE
            WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra' )) THEN
               SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_VALOR_COMPRA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
            WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado' )) THEN
               SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_MON_CTA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
         END AS VALOR_CARTERA
    FROM SALDOS_ACTIVOS SA  WITH (NOLOCK),
         NEMOTECNICOS N   WITH (NOLOCK),
         EMISORES_ESPECIFICO EE  WITH (NOLOCK)
   WHERE SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
     AND N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
     AND EE.COD_CLASIFICADOR    = 'E'
     AND SA.FECHA_CIERRE        = @PFECHA
     AND SA.ID_CUENTA           = @PID_CUENTA
GROUP BY SA.ID_CUENTA, EE.DSC_EMISOR_ESPECIFICO


   SELECT * FROM #TBLTEMP ORDER BY INSTRUMENTO

   DROP TABLE #TBLTEMP

END
SET NOCOUNT OFF
GO
GRANT EXECUTE ON [PKG_MOVIMIENTOS_TOTALES$Buscar_AD5] TO DB_EXECUTESP
GO