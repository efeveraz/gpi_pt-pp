USE [CSGPI]
GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_MOVIMIENTOS_TOTALES$Buscar_AD6]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_MOVIMIENTOS_TOTALES$Buscar_AD6]
GO
CREATE  PROCEDURE [dbo].[PKG_MOVIMIENTOS_TOTALES$Buscar_AD6]
(@PID_CUENTA NUMERIC,
 @PFECHA     DATETIME)
AS
SET NOCOUNT ON
BEGIN

     CREATE TABLE #TBLTEMP (NEMOTECNICO   VARCHAR(50)
                          , VALOR_CARTERA NUMERIC(18,4)
                          , CODSERIE      VARCHAR(50)
                          , CODSERIE0     VARCHAR(50)
                          , VALOR_SERIE   NUMERIC(18,4))

     DECLARE @LTOTAL_CARTERA NUMERIC(18,4)
           , @LTOTAL_SERIE   NUMERIC(18,4)


     INSERT INTO #TBLTEMP
     SELECT NEMOTECNICO
          , VALOR_CARTERA
          , CODSERIE
          , CODSERIE0
          , ISNULL((SELECT (DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(TEMP.ID_CUENTA,
                                                            MONTO_EMISION,
                                                            ID_MONEDA,
                                                            DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                            @PFECHA))
               FROM NEMOTECNICOS  WITH (NOLOCK)
              WHERE ID_NEMOTECNICO      = TEMP.ID_NEMOTECNICO            ),0) AS VALOR_SERIE
       FROM (SELECT SA.ID_CUENTA
                  , VN.ID_NEMOTECNICO
                  , VN.NEMOTECNICO
                  , CASE
                      WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra')) THEN
                        SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                             SA.MONTO_VALOR_COMPRA,
                                                             SA.ID_MONEDA_CTA,
                                                             DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                             SA.FECHA_CIERRE))
                      WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado')) THEN
                        SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                             SA.MONTO_MON_CTA,
                                                             SA.ID_MONEDA_CTA,
                                                             DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                             SA.FECHA_CIERRE))
                    END AS VALOR_CARTERA
                  , (SELECT ISNULL(COD_ACHS,'')
                      FROM REL_NEMOTECNICO_ATRIBUTOS WITH (NOLOCK)
                     WHERE ID_NEMOTECNICO = VN.ID_NEMOTECNICO) AS CODSERIE
                   ,CONVERT(VARCHAR(50),CS.CODSERIE) AS CODSERIE0
               FROM SALDOS_ACTIVOS SA  WITH (NOLOCK),
                    VIEW_NEMOTECNICOS VN WITH (NOLOCK),
                    CS_TB_SERIES CS WITH (NOLOCK),
                    SUBFAMILIAS SF WITH (NOLOCK),
                    EMISORES_ESPECIFICO EE
              WHERE SA.ID_NEMOTECNICO       = VN.ID_NEMOTECNICO
                AND (CS.CODSERIE            = SUBSTRING(VN.NEMOTECNICO,1,6) OR CS.CODSERIE = VN.NEMOTECNICO)
                AND VN.ID_SUBFAMILIA        = SF.ID_SUBFAMILIA
                AND VN.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
                AND NOT ISNULL(EE.RUT,'')   IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
                AND EE.COD_CLASIFICADOR     = 'M'
                AND NOT SF.COD_SUBFAMILIA   IN ('PRC', 'PRD', 'LH')
                AND SA.FECHA_CIERRE         = @PFECHA
                AND SA.ID_CUENTA            = @PID_CUENTA
            GROUP BY CS.CODSERIE, VN.NEMOTECNICO, VN.ID_NEMOTECNICO, SA.ID_CUENTA
            ) TEMP
     ORDER BY CODSERIE

     INSERT INTO #TBLTEMP
     SELECT NEMOTECNICO
          , VALOR_CARTERA
          , CODSERIE
          , CODSERIE0
          , ISNULL((SELECT (DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(TEMP.ID_CUENTA,
                                                               MONTO_EMISION,
                                                               ID_MONEDA,
                                                               DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                               @PFECHA))
               FROM NEMOTECNICOS WITH (NOLOCK)
              WHERE ID_NEMOTECNICO      = TEMP.ID_NEMOTECNICO
            ),0) AS VALOR_SERIE
       FROM (SELECT SA.ID_CUENTA
                  , VN.ID_NEMOTECNICO
                  , VN.NEMOTECNICO
                  , CASE
                       WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra')) THEN
                              SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                             SA.MONTO_VALOR_COMPRA,
                                                             SA.ID_MONEDA_CTA,
                                                             DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                             SA.FECHA_CIERRE))
                       WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado')) THEN
                           SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                             SA.MONTO_MON_CTA,
                                                             SA.ID_MONEDA_CTA,
                                                             DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                             SA.FECHA_CIERRE))
                    END AS VALOR_CARTERA,
                   (SELECT ISNULL(COD_ACHS,'')
                      FROM REL_NEMOTECNICO_ATRIBUTOS WITH (NOLOCK)
                     WHERE ID_NEMOTECNICO = VN.ID_NEMOTECNICO) AS CODSERIE,
                   VN.NEMOTECNICO AS CODSERIE0
              FROM SALDOS_ACTIVOS SA  WITH (NOLOCK),
                   VIEW_NEMOTECNICOS VN WITH (NOLOCK)
             WHERE SA.ID_NEMOTECNICO             = VN.ID_NEMOTECNICO
               AND VN.COD_INSTRUMENTO            = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()
               AND SUBSTRING(VN.NEMOTECNICO,1,1) = 'S'
               AND SA.FECHA_CIERRE               = @PFECHA
               AND SA.ID_CUENTA                  = @PID_CUENTA
          GROUP BY VN.NEMOTECNICO, VN.ID_NEMOTECNICO, SA.ID_CUENTA
          ) TEMP

   SELECT @LTOTAL_CARTERA = SUM(VALOR_CARTERA)
        , @LTOTAL_SERIE   = SUM(VALOR_SERIE)
     FROM #TBLTEMP

   SELECT NEMOTECNICO
        , (VALOR_CARTERA/1000) 'VALOR_CARTERA'
        , CODSERIE
        , CODSERIE0
        , (VALOR_SERIE/1000) 'VALOR_SERIE'
        , CASE VALOR_SERIE WHEN 0 THEN 0
                                  ELSE ((VALOR_CARTERA/1000)/(VALOR_SERIE/1000))
          END 'PORCENTAJE'
        , @LTOTAL_CARTERA 'TOTAL_CARTERA'
        , @LTOTAL_SERIE/1000   'TOTAL_SERIE'
     FROM #TBLTEMP  WITH (NOLOCK)
    ORDER BY CODSERIE

   DROP TABLE #TBLTEMP

END
SET NOCOUNT OFF
GO
GRANT EXECUTE ON [PKG_MOVIMIENTOS_TOTALES$Buscar_AD6] TO DB_EXECUTESP
GO