USE [CSGPI]
GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_MOVIMIENTOS_TOTALES$Buscar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_MOVIMIENTOS_TOTALES$Buscar]
GO
CREATE PROCEDURE [dbo].[PKG_MOVIMIENTOS_TOTALES$Buscar]
(@PID_CUENTA NUMERIC,
 @PFECHA_INI DATETIME,
 @PFECHA_TER DATETIME,
 @PTIPO_MOV  VARCHAR(2)
)
AS
SET NOCOUNT ON
BEGIN

 DECLARE @ID_ORIGEN NUMERIC, @ID_TIPO_CONVERSION NUMERIC
 SELECT @ID_ORIGEN = ID_ORIGEN FROM ORIGENES WITH (NOLOCK) WHERE COD_ORIGEN = 'SEIL'
 SELECT @ID_TIPO_CONVERSION = ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WITH (NOLOCK) WHERE TABLA = 'MONEDAS'

    CREATE TABLE #TBLANEXO2  (FOLIO                NUMERIC(10)
                            , FECHA_OPERACION      DATETIME
                            , TIPO                 VARCHAR(10)
                            , NEMOTECNICO          VARCHAR(50)
                            , SERIE                VARCHAR(50)
                            , PLAZO                VARCHAR(1)
                            , NOMBRE_EMISOR        VARCHAR(100)
                            , FECHA_EMISION        DATETIME
                            , TIR                  NUMERIC(18,4)
                            , UNIDADES             NUMERIC(18, 4)
                            , UNIDAD_MONETARIA     VARCHAR(50)
                            , VALOR                NUMERIC(18,4)
                            , UTILIDAD             NUMERIC(18, 4)
                            , FECHA_LIQUIDACION    DATETIME
                            , FECHA_VENCIMIENTO    DATETIME
                            , TIPO_OPERACION       VARCHAR(3)
                            , CLASIFICACION_RIESGO VARCHAR(10))


    INSERT INTO #TBLANEXO2
    SELECT
           OP.ID_OPERACION AS FOLIO,
           OP.FECHA_OPERACION,
           CASE
           WHEN ((VN.COD_INSTRUMENTO = DBO.PKG_GLOBAL$GCINST_DEPOSITOS_NAC()) AND (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) <= 365)) THEN
              'DPC'
           WHEN ((VN.COD_INSTRUMENTO = DBO.PKG_GLOBAL$GCINST_DEPOSITOS_NAC()) AND (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) > 365)) THEN
              'DPL'
           ELSE
              (SELECT COD_SUBFAMILIA
                 FROM SUBFAMILIAS WITH (NOLOCK)
                WHERE ID_SUBFAMILIA = VN.ID_SUBFAMILIA)
           END AS TIPO,
           VN.NEMOTECNICO,
            (SELECT ISNULL(COD_ACHS,'')
             FROM REL_NEMOTECNICO_ATRIBUTOS WITH (NOLOCK)
             WHERE ID_NEMOTECNICO = VN.ID_NEMOTECNICO) AS SERIE,
           CASE
              WHEN (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) <= 365) THEN 'C'
              WHEN (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) > 365) THEN 'L'
           END AS PLAZO,
           EE.DSC_EMISOR_ESPECIFICO AS NOMBRE_EMISOR,
           VN.FECHA_EMISION AS FECHA_EMISION,
           OPD.PRECIO AS TIR,
           OPD.CANTIDAD AS UNIDADES,
           (SELECT VALOR
              FROM REL_CONVERSIONES WITH (NOLOCK)
             WHERE ID_ORIGEN = @ID_ORIGEN
               AND ID_TIPO_CONVERSION = @ID_TIPO_CONVERSION
               AND ID_ENTIDAD = VN.ID_MONEDA
           ) AS UNIDAD_MONETARIA,
           OPD.MONTO_PAGO AS VALOR,
           CASE
           WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E') THEN
              ROUND(
              (OPD.MONTO_PAGO -
              (
              ISNULL(
                      (SELECT SA.MONTO_VALOR_COMPRA
                         FROM SALDOS_ACTIVOS SA WITH (NOLOCK)
                        WHERE MA.ID_CUENTA            = SA.ID_CUENTA
                          AND MA.ID_NEMOTECNICO       = SA.ID_NEMOTECNICO
                          AND OP.FECHA_OPERACION -1   = SA.FECHA_CIERRE
                          AND MA.ID_MOV_ACTIVO_COMPRA = SA.ID_MOV_ACTIVO),
                      (SELECT SA.CANTIDAD
                         FROM SALDOS_ACTIVOS SA WITH (NOLOCK)
                        WHERE MA.ID_CUENTA            = SA.ID_CUENTA
                          AND MA.ID_NEMOTECNICO       = SA.ID_NEMOTECNICO
                          AND OP.FECHA_OPERACION      = SA.FECHA_CIERRE
                          AND MA.ID_MOV_ACTIVO_COMPRA = SA.ID_MOV_ACTIVO)
                    ) /
              ISNULL(
                      (SELECT SA.CANTIDAD
                         FROM SALDOS_ACTIVOS SA WITH (NOLOCK)
                        WHERE MA.ID_CUENTA            = SA.ID_CUENTA
                          AND MA.ID_NEMOTECNICO       = SA.ID_NEMOTECNICO
                          AND OP.FECHA_OPERACION -1   = SA.FECHA_CIERRE
                          AND MA.ID_MOV_ACTIVO_COMPRA = SA.ID_MOV_ACTIVO),
                      (SELECT SA.MONTO_VALOR_COMPRA
                         FROM SALDOS_ACTIVOS SA WITH (NOLOCK)
                        WHERE MA.ID_CUENTA            = SA.ID_CUENTA
                          AND MA.ID_NEMOTECNICO       = SA.ID_NEMOTECNICO
                          AND OP.FECHA_OPERACION      = SA.FECHA_CIERRE
                          AND MA.ID_MOV_ACTIVO_COMPRA = SA.ID_MOV_ACTIVO)
                    )
              ) * OPD.CANTIDAD
              ),0)
           ELSE NULL
           END AS UTILIDAD,
           OP.FECHA_LIQUIDACION AS FECHA_LIQUIDACION,
           CASE
              WHEN (OP.FLG_TIPO_MOVIMIENTO = 'I') THEN VN.FECHA_VENCIMIENTO
              WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E') THEN OP.FECHA_LIQUIDACION
           END AS FECHA_VENCIMIENTO,
           CASE
              WHEN (OP.FLG_TIPO_MOVIMIENTO = 'I') THEN 'CPA'
              WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E' AND CA.COD_ORIGEN_MOV_CAJA = 'VENC_IIF' AND NOT OP.ID_CARGO_ABONO IS NULL) THEN 'VCT'
              WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E' AND CA.COD_ORIGEN_MOV_CAJA <> 'VENC_IIF' AND NOT OP.ID_CARGO_ABONO IS NULL) THEN 'VTA'
              WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E' AND OP.COD_TIPO_OPERACION = 'VENC_RF' AND OP.ID_CARGO_ABONO IS NULL) THEN 'VCT'
              WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E' AND OP.COD_TIPO_OPERACION <> 'VENC_RF' AND OP.ID_CARGO_ABONO IS NULL) THEN 'VTA'
           END AS TIPO_OPERACION,
           (SELECT COD_VALOR_CLASIFICACION
              FROM REL_NEMOTECNICO_VALOR_CLASIFIC WITH (NOLOCK)
             WHERE ID_NEMOTECNICO = VN.ID_NEMOTECNICO)AS CLASIFICACION_RIESGO
      FROM OPERACIONES OP WITH (NOLOCK)
           INNER JOIN OPERACIONES_DETALLE OPD WITH (NOLOCK) ON (OPD.ID_OPERACION = OP.ID_OPERACION)
           INNER JOIN MOV_ACTIVOS         MA  WITH (NOLOCK) ON (MA.ID_CUENTA      = OP.ID_CUENTA AND
                                                                MA.ID_NEMOTECNICO = OPD.ID_NEMOTECNICO AND
                                                                MA.ID_OPERACION_DETALLE = OPD.ID_OPERACION_DETALLE)
           INNER JOIN VIEW_NEMOTECNICOS   VN  WITH (NOLOCK) ON (VN.ID_NEMOTECNICO = OPD.ID_NEMOTECNICO)
           INNER JOIN EMISORES_ESPECIFICO EE  WITH (NOLOCK) ON (EE.ID_EMISOR_ESPECIFICO = VN.ID_EMISOR_ESPECIFICO)
           LEFT  JOIN CARGOS_ABONOS       CA  WITH (NOLOCK) ON (OP.ID_CARGO_ABONO = CA.ID_CARGO_ABONO AND CA.ID_CUENTA = OP.ID_CUENTA)
     WHERE OP.ID_CUENTA            = @PID_CUENTA
       AND OP.COD_ESTADO           <> 'A'
       AND OP.FLG_TIPO_MOVIMIENTO  = @PTIPO_MOV
       AND OP.FECHA_OPERACION      BETWEEN @PFECHA_INI AND @PFECHA_TER
       AND NOT OP.COD_TIPO_OPERACION IN ('SORTEOLET', 'VENC_RF')
       AND VN.COD_PRODUCTO         IN (DBO.PKG_GLOBAL$GCPROD_RF_NAC())
       AND CA.COD_ESTADO <> 'A'

    INSERT INTO #TBLANEXO2
    SELECT
           OP.ID_OPERACION AS FOLIO,
           OP.FECHA_OPERACION,
           CASE
           WHEN ((VN.COD_INSTRUMENTO = DBO.PKG_GLOBAL$GCINST_DEPOSITOS_NAC()) AND (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) <= 365)) THEN
              'DPC'
           WHEN ((VN.COD_INSTRUMENTO = DBO.PKG_GLOBAL$GCINST_DEPOSITOS_NAC()) AND (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) > 365)) THEN
              'DPL'
           ELSE
              (SELECT COD_SUBFAMILIA
                 FROM SUBFAMILIAS WITH (NOLOCK)
                WHERE ID_SUBFAMILIA = VN.ID_SUBFAMILIA)
           END AS TIPO,
           VN.NEMOTECNICO,
            (SELECT ISNULL(COD_ACHS,'')
             FROM REL_NEMOTECNICO_ATRIBUTOS WITH (NOLOCK)
             WHERE ID_NEMOTECNICO = VN.ID_NEMOTECNICO) AS SERIE,
           CASE
              WHEN (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) <= 365) THEN 'C'
              WHEN (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) > 365) THEN 'L'
           END AS PLAZO,
           EE.DSC_EMISOR_ESPECIFICO AS NOMBRE_EMISOR,
           VN.FECHA_EMISION AS FECHA_EMISION,
           OPD.PRECIO AS TIR,
           OPD.CANTIDAD AS UNIDADES,
           (SELECT VALOR
              FROM REL_CONVERSIONES WITH (NOLOCK)
             WHERE ID_ORIGEN = @ID_ORIGEN
               AND ID_TIPO_CONVERSION = @ID_TIPO_CONVERSION
               AND ID_ENTIDAD = VN.ID_MONEDA
           ) AS UNIDAD_MONETARIA,
           CASE
           WHEN (OP.FLG_TIPO_MOVIMIENTO = 'I') THEN
              (ISNULL((SELECT SA.MONTO_VALOR_COMPRA
                                          FROM SALDOS_ACTIVOS SA WITH (NOLOCK)
                                         WHERE MA.ID_CUENTA            = SA.ID_CUENTA
                                           AND MA.ID_NEMOTECNICO       = SA.ID_NEMOTECNICO
                                           AND OP.FECHA_OPERACION      = SA.FECHA_CIERRE
                                           AND MA.ID_MOV_ACTIVO        = SA.ID_MOV_ACTIVO), 0))
           ELSE
              OPD.MONTO_PAGO
           END AS VALOR,

           CASE
           WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E') THEN
              (OPD.MONTO_PAGO - ((SELECT (SA.MONTO_VALOR_COMPRA / SA.CANTIDAD)
                                    FROM SALDOS_ACTIVOS SA WITH (NOLOCK)
                                   WHERE MA.ID_CUENTA            = SA.ID_CUENTA
                                     AND MA.ID_NEMOTECNICO       = SA.ID_NEMOTECNICO
                                     AND OP.FECHA_OPERACION -1   = SA.FECHA_CIERRE
                                     AND MA.ID_MOV_ACTIVO_COMPRA = SA.ID_MOV_ACTIVO)) * OPD.CANTIDAD)
           ELSE NULL
           END AS UTILIDAD,
           OP.FECHA_LIQUIDACION AS FECHA_LIQUIDACION,
           CASE
              WHEN (OP.FLG_TIPO_MOVIMIENTO = 'I') THEN VN.FECHA_VENCIMIENTO
              WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E') THEN OP.FECHA_LIQUIDACION
           END AS FECHA_VENCIMIENTO,
           CASE
              WHEN (OP.FLG_TIPO_MOVIMIENTO = 'I') THEN 'CPA'
              WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E')  THEN 'SOR'
           END AS TIPO_OPERACION,
           (SELECT COD_VALOR_CLASIFICACION
              FROM REL_NEMOTECNICO_VALOR_CLASIFIC WITH (NOLOCK)
             WHERE ID_NEMOTECNICO = VN.ID_NEMOTECNICO)AS CLASIFICACION_RIESGO
      FROM OPERACIONES OP WITH (NOLOCK)
           INNER JOIN OPERACIONES_DETALLE OPD WITH (NOLOCK) ON (OPD.ID_OPERACION = OP.ID_OPERACION)
           INNER JOIN MOV_ACTIVOS         MA  WITH (NOLOCK) ON (MA.ID_CUENTA      = OP.ID_CUENTA AND
                                                                MA.ID_NEMOTECNICO = OPD.ID_NEMOTECNICO AND
                                                                MA.ID_OPERACION_DETALLE = OPD.ID_OPERACION_DETALLE)
           INNER JOIN VIEW_NEMOTECNICOS   VN  WITH (NOLOCK) ON (VN.ID_NEMOTECNICO = OPD.ID_NEMOTECNICO)
           INNER JOIN EMISORES_ESPECIFICO EE  WITH (NOLOCK) ON (EE.ID_EMISOR_ESPECIFICO = VN.ID_EMISOR_ESPECIFICO)
           LEFT  JOIN CARGOS_ABONOS       CA  WITH (NOLOCK) ON (OP.ID_CARGO_ABONO = CA.ID_CARGO_ABONO AND CA.ID_CUENTA = OP.ID_CUENTA)
     WHERE OP.ID_CUENTA            = @PID_CUENTA
       AND OP.COD_ESTADO           <> 'A'
       AND OP.FECHA_OPERACION      BETWEEN @PFECHA_INI AND @PFECHA_TER
       AND OP.FLG_TIPO_MOVIMIENTO  = @PTIPO_MOV
       AND OP.COD_TIPO_OPERACION   = 'SORTEOLET'
       AND VN.COD_PRODUCTO         IN (DBO.PKG_GLOBAL$GCPROD_RF_NAC())
       AND CA.COD_ESTADO <> 'A'

    INSERT INTO #TBLANEXO2
    SELECT
           MC.ID_MOV_CAJA AS FOLIO,
           MC.FECHA_MOVIMIENTO AS FECHA_OPERACION,
           (SELECT COD_SUBFAMILIA
              FROM SUBFAMILIAS WITH (NOLOCK)
             WHERE ID_SUBFAMILIA = VN.ID_SUBFAMILIA) AS TIPO,
            RTRIM(LTRIM(SUBSTRING(SUBSTRING(MC.DSC_MOV_CAJA,
                                           CHARINDEX('DEL ', MC.DSC_MOV_CAJA, 0) + 4,
                                           LEN(MC.DSC_MOV_CAJA)),
                                 1,
                                 CHARINDEX(' DE ', SUBSTRING(MC.DSC_MOV_CAJA,
                                                             CHARINDEX('DEL ', MC.DSC_MOV_CAJA, 0) + 4,
                                                             LEN(MC.DSC_MOV_CAJA)),
                                           0)
                                )
                      )
                ) AS NEMOTECNICO,
            (SELECT ISNULL(COD_ACHS,'')
             FROM REL_NEMOTECNICO_ATRIBUTOS WITH (NOLOCK)
             WHERE ID_NEMOTECNICO = VN.ID_NEMOTECNICO) AS SERIE,
           CASE
               WHEN (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) <= 365) THEN 'C'
               WHEN (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) > 365) THEN 'L'
           END AS PLAZO,
           EE.DSC_EMISOR_ESPECIFICO AS NOMBRE_EMISOR,
           VN.FECHA_EMISION AS FECHA_EMISION,
           VN.TASA_EMISION AS TIR,
           CONVERT(NUMERIC(18,4), CONVERT(FLOAT,RTRIM(LTRIM(SUBSTRING(SUBSTRING(REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),CHARINDEX(' DE ',
                                                                   REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                 LEN(MC.DSC_MOV_CAJA)),
                                                       1,
                                                       CHARINDEX(' NOMINALES', SUBSTRING(REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),
                                                                                         CHARINDEX(' DE ',
                                                               REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                                         LEN(MC.DSC_MOV_CAJA)),0))
                                                           )
                                                     )))
            AS UNIDADES,
           (SELECT VALOR
              FROM REL_CONVERSIONES WITH (NOLOCK)
             WHERE ID_ENTIDAD =VN.ID_MONEDA
               AND ID_ORIGEN = @ID_ORIGEN
               AND ID_TIPO_CONVERSION = @ID_TIPO_CONVERSION) AS UNIDAD_MONETARIA,
           MC.MONTO AS VALOR,
           CASE
           WHEN (VN.ID_MONEDA = DBO.FNT_DAMEIDMONEDA (DBO.PKG_GLOBAL$cMoneda_Cod_Peso())) THEN
              ((VRF.INTERES/100) *
               CONVERT(NUMERIC(18,4), CONVERT(FLOAT,RTRIM(LTRIM(SUBSTRING(SUBSTRING(REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),
                                                                     CHARINDEX(' DE ',
                                                                       REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                     LEN(MC.DSC_MOV_CAJA)),
                                                           1,
                                                           CHARINDEX(' NOMINALES', SUBSTRING(
                                                                                     REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),
                                                                                     CHARINDEX(' DE ',
                                                                                       REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                                     LEN(MC.DSC_MOV_CAJA)),0))
                                                )
                                          )
                       ))
              )
           ELSE
              DBO.PKG_TIPO_CAMBIO$FNT_CAMBIO_PARIDAD (@PID_CUENTA,
                                                      ((VRF.INTERES/100) *
                                                       CONVERT(NUMERIC(18,4), CONVERT(FLOAT,RTRIM(
                                                                             LTRIM(
                                                                             SUBSTRING(
                                                                               SUBSTRING(
                                                                                 REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),
                                                                                 CHARINDEX(' DE ',
                                                                                   REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                                 LEN(MC.DSC_MOV_CAJA)),
                                                                               1,
                                                                               CHARINDEX(' NOMINALES',                         SUBSTRING(
                                                                                   REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),
                                                                                   CHARINDEX(' DE ',
                                                                                     REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                                   LEN(MC.DSC_MOV_CAJA)),0))
                                                                                         )
                                                         )
                                                              ))
                                                      ),
                                                      VN.ID_MONEDA,
                                                      DBO.FNT_DAMEIDMONEDA (DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                      MC.FECHA_MOVIMIENTO,
                                                      'N')
           END AS UTILIDAD,
           MC.FECHA_LIQUIDACION AS FECHA_LIQUIDACION,
           MC.FECHA_LIQUIDACION AS FECHA_VENCIMIENTO,
           'CCP' AS TIPO_OPERACION,
           (SELECT COD_VALOR_CLASIFICACION
              FROM REL_NEMOTECNICO_VALOR_CLASIFIC WITH (NOLOCK)
             WHERE ID_NEMOTECNICO = VN.ID_NEMOTECNICO)AS CLASIFICACION_RIESGO
      FROM MOV_CAJA            MC  WITH (NOLOCK),
           VIEW_NEMOTECNICOS   VN  WITH (NOLOCK),
           EMISORES_ESPECIFICO EE  WITH (NOLOCK),
           VIEW_CUPONES_RF     VRF WITH (NOLOCK),
           SUBFAMILIAS         SF  WITH (NOLOCK)
     WHERE MC.ID_CAJA_CUENTA       IN (SELECT ID_CAJA_CUENTA FROM CAJAS_CUENTA WITH (NOLOCK) WHERE ID_CUENTA = @PID_CUENTA)
       AND MC.COD_ESTADO           <> 'A'
       AND MC.COD_ORIGEN_MOV_CAJA  = 'CORTECUPON'
       AND MC.FECHA_LIQUIDACION    BETWEEN @PFECHA_INI AND @PFECHA_TER
       AND MC.DSC_MOV_CAJA         LIKE 'CORTE DE CUPON %'
       AND RTRIM(LTRIM(SUBSTRING(SUBSTRING(MC.DSC_MOV_CAJA,
                                           CHARINDEX('DEL ', MC.DSC_MOV_CAJA, 0) + 4,
                                           LEN(MC.DSC_MOV_CAJA)),
                                 1,
                                 CHARINDEX(' DE ', SUBSTRING(MC.DSC_MOV_CAJA,
                                                             CHARINDEX('DEL ', MC.DSC_MOV_CAJA, 0) + 4,
                                                             LEN(MC.DSC_MOV_CAJA)), 0))
                       )
                 ) = VN.NEMOTECNICO
       AND EE.ID_EMISOR_ESPECIFICO = VN.ID_EMISOR_ESPECIFICO
       AND VN.ID_NEMOTECNICO       = VRF.ID_NEMOTECNICO
       AND VRF.FECHA_CUPON         BETWEEN @PFECHA_INI AND @PFECHA_TER
       AND VN.ID_SUBFAMILIA        = SF.ID_SUBFAMILIA
       AND NOT SF.COD_SUBFAMILIA   = 'LH'
  ORDER BY MC.ID_MOV_CAJA


    INSERT INTO #TBLANEXO2
    SELECT
           MC.ID_MOV_CAJA AS FOLIO,
           MC.FECHA_MOVIMIENTO AS FECHA_OPERACION,
           (SELECT COD_SUBFAMILIA
              FROM SUBFAMILIAS WITH (NOLOCK)
             WHERE ID_SUBFAMILIA = VN.ID_SUBFAMILIA) AS TIPO,
           RTRIM(LTRIM(SUBSTRING(SUBSTRING(MC.DSC_MOV_CAJA,
                                           CHARINDEX('DEL ', MC.DSC_MOV_CAJA, 0) + 4,
                                           LEN(MC.DSC_MOV_CAJA)),
                                 1,
                                 CHARINDEX(' DE ', SUBSTRING(MC.DSC_MOV_CAJA,
                                                             CHARINDEX('DEL ', MC.DSC_MOV_CAJA, 0) + 4,
                                                             LEN(MC.DSC_MOV_CAJA)),
                                           0)
                                )
                      )
                ) AS NEMOTECNICO,
            (SELECT ISNULL(COD_ACHS,'')
             FROM REL_NEMOTECNICO_ATRIBUTOS WITH (NOLOCK)
             WHERE ID_NEMOTECNICO = VN.ID_NEMOTECNICO) AS SERIE,
           CASE
               WHEN (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) <= 365) THEN 'C'
               WHEN (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) > 365) THEN 'L'
           END AS PLAZO,
           EE.DSC_EMISOR_ESPECIFICO AS NOMBRE_EMISOR,
           VN.FECHA_EMISION AS FECHA_EMISION,
           VN.TASA_EMISION AS TIR,
           CONVERT(NUMERIC(18,4), CONVERT(FLOAT,RTRIM(LTRIM(SUBSTRING(SUBSTRING(REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),
                                                                 CHARINDEX(' DE ',
                     REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                 LEN(MC.DSC_MOV_CAJA)),
                                                       1,
                                                       CHARINDEX(' NOMINALES', SUBSTRING(REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),
                                                                                         CHARINDEX(' DE ',
                                                                                           REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                                         LEN(MC.DSC_MOV_CAJA)),0))
                                                           )
                                                     )))
            AS UNIDADES,
           (SELECT VALOR
              FROM REL_CONVERSIONES WITH (NOLOCK)
             WHERE ID_ENTIDAD =VN.ID_MONEDA
               AND ID_ORIGEN = @ID_ORIGEN
               AND ID_TIPO_CONVERSION = @ID_TIPO_CONVERSION) AS UNIDAD_MONETARIA,
           MC.MONTO AS VALOR,
           CASE
           WHEN (VN.ID_MONEDA = DBO.FNT_DAMEIDMONEDA (DBO.PKG_GLOBAL$cMoneda_Cod_Peso())) THEN
              ((VRF.INTERES/100) *
               CONVERT(NUMERIC(18,4), CONVERT(FLOAT,RTRIM(LTRIM(SUBSTRING(SUBSTRING(REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),
                                                                     CHARINDEX(' DE ',
                                                                       REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                     LEN(MC.DSC_MOV_CAJA)),
                                                           1,
                                                           CHARINDEX(' NOMINALES', SUBSTRING(
                                                                                     REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),
                                                                                     CHARINDEX(' DE ',
                                                                                       REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                                     LEN(MC.DSC_MOV_CAJA)),0))
                                                )
                                          )
                       ))
              )
           ELSE
              DBO.PKG_TIPO_CAMBIO$FNT_CAMBIO_PARIDAD (@PID_CUENTA,
                                                      ((VRF.INTERES/100) *
                                                       CONVERT(NUMERIC(18,4), CONVERT(FLOAT,RTRIM(
                                                                             LTRIM(
                                                                             SUBSTRING(
                                                                               SUBSTRING(
                   REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),
                                                                                 CHARINDEX(' DE ',
                                                                                   REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                                 LEN(MC.DSC_MOV_CAJA)),
                                                                               1,
                                                                               CHARINDEX(' NOMINALES',
                                                                                 SUBSTRING(
                                                                                   REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''),
                                                                                   CHARINDEX(' DE ',
                                                                                     REPLACE(MC.DSC_MOV_CAJA, 'CORTE DE CUPON DEL ', ''), 0) + 4,
                                                                                   LEN(MC.DSC_MOV_CAJA)),0))
                                                                                         )
                                                                                  )
                                                              ))
                                                      ),
                                                      VN.ID_MONEDA,
                                                      DBO.FNT_DAMEIDMONEDA (DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                      MC.FECHA_MOVIMIENTO,
                                                      'N')
           END AS UTILIDAD,
           MC.FECHA_LIQUIDACION AS FECHA_LIQUIDACION,
           MC.FECHA_LIQUIDACION AS FECHA_VENCIMIENTO,
           'CCP' AS TIPO_OPERACION,
           (SELECT COD_VALOR_CLASIFICACION
              FROM REL_NEMOTECNICO_VALOR_CLASIFIC  WITH (NOLOCK)
             WHERE ID_NEMOTECNICO = VN.ID_NEMOTECNICO)AS CLASIFICACION_RIESGO
      FROM MOV_CAJA            MC  WITH (NOLOCK),
           VIEW_NEMOTECNICOS   VN  WITH (NOLOCK),
           EMISORES_ESPECIFICO EE  WITH (NOLOCK),
           VIEW_CUPONES_RF     VRF WITH (NOLOCK),
           SUBFAMILIAS         SF  WITH (NOLOCK)
     WHERE MC.ID_CAJA_CUENTA       IN (SELECT ID_CAJA_CUENTA FROM CAJAS_CUENTA WITH (NOLOCK) WHERE ID_CUENTA = @PID_CUENTA)
       AND MC.COD_ESTADO           <> 'A'
       AND MC.COD_ORIGEN_MOV_CAJA  = 'CORTECUPON'
       AND MC.FECHA_LIQUIDACION    BETWEEN @PFECHA_INI AND @PFECHA_TER
       AND MC.DSC_MOV_CAJA         LIKE 'CORTE DE CUPON %'
       AND RTRIM(LTRIM(SUBSTRING(SUBSTRING(MC.DSC_MOV_CAJA,
                                           CHARINDEX('DEL ', MC.DSC_MOV_CAJA, 0) + 4,
                                           LEN(MC.DSC_MOV_CAJA)),
                                 1,
                                 CHARINDEX(' DE ', SUBSTRING(MC.DSC_MOV_CAJA,
                                                             CHARINDEX('DEL ', MC.DSC_MOV_CAJA, 0) + 4,
                                                             LEN(MC.DSC_MOV_CAJA)), 0))
                       )
                 ) = VN.NEMOTECNICO
       AND EE.ID_EMISOR_ESPECIFICO = VN.ID_EMISOR_ESPECIFICO
       AND VN.ID_NEMOTECNICO       = VRF.ID_NEMOTECNICO
       AND VRF.FECHA_CUPON         BETWEEN @PFECHA_INI AND @PFECHA_TER
       AND VN.ID_SUBFAMILIA        = SF.ID_SUBFAMILIA
       AND SF.COD_SUBFAMILIA       = 'LH'
  ORDER BY MC.ID_MOV_CAJA

    SELECT FOLIO,          FECHA_OPERACION,         TIPO,
           NEMOTECNICO,    SERIE,                   PLAZO,
           NOMBRE_EMISOR,  FECHA_EMISION,           TIR,
           UNIDADES,       UNIDAD_MONETARIA,        VALOR,
           UTILIDAD,       FECHA_LIQUIDACION,       FECHA_VENCIMIENTO
           TIPO_OPERACION, CLASIFICACION_RIESGO
      FROM #TBLANEXO2 WITH (NOLOCK) ORDER BY tipo_operacion,FECHA_OPERACION, FOLIO

	  DROP table #TBLANEXO2
END
SET NOCOUNT OFF
GO
GRANT EXECUTE ON [PKG_MOVIMIENTOS_TOTALES$Buscar] TO DB_EXECUTESP
GO
