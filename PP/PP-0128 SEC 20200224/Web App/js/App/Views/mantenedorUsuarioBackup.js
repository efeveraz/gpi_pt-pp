﻿var alto;
var rows_selectedUsu = [];
var GetDataTable;

function resize() {
    alto = $(window).height();
    ancho = $(window).width();

    if (alto > 440) {
        $('div.dataTables_scrollBody').height(alto - 281);
    }
}

function eventHandlersMantenedorUsuarioBackup() {

    $("#DDUsuarios").select2({
        theme: "bootstrap",
        width: '100%'
    });

    $("#btnHabilitaEditar").on('click', function () {
        if (rows_selectedUsu.length == 0) {
            return false;
        }
        $('#lblAlertaCheck').addClass("hidden");
        $('#divDDUsuarios').removeClass("hidden");
        $('#btnGuardar').prop('disabled', false);
        $('#chkActivar').bootstrapToggle('enable');
        
        consultarUsuariosNoBackup();
    });

    $("#btnGuardar").on('click', function () {
        guardarRelUsuariosBackup();
    });

    $("#btnCancelar").on('click', function () {
        $("#panelUsuario").removeClass('hidden');
        $("#panelEditar").addClass('hidden');
        $(window).trigger('resize');
    });

    $('#tablaUsuarios tbody').on('click', 'input[type="radio"]', function (e) {
        var data = GetDataTable.row($(this).parents('tr')).data();
        rows_selectedUsu = [];
        if (this.checked) {
            rows_selectedUsu.push({ dsc_usuario: data.dsc_usuario, usuario_backup: data.usuario_backup });
        }
        estadosBotones();
    });

    $('#tablaUsuarios').on('click', 'tbody td div[class="radio"]', function (e) {
        $(this).parent().find('input[type="radio"]').prop('checked', true);

        var data = GetDataTable.row($('#tablaUsuarios tbody input[type="radio"]:checked').parents('tr')).data();
        rows_selectedUsu = [];
        if (data) {
            rows_selectedUsu.push({ dsc_usuario: data.dsc_usuario, usuario_backup: data.usuario_backup });
        }
        estadosBotones();
    });

    $("#DDUsuarios").change(function () {
        var seleccion = $("#DDUsuarios").val()
        if (seleccion.length > 0) {
            $('#chkActivar').bootstrapToggle('on');
        }
    });

    $('#chkActivar').change(function () {
        var chkActivar = $('#chkActivar').prop('checked');
        if (chkActivar === false) {
            $("#DDUsuarios").val(null).trigger("change");
        }
    });

}

function initMantenedorUsuario() {
    $("#idSubtituloPaginaText").text("Usuario Backup");
    document.title = "Usuarios Backup";
    consultarUsuarios();
}

function guardarRelUsuariosBackup() {
    var usuariosCount = $("#DDUsuarios").val().length;
    var usuariosCadena = $("#DDUsuarios").val().join();
    var chkActivar = $('#chkActivar').prop('checked');

    $('#loaderProceso').modal();
    $.ajax({
        url: "MantenedorUsuarioBackup.aspx/asociacionBackupUsuarios",
        data: "{'pUsuarioBackup':'" + rows_selectedUsu[0].dsc_usuario +
        "', 'pUsuariosCadena':'" + usuariosCadena +
        "', 'pChkActivar':" + chkActivar + "}",
        type: "post",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                if (res == true) {
                    alertaColor(0, "Operación grabada correctamente");
                    consultarUsuarios();
                    $("#panelUsuario").removeClass('hidden');
                    $("#panelEditar").addClass('hidden');
                } else {
                    alertaColor(3, "Ocurrió un error al grabar la operación");
                }
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
            $(window).trigger('resize');
        }
    });
}

function consultarUsuariosNoBackup() {
    $.ajax({
        url: "MantenedorUsuarioBackup.aspx/consultarUsuariosNoBackup",
        data: "{'pUsuarioBackup':'" + rows_selectedUsu[0].dsc_usuario + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                $("#DDUsuarios").empty();
                $.each(res, function (key, val) {
                    $("#DDUsuarios").append('<option value="' + val + '">' + val + '</option>');
                });
                $("#DDUsuarios").prop("disabled", false);
                cargarInformacionUsuario();
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function cargarInformacionUsuario() {
    $('#chkActivar').bootstrapToggle(rows_selectedUsu[0].usuario_backup ? 'on' : 'off');
    $("#tituloEditar").text(rows_selectedUsu[0].dsc_usuario);
    $.ajax({
        url: "MantenedorUsuarioBackup.aspx/consultarUsuariosBackup",
        data: "{'pUsuarioBackup':'" + rows_selectedUsu[0].dsc_usuario + "'}",
        type: "post",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var respuesta = JSON.parse(jsondata.responseText).d;
                $("#panelUsuario").addClass('hidden');
                $("#panelEditar").removeClass('hidden');
                $("#DDUsuarios").val(respuesta).change();
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function consultarUsuarios() {
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    GetDataTable = $('#tablaUsuarios').DataTable({
        data: {},
        columns: [
            {
                "searchable": false,
                "orderable": false,
                "defaultContent": "<div class='radio'><input type='radio' name='radioGrupo'><label></label></div>",
                width: '1%',
                className: 'text-center'
            },
            { "data": "dsc_usuario" },
            { "data": "cantidad_usuarios" },
            { "data": "usuario_backup" }
        ],
        destroy: true,
        language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" },
        order: [[1, 'asc']],
        ajax: {
            url: 'MantenedorUsuarioBackup.aspx/consultarUsuarios',
            data: function (d) {
                return JSON.stringify({});
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd',
            complete: function (jsondata, stat) {
                if (stat === "error") {
                    alertaColor(3, "Ocurrió un error en la consulta");
                    $('#loaderProceso').modal('hide');
                }
            }
        },
        columnDefs: [
            {
                targets: [2],
                className: 'text-right'
            },
            {
                targets: [3],
                render: function (data, type, row) {
                    var usuBackup = (data == 1 ? 'SI' : 'NO');
                    return usuBackup;
                },
                className: 'text-center'
            }
        ],
        scrollY: alto - 281,
        scrollX: true,
        scrollCollapse: false,
        fixedColumns: false,
        //responsive: true,
        paging: true,
        rowCallback: function () {
            $(window).trigger('resize');
        }
    });
    $('#loaderProceso').modal('hide');
    rows_selectedUsu = [];
    estadosBotones();
}

function estadosBotones() {
    if (rows_selectedUsu.length == 0) {
        $("#btnHabilitaEditar").addClass('disabled').attr('data-original-title', 'Seleccione Usuario');;
    } else {
        $("#btnHabilitaEditar").removeClass('disabled').attr('data-original-title', 'Editar');;
    }
}

$(document).ready(function () {
    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    initMantenedorUsuario();
    eventHandlersMantenedorUsuarioBackup();

    resize();
});