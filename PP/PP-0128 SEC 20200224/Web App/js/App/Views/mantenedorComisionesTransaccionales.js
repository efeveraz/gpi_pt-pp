﻿var alto;
var accion;
var conFechaTermino = false;
var cierraRegistroAnterior = false;

var validacionComision = true;
var validacionDerBolsa = true;
var validacionFechas = true;

var row_selectTable = [];
var GetDataTable;

function resize() {
    alto = $(window).height();

    if (alto > 500) {
        $('div.dataTables_scrollBody').height(alto - 365);
    }
}

function eventHandlersComTransaccionales() {

    $('#tablaMantenedor tbody').on('click', 'input[type="radio"]', function (e) {
        seleccionInformacionRadio();
    });
    $('#tablaMantenedor').on('click', 'tbody td div[class="radio"], thead th:first-child', function (e) {
        $(this).parent().find('input[type="radio"]').prop('checked', true);
        seleccionInformacionRadio();
    });

    $("#dtRangoFecha").datepicker();

    $("#dtFechaDesde").datepicker().on('changeDate', function () {
        $("#divDDInstrumento").removeClass("has-error");
        $('#lblErrInstrumento').addClass("hidden");

        var vInstrumento = $("#DDInstrumento").val();
        if (vInstrumento !== '-1') {
            validaComisionInstrumento();
        } else {
            $("#divDDInstrumento").addClass("has-error");
            $('#lblErrInstrumento').removeClass("hidden").text("Debe seleccionar Instrumento");
        }
    });

    $("#btnConsultar").button().click(function () {
        if (miInformacionCuenta !== null || $("#chkTodos").is(':checked')) {
            consultaComisionesTransaccionales();
        }
    });

    $("#porCuenta_NroCuenta").change(function () {
        estadoBtnConsultar();
    });
    $("#chkTodos").click(function () {
        if ($("#chkTodos").is(':checked')) {
            $('#btnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        } else {
            estadoBtnConsultar();
        }
    });

    $("#btnHabilitaAgregar").button().click(function () {

        if (miInformacionCuenta !== null) {
            cierraRegistroAnterior = false;
            validacionComision = true;
            validacionDerBolsa = true;
            validacionFechas = true;

            $("#tituloAgregaEdita").text('Agregar Instrumento Cuenta');
            $("#panelPrincipal").addClass('hidden');
            $("#panelAgregar").removeClass('hidden');
            $("#panelAgregar").addClass('top-buffer');
            $("#btnBuscarCuentas").addClass('hidden');
            $("#btnLimpiarCuentas").addClass('hidden');

            $('#divFechaTerminoX').addClass('hidden');
            $('#dtFechaHasta').addClass('hidden');
            $("#divFechaTermino").removeClass('hidden');
            conFechaTermino = false;

            accion = 'guardar';
            limpiarFormulario();
        }
    });

    $("#btnHabilitaEditar").button().click(function () {
        var data = GetDataTable.row($('#tablaMantenedor tbody input[type="radio"]:checked').parents('tr')).data();
        if (data.FECHA_HASTA == null) {
            $("#tituloAgregaEdita").html('Editar Instrumento Cuenta: <strong>' + data.NUM_CUENTA + ' - ' + data.DSC_CUENTA + '<strong>');
            limpiarFormulario();
            accion = 'editar';
            $("#filtroClienteCuentaGrupo").addClass('hidden');
            $("#panelPrincipal").addClass('hidden');
            $("#panelAgregar").removeClass('hidden');
            $("#panelAgregar").removeClass('top-buffer');
            initEdit();
        }
    });

    $("#btnCancelar").button().click(function () {
        $("#panelPrincipal").removeClass('hidden');
        $("#filtroClienteCuentaGrupo").removeClass('hidden');
        $("#panelAgregar").addClass('hidden');
        $("#btnBuscarCuentas").removeClass('hidden');
        $("#btnLimpiarCuentas").removeClass('hidden');
    });
    $("#btnFechaTermino").button().click(function () {
        $("#dtFechaHasta").datepicker('setDate', moment($("#dtFechaDesde").datepicker('getDate')).format('L'));
        $("#divFechaTermino").addClass('hidden');
        $("#divFechaTerminoX").removeClass('hidden');
        $("#dtFechaHasta").removeClass('hidden');
        conFechaTermino = true;
    });
    $("#btnFechaTerminoX").button().click(function () {
        $('#divFechaTerminoX').addClass('hidden');
        $('#dtFechaHasta').addClass('hidden');
        $("#divFechaTermino").removeClass('hidden');
        conFechaTermino = false;
    });

    $("#DDInstrumento").change(function () {
        $("#divDDInstrumento").removeClass("has-error");
        $('#lblErrInstrumento').addClass("hidden");

        var vInstrumento = $("#DDInstrumento").val();
        if (vInstrumento !== '-1') {
            validaComisionInstrumento();
        } else {
            validacionFechas = false;
            $("#divDDInstrumento").addClass("has-error");
            $('#lblErrInstrumento').removeClass("hidden").text("Debe seleccionar Instrumento");
        }
    });
    $("#txtComision").keyup(function () {
        $("#divTxtComision").removeClass("has-error");
        $('#lblErrComision').addClass("hidden");
        var vComision = $("#txtComision").val().trim();
        if (vComision < 0 || vComision > 100) {
            validacionComision = false;
            $("#divTxtComision").addClass("has-error");
            $('#lblErrComision').removeClass("hidden");
            $("#divTxtComision").css("padding-bottom", "0");
        } else {
            validacionComision = true;
            $("#divTxtComision").css("padding-bottom", "20px");
        }
    });
    $("#txtDerechosBolsa").keyup(function () {
        $("#divTxtDerBolsa").removeClass("has-error");
        $('#lblErrDerBolsa').addClass("hidden");
        var vComision = $("#txtDerechosBolsa").val().trim();
        if (vComision < 0 || vComision > 100) {
            validacionDerBolsa = false;
            $("#divTxtDerBolsa").addClass("has-error");
            $('#lblErrDerBolsa').removeClass("hidden");
            $("#divTxtDerBolsa").css("padding-bottom", "0");
        } else {
            validacionDerBolsa = true;
            $("#divTxtDerBolsa").css("padding-bottom", "20px");
        }
    });

    $("#btnGuardar").button().click(function () {
        if (validacionComision && validacionDerBolsa && validacionFechas) {

            if (accion == 'guardar') {
                if (cierraRegistroAnterior) {
                    confir = confirm("La comisión ya existe para este instrumento ¿Desea continuar?");
                    if (confir == true) {
                        guardarNuevo();
                    }
                } else {
                    guardarNuevo();
                }
            } else {
                editarComisionPeriodo();
            }
        }
    });

    $('#btnEliminar').button().click(function () {
        var data = GetDataTable.row($('#tablaMantenedor tbody input[type="radio"]:checked').parents('tr')).data();
        if (data.FECHA_HASTA == null) {
            confir = confirm("¿Desea eliminar el periodo seleccionado?");
            if (confir == true) {
                eliminarComisionPeriodo();
            }
        }
    });
}

function initEdit() {
    var data = GetDataTable.row($('#tablaMantenedor tbody input[type="radio"]:checked').parents('tr')).data();

    $("#DDInstrumento").val(data.COD_INSTRUMENTO).change().prop('disabled', true);
    $("#txtComision").val(data.COMISION * 100);
    $("#txtGastos").val(data.GASTOS);
    $("#txtDerechosBolsa").val(data.DERECHOS_BOLSA * 100);
    if (data.FLG_AFECTA_IVA == 'S') {
        $("#chkIva").prop("checked", true);
    } else {
        $("#chkIva").prop("checked", false);
    }
    $("#dtFechaDesde").datepicker('setDate', moment(data.FECHA_DESDE).format('L'));
    $("#dtFechaHasta").datepicker('setDate', moment(data.FECHA_DESDE).format('L'));
}

function seleccionInformacionRadio() {
    var data = GetDataTable.row($('#tablaMantenedor tbody input[type="radio"]:checked').parents('tr')).data();
    row_selectTable = [];
    if (data) {
        row_selectTable.push(data.ID_COMISION_INSTRUMENTO_CUENTA);
    }
    estadosBotones();

    if (data.FECHA_HASTA !== null) {
        $('#btnHabilitaEditar').addClass('disabled').attr('data-original-title', 'Editar, Periodo cerrado');
        $('#btnEliminar').addClass('disabled').attr('data-original-title', 'Eliminar, Periodo cerrado');
    } else {
        $('#btnHabilitaEditar').removeClass('disabled').attr('data-original-title', 'Editar');
        $('#btnEliminar').removeClass('disabled').attr('data-original-title', 'Eliminar');
    }
}
function validaComisionInstrumento() {
    $("#divTxtFechas").removeClass("has-error");
    $("#lblErrFecha").addClass("hidden");
    $("#btnFechaTermino").removeClass("btn-danger");
    $("#btnFechaTerminoX").removeClass("btn-danger");

    cierraRegistroAnterior = false;

    if (accion == 'guardar') {
        validaComisionInstrumentoGuardar()
    } else {
        validaComisionInstrumentoEditar()
    }
}
function validaComisionInstrumentoGuardar() {
    var idCuenta = miInformacionCuenta.IdCuenta;
    var ddInstrumento = $("#DDInstrumento").val();
    var fechaHoy = moment.utc(Date()).startOf('day').toISOString();

    var fechaDesde = moment.utc($("#dtFechaDesde").datepicker('getDate')).startOf('day').toISOString();
    var jsfechaInicio = fechaDesde.substring(0, fechaDesde.length - 1);

    var respuesta = true;
    if (ddInstrumento !== '-1') {

        $.ajax({
            url: "MantenedorComisionesTransaccionales.aspx/ValidarPeriodoComisionGuardar",
            data: "{'fechaDesde':'" + jsfechaInicio + "', 'idCuenta':" + (miInformacionCuenta == null ? "''" : miInformacionCuenta.IdCuenta) +
            ", 'codInstrumento':" + (ddInstrumento == -1 ? null : "'" + ddInstrumento + "'") + "}",
            datatype: "json",
            type: "post",
            contentType: "application/json; charset=utf-8",
            complete: function (jsondata, stat) {
                if (stat === "success") {
                    var mydata = JSON.parse(jsondata.responseText).d;

                    var fechaBase;

                    if (mydata.length > 0) {

                        if (fechaDesde < fechaHoy) {
                            $("#divTxtFechas").addClass("has-error");
                            $("#lblErrFecha").removeClass("hidden").text("Existe periodo, F.Desde debe ser mayor o igual a F.Hoy");

                            $("#btnFechaTermino").addClass("btn-danger");
                            $("#btnFechaTerminoX").addClass("btn-danger");
                            validacionFechas = false;
                        } else {

                            if (mydata[0].FECHA_HASTA == null) {
                                fechaBase = moment.utc(mydata[0].FECHA_DESDE).startOf('day').toISOString();
                                validacionFechas = true;
                                cierraRegistroAnterior = true;
                            } else {
                                fechaBase = moment.utc(mydata[0].FECHA_HASTA).startOf('day').toISOString();
                            }

                            if (fechaBase < fechaDesde) {
                                validacionFechas = true;
                            } else {
                                $("#divTxtFechas").addClass("has-error");
                                $("#lblErrFecha").removeClass("hidden").text("Fecha desde debe ser mayor a periodo existente");
                                $("#btnFechaTermino").addClass("btn-danger");
                                $("#btnFechaTerminoX").addClass("btn-danger");
                                validacionFechas = false;
                            }
                        }
                    } else {
                        validacionFechas = true;
                    }
                }
                else {
                    alertaColor(3, JSON.parse(jsondata.responseText).Message);
                }
            }
        });
    }
}
function validaComisionInstrumentoEditar() {
    var data = GetDataTable.row($('#tablaMantenedor tbody input[type="radio"]:checked').parents('tr')).data();
    var fechaHoy = moment.utc(Date()).startOf('day').toISOString();

    var fechaDesde = moment.utc($("#dtFechaDesde").datepicker('getDate')).startOf('day').toISOString();
    var jsfechaInicio = fechaDesde.substring(0, fechaDesde.length - 1);

    $.ajax({
        url: "MantenedorComisionesTransaccionales.aspx/ValidarPeriodoComisionEditar",
        data: "{'fechaDesde':'" + jsfechaInicio +
        "', 'idCuenta':" + data.ID_CUENTA +
        ", 'codInstrumento':'" + data.COD_INSTRUMENTO +
        "', 'idComInsCuenta':" + data.ID_COMISION_INSTRUMENTO_CUENTA + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var mydata = JSON.parse(jsondata.responseText).d;

                var fechaBase;

                if (mydata.length > 0) {
                    if (mydata[0].FECHA_HASTA == null) {
                        fechaBase = moment.utc(mydata[0].FECHA_DESDE).startOf('day').toISOString();
                        validacionFechas = true;
                        cierraRegistroAnterior = true;
                    } else {
                        fechaBase = moment.utc(mydata[0].FECHA_HASTA).startOf('day').toISOString();
                    }

                    if (fechaBase < fechaDesde) {
                        validacionFechas = true;
                    } else {
                        $("#divTxtFechas").addClass("has-error");
                        $("#lblErrFecha").removeClass("hidden").text("Fecha desde debe ser mayor a Fecha periodo existente");
                        $("#btnFechaTermino").addClass("btn-danger");
                        $("#btnFechaTerminoX").addClass("btn-danger");
                        validacionFechas = false;
                    }

                } else {
                    validacionFechas = true;
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });

}

function guardarNuevo() {
    var fechaDesde = moment.utc($("#dtFechaDesde").datepicker('getDate')).startOf('day').toISOString();
    var fechaHasta = moment.utc($("#dtFechaHasta").datepicker('getDate')).startOf('day').toISOString();

    var pInstrumento = $("#DDInstrumento").val();
    var pComision = $("#txtComision").val();
    var pGastos = $("#txtGastos").val();
    var pDerechosBolsa = $("#txtDerechosBolsa").val();
    var pAfectoIva = $("#chkIva").is(':checked') ? 'S' : 'N'

    var pFechaDesde = fechaDesde.substring(0, fechaDesde.length - 1);
    var pFechaHasta = fechaHasta.substring(0, fechaHasta.length - 1);

    $('#loaderProceso').modal();
    $.ajax({
        type: "post",
        url: "MantenedorComisionesTransaccionales.aspx/guardarInstrumentoComision",
        data: "{ 'pIdCuenta':" + miInformacionCuenta.IdCuenta +
        ", 'pCodInstrumento':'" + pInstrumento +
        "', 'pComision':" + (pComision == '' ? 0 : pComision) +
        ", 'pGastos':" + (pGastos == '' ? 0 : pGastos) +
        ", 'pDerechosBolsa':" + (pDerechosBolsa == '' ? 0 : pDerechosBolsa) +
        ", 'pAfectoIva':'" + pAfectoIva +
        "', 'pFechaDesde':'" + pFechaDesde +
        "', 'pFechaHasta':" + (!conFechaTermino ? null : "'" + pFechaHasta + "'") +
        ", 'pCierraRegistroAnterior':'" + cierraRegistroAnterior + "'}",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;

                alertaColor(0, "Información guardada correctamente");

                $("#panelPrincipal").removeClass('hidden');
                $("#panelAgregar").addClass('hidden');
                $("#btnBuscarCuentas").removeClass('hidden');
                $("#btnLimpiarCuentas").removeClass('hidden');
                consultaComisionesTransaccionales();
            } else {
                alertaColor(3, "Ocurrió un error al grabar la operación");
            }
            $('#loaderProceso').modal('hide');
        }
    });
}

function editarComisionPeriodo() {
    var data = GetDataTable.row($('#tablaMantenedor tbody input[type="radio"]:checked').parents('tr')).data();
    var fechaDesde = moment.utc($("#dtFechaDesde").datepicker('getDate')).startOf('day').toISOString();
    var fechaHasta = moment.utc($("#dtFechaHasta").datepicker('getDate')).startOf('day').toISOString();

    var pComision = $("#txtComision").val();
    var pGastos = $("#txtGastos").val();
    var pDerechosBolsa = $("#txtDerechosBolsa").val();
    var pAfectoIva = $("#chkIva").is(':checked') ? 'S' : 'N'

    var pFechaDesde = fechaDesde.substring(0, fechaDesde.length - 1);
    var pFechaHasta = fechaHasta.substring(0, fechaHasta.length - 1);

    $('#loaderProceso').modal();
    $.ajax({
        type: "post",
        url: "MantenedorComisionesTransaccionales.aspx/editarInstrumentoComision",
        data: "{ 'pIdComInstCuenta':" + data.ID_COMISION_INSTRUMENTO_CUENTA +
        ", 'pComision':" + (pComision == '' ? 0 : pComision) +
        ", 'pGastos':" + (pGastos == '' ? 0 : pGastos) +
        ", 'pDerechosBolsa':" + (pDerechosBolsa == '' ? 0 : pDerechosBolsa) +
        ", 'pAfectoIva':'" + pAfectoIva +
        "', 'pFechaDesde':'" + pFechaDesde +
        "', 'pFechaHasta':" + (!conFechaTermino ? null : "'" + pFechaHasta + "'") + "}",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;

                alertaColor(0, "Información editada correctamente");
                $("#filtroClienteCuentaGrupo").removeClass('hidden');
                $("#panelPrincipal").removeClass('hidden');
                $("#panelAgregar").addClass('hidden');
                $("#btnBuscarCuentas").removeClass('hidden');
                $("#btnLimpiarCuentas").removeClass('hidden');
                consultaComisionesTransaccionales();
            } else {
                alertaColor(3, "Ocurrió un error al grabar la operación");
            }
            $('#loaderProceso').modal('hide');
        }
    });
}

function eliminarComisionPeriodo() {
    var data = GetDataTable.row($('#tablaMantenedor tbody input[type="radio"]:checked').parents('tr')).data();

    $('#loaderProceso').modal();
    $.ajax({
        type: "post",
        url: "MantenedorComisionesTransaccionales.aspx/eliminarInstrumentoComision",
        data: "{ 'pIdComInstCuenta':" + data.ID_COMISION_INSTRUMENTO_CUENTA + "}",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;

                alertaColor(0, "Periodo eliminado correctamente");
                consultaComisionesTransaccionales();
            } else {
                alertaColor(3, "Ocurrió un error al grabar la operación");
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function estadosBotones() {
    if (row_selectTable.length == 1) {
        mostrarElemento('#btnHabilitaEditar');
        mostrarElemento('#btnEliminar');
    } else {
        ocultarElemento('#btnHabilitaEditar');
        ocultarElemento('#btnEliminar');
    }
}

function ocultarElemento(elemento) {
    $(elemento).removeClass('in');
    $(elemento).addClass('hidden');
}
function mostrarElemento(elemento) {
    $(elemento).addClass('in');
    $(elemento).removeClass('hidden');
}

function initComTransaccionales() {
    $("#idSubtituloPaginaText").text("Comisiones Transaccionales");
    document.title = "Comisiones Transaccionales";

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');

    estadoBtnConsultar();
    consultaComisionesTransaccionales();
    consultaInstrumentos();

    $("#dtFechaDesde").datepicker('setDate', Date());
    $("#dtFechaHasta").datepicker('setDate', Date());
}

function consultaComisionesTransaccionales() {

    var chkTodasCuentas = $("#chkTodos").is(':checked');
    var idCuenta = !miInformacionCuenta ? null : miInformacionCuenta.IdCuenta

    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    GetDataTable = $('#tablaMantenedor').DataTable({
        data: {},
        columns: [
            {
                "searchable": false,
                "orderable": false,
                "defaultContent": "<div class='radio'><input type='radio' name='radioCom'><label></label></div>",
                width: '1%',
                className: 'text-center',
            },
            { "data": "NUM_CUENTA" },
            { "data": "DSC_CUENTA" },
            { "data": "DSC_INTRUMENTO" },
            { "data": "FECHA_DESDE" },
            { "data": "FECHA_HASTA" },
            { "data": "COMISION" },
            { "data": "GASTOS" },
            { "data": "DERECHOS_BOLSA" },
            { "data": "FLG_AFECTA_IVA" }
        ],
        destroy: true,
        language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" },
        order: [[1, 'asc']],
        ajax: {
            url: 'MantenedorComisionesTransaccionales.aspx/consultaComTransaccionales',
            data: function (d) {
                return JSON.stringify({ "idCuenta": idCuenta, "chkTodasCuentas": chkTodasCuentas });
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd'
        },
        columnDefs: [

            {
                targets: [4],
                render: function (data, type, row) {
                    var fechaDes = (data ? moment(data).format("L") : "");
                    return fechaDes;
                },
                className: 'text-center'
            },
            {
                targets: [5],
                render: function (data, type, row) {
                    var fechaHas = (data ? moment(data).format("L") : "");
                    return fechaHas;
                },
                className: 'text-center'
            },
            {
                targets: [6],
                render: function (data, type, row) {
                    var comision = (data ? numeral(data * 100).format('0,000.0000') : numeral(0).format('0,000.0000'));
                    return comision;
                },
                className: 'text-right'
            },
            {
                targets: [7],
                render: function (data, type, row) {
                    var gasto = (data ? numeral(data).format('0,000.00') : numeral(0).format('0,000.00'));
                    return gasto;
                },
                className: 'text-right'
            },
            {
                targets: [8],
                render: function (data, type, row) {
                    var derechos_bolsa = (data ? numeral(data * 100).format('0,000.0000') : numeral(0).format('0,000.0000'));
                    return derechos_bolsa;
                },
                className: 'text-right'
            },
            {
                targets: [9],
                render: function (data, type, row) {
                    var iva = (data == 'S' ? 'SI' : 'NO');
                    return iva;
                },
                className: 'text-center'
            }
        ],
        scrollY: alto - 365,
        scrollX: true,
        scrollCollapse: false,
        fixedColumns: false,
        //responsive: true,
        paging: true,
        rowCallback: function () {
            $(window).trigger('resize');
        }
    });
    $('#loaderProceso').modal('hide');
    row_selectTable = [];
    estadosBotones();
}
function estadoBtnConsultar() {
    var estadoChk = $("#chkTodos").is(':checked');

    if (miInformacionCuenta == null) {
        if (!estadoChk) {
            $('#btnConsultar').tooltip('enable').addClass('disabled');
        }
        $('#btnHabilitaAgregar').tooltip('enable').addClass('disabled').attr('data-original-title', 'Seleccione Cuenta');
        $("#panelPrincipal").removeClass('hidden');
        $("#panelAgregar").addClass('hidden');
    } else {
        $('#btnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        $('#btnHabilitaAgregar').tooltip('enable').removeClass('disabled').attr('data-original-title', 'Nuevo');
    }
}
function consultaInstrumentos() {
    $.ajax({
        url: "../Servicios/Globales.asmx/consultaInstrumentos",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                $("#DDInstrumento").empty();
                $("#DDInstrumento").prepend('<option value="' + -1 + '">' + 'Seleccione Instrumento' + '</option>');
                $.each(res, function (key, val) {
                    $("#DDInstrumento").append('<option value="' + val.COD_INSTRUMENTO + '">' + val.DSC_INTRUMENTO + '</option>');
                });
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}
function limpiarFormulario() {
    $("#DDInstrumento").val(-1).change().prop('disabled', false);
    $("#txtComision").val('');
    $("#txtGastos").val('');
    $("#txtDerechosBolsa").val('');
    $("#chkIva").prop("checked", false);
    $("#dtFechaDesde").datepicker('setDate', Date());
    $("#dtFechaHasta").datepicker('setDate', Date());
}
function desactivaBordetxtGuardar() {
    $("#divDDInstrumento").removeClass("has-error");
    $('#lblErrInstrumento').addClass("hidden");

    $("#divTxtComision").removeClass("has-error");
    $("#divTxtGastos").removeClass("has-error");
    $("#divTxtDerBolsa").removeClass("has-error");
}

$(document).ready(function () {
    initComTransaccionales();
    eventHandlersComTransaccionales();
});