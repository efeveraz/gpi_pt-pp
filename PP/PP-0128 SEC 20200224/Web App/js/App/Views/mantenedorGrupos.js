﻿var accion = 'guardar';
var GetDataGrupos;
var rows_selectGrupo;

var alto;

function resize() {
    var height;
    alto = $(window).height();
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - 38;

    if (ancho <= 992) {
        $("#lisPnlAsociar").height(height - 170);
        $("#lisPnlDisponible").height(height - 170);
        $("#lisGrupoAsociar").height(height - 210);
        $("#lisGrupoDisponible").height(height - 210);
    } else {
        $("#lisPnlAsociar").height(height - 178);
        $("#lisGrupoAsociar").height(height - 218);
        $("#lisPnlDisponible").height(height - 178);
        $("#lisGrupoDisponible").height(height - 218);
    }

    $("#panelAsociar").height(height - 110);
    $("#panelDisponibles").height(height - 110);
    $("#idPnlCtas").height(height - 110);

    if (alto > 440) {
        $('div.dataTables_scrollBody').height(alto - 282);
    }
}
function asociarCtas() {
    $("#ulLisCtaDisponible li.active").each(function (val, li) {
        $("#ulLisCtaAsociada").append('<li class="list-group-item" value="' + $(li).val() + '"> ' + $(li).text() + '</li>');
        $("#ulLisCtaDisponible" + " li[value='" + $(li).val() + "']").remove();
    })
    styleCheckbox();
}
function desAsociarCtas() {
    $("#ulLisCtaAsociada li.active").each(function (val, li) {
        $("#ulLisCtaDisponible").append('<li class="list-group-item" value="' + $(li).val() + '"> ' + $(li).text() + '</li>');
        $("#ulLisCtaAsociada" + " li[value='" + $(li).val() + "']").remove();
    })
    styleCheckbox();
}
function asociarCtasTodos() {
    $("#ulLisCtaDisponible li").each(function (val, li) {
        $("#ulLisCtaAsociada").append('<li class="list-group-item" value="' + $(li).val() + '"> ' + $(li).text() + '</li>');
        $("#ulLisCtaDisponible" + " li[value='" + $(li).val() + "']").remove();
    })
    styleCheckbox();
}
function desAsociarCtasTodos() {
    $("#ulLisCtaAsociada li").each(function (val, li) {
        $("#ulLisCtaDisponible").append('<li class="list-group-item" value="' + $(li).val() + '"> ' + $(li).text() + '</li>');
        $("#ulLisCtaAsociada" + " li[value='" + $(li).val() + "']").remove();
    })
    styleCheckbox();
}
function desactivaBordetxt() {
    $("#divDDGrupo").removeClass("has-error");
    $("#divTxtEmail").removeClass("has-error");
    $("#divTxtTelefono").removeClass("has-error");
}
function desactivaBordetxtGuardar() {
    $("#divTxtAgregarGrupo").removeClass("has-error");
    $("#divTxtAgregarEmail").removeClass("has-error");
    $("#divTxtAgregarTelefono").removeClass("has-error");
}

function guardarNuevoGrupo() {
    var v_dscGrupo = $("#txtAgregarGrupo").val();
    var v_idAsesor = $("#DDAsesorAgregar").val();
    var v_direccion = $("#txtAgregarDireccion").val();
    var v_email = $("#txtAgregarEmail").val();
    var v_telefono = $("#txtAgregarTelefono").val();
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: "post",
        url: "../Servicios/GrupoCuenta.asmx/guardarNuevoGrupo",
        data: "{'nomGrupo':'" + v_dscGrupo +
        "', 'idAsesor':" + (v_idAsesor == 0 ? null : "" + v_idAsesor + "") +
        ", 'direccion':'" + v_direccion +
        "', 'email':'" + v_email +
        "', 'telefono':'" + v_telefono + "'}",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                alertaColor(0, "Grupo Creado Correctamente.");
                consultaGruposCuentas();
                limpiarCamposAgregar();
                $("#panelGrupos").removeClass('hidden');
                $("#panelAgregar").addClass('hidden');
                $("#panelCuentas").addClass('hidden');
            } else {
                alertaColor(3, "Ocurrio un error al grabar la operacion.");
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function actualizarGrupo() {
    var v_idGrupo = rows_selectGrupo[0];
    var v_nomGrupo = $("#txtAgregarGrupo").val().trim();
    var v_idAsesor = $("#DDAsesorAgregar").val();
    var v_direccion = $("#txtAgregarDireccion").val().trim();
    var v_email = $("#txtAgregarEmail").val().trim();
    var v_telefono = $("#txtAgregarTelefono").val().trim();
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "../Servicios/GrupoCuenta.asmx/actualizaGrupo",
        data: "{'idGrupo':'" + v_idGrupo +
        "', 'nomGrupo':'" + v_nomGrupo +
        "', 'direccion':'" + v_direccion +
        "', 'idAsesor':" + (v_idAsesor == 0 ? null : "" + v_idAsesor + "") +
        ", 'email':'" + v_email +
        "', 'telefono':'" + v_telefono + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                alertaColor(0, "Grupo de Cuentas Actualizado Correctamente.");
                consultaGruposCuentas();
                limpiarCamposAgregar();
                $("#panelGrupos").removeClass('hidden');
                $("#panelAgregar").addClass('hidden');
                $("#panelCuentas").addClass('hidden');
            } else {
                alertaColor(3, "Ocurrio un error al grabar la operacion.");
            }
            $('#loaderProceso').modal('hide');
        }
    });
}

function eliminaAsociacion() {
    var v_idGrupo = rows_selectGrupo[0];
    $.ajax({
        type: "post",
        url: "../Servicios/GrupoCuenta.asmx/eliminarAsociacionGrupoCuenta",
        data: "{'idGrupo':'" + v_idGrupo + "'}",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                eliminaGrupoCuenta();
            } else {
                alertaColor(3, "Ocurrio un error al grabar la operacion");
            }
        }
    });
}
function eliminaGrupoCuenta() {
    var v_idGrupo = rows_selectGrupo[0];
    $.ajax({
        type: "post",
        url: "../Servicios/GrupoCuenta.asmx/eliminarGrupoCuenta",
        data: "{'idGrupo':'" + v_idGrupo + "'}",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                consultaGruposCuentas();
                alertaColor(0, "Grupo de Cuentas eliminado Correctamente.");
            } else {
                alertaColor(3, "Ocurrio un error al grabar la operacion");
            }
        }
    });
}
function guardarAsociacionGrupoCuenta() {
    var v_idGrupo = rows_selectGrupo[0];
    var cadenaidCuentas = "";
    var cantAsociadas = $("#ulLisCtaAsociada li").length;

    if (cantAsociadas == 0) {
        alertaColor(0, "Operación grabada Correctamente, Sin asociaciones.");
    } else {

        $("#ulLisCtaAsociada li").each(function (val, li) {
            if (cadenaidCuentas == "") {
                cadenaidCuentas = $(li).val();
            } else {
                cadenaidCuentas = cadenaidCuentas + "," + $(li).val();
            }
        });

        $.ajax({
            url: "../Servicios/GrupoCuenta.asmx/guardarAsociacionGrupoCuenta",
            data: "{'idGrupo':'" + v_idGrupo + "', 'CadenaIdCuenta':'" + cadenaidCuentas + "'}",
            type: "post",
            datatype: "json",
            contentType: "application/json; charset=utf-8",
            complete: function (jsondata, stat) {
                if (stat == "success") {
                    alertaColor(0, "Cuentas asociadas al GRUPO Correctamente.");
                } else {
                    alertaColor(3, "Ocurrió un error al grabar la operación.");
                }
                $(window).trigger('resize');
            }
        });
    }
}

function consultaCuentasAsociadasGrupo() {
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "../Servicios/GrupoCuenta.asmx/consultaCuentasAsociadasPorIdGrupo",
        data: "{'idGrupo':" + rows_selectGrupo[0] + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var grupos = JSON.parse(jsondata.responseText).d;
                $.each(grupos, function (key, val) {
                    $("#ulLisCtaAsociada").append('<li class="list-group-item" value="' + val.ID_CUENTA + '"> ' + val.NUM_CUENTA + ' - ' + val.DSC_CUENTA + '</li>');
                })
                styleCheckbox();
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            consultaCuentasDisponiblesGrupo();
        }
    });
}
function consultaCuentasDisponiblesGrupo() {
    $.ajax({
        url: "../Servicios/GrupoCuenta.asmx/consultaCuentasDisponiblesPorIdGrupo",
        data: "{'idGrupo':" + rows_selectGrupo[0] + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var grupos = JSON.parse(jsondata.responseText).d;
                $.each(grupos, function (key, val) {
                    $("#ulLisCtaDisponible").append('<li class="list-group-item" value="' + val.ID_CUENTA + '"> ' + val.NUM_CUENTA + ' - ' + val.DSC_CUENTA + '</li>');
                })
                styleCheckbox();
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function consultaGruposCuentas() {    
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    GetDataGrupos = $('#tablaGrupos').DataTable({
        data: {},
        columns: [
            {
                "searchable": false,
                "orderable": false,
                "defaultContent": "<div class='radio'><input type='radio' name='radioGrupo'><label></label></div>",
                width: '1%',
                className: 'dt-body-center',
            },
            { "data": "DSC_GRUPO_CUENTA" },
            { "data": "ASESOR" },
            { "data": "DIRECCION" },
            { "data": "EMAIL" },
            { "data": "TELEFONO" }
        ],
        destroy: true,
        language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" },
        order: [[1, 'asc']],
        ajax: {
            url: '../Servicios/GrupoCuenta.asmx/consultaGrupoCuenta',
            data: function (d) {
                return JSON.stringify({});
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd'
        },
        scrollY: alto - 282,
        scrollX: true,
        scrollCollapse: false,
        fixedColumns: false,
        //responsive: true,
        paging: true,
        rowCallback: function () {
            $(window).trigger('resize');
        }
    });
    $('#loaderProceso').modal('hide');
    rows_selectGrupo = [];
    estadosBotones();
}
function styleCheckbox() {
    $('.list-group.checked-list-box .list-group-item').each(function () {

        // Settings
        var $widget = $(this),
            $checkbox = $('<input type="checkbox" class="hidden" />'),
            color = ($widget.data('color') ? $widget.data('color') : "primary"),
            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        $widget.css('cursor', 'pointer')
        $widget.append($checkbox);

        // Event Handlers
        $widget.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $widget.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $widget.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$widget.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $widget.addClass(style + color + ' active');
            } else {
                $widget.removeClass(style + color + ' active');
            }
        }

        // Initialization
        function init() {
            if ($widget.data('checked') == true) {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
            }
            updateDisplay();
            // Inject the icon if applicable
            if ($widget.find('.state-icon').length == 0) {
                $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
            }
        }
        init();
    });

    $('#get-checked-data').on('click', function (event) {
        event.preventDefault();
        var checkedItems = {}, counter = 0;
        $("#check-list-box li.active").each(function (idx, li) {
            checkedItems[counter] = $(li).text();
            counter++;
        });
        $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
    });
}
function validar_numeros(num_fono) {
    var filtro = /^[0-9]+$/;
    if (num_fono == "") {
        return true;
    }
    else {
        if (filtro.test(num_fono)) {
            return true;
        } else {
            return false;
        }
    }
}
function validar_email(email) {
    //var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    var filter = /^[a-zA-Z0-9\._-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,4}$/;
    if (email == "") {
        return true;
    }
    else {
        if (filter.test(email))
            return true;
        else
            return false;
    }
}

function limpiarCamposAgregar() {
    $("#DDAsesorAgregar").prop("disabled", true);
    $("#DDAsesorAgregar").val(0).change();
    $("#txtAgregarDireccion").prop("disabled", true);
    $("#txtAgregarDireccion").val('');
    $("#txtAgregarEmail").prop("disabled", true);
    $("#txtAgregarEmail").val('');
    $("#txtAgregarTelefono").prop("disabled", true);
    $("#txtAgregarTelefono").val('');

    $('#lblErrNomGrupo').addClass("hidden");
    $('#lblErrEmail').addClass("hidden");
    $('#lblErrTelefono').addClass("hidden");
    $("#txtAgregarGrupo").val('');
}
function consultaAsesores() {
    $.ajax({
        url: "MantenedorGrupos.aspx/DatosAsesor",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var grupos = JSON.parse(jsondata.responseText).d;

                $("#DDAsesor").empty();
                $("#DDAsesorAgregar").empty();
                $.each(grupos, function (key, val) {
                    $("#DDAsesor").append('<option value="' + val.ID_ASESOR + '">' + val.NOMBRE + '</option>');
                    $("#DDAsesorAgregar").append('<option value="' + val.ID_ASESOR + '">' + val.NOMBRE + '</option>');
                });
                $("#DDAsesor").prepend('<option value="' + 0 + '"></option>');
                $("#DDAsesorAgregar").prepend('<option value="' + 0 + '"></option>');
                $("#DDAsesor").val(0).change();
                $("#DDAsesorAgregar").val(0).change();
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function estadosBotones() {
    if (rows_selectGrupo.length == 1) {
        mostrarElemento('#btnHabilitaEditar');
        mostrarElemento('#btnEliminar');
        mostrarElemento('#btnPermisosCuenta');
    } else {
        ocultarElemento('#btnHabilitaEditar');
        ocultarElemento('#btnEliminar');
        ocultarElemento('#btnPermisosCuenta');
    }
}
function ocultarElemento(elemento) {
    $(elemento).removeClass('in');
    $(elemento).addClass('hidden');
}
function mostrarElemento(elemento) {
    $(elemento).addClass('in');
    $(elemento).removeClass('hidden');
}

function cargaEditarUsuario() {
    var data = GetDataGrupos.row($('#tablaGrupos tbody input[type="radio"]:checked').parents('tr')).data();
    $("#txtAgregarGrupo").val(data.DSC_GRUPO_CUENTA).prop("disabled", false);
    if (data.ID_ASESOR === null) {
        $("#DDAsesorAgregar").val(0).change().prop("disabled", false);
    } else {
        $("#DDAsesorAgregar").val(data.ID_ASESOR).change().prop("disabled", false);
    }
    $("#txtAgregarDireccion").val(data.DIRECCION).prop("disabled", false);
    $("#txtAgregarEmail").val(data.EMAIL).prop("disabled", false);
    $("#txtAgregarTelefono").val(data.TELEFONO).prop("disabled", false);
}
function validaNombreExistente() {
    var dataRows = GetDataGrupos.rows().data();
    var idGrupo = rows_selectGrupo[0];
    var nomGrupo = $("#txtAgregarGrupo").val().trim().toLowerCase();
    var nomIgual = false;

    if (accion === 'editar') {
        $.each(dataRows, function (key, val) {
            if (nomGrupo === val.DSC_GRUPO_CUENTA.toLowerCase()) {
                if (idGrupo !== val.ID_GRUPO_CUENTA) {
                    nomIgual = true;
                    return false;
                }
            }
        });
    };
    if (accion === 'guardar') {
        $.each(dataRows, function (key, val) {
            if (nomGrupo === val.DSC_GRUPO_CUENTA.toLowerCase()) {
                nomIgual = true;
                return false;
            }
        });
    };
    if (nomIgual) {
        $("#divTxtAgregarGrupo").addClass("has-error");
        $('#lblErrNomGrupo').removeClass("hidden").text("El nombre del grupo ya existe");
        $('#btnGuardarGrupo').prop("disabled", true);
    };
}

function initMantenedorGrupos() {
    consultaGruposCuentas();
    consultaAsesores();
    $("#idSubtituloPaginaText").text("Grupos de Cuentas");
    document.title = "Grupos de Cuentas";
}

function cargarEventHandlersMantenedorGrupos() {
    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $('#tablaGrupos tbody').on('click', 'input[type="radio"]', function (e) {
        var data = GetDataGrupos.row($(this).parents('tr')).data();
        rows_selectGrupo = [];
        if (this.checked) {
            rows_selectGrupo.push(data.ID_GRUPO_CUENTA);
        }
        estadosBotones();
        e.stopPropagation();
    });
    $('#tablaGrupos').on('click', 'tbody td div[class="radio"]', function (e) {
        $(this).parent().find('input[type="radio"]').prop('checked', true);

        var data = GetDataGrupos.row($('#tablaGrupos tbody input[type="radio"]:checked').parents('tr')).data();        
        rows_selectGrupo = [];
        if (data) {
            rows_selectGrupo.push(data.ID_GRUPO_CUENTA);
        }
        estadosBotones();
    });

    $("#btnCancelar").on('click', function () {
        $("#panelGrupos").removeClass('hidden');
        $("#panelAgregar").addClass('hidden');
        $("#panelCuentas").addClass('hidden');
        $('#lblErrNomGrupo').addClass('hidden');
        $('#lblErrEmail').addClass('hidden');
        $('#lblErrTelefono').addClass('hidden');
        limpiarCamposAgregar();
        desactivaBordetxtGuardar();
        $("#txtAgregarGrupo").val('');
    });
    $("#btnCancelarAsociar").on('click', function () {
        $("#panelGrupos").removeClass('hidden');
        $("#panelAgregar").addClass('hidden');
        $("#panelCuentas").addClass('hidden');
    });
    $("#btnHabilitaAgregar").on('click', function () {
        $("#tituloAgregar").removeClass('hidden');
        $("#tituloEditar").addClass('hidden');
        accion = 'guardar';

        $("#panelAgregar").removeClass('hidden');
        $("#panelGrupos").addClass('hidden');
        $("#panelCuentas").addClass('hidden');

        limpiarCamposAgregar();
        $('#btnGuardarGrupo').prop("disabled", true);
    });

    $("#btnHabilitaEditar").on('click', function () {
        $("#tituloAgregar").addClass('hidden');
        $("#tituloEditar").removeClass('hidden');
        accion = 'editar';

        $("#panelAgregar").removeClass('hidden');
        $("#panelGrupos").addClass('hidden');
        $("#panelCuentas").addClass('hidden');

        limpiarCamposAgregar();
        cargaEditarUsuario();
        $('#btnGuardarGrupo').prop("disabled", false);
    });

    $("#btnPermisosCuenta").on('click', function () {
        $("#panelAgregar").addClass('hidden');
        $("#panelGrupos").addClass('hidden');
        $("#panelCuentas").removeClass('hidden');

        $("#ulLisCtaAsociada li").remove();
        $("#ulLisCtaDisponible li").remove();

        var dataRow = GetDataGrupos.row($('#tablaGrupos tbody input[type="radio"]:checked').parents('tr')).data();
        $('#lbGrupoCuenta').text(dataRow.DSC_GRUPO_CUENTA);
        consultaCuentasAsociadasGrupo();
    });

    $("#btnAsociarV").on('click', function () {
        asociarCtas();
    });
    $("#btnAsociarH").on('click', function () {
        asociarCtas();
    });

    $("#btnDesAsociarV").on('click', function () {
        desAsociarCtas();
    });
    $("#btnDesAsociarH").on('click', function () {
        desAsociarCtas();
    });

    $("#btnAsociarTodosV").on('click', function () {
        asociarCtasTodos();
    });
    $("#btnAsociarTodosH").on('click', function () {
        asociarCtasTodos();
    });

    $("#btnDesAsociarTodosV").on('click', function () {
        desAsociarCtasTodos();
    });
    $("#btnDesAsociarTodosH").on('click', function () {
        desAsociarCtasTodos();
    });

    $("#txtBuscarAsociados").keyup(function () {
        $("#ulLisCtaAsociada li").css({ 'display': 'none' });
        $("#ulLisCtaAsociada li").each(function (val, li) {
            var aBuscar = ($("#txtBuscarAsociados").val()).toLowerCase();
            var cadena = ($(li).text()).toLowerCase();

            var pat = new RegExp(aBuscar);
            var res = pat.test(cadena)
            if (res == true) {
                $("#ulLisCtaAsociada li[value='" + $(li).val() + "']").css("display", "");
            }
        });
    });

    $("#btnLimpiarAso").on('click', function () {
        $("#txtBuscarAsociados").val('');
        $("#ulLisCtaAsociada li").css({ 'display': '' });
    });
    $("#btnLimpiarDes").on('click', function () {
        $("#txtBuscarDisponible").val('');
        $("#ulLisCtaDisponible li").css({ 'display': '' });
    });
    $("#txtBuscarDisponible").keyup(function () {
        $("#ulLisCtaDisponible li").css({ 'display': 'none' });
        $("#ulLisCtaDisponible li").each(function (val, li) {
            var aBuscar = ($("#txtBuscarDisponible").val()).toLowerCase();
            var cadena = ($(li).text()).toLowerCase();
            var pat = new RegExp(aBuscar);
            var res = pat.test(cadena)
            if (res == true) {
                $("#ulLisCtaDisponible li[value='" + $(li).val() + "']").css("display", "");
            }
        });
    });

    $("#btnGuardarGrupo").on('click', function () {
        desactivaBordetxtGuardar();
        $('#lblErrNomGrupo').addClass('hidden');
        $('#lblErrEmail').addClass('hidden');
        $('#lblErrTelefono').addClass('hidden');
       
        var email = $("#txtAgregarEmail").val();
        var telefono = $("#txtAgregarTelefono").val();
        var vemail = validar_email($.trim(email));
        var vtelefono = validar_numeros($.trim(telefono));
        if (($.trim(email) == "" || vemail === true) && ($.trim(telefono) == "" || vtelefono === true)) {
            if (accion === 'guardar') {
                guardarNuevoGrupo();
            } else {
                actualizarGrupo();
            }
        }
        else if ((vemail === false) && (vtelefono === false)) {
            $('#lblErrEmail').removeClass("hidden").text("Debe ingresar formato correcto");
            $('#lblErrTelefono').removeClass("hidden").text("Debe ingresar formato correcto");
            $("#divTxtAgregarEmail").addClass("has-error");
            $("#divTxtAgregarTelefono").addClass("has-error");
        }
        else if (vemail == false) {
            $('#lblErrEmail').removeClass("hidden").text("Debe ingresar formato correcto");
            $("#divTxtAgregarEmail").addClass("has-error");
        }
        else if (vtelefono == false) {
            $('#lblErrTelefono').removeClass("hidden").text("Debe ingresar formato correcto");
            $("#divTxtAgregarTelefono").addClass("has-error");
        }

    });

    $("#btnEliminar").on('click', function () {
        var confir = confirm("¿Desea eliminar el GRUPO DE CUENTAS?");
        if (confir == true) {
            eliminaAsociacion();
        }
    });

    $("#btnGuardarAsociar").on('click', function () {
        var v_idGrupo = rows_selectGrupo[0];
        $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
        $.ajax({
            type: "post",
            url: "../Servicios/GrupoCuenta.asmx/eliminarAsociacionGrupoCuenta",
            data: "{'idGrupo':'" + v_idGrupo + "'}",
            datatype: "json",
            contentType: "application/json; charset=utf-8",
            complete: function (jsondata, stat) {
                if (stat == "success") {
                    guardarAsociacionGrupoCuenta();
                } else {
                    alertaColor(3, "Ocurrio un error al grabar la operacion");
                }
                $("#panelAgregar").addClass('hidden');
                $("#panelGrupos").removeClass('hidden');
                $("#panelCuentas").addClass('hidden');
                $('#loaderProceso').modal('hide');
            }
        });
    });

    $("#txtAgregarGrupo").keyup(function () {
        var txtGrupo = $("#txtAgregarGrupo").val().trim();
        $('#lblErrNomGrupo').addClass("hidden");
        $('#lblErrEmail').addClass("hidden");
        $('#lblErrTelefono').addClass("hidden");
        desactivaBordetxtGuardar();
        if (txtGrupo != '') {
            $("#DDAsesorAgregar").prop("disabled", false);
            $("#txtAgregarDireccion").prop("disabled", false);
            $("#txtAgregarEmail").prop("disabled", false);
            $("#txtAgregarTelefono").prop("disabled", false);
            $('#btnGuardarGrupo').prop("disabled", false);
        }
        else {
            $("#DDAsesorAgregar").prop("disabled", true);
            $("#txtAgregarDireccion").prop("disabled", true);
            $("#txtAgregarEmail").prop("disabled", true);
            $("#txtAgregarTelefono").prop("disabled", true);
            $('#btnGuardarGrupo').prop("disabled", true);
        }
        validaNombreExistente();
    });
}

$(document).ready(function () {
    initMantenedorGrupos();
    cargarEventHandlersMantenedorGrupos();
    $(window).trigger('resize');
});