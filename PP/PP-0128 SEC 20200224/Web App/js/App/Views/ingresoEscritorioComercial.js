﻿var empresa = localStorage.DscEmpresa;
var usuario = localStorage.UserName;

function resize() {
    var height;
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - 27;
    $("#iframe").height(height);
}

function initIngresoEscritorioComercial() {
    $("#idSubtituloPaginaText").text("Escritorio Comercial: " + $("#DDTipoEscritorio option:selected").text());
    document.title = "Escritorio Comercial: " + $("#DDTipoEscritorio option:selected").text();

    estadoBtnConsultar();

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');
}
function cargarEventHandlersIngresoEscritorioComercial() {
    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $('#iframe').on('load', function () {
        $('#loaderProceso').modal('hide');
    });
    $("#btnBuscarCuentas").on('click', function () {
        limpiarIframe();
    });
    $("#btnLimpiarCuentas").on('click', function () {
        limpiarIframe();
    });
    $("#DDTipoEscritorio").on('change', function () {
        $("#idSubtituloPaginaText").text("Escritorio Comercial: " + $("#DDTipoEscritorio option:selected").text());
        document.title = "Escritorio Comercial: " + $("#DDTipoEscritorio option:selected").text();
    });
    $("#BtnConsultar").on('click', function () {
        generarURLExterna();
    });
    $("#porCuenta_NroCuenta").change(function () {
        estadoBtnConsultar();
    });
}

function limpiarIframe() {
    $("#iframe").attr('src', 'about: blank');
}
function generarURLExterna() {
    var rutCliente;
    var rutAsesor;
    var NumCuenta;
    rutCliente = miInformacionCuenta == null ? "" : miInformacionCuenta.RutCliente;
    rutAsesor = miInformacionCuenta == null ? "" : miInformacionCuenta.RutAsesor;
    numCuenta = miInformacionCuenta == null ? "" : miInformacionCuenta.NroCuenta;
    if (!miInformacionCuenta) {
        return;
    }
    if (!rutAsesor) {
        alert("La cuenta seleccionada no posee un Asesor, configúrelo en GPI");
        return;
    }
    $('#loaderProceso').modal('show');
    $.ajax({
        url: "../Servicios/ClaveExternaiFrame.asmx/GenerarURLExterna",
        data: "{'RutAsesor':'" + rutAsesor +
        "', 'RutCliente':'" + rutCliente +
        "', 'NumCuenta':'" + numCuenta +
        "', 'CodigoOperacion':'" + $("#DDTipoEscritorio").val() + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#iframe").attr('src', mydata);
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function estadoBtnConsultar() {
    if (!miInformacionCuenta) {
        $('#BtnConsultar').tooltip('enable').addClass('disabled');
    } else {
        $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
    }
}

$(document).ready(function () {
    initIngresoEscritorioComercial();
    cargarEventHandlersIngresoEscritorioComercial();
    $(window).trigger('resize');
});