﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="CarteraDetalleOperacion.aspx.vb" Inherits="AplicacionWeb.CarteraDetalleOperacion" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/carteraDetalleOperacion.min.js") %>'></script>

    <div class="row top-buffer">
        <div id="encSubPantalla" class="col-sm-12">
            <div class="panel panel-default" style="margin-bottom: 0px;">
                <div class="panel-body">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <label class="control-label col-xs-6"><strong>Transacción</strong></label>
                        <p class="col-xs-6" id="txtFechaTransaccion" style="text-align: right"></p>
                    </div>
                    <div class="row hidden-sm hidden-md hidden-lg">
                        <div class="col-xs-12">
                            <hr style="margin-top: 4px; margin-bottom: 7px;">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <label class="control-label col-xs-6"><strong>Tipo</strong></label>
                        <p class="col-xs-6" id="txtOperacion" style="text-align: right"></p>
                    </div>
                    <div class="row hidden-md hidden-lg">
                        <div class="col-xs-12">
                            <hr style="margin-top: 4px; margin-bottom: 7px;">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <label class="control-label col-xs-6"><strong>Monto</strong></label>
                        <p class="col-xs-6" id="txtMontoTransaccion" style="text-align: right"></p>
                    </div>
                    <div class="row hidden-sm hidden-lg">
                        <div class="col-xs-12">
                            <hr style="margin-top: 4px; margin-bottom: 7px;">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <label class="control-label col-xs-6" id="idlblComision"><strong>Comisión</strong></label>
                        <p class="col-xs-6" id="txtComision" style="text-align: right"></p>
                    </div>
                    <div class="row hidden-md">
                        <div class="col-xs-12">
                            <hr style="margin-top: 4px; margin-bottom: 7px;">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <label class="control-label col-xs-6"><strong>Derechos</strong></label>
                        <p class="col-xs-6" id="txtDerechos" style="text-align: right"></p>
                    </div>
                    <div class="row hidden-sm hidden-md hidden-lg">
                        <div class="col-xs-12">
                            <hr style="margin-top: 4px; margin-bottom: 7px;">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <label class="control-label col-xs-6"><strong>Gastos</strong></label>
                        <p class="col-xs-6" id="txtGastos" style="text-align: right"></p>
                    </div>
                    <div class="row hidden-lg">
                        <div class="col-xs-12">
                            <hr style="margin-top: 4px; margin-bottom: 7px;">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <label class="control-label col-xs-6"><strong>Afectos a IVA</strong></label>
                        <p class="col-xs-6" id="txtAfectosIva" style="text-align: right"></p>
                    </div>
                    <div class="row hidden-sm hidden-md hidden-lg">
                        <div class="col-xs-12">
                            <hr style="margin-top: 4px; margin-bottom: 7px;">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <label class="control-label col-xs-6"><strong>IVA</strong></label>
                        <p class="col-xs-6" id="txtIVA" style="text-align: right"></p>
                    </div>
                    <div class="row hidden-md">
                        <div class="col-xs-12">
                            <hr style="margin-top: 4px; margin-bottom: 7px;">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <label class="control-label col-xs-6"><strong>Liquidación</strong></label>
                        <p class="col-xs-6" id="txtFechaLiquidacion" style="text-align: right"></p>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <hr size="30" style="margin-top: 4px; margin-bottom: 7px; border-top: 5px solid #eee;">
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-offset-6 col-md-4 col-md-offset-8 col-lg-3 col-lg-offset-9">
                        <label class="control-label col-xs-6"><strong>TOTAL</strong></label><p class="col-xs-6" id="txtTOTAL" style="text-align: right"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="DivGrillaDetOper" class="row top-buffer">
        <div class="col-sm-12">
            <table id="Lista">
                <tr>
                    <td></td>
                </tr>
            </table>
            <div id="paginadorDetalleOperacion">
            </div>
        </div>
    </div>
</asp:Content>