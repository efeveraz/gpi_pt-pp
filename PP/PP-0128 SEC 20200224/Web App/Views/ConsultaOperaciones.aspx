﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="ConsultaOperaciones.aspx.vb" Inherits="AplicacionWeb.ConsultaOperaciones" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaOperaciones.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <div id="dtRangoFecha" class="input-daterange input-group">
                        <input id="dtFechaConsulta" type="text" class="form-control" />
                        <span class="input-group-addon">a</span>
                        <input id="dtFechaConsultaHasta" type="text" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-sm-12" id="panelDerecho">
            <div class="row">
                <div class="col-sm-12">
                    <table id="ListaOperaciones">
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                    <div id="Paginador">
                    </div>
                </div>
            </div>
            <div class="row top-buffer">
                <div class="col-sm-12">
                    <table id="ListaDetalleOperaciones">
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
