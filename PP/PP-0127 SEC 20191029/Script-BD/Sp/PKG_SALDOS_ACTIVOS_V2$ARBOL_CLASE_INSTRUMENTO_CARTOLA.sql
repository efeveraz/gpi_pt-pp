USE[CSGPI]
GO
IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PKG_SALDOS_ACTIVOS_V2$ARBOL_CLASE_INSTRUMENTO_CARTOLA')
   DROP PROCEDURE PKG_SALDOS_ACTIVOS_V2$ARBOL_CLASE_INSTRUMENTO_CARTOLA
GO
CREATE PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS_V2$ARBOL_CLASE_INSTRUMENTO_CARTOLA]
( @PID_CUENTA  NUMERIC,
  @PID_EMPRESA NUMERIC,
  @PFECHA_CIERRE DATETIME,
  @PMERCADO_TRANSACCION NUMERIC = NULL,
  @PID_MONEDA NUMERIC = NULL,
  @PID_SECTOR NUMERIC = NULL
)AS
BEGIN
SET NOCOUNT ON
       DECLARE @LDECIMALES NUMERIC

       DECLARE @SALIDA TABLE (ID                        NUMERIC,
                              ID_PADRE                  NUMERIC,
                              DSC_ARBOL_CLASE_INST_RAMA VARCHAR(100),
                              DSC_ARBOL_CLASE_INST_HOJA VARCHAR(100),
                              MONTO_MON_CTA_INST        NUMERIC(18,2),
                              MONTO_MON_CTA_HOJA        NUMERIC(18,2),
                              CODIGO                    VARCHAR(20),
                              NIVEL                     INT ,
                              ORDEN                     NUMERIC,
                              CODIGO_PADRE              VARCHAR(20)  )

       DECLARE @Z TABLE (ID_Z           NUMERIC,
                         DSC_ARBOL_RAMA VARCHAR(100),
                         DSC_ARBOL      VARCHAR(100),
                         PADRE          NUMERIC,
                         CODIGO         VARCHAR(10),
                         ORDEN          NUMERIC        )


       DECLARE @Z_2 TABLE (ID_Z_2           NUMERIC,
                           DSC_ARBOL_RAMA_2 VARCHAR(100),
                           DSC_ARBOL_2      VARCHAR(100),
                           PADRE_2          NUMERIC,
                           CODIGO_2         VARCHAR(10),
                           ORDEN            NUMERIC    )

       DECLARE @ARBOL TABLE (ID_ARBOL NUMERIC,
                             SECUENCIA VARCHAR(100),
                             NIVEL  NUMERIC,
                             VALOR_RAMA NUMERIC(18,2),
                             VALOR_HOJA NUMERIC(18,2))

       SELECT @LDECIMALES=DICIMALES_MOSTRAR
         FROM MONEDAS
        WHERE ID_MONEDA = (SELECT ID_MONEDA FROM CUENTAS WHERE ID_CUENTA=@PID_CUENTA)

       INSERT @Z
       SELECT ID_ARBOL_CLASE_INST AS ID,
              DSC_ARBOL_CLASE_INST AS DSC_ARBOL_RAMA,
              DSC_ARBOL_CLASE_INST AS DSC_ARBOL,
              ID_PADRE_ARBOL_CLASE_INST AS PADRE,
              CODIGO AS CODIGO,
              ORDEN AS ORDEN
         FROM ARBOL_CLASE_INSTRUMENTO
        WHERE ID_EMPRESA = @PID_EMPRESA
          AND ID_ACI_TIPO = 1
       ORDER BY ORDEN

       INSERT @Z_2
       SELECT * FROM @Z


       INSERT @ARBOL
       SELECT ID_Z,
              RIGHT(SPACE(10) + CONVERT(VARCHAR(10),ID_Z),10),
              1,
              ISNULL(DBO.PKG_GLOBAL$MONTO_MON_CUENTA_RAMA(ID_Z, @PID_CUENTA, @PFECHA_CIERRE),0) AS VALOR_RAMA,
              0
         FROM @Z
        WHERE PADRE IS NULL

       DECLARE @I INT
       SELECT @I = 0

       WHILE @@ROWCOUNT > 0
       BEGIN
            SELECT @I = @I + 1
            INSERT @ARBOL
            SELECT ID_Z,
                   SECUENCIA + RIGHT(SPACE(10) + CONVERT(VARCHAR(10),ID_Z),10),
                   @I + 1,
                   ISNULL(DBO.PKG_GLOBAL$MONTO_MON_CUENTA_HOJA(ID_ARBOL
                                                             , @PID_CUENTA
                                                             , @PFECHA_CIERRE
                                                             , @PMERCADO_TRANSACCION
                                                             , @PID_MONEDA
                                             , @PID_SECTOR),0),
                   0
              FROM @Z, @ARBOL
             WHERE NIVEL = @I
               AND PADRE = ID_ARBOL
       END


       UPDATE @ARBOL  SET VALOR_HOJA = ISNULL(DBO.PKG_GLOBAL$MONTO_MON_CUENTA_HOJA(ID_ARBOL
                                                                                 , @PID_CUENTA
                                                                                 , @PFECHA_CIERRE
                                                                                 , @PMERCADO_TRANSACCION
                                                                                 , @PID_MONEDA
                                                                                 , @PID_SECTOR),0)
        WHERE NIVEL <> 1

       UPDATE @ARBOL  SET VALOR_HOJA = ( SELECT SUM(VALOR_HOJA) FROM @ARBOL , @Z
                                          WHERE ID_ARBOL = ID_Z
                                            AND  PADRE = ID_ARBOL)
        WHERE (SELECT COUNT(*) FROM @Z WHERE PADRE = ID_ARBOL) > 1


       UPDATE @Z SET DSC_ARBOL_RAMA = (SELECT DSC_ARBOL FROM @ARBOL , @Z_2 WHERE ID_ARBOL = ID_Z_2 AND ID_ARBOL = PADRE_2)

       INSERT INTO @SALIDA
       SELECT ID_Z,
              PADRE,
              (SELECT DSC_ARBOL_2 FROM @ARBOL , @Z_2  WHERE ID_ARBOL = ID_Z_2 AND ID_ARBOL = PADRE) ,
              DSC_ARBOL,
              ISNULL(DBO.PKG_GLOBAL$MONTO_MON_CUENTA_RAMA(PADRE, @PID_CUENTA, @PFECHA_CIERRE),0),
              VALOR_HOJA,
              CODIGO,
              nivel,
              ORDEN,
              (SELECT CODIGO_2 FROM @Z_2   WHERE ID_Z_2= case when padre is null then ID_Z else padre end)
         FROM @ARBOL, @Z
        WHERE ID_ARBOL = ID_Z
       ORDER BY SECUENCIA

       DELETE FROM @SALIDA WHERE DSC_ARBOL_CLASE_INST_RAMA IS NULL

       SELECT ID_PADRE AS ID_ARBOL_CLASE_INST,
              ID   AS ID_ARBOL_CLASE_INST_HOJA,
              DSC_ARBOL_CLASE_INST_RAMA AS DSC_ARBOL_CLASE_INST,
              DSC_ARBOL_CLASE_INST_HOJA AS DSC_ARBOL_CLASE_INST_HOJA,
              CASE MONTO_MON_CTA_HOJA WHEN 0 THEN 0
                                      ELSE ISNULL(DBO.Pkg_Global$CONS_CANTIDAD_POR_HOJA(@PFECHA_CIERRE
                                                        ,DSC_ARBOL_CLASE_INST_RAMA
                                                        ,DSC_ARBOL_CLASE_INST_HOJA
                                                        ,'CTA'
                                                        ,@PID_CUENTA)

                                              ,0)
           END AS CANTIDAD_HOJA,
              ROUND(MONTO_MON_CTA_INST,@LDECIMALES)   AS MONTO_MON_CTA_INST,
              ROUND(MONTO_MON_CTA_HOJA,@LDECIMALES)   AS MONTO_MON_CTA_HOJA,
              CODIGO      AS CODIGO  ,
              (SELECT COUNT(*) FROM @SALIDA WHERE ID_PADRE = S.ID ) AS NIVEL,
              CODIGO_PADRE
         FROM @SALIDA   S
       ORDER BY orden
END
GO
GRANT EXECUTE ON [PKG_SALDOS_ACTIVOS_V2$ARBOL_CLASE_INSTRUMENTO_CARTOLA] TO DB_EXECUTESP
Go