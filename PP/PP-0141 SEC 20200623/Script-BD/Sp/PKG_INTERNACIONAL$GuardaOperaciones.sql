USE CSGPI
GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_INTERNACIONAL$GuardaOperaciones]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_INTERNACIONAL$GuardaOperaciones]
GO
CREATE PROCEDURE [dbo].[PKG_INTERNACIONAL$GuardaOperaciones]
( @PID_CUENTA                 NUMERIC
, @PFLG_TIPO_MOVIMIENTO       VARCHAR(1)
, @PDSC_OPERACION             VARCHAR(200) = NULL
, @PFECHA_OPERACION           DATETIME
, @PFECHA_LIQUIDACION         DATETIME
, @PID_NEMOTECNICO            NUMERIC
, @PCANTIDAD                  NUMERIC(18, 4)
, @PPRECIO                    NUMERIC(18, 4)
, @PMONTO_PAGO                NUMERIC(18, 4)
, @PFECHA_VENCIMIENTO         DATETIME  = NULL
, @PPORC_COMISION             NUMERIC(18, 6) = 0
, @PCOMISION                  NUMERIC(18, 4) = 0
, @PDERECHOS                  NUMERIC(18, 4) = 0
, @PGASTOS                    NUMERIC(18, 4) = 0
, @PIVA                       NUMERIC(18, 4) = 0
, @PMONTO_OPERACION           NUMERIC(18, 4) = NULL
, @PCOD_ORIGEN                VARCHAR(3)
, @PVALOR_REL_CONV            VARCHAR(50)
, @PID_OPERACION              NUMERIC = NULL  OUTPUT
, @PNOMBRE_MONEDA_INT         VARCHAR(10) = NULL
, @PCOD_TIPO_OPERACION        VARCHAR(100) = NULL
, @PMONTO_MONEDA_INT          NUMERIC(18, 4) = NULL
, @PID_CAJA_CUENTA            NUMERIC = NULL
, @PFACTURA                   INT = NULL
, @PAPORTERETIRO              VARCHAR(2) = 'NO'
, @PRESULTADO                 VARCHAR(1000) OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY
  DECLARE @LPROCESO VARCHAR(100)
  SET @LPROCESO = 'PKG_INTERNACIONAL$GuardaOperaciones'

  DECLARE @LRESULT                INT
  ---------------------------------------------------
  DECLARE @LCOD_ESTADO            VARCHAR(1)
  DECLARE @LCOD_PRODUCTO          VARCHAR(100)
  DECLARE @LFLG_CONFIRMACION      VARCHAR(1)
  DECLARE @LCOD_INSTRUMENTO       VARCHAR(100)
  DECLARE @LID_OPERACION_DETALLE  NUMERIC
  DECLARE @LFLG_LIMITE_PRECIO     VARCHAR(1)
  DECLARE @LFLG_TIPO_ORIGEN       VARCHAR(1)
  DECLARE @LCOD_TIPO_OPERACION    VARCHAR(100)
  DECLARE @LID_MONEDA_OPERACION   NUMERIC
  DECLARE @LID_MONEDA_PAGO        NUMERIC
  DECLARE @LBASE                  NUMERIC
  DECLARE @LID_TIPO_CONVERSION    NUMERIC
  DECLARE @LCOD_ORIGEN            NUMERIC
  DECLARE @LID_MOV_ACTIVO         NUMERIC

--------------------------------------------------------
  SET @LFLG_LIMITE_PRECIO     = 'L'
  SET @LFLG_TIPO_ORIGEN       = 'M'
  SET @LCOD_ESTADO            = DBO.PKG_GLOBAL$CCOD_ESTADO_PENDIENTE()

  IF ISNULL(@pCOD_TIPO_OPERACION,'') = ''
     BEGIN
          SET @LCOD_TIPO_OPERACION    = 'INST_INT'
     END
  ELSE
    BEGIN
          SET @LCOD_TIPO_OPERACION = @PCOD_TIPO_OPERACION
          IF @pCOD_TIPO_OPERACION IN ('CUST', 'CUST_NOCAP')
              BEGIN
                   SET @LCOD_ESTADO =  DBO.PKG_GLOBAL$CCOD_ESTADO_CONFIRMADO()
              END
     END
--------------------------------------------------------

  IF @PMONTO_OPERACION IS NULL
     BEGIN
          IF @PFLG_TIPO_MOVIMIENTO = 'E'
             /*Cuando es VENTA debe ser as�: la suma de todas las l�neas de la OPERACI�N_DETALLE � COMISION = TOTAL_OPERACION*/
             SET @PMONTO_OPERACION = @PMONTO_PAGO - (@PCOMISION + @PDERECHOS + @PGASTOS + @PIVA)
          ELSE
             /*Cuando es COMPRA debe ser as�: la suma de todas las l�neas de la OPERACI�N_DETALLE + COMISION = TOTAL_OPERACION*/
             SET @PMONTO_OPERACION = @PMONTO_PAGO + @PCOMISION + @PDERECHOS + @PGASTOS + @PIVA
     END

  SELECT @LID_MONEDA_OPERACION  = N.ID_MONEDA_TRANSACCION
       , @LID_MONEDA_PAGO       = N.ID_MONEDA_TRANSACCION
       , @LCOD_PRODUCTO         = I.COD_PRODUCTO
       , @LCOD_INSTRUMENTO      = I.COD_INSTRUMENTO
       , @LBASE                 = N.BASE
    FROM NEMOTECNICOS N
       , INSTRUMENTOS I
   WHERE N.ID_NEMOTECNICO = @PID_NEMOTECNICO
     AND I.COD_INSTRUMENTO = N.COD_INSTRUMENTO;

  EXECUTE PKG_OPERACIONES$GUARDAR
          @PID_OPERACION             = @PID_OPERACION OUTPUT
        , @PID_CUENTA                = @PID_CUENTA
        , @PCOD_TIPO_OPERACION       = @LCOD_TIPO_OPERACION
        , @PCOD_ESTADO               = @LCOD_ESTADO
        , @PID_CONTRAPARTE           = NULL
        , @PID_TRADER                = NULL
        , @PID_REPRESENTANTE         = NULL
        , @PID_MONEDA_OPERACION      = @LID_MONEDA_OPERACION
        , @PCOD_PRODUCTO             = @LCOD_PRODUCTO
        , @PCOD_INSTRUMENTO          = @LCOD_INSTRUMENTO
        , @PFLG_TIPO_MOVIMIENTO      = @PFLG_TIPO_MOVIMIENTO
        , @PFECHA_OPERACION          = @PFECHA_OPERACION
        , @PFECHA_VIGENCIA           = @PFECHA_OPERACION
        , @PFECHA_LIQUIDACION        = @PFECHA_LIQUIDACION
        , @PDSC_OPERACION            = @PDSC_OPERACION
        , @PFECHA_CONFIRMACION       = NULL
        , @PPORC_COMISION            = @PPORC_COMISION
        , @PCOMISION                 = @PCOMISION
        , @PDERECHOS                 = @PDERECHOS
        , @PGASTOS                   = @PGASTOS
        , @PIVA                      = @PIVA
        , @PMONTO_OPERACION          = @PMONTO_OPERACION
        , @PFLG_LIMITE_PRECIO        = @LFLG_LIMITE_PRECIO
        , @PFLG_TIPO_ORIGEN          = @LFLG_TIPO_ORIGEN
        , @PCONFIRMA_COMISION        = NULL
        , @PCONFIRMA_DERECHOS        = NULL
        , @PCONFIRMA_GASTOS          = NULL
        , @PCONFIRMA_IVA             = NULL
        , @PCONFIRMA_MONTO_OPERACION = NULL
        , @PAPORTERETIRO             = @PAPORTERETIRO
        , @PID_CARGO_ABONO           = NULL

  IF @PRESULTADO IS NOT NULL
  BEGIN
    RAISERROR(@PRESULTADO, 18, 1)
  END

  IF @PID_OPERACION IS NULL BEGIN
    SET @PRESULTADO = 'NO SE PUDO GUARDAR LA OPERACION'
    RAISERROR(@PRESULTADO, 18, 1)
  END

  EXECUTE PKG_OPERACIONES_DETALLE$GUARDAR_INT
            @PID_OPERACION_DETALLE    = @LID_OPERACION_DETALLE OUTPUT
          , @PID_OPERACION            = @PID_OPERACION
          , @PID_NEMOTECNICO          = @PID_NEMOTECNICO
          , @PCANTIDAD                = @PCANTIDAD
          , @PPRECIO                  = @PPRECIO
          , @PPRECIO_GESTION          = NULL
          , @PPLAZO                   = NULL
          , @PBASE                    = @LBASE
          , @PFECHA_VENCIMIENTO       = @PFECHA_VENCIMIENTO
          , @PID_MONEDA_PAGO          = @LID_MONEDA_PAGO
          , @PVALOR_DIVISA            = NULL --ESTE CAMPO NUNCA SE HA LLENDA :P
          , @PMONTO_PAGO              = @PMONTO_PAGO
          , @PCOMISION                = NULL
          , @PINTERES                 = NULL
          , @PUTILIDAD                = NULL
          , @PPORC_COMISION           = NULL
          , @PMONTO_BRUTO             = @PMONTO_PAGO
          , @PFLG_MONTO_REFERENCIADO  = NULL
          , @PFLG_TIPO_DEPOSITO       = NULL
          , @PFECHA_VALUTA            = NULL
          , @PFLG_VENDE_TODO          = 'N'
          , @PFECHA_LIQUIDACION       = @PFECHA_LIQUIDACION
          , @PPRECIO_HISTORICO_COMPRA = NULL
          , @PID_MOV_ACTIVO_COMPRA    = NULL
          , @PFLG_LIMITE_PRECIO       = NULL
          , @PNOMBRE_MONEDA_INT       = @PNOMBRE_MONEDA_INT
          , @PMONTO_MONEDA_INT        = @PMONTO_MONEDA_INT

  IF @PRESULTADO IS NOT NULL
  BEGIN
    RAISERROR(@PRESULTADO, 18, 1)
  END

  IF @LID_OPERACION_DETALLE IS NULL
     BEGIN
       SET @PRESULTADO = 'NO SE PUDO GUARDAR EL DETALLE DE LA OPERACION'
       RAISERROR(@PRESULTADO, 18, 1)
     END
  ELSE
   BEGIN
     IF @pCOD_TIPO_OPERACION IN ('CUST', 'CUST_NOCAP')
      BEGIN
          EXECUTE [PKG_MOV_ACTIVOS$GUARDAR]
                @PID_MOV_ACTIVO         = @LID_MOV_ACTIVO OUTPUT
              , @PID_CUENTA             = @PID_CUENTA
              , @PID_NEMOTECNICO        = @PID_NEMOTECNICO
              , @PID_OPERACION_DETALLE  = @LID_OPERACION_DETALLE
              , @PFLG_TIPO_MOVIMIENTO   = @PFLG_TIPO_MOVIMIENTO
              , @PFECHA_OPERACION       = @PFECHA_OPERACION
              , @PFECHA_MOVIMIENTO      = @PFECHA_OPERACION
              , @PFECHA_CONFIRMACION    = @PFECHA_OPERACION
              , @PCANTIDAD              = @PCANTIDAD
              , @PPRECIO                = @PPRECIO
              , @PTASA                  = 0
              , @PID_MONEDA             = @LID_MONEDA_PAGO
              , @PID_SALDO_ACTIVO       = NULL
              , @PMONTO                 = @PMONTO_PAGO
              , @PMONTO_TOTAL           = @PMONTO_PAGO
              , @PCOD_ESTADO            = 'A' --PARA NO CONSIDERARLO EN EL CIERRE
              , @PID_MOV_ACTIVO_COMPRA  = NULL


          IF @LID_MOV_ACTIVO IS NULL
           BEGIN
                SET @PRESULTADO = 'NO SE PUDO GUARDAR EL DETALLE DE LA OPERACION'
                RAISERROR(@PRESULTADO, 18, 1)
           END
      END

      IF @PCOD_ORIGEN = 'INV' /* Grabar relaci�n numero operacion externa - numero operacion detalle para inversis */
       BEGIN
              SET @LCOD_ORIGEN = (SELECT ID_ORIGEN FROM ORIGENES WHERE COD_ORIGEN = 'INVERSIS')
              SET @LID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WHERE DSC_TIPO_CONVERSION = 'CONV. A OPER DETALLE'
                                                                                        AND TABLA = 'OPERACIONES_DETALLE')
              INSERT INTO REL_CONVERSIONES (ID_ORIGEN,ID_TIPO_CONVERSION,ID_ENTIDAD,VALOR)
              VALUES  (@LCOD_ORIGEN,@LID_TIPO_CONVERSION,@LID_OPERACION_DETALLE,@PVALOR_REL_CONV)

              IF @PCOD_TIPO_OPERACION = 'DIREC'
                 BEGIN
                    EXECUTE PKG_OPERACIONES$MAKE_DIRECT_CONFIRMATION  @PID_OPERACION = @PID_OPERACION,
                                                                    @PID_CAJA_CUENTA  = @PID_CAJA_CUENTA
                    UPDATE MOV_ACTIVOS
                       SET COD_ESTADO='A'
                     WHERE ID_OPERACION_DETALLE=@LID_OPERACION_DETALLE
                  END

             /* GRABA RELACION OPERACIONES DETALLE ATRIBUTOS*/
             EXECUTE PKG_REL_INGR_OPERACION_DET_ATRIBUTOS @PID_OPERACION_DETALLE = @LID_OPERACION_DETALLE,
                                                          @PID_OPERACION         = @PID_OPERACION        ,
                                                          @PCOMISION             = @PCOMISION            ,
                                                          @PDERECHOS             = @PDERECHOS            ,
                                                          @PGASTOS               = @PGASTOS              ,
                                                          @PIVA                  = @PIVA                 ,
                                                          @PFACTURA              = @PFACTURA             ,
                                                          @PGARANTIAS            = 0                     ,
                                                          @PPRESTAMOS            = 0                     ,
                                                          @PSIMULTANEAS          = 0                     ,
                                                          @PBOLSA                = ''                    ,
                                                          @PTIPO_LIQ             = ''                    ,
                                                          @PCUST_NOM             = ''
       END

      IF @PCOD_ORIGEN = 'PSH'
       BEGIN
               SET @LCOD_ORIGEN = (SELECT ID_ORIGEN FROM ORIGENES WHERE COD_ORIGEN = 'PERSHING')
               SET @LID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION
               WHERE TABLA = 'OPERACIONES_DETALLE')
               INSERT INTO REL_CONVERSIONES (ID_ORIGEN,ID_TIPO_CONVERSION,ID_ENTIDAD,VALOR)
               VALUES (@LCOD_ORIGEN,@LID_TIPO_CONVERSION,@LID_OPERACION_DETALLE,@PVALOR_REL_CONV)

              IF @PCOD_TIPO_OPERACION = 'DIREC'
            BEGIN
                    EXECUTE PKG_OPERACIONES$MAKE_DIRECT_CONFIRMATION  @PID_OPERACION = @PID_OPERACION,
                                                                    @PID_CAJA_CUENTA  = @PID_CAJA_CUENTA
                    UPDATE MOV_ACTIVOS
                       SET COD_ESTADO='A'
                     WHERE ID_OPERACION_DETALLE=@LID_OPERACION_DETALLE
                  END

       END
   END

  SET @PRESULTADO = NULL
  SELECT @PRESULTADO  AS 'Resultado', @PID_OPERACION  'ID_OPERACION'
END TRY
BEGIN CATCH
  SET @PRESULTADO =  '[' + ERROR_PROCEDURE() + ':' + CAST(ERROR_LINE() AS NVARCHAR) + ':' + CAST(ERROR_NUMBER() AS NVARCHAR) + ']' + CHAR(13) +
                     ' ERROR=' + ERROR_MESSAGE()
  SELECT @PRESULTADO  AS 'Resultado', @PID_OPERACION  'ID_OPERACION'
END CATCH
GO
GRANT EXECUTE ON [PKG_INTERNACIONAL$GuardaOperaciones] TO DB_EXECUTESP
GO