IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HAF_PROCESOCIERRE_SALDOSACTIVOS_PRECIOS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[HAF_PROCESOCIERRE_SALDOSACTIVOS_PRECIOS]
GO

CREATE PROCEDURE DBO.HAF_PROCESOCIERRE_SALDOSACTIVOS_PRECIOS
( @PID_CUENTA   NUMERIC
, @PFECHA       DATETIME
, @PID_USUARIO  NUMERIC
, @PRESULTADO   VARCHAR(1000) OUTPUT
) --WITH ENCRYPTION
AS
SET NOCOUNT ON
BEGIN TRY
  DECLARE @LPROCESO VARCHAR(100)
  SET @LPROCESO = 'HAF_PROCESOCIERRE_SALDOSACTIVOS_PRECIOS'

  INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)
  VALUES (@PID_USUARIO, @LPROCESO, REPLICATE(CHAR(9), 2) + 'BUSCANDO LOS PRECIO PARA LOS SALDOS...')

--------------------------------------------------------------------------------------------
  DECLARE @LCTA_FUNGIR_RF CHAR(1)
  -- SI LA CUENTA EXISTE EN LA TABLA ENTONCES NO DEBE FUNGIR
  IF (SELECT COUNT(1) FROM CUENTAS_SIN_FUNGIR_RF WITH (NOLOCK) WHERE ID_CUENTA = @PID_CUENTA) = 0
     SET @LCTA_FUNGIR_RF = 'S'
  ELSE
     SET @LCTA_FUNGIR_RF = 'N'
--------------------------------------------------------------------------------------------

  DECLARE @LCURSOR$ID_NEMOTECNICO NUMERIC
        , @LCURSOR$NEMOTECNICO    VARCHAR(100)
        , @LPRECIOTASA            FLOAT
        , @LPRECIOTASA_COMPRA     FLOAT

  DECLARE @LCURSOR$COD_INSTRUMENTO  VARCHAR(100)
        , @LCURSOR$ID_PUBLICADOR    NUMERIC
        , @LCURSOR$COD_PRODUCTO     VARCHAR(100)
        , @LCURSOR$TIPO_PRECIO      CHAR
        , @LFECHA_ANT               DATETIME

  DECLARE @LCURSORNEMOS CURSOR

  DECLARE @LCURSOR CURSOR
  SET @LCURSOR = CURSOR
  FOR
    SELECT N.COD_INSTRUMENTO
         , DBO.PKG_PUBLICADORES$BUSCAR_X_CTA(@PID_CUENTA, N.COD_INSTRUMENTO) AS ID_PUBLICADOR
         , I.COD_PRODUCTO
         , CASE I.COD_PRODUCTO
             WHEN 'RF_NAC' THEN 'T'
             ELSE 'P'
             END AS TIPO_PRECIO
      FROM SALDOS_ACTIVOS SA  WITH (NOLOCK)
     INNER JOIN NEMOTECNICOS N WITH (NOLOCK) ON N.ID_NEMOTECNICO = SA.ID_NEMOTECNICO
     INNER JOIN INSTRUMENTOS I WITH (NOLOCK) ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
     WHERE SA.ID_CUENTA = @PID_CUENTA
       AND SA.FECHA_CIERRE = @PFECHA
     GROUP BY N.COD_INSTRUMENTO, I.COD_PRODUCTO

  OPEN @LCURSOR

  FETCH NEXT FROM @LCURSOR
   INTO @LCURSOR$COD_INSTRUMENTO
      , @LCURSOR$ID_PUBLICADOR
      , @LCURSOR$COD_PRODUCTO
      , @LCURSOR$TIPO_PRECIO

  WHILE @@FETCH_STATUS = 0
  BEGIN
    INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)
    VALUES (@PID_USUARIO, @LPROCESO, REPLICATE(CHAR(9), 3) + ISNULL(@LCURSOR$COD_INSTRUMENTO, 'NULL') +  '...')

    SET @LCURSORNEMOS = CURSOR
    FOR
      SELECT SA.ID_NEMOTECNICO
           , N.NEMOTECNICO
        FROM SALDOS_ACTIVOS SA WITH (NOLOCK)
       INNER JOIN NEMOTECNICOS N WITH (NOLOCK) ON N.ID_NEMOTECNICO  = SA.ID_NEMOTECNICO
                                              AND N.COD_INSTRUMENTO = @LCURSOR$COD_INSTRUMENTO
       WHERE SA.ID_CUENTA = @PID_CUENTA
         AND SA.FECHA_CIERRE = @PFECHA
       GROUP BY SA.ID_NEMOTECNICO, N.NEMOTECNICO

    OPEN @LCURSORNEMOS

    FETCH NEXT FROM @LCURSORNEMOS
     INTO @LCURSOR$ID_NEMOTECNICO
        , @LCURSOR$NEMOTECNICO

    WHILE @@FETCH_STATUS = 0
    BEGIN
      INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)
      VALUES (@PID_USUARIO, @LPROCESO, REPLICATE(CHAR(9), 4) + ISNULL(@LCURSOR$NEMOTECNICO, 'NULL') +  '...')

      SET @LPRECIOTASA = dbo.PKG_PUBLICADORES_PRECIOS$BUSCAR_X_ID(@LCURSOR$ID_PUBLICADOR
                                                                , @LCURSOR$ID_NEMOTECNICO
                                                                , @PFECHA
                                                                , @LCURSOR$TIPO_PRECIO)
      SET @LPRECIOTASA_COMPRA = NULL

      IF NOT @LCURSOR$COD_PRODUCTO = 'RF_NAC'
      BEGIN
        SET @LPRECIOTASA_COMPRA = dbo.entrega_Precio_Promedio (@PFECHA                  --<@Fecha, datetime,>
                                                             , @PID_CUENTA              --<@idcuenta, numeric(10,0),>
                                                             , @LCURSOR$ID_NEMOTECNICO  --<@idnemo, numeric(10,0),>
                                                              )
        IF @LPRECIOTASA IS NULL
        BEGIN
          SET @LPRECIOTASA = @LPRECIOTASA_COMPRA
        END
      END
      ELSE
      BEGIN
        IF @LCTA_FUNGIR_RF = 'S'
         BEGIN
              --IF @LCURSOR$COD_INSTRUMENTO = 'BONOS_NAC'
              -- BEGIN
                    SELECT @LFECHA_ANT = DATEADD(DAY,-1,@PFECHA)
                    SET @LPRECIOTASA_COMPRA = dbo.FNT_ENTREGA_TASA_PROMEDIO (@LFECHA_ANT                  --<@Fecha, datetime,>
                                                                           , @PID_CUENTA              --<@idcuenta, numeric(10,0),>
                                                                           , @LCURSOR$ID_NEMOTECNICO  --<@idnemo, numeric(10,0),>
                                                                            )
                    IF @LPRECIOTASA_COMPRA IS NULL
                     BEGIN
                          SET @LPRECIOTASA_COMPRA = @LPRECIOTASA
                     END

                    IF @LPRECIOTASA IS NULL
                     BEGIN
                          SET @LPRECIOTASA = @LPRECIOTASA_COMPRA
                     END
             --END
         END
    END
    IF @LCTA_FUNGIR_RF = 'S'
     BEGIN
         UPDATE SALDOS_ACTIVOS
            SET PRECIO = CASE @LCURSOR$TIPO_PRECIO WHEN 'T' THEN NULL ELSE @LPRECIOTASA END
              , TASA   = CASE @LCURSOR$TIPO_PRECIO
                            WHEN 'T' THEN
                               CASE @LCURSOR$COD_INSTRUMENTO
                                  WHEN 'BONOS_NAC' THEN @LPRECIOTASA
                                  ELSE
                                     CASE
                                        WHEN @LPRECIOTASA IS NULL THEN
                                           (SELECT M.PRECIO
                                              FROM MOV_ACTIVOS M WITH (NOLOCK)
                                             WHERE M.ID_MOV_ACTIVO = SALDOS_ACTIVOS.ID_MOV_ACTIVO)
                                        ELSE @LPRECIOTASA
                                        END
                                  END
                            ELSE NULL
                            END
              , TASA_COMPRA = CASE @LCURSOR$COD_PRODUCTO
                                 WHEN 'RF_NAC' THEN @LPRECIOTASA_COMPRA
                                 --WHEN 'RF_NAC' THEN
                                 --   CASE @LCURSOR$COD_INSTRUMENTO
                                 --      WHEN 'BONOS_NAC' THEN @LPRECIOTASA_COMPRA
                                 --      ELSE (SELECT M.PRECIO
                                 --              FROM MOV_ACTIVOS M WITH (NOLOCK)
                                 --             WHERE M.ID_MOV_ACTIVO = SALDOS_ACTIVOS.ID_MOV_ACTIVO)
                                 --      END
                                 ELSE NULL
                                 END
             , PRECIO_COMPRA = @LPRECIOTASA_COMPRA
         WHERE ID_CUENTA       = @PID_CUENTA
           AND FECHA_CIERRE    = @PFECHA
           AND ID_NEMOTECNICO  = @LCURSOR$ID_NEMOTECNICO
     END
    ELSE
     BEGIN
         UPDATE SALDOS_ACTIVOS
            SET PRECIO = CASE @LCURSOR$TIPO_PRECIO WHEN 'T' THEN NULL ELSE @LPRECIOTASA END
              , TASA   = CASE @LCURSOR$TIPO_PRECIO
                            WHEN 'T' THEN
                               CASE
                                  WHEN @LPRECIOTASA IS NULL THEN (SELECT M.PRECIO
                                                                    FROM MOV_ACTIVOS M WITH (NOLOCK)
                                                                   WHERE M.ID_MOV_ACTIVO = SALDOS_ACTIVOS.ID_MOV_ACTIVO)
                                  ELSE @LPRECIOTASA
                                  END
                            ELSE NULL
                            END
              , TASA_COMPRA = CASE @LCURSOR$COD_PRODUCTO
                                 WHEN 'RF_NAC' THEN (SELECT M.PRECIO
                                                       FROM MOV_ACTIVOS M WITH (NOLOCK)
                                                      WHERE M.ID_MOV_ACTIVO = SALDOS_ACTIVOS.ID_MOV_ACTIVO)
                                 ELSE NULL
                                 END
              , PRECIO_COMPRA = @LPRECIOTASA_COMPRA
          WHERE ID_CUENTA       = @PID_CUENTA
            AND FECHA_CIERRE    = @PFECHA
            AND ID_NEMOTECNICO  = @LCURSOR$ID_NEMOTECNICO

     END
    FETCH NEXT FROM @LCURSORNEMOS
       INTO @LCURSOR$ID_NEMOTECNICO
          , @LCURSOR$NEMOTECNICO
    END

    CLOSE @LCURSORNEMOS
    --DEALLOCATE @LCURSORNEMOS

    FETCH NEXT FROM @LCURSOR
     INTO @LCURSOR$COD_INSTRUMENTO
        , @LCURSOR$ID_PUBLICADOR
        , @LCURSOR$COD_PRODUCTO
        , @LCURSOR$TIPO_PRECIO
  END

  CLOSE @LCURSOR
  DEALLOCATE @LCURSOR

  INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)
  VALUES (@PID_USUARIO, @LPROCESO, REPLICATE(CHAR(9), 2) + 'PRECIOS BUSCADOS.')

  SET @PRESULTADO = NULL
END TRY
BEGIN CATCH
  SET @PRESULTADO =  '[' + ERROR_PROCEDURE() + ':' + CAST(ERROR_LINE() AS NVARCHAR) + ':' + CAST(ERROR_NUMBER() AS NVARCHAR) + ']' + CHAR(13) +
                     ' ERROR=' + ERROR_MESSAGE()

  INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)
  VALUES (@PID_USUARIO, @LPROCESO, @PRESULTADO)
END CATCH
SET NOCOUNT OFF

GO
GRANT EXECUTE ON [HAF_PROCESOCIERRE_SALDOSACTIVOS_PRECIOS] TO DB_EXECUTESP
GO
