IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HAF_PROCESOCIERRE_CALCSALDOS_CARTERA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[HAF_PROCESOCIERRE_CALCSALDOS_CARTERA]
GO

CREATE PROCEDURE DBO.HAF_PROCESOCIERRE_CALCSALDOS_CARTERA
( @PID_CUENTA       NUMERIC
, @PFECHA           DATETIME
, @PID_USUARIO      NUMERIC
, @PRESULTADO       VARCHAR(1000) OUTPUT
) --WITH ENCRYPTION
AS
SET NOCOUNT ON
BEGIN TRY
  DECLARE @LPROCESO VARCHAR(100)
  SET @LPROCESO = 'HAF_PROCESOCIERRE_CALCSALDOS_CARTERA'

  INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)
  VALUES (@PID_USUARIO, @LPROCESO, CHAR(9) + CHAR(9) + CHAR(9) + 'CALCULANDO SALDO CARTERA...')

  DECLARE @LFCH_AYER DATETIME
  SET @LFCH_AYER = DATEADD(DAY, -1, @PFECHA)

  DECLARE @LID_CIERRE NUMERIC
  SELECT @LID_CIERRE = ID_CIERRE
    FROM CIERRES_CUENTAS WITH (NOLOCK)
   WHERE ID_CUENTA    = @PID_CUENTA
     AND FECHA_CIERRE = @PFECHA

--------------------------------------------------------------------------------------------
  DECLARE @LCTA_FUNGIR_RF CHAR(1)
  -- SI LA CUENTA EXISTE EN LA TABLA ENTONCES NO DEBE FUNGIR
  IF (SELECT COUNT(1) FROM CUENTAS_SIN_FUNGIR_RF WHERE ID_CUENTA = @PID_CUENTA) = 0
     SET @LCTA_FUNGIR_RF = 'S'
  ELSE
     SET @LCTA_FUNGIR_RF = 'N'
--------------------------------------------------------------------------------------------
  DECLARE @TMP_MOV TABLE
  ( ID_MOV_ACTIVO  NUMERIC
  , ID_NEMOTECNICO NUMERIC
  , CANTIDAD       NUMERIC(18,4)
  )

  IF @LCTA_FUNGIR_RF = 'N'
/*
  IF @LCTA_FUNGIR_RF = 'S'
   BEGIN

        INSERT INTO @TMP_MOV
        SELECT CASE M.FLG_TIPO_MOVIMIENTO
               WHEN DBO.PKG_GLOBAL$GCTIPOOPERACION_EGRESO() THEN M.ID_MOV_ACTIVO_COMPRA ELSE M.ID_MOV_ACTIVO END AS ID_MOV_ACTIVO
             , M.ID_NEMOTECNICO
             , SUM(M.CANTIDAD * CASE M.FLG_TIPO_MOVIMIENTO
                           WHEN DBO.PKG_GLOBAL$GCTIPOOPERACION_EGRESO() THEN -1
                           ELSE 1
                           END) AS CANTIDAD
          FROM MOV_ACTIVOS M  WITH (NOLOCK)
               INNER JOIN NEMOTECNICOS N WITH (NOLOCK) ON N.ID_NEMOTECNICO = M.ID_NEMOTECNICO
               INNER JOIN INSTRUMENTOS I WITH (NOLOCK) ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
                                --AND I.COD_PRODUCTO = 'RF_NAC'
                                AND I.COD_INSTRUMENTO IN ('DEPOSITO_NAC','PACTOS_NAC')
         WHERE M.ID_CUENTA = @PID_CUENTA
           AND M.FECHA_MOVIMIENTO = @PFECHA
           AND M.COD_ESTADO   <> 'A'
           AND M.FECHA_CIERRE IS NULL
       GROUP BY M.ID_MOV_ACTIVO_COMPRA, M.ID_MOV_ACTIVO, M.FLG_TIPO_MOVIMIENTO
                             , M.ID_NEMOTECNICO


       --LOS QUE TIENEN SALDO ANTERIOR
       INSERT INTO SALDOS_ACTIVOS
         ( FECHA_CIERRE
         , ID_NEMOTECNICO
         , ID_CUENTA
         , ID_MONEDA_NEMOTECNICO
         , MONTO_MON_NEMOTECNICO
         , ID_MONEDA_ORIGEN
         , ID_CIERRE
         , CANTIDAD
         , ID_MOV_ACTIVO
         , ID_MONEDA_CTA
  ----
         , MONTO_MON_CTA
         , MONTO_MON_ORIGEN
         , VALOR_PARIDAD
         , CANTIDAD_MANTENIDA
         , MONTO_MANTENIDO
         )  SELECT @PFECHA
                 , N.ID_NEMOTECNICO as ID_NEMOTECNICO
                 , @PID_CUENTA
                 , N.ID_MONEDA_TRANSACCION
                 , 0
                 , N.ID_MONEDA
                 , @LID_CIERRE
                 , ISNULL(SA.CANTIDAD,0) + ISNULL(MOV.CANTIDAD, 0) as CANTIDAD
                 , ISNULL(SA.ID_MOV_ACTIVO, MOV.ID_MOV_ACTIVO) AS ID_MOV_ACTIVO
                 , CTA.ID_MONEDA AS ID_MONEDA_CTA
                 ----------------------
                 , 0 --MONTO_MON_CTA
                 , 0 --MONTO_MON_ORIGEN
                 , 0 --VALOR_PARIDAD
                 , 0 --CANTIDAD_MANTENIDA
                 , 0 --MONTO_MANTENIDO
            FROM (SELECT SA.ID_MOV_ACTIVO
                       , SA.ID_NEMOTECNICO
                       , SA.CANTIDAD
                    FROM SALDOS_ACTIVOS SA WITH (NOLOCK)
                   INNER JOIN NEMOTECNICOS N WITH (NOLOCK) ON N.ID_NEMOTECNICO = SA.ID_NEMOTECNICO
                   INNER JOIN INSTRUMENTOS I WITH (NOLOCK) ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
                                            --AND I.COD_PRODUCTO = 'RF_NAC'
                                            AND I.COD_INSTRUMENTO IN ('DEPOSITO_NAC','PACTOS_NAC')
                   WHERE SA.ID_CUENTA = @PID_CUENTA
                     AND SA.FECHA_CIERRE = @LFCH_AYER
                  ) SA
       --     FULL OUTER JOIN (SELECT CASE M.FLG_TIPO_MOVIMIENTO
       --                             WHEN DBO.PKG_GLOBAL$GCTIPOOPERACION_EGRESO() THEN M.ID_MOV_ACTIVO_COMPRA ELSE M.ID_MOV_ACTIVO END AS ID_MOV_ACTIVO
       --                           , M.ID_NEMOTECNICO
       --                           , SUM(M.CANTIDAD * CASE M.FLG_TIPO_MOVIMIENTO
       --                                               WHEN DBO.PKG_GLOBAL$GCTIPOOPERACION_EGRESO() THEN -1
       --                                               ELSE 1
       --                                              END) AS CANTIDAD
       --                        FROM MOV_ACTIVOS M
       --                      INNER JOIN NEMOTECNICOS N ON N.ID_NEMOTECNICO = M.ID_NEMOTECNICO
       --                      INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
       --                                               AND I.COD_PRODUCTO = 'RF_NAC'
       --                      WHERE M.ID_CUENTA = @PID_CUENTA
       --                        AND M.FECHA_MOVIMIENTO = @PFECHA
       --                        AND M.COD_ESTADO   <> 'A'
       --                        AND M.FECHA_CIERRE IS NULL
       --                      GROUP BY M.ID_MOV_ACTIVO_COMPRA, M.ID_MOV_ACTIVO, M.FLG_TIPO_MOVIMIENTO
       --                             , M.ID_NEMOTECNICO
       --                     ) MOV ON SA.ID_NEMOTECNICO = MOV.ID_NEMOTECNICO
       --                          AND SA.ID_MOV_ACTIVO  = MOV.ID_MOV_ACTIVO
            FULL OUTER JOIN (SELECT ID_MOV_ACTIVO, ID_NEMOTECNICO, SUM(CANTIDAD) CANTIDAD
                               FROM @TMP_MOV
                           GROUP BY ID_MOV_ACTIVO, ID_NEMOTECNICO) MOV ON SA.ID_NEMOTECNICO = MOV.ID_NEMOTECNICO
                                                 AND SA.ID_MOV_ACTIVO  = MOV.ID_MOV_ACTIVO
            INNER JOIN NEMOTECNICOS N WITH (NOLOCK) ON N.ID_NEMOTECNICO = ISNULL(SA.ID_NEMOTECNICO, MOV.ID_NEMOTECNICO)
            INNER JOIN CUENTAS CTA WITH (NOLOCK) ON CTA.ID_CUENTA = @PID_CUENTA
            WHERE (ISNULL(SA.CANTIDAD,0) + ISNULL(MOV.CANTIDAD, 0)) > 0

         UPDATE MOV_ACTIVOS
            SET ID_SALDO_ACTIVO  = (SELECT ID_SALDO_ACTIVO
                                      FROM SALDOS_ACTIVOS WITH (NOLOCK)
                                     WHERE ID_CUENTA    = @PID_CUENTA
                                       AND FECHA_CIERRE = @PFECHA
                                       AND ID_MOV_ACTIVO = MOV_ACTIVOS.ID_MOV_ACTIVO)
              , FECHA_CIERRE     = @PFECHA
              , ID_CIERRE        = @LID_CIERRE
          WHERE ID_CUENTA        = @PID_CUENTA
            AND FECHA_MOVIMIENTO = @PFECHA
            AND ID_SALDO_ACTIVO  IS NULL
            AND FECHA_CIERRE     IS NULL
            AND COD_ESTADO       <> 'A'
            AND ID_NEMOTECNICO   IN (SELECT ID_NEMOTECNICO
                                       FROM NEMOTECNICOS N WITH (NOLOCK)
                                      INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
                                                               --AND I.COD_PRODUCTO = 'RF_NAC')
                                              AND I.COD_INSTRUMENTO IN ('DEPOSITO_NAC','PACTOS_NAC'))
   END
 ELSE
*/
  BEGIN
        INSERT INTO @TMP_MOV
        SELECT CASE M.FLG_TIPO_MOVIMIENTO
                  WHEN DBO.PKG_GLOBAL$GCTIPOOPERACION_EGRESO() THEN M.ID_MOV_ACTIVO_COMPRA
                  ELSE M.ID_MOV_ACTIVO
                  END AS ID_MOV_ACTIVO
             , M.ID_NEMOTECNICO
             , SUM(M.CANTIDAD * CASE M.FLG_TIPO_MOVIMIENTO
                                   WHEN DBO.PKG_GLOBAL$GCTIPOOPERACION_EGRESO() THEN -1
                                   ELSE 1
                                   END) AS CANTIDAD
          FROM MOV_ACTIVOS M WITH (NOLOCK)
         INNER JOIN NEMOTECNICOS N WITH (NOLOCK) ON N.ID_NEMOTECNICO = M.ID_NEMOTECNICO
         INNER JOIN INSTRUMENTOS I WITH (NOLOCK) ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
                                                AND I.COD_PRODUCTO = 'RF_NAC'
         WHERE M.ID_CUENTA = @PID_CUENTA
           AND M.FECHA_MOVIMIENTO = @PFECHA
           AND M.COD_ESTADO   <> 'A'
           AND M.FECHA_CIERRE IS NULL
         GROUP BY M.ID_MOV_ACTIVO_COMPRA, M.ID_MOV_ACTIVO, M.FLG_TIPO_MOVIMIENTO, M.ID_NEMOTECNICO

       --LOS QUE TIENEN SALDO ANTERIOR
        INSERT INTO SALDOS_ACTIVOS
             ( FECHA_CIERRE
             , ID_NEMOTECNICO
             , ID_CUENTA
             , ID_MONEDA_NEMOTECNICO
             , MONTO_MON_NEMOTECNICO
             , ID_MONEDA_ORIGEN
             , ID_CIERRE
             , CANTIDAD
             , ID_MOV_ACTIVO
             , ID_MONEDA_CTA
  ----
             , MONTO_MON_CTA
             , MONTO_MON_ORIGEN
             , VALOR_PARIDAD
             , CANTIDAD_MANTENIDA
             , MONTO_MANTENIDO
             ) SELECT @PFECHA
                    , N.ID_NEMOTECNICO as ID_NEMOTECNICO
                    , @PID_CUENTA
                    , N.ID_MONEDA_TRANSACCION
                    , 0
                    , N.ID_MONEDA
                    , @LID_CIERRE
                    , ISNULL(SA.CANTIDAD,0) + ISNULL(MOV.CANTIDAD, 0) as CANTIDAD
                    , ISNULL(SA.ID_MOV_ACTIVO, MOV.ID_MOV_ACTIVO) AS ID_MOV_ACTIVO
                    , CTA.ID_MONEDA AS ID_MONEDA_CTA
                 ----------------------
                    , 0 --MONTO_MON_CTA
                    , 0 --MONTO_MON_ORIGEN
                    , 0 --VALOR_PARIDAD
                    , 0 --CANTIDAD_MANTENIDA
                    , 0 --MONTO_MANTENIDO
                 FROM (SELECT SA.ID_MOV_ACTIVO
                            , SA.ID_NEMOTECNICO
                            , SA.CANTIDAD
                         FROM SALDOS_ACTIVOS SA WITH (NOLOCK)
                        INNER JOIN NEMOTECNICOS N WITH (NOLOCK) ON N.ID_NEMOTECNICO = SA.ID_NEMOTECNICO
                        INNER JOIN INSTRUMENTOS I WITH (NOLOCK) ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
                                                               AND I.COD_PRODUCTO = 'RF_NAC'
                        WHERE SA.ID_CUENTA = @PID_CUENTA
                          AND SA.FECHA_CIERRE = @LFCH_AYER
                      ) SA
       --      FULL OUTER JOIN (SELECT CASE M.FLG_TIPO_MOVIMIENTO
       --                             WHEN DBO.PKG_GLOBAL$GCTIPOOPERACION_EGRESO() THEN M.ID_MOV_ACTIVO_COMPRA ELSE M.ID_MOV_ACTIVO END AS ID_MOV_ACTIVO
       --                           , M.ID_NEMOTECNICO
       --                           , SUM(M.CANTIDAD * CASE M.FLG_TIPO_MOVIMIENTO
       --                                               WHEN DBO.PKG_GLOBAL$GCTIPOOPERACION_EGRESO() THEN -1
       --                                               ELSE 1
       --                                              END) AS CANTIDAD
       --                        FROM MOV_ACTIVOS M
       --                      INNER JOIN NEMOTECNICOS N ON N.ID_NEMOTECNICO = M.ID_NEMOTECNICO
       --                      INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
       --                                               AND I.COD_PRODUCTO = 'RF_NAC'
       --                      WHERE M.ID_CUENTA = @PID_CUENTA
       --                        AND M.FECHA_MOVIMIENTO = @PFECHA
       --                        AND M.COD_ESTADO   <> 'A'
       --                        AND M.FECHA_CIERRE IS NULL
       --                      GROUP BY M.ID_MOV_ACTIVO_COMPRA, M.ID_MOV_ACTIVO, M.FLG_TIPO_MOVIMIENTO
       --                             , M.ID_NEMOTECNICO
       --                     ) MOV ON SA.ID_NEMOTECNICO = MOV.ID_NEMOTECNICO
       --                          AND SA.ID_MOV_ACTIVO  = MOV.ID_MOV_ACTIVO
             FULL OUTER JOIN (SELECT ID_MOV_ACTIVO, ID_NEMOTECNICO, SUM(CANTIDAD) CANTIDAD
                                FROM @TMP_MOV
                               GROUP BY ID_MOV_ACTIVO, ID_NEMOTECNICO) MOV ON SA.ID_NEMOTECNICO = MOV.ID_NEMOTECNICO
                                                                          AND SA.ID_MOV_ACTIVO  = MOV.ID_MOV_ACTIVO
            INNER JOIN NEMOTECNICOS N WITH (NOLOCK) ON N.ID_NEMOTECNICO = ISNULL(SA.ID_NEMOTECNICO, MOV.ID_NEMOTECNICO)
            INNER JOIN CUENTAS CTA WITH (NOLOCK) ON CTA.ID_CUENTA = @PID_CUENTA
            WHERE (ISNULL(SA.CANTIDAD,0) + ISNULL(MOV.CANTIDAD, 0)) > 0

           UPDATE MOV_ACTIVOS
              SET ID_SALDO_ACTIVO  = (SELECT ID_SALDO_ACTIVO
                                        FROM SALDOS_ACTIVOS WITH (NOLOCK)
                                       WHERE ID_CUENTA    = @PID_CUENTA
                                         AND FECHA_CIERRE = @PFECHA
                                         AND ID_MOV_ACTIVO = MOV_ACTIVOS.ID_MOV_ACTIVO)
                , FECHA_CIERRE     = @PFECHA
                , ID_CIERRE        = @LID_CIERRE
            WHERE ID_CUENTA        = @PID_CUENTA
              AND FECHA_MOVIMIENTO = @PFECHA
              AND ID_SALDO_ACTIVO  IS NULL
              AND FECHA_CIERRE     IS NULL
              AND COD_ESTADO       <> 'A'
              AND ID_NEMOTECNICO   IN (SELECT ID_NEMOTECNICO
                                         FROM NEMOTECNICOS N  WITH (NOLOCK)
                                        INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
                                                                 AND I.COD_PRODUCTO = 'RF_NAC')

  END
  SET @PRESULTADO = NULL
END TRY
BEGIN CATCH
  SET @PRESULTADO =  '[' + ERROR_PROCEDURE() + ':' + CAST(ERROR_LINE() AS NVARCHAR) + ':' + CAST(ERROR_NUMBER() AS NVARCHAR) + ']' + CHAR(13) +
                     ' ERROR=' + ERROR_MESSAGE()

  INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)
  VALUES (@PID_USUARIO, @LPROCESO, @PRESULTADO)
END CATCH
SET NOCOUNT OFF

GO
GRANT EXECUTE ON [HAF_PROCESOCIERRE_CALCSALDOS_CARTERA] TO DB_EXECUTESP
GO
