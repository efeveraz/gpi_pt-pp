--USE [CisCB]
--GO
--IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[AD_QRY_CB_SALDO_ACCIONES]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
--DROP PROCEDURE [DBO].[AD_QRY_CB_SALDO_ACCIONES]
--GO 
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

----/******************************************************************    
----Procedimiento         : AD_QRY_CB_SALDO_ACCIONES  
----Objetivo Descripcion  : SP creado para Obtener Saldo de acciones de Clientes GPI  
----Sistema               :     
----Base de Datos         : CisCB    
----Tablas Usadas         :     
----Paramentros Entrada   : Fecha_Saldo (YYYYMMDD),Rut_Cliente  
----Paramentros Salida    :    
----Cadena Ejecucion      : 
----								USE [CISCB]
----								GO

----								DECLARE @return_value int

----								EXEC @return_value = [dbo].[AD_QRY_CB_SALDO_ACCIONES]
----								  @pFecha_saldo = N'20160924',
----								  @pRutCliente = N'76038702-9/2'

----								SELECT 'Return Value' = @return_value

----								GO
----Autor                 : 
----Modificación		  :	Fernando Tapia
----Fecha Creacion        :   
----Fecha Modificación    : 08/10/2018  
    
----****************************************************************/    



--CREATE PROCEDURE [dbo].[AD_QRY_CB_SALDO_ACCIONES]
--	@pFecha_saldo CHAR(8),
--	@pRutCliente VARCHAR(15) = NULL
--AS
--BEGIN
declare

@pFecha_saldo CHAR(8),
@pRutCliente VARCHAR(15)

select
@pFecha_saldo =  '20210420',
@pRutCliente = null --'78780080-7/1'

 set dateformat ymd
 set nocount on

 declare @vFecSal int, @msgerr char(80)
 
 set @msgerr = ''
 if isdate(@pFecha_saldo) = 0
  set @msgerr = 'Fecha Inválida (' + rtrim(@pFecha_saldo)

 if @msgerr=''
 begin
  if rtrim(@pFecha_saldo) = ''
   set @msgerr = 'Debe ingresar una Fecha (YYYYMMDD)'
  else
   set @vFecSal = (CAST(convert(datetime, @pFecha_saldo) AS INT)+693596)
 end

 create table #tmp_acciones(
							  t_Rut_cliente    char(15),
							  t_Nemotecnico    varchar(20),
							  t_Saldo          decimal(16,4),
							  t_Valor_mercado  Decimal(16),
							  t_precio         Decimal(16,4),
							  t_Nombre_cliente varchar(120) NULL,
							  t_simultaneas	   decimal(16,4),	
							  t_garantias	   decimal(16,4),	
							  t_prestamos	   decimal(16,4)	
							 )

 if @msgerr = ''
 begin
     insert into #tmp_acciones
     select 'RUT CLIENTE'=sc.Id_cliente,
            'NEMOTECNICO'=sc.Instrumento,
            'SALDO'=sc.Cantidad,
            'VALOR MERCADO'=convert(numeric(16), sc.Cantidad *
                            isnull((select top 1 pm.Valor_mercado
                                      from premer as pm
                                     where pm.Instrumento=sc.Instrumento and
                                           pm.Fecha_de_tasas <= @vFecSal
                                     order by pm.Fecha_de_tasas desc),1)),
            'PRECIO'=convert(numeric(18,4), isnull((select top 1 pm.Valor_mercado
                                                      from premer as pm
                                                     where pm.Instrumento=sc.Instrumento and
                                                           pm.Fecha_de_tasas <= @vFecSal
                                                     order by pm.Fecha_de_tasas desc),1)),
            'Nombre_cliente'=	NULL	,
            'simultaneas'	=	isnull(s.Cantidad_simultanea,0)  	,
            'garantias'		=	isnull(g.Cantidad_garantia	,0)	 	,
            'prestamos'		=	isnull(p.Cantidad_prestamo	,0)
       from ciscb.dbo.salcuscli as sc
				inner join dbo.VIEW_CLIENTE_GPI_SEC as v 
					ON (v.cuenta=sc.Id_cliente and v.origen_magic='MAGIC_VALORES')
				inner join dbo.persocb as ps 
					ON ps.Id_cliente = sc.Id_cliente and ps.Administracion_de_cartera = 'S'
				inner join Instru as i 
					On  i.Id_instrumento = sc.Instrumento and i.Tipo_instrumento = 'A'
				left join (	SELECT   Gar_idpersona
										,Gar_instrumento	
										,sum(Gar_cantidad)  AS Cantidad_garantia
								FROM	GARANTIA
								WHERE	Gar_fecha_ingreso<=@vFecSal 
										--AND rtrim(gar_idPersona)=@pRutCliente
										AND (Gar_fecha_levantamiento>@vFecSal OR Gar_fecha_levantamiento=0 AND Gar_estado='V')
										AND Gar_tipo<>'SIMULTANEADAS'
										AND Gar_tipo<>'PRESTAMO'
										AND Gar_tipo<>'VENTA CORTA RF'
										AND Gar_tipo<>'VENTA CORTA RV'
										AND Gar_instrumento NOT LIKE 'CFM%'
										AND Gar_folio>0
								GROUP BY Gar_idpersona,Gar_instrumento	  ) g
						 on sc.Id_cliente=g.Gar_idpersona
							and sc.Instrumento=g.gar_Instrumento
				left join (	SELECT   Gar_idpersona
										,Gar_instrumento	
										,sum(Gar_cantidad)  AS Cantidad_prestamo
								FROM	GARANTIA
								WHERE	Gar_fecha_ingreso<=@vFecSal 
										--AND rtrim(Gar_idpersona)=@pRutCliente
										AND Gar_fecha_vencimiento>=@vFecSal 
										AND Gar_fecha_levantamiento>=
										(CASE WHEN Gar_fecha_levantamiento>0 THEN @vFecSal ELSE Gar_fecha_levantamiento END)
										AND UPPER(Gar_estado)<>'A'
										AND Gar_tipo='PRESTAMO'
										AND Gar_instrumento NOT LIKE 'CFM%'
										AND Gar_folio>0
								GROUP BY Gar_instrumento,Gar_idpersona)  p 
									ON	sc.Id_cliente=p.Gar_idpersona
										and sc.Instrumento=p.gar_Instrumento 

				left join	(SELECT	tb_sim.id_cliente,tb_sim.instrumento AS Instrumento
									,SUM(ISNULL(tb_sim.nominales,0)) AS Cantidad_simultanea 
							 FROM   (SELECT bac.instrumento
											,bac.id_cliente
											,bac.nominales
											,bac.concepto
											,bac.id_concepto
											,bac.id_linea_concepto
									FROM dbo.BACKOFF  bac
											LEFT OUTER JOIN dbo.CIEBOL cie 
												ON	bac.Fecha_palo=cie.Fecha_del_cierre
													AND bac.Hora_palo=cie.Hora_del_cierre
													AND bac.codigo_bolsa=cie.Bolsa_del_cierre
													AND bac.id_cierre_bolsa=cie.Id_bolsa_del_cierre
													AND bac.tipo_liquidacion=cie.Tipo_de_liquidacion
											INNER JOIN dbo.VIEW_CLIENTE_GPI_SEC as v 
												ON (v.cuenta=bac.Id_cliente and v.origen_magic='MAGIC_VALORES')
									WHERE	bac.fecha_de_mvto<=@vFecSal
											--AND rtrim(bac.Id_cliente)=@pRutCliente
											AND bac.origen='OP'
											AND bac.concepto IN('IC','IV','CD','VD')
											AND bac.id_concepto>0
											AND bac.id_linea_concepto>0
											AND bac.fecha_liquidacion>@vFecSal
											AND bac.tipo_liquidacion='TP'
											AND bac.modulo_de_entrada='RV'
											AND bac.numero_orden_simultanea>0) tb_sim 
								
									GROUP BY tb_sim.instrumento,tb_sim.id_cliente) s
						ON sc.Id_cliente=s.id_cliente
							and sc.instrumento=s.instrumento
      where sc.Fecha_saldo = @vFecSal
        and sc.Id_cliente  = isnull(@pRutCliente, sc.Id_cliente)
 end

UPDATE #tmp_acciones
SET t_simultaneas = t_simultaneas - prepa.sim_prepago
FROM #tmp_acciones tmp_acc
inner join  (Select  bac.instrumento,bac.id_cliente,SUM(ISNULL(ppg.nominales,0)) sim_prepago
									FROM dbo.BACKOFF  bac
											LEFT OUTER JOIN dbo.CIEBOL cie 
												ON	bac.Fecha_palo=cie.Fecha_del_cierre
													AND bac.Hora_palo=cie.Hora_del_cierre
													AND bac.codigo_bolsa=cie.Bolsa_del_cierre
													AND bac.id_cierre_bolsa=cie.Id_bolsa_del_cierre
													AND bac.tipo_liquidacion=cie.Tipo_de_liquidacion
											INNER JOIN dbo.VIEW_CLIENTE_GPI_SEC as v 
												ON (v.cuenta=bac.Id_cliente and v.origen_magic='MAGIC_VALORES')
 											LEFT OUTER JOIN dbo.PREPAGO_SIMULTANEA  ppg 
										ON	ppg.concepto=bac.concepto
											AND ppg.id_concepto=bac.id_concepto
											AND ppg.id_linea_concepto=bac.Id_Linea_Concepto
											AND ppg.fecha_liquidacion<=@vFecSal
											WHERE	bac.fecha_de_mvto<=@vFecSal
											--AND rtrim(bac.Id_cliente)=@pRutCliente
											AND bac.origen='OP'
											AND bac.concepto IN('IC','IV','CD','VD')
											AND bac.id_concepto>0
											AND bac.id_linea_concepto>0
											AND bac.fecha_liquidacion>@vFecSal
											AND bac.tipo_liquidacion='TP'
											AND bac.modulo_de_entrada='RV'
											AND bac.numero_orden_simultanea>0 
											GROUP BY bac.instrumento,bac.id_cliente) prepa  
	on 	tmp_acc.t_Rut_cliente = prepa.id_cliente and tmp_acc.t_Nemotecnico = prepa.instrumento				

--Retorno de datos

 select 'RUT_CLIENTE'			= t_Rut_cliente,
        'NEMOTECNICO'			= t_Nemotecnico,
        'SALDO'					= t_Saldo,
        'VALOR_MERCADO'			= t_Valor_mercado,
        'PRECIO'				= t_precio,
        'NOMBRE_CLIENTE'		= DBO.fn_va_NombreCliente(t_Rut_cliente),
        'CANTIDAD_GARANTIAS'	= t_garantias,
        'CANTIDAD_PRESTAMOS'	= t_prestamos,
        'CANTIDAD_SIMULTANEAS'	= t_simultaneas
 from	#tmp_acciones
 order by t_Rut_cliente, t_Nemotecnico
 
 drop table #tmp_acciones

--End
--GO
--GRANT EXECUTE ON [AD_QRY_CB_SALDO_ACCIONES] TO DB_EXECUTESP
--GO 