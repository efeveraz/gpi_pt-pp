USE [CSGPI]
GO
IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_PROCESOS_IMPORTADOR$CisCB_SIMULTANEA]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
DROP PROCEDURE [DBO].[PKG_PROCESOS_IMPORTADOR$CisCB_SIMULTANEA]
GO
CREATE PROCEDURE  [dbo].[PKG_PROCESOS_IMPORTADOR$CisCB_SIMULTANEA] (
@pFecha_movimientos char(8) ,
@pRut_Cliente       varchar(15) = null
)
AS

BEGIN
/*
SET @pFecha_movimientos = '20180207'
SET @pRut_Cliente       = null-- '96791610-2'
*/

   SET DATEFORMAT YMD
   SET NOCOUNT ON

   DECLARE @lCantidad             decimal     ,    @lNum_Fila             int         ,    @linstrumento    CHAR(12)   ,
           @lNemotecnicoFin       char(50)    ,    @lid_Nemotecnico       Numeric(10) ,    @lfolio          INT        ,
           @lRut_Cliente          char(15)    ,    @lRegistros            int         ,    @lcuenta_gpi     varchar(10),
           @LCODRESULTADO         varchar(5)  ,    @lNemotecnico          char(50)    ,    @lErrorNoCuenta  varchar(1) ,
           @LMSGRESULTADO         varchar(800),    @DSC_BLOQUEO           varchar(120),    @lid_cuenta      int        ,
           @LCODINSTRUMENTO       char(15)    ,    @intCounter            int         ,    @ID_ORIGEN       numeric    ,
           @ID_TIPO_CONVERSION    numeric     ,    @lOutputLOG_SCTAS_flg  int         ,    @ID_ASESOR       int        ,
           @COD_ESTADO            varchar(3)  ,    @FLG_BLOQUEADO         varchar(1)  ,    @lValor_Cli      varchar(50),
           @lRutCuenta            varchar(20) ,    @lCuentaGPI            varchar(15) ,    @lNumeroOperacion numeric(10)

   SET @intCounter     = 0

--************************************************************************************************************************************************************************

-- CREACION DE TABLA DE RECUPERACION DE DATOS
CREATE TABLE #Tabla1 ( RutCliente           VARCHAR(15)  ,   RutCuenta           VARCHAR(15)  ,   NumeroCuenta       VARCHAR(3)   ,
                       TipoProducto         VARCHAR(3)   ,   DescTipoProducto    VARCHAR(15)  ,   CodProducto        VARCHAR(15)  ,
                       ClaseActivo          VARCHAR(3)   ,   ClaseProducto       VARCHAR(3)   ,   SubClaseProducto   VARCHAR(3)   ,
                       Instrumento          VARCHAR(20)  ,   Mercado             VARCHAR(3)   ,   Canal              INT          ,
                       Agencia              INT          ,   Agente              INT          ,   TipoMoneda         VARCHAR(3)   ,
                       Cantidad             DECIMAL(30,4),   FechaInicio          VARCHAR(10)  ,   FechaInicioInt      INT          ,
					   FechaVcto          VARCHAR(10)  ,     FechaVctoInt         INT          ,   Bolsa               VARCHAR(3)   ,
					   DescBolsa          VARCHAR(5)   , 	 PrecioPromedioCompra DECIMAL(30,4),   MontoInvertido      DECIMAL(30,4),
					   PrecioFinal        DECIMAL(30,4), 	 MontoFinal           DECIMAL(30,4),   PrecioMercadoActual DECIMAL(30,4),
					   MontoCierre        DECIMAL(30,4),	 NombreCliente        VARCHAR(100) ,   NombreAgente        VARCHAR(100) ,
					   CuentaGPI          VARCHAR(50)  ,	 Mandato              VARCHAR(1)   ,   NumeroOperacion     INT ,
					   SaldoActual         DECIMAL(30,4),   SaldoActualEnPesos DECIMAL(30,4))


-- CREACION DE TABLA DE PROCESO
CREATE TABLE #Tabla2  ( NUMERO_FILA  int, RutCliente           VARCHAR(15)  ,   RutCuenta           VARCHAR(15)  ,   NumeroCuenta       VARCHAR(3)   ,
                       TipoProducto         VARCHAR(3)   ,   DescTipoProducto    VARCHAR(15)  ,   CodProducto        VARCHAR(15)   ,
                       ClaseActivo        VARCHAR(3)   ,   ClaseProducto       VARCHAR(3)   ,   SubClaseProducto   VARCHAR(3)   ,
                       Instrumento          VARCHAR(20)  ,   Mercado             VARCHAR(3)   ,   Canal    INT          ,
                       Agencia              INT          ,   Agente              INT          ,   TipoMoneda         VARCHAR(3)   ,
                       Cantidad             DECIMAL(30,4),   SaldoActual         DECIMAL(30,4),   SaldoActualEnPesos DECIMAL(30,4),
                       FechaInicio          VARCHAR(10)  ,   FechaInicioInt      INT          ,   FechaVcto          VARCHAR(10)  ,
                       FechaVctoInt         INT          ,   Bolsa               VARCHAR(3)   ,   DescBolsa          VARCHAR(5)   ,
                       PrecioPromedioCompra DECIMAL(30,4),   MontoInvertido      DECIMAL(30,4),   PrecioFinal        DECIMAL(30,4),
                       MontoFinal           DECIMAL(30,4),   PrecioMercadoActual DECIMAL(30,4),   MontoCierre        DECIMAL(30,4),
                       NombreCliente        VARCHAR(100) ,   NombreAgente        VARCHAR(100) ,   CuentaGPI          VARCHAR(50)  ,
                       Mandato              VARCHAR(1)   ,   NumeroOperacion     NUMERIC(10)  ,   id_cuenta          NUMERIC(10)  ,
                       ID_Nemotecnico       NUMERIC(10)  ,   LCODRESULTADO       VARCHAR(5)   ,   LMSGRESULTADO       varchar(800) )
--************************************************************************************************************************************************************************
CREATE TABLE #Clientes (ValorCli varchar(50))
--************************************************************************************************************************************************************************

   SELECT @ID_ORIGEN = ID_ORIGEN FROM ORIGENES WITH (NOLOCK) WHERE COD_ORIGEN = 'MAGIC_VALORES'
   SELECT @ID_TIPO_CONVERSION = ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WITH (NOLOCK) WHERE TABLA = 'CUENTAS'

   IF @pRut_Cliente IS NULL
      INSERT #Clientes
      SELECT DISTINCT(VALOR)
      FROM rel_conversiones
       WHERE ID_ORIGEN  = @ID_ORIGEN
         AND ID_TIPO_CONVERSION  = @ID_TIPO_CONVERSION
   ELSE
      INSERT #Clientes
      SELECT DISTINCT(VALOR)
      FROM rel_conversiones
      WHERE ID_ORIGEN  = @ID_ORIGEN
       AND ID_TIPO_CONVERSION  = @ID_TIPO_CONVERSION
       AND VALOR like  @pRut_Cliente + '%'


       Declare @id int,
               @count int
       Set @id=1
       select @count=count(1)from #Clientes
       while @id<=@count
             begin
                  select @lValor_Cli =  ValorCli
                  from (select  ValorCli,RANK()OVER (ORDER BY ValorCli ASC)AS RANK from #Clientes) as ji where rank=@id
                  INSERT #Tabla1
                  EXEC [CisCB].[dbo].[AD_QRY_CB_SALDO_SIMULTANEAS] @pRutCliente = @lValor_Cli,
                                                                   @pFechaConsulta =  @pFecha_movimientos ,
                                                                   @pCodErr = 0 ,
                                                                   @pMsgErr  =''
                  select @id=@id+1
             end

   INSERT INTO #Tabla2
   SELECT 1, RutCliente           ,  RutCuenta          ,   NumeroCuenta   ,  TipoProducto   ,
             DescTipoProducto     ,  CodProducto        ,   ClaseActivo    ,  ClaseProducto  ,
             SubClaseProducto     ,  Instrumento        ,   Mercado        ,  Canal          ,
             Agencia              ,  Agente             ,   TipoMoneda     ,  Cantidad       ,
             SaldoActual          ,  SaldoActualEnPesos ,   FechaInicio    ,  FechaInicioInt ,
             FechaVcto            ,  FechaVctoInt       ,   Bolsa          ,  DescBolsa      ,
             PrecioPromedioCompra ,  MontoInvertido     ,   PrecioFinal    ,  MontoFinal     ,
             PrecioMercadoActual  ,  MontoCierre        ,   NombreCliente  ,  NombreAgente   ,
             CuentaGPI            ,  Mandato            ,   NumeroOperacion,  NULL           ,
             NULL                 ,  NULL               ,   NULL
   FROM #Tabla1  WITH (NOLOCK)


   --INCREMENTA EL NUMERO DE FILA EN TABLA DE PROCESO
   UPDATE #Tabla2
   SET @intCounter = NUMERO_FILA = @intCounter + 1;

   --****************************************************************************************************************************
   DECLARE CUR_OPERACION CURSOR FOR
   SELECT NUMERO_FILA,
          instrumento,
          RutCliente ,
          RutCuenta  ,
          CuentaGPI  ,
          NumeroOperacion
   FROM #Tabla2  WITH (NOLOCK)
   order by NUMERO_FILA

--   set @lRegistros = 0

   OPEN CUR_OPERACION
   FETCH CUR_OPERACION INTO @lNum_Fila,
                            @linstrumento,
                            @lRut_Cliente,
                            @lRutCuenta,
                            @lCuentaGPI,
                            @lNumeroOperacion
   WHILE (@@FETCH_STATUS = 0)
   BEGIN

         SET @LCODRESULTADO  = 'OK'
         SET @LMSGRESULTADO  = ''
         SET @lid_Nemotecnico = NULL
         SET @lNemotecnico = rtrim(ltrim(@lNemotecnico))

         -- IDENTIFICADOR DE CUENTA **************************
         SELECT @lid_cuenta = id_entidad
         FROM REL_CONVERSIONES WITH (NOLOCK)
         WHERE ID_ORIGEN          = @ID_ORIGEN
           AND ID_TIPO_CONVERSION = @ID_TIPO_CONVERSION
           AND VALOR = rtrim(ltrim(@lRutCuenta))

         If @@RowCount = 0
            BEGIN
                SET @LCODRESULTADO = 'ERROR'
                SET @LMSGRESULTADO = 'ERROR: Rut Cliente: ' + rtrim(ltrim(@lRut_Cliente))  + ' |Nemotécnico: ' + rtrim(ltrim(@linstrumento)) + '- Movimiento no posee cuenta.'
            END
         ELSE
            BEGIN
                 SELECT @COD_ESTADO    = COD_ESTADO,
                        @FLG_BLOQUEADO = FLG_BLOQUEADO,
                        @DSC_BLOQUEO   = ISNULL(OBS_BLOQUEO,'')
                 FROM CUENTAS WITH (NOLOCK) WHERE ID_CUENTA = @lid_cuenta

                 IF @COD_ESTADO = 'H'
                    BEGIN
                        IF @FLG_BLOQUEADO = 'S'
                           BEGIN
                                SET @LCODRESULTADO = 'ERROR'
                                SET @LMSGRESULTADO = 'ERROR: Cuenta: ' + @lCuentaGPI  + '- Cuenta Bloqueada:' + @DSC_BLOQUEO
                           END
                    END
                 ELSE
                        BEGIN
         SET @LCODRESULTADO = 'ERROR'
                               SET @LMSGRESULTADO = 'ERROR: Cuenta: ' + @lCuentaGPI  + '|Rut Cliente: ' + rtrim(ltrim(@lRut_Cliente)) + '-Cuenta Deshabilitada'
                           END
            END

         /**** VALIDACION DE NEMO *****/

         IF ((SELECT CASE WHEN EXISTS(SELECT 1 FROM dbo.VIEW_NEMOTECNICOS WITH (NOLOCK) WHERE NEMOTECNICO = rtrim(ltrim(@linstrumento))) THEN 1 ELSE 0 END) = 1)
             BEGIN
                  SELECT @lid_Nemotecnico = ID_NEMOTECNICO,
                         @lCodInstrumento = COD_INSTRUMENTO
                  FROM dbo.VIEW_NEMOTECNICOS WITH (NOLOCK)
                  WHERE NEMOTECNICO = rtrim(ltrim(@linstrumento))
             END
         ELSE
             BEGIN
                 SET @LCODRESULTADO = 'ERROR'
                 SET @LMSGRESULTADO = 'ERROR: Problemas al buscar Nemotécnico ' + rtrim(ltrim(@lNemotecnico))
            END

         /***********************************************************************************************************/
 /***********************************************************************************************************/

            UPDATE #Tabla2 SET ID_CUENTA       = @lid_cuenta,
                                  CodProducto     = @lCodInstrumento,
                                  ID_Nemotecnico  = @lid_Nemotecnico,
                                  LCODRESULTADO   = @LCODRESULTADO,
                                  LMSGRESULTADO   = @LMSGRESULTADO
               WHERE Instrumento = @linstrumento
               AND RutCliente = @lRut_Cliente
               AND NUMERO_FILA  = @lNum_Fila
               AND NumeroOperacion = @lNumeroOperacion


        --Lectura de la siguiente fila de un cursor
         FETCH CUR_OPERACION INTO @lNum_Fila,
                                  @linstrumento,
                                  @lRut_Cliente,
                                  @lRutCuenta,
                                  @lCuentaGPI,
                                  @lNumeroOperacion
   END

   -- Cierra el cursor
   CLOSE CUR_OPERACION
   DEALLOCATE CUR_OPERACION

   -- Tabla Resultante


   SELECT  convert(varchar(10),T.NumeroOperacion) + '/' + convert(varchar(10),T.Id_Nemotecnico) ,T.RutCliente     ,   T.RutCuenta          ,   T.NumeroCuenta       ,
           T.TipoProducto   , T.DescTipoProducto     ,   T.CodProducto        ,   T.ClaseActivo      ,
           T.ClaseProducto  , T.SubClaseProducto     ,   T.Instrumento        ,   T.Mercado          ,
           T.Canal          , T.Agencia              ,   T.Agente             ,   T.TipoMoneda       ,
           T.Cantidad       , T.SaldoActual          ,   T.SaldoActualEnPesos ,   T.FechaInicio      ,
           T.FechaInicioInt , T.FechaVcto            ,   T.FechaVctoInt       ,   T.Bolsa            ,
           T.DescBolsa      , T.PrecioPromedioCompra ,   T.MontoInvertido     ,   T.PrecioFinal      ,
           T.MontoFinal     , T.PrecioMercadoActual  ,   T.MontoCierre        ,   T.NombreAgente     ,
           T.CuentaGPI      , T.Mandato              ,   T.NumeroOperacion    ,   T.id_cuenta        ,
           T.Id_Nemotecnico , T.LCODRESULTADO        ,   T.LMSGRESULTADO      ,   E.DSC_EMPRESA      ,
           CASE WHEN CL.TIPO_ENTIDAD = 'J' THEN CL.RAZON_SOCIAL
                WHEN CL.TIPO_ENTIDAD = 'N' THEN CL.NOMBRES + ' ' + CL.PATERNO + ' ' + CL.MATERNO end as NombreCliente
   FROM  #Tabla2 T  WITH (NOLOCK)
   INNER JOIN CUENTAS C   ON C.id_cuenta  = T.id_cuenta
   INNER JOIN EMPRESAS E  ON E.id_empresa  = C.id_empresa
   INNER JOIN CLIENTES CL ON CL.id_cliente = C.id_cliente
   --Where NOT EXISTS (SELECT 1
   --                  FROM REL_CONVERSIONES
   --                  WHERE ID_ORIGEN = (SELECT ID_ORIGEN FROM ORIGENES WITH (NOLOCK) WHERE COD_ORIGEN = 'AD_QRY_CB_SALDO_SIMULTANEAS')
   --                  AND ID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WITH (NOLOCK) WHERE TABLA = 'SALDOS_SIMULTANEAS')
   --                  AND VALOR = convert(varchar(10),NumeroOperacion) + '/' + convert(varchar(10),Id_Nemotecnico))
   order by NUMERO_FILA , ID_CUENTA

   DROP TABLE #Tabla1
   DROP TABLE #Tabla2
   SET NOCOUNT OFF
END
GO
GRANT EXECUTE ON [PKG_PROCESOS_IMPORTADOR$CisCB_SIMULTANEA] TO DB_EXECUTESP
GO