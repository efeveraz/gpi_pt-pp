﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb.master" AutoEventWireup="false"
    CodeBehind="ConsultaClientes.aspx.vb" Inherits="AplicacionWeb.ConsultaClientes" %>

<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPalFiltro" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaClientes.min.js") %>'></script>
    <link href='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.css") %>' rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <select id="DDAsesor" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Asesor" style="min-width: 145px;">
                    </select>
                </div>
                <div class="form-group">
                    <select id="DDTipo" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tipos">
                        <option value="-1">Todos los Tipos</option>
                        <option value="N">Natural</option>
                        <option value="J">Juridico</option>
                    </select>
                </div>
                <div class="form-group">
                    <select id="DDEstado" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Estados">
                        <option value="-1">Todos los Estados</option>
                        <option value="A">Anulado</option>
                        <option value="V">Vigente</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </div>
    </div>
    <div class='row top-buffer'>
        <div class='col-xs-12'>
            <div class="panel panel-default" style="margin-bottom: 0px;">
                <div class="panel-body">
                    <div class='table-responsive'>
                        <table id='tablaClientes' class='table table-striped table-hover table-bordered table-condensed display nowrap' width="100%">
                            <thead style="display: table-header-group">
                                <tr>
                                    <th>Cliente</th>
                                    <th>Rut</th>
                                    <th>Estado</th>
                                    <th>N° de Cuentas</th>
                                    <th>Asesor</th>
                                    <th>Sexo</th>
                                    <th>Fecha de Nacimiento</th>
                                    <th>E-mail</th>
                                    <th>País</th>
                                    <th>Tipo de Persona</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
