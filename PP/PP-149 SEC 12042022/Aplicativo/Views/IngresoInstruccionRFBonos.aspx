﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master"
    CodeBehind="IngresoInstruccionRFBonos.aspx.vb" Inherits="AplicacionWeb.IngresoInstruccionRFBonos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContenidoPPal" runat="server">
    <script type="text/javascript">
        function resize() {
            var height;
            var ancho = $(window).width();
            height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - 129;
            if (ancho < 768) {
                $("#panelbodyOperacion").height(175);
            } else {
                $("#panelbodyOperacion").height(height - $("#GVOperaciones").outerHeight(true) - 28);
            }
            $("#spanelfiltro").height(height);
            $("#spanelComisiones").height(height);
        }

        function confirmacion() {
            if (document.getElementById('<%=HFConfirmacion.ClientID %>').value == "") {
                document.getElementById('<%=HFResConfirmacion.ClientID %>').value = true;
                return;
            }
            var resultado = confirm(document.getElementById('<%=HFConfirmacion.ClientID %>').value);
            document.getElementById('<%=HFResConfirmacion.ClientID %>').value = resultado
        }

        function AplicarDatePickers() {
            $("#<%=DTFechaLiquidacion.ClientID %>").datepicker();
            $('#<%=DTFechaLiquidacion.ClientID %>').datepicker().on('changeDate', function (e) {
                $('#<%=HFFechaLiquidacion.ClientID %>').val(e.format('dd-mm-yyyy'));
            });
            $("#<%=DTFechaOperacion.ClientID %>").datepicker();
            $('#<%=DTFechaOperacion.ClientID %>').datepicker().on('changeDate', function (e) {
                $('#<%=HFFechaOperacion.ClientID %>').val(e.format('dd-mm-yyyy'));
            });
        }
        function pageLoad(sender, args) {
            $(window).trigger('resize');
            $(document).ready(function () {
                AplicarDatePickers();
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(AplicarDatePickers);
                $('#loader').modal({ backdrop: false });
                $(window).trigger('resize');
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(window).bind('resize', function () {
                resize()
            }).trigger('resize');

            $("#idSubtituloPaginaText").text("Ingreso de Instrucciones Bonos");
            document.title = "Ingreso de Instrucciones Bonos";

            if (miInformacionCuenta != null) {
                document.getElementById('<%=HFIdCuenta.ClientID %>').value = miInformacionCuenta.IdCuenta;
                document.getElementById('<%=HFIdCliente.ClientID %>').value = miInformacionCuenta.IdCliente;
                __doPostBack("HFIdCliente", "TextChanged");
            }

            $("#tabsClienteCuentaGrupo").hide();
            $('.nav-tabs a[href="#tabCuenta"]').tab('show');

            $('#porCuenta_NroCuenta').on('change', function () {
                if (miInformacionCuenta == null) {
                    document.getElementById('<%=HFIdCuenta.ClientID %>').value = "";
                    document.getElementById('<%=HFIdCliente.ClientID %>').value = "";
                    __doPostBack("HFIdCliente", "TextChanged");
                    return;
                }
                document.getElementById('<%=HFIdCuenta.ClientID %>').value = miInformacionCuenta.IdCuenta;
                document.getElementById('<%=HFIdCliente.ClientID %>').value = miInformacionCuenta.IdCliente;
                __doPostBack("HFIdCliente", "TextChanged");
            });

        });
    </script>
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
             <asp:UpdateProgress runat="server">
                <ProgressTemplate   >
                    <div class="modal fade" id="loader" role="dialog"  runat="server" >
                        <div class="modal-dialog" role="document"   >
                            <div class="modal-content">
                                <div class="modal-header">
                                    Un momento...
                                </div>
                                <div class="modal-body">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:HiddenField ID="HFIdCuenta" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HFIdCliente" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HFResConfirmacion" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HFConfirmacion" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HFMontoOperacion" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HFCorteMinimo" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HFMontoNominal" runat="server" ClientIDMode="Static" />
            <div class="row top-buffer">
                <div class="col-sm-3 col-lg-2">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headpanelfiltro">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#panelfiltro" aria-expanded="true" aria-controls="panelfiltro">Datos de Operación
                                    </a>
                                </h4>
                            </div>
                            <div id="panelfiltro" class="panel-collapse collapse in">
                                <div id="spanelfiltro" class="panel-body" style="height: 300px; overflow-y: auto;">
                                    <div class="form-group-sm">
                                        <label for="DDContraparte" class="control-label">Contraparte</label>
                                        <asp:DropDownList ID="DDContraparte" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="-1">Seleccionar Contraparte</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group-sm">
                                        <label for="DDTrader" class="control-label">Trader</label>
                                        <asp:DropDownList ID="DDTrader" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="-1">Seleccionar Trader</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group-sm">
                                        <label for="DDRepresentantes" class="control-label">Representantes</label>
                                        <asp:DropDownList ID="DDRepresentantes" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="-1">Seleccionar Representante</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group-sm">
                                        <label for="DDFechaLiquidación" class="control-label">Fecha Liquidación T+</label>
                                        <div class="input-group-sm" style="display: table;">
                                            <span class="input-group-btn">
                                                <asp:DropDownList ID="DDFechaLiquidación" runat="server" AutoPostBack="true" CssClass="dropdown-toggle btn btn-default" data-toggle="dropdown">
                                                    <asp:ListItem Value="0">0</asp:ListItem>
                                                    <asp:ListItem Value="1">1</asp:ListItem>
                                                    <asp:ListItem Value="2">2</asp:ListItem>
                                                    <asp:ListItem Value="3">3</asp:ListItem>
                                                    <asp:ListItem Value="4">4</asp:ListItem>
                                                    <asp:ListItem Value="5">5</asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <asp:TextBox ID="DTFechaLiquidacion" runat="server" ClientIDMode="Static" AutoPostBack="true"
                                                OnTextChanged="DTFechaLiquidacion_onchange" CssClass="form-control pull-right" Style="text-align: center;"></asp:TextBox>
                                        </div>
                                        <asp:HiddenField ID="HFFechaLiquidacion" runat="server" ClientIDMode="Static" />
                                    </div>
                                    <div class="form-group-sm">
                                        <label for="DTFechaOperacion" class="control-label">Fecha Operación</label>
                                        <asp:TextBox ID="DTFechaOperacion" runat="server" ClientIDMode="Static" AutoPostBack="true"
                                            CssClass="form-control" Style="text-align: center;"></asp:TextBox>
                                        <asp:HiddenField ID="HFFechaOperacion" runat="server" ClientIDMode="Static" />
                                    </div>
                                    <div class="form-group-sm">
                                        <label for="TBMontoTotal" class="control-label">Total Aprox.</label>
                                        <asp:TextBox ID="TBMontoTotal" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)" ReadOnly="True">0</asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headpanelComisiones">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#panelComisiones" aria-expanded="true" aria-controls="panelComisiones">Comisión
                                    </a>
                                </h4>
                            </div>
                            <div id="panelComisiones" class="panel-collapse collapse">
                                <div id="spanelComisiones" class="panel-body" style="height: 300px; overflow-y: auto;">
                                    <div class="form-group-sm">
                                        <label class="control-label">Comisión (%)</label>
                                        <asp:TextBox ID="TBPorComision" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                    <div class="form-group-sm">
                                        <label class="control-label">Derechos (%)</label>
                                        <asp:TextBox ID="TBPorDerechos" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                    <div class="form-group-sm">
                                        <label class="control-label">Comisión a Cobrar</label>
                                        <asp:TextBox ID="TBMonComision" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                    <div class="form-group-sm">
                                        <label class="control-label">Derechos Bolsa</label>
                                        <asp:TextBox ID="TBDerechos" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                    <div class="form-group-sm">
                                        <label class="control-label">Gastos</label>
                                        <asp:TextBox ID="TBGastos" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                    <div class="form-group-sm">
                                        <label id="LBIva" runat="server" css="control-label">IVA (19%)</label>
                                        <asp:TextBox ID="TBIva" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 col-lg-10" id="panelDerecho">
                    <asp:GridView ID="GVOperaciones" runat="server" Width="100%" AutoGenerateColumns="False" GridLines="None" ShowHeaderWhenEmpty="True" ClientIDMode="Static"
                        CssClass="table table-hover table-striped">
                        <EmptyDataTemplate>
                            Sin Datos
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                            <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                            <asp:BoundField DataField="idmoneda" HeaderText="idmoneda" Visible="false" />
                            <asp:BoundField DataField="id_nemotecnico" HeaderText="id_nemotecnico" Visible="false" />
                            <asp:BoundField DataField="monto_transferencia" HeaderText="monto_transferencia"
                                Visible="false" />
                            <asp:BoundField DataField="tasa_transferencia" HeaderText="tasa_transferencia" Visible="false" />
                            <asp:BoundField DataField="dsc_nemotecnico" HeaderText="Nemotécnico" />
                            <asp:BoundField DataField="dsc_emisor" HeaderText="Emisor" />
                            <asp:BoundField DataField="tasa" DataFormatString="{0:c}" HeaderText="Tasa Operación" />
                            <asp:BoundField DataField="tasa_historica" HeaderText="Tasa Histórica" Visible="false" />
                            <asp:BoundField DataField="cantidad" HeaderText="Nominales" />
                            <asp:BoundField DataField="monto" DataFormatString="{0:c}" HeaderText="Monto" />
                            <asp:BoundField DataField="utilidad" DataFormatString="{0:c}" HeaderText="Utilidad" />
                            <asp:BoundField DataField="dsc_moneda" HeaderText="Moneda" />
                            <asp:BoundField DataField="flg_vende_todo" HeaderText="flg_vende_todo" Visible="false" />
                            <asp:BoundField DataField="id_mov_activo" HeaderText="id_mov_activo" Visible="false" />
                            <asp:CommandField ShowDeleteButton="True" />
                        </Columns>
                    </asp:GridView>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="form-group nav">
                                <label for="DDCompraVenta" class="col-xs-6 col-sm-4 col-md-3 control-label" style="padding-top: 7px;">Tipo Operación</label>
                                <div class="col-xs-6 col-sm-4 col-md-3">
                                    <asp:DropDownList ID="DDCompraVenta" AutoPostBack="true" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="I">Compra</asp:ListItem>
                                        <asp:ListItem Value="E">Venta</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:Button ID="BTNuevo" runat="server" Text="Nuevo Detalle" CssClass="btn btn-default"/>
                            </div>
                        </div>
                        <div id="panelbodyOperacion" runat="server" clientidmode="Static" class="panel-body" style="overflow-y: auto;">
                            <div class="row">
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="DDNemotecnico" class="control-label control-label-sm">Nemotécnico</label>
                                    <asp:DropDownList ID="DDNemotecnico" AutoPostBack="true" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="DTFechaVencimiento" class="control-label control-label-sm">Vencimiento</label>
                                    <asp:TextBox ID="DTFechaVencimiento" runat="server" ClientIDMode="Static" AutoPostBack="true" CssClass="form-control" ReadOnly="True" Style="text-align: center;"></asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBEmisor" class="control-label control-label-sm">Emisor</label>
                                    <asp:TextBox ID="TBEmisor" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="DTFechaEmision" class="control-label control-label-sm">Emisión</label>
                                    <asp:TextBox ID="DTFechaEmision" runat="server" ClientIDMode="Static" AutoPostBack="true" CssClass="form-control" ReadOnly="True" Style="text-align: center;"></asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBMoneda" class="control-label control-label-sm">Moneda</label>
                                    <asp:TextBox ID="TBMoneda" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                </div>
                                <div id="FGNominalesEnCartera" runat="server" class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBNominalesEnCartera" class="control-label control-label-sm">En Cartera</label>
                                    <asp:TextBox ID="TBNominalesEnCartera" runat="server" AutoPostBack="true" CssClass="form-control" onkeypress="return isNumberKey(event)" ReadOnly="True"></asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <asp:CheckBox ID="CBVendeTodo" runat="server" Text="Vende Todo" AutoPostBack="true" />
                                </div>
                            </div>
                            <hr style="margin: 5px;">
                            <div class="row">
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBNominalesTrans" class="control-label">Nominales</label>
                                    <asp:TextBox ID="TBNominalesTrans" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBPrecioInver" class="control-label">Inversión</label>
                                    <asp:TextBox ID="TBPrecioInver" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBMontoOperacion" class="control-label">Monto Operación</label>
                                    <asp:TextBox ID="TBMontoOperacion" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBPrecioCierre" class="control-label">Transferencia</label>
                                    <asp:TextBox ID="TBPrecioCierre" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBUtilidad" class="control-label">Utilidad</label>
                                    <asp:TextBox ID="TBUtilidad" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                </div>
                            </div>
                            <asp:Button ID="BTValorizar" runat="server" Text="Valorizar" CssClass="btn btn-default" OnClick="BTValorizar_Click" />
                            <asp:Button ID="BTAgregar" runat="server" Text="Agregar" CssClass="btn btn-default" OnClientClick="confirmacion()" OnClick="BTAgregar_Click" />
                            <asp:Button ID="BTActualizar" runat="server" Text="Actualizar" CssClass="btn btn-default" OnClientClick="confirmacion()" />
                            <asp:Button ID="BTCancelar" runat="server" Text="Cancelar" CssClass="btn btn-danger" />
                        </div>
                        <div class="panel-footer">
                            <asp:Button ID="BTGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" OnClientClick="confirmacion()" />
                        </div>
                    </div>
                </div>
            </div>

               
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="TBMontoOperacion" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="TBPrecioInver" EventName="TextChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
