﻿var lista = "#ListaPorCuenta";
var target = "#tabCuenta";
var graficoRent = "graficoPorCuenta";

var jsfechaConsulta;
var options;
var ChartMercado;

var graficoPorCuenta;
var graficoPorCliente;
var graficoPorGrupo;

function resize() {
    var ancho = $(window).width();
    var height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - 37 - 84 - 22;

    if (ancho < 768) {
        $('#ListaPorCuenta').setGridHeight(200);
        $('#ListaPorCliente').setGridHeight(200);
        $('#ListaPorGrupo').setGridHeight(200);
    } else {
        $('#ListaPorCuenta').jqGrid('setGridHeight', height);
        $('#ListaPorCliente').jqGrid('setGridHeight', height);
        $('#ListaPorGrupo').jqGrid('setGridHeight', height);
    }

    $("#ListaPorCuenta").setGridWidth($("#grillaPorCuenta").width());
    $("#ListaPorCliente").setGridWidth($("#grillaPorCliente").width());
    $("#ListaPorGrupo").setGridWidth($("#grillaPorGrupo").width());

    $("#graficoPorCuenta").setGridWidth($("#grillaPorCuenta").width());
    $("#graficoPorCliente").setGridWidth($("#grillaPorCliente").width());
    $("#graficoPorGrupo").setGridWidth($("#grillaPorGrupo").width());

    if ($('#graficoPorCuenta').highcharts() != undefined) {
        $('#graficoPorCuenta').highcharts().setSize($("#grillaPorCuenta").width(), height + 120, doAnimation = false);
    }
    if ($('#graficoPorCliente').highcharts() != undefined) {
        $('#graficoPorCliente').highcharts().setSize($("#grillaPorCliente").width(), height + 120, doAnimation = false);
    }
    if ($('#graficoPorGrupo').highcharts() != undefined) {
        $('#graficoPorGrupo').highcharts().setSize($("#grillaPorGrupo").width(), height + 120, doAnimation = false);
    }
}

function initConsultaRentabilidadHistorica() {

    $("#idSubtituloPaginaText").text("Rentabilidad Histórica");
    document.title = "Rentabilidad Historica";

    if (!miInformacionCuenta) {
        $("#dtFechaConsultaHasta").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
        $("#dtFechaConsulta").datepicker('setDate', moment(new Date(localStorage.UltimaFechaCierre)).add(-1, 'months').toDate());
    } else {
        $("#dtFechaConsultaHasta").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
        $("#dtFechaConsulta").datepicker('setDate', moment($("#dtFechaConsultaHasta").datepicker('getDate')).add(-1, 'months').format('L'));
    }

    Highcharts.setOptions({
        lang: {
            months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            weekdays: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            rangeSelectorFrom: 'Desde',
            rangeSelectorTo: 'Hasta'
        },
        credits: {
            text: 'creasys.cl',
            href: 'http://www.creasys.cl'
        },
        rangeSelector: {
            buttons: [{
                type: 'month',
                count: 1,
                text: '1m'
            }, {
                type: 'month',
                count: 3,
                text: '3m'
            }, {
                type: 'month',
                count: 6,
                text: '6m'
            }, {
                type: 'year',
                count: 1,
                text: '1año'
            }, {
                type: 'ytd',
                text: 'YTD'
            }, {
                type: 'all',
                text: 'Todo'
            }]
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: [{
                        text: 'Descargar como PNG',
                        onclick: function () {
                            this.exportChart();
                        }
                    }, {
                        text: 'Descargar como JPEG',
                        onclick: function () {
                            this.exportChart({
                                type: 'image/jpeg'
                            });
                        }
                    }, {
                        text: 'Descargar como PDF',
                        onclick: function () {
                            this.exportChart({
                                type: 'application/pdf'
                            });
                        }
                    }, {
                        text: 'Descargar como SVG',
                        onclick: function () {
                            this.exportChart({
                                type: 'image/svg+xml'
                            });
                        }
                    }]
                }
            }
        }
    });

    var graficoOpcCuenta = { chart: { renderTo: 'graficoPorCuenta' }, rangeSelector: { selected: 1 }, title: { text: 'Rentabilidad Cuenta' }, series: [{ data: [], tooltip: { valueDecimals: 2 } }] };
    var graficoOpcCliente = { chart: { renderTo: 'graficoPorCliente', width: $('#graficoPorCliente').outerWidth(true), height: $('#graficoPorCliente').outerHeight(true) }, rangeSelector: { selected: 1 }, title: { text: 'Rentabilidad Cliente' }, series: [{ data: [], tooltip: { valueDecimals: 2 } }] };
    var graficoOpcGrupo = { chart: { renderTo: 'graficoPorGrupo', width: $('#grillaPorGrupo').outerWidth(true), height: $('#grillaPorGrupo').outerHeight(true) }, rangeSelector: { selected: 1 }, title: { text: 'Rentabilidad Grupo' }, series: [{ data: [], tooltip: { valueDecimals: 2 } }] };

    graficoPorCuenta = new Highcharts.StockChart(graficoOpcCuenta);
    graficoPorCliente = new Highcharts.StockChart(graficoOpcCliente);
    graficoPorGrupo = new Highcharts.StockChart(graficoOpcGrupo);

    $(window).trigger('resize');

    $("#contenidoPorCuenta").show();
    $("#contenidoPorCliente").hide();
    $("#contenidoPorGrupo").hide();

    estadoBtnConsultar();
}
function eventHandlersConsultaRentabilidadHistorica() {

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $(".input-group.date").datepicker();
    $("#dtRangoFecha").datepicker();

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        target = $(e.target).attr("href") // activated tab
        if (target == "#tabCuenta") {
            $("#contenidoPorCuenta").show();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaPorCuenta";
            graficoRent = "graficoPorCuenta";
            $('#BtnConsultar').attr('data-original-title', 'Seleccione Cuenta');
        }
        if (target == "#tabCliente") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").show();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaPorCliente";
            graficoRent = "graficoPorCliente";
            $('#BtnConsultar').attr('data-original-title', 'Seleccione Cliente');
        }
        if (target == "#tabGrupo") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").show();
            lista = "#ListaPorGrupo";
            graficoRent = "graficoPorGrupo";
            $('#BtnConsultar').attr('data-original-title', 'Seleccione Grupo');
        }
        estadoBtnConsultar();
        $(window).trigger('resize');
    });

    var grilla = {
        shrinkToFit: true,
        loadonce: true,
        datatype: 'local',
        scrollOffset: 1,
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "orden" },
        colNames: ['orden', 'Rentabilidades', '$', 'UF', 'US$'],
        colModel: [
            { name: "orden", index: "orden", sortable: true, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "descripcion", index: "descripcion", width: 150, sortable: true, sorttype: "Text", editable: false, search: true, hidden: false, summaryType: 'count', summaryTpl: '<b>Total</b>' },
            { name: "rentabilidadPeso", index: "rentabilidadPeso", sortable: true, width: 50, sorttype: "currency", formatter: "currency", align: "right", editable: false, search: true, hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "rentabilidadUF", index: "rentabilidadUF", sortable: true, width: 50, sorttype: "currency", formatter: "currency", align: "right", editable: false, search: true, hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "rentabilidadDolar", index: "rentabilidadDolar", sortable: true, width: 50, sorttype: "currency", formatter: "currency", align: "right", editable: false, search: true, hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
        ],
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        caption: "Rentabilidad",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false
    }

    $("#grillaTodasLasCuenta").jqGrid({
        shrinkToFit: false,
        shrinkToFit: true,
        loadonce: true,
        datatype: 'local',
        scrollOffset: 1,
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "orden" },
        colNames: ['Cuenta', 'Orden', 'Rentabilidades', '% $', '% UF', '% US$'],
        colModel: [
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: false, width: 100, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "ORDEN", index: "ORDEN", sortable: true, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "DESCRIPCION", index: "DESCRIPCION", width: 150, sortable: true, sorttype: "Text", editable: false, search: true, hidden: false, summaryType: 'count', summaryTpl: '<b>Total</b>' },
            { name: "RENTABILIDAD_PESO", index: "RENTABILIDAD_PESO", sortable: true, width: 150, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "RENTABILIDAD_UF", index: "RENTABILIDAD_UF", sortable: true, width: 150, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "RENTABILIDAD_DOLAR", index: "RENTABILIDAD_DOLAR", sortable: true, width: 150, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        ],
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        caption: "Rentabilidad",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false
    });

    $("#ListaPorCuenta").jqGrid(grilla);
    $("#ListaPorCuenta").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorCuenta").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });

    $("#ListaPorCliente").jqGrid(grilla);
    $("#ListaPorCliente").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorCliente").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });

    $("#ListaPorGrupo").jqGrid(grilla);
    $("#ListaPorGrupo").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorGrupo").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });

    $("#BtnConsultar").button().click(function () {
        consultaRentabilidadHistorica("N");
    });
    $("#BtnTodasLasCuentas").button().click(function () {
        consultaRentabilidadHistoricaTodasLasCuentas("N");
    });
    
    $("#porCuenta_NroCuenta").on('change', function () {
        estadoBtnConsultar();
    });
}

function consultaRentabilidadHistorica(strCargaInicial) {
    var jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
    var jsfechaFormatoFinal = moment.utc($("#dtFechaConsultaHasta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaInicio = jsfechaFormato.substring(0, jsfechaFormato.length - 1);
    jsfechaFinal = jsfechaFormatoFinal.substring(0, jsfechaFormatoFinal.length - 1);

    var jsFechaCreacion;
    var jsIdCuenta = "";
    var jsIdCliente = "";
    var jsIdGrupo = "";

    if (target == "#tabCuenta") {
        jsIdCuenta = miInformacionCuenta == null ? "" : miInformacionCuenta.IdCuenta;
        jsFechaCreacion = miInformacionCuenta == null ? "" : moment.utc(miInformacionCuenta.FechaCreacion).startOf('day').toISOString();
        if (miInformacionCuenta == null && lista == "#ListaPorCuenta") {
            return;
        }
    }
    if (target == "#tabCliente") {
        jsIdCliente = miInformacionCliente == null ? "" : miInformacionCliente.IdCliente;
        jsFechaCreacion = miInformacionCliente == null ? "" : moment.utc(miInformacionCliente.FechaCreacion).startOf('day').toISOString();
        if (miInformacionCliente == null && lista == "#ListaPorCliente") {
            return;
        }
    }
    if (target == "#tabGrupo") {
        jsIdGrupo = miInformacionGrupo == null ? "" : miInformacionGrupo.IdGrupo;
        jsFechaCreacion = miInformacionGrupo == null ? "" : moment.utc(miInformacionGrupo.FechaOperativa).startOf('day').toISOString();
        if (miInformacionGrupo == null && lista == "#ListaPorGrupo") {
            return;
        }
        if (miInformacionGrupo.CantidadCuentas == 0) {
            if (strCargaInicial == "N") {
                alert("El grupo no contiene cuentas");
            }
            return;
        }
    }
    if (jsfechaInicio === "") {
        if (strCargaInicial == "N") {
            alert("No ha indicado fecha inicial del Periodo");
        }
        return;
    }
    if (jsfechaFinal === "") {
        if (strCargaInicial == "N") {
            alert("No ha indicado fecha final del Periodo");
        }
        return;
    }
    var listaAsync = lista;
    $(listaAsync).jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });

    if (target == "#tabCuenta") {
        $.ajax({
            url: "ConsultaRentabilidadHistorica.aspx/ConsultaRentabilidadHistoricaCuenta",
            data: "{'idCuenta':'" + jsIdCuenta +
            "', 'dfechaInicio':'" + jsfechaInicio +
            "', 'dfechaFinal':'" + jsfechaFinal + "'}",
            datatype: "json",
            type: "post",
            contentType: "application/json; charset=utf-8",
            complete: function (jsondata, stat) {
                if (stat == "success") {
                    var mydata = JSON.parse(jsondata.responseText).d;
                    $(listaAsync).setGridParam({ data: mydata });
                    $(listaAsync).trigger("reloadGrid");
                    if (!JSON.parse(jsondata.responseText).d) {
                        alertaColor(1, "No se encontraron registros.");
                    }
                }
                else {
                    alertaColor(3, JSON.parse(jsondata.responseText).Message);
                }
                $('#loaderProceso').modal('hide');
            }
        });
    } else {
        $.ajax({
            url: "ConsultaRentabilidadHistorica.aspx/ConsultaRentabilidadHistorica",
            data: "{'idCuenta':'" + jsIdCuenta +
            "', 'idCliente':'" + jsIdCliente +
            "', 'idGrupo':'" + jsIdGrupo +
            "', 'dfechaCreacion':'" + jsFechaCreacion +
            "', 'dfechaInicio':'" + jsfechaInicio +
            "', 'dfechaFinal':'" + jsfechaFinal + "'}",
            datatype: "json",
            type: "post",
            contentType: "application/json; charset=utf-8",
            complete: function (jsondata, stat) {
                if (stat == "success") {
                    var mydata = JSON.parse(jsondata.responseText).d;
                    $(listaAsync).setGridParam({ data: mydata });
                    $(listaAsync).trigger("reloadGrid");
                    if (!JSON.parse(jsondata.responseText).d) {
                        alertaColor(1, "No se encontraron registros.");
                    }
                }
                else {
                    alertaColor(3, JSON.parse(jsondata.responseText).Message);
                }
                $('#loaderProceso').modal('hide');
            }
        });
    }

    $.ajax({
        url: "ConsultaRentabilidadHistorica.aspx/ConsultaPeriodoRentabilidadHistorica",
        data: "{'dfechaCreacion':'" + jsFechaCreacion +
        "', 'dfechaFinal':'" + jsfechaFinal +
        "', 'idCuenta':'" + jsIdCuenta +
        "', 'idCliente':'" + jsIdCliente +
        "', 'idGrupo':'" + jsIdGrupo + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var dataMercadoJson = JSON.parse(jsondata.responseText);
                var datatestt = [];
                if (dataMercadoJson.d.rows !== null) {
                    for (var i = 0; i < dataMercadoJson.d.length; i++) {
                        datatestt[i] = [Number(dataMercadoJson.d[i].FECHA_CIERRE.replace("/Date(", "").replace(")/", "")), dataMercadoJson.d[i].VALOR_CUOTA];
                    }
                }

                if (target == "#tabCuenta") {
                    graficoPorCuenta.series[0].setData(datatestt);
                    graficoPorCuenta.title.text = "Rentabilidad Cuenta " + miInformacionCuenta.NroCuenta;
                } if (target == "#tabCliente") {
                    graficoPorCliente.series[0].setData(datatestt);
                    graficoPorCliente.title.text = "Rentabilidad Cliente " + miInformacionCliente.NombreCliente;
                } if (target == "#tabGrupo") {
                    graficoPorGrupo.series[0].setData(datatestt);
                    graficoPorGrupo.title.text = "Rentabilidad Grupo " + miInformacionGrupo.NombreGrupo;
                }

                $(window).trigger('resize');
            }
            else {
                alert(JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function consultaRentabilidadHistoricaTodasLasCuentas(strCargaInicial) {
    var jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
    var jsfechaFormatoFinal = moment.utc($("#dtFechaConsultaHasta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaInicio = jsfechaFormato.substring(0, jsfechaFormato.length - 1);
    jsfechaFinal = jsfechaFormatoFinal.substring(0, jsfechaFormatoFinal.length - 1);

    var jsIdCuenta = "";

    if (jsfechaInicio === "") {
        if (strCargaInicial == "N") {
            alert("No ha indicado fecha inicial del Periodo");
        }
        return;
    }
    if (jsfechaFinal === "") {
        if (strCargaInicial == "N") {
            alert("No ha indicado fecha final del Periodo");
        }
        return;
    }
    $("#grillaTodasLasCuenta").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });

        $.ajax({
            url: "ConsultaRentabilidadHistorica.aspx/ConsultaRentabilidadHistoricaTodasLasCuentas",
            data: "{'idCuenta':'" + jsIdCuenta +
                "', 'dfechaInicio':'" + jsfechaInicio +
                "', 'dfechaFinal':'" + jsfechaFinal + "'}",
            datatype: "json",
            type: "post",
            contentType: "application/json; charset=utf-8",
            complete: function (jsondata, stat) {
                if (stat == "success") {
                    var mydata = JSON.parse(jsondata.responseText).d;
                    $("#grillaTodasLasCuenta").setGridParam({ data: mydata });
                    $("#grillaTodasLasCuenta").trigger("reloadGrid");
                    JqGridAExcelASP.jqGridAExcel('grillaTodasLasCuenta', 'Rentabilidad Todas Las Cuentas', 'Rentabilidad', 'DATACOMPLETA');
                    console.log(mydata);
                    if (!JSON.parse(jsondata.responseText).d) {
                        alertaColor(1, "No se encontraron registros.");
                    }
                }
                else {
                    alertaColor(3, JSON.parse(jsondata.responseText).Message);
                }
                $('#loaderProceso').modal('hide');
            }
        });
}
function estadoBtnConsultar() {
    if (target == "#tabCuenta") {
        if (miInformacionCuenta == null) {
            $('#BtnConsultar').tooltip('enable').addClass('disabled');
        } else {
            $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
    if (target == "#tabCliente") {
        if (miInformacionCliente == null) {
            $('#BtnConsultar').tooltip('enable').addClass('disabled');
        } else {
            $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
    if (target == "#tabGrupo") {
        if (miInformacionGrupo == null) {
            $('#BtnConsultar').tooltip('enable').addClass('disabled');
        } else {
            $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
}

$(document).ready(function () {
    initConsultaRentabilidadHistorica();
    eventHandlersConsultaRentabilidadHistorica();

    consultaRentabilidadHistorica("S");
    $(window).trigger('resize');
});