﻿var alto;

var arbol = []
var permisosVista = [];
var checkPadres = false;
var uncheckPadres = false;
var usuarios = [];
var accion = 'guardar';

var GetDataRoles;
var rows_selectRoles = [];


function resize() {
    alto = $(window).height();

    if (alto > 500) {
        $("#panelArbol").height(alto - 213);
    }
}

function initMantenedorRoles() {
    $("#idSubtituloPaginaText").text("Mantenedor Rol");
    document.title = "Mantenedor Rol";
    consultaRol();
}

function cargarEventHandlersMantenedorRoles() {

    $('#ListaRoles tbody').on('click', 'input[type="radio"]', function (e) {
        var data = GetDataRoles.row($(this).parents('tr')).data();
        rows_selectRoles = [];
        if (this.checked) {
            rows_selectRoles.push(data.ID_ROL);
        }
        estadosBotones();
        e.stopPropagation();
    });
    $('#ListaRoles').on('click', 'tbody td div[class="radio"]', function (e) {
        $(this).parent().find('input[type="radio"]').prop('checked', true);

        var data = GetDataRoles.row($('#ListaRoles tbody input[type="radio"]:checked').parents('tr')).data();
        rows_selectRoles = [];
        if (data) {
            rows_selectRoles.push(data.ID_ROL);
        }
        estadosBotones();
    });

    $("#btnEditarRol").on('click', function () {
        var data = GetDataRoles.row($('#ListaRoles tbody input[type="radio"]:checked').parents('tr')).data();
        $('#lblErrNomRol').addClass("hidden");
        $("#tituloEditar").removeClass('hidden');
        $("#tituloAgregar").addClass('hidden');
        accion = 'editar';
        consultaPermisosRol(data.ID_ROL, data.DSC_ROL);
        $("#panelRol").removeClass('hidden');
        $("#panelListaRoles").addClass('hidden');
    });

    $("#btnCancelar").on('click', function () {
        $("#panelRol").addClass('hidden');
        $("#panelListaRoles").removeClass('hidden');
        $("#txtNombreRol").val('');
    });

    $("#btnNuevoRol").on('click', function () {
        accion = 'nuevo';
        consultaArbolVistas();
        $('#lblErrNomRol').addClass("hidden");
        $('#txtNombreRol').val('');
        $("#tituloAgregar").removeClass('hidden');
        $("#tituloEditar").addClass('hidden');
        $("#panelRol").removeClass('hidden');
        $("#panelListaRoles").addClass('hidden');
    });

    $("#btnGuardarNuevoRol").on('click', function () {
        $('#lblErrNomRol').addClass("hidden");
        var nombreRol = $("#txtNombreRol").val();
        if ($.trim(nombreRol) == '') {
            $('#lblErrNomRol').removeClass("hidden").text("Debe ingresar el nombre del Rol");
        } else {
            if (accion == 'nuevo') {
                guardarRol(nombreRol);
            } else {
                EditarRol(nombreRol);
            }
        }
    });

    $("#btnEliminar").on('click', function () {
        usuarios = usuariosRol();
    });

}

function consultaRol() {

    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    GetDataRoles = $('#ListaRoles').DataTable({
        data: {},
        columns: [
            {
                "searchable": false,
                "orderable": false,
                "defaultContent": "<div class='radio'><input type='radio' name='radioRol'><label></label></div>",
                width: '1%',
                className: 'dt-body-center',
            },
            { "data": "DSC_ROL" },
        ],
        destroy: true,
        ajax: {
            url: '../Servicios/Rol.asmx/ConsultaRoles',
            data: function (d) {
                return JSON.stringify({});
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd'
        },
        scrollX: true,
        scrollCollapse: false,
        fixedColumns: false,
        paging: false,
        info: false,
        searching: false,
        rowCallback: function () {
            $(window).trigger('resize');
        }
    });
    $('#loaderProceso').modal('hide');
    rows_selectRoles = [];
}

function estadosBotones() {
    if (rows_selectRoles.length == 1) {
        mostrarElemento('#btnEditarRol');
        mostrarElemento('#btnEliminar');
    } else {
        ocultarElemento('#btnEditarRol');
        ocultarElemento('#btnEliminar');
    }
}
function ocultarElemento(elemento) {
    $(elemento).removeClass('in');
    $(elemento).addClass('hidden');
}
function mostrarElemento(elemento) {
    $(elemento).addClass('in');
    $(elemento).removeClass('hidden');
}

function consultaPermisosRol(idRol, dscRol) {
    $("#txtNombreRol").val(dscRol);

    $('.loading').show();
    $.ajax({
        url: "MantenedorRoles.aspx/buscarVistasRol",
        data: "{'idRol':'" + idRol + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                permisosVista = JSON.parse(jsondata.responseText).d;
                generarArbolCompleto(permisosVista);
                llenarArbol();
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('.loading').hide();
        }
    });
}
function consultaArbolVistas() {
    $('.loading').show();
    $.ajax({
        url: "MantenedorRoles.aspx/buscarArbolVistas",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                permisosVista = JSON.parse(jsondata.responseText).d;
                generarArbolCompleto(permisosVista);
                llenarArbol();
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('.loading').hide();
        }
    });
}
function generarArbolCompleto(pVistas) {
    var padres = []
    $.each(pVistas, function (key, val) {
        var vista = val;
        if (val.ID_PADRE == null) {
            var nodo = {
                text: val.DSC_ARBOL_SISTEMA,
                href: '#' + val.ID_ARBOL_SISTEMA + val.DSC_ARBOL_SISTEMA,
                id: val.ID_ARBOL_SISTEMA,
                idPadre: val.ID_PADRE,
                textPadre: '',
                tags: ['0'],
                nodes: [],
                padre: true,
                padreRaizHijos: true,
                selectable: false,
                hijosCheck: false,
                state: {
                    checked: false,
                    disabled: false,
                    expanded: true,
                    selected: false
                },
            }
            if (val.ID_PADRE == null && val.PERMISO == '1') {
                nodo.state.checked = true;
            }
            padres.push(nodo);
        }
    })
    arbol = generarArbol(padres, pVistas);
}
function llenarArbol() {

    $('#treeview-checkable').treeview({
        data: arbol,
        //showIcon: false,
        showCheckbox: true,
        onNodeChecked: function (event, node) {
            var children = node['nodes'];
            if (children != null && checkPadres == false) {
                $.each(children, function (key, val) {
                    $('#treeview-checkable').treeview('checkNode', [val.nodeId, { silent: false }]);
                });
            }
            checksPadresF(node);
            $('#checkable-output').prepend('<p>' + node.text + ' was checked 1</p>');
        },
        onNodeUnchecked: function (event, node) {
            var children = node['nodes'];
            if (children != null && uncheckPadres == false) {
                $.each(children, function (key, val) {
                    $('#treeview-checkable').treeview('uncheckNode', [val.nodeId, { silent: false }]);
                });
            }
            uncheckPadresF(node);
            $('#checkable-output').prepend('<p>' + node.text + ' was unchecked</p>');
        }
    });
}
function checksPadresF(nodo) {
    if (nodo.idPadre != null) {
        checkPadres = true;
        var padre = $('#treeview-checkable').treeview('getParent', nodo);
        $('#treeview-checkable').treeview('checkNode', [padre, { silent: true }]);
        checkPadres = false;
    }
}
function uncheckPadresF(nodo) {
    if (nodo.idPadre != null) {
        var hayHijosCheck = false;
        uncheckPadres = true;
        var padre = $('#treeview-checkable').treeview('getParent', nodo);
        var hijos = padre['nodes'];
        for (var i = 0; i < hijos.length; i++) {
            var nodeHijo = hijos[i];
            if (nodeHijo.state.checked == true) {
                hayHijosCheck = true;
            }
        }
        if (hayHijosCheck == false) {
            $('#treeview-checkable').treeview('uncheckNode', [padre, { silent: false }]);
            uncheckPadresF
        }
        uncheckPadres = false;
    }
}
function generarArbol(padres, pVistas) {
    var arbolCompleto = [];
    $.each(padres, function (key, val) {
        var hoja = generarHojas(val, pVistas);

        if (hoja.nodes.length == 0) {
            hoja.padre = false;
        }
        arbolCompleto.push(hoja);
    });
    return arbolCompleto;
}
function generarHojas(hoja, pVistas) {
    var hojaConHijos = hoja;
    var nodo = {};

    $.each(pVistas, function (key2, val2) {
        if (key2 != null && hojaConHijos.id == val2.ID_PADRE) {
            if (val2.PERMISO == '0') {
                nodo = {
                    text: val2.DSC_ARBOL_SISTEMA,
                    href: '#' + val2.ID_ARBOL_SISTEMA + val2.DSC_ARBOL_SISTEMA,
                    id: val2.ID_ARBOL_SISTEMA,
                    idPadre: val2.ID_PADRE,
                    textPadre: hojaConHijos.text,
                    tags: ['0'],
                    nodes: [],
                    selectable: false,
                    hijosCheck: false,
                    padreRaizHijos: false,
                    padre: true,
                    state: {
                        checked: false,
                        disabled: false,
                        expanded: true,
                        selected: false
                    },
                }
            } else {
                nodo = {
                    text: val2.DSC_ARBOL_SISTEMA,
                    href: '#' + val2.ID_ARBOL_SISTEMA + val2.DSC_ARBOL_SISTEMA,
                    id: val2.ID_ARBOL_SISTEMA,
                    idPadre: val2.ID_PADRE,
                    textPadre: hojaConHijos.text,
                    tags: ['0'],
                    selectable: false,
                    hijosCheck: false,
                    padre: false,
                    padreRaizHijos: false,
                    //nodes: [],
                    state: {
                        checked: true,
                        disabled: false,
                        expanded: true,
                        selected: false
                    }
                }
                hojaConHijos.hijosCheck = true;
                hojaConHijos.state.checked = true;
            }

            var hijos = generarHojas(nodo, pVistas);
            if (hijos.nodes != null) {
                if (hijos.nodes.length == 0) {
                    delete hijos.nodes;
                }
            }

            hojaConHijos.nodes.push(hijos);
            if (hijos != null) {
                if (hijos.hijosCheck == true) {
                    hojaConHijos.state.checked = true;
                }
            }
        }
    });
    return hojaConHijos;
}

function guardarRol(nombreNuevoRol) {
    $.ajax({
        url: "../Servicios/Rol.asmx/guardarNombreRol",
        data: "{'nomRol':'" + nombreNuevoRol + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var respuesta = JSON.parse(jsondata.responseText).d;
                if (respuesta[0].ID_ROL > 0) {
                    var idRol = respuesta[0].ID_ROL;
                    var listaVistasPermiso = guardarArbolVista();
                    guardarRolesArbolSistema(listaVistasPermiso, idRol);
                } else {
                    $('#lblErrNomRol').removeClass("hidden").text("El nombre de Rol ya existe");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}
function EditarRol(nombreRol) {
    $.ajax({
        url: "../Servicios/Rol.asmx/actualizaRolNombre",
        data: "{'idRol':'" + rows_selectRoles[0] + "', 'nombreRol':'" + nombreRol + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var listaVistasPermiso = guardarArbolVista();
                guardarRolesArbolSistema(listaVistasPermiso, rows_selectRoles[0]);
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
                $('.loading').hide();
            }
        }
    });
}
function guardarArbolVista() {
    var vistasRol = [];
    var opt;
    opcionesMarcadas = $('#treeview-checkable').treeview('getChecked');

    $.each(opcionesMarcadas, function (key, val) {
        vistasRol.push(val.id);
    });

    return vistasRol;
}
function guardarRolesArbolSistema(listaPermisos, idRol) {
    var listaConDatos = false;
    var cadenaIdArbolSistema = "";
    var permiso = 'C';
    var listaConDatos = listaPermisos.length > 1 ? true : false;

    $(listaPermisos).each(function (val, IdArbolSistema) {
        if (cadenaIdArbolSistema == "") {
            cadenaIdArbolSistema = IdArbolSistema
        } else {
            cadenaIdArbolSistema = cadenaIdArbolSistema + "," + IdArbolSistema
        }
    });

    $.ajax({
        url: "../Servicios/Rol.asmx/guardarRolesArbolSistema",
        data: "{'idRol':'" + idRol +
        "', 'cadenaIdArbolSistema':'" + cadenaIdArbolSistema +
        "', 'tipoPermiso':'" + permiso + "'}",
        type: "post",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                $("#panelRol").addClass('hidden');
                $("#panelListaRoles").removeClass('hidden');
                alertaColor(0, "Rol guardado correctamente.");
                actualizarMenu();
            } else {
                $('.loading').hide();
            }
        }
    });

    consultaRol();
}
function actualizarMenu() {
    $.ajax({
        type: "post",
        url: "../Servicios/Menu.asmx/consultarMenu",
        data: "{}",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                localStorage.Menu = jsondata.responseText;
                cargaMenuRol();
            } 
        }
    });
}
function cargaMenuRol() {
    $('#navbar').empty();
    $('#navbar').append(JSON.parse(localStorage.Menu).d);
    $("li.active").parent().siblings().parent("li").addClass("active").parent().siblings().parent("li").addClass("active");

    $('#navbar .menuPadre').on('click', function () {
        $('#navbar > li > ul').not(this.hash).collapse('hide');
        $('#navbar > li > ul > li > ul').not(this.hash).collapse('hide');
    });
    $('#navbar > li > ul').on('click', 'a', function () {
        $('#navbar > li > ul > li > ul').not(this.hash).collapse('hide');
    });
    $('li', '#navbar')
        .filter(function () {
            return !!$(this).find('a[href="..' + location.pathname + '"]').length;
        }).addClass("active");
}
function usuariosRol() {
    var data = GetDataRoles.row($('#ListaRoles tbody input[type="radio"]:checked').parents('tr')).data();

    $('.loading').show();
    $.ajax({
        url: "MantenedorRoles.aspx/usuariosDeRol",
        data: "{'idRol':'" + data.ID_ROL + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var u = JSON.parse(jsondata.responseText).d;

                if (!u.length) {
                    bootbox.dialog({
                        message: "<p>¿Desea eliminar Rol:  <strong>'" + data.DSC_ROL + "'</strong> , el cual no tiene usuarios asociados? <p>",
                        title: "Eliminar",
                        buttons: {
                            main: {
                                label: "Cancelar",
                                className: "btn-primary",
                                callback: function () {
                                    console.log("Cancelar");
                                    $('.loading').hide();
                                }
                            },
                            danger: {
                                label: "Eliminar",
                                className: "btn-danger",
                                callback: function () {
                                    eliminarRol(data.ID_ROL);
                                    console.log("Eliminar");
                                }
                            }
                        }
                    });
                } else {
                    bootbox.dialog({
                        message: "<div class='alert alert-danger' ><p> ¿Desea eliminar Rol  <strong>'" + data.DSC_ROL + "'</strong>, el cual tiene asociados " + u.length + " usuarios con este Rol?</p><p> Estos usuarios perderan sus roles.</p> </div>",
                        title: "Alerta",
                        buttons: {
                            main: {
                                label: "Cancelar",
                                className: "btn-primary",
                                callback: function () {
                                    console.log("Cancelar");
                                    $('.loading').hide();
                                }
                            },
                            danger: {
                                label: "Eliminar",
                                className: "btn-danger",
                                callback: function () {
                                    eliminarRol(data.ID_ROL);
                                    console.log("Eliminar");
                                }
                            }
                        }
                    });
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('.loading').hide();
        }
    });
}
function eliminarRol(idRol) {
    $.ajax({
        type: "post",
        url: "../Servicios/Rol.asmx/eliminarRol",
        data: "{'idRol':'" + idRol + "'}",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                consultaRol();
                alertaColor(0, "Rol eliminado correctamente.");
            } else {
                alertaColor(3, "Ocurrio un error al eliminar.");
            }
        }
    });
    $('.loading').hide();
}

$(document).ready(function () {

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    initMantenedorRoles();
    cargarEventHandlersMantenedorRoles();

    resize();
});