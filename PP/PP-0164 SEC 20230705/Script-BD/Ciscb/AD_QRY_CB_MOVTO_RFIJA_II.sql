IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[AD_QRY_CB_MOVTO_RFIJA_II]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE   [DBO].[AD_QRY_CB_MOVTO_RFIJA_II]
GO

CREATE PROCEDURE [DBO].[AD_QRY_CB_MOVTO_RFIJA_II]
@PFECHA_MOVIMIENTOS CHAR(8)  -- YYYYMMDD
AS
BEGIN

	SET DATEFORMAT YMD
	SET NOCOUNT ON

	DECLARE @VFECHA_MOVIMIENTOS INT, @MSGERR CHAR(80)

	SET @MSGERR = ''
	IF ISDATE(@PFECHA_MOVIMIENTOS) = 0
		SET @MSGERR = 'FECHA INV�LIDA (' + RTRIM(@PFECHA_MOVIMIENTOS)
	IF @MSGERR=''
	BEGIN
		IF RTRIM(@PFECHA_MOVIMIENTOS) = ''
			SET @MSGERR = 'DEBE INGRESAR UNA FECHA (YYYYMMDD)'
		ELSE
			SET @VFECHA_MOVIMIENTOS = (CAST(CONVERT(DATETIME, @PFECHA_MOVIMIENTOS) AS INT)+693596)
	END

	DECLARE @VTIPO_PERSONA CHAR(2),
	        @VRAZON_SOCIAL VARCHAR(120),
            @VAPELLIDO_PATERNO VARCHAR(40),
            @VAPELLIDO_MATERNO VARCHAR(40),
            @VNOMBRES VARCHAR(40)

	CREATE TABLE #TMP_RFIJA
	(
		T_RUT_CLIENTE       CHAR(15),
		T_TIPO_OPERACION    CHAR(1),
		T_NUMERO_ORDEN      INT,
		T_CORRELATIVO       INT,
		T_NEMOTECNICO       VARCHAR(20),
		T_CANTIDAD          FLOAT,
		T_PRECIO            FLOAT,
		T_MONTO_NETO        FLOAT,
		T_COMISION          FLOAT,
		T_DERECHO           FLOAT,
		T_GASTOS            FLOAT,
		T_IVA               FLOAT,
		T_MONTO_OPERACION   FLOAT,
		T_FECHA_LIQUIDACION DATETIME,
		T_NOMBRE_CLIENTE    VARCHAR(120),
		T_NUMERO_FACTURA    INT,
		T_TIPO_LIQUIDACION  VARCHAR(2),
		T_BOLSA             VARCHAR(1),
        T_CUSTODIA_NOMINAL  VARCHAR(1),
        T_MONEDA            VARCHAR(3),
        T_CONTRAPARTE       CHAR(15)
	)

	IF @MSGERR = ''
	BEGIN
		DECLARE @VRUT_CLIENTE       CHAR(15),
			    @VTIPO_OPERACION    CHAR(1),
                @VNUMERO_ORDEN      INT,
                @VCORRELATIVO       INT,
                @VNEMOTECNICO       VARCHAR(20),
                @VCANTIDAD          FLOAT,
                @VPRECIO            FLOAT,
                @VMONTO_NETO        FLOAT,
                @VCOMISION          FLOAT,
                @VDERECHO           FLOAT,
                @VGASTOS            FLOAT,
                @VIVA               FLOAT,
                @VMONTO_OPERACION   FLOAT,
                @VFECHA_LIQUIDACION DATETIME,
                @INUMEROFACTURA     INT,
                @VTIPO_LIQUIDACION  VARCHAR(2),
                @VBOLSA             VARCHAR(1),
                @VCUSTODIA_NOMINAL  VARCHAR(1),
                @VMONEDA            VARCHAR(3),
                @VCONTRAPARTE       CHAR(15)
--
		DECLARE REC_MOV CURSOR FOR
		SELECT
			   'RUT CLIENTE'      = BK.ID_CLIENTE,
               'TIPO OPERACION'   = CASE BK.CONCEPTO
                                       WHEN 'IV' THEN 'V'
                                       WHEN 'IC' THEN 'C'
                                       WHEN 'IG' THEN 'I'
                                       WHEN 'RG' THEN 'R'
                                       WHEN 'SO' THEN 'S'
                                       WHEN 'VI' THEN 'C'
                                       WHEN 'CI' THEN 'V'
                                       ELSE '?'
                                       END,
			   'NUMERO ORDEN'       = CASE BK.CONCEPTO
                                         WHEN 'VI' THEN BK.ID_CONCEPTO
                                         WHEN 'CI' THEN BK.ID_CONCEPTO
                                         ELSE ISNULL(RO.NUMERO_ORDEN,0)
                                         END,
               'CORRELATIVO'        = ISNULL(RO.LINEA_ORDEN,0),
               'NEMOTECNICO'        = CASE BK.CONCEPTO
                                         WHEN 'VI' THEN 'PACTO-C ' + CONVERT(VARCHAR,CONVERT(DATETIME,PC.FECHA_VCTO - 693596),111)
                                         WHEN 'CI' THEN 'PACTO-V ' + CONVERT(VARCHAR,CONVERT(DATETIME,PC.FECHA_VCTO - 693596),111)
                                         ELSE BK.INSTRUMENTO
                                         END,
               'CANTIDAD/V.FINAL'   = CASE BK.CONCEPTO
                                         WHEN 'VI' THEN PC.VALOR_FINAL
                                         WHEN 'CI' THEN PC.VALOR_FINAL
                                         ELSE BK.NOMINALES
                                         END,
               'PRECIO'             = CASE BK.CONCEPTO
                                         WHEN 'VI' THEN PC.TASA_PACTO
                                         WHEN 'CI' THEN PC.TASA_PACTO
                                         ELSE BK.PRECIO
                                         END,
               'MONTO NETO'         = (CASE
                                          WHEN FE.FACENC_SUBFACTURACION IN ('D') THEN
                                             ROUND((BK.VALOR / (SELECT TOP 1 CONVERT(NUMERIC(16,4),(P.VALOR/POWER(10,ISNULL(P.DECIMALES,0))))
                                                                  FROM VALGEN P
                                                                 WHERE P.CODIGO_DEL_VALOR = '0'
                                                                   AND P.TIPO_DE_VALOR = 'DO'
                                                                   AND P.FECHA_DEL_VALOR <= @VFECHA_MOVIMIENTOS
                                                                 ORDER BY P.FECHA_DEL_VALOR DESC)), 4)
                                          WHEN FE.FACENC_SUBFACTURACION = 'E' THEN
                                             ROUND((BK.VALOR / (SELECT TOP 1 CONVERT(NUMERIC(16,4),(P.VALOR/POWER(10,ISNULL(P.DECIMALES,0))))
                                                                  FROM VALGEN P
                                                                 WHERE P.CODIGO_DEL_VALOR = '0'
                                                                   AND P.TIPO_DE_VALOR = 'EU'
                                                                   AND P.FECHA_DEL_VALOR <= @VFECHA_MOVIMIENTOS
                                                                 ORDER BY P.FECHA_DEL_VALOR DESC)), 4)
                                          ELSE BK.VALOR
                                          END),
               'COMISION'           = ISNULL(BK.COMISIONES,0),
               'DERECHO'            = ISNULL(BK.DERECHOS,0),
               'GASTOS'             = ISNULL(BK.GASTOS,0),
               'IVA'                = ISNULL(BK.IVA,0),
               'MONTO OPERACION'    = (CASE
                                          WHEN FE.FACENC_SUBFACTURACION  IN ('D') THEN
                                             ROUND((BK.VALOR / (SELECT TOP 1 CONVERT(NUMERIC(16,4),(P.VALOR/POWER(10,ISNULL(P.DECIMALES,0))))
                                                                  FROM VALGEN P
                                                                 WHERE P.CODIGO_DEL_VALOR = '0'
                                                                   AND P.TIPO_DE_VALOR = 'DO'
                                                                   AND P.FECHA_DEL_VALOR <= @VFECHA_MOVIMIENTOS
                                                                 ORDER BY P.FECHA_DEL_VALOR DESC)), 4)
                                          WHEN FE.FACENC_SUBFACTURACION  = 'E' THEN
                                             ROUND((BK.VALOR / (SELECT TOP 1 CONVERT(NUMERIC(16,4),(P.VALOR/POWER(10,ISNULL(P.DECIMALES,0))))
                                                                  FROM VALGEN P
                                                                 WHERE P.CODIGO_DEL_VALOR = '0'
                                                                   AND P.TIPO_DE_VALOR = 'EU'
                                                                   AND P.FECHA_DEL_VALOR <= @VFECHA_MOVIMIENTOS
                                                                 ORDER BY P.FECHA_DEL_VALOR DESC)), 4)
                                          ELSE BK.VALOR
                                          END) +
                                      ((ISNULL(BK.COMISIONES,0) + ISNULL(BK.DERECHOS,0) + ISNULL(BK.GASTOS,0) + ISNULL(BK.IVA,0)) *
                                       CASE BK.CONCEPTO
                                          WHEN 'IV' THEN -1
                                          WHEN 'RG' THEN -1
                                          WHEN 'IC' THEN 1
                                          WHEN 'RG' THEN 1
                                          WHEN 'SO' THEN -1
                                          WHEN 'VI' THEN 1
                                          WHEN 'CI' THEN -1
                                          ELSE 1
                                          END),
               'FECHA LIQUIDACION'  = CASE
                                         WHEN ISNULL(BK.FECHA_LIQUIDACION,0) > 693596 THEN CONVERT(DATETIME, BK.FECHA_LIQUIDACION - 693596)
                                         ELSE CONVERT(DATETIME, BK.FECHA_DE_MVTO - 693596)
                                         END,
               'T_NUMERO_FACTURA'   = ISNULL(FD.FACDET_FOLIO, 0),
               'T_TIPO_LIQUIDACION' = ISNULL(BK.TIPO_LIQUIDACION, ''),
               'T_BOLSA'            = ISNULL(BK.BOLSA_PALO, ''),
               'T_CUSTODIA_NOMINAL' = ISNULL(BK.CUSTODIA_ORIGEN, ''),
               'T_MONEDA'           = CASE
                                         WHEN FE.FACENC_SUBFACTURACION  = 'D' THEN 'DO'
                                         WHEN FE.FACENC_SUBFACTURACION  = 'E' THEN 'EU'
                                         ELSE '$$'
                                         END,
               'T_CONTRAPARTE'      = ISNULL(RTRIM(LTRIM(BK.CONTRAPARTE)), '')

		FROM BACKOFF AS BK WITH (NOLOCK)
		LEFT JOIN RELORD AS RO WITH (NOLOCK) ON RO.CONCEPTO       = BK.CONCEPTO AND
		                                        RO.ID_CONCEPTO    = BK.ID_CONCEPTO AND
		                                        RO.CONCEPTO_LINEA = BK.ID_LINEA_CONCEPTO
		LEFT JOIN PACTOS AS PC WITH (NOLOCK) ON  PC.ID_OPERACION = BK.ID_CONCEPTO
		LEFT OUTER JOIN DBO.FACTUDET AS FD WITH (NOLOCK) ON FD.FACDET_CONCEPTO      = BK.CONCEPTO AND
		                                                    FD.FACDET_IDCONCEPTO    = BK.ID_CONCEPTO AND
		                                                    FD.FACDET_LINEACONCEPTO = BK.ID_LINEA_CONCEPTO
		LEFT OUTER JOIN DBO.FACTUENC AS FE WITH (NOLOCK) ON FE.FACENC_SISTEMA       = FD.FACDET_SISTEMA AND
		                                                    FE.FACENC_TIPO          = FD.FACDET_TIPO AND
		                                                    FE.FACENC_AFECTA_IVA    = FD.FACDET_AFECTA_IVA AND
		                                                    FE.FACENC_FOLIO         = FD.FACDET_FOLIO AND
		                                                    FE.FACENC_ESTADO        = 'I'
		JOIN DBO.VIEW_CLIENTE_GPI_SEC AS V WITH (NOLOCK) ON (V.ORIGEN_MAGIC='MAGIC_VALORES' AND V.CUENTA=BK.ID_CLIENTE)
		WHERE BK.FECHA_DE_MVTO = @VFECHA_MOVIMIENTOS AND
			  BK.ORIGEN = 'OP' AND
              (
			    (BK.CONCEPTO IN ('IC','IV','IG','RG') AND
                 BK.ID_LINEA_CONCEPTO > 0 AND
                 BK.MODULO_DE_ENTRADA = 'RF')
                OR
    --(BK.CONCEPTO = 'SO' AND
    -- BK.ID_LINEA_CONCEPTO > 0)
    --OR
                (BK.CONCEPTO IN ('VI','CI') AND
                 BK.ID_LINEA_CONCEPTO = 0)
              )
		OPEN REC_MOV
		FETCH NEXT FROM REC_MOV
		INTO  @VRUT_CLIENTE, @VTIPO_OPERACION,
			  @VNUMERO_ORDEN, @VCORRELATIVO,
			  @VNEMOTECNICO, @VCANTIDAD,
			  @VPRECIO, @VMONTO_NETO,
			  @VCOMISION, @VDERECHO,
			  @VGASTOS, @VIVA,
			  @VMONTO_OPERACION, @VFECHA_LIQUIDACION,
			  @INUMEROFACTURA, @VTIPO_LIQUIDACION,
			  @VBOLSA, @VCUSTODIA_NOMINAL,
			  @VMONEDA, @VCONTRAPARTE
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC DBO.AD_RETORNA_DATOS_CLIENTE
				@VRUT_CLIENTE,
				@VTIPO_PERSONA OUTPUT,
				@VRAZON_SOCIAL OUTPUT,
				@VAPELLIDO_PATERNO OUTPUT,
				@VAPELLIDO_MATERNO OUTPUT,
				@VNOMBRES OUTPUT,
				'',
				0,
				'',
				0,
				'',
				'',
				'',
				'',
				0,
				'',
				0,
				'',
				'',
				'',
				''

			INSERT INTO #TMP_RFIJA
			SELECT @VRUT_CLIENTE,
				   @VTIPO_OPERACION,
                   @VNUMERO_ORDEN,
                   @VCORRELATIVO,
                   @VNEMOTECNICO,
                   @VCANTIDAD,
                   @VPRECIO,
                   @VMONTO_NETO,
                   @VCOMISION,
                   @VDERECHO,
                   @VGASTOS,
                   @VIVA,
                   @VMONTO_OPERACION,
                   @VFECHA_LIQUIDACION,
                   CASE @VTIPO_PERSONA
                      WHEN 'PN' THEN RTRIM(@VNOMBRES) + ' ' + RTRIM(@VAPELLIDO_PATERNO) + ' ' + RTRIM(@VAPELLIDO_MATERNO)
                      ELSE @VRAZON_SOCIAL
                   END,
                   @INUMEROFACTURA,
                   @VTIPO_LIQUIDACION,
                   @VBOLSA,
                   @VCUSTODIA_NOMINAL,
                   @VMONEDA,
                   @VCONTRAPARTE

			FETCH NEXT FROM REC_MOV
			INTO @VRUT_CLIENTE, @VTIPO_OPERACION,
			     @VNUMERO_ORDEN, @VCORRELATIVO,
                 @VNEMOTECNICO, @VCANTIDAD,
                 @VPRECIO, @VMONTO_NETO,
                 @VCOMISION, @VDERECHO,
                 @VGASTOS, @VIVA,
                 @VMONTO_OPERACION, @VFECHA_LIQUIDACION,
                 @INUMEROFACTURA, @VTIPO_LIQUIDACION,
                 @VBOLSA, @VCUSTODIA_NOMINAL, @VMONEDA, @VCONTRAPARTE
		END
		CLOSE REC_MOV
		DEALLOCATE REC_MOV

	END

	SELECT 'RUT_CLIENTE'             = T_RUT_CLIENTE,
           'TIPO_OPERACION'          = T_TIPO_OPERACION,
           'NUMERO_ORDEN'            = T_NUMERO_ORDEN,
           'CORRELATIVO'             = T_CORRELATIVO,
           'NEMOTECNICO'             = T_NEMOTECNICO,
           'CANTIDAD'                = T_CANTIDAD,
           'PRECIO'                  = T_PRECIO,
           'MONTO_NETO'              = T_MONTO_NETO,
           'COMISION'                = T_COMISION,
           'DERECHO'                 = T_DERECHO,
           'GASTOS'                  = T_GASTOS,
           'IVA'                     = T_IVA,
           'MONTO_OPERACION'         = T_MONTO_OPERACION,
           'FECHA_LIQUIDACION'       = T_FECHA_LIQUIDACION,
           'NOMBRE_CLIENTE'          = T_NOMBRE_CLIENTE,
           'NUMERO_FACTURA'          = T_NUMERO_FACTURA,
           'TIPO_LIQ'                = T_TIPO_LIQUIDACION,
           'BOLSA'                   = T_BOLSA,
           'CUST_NOM'                = T_CUSTODIA_NOMINAL,
           'GARANTIAS'               = 0,
           'PRESTAMOS'               = 0,
           'SIMULTANEAS'             = 0 ,
           'NOMBRE_INSTRUMENTO_FECU' = ISNULL(NOMBRE_INSTRUMENTO_FECU,''),
           'MONEDA'                  = T_MONEDA,
           'CONTRAPARTE'             = RTRIM(LTRIM(T_CONTRAPARTE))
	  FROM #TMP_RFIJA WITH (NOLOCK)
      LEFT OUTER JOIN INSTRU I WITH (NOLOCK) ON I.ID_INSTRUMENTO=T_NEMOTECNICO

	DROP TABLE #TMP_RFIJA
END
GO

GRANT EXECUTE ON [AD_QRY_CB_MOVTO_RFIJA_II] TO DB_EXECUTESP
GO
