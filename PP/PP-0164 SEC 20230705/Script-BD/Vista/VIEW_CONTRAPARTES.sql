IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_CONTRAPARTES]') AND type in (N'V', N'PC'))
DROP VIEW [dbo].[VIEW_CONTRAPARTES]
GO

CREATE VIEW [dbo].[VIEW_CONTRAPARTES] AS
	SELECT CO.ID_CONTRAPARTE,
           CO.DSC_CONTRAPARTE,
           TCO.ID_TIPO_CONTRAPARTE,
           TCO.DSC_TIPO_CONTRAPARTE,
           CO.RUT_CONTRAPARTE,
           CO.FLG_EMPRESA_SEC
      FROM CONTRAPARTES CO,
           TIPOS_CONTRAPARTES TCO
     WHERE TCO.ID_TIPO_CONTRAPARTE = CO.ID_TIPO_CONTRAPARTE
GO