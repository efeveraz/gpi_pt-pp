IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_CARGAS_OPERACIONES$General]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_CARGAS_OPERACIONES$General]
GO

CREATE PROCEDURE PKG_CARGAS_OPERACIONES$General
( @PFECHA             DATETIME
, @PCARGA_OPE         VARCHAR(10) = NULL
, @PFECHA_MOVTO       INTEGER
, @PTIPO_OPERACION    VARCHAR(10)
, @PNRO_OPE           VARCHAR(50)
, @PCLIENTE           VARCHAR(20)
, @PINSTRUMENTO       VARCHAR(100)
, @PNOMINALES         FLOAT
, @PPRECIO            FLOAT
, @PVALOR             FLOAT
, @PFECHA_LIQUIDACION NUMERIC
, @PTIPO_LIQUIDACION  VARCHAR(10)
, @PCUENTA_GPI        VARCHAR(20)
, @PTIPO_CUENTA       NUMERIC
, @PID_MOV_ACTIVO     NUMERIC = NULL
) AS
BEGIN

     SET NOCOUNT ON

     DECLARE @LCARGA_CCUPON      CHAR(1)
           , @LCARGA_SORTEO      CHAR(1)
           , @LCARGA_DIVIDENDO   CHAR(1)
           , @LCARGA_OPCION      CHAR(1)
           , @LCARGA_FUERARUEDA  CHAR(1)
           , @LCARGA_SPOT        CHAR(1)
           , @LCARGA_CUSTODIA    CHAR(1)
           , @LCARGA_MERCANTIL   CHAR(1)
           , @LCARGA_VENCIMIENTO CHAR(1)
           , @LTIENE_CAJA        CHAR(1)
           , @LID_MONEDA_ING     NUMERIC
           , @LID_CUENTA         NUMERIC

     SET @LCARGA_SPOT  = 'N'
     SET @LTIENE_CAJA  = 'S'

     IF ISNULL(@PCARGA_OPE,'') = ''
      BEGIN
           SET @LCARGA_CCUPON      = 'S'
           SET @LCARGA_SORTEO      = 'S'
           SET @LCARGA_DIVIDENDO   = 'S'
           SET @LCARGA_OPCION      = 'S'
           SET @LCARGA_FUERARUEDA  = 'S'
           SET @LCARGA_SPOT        = 'S'
           SET @LCARGA_CUSTODIA    = 'S'
           SET @LCARGA_MERCANTIL   = 'S'
           SET @LCARGA_VENCIMIENTO = 'S'
      END
     ELSE
      IF @PCARGA_OPE = 'CUPON'
         SET @LCARGA_CCUPON = 'S'
      ELSE
      IF @PCARGA_OPE = 'SORTEO'
         SET @LCARGA_SORTEO = 'S'
      ELSE
      IF @PCARGA_OPE = 'DIVIDENDO'
         SET @LCARGA_DIVIDENDO = 'S'
      ELSE
      IF @PCARGA_OPE = 'OPCION'
         SET @LCARGA_OPCION = 'S'
      ELSE
      IF @PCARGA_OPE = 'FUERARUEDA'
         SET @LCARGA_FUERARUEDA = 'S'
      ELSE
      IF @PCARGA_OPE = 'SPOT'
         SET @LCARGA_SPOT = 'S'
      ELSE
      IF @PCARGA_OPE = 'CUSTODIA'
         SET @LCARGA_CUSTODIA = 'S'
      ELSE
      IF @PCARGA_OPE = 'MERCANTIL'
         SET @LCARGA_MERCANTIL = 'S'
      ELSE
      IF @PCARGA_OPE = 'VENCIMIENTO'
         SET @LCARGA_VENCIMIENTO = 'S'
      ELSE

-----------------------------------------------------------------------------------------
     DECLARE @LFECHA_PROCESO INT
     SET @LFECHA_PROCESO =(CAST(convert(datetime, @PFECHA) AS INT)+693596)
-----------------------------------------------------------------------------------------
     CREATE TABLE #TBLOPERACIONES  (COD_TIPO_PROD         VARCHAR(20)
                                  , DSC_TIPO_PROD         VARCHAR(50)
                                  , TIPO_OPERACION        VARCHAR(10)
                                  , NRO_OPE               VARCHAR(50)
                                  , FLG_TIPO_OPERACION    INTEGER
                                  , CONTRAPARTE           VARCHAR(20)
                                  , CUENTA_GPI            VARCHAR(20)
                                  , ID_NEMOTECNICO        NUMERIC
                                  , INSTRUMENTO           VARCHAR(100)
                                  , NEMOTECNICO           VARCHAR(100)
                                  , COD_INSTRUMENTO       VARCHAR(20)
                                  , DSC_INSTRUMENTO       VARCHAR(50)
                                  , COD_PRODUCTO          VARCHAR(20)
                                  , ID_MONEDA_TRANSACCION NUMERIC
                                  , NOMINALES             FLOAT
                                  , PRECIO                FLOAT
                                  , VALOR                 FLOAT
                                  , FECHA_LIQUIDACION     NUMERIC
                                  , TIPO_LIQUIDACION      VARCHAR(10)
                                  , ID_OPE_GPI            NUMERIC
                                  , ID_MOV_ACTIVO         NUMERIC)

-----------------------------------------------------------------------------------------
      IF @LCARGA_CCUPON = 'S'
       BEGIN
            INSERT INTO #TBLOPERACIONES
            SELECT @PCARGA_OPE
                 , 'Cortes de Cup�n'
                 , @PTIPO_OPERACION
                 , @PNRO_OPE
                 , 1
                 , @PCLIENTE
                 , @PCUENTA_GPI
                 , ISNULL(N.ID_NEMOTECNICO,-1)
                 , @PINSTRUMENTO
                 , ISNULL(N.NEMOTECNICO,'')
                 , ISNULL(N.COD_INSTRUMENTO,'')
                 , ISNULL(N.DSC_INSTRUMENTO,'')
                 , ISNULL(N.COD_PRODUCTO,'')
                 , ISNULL(N.ID_MONEDA_TRANSACCION,-1)
                 , @PNOMINALES
                 , @PPRECIO
                 , @PVALOR
                 , @PFECHA_LIQUIDACION
                 , @PTIPO_LIQUIDACION
                 , ISNULL(A.ID_ENTIDAD,-1)
                 , NULL
              FROM (SELECT N.ID_NEMOTECNICO
                                         , N.NEMOTECNICO
                                         , N.COD_INSTRUMENTO
                                         , I.COD_PRODUCTO
                                         , I.DSC_INTRUMENTO 'DSC_INSTRUMENTO'
                                         , N.ID_MONEDA_TRANSACCION
                                      FROM NEMOTECNICOS N
                                         , INSTRUMENTOS I
                                     WHERE I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
                                    ) N
                   LEFT OUTER JOIN (SELECT R.ID_ENTIDAD
                                         , R.VALOR
                                      FROM REL_CONVERSIONES R
                                         , ORIGENES O
                                         , TIPOS_CONVERSION C
                                     WHERE O.COD_ORIGEN         = 'MAGIC_CARGA_OPE'
                                       AND C.TABLA              = 'CARGOS_ABONOS'
                                       AND R.ID_ORIGEN          = O.ID_ORIGEN
                                       AND R.ID_TIPO_CONVERSION = C.ID_TIPO_CONVERSION) A ON A.VALOR = @PNRO_OPE
             WHERE N.NEMOTECNICO = dbo.PKG_CARGAS_OPERACIONES$Fnt_BuscaNemo(@PINSTRUMENTO)
       END
-----------------------------------------------------------------------------------------
      IF @LCARGA_VENCIMIENTO = 'S'
       BEGIN
            INSERT INTO #TBLOPERACIONES
            SELECT @PCARGA_OPE
                 , 'Vencimiento de Cup�n'
                 , @PTIPO_OPERACION
                 , @PNRO_OPE
                 , 1
                 , @PCLIENTE
                 , @PCUENTA_GPI
                 , ISNULL(N.ID_NEMOTECNICO,-1)
                 , @PINSTRUMENTO
                 , ISNULL(N.NEMOTECNICO,'')
                 , ISNULL(N.COD_INSTRUMENTO,'')
                 , ISNULL(N.DSC_INSTRUMENTO,'')
                 , ISNULL(N.COD_PRODUCTO,'')
                 , ISNULL(N.ID_MONEDA_TRANSACCION,-1)
                 , @PNOMINALES
                 , @PPRECIO
                 , @PVALOR
                 , @PFECHA_LIQUIDACION
                 , @PTIPO_LIQUIDACION
                 , ISNULL(A.ID_ENTIDAD,-1)
                 , @PID_MOV_ACTIVO
              FROM (SELECT N.ID_NEMOTECNICO
                         , N.NEMOTECNICO
                         , N.COD_INSTRUMENTO
                         , I.COD_PRODUCTO
                         , I.DSC_INTRUMENTO 'DSC_INSTRUMENTO'
                         , N.ID_MONEDA_TRANSACCION
                      FROM NEMOTECNICOS N
                         , INSTRUMENTOS I
                     WHERE I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                   LEFT OUTER JOIN (SELECT R.ID_ENTIDAD
                                         , R.VALOR
                                      FROM REL_CONVERSIONES R
                                         , ORIGENES O
                                         , TIPOS_CONVERSION C
                                     WHERE O.COD_ORIGEN         = 'MAGIC_CARGA_OPE'
                                       AND C.TABLA              = 'OPERACIONES_DETALLE'
                                       AND R.ID_ORIGEN          = O.ID_ORIGEN
                                       AND R.ID_TIPO_CONVERSION = C.ID_TIPO_CONVERSION) A ON A.VALOR = @PNRO_OPE
             WHERE N.NEMOTECNICO = dbo.PKG_CARGAS_OPERACIONES$Fnt_BuscaNemo(@PINSTRUMENTO)
       END
-----------------------------------------------------------------------------------------
      IF @LCARGA_SORTEO = 'S'
       BEGIN
            INSERT INTO #TBLOPERACIONES
            SELECT @PCARGA_OPE
                 , 'Sorteos de Letras'
                 , @PTIPO_OPERACION
                 , @PNRO_OPE
                 , 1
                 , @PCLIENTE
                 , @PCUENTA_GPI
                 , N.ID_NEMOTECNICO
                 , @PINSTRUMENTO
                 , ISNULL(N.NEMOTECNICO,'')
                 , ISNULL(N.COD_INSTRUMENTO,'')
                 , ISNULL(N.DSC_INSTRUMENTO,'')
                 , ISNULL(N.COD_PRODUCTO,'')
                 , ISNULL(N.ID_MONEDA_TRANSACCION,-1)
                 , @PNOMINALES
                 , @PPRECIO
                 , @PVALOR
                 , @PFECHA_LIQUIDACION
                 , @PTIPO_LIQUIDACION
                 , ISNULL(A.ID_ENTIDAD,-1)
                 , @PID_MOV_ACTIVO
              FROM (SELECT N.ID_NEMOTECNICO
                         , N.NEMOTECNICO
                         , N.COD_INSTRUMENTO
                         , I.COD_PRODUCTO
                         , I.DSC_INTRUMENTO 'DSC_INSTRUMENTO'
                         , N.ID_MONEDA_TRANSACCION
                      FROM NEMOTECNICOS N
                         , INSTRUMENTOS I
                     WHERE I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                   LEFT OUTER JOIN (SELECT R.ID_ENTIDAD
               , R.VALOR
                                      FROM REL_CONVERSIONES R
                                         , ORIGENES O
                                         , TIPOS_CONVERSION C
                                     WHERE O.COD_ORIGEN         = 'MAGIC_CARGA_OPE'
                                       AND C.TABLA              = 'OPERACIONES_DETALLE'
                                       AND R.ID_ORIGEN              = O.ID_ORIGEN
                                       AND R.ID_TIPO_CONVERSION = C.ID_TIPO_CONVERSION) A ON A.VALOR = @PNRO_OPE
             WHERE N.NEMOTECNICO = dbo.PKG_CARGAS_OPERACIONES$Fnt_BuscaNemo(@PINSTRUMENTO)
       END
-----------------------------------------------------------------------------------------
      IF @LCARGA_DIVIDENDO = 'S'
       BEGIN
            INSERT INTO #TBLOPERACIONES
            SELECT @PCARGA_OPE
                 , 'Dividendos'
                 , @PTIPO_OPERACION
                 , @PNRO_OPE
                 , 1
                 , @PCLIENTE
                 , @PCUENTA_GPI
                 , ISNULL(N.ID_NEMOTECNICO,-1)
                 , @PINSTRUMENTO
                 , ISNULL(N.NEMOTECNICO,'')
                 , ISNULL(N.COD_INSTRUMENTO,'')
                 , ISNULL(N.DSC_INSTRUMENTO,'')
                 , ISNULL(N.COD_PRODUCTO,'')
                 , ISNULL(N.ID_MONEDA_TRANSACCION,-1)
                 , @PNOMINALES
                 , @PPRECIO
                 , @PVALOR
                 , @PFECHA_LIQUIDACION
                 , @PTIPO_LIQUIDACION
                 , ISNULL(A.ID_ENTIDAD,-1)
                 , NULL
              FROM (SELECT N.ID_NEMOTECNICO
                         , N.NEMOTECNICO
                         , N.COD_INSTRUMENTO
                         , I.COD_PRODUCTO
                         , I.DSC_INTRUMENTO 'DSC_INSTRUMENTO'
                         , N.ID_MONEDA_TRANSACCION
                      FROM NEMOTECNICOS N
                         , INSTRUMENTOS I
                     WHERE I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                   LEFT OUTER JOIN (SELECT R.ID_ENTIDAD
                                         , R.VALOR
                                      FROM REL_CONVERSIONES R
                                         , ORIGENES O
                                         , TIPOS_CONVERSION C
                                     WHERE O.COD_ORIGEN         = 'MAGIC_CARGA_OPE'
                                       AND C.TABLA              = 'CARGOS_ABONOS'
                                       AND R.ID_ORIGEN          = O.ID_ORIGEN
                                       AND R.ID_TIPO_CONVERSION = C.ID_TIPO_CONVERSION) A ON A.VALOR = @PNRO_OPE
             WHERE N.NEMOTECNICO = @PINSTRUMENTO
       END
-----------------------------------------------------------------------------------------
      IF @LCARGA_OPCION = 'S'
       BEGIN
            INSERT INTO #TBLOPERACIONES
            SELECT 'OPCION'
                 , 'Opciones'
                 , @PTIPO_OPERACION      as PTIPO_OPERACION
                 , @PNRO_OPE          as PNRO_OPE
                 , 1
                 , @PCLIENTE      as PCLIENTE
                 , @PCUENTA_GPI      as PCUENTA_GPI
                 , ISNULL(N.ID_NEMOTECNICO,-1)     as ID_NEMOTECNICO
                 , @PINSTRUMENTO       as PINSTRUMENTO
                 , ISNULL(N.NEMOTECNICO,'')    as NEMOTECNICO
                 , ISNULL(N.COD_INSTRUMENTO,'')   as COD_INSTRUMENTO
                 , ISNULL(N.DSC_INSTRUMENTO,'')      as DSC_INSTRUMENTO
                 , ISNULL(N.COD_PRODUCTO,'')     as COD_PRODUCTO
                 , ISNULL(N.ID_MONEDA_TRANSACCION,-1) as ID_MONEDA_TRANSACCION
                 , @PNOMINALES as PNOMINALES
                 , @PPRECIO  as PPRECIO
                 , @PVALOR   as PVALOR
                 , ''   as PFECHA_LIQUIDACION
                 , @PTIPO_LIQUIDACION    as PTIPO_LIQUIDACION
                 , ISNULL(A.ID_ENTIDAD,-1)
                 , NULL
              FROM (SELECT N.ID_NEMOTECNICO
                         , N.NEMOTECNICO
                         , N.COD_INSTRUMENTO
                         , I.COD_PRODUCTO
                         , I.DSC_INTRUMENTO 'DSC_INSTRUMENTO'
                         , N.ID_MONEDA_TRANSACCION
                      FROM NEMOTECNICOS N
                         , INSTRUMENTOS I
                     WHERE I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                   LEFT OUTER JOIN (SELECT R.ID_ENTIDAD
                                         , R.VALOR
                                      FROM REL_CONVERSIONES R
                                         , ORIGENES O
                                         , TIPOS_CONVERSION C
                                     WHERE O.COD_ORIGEN         = 'MAGIC_CARGA_OPE'
                                       AND C.TABLA              = 'DIVIDENDOS'
                                       AND R.ID_ORIGEN          = O.ID_ORIGEN
                                       AND R.ID_TIPO_CONVERSION = C.ID_TIPO_CONVERSION) A ON A.VALOR = @PNRO_OPE
             Where N.NEMOTECNICO = dbo.PKG_CARGAS_OPERACIONES$Fnt_BuscaNemo(@PINSTRUMENTO)
       END

-----------------------------------------------------------------------------------------
      IF @LCARGA_FUERARUEDA = 'S'
       BEGIN
            INSERT INTO #TBLOPERACIONES
            SELECT @PCARGA_OPE
                 , 'Ope. Fuera de Rueda'
                 , @PTIPO_OPERACION
                 , @PNRO_OPE
                 , CASE @PTIPO_OPERACION
                        WHEN 'IV' THEN 0
                        WHEN 'IC' THEN 1
                   END
                 , @PCLIENTE
                 , @PCUENTA_GPI
                 , ISNULL(N.ID_NEMOTECNICO,-1)
                 , @PINSTRUMENTO
                 , ISNULL(N.NEMOTECNICO,'')
                 , ISNULL(N.COD_INSTRUMENTO,'')
                 , ISNULL(N.DSC_INSTRUMENTO,'')
                 , ISNULL(N.COD_PRODUCTO,'')
                 , ISNULL(N.ID_MONEDA_TRANSACCION,-1)
                 , @PNOMINALES
                 , @PPRECIO
                 , @PVALOR
                 , @PFECHA_LIQUIDACION
                 , @PTIPO_LIQUIDACION
                 , ISNULL(A.ID_ENTIDAD,-1)
                 , NULL
              FROM (SELECT N.ID_NEMOTECNICO
                         , N.NEMOTECNICO
                         , N.COD_INSTRUMENTO
                         , I.COD_PRODUCTO
                         , I.DSC_INTRUMENTO 'DSC_INSTRUMENTO'
                         , N.ID_MONEDA_TRANSACCION
                      FROM NEMOTECNICOS N
                         , INSTRUMENTOS I
                     WHERE I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                   LEFT OUTER JOIN (SELECT R.ID_ENTIDAD
                                         , R.VALOR
                                      FROM REL_CONVERSIONES R
                                         , ORIGENES O
                                         , TIPOS_CONVERSION C
                                     WHERE O.COD_ORIGEN         = 'MAGIC_CARGA_OPE'
                                       AND C.TABLA              = 'OPERACIONES_DETALLE'
                                       AND R.ID_ORIGEN          = O.ID_ORIGEN
                                       AND R.ID_TIPO_CONVERSION = C.ID_TIPO_CONVERSION) A ON A.VALOR = @PNRO_OPE
             WHERE N.NEMOTECNICO = dbo.PKG_CARGAS_OPERACIONES$Fnt_BuscaNemo(@PINSTRUMENTO)

             IF ISNULL((SELECT Count(*) FROM #TBLOPERACIONES),0) = 0 /* Nemot�cnico no encontrado */
                INSERT INTO #TBLOPERACIONES
                SELECT @PCARGA_OPE
                     , 'Ope. Fuera de Rueda'
                     , @PTIPO_OPERACION
                     , @PNRO_OPE
                     , CASE @PTIPO_OPERACION
                            WHEN 'IV' THEN 0
                            WHEN 'IC' THEN 1
                       END
                     , @PCLIENTE
                     , @PCUENTA_GPI
                     , -1
                     , @PINSTRUMENTO
                     , ''
                     , ''
                     , ''
                     , ''
                     , -1
                     , @PNOMINALES
                     , @PPRECIO
                     , @PVALOR
                     , @PFECHA_LIQUIDACION
                     , @PTIPO_LIQUIDACION
                     , -1
                     , NULL

       END
-----------------------------------------------------------------------------------------
      IF @LCARGA_SPOT = 'S'
       BEGIN

          SELECT @LID_MONEDA_ING = ID_MONEDA
            FROM MONEDAS
           WHERE COD_MONEDA = CASE @PINSTRUMENTO WHEN 'DOLAR' THEN 'USD'
                                            WHEN 'EURO'  THEN 'EUR'
                                            WHEN 'PESOS' THEN '$$'
                                            ELSE @PINSTRUMENTO END
          SELECT @LID_CUENTA = ID_CUENTA
            FROM CUENTAS WHERE NUM_CUENTA = @pCuenta_Gpi

          IF NOT EXISTS (SELECT 1 FROM CAJAS_CUENTA WHERE ID_CUENTA = @LID_CUENTA AND ID_MONEDA = @LID_MONEDA_ING)
             SET @LTIENE_CAJA = 'N'


            INSERT INTO #TBLOPERACIONES
            SELECT @PCARGA_OPE
                 , 'Operaciones Spot'
                 , @PTIPO_OPERACION
                 , @PNRO_OPE
                 , CASE @PTIPO_OPERACION
                        WHEN 'V' THEN 0
                        WHEN 'C' THEN 1
                   END
                 , @PCLIENTE
                 , @PCUENTA_GPI
                 , 0
                 , @PINSTRUMENTO
                 , @PINSTRUMENTO
                 , 'SPOT'
                 , 'Spot'
                 , ''
                 , 0
                 , @PNOMINALES
                 , @PPRECIO
                 , @PVALOR
                 , @PFECHA_LIQUIDACION
                 , @PTIPO_LIQUIDACION
                 , ISNULL((SELECT R.ID_ENTIDAD
                             FROM REL_CONVERSIONES R
                                , ORIGENES O
                                , TIPOS_CONVERSION C
                            WHERE O.COD_ORIGEN         = 'MAGIC_CARGA_OPE'
                              AND C.TABLA              = 'REMESAS'
                              AND R.ID_ORIGEN          = O.ID_ORIGEN
                              AND R.ID_TIPO_CONVERSION = C.ID_TIPO_CONVERSION
                              AND R.VALOR = @PNRO_OPE),-1)
                 , NULL
       END
-----------------------------------------------------------------------------------------
      IF @LCARGA_CUSTODIA = 'S'
       BEGIN
            INSERT INTO #TBLOPERACIONES
            SELECT @PCARGA_OPE
                 , 'Ingreso/Egreso Custodia'
                 , @PTIPO_OPERACION
                 , @PNRO_OPE
                 , CASE @PTIPO_OPERACION
                        WHEN 'RG' THEN 0
                        WHEN 'IG' THEN 1
                   END
                 , @PCLIENTE
                 , @PCUENTA_GPI
                 , ISNULL(N.ID_NEMOTECNICO,-1)
                 , @PINSTRUMENTO
                 , ISNULL(N.NEMOTECNICO,'')
                 , ISNULL(N.COD_INSTRUMENTO,'')
                 , ISNULL(N.DSC_INSTRUMENTO,'')
                 , ISNULL(N.COD_PRODUCTO,'')
                 , ISNULL(N.ID_MONEDA_TRANSACCION,-1)
                 , @PNOMINALES
                 , @PPRECIO
                 , @PVALOR
                 , @PFECHA_LIQUIDACION
                 , @PTIPO_LIQUIDACION
                 , ISNULL(A.ID_ENTIDAD,-1)
                 , NULL
              FROM (SELECT N.ID_NEMOTECNICO
                         , N.NEMOTECNICO
                         , N.COD_INSTRUMENTO
                         , I.COD_PRODUCTO
                         , I.DSC_INTRUMENTO 'DSC_INSTRUMENTO'
                         , N.ID_MONEDA_TRANSACCION
                      FROM NEMOTECNICOS N
                         , INSTRUMENTOS I
                     WHERE I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                   LEFT OUTER JOIN (SELECT R.ID_ENTIDAD
                                         , R.VALOR
                                      FROM REL_CONVERSIONES R
                                         , ORIGENES O
                                         , TIPOS_CONVERSION C
                                     WHERE O.COD_ORIGEN         = 'MAGIC_CARGA_OPE'
                                       AND C.TABLA              = 'OPERACIONES_DETALLE'
                                       AND R.ID_ORIGEN          = O.ID_ORIGEN
                                       AND R.ID_TIPO_CONVERSION = C.ID_TIPO_CONVERSION) A ON A.VALOR = @PNRO_OPE
             WHERE N.NEMOTECNICO = dbo.PKG_CARGAS_OPERACIONES$Fnt_BuscaNemo(@PINSTRUMENTO)
       END
-----------------------------------------------------------------------------------------
      IF @LCARGA_MERCANTIL = 'S'
       BEGIN
            INSERT INTO #TBLOPERACIONES
            SELECT @PCARGA_OPE
                 , 'Cta. Cte. Mercantil'
                 , @PTIPO_OPERACION
                 , @PNRO_OPE
                 , CASE @PTIPO_OPERACION
                        WHEN 'C' THEN 0
                        WHEN 'A' THEN 1
                   END
                 , @PCLIENTE
                 , @PCUENTA_GPI
                 , 0
                 , ''
                 , ''
                 , 'CCM'
                 , 'Cta.Cte.Mercantil'
                 , ''
                 , ISNULL((SELECT R.ID_ENTIDAD
                            FROM REL_CONVERSIONES R
                               , ORIGENES O
                               , TIPOS_CONVERSION C
                           WHERE O.COD_ORIGEN         = 'MAGIC_CARGA_OPE'
                             AND C.TABLA              = 'MONEDAS_CCM'
                             AND R.ID_ORIGEN          = O.ID_ORIGEN
                             AND R.ID_TIPO_CONVERSION = C.ID_TIPO_CONVERSION
                             AND R.VALOR = CAST(@PTIPO_CUENTA AS NVARCHAR)) ,0)
                 , 0
                 , 0
                 , @PVALOR
                 , (CAST(convert(datetime, GETDATE()) AS INT)+693596)
                 , ''
                 , ISNULL((SELECT R.ID_ENTIDAD
                             FROM REL_CONVERSIONES R
                                , ORIGENES O
                                , TIPOS_CONVERSION C
                            WHERE O.COD_ORIGEN         = 'MAGIC_CARGA_OPE'
                              AND C.TABLA              = 'APORTE_RESCATE_CUENTA'
                              AND R.ID_ORIGEN          = O.ID_ORIGEN
                              AND R.ID_TIPO_CONVERSION = C.ID_TIPO_CONVERSION
                              AND R.VALOR              = @PNRO_OPE),-1)
                 , NULL
       END
-----------------------------------------------------------------------------------------

     SELECT CT.ID_EMPRESA
          , CT.DSC_EMPRESA
          , CT.RUT_CLIENTE
          , CT.NOMBRE_CLIENTE
          , CT.ID_CUENTA
          , CASE WHEN CT.ID_CUENTA is  Null THEN 'No Existe Cuenta'  ELSE 'OK' END 'RESULTADO_CUENTA'
          , CT.NUM_CUENTA
          , T.NRO_OPE
          , T.ID_NEMOTECNICO
          , T.ID_MONEDA_TRANSACCION
          , ISNULL((SELECT DSC_MONEDA FROM MONEDAS WHERE ID_MONEDA=ID_MONEDA_TRANSACCION),'N/A')  'DSC_MONEDA'
          , T.COD_TIPO_PROD
          , T.DSC_TIPO_PROD
          , T.TIPO_OPERACION
          , T.FLG_TIPO_OPERACION
          , T.CONTRAPARTE
          , T.INSTRUMENTO
          , T.NEMOTECNICO
          , T.COD_INSTRUMENTO
          , T.DSC_INSTRUMENTO
          , T.COD_PRODUCTO
          , T.NOMINALES
          , T.PRECIO
          , T.VALOR
          , Case When @LTIENE_CAJA = 'S' THEN 'OK'
                 When @LTIENE_CAJA = 'N' AND @LCARGA_SPOT = 'S' THEN 'Cuenta no tiene Caja en moneda: ' + T.INSTRUMENTO
                 ELSE 'OK' End  'RESULTADO_CAJA'
          , Case When T.FECHA_LIQUIDACION = 0 Then 0
                 Else (CAST(convert(int, T.FECHA_LIQUIDACION ) AS datetime)-693596) End 'fecha_liquidacion'
          , ISNULL(T.TIPO_LIQUIDACION,'N/A') 'TIPO_LIQUIDACION'
          , T.CUENTA_GPI
          , CASE  WHEN T.ID_NEMOTECNICO = -1 THEN 'No Existe Nemot�cnico'  ELSE 'OK' END 'RESULTADO_NEMO'
          , T.ID_OPE_GPI
          , CASE WHEN T.ID_OPE_GPI = -1 THEN 'OK'
                 WHEN T.ID_OPE_GPI <> -1 AND (@LCARGA_SPOT = 'S' OR @LCARGA_CCUPON = 'S') THEN 'Movimiento ya registrado en GPI (# ' + CAST((T.ID_OPE_GPI) AS NVARCHAR) + ')'
                 WHEN T.ID_OPE_GPI <> -1 AND (@LCARGA_SORTEO = 'S' OR @LCARGA_FUERARUEDA = 'S' OR @LCARGA_VENCIMIENTO = 'S') THEN 'Movimiento ya registrado en GPI (# ' + CAST((SELECT ID_OPERACION FROM OPERACIONES_DETALLE WHERE ID_OPERACION_DETALLE = T.ID_OPE_GPI) AS NVARCHAR) + ')'
            END 'RESULTADO_MOV'
          , T.ID_MOV_ACTIVO
       FROM #TBLOPERACIONES T
            LEFT OUTER JOIN (SELECT DISTINCT C.ID_EMPRESA
                                  , C.ID_CUENTA
                                  , C.NUM_CUENTA
                                  , CL.ID_CLIENTE
                                  , CL.RUT_CLIENTE
                                  , CL.NOMBRE_CLIENTE
                                  , VA.VALOR
                                  , E.DSC_EMPRESA
                               FROM CUENTAS C
                                  , VIEW_CLIENTES CL
                                  , VIEW_ALIAS VA
                                  , EMPRESAS E
                              WHERE VA.COD_ORIGEN IN ('MAGIC_VALORES','MAGIC_FFMM')
                                AND C.ID_CUENTA   = VA.ID_ENTIDAD
                                AND va.valor=@PCLIENTE
                                AND va.tabla='CUENTAS'
                                AND C.COD_ESTADO  = 'H'
                                AND CL.ID_CLIENTE = C.ID_CLIENTE
                                AND E.ID_EMPRESA  = C.ID_EMPRESA) CT ON CT.VALOR = T.CONTRAPARTE
      ORDER BY T.COD_TIPO_PROD
             , CT.ID_EMPRESA
             , CT.RUT_CLIENTE
             , CT.NUM_CUENTA
             , T.INSTRUMENTO
             , T.FLG_TIPO_OPERACION

     DROP TABLE #TBLOPERACIONES
     SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_CARGAS_OPERACIONES$General] TO DB_EXECUTESP
GO
