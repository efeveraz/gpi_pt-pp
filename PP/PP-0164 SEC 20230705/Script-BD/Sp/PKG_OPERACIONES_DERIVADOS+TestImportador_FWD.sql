IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'DBO.PKG_OPERACIONES_DERIVADOS$TestImportador_FWD') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
DROP PROCEDURE DBO.PKG_OPERACIONES_DERIVADOS$TestImportador_FWD
GO

CREATE PROCEDURE [dbo].[PKG_OPERACIONES_DERIVADOS$TestImportador_FWD]
( @PFECHA_PROCESO                DATETIME
, @PNUMERO_CONTRATO              NUMERIC
, @PTIPO_FORWARD                 VARCHAR(1)
, @PMONEDA_DERECHO               VARCHAR(5)
, @PMONEDA_OBLIGACION            VARCHAR(5)
, @PMONTO_DERECHO                NUMERIC
, @PMONTO_OBLIGACION             NUMERIC
, @PCUENTA_EXTERNA               VARCHAR(20)
, @PRESULTADO_A_MERCADO          NUMERIC
, @PESTADO_FWD                   VARCHAR(1)
, @PTIPO_ENTREGA_FWD             VARCHAR(1) = NULL
, @PCONTRAPARTE                  VARCHAR(15) = NULL
) AS
BEGIN

     SET NOCOUNT ON

     DECLARE @LID_CUENTA            NUMERIC    , @LCODRESULTADO        VARCHAR(5) , @LMSGRESULTADO      VARCHAR(800),  @LID_ORIGEN_OPER       VARCHAR(10) ,
             @LID_ORIGEN_CTA        NUMERIC    , @LTIPO_MOV            VARCHAR(1) , @LDESC_MOV          VARCHAR(10) ,  @LID_MONEDA_DER        NUMERIC     ,
             @LID_MONEDA_OBL        NUMERIC    , @LID_TIPO_CONVERSION  NUMERIC    , @LID_MONEDA_RECIBIR NUMERIC     ,  @LMONTO_MONEDA_RECIBIR NUMERIC     ,
             @LID_MONEDA_PAGAR      NUMERIC    , @LMONTO_MONEDA_PAGAR  NUMERIC    , @LID_MOV_DERIVADO   NUMERIC     ,  @RC                    INT         ,
             @LID_FWD_VAL_ESTATICO  NUMERIC    , @LNUM_CUENTA          VARCHAR(10), @LID_CLIENTE        NUMERIC     ,  @LNOMBRE_CLIENTE       VARCHAR(100),
             @LRUT_CLIENTE          VARCHAR(10), @LID_EMPRESA          NUMERIC    , @LCOD_ESTADO_CUENTA VARCHAR(1)  ,  @LCOD_ESTADO_CLIENTE   VARCHAR(1)  ,
             @LEXISTE_ALIAS_CUENTA  VARCHAR(1) , @LPOSICION_CARACTER   NUMERIC    , @LID_OPE_DERIVADO   NUMERIC     ,  @LCOD_TIPO_OPERACION   VARCHAR(10) ,
             @LDSC_TIPO_OPERACION   VARCHAR(50), @LID_CONTRAPARTE      NUMERIC

     SET @LMSGRESULTADO = ''
     SET @LCODRESULTADO = 'OK'
---------------------------------------------------------------------------------
     SELECT @LID_ORIGEN_OPER = ID_ORIGEN
       FROM ORIGENES
      WHERE COD_ORIGEN = 'FORWARD'

     SELECT @LID_TIPO_CONVERSION = ID_TIPO_CONVERSION
       FROM TIPOS_CONVERSION
      WHERE TABLA='OPERACIONES_DERIVADOS'
---------------------------------------------------------------------------------
     IF @LCODRESULTADO = 'OK'
        BEGIN
            IF @PESTADO_FWD = 'M'
               BEGIN
                   IF (SELECT COUNT(1) FROM OPERACIONES_DERIVADOS WHERE NRO_CONTRATO = @PNUMERO_CONTRATO AND COD_ESTADO <> 'A'
                                                                    AND COD_TIPO_OPERACION_DERIVADO <> 'ANT_FWD' AND COD_TIPO_OPERACION_DERIVADO = 'VENC_FWD') <> 0
                       BEGIN
                            SET @LCODRESULTADO = 'ERROR'
                            SET @LMSGRESULTADO = 'ERROR: Forward ya tiene un vencimiento asociado'
                       END
                   ELSE
                       SET @LCODRESULTADO = 'OK'
               END
        END
---------------------------------------------------------------------------------
     IF @LCODRESULTADO = 'OK'
        BEGIN
             IF @PESTADO_FWD = 'A'
                BEGIN
                     IF (SELECT COUNT(1) FROM OPERACIONES_DERIVADOS WHERE NRO_CONTRATO = @PNUMERO_CONTRATO AND COD_ESTADO <> 'A' AND COD_TIPO_OPERACION_DERIVADO = 'ANT_FWD') <> 0
                         BEGIN
                              SET @LCODRESULTADO = 'ERROR'
                              SET @LMSGRESULTADO = 'ERROR: Forward ya tiene un anticipo'
                         END
                     ELSE
                         BEGIN
                              SET @LCODRESULTADO = 'OK'
                         END
                END
      END
---------------------------------------------------------------------------------
      IF @LCODRESULTADO = 'OK'
         BEGIN
              IF @PESTADO_FWD = 'V'
                 BEGIN
                      IF (SELECT COUNT(1) FROM OPERACIONES_DERIVADOS WHERE NRO_CONTRATO = @PNUMERO_CONTRATO AND COD_ESTADO <> 'A' AND COD_TIPO_OPERACION_DERIVADO <> 'ANT_FWD'
                                                                       AND COD_TIPO_OPERACION_DERIVADO <> 'VENC_FWD') <> 0
                          BEGIN
                               SET @LCODRESULTADO = 'ERROR'
                               SET @LMSGRESULTADO = 'ERROR: Forward ya existe en GPI'
                          END
                      ELSE
                          IF @PESTADO_FWD = 'A'
                             BEGIN
                                 SET @LCODRESULTADO = 'ERROR'
                                 SET @LMSGRESULTADO = 'ERROR: Forward no existe en GPI, no se puede realizar Anticipo'
                             END
                 END
          END
---------------------------------------------------------------------------------
--      IF @LCODRESULTADO = 'OK'
--         BEGIN
              SELECT @LID_OPE_DERIVADO = ID_OPERACION_DERIVADO
                   , @LID_MOV_DERIVADO =  ID_MOV_DERIVADO
              FROM OPERACIONES_DERIVADOS
              WHERE NRO_CONTRATO = @PNUMERO_CONTRATO
                AND COD_ESTADO <> 'A'
                AND COD_TIPO_OPERACION_DERIVADO <> 'VENC_FWD'
                AND COD_TIPO_OPERACION_DERIVADO <> 'ANT_FWD'

               IF @LID_MOV_DERIVADO IS NOT NULL
                  BEGIN
                      EXECUTE @RC = PKG_FWD_VAL_ESTATICOS$GUARDAR @PID_FWD_VAL_ESTATICO = @LID_FWD_VAL_ESTATICO OUTPUT
                                                                , @PID_MOV_DERIVADO     = @LID_MOV_DERIVADO
                                                                , @PFECHA               = @PFECHA_PROCESO
                                                                , @PVALOR               = @PRESULTADO_A_MERCADO
                  END
--         END
---------------------------------------------------------------------------------
     IF @LCODRESULTADO = 'OK'
        BEGIN
            SET @LDESC_MOV = ( SELECT DescMov =  CASE @PTIPO_FORWARD
                                                      WHEN 'C' THEN 'Compra'
                                                      WHEN 'V' THEN 'Venta'
                                                 ELSE 'N/A'  END )
            IF @LDESC_MOV = 'N/A'
               BEGIN
                   SET @LCODRESULTADO = 'ERROR'
                   SET @LMSGRESULTADO = 'ERROR: Tipo de Forward incorrecto'
               END
        END
---------------------------------------------------------------------------------
     IF @LCODRESULTADO = 'OK'
        BEGIN
             SET @LID_TIPO_CONVERSION = ( SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WHERE TABLA='MONEDAS' )
             SET @LID_MONEDA_DER = ( SELECT ID_ENTIDAD FROM REL_CONVERSIONES WHERE ID_ORIGEN = @LID_ORIGEN_OPER AND ID_TIPO_CONVERSION = @LID_TIPO_CONVERSION AND VALOR = RTRIM(@PMONEDA_DERECHO))

             IF @LID_MONEDA_DER IS NULL
                BEGIN
                    SET @LCODRESULTADO = 'ERROR'
                    SET @LMSGRESULTADO = 'ERROR: Moneda derecho no tiene Alias en GPI'
                END
         END
---------------------------------------------------------------------------------
     IF @LCODRESULTADO = 'OK'
        BEGIN
             SET @LID_TIPO_CONVERSION = ( SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WHERE TABLA='MONEDAS' )
             SET @LID_MONEDA_OBL = ( SELECT ID_ENTIDAD FROM REL_CONVERSIONES WHERE ID_ORIGEN = @LID_ORIGEN_OPER AND ID_TIPO_CONVERSION = @LID_TIPO_CONVERSION AND VALOR = RTRIM(@PMONEDA_OBLIGACION) )

             IF @LID_MONEDA_OBL IS NULL
                BEGIN
                    SET @LCODRESULTADO = 'ERROR'
                    SET @LMSGRESULTADO = 'ERROR: Moneda obligación no tiene Alias en GPI'
                END
        END
---------------------------------------------------------------------------------
     IF @LCODRESULTADO = 'OK'
        BEGIN
             SET @LTIPO_MOV = ( SELECT DescMov =   CASE @PESTADO_FWD
                                                   WHEN 'A' THEN 'E'
                                                   WHEN 'V' THEN 'I'
                                                   WHEN 'M' THEN 'I'
                                                   ELSE ' ' END )
             ---------------------------------------------------------------------------------
             IF @PESTADO_FWD = 'A' -- ANTICIPO
                BEGIN
                     SELECT @LCOD_TIPO_OPERACION  = COD_TIPO_OPERACION_DERIVADO
                          , @LDSC_TIPO_OPERACION  = DSC_TIPO_OPERACION
                     FROM TIPOS_OPERACIONES_DERIVADOS
                     WHERE COD_TIPO_OPERACION_DERIVADO = 'ANT_FWD'
                     SET @LTIPO_MOV = 'E'
                END
             ------------------------------------------------------------------
             IF @PESTADO_FWD = 'V' -- VIGENTE
                BEGIN
                     SELECT @LCOD_TIPO_OPERACION  = COD_TIPO_OPERACION_DERIVADO
                          , @LDSC_TIPO_OPERACION  = DSC_TIPO_OPERACION
                     FROM TIPOS_OPERACIONES_DERIVADOS
                     WHERE COD_TIPO_OPERACION_DERIVADO = 'DIREC'
                END
             ------------------------------------------------------------------
             IF @PESTADO_FWD = 'M' -- MATERIALIZADA (VENCE ESE DIA)
                BEGIN
                     SELECT @LCOD_TIPO_OPERACION  = COD_TIPO_OPERACION_DERIVADO
                          , @LDSC_TIPO_OPERACION  = DSC_TIPO_OPERACION
                     FROM TIPOS_OPERACIONES_DERIVADOS
                     WHERE COD_TIPO_OPERACION_DERIVADO = 'VENC_FWD'
                     SET @LTIPO_MOV = 'E'
                END
             ------------------------------------------------------------------
             IF @PTIPO_FORWARD = 'C'
                BEGIN
                    SET @LID_MONEDA_RECIBIR    = @LID_MONEDA_DER
                    SET @LMONTO_MONEDA_RECIBIR = @PMONTO_DERECHO
                    SET @LID_MONEDA_PAGAR      = @LID_MONEDA_OBL
                    SET @LMONTO_MONEDA_PAGAR   = @PMONTO_OBLIGACION
                    SET @LTIPO_MOV = 'I'
                END
            ELSE IF @PTIPO_FORWARD = 'V'
                BEGIN
                    SET @LID_MONEDA_RECIBIR    = @LID_MONEDA_OBL
                    SET @LMONTO_MONEDA_RECIBIR = @PMONTO_OBLIGACION
                    SET @LID_MONEDA_PAGAR      = @LID_MONEDA_DER
                    SET @LMONTO_MONEDA_PAGAR   = @PMONTO_DERECHO
                    SET @LTIPO_MOV = 'E'
                END
            ELSE
                BEGIN
                    SET @LID_MONEDA_RECIBIR    = 0
                    SET @LMONTO_MONEDA_RECIBIR = 0
                    SET @LID_MONEDA_PAGAR      = 0
                    SET @LMONTO_MONEDA_PAGAR   = 0
                END
        END
---------------------------------------------------------------------------------
    IF @LCODRESULTADO = 'OK' AND @PESTADO_FWD = 'A' -- ANTICIPO
        BEGIN
             SELECT @LID_MOV_DERIVADO = ISNULL(ID_MOV_DERIVADO,0)
               FROM OPERACIONES_DERIVADOS
              WHERE NRO_CONTRATO = @PNUMERO_CONTRATO
                AND COD_ESTADO <> 'A'
                AND COD_TIPO_OPERACION_DERIVADO <> 'VENC_FWD'

             IF @LID_MOV_DERIVADO = 0 or (SELECT COUNT(*) FROM OPERACIONES_DERIVADOS
                                           WHERE NRO_CONTRATO = @PNUMERO_CONTRATO
                                             AND COD_ESTADO <> 'A'
                                             AND COD_TIPO_OPERACION_DERIVADO <> 'VENC_FWD') = 0
                BEGIN
                    SET @LCODRESULTADO = 'ERROR'
                    SET @LMSGRESULTADO = 'ERROR: Anticipo no tiene contrato asociado en GPI'
                END
        END
---------------------------------------------------------------------------------
     SELECT @LID_ORIGEN_CTA = ID_ORIGEN
       FROM ORIGENES
      WHERE COD_ORIGEN = 'MAGIC_VALORES'

     IF ISNULL((SELECT COUNT(1) FROM VIEW_ALIAS WHERE TABLA='CUENTAS' AND COD_ORIGEN='MAGIC_VALORES' AND VALOR = @PCUENTA_EXTERNA),0) <> 0
        BEGIN
             SET @LEXISTE_ALIAS_CUENTA = 'S'

             SELECT @LID_CUENTA = ID_ENTIDAD
               FROM VIEW_ALIAS
              WHERE TABLA='CUENTAS'
                AND COD_ORIGEN='MAGIC_VALORES'
                AND VALOR = @PCUENTA_EXTERNA

             SELECT @LNUM_CUENTA         = NUM_CUENTA
                  , @LID_CLIENTE         = ID_CLIENTE
                  , @LNOMBRE_CLIENTE     = NOMBRE_CLIENTE
                  , @LRUT_CLIENTE        = RUT_CLIENTE
                  , @LID_EMPRESA         = ID_EMPRESA
                  , @LCOD_ESTADO_CUENTA  = COD_ESTADO
                  , @LCOD_ESTADO_CLIENTE = COD_ESTADO_CLIENTE
             FROM VIEW_CUENTAS
             WHERE ID_CUENTA = @LID_CUENTA

             IF @LCOD_ESTADO_CUENTA <> 'H'
                BEGIN
                    SET @LCODRESULTADO = 'ERROR'
                    SET @LMSGRESULTADO = 'ERROR: Cuenta GPI deshabilitada'
                END
             IF @LCOD_ESTADO_CLIENTE = 'A'
                BEGIN
                    SET @LCODRESULTADO = 'ERROR'
                    SET @LMSGRESULTADO = 'ERROR: Cliente de Cuenta anulado en GPI'
                END
         END
     ELSE
         BEGIN
              SET @LID_CUENTA = 0
              SET @LEXISTE_ALIAS_CUENTA = 'N'
              SET @LPOSICION_CARACTER = CharIndex('/',@PCUENTA_EXTERNA,1)
              IF @LPOSICION_CARACTER > 0
                 SET @LRUT_CLIENTE = LEFT(@PCUENTA_EXTERNA,@LPOSICION_CARACTER-1)
              ELSE
                 SET @LRUT_CLIENTE = LEFT(@PCUENTA_EXTERNA,10)
              SET @LCODRESULTADO = 'ERROR'
              SET @LMSGRESULTADO = 'ERROR: Cuenta Contraparte ' + @PCUENTA_EXTERNA + ' no tiene Alias en GPI'
         END
---------------------------------------------------------------------------------

      IF @LCODRESULTADO = 'OK'
         BEGIN
             IF (SELECT FLG_EMPRESA_SEC FROM CONTRAPARTES WHERE RUT_CONTRAPARTE = @PCONTRAPARTE) = 'S'
                BEGIN
                    SET @LID_CONTRAPARTE = (SELECT ID_CONTRAPARTE FROM CONTRAPARTES WHERE RUT_CONTRAPARTE = @PCONTRAPARTE)
                END
             ELSE
                BEGIN
                    SET @LID_CONTRAPARTE = 0
                END
         END

      IF @LCODRESULTADO = 'OK'
         BEGIN
              SELECT EM.DSC_EMPRESA                        'DSC_EMPRESA'
                   , 'FWD'                                 'ORIGEN'
                   , @LDESC_MOV                            'DSC_TIPO_MOV'
                   , @LTIPO_MOV                            'TIPO_MOV'
                   , @LID_CUENTA                           'ID_CUENTA'
                   , CT.NUM_CUENTA                         'NUM_CUENTA'
                   , @LID_CLIENTE                          'ID_CLIENTE'
                   , CL.RUT_CLIENTE                        'RUT_CLIENTE'
                   , @LNOMBRE_CLIENTE                      'NOMBRE_CLIENTE'
                   , @LID_MONEDA_RECIBIR                   'ID_MONEDA_RECIBIR'
                   , @LMONTO_MONEDA_RECIBIR                'MONTO_MONEDA_RECIBIR'
                   , @LID_MONEDA_PAGAR                     'ID_MONEDA_PAGAR'
                   , @LMONTO_MONEDA_PAGAR                  'MONTO_MONEDA_PAGAR'
                   , @LID_MOV_DERIVADO                     'ID_MOV_DERIVADO'
                   , @LEXISTE_ALIAS_CUENTA                 'EXISTE_ALIAS_CUENTA'
                   , @LCODRESULTADO                        'CODRESULTADO'
                   , @LMSGRESULTADO                        'MSGRESULTADO'
                   , @PESTADO_FWD                          'ESTADO'
                   , @LCOD_TIPO_OPERACION                  'COD_TIPO_OPERACION'
                   , @LDSC_TIPO_OPERACION                  'DSC_TIPO_OPERACION'
                   , @PTIPO_ENTREGA_FWD                    'TIPO_ENTREGA_FWD'
                   , @LID_CONTRAPARTE                      'ID_CONTRAPARTE'
                FROM CUENTAS CT  , CLIENTES CL , EMPRESAS EM
               WHERE CT.ID_CUENTA  = @LID_CUENTA
                 AND CL.ID_CLIENTE = CT.ID_CLIENTE
                AND EM.ID_EMPRESA = CT.ID_EMPRESA
         END
      ELSE
         BEGIN
              SELECT ''                                        'DSC_EMPRESA'
                   , 'FWD'                                     'ORIGEN'
                   , CASE @PTIPO_FORWARD WHEN 'C' THEN 'Compra'
                                         WHEN 'V' THEN 'Venta'
                                 ELSE 'N/A'   END              'DSC_TIPO_MOV'
                   , CASE @PESTADO_FWD WHEN 'A' THEN 'E'
                                       WHEN 'V' THEN 'I'
                                       WHEN 'M' THEN 'I'
                                       ELSE ' '  END           'TIPO_MOV'
                   , 0                                         'ID_CUENTA'
                   , ''                                        'NUM_CUENTA'
                   , 0                                         'ID_CLIENTE'
                   , @LRUT_CLIENTE                             'RUT_CLIENTE'
                   , ''                                        'NOMBRE_CLIENTE'
                   , @LID_MONEDA_RECIBIR                       'ID_MONEDA_RECIBIR'
                   , @LMONTO_MONEDA_RECIBIR                    'MONTO_MONEDA_RECIBIR'
                   , @LID_MONEDA_PAGAR                         'ID_MONEDA_PAGAR'
                   , @LMONTO_MONEDA_PAGAR                      'MONTO_MONEDA_PAGAR'
                   , isnull(@LID_MOV_DERIVADO,0)               'ID_MOV_DERIVADO'
                   , @LEXISTE_ALIAS_CUENTA                     'EXISTE_ALIAS_CUENTA'
                   , @LCODRESULTADO                            'CODRESULTADO'
                   , @LMSGRESULTADO                            'MSGRESULTADO'
                   , @PESTADO_FWD                              'ESTADO'
                   , ''                                        'COD_TIPO_OPERACION'
                   , ''                                        'DSC_TIPO_OPERACION'
                   , ''                                        'TIPO_ENTREGA_FWD'
                   , NULL                                      'ID_CONTRAPARTE'
         END
----------------------------------------------------------------
     SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_OPERACIONES_DERIVADOS$TestImportador_FWD] TO DB_EXECUTESP
GO