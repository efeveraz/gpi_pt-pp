USE [CSGPI]
GO

IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_FLUJO_PATRIMONIAL$Buscar_ConCursor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_FLUJO_PATRIMONIAL$Buscar_ConCursor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PKG_FLUJO_PATRIMONIAL$Buscar_ConCursor]
(	@Pid_Cuenta float(53) = null,
	@pfechaIni datetime = null,
	@pfechaFin datetime = null,
	@pId_Moneda_Salida numeric = NULL,
	@pCursor CURSOR VARYING OUTPUT
)
as
declare @salida table(FECHA_MOVIMIENTO	  datetime,
                      FLG_TIPO_MOVIMIENTO varchar(100),
			          ID_MONEDA		      numeric(10),
			          MONTO			      float(53),
			          MONTO_UF		      float(53))
declare @salida2 table(FECHA_MOVIMIENTO	    datetime,
			           FLG_TIPO_MOVIMIENTO	varchar(100),
			           MONTO			    float(53),
			           MONTO_UF		        float(53))
DECLARE @TOTAL_APORTES     numeric(28,6),
	    @TOTAL_APORTES_UF  numeric(28,6),
	    @TOTAL_RESCATES	   numeric(28,6),
	    @TOTAL_RESCATES_UF numeric(28,6)

insert @salida
SELECT APORES.FECHA,
	DBO.Pkg_Global$Dame_Glosa_Tipo_Documento(APORES.TIPO_MOVIMIENTO,'S',DEFAULT) as TIPO_MOVIMIENTO,
	CJ.ID_MONEDA,
	DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta, DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta, APORESDET.MONTO, CJ.ID_MONEDA, dbo.FNT_DAMEIDMONEDA('$$'), ARC.FECHA_MOVIMIENTO), dbo.FNT_DAMEIDMONEDA('$$'), @pId_Moneda_Salida, APORES.FECHA),
	DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta, DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta, APORESDET.MONTO, CJ.ID_MONEDA, dbo.FNT_DAMEIDMONEDA('$$'), ARC.FECHA_MOVIMIENTO), dbo.FNT_DAMEIDMONEDA('$$'), dbo.FNT_DAMEIDMONEDA('UF'), APORES.FECHA)
	FROM APORTES_RETIROS_1862 APORES, 
         APORTE_RESCATE_CUENTA ARC, 
		 CAJAS_CUENTA CJ,
		 CUENTAS CUE,
		 DETALLE_APORTES_RETIROS_1862 APORESDET,
		 MOV_CAJA MC
	WHERE CUE.id_cuenta = @Pid_Cuenta
		  and APORES.FECHA >= @pFechaIni
		  and APORES.FECHA <= @pFechaFin 
		  and CUE.id_cuenta = ARC.ID_CUENTA
		  and ARC.ID_MOV_CAJA = MC.ID_MOV_CAJA 
		  and ARC.ID_APO_RES_CUENTA = APORES.ID_APO_RES_CUENTA
		  and APORES.ID_APO_RES_CUENTA IS NOT NULL 
		  and ARC.ID_CAJA_CUENTA = CJ.ID_CAJA_CUENTA
		  and APORESDET.ID_COMPROBANTE = APORES.ID_COMPROBANTE
		  and APORES.COD_ESTADO <> 'A'
		  and MC.COD_ORIGEN_MOV_CAJA NOT IN ('CARABO_INT','APORES_INT')
ORDER BY APORES.FECHA

insert @salida
SELECT APORES.FECHA,
	DBO.Pkg_Global$Dame_Glosa_Tipo_Documento(APORES.TIPO_MOVIMIENTO,'N',DEFAULT) as TIPO_MOVIMIENTO,
	OPE.ID_MONEDA_OPERACION AS ID_MONEDA,
	DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta, DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta, APORESDET.MONTO, OPE.ID_MONEDA_OPERACION, dbo.FNT_DAMEIDMONEDA('$$'), APORES.FECHA), dbo.FNT_DAMEIDMONEDA('$$'), @pId_Moneda_Salida, APORES.FECHA),
	DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta, DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta, APORESDET.MONTO, OPE.ID_MONEDA_OPERACION, dbo.FNT_DAMEIDMONEDA('$$'), APORES.FECHA), dbo.FNT_DAMEIDMONEDA('$$'), dbo.FNT_DAMEIDMONEDA('UF'), APORES.FECHA)
	FROM APORTES_RETIROS_1862 APORES, 
		 OPERACIONES OPE,
		 CUENTAS CUE,
		 DETALLE_APORTES_RETIROS_1862 APORESDET
	WHERE CUE.id_cuenta = @Pid_Cuenta
		AND APORES.FECHA between @pFechaIni and @pFechaFin 
		AND APORES.ID_OPERACION IS NOT NULL 
		AND OPE.ID_OPERACION = APORES.ID_OPERACION
		AND CUE.id_cuenta = OPE.ID_CUENTA
		and APORESDET.ID_COMPROBANTE = APORES.ID_COMPROBANTE
		AND APORES.COD_ESTADO <> 'A'
ORDER BY APORES.FECHA

INSERT @salida2
SELECT FECHA_MOVIMIENTO,
       FLG_TIPO_MOVIMIENTO,
	   sum(MONTO),
	   sum(MONTO_UF)
from @salida
group by FECHA_MOVIMIENTO, FLG_TIPO_MOVIMIENTO

SELECT @TOTAL_RESCATES = isnull(SUM(r.MONTO),0),
	   @TOTAL_RESCATES_UF = isnull(SUM(r.MONTO_UF),0)
	   from @salida2 r 
	   where flg_tipo_movimiento = 'RETIRO'

SELECT @TOTAL_APORTES = isnull(SUM(r.MONTO),0),
	   @TOTAL_APORTES_UF = isnull(SUM(r.MONTO_UF),0)
	   from @salida2 r 
	   where flg_tipo_movimiento <> 'RETIRO'
	   
SET @pCursor = CURSOR FORWARD_ONLY STATIC FOR
select TOP 9 *,
		@TOTAL_RESCATES AS MONTO_TOTAL_RESCATES,
		@TOTAL_RESCATES_UF AS MONTO_TOTAL_RESCATES_UF,
		@TOTAL_APORTES AS MONTO_TOTAL_APORTES,
		@TOTAL_APORTES_UF AS MONTO_TOTAL_APORTES_UF
from @salida2
order by fecha_movimiento desc

OPEN @pCursor;

GO
GRANT EXECUTE ON [PKG_FLUJO_PATRIMONIAL$Buscar_ConCursor] TO DB_EXECUTESP
GO