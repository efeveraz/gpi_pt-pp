USE CSGPI 
GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_REL_CONVERSIONES$BuscarAliasInversis]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_REL_CONVERSIONES$BuscarAliasInversis]
GO
CREATE PROCEDURE [dbo].[PKG_REL_CONVERSIONES$BuscarAliasInversis]
( @pValor    VARCHAR(50) ,
  @pEntidad  NUMERIC(10) )
AS
BEGIN
     DECLARE @LCOD_ORIGEN         INT,
             @LID_TIPO_CONVERSION INT,
             @LRESULTADO          VARCHAR(15)

     SET @LRESULTADO = 'NO EXISTE'

     SELECT @LCOD_ORIGEN = ID_ORIGEN
       FROM ORIGENES
      WHERE DSC_ORIGEN = 'MAGIC VALORES'

     SELECT @LID_TIPO_CONVERSION = ID_TIPO_CONVERSION
       FROM TIPOs_CONVERSION
      WHERE TABLA = 'CUENTAS'

     If EXISTS (SELECT 1 FROM REL_CONVERSIONES WHERE ID_ORIGEN =  @LCOD_ORIGEN
                                                 AND ((@pEntidad = 0) or (ID_ENTIDAD = @pEntidad))
                                                 AND ID_TIPO_CONVERSION = @LID_TIPO_CONVERSION
                                                 AND VALOR LIKE @pValor + '%')
        SET @LRESULTADO = 'EXISTE'
     SELECT @LRESULTADO as RESULTADO
END
GO
GRANT EXECUTE ON [PKG_REL_CONVERSIONES$BuscarAliasInversis] TO DB_EXECUTESP
GO