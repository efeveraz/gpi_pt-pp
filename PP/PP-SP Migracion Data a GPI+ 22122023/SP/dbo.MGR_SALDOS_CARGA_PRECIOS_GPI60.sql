USE [CSGPI]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MGR_SALDOS_CARGA_PRECIOS_GPI60]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[MGR_SALDOS_CARGA_PRECIOS_GPI60]
GO
CREATE PROCEDURE [dbo].[MGR_SALDOS_CARGA_PRECIOS_GPI60] 
         @PFECHA_CIERRE           DATETIME
     AS
	BEGIN
DECLARE
  @PID_CUENTA              NUMERIC,
  --@PFECHA_CIERRE           DATETIME,
  @PID_MONEDA_SALIDA       NUMERIC--,
  --@PResultado varchar(8000) OUTPUT 

SET @PFECHA_CIERRE     = convert(varchar,@PFECHA_CIERRE,112)--'20221231'

DECLARE @SALIDACUENTA TABLE (            
                              ID_CUENTA  NUMERIC,  
                              ID_CLIENTE NUMERIC,  
                              ID_EMPRESA NUMERIC,  
                              ID_MONEDA  NUMERIC,  
                              NUM_CUENTA VARCHAR(10)
                             )  

INSERT INTO @SALIDACUENTA
SELECT 
        ID_CUENTA,
        ID_CLIENTE,
        ID_EMPRESA,
        ID_MONEDA,
        NUM_CUENTA
   FROM CUENTAS
 -- WHERE --ID_EMPRESA IN (2) --(1, 2, 4)
    --AND COD_ESTADO <> 'D' --and num_Cuenta='0508'

--  SELECT * FROM @SALIDACUENTA

 SET @PID_MONEDA_SALIDA = 1

 DECLARE @SALIDAARBOL TABLE (            
                              ID_ARBOL_CLASE_INST       NUMERIC,  
                              ID_PADRE                  NUMERIC,  
                              ID_EMPRESA                NUMERIC,  
                              DSC_ARBOL_CLASE_INST_RAMA VARCHAR(100),  
                              DSC_ARBOL_CLASE_INST_HOJA VARCHAR(100),  
                              CODIGO                    VARCHAR(20),
                              ID_ACI_TIPO               NUMERIC
                            )  
 
INSERT INTO @SALIDAARBOL
SELECT 
        A.ID_ARBOL_CLASE_INST, 
        A.ID_PADRE_ARBOL_CLASE_INST, 
        A.ID_EMPRESA, 
        A.DSC_ARBOL_CLASE_INST,
        (SELECT AA.DSC_ARBOL_CLASE_INST 
           FROM ARBOL_CLASE_INSTRUMENTO AA
          WHERE AA.ID_ARBOL_CLASE_INST = A.ID_PADRE_ARBOL_CLASE_INST) 'DSC_PADRE_ARBOL_CLASE_INST', 
        A.CODIGO,
        A.ID_ACI_TIPO
   FROM ARBOL_CLASE_INSTRUMENTO A
  WHERE A.ID_ACI_TIPO = 1
    AND NOT A.ID_PADRE_ARBOL_CLASE_INST IS NULL
    AND A.ID_EMPRESA IN (SELECT DISTINCT ID_EMPRESA FROM @SALIDACUENTA)
  ORDER BY ID_EMPRESA, ORDEN
  
----SELECT * FROM @SALIDAARBOL
  
     DECLARE @SALIDA TABLE 
         (CODIGO                            VARCHAR(100),
          PADRE_ARBOL                       VARCHAR(100),
          ARBOL                             VARCHAR(100),
          ID_CUENTA                         NUMERIC,
          ID_SALDO_ACTIVO                   NUMERIC,
          FECHA_CIERRE                      DATETIME,
          ID_NEMOTECNICO                    NUMERIC,
          NEMOTECNICO                       VARCHAR(50),
          EMISOR                            VARCHAR(100),
          COD_EMISOR                        VARCHAR(10),
          DSC_NEMOTECNICO                   VARCHAR(120),
          TASA_EMISION_2                    NUMERIC(18,4),
          CANTIDAD                          NUMERIC(18,4),
          PRECIO                            NUMERIC(18,6),
          TASA_EMISION                      NUMERIC(18,4),
          FECHA_VENCIMIENTO                 DATETIME,
          PRECIO_COMPRA                     NUMERIC(18,4),
          TASA                              NUMERIC(18,4),
          TASA_COMPRA                       NUMERIC(18,4),
          MONTO_VALOR_COMPRA                NUMERIC(18,4),
          MONTO_MON_CTA                     NUMERIC(18,4),
          ID_MONEDA_CTA                     NUMERIC,
          ID_MONEDA_NEMOTECNICO             NUMERIC,
          SIMBOLO_MONEDA                    VARCHAR(3),
          MONTO_MON_NEMOTECNICO             NUMERIC(18,4),
          MONTO_MON_ORIGEN                  NUMERIC(18,4),
          ID_EMPRESA                        NUMERIC,
          ID_ARBOL_CLASE_INST               NUMERIC,
          COD_INSTRUMENTO                   VARCHAR(15),
          DSC_ARBOL_CLASE_INST              VARCHAR(100),
          PORCENTAJE_RAMA                   NUMERIC(18,4),
          PRECIO_PROMEDIO_COMPRA            NUMERIC(18,4),
          DSC_PADRE_ARBOL_CLASE_INST        VARCHAR(100),
          RENTABILIDAD                      FLOAT,
          DIAS                              NUMERIC,
          DURATION                          NUMERIC,
          COD_PRODUCTO                      VARCHAR(10),
          ID_PADRE_ARBOL_CLASE_INST         NUMERIC,
          DURACION                          FLOAT,
          DSC_CLASIFICADOR_RIESGO           VARCHAR(50),
          FECHA_COMPRA                      DATETIME,
          CORTE_MINIMO_PAPEL                NUMERIC(10),
          NROCUPONES                        INT,
          ORIGEN_INT                        VARCHAR(3),
          TOTAL_HOJA                        NUMERIC(18,4),
          CODISIN                           VARCHAR(50),
          ORIGEN                            VARCHAR(50)
         )


       INSERT INTO @SALIDA 
           (CODIGO 
		   , PADRE_ARBOL
           , ARBOL
           , ID_CUENTA
           , ID_SALDO_ACTIVO
           , FECHA_CIERRE
           , ID_NEMOTECNICO
           , NEMOTECNICO
           , EMISOR
           , COD_EMISOR
           , DSC_NEMOTECNICO
           , TASA_EMISION_2
           , CANTIDAD
           , PRECIO
           , TASA_EMISION
           , FECHA_VENCIMIENTO
           , PRECIO_COMPRA
           , TASA
           , TASA_COMPRA
           , MONTO_VALOR_COMPRA
           , MONTO_MON_CTA
           , ID_MONEDA_CTA
           , ID_MONEDA_NEMOTECNICO
           , SIMBOLO_MONEDA
           , MONTO_MON_NEMOTECNICO
           , MONTO_MON_ORIGEN
           , ID_EMPRESA
           , ID_ARBOL_CLASE_INST
           , COD_INSTRUMENTO
           , DSC_ARBOL_CLASE_INST
           , PORCENTAJE_RAMA
           , PRECIO_PROMEDIO_COMPRA
           , DSC_PADRE_ARBOL_CLASE_INST
           , RENTABILIDAD
           , DIAS
           , DURATION
           , COD_PRODUCTO
           , ID_PADRE_ARBOL_CLASE_INST
           , DURACION
           , DSC_CLASIFICADOR_RIESGO
           , FECHA_COMPRA
           , CORTE_MINIMO_PAPEL
           , NROCUPONES
           , CODISIN
           , ORIGEN)
            SELECT 
					SAR.CODIGO
				  , SAR.DSC_ARBOL_CLASE_INST_HOJA
                  , SAR.DSC_ARBOL_CLASE_INST_RAMA
                  , SA.ID_CUENTA
                  , SA.ID_SALDO_ACTIVO
                  , SA.FECHA_CIERRE
                  , SA.ID_NEMOTECNICO
                  , SA.NEMOTECNICO
                  , EE.DSC_EMISOR_ESPECIFICO --EMISOR
                  , EE.COD_SVS_NEMOTECNICO --COD_EMISOR
                  , SA.DSC_NEMOTECNICO
                  , VN.TASA_EMISION
                  , SA.CANTIDAD
                  , SA.PRECIO
                  , VN.TASA_EMISION
                  , VN.FECHA_VENCIMIENTO
                  , CASE
                         WHEN VN.COD_INSTRUMENTO <> 'BONOS_INT' THEN
                             CASE
                                 WHEN @PID_MONEDA_SALIDA = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                                    ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, VN.ID_NEMOTECNICO),0)
                                 ELSE
                                    ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, VN.ID_NEMOTECNICO),0)
                             END
                         WHEN VN.COD_INSTRUMENTO = 'BONOS_INT' THEN
                             SA.TASA_COMPRA
                     END AS PRECIO_COMPRA
                   , SA.TASA
                   , SA.TASA_COMPRA
                   , SA.MONTO_VALOR_COMPRA
                   , CASE
                         WHEN @PID_MONEDA_SALIDA = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                              DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA, SA.MONTO_MON_CTA, SA.ID_MONEDA_CTA, @PID_MONEDA_SALIDA, @PFECHA_CIERRE)
                         ELSE SA.MONTO_MON_CTA
                     END AS MONTO
                   , SA.ID_MONEDA_CTA
                   , SA.ID_MONEDA_NEMOTECNICO
                   , VN.COD_MONEDA
                   , SA.MONTO_MON_NEMOTECNICO
                   , SA.MONTO_MON_ORIGEN
                   , SA.ID_EMPRESA
                   , ACI.ID_ARBOL_CLASE_INST
                   , VN.COD_INSTRUMENTO
                   , SAR.DSC_ARBOL_CLASE_INST_RAMA
                   , null--CASE DBO.PKG_GLOBAL$MONTO_MON_CTA_HOJA(ACI.ID_ARBOL_CLASE_INST, sa.id_cuenta, @PFECHA_CIERRE)
                         --WHEN 0 THEN 0
                         --ELSE monto_mon_cta / ISNULL(DBO.PKG_GLOBAL$MONTO_MON_CTA_HOJA(ACI.ID_ARBOL_CLASE_INST, sa.id_cuenta, @PFECHA_CIERRE),1) * 100
                         --END
                                                              , CASE
                         WHEN @PID_MONEDA_SALIDA = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                             ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)
                         ELSE 
                             ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)
                         END
                                                              , SAR.DSC_ARBOL_CLASE_INST_HOJA
                                                              , CASE @PID_MONEDA_SALIDA
                         WHEN DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                             CASE ID_MONEDA_NEMOTECNICO
                                 WHEN DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                                     CASE ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)*SA.CANTIDAD
                                         WHEN 0 THEN 
                                            NULL
                                         ELSE 
                                            ((SA.MONTO_MON_NEMOTECNICO/(ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE,SA.ID_CUENTA,SA.ID_NEMOTECNICO),1)*SA.CANTIDAD))-1)*100
                                         END
                                 ELSE
                                     CASE ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)*SA.CANTIDAD
                                         WHEN 0 THEN
                                             NULL
                                         ELSE
                                             (((SA.PRECIO * SA.CANTIDAD)/(ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE,SA.ID_CUENTA,SA.ID_NEMOTECNICO),1)*SA.CANTIDAD))-1)*100
                                         END
                                 END
                         ELSE
                             CASE ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)*SA.CANTIDAD
                                 WHEN 0 THEN
                                     NULL
                                 ELSE
                                     (((SA.PRECIO * SA.CANTIDAD)/(ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE,SA.ID_CUENTA,SA.ID_NEMOTECNICO),1)*SA.CANTIDAD))-1)*100
                                 END
                         END
                     , 0
                     , 0
                     , VN.COD_PRODUCTO
                     , SAR.ID_PADRE
                     , (SELECT TOP 1 P.DURACION
                        FROM PUBLICADORES_PRECIOS P
                       WHERE P.ID_NEMOTECNICO = SA.ID_NEMOTECNICO
                         AND P.FECHA <= @PFECHA_CIERRE
                         AND P.DURACION != 0
                       ORDER BY P.FECHA DESC)
                      , (SELECT (SUBSTRING((SELECT DISTINCT(', ' + COD_VALOR_CLASIFICACION)
                                           FROM REL_NEMOTECNICO_VALOR_CLASIFIC RN,
                                                CLASIFICADORES_RIESGO C
                                          WHERE RN.ID_NEMOTECNICO = SA.ID_NEMOTECNICO
                                            AND RN.ID_CLASIFICADOR_RIESGO = C.ID_CLASIFICADOR_RIESGO FOR XML PATH('')),3,100)))
                      , CASE
                         WHEN (VN.COD_PRODUCTO = DBO.PKG_GLOBAL$GCPROD_RF_NAC()) THEN
                             (SELECT CASE 
                                     WHEN (MA.FLG_TIPO_MOVIMIENTO = 'I') THEN MA.FECHA_OPERACION
                                        WHEN (MA.FLG_TIPO_MOVIMIENTO = 'E') THEN
                                            (SELECT MA2.FECHA_OPERACION
                                               FROM MOV_ACTIVOS MA2
                                              WHERE MA2.ID_MOV_ACTIVO  = MA.ID_MOV_ACTIVO_COMPRA 
                                        AND MA2.ID_NEMOTECNICO = MA.ID_NEMOTECNICO)
                                        END
                                FROM MOV_ACTIVOS MA 
                                WHERE MA.ID_MOV_ACTIVO = SA.ID_MOV_ACTIVO)
                         END
                         , CASE
                         WHEN (VN.COD_PRODUCTO = DBO.PKG_GLOBAL$GCPROD_RF_NAC()) THEN VN.CORTE_MINIMO_PAPEL
                         END
                                                              , CASE
                         WHEN VN.COD_INSTRUMENTO = 'BONOS_NAC' AND VN.ID_SUBFAMILIA IN (SELECT ID_SUBFAMILIA FROM SUBFAMILIAS WHERE COD_SUBFAMILIA = 'LH') THEN
                             (SELECT NROCUPONES 
                                                                                                           FROM CS_TB_SERIES 
                                                                                                          WHERE CODSERIE = SUBSTRING(VN.NEMOTECNICO,1,6))
                         WHEN VN.COD_INSTRUMENTO = 'BONOS_NAC' AND NOT VN.ID_SUBFAMILIA IN (SELECT ID_SUBFAMILIA FROM SUBFAMILIAS WHERE COD_SUBFAMILIA IN ('LH', 'BR')) THEN
                             (SELECT NROCUPONES FROM CS_TB_SERIES WHERE CODSERIE = VN.NEMOTECNICO)
                         END
                       , NULL
                       , 'NORMAL'
			FROM VIEW_SALDOS_ACTIVOS SA, 
                 REL_ACI_EMP_NEMOTECNICO ACI,
                 @SALIDAARBOL SAR,
                 VIEW_NEMOTECNICOS VN,
                 EMISORES_ESPECIFICO EE
             WHERE SA.FECHA_CIERRE = @PFECHA_CIERRE
             AND SA.ID_CUENTA IN (SELECT DISTINCT ID_CUENTA FROM @SALIDACUENTA)
             AND SA.ID_NEMOTECNICO = ACI.ID_NEMOTECNICO
             AND SA.ID_EMPRESA = ACI.ID_EMPRESA
             AND ACI.ID_ARBOL_CLASE_INST = SAR.ID_ARBOL_CLASE_INST
             AND SA.ID_NEMOTECNICO = VN.ID_NEMOTECNICO
             AND VN.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
             AND SAR.DSC_ARBOL_CLASE_INST_HOJA IN ('RENTA FIJA NACIONAL', 'RENTA VARIABLE NACIONAL')


       INSERT INTO @SALIDA
           ( CODIGO
		   , PADRE_ARBOL
           , ARBOL
           , ID_CUENTA
           , ID_SALDO_ACTIVO
           , FECHA_CIERRE
           , ID_NEMOTECNICO
           , NEMOTECNICO
           , EMISOR
           , COD_EMISOR
           , DSC_NEMOTECNICO
           , TASA_EMISION_2
           , CANTIDAD
           , PRECIO
           , TASA_EMISION
           , FECHA_VENCIMIENTO
           , PRECIO_COMPRA
           , TASA
           , TASA_COMPRA
           , MONTO_VALOR_COMPRA
           , MONTO_MON_CTA
           , ID_MONEDA_CTA
           , ID_MONEDA_NEMOTECNICO
           , SIMBOLO_MONEDA
           , MONTO_MON_NEMOTECNICO
           , MONTO_MON_ORIGEN
           , ID_EMPRESA
           , ID_ARBOL_CLASE_INST
           , COD_INSTRUMENTO
           , DSC_ARBOL_CLASE_INST
           , PORCENTAJE_RAMA
           , PRECIO_PROMEDIO_COMPRA
           , DSC_PADRE_ARBOL_CLASE_INST
           , RENTABILIDAD
           , DIAS
           , DURATION
           , COD_PRODUCTO
           , ID_PADRE_ARBOL_CLASE_INST
           , DURACION
           , DSC_CLASIFICADOR_RIESGO
           , FECHA_COMPRA
           , CORTE_MINIMO_PAPEL
           , NROCUPONES
           , CODISIN
           , ORIGEN)
            SELECT 
				SAR.CODIGO
				, SAR.DSC_ARBOL_CLASE_INST_HOJA
                , SAR.DSC_ARBOL_CLASE_INST_RAMA
                , SA.ID_CUENTA
                , SA.ID_SALDO_ACTIVO_INT
                , SA.FECHA_CIERRE
                , SA.ID_NEMOTECNICO
                , VN.NEMOTECNICO
                , EE.DSC_EMISOR_ESPECIFICO --EMISOR
                , EE.COD_SVS_NEMOTECNICO --COD_EMISOR
                , VN.DSC_NEMOTECNICO
                , VN.TASA_EMISION
                , SA.CANTIDAD
                ,CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             SA.PRECIO_MERCADO
                         WHEN (SA.ORIGEN = 'PSH') THEN CASE VN.COD_INSTRUMENTO 
								WHEN 'BONOS_INT' THEN (SA.VALOR_MERCADO_MON_USD/sa.cantidad)*100
								WHEN 'RF_INT_INV' THEN (SA.VALOR_MERCADO_MON_USD/sa.cantidad)*100
								WHEN 'RF_INT_PSH' THEN (SA.VALOR_MERCADO_MON_USD/sa.cantidad)*100
                          ELSE SA.PRECIO_MERCADO
							END
                         ELSE 
                            SA.PRECIO_MERCADO
                         END 
                 , ISNULL(VN.TASA_EMISION,0)
                 , VN.FECHA_VENCIMIENTO
                 , CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             SA.PRECIO_PROMEDIO_COMPRA
                         WHEN (SA.ORIGEN = 'PSH') THEN CASE VN.COD_INSTRUMENTO 
								WHEN 'BONOS_INT'  THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
								WHEN 'RF_INT_INV' THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
								WHEN 'RF_INT_PSH' THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
								ELSE SA.PRECIO_PROMEDIO_COMPRA
								END
                         ELSE 
                             SA.PRECIO_PROMEDIO_COMPRA
                  END
                 , CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             NULL
                         WHEN (SA.ORIGEN = 'PSH') THEN CASE VN.COD_INSTRUMENTO 
								WHEN 'BONOS_INT' THEN (SA.VALOR_MERCADO_MON_USD/sa.cantidad)*100
								WHEN 'RF_INT_INV' THEN (SA.VALOR_MERCADO_MON_USD/sa.cantidad)*100
								WHEN 'RF_INT_PSH' THEN (SA.VALOR_MERCADO_MON_USD/sa.cantidad)*100
							ELSE NULL
                         END
                         ELSE 
                         SA.PRECIO_MERCADO
                         END
                 , CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             NULL
                         WHEN (SA.ORIGEN = 'PSH') THEN CASE VN.COD_INSTRUMENTO 
								WHEN 'BONOS_INT'  THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
								WHEN 'RF_INT_INV' THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
								WHEN 'RF_INT_PSH' THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
							ELSE NULL
							END
					END
                 , SA.MONTO_INVERTIDO
				 , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                 , SA.VALOR_MERCADO_MON_USD
                 , 2
                 , @PID_MONEDA_SALIDA
                 , @PFECHA_CIERRE)
                 , VC.ID_MONEDA
                 , 2
                 , CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             (SELECT SIMBOLO FROM MONEDAS M WHERE M.ID_MONEDA = (SELECT ID_ENTIDAD FROM VIEW_ALIAS WHERE COD_ORIGEN = 'INVERSIS' AND TABLA = 'MONEDAS' AND VALOR = SA.MONEDA))
                         WHEN (SA.ORIGEN = 'PSH') THEN
                             (SELECT SIMBOLO FROM MONEDAS M WHERE M.COD_MONEDA = SA.MONEDA)
                         ELSE 
                         SA.MONEDA
                         END
                 , SA.VALOR_MERCADO_MON_USD
                 , SA.VALOR_MERCADO_MON_ORIGEN
                 , VC.ID_EMPRESA
                 , ACI.ID_ARBOL_CLASE_INST
                 , VN.COD_INSTRUMENTO                
                 , SAR.DSC_ARBOL_CLASE_INST_RAMA
                 , NULL
                 , CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             SA.PRECIO_PROMEDIO_COMPRA
                         WHEN (SA.ORIGEN = 'PSH') 
						 THEN CASE VN.COD_INSTRUMENTO 
								WHEN 'BONOS_INT' THEN (SA.VALOR_MERCADO_MON_USD/sa.cantidad)*100
								WHEN 'RF_INT_INV' THEN (SA.VALOR_MERCADO_MON_USD/sa.cantidad)*100
								WHEN 'RF_INT_PSH' THEN (SA.VALOR_MERCADO_MON_USD/sa.cantidad)*100
                         ELSE SA.PRECIO_PROMEDIO_COMPRA
                         END
                         ELSE 
                         SA.PRECIO_PROMEDIO_COMPRA
                         END
                 , SAR.DSC_ARBOL_CLASE_INST_HOJA
                 , CASE ISNULL(SA.PRECIO_PROMEDIO_COMPRA,0) * SA.CANTIDAD
                       WHEN 0 THEN
                           NULL
                       ELSE
                           (((SA.PRECIO_MERCADO * SA.CANTIDAD) / (SA.PRECIO_PROMEDIO_COMPRA * SA.CANTIDAD)) -1) * 100
                       END
                 , 0
                 , 0
                 , VN.COD_PRODUCTO
                 , SAR.ID_PADRE
                 , 0
                 , (SELECT (SUBSTRING((SELECT distinct(', ' + cod_valor_clasificacion)
                                          FROM rel_nemotecnico_valor_clasific rn,
                                               clasificadores_riesgo c
                                         where rn.id_nemotecnico = vn.id_nemotecnico
                                           AND rn.id_clasificador_riesgo = c.id_clasificador_riesgo FOR XML PATH('')),3,100)))
                 , NULL
                 , NULL
                 , NULL
                 , NULL
                 , SA.ORIGEN
					FROM SALDOS_ACTIVOS_INT  SA ,
                         @SALIDACUENTA VC,
                         REL_ACI_EMP_NEMOTECNICO ACI,
                         @SALIDAARBOL SAR,
                         VIEW_NEMOTECNICOS VN,
                         EMISORES_ESPECIFICO EE
               WHERE SA.FECHA_CIERRE = @PFECHA_CIERRE
                 AND SA.ID_CUENTA IN (SELECT DISTINCT ID_CUENTA FROM @SALIDACUENTA)
                 AND SA.ID_CUENTA = VC.ID_CUENTA
                 AND SA.ID_NEMOTECNICO = ACI.ID_NEMOTECNICO
                 AND VC.ID_EMPRESA = ACI.ID_EMPRESA
                 AND ACI.ID_ARBOL_CLASE_INST = SAR.ID_ARBOL_CLASE_INST
                 AND SA.ID_NEMOTECNICO = VN.ID_NEMOTECNICO
                 AND VN.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
                 AND SAR.DSC_ARBOL_CLASE_INST_HOJA IN ('RENTA FIJA INTERNACIONAL', 'RENTA VARIABLE INTERNACIONAL', 'OTROS ACTIVOS')
                 AND NOT SAR.DSC_ARBOL_CLASE_INST_RAMA in ('NOTAS ESTRUCTURADAS', 'FORWARDS')


       INSERT INTO @SALIDA
          ( CODIGO
		  , PADRE_ARBOL
          , ARBOL
          , ID_CUENTA
          , ID_SALDO_ACTIVO
          , FECHA_CIERRE
          , ID_NEMOTECNICO
          , NEMOTECNICO
          , EMISOR
          , COD_EMISOR
          , DSC_NEMOTECNICO
          , TASA_EMISION_2
          , CANTIDAD
          , PRECIO
          , TASA_EMISION
          , FECHA_VENCIMIENTO
          , PRECIO_COMPRA
          , TASA
          , TASA_COMPRA
          , MONTO_VALOR_COMPRA
          , MONTO_MON_CTA
          , ID_MONEDA_CTA
          , ID_MONEDA_NEMOTECNICO
          , SIMBOLO_MONEDA
          , MONTO_MON_NEMOTECNICO
          , MONTO_MON_ORIGEN
          , ID_EMPRESA
          , ID_ARBOL_CLASE_INST
          , COD_INSTRUMENTO
          , DSC_ARBOL_CLASE_INST
          , PORCENTAJE_RAMA
          , PRECIO_PROMEDIO_COMPRA
          , DSC_PADRE_ARBOL_CLASE_INST
          , RENTABILIDAD
          , DIAS
          , DURATION
          , COD_PRODUCTO
          , ID_PADRE_ARBOL_CLASE_INST
          , DURACION
          , DSC_CLASIFICADOR_RIESGO
          , FECHA_COMPRA
          , CORTE_MINIMO_PAPEL
          , NROCUPONES
          , CODISIN
          , ORIGEN)
            SELECT 
				SAR.CODIGO
				, SAR.DSC_ARBOL_CLASE_INST_HOJA
                , SAR.DSC_ARBOL_CLASE_INST_RAMA
                , SA.ID_CUENTA
                , SA.ID_SALDO_ACTIVO
                , SA.FECHA_CIERRE
                , SA.ID_NEMOTECNICO
                , SA.NEMOTECNICO
                , EE.DSC_EMISOR_ESPECIFICO --EMISOR
                , EE.COD_SVS_NEMOTECNICO --COD_EMISOR
                , SA.DSC_NEMOTECNICO
                , VN.TASA_EMISION
                , SA.CANTIDAD
                , SA.PRECIO
                , VN.TASA_EMISION
                , VN.FECHA_VENCIMIENTO
                , CASE
                         WHEN VN.COD_INSTRUMENTO <> 'BONOS_INT' THEN
                             CASE
                                 WHEN @PID_MONEDA_SALIDA = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                                    ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, VN.ID_NEMOTECNICO),0)
                                 ELSE
                                    ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, VN.ID_NEMOTECNICO),0)
                             END
                         WHEN VN.COD_INSTRUMENTO = 'BONOS_INT' THEN
                             SA.TASA_COMPRA
                     END AS PRECIO_COMPRA
                , SA.TASA
                , SA.TASA_COMPRA
                , SA.MONTO_VALOR_COMPRA
                , CASE
                         WHEN @PID_MONEDA_SALIDA = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                              DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA, SA.MONTO_MON_CTA, SA.ID_MONEDA_CTA, @PID_MONEDA_SALIDA, @PFECHA_CIERRE)
                         ELSE SA.MONTO_MON_CTA
                     END AS MONTO
                , SA.ID_MONEDA_CTA
                , SA.ID_MONEDA_NEMOTECNICO
                , VN.COD_MONEDA
                , SA.MONTO_MON_NEMOTECNICO
                , SA.MONTO_MON_ORIGEN
                , SA.ID_EMPRESA
                , ACI.ID_ARBOL_CLASE_INST
                , VN.COD_INSTRUMENTO
                , SAR.DSC_ARBOL_CLASE_INST_RAMA
                , NULL--CASE DBO.PKG_GLOBAL$MONTO_MON_CTA_HOJA(ACI.ID_ARBOL_CLASE_INST, sa.id_cuenta, @PFECHA_CIERRE)
                      --WHEN 0 THEN 0
                      --ELSE monto_mon_cta / ISNULL(DBO.PKG_GLOBAL$MONTO_MON_CTA_HOJA(ACI.ID_ARBOL_CLASE_INST, sa.id_cuenta, @PFECHA_CIERRE),1) * 100
                      --END
                , CASE
                         WHEN @PID_MONEDA_SALIDA = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                             ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)
                         ELSE 
                             ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)
                         END
                , SAR.DSC_ARBOL_CLASE_INST_HOJA
                , CASE @PID_MONEDA_SALIDA
                         WHEN DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                             CASE ID_MONEDA_NEMOTECNICO
                                 WHEN DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                                     CASE ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)*SA.CANTIDAD
                                         WHEN 0 THEN 
                                            NULL
                                         ELSE 
                                            ((SA.MONTO_MON_NEMOTECNICO/(ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE,SA.ID_CUENTA,SA.ID_NEMOTECNICO),1)*SA.CANTIDAD))-1)*100
                                         END
                                 ELSE
                                     CASE ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)*SA.CANTIDAD
                                         WHEN 0 THEN
                                             NULL
                                         ELSE
                                             (((SA.PRECIO * SA.CANTIDAD)/(ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE,SA.ID_CUENTA,SA.ID_NEMOTECNICO),1)*SA.CANTIDAD))-1)*100
                                         END
                                 END
                         ELSE
                             CASE ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)*SA.CANTIDAD
                                 WHEN 0 THEN
                                     NULL
                                 ELSE
                                     (((SA.PRECIO * SA.CANTIDAD)/(ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE,SA.ID_CUENTA,SA.ID_NEMOTECNICO),1)*SA.CANTIDAD))-1)*100
                                 END
                         END
                 , 0
                 , 0
                 , VN.COD_PRODUCTO
                 , SAR.ID_PADRE
                 , (SELECT TOP 1 P.DURACION
                        FROM PUBLICADORES_PRECIOS P
                       WHERE P.ID_NEMOTECNICO = SA.ID_NEMOTECNICO
                         AND P.FECHA <= @PFECHA_CIERRE
                         AND P.DURACION != 0
                       ORDER BY P.FECHA DESC)
                                          , (SELECT (SUBSTRING((SELECT DISTINCT(', ' + COD_VALOR_CLASIFICACION)
                                           FROM REL_NEMOTECNICO_VALOR_CLASIFIC RN,
                                                CLASIFICADORES_RIESGO C
                                          WHERE RN.ID_NEMOTECNICO = SA.ID_NEMOTECNICO
                                            AND RN.ID_CLASIFICADOR_RIESGO = C.ID_CLASIFICADOR_RIESGO FOR XML PATH('')),3,100)))
                                                              , CASE
                         WHEN (VN.COD_PRODUCTO = DBO.PKG_GLOBAL$GCPROD_RF_NAC()) THEN
                             (SELECT    
                                    CASE 
                                    WHEN (MA.FLG_TIPO_MOVIMIENTO = 'I') THEN MA.FECHA_OPERACION
                                        WHEN (MA.FLG_TIPO_MOVIMIENTO = 'E') THEN
                                            (SELECT MA2.FECHA_OPERACION
                                               FROM MOV_ACTIVOS MA2
                                              WHERE MA2.ID_MOV_ACTIVO  = MA.ID_MOV_ACTIVO_COMPRA 
                                    AND MA2.ID_NEMOTECNICO = MA.ID_NEMOTECNICO)
                                        END
                                FROM MOV_ACTIVOS MA 
                                    WHERE MA.ID_MOV_ACTIVO = SA.ID_MOV_ACTIVO)
                         END
                         , CASE
                         WHEN (VN.COD_PRODUCTO = DBO.PKG_GLOBAL$GCPROD_RF_NAC()) THEN VN.CORTE_MINIMO_PAPEL
                         END
                         , CASE
                         WHEN VN.COD_INSTRUMENTO = 'BONOS_NAC' AND VN.ID_SUBFAMILIA IN (SELECT ID_SUBFAMILIA FROM SUBFAMILIAS WHERE COD_SUBFAMILIA = 'LH') THEN
                             (SELECT NROCUPONES 
                         FROM CS_TB_SERIES 
                         WHERE CODSERIE = SUBSTRING(VN.NEMOTECNICO,1,6))
                         WHEN VN.COD_INSTRUMENTO = 'BONOS_NAC' AND NOT VN.ID_SUBFAMILIA IN (SELECT ID_SUBFAMILIA FROM SUBFAMILIAS WHERE COD_SUBFAMILIA IN ('LH', 'BR')) THEN
                             (SELECT NROCUPONES FROM CS_TB_SERIES WHERE CODSERIE = VN.NEMOTECNICO)
                         END
                , NULL
                , 'NORMAL'
              FROM VIEW_SALDOS_ACTIVOS SA, 
                   REL_ACI_EMP_NEMOTECNICO ACI,
                   @SALIDAARBOL SAR,
                   VIEW_NEMOTECNICOS VN,
                   EMISORES_ESPECIFICO EE
             WHERE SA.FECHA_CIERRE = @PFECHA_CIERRE
               AND SA.ID_CUENTA IN (SELECT DISTINCT ID_CUENTA FROM @SALIDACUENTA)
               AND SA.ID_NEMOTECNICO = ACI.ID_NEMOTECNICO
               AND SA.ID_EMPRESA = ACI.ID_EMPRESA
               AND ACI.ID_ARBOL_CLASE_INST = SAR.ID_ARBOL_CLASE_INST
               AND SA.ID_NEMOTECNICO = VN.ID_NEMOTECNICO
               AND VN.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
               AND SAR.DSC_ARBOL_CLASE_INST_HOJA IN ('RENTA FIJA INTERNACIONAL', 'RENTA VARIABLE INTERNACIONAL', 'OTROS ACTIVOS')
               AND NOT SAR.DSC_ARBOL_CLASE_INST_RAMA in ('NOTAS ESTRUCTURADAS', 'FORWARDS')


       INSERT INTO @SALIDA
           ( CODIGO
		   , PADRE_ARBOL
           , ARBOL
           , ID_CUENTA
           , ID_SALDO_ACTIVO
           , FECHA_CIERRE
           , ID_NEMOTECNICO
           , NEMOTECNICO
           , EMISOR
           , COD_EMISOR
           , DSC_NEMOTECNICO
           , TASA_EMISION_2
           , CANTIDAD
           , PRECIO
           , TASA_EMISION
           , FECHA_VENCIMIENTO
           , PRECIO_COMPRA
           , TASA
           , TASA_COMPRA
           , MONTO_VALOR_COMPRA
           , MONTO_MON_CTA
           , ID_MONEDA_CTA
           , ID_MONEDA_NEMOTECNICO
           , SIMBOLO_MONEDA
           , MONTO_MON_NEMOTECNICO
           , MONTO_MON_ORIGEN
           , ID_EMPRESA
           , ID_ARBOL_CLASE_INST
           , COD_INSTRUMENTO
           , DSC_ARBOL_CLASE_INST
           , PORCENTAJE_RAMA
           , PRECIO_PROMEDIO_COMPRA
           , DSC_PADRE_ARBOL_CLASE_INST
           , RENTABILIDAD
           , DIAS
           , DURATION
           , COD_PRODUCTO
           , ID_PADRE_ARBOL_CLASE_INST
           , DURACION
           , DSC_CLASIFICADOR_RIESGO
           , FECHA_COMPRA
           , CORTE_MINIMO_PAPEL
           , NROCUPONES
           , CODISIN
           , ORIGEN)
            SELECT 
			SAR.CODIGO,
                     SAR.DSC_ARBOL_CLASE_INST_HOJA
                   , SAR.DSC_ARBOL_CLASE_INST_RAMA
                   , SD.ID_CUENTA
                   , 0
                   , SD.FECHA_CIERRE
                   , 0
                   , CASE
						WHEN FLG_TIPO_MOVIMIENTO ='I' THEN 'FWDV'+ NRO_CONTRATO + '-' + CONVERT(VARCHAR(10), FCH_VENCIMIENTO, 112) 
						WHEN FLG_TIPO_MOVIMIENTO ='E' THEN 'FWDC'+ NRO_CONTRATO + '-' + CONVERT(VARCHAR(10), FCH_VENCIMIENTO, 112) END 'NEMOTECNICO'
                   , ''
                   , ''
                   , ''
                   , 0
                   , ROUND((CASE WHEN NOT SD.COD_MONEDA_RECIBIR = '$$' THEN SD.MONTO_MON_RECIBIR ELSE SD.MONTO_MON_PAGAR END),1)
                   , ISNULL(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SD.ID_CUENTA
                                                                 , 1--SD.VMM
                                                                 , 2--DBO.FNT_DAMEIDMONEDA('$$')
                                                                 , @PID_MONEDA_SALIDA
                                                                 , @PFECHA_CIERRE) ,0)
                   , SD.NOCIONALES
                    , SD.FCH_VENCIMIENTO
                   , 0
                   , NULL
                   , NULL
                   , 0
                   , SD.VMM
                   , ID_MONEDA_CUENTA
                   , 2
                   , (CASE WHEN SD.COD_MONEDA_PAGAR = '$$' THEN 'PESOS'  ELSE SD.COD_MONEDA_PAGAR END)
                   , SD.VMM
                   , 0
                   , SD.ID_EMPRESA
                   , SAR.ID_ARBOL_CLASE_INST
                   , SD.COD_INSTRUMENTO
                   , SAR.DSC_ARBOL_CLASE_INST_RAMA
                   , NULL
                   , 0
                   , SAR.DSC_ARBOL_CLASE_INST_HOJA
                   , 0
                   , 0
                   , 0
                   , 'DERI_NAC'
                   , SAR.ID_PADRE
                   , 0
                   , '' as dsc_clasificador_riesgo
                   , NULL
                   , NULL
                   , NULL
                   , NULL
                   , 'DERIVADOS'
              FROM VIEW_SALDOS_DERIVADOS SD,
                   @SALIDACUENTA VC,
                   @SALIDAARBOL SAR
             WHERE SD.FECHA_CIERRE = @PFECHA_CIERRE
               AND SD.ID_CUENTA IN (SELECT DISTINCT ID_CUENTA FROM @SALIDACUENTA)
               AND SD.ID_CUENTA = VC.ID_CUENTA
               AND SD.COD_INSTRUMENTO = 'FWD_NAC'
               AND SD.ID_EMPRESA = SAR.ID_EMPRESA
               AND SAR.CODIGO = 'FWD'
              ORDER BY SD.FCH_OPERACION


       INSERT INTO @SALIDA
           (CODIGO
		   , PADRE_ARBOL
           , ARBOL
           , ID_CUENTA
           , ID_SALDO_ACTIVO
           , FECHA_CIERRE
           , ID_NEMOTECNICO
           , NEMOTECNICO
           , EMISOR
           , COD_EMISOR
           , DSC_NEMOTECNICO
           , TASA_EMISION_2
           , CANTIDAD
           , PRECIO
           , TASA_EMISION
           , FECHA_VENCIMIENTO
           , PRECIO_COMPRA
           , TASA
           , TASA_COMPRA
           , MONTO_VALOR_COMPRA
           , MONTO_MON_CTA
           , ID_MONEDA_CTA
           , ID_MONEDA_NEMOTECNICO
           , SIMBOLO_MONEDA
           , MONTO_MON_NEMOTECNICO
           , MONTO_MON_ORIGEN
           , ID_EMPRESA
           , ID_ARBOL_CLASE_INST
           , COD_INSTRUMENTO
           , DSC_ARBOL_CLASE_INST
           , PORCENTAJE_RAMA
           , PRECIO_PROMEDIO_COMPRA
           , DSC_PADRE_ARBOL_CLASE_INST
           , RENTABILIDAD
           , DIAS
           , DURATION
           , COD_PRODUCTO
           , ID_PADRE_ARBOL_CLASE_INST
           , DURACION
           , DSC_CLASIFICADOR_RIESGO
           , FECHA_COMPRA
           , CORTE_MINIMO_PAPEL
           , NROCUPONES
           , CODISIN
           , ORIGEN)
            SELECT 
			SAR.CODIGO,
                     SAR.DSC_ARBOL_CLASE_INST_HOJA
                   , SAR.DSC_ARBOL_CLASE_INST_RAMA
                   , SA.ID_CUENTA
                   , SA.ID_SALDO_ACTIVO
                   , SA.FECHA_CIERRE
                   , SA.ID_NEMOTECNICO
                   , SA.NEMOTECNICO
                   , EE.DSC_EMISOR_ESPECIFICO --EMISOR
                   , EE.COD_SVS_NEMOTECNICO --COD_EMISOR
                   , SA.DSC_NEMOTECNICO
                   , VN.TASA_EMISION
                   , SA.CANTIDAD
                   , SA.PRECIO
                   , VN.TASA_EMISION
                   , VN.FECHA_VENCIMIENTO
                   , CASE
                         WHEN VN.COD_INSTRUMENTO <> 'BONOS_INT' THEN
                             CASE
                                 WHEN @PID_MONEDA_SALIDA = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                                    ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, VN.ID_NEMOTECNICO),0)
                                 ELSE
                                    ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, VN.ID_NEMOTECNICO),0)
                             END
                         WHEN VN.COD_INSTRUMENTO = 'BONOS_INT' THEN
                             SA.TASA_COMPRA
                     END AS PRECIO_COMPRA
                   , SA.TASA
                   , SA.TASA_COMPRA
                   , SA.MONTO_VALOR_COMPRA
                   , CASE
                         WHEN @PID_MONEDA_SALIDA = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                              DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA, SA.MONTO_MON_CTA, SA.ID_MONEDA_CTA, @PID_MONEDA_SALIDA, @PFECHA_CIERRE)
                         ELSE SA.MONTO_MON_CTA
						END AS MONTO
                   , SA.ID_MONEDA_CTA
                   , SA.ID_MONEDA_NEMOTECNICO
                   , VN.COD_MONEDA
                   , SA.MONTO_MON_NEMOTECNICO
                   , SA.MONTO_MON_ORIGEN
                   , SA.ID_EMPRESA
                   , ACI.ID_ARBOL_CLASE_INST
                   , VN.COD_INSTRUMENTO
                   , SAR.DSC_ARBOL_CLASE_INST_RAMA
                   , CASE DBO.PKG_GLOBAL$MONTO_MON_CTA_HOJA(ACI.ID_ARBOL_CLASE_INST, sa.id_cuenta, @PFECHA_CIERRE)
                        WHEN 0 THEN 0
                         ELSE monto_mon_cta / ISNULL(DBO.PKG_GLOBAL$MONTO_MON_CTA_HOJA(ACI.ID_ARBOL_CLASE_INST, sa.id_cuenta, @PFECHA_CIERRE),1) * 100
                         END
                   , CASE
                         WHEN @PID_MONEDA_SALIDA = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                             ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)
                         ELSE 
                             ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)
                         END
                   , SAR.DSC_ARBOL_CLASE_INST_HOJA
                   , CASE @PID_MONEDA_SALIDA
                         WHEN DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                             CASE ID_MONEDA_NEMOTECNICO
                                 WHEN DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) THEN
                                     CASE ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)*SA.CANTIDAD
                                         WHEN 0 THEN 
                                            NULL
                                         ELSE 
                                            ((SA.MONTO_MON_NEMOTECNICO/(ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE,SA.ID_CUENTA,SA.ID_NEMOTECNICO),1)*SA.CANTIDAD))-1)*100
                                         END
                                 ELSE
                                     CASE ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)*SA.CANTIDAD
                                         WHEN 0 THEN
                                             NULL
                                         ELSE
                                             (((SA.PRECIO * SA.CANTIDAD)/(ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE,SA.ID_CUENTA,SA.ID_NEMOTECNICO),1)*SA.CANTIDAD))-1)*100
                                         END
                                 END
                         ELSE
                             CASE ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)*SA.CANTIDAD
                                 WHEN 0 THEN
                                     NULL
                                 ELSE
                                     (((SA.PRECIO * SA.CANTIDAD)/(ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO_MONEDA_CUENTA(@PFECHA_CIERRE,SA.ID_CUENTA,SA.ID_NEMOTECNICO),1)*SA.CANTIDAD))-1)*100
                                 END
                         END
                   , 0
                   , 0
                   , VN.COD_PRODUCTO
                   , SAR.ID_PADRE
                   , (SELECT TOP 1 P.DURACION
                        FROM PUBLICADORES_PRECIOS P
                       WHERE P.ID_NEMOTECNICO = SA.ID_NEMOTECNICO
                         AND P.FECHA <= @PFECHA_CIERRE
                         AND P.DURACION != 0
                       ORDER BY P.FECHA DESC)
                        , (SELECT (SUBSTRING((SELECT DISTINCT(', ' + COD_VALOR_CLASIFICACION)
                           FROM REL_NEMOTECNICO_VALOR_CLASIFIC RN,
                                CLASIFICADORES_RIESGO C
                          WHERE RN.ID_NEMOTECNICO = SA.ID_NEMOTECNICO
                            AND RN.ID_CLASIFICADOR_RIESGO = C.ID_CLASIFICADOR_RIESGO FOR XML PATH('')),3,100)))
                        , CASE
                         WHEN (VN.COD_PRODUCTO = DBO.PKG_GLOBAL$GCPROD_RF_NAC()) THEN
                             (SELECT    
                             CASE 
                             WHEN (MA.FLG_TIPO_MOVIMIENTO = 'I') THEN MA.FECHA_OPERACION
                                        WHEN (MA.FLG_TIPO_MOVIMIENTO = 'E') THEN
                                            (SELECT MA2.FECHA_OPERACION
                                               FROM MOV_ACTIVOS MA2
                                              WHERE MA2.ID_MOV_ACTIVO  = MA.ID_MOV_ACTIVO_COMPRA 
                                              AND MA2.ID_NEMOTECNICO = MA.ID_NEMOTECNICO)
                                        END
                                FROM MOV_ACTIVOS MA 
                                WHERE MA.ID_MOV_ACTIVO = SA.ID_MOV_ACTIVO)
                         END
                         , CASE
                         WHEN (VN.COD_PRODUCTO = DBO.PKG_GLOBAL$GCPROD_RF_NAC()) THEN VN.CORTE_MINIMO_PAPEL
                         END
                         , CASE
                         WHEN VN.COD_INSTRUMENTO = 'BONOS_NAC' AND VN.ID_SUBFAMILIA IN (SELECT ID_SUBFAMILIA FROM SUBFAMILIAS WHERE COD_SUBFAMILIA = 'LH') THEN
                             (SELECT NROCUPONES 
						 FROM CS_TB_SERIES 
						WHERE CODSERIE = SUBSTRING(VN.NEMOTECNICO,1,6))
                         WHEN VN.COD_INSTRUMENTO = 'BONOS_NAC' AND NOT VN.ID_SUBFAMILIA IN (SELECT ID_SUBFAMILIA FROM SUBFAMILIAS WHERE COD_SUBFAMILIA IN ('LH', 'BR')) THEN
                             (SELECT NROCUPONES FROM CS_TB_SERIES WHERE CODSERIE = VN.NEMOTECNICO)
                         END
                         , NULL
                         , 'NORMAL'
              FROM VIEW_SALDOS_ACTIVOS SA, 
                   REL_ACI_EMP_NEMOTECNICO ACI,
                   @SALIDAARBOL SAR,
                   VIEW_NEMOTECNICOS VN,
                   EMISORES_ESPECIFICO EE
             WHERE SA.FECHA_CIERRE = @PFECHA_CIERRE
               AND SA.ID_CUENTA IN (SELECT DISTINCT ID_CUENTA FROM @SALIDACUENTA)
               AND SA.ID_NEMOTECNICO = ACI.ID_NEMOTECNICO
               AND SA.ID_EMPRESA = ACI.ID_EMPRESA
               AND ACI.ID_ARBOL_CLASE_INST = SAR.ID_ARBOL_CLASE_INST
               AND SA.ID_NEMOTECNICO = VN.ID_NEMOTECNICO
               AND VN.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
               AND SAR.DSC_ARBOL_CLASE_INST_HOJA IN ('OTROS ACTIVOS')
               AND SAR.DSC_ARBOL_CLASE_INST_RAMA in ('NOTAS ESTRUCTURADAS')


       INSERT INTO @SALIDA
           ( CODIGO
		   , PADRE_ARBOL
           , ARBOL
           , ID_CUENTA
           , ID_SALDO_ACTIVO
           , FECHA_CIERRE
           , ID_NEMOTECNICO
           , NEMOTECNICO
           , EMISOR
           , COD_EMISOR
           , DSC_NEMOTECNICO
           , TASA_EMISION_2
           , CANTIDAD
           , PRECIO
           , TASA_EMISION
           , FECHA_VENCIMIENTO
           , PRECIO_COMPRA
           , TASA
           , TASA_COMPRA
           , MONTO_VALOR_COMPRA
           , MONTO_MON_CTA
           , ID_MONEDA_CTA
           , ID_MONEDA_NEMOTECNICO
           , SIMBOLO_MONEDA
           , MONTO_MON_NEMOTECNICO
           , MONTO_MON_ORIGEN
           , ID_EMPRESA
           , ID_ARBOL_CLASE_INST
           , COD_INSTRUMENTO
           , DSC_ARBOL_CLASE_INST
           , PORCENTAJE_RAMA
           , PRECIO_PROMEDIO_COMPRA
           , DSC_PADRE_ARBOL_CLASE_INST
           , RENTABILIDAD
           , DIAS
           , DURATION
           , COD_PRODUCTO
           , ID_PADRE_ARBOL_CLASE_INST
           , DURACION
           , DSC_CLASIFICADOR_RIESGO
           , FECHA_COMPRA
           , CORTE_MINIMO_PAPEL
           , NROCUPONES
           , CODISIN
           , ORIGEN)
            SELECT 
					SAR.CODIGO
				   , SAR.DSC_ARBOL_CLASE_INST_HOJA
                   , SAR.DSC_ARBOL_CLASE_INST_RAMA
                   , SA.ID_CUENTA
                   , SA.ID_SALDO_ACTIVO_INT
                   , SA.FECHA_CIERRE
                   , SA.ID_NEMOTECNICO
                   , VN.NEMOTECNICO
                   , EE.DSC_EMISOR_ESPECIFICO --EMISOR
                   , EE.COD_SVS_NEMOTECNICO --COD_EMISOR
                   , VN.DSC_NEMOTECNICO
                   , VN.TASA_EMISION
                   , SA.CANTIDAD
                   , CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             SA.PRECIO_MERCADO
                         WHEN (SA.ORIGEN = 'PSH') THEN
                            (SA.VALOR_MERCADO_MON_USD/sa.cantidad)*100
                         ELSE 
                         SA.PRECIO_MERCADO
                         END 
                         , ISNULL(VN.TASA_EMISION,0)
                         , VN.FECHA_VENCIMIENTO
                         , CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             SA.PRECIO_PROMEDIO_COMPRA
                          WHEN (SA.ORIGEN = 'PSH') THEN CASE VN.COD_INSTRUMENTO 
								WHEN 'BONOS_INT'  THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
								WHEN 'RF_INT_INV' THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
								WHEN 'RF_INT_PSH' THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
								ELSE SA.PRECIO_PROMEDIO_COMPRA
								END
                         ELSE 
                             SA.PRECIO_PROMEDIO_COMPRA
						END
                         , CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             NULL
                         WHEN (SA.ORIGEN = 'PSH') THEN
                            (SA.VALOR_MERCADO_MON_USD/sa.cantidad)*100
                         ELSE 
                         NULL
                         END 
                         , CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             NULL
                         WHEN (SA.ORIGEN = 'PSH') THEN CASE VN.COD_INSTRUMENTO 
								WHEN 'BONOS_INT'  THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
								WHEN 'RF_INT_INV' THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
								WHEN 'RF_INT_PSH' THEN (SA.MONTO_INVERTIDO/sa.cantidad)*100
							ELSE NULL
							END
					END
                    , SA.MONTO_INVERTIDO
                    , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
					, SA.VALOR_MERCADO_MON_USD
					, 2
					, @PID_MONEDA_SALIDA
					, @PFECHA_CIERRE)
					, VC.ID_MONEDA
					, 2
                    , CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             (SELECT SIMBOLO FROM MONEDAS M 
							 WHERE M.ID_MONEDA = (SELECT ID_ENTIDAD FROM VIEW_ALIAS WHERE COD_ORIGEN = 'INVERSIS' AND TABLA = 'MONEDAS' AND VALOR = SA.MONEDA))
                         WHEN (SA.ORIGEN = 'PSH') THEN
                             (SELECT SIMBOLO FROM MONEDAS M WHERE M.COD_MONEDA = SA.MONEDA)
                         ELSE 
                             SA.MONEDA
                         END
                   , SA.VALOR_MERCADO_MON_USD
                   , SA.VALOR_MERCADO_MON_ORIGEN
                   , VC.ID_EMPRESA
                   , ACI.ID_ARBOL_CLASE_INST
                   , VN.COD_INSTRUMENTO                
                   , SAR.DSC_ARBOL_CLASE_INST_RAMA
                   , NULL
                   , CASE
                         WHEN (SA.ORIGEN = 'INV') THEN
                             SA.PRECIO_PROMEDIO_COMPRA
                         WHEN (SA.ORIGEN = 'PSH') THEN
                            (SA.MONTO_INVERTIDO/sa.cantidad)*100
                         ELSE 
                             SA.PRECIO_PROMEDIO_COMPRA
                         END
                   , SAR.DSC_ARBOL_CLASE_INST_HOJA
                   , CASE ISNULL(SA.PRECIO_PROMEDIO_COMPRA,0) * SA.CANTIDAD
                         WHEN 0 THEN
                             NULL
                         ELSE
                             (((SA.PRECIO_MERCADO * SA.CANTIDAD) / (SA.PRECIO_PROMEDIO_COMPRA * SA.CANTIDAD)) -1) * 100
                         END
                   , 0
                   , 0
                   , VN.COD_PRODUCTO
                   , SAR.ID_PADRE
                   , 0
                   , (SELECT (SUBSTRING((SELECT distinct(', ' + cod_valor_clasificacion)
                                          FROM rel_nemotecnico_valor_clasific rn,
                                               clasificadores_riesgo c
                                         where rn.id_nemotecnico = vn.id_nemotecnico
                                           AND rn.id_clasificador_riesgo = c.id_clasificador_riesgo FOR XML PATH('')),3,100)))
                   , NULL
                   , NULL
                   , NULL
                   , NULL
                   , SA.ORIGEN
                FROM SALDOS_ACTIVOS_INT  SA ,
					@SALIDACUENTA VC,
                    REL_ACI_EMP_NEMOTECNICO ACI,
                    @SALIDAARBOL SAR,
                    VIEW_NEMOTECNICOS VN,
                    EMISORES_ESPECIFICO EE
               WHERE SA.FECHA_CIERRE = @PFECHA_CIERRE
                 AND SA.ID_CUENTA IN (SELECT DISTINCT ID_CUENTA FROM @SALIDACUENTA)
                 AND SA.ID_CUENTA = VC.ID_CUENTA
                 AND SA.ID_NEMOTECNICO = ACI.ID_NEMOTECNICO
                 AND VC.ID_EMPRESA = ACI.ID_EMPRESA
                 AND ACI.ID_ARBOL_CLASE_INST = SAR.ID_ARBOL_CLASE_INST
                 AND SA.ID_NEMOTECNICO = VN.ID_NEMOTECNICO
                 AND VN.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
                 AND SAR.DSC_ARBOL_CLASE_INST_HOJA IN ('OTROS ACTIVOS')
                 AND SAR.DSC_ARBOL_CLASE_INST_RAMA in ('NOTAS ESTRUCTURADAS')

                              update @salida set precio = precio * 100 where nemotecnico = 'CALL 100 ISHARES INC OPTION ROOT= EWZ'

                             SELECT convert(varchar,@PFECHA_CIERRE,23) 'fecha',
							  c.num_cuenta 'cuenta',
							  --nemotecnico,
							  case 
							  --when origen='PSH' OR origen='INV' then ISNULL(rna.isim,rna.cusip) + ' - ' + nemotecnico
							  when origen='PSH' AND rna.isim IS NOT NULL THEN rna.isim + ' - ' + nemotecnico
							  when origen='INV' AND rna.cusip IS NOT NULL THEN rna.cusip + ' - ' + nemotecnico
							  else nemotecnico
							  end  'nemotecnico',
							  cantidad,
						      case CODIGO when 'RF' then tasa 
                               --when 'Bonos Empresas' then tasa
                               --when 'Depósitos a Plazo' then tasa
                               --when 'Bonos' then tasa
                              else precio end as precio,
							  MONTO_MON_NEMOTECNICO 'monto',
							  'MIGRACION' 'contraparte',
							--s.precio_compra,
							case CODIGO when 'RF' then tasa_compra 
                               --when 'Bonos Empresas' then tasa
                               --when 'Depósitos a Plazo' then tasa
                               --when 'Bonos' then tasa
                              else s.precio_compra end as precio_tasa_compra,
							s.MONTO_VALOR_COMPRA 'monto_compra',
							0 'INTERES_ACUMULADO',
							CAST(ROUND((MONTO_MON_NEMOTECNICO / cantidad), 6) AS DECIMAL(20,6)) 'FACTOR',
							0 'YIELD_TO_MATURITY',
							s.id_empresa 'id_negocio',
							 case 
							  when origen='PSH' then 'PERSHING'
							  when origen='INV' then 'INVERSIS'
							  when origen='NORMAL' then (SELECT [dbo].[FN_RETORNA_PUBLICADOR](S.ID_NEMOTECNICO,S.ID_CUENTA))
							  else origen
							  end  'publicador', s.id_nemotecnico, rna.cusip,rna.isim,s.duracion

                              FROM @SALIDA S 
                              inner join cuentas c on c.id_cuenta = s.id_cuenta
                              left JOIN rel_cuenta_instrumento_publica rel on rel.id_cuenta = c.id_cuenta and s.cod_instrumento = rel.cod_instrumento
							  left join REL_NEMOTECNICO_ATRIBUTOS rna on rna.id_nemotecnico = s.id_nemotecnico
                              order by ORIGEN,S.cod_instrumento

SELECT 
	S.ID_SIMULTANEA,
	S.ID_CUENTA,
	S.FCH_SIMULTANEA,
	S.FCH_LIQUIDACION,
	S.FCH_VENCIMIENTO,
	S.FLG_TIPO_OPERACION,
	S.ID_NEMOTECNICO,
	S.ID_TIPO_ESTADO,
	S.COD_ESTADO,
	S.ID_MONEDA,
	S.CANTIDAD,
	S.PRECIO,
	S.MONTO,
	S.PRECIO_FUTURO,
	S.TASA,
	S.BASE,
	S.MONTO_FUTURO,
	S.FOLIO,
	S.ID_OPERACION_VENTA,
	S.ID_OPERACION_COMPRA,
	S.ID_CARGO_ABONO_COMPRA,
	S.ID_CARGO_ABONO_VENTA,
	S.FECHA_CIERRE,
	S.ID_USUARIO_INSERT,
	S.FCH_INSERT,
	S.ID_USUARIO_UPDATE,
	S.FCH_UPDATE,
	S.BOLSA,
	SS.ID_CUENTA,
	SS.FECHA_CIERRE,
	SS.ID_SIMULTANEA, 
	CASE 
			WHEN SS.ID_MONEDA =1 THEN 'CLP'
			WHEN SS.ID_MONEDA =2 THEN 'USD'
			WHEN SS.ID_MONEDA =3 THEN 'EUR'
			WHEN SS.ID_MONEDA =4 THEN 'UF'
			WHEN SS.ID_MONEDA =5 THEN 'IVP'
		   END 'COD_MONEDA', 
	SS.CANTIDAD,
	SS.CAPITAL,
	SS.PRECIO,
	SS.INTERES,
	SS.TOTAL,
	SS.PRECIO_MERCADO,
	SS.VALOR_MERCADO,
	CASE 
			WHEN CTA.ID_MONEDA =1 THEN 'CLP'
			WHEN CTA.ID_MONEDA =2 THEN 'USD'
			WHEN CTA.ID_MONEDA =3 THEN 'EUR'
			WHEN CTA.ID_MONEDA =4 THEN 'UF'
			WHEN CTA.ID_MONEDA =5 THEN 'IVP'
		   END 'COD_MONEDA_CUENTA',
	SS.TOTAL_MON_CTA, 
	0 'VALOR_MERCADO_MON_CTA', 
	NULL 'TASA'  
FROM SIMULTANEAS S
INNER JOIN SIMULTANEAS_SALDO SS ON SS.ID_SIMULTANEA = S.ID_SIMULTANEA
INNER JOIN CUENTAS CTA ON SS.ID_CUENTA = CTA.ID_CUENTA
--where S.FCH_SIMULTANEA=@PFECHA_CIERRE
order by s.id_simultanea

----  --SIMULTANEAS
---- select	SS.ID_CUENTA,
----		SS.FECHA_CIERRE,
----		sm.id_simultanea
---- , CASE 
----			WHEN SS.ID_MONEDA =1 THEN 'CLP'
----			WHEN SS.ID_MONEDA =2 THEN 'USD'
----			WHEN SS.ID_MONEDA =3 THEN 'EUR'
----			WHEN SS.ID_MONEDA =4 THEN 'UF'
----			WHEN SS.ID_MONEDA =5 THEN 'IVP'
----		   END 'COD_MONEDA' 
----		   ,SS.CANTIDAD
----		   ,SS.CAPITAL
----		   ,SS.PRECIO
----		   ,SS.INTERES
----		   ,SS.TOTAL
----		   ,SS.PRECIO_MERCADO
----		   ,SS.VALOR_MERCADO 
----		 , CASE 
----			WHEN CTA.ID_MONEDA =1 THEN 'CLP'
----			WHEN CTA.ID_MONEDA =2 THEN 'USD'
----			WHEN CTA.ID_MONEDA =3 THEN 'EUR'
----			WHEN CTA.ID_MONEDA =4 THEN 'UF'
----			WHEN CTA.ID_MONEDA =5 THEN 'IVP'
----		   END 'COD_MONEDA_CUENTA'
----		   ,SS.TOTAL_MON_CTA 
----		   ,0 'VALOR_MERCADO_MON_CTA' 
----		   ,NULL 'TASA'
----from SIMULTANEAS_SALDO SS INNER JOIN CUENTAS CTA ON SS.ID_CUENTA = CTA.ID_CUENTA
----inner join simultaneas sm on sm.id_simultanea = SS.id_simultanea
----where sm.id_simultanea in(SELECT ID_SIMULTANEA  FROM SIMULTANEAS where FCH_SIMULTANEA=@PFECHA_CIERRE)
----order by fch_simultanea
--where S.FECHA_CIERRE=@PFECHA_CIERRE

--para valorizacion_rf
select distinct r.fecha, n.nemotecnico, r.tasa, r.factor,s.duracion from rf_factores r 
inner join nemotecnicos n on r.id_nemotecnico= n.id_nemotecnico
inner join @SALIDA s on s.id_nemotecnico=N.id_nemotecnico
where r.fecha=@PFECHA_CIERRE 

select vc.id_vta_corta
      ,vc.folio
      ,vc.Nombre_bolsa
      ,vc.codigo_bolsa
      ,vc.id_cliente
      ,vc.fecha_de_mvto
      ,vc.tipo_operacion
      ,vc.cantidad
      ,vc.precio_medio
      ,vc.valor
      ,vc.tasa
      ,vc.prima_a_plazo
      ,vc.prima_acumulada
      ,vc.dias_operacion
      ,vc.dias_en_curso
      ,vc.interes_devengado_mes
      ,vc.fecha_vencimiento
      ,vc.base_tasa
      ,vc.precio_medio_mercado
      ,vc.cuenta_gpi
      ,vc.Mandato
      ,vc.Cod_Producto
      ,vc.id_cuenta
      ,vc.ID_Nemotecnico
      ,vc.ID_TIPO_ESTADO
      ,vc.COD_ESTADO
	  ,vcd.id_vta_corta
      ,vcd.folio
      ,vcd.fecha_de_mvto
      ,vcd.fecha_de_devengo
      ,vcd.prima_a_plazo
      ,vcd.prima_acumulada
      ,vcd.dias_operacion
      ,vcd.dias_en_curso
      ,vcd.interes_devengado_mes
      ,vcd.PRECIO_MEDIO_MERCADO
from venta_corta vc inner join venta_corta_devengo vcd on vc.id_vta_corta = vcd.id_vta_corta
order by vc.id_vta_corta

--**APORTE_RETIROS_1862***

DECLARE @TBLAPORES1862 TABLE (FECHA                         DATETIME
                            , RUT_CLIENTE                   VARCHAR(12)
							, NUM_CUENTA                    VARCHAR(10)
							, ID_COMPROBANTE                NUMERIC(10,0)
							, ID_APO_RES_CUENTA             NUMERIC(10,0)
							, COD_ESTADO_APR1862            CHAR(1)
							, NUMERO_FOLIO                  NUMERIC(10,0)
							, TIPO                          VARCHAR(10)
							, NRO_OPERACION                 NUMERIC(10,0)
							, TIPO_OPERACION                VARCHAR(30)
							, DSC_TIPO_OPERACION            VARCHAR(200)
							, COD_PRODUCTO                  VARCHAR(10)
							, COD_INSTRUMENTO               VARCHAR(15)
							, FLG_TIPO_MOVIMIENTO           CHAR(1)
							, FECHA_OPERACION               DATETIME
							, FECHA_LIQUIDACION             DATETIME
							, DSC_OPERACION                 VARCHAR(200)
							, FECHA_CONFIRMACION            DATETIME
							, PORC_COMISION                 NUMERIC(18,6)
							, COMISION                      NUMERIC(18,4)
							, DERECHOS                      NUMERIC(18,4)
							, GASTOS                        NUMERIC(18,4)
							, IVA                           NUMERIC(18,4)
							, MONTO_TOTAL_OPERACION         NUMERIC(18,4)
							, ID_OPERACION_DETALLE          NUMERIC(10,0)
							, COD_INSTRUMENTO_DET           VARCHAR(15)
							, NEMOTECNICO                   VARCHAR(50)
							, DSC_NEMOTECNICO               VARCHAR(120)
							, COD_MONEDA_CAJA               VARCHAR(3)
							, COD_MONEDA_INSTRUMENTO        VARCHAR(3)
                            , COD_MONEDA_TRANS_INSTRUMENTO  VARCHAR(3)
                            , DSC_EMISOR_ESPECIFICO         VARCHAR(100)
	                        , CANTIDAD                      NUMERIC(18,4)             
	                        , PRECIO					    NUMERIC(18,4)
	                        , PRECIO_GESTION			    NUMERIC(18,4)
	                        , MONTO_PAGO				    NUMERIC(18,4)
	                        , MONTO_BRUTO				    NUMERIC(18,4)--FLOAT
	                        , FLG_FIRMADO                   CHAR(1))
INSERT INTO @TBLAPORES1862
SELECT A1862.FECHA
     , CLT.RUT_CLIENTE
     , CTA.NUM_CUENTA
     , A1862.ID_COMPROBANTE
	 , 0   'ID_APO_RES_CUENTA'
	 , A1862.COD_ESTADO
     , A1862.NUMERO_FOLIO
	 , 'OPERACION'                'TIPO'
	 , A1862.ID_OPERACION         'NRO_OPERACION'
	 , OPE.COD_TIPO_OPERACION     'TIPO_OPERACION'
	 , TPO.DSC_TIPO_OPERACION     'DSC_TIPO_OPERACION'
	 , OPE.COD_PRODUCTO
	 , OPE.COD_INSTRUMENTO
	 , OPE.FLG_TIPO_MOVIMIENTO
	 , OPE.FECHA_OPERACION
	 , OPE.FECHA_LIQUIDACION
	 , OPE.DSC_OPERACION
	 , OPE.FECHA_CONFIRMACION
	 , OPE.PORC_COMISION
	 , OPE.COMISION
	 , OPE.DERECHOS
	 , OPE.GASTOS
	 , OPE.IVA
	 , OPE.MONTO_OPERACION        'MONTO_TOTAL_OPERACION'
	 , OPD.ID_OPERACION_DETALLE   'ID_OPERACION_DETALLE'
	 , INM.COD_INSTRUMENTO
	 , INM.NEMOTECNICO
	 , INM.DSC_NEMOTECNICO
	 , ''                        'MONEDA_CAJA'
	 , (SELECT COD_MONEDA FROM MONEDAS WHERE ID_MONEDA=INM.ID_MONEDA) 'MONEDA_INSTRUMENTO'
	 , (SELECT COD_MONEDA FROM MONEDAS WHERE ID_MONEDA=INM.ID_MONEDA_TRANSACCION) 'MONEDA_TRANS_INSTRUMENTO'
	 , ISNULL(EMI.DSC_EMISOR_ESPECIFICO,'SIN EMISOR')
	 , OPD.CANTIDAD
	 , OPD.PRECIO
	 , OPD.PRECIO_GESTION
	 , OPD.MONTO_PAGO
	 , OPD.MONTO_BRUTO
	 , A1862.FLG_FIRMADO
FROM APORTES_RETIROS_1862 A1862
   INNER JOIN OPERACIONES OPE ON OPE.ID_OPERACION=A1862.ID_OPERACION
	INNER JOIN OPERACIONES_DETALLE OPD ON OPD.ID_OPERACION = OPE.ID_OPERACION
	INNER JOIN NEMOTECNICOS INM ON INM.ID_NEMOTECNICO = OPD.ID_NEMOTECNICO
	left JOIN EMISORES_ESPECIFICO EMI ON EMI.ID_EMISOR_ESPECIFICO = INM.ID_EMISOR_ESPECIFICO
	INNER JOIN CUENTAS CTA ON CTA.ID_CUENTA=OPE.ID_CUENTA
	INNER JOIN CLIENTES CLT ON CLT.ID_CLIENTE=CTA.ID_CLIENTE
	INNER JOIN TIPOS_OPERACIONES TPO ON TPO.COD_TIPO_OPERACION = OPE.COD_TIPO_OPERACION
--where a1862.FECHA >='20230901' And  a1862.FECHA <='20230927'
where a1862.FECHA =@PFECHA_CIERRE 

union all
SELECT A1862.FECHA
     , CLT.RUT_CLIENTE
     , CTA.NUM_CUENTA
     , A1862.ID_COMPROBANTE
     , APR.ID_APO_RES_CUENTA      'ID_APO_RES_CUENTA'
     , A1862.COD_ESTADO
     , A1862.NUMERO_FOLIO
	 , 'CAJA'                     'TIPO'
	 , MCJ.ID_MOV_CAJA            'NRO_OPERACION'
	 , MCJ.COD_ORIGEN_MOV_CAJA    'TIPO_OPERACION'
	 , TPO.DSC_ORIGEN_MOV_CAJA    'DSC_TIPO_OPERACION'
	 , 'CAJA'                     'COD_PRODUCTO'
	 , 'CAJA'                     'COD_INSTRUMENTO'
	 , MCJ.FLG_TIPO_MOVIMIENTO    'FLG_TIPO_MOVIMIENTO'
	 , MCJ.FECHA_MOVIMIENTO       'FECHA_OPERACION'
	 , MCJ.FECHA_LIQUIDACION      'FECHA_LIQUIDACION'
	 , MCJ.DSC_MOV_CAJA           'DSC_OPERACION'
	 , MCJ.FECHA_MOVIMIENTO       'FECHA_CONFIRMACION'
	 , 0                          'PORC_COMISION'
	 , 0                          'COMISION'
	 , 0                          'DERECHOS'
	 , 0                          'GASTOS'
	 , 0                          'IVA'
	 , MCJ.MONTO                  'MONTO_TOTAL_OPERACION'
	 , 1                          'ID_OPERACION_DETALLE'
	 , INM.COD_INSTRUMENTO
	 , INM.NEMOTECNICO
	 , INM.DSC_NEMOTECNICO
     , (SELECT COD_MONEDA 
	      FROM MONEDAS M
		       INNER JOIN CAJAS_CUENTA C ON C.ID_MONEDA =  M.ID_MONEDA
			                        AND C.ID_CAJA_CUENTA = MCJ.ID_CAJA_CUENTA)           'MONEDA_CAJA'
     , ISNULL((SELECT COD_MONEDA FROM MONEDAS WHERE ID_MONEDA=INM.ID_MONEDA),'')             'MONEDA_INSTRUMENTO'
	 , ISNULL((SELECT COD_MONEDA FROM MONEDAS WHERE ID_MONEDA=INM.ID_MONEDA_TRANSACCION),'') 'MONEDA_TRANS_INSTRUMENTO'
	 , EMI.DSC_EMISOR_ESPECIFICO
	 , MCJ.CANTIDAD
	 , MCJ.PRECIO
	 , 0                          'PRECIO_GESTION'
	 , 0                          'MONTO_PAGO'
	 , 0                          'MONTO_BRUTO'
	 , A1862.FLG_FIRMADO
FROM APORTES_RETIROS_1862 A1862
    INNER JOIN APORTE_RESCATE_CUENTA APR ON APR.ID_APO_RES_CUENTA = a1862.ID_APO_RES_CUENTA
	INNER JOIN CUENTAS CTA ON CTA.ID_CUENTA=APR.ID_CUENTA
	INNER JOIN CLIENTES CLT ON CLT.ID_CLIENTE=CTA.ID_CLIENTE
	INNER JOIN MOV_CAJA  MCJ ON MCJ.ID_MOV_CAJA = APR.ID_MOV_CAJA
    INNER JOIN MOV_CAJA_ORIGEN TPO ON TPO.COD_ORIGEN_MOV_CAJA = MCJ.COD_ORIGEN_MOV_CAJA
    LEFT OUTER JOIN NEMOTECNICOS INM ON INM.ID_NEMOTECNICO = MCJ.ID_NEMOTECNICO
	LEFT OUTER JOIN EMISORES_ESPECIFICO EMI ON EMI.ID_EMISOR_ESPECIFICO = INM.ID_EMISOR_ESPECIFICO
--where a1862.FECHA >='20230901' AND  a1862.FECHA <='20230927'
where a1862.FECHA =@PFECHA_CIERRE 
order by fecha

select 
convert(varchar,FECHA,112) FECHA,                        
RUT_CLIENTE,                  
NUM_CUENTA,                   
ID_COMPROBANTE,               
ID_APO_RES_CUENTA,            
COD_ESTADO_APR1862,           
NUMERO_FOLIO,                 
TIPO,                         
NRO_OPERACION                ,
TIPO_OPERACION               ,
DSC_TIPO_OPERACION           ,
COD_PRODUCTO                 ,
COD_INSTRUMENTO              ,
FLG_TIPO_MOVIMIENTO          ,
convert(varchar,FECHA_OPERACION,112)FECHA_OPERACION              ,
convert(varchar,FECHA_LIQUIDACION,112) FECHA_LIQUIDACION            ,
DSC_OPERACION                ,
convert(varchar,FECHA_CONFIRMACION,112)  FECHA_CONFIRMACION         ,
PORC_COMISION                ,
COMISION                     ,
DERECHOS                     ,
GASTOS                       ,
IVA                          ,
MONTO_TOTAL_OPERACION        ,
ID_OPERACION_DETALLE         ,
COD_INSTRUMENTO_DET          ,
NEMOTECNICO                  ,
DSC_NEMOTECNICO              ,
COD_MONEDA_CAJA              ,
COD_MONEDA_INSTRUMENTO       ,
COD_MONEDA_TRANS_INSTRUMENTO ,
DSC_EMISOR_ESPECIFICO        ,
CANTIDAD                     ,
PRECIO					   ,
replace(PRECIO_GESTION,',','.')			   ,
replace(MONTO_PAGO,',','.')				   ,
replace(MONTO_BRUTO,',','.'),   
FLG_FIRMADO                 
from @TBLAPORES1862

--***dividendos***
select 
ID_NEMOTECNICO,
convert(varchar,FECHA,112)FECHA,
MONTO,
convert(varchar,FECHA_CORTE,112)FECHA_CORTE,
TIPO_VARIACION,
ID_NEMOTECNICO_OPCION,
CADA_X_CANTIDAD,
ENTREGAR_X_CANTIDAD,
null,
null,
null,
'VIG',
0,
'N',
'N',
'N',
'N',
null,
null,
null,
null,
null,
null
from dividendos 
--where fecha>='20230901' and fecha<='20230927' 
where fecha=@PFECHA_CIERRE
ORDER BY ID_DIVIDENDO

--***valorizacion_magic***

SELECT 
convert(varchar,FECHA_CIERRE,112)FECHA_CIERRE,
ID_CUENTA,
COD_PRODUCTO,
NEMOTECNICO_MAGIC,
CANTIDAD_MAGIC,
PRECIO_MAGIC,
MONTO_MERCADO_MAGIC,
ID_NEMOTECNICO_GPI,
CANTIDAD_GPI,
MONTO_MERCADO_GPI
FROM VALORIZACION_MAGIC 
--WHERE FECHA_CIERRE>='20230918' AND  FECHA_CIERRE<='20230927'
WHERE FECHA_CIERRE=@PFECHA_CIERRE
ORDER BY FECHA_CIERRE

--mgr_patrimonio cuenta
select [ID_CUENTA]
      ,[FECHA_CIERRE]
      ,[SALDO_CAJA_MON_CUENTA]
      ,[SALDO_ACTIVO_MON_CUENTA]
      ,[PATRIMONIO_MON_CUENTA]
      ,[SALDO_CAJA_MON_EMPRESA]
      ,[SALDO_ACTIVO_MON_EMPRESA]
      ,[PATRIMONIO_MON_EMPRESA]
      ,[VALOR_CUOTA_MON_CUENTA]
      ,[TOTAL_CUOTAS_MON_CUENTA]
      ,[RENTABILIDAD_MON_CUENTA]
      ,[APORTE_RETIROS_MON_CUENTA]
      ,[MONTO_X_COBRAR_MON_CTA]
      ,[MONTO_X_PAGAR_MON_CTA]
      ,[MONTO_X_COBRAR_MON_EMPRESA]
      ,[MONTO_X_PAGAR_MON_EMPRESA]
      ,'VIG' [EST_PATRIMONIO_CUENTA]
      ,[COMI_DEVENG_MON_CTA]
      ,'A' [WEB_ESTADO]
 from dbo.PATRIMONIO_CUENTAS 
 where FECHA_CIERRE=@PFECHA_CIERRE

--mgr_comi_hono_ase_cta
SELECT  --ID_COMISION_HONO_ASE_CUENTA,
		ID_CUENTA,
		FECHA_CIERRE,
		COMISION_HONORARIOS,
		COMISION_ASESORIAS,
		COD_INSTRUMENTO,
		NULL 'COD_SUB_CLASE_INSTRUMENTO',
		CASE  WHEN ID_MONEDA=1 THEN 'CLP'
		WHEN ID_MONEDA=2 THEN 'USD'
		WHEN ID_MONEDA=3 THEN 'EUR'
		WHEN ID_MONEDA=4 THEN 'CLP' 
		WHEN ID_MONEDA=5 THEN 'IVP'
  END AS 'COD_MONEDA'
  FROM COMISIONES_HONO_ASE_CUENTA
  where FECHA_CIERRE=@PFECHA_CIERRE 
END  
GO
GRANT EXECUTE ON [MGR_SALDOS_CARGA_PRECIOS_GPI60] TO DB_EXECUTESP
GO