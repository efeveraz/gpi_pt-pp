﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PreLoader.aspx.vb" Inherits="AplicacionWeb.PreLoader" %>

<%@ Import Namespace="AplicacionWeb" %>

<!DOCTYPE html>
<html lang="es">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Sistema/preLoader.js") %>'></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
</html>
