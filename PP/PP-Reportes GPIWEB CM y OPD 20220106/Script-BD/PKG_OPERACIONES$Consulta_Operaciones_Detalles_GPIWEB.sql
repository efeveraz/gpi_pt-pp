USE [CSGPI]
GO
IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_OPERACIONES$Consulta_Operaciones_Detalles_GPIWEB]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
DROP PROCEDURE [DBO].[PKG_OPERACIONES$Consulta_Operaciones_Detalles_GPIWEB]
GO
CREATE PROCEDURE [dbo].[PKG_OPERACIONES$Consulta_Operaciones_Detalles_GPIWEB] (@Pid_usuario numeric
, @Pfecha_ini datetime = NULL
, @Pfecha_ter datetime = NULL
, @Pid_Empresa numeric = NULL
, @Pid_Operacion numeric = NULL
, @Pcod_estado varchar(3) = NULL
, @Pid_Cuenta float(53) = NULL
, @Pid_Asesor float(53) = NULL
, @Pcod_Instrumento varchar(15) = NULL
, @Pcod_tipo_operacion varchar(10) = NULL
, @Pflg_tipo_movimiento varchar(1) = NULL
, @pId_Nemotecnico numeric = NULL)
AS
BEGIN
  SELECT
    FECHA_OPERACION,
    ID_OPERACION,
    VOD.NUM_CUENTA,
    DSC_INTRUMENTO,
    DSC_TIPO_OPERACION,
    CASE
      WHEN FLG_TIPO_MOVIMIENTO = 'E' THEN 'Venta/Egreso'
      WHEN FLG_TIPO_MOVIMIENTO = 'I' THEN 'Compra/Ingreso'
      ELSE FLG_TIPO_MOVIMIENTO 
    END TIPO_MOVIMIENTO,
    VOD.DSC_ESTADO,
    MONTO_OPERACION,
    DSC_MONEDA,
    VCU.DSC_ASESOR ASESOR,
    NEMOTECNICO,
    CANTIDAD,
    PRECIO,
    MONTO_PAGO,
    FECHA_VCTO_NEMOTECNICO AS FECHA_VENCIMIENTO
  FROM VIEW_OPERACION_DETALLE VOD
  INNER JOIN INSTRUMENTOS INS
    ON VOD.COD_INSTRUMENTO = INS.COD_INSTRUMENTO
  INNER JOIN MONEDAS MON
    ON VOD.ID_MONEDA_OPERACION = MON.ID_MONEDA
  INNER JOIN VIEW_CUENTAS VCU
	ON VOD.ID_CUENTA = VCU.ID_CUENTA
  WHERE VOD.COD_ESTADO = ISNULL(@Pcod_estado, VOD.COD_ESTADO)
  AND ID_OPERACION = ISNULL(@Pid_Operacion, ID_OPERACION)
  AND VOD.ID_CUENTA = ISNULL(@Pid_Cuenta, VOD.ID_CUENTA)
  AND VOD.COD_PRODUCTO = ISNULL(NULL, VOD.COD_PRODUCTO)
  AND (FECHA_OPERACION BETWEEN ISNULL(@Pfecha_ini, FECHA_OPERACION) AND ISNULL(@Pfecha_ter, FECHA_OPERACION))
  AND VOD.ID_EMPRESA = ISNULL(@Pid_empresa, VOD.ID_EMPRESA)

  UNION ALL

  SELECT
    V.FECHA_DE_MVTO AS FECHA_OPERACION,
    V.ID_VTA_CORTA  AS ID_OPERACION,
    C.NUM_CUENTA,
    'Acciones Nac' AS DSC_INSTRUMENTO,
    'Venta Corta'  AS DSC_TIPO_OPERACION,
	'Ingreso'      AS TIPO_MOVIMIENTO,
    'Confirmado' AS DSC_ESTADO,
    V.VALOR AS MONTO_OPERACION,
    'PESOS' AS DSC_MONEDA,
    VCUE.DSC_ASESOR ASESOR,
    N.NEMOTECNICO,
    V.CANTIDAD,
    V.precio_medio AS PRECIO,
    V.VALOR AS MONTO_PAGO,
    NULL AS FECHA_VENCIMIENTO
  --V.ID_VTA_CORTA     AS ID_OPERACION ,
  --          V.ID_CUENTA        AS ID_CUENTA    ,
  --          C.ID_EMPRESA   ,
  --          C.NUM_CUENTA   ,
  --          C.RUT_CLIENTE  ,
  --          V.tipo_operacion   AS COD_TIPO_OPERACION ,
  --          'Venta Corta'      AS DSC_TIPO_OPERACION ,
  --          9                  AS ID_TIPO_ESTADO     ,
  --          'C'                AS COD_ESTADO         ,
  --          'Confirmado'       AS DSC_ESTADO         ,
  --          NULL               AS ID_CONTRAPARTE     ,
  --          NULL               AS ID_REPRESENTANTE   ,
  --          NULL               AS ID_SISTEMA_ORIGEN  ,
  --          NULL               AS ID_SISTEMA_ORIGEN_CONFIRMACION ,
  --          1                  AS ID_MONEDA_OPERACION ,
  --          v.Cod_Producto     AS COD_PRODUCTO   ,
  --          NULL               AS ID_CARGO_ABONO ,
  --          'ACCIONES_NAC'     AS COD_INSTRUMENTO ,
  --          'I'                AS FLG_TIPO_MOVIMIENTO ,
  --          V.FECHA_DE_MVTO    AS FECHA_OPERACION ,
  --          NULL               AS FECHA_VIGENCIA  ,
  --          NULL               AS FECHA_LIQUIDACION ,
  --          'Venta Corta'      AS DSC_OPERACION ,
  --         NULL                AS ID_TRADER ,
  --         NULL                AS FECHA_CONFIRMACION ,
  --         0                   AS PORC_COMISION ,
  --         0                   AS COMISION ,
  --         0                   AS DERECHOS ,
  --         0                   AS GASTOS ,
  --         0                   AS IVA  ,
  --         V.VALOR             AS MONTO_OPERACION ,
  --         V.ID_VTA_CORTA      AS ID_OPERACION_DETALLE ,
  --         V.ID_NEMOTECNICO ,
  --         N.NEMOTECNICO    ,
  --         V.CANTIDAD       ,
  --         V.precio_medio      AS PRECIO ,
  --         NULL                AS PRECIO_GESTION ,
  --         NULL                AS PLAZO ,
  --         NULL                AS BASE ,
  --         NULL                AS FECHA_VENCIMIENTO ,
  --         1                   AS ID_MONEDA_PAGO ,
  --         NULL                AS VALOR_DIVISA ,
  --         V.VALOR             AS MONTO_PAGO ,
  --         NULL                AS COMISION_DETALLE ,
  --         NULL                AS INTERES ,
  --         NULL                AS UTILIDAD ,
  --         NULL                AS PORC_COMISION_DETALLE ,
  --         V.VALOR             AS MONTO_BRUTO ,
  --         NULL                AS FLG_MONTO_REFERENCIADO ,
  --         NULL                AS FLG_TIPO_DEPOSITO ,
  --         NULL                AS FECHA_VALUTA ,
  --         'N'                 AS FLG_VENDE_TODO ,
  --         NULL                AS FECHA_LIQUIDACION_DETALLE ,
  --         NULL                AS PRECIO_HISTORICO_COMPRA ,
  --         NULL                AS FLG_LIMITE_PRECIO ,
  --         'N'                 AS TIPO_INVERSION ,
  --         NULL                AS ID_MOV_ACTIVO_COMPRA ,
  --         NULL                AS FECHA_VCTO_NEMOTECNICO
  FROM dbo.VENTA_CORTA V,
       VIEW_CUENTAS_VIGENTES C,
       Nemotecnicos N,
	   VIEW_CUENTAS VCUE
  WHERE (V.ID_CUENTA = C.ID_CUENTA)
  AND (V.ID_NEMOTECNICO = N.ID_NEMOTECNICO)
  AND ((@Pcod_estado IS NULL)
  OR ('C' = @Pcod_estado))
  AND V.ID_VTA_CORTA = ISNULL(@Pid_Operacion, V.ID_VTA_CORTA)
  AND V.ID_CUENTA = ISNULL(@Pid_Cuenta, V.ID_CUENTA)
  --      AND v.Cod_Producto = ISNULL(@PCOD_PRODUCTO, v.Cod_Producto)
  AND (V.FECHA_DE_MVTO BETWEEN ISNULL(@Pfecha_ini, V.FECHA_DE_MVTO) AND ISNULL(@Pfecha_ter, V.FECHA_DE_MVTO))
  AND C.ID_EMPRESA = ISNULL(@Pid_empresa, C.ID_EMPRESA)
  AND V.ID_CUENTA = VCUE.ID_CUENTA
END
GO
GRANT EXECUTE ON [PKG_OPERACIONES$Consulta_Operaciones_Detalles_GPIWEB] TO DB_EXECUTESP
GO