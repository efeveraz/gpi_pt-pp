USE [CISCB]
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[AD_QRY_CB_MOVTO_ACCIONES]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE AD_QRY_CB_MOVTO_ACCIONES
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[AD_QRY_CB_MOVTO_ACCIONES]
--DECLARE
	@pFecha_movimientos char(8)  -- YYYYMMDD
--SET @pFecha_movimientos='20161230'
AS
BEGIN

	SET DATEFORMAT ymd
	SET NOCOUNT ON

	DECLARE @vFecha_movimientos int, @msgerr char(80)

	SET @msgerr = ''

	IF ISDATE(@pFecha_movimientos) = 0
		SET @msgerr = 'Fecha Inválida (' + RTRIM(@pFecha_movimientos)
	IF @msgerr=''
	BEGIN
		IF RTRIM(@pFecha_movimientos) = ''
			SET @msgerr = 'Debe ingresar una Fecha (YYYYMMDD)'
		ELSE
			SET @vFecha_movimientos = (CAST(CONVERT(DATETIME, @pFecha_movimientos) AS INT)+693596)
	END

	DECLARE	@vTipo_persona CHAR(2),
			@vRazon_social VARCHAR(120),
			@vApellido_paterno VARCHAR(40),
			@vApellido_materno VARCHAR(40),
			@vNombres VARCHAR(40)

	CREATE TABLE #tmp_acciones(
		t_Rut_cliente CHAR(15),
		t_Tipo_operacion CHAR(1),
		t_Numero_orden INT,
		t_Correlativo INT,
		t_Nemotecnico VARCHAR(20),
		t_Cantidad FLOAT,
		t_Precio FLOAT,
		t_Monto_neto FLOAT,
		t_Comision FLOAT,
		t_Derecho FLOAT,
		t_Gastos FLOAT,
		t_Iva FLOAT,
		t_Monto_operacion FLOAT,
		t_Fecha_liquidacion DATETIME,
		t_Id_cierre VARCHAR(10),
		t_Nombre_cliente VARCHAR(120),
		t_numero_factura INT,
		t_tipo_liquidacion VARCHAR(2),
		t_bolsa VARCHAR(1),
		t_custodia_nominal VARCHAR(1)
	)


	IF @msgerr = ''
	BEGIN
		DECLARE @acc_Rut_cliente CHAR(15),
				@acc_Tipo_operacion CHAR(1),
				@acc_Numero_orden INT,
				@acc_Correlativo INT,
				@acc_Nemotecnico VARCHAR(20),
				@acc_Cantidad FLOAT,
				@acc_Precio FLOAT,
				@acc_Monto_neto FLOAT,
				@acc_Comision FLOAT,
				@acc_Derecho FLOAT,
				@acc_Gastos FLOAT,
				@acc_Iva FLOAT,
				@acc_Monto_operacion FLOAT,
				@acc_Fecha_liquidacion DATETIME,
				@acc_Id_cierre  VARCHAR(10),
				@iNumeroFactura INT,
				@vTipo_liquidacion VARCHAR(2),
				@vBolsa VARCHAR(1),
				@vCustodia_nominal VARCHAR(1)

	DECLARE rec_acc CURSOR FOR
	SELECT
		'RUT CLIENTE' = bk.Id_cliente,
		'TIPO OPERACION'= CASE bk.Concepto
							WHEN 'IV' then 'V'
							WHEN 'IC' then 'C'
							WHEN 'IG' then 'I'
							WHEN 'RG' then 'R'
							ELSE '?'
						END,
		'NUMERO ORDEN'=isnull(ro.Numero_orden,0),
		'CORRELATIVO'=isnull(ro.Linea_orden,0),
		'NEMOTECNICO'=bk.Instrumento,
		'CANTIDAD'=bk.Nominales,
		'PRECIO'=bk.Precio,
		--'MONTO NETO'=bk.Valor,
		'MONTO NETO' = (
						CASE 
							WHEN i.Tipo_de_reajuste = 'DO' THEN 
			
								(bk.Valor / (SELECT TOP 1 CONVERT(NUMERIC(16,4),(p.valor/POWER(10,ISNULL(p.Decimales,0))))  
												FROM	valgen p  
												WHERE	p.Codigo_del_valor = '0' 
												AND		p.Tipo_de_valor = 'DO'
												AND     p.fecha_del_valor <= @vFecha_movimientos  
												ORDER BY p.fecha_del_valor DESC))

							ELSE
								bk.Valor -- este valor va en la moneda de la operacion por lo cual, no se saca la paridad. 
								--(
								--/ (SELECT TOP 1 CONVERT(NUMERIC(16,4),(p.valor/POWER(10,ISNULL(p.Decimales,0))))  
								--				FROM	valgen p  
								--				WHERE	p.Codigo_del_valor =  i.moneda_transaccion_bolsa
								--				and		p.tipo_de_valor='MO' 
								--				AND		p.fecha_del_valor <= @vFecha_movimientos  
								--				ORDER BY p.fecha_del_valor DESC))
						END
						),
		'COMISION'=bk.Comisiones,
		'DERECHO'=bk.Derechos,
		'GASTOS'=bk.Gastos,
		'IVA'=bk.Iva,
		'MONTO OPERACION'=bk.Valor + ((bk.Comisiones + bk.Derechos + bk.Gastos + bk.Iva) *	CASE bk.Concepto
				WHEN 'IV' THEN -1
				WHEN 'IC' THEN 1
				WHEN 'RG' THEN -1
				WHEN 'IG' THEN 1
				ELSE 1
			END),
		'FECHA LIQUIDACION' = CASE 
								WHEN ISNULL(bk.Fecha_liquidacion,0) > 693596
									THEN CONVERT(DATETIME, bk.Fecha_liquidacion - 693596)
								ELSE 
									null
							END,
		--20071112 - Se agrega el Id del cierre por solicitud de Cristian Garay,
		--20071112 - ademas se filtra para clientes de Administracion de cartera
		--20071112 - solamente.   Se dejaron listos los campos Fecha de Cierre,
		--20071112 - Hora de cierre y Bolsa de cierre, en caso de que mas adelante
		--20071112 - los soliciten.  OPIZARRO.
		--'FECHA CIERRE'=ro.Fecha_del_cierre,
		--'HORA CIERRE'=ro.Hora_del_cierre,
		--'BOLSA'=ro.Bolsa_del_cierre,
		'ID CIERRE' = ro.Id_cierre_de_bolsa,
		t_numero_factura = ISNULL(fd.Facdet_folio, 0),
		t_tipo_liquidacion = isnull(bk.Tipo_liquidacion, ''),
		t_bolsa=isnull(bk.Bolsa_palo, ''),
		t_custodia_nominal=isnull(bk.Custodia_origen, '')

	FROM	backoff as bk
		INNER JOIN instru as i
			ON i.Id_instrumento = bk.Instrumento and
			i.Tipo_instrumento = 'A'
		INNER JOIN persocb as ps
			ON ps.Id_cliente = bk.Id_cliente and
			ps.Administracion_de_cartera = 'S'
		LEFT JOIN relord as ro
			ON ro.Concepto=bk.Concepto and
			ro.Id_concepto = bk.Id_concepto and
			ro.Concepto_linea = bk.Id_linea_concepto
		LEFT OUTER JOIN dbo.FACTUDET as fd
			ON (fd.Facdet_concepto=bk.concepto
			AND fd.Facdet_idconcepto=bk.Id_concepto
			AND fd.Facdet_lineaconcepto=bk.Id_linea_concepto)
		JOIN dbo.FACTUENC as fe
			ON (fe.Facenc_sistema=fd.Facdet_sistema
			AND fe.Facenc_Tipo=fd.Facdet_tipo
			AND fe.Facenc_afecta_iva=fd.Facdet_afecta_iva
			AND fe.Facenc_folio=fd.Facdet_folio
			AND fe.Facenc_estado='I'  )
	WHERE	bk.Fecha_de_mvto = @vFecha_movimientos and
			bk.Origen = 'OP' and
			bk.Concepto in ('IC','IV','IG','RG') and
			bk.Modulo_de_entrada='RV'

	OPEN rec_acc
	FETCH NEXT FROM rec_acc
	INTO	@acc_Rut_cliente, @acc_Tipo_operacion,
			@acc_Numero_orden, @acc_Correlativo,
			@acc_Nemotecnico, @acc_Cantidad,
			@acc_Precio, @acc_Monto_neto,
			@acc_Comision, @acc_Derecho,
			@acc_Gastos, @acc_Iva,
			@acc_Monto_operacion, @acc_Fecha_liquidacion,
			@acc_Id_cierre,
			@iNumeroFactura, @vTipo_liquidacion,
			@vBolsa,@vCustodia_nominal
  
	WHILE @@fetch_status = 0
	BEGIN

		EXEC dbo.AD_Retorna_datos_cliente
			@acc_Rut_cliente,
			@vTipo_persona OUTPUT,
			@vRazon_social OUTPUT,
			@vApellido_paterno OUTPUT,
			@vApellido_materno OUTPUT,
			@vNombres OUTPUT,
			'',
			0,
			'',
			0,
			'',
			'',
			'',
			'',
			0,
			'',
			0,
			'',
			'',
			'',
			''

		INSERT INTO #tmp_acciones
		SELECT  @acc_Rut_cliente,
				@acc_Tipo_operacion,
				@acc_Numero_orden,
				@acc_Correlativo,
				@acc_Nemotecnico,
				@acc_Cantidad,
				@acc_Precio,
				@acc_Monto_neto,
				@acc_Comision,
				@acc_Derecho,
				@acc_Gastos,
				@acc_Iva,
				@acc_Monto_operacion,
				@acc_Fecha_liquidacion,
				@acc_Id_cierre,
				CASE @vTipo_persona
					WHEN 'PN' THEN RTRIM(@vNombres) + ' ' + RTRIM(@vApellido_paterno) + ' ' + RTRIM(@vApellido_materno)
					ELSE @vRazon_social
				END
				, @iNumeroFactura
					, @vTipo_liquidacion
					, @vBolsa
					, @vCustodia_nominal

		FETCH NEXT FROM rec_acc
		INTO	@acc_Rut_cliente, @acc_Tipo_operacion,
				@acc_Numero_orden, @acc_Correlativo,
				@acc_Nemotecnico, @acc_Cantidad,
				@acc_Precio, @acc_Monto_neto,
				@acc_Comision, @acc_Derecho,
				@acc_Gastos, @acc_Iva,
				@acc_Monto_operacion, @acc_Fecha_liquidacion,
				@acc_Id_cierre, @iNumeroFactura,
				@vTipo_liquidacion,
				@vBolsa, @vCustodia_nominal
	END
	CLOSE rec_acc
	DEALLOCATE rec_acc

	END

	SELECT	'RUT_CLIENTE' = t_Rut_cliente,
			'TIPO_OPERACION' = t_Tipo_operacion,
			'NUMERO_ORDEN' = t_Numero_orden,
			'CORRELATIVO' = t_Correlativo,
			'NEMOTECNICO' = t_Nemotecnico,
			'CANTIDAD' = t_Cantidad,
			'PRECIO' = t_Precio,
			'MONTO_NETO' = t_Monto_neto,
			'COMISION' = t_Comision,
			'DERECHO' = t_Derecho,
			'GASTOS' = t_Gastos,
			'IVA'  = t_Iva,
			'MONTO_OPERACION' = t_Monto_operacion,
			'FECHA_LIQUIDACION' = t_Fecha_liquidacion,
			'ID_CIERRE' = t_Id_cierre,
			'NOMBRE_CLIENTE' = t_Nombre_cliente,
			'NUMERO_FACTURA' = t_numero_factura,
			'TIPO_LIQ' = t_tipo_liquidacion,
			'BOLSA'= t_bolsa,
			'CUST_NOM'= t_custodia_nominal,
			0 'GARANTIAS',
			0 'PRESTAMOS',
			0 'SIMULTANEAS',
			ISNULL(Nombre_instrumento_fecu,'') 'Nombre_instrumento_fecu'
	FROM	#tmp_acciones
		LEFT OUTER JOIN instru i 
			ON i.id_instrumento=t_nemotecnico
	
	DROP TABLE #tmp_acciones

END
GO
GRANT EXECUTE ON AD_QRY_CB_MOVTO_ACCIONES TO DB_EXECUTESP
GO