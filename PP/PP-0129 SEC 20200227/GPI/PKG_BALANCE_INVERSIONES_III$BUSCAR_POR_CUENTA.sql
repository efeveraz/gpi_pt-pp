USE[CSGPI]
GO
IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PKG_BALANCE_INVERSIONES_III$BUSCAR_POR_CUENTA')
   DROP PROCEDURE [PKG_BALANCE_INVERSIONES_III$BUSCAR_POR_CUENTA]
GO
create procedure [dbo].[PKG_BALANCE_INVERSIONES_III$BUSCAR_POR_CUENTA]
 @Pid_Cuenta  numeric,
 @PFecha_Cartola  datetime,
 @PFecha_Anterior datetime,
 @Pid_moneda_salida numeric(4) = null,
 @Pid_Empresa  numeric(10) = null,
 @PControl   char(01) = 'L'
as
set nocount on


--declare @Pid_Cuenta  numeric,
--   @PFecha_Cartola  datetime,
--   @PFecha_Anterior datetime,
--   @Pid_moneda_salida numeric(4) ,
--   @Pid_Empresa  numeric(10) ,
--   @PControl   char(01)
   
--set @Pid_Cuenta=(SELECT ID_CUENTA FROM CUENTAS WHERE NUM_CUENTA ='01928')
--set @PFecha_Cartola='20180116'
--set @PFecha_Anterior='20171231'
--set @Pid_moneda_salida ='1'
--set @Pid_Empresa  = '2'
--set @PControl  = 'L'    

 if @Pid_Empresa is null
 begin
  set @Pid_Empresa = isnull((select id_empresa
         from cuentas
         where id_cuenta = @Pid_Cuenta),99999)
  if @Pid_Empresa = 99999
   begin
    set @Pid_Empresa = (select min(id_empresa) from empresas)
   end
 end
declare @FechaVirtual datetime
declare @Z table ( ID    numeric,
     DSC_ARBOL  varchar(100),
     PADRE   numeric NULL)

declare @arbol_paso table(  ID_arbol  int,
       SECUENCIA  varchar(1000),
       NIVEL   int,
       VALOR_RAMA  float NULL,
       VALOR_ANTERIOR_RAMA float NULL,
       VALOR_HOJA  float NULL,
       VALOR_ANTERIOR_HOJA float NULL)

declare @arbol_paso_2 table(ID_arbol_2  integer,
       SECUENCIA  varchar(1000),
       NIVEL   integer,
       VALOR_RAMA  float NULL,
       VALOR_ANTERIOR_RAMA float NULL,
       VALOR_HOJA  float NULL,
       VALOR_ANTERIOR_HOJA float NULL)


DECLARE @TABLA_SALIDA TABLE( ID NUMERIC
							,DSC_ARBOL VARCHAR(100)
							,NIVEL NUMERIC
							,MONTO_MON_CTA FLOAT
							,MONTO_MON_CTA_ANTERIOR FLOAT
							,VALOR_HOJA FLOAT
							,PADRE NUMERIC
							,ID_EMPRESA NUMERIC
							,ID_PADRE_ARBOL_CLASE_INST NUMERIC
							,DSC_ARBOL_CLASE_INST VARCHAR(100)
							,ID_ACI_TIPO NUMERIC
							,MONTO_ACTUAL FLOAT
							,MONTO_ANTERIOR FLOAT
							,FECHA_ULTIMO_CIERRE DATETIME
							,FECHA_MES_ANTERIOR DATETIME)

declare @decimales integer

 if @Pid_moneda_salida is null
  set @Pid_moneda_salida = (select id_moneda from cuentas where id_cuenta = @Pid_Cuenta)
 set @Decimales = (select dicimales_mostrar from monedas where id_moneda = @Pid_moneda_salida)

 insert @Z
 select ID_ARBOL_CLASE_INST as ID,
   DSC_ARBOL_CLASE_INST as DSC_ARBOL,
   ID_PADRE_ARBOL_CLASE_INST as PADRE
 from ARBOL_CLASE_INSTRUMENTO
 where id_empresa = @Pid_Empresa
 and id_aci_tipo = 2

 

-- if not exists( select 1
--     from cierres_cuentas
--     where ID_CUENTA = @Pid_Cuenta
--     and fecha_cierre = @PFecha_Cartola)
--  set @PFecha_Cartola = (select max(fecha_cierre)
--        from cierres_cuentas
--        where ID_CUENTA = @Pid_Cuenta
--        and fecha_cierre <= @PFecha_Cartola)

if @PControl = 'L'
    set @FechaVirtual = dbo.FNT_EntregaFechaCierreCuenta(@Pid_cuenta)
    if @FechaVirtual < @PFecha_Cartola
  set @PFecha_Cartola = @FechaVirtual

/****************************************/
/*CAMBIO LAS FECHAS*/
DECLARE @FER DATETIME

set @FER = @PFecha_Cartola
set @PFecha_Cartola = @PFECHA_ANTERIOR
set @PFECHA_ANTERIOR =  @FER
/****************************************/

insert @arbol_paso
select ID,
 right(space(10) + convert(varchar(10),ID),10),
 1,
 isnull(DBO.PKG_GLOBAL$MONTO_MON_CUENTA_RAMA_POR_MONEDA(ID,
       @PID_CUENTA,
       @PFecha_Cartola,
       @Pid_moneda_salida),0) AS VALOR_RAMA,
 isnull(DBO.PKG_GLOBAL$MONTO_MON_CUENTA_RAMA_POR_MONEDA(ID,
       @PID_CUENTA,
       @PFECHA_ANTERIOR,
       @Pid_moneda_salida),0) AS VALOR_ANTERIOR_RAMA,
 0,
 0
from @Z ZZ where (PADRE is NULL OR (select count(*) from @Z ZS where ZS.PADRE = ZZ.ID)>1)

DECLARE @I INT
SELECT @I = 0
WHILE @@ROWCOUNT > 0
 BEGIN
  SELECT @I = @I + 1
  INSERT @arbol_paso
  SELECT ZZ.ID,
   SECUENCIA + RIGHT(SPACE(10) + CONVERT(VARCHAR(10),ZZ.ID),10),
   @I + 1,
   ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_POR_MONEDA(ID_arbol,@PID_CUENTA,@PFecha_Cartola,@Pid_moneda_salida),0),
   ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_POR_MONEDA(ID_arbol,@PID_CUENTA,@PFecha_Cartola,@Pid_moneda_salida),0),
   0,
   0
  FROM @Z ZZ, @arbol_paso x
  WHERE x.NIVEL = @I
   and ZZ.PADRE = x.ID_arbol
 END
update @arbol_paso set VALOR_HOJA = isnull(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_POR_MONEDA(ID_arbol, @PID_CUENTA, @PFecha_Cartola, @Pid_moneda_salida),0) where NIVEL <> 1
insert @arbol_paso_2
select * from @arbol_paso


update @arbol_paso_2 set VALOR_HOJA = (select sum(A.VALOR_HOJA)
      from @arbol_paso A,
       @Z ZZ
      where A.ID_arbol = ZZ.ID
       and ZZ.PADRE = id_arbol)
  where (select count(*) from @Z where PADRE = ID_arbol_2) > 1
DECLARE @SALIDA TABLE ( ID   NUMERIC,
   DSC_ARBOL_CLASE_INST VARCHAR(100),
   NIVEL   NUMERIC,
   MONTO_MON_CTA  float,
   MONTO_MON_CTA_ANTERIOR float,
   VALOR_HOJA  float,
   PADRE   NUMERIC)
INSERT INTO @SALIDA
SELECT ID_arbol_2,
  SPACE((NIVEL-1)*4) + ZZ.DSC_ARBOL AS DSC_ARBOL_CLASE_INST ,
  NIVEL,
  VALOR_RAMA AS MONTO_MON_CTA,
  VALOR_ANTERIOR_RAMA AS MONTO_MON_CTA_ANTERIOR,
  VALOR_HOJA,
  PADRE
FROM @arbol_paso_2, @Z ZZ
WHERE ID_arbol_2 = ZZ.ID
AND  NIVEL= 1
ORDER BY SECUENCIA
UPDATE @SALIDA SET MONTO_MON_CTA = (MONTO_MON_CTA + (SELECT MONTO_MON_CTA FROM @SALIDA S WHERE S.ID = ID AND PADRE IS NOT NULL)) WHERE ID = (SELECT PADRE FROM @SALIDA S WHERE S.ID = ID AND PADRE IS NOT NULL)
DELETE FROM @SALIDA WHERE PADRE IS NOT NULL

INSERT @TABLA_SALIDA
select ID,
 s.DSC_ARBOL_CLASE_INST as dsc_arbol,
 NIVEL,
 round(MONTO_MON_CTA,@decimales) as monto_mon_cta,
 round(MONTO_MON_CTA_ANTERIOR,@decimales) as monto_mon_cta_anterior,
 round(VALOR_HOJA,@decimales) AS VALOR_HOJA,
 padre,
 --ab.ID_ARBOL_CLASE_INST,
 ab.ID_EMPRESA,
 ab.ID_PADRE_ARBOL_CLASE_INST,
 CASE 
	WHEN ab.DSC_ARBOL_CLASE_INST = 'Fondos Mutuos Corto Plazo' THEN 'Fondos Mutuos'
	WHEN ab.DSC_ARBOL_CLASE_INST = 'Fondos Mutuos Largo Plazo' THEN 'Fondos Mutuos'
	WHEN ab.DSC_ARBOL_CLASE_INST = 'Forwards' THEN 'Derivado/Venta Corta'
	WHEN ab.DSC_ARBOL_CLASE_INST = 'Venta Corta' THEN 'Derivado/Venta Corta'
	ELSE ab.DSC_ARBOL_CLASE_INST
 END DSC_ARBOL_CLASE_INST,
-- ab.ORDEN,
 --ab.CODIGO,
 ab.ID_ACI_TIPO,
round(isnull(dbo.Pkg_Global$MONTO_MON_CUENTA_HOJA_POR_MONEDA(id_arbol_clase_inst, @Pid_Cuenta, @PFecha_Cartola, @Pid_moneda_salida),0),@Decimales) AS MONTO_ACTUAL,
round(isnull(dbo.Pkg_Global$MONTO_MON_CUENTA_HOJA_POR_MONEDA(id_arbol_clase_inst, @Pid_Cuenta, @PFecha_Anterior, @Pid_moneda_salida),0),@Decimales) AS MONTO_ANTERIOR,
 @PFecha_Cartola as Fecha_ultimo_Cierre,
 @PFecha_Anterior as Fecha_Mes_Anterior
 from @SALIDA s,
  arbol_clase_instrumento ab
where s.id = ab.ID_PADRE_ARBOL_CLASE_INST
--group by id
--		, s.DSC_ARBOL_CLASE_INST
--		, nivel
--		, monto_mon_cta
--		, monto_mon_cta_anterior
--		, valor_hoja
--		, padre
--		,ab.ID_EMPRESA
--		,ab.ID_PADRE_ARBOL_CLASE_INST
--		,ab.DSC_ARBOL_CLASE_INST
--		,ab.id_aci_tipo
--		,ab.ORDEN
order by id,ab.ORDEN


SELECT ID
	, DSC_ARBOL
	,NIVEL
	,MONTO_MON_CTA
	,MONTO_MON_CTA_ANTERIOR
	,VALOR_HOJA
	,PADRE
	,ID_EMPRESA
	,ID_PADRE_ARBOL_CLASE_INST
	,DSC_ARBOL_CLASE_INST
	,ID_ACI_TIPO
	,SUM(MONTO_ACTUAL) AS MONTO_ACTUAL
	,SUM(MONTO_ANTERIOR) AS MONTO_ANTERIOR
	,FECHA_ULTIMO_CIERRE
	,FECHA_MES_ANTERIOR
FROM @TABLA_SALIDA
GROUP BY ID
	, DSC_ARBOL
	,NIVEL
	,MONTO_MON_CTA
	,MONTO_MON_CTA_ANTERIOR
	,VALOR_HOJA
	,PADRE
	,ID_EMPRESA
	,ID_PADRE_ARBOL_CLASE_INST
	,DSC_ARBOL_CLASE_INST
	,ID_ACI_TIPO
	--,SUM(MONTO_ACTUAL)
	--,SUM(MONTO_ANTERIOR)
	,FECHA_ULTIMO_CIERRE
	,FECHA_MES_ANTERIOR

GO
GRANT EXECUTE ON [PKG_BALANCE_INVERSIONES_III$BUSCAR_POR_CUENTA] TO DB_EXECUTESP
GO
