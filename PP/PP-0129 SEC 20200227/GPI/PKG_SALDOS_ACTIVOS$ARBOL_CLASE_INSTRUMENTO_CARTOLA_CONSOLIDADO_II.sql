USE [CSGPI]
GO
IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA_CONSOLIDADO_II')
   DROP PROCEDURE [PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA_CONSOLIDADO_II]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA_CONSOLIDADO_II]
( @PID_CUENTA  NUMERIC = NULL,        
  @PID_EMPRESA NUMERIC = NULL,        
  @PFECHA_CIERRE DATETIME,  
  @PID_CLIENTE NUMERIC = NULL,  
  @PID_GRUPO   NUMERIC = NULL,  
  @PID_MONEDA_SALIDA NUMERIC = NULL,  
  @PCONSOLIDADO VARCHAR(3) = NULL        
) AS         
BEGIN         
    SET NOCOUNT ON         
    DECLARE @LDECIMALES NUMERIC
    DECLARE @LID_MONEDA_SALIDA NUMERIC(04)  
    SET @LID_MONEDA_SALIDA = (SELECT ID_MONEDA FROM CUENTAS WHERE ID_CUENTA = @PID_CUENTA)    
  
    SET @PID_MONEDA_SALIDA = ISNULL(@PID_MONEDA_SALIDA,@LID_MONEDA_SALIDA)      

    DECLARE @SALIDA TABLE (ID                        NUMERIC,      
                           ID_PADRE                  NUMERIC,      
                           DSC_ARBOL_CLASE_INST_RAMA VARCHAR(100),      
                           DSC_ARBOL_CLASE_INST_HOJA VARCHAR(100),      
                           MONTO_MON_CTA_INST        NUMERIC(18,2),  
                           MONTO_MON_CTA_HOJA        NUMERIC(18,2),      
                           CODIGO                    VARCHAR(20),    
                           NIVEL                     INT,
                           ORDEN                     NUMERIC,          
                           CODIGO_PADRE              VARCHAR(20))     

    DECLARE @Z TABLE (ID_Z NUMERIC,  
                      DSC_ARBOL_RAMA VARCHAR(100),  
                      DSC_ARBOL  VARCHAR(100),  
                      PADRE NUMERIC,  
                      CODIGO VARCHAR(10),
                      ORDEN          NUMERIC         )  
  
  
  
    DECLARE @Z_2 TABLE (ID_Z_2 NUMERIC,  
                        DSC_ARBOL_RAMA_2 VARCHAR(100),  
                        DSC_ARBOL_2  VARCHAR(100),  
                        PADRE_2 NUMERIC,  
                        CODIGO_2 VARCHAR(10),
                        ORDEN            NUMERIC    )     
  
    DECLARE @ARBOL TABLE (ID_ARBOL NUMERIC,  
                          SECUENCIA VARCHAR(100),  
                          NIVEL  NUMERIC,  
                          VALOR_RAMA NUMERIC,  
                          VALOR_HOJA NUMERIC  )   

    SELECT @LDECIMALES=DICIMALES_MOSTRAR 
      FROM MONEDAS
     WHERE ID_MONEDA = (SELECT ID_MONEDA FROM CUENTAS WHERE ID_CUENTA=@PID_CUENTA)

   IF @PCONSOLIDADO = 'CTA'
    BEGIN
		INSERT @Z       
		SELECT ID_ARBOL_CLASE_INST AS ID,
               DSC_ARBOL_CLASE_INST AS DSC_ARBOL_RAMA,      
               DSC_ARBOL_CLASE_INST AS DSC_ARBOL,      
               ID_PADRE_ARBOL_CLASE_INST AS PADRE,  
               CODIGO,   
               ORDEN  
          FROM ARBOL_CLASE_INSTRUMENTO  
         WHERE ID_EMPRESA = @PID_EMPRESA
         AND ID_ACI_TIPO = 2  
    END
   IF @PCONSOLIDADO = 'CLT'
    BEGIN
       IF @PID_EMPRESA IS NULL
       BEGIN
		INSERT @Z       
		SELECT ID_ARBOL_CLASE_INST AS ID, 
               DSC_ARBOL_CLASE_INST AS DSC_ARBOL_RAMA,
               DSC_ARBOL_CLASE_INST AS DSC_ARBOL,      
               ID_PADRE_ARBOL_CLASE_INST AS PADRE,  
               CODIGO,   
               ORDEN  
        FROM ARBOL_CLASE_INSTRUMENTO  
        WHERE ID_EMPRESA IN (SELECT ID_EMPRESA FROM VIEW_CUENTAS_VIGENTES WHERE ID_CLIENTE = @PID_CLIENTE)
        AND ID_ACI_TIPO = 2 
       END
       ELSE
       BEGIN
		INSERT @Z       
		SELECT ID_ARBOL_CLASE_INST AS ID, 
               DSC_ARBOL_CLASE_INST AS DSC_ARBOL_RAMA,
               DSC_ARBOL_CLASE_INST AS DSC_ARBOL,      
               ID_PADRE_ARBOL_CLASE_INST AS PADRE,  
               CODIGO,   
               ORDEN  
        FROM ARBOL_CLASE_INSTRUMENTO  
        WHERE ID_EMPRESA = @PID_EMPRESA
        AND ID_ACI_TIPO = 2 
       END
    END
   IF @PCONSOLIDADO = 'GRP'
    BEGIN
		INSERT @Z       
		SELECT ID_ARBOL_CLASE_INST AS ID,
               DSC_ARBOL_CLASE_INST AS DSC_ARBOL_RAMA,      
               DSC_ARBOL_CLASE_INST AS DSC_ARBOL,      
               ID_PADRE_ARBOL_CLASE_INST AS PADRE,  
               CODIGO,   
               ORDEN  
          FROM ARBOL_CLASE_INSTRUMENTO  
         WHERE ID_EMPRESA IN (SELECT DISTINCT ID_EMPRESA 
                                FROM VIEW_CUENTAS_VIGENTES 
                               WHERE ID_CUENTA IN (SELECT ID_CUENTA
                                                     FROM REL_CUENTAS_GRUPOS_CUENTAS
                                                    WHERE ID_GRUPO_CUENTA = @PID_GRUPO))
         AND ID_ACI_TIPO = 2                                            
    END
  
    INSERT @Z_2  
    SELECT * FROM @Z    
  
    INSERT @ARBOL      
    SELECT ID_Z,       
           RIGHT(SPACE(10) + CONVERT(VARCHAR(10),ID_Z),10),       
           1,      
           ISNULL(dbo.Pkg_Global$MONTO_MON_CUENTA_RAMA_CONSOLIDADO_I_VR2(ID_Z
															        , @PID_CUENTA
                                                                   , @PFECHA_CIERRE
                                                                   , @PID_CLIENTE
                                                                   , @PID_GRUPO
                                                                   , @PID_MONEDA_SALIDA
                                                                   , @PCONSOLIDADO),0) AS VALOR_RAMA,      
           0      
      FROM @Z WHERE PADRE IS NULL      
      
    DECLARE @I INT      
    SELECT @I = 0      
      
    WHILE @@ROWCOUNT > 0      
    BEGIN      
          SELECT @I = @I + 1      
          INSERT @ARBOL      
          SELECT ID_Z,       
                 SECUENCIA + RIGHT(SPACE(10) + CONVERT(VARCHAR(10),ID_Z),10),       
                 @I + 1,      
                 ISNULL(dbo.Pkg_Global$MONTO_MON_CUENTA_RAMA_CONSOLIDADO_I_VR2(ID_ARBOL
                                                                         , @PID_CUENTA
                                                                         , @PFECHA_CIERRE
                                                                         , @PID_CLIENTE
                                                                         , @PID_GRUPO
                                                                         , @PID_MONEDA_SALIDA
                                                                         , @PCONSOLIDADO),0),
                 0      
            FROM @Z, @ARBOL       
           WHERE NIVEL = @I      
             AND PADRE = ID_ARBOL      
     END      
      
      
     UPDATE @ARBOL  SET VALOR_HOJA = ISNULL(dbo.Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_I_VR2(ID_ARBOL
                                                                                             , @PID_CUENTA
                                                                                             , @PFECHA_CIERRE
                                                                                             , @PID_CLIENTE
                                                                                             , @PID_GRUPO
                                                                                             , @PID_MONEDA_SALIDA
                                                                                             , @PCONSOLIDADO),0) 
      WHERE NIVEL   <> 1       

     UPDATE @ARBOL  SET VALOR_HOJA = ( SELECT SUM(VALOR_HOJA) FROM @ARBOL , @Z       
                                        WHERE ID_ARBOL = ID_Z      
                                          AND  PADRE = ID_ARBOL      
                                     )       
      WHERE (SELECT COUNT(*) FROM @Z WHERE PADRE = ID_ARBOL) > 1      
      
      
     UPDATE @Z SET DSC_ARBOL_RAMA = (SELECT DSC_ARBOL FROM @ARBOL , @Z_2 WHERE ID_ARBOL = ID_Z_2 AND ID_ARBOL = PADRE_2)      
      
     INSERT INTO @SALIDA       
     SELECT ID_Z,      
            PADRE,      
            (SELECT DSC_ARBOL_2 FROM @ARBOL , @Z_2  WHERE ID_ARBOL = ID_Z_2 AND ID_ARBOL = PADRE) ,      
            DSC_ARBOL,      
            ISNULL(dbo.Pkg_Global$MONTO_MON_CUENTA_RAMA_CONSOLIDADO_I_VR2(PADRE, @PID_CUENTA,  
                                                                      @PFECHA_CIERRE,  
                                                                      @PID_CLIENTE,  
                                                                      @PID_GRUPO,  
                                                                      @PID_MONEDA_SALIDA,  
                                                                      @PCONSOLIDADO),0),      
            VALOR_HOJA,      
            CODIGO,    
            NIVEL,
            ORDEN,
            (SELECT CODIGO_2 FROM @Z_2   WHERE ID_Z_2= case when padre is null then ID_Z else padre end)       
       FROM @ARBOL, @Z       
      WHERE ID_ARBOL = ID_Z      
      ORDER BY SECUENCIA      
      
     DELETE FROM @SALIDA WHERE DSC_ARBOL_CLASE_INST_RAMA IS NULL       
      
	 IF @PCONSOLIDADO = 'CTA'
     BEGIN
		  SELECT ID_PADRE AS ID_ARBOL_CLASE_INST,      
                 ID   AS ID_ARBOL_CLASE_INST_HOJA,      
                 DSC_ARBOL_CLASE_INST_RAMA AS DSC_ARBOL_CLASE_INST,      
                 DSC_ARBOL_CLASE_INST_HOJA AS DSC_ARBOL_CLASE_INST_HOJA,      
                 MONTO_MON_CTA_INST   AS MONTO_MON_CTA_INST,      
                 MONTO_MON_CTA_HOJA   AS  MONTO_MON_CTA_HOJA,      
                 CODIGO      AS CODIGO  ,    
                 (SELECT COUNT(*) FROM @SALIDA WHERE ID_PADRE = S.ID ) AS NIVEL ,
                 CODIGO_PADRE   
            FROM @SALIDA   S 
            ORDER BY orden  
     END
     ELSE
	 BEGIN
		SELECT MIN(ID_PADRE) AS ID_ARBOL_CLASE_INST,
               MIN(ID) AS ID_ARBOL_CLASE_INST_HOJA, 
               DSC_ARBOL_CLASE_INST_RAMA AS DSC_ARBOL_CLASE_INST,      
               DSC_ARBOL_CLASE_INST_HOJA AS DSC_ARBOL_CLASE_INST_HOJA,      
               SUM(MONTO_MON_CTA_INST) AS MONTO_MON_CTA_INST,
               SUM(MONTO_MON_CTA_HOJA) AS MONTO_MON_CTA_HOJA,
               CODIGO AS CODIGO,
               0 AS NIVEL ,
               CODIGO_PADRE   
          FROM @SALIDA S
        GROUP BY DSC_ARBOL_CLASE_INST_RAMA, DSC_ARBOL_CLASE_INST_HOJA, CODIGO, CODIGO_PADRE
        ORDER BY 1, 2
     END        
      
END
GO
GRANT EXECUTE ON [PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA_CONSOLIDADO_II] TO DB_EXECUTESP
GO