USE [CSGPI]
GO
 IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_VR2')
   DROP FUNCTION Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_VR2
GO 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_VR2]
(
    @PID_ARBOL_CLASE    NUMERIC,
    @PID_CUENTA         NUMERIC     = NULL,
    @PFECHA_CIERRE      DATETIME,
    @PID_CLIENTE        NUMERIC     = NULL,
    @PID_GRUPO          NUMERIC     = NULL,
    @PID_MONEDA_SALIDA  NUMERIC(4)  = NULL,
    @PCONSOLIDADO       VARCHAR(3)  = NULL,
    @PID_EMPRESA        NUMERIC     = NULL
)
RETURNS FLOAT
AS
BEGIN
     DECLARE @MONTO_MON_CTA FLOAT
     DECLARE @LDECIMALES NUMERIC
     DECLARE @LDSC_ARBOL VARCHAR(50)
           , @LDSC_PADRE VARCHAR(50)
           , @LID_MONEDA_USD NUMERIC
           , @LMONTO_INT     NUMERIC(28,4)
           , @LMONTO_NAC     NUMERIC(28,4)
           , @LMONTO_FWD     NUMERIC(28,4)
           , @LTOTAL_VC    NUMERIC(28,4)
          , @LTOTAL_DEVVC  NUMERIC(28,4)

     SELECT @LID_MONEDA_USD = DBO.FNT_DAMEIDMONEDA('USD')
     SET @MONTO_MON_CTA = 0
     SET @LMONTO_INT    = 0
     SET @LMONTO_NAC    = 0
     SET @LMONTO_FWD    = 0
     SET @LTOTAL_VC   = 0
     SET @LTOTAL_DEVVC  = 0
---------------------------------------------------------------------------

     DECLARE @TBLCUENTAS TABLE (ID_CUENTA NUMERIC
                              , ID_MONEDA NUMERIC
                              , ID_EMPRESA NUMERIC)
     IF @PCONSOLIDADO='CLT'
      BEGIN
         INSERT INTO @TBLCUENTAS
         SELECT ID_CUENTA, ID_MONEDA, ID_EMPRESA
           FROM CUENTAS
          WHERE COD_ESTADO = 'H'
            AND ID_CLIENTE = @PID_CLIENTE
            AND ID_EMPRESA = ISNULL(@PID_EMPRESA,ID_EMPRESA)
      END
     ELSE
      IF @PCONSOLIDADO='GRP'
       BEGIN
         INSERT INTO @TBLCUENTAS
         SELECT ID_CUENTA, ID_MONEDA, ID_EMPRESA
           FROM CUENTAS
          WHERE COD_ESTADO = 'H'
            AND ID_CUENTA IN (SELECT ID_CUENTA FROM REL_CUENTAS_GRUPOS_CUENTAS
                              WHERE ID_GRUPO_CUENTA = @PID_GRUPO)
            AND ID_EMPRESA = ISNULL(@PID_EMPRESA,ID_EMPRESA)
       END
      ELSE
          INSERT INTO @TBLCUENTAS
          SELECT ID_CUENTA, ID_MONEDA, ID_EMPRESA
            FROM CUENTAS
           WHERE ID_CUENTA = @PID_CUENTA

---------------------------------------------------------------------------
    DECLARE @TBLACI TABLE (ID_ARBOL_CLASE_INST NUMERIC)

     SELECT @LDSC_ARBOL = A.DSC_ARBOL_CLASE_INST
          , @LDSC_PADRE = (SELECT DSC_ARBOL_CLASE_INST
                             FROM ARBOL_CLASE_INSTRUMENTO
                            WHERE ID_ARBOL_CLASE_INST= A.ID_PADRE_ARBOL_CLASE_INST)
       FROM ARBOL_CLASE_INSTRUMENTO A
      WHERE A.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE


     SELECT @LDECIMALES = DICIMALES_MOSTRAR
       FROM MONEDAS
      WHERE ID_MONEDA=@PID_MONEDA_SALIDA

    IF @PCONSOLIDADO <> 'CTA'
     BEGIN
          INSERT INTO @TBLACI
          SELECT ID_ARBOL_CLASE_INST
          FROM (SELECT ID_ARBOL_CLASE_INST, CODIGO
                     , ID_EMPRESA
                     , DSC_ARBOL_CLASE_INST
                     , (SELECT DSC_ARBOL_CLASE_INST
                          FROM ARBOL_CLASE_INSTRUMENTO
                         WHERE ID_ARBOL_CLASE_INST= A.ID_PADRE_ARBOL_CLASE_INST)    DSC_PADRE
          FROM ARBOL_CLASE_INSTRUMENTO A  ) T
         WHERE DSC_ARBOL_CLASE_INST = @LDSC_ARBOL
           AND DSC_PADRE =@LDSC_PADRE
           AND ID_EMPRESA IN (SELECT DISTINCT ID_EMPRESA FROM @TBLCUENTAS)

     END
    ELSE
          INSERT INTO @TBLACI
          SELECT @PID_ARBOL_CLASE


    DECLARE @TBLNEMOS TABLE (ID_NEMOTECNICO NUMERIC)
    INSERT INTO @TBLNEMOS
    SELECT DISTINCT ID_NEMOTECNICO
      FROM REL_ACI_EMP_NEMOTECNICO
     WHERE ID_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST FROM @TBLACI)

     SET @LMONTO_INT     = 0
     SET @LMONTO_NAC     = 0
     SET @LMONTO_FWD     = 0
---------------------------------------------------------------------------

      IF @LDSC_ARBOL = 'Forwards' or  @LDSC_ARBOL = 'Venta Corta'
      BEGIN
    IF @LDSC_ARBOL = 'Forwards'
     BEGIN

         SELECT @LMONTO_FWD = ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SD.ID_CUENTA
                                                                              , SD.VMM
                                                                              , DBO.FNT_DAMEIDMONEDA('$$')
                                                                              , @PID_MONEDA_SALIDA
                                                                              , @PFECHA_CIERRE)) ,0)
           FROM VIEW_SALDOS_DERIVADOS SD,
                @TBLCUENTAS C
          WHERE SD.ID_CUENTA       = C.ID_CUENTA
            AND SD.FECHA_CIERRE    = @PFECHA_CIERRE
            AND SD.COD_INSTRUMENTO = 'FWD_NAC'
          END

         IF @LDSC_ARBOL = 'Venta Corta'
          BEGIN
SELECT @LTOTAL_VC = SUM(
                                   ISNULL((V.CANTIDAD * V.PRECIO_MEDIO), 0) -
                                   ISNULL((V.CANTIDAD * VCD.PRECIO_MEDIO_MERCADO), 0) -
                                   ISNULL(VCD.PRIMA_ACUMULADA, 0)
                                   )
              FROM VENTA_CORTA V,
                   VENTA_CORTA_DEVENGO VCD
             WHERE V.ID_CUENTA IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
               AND @PFECHA_CIERRE BETWEEN V.FECHA_DE_MVTO AND V.FECHA_VENCIMIENTO
               AND V.COD_ESTADO = 'C'
               AND V.ID_VTA_CORTA = VCD.ID_VTA_CORTA
               AND V.FOLIO = VCD.FOLIO
               AND V.FECHA_DE_MVTO = VCD.FECHA_DE_MVTO
               AND VCD.FECHA_DE_DEVENGO = @PFECHA_CIERRE

          END

         SET @LTOTAL_DEVVC = @LMONTO_FWD + @LTOTAL_VC
--         IF @LMONTO_FWD < 0
--          BEGIN
--               SET @MONTO_MON_CTA = 0
--          END
--         ELSE
--          BEGIN
               SET @MONTO_MON_CTA =  @LTOTAL_DEVVC
--          END


      END
     ELSE
      BEGIN
            IF UPPER(@LDSC_PADRE) = 'RENTA VARIABLE INTERNACIONAL' OR UPPER(@LDSC_PADRE) = 'RENTA FIJA INTERNACIONAL' OR
              (UPPER(@LDSC_PADRE) = 'OTROS ACTIVOS'  AND UPPER(@LDSC_ARBOL) = 'FONDOS DE INVERSI”N') OR
              (UPPER(@LDSC_PADRE) = 'OTROS ACTIVOS'  AND UPPER(@LDSC_ARBOL) = 'NOTAS ESTRUCTURADAS')
             BEGIN
                  SELECT @LMONTO_INT = ISNULL(SUM(ROUND(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(S.ID_CUENTA
                                                                                             , S.VALOR_MERCADO_MON_USD
                                                                                             , @LID_MONEDA_USD
                                                                                             , @PID_MONEDA_SALIDA
                                                                                             , @PFECHA_CIERRE),@LDECIMALES)),0)
                   FROM SALDOS_ACTIVOS_INT S
                      , CUENTAS C
                  WHERE C.ID_CUENTA      IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
                    AND S.ID_CUENTA      = C.ID_CUENTA
                    AND S.FECHA_CIERRE   = @PFECHA_CIERRE
                    AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO  FROM @TBLNEMOS)
             END

            SELECT @LMONTO_NAC = ISNULL(SUM(ROUND(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA
                                                                                 , SA.MONTO_MON_CTA
                                                                                 , C.ID_MONEDA
                                                                                 , @PID_MONEDA_SALIDA
                                                                                 , @PFECHA_CIERRE),@LDECIMALES)),0)
              FROM SALDOS_ACTIVOS        SA,
                   @TBLCUENTAS C
             WHERE FECHA_CIERRE   = @PFECHA_CIERRE
               AND SA.ID_CUENTA   = C.ID_CUENTA
               AND ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO  FROM @TBLNEMOS)
            SET @MONTO_MON_CTA = ISNULL(@LMONTO_NAC,0) + ISNULL(@LMONTO_INT,0)
  END
     RETURN @MONTO_MON_CTA
END
GO 
GRANT EXECUTE ON [Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_VR2] TO DB_EXECUTESP
GO