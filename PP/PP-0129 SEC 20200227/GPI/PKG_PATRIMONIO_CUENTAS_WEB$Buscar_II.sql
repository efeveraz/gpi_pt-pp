USE [CSGPI]
GO
IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PKG_PATRIMONIO_CUENTAS_WEB$Buscar_II')
   DROP PROCEDURE [PKG_PATRIMONIO_CUENTAS_WEB$Buscar_II]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PKG_PATRIMONIO_CUENTAS_WEB$Buscar_II]      
( @Pid_cuenta FLOAT
, @Pfecha_cierre DATETIME
, @pId_Moneda_Salida NUMERIC = NULL)        
AS      
BEGIN        
    SET NOCOUNT ON      
    DECLARE @LPAT_ANTERIOR    FLOAT
        , @LSMLT            FLOAT
        , @LFECHA_ULT_CARGO DATETIME
        , @LCOMISION        FLOAT
        , @LFECHA_CARGO     DATETIME

    SELECT @LFECHA_ULT_CARGO = MAX(FECHA_TER) 
    FROM COMI_HONO_ASE_CTA_CARGO 
    WHERE (FCH_COMI_HA_CTA_CARGO = @PFECHA_CIERRE OR FECHA_TER <= @PFECHA_CIERRE)
    --WHERE FECHA_TER <= @PFECHA_CIERRE
        AND ID_CUENTA = @Pid_cuenta
     
    IF @LFECHA_ULT_CARGO IS NULL
    BEGIN
        SELECT @LCOMISION=ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta 
                                                                                        , COMISION_HONORARIOS
                                                                                        , CTA.id_moneda
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)  
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) ),0)
        FROM COMISIONES_HONO_ASE_CUENTA C
            , CUENTAS CTA  
        WHERE CTA.ID_CUENTA  = @Pid_cuenta  
            AND C.ID_CUENTA   = CTA.ID_CUENTA
            AND FECHA_CIERRE <= @PFECHA_CIERRE 
    END
    ELSE
    BEGIN
        IF (SELECT COUNT(*) FROM COMI_HONO_ASE_CTA_CARGO WHERE (FCH_COMI_HA_CTA_CARGO = @PFECHA_CIERRE OR FECHA_TER = @PFECHA_CIERRE) AND ID_CUENTA = @Pid_cuenta) <> 0
        BEGIN
            SET @LCOMISION=0
        END
        ELSE
        BEGIN
            SET @LFECHA_ULT_CARGO = DATEADD(DAY,1,@LFECHA_ULT_CARGO)
            SELECT @LCOMISION=ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta 
                                                                                        , COMISION_HONORARIOS
                                                                                        , CTA.id_moneda
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)  
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) ),0)
            FROM COMISIONES_HONO_ASE_CUENTA C
                , CUENTAS CTA  
            WHERE CTA.ID_CUENTA  = @Pid_cuenta  
                AND C.ID_CUENTA   = CTA.ID_CUENTA
                AND FECHA_CIERRE >= @LFECHA_ULT_CARGO
                AND FECHA_CIERRE <=@PFECHA_CIERRE 
        END
    END
   
    SELECT @LPAT_ANTERIOR = DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(id_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(id_cuenta
                                                                                        , patrimonio_mon_cuenta       
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)      
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre)
    FROM PATRIMONIO_CUENTAS      
    WHERE ID_CUENTA = @Pid_cuenta      
        AND FECHA_CIERRE = dbo.Pkg_Global$UltimoDiaMesAnterior(@Pfecha_cierre)

    SELECT @LSMLT = ISNULL( SUM( DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad( SS.ID_CUENTA                  
                                                                        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(S.ID_CUENTA          
                                                                                        , CASE WHEN S.MONTO_FUTURO < 0 THEN S.MONTO_FUTURO *  -1
                                                                                        ELSE  S.MONTO_FUTURO
                                                                                        END       
                                                                                        , SS.ID_MONEDA          
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')          
                                                                                        , SS.FECHA_CIERRE)              
                                                                        , dbo.FNT_DAMEIDMONEDA('$$')                 
                                                                        , @pid_Moneda_salida                  
                                                                        , SS.FECHA_CIERRE                   
                                                                        )), 0)         
    FROM SIMULTANEAS_SALDO SS        
        , SIMULTANEAS S        
    WHERE SS.ID_CUENTA = @PID_CUENTA
		AND S.ID_CUENTA = SS.ID_CUENTA
        AND SS.FECHA_CIERRE = @PFECHA_CIERRE        
        AND S.ID_SIMULTANEA = SS.ID_SIMULTANEA        
        AND S.FLG_TIPO_OPERACION = 'V'  
        
        
        
     SELECT ID_PATRIMONIO_CUENTA
        , id_cuenta
        , fecha_cierre
        , id_moneda_cuenta
        , COMI_DEVENG_MON_CTA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , saldo_caja_mon_cuenta
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS SALDO_CAJA_MON_CUENTA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , saldo_activo_mon_cuenta
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS SALDO_ACTIVO_MON_CUENTA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta
                                        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta
                                                                               , patrimonio_mon_cuenta
                                                                               , id_moneda_cuenta
                                                                               , dbo.FNT_DAMEIDMONEDA('$$')
                                                                               , @pFecha_Cierre)
                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                        , dbo.FNT_DAMEIDMONEDA('UF')
                                        , @pFecha_Cierre) AS PATRIMONIO_UF
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta
                                                                                        , patrimonio_mon_cuenta
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , dbo.FNT_DAMEIDMONEDA('USD')
                                                 , @pFecha_Cierre) AS PATRIMONIO_USD
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , patrimonio_mon_cuenta
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS PATRIMONIO_MON_CUENTA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta
                                                 , patrimonio_mon_cuenta
                                                 , id_moneda_cuenta
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pFecha_Cierre) AS PATRIMONIO_$$
        , patrimonio_mon_cuenta patrimonio_mon_cuenta_mda_cta
        , id_moneda_empresa
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , saldo_caja_mon_empresa
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre) 
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS SALDO_CAJA_MON_EMPRESA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , saldo_activo_mon_empresa
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre) 
                                                 , dbo.FNT_DAMEIDMONEDA('$$'), @pid_Moneda_salida, @pFecha_Cierre) AS SALDO_ACTIVO_MON_EMPRESA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , patrimonio_mon_empresa
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre) 
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS PATRIMONIO_MON_EMPRESA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , valor_cuota_mon_cuenta
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)  
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS VALOR_CUOTA_MON_CUENTA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , total_cuotas_mon_cuenta
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)  
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS TOTAL_CUOTAS_MON_CUENTA
        , rentabilidad_mon_cuenta as rentabilidad_mon_cuenta
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , aporte_retiros_mon_cuenta
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)  
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS APORTE_RETIROS_MON_CUENTA
        , id_cierre
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , monto_x_cobrar_mon_cta
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)  
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS MONTO_X_COBRAR_MON_CTA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , monto_x_pagar_mon_cta
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)  
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS MONTO_X_PAGAR_MON_CTA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , monto_x_cobrar_mon_empresa
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)  
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida,@pFecha_Cierre) AS MONTO_X_COBRAR_MON_EMPRESA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                                                        , monto_x_pagar_mon_empresa
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)  
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS MONTO_X_PAGAR_MON_EMPRESA
        , @LPAT_ANTERIOR as patrimonio_mes_anterior
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta
                                                 , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@pid_cuenta 
                                                                                        , comi_deveng_mon_cta
                                                                                        , id_moneda_cuenta
                                                                                        , dbo.FNT_DAMEIDMONEDA('$$')
                                                                                        , @pFecha_Cierre)  
                                                 , dbo.FNT_DAMEIDMONEDA('$$')
                                                 , @pid_Moneda_salida
                                                 , @pFecha_Cierre) AS COMISIONES_DEVENGADAS        
   
        , @LCOMISION   as comision    
        , @LSMLT   SIMULTANEAS 
    FROM PATRIMONIO_CUENTAS PC        
    WHERE ID_CUENTA = @Pid_cuenta
        AND FECHA_CIERRE = @Pfecha_cierre  
         
    SET NOCOUNT OFF              
  
END      
GO
GRANT EXECUTE ON [PKG_PATRIMONIO_CUENTAS_WEB$Buscar_II] TO DB_EXECUTESP
GO