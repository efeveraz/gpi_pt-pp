USE[CSGPI]
GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_SALDOS_SIMULTANEAS_WEB$BUSCAR]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_SALDOS_SIMULTANEAS_WEB$BUSCAR]
GO
CREATE PROCEDURE [dbo].[PKG_SALDOS_SIMULTANEAS_WEB$BUSCAR]
( @PID_ENTIDAD       NUMERIC  = NULL          
, @PID_EMPRESA       NUMERIC  = NULL          
, @PCONSOLIDADO      VARCHAR(3) = 'CTA'          
, @PFECHA_CIERRE     DATETIME            
, @PID_MONEDA_SALIDA NUMERIC = NULL              
)AS                
BEGIN                
     SET NOCOUNT ON      
     DECLARE @PMSGERROR VARCHAR(MAX)
     
     DECLARE @TBLCUENTAS TABLE (ID_CUENTA NUMERIC)          
     IF @PCONSOLIDADO = 'CTA'          
      BEGIN          
           INSERT INTO @TBLCUENTAS          
           SELECT ID_CUENTA          
             FROM CUENTAS          
            WHERE COD_ESTADO='H'          
              AND ID_CUENTA = ISNULL(@PID_ENTIDAD,ID_CUENTA)          
              AND ID_EMPRESA = ISNULL(@PID_EMPRESA,ID_EMPRESA)          
      END          
     
     BEGIN TRY
     
                  
            IF ISNULL(@PID_MONEDA_SALIDA,0) = 0
             BEGIN
               SELECT @PID_MONEDA_SALIDA = DBO.FNT_DAMEIDMONEDA('$$')
             END

            SELECT 'SIMULTANEAS' AS 'ORIGEN'
          , 'PASIVOS'  AS 'BLOQUE'
          , @PID_ENTIDAD AS 'ID_CUENTA'
          , SS.FECHA_CIERRE                       AS 'FECHA_CIERRE'
          , M.SIMBOLO	                          AS 'DSC_MONEDA'
          , S.CANTIDAD                            AS 'CANTIDAD_OPERACION'
          , SS.PRECIO                              AS 'PRECIO_OPERACION'
          , (S.CANTIDAD * SS.PRECIO)               AS 'MONTO_OPERACION'
          , (SA.CANTIDAD -  S.CANTIDAD)           AS 'CANTIDAD_DISPONIBLE'
          , (SA.CANTIDAD)						  AS 'CANTIDAD_TOTAL'
          , S.PRECIO_FUTURO                       AS 'PRECIO_FINAL'
          , (SS.CANTIDAD * S.PRECIO_FUTURO)       AS 'MONTO_FINAL'
          , CASE 
				WHEN SS.TOTAL < 0 THEN SS.TOTAL * -1
				ELSE
				SS.TOTAL
				END                       AS 'MONTO_DEVENGADO'
          , SS.VALOR_MERCADO_MON_CTA              AS 'VALOR_MERCADO'
          , SS.CAPITAL                            AS 'CAPITAL'
          , SS.PRECIO                             AS 'PRECIO_DEVENG'
          , SS.INTERES                            AS 'INTERES_DEVENG'
          , SS.PRECIO_MERCADO                     AS 'PRECIO_MERCADO'
          , SS.ID_MONEDA_CTA                      AS 'ID_MONEDA_CUENTA'
          , S.FOLIO                               AS 'FOLIO'
          , S.FLG_TIPO_OPERACION                  AS 'FLG_TIPO_OPERACION'
          , S.FCH_SIMULTANEA                     AS 'FECHA_SIMULTANEA'
          , S.FCH_VENCIMIENTO                     AS 'FECHA_VENCIMIENTO'
          , S.ID_NEMOTECNICO                      AS 'ID_NEMOTECNICO'
          , N.DSC_EMISOR_ESPECIFICO               AS 'DSC_EMISOR'
          , N.NEMOTECNICO                         AS 'NEMOTECNICO'
          , N.DSC_NEMOTECNICO                     AS 'DSC_NEMOTECNICO'
          , S.TASA                                AS 'TASA'
          , S.MONTO_FUTURO                        AS 'MONTO_FUTURO'
          , ISNULL(((S.MONTO - SS.TOTAL_MON_CTA) / NULLIF(SS.TOTAL_MON_CTA,0) ) * 100,0) AS 'VARIACION_PRECIO'
          , N.DSC_EMISOR_ESPECIFICO               AS 'DSC_EMISOR_ESPECIFICO'
          , S.BOLSA								  AS 'BOLSA'
		  , SS.TOTAL_MON_CTA					  AS 'MONTO_CIERRE'
       FROM @TBLCUENTAS T
          , SIMULTANEAS_SALDO SS
          , SIMULTANEAS S
          , VIEW_NEMOTECNICOS N
          , MONEDAS M
          , SALDOS_ACTIVOS SA
      WHERE S.ID_CUENTA = T.ID_CUENTA
        AND SA.ID_CUENTA = T.ID_CUENTA
        AND SS.FECHA_CIERRE = @PFECHA_CIERRE
        AND SA.FECHA_CIERRE = @PFECHA_CIERRE
        AND S.ID_SIMULTANEA = SS.ID_SIMULTANEA
        AND S.FLG_TIPO_OPERACION = 'V'
        AND N.ID_NEMOTECNICO = S.ID_NEMOTECNICO
        AND SA.ID_NEMOTECNICO = S.ID_NEMOTECNICO
        AND M.ID_MONEDA = SS.ID_MONEDA
     ORDER BY NEMOTECNICO

                    
     END TRY                
 BEGIN CATCH                
  SET @PMSGERROR = ERROR_MESSAGE()                
  RAISERROR (@PMSGERROR,10, 1)                 
 END CATCH                
 SET NOCOUNT OFF      
END

GO 
GRANT EXECUTE ON [PKG_SALDOS_SIMULTANEAS_WEB$BUSCAR] TO DB_EXECUTESP
GO