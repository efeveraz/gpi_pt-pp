USE CSGPI
GO

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'Pkg_Global$MONTO_MON_CUENTA_RAMA_CONSOLIDADO_vr2')
   DROP FUNCTION Pkg_Global$MONTO_MON_CUENTA_RAMA_CONSOLIDADO_vr2
GO

CREATE FUNCTION [dbo].[Pkg_Global$MONTO_MON_CUENTA_RAMA_CONSOLIDADO_vr2]
(
    @PID_ARBOL_CLASE    NUMERIC,
    @PID_CUENTA         NUMERIC     = NULL,
    @PFECHA_CIERRE      DATETIME,
    @PID_CLIENTE        NUMERIC     = NULL,
    @PID_GRUPO          NUMERIC     = NULL,
    @PID_MONEDA_SALIDA  NUMERIC     = NULL,
    @PCONSOLIDADO       VARCHAR(3)  = NULL,
    @PID_EMPRESA        NUMERIC     = NULL
 )
 RETURNS FLOAT
AS
BEGIN
    DECLARE @MONTO_MON_CTA FLOAT

    DECLARE @LDECIMALES      NUMERIC
    DECLARE @LDSC_ARBOL      VARCHAR(50)
          , @LDSC_PADRE      VARCHAR(50)
          , @LID_EMPRESA     NUMERIC
          , @LID_MONEDA_USD  NUMERIC
          , @LID_MONEDA_PESO NUMERIC
          , @LTOTAL_FWD      NUMERIC(28,4)
          , @LTOTAL_INT      NUMERIC(28,4)
          , @LTOTAL_NAC      NUMERIC(28,4)
          , @LTOTAL_VC   NUMERIC(28,4)
          , @LTOTAL_DEVVC  NUMERIC(28,4)


 SET @MONTO_MON_CTA = 0
 SET @LTOTAL_FWD    = 0
 SET @LTOTAL_VC  = 0
 SET @LTOTAL_DEVVC = 0
 SET @LTOTAL_INT    = 0
 SET @LTOTAL_NAC    = 0
 SELECT @LID_MONEDA_PESO = DBO.FNT_DAMEIDMONEDA('$$')
 SELECT @LID_MONEDA_USD = DBO.FNT_DAMEIDMONEDA('USD')

--------------------------------------------------------------------------------------
  DECLARE @TBLCUENTAS TABLE (ID_CUENTA NUMERIC
                           , ID_MONEDA NUMERIC
                           , ID_EMPRESA NUMERIC)
  IF @PCONSOLIDADO='CLT'
   BEGIN
       INSERT INTO @TBLCUENTAS
       SELECT ID_CUENTA, ID_MONEDA, ID_EMPRESA
         FROM CUENTAS
        WHERE COD_ESTADO = 'H'
          AND ID_CLIENTE = @PID_CLIENTE
          AND ID_EMPRESA = ISNULL(@PID_EMPRESA,ID_EMPRESA)
   END
  ELSE
      IF @PCONSOLIDADO='GRP'
       BEGIN
            INSERT INTO @TBLCUENTAS
            SELECT ID_CUENTA, ID_MONEDA, ID_EMPRESA
              FROM CUENTAS
             WHERE COD_ESTADO = 'H'
               AND ID_CUENTA IN (SELECT ID_CUENTA FROM REL_CUENTAS_GRUPOS_CUENTAS
                                 WHERE ID_GRUPO_CUENTA = @PID_GRUPO)
               AND ID_EMPRESA = ISNULL(@PID_EMPRESA,ID_EMPRESA)
       END
      ELSE
       BEGIN
            INSERT INTO @TBLCUENTAS
            SELECT ID_CUENTA, ID_MONEDA, ID_EMPRESA
              FROM CUENTAS
            WHERE ID_CUENTA = @PID_CUENTA
              AND ID_EMPRESA = ISNULL(@PID_EMPRESA,ID_EMPRESA)
       END
--------------------------------------------------------------------------------------
    SELECT @LDECIMALES = DICIMALES_MOSTRAR
      FROM MONEDAS
     WHERE ID_MONEDA = @PID_MONEDA_SALIDA

    DECLARE @TBLACI TABLE (ID_ARBOL_CLASE_INST NUMERIC)

   SELECT @LDSC_PADRE = DSC_ARBOL_CLASE_INST
        , @LID_EMPRESA = A.ID_EMPRESA
     FROM ARBOL_CLASE_INSTRUMENTO A
    WHERE A.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE

    IF @PCONSOLIDADO <> 'CTA'
     BEGIN
          INSERT INTO @TBLACI
          SELECT ID_ARBOL_CLASE_INST
           FROM ARBOL_CLASE_INSTRUMENTO
          WHERE DSC_ARBOL_CLASE_INST = @LDSC_PADRE
          AND ID_EMPRESA IN (SELECT DISTINCT ID_EMPRESA FROM @TBLCUENTAS)
     END
    ELSE
          INSERT INTO @TBLACI
          SELECT @PID_ARBOL_CLASE

    DECLARE @TBLNEMOS TABLE (ID_NEMOTECNICO NUMERIC)
    INSERT INTO @TBLNEMOS
    SELECT DISTINCT ID_NEMOTECNICO
      FROM REL_ACI_EMP_NEMOTECNICO
     WHERE ID_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST
                                     FROM ARBOL_CLASE_INSTRUMENTO
                                    WHERE ID_PADRE_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST FROM @TBLACI))

--------------------------------------------------------------------------------------
    IF @LDSC_PADRE = 'Otros Activos'
     BEGIN

         SELECT @LTOTAL_FWD = ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SD.ID_CUENTA
                                                                              , SD.VMM
                                                                              , @LID_MONEDA_PESO
                                                                              , @PID_MONEDA_SALIDA
                                                                              , @PFECHA_CIERRE)) ,0)
           FROM VIEW_SALDOS_DERIVADOS SD
          WHERE SD.ID_CUENTA       = @pID_CUENTA
            AND SD.FECHA_CIERRE    = @PFECHA_CIERRE
            AND SD.COD_INSTRUMENTO = 'FWD_NAC'


--            SELECT @LTOTAL_VC = (ISNULL(SUM(VC.PRECIO_MEDIO_MERCADO),0) - ISNULL(SUM(VC.PRECIO_MEDIO),0)) + SUM(ISNULL(VC.PRIMA_ACUMULADA,0))
--           FROM VENTA_CORTA VC
--          WHERE VC.ID_CUENTA IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
--            AND VC.FECHA_DE_MVTO = @PFECHA_CIERRE

        SELECT @LTOTAL_VC = SUM(
                                ISNULL((V.cantidad * V.precio_medio), 0) -
                                ISNULL((V.cantidad * VCD.precio_medio_mercado), 0) -
                                ISNULL(VCD.PRIMA_ACUMULADA, 0)
                                )
          FROM VENTA_CORTA V,
               VENTA_CORTA_DEVENGO VCD
        WHERE V.ID_CUENTA      IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
          AND @PFECHA_CIERRE BETWEEN V.FECHA_DE_MVTO AND V.FECHA_VENCIMIENTO
          AND V.COD_ESTADO = 'C'
          AND V.ID_VTA_CORTA = VCD.ID_VTA_CORTA
          AND V.FOLIO = VCD.FOLIO
          AND V.FECHA_DE_MVTO = VCD.FECHA_DE_MVTO
          AND VCD.FECHA_DE_DEVENGO = @PFECHA_CIERRE


         SET @LTOTAL_DEVVC = isnull(@LTOTAL_FWD,0) + isnull(@LTOTAL_VC,0)

         --IF @LTOTAL_DEVVC < 0
         -- BEGIN
         --      SET @LTOTAL_DEVVC = 0
         -- END

         --INSERT INTO @TBLINT
         --SELECT SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(S.ID_CUENTA
         --                                                , S.VALOR_MERCADO_MON_USD
         --                                                , @LID_MONEDA_USD
         --                                                , @LID_MONEDA_PESO           --                                                , @PFECHA_CIERRE))
         --     , R.ID_ARBOL_CLASE_INST
         --  FROM SALDOS_ACTIVOS_INT S
         --     , REL_ACI_EMP_NEMOTECNICO R
         -- WHERE S.ID_CUENTA           IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
         --   AND S.FECHA_CIERRE        = @PFECHA_CIERRE
         --   AND S.ID_NEMOTECNICO      = R.ID_NEMOTECNICO
         --   AND R.ID_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST
         --                                   FROM ARBOL_CLASE_INSTRUMENTO
         --                                  WHERE ID_PADRE_ARBOL_CLASE_INST = @PID_ARBOL_CLASE)
         -- GROUP BY R.ID_ARBOL_CLASE_INST

         --SELECT @LTOTAL_INT = SUM(ROUND(ISNULL(MONTO,0),@LDECIMALES)) FROM @TBLINT

         SELECT @LTOTAL_INT = ISNULL(SUM(ROUND(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(S.ID_CUENTA
                                                                                    , S.VALOR_MERCADO_MON_USD
                                                                                    , @LID_MONEDA_USD
                                                                                    , @PID_MONEDA_SALIDA
                                                                                    , @PFECHA_CIERRE),@LDECIMALES)),0)
           FROM SALDOS_ACTIVOS_INT S
              , CUENTAS C
          WHERE C.ID_CUENTA IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
                AND S.ID_CUENTA       = C.ID_CUENTA
                AND S.FECHA_CIERRE    = @PFECHA_CIERRE
                AND S.ID_NEMOTECNICO  IN (SELECT ID_NEMOTECNICO
                                            FROM REL_ACI_EMP_NEMOTECNICO
                                           WHERE ID_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST
                                                                           FROM ARBOL_CLASE_INSTRUMENTO
                                                                          WHERE ID_PADRE_ARBOL_CLASE_INST = @PID_ARBOL_CLASE))

         SELECT @LTOTAL_NAC  = ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA
            , ROUND(SA.MONTO_MON_CTA,@LDECIMALES)
                                                                               , C.ID_MONEDA
                                                                               , @PID_MONEDA_SALIDA
                                                                               , @PFECHA_CIERRE)),0)
          FROM SALDOS_ACTIVOS SA,
               @TBLCUENTAS    C
         WHERE FECHA_CIERRE   = @PFECHA_CIERRE
           AND SA.ID_CUENTA   = C.ID_CUENTA
           AND ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO
                                   FROM REL_ACI_EMP_NEMOTECNICO
                                   WHERE ID_ARBOL_CLASE_INST IN(SELECT ID_ARBOL_CLASE_INST
                                                                  FROM ARBOL_CLASE_INSTRUMENTO
                                                                 WHERE ID_PADRE_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
                                                                   AND ID_EMPRESA = @LID_EMPRESA))

          SET @MONTO_MON_CTA = ISNULL(@LTOTAL_NAC,0) + ISNULL(@LTOTAL_DEVVC,0)   + ISNULL(@LTOTAL_INT,0)
     END
    ELSE
     BEGIN
        set @MONTO_MON_CTA = 0
        IF @LDSC_PADRE = 'Renta Variable Internacional' OR @LDSC_PADRE = 'Renta Fija Internacional'
         BEGIN
    --         INSERT INTO @TBLINT
    --SELECT SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(S.ID_CUENTA
    --                                                         , S.VALOR_MERCADO_MON_USD
    --                                                         , @LID_MONEDA_USD
    --                                                         , @LID_MONEDA_PESO
    --                    , @PFECHA_CIERRE))
    --              , R.ID_ARBOL_CLASE_INST
    --           FROM SALDOS_ACTIVOS_INT S
    --              , REL_ACI_EMP_NEMOTECNICO R
    --          WHERE S.ID_CUENTA           IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
    --            AND S.FECHA_CIERRE        = @PFECHA_CIERRE
    --            AND S.ID_NEMOTECNICO      = R.ID_NEMOTECNICO
    --            AND R.ID_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST
    --                                            FROM ARBOL_CLASE_INSTRUMENTO
    --                                           WHERE ID_PADRE_ARBOL_CLASE_INST = @PID_ARBOL_CLASE)
    --          GROUP BY R.ID_ARBOL_CLASE_INST

    --         SELECT @LTOTAL_INT = SUM(ROUND(ISNULL(MONTO,0),@LDECIMALES)) FROM @TBLINT

    SELECT @LTOTAL_INT = ISNULL(SUM(ROUND(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(S.ID_CUENTA
                                                                                        , S.VALOR_MERCADO_MON_USD
                                                                                        , @LID_MONEDA_USD
                                                                                        , @PID_MONEDA_SALIDA
                                                                                        , @PFECHA_CIERRE),@LDECIMALES)),0)
               FROM SALDOS_ACTIVOS_INT S
                  , CUENTAS C
              WHERE S.ID_CUENTA      IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
                AND S.ID_CUENTA      = C.ID_CUENTA
                AND S.FECHA_CIERRE   = @PFECHA_CIERRE
                AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO  FROM @TBLNEMOS)
         END
          SELECT @LTOTAL_NAC = ISNULL(SUM(ROUND(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA
                                                                               , SA.MONTO_MON_CTA
                                       , C.ID_MONEDA
                                                                               , @PID_MONEDA_SALIDA
                                                                               , @PFECHA_CIERRE),@LDECIMALES)),0)
            FROM SALDOS_ACTIVOS SA,
                 @TBLCUENTAS    C
           WHERE FECHA_CIERRE  = @PFECHA_CIERRE
             AND SA.ID_CUENTA  = C.ID_CUENTA
            AND ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO  FROM @TBLNEMOS)

         SET @MONTO_MON_CTA = ISNULL(@LTOTAL_NAC,0) + ISNULL(@LTOTAL_INT,0)
  END
    RETURN @MONTO_MON_CTA
END
GO
GRANT EXECUTE ON [Pkg_Global$MONTO_MON_CUENTA_RAMA_CONSOLIDADO_vr2] TO DB_EXECUTESP
GO