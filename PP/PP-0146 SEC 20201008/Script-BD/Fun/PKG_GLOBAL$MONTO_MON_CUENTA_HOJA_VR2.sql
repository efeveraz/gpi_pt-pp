USE CSGPI
GO

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PKG_GLOBAL$MONTO_MON_CUENTA_HOJA_VR2')
   DROP FUNCTION PKG_GLOBAL$MONTO_MON_CUENTA_HOJA_VR2
GO
CREATE FUNCTION  [DBO].[PKG_GLOBAL$MONTO_MON_CUENTA_HOJA_VR2]
(@PID_ARBOL_CLASE      NUMERIC,
 @PID_CUENTA           NUMERIC,
 @PFECHA_CIERRE        DATETIME,
 @PMERCADO_TRANSACCION NUMERIC = NULL,
 @PID_MONEDA           NUMERIC = NULL,
 @PID_SECTOR           NUMERIC = NULL
)  RETURNS FLOAT
AS
BEGIN
     DECLARE @MONTO_MON_CTA  FLOAT
     DECLARE @LDECIMALES     NUMERIC
     DECLARE @LDSC_ARBOL     VARCHAR(50)
           , @LDSC_PADRE     VARCHAR(50)
           , @LMONTO_FWD     NUMERIC(28,4)
           , @LMONTO_INT     NUMERIC(28,4)
           , @LMONTO_ACT     NUMERIC(28,4)
           , @LID_MONEDA_USD NUMERIC
           , @LTOTAL_VC      NUMERIC(28,4)
           , @LTOTAL_DEVVC   NUMERIC(28,4)
-------------------------------------------------------------------------------------
     SELECT @LDECIMALES=DICIMALES_MOSTRAR
       FROM MONEDAS
      WHERE ID_MONEDA = (SELECT ID_MONEDA FROM CUENTAS WHERE ID_CUENTA=@PID_CUENTA)

     SELECT @LID_MONEDA_USD = DBO.FNT_DAMEIDMONEDA('USD')
-------------------------------------------------------------------------------------
     SELECT @LDSC_ARBOL = DSC_ARBOL_CLASE_INST
          , @LDSC_PADRE = (SELECT DSC_ARBOL_CLASE_INST
                             FROM ARBOL_CLASE_INSTRUMENTO
                            WHERE ID_ARBOL_CLASE_INST= A.ID_PADRE_ARBOL_CLASE_INST)
       FROM ARBOL_CLASE_INSTRUMENTO A
      WHERE A.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
-------------------------------------------------------------------------------------
     SET @MONTO_MON_CTA = 0
     SET @LMONTO_FWD    = 0
     SET @LMONTO_INT    = 0
     SET @LMONTO_ACT    = 0
     SET @LTOTAL_VC		= 0
     SET @LTOTAL_DEVVC	= 0

     IF @LDSC_ARBOL = 'Forwards' or  @LDSC_ARBOL = 'Venta Corta'
      BEGIN
		IF @LDSC_ARBOL = 'Forwards'
		 BEGIN
           SELECT @LMONTO_FWD = ISNULL(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SD.ID_CUENTA
                                                                            , SUM(SD.VMM)
                                                                            , DBO.FNT_DAMEIDMONEDA('$$')
                                                                            , ISNULL(@PID_MONEDA,C.ID_MONEDA)
                                                                            , @PFECHA_CIERRE) ,0)
             --SUM(SD.VMM)
             FROM VIEW_SALDOS_DERIVADOS SD
                , CUENTAS C
            WHERE SD.ID_CUENTA = @PID_CUENTA
              AND SD.FECHA_CIERRE = @PFECHA_CIERRE
              AND SD.COD_INSTRUMENTO = 'FWD_NAC'
              AND C.ID_CUENTA        = SD.ID_CUENTA
             GROUP BY SD.ID_CUENTA,C.ID_MONEDA
            end
           IF @LDSC_ARBOL = 'Venta Corta'
           BEGIN
--               SELECT @LTOTAL_VC = (ISNULL(SUM(VC.PRECIO_MEDIO_MERCADO),0) - ISNULL(SUM(VC.PRECIO_MEDIO),0)) + SUM(ISNULL(VC.PRIMA_ACUMULADA,0))
--                 FROM VENTA_CORTA VC
--                WHERE VC.ID_CUENTA = @PID_CUENTA
--                  AND VC.FECHA_DE_MVTO = @PFECHA_CIERRE


               SELECT @LTOTAL_VC =
                                   SUM(
                                       ISNULL((VC.CANTIDAD * VC.PRECIO_MEDIO), 0) -
                                       ISNULL((VC.CANTIDAD * VCD.PRECIO_MEDIO_MERCADO), 0) -
                                       ISNULL(VCD.PRIMA_ACUMULADA, 0)
                                      )
                 FROM VENTA_CORTA VC,
                      VENTA_CORTA_DEVENGO VCD
                WHERE VC.ID_CUENTA          = @PID_CUENTA
                  AND @PFECHA_CIERRE        BETWEEN VC.FECHA_DE_MVTO AND VC.FECHA_VENCIMIENTO
                  AND VC.COD_ESTADO         = 'C'
                  AND VC.ID_VTA_CORTA       = VCD.ID_VTA_CORTA
                  AND VC.FOLIO              = VCD.FOLIO
                  AND VC.FECHA_DE_MVTO      = VCD.FECHA_DE_MVTO
                  AND VCD.FECHA_DE_DEVENGO  = @PFECHA_CIERRE


           END

           SET @LTOTAL_DEVVC = @LMONTO_FWD + @LTOTAL_VC
--           IF @LMONTO_FWD < 0
--           BEGIN
--               SET @MONTO_MON_CTA = 0
--           END
--           ELSE
--           BEGIN
               SET @MONTO_MON_CTA =  @LTOTAL_DEVVC
--           END
      END
     ELSE
      BEGIN
           IF UPPER(@LDSC_PADRE) = 'RENTA VARIABLE INTERNACIONAL' OR UPPER(@LDSC_PADRE) = 'RENTA FIJA INTERNACIONAL' OR
              (UPPER(@LDSC_PADRE) = 'OTROS ACTIVOS'  AND UPPER(@LDSC_ARBOL) = 'FONDOS DE INVERSIÓN') OR
              (UPPER(@LDSC_PADRE) = 'OTROS ACTIVOS'  AND UPPER(@LDSC_ARBOL) = 'NOTAS ESTRUCTURADAS')
            BEGIN
             SELECT @LMONTO_INT  = ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                                            , VALOR_MERCADO_MON_USD
                                                                            , @LID_MONEDA_USD
                                                                            , ISNULL(@PID_MONEDA,C.ID_MONEDA)
                            , @PFECHA_CIERRE)) ,0)
               FROM SALDOS_ACTIVOS_INT S
                  , CUENTAS C
              WHERE C.ID_CUENTA      = @PID_CUENTA
                AND S.ID_CUENTA      = C.ID_CUENTA
                AND S.FECHA_CIERRE   = @PFECHA_CIERRE
                AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO
                                        FROM REL_ACI_EMP_NEMOTECNICO
                                       WHERE ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE)

           END

           SELECT @LMONTO_ACT = ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(S.ID_CUENTA
                                                                            , MONTO_MON_CTA
                                                                            , S.ID_MONEDA_CTA
                                                                            , ISNULL(@PID_MONEDA,S.ID_MONEDA_CTA )
                                                                            , @PFECHA_CIERRE)) ,0)
             FROM SALDOS_ACTIVOS S,
                  NEMOTECNICOS N,
                  VIEW_EMISORES_ESPECIFICOS E
            WHERE S.ID_CUENTA = @PID_CUENTA
              AND  S.FECHA_CIERRE = @PFECHA_CIERRE
              AND N.ID_MERCADO_TRANSACCION = ISNULL(@PMERCADO_TRANSACCION,N.ID_MERCADO_TRANSACCION)
              --AND N.ID_MONEDA = ISNULL(@PID_MONEDA,N.ID_MONEDA)
              AND E.ID_SECTOR = ISNULL(@PID_SECTOR, E.ID_SECTOR)
              AND S.ID_NEMOTECNICO IN ( SELECT ID_NEMOTECNICO
                                          FROM REL_ACI_EMP_NEMOTECNICO
                                         WHERE ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
                                           AND ID_EMPRESA      = (SELECT ID_EMPRESA
                                                                    FROM CUENTAS
                                                                   WHERE ID_CUENTA = @PID_CUENTA)
                                                                  )
              AND  N.ID_NEMOTECNICO = S.ID_NEMOTECNICO
              AND  E.ID_EMISOR_ESPECIFICO = N.ID_EMISOR_ESPECIFICO

            SET @MONTO_MON_CTA = ISNULL(@MONTO_MON_CTA,0) + ISNULL(@LMONTO_ACT,0)  + ISNULL(@LMONTO_INT,0)
       END

       RETURN @MONTO_MON_CTA
END
GO

GRANT EXECUTE ON [PKG_GLOBAL$MONTO_MON_CUENTA_HOJA_VR2] TO DB_EXECUTESP
GO
