USE CSGPI
GO

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PKG_SALDOS_ACTIVOS$ABRE_RAMA_VR2')
  DROP PROCEDURE PKG_SALDOS_ACTIVOS$ABRE_RAMA_VR2
 GO

CREATE PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS$ABRE_RAMA_VR2]
(
    @PID_ARBOL_CLASE_INST NUMERIC,
    @PID_CUENTA             NUMERIC = NULL,
    @PFECHA_CIERRE         DATETIME,
    @PID_EMPRESA          NUMERIC = NULL,
    @PID_CLIENTE             NUMERIC = NULL,
    @PID_GRUPO               NUMERIC = NULL,
    @PID_MONEDA_SALIDA       NUMERIC = NULL,
    @PCONSOLIDADO            VARCHAR(3) = NULL
)
AS
SET NOCOUNT ON
    DECLARE @FECHA_ANTERIOR DATETIME,
            @LREG           NUMERIC,
            @LMAX_REG       NUMERIC,
            @DSC_ARBOL      VARCHAR(100),
            @MONTO_ACTUAL   NUMERIC(18,5),
            @MONTO_ANTERIOR NUMERIC(18,5)

    SET @FECHA_ANTERIOR = DBO.PKG_GLOBAL$ULTIMODIAMESANTERIOR(@PFECHA_CIERRE)

    DECLARE @LID_MONEDA_SALIDA NUMERIC(04)
    SET @LID_MONEDA_SALIDA = (SELECT ID_MONEDA FROM CUENTAS WHERE ID_CUENTA = @PID_CUENTA)

    SET @PID_MONEDA_SALIDA = ISNULL(@PID_MONEDA_SALIDA,@LID_MONEDA_SALIDA)

    CREATE TABLE #TABLATMP (ID                         NUMERIC IDENTITY,
                            ID_ARBOL_CLASE_INST        NUMERIC(10),
                            ID_EMPRESA                 NUMERIC(10),
                            ID_PADRE_ARBOL_CLASE_INST  NUMERIC(10),
                            DSC_ARBOL_CLASE_INST       VARCHAR(100),
                            ORDEN                      NUMERIC(5),
                            CODIGO                     VARCHAR(50),
                            ID_ACI_TIPO                NUMERIC(10),
                            MONTO_ACTUAL               NUMERIC(18,5),
                            MONTO_ANTERIOR             NUMERIC(18,5)
                           )

    CREATE TABLE #TABLATM2 (DSC_ARBOL_CLASE_INST       VARCHAR(100),
                            MONTO_ACTUAL               NUMERIC(18,5),
                            MONTO_ANTERIOR             NUMERIC(18,5)
                           )

    INSERT INTO #TABLATMP
    SELECT ID_ARBOL_CLASE_INST,
		   ID_EMPRESA,
		   ID_PADRE_ARBOL_CLASE_INST,
		   CASE
              WHEN DSC_ARBOL_CLASE_INST = 'Fondos Mutuos Corto Plazo' THEN 'Fondos Mutuos'
		   	  WHEN DSC_ARBOL_CLASE_INST = 'Fondos Mutuos Largo Plazo' THEN 'Fondos Mutuos'
		   	  WHEN DSC_ARBOL_CLASE_INST = 'Forwards' THEN 'Derivado/Venta Corta'
		   	  WHEN DSC_ARBOL_CLASE_INST = 'Venta Corta' THEN 'Derivado/Venta Corta'
		      ELSE DSC_ARBOL_CLASE_INST
		   END DSC_ARBOL_CLASE_INST,
		   ORDEN,
		   CODIGO,
		   ID_ACI_TIPO,
           ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_VR2(ID_ARBOL_CLASE_INST,
                                                                       @PID_CUENTA,
                                                                       @PFECHA_CIERRE,
                                                                       @PID_CLIENTE,
                                                                       @PID_GRUPO,
                                                                       @PID_MONEDA_SALIDA,
                                                                       @PCONSOLIDADO,
                                                                       @PID_EMPRESA),0) AS MONTO_ACTUAL ,
           ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_VR2(ID_ARBOL_CLASE_INST,
                                                                       @PID_CUENTA,
                                                                       @FECHA_ANTERIOR,
                                                                       @PID_CLIENTE,
                                                                       @PID_GRUPO,
                                                                       @PID_MONEDA_SALIDA,
                                                                       @PCONSOLIDADO,
                                                                       @PID_EMPRESA),0) AS MONTO_ANTERIOR

      FROM ARBOL_CLASE_INSTRUMENTO
     WHERE ID_PADRE_ARBOL_CLASE_INST = @PID_ARBOL_CLASE_INST
       AND ID_EMPRESA = @PID_EMPRESA
     ORDER BY ORDEN

       SET @LREG = 1
    SELECT @LMAX_REG = COUNT(1) FROM #TABLATMP

     WHILE @LREG <= @LMAX_REG
     BEGIN
        SELECT @DSC_ARBOL      = DSC_ARBOL_CLASE_INST
             , @MONTO_ACTUAL   = MONTO_ACTUAL
             , @MONTO_ANTERIOR = MONTO_ANTERIOR
          FROM #TABLATMP
         WHERE ID = @LREG

        IF ((SELECT COUNT(1) FROM #TABLATM2 WHERE DSC_ARBOL_CLASE_INST = @DSC_ARBOL) = 0)
        BEGIN
           INSERT INTO #TABLATM2
           VALUES (@DSC_ARBOL, @MONTO_ACTUAL, @MONTO_ANTERIOR)
        END
        ELSE
        BEGIN
           UPDATE #TABLATM2
           SET MONTO_ACTUAL   = MONTO_ACTUAL + @MONTO_ACTUAL,
               MONTO_ANTERIOR = MONTO_ANTERIOR + @MONTO_ANTERIOR
           WHERE DSC_ARBOL_CLASE_INST = @DSC_ARBOL
        END
        SET @LREG = @LREG + 1
     END

   SELECT DSC_ARBOL_CLASE_INST, MONTO_ACTUAL, MONTO_ANTERIOR FROM #TABLATM2

   DROP TABLE #TABLATMP
   DROP TABLE #TABLATM2
SET NOCOUNT OFF
GO

GRANT EXECUTE ON [PKG_SALDOS_ACTIVOS$ABRE_RAMA_VR2] TO DB_EXECUTESP
GO
