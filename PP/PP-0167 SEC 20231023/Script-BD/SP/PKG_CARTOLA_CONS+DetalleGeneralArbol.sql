IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[PKG_CARTOLA_CONS$DetalleGeneralArbol]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
     DROP PROCEDURE [dbo].[PKG_CARTOLA_CONS$DetalleGeneralArbol]
GO
CREATE PROCEDURE [dbo].[PKG_CARTOLA_CONS$DetalleGeneralArbol]
( @PID_ARBOL_CLASE_INST  NUMERIC = NULL  
, @PFECHA_CIERRE         DATETIME
, @PCONSOLIDADO          VARCHAR(3)
, @PID_CUENTA            NUMERIC = NULL
, @PID_CLIENTE           NUMERIC = NULL
, @PID_GRUPO             NUMERIC = NULL
, @PID_MONEDA_SALIDA     NUMERIC = NULL
, @PDSC_ARBOL_CLASE_INST VARCHAR(100) = NULL

)  
AS  
BEGIN
    SET NOCOUNT ON
    DECLARE @FECHA_ANTERIOR DATETIME  
    DECLARE @LID_MONEDA_SALIDA NUMERIC(04)  
          
    SET @FECHA_ANTERIOR = DBO.PKG_GLOBAL$ULTIMODIAMESANTERIOR(@PFECHA_CIERRE)  
    SET @LID_MONEDA_SALIDA = (SELECT ID_MONEDA FROM CUENTAS WHERE ID_CUENTA = @PID_CUENTA)    
    SET @PID_MONEDA_SALIDA = ISNULL(@PID_MONEDA_SALIDA,@LID_MONEDA_SALIDA)  
          
	IF @PCONSOLIDADO = 'CTA'
    BEGIN
		SELECT *,  
			   ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_I(ID_ARBOL_CLASE_INST,   
																	     @PID_CUENTA,  
																	     @PFECHA_CIERRE,  
																	     @PID_CLIENTE,  
																	     @PID_GRUPO,  
																	     @PID_MONEDA_SALIDA,  
																	     @PCONSOLIDADO),0) AS MONTO_ACTUAL ,  
			   ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_I(ID_ARBOL_CLASE_INST,   
																	     @PID_CUENTA,  
																	     @FECHA_ANTERIOR,  
																	     @PID_CLIENTE,  
																	     @PID_GRUPO,  
																	     @PID_MONEDA_SALIDA,  
																	     @PCONSOLIDADO),0) AS MONTO_ANTERIOR     
		  FROM ARBOL_CLASE_INSTRUMENTO   
		 WHERE ID_PADRE_ARBOL_CLASE_INST = @PID_ARBOL_CLASE_INST  
		 ORDER BY ORDEN  
    END
    ELSE
	BEGIN
		SELECT DSC_ARBOL_CLASE_INST
		     , (SELECT TOP 1 ORDEN FROM ARBOL_CLASE_INSTRUMENTO WHERE DSC_ARBOL_CLASE_INST = A.DSC_ARBOL_CLASE_INST ORDER BY ID_EMPRESA) ORDEN
		     , (SELECT TOP 1 ID_ARBOL_CLASE_INST FROM ARBOL_CLASE_INSTRUMENTO WHERE DSC_ARBOL_CLASE_INST = A.DSC_ARBOL_CLASE_INST ORDER BY ID_EMPRESA) ID_ARBOL_CLASE_INST
			 , SUM(ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_II(ID_ARBOL_CLASE_INST,   
																	         @PID_CUENTA,  
																	         @PFECHA_CIERRE,  
																	         @PID_CLIENTE,  
																	         @PID_GRUPO,  
																	         @PID_MONEDA_SALIDA,  
																	         @PCONSOLIDADO,
																	         ID_EMPRESA),0)) AS MONTO_ACTUAL   
			 , SUM(ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_II(ID_ARBOL_CLASE_INST,   
																	         @PID_CUENTA,  
																	         @FECHA_ANTERIOR,  
																	         @PID_CLIENTE,  
																	         @PID_GRUPO,  
																	         @PID_MONEDA_SALIDA,  
																	         @PCONSOLIDADO,
																	         ID_EMPRESA),0)) AS MONTO_ANTERIOR     
		  FROM ARBOL_CLASE_INSTRUMENTO  A 
		 WHERE ID_PADRE_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST FROM ARBOL_CLASE_INSTRUMENTO WHERE DSC_ARBOL_CLASE_INST = @PDSC_ARBOL_CLASE_INST)
			AND ID_ACI_TIPO = 1
		 GROUP BY DSC_ARBOL_CLASE_INST         
	     ORDER BY 2
    END        
  
    SET NOCOUNT OFF
END
GO
GRANT EXECUTE ON [PKG_CARTOLA_CONS$DetalleGeneralArbol] TO DB_EXECUTESP
GO
