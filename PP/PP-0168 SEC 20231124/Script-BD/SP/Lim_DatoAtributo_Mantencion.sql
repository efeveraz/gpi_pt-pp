USE[GPIMAS]
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Lim_DatoAtributo_Mantencion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Lim_DatoAtributo_Mantencion]
GO

CREATE PROCEDURE [dbo].[Lim_DatoAtributo_Mantencion]
(@pAccion         VARCHAR(10), 
 @pIdDatoAtributo NUMERIC(12, 0) OUTPUT, 
 @pIdAtributo     NUMERIC(10, 0), 
 @pValorString    VARCHAR(100), 
 @pValorNumerico1 NUMERIC(13,0), 
 @pValorNumerico2 NUMERIC(24,6), 
 @pOrden          NUMERIC(10,0), 
 @pEstado         CHAR(3), 
 @pResultado      VARCHAR(200) OUTPUT,
 @pIdNegocio	  NUMERIC(10,0))

AS
DECLARE @lCantidadRegistros NUMERIC(6)
DECLARE @lOrden             NUMERIC(10)
DECLARE @ldelError          INT
DECLARE @lDscTipoAtributo   VARCHAR(100)
DECLARE @vValorNumerico1	NUMERIC(13,0),
		@vValorNumerico2	NUMERIC(24,6)

IF EXISTS(
	SELECT	1
	FROM	LIM_ATRIBUTO ATR
		INNER JOIN LIM_TIPO_ATRIBUTO TIP
			ON	TIP.ID_TIPO_ATRIBUTO = ATR.ID_TIPO_ATRIBUTO
			AND	TIP.DSC_TIPO_ATRIBUTO NOT IN ('Escala', 'Rango')
			AND	ATR.ID_ATRIBUTO = @pIdAtributo
	)
BEGIN
	SELECT	@vValorNumerico1 = NULL,
			@vValorNumerico2 = NULL
END
ELSE
BEGIN
	SELECT	@vValorNumerico1 = @pValorNumerico1,
			@vValorNumerico2 = @pValorNumerico2
END

		
IF UPPER(RTRIM(@pAccion)) NOT IN ('INSERTAR','MODIFICAR','ELIMINAR') 
BEGIN
      SET  @pResultado = 'Operación no válida.'
      RETURN 0
END


   
IF UPPER(RTRIM(@pAccion)) = 'INSERTAR'
BEGIN
      IF NOT EXISTS (SELECT * FROM LIM_DATO_ATRIBUTO LDA WHERE LDA.ID_DATO_ATRIBUTO = @pIdDatoAtributo AND LDA.ID_NEGOCIO = @pIdNegocio)
      BEGIN 
           SELECT @lDscTipoAtributo = TA.DSC_TIPO_ATRIBUTO 
             FROM LIM_ATRIBUTO  LA INNER JOIN LIM_TIPO_ATRIBUTO TA ON LA.ID_TIPO_ATRIBUTO = TA.ID_TIPO_ATRIBUTO
            WHERE LA.ID_ATRIBUTO = @pIdAtributo
           IF @lDscTipoAtributo = 'Escala' 
           BEGIN
               IF EXISTS( SELECT 1 
                           FROM LIM_DATO_ATRIBUTO LDA 
                          WHERE LDA.ID_ATRIBUTO = @pIdAtributo
                            AND LDA.ESTADO      = 'VIG'
                            AND(  @vValorNumerico1> = VALOR_NUMERICO_1  AND @vValorNumerico1<= VALOR_NUMERICO_2
                                 OR @vValorNumerico2> = VALOR_NUMERICO_2 AND @vValorNumerico2<= VALOR_NUMERICO_2)
                            AND LDA.ID_NEGOCIO	= @pIdNegocio)
                BEGIN
                      SET  @pResultado = 'Rango ente mínimo y máximo ingresado ya se encuentra definido dentro de otro'
                      RETURN 0
                END
           END
       
            

			IF ISNULL((SELECT COUNT(VALOR_STRING) FROM LIM_DATO_ATRIBUTO WHERE VALOR_STRING =@pValorString), 0) = 0
			BEGIN
            INSERT INTO LIM_DATO_ATRIBUTO(ID_ATRIBUTO,
                                          VALOR_STRING,
                                          VALOR_NUMERICO_1,
                                          VALOR_NUMERICO_2,
                                          ORDEN,
                                          ESTADO,
                                          ID_NEGOCIO
                                          ) 
                                 VALUES (@pIdAtributo,
                                          @pValorString, -- VALOR_STRING,
                                          @vValorNumerico1, -- VALOR_NUMERICO_1
                                          @vValorNumerico2, -- VALOR_NUMERICO_2
                                          @lOrden, -- ORDEN
                                          @pEstado,  -- ESTADO
                                          @pIdNegocio
                                          )
               
            SET @pIdDatoAtributo = @@identity
            SET @ldelError = @@ERROR
            IF @ldelError <> 0
			BEGIN
               SET @pResultado = 'Error al insertar dato atributo "' + @pValorString + '".'
            END
			ELSE
			BEGIN
               SET @pResultado = 'OK'
            END
			
			END
			ELSE
			BEGIN
				SET @pIdDatoAtributo =(SELECT TOP 1 ID_DATO_ATRIBUTO 
										FROM LIM_DATO_ATRIBUTO 
										WHERE VALOR_STRING =@pValorString 
										AND  ID_NEGOCIO = @pIdNegocio
										AND ID_ATRIBUTO = @pIdAtributo
										AND ESTADO ='VIG')
				IF ISNULL(@pIdDatoAtributo,0) > 0
				BEGIN
					SET @pResultado = 'OK'
				END
				ELSE
				BEGIN
					SET @pResultado = 'Error al buscar id dato atributo "' + @pValorString + '".'
				END
			END
			
		    
		END
END			
ELSE
BEGIN
    SET @pResultado = 'Error al insertar, dato atributo: "' + @pValorString + '" ya existe.'
END

IF UPPER(RTRIM(@pAccion)) = 'MODIFICAR'
BEGIN
      IF EXISTS (SELECT * FROM LIM_DATO_ATRIBUTO LDA WHERE LDA.ID_DATO_ATRIBUTO = @pIdDatoAtributo)
      BEGIN 
            UPDATE LIM_DATO_ATRIBUTO 
               SET ID_ATRIBUTO     = @pIdAtributo     ,      
                   VALOR_STRING    = @pValorString    ,      
                   VALOR_NUMERICO_1= @vValorNumerico1 ,      
                   VALOR_NUMERICO_2= @vValorNumerico2 ,
                   ORDEN           = @pOrden          ,      
                   ESTADO          = @pEstado               
             WHERE ID_DATO_ATRIBUTO = @pIdDatoAtributo
				AND ID_NEGOCIO		= @pIdNegocio
            SET @ldelError = @@ERROR
            IF @ldelError <> 0
               SET @pResultado = 'Error al modificar dato atributo "' + @pValorString + '".'
            ELSE
               SET @pResultado = 'OK'
      END
ELSE
    SET @pResultado = 'Error al modificar, dato atributo (' + @pValorString + ') no existe.'
END

IF UPPER(RTRIM(@pAccion)) = 'ELIMINAR'
BEGIN
      IF EXISTS (SELECT * FROM LIM_DATO_ATRIBUTO LDA WHERE LDA.ID_DATO_ATRIBUTO = @pIdDatoAtributo)
      BEGIN 
            UPDATE LIM_DATO_ATRIBUTO 
               SET ESTADO  = @pEstado 
             WHERE ID_DATO_ATRIBUTO = @pIdDatoAtributo
				AND ID_NEGOCIO		= @pIdNegocio
            SET @ldelError = @@ERROR
            IF @ldelError <> 0
               SET @pResultado = 'Error al eliminar el dato atributo "' + @pValorString + '".'
            ELSE
               SET @pResultado = 'OK'
      END
      ELSE
          SET @pResultado = 'Error al eliminar, dato atributo (' + @pValorString + ') no existe.'
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE ON  [Lim_DatoAtributo_Mantencion]  TO DB_EXECUTESP 
GO