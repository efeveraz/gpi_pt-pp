USE [CSGPI]
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO_CONSOLIDADO_vr2]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO_CONSOLIDADO_vr2]
GO
CREATE PROCEDURE [DBO].[PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO_CONSOLIDADO_vr2]
( @PID_CUENTA         NUMERIC = NULL,
  @PFECHA_CIERRE      DATETIME,
  @PID_ARBOL_CLASE    NUMERIC,
  @PID_MONEDA_SALIDA  NUMERIC = NULL,
  @PID_MERCADO_TRANSACCION NUMERIC = NULL,
  @PID_MONEDA         NUMERIC = NULL,
  @PID_SECTOR         NUMERIC = NULL,
  @PID_EMPRESA        NUMERIC,
  @PCONSOLIDADO       VARCHAR(3) = NULL,
  @PID_CLIENTE        NUMERIC = NULL,
  @PID_GRUPO          NUMERIC = NULL,
  @PID_TIPOCUENTA     NUMERIC = NULL
)AS
BEGIN
   SET NOCOUNT ON;
   DECLARE @LID_ENTIDAD NUMERIC
   DECLARE @SALIDA TABLE(ID_CUENTA                         NUMERIC,        --1
                         ID_SALDO_ACTIVO                   NUMERIC,        --2
                         FECHA_CIERRE                      DATETIME,       --3
                         ID_NEMOTECNICO                    NUMERIC,        --4
                         NEMOTECNICO                       VARCHAR(50),    --5
                         EMISOR                            VARCHAR(100),   --6
                         COD_EMISOR                        VARCHAR(10),    --7
                         DSC_NEMOTECNICO                   VARCHAR(120),   --8
                         TASA_EMISION_2                    NUMERIC(18,4),  --9
                         CANTIDAD                          NUMERIC(18,4),  --10
                         PRECIO                            NUMERIC(18,6),  --11
                         TASA_EMISION                      NUMERIC(18,4),  --12
                         FECHA_VENCIMIENTO                 DATETIME,       --13
                         PRECIO_COMPRA                     NUMERIC(18,4),  --14
                         TASA                              NUMERIC(18,4),  --15
                         TASA_COMPRA                       NUMERIC(18,4),  --16
                         MONTO_VALOR_COMPRA                NUMERIC(18,4),  --17
                         MONTO_MON_CTA                     NUMERIC(18,4),  --18
                         ID_MONEDA_CTA                     NUMERIC,        --19
                         ID_MONEDA_NEMOTECNICO             NUMERIC,        --20
                         SIMBOLO_MONEDA                    VARCHAR(3),     --21
                         MONTO_MON_NEMOTECNICO             NUMERIC(18,4),  --22
                         MONTO_MON_ORIGEN                  NUMERIC(18,4),  --23
                         ID_EMPRESA                        NUMERIC,        --24
                         ID_ARBOL_CLASE_INST               NUMERIC,        --25
                         COD_INSTRUMENTO                   VARCHAR(15),    --26
                         DSC_ARBOL_CLASE_INST              VARCHAR(100),   --27
                         PORCENTAJE_RAMA                   NUMERIC(18,4),  --28
                         PRECIO_PROMEDIO_COMPRA            NUMERIC(18,4),  --29
                         DSC_PADRE_ARBOL_CLASE_INST        VARCHAR(100),   --30
                         RENTABILIDAD                      FLOAT,          --31
                         DIAS                              NUMERIC,        --32
                         DURACION                          NUMERIC(18,4),  --33
                         COD_PRODUCTO                      VARCHAR(10),    --34
                         ID_PADRE_ARBOL_CLASE_INST         NUMERIC,        --35
                         DSC_CLASIFICADOR_RIESGO           VARCHAR(50),    --36
                         CODIGO                            VARCHAR(20)     --37
                        )
   DECLARE @TMP_CUENTAS TABLE (ID_CUENTA NUMERIC)


   DECLARE @LID_CUENTA                         NUMERIC
   DECLARE @LID_MONEDA_SALIDA NUMERIC(04)

   IF @PCONSOLIDADO = 'CLT'
    BEGIN
          INSERT INTO @TMP_CUENTAS
    SELECT ID_CUENTA
      FROM VIEW_CUENTAS_VIGENTES
     WHERE ID_CLIENTE = @PID_CLIENTE
          AND ID_TIPOCUENTA = @PID_TIPOCUENTA
        ORDER BY ID_CUENTA
       SET @LID_ENTIDAD = @PID_CLIENTE
  END
 ELSE
   IF @PCONSOLIDADO = 'GRP'
    BEGIN
            INSERT INTO @TMP_CUENTAS
      SELECT ID_CUENTA
        FROM VIEW_CUENTAS_VIGENTES
       WHERE ID_TIPOCUENTA = @PID_TIPOCUENTA
               AND ID_CUENTA IN (SELECT ID_CUENTA FROM REL_CUENTAS_GRUPOS_CUENTAS
                             WHERE ID_GRUPO_CUENTA = @PID_GRUPO)
         ORDER BY ID_CUENTA
         SET @LID_ENTIDAD = @PID_GRUPO
    END
   ELSE
    BEGIN
                INSERT INTO @TMP_CUENTAS
          SELECT ID_CUENTA
            FROM VIEW_CUENTAS_VIGENTES
           WHERE ID_CUENTA = ISNULL(@PID_CUENTA,ID_CUENTA)
       AND ID_TIPOCUENTA = ISNULL(@PID_TIPOCUENTA,ID_TIPOCUENTA)
        ORDER BY ID_CUENTA
        SET @LID_ENTIDAD = @PID_CUENTA
    END


        SET @PID_MONEDA_SALIDA = ISNULL(@PID_MONEDA_SALIDA,@LID_MONEDA_SALIDA)

         SELECT FECHA_CIERRE
              , ID_NEMOTECNICO
              , NEMOTECNICO
              , EMISOR
              , COD_EMISOR
              , DSC_NEMOTECNICO
              , PRECIO
              , TASA_EMISION
              , FECHA_VENCIMIENTO
              , TASA
              , TASA_COMPRA
              , PRECIO_COMPRA
              , ID_MONEDA_CTA
              , ID_MONEDA
              , SIMBOLO_MONEDA
              , ID_EMPRESA
              , ID_ARBOL_CLASE_INST
              , COD_INSTRUMENTO
              , DSC_ARBOL_CLASE_INST
              , PRECIO_PROMEDIO_COMPRA
              , DSC_PADRE_ARBOL_CLASE_INST
              , DURACION
              , COD_PRODUCTO
              , ID_PADRE_ARBOL_CLASE_INST
              , DSC_CLASIFICADOR_RIESGO
              , COD_SUBFAMILIA
              , CODIGO
              , SUM(CANTIDAD) CANTIDAD
              , SUM(MONTO_VALOR_COMPRA) MONTO_VALOR_COMPRA
              , SUM(MONTO) MONTO
              , SUM(MONTO_MON_NEMOTECNICO) MONTO_MON_NEMOTECNICO
              , SUM(MONTO_MON_ORIGEN)  MONTO_MON_ORIGEN
              , SUM(MONTO_MON_CTA) MONTO_MON_CTA
              , SUM(MONTO_MON_CTA) / ISNULL(DBO.PKG_GLOBAL$MONTO_MON_CUENTA_HOJA_CONSOLIDADO(@PID_ARBOL_CLASE
                                                                       , @PID_CUENTA
                                                                       , @PFECHA_CIERRE
                                  , @PID_CLIENTE
                                                                       , @PID_GRUPO
                                                                       , @PID_MONEDA_SALIDA
                                                                       , @PCONSOLIDADO
                                                                       , @PID_EMPRESA),0) * 100 AS PORCENTAJE_RAMA
              , CASE WHEN COD_PRODUCTO = 'RF_NAC' THEN 0
                     ELSE SUM(CANTIDAD) * PRECIO_PROMEDIO_COMPRA
                END AS MONTO_PROMEDIO_COMPRA
              , CASE WHEN COD_PRODUCTO = 'RF_NAC' THEN 0
                    ELSE CASE (SUM(CANTIDAD) * PRECIO_PROMEDIO_COMPRA) WHEN 0 THEN NULL
   ELSE ((sum(MONTO_MON_NEMOTECNICO)/(PRECIO_PROMEDIO_COMPRA * sum(CANTIDAD)))-1)*100
                   END
      END AS RENTABILIDAD
           FROM ( SELECT SA.FECHA_CIERRE,
                         SA.ID_NEMOTECNICO,
                         N.NEMOTECNICO,
                         EE.DSC_EMISOR_ESPECIFICO AS EMISOR,
                         EE.COD_SVS_NEMOTECNICO AS COD_EMISOR,
                         N.DSC_NEMOTECNICO,
                         SA.CANTIDAD AS CANTIDAD,
                         SA.PRECIO,
                         N.TASA_EMISION,
                         N.FECHA_VENCIMIENTO,
                         TASA,
                         CASE WHEN SA.COD_INSTRUMENTO = 'BONOS_NAC'
                           THEN DBO.FNT_ENTREGA_TASA_PROMEDIO_CONSOLIDADO(SA.FECHA_CIERRE
                                                                        , @PCONSOLIDADO
                                                                        , @LID_ENTIDAD
                                                                        , SA.ID_NEMOTECNICO)
                           ELSE SA.TASA_COMPRA
                         END AS 'TASA_COMPRA',
                         ISNULL(dbo.FNT_ENTREGA_PRECIO_PROMEDIO_CONSOLIDADO(@PFECHA_CIERRE
                                                                  , @PCONSOLIDADO
                                                                  , @LID_ENTIDAD
                                                                  , SA.ID_NEMOTECNICO),0) AS 'PRECIO_COMPRA' ,
                         SA.MONTO_VALOR_COMPRA AS MONTO_VALOR_COMPRA,
                         DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@LID_CUENTA
                                                    , MONTO_MON_CTA
                                                    , ID_MONEDA_CTA
                                                    , @PID_MONEDA_SALIDA
                                                    , @PFECHA_CIERRE) AS MONTO,
                         ID_MONEDA_CTA,
                         N.ID_MONEDA,
                         MO.SIMBOLO AS SIMBOLO_MONEDA,
                         --MONTO_MON_NEMOTECNICO,
                         CASE CODIGO WHEN 'FFMM' THEN  MONTO_VALOR_COMPRA ELSE MONTO_MON_NEMOTECNICO END MONTO_MON_NEMOTECNICO,
                         MONTO_MON_ORIGEN,
                         ACI.ID_EMPRESA,
                         ACI.ID_ARBOL_CLASE_INST,
                         N.COD_INSTRUMENTO,
                         CI.DSC_ARBOL_CLASE_INST,
                         MONTO_MON_CTA,
                         ISNULL(dbo.FNT_ENTREGA_PRECIO_PROMEDIO_CONSOLIDADO(@PFECHA_CIERRE
                                                                  , @PCONSOLIDADO
                                                                  , @LID_ENTIDAD
                                                                  , SA.ID_NEMOTECNICO),0) AS 'PRECIO_PROMEDIO_COMPRA',
                  (SELECT ACII.DSC_ARBOL_CLASE_INST
                            FROM ARBOL_CLASE_INSTRUMENTO ACII
                           WHERE ACII.ID_ARBOL_CLASE_INST = CI.ID_PADRE_ARBOL_CLASE_INST) AS DSC_PADRE_ARBOL_CLASE_INST,
                         (SELECT TOP 1 P.DURACION FROM PUBLICADORES_PRECIOS P
                           WHERE P.ID_NEMOTECNICO = N.ID_NEMOTECNICO
                            AND P.FECHA <= @PFECHA_CIERRE
                            AND P.DURACION != 0
                          ORDER BY P.FECHA DESC)AS DURACION,
                        I.COD_PRODUCTO,
                        CI.ID_PADRE_ARBOL_CLASE_INST,
                        (SELECT TOP 1 RN.COD_VALOR_CLASIFICACION
                           FROM REL_NEMOTECNICO_VALOR_CLASIFIC RN
                              , CLASIFICADORES_RIESGO C
                          WHERE RN.ID_NEMOTECNICO = N.ID_NEMOTECNICO
                            AND RN.ID_CLASIFICADOR_RIESGO = C.ID_CLASIFICADOR_RIESGO) AS DSC_CLASIFICADOR_RIESGO,
                        CI.CODIGO,
                        SF.COD_SUBFAMILIA
                   FROM REL_ACI_EMP_NEMOTECNICO ACI,
                        VIEW_SALDOS_ACTIVOS SA,
                        ARBOL_CLASE_INSTRUMENTO CI,
                        MONEDAS MO,
                        INSTRUMENTOS I,
                        NEMOTECNICOS N
                        LEFT OUTER JOIN EMISORES_ESPECIFICO EE ON EE.ID_EMISOR_ESPECIFICO = N.ID_EMISOR_ESPECIFICO
                        LEFT JOIN SUBFAMILIAS SF ON (N.ID_SUBFAMILIA=SF.ID_SUBFAMILIA)
                  WHERE N.ID_NEMOTECNICO = ACI.ID_NEMOTECNICO
                    AND SA.ID_NEMOTECNICO = N.ID_NEMOTECNICO
                    AND SA.ID_CUENTA IN (SELECT ID_CUENTA FROM @TMP_CUENTAS)
                    AND SA.FECHA_CIERRE = @PFECHA_CIERRE
                    AND ACI.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
                    AND ACI.ID_EMPRESA = @PID_EMPRESA
                    AND CI.ID_ARBOL_CLASE_INST = ACI.ID_ARBOL_CLASE_INST
                    AND MO.ID_MONEDA = N.ID_MONEDA
                    AND N.COD_INSTRUMENTO = I.COD_INSTRUMENTO
                    ) ACT
             GROUP BY FECHA_CIERRE
              , ID_NEMOTECNICO
              , NEMOTECNICO
              , EMISOR
              , COD_EMISOR
              , DSC_NEMOTECNICO
              , PRECIO
              , TASA_EMISION
              , FECHA_VENCIMIENTO
              , TASA
              , TASA_COMPRA
              , PRECIO_COMPRA
              , ID_MONEDA_CTA
              , ID_MONEDA
              , SIMBOLO_MONEDA
              , ID_EMPRESA
              , ID_ARBOL_CLASE_INST
              , COD_INSTRUMENTO
              , DSC_ARBOL_CLASE_INST
              , PRECIO_PROMEDIO_COMPRA
              , DSC_PADRE_ARBOL_CLASE_INST
              , DURACION
              , COD_PRODUCTO
              , ID_PADRE_ARBOL_CLASE_INST
              , DSC_CLASIFICADOR_RIESGO
              , COD_SUBFAMILIA
              , CODIGO
             ORDER BY NEMOTECNICO

     SET NOCOUNT OFF;
     DECLARE @DB_NULL_STATEMENT_2 INT
END
GO

GRANT EXECUTE ON [PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO_CONSOLIDADO_vr2] TO DB_EXECUTESP
GO
