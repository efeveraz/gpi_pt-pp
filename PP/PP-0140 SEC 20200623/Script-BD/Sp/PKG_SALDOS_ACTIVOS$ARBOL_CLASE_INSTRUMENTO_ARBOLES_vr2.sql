USE [CSGPI]
GO
IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_ARBOLES_vr2')
   DROP PROCEDURE PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_ARBOLES_vr2
GO
CREATE PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_ARBOLES_vr2]
( @PID_CUENTA  NUMERIC = null,
  @PID_EMPRESA NUMERIC,
  @PFECHA_CIERRE DATETIME,
  @pId_Cliente numeric = null,
  @pId_Grupo   numeric = null,
  @pId_Moneda_Salida numeric = null,
  @pConsolidado varchar(3) = null,
  @pId_Aci_Tipo numeric = null
) AS
BEGIN
     set nocount on
     DECLARE @PFECHA_ANTERIOR DATETIME
     SET @PFECHA_ANTERIOR = dbo.Pkg_Global$UltimoDiaMesAnterior(@PFECHA_CIERRE)

     DECLARE @LID_MONEDA_SALIDA NUMERIC(04)
     SET @LID_MONEDA_SALIDA = (SELECT ID_MONEDA FROM CUENTAS WHERE ID_CUENTA = @PID_CUENTA)
     SET @PID_MONEDA_SALIDA = ISNULL(@PID_MONEDA_SALIDA,@LID_MONEDA_SALIDA)
     SET @pId_Aci_Tipo = 2
     DECLARE @Z TABLE(ID   NUMERIC,
          DSC_ARBOL VARCHAR(100),
          PADRE  NUMERIC NULL,
          CODIGO  VARCHAR(6),
          ORDEN       NUMERIC)

     DECLARE @ARBOL_PASO TABLE(ID_ARBOL   INT,
      SECUENCIA   VARCHAR(1000),
      NIVEL    INT,
      VALOR_RAMA   FLOAT NULL,
      VALOR_ANTERIOR_RAMA FLOAT NULL,
      VALOR_HOJA   FLOAT NULL,
      VALOR_ANTERIOR_HOJA FLOAT NULL)

     DECLARE @ARBOL_PASO_2 TABLE(ID_ARBOL_2   INT,
       SECUENCIA   VARCHAR(1000),
       NIVEL    INT,
       VALOR_RAMA   FLOAT NULL,
       VALOR_ANTERIOR_RAMA FLOAT NULL,
       VALOR_HOJA   FLOAT NULL,
       VALOR_ANTERIOR_HOJA FLOAT NULL)
     INSERT @Z
     SELECT ID_ARBOL_CLASE_INST AS ID,
         DSC_ARBOL_CLASE_INST AS DSC_ARBOL,
         ID_PADRE_ARBOL_CLASE_INST AS PADRE,
         CODIGO,
         ORDEN
       FROM ARBOL_CLASE_INSTRUMENTO
      WHERE ID_EMPRESA = @PID_EMPRESA
        AND ID_ACI_TIPO = ISNULL(@PID_ACI_TIPO, ID_ACI_TIPO) --JQ
      ORDER BY ORDEN

     INSERT @ARBOL_PASO
     SELECT ID,
         ORDEN, --RIGHT(SPACE(10) + CONVERT(VARCHAR(10),ID),10),
         1,
         ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_RAMA_CONSOLIDADO_vr2(ID,
                                                                 @PID_CUENTA,
                                                  @PFECHA_CIERRE,
                                                  @pId_Cliente,
                                                  @pId_Grupo,
                                                  @pId_Moneda_Salida,
                                                  @pConsolidado,
                                                  @PID_EMPRESA),0) AS VALOR_RAMA,
         ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_RAMA_CONSOLIDADO_vr2(ID,
                                                  @PID_CUENTA,
                                                  @PFECHA_ANTERIOR,
                                                  @pId_Cliente,
                                                  @pId_Grupo,
                                                  @pId_Moneda_Salida,
                                                  @pConsolidado,
                                                  @PID_EMPRESA),0) AS VALOR_ANTERIOR_RAMA,
         0,
         0
       FROM @Z ZZ
   WHERE (PADRE IS NULL OR (SELECT COUNT(*) FROM @Z ZS WHERE ZS.PADRE = ZZ.ID)>1)

     DECLARE @I INT
     SELECT @I = 0

     WHILE @@ROWCOUNT > 0
   BEGIN
     SELECT @I = @I + 1

     INSERT @arbol_paso
     SELECT ZZ.ID,
         SECUENCIA + RIGHT(SPACE(10) + CONVERT(VARCHAR(10),ZZ.ID),10),
         @I + 1,
         ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_VR2(ID_arbol, @PID_CUENTA, @PFECHA_CIERRE,
          @pId_Cliente,
          @pId_Grupo,
          @pId_Moneda_Salida,
          @pConsolidado,
          @PID_EMPRESA),0),
         ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_VR2(ID_arbol, @PID_CUENTA, @PFECHA_ANTERIOR,
          @pId_Cliente,
      @pId_Grupo,
          @pId_Moneda_Salida,
          @pConsolidado,
          @PID_EMPRESA),0),
         0,
         0
         FROM @Z ZZ, @arbol_paso x
      WHERE x.NIVEL = @I
     and ZZ.PADRE = x.ID_arbol
   END

     UPDATE @ARBOL_PASO SET VALOR_HOJA = ISNULL(DBO.Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado_VR2(ID_ARBOL, @PID_CUENTA, @PFECHA_CIERRE,
          @pId_Cliente,
          @pId_Grupo,
          @pId_Moneda_Salida,
          @pConsolidado,
          @PID_EMPRESA),0) where NIVEL <> 1

     INSERT @ARBOL_PASO_2
     SELECT * FROM @ARBOL_PASO

     UPDATE @ARBOL_PASO_2 SET VALOR_HOJA = (SELECT SUM(A.VALOR_HOJA)
                            FROM @ARBOL_PASO A,
                              @Z ZZ
                           WHERE A.ID_ARBOL = ZZ.ID
                          AND ZZ.PADRE = ID_ARBOL)
   WHERE (SELECT COUNT(*) FROM @Z WHERE PADRE = ID_ARBOL_2) > 1

     DECLARE @SALIDA TABLE (ID   NUMERIC,
                   DSC_ARBOL_CLASE_INST VARCHAR(100),
                   NIVEL     NUMERIC,
                   MONTO_MON_CTA   FLOAT,
                   MONTO_MON_CTA_ANTERIOR FLOAT,
                   VALOR_HOJA    FLOAT,
                   PADRE     NUMERIC,
                   CODIGO     VARCHAR(6))

     INSERT INTO @SALIDA
     SELECT ID_ARBOL_2,
            SPACE((NIVEL-1)*4) + ZZ.DSC_ARBOL AS DSC_ARBOL_CLASE_INST ,
            NIVEL,
            VALOR_RAMA AS MONTO_MON_CTA,
            VALOR_ANTERIOR_RAMA AS MONTO_MON_CTA_ANTERIOR,
            VALOR_HOJA,
            PADRE,
            ZZ.CODIGO --JQ
      FROM @ARBOL_PASO_2, @Z ZZ
     WHERE ID_ARBOL_2 = ZZ.ID
       AND  NIVEL= 1
     ORDER BY SECUENCIA


     UPDATE @SALIDA
        SET MONTO_MON_CTA = (MONTO_MON_CTA + (SELECT MONTO_MON_CTA
                                                FROM @SALIDA S
                                               WHERE S.ID = ID AND PADRE IS NOT NULL))
      WHERE ID = (SELECT PADRE FROM @SALIDA S WHERE S.ID = ID AND PADRE IS NOT NULL)

     DELETE FROM @SALIDA WHERE PADRE IS NOT NULL

     SELECT * FROM @SALIDA ORDER BY ID
END
GO

GRANT EXECUTE ON [PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_ARBOLES_vr2] TO DB_EXECUTESP
GO
