USE [CSGPI]
GO
IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[PKG_PATRIMONIO_CUENTAS$RENTABILIDAD_PERIODOS_2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
   DROP PROCEDURE [dbo].[PKG_PATRIMONIO_CUENTAS$RENTABILIDAD_PERIODOS_2]
GO
CREATE PROCEDURE [dbo].[PKG_PATRIMONIO_CUENTAS$RENTABILIDAD_PERIODOS_2]
(@PID_CUENTA    NUMERIC  = NULL
,@PID_CLIENTE   NUMERIC  = NULL
,@PID_GRUPO     NUMERIC  = NULL
,@PFECHA_INICIO DATETIME = NULL
,@PFECHA_FINAL  DATETIME = NULL
,@PID_EMPRESA   NUMERIC  = NULL
)AS
BEGIN

 SET NOCOUNT ON

    DECLARE @LCONSOLIDADO       VARCHAR(3)
          , @LID_MONEDA_CLP     NUMERIC
          , @LID_ENTIDAD        NUMERIC
          , @LCONS_RENTABILIDAD FLOAT
          , @LCONS_FECHA        DATETIME
          , @LCONS_PATRI        FLOAT
          , @LCONS_APO_RET      FLOAT
          , @LCONS_VALOR_CUOTA  FLOAT
          , @LCONS_PATRI_ANT    FLOAT
          , @LCONS_CONTADOR     INT

 CREATE TABLE #TBLCURSORCONS(FECHA_CIERRE   DATETIME
                           , PATRIMONIO     FLOAT
                          , APORTE_RETIRO  FLOAT)

----------------------------------------------------------
 DECLARE @FECHA         DATETIME
       , @VC            NUMERIC(28,8)
       , @LRENTABILIDAD NUMERIC(28,8)
       , @VC_100        NUMERIC(28,8)
       , @RENTAB_ACUM   NUMERIC(28,8)
       , @LFECHA_INICIO DATETIME
       , @LID           NUMERIC

 CREATE TABLE #SALIDA (FECHA_CIERRE    DATETIME
                     , VALOR_CUOTA     FLOAT --NUMERIC(28,8)
                     , RENTABILIDAD    NUMERIC(28,8)
                     , VALOR_CUOTA_100 NUMERIC(28,8)
                     , RENTAB_ACUM     NUMERIC(28,8))

 CREATE TABLE #TBLCUENTAS (ID_CUENTA            NUMERIC
                         , ID_MONEDA            NUMERIC
                         , FECHA_OPERATIVA      DATETIME
                         , PRIMERA_FECHA_CIERRE DATETIME)

 CREATE TABLE #TBLCURSOR(ID NUMERIC IDENTITY
                       , FECHA_CIERRE DATETIME
                       , VALOR_CUOTA  NUMERIC(28,8)
                       , RENTABILIDAD NUMERIC(28,8))

----------------------------------------------------------
 IF ISNULL(@PID_CUENTA, 0) <> 0
 BEGIN
    SET @LCONSOLIDADO = 'CTA'
    SET @LID_ENTIDAD  = @PID_CUENTA

    INSERT INTO #TBLCUENTAS
    SELECT ID_CUENTA
         , ID_MONEDA
         , FECHA_OPERATIVA
         , (SELECT MIN(FECHA_CIERRE) FROM PATRIMONIO_CUENTAS WHERE ID_CUENTA = C.ID_CUENTA)
      FROM CUENTAS C
     WHERE ID_CUENTA = @PID_CUENTA
       AND ID_EMPRESA = ISNULL(@PID_EMPRESA, ID_EMPRESA)
 END

 IF ISNULL(@PID_CLIENTE, 0) <> 0
 BEGIN
    SET @LCONSOLIDADO ='CLT'
    SET @LID_ENTIDAD  = @PID_CLIENTE

    INSERT INTO #TBLCUENTAS
    SELECT ID_CUENTA
         , ID_MONEDA
         , FECHA_OPERATIVA
         , (SELECT MIN(FECHA_CIERRE) FROM PATRIMONIO_CUENTAS WHERE ID_CUENTA = C.ID_CUENTA)
      FROM CUENTAS C
     WHERE ID_CLIENTE = @PID_CLIENTE
       AND COD_ESTADO = 'H'
       AND ID_EMPRESA = ISNULL(@PID_EMPRESA, ID_EMPRESA)
 END

 IF ISNULL(@PID_GRUPO, 0) <> 0
 BEGIN
    SET @LCONSOLIDADO ='GRP'
    SET @LID_ENTIDAD  = @PID_GRUPO

  INSERT INTO #TBLCUENTAS
  SELECT ID_CUENTA
       , ID_MONEDA
       , FECHA_OPERATIVA
       , (SELECT MIN(FECHA_CIERRE) FROM PATRIMONIO_CUENTAS WHERE ID_CUENTA = C.ID_CUENTA)
    FROM CUENTAS C
   WHERE ID_CUENTA  IN (SELECT ID_CUENTA
                          FROM REL_CUENTAS_GRUPOS_CUENTAS
                         WHERE ID_GRUPO_CUENTA = @PID_GRUPO)
     AND COD_ESTADO = 'H'
     AND ID_EMPRESA = ISNULL(@PID_EMPRESA, ID_EMPRESA)
 END
----------------------------------------------------------
 IF @LCONSOLIDADO <> 'CTA'
 BEGIN
      SELECT @LID_MONEDA_CLP = DBO.FNT_DAMEIDMONEDA('$$')

      INSERT INTO #TBLCURSORCONS
      SELECT P.FECHA_CIERRE,
             SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(P.ID_CUENTA,
                                                        P.PATRIMONIO_MON_CUENTA,
                                                        P.ID_MONEDA_CUENTA,
                                                        @LID_MONEDA_CLP,
                                                        P.FECHA_CIERRE)) AS PATRI,
             SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(P.ID_CUENTA,
                                                        P.APORTE_RETIROS_MON_CUENTA,
                                                        P.ID_MONEDA_CUENTA,
                                                        @LID_MONEDA_CLP,
                                                        P.FECHA_CIERRE)) AS APOR_RET
        FROM PATRIMONIO_CUENTAS P
       WHERE P.ID_CUENTA IN (SELECT ID_CUENTA FROM #TBLCUENTAS)
         AND P.FECHA_CIERRE BETWEEN @PFECHA_INICIO - 1 AND @PFECHA_FINAL
       GROUP BY P.FECHA_CIERRE
       ORDER BY P.FECHA_CIERRE

      SET @LCONS_CONTADOR = 1
      SET @VC_100 = 100
      WHILE EXISTS (SELECT 1 FROM #TBLCURSORCONS)
      BEGIN
           SELECT TOP 1 @LCONS_FECHA = FECHA_CIERRE
                , @LCONS_PATRI = PATRIMONIO
                , @LCONS_APO_RET = APORTE_RETIRO
             FROM #TBLCURSORCONS

           IF @LCONS_CONTADOR = 1
           BEGIN
                SET @LCONS_PATRI_ANT = @LCONS_PATRI
                SET @LCONS_VALOR_CUOTA = 100
                SET @LCONS_RENTABILIDAD = 0
           END
           ELSE
           BEGIN
                SET @LCONS_RENTABILIDAD = 0
                IF @LCONS_PATRI_ANT = 0
                    SET @LCONS_RENTABILIDAD = 0
                ELSE
                    IF @LCONS_APO_RET >= 0
                        SET @LCONS_RENTABILIDAD = (@LCONS_PATRI - (@LCONS_PATRI_ANT + @LCONS_APO_RET))/(@LCONS_PATRI_ANT + @LCONS_APO_RET) * 100
                    ELSE
                        SET @LCONS_RENTABILIDAD = (((@LCONS_PATRI - @LCONS_APO_RET) - @LCONS_PATRI_ANT) / @LCONS_PATRI_ANT) * 100

                SET @LCONS_VALOR_CUOTA = @LCONS_VALOR_CUOTA * (1 + @LCONS_RENTABILIDAD)
                SET @LCONS_PATRI_ANT = @LCONS_PATRI
           END

           SET @RENTAB_ACUM = ((@VC_100 * (1 + (@LCONS_RENTABILIDAD / 100))) / 100 - 1) * 100
           SET @VC_100 = @VC_100 * (1 + (@LCONS_RENTABILIDAD / 100))
           SET @LCONS_CONTADOR = @LCONS_CONTADOR + 1

           INSERT INTO #SALIDA
           SELECT @LCONS_FECHA
                , @LCONS_VALOR_CUOTA
                , @LCONS_RENTABILIDAD
                , @VC_100
                , @RENTAB_ACUM

           DELETE FROM #TBLCURSORCONS WHERE FECHA_CIERRE=@LCONS_FECHA
      END
 END
----------------------------------------------------------
 ELSE
 BEGIN
      INSERT INTO #SALIDA
      SELECT FECHA_CIERRE
           , ISNULL(VALOR_CUOTA_MON_CUENTA, 0) AS VALOR_CUOTA_MON_CUENTA
           , ISNULL(RENTABILIDAD_MON_CUENTA, 0)
           , 0.0
           , 0.0
        FROM PATRIMONIO_CUENTAS
       WHERE ID_CUENTA = @PID_CUENTA
         AND FECHA_CIERRE >= ISNULL(@PFECHA_INICIO, FECHA_CIERRE)
         AND FECHA_CIERRE <= ISNULL(@PFECHA_FINAL, FECHA_CIERRE)
       ORDER BY FECHA_CIERRE

      SET @VC_100 = 100

      INSERT INTO #TBLCURSOR ( FECHA_CIERRE
                             , VALOR_CUOTA
                             , RENTABILIDAD)
      SELECT FECHA_CIERRE
           , VALOR_CUOTA
           , RENTABILIDAD
        FROM #SALIDA
       ORDER BY FECHA_CIERRE

      WHILE  EXISTS (SELECT 1 FROM #TBLCURSOR)
      BEGIN
           SELECT TOP 1 @LID = ID
                , @FECHA = FECHA_CIERRE
                , @VC = VALOR_CUOTA
                , @LRENTABILIDAD = RENTABILIDAD
             FROM #TBLCURSOR
             ORDER BY ID

           SET @RENTAB_ACUM = ((@VC_100 * (1 + (@LRENTABILIDAD / 100))) / 100 - 1) * 100
           SET @VC_100 = @VC_100 * (1 + (@LRENTABILIDAD / 100))

           UPDATE #SALIDA
              SET VALOR_CUOTA_100 = @VC_100
                , RENTAB_ACUM = @RENTAB_ACUM
            WHERE FECHA_CIERRE = @FECHA

           DELETE FROM #TBLCURSOR WHERE ID = @LID
      END
 END
----------------------------------------------------------

 SELECT FECHA_CIERRE
      , ISNULL(VALOR_CUOTA, 0) VALOR_CUOTA
      , ISNULL(RENTABILIDAD, 0) RENTABILIDAD
      , ISNULL(VALOR_CUOTA_100, 0) VALOR_CUOTA_100
      , ISNULL(RENTAB_ACUM, 0) RENTAB_ACUM2
      , ISNULL(RENTAB_ACUM, 0) RENTAB_ACUM
   FROM #SALIDA
  ORDER BY FECHA_CIERRE

 SET NOCOUNT OFF

 DROP TABLE #TBLCUENTAS
 DROP TABLE #TBLCURSORCONS
 DROP TABLE #SALIDA
 DROP TABLE #TBLCURSOR
END
GO

GRANT EXECUTE ON [PKG_PATRIMONIO_CUENTAS$RENTABILIDAD_PERIODOS_2] TO DB_EXECUTESP
GO
