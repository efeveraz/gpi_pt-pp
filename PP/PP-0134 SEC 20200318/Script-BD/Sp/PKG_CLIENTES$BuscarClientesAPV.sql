USE [CSGPI]
GO
IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_CLIENTES$BuscarClientesAPV]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
DROP PROCEDURE [DBO].[PKG_CLIENTES$BuscarClientesAPV]
GO

CREATE PROCEDURE [dbo].[PKG_CLIENTES$BuscarClientesAPV]
@Pid_Cliente float(53)   =  NULL,
@PRut_Cliente varchar(10)=  NULL
AS
BEGIN

            SELECT * ,
                   CASE WHEN (tipo_entidad = 'J') THEN  razon_social
		           ELSE
		              (isnull (nombres, '') + ' ' + isnull (paterno, '') + ' ' + isnull (materno, ''))
		           END AS nombre_cliente
              FROM CLIENTES
             WHERE (ID_CLIENTE in (select id_cliente from view_cuentas_vigentes where id_tipocuenta = 2))
			   --AND COD_ESTADO = 'V'
               AND (RUT_CLIENTE = ISNULL(@PRut_Cliente, RUT_CLIENTE))
            --ORDER BY RUT_CLIENTE;

END
GO

GRANT EXECUTE ON [PKG_CLIENTES$BuscarClientesAPV] TO DB_EXECUTESP
GO
