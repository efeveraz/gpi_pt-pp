USE [CisCB]
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[sp_va_cargaCisCb$OperacionesSpot]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE sp_va_cargaCisCb$OperacionesSpot
GO

CREATE PROCEDURE [dbo].[sp_va_cargaCisCb$OperacionesSpot]
  @pFecha  datetime
AS
SET NOCOUNT ON
BEGIN
--
  DECLARE @LFECHA_PROCESO INT
  SET @LFECHA_PROCESO =(CAST(convert(datetime, @PFECHA) AS INT)+693596)  --

  SELECT m.Fecha_movimiento
       , Tipo_operacion = CASE WHEN m.Tipo_de_operacion='C' THEN 'V' ELSE 'C' END
       , cast(m.Fecha_movimiento as nvarchar) + '-' + cast(m.numero_de_nota as nvarchar) 'nro_ope'
       , m.Cliente_contraparte
       , m.Instrumento
       , m.Nominales
       , m.Precio
       , m.Valor
       , Fecha_liquidacion=m.Fecha_movimiento
       , Tipo_liquidacion='PH'
       , g.cuenta_gpi
  from dbo.VIEW_CLIENTE_GPI_SEC as g, dbo.MOVMONE as m
  where g.origen_magic='MAGIC_VALORES'
    and m.Fecha_movimiento=@LFECHA_PROCESO
    and m.Cliente_contraparte=g.cuenta
	and m.estado <> 'A' -- deja fuera las operaciones anuladas
--
END
GO
GRANT EXECUTE ON sp_va_cargaCisCb$OperacionesSpot TO DB_EXECUTESP
GO
