IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_SALDOS_ACTIVOS_INT$Guardar_II]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS_INT$Guardar_II]
GO

CREATE PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS_INT$Guardar_II](
@pID_SALDO_ACTIVO_INT      NUMERIC(18, 0) = NULL OUTPUT,
@pFECHA_CIERRE             DATETIME,
@pORIGEN                   VARCHAR(5),
@pID_CUENTA                NUMERIC(10,0),
@pID_NEMOTECNICO           NUMERIC(10,0) ,
@pMONEDA                   VARCHAR(10) ,
@pFECHA_VENCIMIENTO        DATETIME,
@pCANTIDAD                 NUMERIC(18, 4) ,
@pPRECIO_MERCADO           FLOAT,
@pPRECIO_COMPRA            FLOAT,
@pPRECIO_PROMEDIO_COMPRA   FLOAT,
@pVALOR_MERCADO_MON_ORIGEN NUMERIC(18,4),
@pVALOR_MERCADO_MON_USD    NUMERIC(18,4),
@pMONTO_INVERTIDO          NUMERIC(18,4),
@pCUENTA_PSH               VARCHAR(10) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		DECLARE @LMSG_RESULTADO         VARCHAR(1000),
				@LCOD_RESULTADO         VARCHAR(5),
				@LPRECIO_COMPRA         FLOAT,
				@LCANTIDAD              NUMERIC(18,4),
				@FECHA_EGRESO_CUSTODIA  DATETIME

		SET @LCOD_RESULTADO = 'OK'
		SET @LMSG_RESULTADO = ''

		IF EXISTS(SELECT 1 FROM REL_CUENTA_ATRIBUTOS WHERE ID_CUENTA = @pID_CUENTA AND PSH_MANTIENE_PCOMPRA_GPI = 'S')
		BEGIN
			SELECT @FECHA_EGRESO_CUSTODIA = DATEADD(day,-1,PSH_FECHA_EGRESO_CUSTODIA)
              FROM REL_CUENTA_ATRIBUTOS
             WHERE ID_CUENTA = @pID_CUENTA
               AND PSH_MANTIENE_PCOMPRA_GPI = 'S'

            SELECT @LPRECIO_COMPRA = PRECIO_COMPRA,
                   @LCANTIDAD      = CANTIDAD
              FROM SALDOS_ACTIVOS
             WHERE ID_CUENTA      = @pID_CUENTA
               AND ID_NEMOTECNICO = @pID_NEMOTECNICO
               AND FECHA_CIERRE   = @FECHA_EGRESO_CUSTODIA

			IF @@ROWCOUNT > 0
            BEGIN
				IF @pCANTIDAD > @LCANTIDAD
                BEGIN
					SET @pPRECIO_PROMEDIO_COMPRA = ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@pFECHA_CIERRE, @pID_CUENTA, @pID_NEMOTECNICO),0)
                    SET @pMONTO_INVERTIDO = (@pPRECIO_PROMEDIO_COMPRA * @pCANTIDAD)
                END
                ELSE
                BEGIN
					SET @pPRECIO_COMPRA = @LPRECIO_COMPRA
                    SET @pPRECIO_PROMEDIO_COMPRA = @LPRECIO_COMPRA
                    SET @pMONTO_INVERTIDO = (@pPRECIO_PROMEDIO_COMPRA * @pCANTIDAD)
                END
			END
		END

		INSERT INTO dbo.SALDOS_ACTIVOS_INT
		(FECHA_CIERRE
		,ORIGEN
        ,ID_CUENTA
        ,ID_NEMOTECNICO
        ,MONEDA
        ,FECHA_VENCIMIENTO
        ,CANTIDAD
        ,PRECIO_MERCADO
        ,PRECIO_COMPRA
        ,PRECIO_PROMEDIO_COMPRA
        ,MONTO_INVERTIDO
        ,VALOR_MERCADO_MON_ORIGEN
        ,VALOR_MERCADO_MON_USD
        ,CUENTA_PSH)
		VALUES
		(@pFECHA_CIERRE,
		@pORIGEN,
        @pID_CUENTA,
        @pID_NEMOTECNICO,
        @pMONEDA,
        @pFECHA_VENCIMIENTO,
        @pCANTIDAD,
        @pPRECIO_MERCADO,
        @pPRECIO_COMPRA,
        @pPRECIO_PROMEDIO_COMPRA,
        @pMONTO_INVERTIDO,
        @pVALOR_MERCADO_MON_ORIGEN,
        @pVALOR_MERCADO_MON_USD,
        @pCUENTA_PSH)

		SET @pID_SALDO_ACTIVO_INT = @@identity
    END TRY
    BEGIN CATCH

        SET @LCOD_RESULTADO = 'ERROR'
        SET @LMSG_RESULTADO = ''
		                    + ERROR_PROCEDURE()
							+ ':'
							+ CAST(ERROR_LINE() AS NVARCHAR)
							+ ':'
							+ CAST(ERROR_NUMBER() AS NVARCHAR)
							+ ''
							+ CHAR(13)
							+ ' ERROR=' + ERROR_MESSAGE()

    END CATCH

	SELECT @LCOD_RESULTADO 'CODRESULTADO', @LMSG_RESULTADO 'MSGRESULTADO'
	SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_SALDOS_ACTIVOS_INT$Guardar_II] TO DB_EXECUTESP
GO