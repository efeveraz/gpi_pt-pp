IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_CONCILIADOR$CargaContraparteSaldosPsh_II]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_CONCILIADOR$CargaContraparteSaldosPsh_II]
GO

CREATE PROCEDURE [dbo].[PKG_CONCILIADOR$CargaContraparteSaldosPsh_II]
( @PFECHA_CONTRAPARTE DATETIME
) AS
BEGIN
	SET NOCOUNT ON

	CREATE TABLE #SALDOS_PSH (FECHA                  DATETIME
                            , CUENTA                 VARCHAR(50)
                            , MONEDA_POSICION        VARCHAR(20)
                            , CUSIP                  VARCHAR(50)
                            , FAMILIA_PRODUCTO       VARCHAR(100)
                            , NOMBRE                 VARCHAR(200)
                            , NOMBRE_CORTO           VARCHAR(100)
                            , VALOR_MERCADO_USD      NUMERIC(28,8)
                            , CANTIDAD_ULTIMO_TRADE  NUMERIC(28,8)
                            , COSTO_ORIGINAL         NUMERIC(28,8)
                            , PRECIOACTUAL           NUMERIC(28,8)
                            , RUT_CLIENTE            VARCHAR(20)
                            , ISIN                   VARCHAR(12)
							 )

	BEGIN TRY

		DECLARE @LID_CUENTA            NUMERIC
              , @LCODRESULTADO         VARCHAR(10)
              , @LMSGRESULTADO         VARCHAR(800)
              , @LCODERROR             NUMERIC
              , @LMSGERR               VARCHAR(100)
              , @LFECHA_CONTRAPARTE    VARCHAR(8)

		SET @LMSGRESULTADO = ''
		SET @LCODRESULTADO = 'OK'
		SELECT @LFECHA_CONTRAPARTE = CONVERT(VARCHAR(8),@PFECHA_CONTRAPARTE,112)

		DELETE FROM CONC_SALDOS_PSH

		INSERT #SALDOS_PSH
		EXEC PKG_PERSHING$BuscaSaldos_III @pfecProceso=@LFECHA_CONTRAPARTE, @pCodErr=@LCODERROR,  @pMsgErr=@LMSGERR

		INSERT INTO CONC_SALDOS_PSH(FECHA
                                  , RUT_CLIENTE
                                  , ID_CUENTA
                                  , ID_NEMOTECNICO
                                  , CODIGO_CUSIP
                                  , NEMOTECNICO
                                  , CANTIDAD
                                  , PRECIO_MERCADO
                                  , VALOR_MERCADO
                                  , CUENTA_PSH
                                   )
		SELECT @PFECHA_CONTRAPARTE AS FECHA
             , T.RUT_CLIENTE
             , ISNULL(VA.ID_ENTIDAD,-1) AS ID_CUENTA
             , ISNULL((SELECT TOP 1 ID_NEMOTECNICO
                         FROM REL_NEMOTECNICO_ATRIBUTOS
                        WHERE CUSIP = T.CUSIP),-1)
             , T.CUSIP
             , T.NOMBRE_CORTO
             , T.CANTIDAD_ULTIMO_TRADE
             , CASE
               WHEN FAMILIA_PRODUCTO = '(FIX INC)' THEN (T.PRECIOACTUAL / 100)
               ELSE T.PRECIOACTUAL
               END
             , T.VALOR_MERCADO_USD
             , T.CUENTA
          FROM #SALDOS_PSH T
          LEFT JOIN VIEW_ALIAS VA ON VA.VALOR      = T.RUT_CLIENTE + '/' + T.CUENTA
                                 AND VA.TABLA      = 'CUENTAS'
                                 AND VA.COD_ORIGEN = 'PERSHING'


        SELECT 'OK'

	END TRY
	BEGIN CATCH
		SET @LCODERROR = @@ERROR
		SET @LMSGERR = 'Error en el procedimiento PKG_CONCILIADOR$CargaContraparteSaldosPsh_II: ' + ERROR_MESSAGE()
		SELECT @LMSGERR
	END CATCH

	DROP TABLE #SALDOS_PSH
	SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_CONCILIADOR$CargaContraparteSaldosPsh_II] TO DB_EXECUTESP
GO
