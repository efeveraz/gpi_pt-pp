IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_PERSHING$BuscaSaldos_III]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_PERSHING$BuscaSaldos_III]
GO

CREATE PROCEDURE [dbo].[PKG_PERSHING$BuscaSaldos_III] (
  @pfecProceso DATETIME,
  @pCuenta     NUMERIC(10) = NULL,
  @pRutCliente VARCHAR(15) = NULL,
  @pCodErr     INT           OUTPUT,
  @pMsgErr     VARCHAR(4000) OUTPUT
) AS
BEGIN

	SET NOCOUNT ON

	CREATE TABLE  #TBLCUENTA_PSH (CUENTA  VARCHAR(9))

	BEGIN TRY

		DECLARE @LFECHAPROCESO DATETIME, @LCUENTA NUMERIC(10)

		SET @pCodErr= 0
		SET @pMsgErr= ''
		SET @LFECHAPROCESO = @PFECPROCESO

		INSERT INTO #TBLCUENTA_PSH
		SELECT RTRIM(LTRIM(SUBSTRING ( VALOR, CHARINDEX('/', VALOR) + 1, LEN(VALOR)))) 'VALOR'
		  FROM VIEW_ALIAS
		 WHERE ID_ENTIDAD = @pCuenta
	       AND TABLA = 'CUENTAS'
		   AND COD_ORIGEN = 'PERSHING'

		SELECT @LCUENTA = COUNT(CUENTA) FROM #TBLCUENTA_PSH

		IF @LCUENTA > 0
		BEGIN
			SELECT
				   FECHA,
				   CUENTA,
				   MONEDA_POSICION,
				   CUSIP,
				   FAMILIA_PRODUCTO,
				   NOMBRE,
				   NOMBRE_CORTO,
				   VALOR_MERCADO_USD,
				   CANTIDAD_ULTIMO_TRADE,
				   COSTO_ORIGINAL,
				   PRECIOACTUAL,
				   RUT_CLIENTE,
				   ISIN
			  FROM (
					SELECT @PFECPROCESO 'FECHA' --S.FECHA
					     , S.CUENTA
						 , S.MONEDA_POSICION
						 , S.CUSIP
						 , I.FAMILIA_PRODUCTO
						 , I.NOMBRE
						 , I.NOMBRE_CORTO  AS  'NOMBRE_CORTO'
						 , SUM(S.VALOR_MERCADO_USD) AS 'VALOR_MERCADO_USD'
						 , SUM(S.CANTIDAD_ULTIMO_TRADE) AS 'CANTIDAD_ULTIMO_TRADE'
						 , S.COSTO_ORIGINAL
						 , PRECIOACTUAL = (SELECT TOP 1 ULTIMO_PRECIO
						   					 FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_PRECIOS PS
						   					WHERE PS.CUSIP = S.CUSIP AND FECHA <= @LFECHAPROCESO
						   					ORDER BY FECHA DESC)
						 , RTRIM(LTRIM(MAX_P.RUT_CLIENTE)) 'RUT_CLIENTE'
						 , I.ISIN
                      FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_POSICION S,
						   (SELECT B.CUENTA, MAX(B.FECHA) 'FECHA', RTRIM(LTRIM(C.RUT_CLIENTE)) 'RUT_CLIENTE'
                              FROM [LNKS-FM-P013].PERSHING.PERSHING.CLIENTES_PERSHING C,
                                   [LNKS-FM-P013].PERSHING.PERSHING.PSH_POSICION B
                             WHERE C.ADC            = 'S'
                               AND B.CUENTA         = C.CUENTA_CLIENTE
                               AND B.FECHA          <= @LFECHAPROCESO
                             GROUP BY B.CUENTA, C.RUT_CLIENTE) MAX_P,
                           (SELECT CUSIP, MAX(FECHA_ULTIMA_MODIFICACION) FECHA_ULTIMA_MODIFICACION
                              FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_INSTRUMENTOS
                             WHERE FECHA_ULTIMA_MODIFICACION <= @PFECPROCESO
                             GROUP BY CUSIP) MAX_I,
		                   (SELECT FAMILIA_PRODUCTO, NOMBRE, NOMBRE_CORTO, CUSIP, FECHA_ULTIMA_MODIFICACION, ISIN
                              FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_INSTRUMENTOS) I
                             WHERE S.FECHA = MAX_P.FECHA
                               AND S.CUENTA = MAX_P.CUENTA
	                           AND NOT S.CUSIP LIKE '%99999%'
	                           AND MAX_I.CUSIP = S.CUSIP
                               AND I.CUSIP = S.CUSIP
                               AND I.FECHA_ULTIMA_MODIFICACION = MAX_I.FECHA_ULTIMA_MODIFICACION
                             GROUP BY S.CUENTA, S.CUSIP, S.MONEDA_POSICION, I.FAMILIA_PRODUCTO, I.NOMBRE, I.NOMBRE_CORTO,
						              S.COSTO_ORIGINAL, MAX_P.RUT_CLIENTE, I.ISIN

				   ) as POS
             WHERE CANTIDAD_ULTIMO_TRADE > 0
               AND CUENTA IN (SELECT CUENTA FROM #TBLCUENTA_PSH)
               AND RUT_CLIENTE = ISNULL(@pRutCliente,RUT_CLIENTE)
             ORDER BY CUENTA, CUSIP
        END
        ELSE
        BEGIN
			SELECT
				   FECHA,
				   CUENTA,
				   MONEDA_POSICION,
				   CUSIP,
				   FAMILIA_PRODUCTO,
				   NOMBRE,
				   NOMBRE_CORTO,
				   VALOR_MERCADO_USD,
				   CANTIDAD_ULTIMO_TRADE,
				   COSTO_ORIGINAL,
				   PRECIOACTUAL,
				   RUT_CLIENTE,
				   ISIN
			  FROM (
				    SELECT @PFECPROCESO 'FECHA' --S.FECHA
						 , S.CUENTA
						 , S.MONEDA_POSICION
						 , S.CUSIP
						 , I.FAMILIA_PRODUCTO
						 , I.NOMBRE
						 , I.NOMBRE_CORTO  AS  'NOMBRE_CORTO'
						 , SUM(S.VALOR_MERCADO_USD) AS 'VALOR_MERCADO_USD'
						 , SUM(S.CANTIDAD_ULTIMO_TRADE) AS 'CANTIDAD_ULTIMO_TRADE'
						 , S.COSTO_ORIGINAL
						 , PRECIOACTUAL = (SELECT TOP 1 ULTIMO_PRECIO
						   					 FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_PRECIOS PS
						   				    WHERE PS.CUSIP = S.CUSIP AND FECHA <= @LFECHAPROCESO
						   				    ORDER BY FECHA DESC)
						 , RTRIM(LTRIM(MAX_P.RUT_CLIENTE)) 'RUT_CLIENTE'
						 , I.ISIN
					  FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_POSICION S,
						   (SELECT B.CUENTA, MAX(B.FECHA) 'FECHA', RTRIM(LTRIM(C.RUT_CLIENTE)) 'RUT_CLIENTE'
						   	  FROM [LNKS-FM-P013].PERSHING.PERSHING.CLIENTES_PERSHING C,
						    	   [LNKS-FM-P013].PERSHING.PERSHING.PSH_POSICION B
						     WHERE C.ADC            = 'S'
						   	   AND B.CUENTA         = C.CUENTA_CLIENTE
						       AND B.FECHA          <= @LFECHAPROCESO
						     GROUP BY B.CUENTA, C.RUT_CLIENTE) MAX_P,
						   (SELECT CUSIP, MAX(FECHA_ULTIMA_MODIFICACION) FECHA_ULTIMA_MODIFICACION
						   	  FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_INSTRUMENTOS
						     WHERE FECHA_ULTIMA_MODIFICACION <= @PFECPROCESO
						     GROUP BY CUSIP) MAX_I,
						   (SELECT FAMILIA_PRODUCTO, NOMBRE, NOMBRE_CORTO, CUSIP, FECHA_ULTIMA_MODIFICACION, ISIN
						   	  FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_INSTRUMENTOS) I
				     WHERE S.FECHA = MAX_P.FECHA
					   AND S.CUENTA = MAX_P.CUENTA
					   AND NOT S.CUSIP LIKE '%99999%'
					   AND MAX_I.CUSIP = S.CUSIP
					   AND I.CUSIP = S.CUSIP
					   AND I.FECHA_ULTIMA_MODIFICACION = MAX_I.FECHA_ULTIMA_MODIFICACION
				     GROUP BY S.CUENTA, S.CUSIP, S.MONEDA_POSICION, I.FAMILIA_PRODUCTO, I.NOMBRE, I.NOMBRE_CORTO, S.COSTO_ORIGINAL, MAX_P.RUT_CLIENTE, I.ISIN
			       ) as POS
			   WHERE CANTIDAD_ULTIMO_TRADE > 0
			     AND RUT_CLIENTE = ISNULL(@pRutCliente,RUT_CLIENTE)
			   ORDER BY CUENTA, CUSIP
        END
    END TRY
    BEGIN CATCH
        SET @pCodErr = @@ERROR
        SET @pMsgErr = 'Error en el procedimiento PKG_PERSHING$BuscaSaldos_III: ' + ERROR_MESSAGE()
    END CATCH

	DROP TABLE #TBLCUENTA_PSH
    SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_PERSHING$BuscaSaldos_III] TO DB_EXECUTESP
GO

