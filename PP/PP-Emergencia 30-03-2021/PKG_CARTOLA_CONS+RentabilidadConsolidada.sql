IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PKG_CARTOLA_CONS$RentabilidadConsolidada')
   DROP PROCEDURE PKG_CARTOLA_CONS$RentabilidadConsolidada
GO
CREATE PROCEDURE PKG_CARTOLA_CONS$RentabilidadConsolidada
( @PFECHA_CIERRE     DATETIME
, @PID_EMPRESA       NUMERIC
, @PCONSOLIDADO      VARCHAR(3)
, @PID_ENTIDAD       NUMERIC
, @PID_MONEDA_SALIDA NUMERIC = NULL  
) AS  
BEGIN
   SET NOCOUNT ON
   DECLARE @MONEDACUENTA                      VARCHAR(03),  
           @FECHA_ANUAL_AUX                   DATETIME,
           @ID_MONEDA_$$                      NUMERIC,
           @ID_MONEDA_UF                      NUMERIC,
           @ID_MONEDA_DO                      NUMERIC,
           @FECHA_MES                         DATETIME,
           @FECHA_ANUAL_CALENDARIO            DATETIME,
           @FECHA_ANUAL_ULT_12_MESES          DATETIME
  
   DECLARE @TBLRENTAB_XCTA TABLE (FECHA_CIERRE                   DATETIME
                                , RENTABILIDAD_MON_CUENTA        NUMERIC(28,14)
                                , RENTABILIDAD_MENSUAL           NUMERIC(28,14)
                                , HAY_VALOR_CUOTA_MES            CHAR(2)
                                , RENTABILIDAD_ANUAL             NUMERIC(28,14)
                                , HAY_VALOR_CUOTA_ANO_CALENDARIO CHAR(2)
                                , RENTABILIDAD_ULT_12_MESES      NUMERIC(28,14)
                                , HAY_VALOR_CUOTA_ULT_12_MESES   CHAR(2)
                                , VOLATILIDAD_MENSUAL            NUMERIC(28,14)
                                , VOLATILIDAD_ANUAL              NUMERIC(28,14)
                                , VOLATILIDAD_ULT_12_MESES       NUMERIC(28,14)
                                , RENTABILIDAD_INICIO_CUENTA     NUMERIC(28,14)
                                , RENTABILIDAD_MENSUAL_$$        NUMERIC(28,14)
                                , RENTABILIDAD_ANUAL_$$          NUMERIC(28,11)
                                , RENTABILIDAD_ULT_12_MESES_$$   NUMERIC(28,14)
                                , RENTABILIDAD_INICIO_CUENTA_$$  NUMERIC(28,14)
                                , RENTABILIDAD_MENSUAL_UF        NUMERIC(28,14)
                                , RENTABILIDAD_ANUAL_UF          NUMERIC(28,14)
                                , RENTABILIDAD_ULT_12_MESES_UF   NUMERIC(28,14)
                                , RENTABILIDAD_INICIO_CUENTA_UF  NUMERIC(28,14)
                                , RENTABILIDAD_MENSUAL_DO        NUMERIC(28,14)
                                , RENTABILIDAD_ANUAL_DO          NUMERIC(28,14)
                                , RENTABILIDAD_ULT_12_MESES_DO   NUMERIC(28,14)
                                , RENTABILIDAD_INICIO_CUENTA_DO  NUMERIC(28,14))

   IF @PCONSOLIDADO = 'CTA'
    BEGIN
         INSERT INTO @TBLRENTAB_XCTA
         EXEC PKG_PATRIMONIO_CUENTAS$RENTABILIDADES @PID_CUENTA        = @PID_ENTIDAD
                                                  , @PFECHA_CIERRE     = @PFECHA_CIERRE  
                                                  , @PID_MONEDA_SALIDA = @PID_MONEDA_SALIDA
         SELECT *
           FROM @TBLRENTAB_XCTA                                         
    END
   ELSE
    BEGIN
         SELECT @FECHA_MES = DBO.PKG_GLOBAL$ULTIMODIAMESANTERIOR(@PFECHA_CIERRE)

         SET @FECHA_ANUAL_AUX = CONVERT(DATETIME,CONVERT(CHAR(04),YEAR(@PFECHA_CIERRE)) + '0101')
         SELECT @FECHA_ANUAL_CALENDARIO = DBO.PKG_GLOBAL$ULTIMODIAMESANTERIOR(@FECHA_ANUAL_AUX)
  
         SET @FECHA_ANUAL_ULT_12_MESES = DATEADD(YY,-1,@PFECHA_CIERRE)  

         SELECT @ID_MONEDA_$$ = DBO.FNT_DAMEIDMONEDA('$$')
         SELECT @ID_MONEDA_UF = DBO.FNT_DAMEIDMONEDA('UF')
         SELECT @ID_MONEDA_DO = DBO.FNT_DAMEIDMONEDA('USD')

  
         SELECT DBO.FNT_RENTABILIDAD_CONSOLIDADA(@FECHA_MES
                                          ,@PFECHA_CIERRE
                                          ,@PID_EMPRESA
                                          ,@PCONSOLIDADO
                                          ,@PID_ENTIDAD
                                          ,@ID_MONEDA_$$) AS RENTABILIDAD_MENSUAL_$$,
          DBO.FNT_RENTABILIDAD_CONSOLIDADA(@FECHA_MES
                                          ,@PFECHA_CIERRE
                                          ,@PID_EMPRESA
                                          ,@PCONSOLIDADO
                                          ,@PID_ENTIDAD
                                          ,@ID_MONEDA_UF) AS RENTABILIDAD_MENSUAL_UF,
          DBO.FNT_RENTABILIDAD_CONSOLIDADA(@FECHA_MES
                                          ,@PFECHA_CIERRE
                                          ,@PID_EMPRESA
                                          ,@PCONSOLIDADO
                                          ,@PID_ENTIDAD
                                          ,@ID_MONEDA_DO) AS RENTABILIDAD_MENSUAL_DO,
          DBO.FNT_RENTABILIDAD_CONSOLIDADA(@FECHA_ANUAL_CALENDARIO
                                          ,@PFECHA_CIERRE
                                          ,@PID_EMPRESA
                                          ,@PCONSOLIDADO
                                          ,@PID_ENTIDAD
                                          ,@ID_MONEDA_$$) AS RENTABILIDAD_ANUAL_$$,
          DBO.FNT_RENTABILIDAD_CONSOLIDADA(@FECHA_ANUAL_CALENDARIO
                                          ,@PFECHA_CIERRE
                                          ,@PID_EMPRESA
                                          ,@PCONSOLIDADO
                                          ,@PID_ENTIDAD
                                          ,@ID_MONEDA_UF) AS RENTABILIDAD_ANUAL_UF,
          DBO.FNT_RENTABILIDAD_CONSOLIDADA(@FECHA_ANUAL_CALENDARIO
                                          ,@PFECHA_CIERRE
                                          ,@PID_EMPRESA
                                          ,@PCONSOLIDADO
                                          ,@PID_ENTIDAD
                                          ,@ID_MONEDA_DO) AS RENTABILIDAD_ANUAL_DO,
          DBO.FNT_RENTABILIDAD_CONSOLIDADA(@FECHA_ANUAL_ULT_12_MESES
                                          ,@PFECHA_CIERRE
                                          ,@PID_EMPRESA
                                          ,@PCONSOLIDADO
                                          ,@PID_ENTIDAD
                                          ,@ID_MONEDA_$$) AS RENTABILIDAD_ULT_12_MESES_$$,
          DBO.FNT_RENTABILIDAD_CONSOLIDADA(@FECHA_ANUAL_ULT_12_MESES
                                          ,@PFECHA_CIERRE
                                          ,@PID_EMPRESA
                                          ,@PCONSOLIDADO
                                          ,@PID_ENTIDAD
                                          ,@ID_MONEDA_DO) AS RENTABILIDAD_ULT_12_MESES_DO,
          DBO.FNT_RENTABILIDAD_CONSOLIDADA(@FECHA_ANUAL_ULT_12_MESES
                                          ,@PFECHA_CIERRE
                                          ,@PID_EMPRESA
                                          ,@PCONSOLIDADO
                                          ,@PID_ENTIDAD
                                          ,@ID_MONEDA_UF) AS RENTABILIDAD_ULT_12_MESES_UF

    END 
	SET NOCOUNT OFF
END
GO
