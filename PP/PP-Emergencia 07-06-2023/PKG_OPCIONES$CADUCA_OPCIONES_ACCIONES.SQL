USE [CSGPI]
GO
/****** Object:  StoredProcedure [dbo].[PKG_OPCIONES$CADUCA_OPCIONES_ACCIONES]    Script Date: 12/05/2018 17:58:37 ******/
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE name = 'PKG_OPCIONES$CADUCA_OPCIONES_ACCIONES' AND xtype = 'P')
	DROP PROCEDURE PKG_OPCIONES$CADUCA_OPCIONES_ACCIONES
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PKG_OPCIONES$CADUCA_OPCIONES_ACCIONES]
(@PID_CUENTA NUMERIC,
 @PFECHA_CIERRE DATETIME,
 @PID_USUARIO   NUMERIC,
 @PRESULTADO    VARCHAR(1000) OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY
   DECLARE @LPROCESO VARCHAR(100)
   SET @LPROCESO = 'PKG_OPCIONES$CADUCA_OPCIONES_ACCIONES'

   INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)
   VALUES (@PID_USUARIO, @LPROCESO, REPLICATE(CHAR(9), 2) + 'CADUCANDO LAS OPCIONES DE ACCIONES...')

   DECLARE @ID_OPERACION      NUMERIC(10),
           @ID_MOV_ACTIVO      NUMERIC(10)

   DECLARE @ID_DIVIDENDO            NUMERIC(10),
           @ID_NEMOTECNICO          NUMERIC(10),
           @NEMOTECNICO             VARCHAR(50),
           @ID_NEMOTECNICO_OPCION   NUMERIC(10),
           @CADA_X_CANTIDAD         NUMERIC(18,10),
           @ENTREGAR_X_CANTIDAD     NUMERIC(18,10),
           @CANTIDAD                NUMERIC(18,4),
           @MONTO                   NUMERIC(24,10),
           @SALDO                   NUMERIC(18,4),
           @PRECIO                  NUMERIC(18,10)

   DECLARE CURSOR_REVISA_OPCION CURSOR FAST_FORWARD FOR
      SELECT D.ID_DIVIDENDO,
             D.ID_NEMOTECNICO,
             D.ID_NEMOTECNICO_OPCION,
             D.MONTO,
             S.CANTIDAD
      FROM VIEW_SALDOS_ACTIVOS S WITH (NOLOCK) , DIVIDENDOS D WITH (NOLOCK)
     WHERE ID_CUENTA = @PID_CUENTA
       AND S.FECHA_CIERRE = @PFECHA_CIERRE - 1
       AND D.FECHA_CORTE = @PFECHA_CIERRE
       AND S.ID_NEMOTECNICO = D.ID_NEMOTECNICO_OPCION
       AND D.TIPO_VARIACION = 'OPCION'
   FOR READ ONLY

   OPEN CURSOR_REVISA_OPCION
   FETCH NEXT FROM CURSOR_REVISA_OPCION INTO @ID_DIVIDENDO, @ID_NEMOTECNICO, @ID_NEMOTECNICO_OPCION, @PRECIO, @SALDO
   WHILE @@FETCH_STATUS = 0 AND @@ERROR = 0
   BEGIN
        SELECT @SALDO = @SALDO + ISNULL((SELECT SUM(M.CANTIDAD)
        FROM MOV_ACTIVOS M WITH (NOLOCK) , VIEW_OPERACION_DETALLE O WITH (NOLOCK)
        WHERE M.ID_CUENTA = @PID_CUENTA
        AND M.ID_NEMOTECNICO = @ID_NEMOTECNICO_OPCION
        AND O.ID_OPERACION_DETALLE = M.ID_OPERACION_DETALLE
        AND M.FECHA_OPERACION = @PFECHA_CIERRE
        AND M.COD_ESTADO = 'C'
        AND M.FLG_TIPO_MOVIMIENTO = 'I'),0)

        SELECT @SALDO = @SALDO - ISNULL((SELECT SUM(M.CANTIDAD)
        FROM MOV_ACTIVOS M WITH (NOLOCK), VIEW_OPERACION_DETALLE O WITH (NOLOCK)
        WHERE M.ID_CUENTA = @PID_CUENTA
        AND M.ID_NEMOTECNICO = @ID_NEMOTECNICO_OPCION
        AND O.ID_OPERACION_DETALLE = M.ID_OPERACION_DETALLE
        AND M.FECHA_OPERACION = @PFECHA_CIERRE
        AND M.COD_ESTADO = 'C'
        AND M.FLG_TIPO_MOVIMIENTO = 'E'),0)

       IF @SALDO > 0
          BEGIN
              SET @ID_OPERACION = NULL
              SET @ID_MOV_ACTIVO = NULL

              SELECT @NEMOTECNICO = NEMOTECNICO FROM NEMOTECNICOS WITH (NOLOCK) WHERE ID_NEMOTECNICO = @ID_NEMOTECNICO_OPCION

              -- Se Reemplaza MAKE_1_LINE por MAKE_1_LINE_NEW
              EXECUTE DBO.PKG_OPERACIONES$MAKE_1_LINE_NEW  @PID_CUENTA        = @PID_CUENTA
                                                         , @PFLG_TIPO_MOVIMIENTO  = 'E'
                                                         , @PCOD_TIPO_OPERACION   = 'CUST_RETOP'
                                                         , @PDSC_OPERACION        = 'GENERACION AUTOMATICA RETIRO DE OPCIONES'
                                                         , @PFECHA_OPERACION      = @PFECHA_CIERRE
                                                         , @PFECHA_LIQUIDACION    = @PFECHA_CIERRE
                                                         , @PID_NEMOTECNICO       = @ID_NEMOTECNICO_OPCION
                                                         , @PNEMOTECNICO          = @NEMOTECNICO
                                                         , @PCANTIDAD             = @SALDO
                                                         , @PPRECIO               = 0
                               , @PMONTO_PAGO           = 0
                                                         , @PID_MOV_ACTIVO_COMPRA = NULL
                                                         , @PID_OPERACION         = @ID_OPERACION  OUTPUT
                                                         , @PID_USUARIO           = @PID_USUARIO
                                                         , @PRESULTADO            = @PRESULTADO     OUTPUT
              IF @PRESULTADO IS NOT NULL
                 BEGIN
                      RAISERROR(@PRESULTADO, 18, 1)
                 END

              SELECT @ID_MOV_ACTIVO = MA.ID_MOV_ACTIVO
              FROM MOV_ACTIVOS MA WITH (NOLOCK),
                   OPERACIONES_DETALLE OD WITH (NOLOCK)
              WHERE MA.ID_OPERACION_DETALLE = OD.ID_OPERACION_DETALLE
                AND OD.ID_OPERACION = @ID_OPERACION

              UPDATE MOV_ACTIVOS
              SET ID_DIVIDENDO = @ID_DIVIDENDO
              WHERE ID_MOV_ACTIVO = @ID_MOV_ACTIVO
         END

       FETCH NEXT FROM CURSOR_REVISA_OPCION INTO @ID_DIVIDENDO, @ID_NEMOTECNICO, @ID_NEMOTECNICO_OPCION, @PRECIO, @SALDO
   END
   CLOSE CURSOR_REVISA_OPCION
   DEALLOCATE CURSOR_REVISA_OPCION

   /*******************************************************************************************************************/
   IF EXISTS(SELECT 1 FROM OPERACIONES WITH (NOLOCK) WHERE ID_CUENTA = @PID_CUENTA AND FECHA_OPERACION = @PFECHA_CIERRE AND COD_TIPO_OPERACION = 'DIR_OPC' AND COD_ESTADO <> 'A')
      BEGIN
           IF NOT EXISTS(SELECT 1 FROM OPERACIONES WITH (NOLOCK) WHERE ID_CUENTA = @PID_CUENTA AND FECHA_OPERACION = @PFECHA_CIERRE AND COD_TIPO_OPERACION = 'CUST_RETOP' AND COD_ESTADO <> 'A')
              BEGIN
                   DECLARE CURSOR_REVISA_RETIROS CURSOR FAST_FORWARD FOR
                       SELECT D.ID_DIVIDENDO,
                              D.ID_NEMOTECNICO,
                              D.ID_NEMOTECNICO_OPCION,
                              D.MONTO,OD.CANTIDAD
                       FROM VIEW_OPERACION_DETALLE OD WITH (NOLOCK) , DIVIDENDOS D WITH (NOLOCK)
                       WHERE OD.ID_CUENTA = @PID_CUENTA
                         AND OD.ID_NEMOTECNICO = D.ID_NEMOTECNICO
                         AND OD.FECHA_OPERACION = @PFECHA_CIERRE
                         AND OD.COD_TIPO_OPERACION = 'DIR_OPC'
                         AND D.TIPO_VARIACION = 'OPCION'
                         AND OD.COD_ESTADO <> 'A'
                         AND D.FECHA = (SELECT MAX(D.FECHA)
                                        FROM VIEW_OPERACION_DETALLE OD WITH (NOLOCK) , DIVIDENDOS D WITH (NOLOCK)
                                        WHERE OD.ID_CUENTA = @PID_CUENTA
                                          AND OD.ID_NEMOTECNICO = D.ID_NEMOTECNICO
                                          AND OD.FECHA_OPERACION = @PFECHA_CIERRE
                                          AND OD.COD_TIPO_OPERACION = 'DIR_OPC'
                                          AND D.TIPO_VARIACION = 'OPCION'
                                          AND OD.COD_ESTADO <> 'A')
                       FOR READ ONLY
                       OPEN CURSOR_REVISA_RETIROS
                       FETCH NEXT FROM CURSOR_REVISA_RETIROS INTO @ID_DIVIDENDO, @ID_NEMOTECNICO, @ID_NEMOTECNICO_OPCION, @PRECIO, @SALDO
                       WHILE @@FETCH_STATUS = 0 AND @@ERROR = 0
                            BEGIN
                                 SET @ID_OPERACION = NULL
                                 SET @ID_MOV_ACTIVO = NULL
                                 SELECT @NEMOTECNICO = NEMOTECNICO FROM NEMOTECNICOS WITH (NOLOCK) WHERE ID_NEMOTECNICO = @ID_NEMOTECNICO_OPCION

                                 -- Se Reemplaza MAKE_1_LINE por MAKE_1_LINE_NEW
                                 EXECUTE DBO.PKG_OPERACIONES$MAKE_1_LINE_NEW  @PID_CUENTA            = @PID_CUENTA
                                                                            , @PFLG_TIPO_MOVIMIENTO  = 'E'
                                                                            , @PCOD_TIPO_OPERACION   = 'CUST_RETOP'
                                                                            , @PDSC_OPERACION        = 'GENERACION AUTOMATICA RETIRO DE OPCIONES'
                                                                            , @PFECHA_OPERACION      = @PFECHA_CIERRE
                                                                            , @PFECHA_LIQUIDACION    = @PFECHA_CIERRE
                                                                            , @PID_NEMOTECNICO       = @ID_NEMOTECNICO_OPCION
                                                                            , @PNEMOTECNICO          = @NEMOTECNICO
                                                                            , @PCANTIDAD             = @SALDO
                                                                            , @PPRECIO               = 0
                                                                            , @PMONTO_PAGO           = 0
                                                                            , @PID_MOV_ACTIVO_COMPRA = NULL
                                                                            , @PID_OPERACION         = @ID_OPERACION  OUTPUT
                                                                            , @PID_USUARIO           = @PID_USUARIO
                                                                            , @PRESULTADO            = @PRESULTADO     OUTPUT
                                 IF @PRESULTADO IS NOT NULL
                                    BEGIN
                                         RAISERROR(@PRESULTADO, 18, 1)
                                    END

                                 SELECT @ID_MOV_ACTIVO = MA.ID_MOV_ACTIVO
                                 FROM MOV_ACTIVOS MA WITH (NOLOCK), OPERACIONES_DETALLE OD WITH (NOLOCK)
                                 WHERE MA.ID_OPERACION_DETALLE = OD.ID_OPERACION_DETALLE
                                   AND OD.ID_OPERACION = @ID_OPERACION

                                 UPDATE MOV_ACTIVOS
                                 SET ID_DIVIDENDO = @ID_DIVIDENDO
                                WHERE ID_MOV_ACTIVO = @ID_MOV_ACTIVO
                                FETCH NEXT FROM CURSOR_REVISA_RETIROS INTO @ID_DIVIDENDO, @ID_NEMOTECNICO, @ID_NEMOTECNICO_OPCION, @PRECIO, @SALDO
                            END
                       CLOSE CURSOR_REVISA_RETIROS
                       DEALLOCATE CURSOR_REVISA_RETIROS
              END
      END

  SET @PRESULTADO = NULL
END TRY
BEGIN CATCH
  SET @PRESULTADO =  '[' + ERROR_PROCEDURE() + ':' + CAST(ERROR_LINE() AS NVARCHAR) + ':' + CAST(ERROR_NUMBER() AS NVARCHAR) + ']' + CHAR(13) +
                     ' ERROR=' + ERROR_MESSAGE()
  INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)
  VALUES (@PID_USUARIO, @LPROCESO, @PRESULTADO)
END CATCH
SET NOCOUNT OFF
GO
