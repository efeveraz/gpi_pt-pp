ECHO OFF
@ECHO.
@ECHO --------------------------------------------------------------------------------
@ECHO Iniciando la aplicacion Importador Saldo Simultanea
@ECHO --------------------------------------------------------------------------------
@ECHO.

IF "%~1" == "" Goto ERROR_PARAMETROS

START /WAIT CS_CARGA_SAL_SIM_AUT.exe %1
Set CSGPI_ERRORLEVEL=%ERRORLEVEL%

IF %CSGPI_ERRORLEVEL% == 52 GoTo ERROR_ARCHIVO
IF %CSGPI_ERRORLEVEL% GTR 50 GoTo ERROR_GRAVE
IF %CSGPI_ERRORLEVEL% == 0 GoTo CSGPI_OK

:ERROR_PARAMETROS
@ECHO.
@ECHO ********************************************************************************
@ECHO PARAMETROS NO SUMINISTRADOS O SINTAXIS INCORRECTA
@ECHO SINTAXIS: run_CS_SAL_SIM /YYYYMMDD
@ECHO ********************************************************************************
@ECHO.
GoTo SALIR

:ERROR_ARCHIVO
@ECHO.
@ECHO ********************************************************************************
@ECHO EL ARCHIVO NO EXISTE
@ECHO ERRORLEVEL = %CSGPI_ERRORLEVEL%
@ECHO ********************************************************************************
@ECHO.
GoTo SALIR

:ERROR_GRAVE
@ECHO.
@ECHO ********************************************************************************
@ECHO PROBLEMA GRAVE EN LA APLICACION
@ECHO ERRORLEVEL = %CSGPI_ERRORLEVEL%
@ECHO ********************************************************************************
@ECHO.
GoTo SALIR

:CSGPI_OK
@ECHO.
@ECHO ********************************************************************************
@ECHO EL SISTEMA FINALIZO SIN PROBLEMAS
@ECHO ERRORLEVEL = %CSGPI_ERRORLEVEL%
@ECHO ********************************************************************************
@ECHO.
GoTo SALIR

:SALIR
@echo.
echo _exit %CSGPI_ERRORLEVEL%
exit %CSGPI_ERRORLEVEL%
