USE [CSGPI]
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_CIRCULAR_1981$Saldos_DAPV_DC]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE PKG_CIRCULAR_1981$Saldos_DAPV_DC
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[PKG_CIRCULAR_1981$Saldos_DAPV_DC]
(@pFecha_Desde    datetime,
 @pFecha_Hasta    datetime,
 @pId_Empresa     numeric,
 @pId_Cuenta      numeric = NULL
)
AS
BEGIN
  -- TRAE TODOS LOS SALDOS PARA COMPONER LA CIRCULAR.
  -- SE AGREGARÁ CODIGO PARA IDENTIFICAR A QUE SECCION CORRESPONDE
  -- 1.- SALDOS DAPV
  -- 2.- SALDOS DC
 SET NOCOUNT ON


  DECLARE @LID_TIPOCUENTA           NUMERIC
        , @LRUT_EMPRESA             VARCHAR(15)
        , @LTOTALM_CUENTASAPV       FLOAT
        , @LTOTALM_PARTICIPES       FLOAT
        , @LTOTALM_APORTE           FLOAT
        , @LTOTALM_RETIRO           FLOAT
        , @LTOTALM_SALDO            FLOAT
        , @LTOTALM_BONIFICACION     FLOAT
        , @LTOTALM_DEPOSITOS        FLOAT
        , @LMONTOM_DEPOSITOS        FLOAT
        , @LTOTALM_RETIROS          FLOAT
        , @LMONTOM_RETIROS          FLOAT
        , @LTOTALM_TRASP_RECIBIDOS  FLOAT
        , @LMONTOM_TRASP_RECIBIDOS  FLOAT
        , @LTOTALM_TRASP_REALIZADOS FLOAT
        , @LMONTOM_TRASP_REALIZADOS FLOAT
        , @LTOTALF_CUENTASAPV       FLOAT
        , @LTOTALF_PARTICIPES       FLOAT
        , @LTOTALF_APORTE           FLOAT
        , @LTOTALF_RETIRO           FLOAT
        , @LTOTALF_SALDO            FLOAT
        , @LTOTALF_BONIFICACION     FLOAT
        , @LTOTALF_DEPOSITOS        FLOAT
        , @LMONTOF_DEPOSITOS        FLOAT
        , @LTOTALF_RETIROS          FLOAT
        , @LMONTOF_RETIROS          FLOAT
        , @LTOTALF_TRASP_RECIBIDOS  FLOAT
        , @LMONTOF_TRASP_RECIBIDOS  FLOAT
        , @LTOTALF_TRASP_REALIZADOS FLOAT
        , @LMONTOF_TRASP_REALIZADOS FLOAT


  DECLARE @SALIDA TABLE (SECCION                 VARCHAR(10)
                        ,RUT_EMPRESA             VARCHAR(12)
                        ,TOTALM_CUENTASAPV       FLOAT
                        ,TOTALM_PARTICIPES       FLOAT
                        ,TOTALM_SALDO            FLOAT
                        ,TOTALM_BONIFICACION     FLOAT
                        ,TOTALM_DEPOSITOS        FLOAT
                        ,MONTOM_DEPOSITOS        FLOAT
                        ,TOTALM_RETIROS          FLOAT
                        ,MONTOM_RETIROS          FLOAT
                        ,TOTALM_TRASP_RECIBIDOS  FLOAT
                        ,MONTOM_TRASP_RECIBIDOS  FLOAT
                        ,TOTALM_TRASP_REALIZADOS FLOAT
                        ,MONTOM_TRASP_REALIZADOS FLOAT
                        ,TOTALF_CUENTASAPV       FLOAT
                        ,TOTALF_PARTICIPES       FLOAT
                        ,TOTALF_SALDO            FLOAT
                        ,TOTALF_BONIFICACION     FLOAT
                        ,TOTALF_DEPOSITOS        FLOAT
                        ,MONTOF_DEPOSITOS        FLOAT
                        ,TOTALF_TRASP_RECIBIDOS  FLOAT
                        ,MONTOF_TRASP_RECIBIDOS  FLOAT
                        ,TOTALF_TRASP_REALIZADOS FLOAT
                        ,MONTOF_TRASP_REALIZADOS FLOAT
                        ,TOTALF_RETIROS          FLOAT
                        ,MONTOF_RETIROS          FLOAT)

  SELECT @LID_TIPOCUENTA = ID_TIPOCUENTA FROM TIPO_CUENTAS WHERE DESCRIPCION_CORTA='APV'
  SET @LRUT_EMPRESA = '96.515.580-5'

---------------------------------------------
-- PARA DEPOSITO APV Y COTIZACION VOLUNTARIA
---------------------------------------------
      SET @LTOTALM_CUENTASAPV = 0
      SET @LTOTALM_PARTICIPES = 0
      SET @LTOTALM_APORTE = 0
      SET @LTOTALM_RETIRO = 0
      SET @LTOTALM_SALDO = 0
      SET @LTOTALM_BONIFICACION = 0
      SET @LTOTALM_DEPOSITOS = 0
      SET @LMONTOM_DEPOSITOS = 0
      SET @LTOTALM_RETIROS = 0
      SET @LMONTOM_RETIROS = 0
      SET @LTOTALM_TRASP_RECIBIDOS = 0
      SET @LMONTOM_TRASP_RECIBIDOS = 0
      SET @LTOTALM_TRASP_REALIZADOS = 0
      SET @LMONTOM_TRASP_REALIZADOS = 0
      SET @LTOTALF_CUENTASAPV = 0
      SET @LTOTALF_PARTICIPES = 0
      SET @LTOTALF_APORTE = 0
      SET @LTOTALF_RETIRO = 0
      SET @LTOTALF_SALDO = 0
      SET @LTOTALF_BONIFICACION = 0
      SET @LTOTALF_DEPOSITOS = 0
      SET @LMONTOF_DEPOSITOS = 0
      SET @LTOTALF_RETIROS = 0
      SET @LMONTOF_RETIROS = 0
      SET @LTOTALF_TRASP_RECIBIDOS = 0
      SET @LMONTOF_TRASP_RECIBIDOS = 0
      SET @LTOTALF_TRASP_REALIZADOS = 0
      SET @LMONTOF_TRASP_REALIZADOS = 0

-- TOTAL CUENTAS MASCULINOS
    SELECT @LTOTALM_CUENTASAPV = COUNT(*)
      FROM VIEW_CUENTAS_VIGENTES V,
           PATRIMONIO_CUENTAS P,
           CLIENTES C
     WHERE ID_EMPRESA=@PID_EMPRESA
       AND ID_TIPOCUENTA=@LID_TIPOCUENTA
       AND TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                            FROM TIPOS_AHORRO_APV
                           WHERE DESCRIPCION_LARGA LIKE 'APV%'
                              OR DESCRIPCION_LARGA LIKE 'COTIZA%')
       AND C.ID_CLIENTE = V.ID_CLIENTE
       AND (C.SEXO = 'M' OR C.SEXO IS NULL)
       AND P.ID_CUENTA = V.ID_CUENTA
       AND P.FECHA_CIERRE=@pfecha_hasta

-- TOTAL CUENTAS FEMENINOS
    SELECT @LTOTALF_CUENTASAPV = COUNT(*)
      FROM VIEW_CUENTAS_VIGENTES V,
           PATRIMONIO_CUENTAS P,
           CLIENTES C
     WHERE ID_EMPRESA=@PID_EMPRESA
       AND ID_TIPOCUENTA=@LID_TIPOCUENTA
       AND TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                            FROM TIPOS_AHORRO_APV
                           WHERE DESCRIPCION_LARGA LIKE 'APV%'
                              OR DESCRIPCION_LARGA LIKE 'COTIZA%')
       AND C.ID_CLIENTE = V.ID_CLIENTE
       AND C.SEXO = 'F'
       AND P.ID_CUENTA = V.ID_CUENTA
       AND P.FECHA_CIERRE=@pfecha_hasta


-- TOTAL PARTICIPES MASCULINOS
    SELECT DISTINCT @LTOTALM_PARTICIPES = COUNT(*)
      FROM CLIENTES
     WHERE (SEXO = 'M' OR SEXO IS NULL)
        AND ID_CLIENTE IN (SELECT ID_CLIENTE
                          FROM VIEW_CUENTAS_VIGENTES V,
                               PATRIMONIO_CUENTAS P
                         WHERE V.ID_EMPRESA=@PID_EMPRESA
                           AND V.ID_TIPOCUENTA=@LID_TIPOCUENTA
                           AND V.TIPO_AHORRO IN (SELECT ID_TIPO_AHORRO
                                                   FROM TIPOS_AHORRO_APV
                                                  WHERE DESCRIPCION_LARGA LIKE 'APV%'
                                                     OR DESCRIPCION_LARGA LIKE 'COTIZA%')
                           AND P.ID_CUENTA = V.ID_CUENTA
                           AND P.FECHA_CIERRE=@pFecha_Hasta
                        )

-- TOTAL PARTICIPES FEMENINOS
    SELECT DISTINCT @LTOTALF_PARTICIPES = COUNT(*)
      FROM CLIENTES
      WHERE SEXO = 'F'
        AND ID_CLIENTE IN (SELECT ID_CLIENTE
                          FROM VIEW_CUENTAS_VIGENTES V,
                               PATRIMONIO_CUENTAS P
                         WHERE V.ID_EMPRESA=@PID_EMPRESA
                           AND V.ID_TIPOCUENTA=@LID_TIPOCUENTA
                           AND V.TIPO_AHORRO IN (SELECT ID_TIPO_AHORRO
                                                   FROM TIPOS_AHORRO_APV
                                                  WHERE DESCRIPCION_LARGA LIKE 'APV%'
                                                     OR DESCRIPCION_LARGA LIKE 'COTIZA%')
                           AND P.ID_CUENTA = V.ID_CUENTA
                           AND P.FECHA_CIERRE=@pFecha_Hasta
                        )

-- TOTAL SALDO MASCULINOS
     SELECT @LTOTALM_SALDO= (ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     P.PATRIMONIO_MON_CUENTA,
                                                                     ID_MONEDA_CUENTA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     P.FECHA_CIERRE)),0)
                            ) / 1000
       FROM PATRIMONIO_CUENTAS P
          , VIEW_CUENTAS_VIGENTES CU
          , CLIENTES C
      WHERE CU.ID_EMPRESA=@PID_EMPRESA
        AND CU.ID_TIPOCUENTA=@LID_TIPOCUENTA
        AND CU.TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                                  FROM TIPOS_AHORRO_APV
                                 WHERE DESCRIPCION_LARGA LIKE 'APV%'
                                    OR DESCRIPCION_LARGA LIKE 'COTIZA%')
        AND C.ID_CLIENTE = CU.ID_CLIENTE
        AND (C.SEXO = 'M' OR C.SEXO IS NULL)
        AND P.ID_CUENTA   = CU.ID_CUENTA
        AND P.FECHA_CIERRE  = @PFECHA_HASTA

-- TOTAL SALDO FEMENINOS
     SELECT @LTOTALF_SALDO= (ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     P.PATRIMONIO_MON_CUENTA,
                                                                     ID_MONEDA_CUENTA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     P.FECHA_CIERRE)),0)
                            ) / 1000
       FROM PATRIMONIO_CUENTAS P
          , VIEW_CUENTAS_VIGENTES CU
          , CLIENTES C
      WHERE CU.ID_EMPRESA=@PID_EMPRESA
        AND CU.ID_TIPOCUENTA=@LID_TIPOCUENTA
        AND CU.TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                                  FROM TIPOS_AHORRO_APV
                                 WHERE DESCRIPCION_LARGA LIKE 'APV%'
                                    OR DESCRIPCION_LARGA LIKE 'COTIZA%')
        AND C.ID_CLIENTE = CU.ID_CLIENTE
        AND C.SEXO = 'F'
        AND P.ID_CUENTA   = CU.ID_CUENTA
        AND P.FECHA_CIERRE  = @PFECHA_HASTA

-- TOTAL BONIFICACION MASCULINOS
     SELECT @LTOTALM_BONIFICACION= (ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     P.PATRIMONIO_MON_CUENTA,
                                                                     ID_MONEDA_CUENTA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     P.FECHA_CIERRE)),0)
                            ) / 1000
       FROM PATRIMONIO_CUENTAS P
          , VIEW_CUENTAS_VIGENTES CU
          , CLIENTES C
      WHERE CU.ID_EMPRESA=@PID_EMPRESA
        AND CU.ID_TIPOCUENTA=@LID_TIPOCUENTA
        AND CU.TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                                  FROM TIPOS_AHORRO_APV
                                 WHERE DESCRIPCION_LARGA LIKE 'BONIF%')
        AND C.ID_CLIENTE = CU.ID_CLIENTE
        AND (C.SEXO = 'M' OR C.SEXO IS NULL)
        AND P.ID_CUENTA   = CU.ID_CUENTA
        AND P.FECHA_CIERRE  = @PFECHA_HASTA

-- TOTAL BONIFICACION FEMENINOS
     SELECT @LTOTALF_BONIFICACION= (ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     P.PATRIMONIO_MON_CUENTA,
                                                                     ID_MONEDA_CUENTA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     P.FECHA_CIERRE)),0)
                            ) / 1000
       FROM PATRIMONIO_CUENTAS P
          , VIEW_CUENTAS_VIGENTES CU
          , CLIENTES C
      WHERE CU.ID_EMPRESA=@PID_EMPRESA
        AND CU.ID_TIPOCUENTA=@LID_TIPOCUENTA
        AND CU.TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                                  FROM TIPOS_AHORRO_APV
                                 WHERE DESCRIPCION_LARGA LIKE 'BONIF%')
        AND C.ID_CLIENTE = CU.ID_CLIENTE
        AND C.SEXO = 'F'
        AND P.ID_CUENTA   = CU.ID_CUENTA
        AND P.FECHA_CIERRE  = @PFECHA_HASTA

-- SALDOS MASCULINOS
     SELECT @LTOTALM_DEPOSITOS =SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'A' THEN 1
                                                        ELSE 0 END)
          , @LMONTOM_DEPOSITOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'A' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          , @LTOTALM_RETIROS = SUM(CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'R' THEN 1
                                                        ELSE 0  END)
          , @LMONTOM_RETIROS = SUM(CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'R' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          , @LTOTALM_TRASP_RECIBIDOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'E' THEN 1
                                                        ELSE 0  END )
          , @LMONTOM_TRASP_RECIBIDOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'E' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          ,@LTOTALM_TRASP_REALIZADOS =  SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'S' THEN 1
                                                        ELSE 0  END)
          ,@LMONTOM_TRASP_REALIZADOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'S' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
        FROM APORTE_RESCATE_CUENTA  AR
           , TIPOS_AHORRO_APV TA
           , VIEW_CUENTAS_VIGENTES CU
           , CLIENTES CL
       WHERE CU.ID_EMPRESA        = @pId_Empresa
         AND CU.ID_TIPOCUENTA     = @LID_TIPOCUENTA
         AND CU.ID_CUENTA         = ISNULL(@pId_Cuenta,AR.ID_CUENTA)
         AND CL.ID_CLIENTE        = CU.ID_CLIENTE
         AND (CL.SEXO = 'M' OR CL.SEXO IS NULL)
         AND AR.ID_CUENTA         = CU.ID_CUENTA
         AND AR.FECHA_MOVIMIENTO >= @pFecha_Desde
         AND AR.FECHA_MOVIMIENTO <= @pFecha_Hasta
         AND AR.ID_TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                                      FROM TIPOS_AHORRO_APV
                                     WHERE DESCRIPCION_LARGA LIKE 'APV%'
                                        OR DESCRIPCION_LARGA LIKE 'COTIZA%')
         AND AR.COD_ESTADO        <> dbo.Pkg_Global$cCod_Estado_Anulado()
         AND TA.ID_TIPO_AHORRO    = AR.ID_TIPO_AHORRO

-- SALDOS FEMENINOS
     SELECT @LTOTALF_DEPOSITOS =SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'A' THEN 1
                                                        ELSE 0 END)
          , @LMONTOF_DEPOSITOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'A' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          , @LTOTALF_RETIROS = SUM(CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'R' THEN 1
                                                        ELSE 0  END)
          , @LMONTOF_RETIROS = SUM(CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'R' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          , @LTOTALF_TRASP_RECIBIDOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'E' THEN 1
                                                        ELSE 0  END )
          , @LMONTOF_TRASP_RECIBIDOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'E' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          ,@LTOTALF_TRASP_REALIZADOS =  SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'S' THEN 1
                                                        ELSE 0  END)
          ,@LMONTOF_TRASP_REALIZADOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'S' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
        FROM APORTE_RESCATE_CUENTA  AR
           , TIPOS_AHORRO_APV TA
           , VIEW_CUENTAS_VIGENTES CU
           , CLIENTES CL
       WHERE CU.ID_EMPRESA        = @pId_Empresa
         AND CU.ID_TIPOCUENTA     = @LID_TIPOCUENTA
         AND CU.ID_CUENTA         = ISNULL(@pId_Cuenta,AR.ID_CUENTA)
         AND CL.ID_CLIENTE        = CU.ID_CLIENTE
         AND CL.SEXO              = 'F'
         AND AR.ID_CUENTA         = CU.ID_CUENTA
         AND AR.FECHA_MOVIMIENTO >= @pFecha_Desde
         AND AR.FECHA_MOVIMIENTO <= @pFecha_Hasta
         AND AR.ID_TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                                      FROM TIPOS_AHORRO_APV
                                     WHERE DESCRIPCION_LARGA LIKE 'APV%'
                                        OR DESCRIPCION_LARGA LIKE 'COTIZA%')
         AND TA.ID_TIPO_AHORRO    = AR.ID_TIPO_AHORRO
         AND AR.COD_ESTADO        <> dbo.Pkg_Global$cCod_Estado_Anulado()

       INSERT INTO @SALIDA
       SELECT 'DAPV'                           AS 'SECCION'
            , @LRUT_EMPRESA                    AS 'RUT_EMPRESA'
            , @LTOTALM_CUENTASAPV              AS 'TOTAL_CUENTASAPV_M'
            , @LTOTALM_PARTICIPES              AS 'TOTAL_PARTICIPES_M'
            , @LTOTALM_SALDO                   AS 'MONTO_SALDO_M'
            , @LTOTALM_BONIFICACION            AS 'MONTO_BONIFICACION_M'
            , @LTOTALM_DEPOSITOS               AS 'Numero_Depositos_M'
            , @LMONTOM_DEPOSITOS               AS 'Monto_Total_Depositos_M'
            , @LTOTALM_RETIROS                 AS 'Numero_Retiros_M'
            , @LMONTOM_RETIROS                 AS 'Monto_Total_Retiros_M'
            , @LTOTALM_TRASP_RECIBIDOS         AS 'Numero_Traspasos_Recibidos_M'
            , @LMONTOM_TRASP_RECIBIDOS         AS 'Monto_Total_Traspasos_Recibidos_M'
            , @LTOTALM_TRASP_REALIZADOS        AS 'Numero_Traspasos_Realizados_M'
            , @LMONTOM_TRASP_REALIZADOS        AS 'Monto_Total_Traspasos_Realizados_M'
            , @LTOTALF_CUENTASAPV              AS 'TOTAL_CUENTASAPV_F'
            , @LTOTALF_PARTICIPES              AS 'TOTAL_PARTICIPES_F'
            , @LTOTALF_SALDO                   AS 'MONTO_SALDO_F'
            , @LTOTALF_BONIFICACION            AS 'MONTO_BONIFICACION_F'
            , @LTOTALF_DEPOSITOS               AS 'Numero_Depositos_F'
            , @LMONTOF_DEPOSITOS               AS 'Monto_Total_Depositos_F'
            , @LTOTALF_TRASP_RECIBIDOS         AS 'Numero_Traspasos_Recibidos_F'
            , @LMONTOF_TRASP_RECIBIDOS         AS 'Monto_Total_Traspasos_Recibidos_F'
            , @LTOTALF_TRASP_REALIZADOS        AS 'Numero_Traspasos_Realizados_F'
            , @LMONTOF_TRASP_REALIZADOS        AS 'Monto_Total_Traspasos_Realizados_F'
            , @LTOTALF_RETIROS                 AS 'Numero_Retiros_F'
            , @LMONTOF_RETIROS                 AS 'Monto_Total_Retiros_F'


---------------------------------------------
-- PARA DEPOSITO CONVENIDO
---------------------------------------------

      SET @LTOTALM_CUENTASAPV = 0
      SET @LTOTALM_PARTICIPES = 0
      SET @LTOTALM_APORTE = 0
      SET @LTOTALM_RETIRO = 0
      SET @LTOTALM_SALDO = 0
      SET @LTOTALM_BONIFICACION = 0
      SET @LTOTALM_DEPOSITOS = 0
      SET @LMONTOM_DEPOSITOS = 0
      SET @LTOTALM_RETIROS = 0
      SET @LMONTOM_RETIROS = 0
      SET @LTOTALM_TRASP_RECIBIDOS = 0
      SET @LMONTOM_TRASP_RECIBIDOS = 0
      SET @LTOTALM_TRASP_REALIZADOS = 0
      SET @LMONTOM_TRASP_REALIZADOS = 0
      SET @LTOTALF_CUENTASAPV = 0
      SET @LTOTALF_PARTICIPES = 0
      SET @LTOTALF_APORTE = 0
      SET @LTOTALF_RETIRO = 0
      SET @LTOTALF_SALDO = 0
      SET @LTOTALF_BONIFICACION = 0
      SET @LTOTALF_DEPOSITOS = 0
      SET @LMONTOF_DEPOSITOS = 0
      SET @LTOTALF_RETIROS = 0
      SET @LMONTOF_RETIROS = 0
      SET @LTOTALF_TRASP_RECIBIDOS = 0
      SET @LMONTOF_TRASP_RECIBIDOS = 0
      SET @LTOTALF_TRASP_REALIZADOS = 0
      SET @LMONTOF_TRASP_REALIZADOS = 0

-- TOTAL CUENTAS MASCULINOS
    SELECT @LTOTALM_CUENTASAPV = COUNT(*)
      FROM VIEW_CUENTAS_VIGENTES V,
           PATRIMONIO_CUENTAS P,
           CLIENTES C
     WHERE ID_EMPRESA=@PID_EMPRESA
       AND ID_TIPOCUENTA=@LID_TIPOCUENTA
       AND TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                            FROM TIPOS_AHORRO_APV
                           WHERE DESCRIPCION_LARGA LIKE 'DEP%')
       AND C.ID_CLIENTE = V.ID_CLIENTE
       AND (C.SEXO = 'M' OR C.SEXO IS NULL)
       AND P.ID_CUENTA = V.ID_CUENTA
       AND P.FECHA_CIERRE=@pfecha_hasta

-- TOTAL CUENTAS FEMENINOS
    SELECT @LTOTALF_CUENTASAPV = COUNT(*)
      FROM VIEW_CUENTAS_VIGENTES V,
           PATRIMONIO_CUENTAS P,
           CLIENTES C
     WHERE ID_EMPRESA=@PID_EMPRESA
       AND ID_TIPOCUENTA=@LID_TIPOCUENTA
       AND TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                            FROM TIPOS_AHORRO_APV
                           WHERE DESCRIPCION_LARGA LIKE 'DEP%')
       AND C.ID_CLIENTE = V.ID_CLIENTE
       AND C.SEXO = 'F'
       AND P.ID_CUENTA = V.ID_CUENTA
       AND P.FECHA_CIERRE=@pfecha_hasta


-- TOTAL PARTICIPES MASCULINOS
    SELECT DISTINCT @LTOTALM_PARTICIPES = COUNT(*)
      FROM CLIENTES
     WHERE (SEXO = 'M' OR SEXO IS NULL)
        AND ID_CLIENTE IN (SELECT ID_CLIENTE
                          FROM VIEW_CUENTAS_VIGENTES V,
                               PATRIMONIO_CUENTAS P
                         WHERE V.ID_EMPRESA=@PID_EMPRESA
                           AND V.ID_TIPOCUENTA=@LID_TIPOCUENTA
                           AND V.TIPO_AHORRO IN (SELECT ID_TIPO_AHORRO
                                                   FROM TIPOS_AHORRO_APV
                                                  WHERE DESCRIPCION_LARGA LIKE 'DEP%')
                           AND P.ID_CUENTA = V.ID_CUENTA
                           AND P.FECHA_CIERRE=@pFecha_Hasta
                        )

-- TOTAL PARTICIPES FEMENINOS
    SELECT DISTINCT @LTOTALF_PARTICIPES = COUNT(*)
      FROM CLIENTES
      WHERE SEXO = 'F'
        AND ID_CLIENTE IN (SELECT ID_CLIENTE
                          FROM VIEW_CUENTAS_VIGENTES V,
                               PATRIMONIO_CUENTAS P
                         WHERE V.ID_EMPRESA=@PID_EMPRESA
                           AND V.ID_TIPOCUENTA=@LID_TIPOCUENTA
                           AND V.TIPO_AHORRO IN (SELECT ID_TIPO_AHORRO
                                                   FROM TIPOS_AHORRO_APV
                                                  WHERE DESCRIPCION_LARGA LIKE 'DEP%')
                           AND P.ID_CUENTA = V.ID_CUENTA
                           AND P.FECHA_CIERRE=@pFecha_Hasta
                        )

-- TOTAL SALDO MASCULINOS
     SELECT @LTOTALM_SALDO= (ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     P.PATRIMONIO_MON_CUENTA,
                                                                     ID_MONEDA_CUENTA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     P.FECHA_CIERRE)),0)
                            ) / 1000
       FROM PATRIMONIO_CUENTAS P
          , VIEW_CUENTAS_VIGENTES CU
          , CLIENTES C
      WHERE CU.ID_EMPRESA=@PID_EMPRESA
        AND CU.ID_TIPOCUENTA=@LID_TIPOCUENTA
        AND CU.TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                                  FROM TIPOS_AHORRO_APV
                                 WHERE DESCRIPCION_LARGA LIKE 'DEP%')
        AND C.ID_CLIENTE = CU.ID_CLIENTE
        AND (C.SEXO = 'M' OR C.SEXO IS NULL)
        AND P.ID_CUENTA   = CU.ID_CUENTA
        AND P.FECHA_CIERRE  = @PFECHA_HASTA

-- TOTAL SALDO FEMENINOS
     SELECT @LTOTALF_SALDO= (ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     P.PATRIMONIO_MON_CUENTA,
                                                                     ID_MONEDA_CUENTA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     P.FECHA_CIERRE)),0)
                            ) / 1000
       FROM PATRIMONIO_CUENTAS P
          , VIEW_CUENTAS_VIGENTES CU
          , CLIENTES C
      WHERE CU.ID_EMPRESA=@PID_EMPRESA
        AND CU.ID_TIPOCUENTA=@LID_TIPOCUENTA
        AND CU.TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                                  FROM TIPOS_AHORRO_APV
                                 WHERE DESCRIPCION_LARGA LIKE 'DEP%')
        AND C.ID_CLIENTE = CU.ID_CLIENTE
        AND C.SEXO = 'F'
        AND P.ID_CUENTA   = CU.ID_CUENTA
        AND P.FECHA_CIERRE  = @PFECHA_HASTA

-- SALDOS MASCULINOS
     SELECT @LTOTALM_DEPOSITOS =SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'A' THEN 1
                                                        ELSE 0 END)
          , @LMONTOM_DEPOSITOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'A' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          , @LTOTALM_RETIROS = SUM(CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'R' THEN 1
                                                        ELSE 0  END)
          , @LMONTOM_RETIROS = SUM(CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'R' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          , @LTOTALM_TRASP_RECIBIDOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'E' THEN 1
                                                        ELSE 0  END )
          , @LMONTOM_TRASP_RECIBIDOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'E' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          ,@LTOTALM_TRASP_REALIZADOS =  SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'S' THEN 1
                                                        ELSE 0  END)
          ,@LMONTOM_TRASP_REALIZADOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'S' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
        FROM APORTE_RESCATE_CUENTA  AR
           , TIPOS_AHORRO_APV TA
           , VIEW_CUENTAS_VIGENTES CU
           , CLIENTES CL
       WHERE CU.ID_EMPRESA        = @pId_Empresa
         AND CU.ID_TIPOCUENTA     = @LID_TIPOCUENTA
         AND CU.ID_CUENTA         = ISNULL(@pId_Cuenta,AR.ID_CUENTA)
         AND CL.ID_CLIENTE        = CU.ID_CLIENTE
         AND (CL.SEXO = 'M' OR CL.SEXO IS NULL)
         AND AR.ID_CUENTA         = CU.ID_CUENTA
         AND AR.FECHA_MOVIMIENTO >= @pFecha_Desde
         AND AR.FECHA_MOVIMIENTO <= @pFecha_Hasta
         AND AR.ID_TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                                      FROM TIPOS_AHORRO_APV
                                     WHERE DESCRIPCION_LARGA LIKE 'DEP%')
         AND AR.COD_ESTADO        <> dbo.Pkg_Global$cCod_Estado_Anulado()
         AND TA.ID_TIPO_AHORRO    = AR.ID_TIPO_AHORRO

-- SALDOS FEMENINOS
     SELECT @LTOTALF_DEPOSITOS =SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'A' THEN 1
                                                        ELSE 0 END)
          , @LMONTOF_DEPOSITOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'A' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          , @LTOTALF_RETIROS = SUM(CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'R' THEN 1
                                                        ELSE 0  END)
          , @LMONTOF_RETIROS = SUM(CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'R' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          , @LTOTALF_TRASP_RECIBIDOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'E' THEN 1
                                                        ELSE 0  END )
          , @LMONTOF_TRASP_RECIBIDOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'E' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
          ,@LTOTALF_TRASP_REALIZADOS =  SUM (CASE AR.FLG_TIPO_MOVIMIENTO  WHEN 'S' THEN 1
                                                        ELSE 0  END)
          ,@LMONTOF_TRASP_REALIZADOS = SUM (CASE AR.FLG_TIPO_MOVIMIENTO
                WHEN 'S' THEN DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO)
                         ELSE 0  END) / 1000
        FROM APORTE_RESCATE_CUENTA  AR
           , TIPOS_AHORRO_APV TA
           , VIEW_CUENTAS_VIGENTES CU
           , CLIENTES CL
       WHERE CU.ID_EMPRESA        = @pId_Empresa
         AND CU.ID_TIPOCUENTA     = @LID_TIPOCUENTA
         AND CU.ID_CUENTA         = ISNULL(@pId_Cuenta,AR.ID_CUENTA)
         AND CL.ID_CLIENTE        = CU.ID_CLIENTE
         AND CL.SEXO              = 'F'
         AND AR.ID_CUENTA         = CU.ID_CUENTA
         AND AR.FECHA_MOVIMIENTO >= @pFecha_Desde
         AND AR.FECHA_MOVIMIENTO <= @pFecha_Hasta
         AND AR.ID_TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                                      FROM TIPOS_AHORRO_APV
                                     WHERE DESCRIPCION_LARGA LIKE 'DEP%')
         AND TA.ID_TIPO_AHORRO    = AR.ID_TIPO_AHORRO
         AND AR.COD_ESTADO        <> dbo.Pkg_Global$cCod_Estado_Anulado()

       INSERT INTO @SALIDA
       SELECT 'DC'
            , @LRUT_EMPRESA
            , ISNULL(@LTOTALM_CUENTASAPV,0)
            , ISNULL(@LTOTALM_PARTICIPES,0)
            , ISNULL(@LTOTALM_SALDO,0)
            , ISNULL(@LTOTALM_BONIFICACION,0)
            , ISNULL(@LTOTALM_DEPOSITOS,0)
            , ISNULL(@LMONTOM_DEPOSITOS,0)
            , ISNULL(@LTOTALM_RETIROS,0)
            , ISNULL(@LMONTOM_RETIROS,0)
            , ISNULL(@LTOTALM_TRASP_RECIBIDOS,0)
            , ISNULL(@LMONTOM_TRASP_RECIBIDOS,0)
            , ISNULL(@LTOTALM_TRASP_REALIZADOS,0)
            , ISNULL(@LMONTOM_TRASP_REALIZADOS,0)
            , ISNULL(@LTOTALF_CUENTASAPV,0)
            , ISNULL(@LTOTALF_PARTICIPES,0)
            , ISNULL(@LTOTALF_SALDO,0)
            , ISNULL(@LTOTALF_BONIFICACION,0)
            , ISNULL(@LTOTALF_DEPOSITOS,0)
            , ISNULL(@LMONTOF_DEPOSITOS,0)
            , ISNULL(@LTOTALF_TRASP_RECIBIDOS,0)
            , ISNULL(@LMONTOF_TRASP_RECIBIDOS,0)
            , ISNULL(@LTOTALF_TRASP_REALIZADOS,0)
            , ISNULL(@LMONTOF_TRASP_REALIZADOS,0)
            , ISNULL(@LTOTALF_RETIROS,0)
            , ISNULL(@LMONTOF_RETIROS,0)

---------------------------------------------
-- NUEVAS POLIZAS
---------------------------------------------

DECLARE @SALIDA2 TABLE ( SECCION               VARCHAR(10)
                        ,RUT_EMPRESA           VARCHAR(12)
                        ,POLIZA                VARCHAR(10)
                        ,SERIE                 VARCHAR(100)
						,CUENTASAPV_LA_M       FLOAT
						,CUENTASAPV_LA_F       FLOAT
						,CUENTASAPV_TOTAL_M    FLOAT
						,CUENTASAPV_TOTAL_F    FLOAT
						,PARTICIPES_LA_M       FLOAT
						,PARTICIPES_LA_F       FLOAT
						,PARTICIPES_TOTAL_M    FLOAT
						,PARTICIPES_TOTAL_F    FLOAT
						,SALDOTOTACUM_LA_M     FLOAT
						,SALDOTOTACUM_LA_F     FLOAT
						,SALDOTOTACUM_LB_M     FLOAT
						,SALDOTOTACUM_LB_F     FLOAT
						,TOTAL_BONIFICACION_M  FLOAT
						,TOTAL_BONIFICACION_F  FLOAT
						,NUMDEPOSITOS_LA_M     FLOAT
						,NUMDEPOSITOS_LA_F     FLOAT
						,NUMDEPOSITOS_LB_M     FLOAT
						,NUMDEPOSITOS_LB_F     FLOAT
						,MTODEPOSITOS_LA_M     FLOAT
						,MTODEPOSITOS_LA_F     FLOAT
						,MTODEPOSITOS_LB_M     FLOAT
						,MTODEPOSITOS_LB_F	   FLOAT
						,NUMTRASP_REC_M	       FLOAT
						,NUMTRASP_REC_F        FLOAT
						,MTOTRASP_REC_M        FLOAT
						,MTOTRASP_REC_F        FLOAT
						,NUMTRASP_REA_M	       FLOAT
						,NUMTRASP_REA_F	       FLOAT
						,MTOTRASP_REA_M	       FLOAT
						,MTOTRASP_REA_F        FLOAT
						,NUMRETIROS_LA_M       FLOAT
						,NUMRETIROS_LA_F       FLOAT
						,NUMRETIROS_LB_M	   FLOAT
						,NUMRETIROS_LB_F	   FLOAT
						,MTORETIROS_LA_M	   FLOAT
						,MTORETIROS_LA_F	   FLOAT
						,MTORETIROS_LB_M	   FLOAT
						,MTORETIROS_LB_F	   FLOAT)
/*
INSERT INTO @SALIDA2
SELECT DISTINCT 'DAPV' 'SECCION', @LRUT_EMPRESA 'RUT_EMPRESA'
                ,C.POLIZA, C.SERIE
--<NumeroCuentasApvVigentes>
,ISNULL((SELECT COUNT(DISTINCT APV_CONTRATO)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS CUENTASAPV_LA_M
,ISNULL((SELECT COUNT(DISTINCT APV_CONTRATO)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS CUENTASAPV_LA_F
,ISNULL((SELECT COUNT(DISTINCT APV_CONTRATO)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS CUENTASAPV_TOTAL_M
,ISNULL((SELECT COUNT(DISTINCT APV_CONTRATO)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS CUENTASAPV_TOTAL_F
--<NumeroAseguradosParticipesAportantesClientesPlan>
,ISNULL((SELECT COUNT(DISTINCT APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS PARTICIPES_LA_M
,ISNULL((SELECT COUNT(DISTINCT APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS PARTICIPES_LA_F
,ISNULL((SELECT COUNT(DISTINCT APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS PARTICIPES_TOTAL_M
,ISNULL((SELECT COUNT(DISTINCT APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS PARTICIPES_TOTAL_F
--<SaldoTotalAcumulado>
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS SALDOTOTACUM_LA_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS SALDOTOTACUM_LA_F
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'B'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS SALDOTOTACUM_LB_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'B'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS SALDOTOTACUM_LB_F
--<Bonificacion>
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO IN ('DAPB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS TOTAL_BONIFICACION_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO IN ('DAPB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS TOTAL_BONIFICACION_F
--<NumeroDepositos>
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'M'
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS NUMDEPOSITOS_LA_M
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'F'
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS NUMDEPOSITOS_LA_F
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'B'
	 AND   APV_SEXO = 'M'
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS NUMDEPOSITOS_LB_M
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'B'
	 AND   APV_SEXO = 'F'
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS NUMDEPOSITOS_LB_F
--<MontoTotalDepositos>
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'M'
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS MTODEPOSITOS_LA_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'F'
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS MTODEPOSITOS_LA_F
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'B'
	 AND   APV_SEXO = 'M'
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS MTODEPOSITOS_LB_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_REGIMEN = 'B'
	 AND   APV_SEXO = 'F'
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS MTODEPOSITOS_LB_F
--<NumeroTraspasosRecibidos>
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TI'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS NUMTRASP_REC_M
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TI'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS NUMTRASP_REC_F
--<MontoTotalTraspasosRecibidos>
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TI'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS MTOTRASP_REC_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TI'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS MTOTRASP_REC_F
--<NumeroTraspasosRealizados>
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TO'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS NUMTRASP_REA_M
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TO'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS NUMTRASP_REA_F
--<MontoTotalTraspasosRealizados>
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TO'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS MTOTRASP_REA_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TO'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS MTOTRASP_REA_F
--<NumeroRetiros>
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'ST'
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS NUMRETIROS_LA_M
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'ST'
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS NUMRETIROS_LA_F
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'ST'
	 AND   APV_REGIMEN = 'B'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS NUMRETIROS_LB_M
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'ST'
	 AND   APV_REGIMEN = 'B'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS NUMRETIROS_LB_F
--<MontoTotalRetiros>
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'ST'
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS MTORETIROS_LA_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'ST'
	 AND   APV_REGIMEN = 'A'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS MTORETIROS_LA_F
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'ST'
	 AND   APV_REGIMEN = 'B'
	 AND   APV_SEXO = 'M'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS MTORETIROS_LB_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'ST'
	 AND   APV_REGIMEN = 'B'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS MTORETIROS_LB_F
FROM VIEW_CIRCULAR_1981_CODPLANES C
UNION
SELECT DISTINCT 'DC' 'SECCION', @LRUT_EMPRESA 'RUT_EMPRESA'
                ,C.POLIZA, C.SERIE
--<NumeroCuentasApvVigentes>
, 0 AS CUENTASAPV_LA_M
, 0 AS CUENTASAPV_LA_F
,ISNULL((SELECT COUNT(DISTINCT APV_CONTRATO)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO IN ('M','N')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS CUENTASAPV_TOTAL_M
,ISNULL((SELECT COUNT(DISTINCT APV_CONTRATO)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS CUENTASAPV_TOTAL_F
--<NumeroAseguradosParticipesAportantesClientesPlan>
, 0 AS PARTICIPES_LA_M
, 0 AS PARTICIPES_LA_F
,ISNULL((SELECT COUNT(DISTINCT APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO IN ('M','N')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS PARTICIPES_TOTAL_M
,ISNULL((SELECT COUNT(DISTINCT APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS PARTICIPES_TOTAL_F
--<SaldoTotalAcumulado>
, 0 AS SALDOTOTACUM_LA_M
, 0 AS SALDOTOTACUM_LA_F
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO IN ('M','N')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS SALDOTOTACUM_LB_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'S'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS SALDOTOTACUM_LB_F
--<Bonificacion>
, 0 AS TOTAL_BONIFICACION_M
, 0 AS TOTAL_BONIFICACION_F
--<NumeroDepositos>
, 0 AS NUMDEPOSITOS_LA_M
, 0 AS NUMDEPOSITOS_LA_F
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_SEXO IN ('M','N')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS NUMDEPOSITOS_LB_M
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS NUMDEPOSITOS_LB_F
--<MontoTotalDepositos>
, 0 AS MTODEPOSITOS_LA_M
, 0 AS MTODEPOSITOS_LA_F
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_SEXO IN ('M','N')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS MTODEPOSITOS_LB_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO IN ('CJ','CO','DR','PA','PR')
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA)) ,0) AS MTODEPOSITOS_LB_F
--<NumeroTraspasosRecibidos>
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TI'
	 AND   APV_SEXO IN ('M','N')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS NUMTRASP_REC_M
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TI'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS NUMTRASP_REC_F
--<MontoTotalTraspasosRecibidos>
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TI'
	 AND   APV_SEXO IN ('M','N')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS MTOTRASP_REC_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'A'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TI'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS MTOTRASP_REC_F
--<NumeroTraspasosRealizados>
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TO'
	 AND   APV_SEXO IN ('M','N')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS NUMTRASP_REA_M
,ISNULL((SELECT COUNT(APV_RUT)
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TO'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS NUMTRASP_REA_F
--<MontoTotalTraspasosRealizados>
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TO'
	 AND   APV_SEXO IN ('M','N')
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS MTOTRASP_REA_M
,ISNULL((SELECT ISNULL(SUM(APV_VALOR),0) / 1000
	 FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
	 WHERE APV_TIPO_REGISTRO = 'R'
	 AND   APV_TIPO = 'DPCV'
	 AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
	 AND   APV_TIPO_MOVIMIENTO = 'TO'
	 AND   APV_SEXO = 'F'
	 AND   APV_PLAN IN (SELECT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2 WHERE C2.POLIZA=C.POLIZA AND C2.FLG_TRASP ='S')) ,0) AS MTOTRASP_REA_F
--<NumeroRetiros>
, 0 AS NUMRETIROS_LA_M
, 0 AS NUMRETIROS_LA_F
, 0 AS NUMRETIROS_LB_M
, 0 AS NUMRETIROS_LB_F
--<MontoTotalRetiros>
, 0 AS MTORETIROS_LA_M
, 0 AS MTORETIROS_LA_F
, 0 AS MTORETIROS_LB_M
, 0 AS MTORETIROS_LB_F
FROM VIEW_CIRCULAR_1981_CODPLANES C
ORDER BY SECCION, C.POLIZA
*/


IF (SELECT VALOR FROM SIS_PARAMETROS WHERE CODIGO = 'ULLA_CIRCULAR1981' AND ESTADO = 'VIG') = 1 
BEGIN
insert into @SALIDA2 values ('DAPV', '96.515.580-5' ,2002080101, 'FONDOS GLOBALES APV'   ,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0  )
insert into @SALIDA2 values ('DC', '96.515.580-5' ,2002080101, 'FONDOS GLOBALES APV'	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	)
insert into @SALIDA2 values ('DAPV', '96.515.580-5' ,2002080102, 'FONDOS GLOBALES APV LARGO PLAZO'	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	)
insert into @SALIDA2 values ('DC', '96.515.580-5' ,2002080102, 'FONDOS GLOBALES APV LARGO PLAZO'	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	)
insert into @SALIDA2 values ('DAPV', '96.515.580-5' ,2004051203, 'PREMIUM GLOBAL PENSION 4'	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	)
insert into @SALIDA2 values ('DC', '96.515.580-5' ,2004051203, 'PREMIUM GLOBAL PENSION 4'	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	)
insert into @SALIDA2 values ('DAPV', '96.515.580-5' ,2005012304, 'MULTIFUND APV'	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	)
insert into @SALIDA2 values ('DC', '96.515.580-5' ,2005012304, 'MULTIFUND APV'	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	,0	)
END
--------------------------------------------------------
--SALIDA
--------------------------------------------------------
  --SET NOCOUNT OFF

        SELECT SECCION     AS 'SECCION'
			, RUT_EMPRESA  AS 'RUT_EMPRESA'
			, '2009100101' AS 'POLIZA'
			, 'VALORES SECURITY ADMINISTRACION TOTAL' AS 'SERIE'
			, 0                                 AS 'CUENTASAPV_LA_M'
			, 0                                 AS 'CUENTASAPV_LA_F'
			, ISNULL(TOTALM_CUENTASAPV,0)		AS 'TOTALM_CUENTASAPV'
			, ISNULL(TOTALF_CUENTASAPV,0)		AS 'TOTALF_CUENTASAPV'

			, 0                                 AS 'PARTICIPES_LA_M'
			, 0                                 AS 'PARTICIPES_LA_F'
			, ISNULL(TOTALM_PARTICIPES,0)		AS 'TOTALM_PARTICIPES'
			, ISNULL(TOTALF_PARTICIPES,0)		AS 'TOTALF_PARTICIPES'

			, 0                                 AS 'SALDOTOTACUM_LA_M'
			, 0                                 AS 'SALDOTOTACUM_LA_F'
			, ISNULL(TOTALM_SALDO,0)			AS 'TOTALM_SALDO'
			, ISNULL(TOTALF_SALDO,0)			AS 'TOTALF_SALDO'

			, ISNULL(TOTALM_BONIFICACION,0)		AS 'TOTALM_BONIFICACION'
			, ISNULL(TOTALF_BONIFICACION,0)		AS 'TOTALF_BONIFICACION'

			, 0                                 AS 'NUMDEPOSITOS_LA_M'
			, 0                                 AS 'NUMDEPOSITOS_LA_F'
			, ISNULL(TOTALM_DEPOSITOS,0)		AS 'TOTALM_DEPOSITOS'
			, ISNULL(TOTALF_DEPOSITOS,0)		AS 'TOTALF_DEPOSITOS'

			, 0                                 AS 'MTODEPOSITOS_LA_M'
			, 0                                 AS 'MTODEPOSITOS_LA_F'
			, ISNULL(MONTOM_DEPOSITOS,0)		AS 'MONTOM_DEPOSITOS'
			, ISNULL(MONTOF_DEPOSITOS,0)		AS 'MONTOF_DEPOSITOS'

			, ISNULL(TOTALM_TRASP_RECIBIDOS,0)	AS 'TOTALM_TRASP_RECIBIDOS'
			, ISNULL(TOTALF_TRASP_RECIBIDOS,0)	AS 'TOTALF_TRASP_RECIBIDOS'
			, ISNULL(MONTOM_TRASP_RECIBIDOS,0)	AS 'MONTOM_TRASP_RECIBIDOS'
			, ISNULL(MONTOF_TRASP_RECIBIDOS,0)	AS 'MONTOF_TRASP_RECIBIDOS'

			, ISNULL(TOTALM_TRASP_REALIZADOS,0)	AS 'TOTALM_TRASP_REALIZADOS'
			, ISNULL(TOTALF_TRASP_REALIZADOS,0)	AS 'TOTALF_TRASP_REALIZADOS'
			, ISNULL(MONTOM_TRASP_REALIZADOS,0)	AS 'MONTOM_TRASP_REALIZADOS'
			, ISNULL(MONTOF_TRASP_REALIZADOS,0)	AS 'MONTOF_TRASP_REALIZADOS'

			, 0                                 AS 'NUMRETIROS_LA_M'
			, 0                                 AS 'NUMRETIROS_LA_F'
			, ISNULL(TOTALM_RETIROS,0)			AS 'TOTALM_RETIROS'
			, ISNULL(TOTALF_RETIROS,0)			AS 'TOTALF_RETIROS'

			, 0                                 AS 'MTORETIROS_LA_M'
			, 0                                 AS 'MTORETIROS_LA_F'
			, ISNULL(MONTOM_RETIROS,0)			AS 'MONTOM_RETIROS'
			, ISNULL(MONTOF_RETIROS,0)			AS 'MONTOF_RETIROS'
			, 1 AS 'ORDEN'
          FROM @SALIDA
          UNION
		  SELECT
			 SECCION     AS 'SECCION'
			,RUT_EMPRESA AS 'RUT_EMPRESA'
			,POLIZA      AS 'POLIZA'
			,SERIE       AS 'SERIE'
			, ISNULL(CUENTASAPV_LA_M,0) AS 'CUENTASAPV_LA_M'
			, ISNULL(CUENTASAPV_LA_F,0) AS 'CUENTASAPV_LA_F'
			, ISNULL(CUENTASAPV_TOTAL_M,0) AS 'TOTALM_CUENTASAPV'
			, ISNULL(CUENTASAPV_TOTAL_F,0) AS 'TOTALF_CUENTASAPV'

			, ISNULL(PARTICIPES_LA_M,0) AS 'PARTICIPES_LA_M'
			, ISNULL(PARTICIPES_LA_F,0) AS 'PARTICIPES_LA_F'
			, ISNULL(PARTICIPES_TOTAL_M,0) AS 'TOTALM_PARTICIPES'
			, ISNULL(PARTICIPES_TOTAL_F,0) AS 'TOTALF_PARTICIPES'

			, ISNULL(SALDOTOTACUM_LA_M,0) AS 'SALDOTOTACUM_LA_M'
			, ISNULL(SALDOTOTACUM_LA_F,0) AS 'SALDOTOTACUM_LA_F'
			, ISNULL(SALDOTOTACUM_LB_M,0) AS 'TOTALM_SALDO'
			, ISNULL(SALDOTOTACUM_LB_F,0) AS 'TOTALF_SALDO'

			, ISNULL(TOTAL_BONIFICACION_M,0) AS 'TOTALM_BONIFICACION'
			, ISNULL(TOTAL_BONIFICACION_F,0) AS 'TOTALF_BONIFICACION'

			, ISNULL(NUMDEPOSITOS_LA_M,0) AS 'NUMDEPOSITOS_LA_M'
			, ISNULL(NUMDEPOSITOS_LA_F,0) AS 'NUMDEPOSITOS_LA_F'
			, ISNULL(NUMDEPOSITOS_LB_M,0) AS 'TOTALM_DEPOSITOS'
			, ISNULL(NUMDEPOSITOS_LB_F,0) AS 'TOTALF_DEPOSITOS'

			, ISNULL(MTODEPOSITOS_LA_M,0) AS 'MTODEPOSITOS_LA_M'
			, ISNULL(MTODEPOSITOS_LA_F,0) AS 'MTODEPOSITOS_LA_F'
			, ISNULL(MTODEPOSITOS_LB_M,0) AS 'MONTOM_DEPOSITOS'
			, ISNULL(MTODEPOSITOS_LB_F,0) AS 'MONTOF_DEPOSITOS'

			, ISNULL(NUMTRASP_REC_M,0) AS 'TOTALM_TRASP_RECIBIDOS'
			, ISNULL(NUMTRASP_REC_F,0) AS 'TOTALF_TRASP_RECIBIDOS'
			, ISNULL(MTOTRASP_REC_M,0) AS 'MONTOM_TRASP_RECIBIDOS'
			, ISNULL(MTOTRASP_REC_F,0) AS 'MONTOF_TRASP_RECIBIDOS'

			, ISNULL(NUMTRASP_REA_M,0) AS 'TOTALM_TRASP_REALIZADOS'
			, ISNULL(NUMTRASP_REA_F,0) AS 'TOTALF_TRASP_REALIZADOS'
			, ISNULL(MTOTRASP_REA_M,0) AS 'MONTOM_TRASP_REALIZADOS'
			, ISNULL(MTOTRASP_REA_F,0) AS 'MONTOF_TRASP_REALIZADOS'

			, ISNULL(NUMRETIROS_LA_M,0) AS 'NUMRETIROS_LA_M'
			, ISNULL(NUMRETIROS_LA_F,0) AS 'NUMRETIROS_LA_F'
			, ISNULL(NUMRETIROS_LB_M,0) AS 'TOTALM_RETIROS'
			, ISNULL(NUMRETIROS_LB_F,0) AS 'TOTALF_RETIROS'

			, ISNULL(MTORETIROS_LA_M,0) AS 'MTORETIROS_LA_M'
			, ISNULL(MTORETIROS_LA_F,0) AS 'MTORETIROS_LA_F'
			, ISNULL(MTORETIROS_LB_M,0) AS 'MONTOM_RETIROS'
			, ISNULL(MTORETIROS_LB_F,0) AS 'MONTOF_RETIROS'
			, 2 AS 'ORDEN'
FROM @SALIDA2
ORDER BY SECCION, ORDEN, POLIZA

END
GO
GRANT EXECUTE ON PKG_CIRCULAR_1981$Saldos_DAPV_DC TO DB_EXECUTESP
GO
