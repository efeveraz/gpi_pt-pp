IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[PKG_SALDOS_ACTIVOS$RESUMEN_X_HOJA_ARBOL_CLASE_INST_CDS]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS$RESUMEN_X_HOJA_ARBOL_CLASE_INST_CDS]
GO

CREATE PROCEDURE  [dbo].[PKG_SALDOS_ACTIVOS$RESUMEN_X_HOJA_ARBOL_CLASE_INST_CDS]
  @PID_CUENTA    NUMERIC = null,
  @PFECHA_CIERRE   DATETIME,
  @PID_ARBOL_CLASE   NUMERIC,
  @pId_Moneda_Salida  NUMERIC = null,
  @pid_Mercado_Transaccion NUMERIC = null,
  @pid_Moneda    NUMERIC = null,
  @pid_sector    NUMERIC = null,
  @pid_Nemotecnico   NUMERIC = null
AS
BEGIN
 set nocount on

    declare @LTOTAL_INT NUMERIC(28,8)
    declare @LID_EMPRESA NUMERIC,
            @LID_MONEDA_USD  NUMERIC

         declare @salida table(
          id_cuenta                         numeric,
          id_saldo_activo                   numeric,
          fecha_cierre                      datetime,
          id_nemotecnico                    numeric,
          nemotecnico                       varchar(50),
          EMISOR                            varchar(100),
          cod_emisor                        varchar(10),
          dsc_nemotecnico                   varchar(120),
          tasa_emision_2                    numeric(18,4),
          cantidad                          numeric(18,4),
          precio                            numeric(18,6),
          tasa_emision                      numeric(18,4),
          FECHA_VENCIMIENTO                 datetime,
          precio_compra                     numeric(18,4),
          tasa                              numeric(18,4),
          tasa_compra                       numeric(18,4),
          monto_valor_compra                numeric(18,4),
          monto_mon_cta                     numeric(18,4),
          id_moneda_cta                     numeric,
          id_moneda_nemotecnico             numeric,
          SIMBOLO_MONEDA                    varchar(3),
          monto_mon_nemotecnico             numeric(18,4),
          monto_mon_origen                  numeric(18,4),
          id_empresa                        numeric,
          id_arbol_clase_inst               numeric,
          cod_instrumento                   varchar(15),
          dsc_arbol_clase_inst              varchar(100),
          porcentaje_rama                   numeric(18,4),
          precio_promedio_compra            numeric(18,4),
          dsc_padre_arbol_clase_inst        varchar(100),
          rentabilidad                      float,
          dias                              numeric,
          duration                          numeric,
          cod_producto                      varchar(10),
          id_padre_arbol_clase_inst         numeric,
          duracion                          float,
          dsc_clasificador_riesgo           varchar(50),
          Fecha_Compra                      datetime,
          CORTE_MINIMO_PAPEL                numeric(10),
          NroCupones                        int,
          Origen_int                        varchar(3),
          total_hoja                        numeric(18,4),
		  CodIsin							varchar(50)
         )

 SELECT @LID_EMPRESA = ID_EMPRESA,
        @pId_Moneda_Salida = isnull(@pId_Moneda_Salida, ID_MONEDA)
   FROM CUENTAS
  WHERE ID_CUENTA  = @PID_CUENTA

 INSERT INTO @salida
           ( id_cuenta
           , id_saldo_activo
           , fecha_cierre
           , id_nemotecnico
           , nemotecnico
           , EMISOR
           , cod_emisor
           , dsc_nemotecnico
           , tasa_emision_2
           , cantidad
           , precio
           , tasa_emision
           , FECHA_VENCIMIENTO
           , precio_compra
           , tasa
           , tasa_compra
           , monto_valor_compra
           , monto_mon_cta
           , id_moneda_cta
           , id_moneda_nemotecnico
           , SIMBOLO_MONEDA
           , monto_mon_nemotecnico
           , monto_mon_origen
           , id_empresa
           , id_arbol_clase_inst
           , cod_instrumento
           , dsc_arbol_clase_inst
           , porcentaje_rama
           , precio_promedio_compra
           , dsc_padre_arbol_clase_inst
           , rentabilidad
           , dias
           , duration
           , cod_producto
           , id_padre_arbol_clase_inst
           , duracion
           , dsc_clasificador_riesgo
           , Fecha_Compra
           , CORTE_MINIMO_PAPEL
           , NroCupones
		   , CodIsin)
 SELECT
        sa.id_cuenta,
        sa.id_saldo_activo,
        sa.fecha_cierre,
        sa.id_nemotecnico,
        n.nemotecnico,
        EE.DSC_EMISOR_ESPECIFICO AS EMISOR,
        ee.cod_svs_nemotecnico as cod_emisor,
        n.dsc_nemotecnico,
        ISNULL(tasa_emision,0),
        sa.cantidad as cantidad,
        CASE
           when @pId_Moneda_Salida = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) then
              sa.precio
           else
              DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta, sa.precio, id_moneda_nemotecnico, @pId_Moneda_Salida, @pfecha_cierre)
        end as precio,
        n.tasa_emision,
        FECHA_VENCIMIENTO,
        CASE
           WHEN N.COD_INSTRUMENTO <> 'BONOS_INT' THEN
              CASE
                 when @pId_Moneda_Salida = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) then
                    isnull(dbo.entrega_Precio_Promedio(@Pfecha_Cierre, sa.id_cuenta, n.id_nemotecnico),0)
                 else
                    isnull(dbo.entrega_Precio_Promedio_Moneda_Cuenta(@Pfecha_Cierre, sa.id_cuenta, n.id_nemotecnico),0)
              end
           WHEN N.COD_INSTRUMENTO = 'BONOS_INT' THEN
              sa.tasa_compra
        END as precio_compra,
        sa.tasa,
        sa.tasa_compra as tasa_compra,
        sa.monto_valor_compra as monto_valor_compra,
        CASE
           when @pId_Moneda_Salida = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) then
              DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(@Pid_Cuenta, monto_mon_cta, id_moneda_cta, @pId_Moneda_Salida, @pfecha_cierre)
           else
              monto_mon_cta
        end as MONTO,
        id_moneda_cta,
        id_moneda_nemotecnico,
        MO.SIMBOLO AS SIMBOLO_MONEDA,
        monto_mon_nemotecnico,
        monto_mon_origen,
        aci.id_empresa,
        aci.id_arbol_clase_inst,
        n.cod_instrumento,
        CI.dsc_arbol_clase_inst,
        CASE DBO.PKG_GLOBAL$MONTO_MON_CTA_HOJA(@pid_arbol_Clase, sa.id_cuenta, @PFECHA_CIERRE)
           WHEN 0 THEN
              0
           ELSE
              monto_mon_cta / ISNULL(DBO.PKG_GLOBAL$MONTO_MON_CTA_HOJA(@pid_arbol_Clase, sa.id_cuenta, @PFECHA_CIERRE),1) * 100
        END AS porcentaje_rama,
        CASE
           when @pId_Moneda_Salida = DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) then
              isnull(dbo.entrega_Precio_Promedio(@Pfecha_Cierre, sa.id_cuenta, SA.id_nemotecnico),0)
           else
              isnull(dbo.entrega_Precio_Promedio_Moneda_Cuenta(@Pfecha_Cierre, sa.id_cuenta, SA.id_nemotecnico),0)
        END AS precio_promedio_compra,
        (SELECT ACII.DSC_Arbol_clase_inst
           FROM ARBOL_CLASE_INSTRUMENTO ACII
          WHERE ACII.ID_ARBOL_CLASE_INST = CI.id_padre_arbol_clase_inst) as dsc_padre_arbol_clase_inst,
        Case @pId_Moneda_Salida
           when DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) then
              Case id_moneda_nemotecnico
                 when DBO.PKG_MONEDAS$FNT_BUSCA_ID(DBO.PKG_GLOBAL$CMONEDA_COD_PESO()) then
                    CASE isnull(dbo.entrega_Precio_Promedio(@Pfecha_Cierre, sa.id_cuenta, sa.id_nemotecnico),0)*sa.cantidad
                       WHEN 0 THEN
                          null
                       ELSE
       ((sa.monto_mon_nemotecnico/(isnull(dbo.entrega_Precio_Promedio(@Pfecha_Cierre,sa.id_cuenta,sa.id_nemotecnico),1)*sa.cantidad))-1)*100
                    END
                 else
                    CASE isnull(dbo.entrega_Precio_Promedio_Moneda_Cuenta(@Pfecha_Cierre, sa.id_cuenta, sa.id_nemotecnico),0)*sa.cantidad
                       WHEN 0 THEN
                null
                       ELSE
                          (((sa.precio * SA.CANTIDAD)/(isnull(dbo.entrega_Precio_Promedio_Moneda_Cuenta(@Pfecha_Cierre,sa.id_cuenta,sa.id_nemotecnico),1)*sa.cantidad))-1)*100
                    END
              end
           else
              CASE isnull(dbo.entrega_Precio_Promedio_Moneda_Cuenta(@Pfecha_Cierre, sa.id_cuenta, sa.id_nemotecnico),0)*sa.cantidad
                 WHEN 0 THEN
                    null
                 ELSE
                    (((sa.precio * SA.CANTIDAD)/(isnull(dbo.entrega_Precio_Promedio_Moneda_Cuenta(@Pfecha_Cierre,sa.id_cuenta,sa.id_nemotecnico),1)*sa.cantidad))-1)*100
              END
        end as rentabilidad,
        0 as dias,
        0 as duration,
        i.cod_producto,
        ci.id_padre_arbol_clase_inst,

        (SELECT TOP 1 P.DURACION
           FROM PUBLICADORES_PRECIOS P
          WHERE P.ID_NEMOTECNICO = N.ID_NEMOTECNICO
            AND P.FECHA <= @pfecha_cierre
            AND P.DURACION != 0
          ORDER BY P.FECHA desc)as duracion,
        (SELECT (SUBSTRING((SELECT distinct(', ' + cod_valor_clasificacion)
           FROM rel_nemotecnico_valor_clasific rn,
                clasificadores_riesgo c
          where rn.id_nemotecnico = isnull(@pid_Nemotecnico, n.id_nemotecnico)
            AND rn.id_clasificador_riesgo = c.id_clasificador_riesgo FOR XML PATH('')),3,100))) as dsc_clasificador_riesgo,
        CASE
           WHEN (P.COD_PRODUCTO = dbo.Pkg_Global$gcPROD_RF_NAC()) THEN
              (SELECT    CASE WHEN (MA.FLG_TIPO_MOVIMIENTO = 'I') THEN MA.FECHA_OPERACION
                              WHEN (MA.FLG_TIPO_MOVIMIENTO = 'E') THEN
                                 (SELECT MA2.FECHA_OPERACION
                                    FROM MOV_ACTIVOS MA2
                                   WHERE MA2.ID_MOV_ACTIVO  = MA.ID_MOV_ACTIVO_COMPRA AND
                                         MA2.ID_NEMOTECNICO = MA.ID_NEMOTECNICO)
                         END
                 FROM MOV_ACTIVOS MA WHERE MA.ID_MOV_ACTIVO = SA.ID_MOV_ACTIVO)
        END AS FECHA_COMPRA,
        CASE
           WHEN (P.COD_PRODUCTO = dbo.Pkg_Global$gcPROD_RF_NAC()) THEN N.CORTE_MINIMO_PAPEL
        END AS CORTE_MINIMO_PAPEL,
        CASE
           WHEN N.COD_INSTRUMENTO = 'BONOS_NAC' And N.ID_SUBFAMILIA IN (SELECT ID_SUBFAMILIA FROM subfamilias WHERE COD_SUBFAMILIA = 'LH') THEN
              (SELECT NROCUPONES FROM CS_TB_SERIES WHERE CODSERIE = SUBSTRING(N.NEMOTECNICO,1,6))
           WHEN N.COD_INSTRUMENTO = 'BONOS_NAC' And NOT N.ID_SUBFAMILIA IN (SELECT ID_SUBFAMILIA FROM subfamilias WHERE COD_SUBFAMILIA IN ('LH', 'BR')) THEN
              (SELECT NROCUPONES FROM CS_TB_SERIES WHERE CODSERIE = N.NEMOTECNICO)
        END AS NROCUPONES,
		(SELECT 
				r.ISIM
                FROM REL_NEMOTECNICO_ATRIBUTOS R
                INNER JOIN INSTRUMENTOS_SVS I
                ON I.ID_INSTRUMENTO_SVS = R.ID_INSTRUMENTO_SVS
                WHERE  R.ID_NEMOTECNICO = N.ID_NEMOTECNICO
		) as CodIsin
   FROM
        REL_ACI_EMP_NEMOTECNICO ACI,
        NEMOTECNICOS N,
        SALDOS_ACTIVOS SA,
        ARBOL_CLASE_INSTRUMENTO CI,
        MONEDAS MO,
        EMISORES_ESPECIFICO EE,
        INSTRUMENTOS i,
        PRODUCTOS P
  WHERE
        N.ID_NEMOTECNICO        = ACI.ID_NEMOTECNICO
    AND SA.ID_NEMOTECNICO       = N.ID_NEMOTECNICO
    AND EE.ID_EMISOR_ESPECIFICO = N.ID_EMISOR_ESPECIFICO
    AND ACI.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
    AND SA.ID_CUENTA            = @pid_cuenta
    AND SA.FECHA_CIERRE         = @pfecha_cierre
    AND CI.ID_ARBOL_CLASE_INST  = ACI.ID_ARBOL_CLASE_INST
    AND MO.ID_MONEDA            = n.ID_MONEDA
    AND N.COD_INSTRUMENTO       = I.COD_INSTRUMENTO
    AND P.COD_PRODUCTO          = I.COD_PRODUCTO
    AND CI.ID_EMPRESA           = @LID_EMPRESA
    AND ACI.ID_EMPRESA          = CI.ID_EMPRESA
    AND N.ID_NEMOTECNICO        = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)

    DECLARE @LDSC_ARBOL VARCHAR(50)
          , @LDSC_PADRE VARCHAR(50)

    SELECT @LDSC_ARBOL = DSC_ARBOL_CLASE_INST
         , @LDSC_PADRE = (SELECT DSC_ARBOL_CLASE_INST
                            FROM ARBOL_CLASE_INSTRUMENTO
              WHERE ID_ARBOL_CLASE_INST= A.ID_PADRE_ARBOL_CLASE_INST)
      FROM ARBOL_CLASE_INSTRUMENTO A
     WHERE A.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE


    IF UPPER(@LDSC_PADRE) = 'RENTA FIJA INTERNACIONAL' OR UPPER(@LDSC_PADRE) = 'RENTA VARIABLE INTERNACIONAL'
    BEGIN
       INSERT INTO @salida (id_cuenta
                          , id_saldo_activo
                          , fecha_cierre
                          , id_nemotecnico
                          , nemotecnico
                          , EMISOR
                          , cod_emisor
                          , dsc_nemotecnico
                          , tasa_emision_2
                          , cantidad
                          , precio
                          , tasa_emision
                          , FECHA_VENCIMIENTO
                          , precio_compra
                          , tasa
                          , tasa_compra
                          , monto_valor_compra
                          , monto_mon_cta
                          , id_moneda_cta
                          , id_moneda_nemotecnico
                          , SIMBOLO_MONEDA
                          , monto_mon_nemotecnico
                          , monto_mon_origen
                          , id_empresa
                          , id_arbol_clase_inst
                          , cod_instrumento
                          , dsc_arbol_clase_inst
                          , porcentaje_rama
                          , precio_promedio_compra
                          , dsc_padre_arbol_clase_inst
                          , rentabilidad
                          , dias
                          , duration
                          , cod_producto
                          , id_padre_arbol_clase_inst
                          , duracion
                          , dsc_clasificador_riesgo
                          , Fecha_Compra
                          , CORTE_MINIMO_PAPEL
                          , NroCupones
                          , Origen_int
						  , CodIsin)
         SELECT
                sa.id_cuenta,
                sa.id_saldo_activo_INT,
                sa.fecha_cierre,
                sa.id_nemotecnico,
                n.nemotecnico,
                EE.DSC_EMISOR_ESPECIFICO AS EMISOR,
                ee.cod_svs_nemotecnico as cod_emisor,
                n.dsc_nemotecnico,
                ISNULL(N.tasa_emision,0),
                sa.cantidad,
                SA.PRECIO_MERCADO,
                ISNULL(N.tasa_emision,0),
                SA.FECHA_VENCIMIENTO,
                SA.PRECIO_PROMEDIO_COMPRA,
                NULL,
                NULL,
                SA.MONTO_INVERTIDO,
                DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                     , SA.VALOR_MERCADO_MON_USD
                                                     , 2
                                                     , @PID_MONEDA_SALIDA
                                                     , @PFECHA_CIERRE),
                (SELECT ID_MONEDA FROM CUENTAS WHERE ID_CUENTA = @PID_CUENTA),
                2,
                CASE
                   WHEN (SA.ORIGEN = 'INV') THEN
                      (SELECT SIMBOLO FROM MONEDAS M WHERE M.ID_MONEDA = (SELECT ID_ENTIDAD FROM VIEW_ALIAS WHERE COD_ORIGEN = 'INVERSIS' AND TABLA = 'MONEDAS' AND VALOR = SA.MONEDA))
                   WHEN (SA.ORIGEN = 'PSH') THEN
                      (SELECT SIMBOLO FROM MONEDAS M WHERE M.COD_MONEDA = SA.MONEDA)
                   ELSE  SA.MONEDA
                END ,
                SA.VALOR_MERCADO_MON_USD,
                SA.VALOR_MERCADO_MON_ORIGEN,
                (SELECT ID_EMPRESA FROM ARBOL_CLASE_INSTRUMENTO WHERE id_arbol_clase_inst = @PID_ARBOL_CLASE),
                @PID_ARBOL_CLASE,
                N.COD_INSTRUMENTO,
                (SELECT DSC_arbol_clase_inst FROM ARBOL_CLASE_INSTRUMENTO WHERE id_arbol_clase_inst = @PID_ARBOL_CLASE),
                NULL,
                SA.PRECIO_PROMEDIO_COMPRA,
                @LDSC_PADRE,
                CASE ISNULL(SA.PRECIO_PROMEDIO_COMPRA,0) * SA.CANTIDAD
                   WHEN 0 THEN
                      NULL
                   ELSE
                      (((SA.PRECIO_MERCADO * SA.CANTIDAD) / (SA.PRECIO_PROMEDIO_COMPRA * SA.CANTIDAD)) -1) * 100
                END,
                0,
                0,
                I.COD_PRODUCTO,
                NULL,
                0,
                (SELECT (SUBSTRING((SELECT distinct(', ' + cod_valor_clasificacion)
                FROM rel_nemotecnico_valor_clasific rn,
                clasificadores_riesgo c
                where rn.id_nemotecnico = isnull(@pid_Nemotecnico, n.id_nemotecnico)
                AND rn.id_clasificador_riesgo = c.id_clasificador_riesgo FOR XML PATH('')),3,100))) as dsc_clasificador_riesgo,
                NULL,
                NULL,
                NULL,
                SA.ORIGEN,
				(SELECT 
				r.ISIM
                FROM REL_NEMOTECNICO_ATRIBUTOS R
                INNER JOIN INSTRUMENTOS_SVS I
                ON I.ID_INSTRUMENTO_SVS = R.ID_INSTRUMENTO_SVS
                WHERE  R.ID_NEMOTECNICO = N.ID_NEMOTECNICO
				) as CodIsin
           FROM REL_ACI_EMP_NEMOTECNICO ACI,
                NEMOTECNICOS N,
                SALDOS_ACTIVOS_INT SA,
                MONEDAS MO,
                EMISORES_ESPECIFICO EE,
                INSTRUMENTOS I,
                PRODUCTOS P
          where N.ID_NEMOTECNICO        = ACI.ID_NEMOTECNICO
            AND SA.ID_NEMOTECNICO       = N.ID_NEMOTECNICO
            AND SA.FECHA_CIERRE         = @pfecha_cierre
            AND SA.ID_CUENTA            = @pid_cuenta
            AND EE.ID_EMISOR_ESPECIFICO = N.ID_EMISOR_ESPECIFICO
            AND MO.ID_MONEDA            = n.ID_MONEDA
            AND ACI.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
            AND N.COD_INSTRUMENTO       = I.COD_INSTRUMENTO
            AND P.COD_PRODUCTO          = I.COD_PRODUCTO
            AND N.ID_NEMOTECNICO        = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)

        SET @LTOTAL_INT = 0
        SELECT @LTOTAL_INT = SUM(MONTO_MON_CTA)
          FROM @salida
         WHERE ORIGEN_INT = 'INV'

        UPDATE @salida
           SET TOTAL_HOJA= ISNULL(@LTOTAL_INT,0)
         WHERE ORIGEN_INT = 'INV'

        SET @LTOTAL_INT = 0
        SELECT @LTOTAL_INT = SUM(MONTO_MON_CTA)
          FROM @salida
         WHERE ORIGEN_INT = 'PSH'

        UPDATE @salida
           SET TOTAL_HOJA = ISNULL(@LTOTAL_INT,0)
         WHERE ORIGEN_INT = 'PSH'

        UPDATE @salida
           SET PORCENTAJE_RAMA = CASE TOTAL_HOJA WHEN 0 THEN 0 ELSE ((MONTO_MON_CTA / TOTAL_HOJA) * 100) END
             , RENTABILIDAD    = CASE (PRECIO_PROMEDIO_COMPRA * CANTIDAD)
                                    WHEN 0 THEN
                                       0
                                    ELSE
                                       ((MONTO_MON_NEMOTECNICO/(PRECIO_PROMEDIO_COMPRA * CANTIDAD))-1)*100
                                 END
         where ORIGEN_INT = 'PSH' OR ORIGEN_INT = 'INV'
    END


   IF (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'FORWARDS')
       BEGIN
          INSERT INTO @salida (id_cuenta
                             , id_saldo_activo
                             , fecha_cierre
                             , id_nemotecnico
                             , nemotecnico
                             , EMISOR
                             , cod_emisor
                             , dsc_nemotecnico
                             , tasa_emision_2
                             , cantidad
                             , precio
                             , tasa_emision
                             , FECHA_VENCIMIENTO
                             , precio_compra
                             , tasa
                             , tasa_compra
                             , monto_valor_compra
                             , monto_mon_cta
                             , id_moneda_cta
                             , id_moneda_nemotecnico
                             , SIMBOLO_MONEDA
                             , monto_mon_nemotecnico
                             , monto_mon_origen
                             , id_empresa
                             , id_arbol_clase_inst
                             , cod_instrumento
                             , dsc_arbol_clase_inst
                             , porcentaje_rama
                             , precio_promedio_compra
                             , dsc_padre_arbol_clase_inst
                             , rentabilidad
                             , dias
                             , duration
                             , cod_producto
                             , id_padre_arbol_clase_inst
                             , duracion
                             , dsc_clasificador_riesgo
                             , Fecha_Compra
                             , CORTE_MINIMO_PAPEL
                             , NroCupones
                            , Origen_int,
                            codisin)

               SELECT SD.ID_CUENTA   ,
                      0              ,
                      SD.FECHA_CIERRE,
                      0              ,
                      ''             ,
                      ''             ,
                      ''             ,
                      ''             ,
                       0             ,
                      ROUND((CASE WHEN NOT SD.COD_MONEDA_RECIBIR = '$$' THEN SD.MONTO_MON_RECIBIR
                                  ELSE SD.MONTO_MON_PAGAR END),1),
                      ISNULL(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SD.ID_CUENTA
                                                                  , SD.VMM
                                                                  , DBO.FNT_DAMEIDMONEDA('$$')
                                                                  , @PID_MONEDA_SALIDA
                                                                  , @PFECHA_CIERRE) ,0),
                      SD.NOCIONALES                 ,
					  SD.FCH_VENCIMIENTO,
                      0                 ,
                      NULL              ,
                      NULL              ,
                      0                 ,
                      0                 ,
                     (SELECT ID_MONEDA FROM CUENTAS WHERE ID_CUENTA = @PID_CUENTA),
                      2                 ,
                     (CASE WHEN SD.COD_MONEDA_PAGAR = '$$' THEN 'PESOS'  ELSE SD.COD_MONEDA_PAGAR END) ,
                      0                 ,
                      0                 ,
                     (SELECT ID_EMPRESA FROM ARBOL_CLASE_INSTRUMENTO WHERE id_arbol_clase_inst = @PID_ARBOL_CLASE),
                      @PID_ARBOL_CLASE,
                      SD.COD_INSTRUMENTO,
                     (SELECT DSC_arbol_clase_inst FROM ARBOL_CLASE_INSTRUMENTO WHERE id_arbol_clase_inst = @PID_ARBOL_CLASE),
                      NULL,
                      0   ,
                      @LDSC_PADRE,
                      0   ,
                      0,
                      0,
                      'DERI_NAC',
                      NULL,
                      0,
                      '' as dsc_clasificador_riesgo,
                      NULL,
                      NULL,
                      NULL,
                      '',
					  null
              FROM VIEW_SALDOS_DERIVADOS SD
             WHERE SD.ID_CUENTA = @PID_CUENTA
               AND SD.FECHA_CIERRE = @PFECHA_CIERRE
               AND SD.COD_INSTRUMENTO = 'FWD_NAC'
              order by SD.FCH_OPERACION

              SELECT
                S.ID_CUENTA,
                S.ID_SALDO_ACTIVO,
                S.FECHA_CIERRE,
                S.ID_NEMOTECNICO,
                S.NEMOTECNICO,
                S.EMISOR,
                S.COD_EMISOR,
                S.DSC_NEMOTECNICO,
                S.TASA_EMISION_2,
                S.CANTIDAD,
                S.PRECIO,
                S.TASA_EMISION,
                S.FECHA_VENCIMIENTO,
                S.PRECIO_COMPRA,
                S.TASA,
                S.TASA_COMPRA,
                S.MONTO_VALOR_COMPRA,
                S.MONTO_MON_CTA,
                S.ID_MONEDA_CTA,
                S.ID_MONEDA_NEMOTECNICO,
                S.SIMBOLO_MONEDA,
                S.MONTO_MON_NEMOTECNICO,
                S.MONTO_MON_ORIGEN,
                S.ID_EMPRESA,
                S.ID_ARBOL_CLASE_INST,
                S.COD_INSTRUMENTO,
                S.DSC_ARBOL_CLASE_INST,
                S.PORCENTAJE_RAMA,
                S.PRECIO_PROMEDIO_COMPRA,
                S.CANTIDAD * S.PRECIO_PROMEDIO_COMPRA MONTO_PROMEDIO_COMPRA,
                S.DSC_PADRE_ARBOL_CLASE_INST,
                S.RENTABILIDAD,
                S.DIAS,
                S.COD_PRODUCTO,
                S.ID_PADRE_ARBOL_CLASE_INST,
                S.DURACION,
                0 DECIMALES_NEMO,
                S.dsc_clasificador_riesgo,
                '' as id_subfamilia,
                '' as cod_subfamilia,
                '' as DSC_SUBFAMILIA,
                S.FECHA_COMPRA,
                S.CORTE_MINIMO_PAPEL,
                S.NROCUPONES,
                '' 'COD_INSTRUMENTO_SVS',
				'' 'CodIsin'
             FROM  @SALIDA S




            ORDER BY S.FECHA_VENCIMIENTO
            return
       end


 IF (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'NOTAS ESTRUCTURADAS')
     BEGIN
          SELECT @LID_MONEDA_USD = DBO.FNT_DAMEIDMONEDA('USD')
          INSERT INTO @salida (id_cuenta
                             , id_saldo_activo
                             , fecha_cierre
                             , id_nemotecnico
                             , nemotecnico
                             , EMISOR
                             , cod_emisor
                             , dsc_nemotecnico
                             , tasa_emision_2
                             , cantidad
                             , precio
                             , tasa_emision
                             , FECHA_VENCIMIENTO
                             , precio_compra
                             , tasa
                             , tasa_compra
                             , monto_valor_compra
                             , monto_mon_cta
                             , id_moneda_cta
                             , id_moneda_nemotecnico
                             , SIMBOLO_MONEDA
                             , monto_mon_nemotecnico
                             , monto_mon_origen
                             , id_empresa
                             , id_arbol_clase_inst
                             , cod_instrumento
                             , dsc_arbol_clase_inst
                             , porcentaje_rama
                             , precio_promedio_compra
                             , dsc_padre_arbol_clase_inst
                             , rentabilidad
                             , dias
                             , duration
                             , cod_producto
                             , id_padre_arbol_clase_inst
                             , duracion
                             , dsc_clasificador_riesgo
                             , Fecha_Compra
                             , CORTE_MINIMO_PAPEL
                             , NroCupones
                            , Origen_int
							, CodIsin)

                           SELECT S.ID_CUENTA          ,
                                  S.ID_SALDO_ACTIVO_INT,
                                  S.FECHA_CIERRE       ,
                                  N.ID_NEMOTECNICO     ,
                                  N.NEMOTECNICO        ,
                                  ''                   ,
                                  ''                   ,
                                  N.DSC_NEMOTECNICO    ,
                                  0                   ,
                                  S.CANTIDAD           ,
                                  S.PRECIO_PROMEDIO_COMPRA ,
                                  0                    ,
                                  S.FECHA_VENCIMIENTO  ,
                                  S.PRECIO_PROMEDIO_COMPRA,
                                  0                   ,
                                  0                   ,
                                  S.PRECIO_MERCADO    ,
                                  DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA , VALOR_MERCADO_MON_USD, @LID_MONEDA_USD, @PID_MONEDA_SALIDA, @PFECHA_CIERRE)                   ,
                                  N.ID_MONEDA         ,
                                  N.ID_MONEDA         ,
                                  S.MONEDA            ,
                                  S.MONTO_INVERTIDO   ,
                                  S.VALOR_MERCADO_MON_ORIGEN ,
                                  @LID_EMPRESA        ,
                                  @PID_ARBOL_CLASE    ,
                                  N.COD_INSTRUMENTO   ,
                                  (SELECT DSC_arbol_clase_inst FROM ARBOL_CLASE_INSTRUMENTO WHERE id_arbol_clase_inst = @PID_ARBOL_CLASE),
                                  null                ,
                                  S.PRECIO_PROMEDIO_COMPRA ,
                                  @LDSC_PADRE         ,
                                  0                   ,
                                  0                   ,
                                  0                   ,
                                  N.COD_PRODUCTO      ,
                                  null                ,
                                  0                   ,
                                  ''                  ,
                                  null                ,
                                  null                ,
                                  null                ,
                                  '',
								   (SELECT 
									r.ISIM
									FROM REL_NEMOTECNICO_ATRIBUTOS R
									INNER JOIN INSTRUMENTOS_SVS I
									ON I.ID_INSTRUMENTO_SVS = R.ID_INSTRUMENTO_SVS
									WHERE  R.ID_NEMOTECNICO = S.ID_NEMOTECNICO
									) as CodIsin
                           FROM SALDOS_ACTIVOS_INT  S ,(SELECT N.ID_NEMOTECNICO
                                                             , N.NEMOTECNICO
                                                             , N.DSC_NEMOTECNICO
                                                              , N.COD_INSTRUMENTO
                                                             , N.ID_MONEDA
                                                             , I.COD_PRODUCTO
                                                        FROM NEMOTECNICOS N
                                                        INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                           WHERE S.ID_CUENTA = @PID_CUENTA
                             AND S.FECHA_CIERRE = @PFECHA_CIERRE
                             AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO
                                                      FROM REL_ACI_EMP_NEMOTECNICO
                                                       WHERE ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE)
                                                        AND N.ID_NEMOTECNICO = S.ID_NEMOTECNICO
                           Union
                           SELECT S.ID_CUENTA        ,
                                  S.ID_SALDO_ACTIVO  ,
                                  S.FECHA_CIERRE     ,
                                  N.ID_NEMOTECNICO   ,
                                  N.NEMOTECNICO      ,
                                  ''                 , -- EMISOR
                                  ''                 , -- cod_emisor
                                  N.DSC_NEMOTECNICO  ,
                                  N.TASA_EMISION     ,
                                  S.CANTIDAD         ,
                                  ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, S.ID_CUENTA, S.ID_NEMOTECNICO),0)  ,
                                  0                  ,
                                  N.FECHA_VENCIMIENTO,
                                  ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, S.ID_CUENTA, S.ID_NEMOTECNICO),0)  ,
                                  0                  ,
                                  S.TASA_COMPRA      ,
                                  ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, S.ID_CUENTA, S.ID_NEMOTECNICO),0) ,
                                  S.MONTO_MON_CTA    ,
                                  S.ID_MONEDA_CTA    ,
                                  N.ID_MONEDA        ,
                                  M.SIMBOLO          ,
                                  S.MONTO_MON_NEMOTECNICO ,
                                  S.MONTO_MON_ORIGEN ,
                                  @LID_EMPRESA       ,
                                  @PID_ARBOL_CLASE   ,
                                  N.COD_INSTRUMENTO  ,
                                  (SELECT DSC_arbol_clase_inst FROM ARBOL_CLASE_INSTRUMENTO WHERE id_arbol_clase_inst = @PID_ARBOL_CLASE),
                                  null ,
                                  S.PRECIO_COMPRA ,
                                  @LDSC_PADRE         ,
                                  0                   ,
                                  0                   ,
                                  0         ,
                                  N.COD_PRODUCTO      ,
                                  null ,
                                  0    ,
                                  ''   ,
                                  null ,
                                  0      ,
                                  0      ,
                                  '',
								  (SELECT 
									r.ISIM
									FROM REL_NEMOTECNICO_ATRIBUTOS R
									INNER JOIN INSTRUMENTOS_SVS I
									ON I.ID_INSTRUMENTO_SVS = R.ID_INSTRUMENTO_SVS
									WHERE  R.ID_NEMOTECNICO = N.ID_NEMOTECNICO
									) as CodIsin


                              FROM (SELECT FECHA_CIERRE
                                         , ID_CUENTA
                                         , ID_MONEDA_CTA
                                         , ID_NEMOTECNICO
                                         , PRECIO
                                         , SUM(CANTIDAD) CANTIDAD
                                         , SUM(MONTO_MON_CTA) MONTO_MON_CTA
                                         , SUM(MONTO_MON_NEMOTECNICO) MONTO_MON_NEMOTECNICO
                                         , ID_SALDO_ACTIVO
                                         , TASA_COMPRA
                                         , sum(MONTO_MON_ORIGEN) MONTO_MON_ORIGEN
                                         , sum(PRECIO_COMPRA) PRECIO_COMPRA
                                 FROM SALDOS_ACTIVOS
                                     WHERE ID_CUENTA = @PID_CUENTA
                                       AND FECHA_CIERRE = @PFECHA_CIERRE
                                     GROUP BY FECHA_CIERRE,ID_CUENTA, ID_MONEDA_CTA,ID_NEMOTECNICO, PRECIO,ID_SALDO_ACTIVO,TASA_COMPRA) S
                                 , (SELECT N.ID_NEMOTECNICO
                                         , N.NEMOTECNICO
                                         , N.DSC_NEMOTECNICO
                                         , N.COD_INSTRUMENTO
                                         , N.ID_MONEDA
                                         , I.COD_PRODUCTO
                                         , N.TASA_EMISION
                                         , N.FECHA_VENCIMIENTO
                                  FROM NEMOTECNICOS N
                                     INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                                    , MONEDAS M
                                     WHERE S.ID_CUENTA = @PID_CUENTA
                                       AND S.FECHA_CIERRE = @PFECHA_CIERRE
                                       AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO
                                                                FROM REL_ACI_EMP_NEMOTECNICO
                                                                WHERE ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE)
                                                                  AND N.ID_NEMOTECNICO = S.ID_NEMOTECNICO
                                                                  AND M.ID_MONEDA = N.ID_MONEDA

     END

          SELECT
                S.ID_CUENTA,
                S.ID_SALDO_ACTIVO,
                S.FECHA_CIERRE,
                S.ID_NEMOTECNICO,
                S.NEMOTECNICO,
                S.EMISOR,
                S.COD_EMISOR,
                S.DSC_NEMOTECNICO,
                S.TASA_EMISION_2,
                S.CANTIDAD,
                S.PRECIO,
                S.TASA_EMISION,
                S.FECHA_VENCIMIENTO,
                S.PRECIO_COMPRA,
                S.TASA,
                S.TASA_COMPRA,
                S.MONTO_VALOR_COMPRA,
                S.MONTO_MON_CTA,
                S.ID_MONEDA_CTA,
                S.ID_MONEDA_NEMOTECNICO,
                S.SIMBOLO_MONEDA,
                S.MONTO_MON_NEMOTECNICO,
                S.MONTO_MON_ORIGEN,
                S.ID_EMPRESA,
                S.ID_ARBOL_CLASE_INST,
                S.COD_INSTRUMENTO,
                S.DSC_ARBOL_CLASE_INST,
                S.PORCENTAJE_RAMA,
                S.PRECIO_PROMEDIO_COMPRA,
                S.CANTIDAD * S.PRECIO_PROMEDIO_COMPRA MONTO_PROMEDIO_COMPRA,
                S.DSC_PADRE_ARBOL_CLASE_INST,
                S.RENTABILIDAD,
                S.DIAS,
                S.COD_PRODUCTO,
                S.ID_PADRE_ARBOL_CLASE_INST,
                S.DURACION,
                M.DICIMALES_MOSTRAR DECIMALES_NEMO,
                S.dsc_clasificador_riesgo,
                SF.id_subfamilia as id_subfamilia,
                SF.cod_subfamilia as cod_subfamilia,
                SF.DSC_SUBFAMILIA as DSC_SUBFAMILIA,
                S.FECHA_COMPRA,
                S.CORTE_MINIMO_PAPEL,
                S.NROCUPONES,
                ISNULL(ISVS.COD_INSTRUMENTO_SVS,'') 'COD_INSTRUMENTO_SVS',
				CodIsin
             FROM
                @SALIDA S
                LEFT JOIN NEMOTECNICOS N ON (S.ID_NEMOTECNICO = N.ID_NEMOTECNICO)
                LEFT JOIN VIEW_EMISORES_ESPECIFICOS E ON (N.ID_EMISOR_ESPECIFICO = E.ID_EMISOR_ESPECIFICO)
                LEFT JOIN MONEDAS M ON (M.ID_MONEDA = N.ID_MONEDA)
                LEFT JOIN SUBFAMILIAS SF ON (N.ID_SUBFAMILIA=SF.ID_SUBFAMILIA)
                LEFT OUTER JOIN (SELECT R.ID_NEMOTECNICO
                                      , R.ID_INSTRUMENTO_SVS
                                      , I.COD_INSTRUMENTO_SVS
                                 FROM REL_NEMOTECNICO_ATRIBUTOS R
                                      INNER JOIN INSTRUMENTOS_SVS I
                                            ON I.ID_INSTRUMENTO_SVS = R.ID_INSTRUMENTO_SVS
                               )ISVS ON ISVS.ID_NEMOTECNICO = N.ID_NEMOTECNICO
            WHERE N.ID_MERCADO_TRANSACCION = ISNULL(@PID_MERCADO_TRANSACCION,N.ID_MERCADO_TRANSACCION)
              AND N.ID_MONEDA              = ISNULL(@PID_MONEDA,N.ID_MONEDA)
              AND E.ID_SECTOR              = ISNULL(@PID_SECTOR, E.ID_SECTOR)
              AND N.ID_NEMOTECNICO         = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)
         ORDER BY N.ID_MONEDA, N.NEMOTECNICO

   set nocount off
   declare @db_null_statement_2 int
END
GO
GRANT EXECUTE ON [PKG_SALDOS_ACTIVOS$RESUMEN_X_HOJA_ARBOL_CLASE_INST_CDS] TO DB_EXECUTESP
GO