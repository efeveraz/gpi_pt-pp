USE CSGPI 
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_SALDOS_ACTIVOS_WEB$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO_VR2]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE PKG_SALDOS_ACTIVOS_WEB$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO_VR2
GO
ALTER PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS_WEB$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO_VR2]
( @PID_CUENTA              NUMERIC = NULL
, @PFECHA_CIERRE           DATETIME
, @PID_ARBOL_CLASE         NUMERIC
, @pId_Moneda_Salida       NUMERIC = NULL
, @pid_Mercado_Transaccion NUMERIC = NULL
, @pid_Moneda              NUMERIC = NULL
, @pid_sector              NUMERIC = NULL
, @pid_Nemotecnico         NUMERIC = NULL
) AS
BEGIN
    SET NOCOUNT ON

--DECLARE @PID_CUENTA              NUMERIC 
--, @PFECHA_CIERRE           DATETIME
--, @PID_ARBOL_CLASE         NUMERIC
--, @pId_Moneda_Salida       NUMERIC
--, @pid_Mercado_Transaccion NUMERIC
--, @pid_Moneda              NUMERIC
--, @pid_sector              NUMERIC
--, @pid_Nemotecnico         NUMERIC

--SET @PID_CUENTA               = 2016
--SET @PFECHA_CIERRE           ='20200415'
--SET @PID_ARBOL_CLASE         =1612
----372
--SET @pId_Moneda_Salida       = 1
--SET @pid_Mercado_Transaccion = NULL
--SET @pid_Moneda              = NULL
--SET @pid_sector              = NULL
--SET @pid_Nemotecnico         = NULL

    DECLARE @LID_EMPRESA NUMERIC
          , @LCODIGO_ARBOL VARCHAR(20)
          , @LID_MONEDA_USD  NUMERIC
          , @LTOTAL_INT      NUMERIC(28,8)
          , @LORIGEN     VARCHAR(20)

    DECLARE @LDSC_ARBOL VARCHAR(50)
          , @LDSC_PADRE VARCHAR(50)
          , @LID_PADRE  NUMERIC
          , @LCODIGO_PADRE_ARBOL VARCHAR(20)
		  


    CREATE TABLE #TBLTEMP ( ORIGEN                         VARCHAR(20)
                         , ID_CUENTA        NUMERIC
                         , ID_SALDO_ACTIVO                   NUMERIC
                         , FECHA_CIERRE                      DATETIME
                         , ID_NEMOTECNICO                    NUMERIC
                         , NEMOTECNICO                       VARCHAR(50)
                         , EMISOR                            VARCHAR(100)
                         , COD_EMISOR                        VARCHAR(10)
                         , DSC_NEMOTECNICO                   VARCHAR(120)
                         , TASA_EMISION_2                    NUMERIC(18,4)
                         , CANTIDAD                          NUMERIC(18,4)
                         , DISPONIBLES						 NUMERIC(18,4)
                         , GARANTIAS						 NUMERIC(18,4)
                         , SIMULTANEAS						 NUMERIC(18,4)
                         , PRECIO                            NUMERIC(18,6)
                         , TASA_EMISION                      NUMERIC(18,4)
                         , FECHA_VENCIMIENTO                 DATETIME
                         , PRECIO_COMPRA                     NUMERIC(18,4)
                         , TASA                              NUMERIC(18,4)
                         , TASA_COMPRA                       NUMERIC(18,4)
                         , MONTO_VALOR_COMPRA                NUMERIC(18,4)
                         , MONTO_MON_CTA                     NUMERIC(18,4)
                         , MONTO_MON_USD                     NUMERIC(18,4)
                         , ID_MONEDA_CTA                     NUMERIC
                         , ID_MONEDA_NEMOTECNICO             NUMERIC
                         , SIMBOLO_MONEDA                    VARCHAR(3)
                         , MONTO_MON_NEMOTECNICO             NUMERIC(18,4)
                         , MONTO_MON_ORIGEN                  NUMERIC(18,4)
                         , ID_EMPRESA                        NUMERIC
                         , ID_ARBOL_CLASE_INST               NUMERIC
                         , COD_INSTRUMENTO                   VARCHAR(15)
                         , DSC_ARBOL_CLASE_INST              VARCHAR(100)
                         , PORCENTAJE_RAMA                   NUMERIC(18,4)
                         , PRECIO_PROMEDIO_COMPRA            NUMERIC(18,4)
                         , DSC_PADRE_ARBOL_CLASE_INST        VARCHAR(100)
                         , RENTABILIDAD                      FLOAT
                         , DIAS                              NUMERIC
							, DURATION                          NUMERIC
                         , COD_PRODUCTO                      VARCHAR(10)
                         , ID_PADRE_ARBOL_CLASE_INST         NUMERIC
                         , DURACION                          FLOAT
                         , DSC_CLASIFICADOR_RIESGO           VARCHAR(50)
                         , CODIGO                            VARCHAR(20)
                         )
    CREATE TABLE #TBLSALIDA ( ORIGEN                        VARCHAR(20)
						 , MERCADO							VARCHAR(20)
                         , ID_CUENTA                         NUMERIC
                         , FECHA_CIERRE                      DATETIME
                         , ID_NEMOTECNICO                    NUMERIC
                         , NEMOTECNICO                       VARCHAR(50)
                         , EMISOR                            VARCHAR(100)
                         , COD_EMISOR                        VARCHAR(10)
                         , DSC_NEMOTECNICO                   VARCHAR(120)
                         , TASA_EMISION_2                    NUMERIC(18,4)
                         , CANTIDAD                          NUMERIC(18,4)
                         , DISPONIBLES                          NUMERIC(18,4)
                         , GARANTIAS                          NUMERIC(18,4)
                         , SIMULTANEAS                          NUMERIC(18,4)
                         , PRECIO                            NUMERIC(18,6)
                         , TASA_EMISION                      NUMERIC(18,4)
                         , FECHA_VENCIMIENTO                 DATETIME
                         , PRECIO_COMPRA                     NUMERIC(18,4)
                         , TASA                              NUMERIC(18,4)
                         , TASA_COMPRA                       NUMERIC(18,4)
                         , MONTO_VALOR_COMPRA                NUMERIC(18,4)
                         , MONTO_MON_CTA                     NUMERIC(18,4)
                         , ID_MONEDA_CTA                     NUMERIC
						 , ID_MONEDA							NUMERIC
                         , ID_MONEDA_NEMOTECNICO             NUMERIC
                         , SIMBOLO_MONEDA                    VARCHAR(3)
                         , MONTO_MON_NEMOTECNICO             NUMERIC(18,4)
                         , MONTO_MON_ORIGEN                  NUMERIC(18,4)
                         , MONTO_PROMEDIO_COMPRA             NUMERIC(18,4)
                         , ID_EMPRESA                        NUMERIC
                         , ID_ARBOL_CLASE_INST               NUMERIC
                         , COD_INSTRUMENTO                   VARCHAR(15)
                         , DSC_ARBOL_CLASE_INST              VARCHAR(100)
                         , PORCENTAJE_RAMA                   NUMERIC(18,4)
                         , PRECIO_PROMEDIO_COMPRA            NUMERIC(18,4)
                         , DSC_PADRE_ARBOL_CLASE_INST        VARCHAR(100)
                         , RENTABILIDAD                      FLOAT
                         , DIAS                              NUMERIC
						 , DURATION							NUMERIC(18,4)
                         , DECIMALES_MOSTRAR                 NUMERIC
                         , COD_PRODUCTO                      VARCHAR(10)
                         , ID_PADRE_ARBOL_CLASE_INST         NUMERIC
                         , DURACION                          FLOAT
                         , ID_SUBFAMILIA                     NUMERIC
                         , COD_SUBFAMILIA                    VARCHAR(50)
                         , DSC_CLASIFICADOR_RIESGO           VARCHAR(50)
                         , CODIGO                            VARCHAR(20)
                         , MONTO_INVERTIDO                   NUMERIC(18,6)
                         , MONTO_MON_USD                     NUMERIC(18,6)
                         , VALOR_MERCADO                     NUMERIC(18,6)
                         , UTILIDAD_PERDIDA                  NUMERIC(18,6)
                         , NUMERO_CONTRATO                   VARCHAR(50)
                         , TIPO_FWD                          VARCHAR(10)
                         , MONEDA_EMISION                    VARCHAR(10)
                         , MODALIDAD                         VARCHAR(15)
                         , UNIDAD_ACTIVO_SUBYACENTE          VARCHAR(10)
                         , DECIMALES_ACTIVO_SUBYACENTE       NUMERIC
                         , VALOR_NOMINAL                     NUMERIC(18,4)
                         , FECHA_INICIO                      DATETIME
                         , PRECIO_PACTADO                    NUMERIC(18,6)
                         , VALOR_PRECIO_PACTADO              NUMERIC(18,4)
                         , PRECIO_MEDIO						 NUMERIC(18,4)
						 , VALOR_INI						 NUMERIC(18,4)
						 , PRECIO_MEDIO_MERCADO				 NUMERIC(18,4)
						 , VALOR_FIN						 NUMERIC(18,4)
						 , PRIMA_A_PLAZO					 NUMERIC(18,4)
						 , PRIMA_ACUMULADA					 NUMERIC(18,4)
						 , DIAS_EN_CURSO					 integer
						 , DIAS_OPERACION					 integer
						 , RESULTADO						 NUMERIC(18,4)
						 
						 )
    CREATE TABLE #TBLINT (ORIGEN                         VARCHAR(20)
						  , MERCADO						 VARCHAR(20)
                            , ID_ARBOL_CLASE_INST               NUMERIC
                            , SIMBOLO_MONEDA                    VARCHAR(20)
                            , COD_PRODUCTO                      VARCHAR(20)
                            , COD_INSTRUMENTO                   VARCHAR(30)
                            , ID_NEMOTECNICO                    NUMERIC
                            , NEMOTECNICO                       VARCHAR(50)
                            , DSC_NEMOTECNICO                   VARCHAR(120)
                            , ID_MONEDA_NEMOTECNICO             NUMERIC
                            , ID_MONEDA_CTA                     NUMERIC
                            , EMISOR                            VARCHAR(120)
                            , COD_EMISOR                        VARCHAR(20)
                            , FECHA_VENCIMIENTO                 DATETIME
                            , CANTIDAD                          NUMERIC(18,4)
                            , PRECIO                            NUMERIC(18,6)
                            , PRECIO_COMPRA                     NUMERIC(18,6)
                            , PRECIO_PROMEDIO_COMPRA            NUMERIC(18,4)
                            , PRECIO_ACTUAL                     NUMERIC(18,6)
                            , MONTO_INVERTIDO                   NUMERIC(18,6)
                            , MONTO_MON_NEMOTECNICO             NUMERIC(18,4)
                            , MONTO_VALOR_COMPRA                NUMERIC(18,4)
                            , VALOR_MERCADO                     NUMERIC(18,6)
                            , UTILIDAD_PERDIDA                  NUMERIC(18,6)
                            , MONTO_MON_CTA                     NUMERIC(18,4)
                            , MONTO_MON_USD                     NUMERIC(18,4)
                            , PORCENTAJE_RAMA                   NUMERIC(18,4)
                            , TOTAL_HOJA                        NUMERIC(18,4)
                            , MONTO_PROMEDIO_COMPRA             NUMERIC(18,4)
                            , RENTABILIDAD                      FLOAT
                            , DURACION                          FLOAT
                            )

----------------------------------------------------------------------------------------------------------------
    SELECT @LDSC_ARBOL = DSC_ARBOL_CLASE_INST
         , @LDSC_PADRE = (SELECT DSC_ARBOL_CLASE_INST
                            FROM ARBOL_CLASE_INSTRUMENTO
                           WHERE ID_ARBOL_CLASE_INST= A.ID_PADRE_ARBOL_CLASE_INST)
        , @LCODIGO_PADRE_ARBOL = (SELECT CODIGO
							FROM ARBOL_CLASE_INSTRUMENTO
						   WHERE ID_ARBOL_CLASE_INST= A.ID_PADRE_ARBOL_CLASE_INST)
         , @LCODIGO_ARBOL = CODIGO
         , @LID_PADRE  = A.ID_PADRE_ARBOL_CLASE_INST
      FROM ARBOL_CLASE_INSTRUMENTO A
     WHERE A.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE

    SELECT @LID_EMPRESA = ID_EMPRESA
        , @pId_Moneda_Salida = isnull(@pId_Moneda_Salida, ID_MONEDA )
    FROM CUENTAS
    WHERE ID_CUENTA  = @PID_CUENTA;

    SELECT @LID_MONEDA_USD = DBO.FNT_DAMEIDMONEDA('USD')
   
  -- select @LCODIGO_ARBOL
   
------------------------------------------------------------------------------------------------------------------
--IF  @LDSC_ARBOL = 'Fondos Mutuos' and   @LCODIGO_PADRE_ARBOL = 'RF' OR @LDSC_ARBOL = 'Fondos Mutuos Largo Plazo' OR @LDSC_ARBOL = 'Fondos Mutuos Corto Plazo' and   @LCODIGO_PADRE_ARBOL = 'RF'
--BEGIN
 
    
	

--	 INSERT INTO #TBLSALIDA (
--			ID_CUENTA,
--			FECHA_CIERRE,
--			ID_NEMOTECNICO,
--			NEMOTECNICO,
--			EMISOR,
--			COD_EMISOR,
--			DSC_NEMOTECNICO,
--			TASA_EMISION_2,
--			CANTIDAD,
--			GARANTIAS,
--			PRESTAMOS,
--			SIMULTANEAS,
--			PRECIO,
--			TASA_EMISION,
--			FECHA_VENCIMIENTO,
--			PRECIO_COMPRA,
--			TASA,
--			TASA_COMPRA,
--			MONTO_VALOR_COMPRA,
--			MONTO_MON_CTA,
--			MONTO_MON_USD,
--			ID_MONEDA_CTA,
--			ID_MONEDA,
--			SIMBOLO_MONEDA,
--			MONTO_MON_NEMOTECNICO,
--			MONTO_MON_ORIGEN,
--			ID_EMPRESA,
--			ID_ARBOL_CLASE_INST,
--			COD_INSTRUMENTO,
--			DSC_ARBOL_CLASE_INST,
--			PORCENTAJE_RAMA,
--			PRECIO_PROMEDIO_COMPRA,
--			DSC_PADRE_ARBOL_CLASE_INST,
--			RENTABILIDAD,
--			DIAS,
--			DURATION,
--			COD_PRODUCTO,
--			ID_PADRE_ARBOL_CLASE_INST,
--			DURACION,
--			DSC_CLASIFICADOR_RIESGO,
--			CODIGO) 
--     SELECT  SA.ID_CUENTA  
--      --, SA.ID_SALDO_ACTIVO  
--      , SA.FECHA_CIERRE  
--      , SA.ID_NEMOTECNICO  
--      , N.NEMOTECNICO  
--      , ISNULL(EE.DSC_EMISOR_ESPECIFICO,'') AS EMISOR  
--      , EE.COD_SVS_NEMOTECNICO AS COD_EMISOR  
--      , N.DSC_NEMOTECNICO  
--      , ISNULL(TASA_EMISION,0)  
--      , SA.CANTIDAD AS CANTIDAD
--       , ISNULL(GPS.GARANTIAS, 0) GARANTIAS
--	 , ISNULL(GPS.PRESTAMOS, 0) PRESTAMOS
--	 , ISNULL(GPS.SIMULTANEAS, 0) SIMULTANEAS   
--      , SA.PRECIO PRECIO  
--      , N.TASA_EMISION  
--      , N.FECHA_VENCIMIENTO  
--      , ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, N.ID_NEMOTECNICO),0) AS PRECIO_COMPRA  
--      , TASA  
--      , CASE WHEN SA.COD_PRODUCTO = 'RF_NAC' THEN DBO.FNT_ENTREGA_TASA_PROMEDIO (SA.FECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO)  
--       ELSE SA.TASA_COMPRA  
--        END AS TASA_COMPRA  
--      , SA.MONTO_VALOR_COMPRA AS MONTO_VALOR_COMPRA  
--      , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA, MONTO_MON_CTA, ID_MONEDA_CTA, @PID_MONEDA_SALIDA, @PFECHA_CIERRE)   MONTO  
--      , 0 'MONTO_MON_USD'  
--      , ID_MONEDA_CTA  
--      , N.ID_MONEDA  
--      , MO.SIMBOLO AS SIMBOLO_MONEDA  
--      , MONTO_MON_NEMOTECNICO  
--      , MONTO_MON_ORIGEN  
--      , ACI.ID_EMPRESA  
--      , ACI.ID_ARBOL_CLASE_INST  
--      , N.COD_INSTRUMENTO  
--      , CI.DSC_ARBOL_CLASE_INST  
--      --, CASE DBO.PKG_GLOBAL$MONTO_MON_CUENTA_HOJA(@PID_ARBOL_CLASE, SA.ID_CUENTA, @PFECHA_CIERRE,NULL,NULL,NULL)  
--      --    WHEN 0 THEN  
--      --        0  
--      --    ELSE  
--      --        MONTO_MON_CTA / ISNULL(DBO.PKG_GLOBAL$MONTO_MON_CUENTA_HOJA(@PID_ARBOL_CLASE, SA.ID_CUENTA, @PFECHA_CIERRE,NULL,NULL,NULL),1) * 100  
--      --    END AS PORCENTAJE_RAMA  
--		, DBO.PKG_GLOBAL$MONTO_MON_CUENTA_HOJA_VR2(ACI.ID_ARBOL_CLASE_INST, SA.ID_CUENTA, @PFECHA_CIERRE,NULL,NULL,NULL) AS PORCENTAJE_RAMA  
--      , ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)  
--      , (SELECT ACII.DSC_ARBOL_CLASE_INST  
--       FROM ARBOL_CLASE_INSTRUMENTO ACII  
--       WHERE ACII.ID_ARBOL_CLASE_INST = CI.ID_PADRE_ARBOL_CLASE_INST) AS DSC_PADRE_ARBOL_CLASE_INST  
--     -- , CASE ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)*SA.CANTIDAD  
--     --    WHEN 0 THEN NULL  
--     --       ELSE ((SA.MONTO_MON_NEMOTECNICO/(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE,SA.ID_CUENTA,SA.ID_NEMOTECNICO)*SA.CANTIDAD))-1)*100  
--     --END AS RENTABILIDAD  
--     , 0  
--      , 0 AS DIAS  
--      , 0 AS DURATION  
--      , I.COD_PRODUCTO  
--      , CI.ID_PADRE_ARBOL_CLASE_INST  
--      , (SELECT TOP 1 P.DURACION FROM PUBLICADORES_PRECIOS P  
--       WHERE P.ID_NEMOTECNICO = N.ID_NEMOTECNICO  
--        AND P.FECHA <= @PFECHA_CIERRE  
--        AND P.DURACION != 0  
--       ORDER BY P.FECHA DESC)AS DURACION  
--      , (SELECT TOP 1 RN.COD_VALOR_CLASIFICACION  
--       FROM REL_NEMOTECNICO_VALOR_CLASIFIC RN  
--     , CLASIFICADORES_RIESGO C  
--       WHERE RN.ID_NEMOTECNICO = ISNULL(@PID_NEMOTECNICO, N.ID_NEMOTECNICO)  
--        AND RN.ID_CLASIFICADOR_RIESGO = C.ID_CLASIFICADOR_RIESGO) AS DSC_CLASIFICADOR_RIESGO  
--      , CI.CODIGO  
--     FROM REL_ACI_EMP_NEMOTECNICO ACI  
--      , VIEW_SALDOS_ACTIVOS SA
--      LEFT OUTER JOIN GAR_PRE_SIM GPS ON GPS.ID_CUENTA  = SA.ID_CUENTA   AND GPS.ID_NEMOTECNICO =    SA.ID_NEMOTECNICO AND GPS.FECHA_CIERRE = SA.FECHA_CIERRE   
--      , ARBOL_CLASE_INSTRUMENTO CI  
--      , MONEDAS MO  
--      , INSTRUMENTOS I  
--      , NEMOTECNICOS N  
--        LEFT OUTER JOIN EMISORES_ESPECIFICO EE ON EE.ID_EMISOR_ESPECIFICO = N.ID_EMISOR_ESPECIFICO  
--     WHERE N.ID_NEMOTECNICO          = ACI.ID_NEMOTECNICO  
--      AND SA.ID_NEMOTECNICO       = N.ID_NEMOTECNICO  
--      AND ACI.ID_ARBOL_CLASE_INST IN(SELECT ID_ARBOL_CLASE_INST FROM ARBOL_CLASE_INSTRUMENTO WHERE DSC_ARBOL_CLASE_INST IN('Fondos Mutuos','Fondos Mutuos Largo Plazo','Fondos Mutuos Corto Plazo') AND ID_PADRE_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST FROM ARBOL_CLASE_INSTRUMENTO WHERE DSC_ARBOL_CLASE_INST = @LDSC_PADRE AND ID_ACI_TIPO =2 AND ID_EMPRESA = @LID_EMPRESA  ))
--      AND SA.ID_CUENTA            = @pid_cuenta  
--      AND SA.FECHA_CIERRE         = @pfecha_cierre  
--      AND CI.ID_ARBOL_CLASE_INST  = ACI.ID_ARBOL_CLASE_INST  
--      AND MO.ID_MONEDA            = n.ID_MONEDA  
--      AND N.COD_INSTRUMENTO       = I.COD_INSTRUMENTO  
--      AND CI.ID_EMPRESA           = @LID_EMPRESA  
--      AND ACI.ID_EMPRESA          = CI.ID_EMPRESA  
--      AND N.ID_NEMOTECNICO        = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)  

	  
--	 -- SELECT 'NACIONAL' 'ORIGEN'  
--  --    , ID_CUENTA  
--  --    , FECHA_CIERRE  
--  --    , ID_NEMOTECNICO  
--  --    , NEMOTECNICO  
--  --    , EMISOR  
--  --    , COD_EMISOR  
--  --    , DSC_NEMOTECNICO  
--  --    , TASA_EMISION_2  
--  --    , CANTIDAD
--  --    , GARANTIAS
--	 --, PRESTAMOS
--	 --, SIMULTANEAS   
--  --    , PRECIO  
--  --    , TASA_EMISION  
--  --    , FECHA_VENCIMIENTO  
--  --    , PRECIO_COMPRA  
--  --    , TASA  
--  --    , TASA_COMPRA  
--  --    , MONTO_VALOR_COMPRA  
--  --    , MONTO_MON_CTA  
--  --    , ID_MONEDA_CTA  
--  --    , ID_MONEDA_NEMOTECNICO  
--  --    , SIMBOLO_MONEDA  
--  --    , CASE CODIGO WHEN 'FFMM' THEN  MONTO_VALOR_COMPRA ELSE MONTO_MON_NEMOTECNICO  
--  --      END  MONTO_MON_NEMOTECNICO  
--  --    , MONTO_MON_ORIGEN  
--  --    , ID_EMPRESA  
--  --    , ID_ARBOL_CLASE_INST  
--  --    , COD_INSTRUMENTO  
--  --    , DSC_ARBOL_CLASE_INST  
--  --    , CASE ISNULL(PORCENTAJE_RAMA,0)  
--  --          WHEN 0 THEN 0  
--  --                 ELSE (MONTO_MON_CTA / PORCENTAJE_RAMA/2) * 100  
--  --      END AS PORCENTAJE_RAMA  
--  --    , PRECIO_PROMEDIO_COMPRA  
--  --    , MONTO_PROMEDIO_COMPRA  
--  --    , DSC_PADRE_ARBOL_CLASE_INST  
--  --    --, CASE WHEN COD_PRODUCTO = 'RF_NAC' THEN 0  
--  --    --       ELSE CASE (MONTO_PROMEDIO_COMPRA) WHEN 0 THEN NULL  
--  --    --       ELSE ((MONTO_MON_NEMOTECNICO/(PRECIO_PROMEDIO_COMPRA * CANTIDAD))-1)*100  
--  --    --   END  
--  --   --END AS RENTABILIDAD  
--  --    , CASE (PRECIO_PROMEDIO_COMPRA * CANTIDAD) WHEN 0 THEN 0  
--  --                                     ELSE ((MONTO_MON_NEMOTECNICO/(PRECIO_PROMEDIO_COMPRA * CANTIDAD))-1)*100  
--  --                            END  AS RENTABILIDAD  
--  --    , DIAS  
--  --    , COD_PRODUCTO  
--  --    , ID_PADRE_ARBOL_CLASE_INST  
--  --    , DURACION  
--  --    , DICIMALES_MOSTRAR  
--  --    , DSC_CLASIFICADOR_RIESGO  
--  --    , ID_SUBFAMILIA AS ID_SUBFAMILIA  
--  --    , COD_SUBFAMILIA AS COD_SUBFAMILIA  
--  --    , CODIGO  
--  --   FROM (SELECT S.ID_CUENTA  
--  --    --, S.ID_SALDO_ACTIVO  
--  --    , S.FECHA_CIERRE  
--  --    , S.ID_NEMOTECNICO  
--  --    , S.NEMOTECNICO  
--  --    , S.EMISOR  
--  --    , S.COD_EMISOR  
--  --    , S.DSC_NEMOTECNICO  
--  --    , S.TASA_EMISION_2  
--  --    , sum(S.CANTIDAD) cantidad
--  --    ,S.GARANTIAS
--	 --,S.PRESTAMOS
--	 --,S.SIMULTANEAS  
--  --    , S.PRECIO  
--  --    , S.TASA_EMISION  
--  --    , S.FECHA_VENCIMIENTO  
--  --    , S.PRECIO_COMPRA  
--  --    , S.TASA  
--  --    , S.TASA_COMPRA  
--  --    , sum(S.MONTO_VALOR_COMPRA) MONTO_VALOR_COMPRA  
--  --    , sum(CASE WHEN S.ID_MONEDA_CTA = DBO.FNT_DAMEIDMONEDA('$$') THEN ROUND(S.MONTO_MON_CTA,0)  
--  --        ELSE S.MONTO_MON_CTA  
--  --      END) as 'MONTO_MON_CTA'  
--  --    , S.ID_MONEDA_CTA  
--  --    , S.ID_MONEDA_NEMOTECNICO  
--  --    , S.SIMBOLO_MONEDA  
--  --    , sum(S.MONTO_MON_NEMOTECNICO) MONTO_MON_NEMOTECNICO  
--  --    , sum(S.MONTO_MON_ORIGEN) MONTO_MON_ORIGEN  
--  --    , S.ID_EMPRESA  
--  --    , S.ID_ARBOL_CLASE_INST  
--  --    , S.COD_INSTRUMENTO  
--  --    , S.DSC_ARBOL_CLASE_INST  
--  --    , S.PORCENTAJE_RAMA  
--  --    , S.PRECIO_PROMEDIO_COMPRA  
--  --    , SUM(S.CANTIDAD * S.PRECIO_PROMEDIO_COMPRA) as MONTO_PROMEDIO_COMPRA  
--  --    , S.DSC_PADRE_ARBOL_CLASE_INST  
--  --    , S.RENTABILIDAD  
--  --    , S.DIAS  
--  --    , S.COD_PRODUCTO  
--  --    , S.ID_PADRE_ARBOL_CLASE_INST  
--  --    , S.DURACION  
--  --    , M.DICIMALES_MOSTRAR  
--  --    , S.DSC_CLASIFICADOR_RIESGO  
--  --    , SF.ID_SUBFAMILIA AS ID_SUBFAMILIA  
--  --    , SF.COD_SUBFAMILIA AS COD_SUBFAMILIA  
--  --    , S.CODIGO  
--  --   FROM #TBLSALIDA S  
--  --    LEFT JOIN NEMOTECNICOS N ON (S.ID_NEMOTECNICO = N.ID_NEMOTECNICO)  
--  --    LEFT JOIN VIEW_EMISORES_ESPECIFICOS E ON (N.ID_EMISOR_ESPECIFICO = E.ID_EMISOR_ESPECIFICO)  
--  --    LEFT JOIN MONEDAS M ON (M.ID_MONEDA = N.ID_MONEDA)  
--  --    LEFT JOIN SUBFAMILIAS SF ON (N.ID_SUBFAMILIA=SF.ID_SUBFAMILIA)  
--  --   WHERE N.ID_MERCADO_TRANSACCION = ISNULL(@PID_MERCADO_TRANSACCION,N.ID_MERCADO_TRANSACCION)  
--  --    AND N.ID_MONEDA              = ISNULL(@PID_MONEDA,N.ID_MONEDA)  
--  --    AND E.ID_SECTOR              = ISNULL(@PID_SECTOR, E.ID_SECTOR)  
--  --    AND N.ID_NEMOTECNICO         = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)  
--  --   GROUP BY S.ID_CUENTA  
--  --    , S.FECHA_CIERRE  
--  --    , S.ID_NEMOTECNICO  
--  --    , S.NEMOTECNICO  
--  --    , S.EMISOR  
--  --    , S.COD_EMISOR  
--  --    , S.DSC_NEMOTECNICO  
--  --    , S.TASA_EMISION_2  
--  --    , S.PRECIO  
--  --    , S.TASA_EMISION  
--  --    , S.FECHA_VENCIMIENTO  
--  --    , S.PRECIO_COMPRA  
--  --    , S.TASA  
--  --    , S.TASA_COMPRA  
--  --    , S.ID_MONEDA_CTA  
--  --    , S.ID_MONEDA_NEMOTECNICO  
--  --    , S.SIMBOLO_MONEDA  
--  --    , S.ID_EMPRESA  
--  --    , S.ID_ARBOL_CLASE_INST  
--  --    , S.COD_INSTRUMENTO  
--  --    , S.DSC_ARBOL_CLASE_INST  
--  --    , S.PORCENTAJE_RAMA  
--  --    , S.PRECIO_PROMEDIO_COMPRA  
--  --    , S.DSC_PADRE_ARBOL_CLASE_INST  
--  --    , S.RENTABILIDAD  
--  --    , S.DIAS  
--  --    , S.COD_PRODUCTO  
--  --    , S.ID_PADRE_ARBOL_CLASE_INST  
--  --    , S.DURACION  
--  --    , M.DICIMALES_MOSTRAR  
--  --    , S.DSC_CLASIFICADOR_RIESGO  
--  --    , SF.ID_SUBFAMILIA  
--  --    , SF.COD_SUBFAMILIA  
--  --    , S.CODIGO 
--  --    , S.GARANTIAS
--  --                   ,S.PRESTAMOS
--  --                   ,S.SIMULTANEAS) TEMP  
--  --   ORDER BY ID_MONEDA_NEMOTECNICO,NEMOTECNICO;  
   
   
--   --select * from #TBLSALIDA
	   
--END
   IF @LCODIGO_ARBOL = 'FWD'
    BEGIN
    
    
         INSERT INTO #TBLSALIDA ( ORIGEN
                             , ID_CUENTA
                             , FECHA_CIERRE
                             , NUMERO_CONTRATO
                             , TIPO_FWD
                             , MONEDA_EMISION
                             , MODALIDAD
                             , UNIDAD_ACTIVO_SUBYACENTE
                             , DECIMALES_ACTIVO_SUBYACENTE
                             , VALOR_NOMINAL
                             , FECHA_INICIO
                             , FECHA_VENCIMIENTO
                             , PRECIO_PACTADO
                             , VALOR_PRECIO_PACTADO
                             , VALOR_MERCADO)
            SELECT 'FORWARDS'
                 , SD.ID_CUENTA
                 , SD.FECHA_CIERRE
                 , SD.NRO_CONTRATO NUMERO_CONTRATO
                 , CASE SD.FLG_TIPO_MOVIMIENTO WHEN 'I' THEN 'COMPRA'
                                                    ELSE 'VENTA'
                   END TIPO_FWD
                 ,(CASE WHEN SD.COD_MONEDA_PAGAR = '$$' THEN 'PESOS'
                                                        ELSE SD.COD_MONEDA_PAGAR
                   END) MONEDA_EMISION
                 , UPPER((CASE SD.FLG_TIPO_CUMPLIMIENTO WHEN 'E' THEN 'E. Fisica'
                                                        WHEN 'C' THEN 'COMPENSACION'
                                                        ELSE ''
                   END)) MODALIDAD
                 , UPPER((CASE WHEN SD.COD_MONEDA_RECIBIR = '$$' THEN 'PESOS'
                                                                 ELSE SD.COD_MONEDA_RECIBIR
                   END)) UNIDAD_ACTIVO_SUBYACENTE
                 , UPPER((CASE WHEN SD.COD_MONEDA_RECIBIR = '$$' THEN 0
                                                                 ELSE 2
                   END)) DECIMALES_ACTIVO_SUBYACENTE
                 , ROUND((CASE WHEN NOT SD.COD_MONEDA_RECIBIR = '$$'
                                    THEN SD.MONTO_MON_RECIBIR
                                    ELSE SD.MONTO_MON_PAGAR
                   END),1) VALOR_NOMINAL
                 , SD.FCH_OPERACION FECHA_INICIO
                 , SD.FCH_VENCIMIENTO AS FECHA_VENCIMIENTO
                 , ROUND(SD.PARIDAD,3) PRECIO_PACTADO
                 , ROUND((SD.PARIDAD * (CASE WHEN NOT SD.COD_MONEDA_RECIBIR = '$$'
                                                  THEN SD.MONTO_MON_RECIBIR
                       ELSE SD.MONTO_MON_PAGAR
                   END)),3)VALOR_PRECIO_PACTADO
                 , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SD.ID_CUENTA
                                                        , ROUND(SD.VMM,3)
                                                        , DBO.FNT_DAMEIDMONEDA('$$')
                                                        , @PID_MONEDA_SALIDA
                                                        , @PFECHA_CIERRE) VALOR_MERCADO
              FROM VIEW_SALDOS_DERIVADOS SD
             WHERE SD.ID_CUENTA       = @PID_CUENTA
               AND SD.FECHA_CIERRE    = @PFECHA_CIERRE
               AND SD.COD_INSTRUMENTO = 'FWD_NAC'
              order by SD.FCH_OPERACION

    END
   ELSE
   IF @LCODIGO_ARBOL ='VC'
    
		  BEGIN
		  
		  
		  	
		INSERT INTO #TBLSALIDA (ORIGEN,
								NEMOTECNICO,
								CANTIDAD,
								FECHA_CIERRE,
								FECHA_VENCIMIENTO,
								TASA,
								PRECIO_MEDIO,
								VALOR_INI,
								PRECIO_MEDIO_MERCADO,	
								VALOR_FIN,			
								PRIMA_A_PLAZO,
								PRIMA_ACUMULADA,		
								DIAS_EN_CURSO,		
								DIAS_OPERACION,		
								RESULTADO)			
		  SELECT 'Venta Corta' TIPO_OPERACION
			,  N.NEMOTECNICO
			, VC.CANTIDAD
			,VC.FECHA_DE_MVTO
			,  VC.FECHA_VENCIMIENTO
			, VC.TASA
			, VC.PRECIO_MEDIO
			, (VC.CANTIDAD * VC.PRECIO_MEDIO) AS VALOR_INI
			, VCD.PRECIO_MEDIO_MERCADO
			, (VC.CANTIDAD * VCD.PRECIO_MEDIO_MERCADO) AS VALOR_FIN
			, VC.PRIMA_A_PLAZO
			, VCD.PRIMA_ACUMULADA
			, VCD.DIAS_EN_CURSO
			, (VC.DIAS_OPERACION - VCD.DIAS_EN_CURSO) AS DIAS_OPERACION
			, ISNULL((VC.CANTIDAD * VC.PRECIO_MEDIO), 0) -
              ISNULL((VC.CANTIDAD * VCD.PRECIO_MEDIO_MERCADO), 0) - 
              ISNULL(VCD.PRIMA_ACUMULADA, 0) AS 'RESULTADO'  
			
			FROM VENTA_CORTA VC, NEMOTECNICOS N , VENTA_CORTA_DEVENGO VCD 
			WHERE N.ID_NEMOTECNICO = VC.ID_NEMOTECNICO
			AND @PFECHA_CIERRE BETWEEN VC.FECHA_DE_MVTO AND VC.FECHA_VENCIMIENTO
			AND VC.COD_ESTADO = 'C'
			AND VC.ID_CUENTA  = @PID_CUENTA
            AND VC.ID_VTA_CORTA = VCD.ID_VTA_CORTA 
            AND VC.FOLIO = VCD.FOLIO 
            AND VC.FECHA_DE_MVTO = VCD.FECHA_DE_MVTO 
            AND VCD.FECHA_DE_DEVENGO = @PFECHA_CIERRE
						
			ORDER BY N.NEMOTECNICO
    END
    ELSE 
    
    
----------------------------------------------------------------------------------------------------------------
    IF UPPER(@LDSC_PADRE) = 'RENTA FIJA INTERNACIONAL' OR UPPER(@LDSC_PADRE) = 'RENTA VARIABLE INTERNACIONAL'
      or (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'FONDOS DE INVERSI”N')
      or (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'FONDOS MUTUOS MIXTOS')
     BEGIN
     

                 INSERT INTO #TBLINT
                      ( ORIGEN
                      , MERCADO
                      , ID_ARBOL_CLASE_INST
                      , SIMBOLO_MONEDA
                      , COD_PRODUCTO
                      , COD_INSTRUMENTO
                      , ID_NEMOTECNICO
                      , NEMOTECNICO
                      , DSC_NEMOTECNICO
                      , EMISOR
                      , COD_EMISOR
                      , ID_MONEDA_NEMOTECNICO
                      , ID_MONEDA_CTA
                      , CANTIDAD
                      , PRECIO_COMPRA
                      , PRECIO_PROMEDIO_COMPRA
                      , MONTO_INVERTIDO
                      , PRECIO_ACTUAL
                      , VALOR_MERCADO
                      , UTILIDAD_PERDIDA
                      , MONTO_MON_CTA
                      , MONTO_MON_USD
                      , MONTO_MON_NEMOTECNICO)
                 SELECT 'VALORES'
					  , 'VALORES'
                      , @PID_ARBOL_CLASE
                      , s.MONEDA
                      , N.COD_PRODUCTO
                      , N.COD_INSTRUMENTO
                      , N.ID_NEMOTECNICO
                      , N.NEMOTECNICO
                      , N.DSC_NEMOTECNICO
                      , N.DSC_EMISOR_ESPECIFICO
                      , N.COD_EMISOR_ESPECIFICO
                      , N.ID_MONEDA
                      , C.ID_MONEDA
                      , S.CANTIDAD
                      , S.PRECIO_PROMEDIO_COMPRA
                      , S.PRECIO_PROMEDIO_COMPRA
                      , S.MONTO_INVERTIDO
                      , S.PRECIO_MERCADO
                      , S.VALOR_MERCADO_MON_ORIGEN
                      , VALOR_MERCADO_MON_ORIGEN - MONTO_INVERTIDO
                      , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                             , VALOR_MERCADO_MON_USD
                                                             , @LID_MONEDA_USD
                                                             , @PID_MONEDA_SALIDA
                                                             , @PFECHA_CIERRE)   MONTO
                      , S.VALOR_MERCADO_MON_USD
                      , S.VALOR_MERCADO_MON_ORIGEN
                   FROM SALDOS_ACTIVOS_INT  S
                      , (SELECT NE.ID_NEMOTECNICO
                              , NE.NEMOTECNICO
              , NE.DSC_NEMOTECNICO
                              , EE.DSC_EMISOR_ESPECIFICO
                              , EE.COD_EMISOR_ESPECIFICO
                              , INS.COD_PRODUCTO
                              , NE.ID_MONEDA
                              , NE.COD_INSTRUMENTO
                           FROM NEMOTECNICOS NE
                                LEFT OUTER JOIN EMISORES_ESPECIFICO EE ON EE.ID_EMISOR_ESPECIFICO = NE.ID_EMISOR_ESPECIFICO
                               , INSTRUMENTOS INS
                          WHERE INS.COD_INSTRUMENTO = NE.COD_INSTRUMENTO            ) N
                      , CUENTAS C
                  WHERE S.ORIGEN       = 'INV'
                    AND S.ID_CUENTA = @PID_CUENTA
                    AND S.FECHA_CIERRE = @PFECHA_CIERRE
                    AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO
                                               FROM REL_ACI_EMP_NEMOTECNICO
                                              WHERE ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE)
                    AND N.ID_NEMOTECNICO = S.ID_NEMOTECNICO
                    AND C.ID_CUENTA      = S.ID_CUENTA


                 INSERT INTO #TBLINT
                      ( ORIGEN
                      , MERCADO
                      , ID_ARBOL_CLASE_INST
                      , SIMBOLO_MONEDA
                      , COD_PRODUCTO
                      , COD_INSTRUMENTO
                      , ID_NEMOTECNICO
                      , NEMOTECNICO
                      , DSC_NEMOTECNICO
                      , EMISOR
                      , COD_EMISOR
                      , ID_MONEDA_NEMOTECNICO
                      , ID_MONEDA_CTA
                      , CANTIDAD
                      , PRECIO_COMPRA
                      , PRECIO_PROMEDIO_COMPRA
                      , MONTO_INVERTIDO
                      , PRECIO_ACTUAL
                      , VALOR_MERCADO
                      , UTILIDAD_PERDIDA
                      , MONTO_MON_CTA
                      , MONTO_MON_USD
                      , MONTO_MON_NEMOTECNICO)
                 SELECT 'PERSHING'
					  , 'PERSHING'
                      , @PID_ARBOL_CLASE
                      , s.MONEDA
                      , N.COD_PRODUCTO
                      , N.COD_INSTRUMENTO
                      , N.ID_NEMOTECNICO
                      , N.NEMOTECNICO
                      , N.DSC_NEMOTECNICO
                      , N.DSC_EMISOR_ESPECIFICO
                      , N.COD_EMISOR_ESPECIFICO
                      , N.ID_MONEDA
                      , C.ID_MONEDA
                      , S.CANTIDAD
                      , S.PRECIO_PROMEDIO_COMPRA
                      , S.PRECIO_PROMEDIO_COMPRA
                      , S.MONTO_INVERTIDO
                      , S.PRECIO_MERCADO
                      , S.VALOR_MERCADO_MON_ORIGEN
                      , VALOR_MERCADO_MON_ORIGEN - MONTO_INVERTIDO
                      , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                             , VALOR_MERCADO_MON_USD
                                                             , @LID_MONEDA_USD
                                                             , @PID_MONEDA_SALIDA
                                                             , @PFECHA_CIERRE)   MONTO
                      , S.VALOR_MERCADO_MON_USD
                      , S.VALOR_MERCADO_MON_ORIGEN
                   FROM SALDOS_ACTIVOS_INT  S
                      , (SELECT NE.ID_NEMOTECNICO
                              , NE.NEMOTECNICO
                              , NE.DSC_NEMOTECNICO
                              , EE.DSC_EMISOR_ESPECIFICO
                        , EE.COD_EMISOR_ESPECIFICO
                              , INS.COD_PRODUCTO
                              , NE.ID_MONEDA
                              , NE.COD_INSTRUMENTO
                           FROM NEMOTECNICOS NE
                                LEFT OUTER JOIN EMISORES_ESPECIFICO EE ON EE.ID_EMISOR_ESPECIFICO = NE.ID_EMISOR_ESPECIFICO
                               , INSTRUMENTOS INS
                          WHERE INS.COD_INSTRUMENTO = NE.COD_INSTRUMENTO
                         ) N
                      , CUENTAS C
                  WHERE S.ORIGEN       = 'PSH'
                    AND S.ID_CUENTA = @PID_CUENTA
                    AND S.FECHA_CIERRE = @PFECHA_CIERRE
                    AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO
                                               FROM REL_ACI_EMP_NEMOTECNICO
                                              WHERE ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE)
                    AND N.ID_NEMOTECNICO = S.ID_NEMOTECNICO
                    AND C.ID_CUENTA      = S.ID_CUENTA

        ----------------------------------------------------------------
        SET @LTOTAL_INT = 0
        SELECT @LTOTAL_INT = SUM(MONTO_MON_CTA)
          FROM #TBLINT
         WHERE ORIGEN = 'VALORES'
        UPDATE #TBLINT
           SET TOTAL_HOJA= ISNULL(@LTOTAL_INT,0)
         WHERE ORIGEN = 'VALORES'

        SET @LTOTAL_INT = 0
        SELECT @LTOTAL_INT = SUM(MONTO_MON_CTA)
          FROM #TBLINT
         WHERE ORIGEN = 'PERSHING'
        UPDATE #TBLINT
           SET TOTAL_HOJA = ISNULL(@LTOTAL_INT,0)
         WHERE ORIGEN = 'PERSHING'

        UPDATE #TBLINT
           SET PORCENTAJE_RAMA = CASE TOTAL_HOJA WHEN 0 THEN 0 ELSE ((MONTO_MON_CTA / TOTAL_HOJA) * 100) END
             , RENTABILIDAD = CASE (PRECIO_PROMEDIO_COMPRA * CANTIDAD) WHEN 0 THEN 0
                                       ELSE ((MONTO_MON_NEMOTECNICO/(PRECIO_PROMEDIO_COMPRA * CANTIDAD))-1)*100
                              END

         INSERT INTO #TBLSALIDA (ORIGEN
							 , MERCADO
                              , ID_CUENTA
                              , FECHA_CIERRE
                              , SIMBOLO_MONEDA
                              , COD_PRODUCTO
                              , COD_INSTRUMENTO
                              , ID_NEMOTECNICO
                              , NEMOTECNICO
                              , DSC_NEMOTECNICO
                              , EMISOR
                              , COD_EMISOR
                              , ID_MONEDA_NEMOTECNICO
                              , ID_MONEDA_CTA
                              , FECHA_VENCIMIENTO
                              , CANTIDAD
                              , PRECIO_COMPRA
                              , PRECIO_PROMEDIO_COMPRA
                              , PRECIO
                              , MONTO_PROMEDIO_COMPRA
                              , MONTO_MON_CTA
                              , MONTO_MON_ORIGEN
                              , MONTO_MON_NEMOTECNICO
                              , PORCENTAJE_RAMA
                              , RENTABILIDAD
                              , DURACION
                              , ID_PADRE_ARBOL_CLASE_INST
                              , DSC_PADRE_ARBOL_CLASE_INST
                              , ID_ARBOL_CLASE_INST
                              , DSC_ARBOL_CLASE_INST
                              , CODIGO
                              , DECIMALES_MOSTRAR
                              , MONTO_INVERTIDO
                              , VALOR_MERCADO
                              , UTILIDAD_PERDIDA
                              , MONTO_MON_USD
         )
                        SELECT ORIGEN
							 , MERCADO
                              , @PID_CUENTA
                              , @PFECHA_CIERRE
                              , SIMBOLO_MONEDA
                              , COD_PRODUCTO
                              , COD_INSTRUMENTO
                              , ID_NEMOTECNICO
                              , NEMOTECNICO
                              , DSC_NEMOTECNICO
                              , EMISOR
                              , COD_EMISOR
                              , ID_MONEDA_NEMOTECNICO
                              , ID_MONEDA_CTA
                              , FECHA_VENCIMIENTO
                              , CANTIDAD
                              , PRECIO_COMPRA
                              , PRECIO_PROMEDIO_COMPRA
                              , PRECIO_ACTUAL
                              , MONTO_PROMEDIO_COMPRA
                              , MONTO_MON_CTA
                              , MONTO_MON_USD
                              , MONTO_MON_NEMOTECNICO
                              , PORCENTAJE_RAMA
                              , RENTABILIDAD
                              , DURACION
                              , @LID_PADRE
                              , @LDSC_PADRE
                              , @PID_ARBOL_CLASE
                              , @LDSC_ARBOL
                              , @LCODIGO_ARBOL
                              , M.DICIMALES_MOSTRAR
                              , MONTO_INVERTIDO
                              , VALOR_MERCADO
                              , UTILIDAD_PERDIDA
                              , MONTO_MON_USD
           FROM #TBLINT T
              , MONEDAS M
          WHERE M.ID_MONEDA = T.ID_MONEDA_NEMOTECNICO

       INSERT INTO #TBLTEMP
            (ID_CUENTA
            , ID_SALDO_ACTIVO
            , FECHA_CIERRE
            , ID_NEMOTECNICO
            , NEMOTECNICO
            , EMISOR
            , COD_EMISOR
            , DSC_NEMOTECNICO
            , TASA_EMISION_2
            , CANTIDAD
            , DISPONIBLES
            , GARANTIAS
            , SIMULTANEAS
            , PRECIO
            , TASA_EMISION
            , FECHA_VENCIMIENTO
            , PRECIO_COMPRA
            , TASA
            , TASA_COMPRA
            , MONTO_VALOR_COMPRA
            , MONTO_MON_CTA
            , MONTO_MON_USD
            , ID_MONEDA_CTA
            , ID_MONEDA_NEMOTECNICO
            , SIMBOLO_MONEDA
            , MONTO_MON_NEMOTECNICO
            , MONTO_MON_ORIGEN
            , ID_EMPRESA
            , ID_ARBOL_CLASE_INST
            , COD_INSTRUMENTO
            , DSC_ARBOL_CLASE_INST
            , PORCENTAJE_RAMA
            , PRECIO_PROMEDIO_COMPRA
            , DSC_PADRE_ARBOL_CLASE_INST
            , RENTABILIDAD
            , DIAS
            , DURATION
            , I.COD_PRODUCTO
            , ID_PADRE_ARBOL_CLASE_INST
            , DURACION
            , DSC_CLASIFICADOR_RIESGO
            , CODIGO
            )
       SELECT SA.ID_CUENTA
            , SA.ID_SALDO_ACTIVO
            , SA.FECHA_CIERRE
            , SA.ID_NEMOTECNICO
            , N.NEMOTECNICO
            , ISNULL(EE.DSC_EMISOR_ESPECIFICO,'') AS EMISOR
            , EE.COD_SVS_NEMOTECNICO AS COD_EMISOR
            , N.DSC_NEMOTECNICO
            , ISNULL(TASA_EMISION,0)
            , SA.CANTIDAD AS CANTIDAD
            , SA.CANTIDAD -( ISNULL(GPS.GARANTIAS, 0)+ ISNULL(GPS.PRESTAMOS, 0)+ISNULL(GPS.SIMULTANEAS, 0)) AS DISPONIBLE
			, ISNULL(GPS.PRESTAMOS, 0) + ISNULL(GPS.GARANTIAS, 0) GARANTIAS
			, ISNULL(GPS.SIMULTANEAS, 0) SIMULTANEAS
            , SA.PRECIO PRECIO
            , N.TASA_EMISION
            , N.FECHA_VENCIMIENTO
            , ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, N.ID_NEMOTECNICO),0) AS PRECIO_COMPRA
            , TASA
            , CASE WHEN SA.COD_PRODUCTO = 'RF_NAC' THEN DBO.FNT_ENTREGA_TASA_PROMEDIO (SA.FECHA_CIERRE
                                                                                     , SA.ID_CUENTA
                                                                                     , SA.ID_NEMOTECNICO)
                   ELSE SA.TASA_COMPRA
              END AS TASA_COMPRA
            , SA.MONTO_VALOR_COMPRA AS MONTO_VALOR_COMPRA
            , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                   , MONTO_MON_CTA
                                 , ID_MONEDA_CTA
                                                   , @PID_MONEDA_SALIDA
                                                   , @PFECHA_CIERRE)   MONTO
            , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                   , MONTO_MON_CTA
                                                   , ID_MONEDA_CTA
                                                   , @LID_MONEDA_USD
                                                   , @PFECHA_CIERRE)   MONTO_MON_USD
            , ID_MONEDA_CTA
            , N.ID_MONEDA
            , MO.SIMBOLO AS SIMBOLO_MONEDA
            , MONTO_MON_NEMOTECNICO
            , MONTO_MON_ORIGEN
            , ACI.ID_EMPRESA
            , ACI.ID_ARBOL_CLASE_INST
            , N.COD_INSTRUMENTO
            , CI.DSC_ARBOL_CLASE_INST
            , DBO.PKG_GLOBAL$MONTO_MON_CUENTA_HOJA(@PID_ARBOL_CLASE
                                                 , SA.ID_CUENTA
                                                 , @PFECHA_CIERRE
                                                 , NULL
                                                 , NULL
                                                 , NULL) AS PORCENTAJE_RAMA
            , ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE
                                               , SA.ID_CUENTA
                                               , SA.ID_NEMOTECNICO),0)
            , (SELECT ACII.DSC_ARBOL_CLASE_INST
                 FROM ARBOL_CLASE_INSTRUMENTO ACII
                WHERE ACII.ID_ARBOL_CLASE_INST = CI.ID_PADRE_ARBOL_CLASE_INST) AS DSC_PADRE_ARBOL_CLASE_INST
            , 0
            , 0 AS DIAS
            , 0 AS DURATION
            , I.COD_PRODUCTO
            , CI.ID_PADRE_ARBOL_CLASE_INST
            , (SELECT TOP 1 P.DURACION FROM PUBLICADORES_PRECIOS P
                   WHERE P.ID_NEMOTECNICO = N.ID_NEMOTECNICO
                    AND P.FECHA <= @PFECHA_CIERRE
                    AND P.DURACION != 0
                   ORDER BY P.FECHA DESC)AS DURACION
            , (SELECT TOP 1 RN.COD_VALOR_CLASIFICACION
                 FROM REL_NEMOTECNICO_VALOR_CLASIFIC RN
                    , CLASIFICADORES_RIESGO C
                WHERE RN.ID_NEMOTECNICO = ISNULL(@PID_NEMOTECNICO, N.ID_NEMOTECNICO)
                  AND RN.ID_CLASIFICADOR_RIESGO = C.ID_CLASIFICADOR_RIESGO) AS DSC_CLASIFICADOR_RIESGO
            , CI.CODIGO
         FROM REL_ACI_EMP_NEMOTECNICO ACI
            , VIEW_SALDOS_ACTIVOS SA
             LEFT OUTER JOIN GAR_PRE_SIM GPS ON GPS.ID_CUENTA  = SA.ID_CUENTA   AND GPS.ID_NEMOTECNICO =    SA.ID_NEMOTECNICO AND GPS.FECHA_CIERRE = SA.FECHA_CIERRE
            , ARBOL_CLASE_INSTRUMENTO CI
            , MONEDAS MO
            , INSTRUMENTOS I
            , NEMOTECNICOS N
              LEFT OUTER JOIN EMISORES_ESPECIFICO EE ON EE.ID_EMISOR_ESPECIFICO = N.ID_EMISOR_ESPECIFICO
        WHERE N.ID_NEMOTECNICO          = ACI.ID_NEMOTECNICO
          AND SA.ID_NEMOTECNICO       = N.ID_NEMOTECNICO
          AND ACI.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
          AND SA.ID_CUENTA            = @pid_cuenta
          AND SA.FECHA_CIERRE         = @pfecha_cierre
          AND CI.ID_ARBOL_CLASE_INST  = ACI.ID_ARBOL_CLASE_INST
          AND MO.ID_MONEDA            = n.ID_MONEDA
          AND N.COD_INSTRUMENTO       = I.COD_INSTRUMENTO
          AND CI.ID_EMPRESA           = @LID_EMPRESA
          AND ACI.ID_EMPRESA          = CI.ID_EMPRESA
          AND N.ID_NEMOTECNICO        = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)


          IF (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'FONDOS DE INVERSI”N') OR
             (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'FONDOS MUTUOS MIXTOS')
             SET @LORIGEN='NACIONAL'
          ELSE
             SET @LORIGEN = 'VALORES'
    
            INSERT INTO #TBLSALIDA (ORIGEN
										, MERCADO
                                      , ID_CUENTA
                                      , FECHA_CIERRE
                                      , SIMBOLO_MONEDA
                                      , COD_PRODUCTO
                                      , COD_INSTRUMENTO
                                      , ID_NEMOTECNICO
                                      , NEMOTECNICO
                                      , DSC_NEMOTECNICO
                                      , ID_MONEDA_NEMOTECNICO
                                      , EMISOR
                                      , CANTIDAD
                                      , PRECIO
                                      , PRECIO_COMPRA
                                      , PRECIO_PROMEDIO_COMPRA
                                      , MONTO_VALOR_COMPRA
                                      , MONTO_MON_CTA
                                      , MONTO_MON_USD
                                      , MONTO_MON_NEMOTECNICO
                                      , PORCENTAJE_RAMA
                                      , MONTO_PROMEDIO_COMPRA
                                      , RENTABILIDAD
                                      , ID_PADRE_ARBOL_CLASE_INST
                                      , DSC_PADRE_ARBOL_CLASE_INST
                                      , ID_ARBOL_CLASE_INST
                                      , DSC_ARBOL_CLASE_INST
                                      , CODIGO
                                      , MONTO_INVERTIDO
                                      , VALOR_MERCADO
                                      , UTILIDAD_PERDIDA
                                      )
                               SELECT @LORIGEN
									,@LORIGEN
                                    , ID_CUENTA
                                    , FECHA_CIERRE
                                    , SIMBOLO_MONEDA
                                    , COD_PRODUCTO
                                    , COD_INSTRUMENTO
                                    , ID_NEMOTECNICO
                          , NEMOTECNICO
                                    , DSC_NEMOTECNICO
                                    , ID_MONEDA_NEMOTECNICO
                                    , EMISOR
                                    , CANTIDAD
                                    , PRECIO
                                    , PRECIO_COMPRA
                                    , PRECIO_PROMEDIO_COMPRA
                                    , MONTO_VALOR_COMPRA
                                    , MONTO_MON_CTA
                                    , MONTO_MON_USD
                                    , MONTO_MON_NEMOTECNICO
                                    , CASE ISNULL(PORCENTAJE_RAMA,0)
                                         WHEN 0 THEN 0
                                                ELSE (MONTO_MON_CTA / PORCENTAJE_RAMA) * 100
                                      END AS PORCENTAJE_RAMA
                                    , MONTO_PROMEDIO_COMPRA
                                    , CASE WHEN COD_PRODUCTO = 'RF_NAC' THEN 0
                                               ELSE CASE (MONTO_PROMEDIO_COMPRA) WHEN 0 THEN NULL
                                                    ELSE (((PRECIO * CANTIDAD)/(PRECIO_PROMEDIO_COMPRA * CANTIDAD))-1)*100
                                                    END
                                      END AS RENTABILIDAD
                                    , @LID_PADRE
                                    , @LDSC_PADRE
                                    , @PID_ARBOL_CLASE
                                    , @LDSC_ARBOL
                                    , @LCODIGO_ARBOL
                                    , MONTO_VALOR_COMPRA
                                    , MONTO_MON_NEMOTECNICO
                                    , MONTO_MON_NEMOTECNICO - (CANTIDAD * (ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, ID_CUENTA, ID_NEMOTECNICO),0)))
                             FROM (SELECT S.ID_CUENTA
                                        , S.FECHA_CIERRE
                                        , S.ID_NEMOTECNICO
                                        , S.NEMOTECNICO
                                        , S.EMISOR
                                        , S.COD_EMISOR
                                        , S.DSC_NEMOTECNICO
                                        , S.TASA_EMISION_2
                                        , sum(S.CANTIDAD) cantidad
                                        , S.PRECIO
                                        , S.TASA_EMISION
                                        , S.FECHA_VENCIMIENTO
                                        , S.PRECIO_COMPRA
                                        , S.TASA
                                        , S.TASA_COMPRA
                                        , sum(S.MONTO_VALOR_COMPRA) MONTO_VALOR_COMPRA
                                        , sum(CASE WHEN S.ID_MONEDA_CTA = DBO.FNT_DAMEIDMONEDA('$$') THEN ROUND(S.MONTO_MON_CTA,0)
                                            ELSE S.MONTO_MON_CTA
                                          END) as 'MONTO_MON_CTA'
                                        , SUM(MONTO_MON_USD) 'MONTO_MON_USD'
                                        , S.ID_MONEDA_CTA
                                , S.ID_MONEDA_NEMOTECNICO
                                        , S.SIMBOLO_MONEDA
                                        --, sum(S.MONTO_MON_NEMOTECNICO) MONTO_MON_NEMOTECNICO
                                        , sum(CASE CODIGO WHEN 'FM_OA' THEN  MONTO_VALOR_COMPRA ELSE MONTO_MON_NEMOTECNICO
                                               END  ) MONTO_MON_NEMOTECNICO
                                        , sum(S.MONTO_MON_ORIGEN) MONTO_MON_ORIGEN
                                        , S.ID_EMPRESA
                                        , S.ID_ARBOL_CLASE_INST
                                        , S.COD_INSTRUMENTO
                                        , S.DSC_ARBOL_CLASE_INST
                                        , S.PORCENTAJE_RAMA
                                        , S.PRECIO_PROMEDIO_COMPRA
                                        , SUM(S.CANTIDAD * S.PRECIO_PROMEDIO_COMPRA) as MONTO_PROMEDIO_COMPRA
                                        , S.DSC_PADRE_ARBOL_CLASE_INST
                                        , S.RENTABILIDAD
                                        , S.DIAS
                                        , S.COD_PRODUCTO
                                        , S.ID_PADRE_ARBOL_CLASE_INST
                                        , S.DURACION
                                        , M.DICIMALES_MOSTRAR
                                        , S.DSC_CLASIFICADOR_RIESGO
                                        , SF.ID_SUBFAMILIA AS ID_SUBFAMILIA
                                        , SF.COD_SUBFAMILIA AS COD_SUBFAMILIA
                                        , S.CODIGO
                                   FROM #TBLTEMP S
                                        LEFT JOIN NEMOTECNICOS N ON (S.ID_NEMOTECNICO = N.ID_NEMOTECNICO)
                                        LEFT JOIN VIEW_EMISORES_ESPECIFICOS E ON (N.ID_EMISOR_ESPECIFICO = E.ID_EMISOR_ESPECIFICO)
                                        LEFT JOIN MONEDAS M ON (M.ID_MONEDA = N.ID_MONEDA)
                                        LEFT JOIN SUBFAMILIAS SF ON (N.ID_SUBFAMILIA=SF.ID_SUBFAMILIA)
                                  WHERE N.ID_MERCADO_TRANSACCION = ISNULL(@PID_MERCADO_TRANSACCION,N.ID_MERCADO_TRANSACCION)
                                    AND N.ID_MONEDA              = ISNULL(@PID_MONEDA,N.ID_MONEDA)
                                    AND E.ID_SECTOR              = ISNULL(@PID_SECTOR, E.ID_SECTOR)
                                    AND N.ID_NEMOTECNICO         = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)
                                   GROUP BY S.ID_CUENTA
                                    , S.FECHA_CIERRE
                                    , S.ID_NEMOTECNICO
                                    , S.NEMOTECNICO
                                    , S.EMISOR
                                    , S.COD_EMISOR
                                    , S.DSC_NEMOTECNICO
                                    , S.TASA_EMISION_2
                                    , S.PRECIO
                                    , S.TASA_EMISION
                                    , S.FECHA_VENCIMIENTO
                                    , S.PRECIO_COMPRA
                                    , S.TASA
                                    , S.TASA_COMPRA
                                    , S.ID_MONEDA_CTA
                                    , S.ID_MONEDA_NEMOTECNICO
                                    , S.SIMBOLO_MONEDA
                                    , S.ID_EMPRESA
                                    , S.ID_ARBOL_CLASE_INST
                                    , S.COD_INSTRUMENTO
                                    , S.DSC_ARBOL_CLASE_INST
                                    , S.PORCENTAJE_RAMA
                                    , S.PRECIO_PROMEDIO_COMPRA
                                    , S.DSC_PADRE_ARBOL_CLASE_INST
                                    , S.RENTABILIDAD
                                    , S.DIAS
                                    , S.COD_PRODUCTO
                                    , S.ID_PADRE_ARBOL_CLASE_INST
                                    , S.DURACION
                                    , M.DICIMALES_MOSTRAR
                                    , S.DSC_CLASIFICADOR_RIESGO
                                    , SF.ID_SUBFAMILIA
                                    , SF.COD_SUBFAMILIA
                                    , S.CODIGO ) TEMP
   ORDER BY ID_MONEDA_NEMOTECNICO,NEMOTECNICO;


     END
----------------------------------------------------------------------------------------------------------------
    ELSE
          IF (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'NOTAS ESTRUCTURADAS')
           BEGIN
           
                INSERT INTO #TBLSALIDA(ORIGEN
                                     , ID_CUENTA
                                     , ID_MONEDA_CTA
                                     , FECHA_CIERRE
                                     , SIMBOLO_MONEDA
                                     , COD_PRODUCTO
                                     , COD_INSTRUMENTO
                                     , ID_NEMOTECNICO
                                     , ID_MONEDA_NEMOTECNICO
                                     , NEMOTECNICO
                                     , DSC_NEMOTECNICO
                                     , CANTIDAD
                                     , PRECIO_COMPRA
                                     , MONTO_INVERTIDO
                                     , PRECIO
                                     , VALOR_MERCADO
                                     , UTILIDAD_PERDIDA
                                     , MONTO_MON_CTA
                                     , MONTO_MON_USD
                                     , ID_PADRE_ARBOL_CLASE_INST
                                     , DSC_PADRE_ARBOL_CLASE_INST
                                     , ID_ARBOL_CLASE_INST
                                     , DSC_ARBOL_CLASE_INST
                                     , CODIGO
                                     )
                SELECT 'NOTAS_INT'
                     , S.ID_CUENTA
                     , C.ID_MONEDA
                     , S.FECHA_CIERRE
                     , 'USD'
                     , N.COD_PRODUCTO
                     , N.COD_INSTRUMENTO
                     , N.ID_NEMOTECNICO
                     , N.ID_MONEDA
                     , N.NEMOTECNICO
                     , N.DSC_NEMOTECNICO
                     , S.CANTIDAD
                     , S.PRECIO_PROMEDIO_COMPRA as PRECIO_COMPRA
                     , S.MONTO_INVERTIDO
                     , S.PRECIO_MERCADO
                     , S.VALOR_MERCADO_MON_ORIGEN
                     , VALOR_MERCADO_MON_USD - MONTO_INVERTIDO
                     , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                            , VALOR_MERCADO_MON_USD
                                                            , @LID_MONEDA_USD
                                                            , @PID_MONEDA_SALIDA
                                                            , @PFECHA_CIERRE)   MONTO
                     , S.VALOR_MERCADO_MON_USD
                     , @LID_PADRE
                     , @LDSC_PADRE
                     , @PID_ARBOL_CLASE
                     , @LDSC_ARBOL
                     , @LCODIGO_ARBOL
                  FROM SALDOS_ACTIVOS_INT  S
                     , (SELECT N.ID_NEMOTECNICO
                             , N.NEMOTECNICO
                             , N.DSC_NEMOTECNICO
                             , N.COD_INSTRUMENTO
                             , I.COD_PRODUCTO
                             , N.ID_MONEDA
                          FROM NEMOTECNICOS N
                               INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                     , CUENTAS C
                 WHERE S.ID_CUENTA = @PID_CUENTA
                   AND S.FECHA_CIERRE = @PFECHA_CIERRE
                   AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO
                                              FROM REL_ACI_EMP_NEMOTECNICO
                                             WHERE ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE)
                   AND N.ID_NEMOTECNICO = S.ID_NEMOTECNICO
                   AND C.ID_CUENTA      = S.ID_CUENTA

                INSERT INTO #TBLSALIDA(ORIGEN
                                     , ID_CUENTA
                                     , ID_MONEDA_CTA
                                     , FECHA_CIERRE
                                     , SIMBOLO_MONEDA
                                     , COD_PRODUCTO
                                     , COD_INSTRUMENTO
                                     , ID_NEMOTECNICO
                                     , ID_MONEDA_NEMOTECNICO
                                     , NEMOTECNICO
                                     , DSC_NEMOTECNICO
                                     , CANTIDAD
                                     , PRECIO_COMPRA
                                     , MONTO_INVERTIDO
                                     , PRECIO
                                     , VALOR_MERCADO
                                     , UTILIDAD_PERDIDA
                                     , MONTO_MON_CTA
                                     , MONTO_MON_USD
                                     , ID_PADRE_ARBOL_CLASE_INST
                                     , DSC_PADRE_ARBOL_CLASE_INST
                                     , ID_ARBOL_CLASE_INST
                                     , DSC_ARBOL_CLASE_INST
                                     , CODIGO                                     )
                SELECT 'NOTAS_NAC'
                     , S.ID_CUENTA
                     , S.ID_MONEDA_CTA
                     , S.FECHA_CIERRE
                     , M.SIMBOLO
                     , N.COD_PRODUCTO
                     , N.COD_INSTRUMENTO
                     , N.ID_NEMOTECNICO
                     , N.ID_MONEDA
                     , N.NEMOTECNICO
                     , N.DSC_NEMOTECNICO
                     , S.CANTIDAD
                     , ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, S.ID_CUENTA, S.ID_NEMOTECNICO),0)
                     , S.CANTIDAD * (ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, S.ID_CUENTA, S.ID_NEMOTECNICO),0))
                     , S.PRECIO
                     , S.CANTIDAD * S.PRECIO
                     , S.MONTO_MON_NEMOTECNICO - (S.CANTIDAD * (ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, S.ID_CUENTA, S.ID_NEMOTECNICO),0)))
                     , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                            , MONTO_MON_CTA
                                                            , ID_MONEDA_CTA
                                                            , @PID_MONEDA_SALIDA
                                                            , @PFECHA_CIERRE)
                     , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                            , S.MONTO_MON_NEMOTECNICO
                                                            , N.ID_MONEDA
                                                            , @LID_MONEDA_USD
                                                            , @PFECHA_CIERRE)
                     , @LID_PADRE
                     , @LDSC_PADRE
                     , @PID_ARBOL_CLASE
                     , @LDSC_ARBOL
                     , @LCODIGO_ARBOL
                  FROM (SELECT FECHA_CIERRE
                             , ID_CUENTA
                             , ID_MONEDA_CTA
                             , ID_NEMOTECNICO
                             , PRECIO
                             , SUM(CANTIDAD) CANTIDAD
                             , SUM(MONTO_MON_CTA) MONTO_MON_CTA
                             , SUM(MONTO_MON_NEMOTECNICO) MONTO_MON_NEMOTECNICO
                          FROM SALDOS_ACTIVOS
                    WHERE ID_CUENTA = @PID_CUENTA
                           AND FECHA_CIERRE = @PFECHA_CIERRE
                         GROUP BY FECHA_CIERRE
                                 ,ID_CUENTA
                                 ,ID_MONEDA_CTA
                                 ,ID_NEMOTECNICO
                                 ,PRECIO

                       ) S
                     , (SELECT N.ID_NEMOTECNICO
                             , N.NEMOTECNICO
                             , N.DSC_NEMOTECNICO
                             , N.COD_INSTRUMENTO
                             , N.ID_MONEDA
                             , I.COD_PRODUCTO
                          FROM NEMOTECNICOS N
                               INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                     , MONEDAS M
                 WHERE S.ID_CUENTA = @PID_CUENTA
                   AND S.FECHA_CIERRE = @PFECHA_CIERRE
                   AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO
                                              FROM REL_ACI_EMP_NEMOTECNICO
                                             WHERE ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE)
                   AND N.ID_NEMOTECNICO = S.ID_NEMOTECNICO
                   AND M.ID_MONEDA = N.ID_MONEDA



     END
----------------------------------------------------------------------------------------------------------------
    ELSE
     BEGIN
     
     
          INSERT INTO #TBLTEMP
              ( ORIGEN
              , ID_CUENTA
              , ID_SALDO_ACTIVO
              , FECHA_CIERRE
              , ID_NEMOTECNICO
              , NEMOTECNICO
              , EMISOR
              , COD_EMISOR
              , DSC_NEMOTECNICO
              , TASA_EMISION_2
              , CANTIDAD
              , DISPONIBLES
              , GARANTIAS
              , SIMULTANEAS
			  , PRECIO
              , TASA_EMISION
              , FECHA_VENCIMIENTO
              , PRECIO_COMPRA
              , TASA
              , TASA_COMPRA
              , MONTO_VALOR_COMPRA
              , MONTO_MON_CTA
              , ID_MONEDA_CTA
              , ID_MONEDA_NEMOTECNICO
              , SIMBOLO_MONEDA
              , MONTO_MON_NEMOTECNICO
              , MONTO_MON_ORIGEN
              , ID_EMPRESA
              , ID_ARBOL_CLASE_INST
              , COD_INSTRUMENTO
              , DSC_ARBOL_CLASE_INST
              , PORCENTAJE_RAMA
              , PRECIO_PROMEDIO_COMPRA
              , DSC_PADRE_ARBOL_CLASE_INST
              , RENTABILIDAD
              , DIAS
              , COD_PRODUCTO
              , ID_PADRE_ARBOL_CLASE_INST
              , DURACION
              , DSC_CLASIFICADOR_RIESGO
              , CODIGO   )
          SELECT 'NACIONAL'
               , SA.ID_CUENTA
               , SA.ID_SALDO_ACTIVO
               , SA.FECHA_CIERRE
               , SA.ID_NEMOTECNICO
               , N.NEMOTECNICO
               , ISNULL(EE.DSC_EMISOR_ESPECIFICO,'') AS EMISOR
               , EE.COD_SVS_NEMOTECNICO AS COD_EMISOR
               , N.DSC_NEMOTECNICO
               , ISNULL(TASA_EMISION,0)
               , SA.CANTIDAD AS CANTIDAD
               , SA.CANTIDAD - ( ISNULL(GPS.GARANTIAS, 0)+ ISNULL(GPS.PRESTAMOS, 0)+ISNULL(GPS.SIMULTANEAS, 0)) AS DISPONIBLES
			   , (ISNULL(GPS.PRESTAMOS, 0) + ISNULL(GPS.GARANTIAS, 0)) GARANTIAS
			   , ISNULL(GPS.SIMULTANEAS, 0) SIMULTANEAS
               , SA.PRECIO PRECIO
               , N.TASA_EMISION
               , N.FECHA_VENCIMIENTO
               , ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, N.ID_NEMOTECNICO),0) AS PRECIO_COMPRA
               , TASA
               , CASE WHEN SA.COD_PRODUCTO = 'RF_NAC' THEN DBO.FNT_ENTREGA_TASA_PROMEDIO (SA.FECHA_CIERRE
                                                                                        , SA.ID_CUENTA
                                                                                        , SA.ID_NEMOTECNICO)
                      ELSE SA.TASA_COMPRA
                 END AS TASA_COMPRA
               , SA.MONTO_VALOR_COMPRA AS MONTO_VALOR_COMPRA
               , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                      , MONTO_MON_CTA
                                                      , ID_MONEDA_CTA
                                                      , @PID_MONEDA_SALIDA
                                                      , @PFECHA_CIERRE)   MONTO
               , ID_MONEDA_CTA
               , N.ID_MONEDA
               , MO.SIMBOLO AS SIMBOLO_MONEDA
               , MONTO_MON_NEMOTECNICO
               , MONTO_MON_ORIGEN
               , ACI.ID_EMPRESA
               , ACI.ID_ARBOL_CLASE_INST
               , N.COD_INSTRUMENTO
               , CI.DSC_ARBOL_CLASE_INST
               , DBO.PKG_GLOBAL$MONTO_MON_CUENTA_HOJA(@PID_ARBOL_CLASE, SA.ID_CUENTA, @PFECHA_CIERRE,NULL,NULL,NULL) AS PORCENTAJE_RAMA
               , ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)
               , (SELECT ACII.DSC_ARBOL_CLASE_INST
                    FROM ARBOL_CLASE_INSTRUMENTO ACII
                   WHERE ACII.ID_ARBOL_CLASE_INST = CI.ID_PADRE_ARBOL_CLASE_INST) AS DSC_PADRE_ARBOL_CLASE_INST
               , 0
               , 0 AS DIAS
               , I.COD_PRODUCTO
               , CI.ID_PADRE_ARBOL_CLASE_INST
               , (SELECT TOP 1 P.DURACION
                    FROM PUBLICADORES_PRECIOS P
                   WHERE P.ID_NEMOTECNICO = N.ID_NEMOTECNICO
                     AND P.FECHA <= @PFECHA_CIERRE
                     AND P.DURACION != 0
                  ORDER BY P.FECHA DESC)AS DURACION
               , (SELECT TOP 1 RN.COD_VALOR_CLASIFICACION
                    FROM REL_NEMOTECNICO_VALOR_CLASIFIC RN
                       , CLASIFICADORES_RIESGO C
                   WHERE RN.ID_NEMOTECNICO = ISNULL(@PID_NEMOTECNICO, N.ID_NEMOTECNICO)
                     AND RN.ID_CLASIFICADOR_RIESGO = C.ID_CLASIFICADOR_RIESGO) AS DSC_CLASIFICADOR_RIESGO
               , CI.CODIGO
            FROM REL_ACI_EMP_NEMOTECNICO ACI
               , VIEW_SALDOS_ACTIVOS SA
               LEFT OUTER JOIN GAR_PRE_SIM GPS ON GPS.ID_CUENTA  = SA.ID_CUENTA   AND GPS.ID_NEMOTECNICO =    SA.ID_NEMOTECNICO AND GPS.FECHA_CIERRE = SA.FECHA_CIERRE
               , ARBOL_CLASE_INSTRUMENTO CI
               , MONEDAS MO
               , INSTRUMENTOS I
               , NEMOTECNICOS N
                 LEFT OUTER JOIN EMISORES_ESPECIFICO EE ON EE.ID_EMISOR_ESPECIFICO = N.ID_EMISOR_ESPECIFICO
           WHERE N.ID_NEMOTECNICO          = ACI.ID_NEMOTECNICO
             AND SA.ID_NEMOTECNICO       = N.ID_NEMOTECNICO
             AND ACI.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
             AND SA.ID_CUENTA            = @pid_cuenta
             AND SA.FECHA_CIERRE         = @pfecha_cierre
             AND CI.ID_ARBOL_CLASE_INST  = ACI.ID_ARBOL_CLASE_INST
             AND MO.ID_MONEDA            = n.ID_MONEDA
             AND N.COD_INSTRUMENTO       = I.COD_INSTRUMENTO
             AND CI.ID_EMPRESA           = @LID_EMPRESA
             AND ACI.ID_EMPRESA          = CI.ID_EMPRESA
             AND N.ID_NEMOTECNICO        = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)

         INSERT INTO #TBLSALIDA
              ( ORIGEN
              , MERCADO
              , ID_CUENTA
              , FECHA_CIERRE
              , ID_NEMOTECNICO
              , NEMOTECNICO
              , EMISOR
              , COD_EMISOR
              , DSC_NEMOTECNICO
              , TASA_EMISION_2
              , CANTIDAD
              , DISPONIBLES
              , GARANTIAS
              , SIMULTANEAS
              , PRECIO
              , TASA_EMISION
              , FECHA_VENCIMIENTO
              , PRECIO_COMPRA
              , TASA
              , TASA_COMPRA
              , MONTO_VALOR_COMPRA
              , MONTO_MON_CTA
              , ID_MONEDA_CTA
              , ID_MONEDA_NEMOTECNICO
              , SIMBOLO_MONEDA
              , MONTO_MON_NEMOTECNICO
              , MONTO_MON_ORIGEN
              , ID_EMPRESA
              , ID_ARBOL_CLASE_INST
              , COD_INSTRUMENTO
              , DSC_ARBOL_CLASE_INST
              , PORCENTAJE_RAMA
              , PRECIO_PROMEDIO_COMPRA
			, MONTO_PROMEDIO_COMPRA
            , DSC_PADRE_ARBOL_CLASE_INST
              , RENTABILIDAD
              , DIAS
              , COD_PRODUCTO
              , ID_PADRE_ARBOL_CLASE_INST
              , DURACION
              , DECIMALES_MOSTRAR
              , DSC_CLASIFICADOR_RIESGO
              , ID_SUBFAMILIA
              , COD_SUBFAMILIA
              , CODIGO   )
         SELECT 'NACIONAL'
			, 'NACIONAL'
              , ID_CUENTA
              , FECHA_CIERRE
              , ID_NEMOTECNICO
              , NEMOTECNICO
              , EMISOR
              , COD_EMISOR
              , DSC_NEMOTECNICO
              , TASA_EMISION_2
              , CANTIDAD
              , DISPONIBLES
              , GARANTIAS
              , SIMULTANEAS
              , PRECIO
              , TASA_EMISION
              , FECHA_VENCIMIENTO
              , PRECIO_COMPRA
              , TASA
              , TASA_COMPRA
              , MONTO_VALOR_COMPRA
              , MONTO_MON_CTA
              , ID_MONEDA_CTA
              , ID_MONEDA_NEMOTECNICO
              , SIMBOLO_MONEDA
              , CASE CODIGO when 'FFMM' THEN  MONTO_VALOR_COMPRA ELSE MONTO_MON_NEMOTECNICO
                END  MONTO_MON_NEMOTECNICO
              , MONTO_MON_ORIGEN
              , ID_EMPRESA
              , ID_ARBOL_CLASE_INST
              , COD_INSTRUMENTO
              , DSC_ARBOL_CLASE_INST
              , CASE ISNULL(PORCENTAJE_RAMA,0)
                      WHEN 0 THEN 0
                      ELSE (MONTO_MON_CTA / PORCENTAJE_RAMA) * 100
                END AS PORCENTAJE_RAMA
              , PRECIO_PROMEDIO_COMPRA
              , MONTO_PROMEDIO_COMPRA
              , DSC_PADRE_ARBOL_CLASE_INST
              --, CASE WHEN COD_PRODUCTO = 'RF_NAC'
              --               THEN 0
              --               ELSE CASE (MONTO_PROMEDIO_COMPRA)
              --                         WHEN 0 THEN NULL
              --                         ELSE ((MONTO_MON_NEMOTECNICO/(PRECIO_PROMEDIO_COMPRA * CANTIDAD))-1)*100
              --                    END
              --  END AS RENTABILIDAD
                    , CASE (PRECIO_PROMEDIO_COMPRA * CANTIDAD) WHEN 0 THEN 0
                                       ELSE ((MONTO_MON_NEMOTECNICO/(PRECIO_PROMEDIO_COMPRA * CANTIDAD))-1)*100
                              END  AS RENTABILIDAD

              , DIAS
              , COD_PRODUCTO
              , ID_PADRE_ARBOL_CLASE_INST
              , DURACION
              , DICIMALES_MOSTRAR
              , DSC_CLASIFICADOR_RIESGO
              , ID_SUBFAMILIA
              , COD_SUBFAMILIA
              , CODIGO
          FROM (SELECT S.ID_CUENTA
                     , S.FECHA_CIERRE
                     , S.ID_NEMOTECNICO
                     , S.NEMOTECNICO
                     , S.EMISOR
                     , S.COD_EMISOR
                     , S.DSC_NEMOTECNICO
                     , S.TASA_EMISION_2
					 , sum(S.CANTIDAD) cantidad
					 , SUM(S.DISPONIBLES) DISPONIBLES
					 , SUM(S.GARANTIAS) GARANTIAS
					 , SUM(S.SIMULTANEAS) SIMULTANEAS
                     , S.PRECIO
                     , S.TASA_EMISION
                     , S.FECHA_VENCIMIENTO
                     , S.PRECIO_COMPRA
                     , S.TASA
                     , S.TASA_COMPRA
                     , sum(S.MONTO_VALOR_COMPRA) MONTO_VALOR_COMPRA
                     , sum(CASE WHEN S.ID_MONEDA_CTA = DBO.FNT_DAMEIDMONEDA('$$')
                                     THEN ROUND(S.MONTO_MON_CTA,0)
                                     ELSE S.MONTO_MON_CTA
                           END) as 'MONTO_MON_CTA'
                     , S.ID_MONEDA_CTA
                     , S.ID_MONEDA_NEMOTECNICO
                     , S.SIMBOLO_MONEDA
                     , sum(S.MONTO_MON_NEMOTECNICO) MONTO_MON_NEMOTECNICO
                     , sum(S.MONTO_MON_ORIGEN) MONTO_MON_ORIGEN
                     , S.ID_EMPRESA
                     , S.ID_ARBOL_CLASE_INST
                     , S.COD_INSTRUMENTO
                     , S.DSC_ARBOL_CLASE_INST
                     , S.PORCENTAJE_RAMA
                     , S.PRECIO_PROMEDIO_COMPRA
                     , SUM(S.CANTIDAD * S.PRECIO_PROMEDIO_COMPRA) as MONTO_PROMEDIO_COMPRA
                     , S.DSC_PADRE_ARBOL_CLASE_INST
                     , S.RENTABILIDAD
                     , S.DIAS
                     , S.COD_PRODUCTO
                     , S.ID_PADRE_ARBOL_CLASE_INST
                     , S.DURACION
                     , M.DICIMALES_MOSTRAR
                     , S.DSC_CLASIFICADOR_RIESGO
                     , SF.ID_SUBFAMILIA AS ID_SUBFAMILIA
                     , SF.COD_SUBFAMILIA AS COD_SUBFAMILIA
                     , S.CODIGO
                FROM #TBLTEMP S
                     LEFT JOIN NEMOTECNICOS N ON (S.ID_NEMOTECNICO = N.ID_NEMOTECNICO)
                     LEFT JOIN VIEW_EMISORES_ESPECIFICOS E ON (N.ID_EMISOR_ESPECIFICO = E.ID_EMISOR_ESPECIFICO)
                     LEFT JOIN MONEDAS M ON (M.ID_MONEDA = N.ID_MONEDA)
                     LEFT JOIN SUBFAMILIAS SF ON (N.ID_SUBFAMILIA=SF.ID_SUBFAMILIA)
               WHERE N.ID_MERCADO_TRANSACCION = ISNULL(@PID_MERCADO_TRANSACCION,N.ID_MERCADO_TRANSACCION)
                 AND N.ID_MONEDA              = ISNULL(@PID_MONEDA,N.ID_MONEDA)
                 AND E.ID_SECTOR              = ISNULL(@PID_SECTOR, E.ID_SECTOR)
                 AND N.ID_NEMOTECNICO         = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)
              GROUP BY S.ID_CUENTA
                     , S.FECHA_CIERRE
                     , S.ID_NEMOTECNICO
                     , S.NEMOTECNICO
                     , S.EMISOR
                     , S.COD_EMISOR
                     , S.DSC_NEMOTECNICO
                     , S.TASA_EMISION_2
                     , S.PRECIO
                     , S.TASA_EMISION
                     , S.FECHA_VENCIMIENTO
                     , S.PRECIO_COMPRA
                     , S.TASA
                     , S.TASA_COMPRA
					 , S.ID_MONEDA_CTA
                     , S.ID_MONEDA_NEMOTECNICO
                     , S.SIMBOLO_MONEDA
                     , S.ID_EMPRESA
                     , S.ID_ARBOL_CLASE_INST
                     , S.COD_INSTRUMENTO
                     , S.DSC_ARBOL_CLASE_INST
                     , S.PORCENTAJE_RAMA
                     , S.PRECIO_PROMEDIO_COMPRA
                     , S.DSC_PADRE_ARBOL_CLASE_INST
                     , S.RENTABILIDAD
                     , S.DIAS
                     , S.COD_PRODUCTO
                     , S.ID_PADRE_ARBOL_CLASE_INST
                     , S.DURACION
                     , M.DICIMALES_MOSTRAR
                     , S.DSC_CLASIFICADOR_RIESGO
                     , SF.ID_SUBFAMILIA
                     , SF.COD_SUBFAMILIA
                     , S.CODIGO ) TEMP


    END
----------------------------------------------------------------------------------------------------------------

SET @LORIGEN = (SELECT top (1) ORIGEN FROM #TBLSALIDA)

--select @LCODIGO_ARBOL
----------------------------------------------------------------------------------------------------------------
IF @LORIGEN ='Venta Corta' OR  @LCODIGO_ARBOL ='VC'
BEGIN
	SELECT ORIGEN,
			NEMOTECNICO,
			CANTIDAD,
			FECHA_CIERRE,
			FECHA_VENCIMIENTO,
			TASA,
			PRECIO_MEDIO,
			VALOR_INI,
			PRECIO_MEDIO_MERCADO,	
			VALOR_FIN,			
			PRIMA_A_PLAZO,
			PRIMA_ACUMULADA,		
			DIAS_EN_CURSO,		
			DIAS_OPERACION,		
			RESULTADO
	  FROM #TBLSALIDA
         ORDER BY ORIGEN
                , ID_MONEDA_NEMOTECNICO
                , NEMOTECNICO
END
ELSE
BEGIN
	SELECT ORIGEN
			,MERCADO
              , ID_CUENTA
              , FECHA_CIERRE
              , ID_NEMOTECNICO
              , NEMOTECNICO
              , EMISOR
              , COD_EMISOR
              , DSC_NEMOTECNICO
              , TASA_EMISION_2
              , CANTIDAD
              , DISPONIBLES
              , GARANTIAS
              , SIMULTANEAS
              , PRECIO
              , TASA_EMISION
              , FECHA_VENCIMIENTO
              , PRECIO_COMPRA
              , TASA
              , TASA_COMPRA
              , MONTO_VALOR_COMPRA
              , MONTO_MON_CTA
              , ID_MONEDA_CTA
              , ID_MONEDA_NEMOTECNICO
              , SIMBOLO_MONEDA
              , MONTO_MON_NEMOTECNICO
              , MONTO_MON_ORIGEN
              , ID_EMPRESA
              , ID_ARBOL_CLASE_INST
              , COD_INSTRUMENTO
              , DSC_ARBOL_CLASE_INST
              , PORCENTAJE_RAMA
              , PRECIO_PROMEDIO_COMPRA
              , MONTO_PROMEDIO_COMPRA
              , DSC_PADRE_ARBOL_CLASE_INST
              , RENTABILIDAD
              , DIAS
              , COD_PRODUCTO
              , ID_PADRE_ARBOL_CLASE_INST
              , DURACION
              , DECIMALES_MOSTRAR
              , DSC_CLASIFICADOR_RIESGO
              , ID_SUBFAMILIA
              , COD_SUBFAMILIA
              , CODIGO
              , MONTO_INVERTIDO
              , VALOR_MERCADO
              , UTILIDAD_PERDIDA
              , NUMERO_CONTRATO
              , TIPO_FWD
              , MONEDA_EMISION
              , MODALIDAD
              , UNIDAD_ACTIVO_SUBYACENTE
              , DECIMALES_ACTIVO_SUBYACENTE
              , VALOR_NOMINAL
              , FECHA_INICIO
              , PRECIO_PACTADO
              , VALOR_PRECIO_PACTADO
              , MONTO_MON_USD
          FROM #TBLSALIDA
         ORDER BY ORIGEN
                , ID_MONEDA_NEMOTECNICO
                , NEMOTECNICO
END


    

    DROP TABLE #TBLINT
    DROP TABLE #TBLSALIDA
    DROP TABLE #TBLTEMP

    SET NOCOUNT OFF;
    DECLARE @db_null_statement_2 INT
END
GO
GRANT EXECUTE ON [PKG_SALDOS_ACTIVOS_WEB$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO_VR2] TO DB_EXECUTESP
GO