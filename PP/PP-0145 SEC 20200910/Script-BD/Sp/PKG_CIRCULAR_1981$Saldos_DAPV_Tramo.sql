USE CSGPI
GO
IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[PKG_CIRCULAR_1981$Saldos_DAPV_Tramo]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [DBO].[PKG_CIRCULAR_1981$Saldos_DAPV_Tramo]
GO

CREATE PROCEDURE  [dbo].[PKG_CIRCULAR_1981$Saldos_DAPV_Tramo]
( @pFecha_Desde    datetime,
 @pFecha_Hasta    datetime,
 @pId_Empresa     numeric,
 @pId_Cuenta      numeric =null
)
AS
BEGIN
  -- TRAE TODOS LOS SALDOS PARA COMPONER LA CIRCULAR.
  -- SE AGREGAR� CODIGO PARA IDENTIFICAR A QUE SECCION CORRESPONDE
  -- 1.- SALDOS DAPV
  -- 2.- SALDOS DC
 SET NOCOUNT ON

  DECLARE @LSEXO                   VARCHAR(1)
        , @LMONTO                  FLOAT
        , @LEDAD                   NUMERIC
        , @LID_TIPOCUENTA          NUMERIC

      DECLARE @SALIDA TABLE (SALDO_INI         FLOAT
                            ,SALDO_FIN         FLOAT
                            ,EDAD_A_20_M       NUMERIC
                            ,EDAD_A_20_F       NUMERIC
                            ,EDAD_20_25_M      NUMERIC
                            ,EDAD_20_25_F      NUMERIC
                            ,EDAD_25_30_M      NUMERIC
                            ,EDAD_25_30_F      NUMERIC
                            ,EDAD_30_35_M      NUMERIC
                            ,EDAD_30_35_F      NUMERIC
                            ,EDAD_35_40_M      NUMERIC
                            ,EDAD_35_40_F      NUMERIC
                            ,EDAD_40_45_M      NUMERIC
                            ,EDAD_40_45_F      NUMERIC
                            ,EDAD_45_50_M      NUMERIC
                            ,EDAD_45_50_F      NUMERIC
                            ,EDAD_50_55_M      NUMERIC
                            ,EDAD_50_55_F      NUMERIC
                            ,EDAD_55_60_M      NUMERIC
                            ,EDAD_55_60_F      NUMERIC
                            ,EDAD_60_65_M      NUMERIC
                            ,EDAD_60_65_F      NUMERIC
                            ,EDAD_65_70_M      NUMERIC
                            ,EDAD_65_70_F      NUMERIC
                            ,EDAD_M70_M        NUMERIC
                            ,EDAD_M70_F        NUMERIC
                            ,EDAD_SIN_M        NUMERIC
                            ,EDAD_SIN_F        NUMERIC )

      DECLARE @TMP_MONTOS TABLE (SECCION                VARCHAR(10)
                                ,RUT_CLIENTE            VARCHAR(15)
                                ,EDAD                   NUMERIC
                                ,GENERO                 VARCHAR(1)
                                ,MONTO                  FLOAT)


   INSERT INTO @SALIDA SELECT 0,5,         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 5,10,        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 10,20,       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 20,50,       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 50,100,      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 100,200,     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 200,500,     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 500,1000,    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 1000,2000,   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 2000,3000,   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 3000,4000,   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 4000,5000,   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 5000,7000,   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 7000,10000,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 10000,15000, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 15000,20000, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 20000,30000, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 30000,40000, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 40000,50000, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 50000,60000, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 60000,80000, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 80000,100000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   INSERT INTO @SALIDA SELECT 100000,9999999999999999999,     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0


      INSERT INTO @TMP_MONTOS
      SELECT 'DAPV'
           , CU.RUT_CLIENTE
           , ISNULL(datediff(year,CL.FECHA_NACIMIENTO,getdate()),0)
           , ISNULL(CL.SEXO,'M')
           , SUM(ISNULL(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                               P.PATRIMONIO_MON_CUENTA,
                                                               ID_MONEDA_CUENTA,
                                                               DBO.FNT_DAMEIDMONEDA('$$'),
                                                               P.FECHA_CIERRE),0)   ) / 1000
        FROM PATRIMONIO_CUENTAS P
           , VIEW_CUENTAS_VIGENTES CU
           , CLIENTES CL
       WHERE CU.ID_EMPRESA=@PID_EMPRESA
         AND CU.ID_TIPOCUENTA=@LID_TIPOCUENTA
         AND CU.TIPO_AHORRO  IN (SELECT ID_TIPO_AHORRO
                                   FROM TIPOS_AHORRO_APV
                                  WHERE DESCRIPCION_LARGA LIKE 'APV%'
                                     OR DESCRIPCION_LARGA LIKE 'COTIZA%')
         AND CL.ID_CLIENTE = CU.ID_CLIENTE
         AND P.ID_CUENTA   = CU.ID_CUENTA
         AND P.FECHA_CIERRE  = @PFECHA_HASTA
      GROUP BY CU.RUT_CLIENTE
           , ISNULL(datediff(year,CL.FECHA_NACIMIENTO,getdate()),0)
           , ISNULL(CL.SEXO,'M')

		---------------------------------------------
		-- NUEVAS POLIZAS
		---------------------------------------------
/*
		INSERT INTO @TMP_MONTOS
		SELECT 'DAPV'
		   , APV_RUT
		   , ISNULL(datediff(year, CONVERT(DATETIME,CONVERT(VARCHAR(8),CONVERT(NUMERIC,APV_FECHA_NACIMIENTO)))   ,@pFecha_Hasta),0)
		   ,CASE
				WHEN (APV_SEXO = 'N')   THEN 'M'
				WHEN (APV_SEXO IS NULL) THEN 'S/I'
				ELSE APV_SEXO
			END 'APV_SEXO'
		   , ISNULL(SUM(APV_VALOR),0) / 1000
		FROM GPISEC_GPICDS_2.SkClDMVentas.dbo.APV_INFORME
		WHERE APV_TIPO_REGISTRO = 'S'
		AND   APV_TIPO IN ('DAPV','DAPB','CTVL','CTVB')
		AND   APV_FECHA_MOVIMIENTO BETWEEN CONVERT(VARCHAR(8),@PFECHA_DESDE,112) AND CONVERT(VARCHAR(8),@PFECHA_HASTA,112)
		AND   APV_PLAN IN (SELECT DISTINCT C2.APV_PLAN FROM VIEW_CIRCULAR_1981_CODPLANES C2)
		GROUP BY APV_RUT
		   , ISNULL(datediff(year, CONVERT(DATETIME,CONVERT(VARCHAR(8),CONVERT(NUMERIC,APV_FECHA_NACIMIENTO)))   ,@pFecha_Hasta),0)
		   ,CASE
				WHEN (APV_SEXO = 'N')   THEN 'M'
				WHEN (APV_SEXO IS NULL) THEN 'S/I'
				ELSE APV_SEXO
			END
*/
      DECLARE
      LCURSOR CURSOR LOCAL
      FOR
       SELECT EDAD
            , GENERO
            , MONTO AS MONTO
       FROM @TMP_MONTOS
       ORDER BY MONTO, EDAD, GENERO

      OPEN LCURSOR
      FETCH NEXT FROM LCURSOR INTO @LEDAD, @LSEXO, @LMONTO

      WHILE  NOT(@@FETCH_STATUS = -1)
      BEGIN

          IF @LSEXO = 'M'
          BEGIN
             IF @LEDAD <=20
                UPDATE @SALIDA SET EDAD_A_20_M = EDAD_A_20_M + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >20 AND @LEDAD <= 25
                UPDATE @SALIDA SET EDAD_20_25_M = EDAD_20_25_M + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >25 AND @LEDAD <= 30
                UPDATE @SALIDA SET EDAD_25_30_M = EDAD_25_30_M + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >30 AND @LEDAD <= 35
                UPDATE @SALIDA SET EDAD_30_35_M = EDAD_30_35_M + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >35 AND @LEDAD <= 40
                UPDATE @SALIDA SET EDAD_35_40_M = EDAD_35_40_M + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >40 AND @LEDAD <= 45
                UPDATE @SALIDA SET EDAD_40_45_M = EDAD_40_45_M + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >45 AND @LEDAD <= 50
                UPDATE @SALIDA SET EDAD_45_50_M = EDAD_45_50_M + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >50 AND @LEDAD <= 55
                UPDATE @SALIDA SET EDAD_50_55_M = EDAD_50_55_M + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >55 AND @LEDAD <= 60
                UPDATE @SALIDA SET EDAD_55_60_M = EDAD_55_60_M + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >60 AND @LEDAD <= 65
                UPDATE @SALIDA SET EDAD_60_65_M = EDAD_60_65_M  + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >65 AND @LEDAD <= 70
                UPDATE @SALIDA SET EDAD_65_70_M = EDAD_65_70_M  + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >70
                UPDATE @SALIDA SET EDAD_M70_M = EDAD_M70_M + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
                UPDATE @SALIDA SET EDAD_SIN_M = EDAD_SIN_M + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
          END
          ELSE
          IF @LSEXO = 'F'
          BEGIN
             IF @LEDAD <=20
                UPDATE @SALIDA SET EDAD_A_20_F = EDAD_A_20_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >20 AND @LEDAD <= 25
                UPDATE @SALIDA SET EDAD_20_25_F = EDAD_20_25_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >25 AND @LEDAD <= 30
                UPDATE @SALIDA SET EDAD_25_30_F = EDAD_25_30_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >30 AND @LEDAD <= 35
                UPDATE @SALIDA SET EDAD_30_35_F = EDAD_30_35_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >35 AND @LEDAD <= 40
                UPDATE @SALIDA SET EDAD_35_40_F = EDAD_35_40_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >40 AND @LEDAD <= 45
                UPDATE @SALIDA SET EDAD_40_45_F = EDAD_40_45_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >45 AND @LEDAD <= 50
                UPDATE @SALIDA SET EDAD_45_50_F = EDAD_45_50_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >50 AND @LEDAD <= 55
                UPDATE @SALIDA SET EDAD_50_55_F = EDAD_50_55_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >55 AND @LEDAD <= 60
                UPDATE @SALIDA SET EDAD_55_60_F = EDAD_55_60_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >60 AND @LEDAD <= 65
                UPDATE @SALIDA SET EDAD_60_65_F = EDAD_60_65_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >65 AND @LEDAD <= 70
                UPDATE @SALIDA SET EDAD_65_70_F = EDAD_65_70_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
             IF @LEDAD >70
                UPDATE @SALIDA SET EDAD_M70_F = EDAD_M70_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
             ELSE
                UPDATE @SALIDA SET EDAD_SIN_F = EDAD_SIN_F + 1
                 WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
          END
          ELSE
             UPDATE @SALIDA SET EDAD_SIN_M = EDAD_SIN_M + 1
              WHERE @LMONTO BETWEEN SALDO_INI AND SALDO_FIN
          FETCH NEXT FROM LCURSOR INTO @LEDAD, @LSEXO, @LMONTO
      END

     SELECT * FROM @SALIDA

END
GO
GRANT EXECUTE ON [PKG_CIRCULAR_1981$Saldos_DAPV_Tramo] TO DB_EXECUTESP
GO
