﻿<%@ Page Language="vb" MasterPageFile="~/Login/Login.Master" AutoEventWireup="false"
    CodeBehind="Login.aspx.vb" Inherits="AplicacionWeb.Login1" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/Login/Login.Master" %>
<asp:Content ID="LoginUsuario" ContentPlaceHolderID="miLogin" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            localStorage.clear();
            var empresas
            var codigoparametro = 'RCGPIWEB'
            var parametroRecuperarContraseña
            
            function cargaEmpresas() {
                $.ajax({
                    url: "../Servicios/Empresas.asmx/ConsultaNombreEmpresas",
                    data: "{}",
                    datatype: "json",
                    type: "post",
                    contentType: "application/json; charset=utf-8",
                    complete: function (jsondata, stat) {
                        if (stat == "success") {
                            empresas = JSON.parse(jsondata.responseText).d;
                            $("#DDEmpresa").html('');
                            $.each(empresas, function (key, val) {
                                $("#DDEmpresa").append('<option value="' + val.ID_EMPRESA + '">' + val.DSC_EMPRESA + '</option>');
                            })
                            cambioColor();
                        }
                        else {
                            alert(JSON.parse(jsondata.responseText).Message);
                        }
                        $('.loading').hide();
                    }
                });
            };

            function cargaParametroContraseña() {
                $.ajax({
                    url: "../Servicios/Globales.asmx/ObtenerValorParametroLogin",
                    data: "{'codigo':'" + codigoparametro + "'}",
                    datatype: "json",
                    type: "post",
                    contentType: "application/json; charset=utf-8",
                    complete: function (jsondata, stat) {
                        if (stat == "success") {
                            parametroRecuperarContraseña = JSON.parse(jsondata.responseText).d;
                            console.log(parametroRecuperarContraseña);
                            if (parametroRecuperarContraseña == 1) {
                                $('#recContrasena').show();
                            }
                            else {
                                $('#recContrasena').hide();
                            }
                        } 
                        else {
                            alert(JSON.parse(jsondata.responseText).Message);
                        }
                        $('.loading').hide();
                    }
                });
            };

            function validarCredenciales() {
                $.ajax({
                    url: "../Servicios/Login.asmx/ValidarCredenciales",
                    data: "{'usuario':'" + $("#txtNombreUsuario").val() +
                        "', 'contraseña':'" + $("#txtContraseña").val() +
                        "', 'empresa':" + $("#DDEmpresa").val() + "}",
                    datatype: "json",
                    type: "post",
                    contentType: "application/json; charset=utf-8",
                    complete: function (jsondata, stat) {
                        if (stat == "success") {
                            var res = JSON.parse(jsondata.responseText).d;
                            if (res.mensaje == "") {
                                localStorage.setItem("sesionCerrada", "False");
                                window.location.href = '../Sistema/PreLoader.aspx';
                            } else if (res.respuesta == true && res.mensaje != "") {
                                $('#modalCambioPswUsuario').modal('show');
                                alertaLogin(1, res.mensaje);
                            } else if (res.respuesta == false && res.mensaje != "") {
                                alertaLogin(3, res.mensaje);
                            } else {
                                $("#mensaje").text(res.mensaje);
                                $("#alert").fadeIn('slow');
                            }
                        }
                        else {
                            alert(JSON.parse(jsondata.responseText).Message);
                        }
                        $('.loading').hide();
                    }
                });
            };

            function enviarContraseñaTemporal(email) {
                $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
                $.ajax({
                    url: "../Servicios/Login.asmx/enviarEmail",
                    data: "{'emailUsuario':'" + email + "'}",
                    datatype: "json",
                    type: "post",
                    contentType: "application/json; charset=utf-8",
                    complete: function (jsondata, stat) {
                        if (stat == "success") {
                            var mensaje = JSON.parse(jsondata.responseText).d;
                            if (mensaje.respuesta) {
                                alertaLogin(0, mensaje.mensaje);
                            } else {
                                alertaLogin(3, mensaje.mensaje);
                            }
                            $('#loaderProceso').modal('hide');
                            $('#modalRecContra').modal('hide');
                        }
                        else {
                            alert(JSON.parse(jsondata.responseText).Message);
                        }
                    }
                });
            };

            function cambiarContraseñaTemporal(contraseña) {
                var vDscUsuarioDes = $("#txtNombreUsuario").val();
                var vPassword = contraseña;
                $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
                $.ajax({
                    type: "post",
                    url: "../Servicios/Login.asmx/actualizaPwdTemporalUsuario",
                    data: "{'pDscUsuarioAnt':'" + vDscUsuarioDes +
                        "', 'pPassword':'" + vPassword + "'}",
                    datatype: "json",
                    contentType: "application/json; charset=utf-8",
                    complete: function (jsondata, stat) {
                        if (stat === "success") {
                            var res = JSON.parse(jsondata.responseText).d;
                            if (res.respuesta == true) {
                                alertaLogin(0, "Contraseña Modificada Correctamente");
                            } else if (res.respuesta == false && res.mensaje != "") {
                                alertaLogin(3, res.mensaje);
                            }
                        } else {
                            alertaLogin(3, "Ocurrió un error al grabar la operación");
                        }
                        $('#loaderProceso').modal('hide');
                        $('#modalCambioPswUsuario').modal('hide');
                    }
                });
            }
            function cambioColor() {
                var model = empresas;
                var empresa = $.grep(model, function (e) { return e.ID_EMPRESA == $('#DDEmpresa').val(); });
                $('#PanelSubtitulo').css('background-color', 'rgb(' + empresa[0].COLOR_EMPRESA + ')');
            }

            $('#DDEmpresa').change(function () {
                cambioColor();
            });

            $("#logIn").on('click', function () {
                validarCredenciales();
            })

            $("#RecPass").on('click', function () {
                //   $('#myModal').modal({ show: true, backdrop: true });
                $('#modalRecContra').modal('show');
            })

            $("#envioContraseña").on('click', function () {
                var email = $("#emailInput").val();
                enviarContraseñaTemporal(email);
                //$('#myModal').modal('hide');
            })

            $("#cambioDeContraseñaUsuario").on('click', function () {
                var contraseña = $("#contraseña").val();
                var contraseñaRep = $("#repetirContraseña").val();
                if (contraseña === contraseñaRep) {
                    cambiarContraseñaTemporal(contraseña);
                    
                }
                else
                    alertaLogin(3, "Las Contraseñas No Coinciden");
                //enviarContraseñaTemporal(email);
            })


            $(document).on('click', function (event) {
                if (!$(event.target).closest('#alert').length) {
                    $("#alert").hide();
                }
            });

            $('#txtContraseña').keypress(function (e) {
                var key = e.which;
                if (key == 13)  // the enter key code
                {
                    validarCredenciales();
                }
            });

            $("#modalRecContra").on("hidden.bs.modal", function (e) {
                console.log("Modal hidden");
                document.getElementById("emailInput").value = '';
            });

            $("#modalCambioPswUsuario").on("hidden.bs.modal", function (e) {
                console.log("Modal hidden");
                document.getElementById("contraseña").value = '';
                document.getElementById("repetirContraseña").value = '';
            });

            $("[data-hide]").on("click", function () {
                $(this).closest("." + $(this).attr("data-hide")).hide();
            });
            
            cargaEmpresas();
            cargaParametroContraseña();

        });
        function alertaLogin(tipo, mensaje) {
            var cl;
            var text;
            var icon;
            $('#alert').removeClass("alert-success");
            $('#alert').removeClass("alert-info");
            $('#alert').removeClass("alert-warning");
            $('#alert').removeClass("alert-danger");

            switch (tipo) {
                case 0:
                    cl = "alert-success";
                    text = "Éxito! ";
                    icon = "fa fa-check-circle";
                    break;
                case 1:
                    cl = "alert-info";
                    text = "Atención! ";
                    icon = "fa fa-info-circle";
                    break;
                case 2:
                    cl = "alert-warning";
                    text = "Alerta! ";
                    icon = "fa fa-exclamation-circle";
                    break;
                case 3:
                    cl = "alert-danger";
                    text = "Error! ";
                    icon = "fa fa-exclamation-triangle";
                    break;
                default:
                    cl = "alert-danger";
                    text = "Error! ";
                    icon = "fa fa-exclamation-triangle";
            }
            $('#alert').addClass(cl);
            $('#tipoAlerta').text(text);
            $('#mensaje').text(mensaje);
            $('#iconAlerta').removeClass();
            $('#iconAlerta').addClass(icon);
            $('#alert').fadeIn('slow');
            $('#alert').delay(3000).fadeOut('slow');
        }
    </script>
    <div id="alert" class="modal-body alert" role="alert" style="position: fixed; bottom: 5vh; right: 5vw; width: 90vw; max-width: 450px; display: none; z-index: 2000;">
        <button type="button" class="close" data-hide="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <i id='iconAlerta' class='fa' aria-hidden='true'></i><strong id="tipoAlerta"></strong>
        <div id="mensaje"></div>
    </div>
    <div class="modal fade" id="loaderProceso" role="dialog" style="z-index: 2000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Un momento...
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="idLoginUsuario">
        <div style="text-align: center; height: 25vh;">
            <h3 style="margin: auto; color: #A6A9B2; font-weight: bold; display: table-cell; vertical-align: middle; height: inherit; width: 100vw;">Gestor de Portafolios de Inversión</h3>
        </div>
        <div class="col-xs-12 col-sm-offset-3 col-sm-6 col-lg-offset-4 col-lg-4">
            <div class="panel panel-default">
                <div id="PanelSubtitulo" runat="server" clientidmode="Static" class="panel-heading" style="font-size: 12pt; color: #FFFFFF; text-align: center; font-weight: bold;">
                    Ingreso al Sistema
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="txtNombreUsuario" class="col-sm-4 control-label">Usuario</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="txtNombreUsuario" runat="server" clientidmode="Static" maxlength="50" placeholder="usuario">
                            </div>
                        </div>
                        <div class="form-group" >
                            <label for="txtContraseña" class="col-sm-4 control-label">Contraseña</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="txtContraseña" runat="server" clientidmode="Static" maxlength="50" placeholder="contraseña">
                                <span id="recContrasena" class="psw"  hidden><a id="RecPass" href="#">¿Olvidó Su Contraseña?</a></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="DDEmpresa" class="col-sm-4 control-label">Empresa</label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="DDEmpresa" runat="server" CssClass="form-control" ClientIDMode="Static">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 0;">
                            <div class="col-sm-offset-4 col-sm-8">
                                <a href="#" type="button" id="logIn" class="btn btn-default">Iniciar Sesión</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalRecContra">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="myModalContainer" class="modal-body">
                    <div id="modalRecuperarContraseña">
                        <div class="row form-group-sm">
                            <div class="col-xs-12">
                                <div class='table-responsive'>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="text-center">
                                                <h3><i class="fa fa-lock fa-4x"></i></h3>
                                                <h2 class="text-center">¿Olvidó Su Contraseña?</h2>
                                                <p>Puedes Restablecer Tu Contraseña Aquí</p>
                                                <div class="panel-body">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                                                <input id="emailInput" placeholder="Correo Electrónico" class="form-control" type="email" oninvalid="setCustomValidity('Por favor, introduce una dirección de correo electrónico válida!')" onchange="try{setCustomValidity('')}catch(e){}" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="btn btn-lg btn-danger btn-block" value="Enviar mi contraseña" type="submit" id="envioContraseña">
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalCambioPswUsuario">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="myModalContainer2" class="modal-body">
                    <div id="modalCambioPsw">
                        <div class="row form-group-sm">
                            <div class="col-xs-12">
                                <div class='table-responsive'>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="text-center">

                                                <div class="panel-body">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="min-width: 150px; text-align: left;">Nueva Contraseña</span>
                                                                <input id="contraseña" placeholder="Ingrese Su Contraseña" class="form-control" type="password" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="min-width: 150px; text-align: left;">Repetir Contraseña</span>
                                                                <input id="repetirContraseña" placeholder="Ingrese Nuevamente Su Contraseña" class="form-control" type="password" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="btn btn-lg btn-danger btn-block" value="Modificar Contraseña" type="submit" id="cambioDeContraseñaUsuario">
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
