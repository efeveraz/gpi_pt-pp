﻿
var urlVarOper = getUrlVars()["Oper"];
var urlVarAgrupadoPor = getUrlVars()["AgrupadoPor"];
var xmlData = "";

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#paginadorDetalleOperacion").outerHeight(true) - 94;

    if (ancho < 992) {
        $("#Lista").setGridWidth($("#encSubPantalla").width() - 2);
        $('#Lista').setGridHeight(220);
    } else {
        $("#Lista").setGridWidth($("#encSubPantalla").width() - 2);
        $('#Lista').jqGrid('setGridHeight', height);
    }

}

function initCarteraDetalleOperacion() {
    $("#idSubtituloPaginaText").text("Detalle Operación: " + urlVarOper);
    document.title = "Detalle Instrumento: " + urlVarOper;

    $("#btnBuscarCuentas").hide();
    $("#btnBuscarClientes").hide();
    $("#btnBuscarGrupos").hide();
    $("#btnLimpiarCuentas").hide();
    $("#btnLimpiarClientes").hide();
    $("#btnLimpiarGrupos").hide();

    if (urlVarAgrupadoPor == "#ListaPorCuenta") {
        $("#tabsClienteCuentaGrupo").hide();
        $('.nav-tabs a[href="#tabCuenta"]').tab('show');
    } else if (urlVarAgrupadoPor == "#ListaPorCliente") {
        $("#tabsClienteCuentaGrupo").hide();
        $('.nav-tabs a[href="#tabCliente"]').tab('show');
    } else if (urlVarAgrupadoPor == "#ListaPorGrupo") {
        $("#tabsClienteCuentaGrupo").hide();
        $('.nav-tabs a[href="#tabGrupo"]').tab('show');
    }
}
function eventHandlersCarteraDetalleOperacion() {

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $("#Lista").jqGrid({
        autowidth: false,
        shrinkToFit: false,
        loadonce: true,
        rownumbers: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID" },
        colNames: ["", "Nro. Oper. Detalle", "Nemotécnico", "Fecha Emisión", "Cantidad", "Monto Pago", "País", "Producto", "Detalle", "Emisor", "Fecha Vencimiento", "Precio", "Moneda Operación", "Mercado", "Instrumento", "Estado"],
        colModel: [
             { name: "ID", index: "ID", sortable: true, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
             { name: "ID_OPERACION", index: "ID_OPERACION", sortable: true, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
             { name: "NEMOTECNICO", index: "NEMOTECNICO", sortable: true, width: 160, sorttype: "text", editable: false, search: true, hidden: false },
             { name: "FECHA_EMISION", index: "FECHA_EMISION", sortable: true, width: 110, sorttype: "date", formatter: "date", align: "center", editable: false, search: true, hidden: false },
             { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 100, sorttype: "number", formatter: "number", align: "right", editable: false, search: true, hidden: false },
             { name: "MONTO_PAGO", index: "MONTO_PAGO", sortable: true, width: 100, sorttype: "number", formatter: "number", align: "right", editable: false, search: true, hidden: false },
             { name: "DSC_PAIS", index: "DSC_PAIS", sortable: true, width: 70, sorttype: "text", editable: false, search: true, hidden: false },
             { name: "DSC_PRODUCTO", index: "DSC_PRODUCTO", sortable: true, width: 180, sorttype: "text", editable: false, search: true, hidden: false },
             { name: "DSC_NEMOTECNICO", index: "DSC_NEMOTECNICO", sortable: true, width: 180, sorttype: "text", editable: false, search: true, hidden: false },
             { name: "DSC_EMISOR_ESPECIFICO", index: "DSC_EMISOR_ESPECIFICO", sortable: true, width: 180, sorttype: "text", editable: false, search: true, hidden: false },
             { name: "FECHA_VENCIMIENTO", index: "FECHA_VENCIMIENTO", sortable: true, width: 130, sorttype: "date", formatter: "date", align: "center", editable: false, search: true, hidden: false },
             { name: "PRECIO", index: "PRECIO", sortable: true, width: 110, sorttype: "number", formatter: "number", align: "right", editable: false, search: true, hidden: false },
             { name: "DSC_MONEDA", index: "DSC_MONEDA", sortable: true, width: 140, sorttype: "text", editable: false, search: true, hidden: false },
             { name: "DESC_MERCADO", index: "DESC_MERCADO", sortable: true, width: 80, sorttype: "text", editable: false, search: true, hidden: false },
             { name: "DSC_INTRUMENTO", index: "DSC_INTRUMENTO", sortable: true, width: 120, sorttype: "text", editable: false, search: true, hidden: false },
             { name: "DSC_ESTADO", index: "DSC_ESTADO", sortable: true, width: 70, sorttype: "text", editable: false, search: true, hidden: false }
        ],
        pager: "#paginadorDetalleOperacion", //.
        loadtext: "Cargando datos...",
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}', //Paging input control text format.
        rowNum: intGrillaFilasPagina, // PageSize.
        rowList: arrGrillaFilasHoja, //Variable PageSize DropDownList. 
        viewrecords: true, //Show the RecordCount in the pager.
        multiselect: false,
        //width: "100%",
        height: "100%",
        styleUI: 'Bootstrap',
        sortname: "ID", //Default SortColumn
        sortorder: "asc", //Default SortOrder.
        caption: "Detalle",
        regional: localeCorto,
        hidegrid: false
    });
    $("#Lista").jqGrid().bindKeys().scrollingRows = true
    $("#Lista").jqGrid('navGrid', '#paginadorDetalleOperacion', { add: false, edit: false, del: false, excel: true, search: false });
    $("#Lista").jqGrid('navButtonAdd', '#paginadorDetalleOperacion', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('Lista', 'DetalleOper', 'List. Detalle Operación', '');
        }
    });
}

function consultaOperacionDetalle() {
    var a = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "mm",
            billion: "b",
            trillion: "t"
        },
        ordinal: function (a) {
            var b = a % 10;
            return 1 === b || 3 === b ? "er" : 2 === b ? "do" : 7 === b || 0 === b ? "mo" : 8 === b ? "vo" : 9 === b ? "no" : "to"
        },
        currency: {
            symbol: "$"
        }
    };
    numeral.language('cl', a);
    numeral.language('cl');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $("#Lista").jqGrid('clearGridData');
    $.ajax({
        url: "CarteraDetalleOperacion.aspx/ConsultaOperacionDetalle",
        data: "{'Operacion':'" + urlVarOper + "'}",
        datatype: "json",
        type: "post",
        async: true,
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#Lista").setGridParam({ data: mydata });
                $("#Lista").trigger("reloadGrid");

                $('#txtFechaTransaccion').text(moment(mydata[0].FECHA_OPERACION).format('DD-MM-YYYY'));
                $('#txtOperacion').text(mydata[0].FLG_TIPO_MOVIMIENTO);
                $('#txtMontoTransaccion').text(numeral(mydata[0].MONTO_PAGO).format('0,000.00'));
                if (mydata[0].PORC_COMISION !== 0) {
                    document.getElementById('idlblComision').innerText = "Comisión (" + numeral(mydata[0].PORC_COMISION).format('0,000.00') + "%)";
                }
                $('#txtComision').text(numeral(mydata[0].COMISION).format('0,000.00'));
                $('#txtDerechos').text(numeral(mydata[0].DERECHOS).format('0,000.00'));
                $('#txtGastos').text(numeral(mydata[0].GASTOS).format('0,000.00'));
                $('#txtAfectosIva').text(numeral(mydata[0].COMISION + mydata[0].DERECHOS + mydata[0].GASTOS).format('0,000.00'));
                $('#txtIVA').text(numeral(mydata[0].IVA).format('0,000.00'));
                $('#txtTOTAL').text(numeral(mydata[0].MONTO_OPERACION).format('0,000.00'));
                $('#txtFechaLiquidacion').text(moment(mydata[0].FECHA_LIQUIDACION).format('DD-MM-YYYY'));

                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
            $(window).trigger('resize');
        }
    });
};

$(document).ready(function () {
    initCarteraDetalleOperacion();
    eventHandlersCarteraDetalleOperacion();
    consultaOperacionDetalle();
    resize();
});