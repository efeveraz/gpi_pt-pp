﻿var urlVarMercado = getUrlVars()["Mercado"];
var urlVarMoneda = getUrlVars()["Moneda"];
var urlVarSector = getUrlVars()["Sector"];
var urlVarSuitability = getUrlVars()["Suitability"];
var urlDescripcion = "";

var jsfechaConsulta;
var jsfechaConsultaHasta;
var jsfechaFormato;
var jsfechaFormatoHasta;

function resize() {
    var ancho = $(window).width();
    var height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#paginador").outerHeight(true) - 190;

    if (ancho < 768) {
        $('#ListaVencimientos').setGridHeight(180);
    } else {
        $('#ListaVencimientos').jqGrid('setGridHeight', height);
    }
}

function initConsultaVencimientos() {

    $("#idSubtituloPaginaText").text("Consulta de Vencimientos");
    document.title = "Consulta de Vencimientos";

    estadoBtnConsultar();

    if (!miInformacionCuenta) {
        $("#dtFechaConsulta").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
        $("#dtFechaConsultaHasta").datepicker('setDate', moment(new Date(localStorage.UltimaFechaCierre)).add(-1, 'months').toDate());
    } else {
        $("#dtFechaConsulta").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
        $("#dtFechaConsultaHasta").datepicker('setDate', moment($("#dtFechaConsulta").datepicker('getDate')).add(1, 'months').format('L'));
    }

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');

    if (typeof urlVarMercado !== "undefined") {
        var urlDescripcionMercado = getUrlVars()["DescripcionMercado"];
        document.getElementById("idSubtituloPagina").value = "Visualizar por producto - Mercado: " + urlDescripcionMercado;
        document.getElementById("idSubtituloPagina").innerHTML = "Visualizar por producto - Mercado: " + urlDescripcionMercado;
    } else {
        urlVarMercado = "-1";
    };

    if (typeof urlVarMoneda !== "undefined") {
        var urlDescripcionMoneda = getUrlVars()["DescripcionMoneda"];
        document.getElementById("idSubtituloPagina").value = "Visualizar por producto - Moneda: " + urlDescripcionMoneda;
        document.getElementById("idSubtituloPagina").innerHTML = "Visualizar por producto - Moneda: " + urlDescripcionMoneda;
    } else {
        urlVarMoneda = "-1";
    };

    if (typeof urlVarSector !== "undefined") {
        var urlDescripcionSector = getUrlVars()["DescripcionSector"];
        document.getElementById("idSubtituloPagina").value = "Visualizar por producto - Sector: " + urlDescripcionSector;
        document.getElementById("idSubtituloPagina").innerHTML = "Visualizar por producto - Sector: " + urlDescripcionSector;
    } else {
        urlVarSector = "-1";
    };
    if (typeof urlVarSuitability !== "undefined") {
        var urlDescripcionSuitability = getUrlVars()["DescripcionSuitability"];
        document.getElementById("idSubtituloPagina").value = "Visualizar por producto - Suitability: " + urlDescripcionSuitability;
        document.getElementById("idSubtituloPagina").innerHTML = "Visualizar por producto - Suitability: " + urlDescripcionSuitability;
    } else {
        urlVarSuitability = "-1";
    };
}
function cargarEventHandlersConsultaVencimientos() {

    $(window).bind('resize', function () {
        resize()
    }).trigger('resize');

    $("#dtRangoFecha").datepicker();

    $("#ListaVencimientos").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        scrollOffset: 1,
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID" },
        colModel: [
            { name: "ID", index: "ID", sortable: true, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "FECHA_VENCIMIENTO", index: "FECHA_VENCIMIENTO", sortable: true, width: 110, sorttype: "date", formatter: "date", editable: false, search: true, align: "center", searchoptions: { sopt: ["ge", "le", "eq"], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "MONEDA_OPERACION", index: "MONEDA_OPERACION", width: 80, sortable: true, sorttype: "text", editable: false, search: true, align: "center" },
            { name: "DSC_PRODUCTO", index: "DSC_PRODUCTO", sortable: true, width: 120, sorttype: "text", editable: false, search: true },
            { name: "DSC_INSTRUMENTO", index: "DSC_INSTRUMENTO", sortable: true, width: 120, sorttype: "text", editable: false, search: true },
            { name: "NRO_OPERACION", index: "NRO_OPERACION", width: 70, sortable: true, sorttype: "int", editable: false, search: true, align: "right" },
            { name: "NEMOTECNICO", index: "NEMOTECNICO", sortable: true, width: 120, sorttype: "text", editable: false, search: true },
            { name: "TIPO", index: "TIPO", sortable: true, width: 80, sorttype: "text", editable: false, search: true },
            { name: "FECHA_OPERACION", index: "FECHA_OPERACION", width: 100, sortable: true, sorttype: "date", formatter: "date", editable: false, search: true, align: "center", searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "TASA", index: "TASA", sortable: true, width: 80, sorttype: "int", formatter: "number", editable: false, search: true, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONEDA_ORIGEN", index: "MONEDA_ORIGEN", width: 70, sortable: true, sorttype: "text", editable: false, search: true, align: "center" },
            { name: "MONTO_PAGO", index: "MONTO_PAGO", width: 120, sortable: true, sorttype: "int", formatter: "number", align: "right", editable: false, search: true, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_PAGO_PESO", index: "MONTO_PAGO_PESO", width: 120, sortable: true, sorttype: "int", formatter: "number", editable: false, search: true, align: "right", summaryType: 'sum', searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "TP", index: "TP", sortable: true, width: 70, sorttype: "text", editable: false, search: true, hidden: true },
            { name: "ID_CUENTA", index: "ID_CUENTA", width: 70, sortable: true, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "ID_MONEDA_TRANSACCION", index: "ID_MONEDA_TRANSACCION", width: 80, sortable: true, sorttype: "text", editable: false, search: true, align: "right", hidden: true },
            { name: "ID_MONEDA_NEMOTECNICO", index: "ID_MONEDA_NEMOTECNICO", width: 80, sortable: true, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "RUT_CLIENTE", index: "RUT_CLIENTE", width: 80, sortable: true, sorttype: "text", editable: false, search: true, hidden: true },
            { name: "NOMBRE_CLIENTE", index: "NOMBRE_CLIENTE", width: 80, sortable: true, sorttype: "text", editable: false, search: true, hidden: true },
            { name: "NOMBRE_ASESOR", index: "NOMBRE_ASESOR", width: 80, sortable: true, sorttype: "text", editable: false, search: true, hidden: true }
        ],
        pager: "#paginador",
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        sortname: "FECHA_VENCIMIENTO",
        sortorder: "asc",
        caption: "Vencimientos",
        ignoreCase: true,
        hidegrid: false,
        grouping: false,
        footerrow: true, userDataOnFooter: true,
        regional: localeCorto,
        loadComplete: function () {
            NombredeColumnas();
        },
        groupingView: {
            groupField: ['TP'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        },
        gridComplete: function () {
            var ids = $("#ListaVencimientos").jqGrid("getDataIDs"), l = ids.length, i, rowid, status;
            var rowid = "";
            var sum = $("#ListaVencimientos").jqGrid('getCol', 'MONTO_PAGO_PESO', false, 'sum');
            $("#ListaVencimientos").jqGrid('footerData', 'set', { FECHA_VENCIMIENTO: 'Total:', MONTO_PAGO_PESO: sum });
        }
    });
    $("#ListaVencimientos").jqGrid().bindKeys().scrollingRows = true
    $("#ListaVencimientos").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaVencimientos").jqGrid('navGrid', '#paginador', { add: false, edit: false, del: false, excel: true, search: false });

    $("#ListaVencimientos").jqGrid('navButtonAdd', '#paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaVencimientos', 'ListadoVencimientos', 'Vencimientos', '');
        }
    });

    $("#BtnConsultar").button().click(function () {
        consultarVencimientos("N");
    });

    $("#porCuenta_NroCuenta").on('change', function () {
        estadoBtnConsultar();
    });
}

function NombredeColumnas() {
    $('#ListaVencimientos').jqGrid('setLabel', "FECHA_VENCIMIENTO", "Fecha</br>Vencimiento");
    $('#ListaVencimientos').jqGrid('setLabel', "MONEDA_OPERACION", "Moneda</br>Operación");
    $('#ListaVencimientos').jqGrid('setLabel', "DSC_PRODUCTO", "Producto");
    $('#ListaVencimientos').jqGrid('setLabel', "DSC_INSTRUMENTO", "Instrumento");
    $('#ListaVencimientos').jqGrid('setLabel', "NRO_OPERACION", "Nro.</br>Operación");
    $('#ListaVencimientos').jqGrid('setLabel', "NEMOTECNICO", "Nemotécnico");
    $('#ListaVencimientos').jqGrid('setLabel', "TIPO", "Tipo");
    $('#ListaVencimientos').jqGrid('setLabel', "FECHA_OPERACION", "Fecha</br>Operación");
    $('#ListaVencimientos').jqGrid('setLabel', "TASA", "Tasa</br>Pactada");
    $('#ListaVencimientos').jqGrid('setLabel', "MONEDA_ORIGEN", "Moneda</br>Origen");
    $('#ListaVencimientos').jqGrid('setLabel', "MONTO_PAGO", "Monto al Vcto</br>Moneda Origen");
    $('#ListaVencimientos').jqGrid('setLabel', "MONTO_PAGO_PESO", "Monto al Vcto</br>en $");
}
function consultarVencimientos(strCargaInicial) {
    jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaFormatoHasta = moment.utc($("#dtFechaConsultaHasta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1)
    jsfechaConsultaHasta = jsfechaFormatoHasta.substring(0, jsfechaFormatoHasta.length - 1)
    if (miInformacionCuenta == null) {
        return;
    }
    if (jsfechaConsulta === "") {
        if (strCargaInicial == "N") {
            alert("No ha indicado fecha de consulta");
        }
        return;
    }
    $("#ListaVencimientos").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaVencimientos.aspx/ConsultarVencimientos",
        data: "{'dfechaConsulta':'" + jsfechaConsulta +
        "','dfechaConsultaHasta':'" + jsfechaConsultaHasta +
        "', 'intNroCuenta':'" + miInformacionCuenta.IdCuenta + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#ListaVencimientos").setGridParam({ data: mydata });
                $("#ListaVencimientos").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function estadoBtnConsultar() {
    if (miInformacionCuenta == null) {
        $('#BtnConsultar').tooltip('enable').addClass('disabled');
    } else {
        $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
    }
}

$(document).ready(function () {
    initConsultaVencimientos();
    cargarEventHandlersConsultaVencimientos();
    consultarVencimientos("S");
    $(window).trigger('resize');
});