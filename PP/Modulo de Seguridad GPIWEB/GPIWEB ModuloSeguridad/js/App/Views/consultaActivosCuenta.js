﻿var jsfechaConsulta;

function resize() {
    var ancho = $(window).width();
    var height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#PaginadorDetalle").outerHeight(true) - 214;
    var heightTab2 = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true);
    if (ancho < 768) {
        $("#ListaActivosCuenta").setGridHeight(300);
    } else {
        $("#ListaActivosCuenta").setGridHeight(height);
    }
}

function initConsultaActivosCuenta() {
    $("#idSubtituloPaginaText").text("Consulta Activos Cuenta");
    document.title = "Consulta Activos Cuenta";
    $('#tabDetalle').hide();

    estadoBtnConsultar();

    if (!miInformacionCuenta) {
        $(".input-group.date").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
    } else {
        $(".input-group.date").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
    }

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');
}
function cargarEventHandlersConsultaActivosCuenta() {
    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $(".input-group.date").datepicker();

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(window).trigger('resize');
    });

    $("#ListaActivosCuenta").jqGrid({
        shrinkToFit: false,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID_SALDO_ACTIVO" },
        colNames: ['ID_SALDO_ACTIVO', 'Instrumento', 'Nemotécnico', 'Cantidad', 'Precio Mercado', 'Precio Promedio Compra', 'Moneda', 'Duración', 'Valor Mercado', 'Valor Mercado Mon. Cuenta'],
        colModel: [
            { name: "ID_SALDO_ACTIVO", index: "ID_SALDO_ACTIVO", sortable: false, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "DSC_INTRUMENTO", index: "DSC_INTRUMENTO", sortable: true, width: 70, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NEMOTECNICO", index: "NEMOTECNICO", sortable: true, width: 190, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 110, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "PRECIO", index: "PRECIO", sortable: true, width: 130, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "PRECIO_COMPRA", index: "PRECIO_COMPRA", sortable: true, width: 180, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "DSC_MONEDA", index: "DSC_MONEDA", sortable: true, width: 110, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "DURACION", index: "DURACION", sortable: true, width: 110, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "MONTO_MON_NEMOTECNICO", index: "MONTO_MON_NEMOTECNICO", sortable: true, width: 110, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_MON_CTA", index: "MONTO_MON_CTA", sortable: true, width: 200, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] }, summaryType: 'sum' }
        ],
        pager: '#PaginadorDetalle',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        height: 'auto',
        styleUI: 'Bootstrap',
        sortname: "DSC_INTRUMENTO",
        sortorder: "asc",
        caption: "Activos Cuenta",
        grouping: true,
        footerrow: true,
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        loadComplete: function () {
            var $self = $(this),
                sum = $self.jqGrid("getCol", "MONTO_MON_CTA", false, "sum");

            $self.jqGrid("footerData", "set", { DSC_MONEDA: "Total:", MONTO_MON_CTA: sum });
        },
        groupingView: {
            groupField: ['DSC_INTRUMENTO'],
            groupText: ['<b>{0}</b>'],
            groupColumnShow: [false],
            groupSummary: [true],
            showSummaryOnHide: true
        },
        gridComplete: function () {
            var ids = $("#ListaActivosCuenta").jqGrid("getDataIDs"), l = ids.length, i, rowid, status;
            var rowid = "";

            $("#ListaActivosCuenta").jqGrid("sortGrid", "DSC_INTRUMENTO", true);
            var $self = $(this),
                sum = $self.jqGrid("getCol", "MONTO_MON_CTA", false, "sum");

            $self.jqGrid("footerData", "set", { DSC_MONEDA: "Total:", MONTO_MON_CTA: sum });
        }
    });
    $("#ListaActivosCuenta").jqGrid().bindKeys().scrollingRows = true;
    $("#ListaActivosCuenta").jqGrid('navGrid', '#PaginadorDetalle', { add: false, edit: false, del: false, excel: false, search: false });

    $("#ListaActivosCuenta").jqGrid('navButtonAdd', '#PaginadorDetalle', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaActivosCuenta', 'Activos Cuenta', 'Activos Cuenta', '');
        }
    });
    $('#PaginadorDetalle_left').width('auto');

    $("#BtnConsultar").button().click(function () {
        $('#txtAsesor').text('');
        $('#txtMoneda').text('');
        $('#txtPatrimonio').text('');
        $('#txtRentabilidad').text('');
        $('#divCajasCuenta').html('');
        consultaActivosPorProducto();
        $('#tabDetalle').show();
    });

    $("#porCuenta_NroCuenta").change(function () {
        estadoBtnConsultar();
    });
}

function consultaActivosPorProducto() {
    jsfechaConsulta = document.getElementById("dtFechaConsulta").value;

    if (!miInformacionCuenta) {
        return;
    }

    $("#ListaActivosCuenta").jqGrid('clearGridData');

    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }

    $("#ListaActivosCuenta").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    var jsfechaFormato = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();

    $.ajax({
        url: "ConsultaActivosCuenta.aspx/ConsultaActivosCuenta",
        data: "{'fechaConsulta':'" + jsfechaFormato.substring(0, jsfechaFormato.length - 1) +
        "', 'idCuenta':" + (!miInformacionCuenta ? "''" : miInformacionCuenta.IdCuenta) + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#ListaActivosCuenta").setGridParam({ data: mydata });
                $("#ListaActivosCuenta").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
    $.ajax({
        url: "ConsultaActivosCuenta.aspx/PatrimonioCuenta",
        data: "{'fechaConsulta':'" + jsfechaFormato.substring(0, jsfechaFormato.length - 1) +
        "', 'idCuenta':" + (!miInformacionCuenta ? "''" : miInformacionCuenta.IdCuenta) + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                if (mydata.length > 0) {
                    $('#txtAsesor').text(mydata[0].NOMBRE_ASESOR);
                    $('#txtMoneda').text(mydata[0].DSC_MONEDA);
                    $('#txtPatrimonio').text(numeral(mydata[0].PATRIMONIO_MON_CUENTA).format('0,000.00'));
                    $('#txtRentabilidad').text(numeral(mydata[0].RENTABILIDAD_MON_CUENTA).format('0,000.0000') + "%");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
    $.ajax({
        url: "ConsultaActivosCuenta.aspx/ConsultaSaldoCajas",
        data: "{'fechaConsulta':'" + jsfechaFormato.substring(0, jsfechaFormato.length - 1) +
        "', 'idCuenta':" + (!miInformacionCuenta ? "''" : miInformacionCuenta.IdCuenta) + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var mydata = JSON.parse(jsondata.responseText).d;

                var htmlFieldset = "";
                $.each(mydata, function (key, val) {
                    htmlFieldset += agregaHtml(key, val);
                });
                $('#divCajasCuenta').append(htmlFieldset);
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
    $('#loaderProceso').modal('hide');
    $(window).trigger('resize');
}

function agregaHtml(key, val) {
    var html =
        "<fieldset>" +
        "<legend>" + capitalizeEachWord(val.DSC_CAJA_CUENTA) + "</legend>" +
        "<div class='form-group'>" +
        "<label class='control-label col-xs-6'>Moneda</label>" +
        "<div class='col-xs-6'>" + val.DSC_MONEDA + "</div>" +
        "</div>" +
        "<div class='row'><div class='col-xs-12'><hr class='margin-tb'></div></div>" +
        "<div class='form-group'>" +
        "<label class='control-label col-xs-6'>Saldo</label>" +
        "<div class='col-xs-6'>" + numeral(val.MONTO_MON_CAJA).format('0,000.00') + "</div>" +
        "</div>" +
        "<div class='row'><div class='col-sm-12 margin-tb'></div></div>" +
        "</fieldset>";

    return html;
}

function estadoBtnConsultar() {
    if (!miInformacionCuenta) {
        $('#BtnConsultar').tooltip('enable').addClass('disabled');
    } else {
        $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
    }
}

$(document).ready(function () {
    initConsultaActivosCuenta();
    cargarEventHandlersConsultaActivosCuenta();
    $(window).trigger('resize');
});