﻿var jsfechaConsulta;
var jsfechaConsultaHasta;
var jsfechaFormato;
var jsfechaFormatoHasta;

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#Paginador").outerHeight(true) - 221;

    if (ancho < 768) {
        $("#ListaOperaciones").setGridHeight(165);
        $("#ListaDetalleOperaciones").setGridHeight(165);
        $("#panelfiltro").height(365);
    } else {
        $("#ListaOperaciones").setGridHeight(height * 0.7);
        $("#ListaDetalleOperaciones").setGridHeight(height * 0.3);
        $("#panelfiltro").height($("#panelDerecho").height() - 30);
    }
}

function initConsultaOperaciones() {
    $("#idSubtituloPaginaText").text("Consulta de Operaciones");
    document.title = "Consulta de Operaciones";

    if (!miInformacionCuenta) {
        $("#dtFechaConsultaHasta").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
        $("#dtFechaConsulta").datepicker('setDate', moment(new Date(localStorage.UltimaFechaCierre)).add(-1, 'months').toDate());
    } else {
        $("#dtFechaConsultaHasta").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
        $("#dtFechaConsulta").datepicker('setDate', moment($("#dtFechaConsultaHasta").datepicker('getDate')).add(-1, 'months').format('L'));
    }

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');
}

function cargarEventHandlersConsultaOperaciones() {
    $(window).bind('resize', function () {
        resize()
    }).trigger('resize');

    $("#dtRangoFecha").datepicker();

    $("#ListaOperaciones").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID_OPERACION" },
        colNames: ['N° Operación', 'Cuenta', 'Instrumento', 'Tipo Operación', 'Tipo Movimiento', 'Estado', 'Monto Total', 'Moneda', 'Fecha', 'Asesor'],
        colModel: [
            { name: "ID_OPERACION", index: "ID_OPERACION", sortable: true, width: 110, sorttype: "int", formatter: "int", editable: false, search: true, hidden: false, align: "right" },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 100, sorttype: "int", formatter: "int", editable: false, search: true, hidden: false, align: "right" },
            { name: "DSC_INTRUMENTO", index: "DSC_INTRUMENTO", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "DSC_TIPO_OPERACION", index: "DSC_TIPO_OPERACION", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "FLG_TIPO_MOVIMIENTO", index: "FLG_TIPO_MOVIMIENTO", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "DSC_ESTADO", index: "DSC_ESTADO", sortable: true, width: 100, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "MONTO_OPERACION", index: "MONTO_OPERACION", sortable: true, width: 110, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "DSC_MONEDA", index: "DSC_MONEDA", sortable: true, width: 100, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "FECHA_OPERACION", index: "FECHA_OPERACION", sortable: true, width: 100, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", searchoptions: { sopt: ["ge", "le", "eq"], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "NOMBRE", index: "NOMBRE", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false }
        ],
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        sortname: "ID_OPERACION",
        sortorder: "asc",
        caption: "Operaciones",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false,
        groupingView: {
            groupField: ['ID_OPERACION'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        },
        onSelectRow: function (rowId, status, e) {
            var jsIdOperacion = $("#ListaOperaciones").jqGrid("getCell", rowId, "ID_OPERACION");
            $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
            $.ajax({
                url: "ConsultaOperaciones.aspx/ConsultaDetalleOperacion",
                data: "{'idOperacion':" + jsIdOperacion + "}",
                datatype: "json",
                type: "post",
                contentType: "application/json; charset=utf-8",
                complete: function (jsondata, stat) {
                    if (stat == "success") {
                        var mydata = JSON.parse(jsondata.responseText).d;
                        $("#ListaDetalleOperaciones").setGridParam({ data: mydata });
                        $("#ListaDetalleOperaciones").trigger("reloadGrid");
                    }
                    else {
                        alert(JSON.parse(jsondata.responseText).Message);
                    }
                    $('#loaderProceso').modal('hide');
                }
            });
        }
    });
    $("#ListaOperaciones").jqGrid().bindKeys().scrollingRows = true
    $("#ListaOperaciones").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaOperaciones").jqGrid('navGrid', '#Paginador', { add: false, edit: false, del: false, excel: false, search: false });
    $("#ListaOperaciones").jqGrid('navButtonAdd', '#Paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaOperaciones', 'Operaciones', 'Operaciones', '');
        }
    });

    $("#ListaDetalleOperaciones").jqGrid({
        autowidth: true,
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID_OPERACION_DETALLE" },
        colNames: ['ID_OPERACION_DETALLE', 'Nemotécnico', 'Cantidad', 'Precio', 'Monto', 'Moneda'],
        colModel: [
            { name: "ID_OPERACION_DETALLE", index: "ID_OPERACION_DETALLE", sortable: false, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "NEMOTECNICO", index: "NEMOTECNICO", sortable: true, width: 250, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 190, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "PRECIO", index: "PRECIO", sortable: true, width: 190, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_PAGO", index: "MONTO_PAGO", sortable: true, width: 190, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "DSC_MONEDA", index: "DSC_MONEDA", sortable: true, width: 190, sorttype: "text", editable: false, search: true, hidden: false }
        ],
        pager: '#PaginadorDetalle',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        sortname: "ID_OPERACION_DETALLE",
        sortorder: "asc",
        caption: "Detalle de Operación",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false,
        groupingView: {
            groupField: ['ID_OPERACION_DETALLE'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        }
    });
    $("#ListaDetalleOperaciones").jqGrid().bindKeys().scrollingRows = true

    $("#BtnConsultar").button().click(function () {
        consultaOperaciones();
    });
}

function consultaOperaciones() {
    jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaFormatoHasta = moment.utc($("#dtFechaConsultaHasta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1)
    jsfechaConsultaHasta = jsfechaFormatoHasta.substring(0, jsfechaFormatoHasta.length - 1)

    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }
    if (jsfechaConsultaHasta === "") {
        alert("No ha indicado fecha de consulta hasta");
        return;
    }

    $('#ListaOperaciones').jqGrid('clearGridData');
    $('#ListaDetalleOperaciones').jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaOperaciones.aspx/ConsultaOperaciones",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'fechaConsultaHasta':'" + jsfechaConsultaHasta +
        "', 'idCuenta':" + (miInformacionCuenta == null ? "''" : miInformacionCuenta.IdCuenta) + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#ListaOperaciones").setGridParam({ data: mydata });
                $("#ListaOperaciones").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}

$(document).ready(function () {
    initConsultaOperaciones();
    cargarEventHandlersConsultaOperaciones();
    $(window).trigger('resize');
});