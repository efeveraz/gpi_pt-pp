﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="ReporteCartola.aspx.vb" Inherits="AplicacionWeb.ReporteCartola" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/reporteCartola.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <select id="DDTipoCartola" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tipo Cartola">
                        <option value="1">Balance</option>
                        <option value="2">APV Consolidada</option>
                        <option value="3">APV Ahorro</option>
                    </select>
                </div>
                <div class="form-group">
                    <div class="input-group date">
                        <input type="text" id="dtFechaConsulta" class="form-control" style="text-align: center;">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Seleccione Cuenta">Consultar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
