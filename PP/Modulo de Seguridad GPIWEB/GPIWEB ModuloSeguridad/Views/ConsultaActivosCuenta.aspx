﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="ConsultaActivosCuenta.aspx.vb" Inherits="AplicacionWeb.ConsultaActivosCuenta"
    EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaActivosCuenta.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <div class="input-group date">
                        <input type="text" id="dtFechaConsulta" class="form-control" style="text-align: center;">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Seleccione Cuenta">Consultar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-xs-12">
            <ul id="idNavTabs" class="nav nav-tabs">
                <li class="active"><a href="#tabs-1" data-toggle="tab">Activos Cuenta</a></li>
                <li id="tabDetalle"><a href="#tabs-2" data-toggle="tab">Detalle Cuenta</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade  in active" id="tabs-1">
                    <div class="row top-buffer">
                        <div class="col-sm-12">
                            <table id="ListaActivosCuenta">
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                            <div id="PaginadorDetalle">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tabs-2">
                    <div id="divDatosCajas" class="row top-buffer">
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading ">Datos Cuenta</div>
                                <div class="panel-body">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="control-label col-xs-6">Asesor</label>
                                            <div class="col-xs-6" id="txtAsesor"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <hr class="margin-tb">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-xs-6">Moneda</label>
                                            <div class="col-xs-6" id="txtMoneda"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <hr class="margin-tb">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-xs-6">Patrimonio</label>
                                            <div class="col-xs-6" id="txtPatrimonio"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <hr class="margin-tb">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-xs-6">Rentabilidad</label>
                                            <div class="col-xs-6" id="txtRentabilidad"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 margin-tb">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading ">Cajas Asociadas</div>
                                <div class="panel-body" id="divCajasCuenta"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>