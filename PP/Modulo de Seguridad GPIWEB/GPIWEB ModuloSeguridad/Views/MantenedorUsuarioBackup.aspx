﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Sistema/GPIWeb.Master"
    CodeBehind="MantenedorUsuarioBackup.aspx.vb" Inherits="AplicacionWeb.MantenedorUsuarioBackup" %>

<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContenidoPPalFiltro" runat="server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/mantenedorUsuarioBackup.min.js") %>'></script>

    <link href='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.css") %>' rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.js") %>'></script>

    <link href='<%= VersionLinkHelper.GetVersion("../Estilos/vendor/select2/select2.min.css") %>' rel="stylesheet" />
    <link href='<%= VersionLinkHelper.GetVersion("../Estilos/vendor/select2/select2-bootstrap.min.css") %>' rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/select2/select2.full.min.js") %>'></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/select2/i18n/es.js") %>'></script>

    <link href='<%= VersionLinkHelper.GetVersion("../Estilos/vendor/bootstrap-toggle-master/bootstrap-toggle.min.css") %>' rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/bootstrap-toggle-master/bootstrap-toggle.min.js") %>'></script>

    <div class="row">
        <div class="col-xs-12" id="panelUsuario">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnHabilitaEditar" class='btn btn-default navbar-btn' data-toggle='tooltip' data-placement='bottom' data-original-title='Seleccione Usuario'><i class='fa fa-pencil'></i></button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default mb-0">
                <div class="panel-body">
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='table-responsive'>
                                <table class='table table-striped table-hover table-bordered table-condensed' id='tablaUsuarios'>
                                    <thead style="display: table-header-group">
                                        <tr>
                                            <th></th>
                                            <th>Usuario</th>
                                            <th>Cantidad Usuario</th>
                                            <th>Backup</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="panelEditar" class="col-sm-12 top-buffer hidden">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnGuardar" class="btn btn-primary navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Guardar'><i class="fa fa-floppy-o"></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnCancelar" class="btn btn-danger navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Cancelar'><i class="fa fa-times"></i></button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Editar Usuario:&nbsp;<strong id="tituloEditar"></strong></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="form-horizontal">
                            <div id="divCheck" class="col-xs-12 col-sm-5 col-md-4" style="padding-top: 9px">
                                <label class="control-label">Activar Usuario Backup</label><br />
                                <input id="chkActivar" type="checkbox" data-toggle="toggle" data-size="small" data-on="Activo" data-off="Inactivo" data-width="100"><br />
                                <span id="lblAlertaCheck" class="label label-danger hidden">Este usuario ya está siendo reemplazado</span>
                            </div>
                            <div id="divDDUsuarios" class="col-xs-12 col-sm-7 col-md-8" style="padding-top: 5px;">
                                <label class="control-label">Usuarios</label>
                                <select id="DDUsuarios" class="form-control input-sm" data-placeholder='Seleccione' data-minimum-results-for-search='1' multiple>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
