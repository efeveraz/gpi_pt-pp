﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="IngresoEscritorioComercial.aspx.vb" Inherits="AplicacionWeb.IngresoEscritorioComercial" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/ingresoEscritorioComercial.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <select id="DDTipoEscritorio" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Opción tipo Escritorio Comercial">
                        <option value="MA">Acciones Internacionales</option>
                        <option value="AC">Acciones Nacionales</option>
                        <option value="MF">Fondos Mutuos Internacionales</option>
                        <option value="IC">Ingreso de Custodia</option>
                        <option value="IN">Inversiones</option>
                        <option value="OC">Orden Ejecutivo</option>
                        <option value="RE">Rescates</option>
                        <option value="MO">Spot</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Seleccione Cuenta">Ingresar</button>
                </div>
            </div>
        </div>
    </div>
    <iframe id="iframe" src="about: blank" class="top-buffer panel panel-default" style="height: 70vh; width: 100%; margin-bottom: 0; background-color: whitesmoke;"></iframe>
</asp:Content>
