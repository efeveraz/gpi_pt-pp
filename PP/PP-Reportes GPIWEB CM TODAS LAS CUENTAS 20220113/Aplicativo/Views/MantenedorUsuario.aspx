﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Sistema/GPIWeb.Master"
    CodeBehind="MantenedorUsuario.aspx.vb" Inherits="AplicacionWeb.MantenedorUsuario" %>

<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContenidoPPalFiltro" runat="server">
    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/mantenedorUsuario.min.js") %>'></script>

    <link href='<%= VersionLinkHelper.GetVersion("../Estilos/vendor/select2/select2.min.css") %>' rel="stylesheet" />
    <link href='<%= VersionLinkHelper.GetVersion("../Estilos/vendor/select2/select2-bootstrap.min.css") %>' rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/select2/select2.full.min.js") %>'></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/select2/i18n/es.js") %>'></script>
    <link href='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.css") %>' rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.js") %>'></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/jqRut/jquery.rut.js") %>'></script>

    <div class="row">
        <div class="col-xs-12" id="panelUsuario">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnNuevoUsuario" class="btn btn-default navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Nuevo'><i class='fa fa-plus'></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnEditarUsuario" class='btn btn-default navbar-btn fade hidden' data-toggle='tooltip' data-placement='bottom' data-original-title='Editar'><i class='fa fa-pencil'></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnEliminar" class='btn btn-danger navbar-btn fade hidden' data-toggle='tooltip' data-placement='bottom' data-original-title='Eliminar'><i class='fa fa-trash'></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnPermisosCuenta" class='btn btn-default navbar-btn fade hidden' data-toggle='tooltip' data-placement='bottom' data-original-title='Permisos Cuenta'>Cuentas</button>
                        </li>
                        <li>
                            <div class='navbar-btn'>
                                <select id="DDEstado" class="form-control" data-toggle='tooltip' data-placement='bottom' data-original-title='Estados Usuario'>
                                    <option value="V" selected>Vigente</option>
                                    <option value="A">Anulado</option>
                                    <option value="B">Bloqueado</option>
                                    <option value="0">Todos</option>
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default mb-0">
                <div class="panel-body">
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='table-responsive'>
                                <table class='table table-striped table-hover table-bordered table-condensed' id='tablaUsuarios'>
                                    <thead style="display: table-header-group">
                                        <tr>
                                            <th>
                                                <div class="checkbox">
                                                    <input id="select_all" value="1" type="checkbox">
                                                    <label for="select_all"></label>
                                                </div>
                                            </th>
                                            <th>Usuario</th>
                                            <th>Rut</th>
                                            <th>Asesor</th>
                                            <th>Roles</th>
                                            <th>Empresas</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="panelAgregar" class="col-sm-12 hidden">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnGuardarUsuario" class="btn btn-primary navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Guardar'><i class="fa fa-floppy-o"></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnCancelar" class="btn btn-danger navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Cancelar'><i class="fa fa-times"></i></button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default mb-0">
                <div id="tituloNuevoEditar" class="panel-heading "></div>
                <div id="sPanelAgregar" class="panel-body">
                    <div class="row">
                        <div class="form-horizontal">
                            <div id="divTxtAgregarUsuario" class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <label for="txtNombreUsuario">Nombre Usuario</label>
                                <input type="text" id="txtNombreUsuario" maxlength="50" class="form-control input-sm" required>
                                <span id="lblErrNomUsuario" class="label label-danger hidden"></span>
                            </div>
                            <div id="divDDAsesorAgregar" class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <label for="DDAsesorAgregar">Asesor</label>
                                <select id="DDAsesorAgregar" class="form-control input-sm">
                                </select>
                            </div>
                            <div id="divMailUsuario" class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <label for="txtMailUsuario">Mail</label>
                                <input type="email" id="txtMailUsuario" maxlength="50" class="form-control input-sm" required>
                                <span id="lblErrMailUsuario" class="label label-danger hidden"></span>
                            </div>
                            <div id="divtxtRut" class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <label for="txtRut">Rut</label>
                                <input id="txtRut" type="text" maxlength="10" class="form-control input-sm">
                                <span id="lbltxtRut" class="label label-danger hidden"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-horizontal">
                            <fieldset class="col-lg-12" style="padding-top: 5px;">
                                <legend>Empresas</legend>
                                <div id="divInfoUsuarioEmpresa">
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div id="divHr" class="row">
                        <div class="col-lg-12">
                            <hr />
                        </div>
                    </div>
                    <div id="divSelectEmpresas" class="col-xs-12 col-sm-6 col-md-4 col-lg-3 input-group-sm" style="padding-top: 3px;">
                        <label class="control-label" for="selectEmpresas">Crear Usuario en Empresa</label>
                        <select class="form-control input-sm" data-placeholder="Seleccione" data-minimum-results-for-search="1" multiple id="selectEmpresas">
                        </select>
                    </div>
                </div>

            </div>
        </div>
        <div id="panelUsuarioCuenta" class="col-sm-12 hidden">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnGuardarAsociar" class="btn btn-primary navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Guardar'><i class="fa fa-floppy-o"></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnCancelarAsignacion" class="btn btn-danger navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Cancelar'><i class="fa fa-times"></i></button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default mb-0">
                <div class="panel-heading form-inline">
                    <div class="row">
                        <div class="col-sm-6">
                            Asignacion de cuentas a Usuario:
                            <label id="lbUsuarioCuenta"></label>
                        </div>
                        <div class="col-sm-6 text-right">
                            <span>Cuentas Disponibles:</span>
                            <select id="DDFiltroCuentas" class="form-control input-sm">
                                <option value="TOD">Todas</option>
                                <option value="ASE">Por asesor</option>
                                <option value="EMP">Por empresa</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="idPnlCtas" class="panel-body" style="overflow: auto;">
                    <div id="panelAsociar" class="col-xs-12 col-md-5 col-sm-5">
                        <div class="panel panel-default mb-0">
                            <div class="panel-heading">Cuentas asignadas</div>
                            <div id="lisPnlAsociar" class="panel-body">
                                <div class="input-group">
                                    <input id="txtBuscarAsociados" type="text" class="form-control" placeholder="Buscar...">
                                    <span class="input-group-btn">
                                        <button id="btnLimpiarAso" class="btn btn-default" type="button">
                                            <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                                        </button>
                                    </span>
                                </div>
                                <div id="lisUsuarioAsociar" style="overflow: auto;">
                                    <ul id="ulLisCtaAsociada" style="width: 600px;" class="list-group checked-list-box list-il-block">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="panelBtnAsociarV" class="hidden-xs col-md-2 col-sm-2 text-center">
                        <div class="panel-body">
                            <div class="form-group-sm">
                                <button type="button" id="btnAsociarV" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Asociar">
                                    <span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>
                                </button>
                            </div>
                            <div class="form-group-sm">
                                <button type="button" id="btnDesAsociarV" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Desasociar">
                                    <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
                                </button>
                            </div>
                            <div class="form-group-sm">
                                <button type="button" id="btnAsociarTodosV" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Asociar Todas">
                                    <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
                                </button>
                            </div>
                            <div class="form-group-sm">
                                <button type="button" id="btnDesAsociarTodosV" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Desasociar Todas">
                                    <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
                                </button>
                            </div>
                            <div class="form-group-sm">
                                <button type="button" id="btnResetCuentas" class="btn btn-default" data-toggle='tooltip' data-placement='bottom' data-original-title="Deshacer cambios">
                                    <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="panelBtnAsociarH" class="col-xs-12  hidden-lg hidden-md hidden-sm mb-0 text-center">
                        <div class="panel-body">
                            <div class="form-group-sm">
                                <button type="button" id="btnAsociarH" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Asociar">
                                    <span class="glyphicon glyphicon-triangle-left fa-rotate-90" aria-hidden="true"></span>
                                </button>
                                <button type="button" id="btnDesAsociarH" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Desasociar">
                                    <span class="glyphicon glyphicon-triangle-right fa-rotate-90" aria-hidden="true"></span>
                                </button>
                                <button type="button" id="btnAsociarTodosH" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Asociar Todas">
                                    <span class="glyphicon glyphicon-backward fa-rotate-90" aria-hidden="true"></span>
                                </button>
                                <button type="button" id="btnDesAsociarTodosH" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Desasociar Todas">
                                    <span class="glyphicon glyphicon-forward fa-rotate-90" aria-hidden="true"></span>
                                </button>
                                <button type="button" id="btnResetCuentasH" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Deshacer cambios">
                                    <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="panelDisponibles" class="col-xs-12 col-md-5 col-sm-5">
                        <div class="panel panel-default mb-0">
                            <div class="panel-heading form-inline">
                                Cuentas disponibles
                            </div>
                            <div id="lisPnlDisponible" class="panel-body">
                                <select id="DDAsesorCuenta" class="form-control" style="margin-bottom: 10px;" data-toggle="tooltip" data-placement="bottom" data-original-title="Asesores">
                                </select>
                                <select id="DDEmpresaCuenta" class="form-control" style="margin-bottom: 10px;" data-toggle="tooltip" data-placement="bottom" data-original-title="Empresas">
                                </select>
                                <div class="input-group">
                                    <input id="txtBuscarDisponible" type="text" class="form-control" placeholder="Buscar...">
                                    <span class="input-group-btn">
                                        <button id="btnLimpiarDes" class="btn btn-default" type="button">
                                            <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                                        </button>
                                    </span>
                                </div>
                                <div id="lisUsuarioDisponible" style="overflow: auto;">
                                    <ul id="ulLisCtaDisponible" style="width: 600px;" class="list-group checked-list-box list-il-block">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
