﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="ConsultaRentabilidadHistorica.aspx.vb" Inherits="AplicacionWeb.ConsultaRentabilidadHistorica" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">
    <script src='<%= VersionLinkHelper.GetVersion("../js/Stock/highstock.js") %>' type="text/javascript"></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/Stock/modules/exporting.js") %>' type="text/javascript"></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaRentabilidadHistorica.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <div id="dtRangoFecha" class="input-daterange input-group">
                        <input id="dtFechaConsulta" type="text" class="form-control" />
                        <span class="input-group-addon">a</span>
                        <input id="dtFechaConsultaHasta" type="text" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Seleccione Cuenta">Consultar</button>
                    <button type="button" id="BtnTodasLasCuentas" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom">Consultar Todas Las Cuentas</button>
                </div>
            </div>
        </div>
    </div>
    <div id="contenidoPorCuenta" class="row top-buffer">
        <div id="grillaPorCuenta" class="col-xs-12 col-sm-6">
            <table id="ListaPorCuenta">
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>
        <div id="graficoPorCuenta" style="height: 100%" class="hidden-xs col-sm-6">
        </div>
    </div>
    <div id="contenidoPorCliente" class="row top-buffer">
        <div id="grillaPorCliente" class="col-xs-12 col-sm-6">
            <table id="ListaPorCliente">
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>
        <div id="graficoPorCliente" style="height: 100%" class="hidden-xs col-sm-6">
        </div>
    </div>
    <div id="contenidoPorGrupo" class="row top-buffer">
        <div id="grillaPorGrupo" class="col-xs-12 col-sm-6">
            <table id="ListaPorGrupo">
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>
        <div id="graficoPorGrupo" style="height: 100%" class="hidden-xs col-sm-6">
        </div>
    </div>
    <div style="display: none;">
            <table id="grillaTodasLasCuenta">
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>
</asp:Content>