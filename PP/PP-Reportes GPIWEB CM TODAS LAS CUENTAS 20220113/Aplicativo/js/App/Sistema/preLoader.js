﻿(async function preloader() {
    if (localStorage.getItem("UltimaFechaCierre") === null) {
        localStorage.UltimaFechaCierre = await BuscarUltimaFechaCierre();
    }
    if (localStorage.getItem("ValoresMonedas") === null) {
        localStorage.ValoresMonedas = await BuscarValoresMonedas();
    }
    if (localStorage.getItem("Menu") === null) {
        localStorage.Menu = await BuscarMenu();
    }
    if (localStorage.getItem("LogoEmpresa") === null) {
        localStorage.LogoEmpresa = await BuscarLogoEmpresa();
    }
    if (localStorage.getItem("ColorEmpresa") === null) {
        localStorage.ColorEmpresa = await BuscarColorEmpresa();
    }
    if (localStorage.getItem("RolUsuario") === null) {
        localStorage.RolUsuario = await BuscarRolUsuario();
    }
    if (localStorage.getItem("CssSidebarHover") === null) {
        localStorage.CssDinamico = await CargaCssDinamico();
    }
    if (localStorage.getItem("UserName") === null) {
        localStorage.UserName = await BuscarUserName();
    }
    if (localStorage.getItem("DscEmpresa") === null) {
        localStorage.DscEmpresa = await BuscarDscEmpresa();
    }

    redirect();
})();

function redirect() {
    window.location.href = '../Sistema/index.aspx';
}

async function BuscarUltimaFechaCierre() {
    const response = await fetch('../Servicios/Globales.asmx/BuscarUltimaFechaCierre',
        {
            method: "POST",
            body: '{}',
            credentials: 'same-origin',
            headers: new Headers({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    let data = await response.json();
    data = JSON.parse(data.d);
    return data;
}
async function BuscarValoresMonedas() {
    const response = await fetch('../Servicios/Menu.asmx/consultarValores',
        {
            method: "POST",
            body: '{}',
            credentials: 'same-origin',
            headers: new Headers({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    return await response.text();
}
async function BuscarMenu() {
    const response = await fetch('../Servicios/Menu.asmx/consultarMenu',
        {
            method: "POST",
            body: '{}',
            credentials: 'same-origin',
            headers: new Headers({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    return await response.text();
}
async function BuscarRolUsuario() {
    const response = await fetch('../Servicios/Session.asmx/BuscarVariableSesion',
        {
            method: "POST",
            body: "{ strNombre:'rolusuario'}",
            credentials: 'same-origin',
            headers: new Headers({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    let data = await response.json();
    return data.d;
}
async function CargaCssDinamico() {
    var varCss = "<style>" +
        " .sidebar-nav>li>a:hover { background-color: rgb(" + localStorage.ColorEmpresa + "); color: #f8f8f8; } " +
        " .navbar-nav>.open>a { background-color: rgba(" + localStorage.ColorEmpresa + ", 1) } " +
        " .navbar-nav>.open>a:hover { background-color: rgba(" + localStorage.ColorEmpresa + ", 1) } " +
        " .table thead { background-color: rgba(" + localStorage.ColorEmpresa + ", 1) } " +
        " .table thead tr th { border-color: rgba(" + localStorage.ColorEmpresa + ", 1) } " +
        " .ui-th-column.ui-th-ltr:hover { background-color: rgba(" + localStorage.ColorEmpresa + ", 1) } " +
    "</style>";
    return varCss;
}
async function BuscarUserName() {
    const response = await fetch('../Servicios/Session.asmx/BuscarVariableSesion',
        {
            method: "POST",
            body: "{ strNombre:'username'}",
            credentials: 'same-origin',
            headers: new Headers({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    let data = await response.json();
    return data.d;
}
async function BuscarDscEmpresa() {
    const response = await fetch('../Servicios/Session.asmx/BuscarVariableSesion',
        {
            method: "POST",
            body: "{ strNombre:'DscEmpresa'}",
            credentials: 'same-origin',
            headers: new Headers({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    let data = await response.json();
    return data.d;
}
async function BuscarLogoEmpresa() {
    const response = await fetch('../Servicios/Session.asmx/BuscarVariableSesion',
        {
            method: "POST",
            body: "{ strNombre:'LogoEmpresa'}",
            credentials: 'same-origin',
            headers: new Headers({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    let data = await response.json();
    return data.d;
}
async function BuscarColorEmpresa() {
    const response = await fetch('../Servicios/Session.asmx/BuscarVariableSesion',
        {
            method: "POST",
            body: "{ strNombre:'ColorEmpresa'}",
            credentials: 'same-origin',
            headers: new Headers({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    let data = await response.json();
    return data.d;
}