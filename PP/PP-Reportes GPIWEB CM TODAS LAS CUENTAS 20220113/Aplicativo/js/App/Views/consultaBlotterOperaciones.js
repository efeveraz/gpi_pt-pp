﻿var jsfechaConsulta;

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#Paginador").outerHeight(true) - 144;

    if (ancho < 768) {
        $("#ListaBlotterOperaciones").setGridHeight(210);
    } else {
        $("#ListaBlotterOperaciones").setGridHeight(height);
    }
}

function initConsultaBlotterOperaciones() {

    $("#idSubtituloPaginaText").text("Consulta Blotter Operaciones");
    document.title = "Consulta Blotter Operaciones";

    cargarInstrumentos();

    if (!miInformacionCuenta) {
        $(".input-group.date").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
    } else {
        $(".input-group.date").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
    }

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');

    jsfechaConsulta = document.getElementById("dtFechaConsulta").value;
}

function eventHandlersConsultaBlotterOperaciones() {

    $(".input-group.date").datepicker();

    $(window).bind('resize', function () {
        resize()
    }).trigger('resize');

    $("#ListaBlotterOperaciones").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID_OPERACION" },
        colNames: ['ID_CUENTA', 'Cuenta', 'ID_CLIENTE', 'Rut', 'Cliente', 'Asesor', 'Nemotecnico', 'Cantidad', 'Precio/Tasa', 'Monto', 'Ord. Magic', 'ID_MONEDA', 'Movimiento', 'P/N'],
        colModel: [
            { name: "ID_CUENTA", index: "ID_CUENTA", sortable: true, width: 50, sorttype: "int", formatter: "int", editable: false, search: true, hidden: true, align: "right" },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 80, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "ID_CLIENTE", index: "ID_CLIENTE", sortable: true, width: 50, sorttype: "int", formatter: "int", editable: false, search: true, hidden: true, align: "right" },
            { name: "RUT_CLIENTE", index: "RUT_CLIENTE", sortable: true, width: 90, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NOMBRE_CLIENTE", index: "NOMBRE_CLIENTE", sortable: true, width: 190, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NOMBRE_ASESOR", index: "NOMBRE_ASESOR", sortable: true, width: 140, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NEMOTECNICO", index: "NEMOTECNICO", sortable: true, width: 170, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 90, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "PRECIO_TASA", index: "PRECIO_TASA", sortable: true, width: 100, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO", index: "MONTO", sortable: true, width: 110, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "ORDEN_MAGIC", index: "ORDEN_MAGIC", sortable: true, width: 90, sorttype: "text", formatter: "text", editable: false, search: true, hidden: false },
            { name: "ID_MONEDA", index: "ID_MONEDA", sortable: true, width: 50, sorttype: "int", formatter: "int", editable: false, search: true, hidden: true, align: "right" },
            { name: "DSC_MOVIMIENTO_OPERACION", index: "DSC_MOVIMIENTO_OPERACION", sortable: true, width: 90, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "TIPO_INVERSION", index: "TIPO_INVERSION", sortable: true, width: 70, sorttype: "text", editable: false, search: true, hidden: false }
        ],
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        height: "50%",
        styleUI: 'Bootstrap',
        sortname: "ID_CUENTA",
        sortorder: "asc",
        caption: "Blotter Operaciones",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false,
        groupingView: {
            groupField: ['ID_CUENTA'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        },
        gridComplete: function () {
            var ids = $("#ListaBlotterOperaciones").jqGrid("getDataIDs"), l = ids.length, i, rowid, status;
            var rowid = "";
        }
    });

    $("#ListaBlotterOperaciones").jqGrid().bindKeys().scrollingRows = true
    $("#ListaBlotterOperaciones").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaBlotterOperaciones").jqGrid('navGrid', '#Paginador', { add: false, edit: false, del: false, excel: false, search: false });
    $("#ListaBlotterOperaciones").jqGrid('navButtonAdd', '#Paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaBlotterOperaciones', 'Blotter Operaciones', 'Blotter Operaciones', 'DATACOMPLETA');
        }
    });
    $('#Paginador_left').width('auto');

    $("#BtnConsultar").button().click(function () {
        consultaOperaciones();
        resize();
    });
}

function consultaOperaciones() {
    var jsfechaFormato = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1);

    var jsCodInstrumento = $("#DDInstrumento").val();

    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }

    $("#ListaBlotterOperaciones").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaBlotterOperaciones.aspx/ConsultaBlotterOperaciones",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'idCuenta':" + (miInformacionCuenta == null ? "''" : miInformacionCuenta.IdCuenta) +
        ", 'CodInstrumento':" + (jsCodInstrumento == -1 ? null : "'" + jsCodInstrumento + "'") + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#ListaBlotterOperaciones").setGridParam({ data: mydata });
                $("#ListaBlotterOperaciones").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });

};
function cargarInstrumentos() {
    $.ajax({
        url: "../Servicios/Globales.asmx/consultaInstrumentos",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                $("#DDInstrumento").empty();
                $("#DDInstrumento").prepend('<option value="' + -1 + '">' + 'Todos los Instrumentos' + '</option>');
                $.each(res, function (key, val) {
                    $("#DDInstrumento").append('<option value="' + val.COD_INSTRUMENTO + '">' + val.DSC_INTRUMENTO + '</option>');
                });
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

$(document).ready(function () {
    initConsultaBlotterOperaciones();
    eventHandlersConsultaBlotterOperaciones();
    resize();
});
