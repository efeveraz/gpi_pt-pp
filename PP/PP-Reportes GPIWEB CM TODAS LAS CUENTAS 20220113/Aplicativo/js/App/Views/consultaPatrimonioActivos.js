﻿var jsfechaConsulta;

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#PaginadorDetalle").outerHeight(true) - $("#PaginadorResumen").outerHeight(true) - 242;

    if (ancho < 768) {
        $("#PatrimonioYActivosDetallado").setGridHeight(190);
        $("#PatrimonioYActivosResumen").setGridHeight(190);
    } else {
        $("#PatrimonioYActivosDetallado").setGridHeight(height);
        $("#PatrimonioYActivosResumen").setGridHeight(height);
    }
}

function initConsultaPatrimonioActivos() {

    $("#idSubtituloPaginaText").text("Consulta de Patrimonio y Activos");
    document.title = "Consulta de Patrimonio y Activos";

    cargarMonedas();
    cargarAsesores();

    if (!miInformacionCuenta) {
        $(".input-group.date").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
    } else {
        $(".input-group.date").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
    }

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');

    jsfechaConsulta = document.getElementById("dtFechaConsulta").value;
    var arrFechaDesde = jsfechaConsulta.split(strSeparadorFecha);
    var prevCellVal = [{ id: 0, col: undefined, cellId: undefined, value: undefined }];

    pivotPatrimonioYActivosDetallado({});
    pivotPatrimonioYActivosResumen({});
}
function cargarEventHandlersConsultaPatrimonioAct() {

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(window).trigger('resize');
    });

    $(".input-group.date").datepicker();

    $("#BtnConsultar").button().click(function () {
        $('#PatrimonioYActivosDetallado').jqGrid('clearGridData');
        consultaOperaciones();
    });
}

function pivotPatrimonioYActivosDetallado(data) {
    $.jgrid.gridUnload("PatrimonioYActivosDetallado");
    $("#PatrimonioYActivosDetallado").jqGrid('jqPivot', data, {
        xDimension: [{ dataName: 'NUM_CUENTA', label: 'Cuenta', isGroupField: false, width: 80 },
            { dataName: 'DSC_CUENTA', label: 'Nombre Corto', isGroupField: false },
            { dataName: 'PATRIMONIO_MON_CUENTA', isGroupField: false, sorttype: "number", formatter: 'number', label: 'Total Patrimonio', align: "right" },
            { dataName: 'PROMEDIO', isGroupField: false, sorttype: "number", width: 130, formatter: 'number', label: 'Promedio Mensual Activos', align: "right" }],
        yDimension: [{ dataName: 'DSC_ARBOL_CLASE_INST_PADRE' }, { dataName: 'DSC_ARBOL_CLASE_INST' }],
        aggregates: [{
            member: 'MONTO',
            aggregator: 'sum',
            sorttype: "number",
            formatter: 'number',
            width: 195,
            align: 'right'
        }],
        rowTotals: true,
        colTotals: true,
        rowTotalsText: 'Total Activos'
    }, {
        autowidth: true,
        shrinkToFit: false,
        responsive: true,
        sortname: "NUM_CUENTA",
        styleUI: 'Bootstrap',
        pager: "#PaginadorDetalle",
        regional: localeCorto,
        caption: "Patrimonio y Activos Detallado"
    });
    $("#PatrimonioYActivosDetallado").jqGrid().bindKeys().scrollingRows = true
    $("#PatrimonioYActivosDetallado").jqGrid('navGrid', '#PaginadorDetalle', { add: false, edit: false, del: false, excel: false, search: false });
    $("#PatrimonioYActivosDetallado").jqGrid('navButtonAdd', '#PaginadorDetalle', {
        caption: "Excel", buttonicon: "ui-icon-print", title: "Exporta la información completa a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcelPatrimonioActivos('PatrimonioYActivosDetallado', 'PatrimonioYActivosResumen', 'Consulta Patrimonio y Activos', 'Patrimonio y Activos Detalle', 'Patrimonio y Activos Resumen');
        }
    });
}
function pivotPatrimonioYActivosResumen(data) {
    $.jgrid.gridUnload("PatrimonioYActivosResumen");
    $("#PatrimonioYActivosResumen").jqGrid('jqPivot', data, {
        xDimension: [{ dataName: 'NOMBRE_ASESOR', label: 'Asesor', isGroupField: false, width: 160 },
            { dataName: 'TOTAL_PATRIMONIO', isGroupField: false, sorttype: "number", formatter: 'number', label: 'Total Patrimonio Administrado', align: "right" },
            { dataName: 'PROMEDIO', isGroupField: false, sorttype: "number", width: 130, formatter: 'number', label: 'Promedio Mensual Activos', align: "right" }],
        yDimension: [{ dataName: 'DSC_PADRE_ARBOL_CLASE_INST' }, { dataName: 'DSC_ARBOL_CLASE_INST' }],
        aggregates: [{
            member: 'MONTO',
            aggregator: 'sum',
            sorttype: "number",
            formatter: 'number',
            width: 195,
            align: 'right'
        }],
        rowTotals: true,
        colTotals: true,
        rowTotalsText: 'Total Activos'
    }, {
        autowidth: true,
        shrinkToFit: false,
        responsive: true,
        sortname: "NOMBRE_ASESOR",
        styleUI: 'Bootstrap',
        pager: "#PaginadorResumen",
        regional: localeCorto,
        caption: "Patrimonio y Activos Resumen"
    });
    $("#PatrimonioYActivosResumen").jqGrid('navGrid', '#PaginadorResumen', { add: false, edit: false, del: false, excel: true, search: false });
    $("#PatrimonioYActivosResumen").jqGrid('navButtonAdd', '#PaginadorResumen', {
        caption: "Excel", buttonicon: "ui-icon-print", title: "Exporta la información completa a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcelPatrimonioActivos('PatrimonioYActivosDetallado', 'PatrimonioYActivosResumen', 'Consulta Patrimonio y Activos', 'Patrimonio y Activos Detalle', 'Patrimonio y Activos Resumen');
        }
    });
}
function consultaOperaciones() {
    var jsfechaFormato = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1);

    var jsidMoneda = $("#DDMoneda").val();
    var pIdAsesor = ($("#DDAsesor").val() == -1 ? null : $("#DDAsesor").val());

    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }
    var a = 0;
    var b = 0;
    $("#PatrimonioYActivosDetallado").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });

    $.ajax({
        url: "ConsultaPatrimonioActivos.aspx/ConsultaPatrimonioActivosDetalle",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'idCuenta':" + (miInformacionCuenta == null ? "''" : miInformacionCuenta.IdCuenta) +
        ", 'idMoneda':" + jsidMoneda +
        ", 'idAsesor':" + pIdAsesor + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            a = 1;
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                pivotPatrimonioYActivosDetallado(mydata);
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            if (a == 1 && b == 1) {
                $('#loaderProceso').modal('hide');
                $(window).trigger('resize');
            }
        }
    });
    $("#PatrimonioYActivosResumen").jqGrid('clearGridData');
    $.ajax({
        url: "ConsultaPatrimonioActivos.aspx/ConsultaPatrimonioActivosResumen",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'idCuenta':" + (miInformacionCuenta == null ? "''" : miInformacionCuenta.IdCuenta) +
        ", 'idMoneda':" + jsidMoneda + ", 'idAsesor':" + pIdAsesor + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            b = 1;
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                pivotPatrimonioYActivosResumen(mydata);
            }
            else {
                alert(JSON.parse(jsondata.responseText).Message);
            }
            //$("#PatrimonioYActivosResumen").trigger("reloadGrid");
            if (a == 1 && b == 1) {
                $('#loaderProceso').modal('hide');
                $(window).trigger('resize');
            }
        }
    });
}
function cargarMonedas() {
    $.ajax({
        url: "../Servicios/Globales.asmx/consultaMonedas",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                $("#DDMoneda").empty();
                $.each(res, function (key, val) {
                    $("#DDMoneda").append('<option value="' + val.ID_MONEDA + '">' + capitalizeEachWord(val.DSC_MONEDA) + '</option>');
                });
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}
function cargarAsesores() {
    $('#loaderProceso').modal();
    $.ajax({
        url: "ConsultaClientes.aspx/ConsultaAsesores",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                $("#DDAsesor").empty();
                $("#DDAsesor").prepend('<option value="' + -1 + '">' + 'Todos los Asesores' + '</option>');
                $.each(res, function (key, val) {
                    $("#DDAsesor").append('<option value="' + val.ID_ASESOR + '">' + capitalizeEachWord(val.NOMBRE) + '</option>');
                });
                $("#DDAsesor").val(-1).change();
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}

$(document).ready(function () {
    initConsultaPatrimonioActivos();
    cargarEventHandlersConsultaPatrimonioAct();
    resize();
});