﻿var lista = "#ListaCuentasPorCuenta";
var target = "#tabCuenta";
var tablaLista = "ListaCuentasPorCuenta";

var urlVarFechaConsulta = getUrlVars()["FechaConsulta"];

var GetCuentasFiltroCuentas;
var GetCuentasFiltroClientes;
var GetCuentasFiltroGrupos;

var rows_selectCta = [];
var rows_selectCtaCli = [];
var rows_selectCtaGru = [];

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#tabsClienteCuentaGrupo").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - 86 - 84 - 70;
}

function generarReporte(fecha, fechaHasta, nomReporte, nomFunction) {

    var newArrCtas = [];
    var arrayIdCuentas = [];

    if (target == "#tabCuenta") {
        var data = GetCuentasFiltroCuentas.data();
        var arrayIdCuentas = rows_selectCta;
    }
    if (target == "#tabCliente") {
        var data = GetCuentasFiltroClientes.data();
        var arrayIdCuentas = rows_selectCtaCli;
    }
    if (target == "#tabGrupo") {
        var data = GetCuentasFiltroGrupos.data();
        var arrayIdCuentas = rows_selectCtaGru;
    }

    $.each(arrayIdCuentas, function (index, value) {
        var dataRow = jQuery.map(data, function (obj) {
            if (obj.ID_CUENTA == value)
                return obj;
        });
        newArrCtas.push({
            id_empresa: dataRow[0].ID_EMPRESA,
            id_cuenta: dataRow[0].ID_CUENTA,
            nro_cuenta: dataRow[0].NUM_CUENTA,
            rut_cliente: dataRow[0].RUT_CLIENTE,
            nom_cliente: dataRow[0].NOM_CLIENTE,
            nom_asesor: dataRow[0].NOM_ASESOR,
            abr_empresa: dataRow[0].ABR_EMPRESA
        });
    });

    if (arrayIdCuentas.length == 0) {
        alert("Debe seleccionar al menos una Cuenta a generar.");
    } else {
        JqGridAExcelASP.jqGridMantenedorComisionAdm(newArrCtas, fecha, fechaHasta, nomReporte, nomFunction);
    }
}

function consultaCuentasPorCuenta(jsIdCuenta, jsIdCliente, jsIdGrupo, tabla) {

    GetCuentasFiltroCuentas = $(tabla).DataTable({
        data: {},
        columns: [
            {
                "searchable": false,
                "orderable": false,
                "defaultContent": "<div class='checkbox'><input value='' type='checkbox'><label></label></div>",
                width: '1%',
                className: 'dt-body-center',
            },
            { "data": "DSC_EMPRESA" },
            { "data": "NUM_CUENTA" },
            { "data": "DSC_CUENTA" },
        ],
        destroy: true,
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        ajax: {
            url: '../Servicios/ComisionAdministracion.asmx/consultaCuentas',
            data: function (d) {
                return JSON.stringify({ 'idCuenta': jsIdCuenta, 'idCliente': jsIdCliente, 'idGrupo': jsIdGrupo });
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd'
        },
        scrollY: 300,
        scrollX: true,
        scrollCollapse: false,
        fixedColumns: false,
        paging: false,
        info: false,
        searching: false,
        rowCallback: function () {
            $(window).trigger('resize');
        }
    });

    rows_selectCta = [];

    GetCuentasFiltroCuentas.on('draw', function () {
        updateCheckTable(GetCuentasFiltroCuentas);
        $('#ListaCuentasPorCuenta tbody input[type="checkbox"]:not(:checked)').trigger('click');
    });
}
function consultaCuentasPorCliente(jsIdCuenta, jsIdCliente, jsIdGrupo, tabla) {

    GetCuentasFiltroClientes = $(tabla).DataTable({
        data: {},
        columns: [
            {
                "searchable": false,
                "orderable": false,
                "defaultContent": "<div class='checkbox'><input value='' type='checkbox'><label></label></div>",
                width: '1%',
                className: 'dt-body-center',
            },
            { "data": "DSC_EMPRESA" },
            { "data": "NUM_CUENTA" },
            { "data": "DSC_CUENTA" },
        ],
        destroy: true,
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        ajax: {
            url: '../Servicios/ComisionAdministracion.asmx/consultaCuentas',
            data: function (d) {
                return JSON.stringify({ 'idCuenta': jsIdCuenta, 'idCliente': jsIdCliente, 'idGrupo': jsIdGrupo });
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd'
        },
        scrollY: 300,
        scrollX: true,
        scrollCollapse: false,
        fixedColumns: false,
        paging: false,
        info: false,
        searching: false,
        rowCallback: function () {
            $(window).trigger('resize');
        }
    });

    rows_selectCtaCli = [];

    GetCuentasFiltroClientes.on('draw', function () {
        updateCheckTable(GetCuentasFiltroClientes);
        $('#ListaCuentasPorCliente tbody input[type="checkbox"]:not(:checked)').trigger('click');
    });
}
function consultaCuentasPorGrupo(jsIdCuenta, jsIdCliente, jsIdGrupo, tabla) {

    GetCuentasFiltroGrupos = $(tabla).DataTable({
        data: {},
        columns: [
            {
                "searchable": false,
                "orderable": false,
                "defaultContent": "<div class='checkbox'><input value='' type='checkbox'><label></label></div>",
                width: '1%',
                className: 'dt-body-center',
            },
            { "data": "DSC_EMPRESA" },
            { "data": "NUM_CUENTA" },
            { "data": "DSC_CUENTA" },
        ],
        destroy: true,
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        ajax: {
            url: '../Servicios/ComisionAdministracion.asmx/consultaCuentas',
            data: function (d) {
                return JSON.stringify({ 'idCuenta': jsIdCuenta, 'idCliente': jsIdCliente, 'idGrupo': jsIdGrupo });
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd'
        },
        scrollY: 300,
        scrollX: true,
        scrollCollapse: false,
        fixedColumns: false,
        paging: false,
        info: false,
        searching: false,
        rowCallback: function () {
            $(window).trigger('resize');
        }
    });

    rows_selectCtaGru = [];

    GetCuentasFiltroGrupos.on('draw', function () {
        updateCheckTable(GetCuentasFiltroGrupos);
        $('#ListaCuentasPorGrupo tbody input[type="checkbox"]:not(:checked)').trigger('click');
    });
}

function inicioConsultarCuentas() {
    var jsIdCuenta = "";
    var jsIdCliente = "";
    var jsIdGrupo = "";

    if (target == "#tabCuenta") {
        jsIdCuenta = miInformacionCuenta == null ? "" : miInformacionCuenta.IdCuenta;
        if (miInformacionCuenta == null) {
            return;
        } else if (miInformacionCuenta !== null) {
            consultaCuentasPorCuenta(jsIdCuenta, jsIdCliente, jsIdGrupo, lista);
        }
    }

    if (target == "#tabCliente") {
        jsIdCliente = miInformacionCliente == null ? "" : miInformacionCliente.IdCliente;
        if (miInformacionCliente == null) {
            return;
        } else if (miInformacionCliente !== null) {
            consultaCuentasPorCliente(jsIdCuenta, jsIdCliente, jsIdGrupo, lista, GetCuentasFiltroClientes);
        }
    }

    if (target == "#tabGrupo") {
        jsIdGrupo = miInformacionGrupo == null ? "" : miInformacionGrupo.IdGrupo;
        if (miInformacionGrupo == null) {
            return;
        } else if (miInformacionGrupo !== null) {
            consultaCuentasPorGrupo(jsIdCuenta, jsIdCliente, jsIdGrupo, lista, GetCuentasFiltroGrupos);
        }
        if (miInformacionGrupo.CantidadCuentas == 0) {
            alert("El grupo no contiene cuentas");
            return;
        }
    }

}

function initReporteComisionAdministracion() {

    $("#idSubtituloPaginaText").text("Comisión Por Administración");
    document.title = "Comisión Por Administración";

    if (miInformacionCuenta == null) {
        GetCuentasFiltroCuentas = $('#ListaCuentasPorCuenta').DataTable({ destroy: true, language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" }, select: { style: 'os', selector: 'td:first-child' }, scrollY: 300, scrollX: true, scrollCollapse: false, fixedColumns: false, paging: false, info: false, searching: false, rowCallback: function () { $(window).trigger('resize'); } });
    }
    GetCuentasFiltroClientes = $('#ListaCuentasPorCliente').DataTable({ destroy: true, language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" }, select: { style: 'os', selector: 'td:first-child' }, scrollY: 300, scrollX: true, scrollCollapse: false, fixedColumns: false, paging: false, info: false, searching: false, rowCallback: function () { $(window).trigger('resize'); } });
    GetCuentasFiltroGrupos = $('#ListaCuentasPorGrupo').DataTable({ destroy: true, language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" }, select: { style: 'os', selector: 'td:first-child' }, scrollY: 300, scrollX: true, scrollCollapse: false, fixedColumns: false, paging: false, info: false, searching: false, rowCallback: function () { $(window).trigger('resize'); } });

    inicioConsultarCuentas();

    $("#dtRangoFecha").datepicker();
    $(".input-group.date").datepicker();
    if (!miInformacionCuenta) {
        $("#dtFechaConsulta").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
        $("#dtFechaConsultaHasta").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
        $("#dtFechaConsultaDesde").datepicker('setDate', moment(new Date(localStorage.UltimaFechaCierre)).add(-1, 'months').toDate());
    } else {
        $("#dtFechaConsulta").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
        $("#dtFechaConsultaHasta").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
        $("#dtFechaConsultaDesde").datepicker('setDate', moment($("#dtFechaConsultaHasta").datepicker('getDate')).add(-1, 'months').format('L'));
    }
    estadoBtnConsultar();
}

function cargarEventHandlersRepComAdm() {

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $('#tabsClienteCuentaGrupo a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        target = $(e.target).attr("href")
        if (target == "#tabCuenta") {
            $("#contenidoPorCuenta").show();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaCuentasPorCuenta";
            tablaLista = "ListaCuentasPorCuenta";
        }
        if (target == "#tabCliente") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").show();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaCuentasPorCliente";
            tablaLista = "ListaCuentasPorCliente";
        }
        if (target == "#tabGrupo") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").show();
            lista = "#ListaCuentasPorGrupo";
            tablaLista = "ListaCuentasPorGrupo";
        }
        estadoBtnConsultar();
        $(window).trigger('resize');
        resize();
    });

    $("#porCuenta_NroCuenta").change(function () {
        inicioConsultarCuentas();
        estadoBtnConsultar();
    });

    $("#btnGenerarDev").button().click(function () {
        var fechaDesde = moment.utc($("#dtFechaConsultaDesde").datepicker('getDate')).startOf('day').toISOString();
        var fechaHasta = moment.utc($("#dtFechaConsultaHasta").datepicker('getDate')).startOf('day').toISOString();

        generarReporte(fechaDesde, fechaHasta, 'Reporte Comisiones Devengadas', 'ExpReporteComisionDevengada');
    });

    $("#btnGenerarLiq").button().click(function () {
        var fecha = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
        generarReporte(fecha, '', 'Reporte Comisiones Liquidadas', 'ExpReporteComisionLiquidada');
    });

    $('#chkAllCta').on('click', function (e) {
        if (this.checked) {
            $('#ListaCuentasPorCuenta tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#ListaCuentasPorCuenta tbody input[type="checkbox"]:checked').trigger('click');
        }
        e.stopPropagation();
    });
    $('#chkAllCtaCli').on('click', function (e) {
        if (this.checked) {
            $('#ListaCuentasPorCliente tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#ListaCuentasPorCliente tbody input[type="checkbox"]:checked').trigger('click');
        }
        e.stopPropagation();
    });
    $('#chkAllCtaGru').on('click', function (e) {
        if (this.checked) {
            $('#ListaCuentasPorGrupo tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#ListaCuentasPorGrupo tbody input[type="checkbox"]:checked').trigger('click');
        }
        e.stopPropagation();
    });

    $('#ListaCuentasPorCuenta tbody').on('click', 'input[type="checkbox"]', function (e) {
        var data = GetCuentasFiltroCuentas.row($(this).parents('tr')).data();
        if (this.checked) {
            rows_selectCta.push(data.ID_CUENTA);
        } else if (!this.checked) {
            rows_selectCta.splice(rows_selectCta.indexOf(data.ID_CUENTA), 1);
        }
        updateCheckTable(GetCuentasFiltroCuentas);
        e.stopPropagation();
    });
    $('#ListaCuentasPorCliente tbody').on('click', 'input[type="checkbox"]', function (e) {
        var data = GetCuentasFiltroClientes.row($(this).parents('tr')).data();
        if (this.checked) {
            rows_selectCtaCli.push(data.ID_CUENTA);
        } else if (!this.checked) {
            rows_selectCtaCli.splice(rows_selectCtaCli.indexOf(data.ID_CUENTA), 1);
        }
        updateCheckTable(GetCuentasFiltroClientes);
        e.stopPropagation();
    });
    $('#ListaCuentasPorGrupo tbody').on('click', 'input[type="checkbox"]', function (e) {
        var data = GetCuentasFiltroGrupos.row($(this).parents('tr')).data();
        if (this.checked) {
            rows_selectCtaGru.push(data.ID_CUENTA);
        } else if (!this.checked) {
            rows_selectCtaGru.splice(rows_selectCtaGru.indexOf(data.ID_CUENTA), 1);
        }
        updateCheckTable(GetCuentasFiltroGrupos);
        e.stopPropagation();
    });

    $('#ListaCuentasPorCuenta').on('click', 'tbody td div[class="checkbox"], thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });
    $('#ListaCuentasPorCliente').on('click', 'tbody td div[class="checkbox"], thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });
    $('#ListaCuentasPorGrupo').on('click', 'tbody td div[class="checkbox"], thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });
}

function updateCheckTable(table) {

    if (target == "#tabCuenta") {
        var chkAll = 'chkAllCta';
    }
    if (target == "#tabCliente") {
        var chkAll = 'chkAllCtaCli';
    }
    if (target == "#tabGrupo") {
        var chkAll = 'chkAllCtaGru';
    }

    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[id=' + chkAll + ']', $table).get(0);

    if ($chkbox_checked.length === 0) {
        $('thead input[id=' + chkAll + ']').prop('checked', false)
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        $('thead input[id=' + chkAll + ']').prop('checked', true)
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }
    } else {
        $('thead input[id=' + chkAll + ']').prop('checked', true)
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

function estadoBtnConsultar() {
    if (target == "#tabCuenta") {
        $('#btnGenerarDev').attr('data-original-title', 'Seleccione Cuenta');
        $('#btnGenerarLiq').attr('data-original-title', 'Seleccione Cuenta');
        if (miInformacionCuenta == null) {
            $('#btnGenerarDev').tooltip('enable').addClass('disabled');
            $('#btnGenerarLiq').tooltip('enable').addClass('disabled');
        } else {
            $('#btnGenerarDev').tooltip('disable').tooltip('hide').removeClass('disabled');
            $('#btnGenerarLiq').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
    if (target == "#tabCliente") {
        $('#btnGenerarDev').attr('data-original-title', 'Seleccione Cliente');
        $('#btnGenerarLiq').attr('data-original-title', 'Seleccione Cliente');
        if (miInformacionCliente == null) {
            $('#btnGenerarDev').tooltip('enable').addClass('disabled');
            $('#btnGenerarLiq').tooltip('enable').addClass('disabled');
        } else {
            $('#btnGenerarDev').tooltip('disable').tooltip('hide').removeClass('disabled');
            $('#btnGenerarLiq').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
    if (target == "#tabGrupo") {
        $('#btnGenerarDev').attr('data-original-title', 'Seleccione Grupo');
        $('#btnGenerarLiq').attr('data-original-title', 'Seleccione Grupo');
        if (miInformacionGrupo == null) {
            $('#btnGenerarDev').tooltip('enable').addClass('disabled');
            $('#btnGenerarLiq').tooltip('enable').addClass('disabled');
        } else {
            $('#btnGenerarDev').tooltip('disable').tooltip('hide').removeClass('disabled');
            $('#btnGenerarLiq').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
}

$(document).ready(function () {
    initReporteComisionAdministracion();
    cargarEventHandlersRepComAdm();

    $(window).trigger('resize');
});