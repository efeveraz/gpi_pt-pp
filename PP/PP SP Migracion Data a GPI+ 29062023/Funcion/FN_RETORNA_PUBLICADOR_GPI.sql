if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FN_RETORNA_PUBLICADOR]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[FN_RETORNA_PUBLICADOR]
GO
CREATE FUNCTION [dbo].[FN_RETORNA_PUBLICADOR]( @PID_INSTRUMENTO numeric(10,0),@PID_CUENTA numeric (10,0) ) 
RETURNS  varchar(50)  AS
 
BEGIN

     DECLARE @PUBLICADOR VARCHAR(50)
      --DECLARE @ID_NEMOTECNICO NUMERIC(10,0)
      SET @PUBLICADOR =NULL
      SELECT @PUBLICADOR = P.DSC_PUBLICADOR 
      FROM REL_CUENTA_INSTRUMENTO_PUBLICA REL
      INNER JOIN VIEW_NEMOTECNICOS VN ON VN.COD_INSTRUMENTO = REL.COD_INSTRUMENTO
      INNER JOIN PUBLICADORES P ON P.ID_PUBLICADOR = REL.ID_PUBLICADOR
      WHERE ID_NEMOTECNICO=@PID_INSTRUMENTO AND ID_CUENTA =@PID_CUENTA
       IF (@PUBLICADOR IS NULL)
       BEGIN
      	SELECT @PUBLICADOR = P.DSC_PUBLICADOR 
      	FROM REL_EMPRESA_INSTRUMENTO_PUBLIC REL
      	INNER JOIN VIEW_NEMOTECNICOS VN ON VN.COD_INSTRUMENTO = REL.COD_INSTRUMENTO
      	INNER JOIN CUENTAS CTA ON CTA.ID_EMPRESA = REL.ID_EMPRESA
      	INNER JOIN PUBLICADORES P ON P.ID_PUBLICADOR = REL.ID_PUBLICADOR
		WHERE ID_NEMOTECNICO=@PID_INSTRUMENTO AND ID_CUENTA =@PID_CUENTA
      	--WHERE ID_NEMOTECNICO=11605 AND ID_CUENTA =1135
		
       END
      RETURN @PUBLICADOR
END 