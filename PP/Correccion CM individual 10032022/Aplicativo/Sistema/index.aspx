﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb.Master" AutoEventWireup="false" CodeBehind="index.aspx.vb" Inherits="AplicacionWeb.index" %>

<%@ Import Namespace="AplicacionWeb" %>
<%@ MasterType VirtualPath="~/Sistema/GPIWeb.Master" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPalFiltro" runat="Server">
    <script src='<%= VersionLinkHelper.GetVersion("../js/Stock/highstock.js") %>' type="text/javascript"></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/Stock/modules/exporting.js") %>' type="text/javascript"></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Sistema/index.js") %>'></script>

    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/chart/Chart.min.js") %>' type="text/javascript"></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/morris/morris.min.js") %>' type="text/javascript"></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/raphael/raphael.min.js") %>' type="text/javascript"></script>

    <div id="resumenUsuariosRoles" hidden>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel table-panel">
                    <div class="table-header">
                        <div class="table-caption">
                            Estados Usuarios
                        </div>
                    </div>
                    <div class="panel-body" style="text-align: -webkit-center;">
                        <canvas id="chartEstadosUsuarios" height="200"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="panel table-panel">
                    <div class="table-header">
                        <div class="table-caption">
                            Roles Usuarios
                        </div>
                    </div>
                    <div class="panel-body" style="text-align: -webkit-center;">
                        <div id="chartPerfilesUsuarios" style="height: 200px"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel table-panel">
                    <div class="table-header">
                        <div class="table-caption">
                            Inicios de sesión
                        </div>
                    </div>
                    <div class="panel-body" style="padding: 0px;">
                        <section id="secRegUsu" class='widget bg-widget-dolar text-white' style="margin-bottom: 0px; height: 230px">
                            <div class="widget-body clearfix">
                                <div class="row">
                                    <div class="col-xs-2" style="text-align: center;"><span id='codReg' class="widget-icon lh-1"><i class="fa fa-users"></i></span></div>
                                    <div class="col-xs-10">
                                        <h5 class="no-margin">Inicios de sesión hoy</h5>
                                        <p id='valorCantUsu' class="h2 no-margin fw-normal"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id='chartRegistroUsuarios' class='chart-overflow-bottom'></div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div id="resumenAdministrador" hidden>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="panel table-panel">
                    <div class="table-header">
                        <div class="table-caption" id="idTituloCierreCuentas">
                            
                        </div>
                    </div>
                    <div class="panel-body" style="text-align: -webkit-center;">
                        <div>
                               <div id="chartestadocuenta" style="height: 200px"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="panel table-panel">
                    <div class="table-header">
                        <div class="table-caption">
                            Cuentas 
                        </div>
                    </div>
                    <div class="panel-body" style="text-align: -webkit-center;">
                        <div>
                            <div id="chartcuenta" style="height: 200px"></div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="panel table-panel">
                    <div class="table-header">
                        <div class="table-caption">
                            Clientes
                        </div>
                    </div>
                    <div class="panel-body" style="text-align: -webkit-center;">
                         <div id="chart_Clientes" style="height: 200px"></div>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="panel table-panel">
                    <div class="table-header">
                        <div class="table-caption">
                           Cambio en el valor de los activos
                        </div>
                    </div>
                    <div class="panel-body" style="padding: 0px;">
                        <section id="secPatPeso" class='widget bg-widget-dolar text-white' style="margin-bottom: 0px; height: 230px">
                            <div class="widget-body clearfix">
                                <div class="row">
                                    <div class="col-xs-2" style="text-align: center;">
                                        <span id='codPatPeso' class="widget-icon lh-1"><i  id="Imagenpeso"></i></span>
                                    </div>
                                    <div class="col-xs-10">
                                        <h5 class="no-margin">Pesos</h5>
                                        <p id='valorCanPatPeso' class="h2 no-margin fw-normal"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id='chartSaldoActivoPeso' class='chart-overflow-bottom'></div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
             
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="panel table-panel">
                    <div class="table-header">
                        <div class="table-caption">
                            Cambio en el valor de los activos
                        </div>
                    </div>
                    <div class="panel-body" style="padding: 0px;">
                        <section id="secPatDolar" class='widget bg-widget-dolar text-white' style="margin-bottom: 0px; height: 230px">
                            <div class="widget-body clearfix">
                                <div class="row">
                                    <div class="col-xs-2" style="text-align: center;">
                                        <span id='codPatDolar' class="widget-icon lh-1">
                                            <i  id ="Imagendolar"></i></span>
                                    </div>
                                    <div class="col-xs-10">
                                        <h5 class="no-margin">USD</h5>
                                        <p id='valorCanPatDolar' class="h2 no-margin fw-normal"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id='chartSaldoActivoDolar' class='chart-overflow-bottom'></div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" id="divTblSalCuota" style="padding-bottom: 12px;">
                <div class="panel table-panel">
                    <div class="table-header">
                        <div class="table-caption">
                            Salto de Cuota 
                        </div>
                    </div>
                    <div class="table-primary responsive-table" style="height: 234px;">
                        <table id='tblSalCuota' class='table table-scroll table-striped table-hover table-bordered table-condensed table-fixed col-xs-12 col-sm-6 col-md-4 col-lg-4' style="border-right-width: 0px; border-left-width: 0px;">
                            <thead>
                                <tr class="tr-scroll" id="trTblSalCuota">
                                    <th>Cuenta</th>
                                    <th>Nombre</th>
                                    <th>Variación</th>
                                    <th>Limite</th>
                                </tr>
                            </thead>
                            <tbody class="tbody-scroll" style="height: 205px;"></tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div id="resumenCliente" hidden>
        <div class="row">
            <div class="col-sm-6">
                <div class="algo" id="grillaArbolActivos" style="margin-bottom: 13px;">
                    <table id="ListaArbolActivos">
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="divSupDer" class="panel table-panel" style="margin-bottom: 13px;">
                    <div id="cambioFecha" class="table-header">
                        <span class="glyphicon glyphicon-stats"></span>&nbsp;&nbsp;Resumen Árbol de Activos
                        <div class="form-group" style="display: inline-table; vertical-align: middle; margin: 0;">
                            <div class="input-group date" style="width: 150px">
                                <input type="text" id="dtFechaConsulta" class="form-control input-sm" style="">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <button type="button" id="BtnConsultar" class="btn btn-primary btn-sm">Actualizar</button>
                    </div>
                    <div class="panel-body" style="margin-bottom: 0px; padding: 1px;">
                        <div id="grafTortaClase"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div id="divInfIzq" class="panel table-panel">
                    <div class="table-header"><span class="glyphicon glyphicon-stats"></span>&nbsp;&nbsp;Rentabilidad Histórica</div>
                    <div class="panel-body" style="margin-bottom: 0px; padding: 1px;">
                        <div id="graficoPorGrupo"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="divInfDer" class="panel table-panel">
                    <div class="table-header"><span class="glyphicon glyphicon-stats"></span>&nbsp;&nbsp;Detalle Árbol de Activos</div>
                    <div class="panel-body" style="margin-bottom: 0px; padding: 1px;">
                        <div id="grafTortaSubClase"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

