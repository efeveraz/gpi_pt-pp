﻿
var urlVarTipo = getUrlVars()["tipo"];
var urlVarCodigo = getUrlVars()["Codigo"];
var urlVarDescripcion = getUrlVars()["Descripcion"];
var urlVarMercado = getUrlVars()["Mercado"];
var urlVarMoneda = getUrlVars()["Moneda"];
var urlVarSector = getUrlVars()["Sector"];
var urlVarAgrupadoPor = getUrlVars()["AgrupadoPor"];
var urlVarFechaConsulta = getUrlVars()["FechaConsulta"]
var target = "#tabCuenta";
var jsfechaConsulta
var jsfechaFormato

var vColModel = [];
var vColNames = [];

var lista;
var urlDescripcionMercado;
var urlDescripcionMoneda;
var urlDescripcionSector;

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#paginadorPorCuenta").outerHeight(true) - $("#paginadorPorCliente").outerHeight(true) - $("#paginadorPorGrupo").outerHeight(true) - 37 - 84 - 26;

    if (ancho < 768) {
        $('#ListaPorCuenta').setGridHeight(220);
        $('#ListaPorCliente').setGridHeight(220);
        $('#ListaPorGrupo').setGridHeight(220);
    } else {
        $('#ListaPorCuenta').jqGrid('setGridHeight', height);
        $('#ListaPorCliente').jqGrid('setGridHeight', height);
        $('#ListaPorGrupo').jqGrid('setGridHeight', height);
    }

    $("#ListaPorCuenta").setGridWidth($("#grillaPorCuenta").width() - 2);
    $("#ListaPorCliente").setGridWidth($("#grillaPorCliente").width() - 2);
    $("#ListaPorGrupo").setGridWidth($("#grillaPorGrupo").width() - 2);

}

function initCarteraDetalleInstrumento() {
    $("#idSubtituloPaginaText").text("Detalle Instrumento: " + urlVarDescripcion);
    document.title = "Detalle Instrumento: " + urlVarDescripcion;

    $("#btnBuscarCuentas").hide();
    $("#btnBuscarClientes").hide();
    $("#btnBuscarGrupos").hide();
    $("#btnLimpiarCuentas").hide();
    $("#btnLimpiarClientes").hide();
    $("#btnLimpiarGrupos").hide();

    lista = urlVarAgrupadoPor == null ? "#ListaPorCuenta" : urlVarAgrupadoPor;

    if (typeof urlVarFechaConsulta !== "undefined") {
        $(".input-group.date").datepicker('setDate', urlVarFechaConsulta);
    } else {
        jsfechaConsulta = moment($(".input-group.date").datepicker('getDate')).format("L");
        urlVarFechaConsulta = jsfechaConsulta;
    }
    if (typeof urlVarMercado !== "undefined") {
        urlDescripcionMercado = getUrlVars()["DescripcionMercado"];
        $("#idSubtituloPaginaText").text("Detalle instrumento : " + urlVarDescripcion + " - Mercado: " + urlDescripcionMercado);
        document.title = "Detalle instrumento : " + urlVarDescripcion + " - Mercado: " + urlDescripcionMercado;
    } else {
        urlVarMercado = "";
    }
    if (typeof urlVarMoneda !== "undefined") {
        urlDescripcionMoneda = getUrlVars()["DescripcionMoneda"];
        $("#idSubtituloPaginaText").text("Detalle instrumento : " + urlVarDescripcion + " - Moneda: " + urlDescripcionMoneda);
        document.title = "Detalle instrumento : " + urlVarDescripcion + " - Moneda: " + urlDescripcionMoneda;
    } else {
        urlVarMoneda = "";
    }
    if (typeof urlVarSector !== "undefined") {
        urlDescripcionSector = getUrlVars()["DescripcionSector"];
        $("#idSubtituloPaginaText").text("Detalle instrumento : " + urlVarDescripcion + " - Sector: " + urlDescripcionSector);
        document.title = "Detalle instrumento : " + urlVarDescripcion + " - Sector: " + urlDescripcionSector;
    } else {
        urlVarSector = "";
    }

    if (urlVarAgrupadoPor == "#ListaPorCuenta") {
        $("#contenidoPorCuenta").show();
        $("#contenidoPorCliente").hide();
        $("#contenidoPorGrupo").hide();
        $("#tabsClienteCuentaGrupo").hide();
        $('.nav-tabs a[href="#tabCuenta"]').tab('show');
    } else if (urlVarAgrupadoPor == "#ListaPorCliente") {
        $("#contenidoPorCuenta").hide();
        $("#contenidoPorCliente").show();
        $("#contenidoPorGrupo").hide();
        $("#tabsClienteCuentaGrupo").hide();
        $('.nav-tabs a[href="#tabCliente"]').tab('show');
    } else if (urlVarAgrupadoPor == "#ListaPorGrupo") {
        $("#contenidoPorCuenta").hide();
        $("#contenidoPorCliente").hide();
        $("#contenidoPorGrupo").show();
        $("#tabsClienteCuentaGrupo").hide();
        $('.nav-tabs a[href="#tabGrupo"]').tab('show');
    } else {
        $("#contenidoPorCuenta").show();
        $("#contenidoPorCliente").hide();
        $("#contenidoPorGrupo").hide();
    }

    creaColumnasOrden();
    habilitaColumnas();
}

function eventHandlersCarteraDetalleInstrumento() {

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $(".input-group.date").datepicker();

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        target = $(e.target).attr("href") // activated tab
        if (target == "#tabCuenta") {
            $("#contenidoPorCuenta").show();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaPorCuenta";
        }
        if (target == "#tabCliente") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").show();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaPorCliente";
        }
        if (target == "#tabGrupo") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").show();
            lista = "#ListaPorGrupo";
        }
        resize();
    });

    var grilla = {
        shrinkToFit: false,
        loadonce: true,
        datatype: "local",
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records" },
        colNames: vColNames,
        colModel: vColModel,
        pager: "#paginadorPorCuenta",
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        width: '100%',
        height: "100%",
        styleUI: 'Bootstrap',
        caption: "Detalle Instrumento",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: true,
        groupingView: {
            groupField: ['DSC_NEMOTECNICO'],
            groupSummaryPos: ['header'],
            groupCollapse: true,
            groupSummary: [true]
        },
        onCellSelect: function (rowid) {
            if (urlVarTipo != "FWD" && urlVarTipo != "SMLTV") {
                var href;
                vIdNemotecnico = $(lista).jqGrid("getCell", rowid, "ID_NEMOTECNICO");
                Descripcion = $(lista).jqGrid("getCell", rowid, "NEMOTECNICO");
                if (!Descripcion) {
                    Descripcion = $(lista).jqGrid("getCell", rowid, "DSC_NEMOTECNICO");
                }
                Descripcion = Descripcion.replace("%", "%25");
                var href = ""
                if (urlVarMercado != "") {
                    href = "CarteraListadoMovimientos.aspx?idNemo=" + vIdNemotecnico + "&Descripcion=" + Descripcion + "&Mercado=" + urlVarMercado + "&DescripcionMercado=" + urlDescripcionMercado + "&FechaConsulta=" + urlVarFechaConsulta + "&AgrupadoPor=" + lista
                } else if (urlVarMoneda != "") {
                    href = "CarteraListadoMovimientos.aspx?idNemo=" + vIdNemotecnico + "&Descripcion=" + Descripcion + "&Moneda=" + urlVarMoneda + "&DescripcionMoneda=" + urlDescripcionMoneda + "&FechaConsulta=" + urlVarFechaConsulta + "&AgrupadoPor=" + lista
                } else if (urlVarSector != "") {
                    href = "CarteraListadoMovimientos.aspx?idNemo=" + vIdNemotecnico + "&Descripcion=" + Descripcion + "&Sector=" + urlVarSector + "&DescripcionSector=" + urlDescripcionSector + "&FechaConsulta=" + urlVarFechaConsulta + "&AgrupadoPor=" + lista
                } else {
                    href = "CarteraListadoMovimientos.aspx?idNemo=" + vIdNemotecnico + "&Descripcion=" + Descripcion + "&FechaConsulta=" + urlVarFechaConsulta + "&AgrupadoPor=" + lista
                }
                window.location.href = '../Views/' + href;
            }
        }
    }

    grilla.pager = "#paginadorPorCuenta";
    grilla.caption = "Instrumentos Por Cuenta";
    $("#ListaPorCuenta").jqGrid(grilla);
    grilla.pager = "#paginadorPorCliente";
    grilla.caption = "Instrumentos Por Cliente";
    $("#ListaPorCliente").jqGrid(grilla);
    grilla.pager = "#paginadorPorGrupo";
    grilla.caption = "Instrumentos Por Grupo";
    $("#ListaPorGrupo").jqGrid(grilla);

    $("#ListaPorCuenta").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorCuenta").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaPorCuenta").jqGrid('navGrid', '#paginadorPorCuenta', { add: false, edit: false, del: false, excel: true, search: false });
    $("#ListaPorCuenta").jqGrid('navButtonAdd', '#paginadorPorCuenta', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaPorCuenta', 'ListaPorCuenta', 'List. Por Cuenta', '');
        }
    });
    $('#paginadorPorCuenta_left').width('auto');

    $("#ListaPorCliente").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorCliente").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaPorCliente").jqGrid('navGrid', '#paginadorPorCliente', { add: false, edit: false, del: false, excel: true, search: false });
    $("#ListaPorCliente").jqGrid('navButtonAdd', '#paginadorPorCliente', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaPorCliente', 'ListaPorCliente', 'List. Por Cliente', '');
        }
    });

    $("#ListaPorGrupo").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorGrupo").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaPorGrupo").jqGrid('navGrid', '#paginadorPorGrupo', { add: false, edit: false, del: false, excel: true, search: false });
    $("#ListaPorGrupo").jqGrid('navButtonAdd', '#paginadorPorGrupo', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaPorGrupo', 'ListaPorGrupo', 'List. Por Grupo', '');
        }
    });

    $("#BtnConsultar").on('click', function () {
        consultaDetalleInstrumento("N");
    });

    $("#porCuenta_NroCuenta").on('change', function () {
        consultaDetalleInstrumento("S");
    });
}

function creaColumnasOrden() {
    if (urlVarTipo == "RV" || urlVarTipo == "RVN" || urlVarTipo == "RVI" || urlVarTipo == "RVINT") {
        vColModel = [
            { name: "DSC_NEMOTECNICO", index: "DSC_NEMOTECNICO", sortable: true, width: 220, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "ID_NEMOTECNICO", index: "ID_NEMOTECNICO", sortable: true, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 70, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "SIMBOLO_MONEDA", index: "SIMBOLO_MONEDA", sortable: true, width: 70, sorttype: "text", editable: false, search: true, align: "center", hidden: false },
            { name: "PORCENTAJE_RAMA", index: "PORCENTAJE_RAMA", sortable: true, width: 100, sorttype: "currency", editable: false, formatter: "currency", formatoptions: { suffix: '%' }, search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 110, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", summaryType: "sum", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "PRECIO_COMPRA", index: "PRECIO_PROMEDIO_COMPRA", sortable: true, width: 140, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_PROMEDIO_COMPRA", index: "MONTO_PROMEDIO_COMPRA", sortable: true, width: 110, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "PRECIO", index: "PRECIO", sortable: true, width: 110, sorttype: "number", formatter: "number", editable: false, search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_MON_NEMOTECNICO", index: "MONTO_MON_NEMOTECNICO", sortable: true, width: 130, sorttype: "currency", editable: false, formatter: "currency", search: true, align: "right", summaryType: "sum", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "RENTABILIDAD", index: "RENTABILIDAD", sortable: true, width: 120, sorttype: "currency", editable: false, formatter: "currency", formatoptions: { suffix: '%' }, search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
        ];
    }
    if (urlVarTipo == "RF" || urlVarTipo == "RFN" || urlVarTipo == "RFI" || urlVarTipo == "RFINT") {
        vColModel = [
            { name: "DSC_NEMOTECNICO", index: "DSC_NEMOTECNICO", sortable: false, width: 220, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "ID_NEMOTECNICO", index: "ID_NEMOTECNICO", sortable: true, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 70, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "COD_EMISOR", index: "COD_EMISOR", sortable: false, width: 100, sorttype: "text", editable: false, search: true, align: "center", hidden: false },
            { name: "SIMBOLO_MONEDA", index: "SIMBOLO_MONEDA", sortable: true, width: 70, sorttype: "text", editable: false, search: true, align: "center", hidden: false },
            { name: "PORCENTAJE_RAMA", index: "PORCENTAJE_RAMA", sortable: true, width: 110, sorttype: "currency", editable: false, formatter: "currency", formatoptions: { suffix: '%' }, search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 130, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", summaryType: "sum", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "TASA_EMISION", index: "TASA_EMISION", sortable: true, width: 90, sorttype: "number", editable: false, search: true, formatter: "number", align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "FECHA_VENCIMIENTO", index: "FECHA_VENCIMIENTO", sortable: true, width: 130, sorttype: "date", formatter: "date", editable: false, search: true, align: "center", hidden: false, searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "TASA_COMPRA", index: "TASA_COMPRA", sortable: true, width: 100, sorttype: "number", editable: false, search: true, formatter: "number", align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "TASA", index: "TASA", sortable: true, width: 100, sorttype: "number", editable: false, search: true, formatter: "number", align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "VARIACION_PRECIO", index: "VARIACION_PRECIO", sortable: true, width: 110, sorttype: "currency", editable: false, search: true, formatter: "currency", formatoptions: { suffix: '%' }, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_MON_CTA", index: "MONTO_MON_CTA", sortable: true, width: 120, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", summaryType: "sum", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
        ];
    }
    if (urlVarTipo == "FFMM" || urlVarTipo == "CFIX") {
        vColModel = [
            { name: "DSC_NEMOTECNICO", index: "DSC_NEMOTECNICO", sortable: true, width: 220, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "ID_NEMOTECNICO", index: "ID_NEMOTECNICO", sortable: true, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 70, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "SIMBOLO_MONEDA", index: "SIMBOLO_MONEDA", sortable: true, width: 70, sorttype: "text", editable: false, search: true, align: "center", hidden: false },
            { name: "PORCENTAJE_RAMA", index: "PORCENTAJE_RAMA", sortable: true, width: 100, sorttype: "currency", editable: false, formatter: "currency", formatoptions: { suffix: '%' }, search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 120, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", summaryType: "sum", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "PRECIO_COMPRA", index: "PRECIO_COMPRA", sortable: true, width: 230, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_MON_NEMOTECNICO", index: "MONTO_MON_NEMOTECNICO", sortable: true, width: 110, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_MON_CTA", index: "MONTO_MON_CTA", sortable: true, width: 120, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", summaryType: "sum", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "RENTABILIDAD", index: "RENTABILIDAD", sortable: true, width: 120, sorttype: "currency", editable: false, formatter: "currency", formatoptions: { suffix: '%' }, search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
        ];
    }
    if (urlVarTipo == "FWD") {
        vColModel = [
            { name: "NRO_CONTRATO", index: "NRO_CONTRATO", sortable: true, width: 220, sorttype: "text", editable: false, search: true, align: "right", hidden: false },
            { name: "ID_CUENTA", index: "ID_CUENTA", sortable: true, width: 70, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", hidden: true },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 70, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "TIPO_FWD", index: "TIPO_FWD", sortable: true, width: 80, sorttype: "text", editable: false, search: true, align: "left", hidden: false },
            { name: "FCH_OPERACION", index: "FCH_OPERACION", sortable: true, width: 120, sorttype: "date", formatter: "date", editable: false, search: true, align: "center", hidden: false, searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "FCH_VENCIMIENTO", index: "FCH_VENCIMIENTO", sortable: true, width: 120, sorttype: "date", formatter: "date", editable: false, search: true, align: "center", hidden: false, searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "PLAZO", index: "PLAZO", sortable: true, width: 80, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", hidden: false },
            { name: "DSC_TIPO_CUMPLIMIENTO", index: "DSC_TIPO_CUMPLIMIENTO", sortable: true, width: 80, sorttype: "text", editable: false, formatter: "text", search: true, align: "left", hidden: false },
            { name: "PARIDAD", index: "PARIDAD", sortable: true, width: 80, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "VALOR_NOMINAL", index: "VALOR_NOMINAL", sortable: true, width: 110, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "VALOR_PESOS", index: "VALOR_PESOS", sortable: true, width: 110, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "VMM", index: "VMM", sortable: true, width: 80, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 80, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", hidden: true }
        ];
    }
};
function habilitaColumnas() {
    if (urlVarTipo == "RV" || urlVarTipo == "RVN" || urlVarTipo == "RVI" || urlVarTipo == "RVINT") {
        vColNames = ["Nemotecnico", "ID_NEMOTECNICO", "Cuenta", "Moneda", "Inversión", "Cantidad Total", "Pr. Prom. Com.", "Mto Prom. Com.", "Precio Actual", "Valor Mercado", "Rentabilidad"];
    }
    if (urlVarTipo == "RF" || urlVarTipo == "RFN" || urlVarTipo == "RFI" || urlVarTipo == "RFINT") {
        vColNames = ["Nemotecnico", "ID_NEMOTECNICO", "Cuenta", "Emisor", "Moneda", "Inversión", "Total Nominales", "Tasa Cupón", "Fecha Vencimiento", "Tasa Compra", "Tasa Mercado", "Variación Tasa", "Valorización"];
    }
    if (urlVarTipo == "FFMM" || urlVarTipo == "CFI") {
        vColNames = ["Nemotecnico", "ID_NEMOTECNICO", "Cuenta", "Moneda", "% Inversion", "Cantidad Cuotas", "Valor Cuota Promedio Compra", "Valor Mda", "Valor Actual", "% Rentabilidad"];
    }
    if (urlVarTipo == "FWD") {
        vColNames = ["Contrato", "ID_CUENTA", "Cuenta", "Tipo", "Fecha Inicio", "Fecha  Vencimiento", "Plazo", "Modalidad", "Valor Futuro", "Valor Nominal", "Valor en Pesos", "Valor Mercado", "NUM_CUENTA"];
    }
};
function consultaDetalleInstrumento(strCargaInicial) {
    jsfechaConsulta = document.getElementById("dtFechaConsulta").value;
    var jsIdCuenta = "";
    var jsIdCliente = "";
    var jsIdGrupo = "";

    if (target == "#tabCuenta") {
        jsIdCuenta = miInformacionCuenta == null ? "" : miInformacionCuenta.IdCuenta;
        if (miInformacionCuenta == null && lista == "#ListaPorCuenta") {
            if (strCargaInicial == "N") {
                alert("Primero debe seleccionar una cuenta");
            }
            return;
        }
    }
    if (target == "#tabCliente") {
        jsIdCliente = miInformacionCliente == null ? "" : miInformacionCliente.IdCliente;
        if (miInformacionCliente == null && lista == "#ListaPorCliente") {
            if (strCargaInicial == "N") {
                alert("Primero debe seleccionar un cliente");
            }
            return;
        }
    }
    if (target == "#tabGrupo") {
        jsIdGrupo = miInformacionGrupo == null ? "" : miInformacionGrupo.IdGrupo;
        if (miInformacionGrupo == null && lista == "#ListaPorGrupo") {
            if (strCargaInicial == "N") {
                alert("Primero debe seleccionar un grupo");
            }
            return;
        }
    }
    if (jsfechaConsulta === "") {
        if (strCargaInicial == "N") {
            alert("No ha indicado fecha de consulta");
        }
        return;
    }
    $(lista).jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    jsfechaFormato = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();
    $.ajax({
        url: "CarteraDetalleInstrumento.aspx/ConsultaDetalleInstrumento",
        data: "{'dfechaConsulta':'" + jsfechaFormato.substring(0, jsfechaFormato.length - 1) +
            "', 'idCuenta':'" + jsIdCuenta +
            "', 'idCliente':'" + jsIdCliente +
            "', 'idGrupo':'" + jsIdGrupo +
            "', 'tipo':'" + urlVarTipo +
            "', 'ClaseArbol':'" + urlVarCodigo +
            "', 'intMercado':'" + urlVarMercado +
            "', 'intMoneda':'" + urlVarMoneda +
            "', 'intSector':'" + urlVarSector + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $(lista).setGridParam({ data: mydata });
                $(lista).trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
            $(window).trigger('resize');
        }
    });
};

$(document).ready(function () {    
    initCarteraDetalleInstrumento();
    eventHandlersCarteraDetalleInstrumento();
    consultaDetalleInstrumento("S");
    resize();
});