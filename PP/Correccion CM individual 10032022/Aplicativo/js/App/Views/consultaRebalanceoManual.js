﻿var jsfechaConsulta;

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#PaginadorRV").outerHeight(true) - $("#PaginadorRF").outerHeight(true) - $("#PaginadorFFMM").outerHeight(true) - 228;
    if (ancho < 768) {
        $("#ListaOperacionesRV").setGridHeight(190);
        $("#ListaOperacionesRF").setGridHeight(190);
        $("#ListaOperacionesFFMM").setGridHeight(190);
    } else {
        $("#ListaOperacionesRV").setGridHeight(height);
        $("#ListaOperacionesRF").setGridHeight(height);
        $("#ListaOperacionesFFMM").setGridHeight(height);
    }
}

function initConsultaRebalanceoManual() {
    $("#idSubtituloPaginaText").text("Consulta Rebalanceo Manual");
    document.title = "Consulta Rebalanceo Manual";

    cargarInstrumentos();

    if (!miInformacionCuenta) {
        $(".input-group.date").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
    } else {
        $(".input-group.date").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
    }

    cargaFechaCierre();

    jsfechaConsulta = document.getElementById("dtFechaConsulta").value;
    var arrFechaDesde = jsfechaConsulta.split(strSeparadorFecha);
}

function eventHandlersConsultaRebalanceoManual() {
    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(window).trigger('resize');
    });

    var opt = {
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID_OPERACION" },
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        height: '100px',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        sortname: "NUM_CUENTA",
        sortorder: "asc",
        caption: "Rebalanceo Manual",
        ignoreCase: true,
        hidegrid: false,
        grouping: false,
        regional: localeCorto,
        groupingView: {
            groupField: ['NUM_CUENTA'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        }
    }
    var rvColNames = ['Cuenta', 'Nombre Corto', 'Asesor', 'Nemotécnico', 'Moneda Cuenta', 'Cantidad', 'Precio Promedio Compra', 'Monto Promedio Compra', 'Precio Actual', 'Valor Mercado', '% Rentabilidad'];
    var rvModel = [
        { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 100, sorttype: "int", formatter: "int", editable: false, search: true, hidden: false },
        { name: "DSC_CUENTA", index: "DSC_CUENTA", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "NOMBRE", index: "NOMBRE", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "NEMOTECNICO", index: "NEMOTECNICO", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "SIMBOLO_MONEDA_CUENTA", index: "SIMBOLO_MONEDA_CUENTA", sortable: true, width: 110, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 100, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 0 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "PRECIO_COMPRA", index: "PRECIO_COMPRA", sortable: true, width: 180, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "MONTO_PROMEDIO_COMPRA", index: "MONTO_PROMEDIO_COMPRA", sortable: true, width: 180, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "PRECIO", index: "PRECIO", sortable: true, width: 110, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "MONTO_MON_CTA", index: "MONTO_MON_CTA", sortable: true, width: 120, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "RENTABILIDAD", index: "RENTABILIDAD", sortable: true, width: 110, sorttype: "number", formatter: 'number', formatoptions: { suffix: '%', decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
    ];
    var rfColNames = ['Cuenta', 'Nombre Corto', 'Asesor', 'Nemotécnico', 'Moneda Cuenta', 'Cantidad', 'Tasa Cupón', 'Tasa Compra', 'Tasa Mercado', 'Monto Promedio Compra', 'Valorización'];
    var rfModel = [
        { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 100, sorttype: "int", formatter: "int", editable: false, search: true, hidden: false },
        { name: "DSC_CUENTA", index: "DSC_CUENTA", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "NOMBRE", index: "NOMBRE", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "NEMOTECNICO", index: "NEMOTECNICO", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "SIMBOLO_MONEDA_CUENTA", index: "SIMBOLO_MONEDA_CUENTA", sortable: true, width: 110, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 110, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "TASA_EMISION", index: "TASA_EMISION", sortable: true, width: 110, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "TASA_COMPRA", index: "TASA_COMPRA", sortable: true, width: 110, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "TASA", index: "TASA", sortable: true, width: 110, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "MONTO_VALOR_COMPRA", index: "MONTO_VALOR_COMPRA", sortable: true, width: 130, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "MONTO_MON_CTA", index: "MONTO_MON_CTA", sortable: true, width: 130, sorttype: "number", formatter: 'number', formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
    ];
    var ffmmColNames = ['Cuenta', 'Nombre Corto', 'Asesor', 'Nemotécnico', 'Moneda Cuenta', 'Cantidad de Cuotas', 'Valor Cuota Promedio Compra', 'Monto Promedio Compra', 'Valor Cuota Actual', 'Valor Actual', '% Rentabilidad'];
    var ffmmModel = [
        { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 100, sorttype: "int", formatter: "int", editable: false, search: true, hidden: false },
        { name: "DSC_CUENTA", index: "DSC_CUENTA", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "NOMBRE", index: "NOMBRE", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "NEMOTECNICO", index: "NEMOTECNICO", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "SIMBOLO_MONEDA_CUENTA", index: "SIMBOLO_MONEDA_CUENTA", sortable: true, width: 110, sorttype: "text", editable: false, search: true, hidden: false },
        { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 130, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "PRECIO_COMPRA", index: "PRECIO_COMPRA", sortable: true, width: 220, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "MONTO_PROMEDIO_COMPRA", index: "MONTO_PROMEDIO_COMPRA", sortable: true, width: 180, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "PRECIO", index: "PRECIO", sortable: true, width: 130, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "MONTO_MON_CTA", index: "MONTO_MON_CTA", sortable: true, width: 130, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "RENTABILIDAD", index: "RENTABILIDAD", sortable: true, width: 130, sorttype: "number", formatter: 'number', formatoptions: { suffix: '%', decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
    ]

    opt.colNames = rvColNames;
    opt.colModel = rvModel;
    opt.pager = '#PaginadorRV',
        $("#ListaOperacionesRV").jqGrid(opt);
    $("#ListaOperacionesRV").jqGrid().bindKeys().scrollingRows = true
    $("#ListaOperacionesRV").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaOperacionesRV").jqGrid('navGrid', '#PaginadorRV', { add: false, edit: false, del: false, excel: false, search: false });
    $("#ListaOperacionesRV").jqGrid('navButtonAdd', '#PaginadorRV', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaOperacionesRV', 'Rebalanceo Manual Renta Variable', 'Rebalanceo Manual Renta Variable', '');
        }
    });
    opt.colNames = rfColNames;
    opt.colModel = rfModel;
    opt.pager = '#PaginadorRF',
        $("#ListaOperacionesRF").jqGrid(opt);
    $("#ListaOperacionesRF").jqGrid().bindKeys().scrollingRows = true
    $("#ListaOperacionesRF").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaOperacionesRF").jqGrid('navGrid', '#PaginadorRF', { add: false, edit: false, del: false, excel: false, search: false });
    $("#ListaOperacionesRF").jqGrid('navButtonAdd', '#PaginadorRF', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaOperacionesRF', 'Rebalanceo Manual Renta Fija', 'Rebalanceo Manual Renta Fija', '');
        }
    });
    opt.colNames = ffmmColNames;
    opt.colModel = ffmmModel;
    opt.pager = '#PaginadorFFMM',
        $("#ListaOperacionesFFMM").jqGrid(opt);
    $("#ListaOperacionesFFMM").jqGrid().bindKeys().scrollingRows = true
    $("#ListaOperacionesFFMM").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaOperacionesFFMM").jqGrid('navGrid', '#PaginadorFFMM', { add: false, edit: false, del: false, excel: false, search: false });
    $("#ListaOperacionesFFMM").jqGrid('navButtonAdd', '#PaginadorFFMM', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaOperacionesFFMM', 'Rebalanceo Manual Fondos Mutuos', 'Rebalanceo Manual Fondos Mutuos', '');
        }
    });

    $("#BtnConsultar").button().click(function () {
        consultaOperaciones();
    });
}

function cargaFechaCierre() {
    $.ajax({
        url: "../Servicios/Globales.asmx/BuscarUltimaFechaCierre",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                document.getElementById("dtFechaConsulta").value = moment(JSON.parse(jsondata.responseText).d).format('DD-MM-YYYY');
            }
            else {
                alert(JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function groupBy(arr, property) {
    return arr.reduce(function (memo, x) {
        if (!memo[x[property]]) { memo[x[property]] = []; }
        memo[x[property]].push(x);
        return memo;
    }, {});
}
function consultaOperaciones() {
    var jsfechaFormato = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1);

    var jsCodInstrumento = $("#DDInstrumento").val();
    var jsAsesor = $("#DDAsesor").val();

    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }

    $('#ListaOperacionesRV').jqGrid('clearGridData');
    $('#ListaOperacionesRF').jqGrid('clearGridData');
    $('#ListaOperacionesFFMM').jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaRebalanceoManual.aspx/ConsultaRebalanceoManual",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'CodInstrumento':" + (jsCodInstrumento == -1 ? null : "'" + jsCodInstrumento + "'") + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
                else {
                    var o = groupBy(mydata, 'COD_PRODUCTO');
                    $("#ListaOperacionesRV").setGridParam({ data: o.RV_NAC });
                    $("#ListaOperacionesRF").setGridParam({ data: o.RF_NAC });
                    $("#ListaOperacionesFFMM").setGridParam({ data: o.FFMM_NAC });
                    $("#ListaOperacionesRV").trigger("reloadGrid");
                    $("#ListaOperacionesRF").trigger("reloadGrid");
                    $("#ListaOperacionesFFMM").trigger("reloadGrid");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
};
function cargarInstrumentos() {
    $.ajax({
        url: "../Servicios/Globales.asmx/consultaInstrumentos",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                $("#DDInstrumento").empty();
                $("#DDInstrumento").prepend('<option value="' + -1 + '">' + 'Todos los Instrumentos' + '</option>');
                $.each(res, function (key, val) {
                    $("#DDInstrumento").append('<option value="' + val.COD_INSTRUMENTO + '">' + val.DSC_INTRUMENTO + '</option>');
                });
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

$(document).ready(function () {
    initConsultaRebalanceoManual();
    eventHandlersConsultaRebalanceoManual();

    $(window).trigger('resize');
});