﻿var jsfechaConsulta;

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#Paginador").outerHeight(true) - 145;

    if (ancho < 768) {
        $("#ListaFlujoCaja").setGridHeight(210);
    } else {
        $("#ListaFlujoCaja").setGridHeight(height);
    }
}

function initConsultaFlujoCaja() {

    $("#idSubtituloPaginaText").text("Consulta Flujo Caja");
    document.title = "Consulta Flujo Caja";
    $("#tabsClienteCuentaGrupo").hide();

    if (!miInformacionCuenta) {
        $(".input-group.date").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
    } else {
        $(".input-group.date").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
    }

    $('.nav-tabs a[href="#tabCuenta"]').tab('show');
}

function cargarEventHandlersConsultaFlujoCaja() {

    $(window).bind('resize', function () {
        resize()
    }).trigger('resize');

    $(".input-group.date").datepicker();

    $("#ListaFlujoCaja").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID_OPERACION" },
        colNames: ['Cuenta', 'Rut', 'Cliente', 'Asesor', 'Moneda', 'Disponible T+0', 'Retenidos T+1', 'Retenidos T+2', 'Moneda Caja 2', 'Saldo Caja 2'],
        colModel: [
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 100, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "RUT_CLIENTE", index: "RUT_CLIENTE", sortable: true, width: 100, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NOMBRE_CLIENTE", index: "NOMBRE_CLIENTE", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NOMBRE_ASESOR", index: "NOMBRE_ASESOR", sortable: true, width: 160, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "SIMBOLO", index: "SIMBOLO", sortable: true, width: 80, sorttype: "text", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_T0", index: "MONTO_T0", sortable: true, width: 120, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_T1", index: "MONTO_T1", sortable: true, width: 120, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_T2", index: "MONTO_T2", sortable: true, width: 120, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "SIMBOLOCAJA2", index: "SIMBOLOCAJA2", sortable: true, width: 110, sorttype: "text", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "SALDOCAJA2", index: "SALDOCAJA2", sortable: true, width: 110, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
        ],
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        sortname: "ID_CUENTA",
        sortorder: "asc",
        caption: "Flujo Caja",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false,
        groupingView: {
            groupField: ['ID_CUENTA'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        }
    });
    $("#ListaFlujoCaja").jqGrid().bindKeys().scrollingRows = true
    $("#ListaFlujoCaja").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaFlujoCaja").jqGrid('navGrid', '#Paginador', { add: false, edit: false, del: false, excel: false, search: false });
    $("#ListaFlujoCaja").jqGrid('navButtonAdd', '#Paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaFlujoCaja', 'Flujo Caja', 'Flujo Caja', '');
        }
    });

    $("#BtnConsultar").button().click(function () {
        consultaFlujoCaja();
        resize();
    });
}

function consultaFlujoCaja() {
    var jsfechaFormato = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1);

    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }

    $("#ListaFlujoCaja").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaFlujoCaja.aspx/ConsultaFlujoCaja",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'idCuenta':" + (miInformacionCuenta == null ? "''" : miInformacionCuenta.IdCuenta) + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#ListaFlujoCaja").setGridParam({ data: mydata });
                $("#ListaFlujoCaja").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
};

$(document).ready(function () {
    initConsultaFlujoCaja();
    cargarEventHandlersConsultaFlujoCaja();
    resize();
});