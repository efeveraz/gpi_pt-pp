﻿var jsfechaConsulta;
var graficoPorGrupo;
var graphRegistro;
var graphSaldoActivoDolar;
var graphSaldoActivoPeso;
var rolusuario;

function resize() {
    var height;

    var ancho = $("#grillaArbolActivos").width() - 15;
    if ($("#content").outerHeight(true) < 500) {
        height = 300;
    }
    else {
        height = ($("#content").outerHeight(true) - 15) / 2;
    }

    $("#ListaArbolActivos").setGridWidth(ancho);
    $("#divSupIzq").height(height);

    $("#ListaArbolActivos").setGridHeight(height - 95);

    $("#divSupDer").height(height - 20);
    $("#grafTortaClase").height(height - 20 - ($("#cambioFecha").outerHeight(true)));

    $("#divInfIzq").height(height - 13);
    $("#graficoPorGrupo").height(height - 58);

    $("#divInfDer").height(height - 13);
    $("#grafTortaSubClase").height(height - 58);

    $(".ui-jqgrid-bdiv").css('overflow-x', 'hidden');

    switch (localStorage.RolUsuario) {
        case 'Administrador':
            // grafico Patrimonio monedas
            var heightSecPeso = $('#secPatPeso').height();
            var widthSecPeso = $('#secPatPeso').width();
            $('#chartSaldoActivoPeso svg').height(heightSecPeso - 35);
            $('#chartSaldoActivoPeso svg').width(widthSecPeso + 40);
            graphSaldoActivoPeso.height = heightSecPeso - 35;
            graphSaldoActivoPeso.width = widthSecPeso + 40;
            graphSaldoActivoPeso.render();

            var heightSecDolar = $('#secPatDolar').height();
            var widthSecDolar = $('#secPatDolar').width();
            $('#chartSaldoActivoDolar svg').height(heightSecDolar - 35);
            $('#chartSaldoActivoDolar svg').width(widthSecDolar + 40);
            graphSaldoActivoDolar.height = heightSecDolar - 35;
            graphSaldoActivoDolar.width = widthSecDolar + 40;
            graphSaldoActivoDolar.render();
            //Tabla Salto cuota
            var widDiv02 = $("#divTblSalCuota").width();
            $("#trTblSalCuota").width(widDiv02 - 17);
            //Titulo Cierre Cuentas
            $("#idTituloCierreCuentas").text("Cierre Cuentas al " + moment(new Date(localStorage.UltimaFechaCierre)).format('L'));

            break;
        case 'Usuarios y Roles':
            var heightSec = $('#secRegUsu').height();
            var widthSec = $('#secRegUsu').width();
            $('#chartRegistroUsuarios svg').height(heightSec - 28);
            $('#chartRegistroUsuarios svg').width(widthSec + 40);
            graphRegistro.height = heightSec - 28;
            graphRegistro.width = widthSec + 40;
            graphRegistro.render();
            break;

        case 'resumenCliente':


        default:
    }
}

function formatoArbol() {
    var ids = $("#ListaArbolActivos").jqGrid("getDataIDs"),
        l = ids.length,
        i,
        rowid;
    var rowid = "";
    var base = 20;
    var indent = 0;
    for (i = 0; i < l; i++) {
        rowid = ids[i];
        nivel = $("#ListaArbolActivos").jqGrid("getCell", rowid, "NIVEL");
        nivelMax = $("#ListaArbolActivos").jqGrid("getCell", rowid, "NIVEL_MAX");
        indent = base * nivel;
        $("#ListaArbolActivos" + '> > #' + $.jgrid.jqID(rowid)).css('text-indent', indent + 'px');
        if (nivel == 0) {
            $("#ListaArbolActivos" + '> > #' + $.jgrid.jqID(rowid)).addClass('TotalPatrimonio');
        } else {
            if (nivel == 1) {
                $("#ListaArbolActivos" + '> > #' + $.jgrid.jqID(rowid)).addClass('FilaGruposJqgrid');
            } else {
                if (nivel < nivelMax) {
                    $("#ListaArbolActivos" + '> > #' + $.jgrid.jqID(rowid)).addClass('FilaBaseJqgrid');
                }
            }
        }
    }
}
function consultaArbolActivos() {
    jsfechaConsulta = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();
    var fechaFinal = jsfechaConsulta.substring(0, jsfechaConsulta.length - 1);
    if (fechaFinal === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }
    $("#ListaArbolActivos").jqGrid('clearGridData');
    $('#grafTortaClase').empty();
    $('#grafTortaSubClase').empty();
    $('.loading').show();
    $.ajax({
        url: "../Servicios/Index.asmx/consultaArbolActivos",
        data: "{'fechaConsulta':'" + fechaFinal + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                if (mydata.length >= 1) {
                    $("#ListaArbolActivos").setGridParam({ data: mydata });
                    $("#ListaArbolActivos").trigger("reloadGrid");
                    formatoLista();
                    consultaPatrimonioClase(mydata);
                    consultaPatrimonioSubClase(mydata);
                } else {
                    alertaColor(1, "No se encontraron registros.");
                }
            } else {
                alertaColor(3, 'Error en Consulta Gráficos Patrimonio Activos.');
            }
            $('.loading').hide();
        }
    });
}
function formatoLista() {
    var ids = $("#ListaArbolActivos").jqGrid("getDataIDs"), l = ids.length, i, rowid, status;
    var rowid = "";
    var monto = "0";
    var codigo = "";
    var Descripcion = "";
    var tipo = "";
    for (i = 0; i < l; i++) {
        rowid = ids[i];
        nivel = $("#ListaArbolActivos").jqGrid("getCell", rowid, "NIVEL");
        nivelMax = $("#ListaArbolActivos").jqGrid("getCell", rowid, "NIVELMAX");
        if (nivel > 1 && nivelMax <= nivel) {
            monto = $("#ListaArbolActivos").jqGrid("getCell", rowid, "MONTO");
            tipo = $("#ListaArbolActivos").jqGrid("getCell", rowid, "CODIGO");
            codigo = $("#ListaArbolActivos").jqGrid("getCell", rowid, "ID_ARBOL_CLASIFICACION_INSTRUMENTO");
            Descripcion = $("#ListaArbolActivos").jqGrid("getCell", rowid, "DSC_ARBOL_CLASIFICACION_INSTRUMENTO");
        }
    }
}
function consultaPatrimonioClase(dataRent) {

    var ArrayRes = [];
    $.each(dataRent, function (key, val) {
        var objArray = {};
        if (val.NIVEL == 1 && val.MONTO > 0) {
            objArray.name = val.DSC_ARBOL_CLASE_INST;
            objArray.y = val.PORCENTAJE;
            ArrayRes.push(objArray);
        }
    });
    ArrayRes.toString();
    $('#grafTortaClase').highcharts({
        credits: {
            text: '',
            href: ''
        },
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Porcentaje',
            colorByPoint: true,
            data: ArrayRes
            //[{ name: 'Microsoft', y: 56.33 }, { name: '', y: 0.2 }]
        }]
    });
}
function consultaPatrimonioSubClase(dataRent) {
    var ArrayRes = [];
    $.each(dataRent, function (key, val) {
        var objArray = {};
        if (val.NIVEL == 2 && val.MONTO > 0) {
            objArray.name = val.DSC_ARBOL_CLASE_INST;
            objArray.y = val.PORCENTAJE;
            ArrayRes.push(objArray);
        }
    });
    ArrayRes.toString();
    $('#grafTortaSubClase').highcharts({
        credits: {
            text: '',
            href: ''
        },
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Porcentaje',
            colorByPoint: true,
            data: ArrayRes
        }]
    });
}
function consultaRentabilidadHistorica() {
    jsfechaConsulta = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();
    var fechaFinal = jsfechaConsulta.substring(0, jsfechaConsulta.length - 1);
    if (fechaFinal === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }

    $("#graficoPorGrupo").empty();
    var graficoOpcGrupo = {
        chart: { renderTo: 'graficoPorGrupo' },
        title: { text: '' },
        series: [{ data: [], tooltip: { valueDecimals: 2 } }],
        credits: { text: '', href: '' },
        rangeSelector: {
            selected: 1,
            buttons: [{ type: 'month', count: 1, text: '1m' },
            { type: 'month', count: 3, text: '3m' },
            { type: 'month', count: 6, text: '6m' },
            { type: 'year', count: 1, text: '1año' },
            { type: 'ytd', text: 'YTD' },
            { type: 'all', text: 'Todo' }]
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: [
                        { text: 'Descargar como PNG', onclick: function () { this.exportChart(); } },
                        { text: 'Descargar como JPEG', onclick: function () { this.exportChart({ type: 'image/jpeg' }); } },
                        { text: 'Descargar como PDF', onclick: function () { this.exportChart({ type: 'application/pdf' }); } },
                        { text: 'Descargar como SVG', onclick: function () { this.exportChart({ type: 'image/svg+xml' }); } }
                    ]
                }
            }
        }
    };
    graficoPorGrupo = new Highcharts.StockChart(graficoOpcGrupo);


    $.ajax({
        url: "../Servicios/Index.asmx/ConsultaRentabilidadHistorica",
        data: "{'fechaConsulta':'" + fechaFinal + "'}",
        datatype: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var dataMercadoJson = JSON.parse(jsondata.responseText);
                var datatestt = [];
                if (dataMercadoJson.d.rows !== null) {
                    for (var i = 0; i < dataMercadoJson.d.length; i++) {
                        datatestt[i] = [Number(dataMercadoJson.d[i].FECHA_CIERRE.replace("/Date(", "").replace(")/", "")), dataMercadoJson.d[i].VALOR_CUOTA];
                    }
                }
                graficoPorGrupo.series[0].setData(datatestt);
                $(window).trigger('resize');
            } else {
                alert(JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}
function consultarChartEstadisticaUsuarios(callback) {
    $.ajax({
        url: "../Servicios/Index.asmx/ConsultarEstadisticasUsuarios",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var data = JSON.parse(jsondata.responseText).d;

                var dscEstados = [];
                var cantEstados = [];

                var dscCantPerfiles = [];

                var dscCantInicioSesion = [];
                var secuencia = 0;

                $.each(data, function (key, val) {
                    if (val.TIPO_GRUPO == 'consulta_estados_usuario') {
                        dscEstados.push(val.DSC_AGRUPACION);
                        cantEstados.push(val.CANTIDAD);
                    }

                    if (val.TIPO_GRUPO == 'consulta_roles_usuario') {
                        dscCantPerfiles.push({ label: val.DSC_AGRUPACION, value: val.CANTIDAD });
                    }


                    if (val.TIPO_GRUPO == 'consulta_registro_sesion_usuario') {
                        secuencia++
                        dscCantInicioSesion.push({ x: secuencia, y: val.CANTIDAD });
                        $('#valorCantUsu').html(val.CANTIDAD);
                    }
                });

                cargarChartEstados(dscEstados, cantEstados);
                cargarChartPerfiles(dscCantPerfiles);
                graficoRegistrosUsuarios(dscCantInicioSesion);
                callback();
            }
            else {
                alertaColor(3, 'Error en Consulta de Gráficos.');
            }
        }
    });
}

function consultarChartSaldoActivoMoneda(callback) {
    $.ajax({
        url: "../Servicios/Index.asmx/ConsultaSaldoActivoMoneda",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var data = JSON.parse(jsondata.responseText).d;
                var dscSaldoActivoDolar = [];
                var dscSaldoActivoPeso = [];
                var secuenciaDolar = 0;
                var secuenciaPeso = 0;
                var valorCanPatPeso = 0;
                var valorCanPatDolar = 0;
                $.each(data, function (key, val) {

                    if (val.ID_MONEDA_CTA == 2) {

                        secuenciaDolar++
                        dscSaldoActivoDolar.push({ x: secuenciaDolar, y: val.MONTO_MON_CTA });

                        valorCanPatDolar = val.MONTO_MON_CTAFINAL;

                    }
                    if (val.ID_MONEDA_CTA == 1) {
                        secuenciaPeso++
                        dscSaldoActivoPeso.push({ x: secuenciaPeso, y: val.MONTO_MON_CTA });
                        valorCanPatPeso = val.MONTO_MON_CTAFINAL;
                    }
                });
                if (valorCanPatPeso > 0) {
                    $('#Imagenpeso').addClass("fa fa-arrow-up");
                }
                else if (valorCanPatPeso < 0) {
                    $('#Imagenpeso').addClass("fa fa-arrow-down");
                    document.getElementById("secPatPeso").style.backgroundColor = "#F7464A";
                }
                else {
                    $('#Imagenpeso').addClass("fa fa-minus");
                    document.getElementById("secPatPeso").style.backgroundColor = "#949FB1";
                }


                if (valorCanPatDolar > 0) {
                    $('#Imagendolar').addClass("fa fa-arrow-up");
                }
                else if (valorCanPatDolar < 0) {
                    $('#Imagendolar').addClass("fa fa-arrow-down");
                    document.getElementById("secPatDolar").style.backgroundColor = "#F7464A";
                }
                else {
                    $('#Imagendolar').addClass("fa fa-minus");
                    document.getElementById("secPatDolar").style.backgroundColor = "#949FB1";

                }
                $('#valorCanPatPeso').html(numeral(valorCanPatPeso).format('0,000.0000 %'));
                $('#valorCanPatDolar').html(numeral(valorCanPatDolar).format('0,000.0000 %'));

                graficoSaldoActivoMonedaDolar(dscSaldoActivoDolar);
                graficoSaldoActivoMonedaPeso(dscSaldoActivoPeso);
                callback();

            }
            else {
                alertaColor(3, 'Error en Consulta de Gráficos.');
            }
        }
    });
}
function consultarChartcuentas(callback) {
    $.ajax({
        url: "../Servicios/Index.asmx/ConsultaCuentaAgrupado",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var data = JSON.parse(jsondata.responseText).d;

                var dscEstados = [];

                $.each(data, function (key, val) {
                    dscEstados.push({ label: val.Descripcion, value: val.Cantidad });

                });

                cargarChartCuentas(dscEstados);

            }
            else {
                alertaColor(3, 'Error en Consulta de Gráficos.');
            }
        }
    });
}
function consultarChartClientes(callback) {
    $.ajax({
        url: "../Servicios/Index.asmx/ConsultaClienteAgrupado",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var data = JSON.parse(jsondata.responseText).d;

                var dscEstados = [];
                var cantEstados = [];
                $.each(data, function (key, val) {
                    dscEstados.push({ label: val.Descripcion, value: val.Cantidad });

                });

                cargarChartClientes(dscEstados);
            }
            else {
                alertaColor(3, 'Error en Consulta de Gráficos.');
            }
        }
    });
}
function htmlTablaSaltoCuota(val) {
    var html =
        "<tr class='tr-scroll'>" +
        "<td>" + val.NUM_CUENTA + "</td>" +
        "<td>" + val.ABR_CUENTA + "</td>" +
        "<td class='text-right'>" + val.VARIACION + "</td>" +
        "<td class='text-right'>" + val.PORC_SALTO_CUOTA + "</td>" +
        "</tr>";

    return html;
}
function graficoSaldoActivoMonedaDolar(data) {
    graphSaldoActivoDolar = new Rickshaw.Graph({
        element: document.querySelector("#chartSaldoActivoDolar"),
        min: 'auto',
        width: 593,
        height: 168,
        series: [{
            color: 'steelblue',
            data: data
        }]
    });
    graphSaldoActivoDolar.render();
}
function graficoSaldoActivoMonedaPeso(data) {
    graphSaldoActivoPeso = new Rickshaw.Graph({
        element: document.querySelector("#chartSaldoActivoPeso"),
        min: 'auto',
        width: 593,
        height: 168,
        series: [{
            color: 'steelblue',
            data: data
        }]
    });
    graphSaldoActivoPeso.render();
}
function graficoRegistrosUsuarios(data) {
    graphRegistro = new Rickshaw.Graph({
        element: document.querySelector("#chartRegistroUsuarios"),
        width: 593,
        height: 168,
        series: [{
            color: 'steelblue',
            data: data
        }]
    });
    graphRegistro.render();
}

function cargarChartEstados(dscEstados, cantEstados) {

    var datos = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: cantEstados,
                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"]
            }],
            labels: dscEstados
        },
        options: {
            responsive: false
        }
    };
    var canvas = document.getElementById('chartEstadosUsuarios').getContext('2d');
    window.pie = new Chart(canvas, datos);
}
function cargarChartCuentas(dscEstados) {

    new Morris.Donut({
        element: 'chartcuenta',
        data: dscEstados,
        colors: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
        resize: true,
        labelColor: '#888'
    });
}
function cargarChartClientes(dscEstados) {


    new Morris.Donut({
        element: 'chart_Clientes',
        data: dscEstados,
        colors: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
        resize: true,
        labelColor: '#888'
    });

}
function cargarChartPerfiles(dscCantPerfiles) {
    new Morris.Donut({
        element: 'chartPerfilesUsuarios',
        data: dscCantPerfiles,
        colors: ['#009688', '#4CAF50', '#D32F2F', '#795548'],
        resize: true,
        labelColor: '#888'
    });
}
function consultaEstadocierrecta() {
    var cerrada_cant = 0;
    var Pendiente_cant = 0;
    var ReProceso_cant = 0;
    var Errorcierre_cant = 0;

    $.ajax({
        url: "../Servicios/Index.asmx/Consultaestadocuenta",
        data: "{}",
        datatype: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var usuariosPWD = JSON.parse(jsondata.responseText).d;
                var dscEstados = [];
                var registro;
                $('#tblUsuariosTodCuentas tbody').html('');
                var data = [];

                $.each(usuariosPWD, function (key, val) {
                    if (val.DSC_ESTADO == 'Cerrada') {
                        cerrada_cant++
                    }
                    if (val.DSC_ESTADO == 'Pendiente') {
                        Pendiente_cant++
                    }
                    if (val.DSC_ESTADO == 'ReProceso') {
                        ReProceso_cant++
                    }
                    if (val.DSC_ESTADO == 'Error de cierre') {
                        Errorcierre_cant++
                    }
                });

                dscEstados.push({ label: 'Cerrada', value: cerrada_cant });
                dscEstados.push({ label: 'Pendiente', value: Pendiente_cant });
                dscEstados.push({ label: 'ReProceso', value: ReProceso_cant });
                dscEstados.push({ label: 'Error de cierre', value: Errorcierre_cant });

                CargarGraficoCierreCta(dscEstados);
            } else {
                alertaColor(3, 'Error en Consulta de Gráficos.');
            }

        }
    });
}
function consultaSaltoCuota() {
    $.ajax({
        url: "../Servicios/Index.asmx/ConsultaSaltoCuota",
        data: "{}",
        datatype: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var SaltoCuota = JSON.parse(jsondata.responseText).d;
                if (SaltoCuota.length != 0) {

                    var registro;
                    $('#tblSalCuota tbody').html('');

                    $.each(SaltoCuota, function (key, val) {
                        registro += htmlTablaSaltoCuota(val);
                    });
                    $('#tblSalCuota tbody').append(registro);
                } else {
                    $('#tblSalCuota tbody').html('');

                    var html = "<tr class='tr-scroll ' style='font-weight: bold;'>" +
                        "<td> No se registraron salto cuotas </td>" +
                        "</tr>";

                    $('#tblSalCuota tbody').html(html);

                }
            } else {
                alertaColor(3, 'Error en Consulta de salto cuota.');
            }
        }

    });

}
function CargarGraficoCierreCta(dscEstados) {
    new Morris.Donut({
        element: 'chartestadocuenta',
        data: dscEstados,
        colors: ["#46BFBD", "#FDB45C", "#949FB1", "#F7464A", "#4D5360"],
        resize: true,
        labelColor: '#888'
    });

}

function cargarEventHandlersIndex() {
    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');
    $(".input-group.date").datepicker();
    $("#BtnConsultar").button().click(function () {
        consultaArbolActivos();
        consultaRentabilidadHistorica();
    });
    $("#ListaArbolActivos").jqGrid({
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "NumeroRegistro" },
        colNames: ['ID_ARBOL_CLASIFICACION_INSTRUMENTO', 'Codigo', 'Descripción', 'Porcentaje', 'Monto', 'Nivel', 'NivelMax'],
        colModel: [{ name: "ID_ARBOL_CLASIFICACION_INSTRUMENTO", index: "ID_ARBOL_CLASIFICACION_INSTRUMENTO", width: 80, sortable: false, sorttype: "int", editable: false, search: true, hidden: true },
        { name: "CODIGO", index: "Codigo", width: 70, sortable: false, sorttype: "int", editable: false, search: true, hidden: true },
        { name: "DSC_ARBOL_CLASE_INST", index: "DSC_ARBOL_CLASE_INST", sortable: false, width: 70, sorttype: "text", editable: false, search: true },
        { name: "PORCENTAJE", index: "PORCENTAJE", sortable: false, width: 50, sorttype: "number", formatter: 'number', formatoptions: { suffix: '%' }, editable: false, search: true, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "MONTO", index: "MONTO", width: 60, sortable: false, sorttype: "number", formatter: 'number', editable: false, search: true, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "NIVEL", index: "NIVEL", width: 80, sortable: false, sorttype: "int", editable: false, search: true, summaryType: 'sum', hidden: true },
        { name: "NIVEL_MAX", index: "NIVEL_MAX", width: 80, sortable: false, sorttype: "int", editable: false, search: true, summaryType: 'sum', hidden: true }
        ],
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: 100000,
        rowList: [],
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        caption: "Total Patrimonio",
        grouping: false,
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        groupingView: {
            groupField: ['Codigo'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        },
        gridComplete: function () {
            $("#ListaArbolActivos").trigger('reloadGrid');
            $('#Paginador_center').hide();
            formatoArbol();
        }
    });
}
function initIndex(callback) {
    if (localStorage.getItem("sesionCerrada") === "True") {
        localStorage.removeItem("sesionCerrada");
        window.location.reload(true);
        return;
    }
    jsfechaConsulta = document.getElementById("dtFechaConsulta").value;
    $("#idSubtituloPaginaText").text("Dashboard");
    document.title = "Dashboard";

    $(".input-group.date").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
    callback();

    Highcharts.setOptions({
        lang: {
            months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            weekdays: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            rangeSelectorFrom: 'Desde',
            rangeSelectorTo: 'Hasta'
        },
        credits: {
            text: 'creasys.cl',
            href: 'http://www.creasys.cl'
        },
        rangeSelector: {
            buttons: [{
                type: 'month',
                count: 1,
                text: '1m'
            }, {
                type: 'month',
                count: 3,
                text: '3m'
            }, {
                type: 'month',
                count: 6,
                text: '6m'
            }, {
                type: 'year',
                count: 1,
                text: '1año'
            }, {
                type: 'ytd',
                text: 'YTD'
            }, {
                type: 'all',
                text: 'Todo'
            }]
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: [
                        {
                            text: 'Descargar como PNG',
                            onclick: function () {
                                this.exportChart();
                            }
                        }, {
                            text: 'Descargar como JPEG',
                            onclick: function () {
                                this.exportChart({
                                    type: 'image/jpeg'
                                });
                            }
                        }, {
                            text: 'Descargar como PDF',
                            onclick: function () {
                                this.exportChart({
                                    type: 'application/pdf'
                                });
                            }
                        }, {
                            text: 'Descargar como SVG',
                            onclick: function () {
                                this.exportChart({
                                    type: 'image/svg+xml'
                                });
                            }
                        }
                    ]
                }
            }
        }
    });
}
function initIndexAdmin(callback) {
    if (localStorage.getItem("sesionCerrada") === "True") {
        localStorage.removeItem("sesionCerrada");
        window.location.reload(true);
        return;
    }
    $("#idSubtituloPaginaText").text("Dashboard");
    document.title = "Dashboard Administrador";
    callback();
}
function initIndexUsuariosRoles(callback) {
    if (localStorage.getItem("sesionCerrada") === "True") {
        localStorage.removeItem("sesionCerrada");
        window.location.reload(true);
        return;
    }
    $("#idSubtituloPaginaText").text("Dashboard");
    document.title = "Dashboard Usuarios y Roles";
    callback();
}
function consultarData() {
    consultaArbolActivos();
    consultaRentabilidadHistorica();
}
function consultarDataAdmin() {

    consultaEstadocierrecta();
    consultarChartClientes();
    consultarChartcuentas();
    consultaSaltoCuota();
    consultarChartSaldoActivoMoneda(function () {
        $("#tblSalCuota").css({ "border-color": "rgb(" + localStorage.ColorEmpresa + ") " });
        $(window).bind('resize', function () {
            resize();
        }).trigger('resize');
    });
}
function consultarDatUsuariosRoles() {
    consultarChartEstadisticaUsuarios(function () {
        $(window).bind('resize', function () {
            resize();
        }).trigger('resize');
    });
}

$(document).ready(function () {

    switch (localStorage.RolUsuario) {
        case 'Administrador':
            $('#resumenAdministrador').show();
            initIndexAdmin(function () {
                consultarDataAdmin();
                $(window).trigger('resize');

            });
            break;
        case 'Usuarios y Roles':
            $('#resumenUsuariosRoles').show();
            initIndexUsuariosRoles(function () {
                consultarDatUsuariosRoles();
                $(window).trigger('resize');
            });
            break;
        default:
            $('#resumenCliente').show();
            initIndex(function () {
                cargarEventHandlersIndex();
                consultarData();
                $(window).trigger('resize');
            });
    }
});