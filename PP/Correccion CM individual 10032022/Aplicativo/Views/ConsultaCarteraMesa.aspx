﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="ConsultaCarteraMesa.aspx.vb" Inherits="AplicacionWeb.ConsultaCarteraMesa" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaCarteraMesa.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion container-fluid">
            <div class="row top-buffer">
                <div class="form-inline col-xs-12">
                    <div class="form-group">
                        <div class="input-group date">
                            <input type="text" id="dtFechaConsulta" class="form-control" style="text-align: center;">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="button" id="BtnConsultar" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Seleccione Cuenta">Consultar</button>
                        <button type="button" id="BtnCarteraCompleta" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom">Consultar Todas Las Cuentas</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-sm-12">
            <table id="CarteraMesa">
                <tr>
                    <td></td>
                </tr>
            </table>
            <div id="Paginador">
            </div>
        </div>
    </div>
    <div style="display: none;">
        <table id="CarteraMesaTodasLasCuentas">
            <tr>
                <td></td>
            </tr>
        </table>
        <div id="PaginadorTodasLasCuentas">
        </div>
    </div>
</asp:Content>
