﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="ConsultaSaldoCajas.aspx.vb" Inherits="AplicacionWeb.ConsultaSaldoCajas" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">
    
    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaSaldoCajas.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <div class="input-group date">
                        <input type="text" id="dtFechaConsulta" class="form-control" style="text-align: center;">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-sm-12 col-lg-12" id="panelDerecho">
            <div class="row">
                <div class="col-sm-12">
                    <table id="SaldoCajas">
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                    <div id="paginador">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>