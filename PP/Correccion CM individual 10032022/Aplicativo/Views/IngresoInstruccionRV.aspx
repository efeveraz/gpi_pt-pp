﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master"
    CodeBehind="IngresoInstruccionRV.aspx.vb" Inherits="AplicacionWeb.IngresoInstruccionRV" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContenidoPPal" runat="server">
    <script type="text/javascript">
        function resize() {
            var height;
            var ancho = $(window).width();
            height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - 129;
            if (ancho < 768) {
                $("#panelbodyOperacion").height(175);
            } else {
                $("#panelbodyOperacion").height(height - $("#GVOperaciones").outerHeight(true) - 28);
            }
            $("#spanelfiltro").height(height);
            $("#spanelComisiones").height(height);
        }

        function validarFinanciamiento() {
            jsFechaLiquidacion = moment($("#<%=DTFechaLiquidacion.ClientID %>").datepicker('getDate')).toISOString();
            jsTipoOperacion = $("#<%=DDCompraVenta.ClientID%>").val()

            $.ajax({
                url: "IngresoInstruccionRV.aspx/validarFinanciamiento",
                data: "{'idCuenta':" + document.getElementById('HFIdCuenta').value +
                ", 'idMoneda':" + document.getElementById('HFIdMoneda').value +
                ", 'fechaLiquidacion':'" + jsFechaLiquidacion +
                "', 'tipoOperacion':'" + jsTipoOperacion + "'}",
                datatype: "json",
                type: "post",
                contentType: "application/json; charset=utf-8",
                complete: function (jsondata, stat) {
                    if (stat == "success") {
                        var mydata = JSON.parse(jsondata.responseText).d;
                        if (mydata.mensaje != "") {
                            if (mydata.sobregiro) {
                                bootbox.dialog({
                                    message: mydata.mensaje + 'Desea Continuar?',
                                    title: "Alerta",
                                    buttons: {
                                        main: {
                                            label: "Cancelar",
                                            className: "btn-primary",
                                            callback: function () {
                                            }
                                        },
                                        danger: {
                                            label: "Continuar",
                                            className: "btn-danger",
                                            callback: function () {
                                                var myControl = document.getElementById('<%=BTGuardar.ClientID %>');
                                                myControl.click();
                                            }
                                        }
                                    }
                                });
                            }
                            else {
                                bootbox.dialog({
                                    message: mydata.mensaje,
                                    title: "Alerta",
                                    buttons: {
                                        main: {
                                            label: "Aceptar",
                                            className: "btn-primary",
                                            callback: function () {
                                            }
                                        }
                                    }
                                });
                            }
                        }
                        else {
                            var myControl = document.getElementById('<%=BTGuardar.ClientID %>');
                            myControl.click();
                        }
                    }
                    else {
                        alertaColor(3, JSON.parse(jsondata.responseText).Message);
                    }
                }
            });
        }
        function confirmacion() {
            if (document.getElementById('<%=HFConfirmacion.ClientID %>').value == "") {
                document.getElementById('<%=HFResConfirmacion.ClientID %>').value = true;
                return true;
            }
            var resultado = confirm(document.getElementById('<%=HFConfirmacion.ClientID %>').value);
            document.getElementById('<%=HFResConfirmacion.ClientID %>').value = resultado
        }
        function AplicarDatePickers() {
            $("#<%=DTFechaLiquidacion.ClientID %>").datepicker();
            $('#<%=DTFechaLiquidacion.ClientID %>').datepicker().on('changeDate', function (e) {
                $('#<%=HFFechaLiquidacion.ClientID %>').val(e.format('dd-mm-yyyy'));
            });
            $("#<%=DTFechaOperacion.ClientID %>").datepicker();
            $('#<%=DTFechaOperacion.ClientID %>').datepicker().on('changeDate', function (e) {
                $('#<%=HFFechaOperacion.ClientID %>').val(e.format('dd-mm-yyyy'));
            });
        }
        function pageLoad(sender, args) {
            $(window).trigger('resize');
            $('#RBCantidadMonto').on('change', function (event) {
                __doPostBack('RBCantidadMonto')
            });
            $(document).ready(function () {
                AplicarDatePickers();
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(AplicarDatePickers);

                $('#loader').modal({ backdrop: false });
                $(window).trigger('resize');
                $('#BtnGuardar').on('click', function () {
                    if (parseFloat(document.getElementById('<%=TBMontoTotal.ClientID %>').value) > 0) {
                        validarFinanciamiento();
                    } else {
                        var myControl = document.getElementById('<%=BTGuardar.ClientID %>');
                        myControl.click();
                    }
                });
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(window).bind('resize', function () {
                resize()
            }).trigger('resize');

            $("#idSubtituloPaginaText").text("Ingreso de Instrucciones RV");
            document.title = "Ingreso de Instrucciones RV";

            if (miInformacionCuenta != null) {
                document.getElementById('<%=HFIdCuenta.ClientID %>').value = miInformacionCuenta.IdCuenta;
                document.getElementById('<%=HFIdCliente.ClientID %>').value = miInformacionCuenta.IdCliente;
                __doPostBack("HFIdCliente", "TextChanged");
            }

            $("#tabsClienteCuentaGrupo").hide();
            $('.nav-tabs a[href="#tabCuenta"]').tab('show');

            $('#porCuenta_NroCuenta').on('change', function () {
                if (miInformacionCuenta == null) {
                    document.getElementById('<%=HFIdCuenta.ClientID %>').value = "";
                    document.getElementById('<%=HFIdCliente.ClientID %>').value = "";
                    __doPostBack("HFIdCliente", "TextChanged");
                    return;
                }
                document.getElementById('<%=HFIdCuenta.ClientID %>').value = miInformacionCuenta.IdCuenta;
                document.getElementById('<%=HFIdCliente.ClientID %>').value = miInformacionCuenta.IdCliente;
                __doPostBack("HFIdCliente", "TextChanged");
            });

            <%--$('#<%=TBCantidadCompra.ClientID %>').on('propertychange change keyup paste input', function () {
                alert("cambio");
            });--%>

            function CheckMarcado() {
                var radioButtonlist = document.getElementsByName("<%=RBCantidadMonto.ClientID%>");
                for (var x = 0; x < radioButtonlist.length; x++) {
                    if (radioButtonlist[x].checked) {
                        alert("Selected item Value " + radioButtonlist[x].value);
                    }
                }
            }
        });
    </script>
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress runat="server">
                <ProgressTemplate>
                    <div class="modal fade" id="loader" role="dialog" runat="server">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    Un momento...
                                </div>
                                <div class="modal-body">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:HiddenField ID="HFIdCuenta" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HFIdCliente" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HFIdMoneda" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HFResConfirmacion" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HFConfirmacion" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HFMontoOperacion" runat="server" ClientIDMode="Static" />
            <div class="row top-buffer">
                <div class="col-sm-3 col-lg-2">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom: 0px;">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headpanelfiltro">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#panelfiltro" aria-expanded="true" aria-controls="panelfiltro">Datos de Operación
                                    </a>
                                </h4>
                            </div>
                            <div id="panelfiltro" class="panel-collapse collapse in">
                                <div id="spanelfiltro" class="panel-body" style="height: 300px; overflow-y: auto;">
                                    <div class="form-group-sm">
                                        <label for="DDContraparte" class="control-label">Contraparte</label>
                                        <asp:DropDownList ID="DDContraparte" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="-1">Seleccionar Contraparte</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group-sm">
                                        <label for="DDTrader" class="control-label">Trader</label>
                                        <asp:DropDownList ID="DDTrader" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="-1">Seleccionar Trader</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group-sm">
                                        <label for="DDRepresentantes" class="control-label">Representantes</label>
                                        <asp:DropDownList ID="DDRepresentantes" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="-1">Seleccionar Representante</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group-sm">
                                        <label for="DDFechaLiquidación" class="control-label">Fecha Liquidación</label>
                                        <div class="input-group-sm" style="display: table;">
                                            <span class="input-group-btn">
                                                <asp:DropDownList ID="DDFechaLiquidación" runat="server" AutoPostBack="true" CssClass="dropdown-toggle btn btn-default" data-toggle="dropdown">
                                                    <asp:ListItem Value="PH">PH</asp:ListItem>
                                                    <asp:ListItem Value="PM">PM</asp:ListItem>
                                                    <asp:ListItem Value="CN">CN</asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <asp:TextBox ID="DTFechaLiquidacion" runat="server" ClientIDMode="Static" AutoPostBack="true"
                                                OnTextChanged="DTFechaLiquidacion_onchange" CssClass="form-control pull-right" Style="text-align: center;"></asp:TextBox>
                                        </div>
                                        <asp:HiddenField ID="HFFechaLiquidacion" runat="server" ClientIDMode="Static" />
                                    </div>
                                    <div class="form-group-sm">
                                        <label for="DTFechaOperacion" class="control-label">Fecha Operación</label>
                                        <asp:TextBox ID="DTFechaOperacion" runat="server" ClientIDMode="Static" AutoPostBack="true"
                                            OnTextChanged="DTFechaOperacion_onchange" CssClass="form-control" Style="text-align: center;"></asp:TextBox>
                                        <asp:HiddenField ID="HFFechaOperacion" runat="server" ClientIDMode="Static" />
                                    </div>
                                    <div class="form-group-sm">
                                        <label for="TBMontoTotal" class="control-label">Total Aprox.</label>
                                        <asp:TextBox ID="TBMontoTotal" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)" ReadOnly="True">0</asp:TextBox>
                                    </div>
                                    <div class="form-group-sm">
                                        <label for="TBDiasVigencia" class="control-label">Dias Vigencia</label>
                                        <asp:TextBox ID="TBDiasVigencia" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)" ReadOnly="True">0</asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="panelComisiones" runat="server" clientidmode="Static" class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headpanelComisiones">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#panelCollapseComisiones" aria-expanded="true" aria-controls="panelCollapseComisiones">Comisión
                                    </a>
                                </h4>
                            </div>
                            <div id="panelCollapseComisiones" class="panel-collapse collapse">
                                <div id="spanelComisiones" class="panel-body" style="height: 300px; overflow-y: auto;">
                                    <div class="form-group-sm">
                                        <label class="control-label">Comisión (%)</label>
                                        <asp:TextBox ID="TBPorComision" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                    <div class="form-group-sm">
                                        <label class="control-label">Derechos (%)</label>
                                        <asp:TextBox ID="TBPorDerechos" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                    <div class="form-group-sm">
                                        <label class="control-label">Comisión a Cobrar</label>
                                        <asp:TextBox ID="TBMonComision" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                    <div class="form-group-sm">
                                        <label class="control-label">Derechos Bolsa</label>
                                        <asp:TextBox ID="TBDerechos" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                    <div class="form-group-sm">
                                        <label class="control-label">Gastos</label>
                                        <asp:TextBox ID="TBGastos" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                    <div class="form-group-sm">
                                        <label id="LBIva" runat="server" css="control-label">IVA (19%)</label>
                                        <asp:TextBox ID="TBIva" runat="server" CssClass="form-control" MaxLength="22" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 col-lg-10" id="panelDerecho">
                    <asp:GridView ID="GVOperaciones" runat="server" Width="100%" AutoGenerateColumns="False" GridLines="None" ShowHeaderWhenEmpty="True" ClientIDMode="Static"
                        CssClass="table table-hover table-striped">
                        <EmptyDataTemplate>
                            Sin Datos
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                            <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                            <asp:BoundField DataField="idmoneda" HeaderText="idmoneda" Visible="false" />
                            <asp:BoundField DataField="id_nemotecnico" HeaderText="id_nemotecnico" Visible="false" />
                            <asp:BoundField DataField="cod_operacion" HeaderText="cod_operacion" Visible="false" />
                            <asp:BoundField DataField="dsc_operacion" HeaderText="Operación" />
                            <asp:BoundField DataField="dsc_nemotecnico" HeaderText="Nemotécnico" />
                            <asp:BoundField DataField="tipo_inversion" HeaderText="P / N" />
                            <asp:BoundField DataField="cantidad" HeaderText="Cantidad" />
                            <asp:BoundField DataField="precio" DataFormatString="{0:N}" HeaderText="Precio" />
                            <asp:BoundField DataField="KEY_TIPO_PRECIO" HeaderText="IDTipo Precio" Visible="false" />
                            <asp:BoundField DataField="DSC_TIPO_PRECIO" HeaderText="Tipo Precio" />
                            <asp:BoundField DataField="precio_historico" HeaderText="Precio Histórico" Visible="false" />
                            <asp:BoundField DataField="monto" DataFormatString="{0:N}" HeaderText="Monto" />
                            <asp:BoundField DataField="monto_ingresado" HeaderText="Monto Ingresado" Visible="false" />
                            <asp:BoundField DataField="dsc_moneda" HeaderText="Moneda" />
                            <asp:BoundField DataField="comision_monto" HeaderText="comision_monto" Visible="false" />
                            <asp:BoundField DataField="flg_vende_todo" HeaderText="flg_vende_todo" Visible="false" />
                            <asp:CommandField ShowDeleteButton="True" />
                        </Columns>
                    </asp:GridView>
                    <div class="panel panel-default" style="margin-bottom: 0px;">
                        <div class="panel-heading">
                            <div class="form-group nav">
                                <label for="DDCompraVenta" class="col-xs-6 col-sm-4 col-md-3 control-label" style="padding-top: 7px;">Tipo Operación</label>
                                <div class="col-xs-6 col-sm-4 col-md-3">
                                    <asp:DropDownList ID="DDCompraVenta" AutoPostBack="true" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="I">Compra</asp:ListItem>
                                        <asp:ListItem Value="E">Venta</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:Button ID="BTNuevo" runat="server" Text="Nuevo Detalle" CssClass="btn btn-default" />
                            </div>
                        </div>
                        <div id="panelbodyOperacion" runat="server" clientidmode="Static" class="panel-body" style="overflow-y: auto;">
                            <div class="row">
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="DDNemotecnico" class="control-label control-label-sm">Nemotécnico</label>
                                    <asp:DropDownList ID="DDNemotecnico" AutoPostBack="true" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div id="FGCantidadEnCartera" runat="server" class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBCantidadEnCartera" class="control-label">Cantidad en Cartera</label>
                                    <asp:TextBox ID="TBCantidadEnCartera" runat="server" ClientIDMode="Static" AutoPostBack="true" CssClass="form-control" disabled>0</asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBEmisor" class="control-label">Emisor</label>
                                    <asp:TextBox ID="TBEmisor" runat="server" CssClass="form-control" disabled></asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBMoneda" class="control-label">Moneda</label>
                                    <asp:TextBox ID="TBMoneda" runat="server" CssClass="form-control" disabled></asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <asp:CheckBox ID="CBVendeTodo" runat="server" Text="Vende Todo" AutoPostBack="true" />
                                </div>
                            </div>
                            <hr style="margin: 5px;">
                            <div class="row">
                                <div id="FGCantidadMonto" runat="server" class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="RBCantidadMonto" class="control-label">Tipo Compra</label>
                                    <asp:RadioButtonList ID="RBCantidadMonto" AutoPostBack="true" ClientIDMode="Static" RepeatLayout="flow" RepeatDirection="Horizontal" runat="server" CssClass="btn-group btn-group-sm" data-toggle="buttons">
                                        <asp:ListItem style="height: 30px;" class="btn btn-default " Text="Cantidad" Value="N"></asp:ListItem>
                                        <asp:ListItem style="height: 30px;" class="btn btn-default" Text="Monto" Value="P"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="DDTipoPM0" class="control-label">Tipo Precio</label>
                                    <asp:DropDownList ID="DDTipoPM0" runat="server" AutoPostBack="true" CssClass="form-control">
                                        <asp:ListItem Value="M">Mercado</asp:ListItem>
                                        <asp:ListItem Value="L">Límite</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBPrecioInver" class="control-label">Precio</label>
                                    <asp:TextBox ID="TBPrecioInver" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBPrecioCierre" class="control-label">Cierre</label>
                                    <asp:TextBox ID="TBPrecioCierre" runat="server" CssClass="form-control" disabled></asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBCantidadCompra" class="control-label">Cantidad</label>
                                    <asp:TextBox ID="TBCantidadCompra" runat="server" AutoPostBack="true" CssClass="form-control" onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                </div>
                                <div class="form-group form-group-sm col-xs-6 col-sm-3 col-lg-2">
                                    <label for="TBMontoOperacion" class="control-label">Monto Operación</label>
                                    <asp:TextBox ID="TBMontoOperacion" runat="server" AutoPostBack="true" CssClass="form-control"
                                        onkeypress="return isNumberKey(event)">0</asp:TextBox>
                                </div>
                            </div>
                            <asp:Button ID="BTValorizar" runat="server" Text="Valorizar" CssClass="btn btn-default" />
                            <asp:Button ID="BTAgregar" runat="server" Text="Agregar" CssClass="btn btn-default" OnClientClick="confirmacion()" OnClick="BTAgregar_Click" />
                            <asp:Button ID="BTActualizar" runat="server" Text="Actualizar" CssClass="btn btn-default" OnClientClick="confirmacion()" />
                            <asp:Button ID="BTCancelar" runat="server" Text="Cancelar" CssClass="btn btn-danger" />
                        </div>
                        <div class="panel-footer">
                            <asp:Button ID="BTGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" Style="display: none" />
                            <button type="button" id="BtnGuardar" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="TBCantidadCompra" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="TBMontoOperacion" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="TBPrecioInver" EventName="TextChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
