﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="CarteraPorCategoria.aspx.vb" Inherits="AplicacionWeb.CarteraPorCategoria" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">
    <script src='<%= VersionLinkHelper.GetVersion("../js/Stock/highstock.js") %>' type="text/javascript"></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/Stock/modules/exporting.js") %>' type="text/javascript"></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/carteraPorCategoria.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <select id="DDCategoria" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Categoría">
                        <option value="M">Mercado</option>
                        <option value="O">Moneda</option>
                        <option value="E">Sector</option>
                    </select>
                </div>
                <div class="form-group">
                    <div class="input-group date">
                        <input type="text" id="dtFechaConsulta" class="form-control" style="text-align: center;">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Seleccione Cuenta">Consultar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="contenidoPorCuenta" class="row top-buffer">
        <div id="grillaPorCuenta" class="col-xs-12 col-sm-6">
            <table id="ListaPorCuenta">
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>
        <div id="graficoPorCuenta" style="height: 100%" class="hidden-xs col-sm-6">
        </div>
    </div>
    <div id="contenidoPorCliente" class="row top-buffer">
        <div id="grillaPorCliente" class="col-xs-12 col-sm-6">
            <table id="ListaPorCliente">
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>
        <div id="graficoPorCliente" style="height: 100%" class="col-sm-6 hidden-xs">
        </div>
    </div>
    <div id="contenidoPorGrupo" class="row top-buffer">
        <div id="grillaPorGrupo" class="col-xs-12 col-sm-6">
            <table id="ListaPorGrupo">
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>
        <div id="graficoPorGrupo" style="height: 100%" class="col-sm-6 hidden-xs">
        </div>
    </div>
</asp:Content>
