﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="ConsultaMovimientosCaja.aspx.vb" Inherits="AplicacionWeb.ConsultaMovimientosCaja" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaMovimientosCaja.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <select id="DDCajaCta" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Caja">
                    </select>
                </div>
                <div class="form-group">
                    <div id="dtRangoFecha" class="input-daterange input-group">
                        <input id="dtFechaConsulta" type="text" class="form-control" />
                        <span class="input-group-addon">a</span>
                        <input id="dtFechaConsultaHasta" type="text" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Seleccione Cuenta">Consultar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-sm-12">
            <table id="MovimientosCaja">
                <tr>
                    <td></td>
                </tr>
            </table>
            <div id="Paginador">
            </div>
        </div>
    </div>
</asp:Content>
