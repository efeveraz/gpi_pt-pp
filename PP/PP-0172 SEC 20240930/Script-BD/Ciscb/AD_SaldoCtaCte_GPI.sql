CREATE PROCEDURE [dbo].[AD_SaldoCtaCte_GPI]
@pFecha_saldo int
As
Begin
Set nocount on

      create table #tmp_clientes
      (     t_Id_cliente       char(15),
            t_Disponible       numeric(16,2),
            t_Retenido_favor   numeric(16,2),
            t_Retenido_contra  numeric(16,2),
            t_Saldo_inicial    numeric(16,2),
            t_Simbolo          varchar(3)
      )

      -- Cargo cursor con los clientes
      declare rec_clientes cursor for
      Select a.Id_cliente,
             a.Tipo_de_cuenta
        from dbo.SALLINCTACTE a , dbo.PERSON b, VIEW_CLIENTE_GPI_SEC vc
       where a.Id_cliente = b.id_persona and
             a.Tipo_de_cuenta in (0, 1, 8) and
             ((a.Tipo_de_cuenta=0 and a.Monto_pesos<>0) or (a.Tipo_de_cuenta>0 and a.Monto_moneda<>0)) and
             vc.ORIGEN_MAGIC = 'MAGIC_VALORES' and
             a.Id_cliente = vc.CUENTA

      -- Ahora comienza el calculo por cada cliente
      declare @vId_cliente char(15), @vTipo_de_cuenta int,
              @vAbonos Numeric(16,2) , @vCargos Numeric(16,2),
              @vDisponible Numeric(16,2), @vRetenido_favor Numeric(16,2),
              @vRetenido_contra Numeric(16,2), @vSaldo_inicial Numeric(16,2)

      open rec_clientes
      fetch next from rec_clientes
            into @vId_cliente, @vTipo_de_cuenta
      while @@fetch_Status = 0
      begin
      --      ABONOS
            select @vAbonos = sum(CASE WHEN @vTipo_de_cuenta=0 THEN c.Monto_pesos ELSE c.Monto_moneda END)
              from dbo.REGCTACTECLI c
             where c.Tipo_de_cuenta = @vTipo_de_cuenta and
                   c.Id_cliente = @vId_cliente and
                   c.Cargo_abono = 'A' and
                   (c.Fecha_movimiento + isnull(c.Dias_retencion,0)) <= @pFecha_saldo
            if @vAbonos is null
                  set @vAbonos = 0

      --      CARGOS
            select @vCargos = sum(CASE WHEN @vTipo_de_cuenta=0 THEN c.Monto_pesos ELSE c.Monto_moneda END)
              from dbo.REGCTACTECLI c
             where c.Tipo_de_cuenta = @vTipo_de_cuenta and
                   c.Id_cliente = @vId_cliente and
                   c.Cargo_abono = 'C' and
                   (c.Fecha_movimiento + isnull(c.Dias_retencion,0)) <= @pFecha_saldo
            if @vCargos is null
                  set @vCargos = 0

      --    DISPONIBLE
            set @vDisponible = @vAbonos - @vCargos

      --    RETENIDO FAVOR
            select @vRetenido_favor = sum(CASE WHEN @vTipo_de_cuenta=0 THEN c.Monto_pesos ELSE c.Monto_moneda END)
              from dbo.REGCTACTECLI c
             where c.Tipo_de_cuenta = @vTipo_de_cuenta and
                   c.Id_cliente = @vId_cliente and
                   c.Cargo_abono = 'A' and
                   c.Fecha_movimiento <= @pFecha_saldo and
                   (c.Fecha_movimiento + isnull(c.Dias_retencion,0)) > @pFecha_saldo
            if @vRetenido_favor is null
                  set @vRetenido_favor = 0

      --    RETENIDO CONTRA
            select @vRetenido_contra = sum(CASE WHEN @vTipo_de_cuenta=0 THEN c.Monto_pesos ELSE c.Monto_moneda END)
              from dbo.REGCTACTECLI c
             where c.Tipo_de_cuenta = @vTipo_de_cuenta and
                   c.Id_cliente = @vId_cliente and
                   c.Cargo_abono = 'C' and
                   c.Fecha_movimiento <= @pFecha_saldo and
                   (c.Fecha_movimiento + isnull(c.Dias_retencion,0)) > @pFecha_saldo
            if @vRetenido_contra is null
                  set @vRetenido_contra = 0

      --    SALDO INICIAL
            select top 1 @vSaldo_inicial = s.Monto_pesos
              from dbo.SALCTACTECLI s
             where s.Tipo_de_cuenta=@vTipo_de_cuenta and
                   s.Id_cliente=@vId_cliente
             order by s.fecha_saldo
            if @vSaldo_inicial is null
                  set @vSaldo_inicial = 0

      --    GRABA SALIDA
            insert into #tmp_clientes
            select @vId_cliente,
                   @vDisponible,
                   @vRetenido_favor,
                   @vRetenido_contra,
                   @vSaldo_inicial,
                   case
                     when @vTipo_de_cuenta = 0 then '$'
                     when @vTipo_de_cuenta = 1 then 'US$'
                     when @vTipo_de_cuenta = 8 then 'EUR'
                   end

            fetch next from rec_clientes
                  into @vId_cliente, @vTipo_de_cuenta
      end
      close rec_clientes
      deallocate rec_clientes

      -- Aqui entrega la salida
      select 'ID_CLIENTE'       = t_Id_cliente,
             'DISPONIBLE'       = t_Disponible,
             'RETENIDO FAVOR'   = t_retenido_favor,
             'RETENIDO CONTRA'  = t_retenido_contra,
             'SALDO INICIAL'    = t_Saldo_inicial,
             'SIMBOLO'          = t_Simbolo
        from #tmp_clientes

        drop table #tmp_clientes

END
GO

GRANT EXECUTE ON [AD_SaldoCtaCte_GPI] TO DB_EXECUTESP
GO