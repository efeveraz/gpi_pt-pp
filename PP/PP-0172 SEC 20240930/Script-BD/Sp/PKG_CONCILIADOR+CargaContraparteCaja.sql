ALTER PROCEDURE PKG_CONCILIADOR$CargaContraparteCaja
( @PFECHA_CONTRAPARTE DATETIME
)
AS
BEGIN
 SET NOCOUNT ON

      DECLARE @LSIMBOLO                    VARCHAR(20)
            , @LCURSOR_CTP$FECHA_SALDO     INTEGER
            , @LCURSOR_CTP$RUT_CLIENTE     VARCHAR(50)
            , @LCURSOR_CTP$NOMBRE_CLIENTE  VARCHAR(200)
            , @LCURSOR_CTP$ID_CUENTA       NUMERIC
            , @LCURSOR_CTP$NUM_CUENTA      VARCHAR(20)
            , @LCURSOR_CTP$ID_EMPRESA      NUMERIC
            , @LCURSOR_CTP$VALOR           VARCHAR(50)
            , @LID_ORIGEN                  NUMERIC
            , @LID_TIPO_CONVERSION         NUMERIC
            , @LCONTRAPARTE                VARCHAR(200)
            , @LTOTAL                      NUMERIC
            , @LFECHA_ULT_CIERRE           DATETIME
            , @LFECHA_SALDOINT             INT

      SELECT @LID_ORIGEN= ID_ORIGEN
        FROM ORIGENES
       WHERE COD_ORIGEN='MAGIC_VALORES'

      SELECT @LID_TIPO_CONVERSION = ID_TIPO_CONVERSION
        FROM TIPOS_CONVERSION
       WHERE TABLA='CUENTAS'

      SELECT @LFECHA_ULT_CIERRE = MAX(FECHA_CIERRE) FROM CIERRES
      IF @PFECHA_CONTRAPARTE > @LFECHA_ULT_CIERRE
       BEGIN
         SET @LTOTAL=0
         GOTO FIN
       END

      CREATE TABLE #TEMP_CONTRAPARTE (ID_CLIENTE  CHAR(15)
                                    , DISPONIBLE  NUMERIC(16,2)
                                    , RETENIDO_FAVOR  NUMERIC(16,2)
                                    , RETENIDO_CONTRA  NUMERIC(16,2)
                                    , SALDO_INICIAL  NUMERIC(16,2)
                                    , SIMBOLO VARCHAR(3))

      SET @LFECHA_SALDOINT = ((CAST((SELECT CONVERT(DATETIME,@PFECHA_CONTRAPARTE)) AS INT)+693596))

      INSERT #TEMP_CONTRAPARTE(ID_CLIENTE, DISPONIBLE, RETENIDO_FAVOR, RETENIDO_CONTRA, SALDO_INICIAL, SIMBOLO)
      EXEC [LNKCISCB].[CisCB].[dbo].[AD_SaldoCtaCte_GPI] @pFecha_saldo = @LFECHA_SALDOINT

      DELETE FROM CONC_CAJA

      INSERT INTO CONC_CAJA (KEY_CONCILIADOR
                           , RUT_CLIENTE
                           , NOMBRE_CLIENTE
                           , NUM_CUENTA
                           , ID_CUENTA
                           , ID_EMPRESA
                           , CUENTA_CONTRAPARTE
                           , SIMBOLO
                           , MONTO)
      SELECT CAST(C.ID_CUENTA AS VARCHAR(12)) + '-' + T.SIMBOLO 'KEY_CONCILIADOR'
           , C.RUT_CLIENTE
           , C.NOMBRE_CLIENTE
           , C.NUM_CUENTA
           , C.ID_CUENTA
           , C.ID_EMPRESA
           , T.ID_CLIENTE
           , T.SIMBOLO
           , T.DISPONIBLE
        FROM VIEW_ALIAS A
           , VIEW_CUENTAS_VIGENTES C
           , #TEMP_CONTRAPARTE T
       WHERE A.COD_ORIGEN = 'MAGIC_VALORES'
         AND A.TABLA      = 'CUENTAS'
         AND A.ID_ENTIDAD = C.ID_CUENTA
         AND A.VALOR      = T.ID_CLIENTE
       ORDER BY A.VALOR

      DROP TABLE #TEMP_CONTRAPARTE

      SELECT @LTOTAL = COUNT(*) FROM CONC_CAJA
FIN:
    SELECT @LTOTAL 'TOTAL_A_CONCILIAR'

    SET NOCOUNT OFF
END
GO