ALTER PROCEDURE PKG_CONCILIADOR$CAJAS
( @PFECHA_CIERRE DATETIME
, @PID_CUENTA    NUMERIC=NULL
)
AS
BEGIN
 SET NOCOUNT ON

      DECLARE @LCURSOR$KEY_CONCILIADOR     VARCHAR(100)
            , @LCURSOR$RUT_CLIENTE         VARCHAR(30)
            , @LCURSOR$NOMBRE_CLIENTE      VARCHAR(300)
            , @LCURSOR$NUM_CUENTA          VARCHAR(20)
            , @LCURSOR$ID_CUENTA           NUMERIC
            , @LCURSOR$ID_EMPRESA          NUMERIC
            , @LCURSOR$SIMBOLO             VARCHAR(20)
            , @LCURSOR$MONTO               NUMERIC(28,4)
            , @LID_ORIGEN                  NUMERIC
            , @LID_TIPO_CONVERSION         NUMERIC
            , @LCONTRAPARTE                VARCHAR(200)

      SELECT @LID_ORIGEN= ID_ORIGEN
        FROM ORIGENES
       WHERE COD_ORIGEN='MAGIC_VALORES'

      SELECT @LID_TIPO_CONVERSION = ID_TIPO_CONVERSION
        FROM TIPOS_CONVERSION
       WHERE TABLA='CUENTAS'

-----------------------------------------------------
-- CONCILIACION
-----------------------------------------------------
     CREATE TABLE #TEMP_CONCILIA_RES(KEY_CONCILIADOR    VARCHAR(200)
                                   , RUT_CLIENTE        VARCHAR(20)
                                   , NOMBRE_CLIENTE     VARCHAR(200)
                                   , NUM_CUENTA         VARCHAR(20)
                                   , ID_EMPRESA         NUMERIC
                                   , CONTRAPARTE        VARCHAR(100)
                                   , CAJA               VARCHAR(100)
                                   , GPI_MONTO          NUMERIC(28,4)
                                   , CTP_MONTO          NUMERIC(28,4)
                                   , DIF_MONTO          NUMERIC(28,4)
                                   , ESTADO             VARCHAR(3)
                                   )

     CREATE TABLE #TEMP_CONTRAPARTES(KEY_CONCILIADOR    VARCHAR(200)
                                   , RUT_CLIENTE        VARCHAR(20)
                                   , NOMBRE_CLIENTE     VARCHAR(200)
                                   , NUM_CUENTA         VARCHAR(20)
                                   , ID_CUENTA          NUMERIC
                                   , ID_EMPRESA         NUMERIC
                                   , SIMBOLO            VARCHAR(20)
                                   , MONTO              NUMERIC(28,4)
                                   , CONTRAPARTE        VARCHAR(100)
                                   )

     INSERT INTO #TEMP_CONCILIA_RES (KEY_CONCILIADOR
                                   , RUT_CLIENTE
                                   , NOMBRE_CLIENTE
                                   , NUM_CUENTA
                                   , ID_EMPRESA
                                   , CONTRAPARTE
                                   , CAJA
                                   , CTP_MONTO
                                   , ESTADO)
     SELECT KEY_CONCILIADOR
          , RUT_CLIENTE
          , NOMBRE_CLIENTE
          , NUM_CUENTA
          , ID_EMPRESA
          , CUENTA_CONTRAPARTE
          , SIMBOLO
          , SUM(MONTO)
          , 'CTP'
       FROM CONC_CAJA
      WHERE ID_CUENTA = ISNULL(@PID_CUENTA,ID_CUENTA)
      GROUP BY KEY_CONCILIADOR
             , RUT_CLIENTE
             , NOMBRE_CLIENTE
             , NUM_CUENTA
             , ID_EMPRESA
             , CUENTA_CONTRAPARTE
             , SIMBOLO

      INSERT INTO #TEMP_CONTRAPARTES
      SELECT KEY_CONCILIADOR
           , RUT_CLIENTE
           , NOMBRE_CLIENTE
           , NUM_CUENTA
           , ID_CUENTA
           , ID_EMPRESA
           , SIMBOLO
           , ROUND(SUM(MONTO_MON_CAJA), DICIMALES_MOSTRAR)
           , (SELECT VALOR FROM REL_CONVERSIONES
                   WHERE ID_ORIGEN = @LID_ORIGEN
                     AND ID_ENTIDAD = ID_CUENTA
                     AND ID_TIPO_CONVERSION = @LID_TIPO_CONVERSION)
        FROM (SELECT CAST(SC.ID_CUENTA AS VARCHAR(12)) + '-' + MO.SIMBOLO 'KEY_CONCILIADOR'
                   , CA.RUT_CLIENTE
                   , CA.NOMBRE_CLIENTE
                   , CA.NUM_CUENTA
                   , CA.ID_CUENTA
                   , CA.ID_EMPRESA
                   , MO.SIMBOLO
                   , SC.MONTO_MON_CAJA
                   , MO.DICIMALES_MOSTRAR
                FROM VIEW_SALDOS_CAJA SC,
                     MONEDAS MO,
                     CAJAS_CUENTA CC,
                     VIEW_CUENTAS_VIGENTES CA
               WHERE SC.FECHA_CIERRE   = @PFECHA_CIERRE
                 AND MO.ID_MONEDA      = SC.ID_MONEDA_CAJA
                 AND SC.ID_CAJA_CUENTA = CC.ID_CAJA_CUENTA
                 AND CC.COD_MERCADO    = 'N'
                 AND MO.ID_MONEDA      = CC.ID_MONEDA
                 AND CA.ID_CUENTA      = CC.ID_CUENTA
                 AND CA.ID_CUENTA      = SC.ID_CUENTA
                 AND CA.ID_CUENTA      = ISNULL (@PID_CUENTA, CA.ID_CUENTA)  ) TEMP
       GROUP BY KEY_CONCILIADOR
              , RUT_CLIENTE
              , NOMBRE_CLIENTE
              , NUM_CUENTA
              , ID_CUENTA
              , ID_EMPRESA
              , SIMBOLO
              , DICIMALES_MOSTRAR


      UPDATE #TEMP_CONCILIA_RES
         SET #TEMP_CONCILIA_RES.GPI_MONTO = #TEMP_CONTRAPARTES.MONTO,
             #TEMP_CONCILIA_RES.ESTADO    = 'VIG'
        FROM #TEMP_CONCILIA_RES
       INNER JOIN #TEMP_CONTRAPARTES ON #TEMP_CONCILIA_RES.KEY_CONCILIADOR = #TEMP_CONTRAPARTES.KEY_CONCILIADOR
       WHERE #TEMP_CONCILIA_RES.ESTADO = 'CTP' AND
             #TEMP_CONCILIA_RES.CONTRAPARTE = #TEMP_CONTRAPARTES.CONTRAPARTE


      INSERT INTO #TEMP_CONCILIA_RES (KEY_CONCILIADOR
                                    , RUT_CLIENTE
                                    , NOMBRE_CLIENTE
                                    , NUM_CUENTA
                                    , ID_EMPRESA
                                    , CONTRAPARTE
                                    , CAJA
                                    , GPI_MONTO
                                    , CTP_MONTO
                                    , DIF_MONTO
                                    , ESTADO)
      SELECT A.KEY_CONCILIADOR
           , A.RUT_CLIENTE
           , A.NOMBRE_CLIENTE
           , A.NUM_CUENTA
           , A.ID_EMPRESA
           , A.CONTRAPARTE
           , A.SIMBOLO
           , A.MONTO
           , 0
           , 0
           , 'GPI'
        FROM #TEMP_CONTRAPARTES A
       WHERE NOT EXISTS (SELECT 1 FROM #TEMP_CONCILIA_RES WHERE KEY_CONCILIADOR = A.KEY_CONCILIADOR)

      UPDATE #TEMP_CONCILIA_RES
         SET DIF_MONTO    = ISNULL(GPI_MONTO,0) - ISNULL(CTP_MONTO,0)
       WHERE ESTADO = 'VIG'

      UPDATE #TEMP_CONCILIA_RES
         SET ESTADO = 'OK'
       WHERE DIF_MONTO = 0
         AND ESTADO = 'VIG'

      UPDATE #TEMP_CONCILIA_RES
         SET ESTADO = 'NOK'
       WHERE DIF_MONTO <> 0
         AND ESTADO = 'VIG'

      SELECT E.DSC_EMPRESA
           , KEY_CONCILIADOR
           , RUT_CLIENTE
           , NOMBRE_CLIENTE
           , NUM_CUENTA
           , T.ID_EMPRESA
           , CONTRAPARTE
           , CAJA
           , GPI_MONTO
           , CTP_MONTO
           , DIF_MONTO
           , ESTADO
           , M.DICIMALES_MOSTRAR
        FROM #TEMP_CONCILIA_RES T
           , EMPRESAS E
           , MONEDAS M
       WHERE E.ID_EMPRESA = T.ID_EMPRESA
         AND T.CAJA = M.SIMBOLO
       ORDER BY T.ID_EMPRESA
              , NUM_CUENTA
              , CAJA

      DROP TABLE #TEMP_CONCILIA_RES
      DROP TABLE #TEMP_CONTRAPARTES

    SET NOCOUNT OFF
END
GO