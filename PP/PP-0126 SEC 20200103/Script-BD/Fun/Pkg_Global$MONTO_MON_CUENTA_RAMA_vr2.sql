USE CSGPI
GO
IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'Pkg_Global$MONTO_MON_CUENTA_RAMA_vr2')
   DROP FUNCTION Pkg_Global$MONTO_MON_CUENTA_RAMA_vr2
GO
CREATE FUNCTION [dbo].[Pkg_Global$MONTO_MON_CUENTA_RAMA_vr2]
(
    @PID_ARBOL_CLASE    NUMERIC,
    @PID_CUENTA         NUMERIC,
    @PFECHA_CIERRE      DATETIME
)
  RETURNS FLOAT
AS
BEGIN

    DECLARE @MONTO_MON_CTA   FLOAT
    DECLARE @LDECIMALES      NUMERIC
    DECLARE @LDSC_ARBOL      VARCHAR(50)
          , @LDSC_PADRE      VARCHAR(50)
          , @LMONTO_PERSHING FLOAT
          , @LID_MONEDA_PESO NUMERIC
          , @LTOTAL_FWD      NUMERIC(28,4)
          , @LMONTO_INT      NUMERIC(28,4)
          , @LMONTO_NAC      NUMERIC(28,4)
          , @LID_MONEDA_USD  NUMERIC
          , @LID_MONEDA_CTA  NUMERIC
          , @LTOTAL_VC       NUMERIC(28,4)
          , @LTOTAL_DEVVC    NUMERIC(28,4)

 SET @MONTO_MON_CTA = 0
 SET @LTOTAL_FWD    = 0
 SET @LTOTAL_VC     = 0
 SET @LTOTAL_DEVVC	= 0
 SET @LMONTO_INT    = 0
 SET @LMONTO_NAC    = 0

 SELECT @LID_MONEDA_PESO = DBO.FNT_DAMEIDMONEDA('$$')
 SELECT @LID_MONEDA_USD = DBO.FNT_DAMEIDMONEDA('USD')

 SELECT @LDSC_PADRE = DSC_ARBOL_CLASE_INST
     FROM ARBOL_CLASE_INSTRUMENTO A
    WHERE A.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE

    SELECT @LDECIMALES=DICIMALES_MOSTRAR
   FROM MONEDAS
  WHERE ID_MONEDA = (SELECT ID_MONEDA FROM CUENTAS WHERE ID_CUENTA=@PID_CUENTA)

     SELECT @LID_MONEDA_CTA = ID_MONEDA FROM CUENTAS WHERE ID_CUENTA=@PID_CUENTA

    IF @LDSC_PADRE = 'Otros Activos'
     BEGIN

         SELECT @LTOTAL_FWD = ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                                              , SD.VMM
                                                                              , @LID_MONEDA_PESO
                                                                              , @LID_MONEDA_CTA
                                                                              , @PFECHA_CIERRE)) ,0)
           FROM VIEW_SALDOS_DERIVADOS SD
          WHERE SD.ID_CUENTA = @pID_CUENTA
            AND SD.FECHA_CIERRE = @PFECHA_CIERRE
            AND SD.COD_INSTRUMENTO = 'FWD_NAC'

--         SELECT @LTOTAL_VC = (ISNULL(SUM(VC.PRECIO_MEDIO_MERCADO),0) - ISNULL(SUM(VC.PRECIO_MEDIO),0)) + SUM(ISNULL(VC.PRIMA_ACUMULADA,0))
--           FROM VENTA_CORTA VC
--          WHERE VC.ID_CUENTA  = @PID_CUENTA
--            AND VC.FECHA_DE_MVTO = @PFECHA_CIERRE

        SELECT @LTOTAL_VC = SUM(
                                ISNULL((V.cantidad * V.precio_medio), 0) -
                                ISNULL((V.cantidad * VCD.precio_medio_mercado), 0) -
                                ISNULL(VCD.PRIMA_ACUMULADA, 0)
                                )
          FROM VENTA_CORTA V,
               VENTA_CORTA_DEVENGO VCD
        WHERE V.ID_CUENTA  = @pID_CUENTA
          AND @PFECHA_CIERRE BETWEEN V.FECHA_DE_MVTO AND V.FECHA_VENCIMIENTO
          AND V.COD_ESTADO = 'C'
          AND V.ID_VTA_CORTA = VCD.ID_VTA_CORTA
          AND V.FOLIO = VCD.FOLIO
          AND V.FECHA_DE_MVTO = VCD.FECHA_DE_MVTO
          AND VCD.FECHA_DE_DEVENGO = @PFECHA_CIERRE

         SET @LTOTAL_DEVVC = @LTOTAL_FWD + @LTOTAL_VC

         IF @LTOTAL_DEVVC < 0
          BEGIN
               SET @LTOTAL_DEVVC = 0
          END

         SELECT @LMONTO_INT = ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                                           , VALOR_MERCADO_MON_USD
                                                                           , @LID_MONEDA_USD
                                                                           , @LID_MONEDA_CTA
                                                                           , @PFECHA_CIERRE))   ,0)
           FROM SALDOS_ACTIVOS_INT S
              , CUENTAS C
          WHERE C.ID_CUENTA = @PID_CUENTA
            AND S.ID_CUENTA = C.ID_CUENTA
            AND S.FECHA_CIERRE     = @PFECHA_CIERRE
            AND S.ID_NEMOTECNICO  IN (SELECT ID_NEMOTECNICO
                                        FROM REL_ACI_EMP_NEMOTECNICO
                                       WHERE ID_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST
                                                                      FROM ARBOL_CLASE_INSTRUMENTO
                                                                     WHERE ID_PADRE_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
                                                                     )
                                     )

  SELECT @LMONTO_NAC  = ISNULL(SUM(ROUND(MONTO_MON_CTA,@LDECIMALES)),0)
    FROM SALDOS_ACTIVOS
   WHERE ID_CUENTA    = @PID_CUENTA
     AND FECHA_CIERRE    = @PFECHA_CIERRE
     AND ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO
         FROM REL_ACI_EMP_NEMOTECNICO
           WHERE ID_ARBOL_CLASE_INST IN(SELECT ID_ARBOL_CLASE_INST
                  FROM ARBOL_CLASE_INSTRUMENTO
               WHERE ID_PADRE_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
                )
          AND ID_EMPRESA = (SELECT ID_EMPRESA
               FROM CUENTAS
              WHERE ID_CUENTA = @PID_CUENTA
              )
             )
          SET @MONTO_MON_CTA = ISNULL(@LMONTO_NAC,0) + ISNULL(@LTOTAL_FWD,0) + ISNULL(@LMONTO_INT,0)
     END
    ELSE
    BEGIN
        set @MONTO_MON_CTA = 0
        IF @LDSC_PADRE = 'Renta Variable Internacional' OR @LDSC_PADRE = 'Renta Fija Internacional'
         BEGIN
             SELECT @LMONTO_INT = ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(@PID_CUENTA
                                                                           , VALOR_MERCADO_MON_USD
                                                                           , @LID_MONEDA_USD
                                                                           , @LID_MONEDA_CTA
                                                                           , @PFECHA_CIERRE))   ,0)
               FROM SALDOS_ACTIVOS_INT S
                  , CUENTAS C
              WHERE C.ID_CUENTA = @PID_CUENTA
                AND S.ID_CUENTA = C.ID_CUENTA
                AND S.FECHA_CIERRE     = @PFECHA_CIERRE
                AND S.ID_NEMOTECNICO  IN (SELECT ID_NEMOTECNICO
                                        FROM REL_ACI_EMP_NEMOTECNICO
                                       WHERE ID_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST
                                                                      FROM ARBOL_CLASE_INSTRUMENTO
                                                                     WHERE ID_PADRE_ARBOL_CLASE_INST = @PID_ARBOL_CLASE))


         END

  SELECT @LMONTO_NAC  = SUM(ROUND(MONTO_MON_CTA,@LDECIMALES))
    FROM SALDOS_ACTIVOS
   WHERE ID_CUENTA       = @PID_CUENTA
     AND FECHA_CIERRE    = @PFECHA_CIERRE
     AND ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO
         FROM REL_ACI_EMP_NEMOTECNICO
           WHERE ID_ARBOL_CLASE_INST IN(SELECT ID_ARBOL_CLASE_INST
                  FROM ARBOL_CLASE_INSTRUMENTO
                 WHERE ID_PADRE_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
                )
          AND ID_EMPRESA = (SELECT ID_EMPRESA
               FROM CUENTAS
              WHERE ID_CUENTA = @PID_CUENTA
              )
          )
     SET @MONTO_MON_CTA = ISNULL(@LMONTO_NAC,0) + ISNULL(@LMONTO_INT,0)
 END

    RETURN @MONTO_MON_CTA

END

GO
GRANT EXECUTE ON [Pkg_Global$MONTO_MON_CUENTA_RAMA_vr2] TO DB_EXECUTESP
GO