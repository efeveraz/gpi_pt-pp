USE CSGPI
GO
IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PKG_CONCILIADOR$CargaContraparteSaldosInv')
   DROP PROCEDURE PKG_CONCILIADOR$CargaContraparteSaldosInv
GO
CREATE PROCEDURE [dbo].[PKG_CONCILIADOR$CargaContraparteSaldosInv]
( @PFECHA_CONTRAPARTE DATETIME
)
AS
BEGIN
    SET NOCOUNT ON

         DECLARE @LID_CUENTA            NUMERIC
               , @LCODRESULTADO         VARCHAR(10)
               , @LMSGRESULTADO         VARCHAR(800)
               , @LCOD_ORIGEN           VARCHAR(20)
               , @LID_NEMOTECNICO       NUMERIC
               , @LNEMOTECNICO          VARCHAR(50)
               , @LCOD_INSTRUMENTO      VARCHAR(20)
               , @LCODIGO_ISIN          VARCHAR(12)
               , @LID_MONEDA_USD        NUMERIC
               , @LID_MONEDA_EUR        NUMERIC
               , @LID_MONEDA_CLP        NUMERIC
               , @LID_EMISOR_ESPECIFICO NUMERIC
               , @LID_EMISOR_GENERAL    NUMERIC
               , @LFECHAVARCHAR         VARCHAR(10)

     SET @LMSGRESULTADO = ''
     SET @LCODRESULTADO = 'OK'
---------------------------------------------------------------------------------
     SELECT @LID_MONEDA_USD = ID_MONEDA FROM MONEDAS WHERE COD_MONEDA='USD'
     SELECT @LID_MONEDA_EUR = ID_MONEDA FROM MONEDAS WHERE COD_MONEDA='EUR'
     SELECT @LID_MONEDA_CLP = ID_MONEDA FROM MONEDAS WHERE COD_MONEDA='$$'

     SET @LFECHAVARCHAR = CONVERT(VARCHAR(8),@PFECHA_CONTRAPARTE, 112)

    CREATE TABLE #Tabla1  (FECHA             VARCHAR(10),
                           NOMBRE_MONEDA     VARCHAR(70),
                           CODIGO_MONEDA     VARCHAR(10),
                           CODIGO_MONEDA_SVS VARCHAR(10),
                           PARIDAD           NUMERIC(18,4),
                           TIPO_CAMBIO_USD   NUMERIC(18,4))

     INSERT #Tabla1
     exec [FFMM].[MG_SAFMUTNW].[dbo].[sp_fm_cargaMGSafmutNW$ParidadesMoneda] @LFECHAVARCHAR

     DECLARE @vValorEuro NUMERIC(18,4)

      SELECT @vValorEuro = TIPO_CAMBIO_USD
        FROM #Tabla1
       WHERE CODIGO_MONEDA_SVS = 'EUR'
---------------------------------------------------------------------------------

    CREATE TABLE #SALDOS_INV (CUENTA_EXTERNA_CLIENTE       VARCHAR(28),
                              CODIGO_ISIN                  VARCHAR(12),
                              TIPO_PRODUCTO                VARCHAR(10),
                              TIPO_ACTIVO                  VARCHAR(10),
                              DESCRIPCION_VALOR            VARCHAR(40),
                              CODIGO_EMISOR_VALOR          INTEGER,
                              DESCRIPCION_EMISOR           VARCHAR(40),
                              CODIGO_DIVISA_COTIZACION     VARCHAR(3),
                              DSC_DIVISA_COTIZACION        VARCHAR(50),
                              TOTAL_TITULOS                FLOAT,
                              TOTAL_NOMINAL                FLOAT,
                              COSTE_MEDIO_POSICION         FLOAT,
                              PRECIO_MERCADO_COTIZACION    FLOAT,
                              RATIO_CONVERSION_CONTRA_EURO FLOAT,
                              FECHA_VENCIMIENTO            VARCHAR(8),
                              PRECIO                       FLOAT,
                              VALOR                        FLOAT,
                              CODIGO_MERCADO_DEFECTO       VARCHAR(10))

   DELETE FROM CONC_SALDOS_INV


   INSERT #SALDOS_INV(CUENTA_EXTERNA_CLIENTE,
                      CODIGO_ISIN,
                      TIPO_PRODUCTO,
                      TIPO_ACTIVO,
                      DESCRIPCION_VALOR,
                      CODIGO_EMISOR_VALOR,
                      DESCRIPCION_EMISOR,
                      CODIGO_DIVISA_COTIZACION,
                      DSC_DIVISA_COTIZACION,
                      TOTAL_TITULOS,
                      TOTAL_NOMINAL,
                      COSTE_MEDIO_POSICION,
                      PRECIO_MERCADO_COTIZACION,
                      RATIO_CONVERSION_CONTRA_EURO,
                      FECHA_VENCIMIENTO,
                      PRECIO,
                      VALOR,
                      CODIGO_MERCADO_DEFECTO)

   EXEC PKG_INVERSIS$Importador_Saldos @PFECHA_CONTRAPARTE

   INSERT INTO CONC_SALDOS_INV(FECHA
                             , ID_CUENTA
                             , ID_NEMOTECNICO
                             , CUENTA_EXTERNA_CLIENTE
                             , CODIGO_ISIN
                             , NEMOTECNICO
                             , CANTIDAD
                             , PRECIO_MERCADO
                             , VALOR_MERCADO
                              )
   SELECT @PFECHA_CONTRAPARTE AS FECHA
        , ISNULL(VA.ID_ENTIDAD,-1) AS ID_CUENTA
        , ISNULL((SELECT TOP 1 ID_NEMOTECNICO FROM REL_NEMOTECNICO_ATRIBUTOS WHERE ISIM = SI.CODIGO_ISIN),-1)
        , CUENTA_EXTERNA_CLIENTE
        , CODIGO_ISIN
        , DESCRIPCION_VALOR
        , CASE TIPO_PRODUCTO
             WHEN 'RV' THEN TOTAL_TITULOS
             WHEN 'IIC' THEN TOTAL_TITULOS
             ELSE TOTAL_NOMINAL
          END AS 'CANTIDAD'
        , PRECIO_MERCADO_COTIZACION AS PRECIO
        , ROUND(CASE TIPO_PRODUCTO
             WHEN 'RV' THEN
                CASE
                   WHEN (CODIGO_DIVISA_COTIZACION = 'USD') THEN (PRECIO_MERCADO_COTIZACION * TOTAL_TITULOS)
                   ELSE (((PRECIO_MERCADO_COTIZACION * TOTAL_TITULOS) * RATIO_CONVERSION_CONTRA_EURO) / @vValorEuro)
                END
             WHEN 'IIC' THEN
                CASE
                   WHEN (CODIGO_DIVISA_COTIZACION = 'USD') THEN (PRECIO_MERCADO_COTIZACION * TOTAL_TITULOS)
                   ELSE (((PRECIO_MERCADO_COTIZACION * TOTAL_TITULOS) * RATIO_CONVERSION_CONTRA_EURO) / @vValorEuro)
                END
             ELSE
                CASE
                   WHEN (CODIGO_DIVISA_COTIZACION = 'USD') THEN ((PRECIO_MERCADO_COTIZACION * TOTAL_NOMINAL)/100)
                   ELSE ((((PRECIO_MERCADO_COTIZACION * TOTAL_NOMINAL)/100) * RATIO_CONVERSION_CONTRA_EURO) / @vValorEuro)
                END
          END, 2)
     FROM #SALDOS_INV SI
     LEFT JOIN VIEW_ALIAS VA on SI.CUENTA_EXTERNA_CLIENTE = VA.VALOR
                               and VA.TABLA = 'CUENTAS' AND VA.COD_ORIGEN = 'MAGIC_VALORES'


   --SELECT 'OK'
   DROP TABLE #SALDOS_INV
END
GO
GRANT EXECUTE ON [PKG_CONCILIADOR$CargaContraparteSaldosInv] TO DB_EXECUTESP
GO

