USE [CSGPI]
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_CERT24_DJ1899$DJ1899_Registro01]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_CERT24_DJ1899$DJ1899_Registro01]
GO

CREATE PROCEDURE [dbo].[PKG_CERT24_DJ1899$DJ1899_Registro01]
( @PTIPO_DECLARACION CHAR(1) = NULL  -- TIPO PARA INDICAR SI ES ORIGINAL O RECTIFICATORIA VIENE DESDE GPI
, @PNRO_FOLIO        NUMERIC   -- NRO FOLIO. OBTENERLO EN EL MISMO QUERY O DESDE GPI
, @PANNO_TRIBUTARIO  NUMERIC
) AS
BEGIN
     SET NOCOUNT ON

	  IF (@PTIPO_DECLARACION = 'O')
      BEGIN
	 	 SET @PNRO_FOLIO = 9165101
      END
      ELSE
	  BEGIN
		 SET @PNRO_FOLIO = ISNULL((SELECT MAX(NRO_FOLIO) FROM DECLARACION_JURADA_1899 WHERE ANNO_TRIBUTARIO = @PANNO_TRIBUTARIO), @PNRO_FOLIO) + 1
	  END

      SELECT 1                                 AS 'TIPO_REGISTRO'
           , 1                                 AS 'ORDEN'
           , @PANNO_TRIBUTARIO                 AS 'ANNO_TRIBUTARIO'
           , 1899                              AS 'NUMERO_FORMULARIO'
           , 'I'                               AS 'CODIGO_PRESENTACION'
           , @PNRO_FOLIO                       AS 'NUMERO_FOLIO'			-- OBTENER FOLIO
           , @PTIPO_DECLARACION                AS 'TIPO_DECLARACION'
           , 'VALORES SECURITY SA CORREDORES'  AS 'RAZON_SOCIAL'			-- EN GPI DEBE COMPLETAR 30 CARACTERES
           , 'AV. APOQUINDO  # 3150  PISO 7'   AS 'DIRECCION_POSTAL'		-- EN GPI DEBE COMPLETAR 35 CARACTERES
           , 'LAS CONDES'                      AS 'COMUNA'					-- EN GPI DEBE COMPLETAR 15 CARACTERES
           , 'piero.nasi@security.cl'          AS 'CORREO_ELECTRONICO'		-- EN GPI DEBE COMPLETAR 30 CARACTERES
           , 56025844017                       AS 'FAX'
           , 56025844700                       AS 'TELEFONO'
           , '91'                              AS 'CODIGO_EMPRESA'
           , '0020'                            AS 'NUMERO_CLIENTE'
           , '00000'                           AS 'UNIDAD_CAJA'
           , '000'                             AS 'NUMERO_CAJA'
           , '00000'                           AS 'NUMERO_PAQUETE'

    SET NOCOUNT OFF
END
GO
GRANT EXECUTE ON [PKG_CERT24_DJ1899$DJ1899_Registro01] TO DB_EXECUTESP
GO