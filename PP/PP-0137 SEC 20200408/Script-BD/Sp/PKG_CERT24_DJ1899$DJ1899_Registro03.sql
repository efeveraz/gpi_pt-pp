USE [CSGPI]
GO

IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_CERT24_DJ1899$DJ1899_Registro03]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_CERT24_DJ1899$DJ1899_Registro03]
GO

CREATE PROCEDURE [dbo].[PKG_CERT24_DJ1899$DJ1899_Registro03]
( @PID_EMPRESA       AS NUMERIC
, @PID_CLIENTE       AS NUMERIC = NULL
, @PANNO_REPORTE     AS NUMERIC
, @PTIPO_DECLARACION AS CHAR(1) = NULL
) AS
BEGIN
      SET NOCOUNT ON

	  DECLARE @PNRO_FOLIO NUMERIC(7)

	  CREATE TABLE #TEMP_SALIDA  ( TIPO_REGISTRO  NUMERIC
							,ORDEN	 NUMERIC
							,NRO_FORMULARIO NUMERIC
							,CODIGO_PRESENTACION  VARCHAR(4)
							,NRO_FOLIO	 NUMERIC
							,DC_UF FLOAT
							,COTIZACION  FLOAT
							,AVC  FLOAT
							,AVC_TRASPASO  FLOAT
							,AVC_RETIRO  FLOAT
							,AVC_RETENCION  FLOAT
							,AHORRO_INDIREC  FLOAT
							,AHORRO_DIREC  FLOAT
							,DAPV_INDIREC  FLOAT
							,DAPV_DIREC  FLOAT
							,DAPV_CV  FLOAT)

	  IF (@PTIPO_DECLARACION = 'O')
      BEGIN
	 	 SET @PNRO_FOLIO = 9165101
      END
      ELSE
	  BEGIN
		 SET @PNRO_FOLIO = ISNULL((SELECT MAX(NRO_FOLIO) FROM DECLARACION_JURADA_1899 WHERE ANNO_TRIBUTARIO = @PANNO_REPORTE), @PNRO_FOLIO) + 1
	  END

      DECLARE @LID_TIPOCUENTA        AS NUMERIC
      DECLARE @LAPO_DAPV_CV_B        AS FLOAT
	  DECLARE @LAPO_DAPV_CV_B_I      AS FLOAT
      DECLARE @LAPO_DAPV_B           AS FLOAT

      SELECT @LID_TIPOCUENTA = ID_TIPOCUENTA FROM TIPO_CUENTAS WHERE DESCRIPCION_CORTA='APV'

      SET @LAPO_DAPV_CV_B = 0
	  SELECT @LAPO_DAPV_CV_B = ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                        ISNULL(AR.MONTO,0),
                                                                        ID_MONEDA,
                                                                        DBO.FNT_DAMEIDMONEDA('UF'),
                                                                        AR.FECHA_MOVIMIENTO)),0)
        FROM APORTE_RESCATE_CUENTA  AR
           , TIPOS_AHORRO_APV TA
           , VIEW_CUENTAS_VIGENTES_CON_CAJA CU
		   , CLIENTES CL
       WHERE CU.ID_EMPRESA             = @pId_Empresa
         AND CU.ID_CLIENTE             = ISNULL(@PID_CLIENTE,CU.ID_CLIENTE)
         AND CU.ID_TIPOCUENTA          = @LID_TIPOCUENTA
         AND AR.ID_CUENTA              = CU.ID_CUENTA
         AND AR.FLG_TIPO_MOVIMIENTO    = 'A'
         AND YEAR(AR.FECHA_MOVIMIENTO) = @PANNO_REPORTE - 1
         AND AR.ID_TIPO_AHORRO         IN (SELECT ID_TIPO_AHORRO FROM TIPOS_AHORRO_APV WHERE DESCRIPCION_CORTA IN ('APV-B','CV REGIMEN B'))
         AND TA.ID_TIPO_AHORRO         = AR.ID_TIPO_AHORRO
         AND AR.COD_ESTADO             <> dbo.Pkg_Global$cCod_Estado_Anulado()
         AND CL.ID_CLIENTE             = ISNULL(@PID_CLIENTE,CU.ID_CLIENTE)
         AND CL.TIPO_TRABAJADOR        = 'D'


      SET @LAPO_DAPV_B = 0
	  --SELECT @LAPO_DAPV_B = ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
   --                                                                     ISNULL(AR.MONTO,0),
   --                                                                     ID_MONEDA,
   --                                                                     DBO.FNT_DAMEIDMONEDA('UF'),
   --                                                                     AR.FECHA_MOVIMIENTO)),0)
   --     FROM APORTE_RESCATE_CUENTA  AR
   --        , TIPOS_AHORRO_APV TA
   --        , VIEW_CUENTAS_VIGENTES_CON_CAJA CU
		 --  , CLIENTES CL
   --    WHERE CU.ID_EMPRESA             = @pId_Empresa
   --      AND CU.ID_CLIENTE             = ISNULL(@PID_CLIENTE,CU.ID_CLIENTE)
   --      AND CU.ID_TIPOCUENTA          = @LID_TIPOCUENTA
   --      AND AR.ID_CUENTA              = CU.ID_CUENTA
   --      AND AR.FLG_TIPO_MOVIMIENTO    = 'A'
   --      AND YEAR(AR.FECHA_MOVIMIENTO) = @PANNO_REPORTE - 1
   --      AND AR.ID_TIPO_AHORRO         IN (SELECT ID_TIPO_AHORRO FROM TIPOS_AHORRO_APV WHERE DESCRIPCION_CORTA IN ('APV-B'))
   --      AND TA.ID_TIPO_AHORRO         = AR.ID_TIPO_AHORRO
   --      AND AR.COD_ESTADO             <> dbo.Pkg_Global$cCod_Estado_Anulado()
   --      AND CL.ID_CLIENTE             = ISNULL(@PID_CLIENTE,CU.ID_CLIENTE)
   --      AND CL.TIPO_TRABAJADOR        = 'D'

      SET @LAPO_DAPV_CV_B_I = 0
	  SELECT @LAPO_DAPV_CV_B_I = ISNULL(SUM(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                        ISNULL(AR.MONTO,0),
                                                                        ID_MONEDA,
                                                                        DBO.FNT_DAMEIDMONEDA('UF'),
                                                                        AR.FECHA_MOVIMIENTO)), 0)
        FROM APORTE_RESCATE_CUENTA  AR
           , TIPOS_AHORRO_APV TA
           , VIEW_CUENTAS_VIGENTES_CON_CAJA CU
           , CLIENTES CL
       WHERE CU.ID_EMPRESA             = @pId_Empresa
         AND CU.ID_CLIENTE             = ISNULL(@PID_CLIENTE,CU.ID_CLIENTE)
         AND CU.ID_TIPOCUENTA          = @LID_TIPOCUENTA
         AND AR.ID_CUENTA              = CU.ID_CUENTA
         AND AR.FLG_TIPO_MOVIMIENTO    = 'A'
         AND YEAR(AR.FECHA_MOVIMIENTO) = @PANNO_REPORTE - 1
         AND AR.ID_TIPO_AHORRO         IN (SELECT ID_TIPO_AHORRO FROM TIPOS_AHORRO_APV WHERE DESCRIPCION_CORTA IN ('APV-B','CV REGIMEN B'))
         AND TA.ID_TIPO_AHORRO         = AR.ID_TIPO_AHORRO
         AND AR.COD_ESTADO             <> dbo.Pkg_Global$cCod_Estado_Anulado()
         AND CL.ID_CLIENTE             = ISNULL(@PID_CLIENTE,CU.ID_CLIENTE)
         AND CL.TIPO_TRABAJADOR        = 'I'


      INSERT #TEMP_SALIDA
      SELECT 3                                             AS 'TIPO_REGISTRO'
           , 1                                             AS 'ORDEN'
           , 1899                                          AS 'NRO_FORMULARIO'
           , 'I'                                           AS 'CODIGO_PRESENTACION'
           , @PNRO_FOLIO                                   AS 'NRO_FOLIO'
           , 0                                             AS 'DC_UF'
           , 0                                             AS 'COTIZACION'
		   , 0											   AS 'AVC'
		   , 0											   AS 'AVC_TRASPASO'
		   , 0											   AS 'AVC_RETIRO'
		   , 0											   AS 'AVC_RETENCION'
		   , 0											   AS 'AHORRO_INDIREC'
		   , 0											   AS 'AHORRO_DIREC'
           , @LAPO_DAPV_CV_B_I                             AS 'DAPV_INDIREC'
           , @LAPO_DAPV_B                                  AS 'DAPV_DIREC'
           , @LAPO_DAPV_CV_B                               AS 'DAPV_CV'            -- EN GPI SEPARAR LA PARTE ENTERA CON LA DECIMAL

      INSERT #TEMP_SALIDA
      SELECT 3														AS 'TIPO_REGISTRO'
		    , 1														AS 'ORDEN'
	        , 1899													AS 'NRO_FORMULARIO'
	        , 'I'													AS 'CODIGO_PRESENTACION'
	        , @PNRO_FOLIO											AS 'NRO_FOLIO'
		    , SUM(CAST(CAST(AKAIREP.AID8XT AS VARCHAR(50)) + '.' + CAST(AKAIREP.AID9XT AS VARCHAR(50)) AS DECIMAL(24,3))) AS 'DC_UF'
            , 0														AS 'COTIZACION'
            , 0                                                     AS 'AVC'
            , 0                                                     AS 'AVC_TRASPASO'
            , 0                                                     AS 'AVC_RETIRO'
            , 0                                                     AS 'AVC_RETENCION'
            , 0                                                     AS 'AHORRO_INDIREC'
            , 0                                                     AS 'AHORRO_DIREC'
            , SUM(CAST(CAST(AKAIREP.AIEAXT AS VARCHAR(50))  + '.' + CAST(AKAIREP.AIEBXT AS VARCHAR(50)) AS DECIMAL(24,3))) AS 'DAPV_INDIREC'
            , SUM(CAST(CAST(AKAIREP.AIECXT AS VARCHAR(50))  + '.' + CAST(AKAIREP.AIEDXT AS VARCHAR(50)) AS DECIMAL(24,3))) AS 'DAPV_DIREC'
            , SUM(CAST(CAST(AKAIREP.AIERXT AS VARCHAR(50))  + '.' + CAST(AKAIREP.AIESXT AS VARCHAR(50)) AS DECIMAL(24,3))) AS 'DAPV_CV'
        FROM FFMM.INVERSIONES.DBO.tb_Ulla_As400_AKAIREP AKAIREP


       SELECT TIPO_REGISTRO
			, ORDEN
			, NRO_FORMULARIO
			, CODIGO_PRESENTACION
			, NRO_FOLIO
			, SUM(DC_UF) AS 'DC_UF'
			, SUM(COTIZACION) AS 'COTIZACION'
			, SUM(AVC) AS 'AVC'
			, SUM(AVC_TRASPASO) AS 'AVC_TRASPASO'
			, SUM(AVC_RETIRO) AS 'AVC_RETIRO'
			, SUM(AVC_RETENCION) AS 'AVC_RETENCION'
			, SUM(AHORRO_INDIREC) AS 'AHORRO_INDIREC'
			, SUM(AHORRO_DIREC) AS 'AHORRO_DIREC'
			, SUM(DAPV_INDIREC) AS 'DAPV_INDIREC'
			, SUM(DAPV_DIREC) AS 'DAPV_DIREC'
			, SUM(DAPV_CV) AS 'DAPV_CV'
		 FROM #TEMP_SALIDA
	    GROUP BY TIPO_REGISTRO,ORDEN,NRO_FORMULARIO,CODIGO_PRESENTACION,NRO_FOLIO

      DROP TABLE #TEMP_SALIDA
      SET NOCOUNT OFF
END
GO
GRANT EXECUTE ON [PKG_CERT24_DJ1899$DJ1899_Registro03] TO DB_EXECUTESP
GO