USE [CSGPI]
GO
/****** Object:  StoredProcedure [dbo].[PKG_NEMOTECNICOS$Guardar]    Script Date: 04/29/2022 16:55:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PKG_NEMOTECNICOS$Guardar]
( @Pid_Nemotecnico              NUMERIC OUTPUT,
  @Pcod_Instrumento             VARCHAR(15),
  @Pnemotecnico                 VARCHAR(50),
  @Pid_Mercado_Transaccion      NUMERIC,
  @Pid_Emisor_Especifico        NUMERIC,
  @Pid_Moneda                   NUMERIC,
  @Pid_Tipo_Estado              NUMERIC,
  @Pcod_Estado                  VARCHAR(3),
  @Pdsc_Nemotecnico             VARCHAR(120),
  @Ptasa_Emision                FLOAT,
  @Ptipo_Tasa                   VARCHAR(10),
  @Pperiodicidad                VARCHAR(10),
  @Pfecha_Vencimiento           DATETIME,
  @Pcorte_Minimo_Papel          FLOAT,
  @Pmonto_Emision               FLOAT,
  @Pliquidez                    FLOAT,
  @Pbase                        FLOAT,
  @Pcod_Pais                    VARCHAR(3),
  @Pid_Emisor_Especifico_Origen NUMERIC,
  @Pid_Moneda_Transaccion       NUMERIC,
  @Pid_Subfamilia               NUMERIC,
  @Pflg_Fungible                VARCHAR(1),
  @PFecha_Emision               DATETIME,
  @PFLG_TIPO_CUOTA_INGRESO      VARCHAR(1) = NULL,
  @PFLG_TIPO_CUOTA_EGRESO       VARCHAR(1) = NULL,
  @PID_USUARIO_INSERT           NUMERIC,
  @PID_USUARIO_UPDATE           NUMERIC,
  @PFLG_EXCLUSION_COMISION		varchar(5) = NULL
) AS
BEGIN
	
	DECLARE @vAccion varchar(10)
    SET @vAccion = 'UPDATE'
    DECLARE @ID_SUBCLASE NUMERIC(18)
	SET @ID_SUBCLASE = (case when @Pcod_Instrumento in (dbo.Pkg_Global$Gcinst_Acciones_Nac()) then 1
				             when @Pcod_Instrumento in (dbo.Pkg_Global$Gcinst_Ffmm_Rf_Nac()) then 1
				             when @Pcod_Instrumento in (dbo.Pkg_Global$Gcinst_Ffmm_RV_Nac()) then 2
				             else 5 end)
			             
       UPDATE dbo.NEMOTECNICOS
          SET COD_INSTRUMENTO = @Pcod_Instrumento, 
              NEMOTECNICO = @Pnemotecnico, 
              ID_MERCADO_TRANSACCION = @Pid_Mercado_Transaccion, 
              ID_EMISOR_ESPECIFICO = @Pid_Emisor_Especifico, 
              ID_MONEDA = @Pid_Moneda, 
              ID_TIPO_ESTADO = @Pid_Tipo_Estado, 
              COD_ESTADO = @Pcod_Estado, 
              DSC_NEMOTECNICO = @Pdsc_Nemotecnico, 
              TASA_EMISION = @Ptasa_Emision, 
              TIPO_TASA = @Ptipo_Tasa, 
              PERIODICIDAD = @Pperiodicidad, 
              FECHA_VENCIMIENTO = @Pfecha_Vencimiento, 
              CORTE_MINIMO_PAPEL = @Pcorte_Minimo_Papel, 
              MONTO_EMISION = @Pmonto_Emision, 
              DIAS_LIQUIDEZ = @Pliquidez, 
              BASE = @Pbase, 
              COD_PAIS = @Pcod_Pais, 
              ID_EMISOR_ESPECIFICO_ORIGEN = @Pid_Emisor_Especifico_Origen, 
              ID_MONEDA_TRANSACCION = @Pid_Moneda_Transaccion, 
              FECHA_EMISION = @PFecha_Emision, 
              ID_SUBFAMILIA = @Pid_Subfamilia, 
              FLG_FUNGIBLE = @Pflg_Fungible, 
	          FLG_TIPO_CUOTA_INGRESO = @PFLG_TIPO_CUOTA_INGRESO,
	          FLG_TIPO_CUOTA_EGRESO = @PFLG_TIPO_CUOTA_EGRESO,
              ID_USUARIO_UPDATE = @PID_USUARIO_UPDATE,
              FCH_UPDATE = getdate(),
	          ID_SUBCLASE = @ID_SUBCLASE--,
			  --FLG_EXCLUSION_COMISION = @FLG_EXCLUSION_COMISION
        WHERE (ID_NEMOTECNICO = @Pid_Nemotecnico)

		

        IF (@@ROWCOUNT = 0)
          BEGIN
            INSERT INTO dbo.NEMOTECNICOS
              (
                COD_INSTRUMENTO, 
                NEMOTECNICO, 
                ID_MERCADO_TRANSACCION, 
                ID_EMISOR_ESPECIFICO, 
                ID_MONEDA, 
                ID_TIPO_ESTADO, 
                COD_ESTADO, 
                DSC_NEMOTECNICO, 
                TASA_EMISION, 
                TIPO_TASA, 
                PERIODICIDAD, 
                FECHA_VENCIMIENTO, 
                CORTE_MINIMO_PAPEL, 
                MONTO_EMISION, 
                DIAS_LIQUIDEZ, 
                BASE, 
                COD_PAIS, 
                ID_EMISOR_ESPECIFICO_ORIGEN, 
                ID_MONEDA_TRANSACCION, 
                FECHA_EMISION, 
                ID_SUBFAMILIA, 
                FLG_FUNGIBLE, 
                FLG_TIPO_CUOTA_INGRESO, 
                FLG_TIPO_CUOTA_EGRESO,
                ID_USUARIO_INSERT,
                FCH_INSERT,
		        ID_SUBCLASE--,
				--FLG_EXCLUSION_COMISION
              )
              VALUES 
              (
                  @Pcod_Instrumento, 
                  @Pnemotecnico, 
                  @Pid_Mercado_Transaccion, 
                  @Pid_Emisor_Especifico, 
                  @Pid_Moneda, 
                  @Pid_Tipo_Estado, 
                  @Pcod_Estado, 
                  @Pdsc_Nemotecnico, 
                  @Ptasa_Emision, 
                  @Ptipo_Tasa, 
                  @Pperiodicidad, 
                  @Pfecha_Vencimiento, 
   		          @Pcorte_Minimo_Papel, 
                  @Pmonto_Emision, 
                  @Pliquidez, 
                  @Pbase, 
                  @Pcod_Pais, 
                  @Pid_Emisor_Especifico_Origen, 
                  @Pid_Moneda_Transaccion, 
                  @PFecha_Emision, 
                  @Pid_Subfamilia, 
                  @Pflg_Fungible, 
                  @PFLG_TIPO_CUOTA_INGRESO, 
                  @PFLG_TIPO_CUOTA_EGRESO, 
                  @PID_USUARIO_INSERT,
                  getdate(),
		          @ID_SUBCLASE--,
				  --@FLG_EXCLUSION_COMISION
              )
            SET @Pid_Nemotecnico = @@IDENTITY
            SET @vAccion = 'INSERT'
          END
	
	
	IF EXISTS (SELECT 1 FROM SUBCUOTA_INSTRUMENTOS_EXCLUSION_NEMOS where ID_NEMOTECNICO = @Pid_Nemotecnico)
	BEGIN 
			
		UPDATE	SUBCUOTA_INSTRUMENTOS_EXCLUSION_NEMOS
		SET		GENERAL = @PFLG_EXCLUSION_COMISION
		WHERE	ID_NEMOTECNICO = @Pid_Nemotecnico
	END
	ELSE
	BEGIN
		INSERT INTO SUBCUOTA_INSTRUMENTOS_EXCLUSION_NEMOS(ID_NEMOTECNICO,GENERAL)
		SELECT	@Pid_Nemotecnico, 
				@PFLG_EXCLUSION_COMISION
	END 
	

	DECLARE @vMensaje VARCHAR(4000)
	
	SELECT @vMensaje  = 'COD_INSTRUMENTO = ' + isnull(convert(varchar(10),(@Pcod_Instrumento)),'') + ',' + 
						'NEMOTECNICO = ' + isnull(convert(varchar(50),(@Pnemotecnico)),'') + ',' +
						'ID_MERCADO_TRANSACCION = ' + isnull(convert(varchar(50),(@Pid_Mercado_Transaccion)),'') + ',' +
						'ID_EMISOR_ESPECIFICO = ' + isnull(convert(varchar(50),(@Pid_Emisor_Especifico)),'') + ',' +
						'ID_MONEDA = ' + isnull(convert(varchar(50),(@Pid_Moneda)),'') + ',' +
						'ID_TIPO_ESTADO = ' + isnull(convert(varchar(50),(@Pid_Tipo_Estado)),'') + ',' +
						'COD_ESTADO = ' + isnull(convert(varchar(50),(@Pcod_Estado)),'') + ',' +
						'DSC_NEMOTECNICO = ' + isnull(convert(varchar(50),(@Pdsc_Nemotecnico)),'') + ',' +
						'TASA_EMISION = ' + isnull(convert(varchar(50),(@Ptasa_Emision)),'') + ',' +
						'TIPO_TASA = ' + isnull(convert(varchar(50),(@Ptipo_Tasa)),'') + ',' +
						'PERIODICIDAD = ' + isnull(convert(varchar(50),(@Pperiodicidad)),'') + ',' +
						'FECHA_VENCIMIENTO = ' + isnull(convert(varchar(50),(@Pfecha_Vencimiento)),'') + ',' +
						'CORTE_MINIMO_PAPEL = ' + isnull(convert(varchar(50),(@Pcorte_Minimo_Papel)),'') + ',' +
						'MONTO_EMISION = ' + isnull(convert(varchar(50),(@Pmonto_Emision)),'') + ',' +
						'DIAS_LIQUIDEZ = ' + isnull(convert(varchar(50),(@Pliquidez)),'') + ',' +
						'BASE = ' + isnull(convert(varchar(50),(@Pbase)),'') + ',' +
						'COD_PAIS = ' + isnull(convert(varchar(50),(@Pcod_Pais)),'') + ',' +
						'ID_EMISOR_ESPECIFICO_ORIGEN = ' + isnull(convert(varchar(50),(@Pid_Emisor_Especifico_Origen)),'') + ',' +
						'ID_MONEDA_TRANSACCION = ' + isnull(convert(varchar(50),(@Pid_Moneda_Transaccion)),'') + ',' +
						'ID_SUBFAMILIA = ' + isnull(convert(varchar(50),(@Pid_Subfamilia)),'') + ',' +
						'FLG_FUNGIBLE = ' + isnull(convert(varchar(50),(@Pflg_Fungible)),'') + ',' +
						'FECHA_EMISION = ' + isnull(convert(varchar(50),(@PFecha_Emision)),'') + ',' +
						'FLG_TIPO_CUOTA_INGRESO = ' + isnull(convert(varchar(50),(@PFLG_TIPO_CUOTA_INGRESO)),'') + ',' +
						'FLG_TIPO_CUOTA_EGRESO = ' + isnull(convert(varchar(50),(@PFLG_TIPO_CUOTA_EGRESO)),'') + ',' +
						'ID_USUARIO_INSERT = ' + isnull(convert(varchar(50),(@PID_USUARIO_INSERT)),'') + ',' +
						'ID_USUARIO_UPDATE = ' + isnull(convert(varchar(50),(@PID_USUARIO_UPDATE)),'') +
						'FLG_EXCLUSION_COMISION = ' + isnull(convert(varchar(50),(@PFLG_EXCLUSION_COMISION)),'')

	INSERT INTO Nemoslog (
		Nemotecnico,
		IdUsuarioInsert,
		FechaInsert,
		IdUsuarioMod,
		CodEstado,
		Accion,
		Cadena
	)
	SELECT	@Pnemotecnico,
			@PID_USUARIO_INSERT,
			getdate(),
			@PID_USUARIO_UPDATE,
			'VIG',
			@vAccion,
			@vMensaje

END
