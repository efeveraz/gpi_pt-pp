USE [CSGPI]
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_NEMOTECNICOS$Buscarview]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE PKG_NEMOTECNICOS$Buscarview
GO
  
CREATE PROCEDURE PKG_NEMOTECNICOS$Buscarview  
( @Pcod_Instrumento varchar(15) =  NULL  
, @Pid_Nemotecnico float(53) =  NULL  output  
, @Pcod_Producto varchar(10) =  NULL  
, @pnemotecnico  varchar(100) = null  
) AS  
BEGIN  
        SELECT VN.ID_NEMOTECNICO     ,     NEMOTECNICO           ,     ID_MERCADO_TRANSACCION ,     ID_EMISOR_ESPECIFICO        ,  
             ID_MONEDA          ,     ID_TIPO_ESTADO        ,     COD_ESTADO             ,     DSC_NEMOTECNICO             ,  
             TASA_EMISION       ,     TIPO_TASA             ,     PERIODICIDAD           ,     FECHA_VENCIMIENTO           ,  
             CORTE_MINIMO_PAPEL ,     MONTO_EMISION         ,     DIAS_LIQUIDEZ          ,     BASE                        ,  
             FECHA_EMISION      ,     COD_INSTRUMENTO       ,     DSC_INTRUMENTO         ,     COD_PRODUCTO                ,  
             DSC_PRODUCTO       ,     DSC_EMISOR_ESPECIFICO ,     COD_EMISOR_ESPECIFICO  ,     COD_MERCADO                 ,  
             DESC_MERCADO       ,     COD_PAIS              ,     DSC_PAIS               ,     ID_EMISOR_ESPECIFICO_ORIGEN ,  
             COD_MONEDA         ,     DSC_MONEDA            ,     ID_MONEDA_TRANSACCION  ,     DSC_MONEDA_TRANSACCION      ,  
             ID_SUBFAMILIA      ,     FLG_FUNGIBLE          ,     FLG_TIPO_CUOTA_INGRESO ,     FLG_TIPO_CUOTA_EGRESO       ,  
             COD_SUBFAMILIA     ,     DSC_SUBFAMILIA		,	  
             ISNULL(GENERAL,'N')				'FLG_EXCLUSION_COMISION'
        FROM VIEW_NEMOTECNICOS  VN WITH (NOLOCK)
			LEFT JOIN SUBCUOTA_INSTRUMENTOS_EXCLUSION_NEMOS SIEN WITH (NOLOCK)
				on VN.ID_NEMOTECNICO = SIEN.ID_NEMOTECNICO
       WHERE COD_INSTRUMENTO = ISNULL(@PCOD_INSTRUMENTO,COD_INSTRUMENTO)  
         AND VN.ID_NEMOTECNICO  = ISNULL(@PID_NEMOTECNICO, VN.ID_NEMOTECNICO)  
         AND COD_PRODUCTO    = ISNULL(@PCOD_PRODUCTO, COD_PRODUCTO)  
         AND NEMOTECNICO     = ISNULL(@PNEMOTECNICO,NEMOTECNICO)  
      ORDER BY COD_INSTRUMENTO, NEMOTECNICO;   
END
 
GO
GRANT EXECUTE ON PKG_NEMOTECNICOS$Buscarview TO DB_EXECUTESP
GO