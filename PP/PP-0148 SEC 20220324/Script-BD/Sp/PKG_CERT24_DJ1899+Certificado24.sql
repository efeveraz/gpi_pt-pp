IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_CERT24_DJ1899$Certificado24]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
DROP PROCEDURE [DBO].[PKG_CERT24_DJ1899$Certificado24]
GO

CREATE PROCEDURE [dbo].[PKG_CERT24_DJ1899$Certificado24]
 ( @pId_Empresa     NUMERIC
 , @pId_Cliente     NUMERIC = NULL
 , @pAnno           NUMERIC)
AS
BEGIN

  SET NOCOUNT ON

  DECLARE @LID_TIPOCUENTA          NUMERIC

  SELECT @LID_TIPOCUENTA = ID_TIPOCUENTA FROM TIPO_CUENTAS WHERE DESCRIPCION_CORTA = 'APV'

  DECLARE @SALIDA TABLE (ID_CLIENTE        NUMERIC(10)
                       , NOMBRE_CLIENTE    VARCHAR(150)
                       , RUT_CLIENTE       VARCHAR(10)
                       , ACTIVO_PENSIONADO CHAR(1)
                       , SECCION           VARCHAR(20)
                       , MES               NUMERIC
                       , MONTO             FLOAT)

  DECLARE @SALIDA_RETIRO TABLE (ID_CUENTA              NUMERIC(10)
                              , MES                    NUMERIC
                              , MONTO_RETIRO           NUMERIC(18,4)
                              , PROMEDIO               FLOAT
                              , MONTO_RETIRO_CALC      NUMERIC(18,4)
                              , MONTO_RETENCION_CALC   NUMERIC(18,4)
                              , MONTO_RETIRO_INC2      NUMERIC(18,4)
                              , MONTO_RETIRO_INC2_CALC NUMERIC(18,4))
                         
  DECLARE @APORTES_DEPENDIENTE TABLE (MES        NUMERIC
                                    , MONTO      FLOAT)


  --- aportes dependientes
    INSERT INTO @SALIDA
         SELECT CU.ID_CLIENTE,
                CU.NOMBRE_CLIENTE,
                CU.RUT_CLIENTE,
                CL.ACTIVO_PENSIONADO,
                'APORTE_DEP',
                MONTH(AR.FECHA_MOVIMIENTO),
                (ROUND(ISNULL(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO),0)
                / (SELECT dbo.FNT_VALOR_TIPO_CAMBIO(DBO.FNT_DAMEIDMONEDA('UF'), AR.FECHA_MOVIMIENTO)), 3, 0)
                * (SELECT VALOR FROM VALOR_TIPO_CAMBIO WHERE ID_TIPO_CAMBIO = (SELECT ID_TIPO_CAMBIO
                                                                                 FROM TIPO_CAMBIOS
                                                                                WHERE ID_MONEDA_DESTINO = DBO.FNT_DAMEIDMONEDA('UF'))
                                                                                  AND DAY(FECHA) = 31 AND MONTH(FECHA)=12 AND YEAR(FECHA) = (@pAnno - 1)))
           FROM APORTE_RESCATE_CUENTA  AR
              , TIPOS_AHORRO_APV TA
              , VIEW_CUENTAS_VIGENTES CU
              , CLIENTES CL
          WHERE CU.ID_EMPRESA             = @pId_Empresa
            AND CU.ID_TIPOCUENTA          = @LID_TIPOCUENTA
            AND CU.ID_CLIENTE             = ISNULL(@pId_Cliente,CU.ID_CLIENTE)
            AND CL.ID_CLIENTE             = CU.ID_CLIENTE
            AND (CL.TIPO_TRABAJADOR       = 'D' OR CL.TIPO_TRABAJADOR IS NULL)
            AND AR.ID_CUENTA              = CU.ID_CUENTA
            AND YEAR(AR.FECHA_MOVIMIENTO) = @pAnno - 1
            AND AR.ID_TIPO_AHORRO         IN (SELECT ID_TIPO_AHORRO FROM TIPOS_AHORRO_APV
                                               WHERE (DESCRIPCION_LARGA LIKE 'APV%' OR DESCRIPCION_LARGA LIKE 'COTIZA%') AND IND_ACOGIDO_42BIS = 'S')
            AND AR.COD_ESTADO             <> dbo.Pkg_Global$cCod_Estado_Anulado()
            AND AR.FLG_TIPO_MOVIMIENTO    = 'A'
            AND TA.ID_TIPO_AHORRO         = AR.ID_TIPO_AHORRO


      --- APORTES INDEPENDIENTES
    INSERT INTO @SALIDA
         SELECT CU.ID_CLIENTE,
                CU.NOMBRE_CLIENTE,
                CU.RUT_CLIENTE,
                CL.ACTIVO_PENSIONADO,
                'APORTE_IND' ,
                MONTH(AR.FECHA_MOVIMIENTO),
                (ROUND(ISNULL(DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                                     AR.MONTO,
                                                                     ID_MONEDA,
                                                                     DBO.FNT_DAMEIDMONEDA('$$'),
                                                                     AR.FECHA_MOVIMIENTO),0)
                / (SELECT dbo.FNT_VALOR_TIPO_CAMBIO(DBO.FNT_DAMEIDMONEDA('UF'), AR.FECHA_MOVIMIENTO)), 3, 0)
                * (SELECT VALOR FROM VALOR_TIPO_CAMBIO WHERE ID_TIPO_CAMBIO = (SELECT ID_TIPO_CAMBIO
                                                                                 FROM TIPO_CAMBIOS
                                                                                WHERE ID_MONEDA_DESTINO = DBO.FNT_DAMEIDMONEDA('UF'))
                                                                                  AND DAY(FECHA) = 31 AND MONTH(FECHA)=12 AND YEAR(FECHA) = (@pAnno - 1)))
           FROM APORTE_RESCATE_CUENTA  AR
              , TIPOS_AHORRO_APV TA
              , VIEW_CUENTAS_VIGENTES CU
              , CLIENTES CL
          WHERE CU.ID_EMPRESA             = @pId_Empresa
            AND CU.ID_TIPOCUENTA          = @LID_TIPOCUENTA
            AND CU.ID_CLIENTE             = ISNULL(@pId_Cliente,CU.ID_CLIENTE)
            AND CL.ID_CLIENTE             = CU.ID_CLIENTE
            AND CL.TIPO_TRABAJADOR        = 'I'
            AND AR.ID_CUENTA              = CU.ID_CUENTA
            AND YEAR(AR.FECHA_MOVIMIENTO) = @pAnno - 1
            AND AR.ID_TIPO_AHORRO         IN (SELECT ID_TIPO_AHORRO FROM TIPOS_AHORRO_APV
                                               WHERE (DESCRIPCION_LARGA LIKE 'APV%' OR DESCRIPCION_LARGA LIKE 'COTIZA%') AND IND_ACOGIDO_42BIS = 'S')
            AND AR.COD_ESTADO             <> dbo.Pkg_Global$cCod_Estado_Anulado()
            AND AR.FLG_TIPO_MOVIMIENTO    = 'A'
            AND TA.ID_TIPO_AHORRO         = AR.ID_TIPO_AHORRO


          /****** RETIROS  ******/
    INSERT INTO @SALIDA_RETIRO
         SELECT CU.ID_CUENTA
              , MONTH(AR.FECHA_MOVIMIENTO)
              , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                       AR.MONTO,
                                                       ID_MONEDA,
                                                       DBO.FNT_DAMEIDMONEDA('$$'),
                                                       AR.FECHA_MOVIMIENTO)
              , ROUND(((SELECT VARIACION_MENSUAL FROM IPC 
			             WHERE ANNO = YEAR(AR.FECHA_MOVIMIENTO) AND MES = 11)
                /      (SELECT VARIACION_MENSUAL FROM IPC 
				         WHERE ANNO = YEAR(DATEADD(MONTH, -1, AR.FECHA_MOVIMIENTO)) AND MES = MONTH(DATEADD(MONTH, -1, AR.FECHA_MOVIMIENTO)))), 3)
              , 0
              , 0
              , 0
              , 0                            
           FROM APORTE_RESCATE_CUENTA  AR
              , TIPOS_AHORRO_APV TA
              , VIEW_CUENTAS_VIGENTES CU
              , CLIENTES CL
          WHERE CU.ID_EMPRESA             = @pId_Empresa
            AND CU.ID_TIPOCUENTA          = @LID_TIPOCUENTA
            AND CU.ID_CLIENTE             = ISNULL(@pId_Cliente,CU.ID_CLIENTE)
            AND CL.ID_CLIENTE             = CU.ID_CLIENTE
            AND AR.ID_CUENTA              = CU.ID_CUENTA
            AND YEAR(AR.FECHA_MOVIMIENTO) = @pAnno - 1
            AND AR.ID_TIPO_AHORRO         IN (SELECT ID_TIPO_AHORRO FROM TIPOS_AHORRO_APV
                                               WHERE ((DESCRIPCION_LARGA LIKE 'APV%' OR DESCRIPCION_LARGA LIKE 'COTIZA%') AND IND_ACOGIDO_42BIS = 'S'))
            AND AR.COD_ESTADO             <> dbo.Pkg_Global$cCod_Estado_Anulado()
            AND AR.FLG_TIPO_MOVIMIENTO    = 'R'
            AND TA.ID_TIPO_AHORRO         = AR.ID_TIPO_AHORRO

         UPDATE @SALIDA_RETIRO SET MONTO_RETIRO_CALC    = (CASE WHEN (PROMEDIO > 1) THEN PROMEDIO * MONTO_RETIRO ELSE MONTO_RETIRO END)
                                 , MONTO_RETENCION_CALC = (CASE WHEN (PROMEDIO <= 1) THEN (MONTO_RETIRO * 0.15) ELSE (PROMEDIO * MONTO_RETIRO * 0.15) END)

    INSERT INTO @SALIDA_RETIRO
         SELECT CU.ID_CUENTA
              , MONTH(AR.FECHA_MOVIMIENTO)
              , 0
              , ROUND(((SELECT VARIACION_MENSUAL FROM IPC 
			             WHERE ANNO = YEAR(AR.FECHA_MOVIMIENTO) AND MES = 11)
                /      (SELECT VARIACION_MENSUAL FROM IPC 
				         WHERE ANNO = YEAR(DATEADD(MONTH, -1, AR.FECHA_MOVIMIENTO)) AND MES = MONTH(DATEADD(MONTH, -1, AR.FECHA_MOVIMIENTO)))), 3)
              , 0
              , 0
              , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CU.ID_CUENTA,
                                                       AR.MONTO,
                                                       ID_MONEDA,
                                                       DBO.FNT_DAMEIDMONEDA('$$'),
                                                       AR.FECHA_MOVIMIENTO)
              , 0                            
           FROM APORTE_RESCATE_CUENTA  AR
              , TIPOS_AHORRO_APV TA
              , VIEW_CUENTAS_VIGENTES CU
              , CLIENTES CL
          WHERE CU.ID_EMPRESA             = @pId_Empresa
            AND CU.ID_TIPOCUENTA          = @LID_TIPOCUENTA
            AND CU.ID_CLIENTE             = ISNULL(@pId_Cliente,CU.ID_CLIENTE)
            AND CL.ID_CLIENTE             = CU.ID_CLIENTE
            AND AR.ID_CUENTA              = CU.ID_CUENTA
            AND YEAR(AR.FECHA_MOVIMIENTO) = @pAnno - 1
            AND AR.ID_TIPO_AHORRO         IN (SELECT ID_TIPO_AHORRO FROM TIPOS_AHORRO_APV WHERE DESCRIPCION_CORTA IN ('APV-A'))
            AND AR.COD_ESTADO             <> dbo.Pkg_Global$cCod_Estado_Anulado()
            AND AR.FLG_TIPO_MOVIMIENTO    = 'R'
            AND TA.ID_TIPO_AHORRO         = AR.ID_TIPO_AHORRO

         UPDATE @SALIDA_RETIRO SET MONTO_RETIRO_INC2_CALC = (CASE WHEN (PROMEDIO > 1) THEN PROMEDIO * MONTO_RETIRO_INC2 ELSE MONTO_RETIRO_INC2 END)

    INSERT INTO @SALIDA
         SELECT CU.ID_CLIENTE,
                CU.NOMBRE_CLIENTE,
                CU.RUT_CLIENTE,
                CL.ACTIVO_PENSIONADO,
                'RETIROS',
                S.MES ,
                MONTO_RETIRO_CALC
           FROM @SALIDA_RETIRO S
              , CLIENTES CL
              , VIEW_CUENTAS_VIGENTES CU
          WHERE CU.ID_CUENTA   = S.ID_CUENTA
            AND CL.ID_CLIENTE  = CU.ID_CLIENTE

    INSERT INTO @SALIDA
         SELECT CU.ID_CLIENTE,
                CU.NOMBRE_CLIENTE,
                CU.RUT_CLIENTE,
                CL.ACTIVO_PENSIONADO,
                'RETENCION',
                S.MES   ,
                MONTO_RETENCION_CALC
           FROM @SALIDA_RETIRO S
              , CLIENTES CL
              , VIEW_CUENTAS_VIGENTES CU
          WHERE CU.ID_CUENTA   = S.ID_CUENTA
            AND CL.ID_CLIENTE  = CU.ID_CLIENTE

    INSERT INTO @SALIDA
         SELECT CU.ID_CLIENTE,
                CU.NOMBRE_CLIENTE,
                CU.RUT_CLIENTE,
                CL.ACTIVO_PENSIONADO,
                'RETIRO_INCISO2',
                S.MES   ,
                MONTO_RETIRO_INC2_CALC
           FROM @SALIDA_RETIRO S
              , CLIENTES CL
              , VIEW_CUENTAS_VIGENTES CU
          WHERE CU.ID_CUENTA   = S.ID_CUENTA
            AND CL.ID_CLIENTE  = CU.ID_CLIENTE
            
         SELECT SALIDA_ALL.ID_CLIENTE
			  , SALIDA_ALL.NOMBRE_CLIENTE
			  , SALIDA_ALL.RUT_CLIENTE
			  , SALIDA_ALL.ACTIVO_PENSIONADO
			  , SALIDA_ALL.SECCION
			  , ISNULL(SUM(SALIDA_ALL.MONTO),0) AS 'MONTO'
              , SALIDA_ALL.MES
			  , NULL AS NRO_FOLIO
	       FROM (
		         SELECT ID_CLIENTE
                      , NOMBRE_CLIENTE
                      , RUT_CLIENTE
                      , ACTIVO_PENSIONADO
                      , SECCION
                      , ISNULL(SUM(MONTO),0) AS 'MONTO'
                      , MES
                      , NULL AS NRO_FOLIO
			       FROM @SALIDA
			      GROUP BY ID_CLIENTE, NOMBRE_CLIENTE, RUT_CLIENTE, ACTIVO_PENSIONADO, SECCION, MES
/*
				 UNION ALL
				SELECT CONVERT(INT,SUBSTRING(LTRIM(RTRIM(( REPLACE(REPLACE(AKA5CPP.A5T3CZ,'.',''),'-','')))),1,LEN(LTRIM(RTRIM((REPLACE(REPLACE(AKA5CPP.A5T3CZ,'.',''),'-','')))))-1)) ID_CLIENTE
					 , AKA4CPP.A4HNXT NOMBRE_CLIENTE
					 , LTRIM(RTRIM((REPLACE(AKA5CPP.A5T3CZ,'.','')))) RUT_CLIENTE
					 , 'A' ACTIVO_PENSIONADO
					 , 'RETIROS' SECCION
					 , ISNULL((SUM(AKA5CPP.A5JUAA) + SUM(AKA5CPP.A5JVAA)),0) AS 'MONTO'
                     , AKA5CPP.A5UONU                     'MES'
					 , NULL AS NRO_FOLIO
				  FROM FFMM.INVERSIONES.DBO.TB_ULLA_AS400_AKA4CPP AKA4CPP,
					   FFMM.INVERSIONES.DBO.TB_Ulla_As400_AKA5CPP AKA5CPP
				 WHERE AKA4CPP.a4t2cz = AKA5CPP.a5t2cz
				   AND AKA4CPP.A4UMNU = AKA5CPP.A5UMNU
				   AND AKA4CPP.A4T3CZ = AKA5CPP.A5T3CZ
				   AND AKA4CPP.A4UMNU = @PANNO - 1
				   AND AKA5CPP.A5UONU <> 13
				   AND (AKA5CPP.A5JUAA <> 0 OR AKA5CPP.A5JVAA <> 0)
				 GROUP BY AKA5CPP.A5T3CZ,AKA5CPP.A5UMNU,AKA5CPP.A5UONU,AKA4CPP.A4HNXT,AKA5CPP.A5SACD
				 UNION ALL
				SELECT CONVERT(INT,SUBSTRING(LTRIM(RTRIM(( REPLACE(REPLACE(AKA5CPP.A5T3CZ,'.',''),'-','')))),1,LEN(LTRIM(RTRIM((REPLACE(REPLACE(AKA5CPP.A5T3CZ,'.',''),'-','')))))-1)) ID_CLIENTE
					 , AKA4CPP.A4HNXT NOMBRE_CLIENTE
					 , LTRIM(RTRIM((REPLACE(AKA5CPP.A5T3CZ,'.','')))) RUT_CLIENTE
					 , 'A' ACTIVO_PENSIONADO
					 , 'RETENCION' SECCION
					 , ISNULL((SUM(AKA5CPP.A5KAAA)),0) AS 'MONTO'
                     , AKA5CPP.A5UONU                     'MES'
					 , NULL AS NRO_FOLIO
				  FROM FFMM.INVERSIONES.DBO.TB_ULLA_AS400_AKA4CPP AKA4CPP,
					   FFMM.INVERSIONES.DBO.TB_Ulla_As400_AKA5CPP AKA5CPP
				 WHERE AKA4CPP.a4t2cz = AKA5CPP.a5t2cz
				   AND AKA4CPP.A4UMNU = AKA5CPP.A5UMNU
				   AND AKA4CPP.A4T3CZ = AKA5CPP.A5T3CZ
				   AND AKA4CPP.A4UMNU = @PANNO - 1
				   AND AKA5CPP.A5UONU <> 13
				   AND AKA5CPP.A5KAAA <> 0
				 GROUP BY AKA5CPP.A5T3CZ,AKA5CPP.A5UMNU,AKA5CPP.A5UONU, AKA4CPP.A4HNXT,AKA5CPP.A5SACD
				 UNION ALL
				SELECT convert(int,SUBSTRING(LTRIM(RTRIM(( REPLACE(REPLACE(AKA5CPP.A5T3CZ,'.',''),'-','')))),1,LEN(LTRIM(RTRIM((REPLACE(REPLACE(AKA5CPP.A5T3CZ,'.',''),'-','')))))-1)) ID_CLIENTE
				     , AKA4CPP.A4HNXT NOMBRE_CLIENTE
					 , LTRIM(RTRIM((REPLACE(AKA5CPP.A5T3CZ,'.','')))) RUT_CLIENTE
					 , 'A' ACTIVO_PENSIONADO
					 , 'APORTE_DEP' SECCION
					 , isnull((sum(AKA5CPP.A5JRAA) + sum(AKA5CPP.A5JQAA)),0) as 'Monto'
                     , AKA5CPP.A5UONU                                           'Mes'
					 , null as NRO_FOLIO
				 FROM FFMM.INVERSIONES.DBO.TB_ULLA_AS400_AKA4CPP AKA4CPP,
					  FFMM.INVERSIONES.DBO.TB_Ulla_As400_AKA5CPP AKA5CPP
				WHERE AKA4CPP.a4t2cz = AKA5CPP.a5t2cz
				  AND AKA4CPP.A4UMNU = AKA5CPP.A5UMNU
				  AND AKA4CPP.A4T3CZ = AKA5CPP.A5T3CZ
				  AND AKA4CPP.A4UMNU = @PANNO - 1
				  AND AKA5CPP.A5UONU <> 13
				  AND (AKA5CPP.A5JRAA <> 0 OR AKA5CPP.A5JQAA <> 0)
				GROUP BY AKA4CPP.A4HNXT,AKA5CPP.A5T3CZ,AKA5CPP.A5SACD,AKA5CPP.A5UONU
				UNION ALL
			   SELECT CONVERT(INT,SUBSTRING(LTRIM(RTRIM(( REPLACE(REPLACE(AKA5CPP.A5T3CZ,'.',''),'-','')))),1,LEN(LTRIM(RTRIM((REPLACE(REPLACE(AKA5CPP.A5T3CZ,'.',''),'-','')))))-1)) ID_CLIENTE
					, AKA4CPP.A4HNXT NOMBRE_CLIENTE
					, LTRIM(RTRIM((REPLACE(AKA5CPP.A5T3CZ,'.','')))) RUT_CLIENTE
					, 'A' ACTIVO_PENSIONADO
					, 'APORTE_IND' SECCION
					, ISNULL(SUM(AKA5CPP.A5JTAA),0) AS 'MONTO'
                    , AKA5CPP.A5UONU                   'MES'
					, NULL AS NRO_FOLIO
				 FROM FFMM.INVERSIONES.DBO.TB_ULLA_AS400_AKA4CPP AKA4CPP,
					  FFMM.INVERSIONES.DBO.TB_ULLA_AS400_AKA5CPP AKA5CPP
				WHERE AKA4CPP.A4T2CZ = AKA5CPP.A5T2CZ
				AND AKA4CPP.A4UMNU = AKA5CPP.A5UMNU
				AND AKA4CPP.A4T3CZ = AKA5CPP.A5T3CZ
				AND AKA4CPP.A4UMNU = @PANNO - 1
				AND AKA5CPP.A5UONU <> 13
				AND ISNULL(AKA5CPP.A5JTAA,0) <> 0
			  GROUP BY AKA5CPP.A5T3CZ,AKA5CPP.A5UMNU,AKA5CPP.A5UONU, AKA4CPP.A4HNXT,AKA5CPP.A5SACD,AKA5CPP.A5UONU
*/			  

		        ) SALIDA_ALL		      
          GROUP BY SALIDA_ALL.ID_CLIENTE,SALIDA_ALL.NOMBRE_CLIENTE,SALIDA_ALL.RUT_CLIENTE,SALIDA_ALL.ACTIVO_PENSIONADO,SALIDA_ALL.SECCION,SALIDA_ALL.MES
	      ORDER BY SALIDA_ALL.ID_CLIENTE,SALIDA_ALL.MES,SALIDA_ALL.SECCION ASC

   SET NOCOUNT OFF

END
GO

GRANT EXECUTE ON [PKG_CERT24_DJ1899$Certificado24] TO DB_EXECUTESP
GO