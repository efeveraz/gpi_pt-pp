IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_INFORME_CERTIFICADO24$BUSCAR_CLIENTES]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_INFORME_CERTIFICADO24$BUSCAR_CLIENTES]
GO

CREATE PROCEDURE [dbo].[PKG_INFORME_CERTIFICADO24$BUSCAR_CLIENTES]
@PANNO_TRIBUTARIO       NUMERIC(4) = NULL
AS
BEGIN

   SET NOCOUNT ON

   SELECT CE.ID_CLIENTE, CE.ANNO_TRIBUTARIO, CL.RUT_CLIENTE, CL.NOMBRE_CLIENTE, MAX(NRO_FOLIO) AS CERTIFICADO,
          (SELECT TOP 1 DIR.DIRECCION FROM DIRECCIONES_CLIENTES DIR WHERE CE.ID_CLIENTE = DIR.ID_CLIENTE) AS DIRECCION,
          CL.ACTIVO_PENSIONADO
     FROM CERTIFICADO24 CE,
          VIEW_CLIENTES CL
    WHERE CE.ID_CLIENTE = CL.ID_CLIENTE
          AND (CE.ANNO_TRIBUTARIO = ISNULL(@PANNO_TRIBUTARIO, CE.ANNO_TRIBUTARIO))
    GROUP BY CE.ID_CLIENTE, CE.ANNO_TRIBUTARIO, CL.RUT_CLIENTE, CL.NOMBRE_CLIENTE, CL.ACTIVO_PENSIONADO
   -- UNION ALL
   --SELECT CE.ID_CLIENTE, CE.ANNO_TRIBUTARIO, AKA4CPP.A4T3CZ AS RUT_CLIENTE , AKA4CPP.A4HNXT AS NOMBRE_CLIENTE, MAX(NRO_FOLIO) AS CERTIFICADO, AKA4CPP.A4HOXT AS DIRECCION, 'A' AS ACTIVO_PENSIONADO
   --  FROM CERTIFICADO24 CE,
   --       FFMM.INVERSIONES.DBO.TB_ULLA_AS400_AKA4CPP AKA4CPP
   -- WHERE CE.ID_CLIENTE = convert(int,SUBSTRING(LTRIM(RTRIM(( REPLACE(REPLACE(AKA4CPP.A4T3CZ,'.',''),'-','')))),1,LEN(LTRIM(RTRIM((REPLACE(REPLACE(AKA4CPP.A4T3CZ,'.',''),'-','')))))-1))
   --   AND (CE.ANNO_TRIBUTARIO = ISNULL(@PANNO_TRIBUTARIO, CE.ANNO_TRIBUTARIO))
   -- GROUP BY CE.ID_CLIENTE, CE.ANNO_TRIBUTARIO, AKA4CPP.A4T3CZ, AKA4CPP.A4HNXT,AKA4CPP.A4HOXT

   SET NOCOUNT OFF

END
GO

GRANT EXECUTE ON [PKG_INFORME_CERTIFICADO24$BUSCAR_CLIENTES] TO DB_EXECUTESP
GO
