If ((Select Count(*) From SysObjects A Inner Join SysColumns B on A.Id = B.Id
     Where (A.Name = 'CERTIFICADO24') And (B.Name = 'RETIROS_INCISO_2')) = 0)
Begin
	ALTER TABLE CERTIFICADO24
    ADD RETIROS_INCISO_2 NUMERIC(18,4)
End

