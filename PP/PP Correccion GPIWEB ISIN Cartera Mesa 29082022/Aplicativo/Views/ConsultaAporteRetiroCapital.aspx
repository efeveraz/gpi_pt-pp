﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="ConsultaAporteRetiroCapital.aspx.vb" Inherits="AplicacionWeb.ConsultaAporteRetiroCapital" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaAporteRetiroCapital.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <select id="DDMoneda" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Moneda de Salida">
                    </select>
                </div>
                <div class="form-group">
                    <div id="dtRangoFecha" class="input-daterange input-group">
                        <input id="dtFechaConsulta" type="text" class="form-control" />
                        <span class="input-group-addon">a</span>
                        <input id="dtFechaConsultaHasta" type="text" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tabs-1" data-toggle="tab">Detalle</a></li>
                <li><a href="#tabs-2" data-toggle="tab">Resumen</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tabs-1">
                    <div id="DivGrillaAseProd" style="height: auto" class="DivCentradosGrillaCompleta">
                        <table id="PatrimonioYActivosDetallado">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                        <div id="Paginador">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tabs-2">
                    <div id="DivGrillaDetalle" style="height: auto;" class="DivCentradosGrillaCompleta">
                        <table id="PatrimonioYActivosResumen">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                        <div id="PaginadorResumen">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
