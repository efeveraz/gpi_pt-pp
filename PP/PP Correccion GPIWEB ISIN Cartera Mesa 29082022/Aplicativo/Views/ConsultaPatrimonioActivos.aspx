﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="ConsultaPatrimonioActivos.aspx.vb" Inherits="AplicacionWeb.ConsultaPatrimonioActivos" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaPatrimonioActivos.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <select id="DDAsesor" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Asesor" style="min-width: 145px;">
                    </select>
                </div>
                <div class="form-group">
                    <select id="DDMoneda" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Moneda de Salida">
                    </select>
                </div>
                <div class="form-group">
                    <div class="input-group date">
                        <input type="text" id="dtFechaConsulta" class="form-control" style="text-align: center;">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-sm-12">
            <ul id="tabLista" class="nav nav-tabs">
                <li class="active"><a id="aDetalle" href="#tabs-1" data-toggle="tab">Detalle</a></li>
                <li><a id="aResumen" href="#tabs-2" data-toggle="tab">Resumen</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tabs-1">
                    <div id="DivGrillaAseProd" style="height: auto" class="DivCentradosGrillaCompleta">
                        <table id="PatrimonioYActivosDetallado">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                        <div id="PaginadorDetalle">
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tabs-2">
                    <div id="DivGrillaDetalle" style="height: auto;" class="DivCentradosGrillaCompleta">
                        <table id="PatrimonioYActivosResumen">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                        <div id="PaginadorResumen">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
