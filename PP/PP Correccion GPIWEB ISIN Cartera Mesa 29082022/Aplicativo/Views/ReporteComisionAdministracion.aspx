﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="ReporteComisionAdministracion.aspx.vb" Inherits="AplicacionWeb.ReporteComisionAdministracion" %>

<%@ Import Namespace="AplicacionWeb" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContenidoPPal" runat="server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/reporteComisionAdministracion.min.js") %>'></script>
    <link href='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.css") %>' rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.js") %>'></script>

    <div class="row top-buffer">
        <div class="col-xs-12">
            <ul id="idNavTabs" class="nav nav-tabs">
                <li class="active"><a href="#tabs-1" data-toggle="tab">Devengadas</a></li>
                <li><a href="#tabs-2" data-toggle="tab">Liquidadas</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade  in active" id="tabs-1">
                    <div id="encSubPantalla" style="height: auto" class="DivCentradosGrillaCompleta">
                        <div class="divConsultarSeccion row top-buffer">
                            <div class="form-inline col-xs-12">
                                <div class="form-group">
                                    <div id="dtRangoFecha" class="input-daterange input-group">
                                        <input id="dtFechaConsultaDesde" type="text" class="form-control" />
                                        <span class="input-group-addon">a</span>
                                        <input id="dtFechaConsultaHasta" type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="button" id="btnGenerarDev" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Seleccione Cuenta">Generar Reporte</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tabs-2">
                    <div id="encSubPantallaD" style="height: auto" class="DivCentradosGrillaCompleta">
                        <div class="divConsultarSeccion row top-buffer">
                            <div class="form-inline col-xs-12">
                                <div class="form-group">
                                    <div class="input-group date">
                                        <input type="text" id="dtFechaConsulta" class="form-control" style="text-align: center;">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="button" id="btnGenerarLiq" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Seleccione Cuenta">Generar Reporte</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div id="contenidoPorCuenta" class='table-responsive'>
                <table class='table table-striped table-hover table-bordered table-condensed display nowrap' id='ListaCuentasPorCuenta' width="100%">
                    <thead style="display: table-header-group">
                        <tr>
                            <th>
                                <div class="checkbox">
                                    <input id="chkAllCta" value="1" type="checkbox">
                                    <label for="chkAllCta"></label>
                                </div>
                            </th>
                            <th>Empresa</th>
                            <th>Cuenta</th>
                            <th>Nombre Cuenta</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div id="contenidoPorCliente" class='table-responsive' hidden>
                <table class='table table-striped table-hover table-bordered table-condensed display nowrap' id='ListaCuentasPorCliente' width="100%">
                    <thead style="display: table-header-group">
                        <tr>
                            <th>
                                <div class="checkbox">
                                    <input id="chkAllCtaCli" value="1" type="checkbox">
                                    <label for="chkAllCtaCli"></label>
                                </div>
                            </th>
                            <th>Empresa</th>
                            <th>Cuenta</th>
                            <th>Nombre Cuenta</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div id="contenidoPorGrupo" class='table-responsive' hidden>
                <table class='table table-striped table-hover table-bordered table-condensed display nowrap' id='ListaCuentasPorGrupo' width="100%">
                    <thead style="display: table-header-group">
                        <tr>
                            <th>
                                <div class="checkbox">
                                    <input id="chkAllCtaGru" value="1" type="checkbox">
                                    <label for="chkAllCtaGru"></label>
                                </div>
                            </th>
                            <th>Empresa</th>
                            <th>Cuenta</th>
                            <th>Nombre Cuenta</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
