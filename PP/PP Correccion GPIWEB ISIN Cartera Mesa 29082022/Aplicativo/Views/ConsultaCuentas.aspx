﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb.master" AutoEventWireup="false"
    CodeBehind="ConsultaCuentas.aspx.vb" Inherits="AplicacionWeb.ConsultaCuentas" %>

<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPalFiltro" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaCuentas.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <div id="dtRangoFecha" class="input-daterange input-group">
                        <input id="dtFechaConsulta" type="text" class="form-control" />
                        <span class="input-group-addon">a</span>
                        <input id="dtFechaConsultaHasta" type="text" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-sm-3 col-lg-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group form-group-sm">
                        <label for="DDCliente">Cliente</label>
                        <select id="DDCliente" class="form-control input-sm">
                        </select>
                    </div>
                    <div class="form-group-sm">
                        <label for="DDEstado">Estado</label>
                        <select id="DDEstado" class="form-control input-sm">
                            <option value="-1">Todos</option>
                            <option value="D">Deshabilitado</option>
                            <option value="H">Habilitada</option>
                        </select>
                    </div>
                    <div class="checkbox">
                        <input id="CBCreadas" type="checkbox" checked>
                        <label for="CBCreadas"><strong>Creadas</strong></label>
                    </div>
                    <div class="checkbox">
                        <input id="CBCerradas" type="checkbox" checked>
                        <label for="CBCerradas"><strong>Cerradas</strong></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9 col-lg-10">
            <table id="ListaCuentas">
                <tr>
                    <td></td>
                </tr>
            </table>
            <div id="Paginador">
            </div>
        </div>
    </div>
</asp:Content>
