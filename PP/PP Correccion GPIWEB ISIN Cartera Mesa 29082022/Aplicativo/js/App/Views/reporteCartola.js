﻿var empresa = localStorage.DscEmpresa;
var target;
var vFechaTopeConsulta;

function initReporteCartola() {
    $("#idSubtituloPaginaText").text("Cartola");
    document.title = "Cartola";

    if (empresa == 'Valores APV') {
        $("#tabsClienteCuentaGrupo").hide();
        $('.nav-tabs a[href="#tabCliente"]').tab('show');
        $("#DDTipoCartola option[value=1]").hide();
        $("#DDTipoCartola").val('2');
        $('#BtnConsultar').attr('data-original-title', 'Seleccione Cliente');
        target = "#tabCliente";
    } else {
        $("#tabsClienteCuentaGrupo").hide();
        $('.nav-tabs a[href="#tabCuenta"]').tab('show');
        $("#DDTipoCartola option[value=2]").hide();
        $("#DDTipoCartola option[value=3]").hide();
        $('#BtnConsultar').attr('data-original-title', 'Seleccione Cuenta');
        target = "#tabCuenta";
    }

    estadoBtnConsultar();

    if (target == "#tabCuenta") {
        if (!miInformacionCuenta) {
            setDateDatepicker(new Date(localStorage.UltimaFechaCierre), null);
        } else if (!miInformacionCuenta.FechaCierreVirtual) {
            setDateDatepicker(moment(miInformacionCuenta.UltimaFechaConsulta).format('L'), null);
        } else {
            setDateDatepicker(moment(miInformacionCuenta.FechaCierreVirtual).format('L'), miInformacionCuenta.FechaCierreVirtual);
        }
    }

    if (target == "#tabCliente") {
        if (!miInformacionCliente) {
            setDateDatepicker(new Date(localStorage.UltimaFechaCierre), null);
        } else if (!miInformacionCliente.FechaCierreVirtual) {
            setDateDatepicker(new Date(localStorage.UltimaFechaCierre), null);
        } else {
            setDateDatepicker(moment(miInformacionCliente.FechaCierreVirtual).format('L'), miInformacionCliente.FechaCierreVirtual);
        }
    }

}

function cargarEventHandlersReporteCartola() {
    $(".input-group.date").datepicker();

    $("#BtnConsultar").on('click', function () {
        jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
        vFechaTopeConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1);
        console.log(vFechaTopeConsulta);
        if (!vFechaTopeConsulta) {
            alert('Fecha reporte mayor a fecha cierre cuenta');
            return false;
        }

        var campoFechaConsulta = moment($(".input-group.date").datepicker('getDate')).format();
        var vFechaTopeFormato = moment(vFechaTopeConsulta).format();

        if (campoFechaConsulta > vFechaTopeFormato) {
            $(".input-group.date").datepicker('setDate', moment(vFechaTopeConsulta).format('L'));
            bootbox.alert("Fecha Reporte Mayor a Fecha Cierre Cuentas");
        } else {
            generaCartola();
        }
    });

    $("#porCuenta_NroCuenta").on('change', function () {
        estadoBtnConsultar();
        actualizaFechaConsulta();
    });

}

function generaCartola() {
    var cat = $("#DDTipoCartola").val();
    if (cat == 1) {
        obtenerCartolaBalance();
    } else if (cat == 2) {
        obtenerCartolaAPVConsolidada();
    } else if (cat == 3) {
        obtenerCartolaAPVAhorro();
    }
}
function obtenerCartolaBalance() {
    var rutCliente;
    var numCuenta;
    var CodError = 0;
    var MsgError = "";
    var jsfechaConsulta = moment($(".input-group.date").datepicker('getDate')).toISOString();
    rutCliente = miInformacionCuenta == null ? "" : miInformacionCuenta.RutCliente;
    numCuenta = miInformacionCuenta == null ? "" : miInformacionCuenta.NroCuenta;
    if (!miInformacionCuenta) {
        return;
    }
    $('#loaderProceso').modal('show');
    $.ajax({
        url: "../Servicios/Cartolas.asmx/ObtenerCartolaBalanceAdministracionCartera",
        data: "{'FechaConsulta':'" + jsfechaConsulta +
        "', 'RutCliente':'" + rutCliente +
        "', 'Cuenta':" + numCuenta +
        ", 'CodError':" + CodError +
        ", 'MsgError':'" + MsgError +
        "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                //window.open("data:application/pdf;base64," + mydata);

                var link = document.createElement("a");
                link.href = "data:application/pdf;base64," + mydata;

                //Set properties as you wise
                link.download = "CartolaBalance";
                link.target = "blank";

                //this part will append the anchor tag and remove it after automatic click
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function obtenerCartolaAPVConsolidada() {
    var rutCliente;
    var numCuenta;
    var CodError = 0;
    var MsgError = "";
    var jsfechaConsulta = moment($(".input-group.date").datepicker('getDate')).toISOString();
    rutCliente = miInformacionCliente == null ? "" : miInformacionCliente.RutCliente;
    if (!miInformacionCliente) {
        return;
    }
    $('#loaderProceso').modal('show');
    $.ajax({
        url: "../Servicios/Cartolas.asmx/ObtenerCartolaAPVConsolidadoAdministracionCartera",
        data: "{'FechaConsulta':'" + jsfechaConsulta +
        "', 'RutCliente':'" + rutCliente +
        "', 'CodError':" + CodError +
        ", 'MsgError':'" + MsgError +
        "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                //window.open("data:application/pdf;base64," + mydata);

                var link = document.createElement("a");
                link.href = "data:application/pdf;base64," + mydata;

                //Set properties as you wise
                link.download = "CartolaAPVConsolidada";
                link.target = "blank";

                //this part will append the anchor tag and remove it after automatic click
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function obtenerCartolaAPVAhorro() {
    var rutCliente;
    var numCuenta;
    var CodError = 0;
    var MsgError = "";
    var jsfechaConsulta = moment($(".input-group.date").datepicker('getDate')).toISOString();
    rutCliente = miInformacionCliente == null ? "" : miInformacionCliente.RutCliente;
    if (!miInformacionCliente) {
        return;
    }
    $('#loaderProceso').modal('show');
    $.ajax({
        url: "../Servicios/Cartolas.asmx/ObtenerCartolaAPVAhorroAdministracionCartera",
        data: "{'FechaConsulta':'" + jsfechaConsulta +
        "', 'RutCliente':'" + rutCliente +
        "', 'CodError':" + CodError +
        ", 'MsgError':'" + MsgError +
        "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                //window.open("data:application/pdf;base64," + mydata);

                var link = document.createElement("a");
                link.href = "data:application/pdf;base64," + mydata;

                //Set properties as you wise
                link.download = "CartolaAPVAhorro";
                link.target = "blank";

                //this part will append the anchor tag and remove it after automatic click
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function estadoBtnConsultar() {
    if (target == "#tabCuenta") {
        if (miInformacionCuenta == null) {
            $('#BtnConsultar').tooltip('enable').addClass('disabled');
        } else {
            $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
    if (target == "#tabCliente") {
        if (miInformacionCliente == null) {
            $('#BtnConsultar').tooltip('enable').addClass('disabled');
        } else {
            $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
}

function actualizaFechaConsulta() {

    if (target == "#tabCuenta") {
        if (!miInformacionCuenta) {
            setDateDatepicker(new Date(localStorage.UltimaFechaCierre), null);
        } else if (!miInformacionCuenta.FechaCierreVirtual) {
            setDateDatepicker(moment(miInformacionCuenta.UltimaFechaConsulta).format('L'), null);
        } else {
            setDateDatepicker(moment(miInformacionCuenta.FechaCierreVirtual).format('L'), miInformacionCuenta.FechaCierreVirtual);
        }
    }

    if (target == "#tabCliente") {
        if (!miInformacionCliente) {
            setDateDatepicker(new Date(localStorage.UltimaFechaCierre), null);
        } else if (!miInformacionCliente.FechaCierreVirtual) {
            setDateDatepicker(new Date(localStorage.UltimaFechaCierre), null);
        } else {
            setDateDatepicker(moment(miInformacionCliente.FechaCierreVirtual).format('L'), miInformacionCliente.FechaCierreVirtual)
        }
    }
}
function setDateDatepicker(pFecha, pFechaCierreVirtual) {
    $(".input-group.date").datepicker('setDate', pFecha);
    vFechaTopeConsulta = pFechaCierreVirtual;
}

$(document).ready(function () {
    initReporteCartola();
    cargarEventHandlersReporteCartola();
    $(window).trigger('resize');
});