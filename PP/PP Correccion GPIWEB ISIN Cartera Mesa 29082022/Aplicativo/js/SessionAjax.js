﻿// SessionAjax.js
var miSessionAjax = {
    SetSessionVar: function (strNombreVar, strValor) {
        var strSalida = "";
        $.ajax({
            url: "../Servicios/Session.asmx/GuardarVariableSesion",
            data: "{'strNombre':'" + strNombreVar + "', 'strValor':'" + strValor + "'}",
            datatype: "json",
            type: "post",
            async: false,
            contentType: "application/json; charset=utf-8",
            complete: function (jsondata, stat) {
                if (stat == "success") {
                    var jsonOperacion = JSON.parse(jsondata.responseText).d
                    strSalida = jsonOperacion;
                }
                else {
                    strSalida = JSON.parse(jsondata.responseText).Message;
                }
            }

        })
        return strSalida;
    },
    SetSessionVarObj: function (strNombreVar, strValor, callback) {
        var strSalida = "";
        $.ajax({
            url: "../Servicios/Session.asmx/GuardarVariableSesion",
            data: "{'strNombre':'" + strNombreVar + "', 'strValor':" + strValor + "}",
            type: "post",
            async: false,
            contentType: "application/json; charset=utf-8",
            complete: function (jsondata, stat) {
                if (stat == "success") {
                    strSalida = jsondata.responseText;
                    callback(null, strSalida);
                }
                else {
                    strSalida = JSON.parse(jsondata.responseText).Message;
                    callback(strSalida);
                }
            }

        })
    },
    GblUltFechaServidor: function (callback) {
        var strSalida = "";
        $.ajax({
            url: "../Servicios/Globales.asmx/BuscarFechaServidor",
            data: "{}",
            datatype: "json",
            type: "post",
            async: true,
            async: false,
            contentType: "application/json; charset=utf-8",
            complete: function (jsondata, stat) {
                if (stat == "success") {
                    var jsonOperacion = moment(JSON.parse(jsondata.responseText).d).format('l');
                    strSalida = jsonOperacion;
                    callback(null, strSalida);
                }
                else {
                    strSalida = JSON.parse(jsondata.responseText).Message;
                    callback(strSalida);
                }
            }
        })
    }

};