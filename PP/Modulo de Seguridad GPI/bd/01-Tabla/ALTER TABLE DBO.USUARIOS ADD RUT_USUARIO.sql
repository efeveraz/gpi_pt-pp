USE [CSGPI]
GO

Set NoCount On
SET XACT_ABORT ON

BEGIN TRANSACTION;
BEGIN TRY


	IF ((SELECT COUNT(*) FROM SYSOBJECTS A INNER JOIN SYSCOLUMNS B ON A.ID = B.ID
		 WHERE (A.NAME = 'USUARIOS') AND (B.NAME = 'RUT')) = 0)
		BEGIN
		ALTER TABLE DBO.USUARIOS
		ADD RUT VARCHAR(10) NULL
	END
	
	
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE()
        , @ErrorSeverity = ERROR_SEVERITY()
        , @ErrorState = ERROR_STATE();
    IF @ErrorState = 0
        SET @ErrorState = 1
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
    RAISERROR ( @ErrorMessage
              , @ErrorSeverity
              , @ErrorState);
END CATCH;
IF @@TRANCOUNT > 0
BEGIN
	COMMIT TRANSACTION;
	SELECT 'OK' AS ErrorMessage
END

