USE CSGPI 
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_USUARIOS$Guardar]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE PKG_USUARIOS$Guardar
GO
CREATE  PROCEDURE [dbo].[PKG_USUARIOS$Guardar] (
    @PId_Usuario		FLOAT(53) OUTPUT,
  	@PId_Asesor			FLOAT(53),
    @PId_Empresa		FLOAT(53),
    @PId_Tipo_Estado	FLOAT(53),
    @PCod_Estado		VARCHAR(3),
    @PDsc_Usuario		VARCHAR(100),
	@PPassword			VARCHAR(200),
	@PFechaVigencia		VARCHAR(200),
	@pEmail				VARCHAR(100)
)
AS
BEGIN

	DECLARE @vClave varchar(200),
			@vCodEstado varchar(3)

	SELECT	@vClave = PASSWORD,
			@vCodEstado = Cod_Estado
	FROM	USUARIOS with (nolock)
	WHERE	Id_Usuario = @PId_Usuario
	AND		Id_Empresa = @PId_Empresa


	UPDATE	USUARIOS
	SET	Id_Asesor = @PId_Asesor,
		Id_Empresa = @PId_Empresa,
		Id_Tipo_Estado = @PId_Tipo_Estado,
		Cod_Estado = @PCod_Estado,
		Dsc_Usuario = @PDsc_Usuario,
		--PASSWORD = @PPassword,
		FECHA_INSERT =GETDATE(),
		--FECHA_VIGENCIA = @pFechaVigencia,
		EMAIL_USUARIO = @pEmail
	WHERE	Id_Usuario = @PId_Usuario
	AND		Id_Empresa = @PId_Empresa

	IF (@@ROWCOUNT = 0)
    BEGIN
		INSERT INTO USUARIOS( 
			Id_Asesor,
			Id_Empresa,
			Id_Tipo_Estado,
			Cod_Estado,
			Dsc_Usuario,
			--PASSWORD,
			FECHA_INSERT ,
			--FECHA_VIGENCIA,
			EMAIL_USUARIO
		)
		VALUES ( 
			@PId_Asesor,
	 		@PId_Empresa,
			@PId_Tipo_Estado,
			@PCod_Estado,
			@PDsc_Usuario,
			--@PPassword,
			GETDATE(),
			--@pFechaVigencia,
			@pEmail
		)

        SET @Pid_Usuario = @@IDENTITY

	END
	ELSE
	BEGIN
	IF 
				    (@vClave <> (SELECT TOP 1 CLAVE FROM SIS_HIST_CLAVE 
					WHERE ID_USUARIO = @Pid_Usuario AND ID_NEGOCIO = @PId_Empresa
					ORDER BY ID_CLAVE DESC))
	BEGIN				
		INSERT INTO SIS_HIST_CLAVE (
				ID_NEGOCIO,
				ID_USUARIO,
				CLAVE,
				FECHA,
				ESTADO
				
		)
		SELECT	@PId_Empresa,
				@PId_Usuario,
				@vClave,
				GETDATE(),
				@vCodEstado
		END		
	END

END
GO
GRANT EXECUTE ON PKG_USUARIOS$Guardar TO DB_EXECUTESP
GO

