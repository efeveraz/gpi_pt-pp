USE CSGPI 
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_USUARIOS$Buscar_Usuarios]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE PKG_USUARIOS$Buscar_Usuarios
GO
CREATE   	
  PROCEDURE [dbo].[PKG_USUARIOS$Buscar_Usuarios]    	
  ( @PID_EMPRESA NUMERIC =  NULL   	
  ) AS   	
   BEGIN   	
      SELECT  U.ID_USUARIO   	
            , U.DSC_USUARIO   	
            , U.ID_EMPRESA   	
            , U.ID_ASESOR   	
            , A.NOMBRE   	
            , U.ID_TIPO_ESTADO   	
            , U.COD_ESTADO   	
            , dbo.Pkg_Global$Dame_Glosa_Estado(u.id_tipo_estado,u.cod_estado) AS 'DSC_ESTADO'  	  	
			, U.FECHA_VIGENCIA
			, U.EMAIL_USUARIO
        FROM  USUARIOS U with (nolock)
			LEFT OUTER JOIN ASESORES A with (nolock)	
        ON A.ID_ASESOR=U.ID_ASESOR   	
        WHERE U.ID_EMPRESA=@PID_EMPRESA   	
     ORDER BY U.DSC_USUARIO
END
GO
GRANT EXECUTE ON PKG_USUARIOS$Buscar_Usuarios TO DB_EXECUTESP
GO

