USE CSGPI 
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[Sis_ParametrosMantencion]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE Sis_ParametrosMantencion
GO
CREATE PROCEDURE Sis_ParametrosMantencion
(
	@pIdNegocio NUMERIC(10),
	@pCodigo	VARCHAR(100),
	@pValor		VARCHAR(500),
	@pAccion	VARCHAR(10),
	@pResultado	VARCHAR(2000)
)
AS
BEGIN TRY

	SET @pResultado	 = 'OK'

	DECLARE @vIdParametro	numeric(10),
			@vEstado		varchar(3)

	IF @pAccion = 'INSERT'		
	BEGIN
		INSERT INTO SIS_PARAMETROS(ID_NEGOCIO,
				CODIGO,
				VALOR,
				ESTADO
		)
		SELECT	@pIdNegocio, @pCodigo, @pValor, 'VIG'
		EXCEPT 
		SELECT	ID_NEGOCIO,
				CODIGO,
				VALOR,
				ESTADO
		FROM	SIS_PARAMETROS WITH (NOLOCK)
	END
	ELSE
	BEGIN
		SELECT	@vEstado =	CASE	
								WHEN @pAccion = 'MODIFICAR' THEN 'VIG'
								WHEN @pAccion = 'ELIMINAR' THEN 'ELI'
							END

		SELECT @vIdParametro =	(	
									SELECT	id_parametro 
									FROM	SIS_PARAMETROS WITH (NOLOCK)
									WHERE	codigo = @pCodigo
								)
		UPDATE	SIS_PARAMETROS
		SET		ESTADO = @vEstado 
		WHERE	ID_PARAMETRO = @vIdParametro
	END
END TRY
BEGIN CATCH
	SET @pResultado =  ERROR_MESSAGE()	+ ' procedimiento: ' + ERROR_PROCEDURE() + ' l�nea: ' + CONVERT(VARCHAR,ERROR_LINE())
END CATCH
GO
GRANT EXECUTE ON Sis_ParametrosMantencion TO DB_EXECUTESP
GO

