USE CSGPI 
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_FECHA_VIGENCIA_USUARIO]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE PKG_FECHA_VIGENCIA_USUARIO
GO

/****** Object:  StoredProcedure [dbo].[PKG_FECHA_VIGENCIA_USUARIO]    Script Date: 14-10-2021 12:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PKG_FECHA_VIGENCIA_USUARIO]
		@Pid_Usuario numeric
AS
BEGIN
DECLARE	 @fecha_actual AS DATETIME,
		 @fecha_vigencia AS DATETIME,
		 @diferencia_fechas AS DATETIME

SET @fecha_actual = getdate()
SET @fecha_vigencia = (select fecha_vigencia from usuarios where id_usuario = @Pid_Usuario)
SET @diferencia_fechas = (select datediff(dayofyear,@fecha_actual,@fecha_vigencia))

IF (@diferencia_fechas > 0)
    SELECT 'V' Estado
ELSE
    SELECT 'NV' Estado
END
GO
GRANT EXECUTE ON PKG_FECHA_VIGENCIA_USUARIO TO DB_EXECUTESP
GO

