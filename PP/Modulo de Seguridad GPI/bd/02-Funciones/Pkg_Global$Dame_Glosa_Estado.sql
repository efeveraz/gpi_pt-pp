USE CSGPI 
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[dbo].[Pkg_Global$Dame_Glosa_Estado]') AND XTYPE IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [dbo].[Pkg_Global$Dame_Glosa_Estado]
GO
CREATE FUNCTION [dbo].[Pkg_Global$Dame_Glosa_Estado](   
	@pId_Tipo_estado	as numeric,  
	@pCod_estado		as Varchar(3)  
) 
RETURNS varchar(20)  
AS  
BEGIN  
	DECLARE @ValorRetorno Varchar(20) 
	  
	SET @ValorRetorno = 'Estado No Definido'  

	SELECT	@ValorRetorno = DSC_ESTADO
	FROM	ESTADOS 
	WHERE	ID_TIPO_ESTADO = @pId_Tipo_Estado 
    AND		COD_ESTADO = @pCod_Estado

	SET	@ValorRetorno = isnull(@ValorRetorno, 'Estado No Definido')

	RETURN @ValorRetorno  
END
GO
GRANT EXECUTE ON [Pkg_Global$Dame_Glosa_Estado] TO DB_EXECUTESP
GO

