USE [CisCB]
GO
/****** Object:  StoredProcedure [dbo].[sp_CartolaApvFlexibleConsDetalleBal_temp]    Script Date: 12/29/2020 16:49:28 ******/
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CartolaApvFlexibleConsDetalleBal_temp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CartolaApvFlexibleConsDetalleBal_temp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_CartolaApvFlexibleConsDetalleBal_temp]
@RutCliente      VARCHAR(15)
,@FechaDesde      VARCHAR(8)
,@FechaHasta      VARCHAR(8)
,@CuentaEntrada   INT
,@CodErr          INT            OUTPUT
,@MsgErr          VARCHAR(4000)   OUTPUT


AS
BEGIN

   SET NOCOUNT ON;
/*

declare
@RutCliente      VARCHAR(15)
,@FechaDesde      VARCHAR(8)
,@FechaHasta      VARCHAR(8)
,@CuentaEntrada   INT
,@CodErr          INT
,@MsgErr          VARCHAR(4000)

set @RutCliente     = '76258327-5'
set @FechaDesde      ='20170531'
set @FechaHasta     ='20170611'
set @CuentaEntrada   =1929
set @CodErr          =0
set @MsgErr         =''
 */
   --***********************************************************************************************
   -- DECLARACION DE VARIABLES
   DECLARE @UF1                  NUMERIC(10,2)
   DECLARE @UF2        NUMERIC(10,2)
   DECLARE @DOLAR1                  NUMERIC(10,2)
   DECLARE @DOLAR2                  NUMERIC(10,2)
   DECLARE @Consolidado            VARCHAR(10)
   DECLARE @ID_CLIENTE               INT
   DECLARE @ID_EMPRESA               INT
   DECLARE @ID_MONEDA               INT
   DECLARE @ID_TIPOCUENTA            INT
   DECLARE @CONT                  INT
   DECLARE @CANTIDAD               INT
   DECLARE @ID_ARBOL_CLASE_INST_HOJA   INT
   DECLARE @DSC_ARBOL_CLASE_INST      VARCHAR(100)
   DECLARE @DSC_ARBOL_CLASE_INST_HOJA   VARCHAR(100)
   DECLARE @SALDO_ANTERIOR            FLOAT
   DECLARE @SALDO                  FLOAT
   DECLARE @VECNOM                  VARCHAR(MAX)
   DECLARE @VECMTO                  VARCHAR(MAX)
   DECLARE @ID_CUENTA             INT
   DECLARE @PID_CAJA_CUENTA INT
   DECLARE @pId_Moneda_Salida INT
   DECLARE @ID_CAJA_CUENTA  INT
   DECLARE @CANTIDAD2   INT
   DECLARE @CONT2       INT
   DECLARE @SALDO_PRIMER FLOAT
   DECLARE @ID_ARBOL_CLASE_INST_HOJA_ELM   INT
   declare @CODIGO varchar(50)
   declare @Decimales int
   declare @DESC_CAJA_CUENTA varchar(100)
   DECLARE @ID_ARBOL_SEC_ECONOMICO INT
   DECLARE @fecha_desde_caja varchar(8)

   --***********************************************************************************************
   -- DECLARACION DE TABLAS

   DECLARE @INDICADORES TABLE (ID_TIPO_CAMBIO    INT
                              ,DSC_MONEDA        VARCHAR(50)
                              ,VALOR             NUMERIC(18,2)
                              ,DSC_TIPO_CAMBIO   VARCHAR(100)
                              ,ABR_TIPO_CAMBIO   VARCHAR(30) )

   DECLARE @INDICADORES_SALIDA TABLE (ID_TABLA               INT      IDENTITY(1,1)
                                     ,Descripcion            VARCHAR(30)
                                     ,VALOR1                  VARCHAR(30)
                                     ,VALOR2                  VARCHAR(30) )

   /*DECLARE @Cuentas TABLE (ID                      INT IDENTITY(1,1),
                           id_cuenta               INT,
                           id_contrato_cuenta      INT,
                           num_cuenta              INT,
                           ABR_CUENTA              VARCHAR(30),
                           DSC_CUENTA              VARCHAR(100),
                           ID_MONEDA               INT,
                           COD_MONEDA              VARCHAR(10),
                           DSC_MONEDA              VARCHAR(30),
                           FLG_ES_MONEDA_PAGO      VARCHAR(10),
                           OBSERVACION             VARCHAR(100),
                           FLG_BLOQUEADO           VARCHAR(1),
                           OBS_BLOQUEO             VARCHAR(100),
                           FLG_IMP_INSTRUCCIONES   VARCHAR(1),
                           id_cliente         INT,
                           rut_cliente             VARCHAR(15),
                           nombre_cliente          VARCHAR(100),
                           ID_TIPO_ESTADO          INT,
                           COD_ESTADO              VARCHAR(1),
                           DSC_ESTADO              VARCHAR(30),
                           COD_TIPO_ADMINISTRACION VARCHAR(10),
                           DSC_TIPO_ADMINISTRACION VARCHAR(30),
                           ID_EMPRESA              INT,
                           DSC_EMPRESA             VARCHAR(30),
                           ID_PERFIL_RIESGO        INT,
                           DSC_PERFIL_RIESGO       VARCHAR(30),
                           FECHA_OPERATIVA         DATETIME,
                           FLG_MOV_DESCUBIERTOS    VARCHAR(1),
                           ID_ASESOR               INT,
                           Fecha_Cierre_Cuenta     DATETIME,
                           ID_CAJA_CUENTA          INT,
                           DSC_CAJA_CUENTA         VARCHAR(30),
       DECIMALES_CUENTA        INT,
                           Simbolo_Moneda          VARCHAR(10),
                           FECHA_CONTRATO          DATETIME,
                           ID_TIPOCUENTA           INT,
                           NUMERO_FOLIO            INT,
                           TIPO_AHORRO             INT,
                           RUT_ASESOR              VARCHAR(15),
                           NOMBRE_ASESOR           VARCHAR(100),
                           Flg_Considera_Com_VC    VARCHAR(1),
                           DSC_TIPOAHORRO          VARCHAR(250) )
             */


 ---*********************
DECLARE @Cuentas TABLE (ID      INT IDENTITY(1,1),
      ID_CUENTA                  INT,
      ID_CLIENTE                 INT,
      ID_TIPO_ESTADO             INT,
      COD_ESTADO                 VARCHAR(1),
      COD_TIPO_ADMINISTRACION    VARCHAR(10),
      ID_CONTRATO_CUENTA         INT,
      ID_EMPRESA                 INT,
      ID_ASESOR                  INT,
      ID_MONEDA                  INT,
      ID_PERFIL_RIESGO           INT,
      NUM_CUENTA                 INT,
      ABR_CUENTA                 VARCHAR(30),
      DSC_CUENTA                 VARCHAR(100),
      OBSERVACION                VARCHAR(100),
      FLG_BLOQUEADO              VARCHAR(1),
      OBS_BLOQUEO                VARCHAR(100),
      FLG_IMP_INSTRUCCIONES      VARCHAR(1),
      FECHA_OPERATIVA            DATETIME,
      FLG_MOV_DESCUBIERTOS       VARCHAR(1),
      ID_USUARIO_INSERT          INT,
      FCH_INSERT                 DATETIME,
      ID_USUARIO_UPDATE          INT,
      FCH_UPDATE                 DATETIME,
      PORCEN_RF                  NUMERIC(18,4),
      PORCEN_RV                  NUMERIC(18,4),
      FECHA_CIERRE_CUENTA        DATETIME,
      FECHA_CONTRATO             DATETIME,
      ID_TIPOCUENTA              INT,
      NUMERO_FOLIO               INT,
      TIPO_AHORRO                INT,
      FLG_CONSIDERA_COM_VC       VARCHAR(1),
      FLG_COMISION_AFECTA_IMPUESTO    VARCHAR(1),
      CODIGO_EXTERNO_CUENTA           INT    --06-01-2014
      )


   --*************************


   DECLARE @Arbol   TABLE (ID_TABLA                  INT            IDENTITY(1,1),
                           ID_ARBOL_CLASE_INST       INT,
                           ID_ARBOL_CLASE_INST_HOJA  INT,
                           DSC_ARBOL_CLASE_INST      VARCHAR(100),
                           DSC_ARBOL_CLASE_INST_HOJA VARCHAR(100),
                           MONTO_MON_CTA_INST        BIGINT,
                           MONTO_MON_CTA_HOJA        BIGINT,
                           CODIGO                    VARCHAR(10),
                           NIVEL                     INT ,
         CODIGO_PADRE    VARCHAR(50))

   /*DECLARE   @SaldosActivos TABLE (ID_TABLA                    INT            IDENTITY(1,1),
                                   fecha_cierre                DATETIME,
                                   id_nemotecnico              NUMERIC,
                                   nemotecnico                 VARCHAR(50),
                                   EMISOR                      VARCHAR(100),
                                   cod_emisor                  VARCHAR(10),
                                   dsc_nemotecnico             VARCHAR(120),
                                   FECHA_VENCIMIENTO           DATETIME,
                                   id_moneda_cta               NUMERIC,
                                   id_moneda_nemotecnico       NUMERIC,
                       SIMBOLO_MONEDA              VARCHAR(3),
                                   id_empresa                  NUMERIC,
                                   id_arbol_clase_inst         NUMERIC,
                                   cod_instrumento             VARCHAR(15),
                                   dsc_arbol_clase_inst        VARCHAR(100),
                                   dsc_padre_arbol_clase_inst  VARCHAR(100),
                                   dias                        NUMERIC,
                                   duration                    NUMERIC,
                                   cod_producto                VARCHAR(10),
                                   id_padre_arbol_clase_inst   NUMERIC,
                                   decimales_nemo              NUMERIC,
                                   cantidad                    NUMERIC(18,4),
                                   precio                      NUMERIC(18,6),
                                   tasa_emision                NUMERIC(18,4),
                                   precio_compra               NUMERIC(18,4),
                                   tasa                        NUMERIC(18,4),
                                   tasa_compra                 NUMERIC(18,4),
                                   monto_valor_compra          NUMERIC(18,4),
                                   monto_mon_cta               NUMERIC(18,4),
                                   precio_promedio_compra      NUMERIC(18,4),
                                   tasa_promedio_compra        NUMERIC(18,4),
                                   monto_promedio_compra       NUMERIC(18,4),
                                   monto_mon_nemotecnico       NUMERIC(18,4),
                                   monto_mon_origen            NUMERIC(18,4),
                                   porcentaje_rama             NUMERIC(18,4),
                                   rentabilidad                FLOAT )
*/


DECLARE   @SaldosActivos TABLE( origen                       VARCHAR(50)
  ,ID_CUENTA        NUMERIC
  --, ID_SALDO_ACTIVO                   NUMERIC
  , FECHA_CIERRE                      DATETIME
  , ID_NEMOTECNICO                    NUMERIC
  , NEMOTECNICO                       VARCHAR(50)
  , EMISOR                            VARCHAR(100)
  , COD_EMISOR                        VARCHAR(10)
  , DSC_NEMOTECNICO                   VARCHAR(120)
  , TASA_EMISION_2                    NUMERIC(18,4)
  , CANTIDAD                          NUMERIC(18,4)
  , PRECIO                            NUMERIC(18,6)
  , TASA_EMISION                      NUMERIC(18,4)
  , FECHA_VENCIMIENTO                 DATETIME
  , PRECIO_COMPRA                     NUMERIC(18,4)
  , TASA                              NUMERIC(18,4)
  , TASA_COMPRA                       NUMERIC(18,4)
  , MONTO_VALOR_COMPRA                NUMERIC(18,4)
  , MONTO_MON_CTA                     NUMERIC(18,4)
  , ID_MONEDA_CTA                     NUMERIC
  , ID_MONEDA_NEMOTECNICO             NUMERIC
  , SIMBOLO_MONEDA                    VARCHAR(3)
  , MONTO_MON_NEMOTECNICO             NUMERIC(18,4)
  , MONTO_MON_ORIGEN                  NUMERIC(18,4)
  --, MONTO_PROMEDIO_COMPRA             NUMERIC(18,4)
  , ID_EMPRESA                        NUMERIC
  , ID_ARBOL_CLASE_INST               NUMERIC
  , COD_INSTRUMENTO                   VARCHAR(15)
  , DSC_ARBOL_CLASE_INST              VARCHAR(100)
  , PORCENTAJE_RAMA                   NUMERIC(18,4)
  , PRECIO_PROMEDIO_COMPRA            NUMERIC(18,4)
  , MONTO_PROMEDIO_COMPRA             NUMERIC(18,4)
  , DSC_PADRE_ARBOL_CLASE_INST        VARCHAR(100)
  , RENTABILIDAD                      FLOAT
  , DIAS                              NUMERIC
  --, DECIMALES_MOSTRAR                 NUMERIC
  , COD_PRODUCTO                      VARCHAR(10)
  , ID_PADRE_ARBOL_CLASE_INST         NUMERIC
  , DURATION                          FLOAT
  , DECIMALES_MOSTRAR                 NUMERIC
  , DSC_CLASIFICADOR_RIESGO           VARCHAR(50)
  , ID_SUBFAMILIA                     NUMERIC
  , COD_SUBFAMILIA                    VARCHAR(50)
  --, DSC_CLASIFICADOR_RIESGO           VARCHAR(50)
  , CODIGO                            VARCHAR(20)
  , MONTO_INVERTIDO                   NUMERIC(18,6)
  --, MONTO_MON_USD                     NUMERIC(18,6)
  , VALOR_MERCADO                     NUMERIC(18,6)
  , UTILIDAD_PERDIDA                  NUMERIC(18,6)
  , NUMERO_CONTRATO                   VARCHAR(50)
  , TIPO_FWD                          VARCHAR(10)
  , MONEDA_EMISION                    VARCHAR(10)
  , MODALIDAD                         VARCHAR(15)
  , UNIDAD_ACTIVO_SUBYACENTE          VARCHAR(10)
  , DECIMALES_ACTIVO_SUBYACENTE       NUMERIC
  , VALOR_NOMINAL                     NUMERIC(18,4)
  , FECHA_INICIO                      DATETIME
  --, FECHA_VENCIMIENTO2                 DATETIME
  , PRECIO_PACTADO                    NUMERIC(18,6)
  , VALOR_PRECIO_PACTADO              NUMERIC(18,4)
  , MONTO_MON_USD                     NUMERIC(18,6)
  )

--*****







--*****

   --*** Renta variable nacional
   DECLARE @RentaVariable TABLE (ID_TABLA               INT            IDENTITY(1,1),
         origen                       VARCHAR(50),
                                 TipoInstrumento        VARCHAR(50),
                                 Nemotecnico            VARCHAR(100),
                                 Moneda                 VARCHAR(10),
                                 PorcentajeInversion    NUMERIC(6,2),
                                 Cantidad               NUMERIC(18,4), --INT,
                                 PrecioCompra           NUMERIC(18,4),
                                 MontoCompra            NUMERIC(18,4),-- BIGINT
                                 PrecioActual           NUMERIC(18,4),
                                 ValorMercado           NUMERIC(18,6),  -- BIGINT,
         Monto_mon_origen     NUMERIC(18,6),   --BIGINT,
                                 Rentabilidad           NUMERIC(18,4),
         cod_producto            varchar(10),
         duration    FLOAT,
                                 CODIGO                  VARCHAR(20))--NUMERIC(6,2) )


DECLARE @RVOtrosActivosFinv TABLE (ID_TABLA               INT            IDENTITY(1,1),
         origen                       VARCHAR(50),
                                 TipoInstrumento        VARCHAR(50),
                                 Nemotecnico            VARCHAR(50),
                                 Moneda                 VARCHAR(10),
                                 PorcentajeInversion    NUMERIC(6,2),
                                 Cantidad               NUMERIC(18,4), --INT,
                                 PrecioCompra           NUMERIC(18,4),
                                 MontoCompra            NUMERIC(18,4),-- BIGINT
                                 PrecioActual           NUMERIC(18,4),
                                 ValorMercado           NUMERIC(18,6),  -- BIGINT,
         Monto_mon_origen     NUMERIC(18,6),   --BIGINT,
                                 Rentabilidad           NUMERIC(18,4),
         cod_producto            varchar(10),
         duration    FLOAT,
                                 CODIGO                  VARCHAR(20))--NUMERIC(6,2) )

  --******Renta variable internacional Acciones, fondos mutuos, ETFs
  DECLARE @RentaVariableInternacional TABLE (ID_TABLA               INT            IDENTITY(1,1),
                                               origen                       VARCHAR(50),
              TipoInstrumento        VARCHAR(100),
              Nemotecnico            VARCHAR(350),
              Moneda                 VARCHAR(10),
              PorcentajeInversion    NUMERIC(6,2),
              Cantidad               NUMERIC(18,4), --INT,
              PrecioCompra           NUMERIC(18,4),
              MontoCompra            NUMERIC(18,4), --BIGINT,
              PrecioActual           NUMERIC(18,4),
              ValorMercado           NUMERIC(18,4), --BIGINT,
              Monto_mon_origen     NUMERIC(18,4),--BIGINT,
              Rentabilidad           NUMERIC(18,4),
              cod_producto            varchar(10),
              duration    FLOAT,
              CODIGO                  VARCHAR(20),
              MontoInvertido         NUMERIC(18,4),
              UtilidadPerdida        NUMERIC(18,4),
              ValorizacionUSD        NUMERIC(18,4),
              ValorizacionPeso       NUMERIC(18,4)
              )--NUMERIC(6,2) )


  --***renata fija nacional fondos largo y corto ,estructurado,inversiones, venta corta
  DECLARE @RentaFijaFondos TABLE (ID_TABLA               INT            IDENTITY(1,1),
                           origen                       VARCHAR(50),
                                 TipoInstrumento        VARCHAR(50),
                                 Nemotecnico            VARCHAR(350),
                                 Moneda                 VARCHAR(10),
                                 PorcentajeInversion    NUMERIC(6,2),
                                 Cantidad               NUMERIC(18,4), --INT,
                                 PrecioCompra           NUMERIC(18,4),
                                 MontoCompra            BIGINT,
                                 PrecioActual           NUMERIC(18,4),
                                 ValorMercado          NUMERIC(18,4), --BIGINT,  12-02-2014
         Monto_mon_origen     NUMERIC(18,4),
         monto_mon_nemotecnico  NUMERIC(18,4), --nuevo 23-01-2014
                                 Rentabilidad           NUMERIC(18,4),
         cod_producto                 varchar(10),
         duration    FLOAT,
         CODIGO                  VARCHAR(20))



   --***renata fija Internacional bonos ,fondos mutuos,ETFs
  DECLARE @RentaFijaFondosInter TABLE (ID_TABLA               INT            IDENTITY(1,1),
            origen                       VARCHAR(50),
                                 TipoInstrumento        VARCHAR(50),
                                 Nemotecnico            VARCHAR(350),
                                 Moneda                 VARCHAR(10),
                                 PorcentajeInversion    NUMERIC(6,2),
                                 Cantidad               NUMERIC(18,4), --INT,
                                 PrecioCompra           NUMERIC(18,4),
                                 MontoCompra            NUMERIC(18,4), --BIGINT,
                                 PrecioActual           NUMERIC(18,4),
                                 ValorMercado           NUMERIC(18,4), --BIGINT,  12-02-2014
         Monto_mon_origen     NUMERIC(18,4),
         monto_mon_nemotecnico  NUMERIC(18,4), --nuevo 23-01-2014
                                 Rentabilidad           NUMERIC(18,4),
         cod_producto           varchar(10),
         duration    FLOAT,
         CODIGO                 VARCHAR(20),
         MontoInvertido         NUMERIC(18,4),
         UtilidadPerdida        NUMERIC(18,4),
         ValorizacionUSD        NUMERIC(18,4),
         ValorizacionPeso       NUMERIC(18,4)

         )


 --fondos mixtos
 DECLARE @RentaFondosMixtos TABLE (ID_TABLA     INT            IDENTITY(1,1),
         origen                       VARCHAR(50),
                                 TipoInstrumento        VARCHAR(50),
                                 Nemotecnico            VARCHAR(350),
                                 Moneda                 VARCHAR(10),
                                 PorcentajeInversion    NUMERIC(6,2),
                                 Cantidad               NUMERIC(18,4), --INT,
                                 PrecioCompra           NUMERIC(18,4),
                                 MontoCompra            BIGINT,
                                 PrecioActual           NUMERIC(18,4),
                                 ValorMercado           BIGINT,
         Monto_mon_origen     NUMERIC(18,4),
                                 Rentabilidad           NUMERIC(18,4),
         cod_producto                 varchar(10),
         duration    FLOAT,
         CODIGO                  VARCHAR(20))


--renta fija nacional : Bonos estatales, empresas, letras hipotecario,depositos a plazo, pactos
   DECLARE @RentaFija TABLE (ID_TABLA               INT            IDENTITY(1,1),
        origen                       VARCHAR(50),
                             TipoInstrumento        VARCHAR(50),
                             Nemotecnico            VARCHAR(350),
                             Emisor                 VARCHAR(50),
                             Moneda                 VARCHAR(10),
                             PorcentajeInversion    NUMERIC(6,2),
                             Nominales              NUMERIC(18,4),
                             TasaCupon              VARCHAR(10),
                             FechaVencimiento       VARCHAR(8),
                             TasaCompra             NUMERIC(18,4),
                             TasaMercado            NUMERIC(6,2),
                             Valorizacion           BIGINT,
        cod_producto                 varchar(10),
        duration    FLOAT,
       CODIGO                  VARCHAR(20))



--renta fija Internacional : Bonos
   DECLARE @RentaFijaInternacional TABLE (ID_TABLA               INT            IDENTITY(1,1),
        origen                       VARCHAR(50),
               TipoInstrumento        VARCHAR(50),
                             Nemotecnico            VARCHAR(350),
                             Emisor                 VARCHAR(50),
                             Moneda                 VARCHAR(10),
                             PorcentajeInversion    NUMERIC(6,2),
                             Nominales              NUMERIC(18,4),
                             TasaCupon              VARCHAR(10),
                             FechaVencimiento       VARCHAR(8),
                             TasaCompra             NUMERIC(18,4),
                             TasaMercado            NUMERIC(6,2),
                             Valorizacion            NUMERIC(18,4), --BIGINT,
        cod_producto           varchar(10),
        duration    FLOAT,
       CODIGO                  VARCHAR(20))


   DECLARE @CajasCuentas TABLE (ID_TABLA            INT            IDENTITY(1,1),
                                ID_CAJA_CUENTA      INT,
                                DSC_CAJA_CUENTA     VARCHAR(30),
                                ID_CUENTA           INT,
                                NUM_CUENTA          VARCHAR(10),
                                ID_MONEDA           INT,
                                COD_MONEDA          VARCHAR(10),
                                DSC_MONEDA          VARCHAR(30),
                                COD_MERCADO         VARCHAR(1),
                                DESC_MERCADO        VARCHAR(30),
                                Decimales           INT )

   DECLARE @SaldoInicial TABLE (id_Moneda           INT,
                                SaldoCaja           NUMERIC(18,4) )

   DECLARE @InformesCajas   TABLE (ID_TABLA           INT            IDENTITY(1,1),
                                   ID_CUENTA          INT,
                                   ID_CIERRE		  INT,
                                   ID_OPERACION       INT,
                                   FECHA_LIQUIDACION  DATETIME,
                                   DESCRIPCION        VARCHAR(100),
           DESCRIPCION_CAJA   VARCHAR(100),
                                   MONEDA             VARCHAR(10),
                                   MONTO_MOVTO_ABONO  NUMERIC(18,6),
                                   MONTO_MOVTO_CARGO  NUMERIC(18,6),
                                   SALDO              NUMERIC(18,6),
           --SALDO_DOLAR        NUMERIC(18,4),
           ID_MONEDA    INT ,
         DSC_CAJA_CUENTA VARCHAR(100),
         ID_CAJA_CUENTA INT)

   DECLARE @RvSector TABLE (ID_TABLA        INT            IDENTITY(1,1),
                            ID_SECTOR       INT,
                            DSC_SECTOR      VARCHAR(50),
                            MONTO_MON_CTA   BIGINT,
                            PORC_SECTOR     NUMERIC(10,6) )


--******glosa moneda
DECLARE @GlosaMoneda TABLE (ID_TABLA        INT            IDENTITY(1,1),
                            DSC_MONEDA      VARCHAR(50)
                           )


  DECLARE @TMP TABLE (
      ID_ARBOL_CLASE_INST                        NUMERIC,
                       ID_ARBOL_CLASE_INST_HOJA                  NUMERIC,
                       DSC_ARBOL_CLASE_INST VARCHAR(100),
                       DSC_ARBOL_CLASE_INST_HOJA VARCHAR(100),
                       MONTO_MON_CTA_INST        NUMERIC(18,2),
                       MONTO_MON_CTA_HOJA        NUMERIC(18,2),
                       CODIGO                    VARCHAR(20),
                       NIVEL                     INT ,
                       CODIGO_PADRE              VARCHAR(20)  )

DECLARE @Forwards TABLE (ID_TABLA               INT            IDENTITY(1,1),
                         origen                       VARCHAR(50),
       TipoInstrumento        VARCHAR(50),
       NumeroContrato         NUMERIC(18,4),
       MonedaEmision          VARCHAR(50),
       Modalidad              VARCHAR(100),
       UnidadActivo    VARCHAR(50),
       CantidadNominal        NUMERIC(18,4), --INT,
       FechaInicial          datetime,
       FechaFinal          datetime,
       PrecioPactado           NUMERIC(18,4),
       Resultado              NUMERIC(18,4) --BIGINT,

              )--NUMERIC(6,2) )

 declare @transaccion table(
   FechaOperacion datetime
   ,fechaliquidacion datetime
   ,tipoMovimiento varchar(1)
   ,descTipoMovimiento varchar(50)
   ,numCuenta varchar(5)
   ,idOperacion int
   ,factura int
   ,nemotecnico varchar (350)
   ,cantidad decimal(16,4)
   ,moneda varchar(3)
   ,decimalesMostrar int
   ,precio decimal(16,4)
   ,comision decimal(16,4)
   ,derechos decimal(16,4)
   ,gastos decimal(16,4)
   ,iva decimal(16,4)
   ,monto decimal(16,4)
   ,montoTotalOperacion decimal(16,4)
   ,descTipoOperacion varchar (350)
  )



   --***********************************************************************************************

  BEGIN TRY

      SET @CodErr = 0
      SET @MsgErr = ''
      SET @Consolidado = 'CLT'

      --***********************************************************************************************
      -- INDICADORES
      --***********************************************************************************************

      INSERT INTO @INDICADORES EXEC CSGPI.dbo.PKG_VALOR_TIPO_CAMBIO$BuscarIndicadores @FechaHasta

      INSERT INTO @INDICADORES_SALIDA
      SELECT 'Valores al'
            ,@FechaHasta
            ,@FechaDesde

      SELECT @DOLAR1 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 1
      SELECT @UF1    = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 2

      INSERT INTO @INDICADORES EXEC CSGPI.dbo.PKG_VALOR_TIPO_CAMBIO$BuscarIndicadores @FechaDesde

      SELECT @DOLAR2 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 1
      SELECT @UF2    = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 2

      INSERT INTO @INDICADORES_SALIDA
      SELECT   'UF',@UF1,@UF2

      INSERT INTO @INDICADORES_SALIDA
      SELECT   'Dolar OBS.',@DOLAR1,@DOLAR2

      SELECT 'Indicadores'       AS InicioBloque
            ,Descripcion         AS Descripcion
            ,ISNULL(Valor1, '')  AS Valor1
            ,ISNULL(Valor2, '')  AS Valor2
      FROM   @INDICADORES_SALIDA

      --***********************************************************************************************

      -- Para buscar los IDs de Cliente, Empresa, Moneda y Tipo de cuenta
--      SELECT TOP 1   @ID_CLIENTE = id_cliente
--        FROM CSGPI.dbo.VIEW_CUENTAS_VIGENTES
--       WHERE rut_cliente = @RutCliente

       SELECT   @ID_CLIENTE = Cu.ID_CLIENTE
       FROM     CSGPI.dbo.CUENTAS AS Cu with (NOLOCK) INNER JOIN CSGPI.dbo.CLIENTES AS Cl with (NOLOCK)
                                              ON Cu.ID_CLIENTE = Cl.id_cliente
       WHERE    Cl.RUT_CLIENTE = @RutCliente
       AND      Cu.COD_ESTADO = 'H'

      --INSERT INTO @Cuentas EXEC CSGPI.dbo.PKG_CUENTAS$BuscarCuentasAPV @ID_CLIENTE, NULL
  --***nuevo
print @ID_CLIENTE
print @CuentaEntrada
  select @ID_CUENTA=ID_CUENTA
   from CSGPI.dbo.cuentas with (NOLOCK)
   where id_cliente =@ID_CLIENTE
      AND num_cuenta=@CuentaEntrada
declare @DICIMALES_MOSTRAR NUMERIC(2)
        select @DICIMALES_MOSTRAR = m.DICIMALES_MOSTRAR
          from csgpi.dbo.CUENTAS c,
               csgpi.dbo.MONEDAS m
         where c.id_CUENTA = @ID_CUENTA and
               c.ID_MONEDA= m.ID_MONEDA

-------------------------------------------------------------------------------------------------------------------------------------------------1
   INSERT INTO @Cuentas
   select *
   from CSGPI.dbo.cuentas with (NOLOCK)
   where id_cliente =@ID_CLIENTE
      AND num_cuenta=@CuentaEntrada
  --fin nuevo

       SELECT   TOP 1
                @ID_EMPRESA = ID_EMPRESA,
                @ID_MONEDA = ID_MONEDA,
                @ID_TIPOCUENTA = ID_TIPOCUENTA
       FROM     @Cuentas
print 'de aqui'
print @ID_EMPRESA
 print  @FechaHasta
 print @ID_CLIENTE
print @ID_MONEDA
print @Consolidado
print @ID_CUENTA
print 'a aqui'



      INSERT INTO @Arbol
         EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA_Consolidado NULL,@ID_EMPRESA,@FechaHasta,@ID_CLIENTE,NULL,@ID_MONEDA,@Consolidado


      SET @Cantidad = ISNULL((SELECT COUNT(ID_ARBOL_CLASE_INST_HOJA) FROM @Arbol), 0)

      SET @Cont = 1

      WHILE @Cont <= @Cantidad
      BEGIN

          SELECT @ID_ARBOL_CLASE_INST_HOJA = ID_ARBOL_CLASE_INST_HOJA
                ,@DSC_ARBOL_CLASE_INST = DSC_ARBOL_CLASE_INST
                ,@DSC_ARBOL_CLASE_INST_HOJA = DSC_ARBOL_CLASE_INST_HOJA
                ,@CODIGO = CODIGO
          FROM   @Arbol
          WHERE  ID_TABLA = @Cont
--print @ID_CUENTA
   --print @FechaHasta
   --print @ID_ARBOL_CLASE_INST_HOJA

  --IF (@CODIGO != 'FWD')
  --begin
         INSERT INTO @SaldosActivos
            --EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO_Consolidado NULL,@FechaHasta,@ID_ARBOL_CLASE_INST_HOJA,@ID_MONEDA,NULL,NULL,NULL,@ID_EMPRESA,@Consolidado,@ID_CLIENTE,NULL,@ID_TIPOCUENTA
   --SE COMENTA Y REEMPLAZA TEMPORALMENTE POR SP SOLO PARA WEB
   --exec  CSGPI.dbo.PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO @ID_CUENTA, @FechaHasta, @ID_ARBOL_CLASE_INST_HOJA,NULL,NULL,NULL,NULL
   exec  CSGPI.dbo.PKG_SALDOS_ACTIVOS_WEB$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO @ID_CUENTA, @FechaHasta, @ID_ARBOL_CLASE_INST_HOJA,NULL,NULL,NULL,NULL
   --print '***'

   --select* from @SaldosActivos
   --end

         SET @Cont = @Cont + 1

      END

--*** para saver cuenta

select @pId_Moneda_Salida= ID_MONEDA from CSGPI.dbo.CUENTAS with (NOLOCK) where ID_CUENTA=@ID_CUENTA
set @Decimales =(select dicimales_mostrar from CSGPI.dbo.monedas with (NOLOCK) where id_moneda =@pId_Moneda_Salida)
--print convert(varchar,@pId_Moneda_Salida)
--***
  --***********************************************************************************************
      -- FORWARDS
      --***********************************************************************************************
 --SELECT * FROM @SaldosActivos

 INSERT INTO @Forwards   (
                         origen               ,
       TipoInstrumento    ,
       NumeroContrato      ,
       MonedaEmision    ,
       Modalidad       ,
       UnidadActivo    ,
       CantidadNominal      , --INT,
       FechaInicial  ,
       FechaFinal     ,
       PrecioPactado        ,
       Resultado     )

                           SELECT
                                  'Forwards',
           TIPO_FWD,
          NUMERO_CONTRATO,
                                  MONEDA_EMISION,
          MODALIDAD,
          UNIDAD_ACTIVO_SUBYACENTE,
          VALOR_NOMINAL,
          FECHA_INICIO,
          FECHA_VENCIMIENTO,
          PRECIO_PACTADO,
          VALOR_MERCADO
                           FROM   @SaldosActivos
                           WHERE  ORIGEN = 'FORWARDS'

--***********************************************************************************************
      -- RENTA VARIABLE INTERNACIONAL
      --***********************************************************************************************
 --select * from @SaldosActivos

    INSERT INTO @RentaVariableInternacional (origen,TipoInstrumento,
          Nemotecnico,
          Moneda,
                                  PorcentajeInversion,
          Cantidad,
          PrecioCompra,
                                  MontoCompra,
          PrecioActual,
          ValorMercado,
          Monto_mon_origen,
                                  Rentabilidad ,
                                  cod_producto, --07-02-2014
          CODIGO,
          MontoInvertido       ,
         UtilidadPerdida   ,
         ValorizacionUSD   ,
         ValorizacionPeso
         ) --10-02-2014
                           SELECT origen,dsc_arbol_clase_inst,
           nemotecnico  AS nemotecnico,
          SIMBOLO_MONEDA,
                                  porcentaje_rama,
          cantidad,
          precio_compra,
                                  round(monto_promedio_compra,0),
          precio     ,
            VALOR_MERCADO    as   monto_mon_cta,
          Monto_mon_origen,
                                  rentabilidad,
                                  cod_producto, --07-02-2014
          CODIGO ,
             MONTO_INVERTIDO   as MontoInvertido,
             UTILIDAD_PERDIDA   as UtilidadPerdida,
             MONTO_MON_USD   as ValorizacionUSD,
          Round(MONTO_MON_CTA,@DICIMALES_MOSTRAR)  as ValorizacionPeso

             --10-02-2014
                           FROM   @SaldosActivos
                           WHERE  origen in ('NOTAS_NAC','NOTAS_INT')

     --***********************************************************************************************
      -- otros activos Finv
      --***********************************************************************************************


                 INSERT INTO @RentaVariableInternacional (origen,TipoInstrumento,
          Nemotecnico,
          Moneda,
                                  PorcentajeInversion,
          Cantidad,
          PrecioCompra,
                                  MontoCompra,
          PrecioActual,
          ValorMercado,
          Monto_mon_origen,
                                  Rentabilidad ,
                                  cod_producto, --07-02-2014
          CODIGO,
          MontoInvertido       ,
         UtilidadPerdida   ,
         ValorizacionUSD   ,
         ValorizacionPeso
         ) --10-02-2014
                           SELECT origen,dsc_arbol_clase_inst,
           nemotecnico  AS nemotecnico,
          SIMBOLO_MONEDA,
                                  porcentaje_rama,
          cantidad,
          precio_compra,
                                  round(monto_promedio_compra,0),
          precio     ,
            VALOR_MERCADO    as   monto_mon_cta,
          Monto_mon_origen,
                                  rentabilidad,
                                  cod_producto, --07-02-2014
          CODIGO ,
             MONTO_INVERTIDO   as MontoInvertido,
             UTILIDAD_PERDIDA   as UtilidadPerdida,
             MONTO_MON_USD   as ValorizacionUSD,
          Round(MONTO_MON_CTA,@DICIMALES_MOSTRAR)   as ValorizacionPeso
             --10-02-2014
                           FROM   @SaldosActivos
                           WHERE  COD_INSTRUMENTO ='ACCIONES_NAC'
                           and DSC_ARBOL_CLASE_INST='Fondos de Inversión'







      --***********************************************************************************************
      -- RENTA VARIABLE NACIONAL
      --***********************************************************************************************
--PRINT @pId_Moneda_Salida

     INSERT INTO @RentaVariable (origen,TipoInstrumento,
          Nemotecnico,
          Moneda,
                                  PorcentajeInversion,
          Cantidad,
          PrecioCompra,
                                  MontoCompra,
          PrecioActual,
          ValorMercado,
          Monto_mon_origen,
                                  Rentabilidad ,
                                  cod_producto, --07-02-2014
          CODIGO) --10-02-2014
                           SELECT origen,dsc_arbol_clase_inst,
          CASE codigo
           WHEN 'FFMM' THEN dsc_nemotecnico
           ELSE nemotecnico
         END   AS nemotecnico,
          SIMBOLO_MONEDA,
                                  porcentaje_rama,
          cantidad,
          precio_promedio_compra,
          monto_promedio_compra,--ROUND(monto_promedio_compra,0),
          precio     ,
          monto_mon_cta, --ROUND(monto_mon_cta,0), 14-04-2014
          CASE CODIGO WHEN 'FFMM' THEN  MONTO_VALOR_COMPRA ELSE Monto_mon_origen END, --10/11/2017
                                  rentabilidad,
                                  cod_producto, --07-02-2014
          CODIGO --10-02-2014
                           FROM   @SaldosActivos
                           WHERE  dsc_padre_arbol_clase_inst in ('RENTA VARIABLE','Renta Variable Nacional')


      INSERT INTO @RentaVariable (origen, TipoInstrumento,
                                  PorcentajeInversion,
                                  MontoCompra,
                                  ValorMercado)
                          SELECT  origen,dsc_arbol_clase_inst,
                                  100,
                                  SUM(ROUND(monto_promedio_compra,2)),
                                  round(SUM(monto_mon_cta),2)--SUM(round(monto_mon_cta,2)) 14-04-2014
          --round(sum(monto_mon_cta),0) --24-01-2014
                          FROM    @SaldosActivos
                          WHERE   dsc_padre_arbol_clase_inst in ('RENTA VARIABLE','Renta Variable Nacional')
                          GROUP   BY dsc_arbol_clase_inst,origen
-------------------------------------------------------------------------------------------------------------------------------------------------2

       SELECT  origen, 'RentaVariable'      AS InicioBloque,
      TipoInstrumento,
               Nemotecnico = ISNULL(Nemotecnico, ''),
               Moneda = ISNULL(Moneda,''),
               PorcentajeInversion,
               Cantidad,
               PrecioCompra,
               CASE
               when ISNULL(Moneda,'') ='US$' then round(MontoCompra,2)
               else round(MontoCompra,0) end
               MontoCompra,
               PrecioActual,
               round(ValorMercado,2) AS ValorMercado, --14-04-2014
      round(Monto_mon_origen,2) AS Monto_mon_origen, --nuevo 14-04-2014
               Rentabilidad,
               cod_producto,   --07-02-2014
      CODIGO --10-02-2014
           FROM @RentaVariable
         ORDER BY TipoInstrumento, ID_TABLA

--***********************************************************************************************
      -- RV Otros Activos Finv
      --***********************************************************************************************

 INSERT INTO @RVOtrosActivosFinv (origen,TipoInstrumento,
          Nemotecnico,
          Moneda,
                                  PorcentajeInversion,
          Cantidad,
          PrecioCompra,
                                  MontoCompra,
          PrecioActual,
          ValorMercado,
          Monto_mon_origen,
                                  Rentabilidad ,
                                  cod_producto, --07-02-2014
          CODIGO) --10-02-2014
                           SELECT origen,dsc_arbol_clase_inst,
            nemotecnico  nemotecnico,
          SIMBOLO_MONEDA,
                                  porcentaje_rama,
          cantidad,
          precio_promedio_compra,
          monto_promedio_compra,--ROUND(monto_promedio_compra,0),
          precio     ,
          monto_mon_cta, --ROUND(monto_mon_cta,0), 14-04-2014
          Monto_mon_origen, --ROUND(Monto_mon_origen,0), 14-04-2014
                                  rentabilidad,
                                  cod_producto, --07-02-2014
          CODIGO --10-02-2014
                           FROM   @SaldosActivos
                           WHERE  CODIGO in ('FM_OA')



  --***********************************************************************************************
      -- RENTA VARIABLE INTERNACIONAL
      --***********************************************************************************************
 -- select * from @SaldosActivos

    INSERT INTO @RentaVariableInternacional (origen,TipoInstrumento,
          Nemotecnico,
          Moneda,
                                  PorcentajeInversion,
          Cantidad,
          PrecioCompra,
                                  MontoCompra,
          PrecioActual,
          ValorMercado,
          Monto_mon_origen,
                                  Rentabilidad ,
                                  cod_producto, --07-02-2014
          CODIGO,
          MontoInvertido       ,
         UtilidadPerdida   ,
         ValorizacionUSD   ,
         ValorizacionPeso
         ) --10-02-2014
                           SELECT origen,dsc_arbol_clase_inst,
          CASE codigo
          WHEN 'FFMM'  THEN dsc_nemotecnico
           WHEN 'FM_INT'  THEN dsc_nemotecnico
           WHEN 'RF_INT'  THEN dsc_nemotecnico
           WHEN 'RVINT'  THEN dsc_nemotecnico
          ELSE nemotecnico
        END   AS nemotecnico,
          SIMBOLO_MONEDA,
                                  porcentaje_rama,
          cantidad,
          precio_promedio_compra,
          CASE origen
          WHEN 'PERSHING' THEN ROUND(MONTO_INVERTIDO,2)
          ELSE round(monto_promedio_compra,0)
          END,
          precio,
          case
         when origen = 'NACIONAL' then  monto_mon_cta
         when origen = 'INTERNACIONAL' OR origen = 'PERSHING' then VALOR_MERCADO
         end
             as
          monto_mon_cta,
          Monto_mon_origen,
                                  rentabilidad,
                                  cod_producto, --07-02-2014
          CODIGO ,

          case
         when origen = 'NACIONAL' then  0
         when origen = 'INTERNACIONAL' OR origen = 'PERSHING' then  MONTO_INVERTIDO
         end
             as MontoInvertido,
          case
         when origen = 'NACIONAL' then  0
         when origen = 'INTERNACIONAL' OR origen = 'PERSHING' then  UTILIDAD_PERDIDA
         end
             as UtilidadPerdida,
          case
         when origen = 'NACIONAL' then  0
         when origen = 'INTERNACIONAL' OR origen = 'PERSHING' then  MONTO_MON_USD
         end
             as ValorizacionUSD,

          case
         when origen = 'NACIONAL' then  0
         when origen = 'INTERNACIONAL' OR origen = 'PERSHING' then  round(monto_mon_cta,@DICIMALES_MOSTRAR)--MONTO_MON_CTA
         end
             as ValorizacionPeso
             --10-02-2014
                           FROM   @SaldosActivos
                           WHERE  dsc_padre_arbol_clase_inst in ('RENTA VARIABLE','Renta Variable Internacional')

      INSERT INTO @RentaVariableInternacional (origen,TipoInstrumento,
                                  PorcentajeInversion,
                                  MontoCompra,
                                  ValorMercado)
                          SELECT  origen,dsc_arbol_clase_inst,
                                  100,
                                  CASE origen
                                  WHEN 'PERSHING' THEN SUM(MONTO_MON_USD)--ROUND(MONTO_INVERTIDO,2)
                                  ELSE SUM(monto_promedio_compra)
                                  END,
                                  SUM(round(monto_mon_cta,@DICIMALES_MOSTRAR))
          --round(sum(monto_mon_cta),0) --24-01-2014
                          FROM    @SaldosActivos
                          WHERE   dsc_padre_arbol_clase_inst in ('RENTA VARIABLE','Renta Variable Internacional')
          or cod_producto in ('RV_INT')

                          GROUP   BY dsc_arbol_clase_inst,origen

-------------------------------------------------------------------------------------------------------------------------------------------------3


       SELECT  origen, 'RentaVariableInter'      AS InicioBloque,
               TipoInstrumento,
               Nemotecnico = ISNULL(Nemotecnico, ''),
               Moneda = ISNULL(Moneda,''),
               PorcentajeInversion,
               dbo.fn_CC_IncluirMilesCartola(Cantidad,4) as Cantidad,
               dbo.fn_CC_IncluirMilesCartola(PrecioCompra,4) as PrecioCompra,
               dbo.fn_CC_IncluirMilesCartola(MontoCompra,4) as MontoCompra,
               dbo.fn_CC_IncluirMilesCartola(PrecioActual,4) as PrecioActual,
               dbo.fn_CC_IncluirMilesCartola(ValorMercado,2) as ValorMercado,
      dbo.fn_CC_IncluirMilesCartola(Monto_mon_origen,2) as Monto_mon_origen, --nuevo
               dbo.fn_CC_IncluirMilesCartola(Rentabilidad,2) as  Rentabilidad,
               cod_producto,   --07-02-2014
      CODIGO ,
      MontoInvertido,
      UtilidadPerdida,
      ValorizacionUSD,
      ValorizacionPeso
      --10-02-2014
           FROM @RentaVariableInternacional
         ORDER BY TipoInstrumento, ID_TABLA


--SELECT * FROM   @SaldosActivos


   --***********************************************************************************************
      -- RENTA VARIABLE POR SECTORES ECONOMICOS
      --***********************************************************************************************


  INSERT INTO @TMP
  EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA @ID_CUENTA, @ID_EMPRESA, @FechaHasta

     SELECT @ID_ARBOL_SEC_ECONOMICO = ID_ARBOL_CLASE_INST_HOJA from  @TMP
        WHERE DSC_ARBOL_CLASE_INST= 'Renta Variable Nacional' AND DSC_ARBOL_CLASE_INST_HOJA='Acciones'

  INSERT INTO @RvSector
  EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$BUSCAR_RV_SECTOR @ID_CUENTA,@FechaHasta,@ID_ARBOL_SEC_ECONOMICO,@ID_MONEDA


--        INSERT INTO @RvSector
--         EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$BUSCAR_RV_SECTOR_consolidado NULL,@FechaHasta,'39',@ID_MONEDA,@ID_CLIENTE,@Consolidado,@ID_EMPRESA

      -- Genera parametros del gráfico
      SET @VECNOM = ''
      SET @VECMTO = ''
      SET @CANTIDAD = ISNULL((SELECT COUNT(ID_TABLA) FROM @RvSector),0)
      SET @CONT = 1

      WHILE @CONT <= @CANTIDAD
      BEGIN
         SET @VECNOM = @VECNOM + ISNULL((SELECT DSC_SECTOR FROM @RvSector WHERE ID_TABLA = @CONT),'') + ' ' +  REPLACE(ISNULL(CAST(CAST(((SELECT PORC_SECTOR FROM @RvSector WHERE ID_TABLA = @CONT)*100)as decimal(6,2)) AS VARCHAR(MAX)),''),'.',',') + '%' +

 ';'
         SET @VECMTO = @VECMTO + REPLACE(ISNULL(CAST(((SELECT PORC_SECTOR FROM @RvSector WHERE ID_TABLA = @CONT)*100) AS VARCHAR(MAX)),''),'.',',') + ';'

         SET @CONT = @CONT + 1
      END

      -- Elimina el último ;
      IF LEN(@VECNOM) > 1
      BEGIN
         SET @VECNOM = SUBSTRING(@VECNOM,1,LEN(@VECNOM)-1)
         SET @VECMTO = SUBSTRING(@VECMTO,1,LEN(@VECMTO)-1)
      END

      -- Inserta Total
      IF EXISTS(SELECT * FROM @RvSector)
         INSERT INTO @RvSector (
            DSC_SECTOR,
            MONTO_MON_CTA
         )
         SELECT   'Total',SUM(MONTO_MON_CTA) FROM @RvSector
-------------------------------------------------------------------------------------------------------------------------------------------------4

      -- Imprime resultados
       SELECT   'SectoresEconomicos'      AS InicioBloque,
            DSC_SECTOR               AS SectoresEconomicos,
            MONTO_MON_CTA            AS Monto
         FROM @RvSector

      IF @VECNOM <> ''
          SELECT   'Grafico'               AS InicioBloque,
               @VECNOM                  AS VECNOM,
               @VECMTO                  AS VECMTO
      ELSE
         SELECT   ''               AS InicioBloque,
         @VECNOM                  AS VECNOM,
         @VECMTO                  AS VECMTO

      --***********************************************************************************************
      -- RENTA FIJA NACIONAL BONOS
      --***********************************************************************************************

      INSERT INTO @RentaFija (origen,
            TipoInstrumento,
            Nemotecnico,
            Emisor,
            Moneda,
            PorcentajeInversion,
            Nominales,
            TasaCupon,
            FechaVencimiento,
            TasaCompra,
            TasaMercado,
            Valorizacion,
   cod_producto, --07-02-2014
   duration, --10-02-2014
   CODIGO) --10-02-2014
       SELECT origen,
            dsc_arbol_clase_inst,
   CASE codigo
      WHEN 'FFMM' THEN dsc_nemotecnico
      ELSE nemotecnico
    END   AS nemotecnico,

            cod_emisor,--EMISOR,
            SIMBOLO_MONEDA,
            CAST(ROUND(porcentaje_rama,2) AS NUMERIC(6,2)),
            cantidad,
            CAST(tasa_emision AS VARCHAR(10)),
            CONVERT(VARCHAR(8),isnull(FECHA_VENCIMIENTO,''),112),
            CAST(tasa_compra AS NUMERIC(18,4)),
            CAST(tasa AS NUMERIC(6,2)),
            CAST(monto_mon_cta AS BIGINT),
            cod_producto, --07-02-2014
            duration,      --10-02-2014
            CODIGO --10-02-2014
         FROM @SaldosActivos
        WHERE dsc_padre_arbol_clase_inst in ('RENTA FIJA','Renta Fija Nacional')




      INSERT INTO @RentaFija (origen,
            TipoInstrumento,
            PorcentajeInversion,
            TasaCompra,
            Valorizacion)
       SELECT origen,
            dsc_arbol_clase_inst,
            100,
            SUM(CAST(tasa_compra AS NUMERIC(18,4))),
            SUM(CAST(monto_mon_cta AS BIGINT))
         FROM @SaldosActivos
        WHERE dsc_padre_arbol_clase_inst in ('RENTA FIJA','Renta Fija Nacional')
      GROUP BY dsc_arbol_clase_inst,origen
-------------------------------------------------------------------------------------------------------------------------------------------------5


       SELECT  origen, 'RentaFija'      AS InicioBloque,
            TipoInstrumento,
            Nemotecnico = ISNULL(Nemotecnico, ''),
            Emisor = ISNULL(Emisor, ''),
            Moneda = ISNULL(Moneda, ''),
            PorcentajeInversion,
            Nominales,
            TasaCupon,
            FechaVencimiento,
            TasaCompra,
            TasaMercado,
            Valorizacion,
          cod_producto, --07-02-2014
            CAST(duration  AS NUMERIC(6,4)) as duration,--10-02-2014
   CODIGO --10-02-2014
         FROM @RentaFija
      ORDER BY TipoInstrumento, ID_TABLA

   --***********************************************************************************************
      -- RENTA FIJA INTERNACIONAL BONOS
      --***********************************************************************************************
   INSERT INTO @RentaFijaInternacional ( origen,
            TipoInstrumento,
            Nemotecnico,
            Emisor,
            Moneda,
            PorcentajeInversion,
            Nominales,
            TasaCupon,
            FechaVencimiento,
            TasaCompra,
            TasaMercado,
            Valorizacion,
   cod_producto, --07-02-2014
   duration, --10-02-2014
   CODIGO) --10-02-2014
       SELECT origen,
            dsc_arbol_clase_inst,
            CASE codigo
      WHEN 'FFMM' THEN dsc_nemotecnico
      ELSE nemotecnico
   END   AS nemotecnico,
            cod_emisor,--EMISOR,
            SIMBOLO_MONEDA,
            CAST(porcentaje_rama AS NUMERIC(6,2)),
            cantidad,
            CAST(tasa_emision AS VARCHAR(10)),
            CONVERT(VARCHAR(8),isnull(FECHA_VENCIMIENTO,''),112),
            CAST(tasa_compra AS NUMERIC(18,4)),
            CAST(tasa AS NUMERIC(6,2)),
    --        case
    --when origen = 'NACIONAL' then  monto_mon_cta
    --when origen = 'INTRNACIONAL' then  MONTO_INVERTIDO
    --end
    --                   as
            monto_mon_cta,
            cod_producto, --07-02-2014
            duration,      --10-02-2014
            CODIGO --10-02-2014
         FROM @SaldosActivos
        WHERE dsc_padre_arbol_clase_inst in ('Renta Fija Internacional') and origen='NACIONAL'

      INSERT INTO @RentaFijaInternacional ( origen,
            TipoInstrumento,
            PorcentajeInversion,
            TasaCompra,
            Valorizacion)
       SELECT origen,
            dsc_arbol_clase_inst,
            100,
            SUM(CAST(tasa_compra AS NUMERIC(18,4))),
            SUM(round(monto_mon_cta,@DICIMALES_MOSTRAR))
            --SUM(monto_mon_cta)
         FROM @SaldosActivos
        WHERE dsc_padre_arbol_clase_inst in ('Renta Fija Internacional') and origen='NACIONAL'
      GROUP BY dsc_arbol_clase_inst,origen

-------------------------------------------------------------------------------------------------------------------------------------------------6

       SELECT origen,  'RentaFijaIntBonos'      AS InicioBloque,
            TipoInstrumento,
            Nemotecnico = ISNULL(Nemotecnico, ''),
            Emisor = ISNULL(Emisor, ''),
            Moneda = ISNULL(Moneda, ''),
            dbo.fn_CC_IncluirMilesCartola(PorcentajeInversion,2) as PorcentajeInversion,
            dbo.fn_CC_IncluirMilesCartola(Nominales,4) as Nominales,
            TasaCupon as TasaCupon,
            FechaVencimiento,
            dbo.fn_CC_IncluirMilesCartola(TasaCompra,4) as TasaCompra,
            dbo.fn_CC_IncluirMilesCartola(TasaMercado,4) as  TasaMercado,
            dbo.fn_CC_IncluirMilesCartola(Valorizacion,2) as Valorizacion,
            cod_producto, --07-02-2014
            dbo.fn_CC_IncluirMilesCartola(duration,4) as duration,--10-02-2014
   CODIGO --10-02-2014

         FROM @RentaFijaInternacional
      ORDER BY TipoInstrumento, ID_TABLA



 --***********************************************************************************************
      -- RENTA FIJA NACIONAL POR FONDOS MUTUOS CORTO , LARGO PLAZO NACIONAL
      --***********************************************************************************************

   INSERT INTO @RentaFijaFondos (origen, TipoInstrumento,
      Nemotecnico,
          Moneda,
                                  PorcentajeInversion,
          Cantidad,
          PrecioCompra,
                                  MontoCompra,
          PrecioActual,
          ValorMercado,
          Monto_mon_origen,
          monto_mon_nemotecnico, --23-01-2014
                                  Rentabilidad,
          cod_producto, --07-02-2014
          duration, --10-02-2014
          CODIGO) --10-02-2014
                           SELECT origen, dsc_arbol_clase_inst,
          CASE codigo
         WHEN 'FFMM' THEN dsc_nemotecnico
         ELSE nemotecnico
          END   AS nemotecnico,
          SIMBOLO_MONEDA,
                                  porcentaje_rama,
          cantidad,
          precio_promedio_compra,
          monto_promedio_compra, --ROUND(monto_promedio_compra,0),
          precio,
          monto_mon_cta, --ROUND(monto_mon_cta,0),
          Monto_mon_origen,
                                  monto_mon_nemotecnico,  --23-01-2014
                                  rentabilidad,
          cod_producto, --07-02-2014
                                  duration,     --10-02-2014
          CODIGO --10-02-2014
                           FROM   @SaldosActivos
                           WHERE  dsc_padre_arbol_clase_inst in ('Renta Fija Nacional')

      INSERT INTO @RentaFijaFondos (origen, TipoInstrumento,
                                  PorcentajeInversion,
                                  MontoCompra,
                                  ValorMercado)
                          SELECT  origen, dsc_arbol_clase_inst,
                                  100,
                                  SUM(monto_promedio_compra), --SUM(ROUND(monto_promedio_compra,0)),
                                  SUM(monto_mon_cta) --SUM(round(monto_mon_cta,0))--round(SUM(monto_mon_cta),0) --24-01-204 SUM(monto_mon_cta)
                          FROM    @SaldosActivos
                          WHERE   dsc_padre_arbol_clase_inst in ('Renta Fija Nacional')
                          GROUP   BY dsc_arbol_clase_inst,origen
-------------------------------------------------------------------------------------------------------------------------------------------------7


 SELECT   origen,'RentaFijaFondos'      AS InicioBloque,
               TipoInstrumento,
               Nemotecnico = ISNULL(Nemotecnico, ''),
               Moneda = ISNULL(Moneda,''),
               PorcentajeInversion,
               Cantidad,
               PrecioCompra,
               MontoCompra,
               PrecioActual,
               dbo.fn_CC_IncluirMilesCartola(ValorMercado,@Decimales) as ValorMercado,
               ---23-01-2014
              CASE Moneda
      WHEN 'UF' THEN monto_mon_nemotecnico
      ELSE Monto_mon_origen
    END   AS Monto_mon_origen,
               ---
     -- Monto_mon_origen, --nuevo
               Rentabilidad,
      cod_producto,
      duration,
    CODIGO --10-02-2014
           FROM @RentaFijaFondos
         ORDER BY TipoInstrumento, ID_TABLA



--***********************************************************************************************
      -- RENTA FIJA INTERNACIONAL POR FONDOS MUTUOS, ETFs
--***********************************************************************************************
INSERT INTO @RentaFijaFondosInter (origen, TipoInstrumento,
          Nemotecnico,
          Moneda,
                                  PorcentajeInversion,
          Cantidad,
          PrecioCompra,
                                  MontoCompra,
          PrecioActual,
          ValorMercado,
          Monto_mon_origen,
          monto_mon_nemotecnico, --23-01-2014
                                  Rentabilidad,
          cod_producto, --07-02-2014
          duration, --10-02-2014
          CODIGO,
         MontoInvertido   ,
         UtilidadPerdida  ,
         ValorizacionUSD  ,
         ValorizacionPeso
          ) --10-02-2014
                           SELECT origen, dsc_arbol_clase_inst,
          CASE codigo
           WHEN 'FFMM'  THEN dsc_nemotecnico
           WHEN 'FM_INT'  THEN dsc_nemotecnico
           WHEN 'RFINT'  THEN dsc_nemotecnico
           WHEN 'RVINT'  THEN dsc_nemotecnico
           ELSE nemotecnico
          END   AS nemotecnico,
          SIMBOLO_MONEDA,
         --                          case
         --when origen = 'NACIONAL' then  porcentaje_rama
         --when origen = 'INTERNACIONAL' then  MONTO_MON_USD
         --end
         --    as
          porcentaje_rama,
          cantidad,
          precio_promedio_compra,
         --  case
         --when origen = 'NACIONAL' then  monto_promedio_compra
         --when origen = 'INTERNACIONAL' then  MONTO_INVERTIDO
         --end
         --    as
          /*case
         when origen = 'NACIONAL' then  monto_promedio_compra
         when origen = 'INTERNACIONAL' then  MONTO_MON_CTA
         end
             as   */
         CASE
         WHEN ORIGEN = 'PERSHING' THEN MONTO_INVERTIDO
         ELSE monto_promedio_compra
         END,
             --monto_promedio_compra, --ROUND(monto_promedio_compra,0),
          precio,
           VALOR_MERCADO
           /*case
         when origen = 'NACIONAL' then  monto_mon_cta
         when origen = 'INTERNACIONAL' then  VALOR_MERCADO
         end
             as   */

          monto_mon_cta, --ROUND(monto_mon_cta,0),
         -- case
         --when origen = 'NACIONAL' then  Monto_mon_origen
         --when origen = 'INTERNACIONAL' then  MONTO_INVERTIDO
         --end
         --    as
         Monto_mon_origen,
                                  monto_mon_nemotecnico,  --23-01-2014
         --                         case
         --when origen = 'NACIONAL' then  rentabilidad
         --when origen = 'INTERNACIONAL' then  UTILIDAD_PERDIDA
         --end
         --    as
         rentabilidad,
          cod_producto, --07-02-2014
                                  duration,     --10-02-2014
          CODIGO, --10-02-2014
          case
         when origen = 'NACIONAL' then  0
         when origen = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' then  MONTO_INVERTIDO
         end
             as MontoInvertido,
          case
         when origen = 'NACIONAL' then  0
         when origen = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' then  UTILIDAD_PERDIDA
         end
             as UtilidadPerdida,
          case
         when origen = 'NACIONAL' then  0
         when origen = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' then  MONTO_MON_USD
         end
             as ValorizacionUSD,

          case
         when origen = 'NACIONAL' then  0
         when origen = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' then  Round(MONTO_MON_CTA,@DICIMALES_MOSTRAR)
         end
             as ValorizacionPeso

                           FROM   @SaldosActivos
                           WHERE  dsc_padre_arbol_clase_inst in ('Renta Fija Internacional') and origen <> 'NACIONAL'

      INSERT INTO @RentaFijaFondosInter (origen, TipoInstrumento,
                                  PorcentajeInversion,
                          MontoCompra,
                                  ValorMercado)
                          SELECT  origen, dsc_arbol_clase_inst,
                                  100,
                                  CASE ORIGEN                                      WHEN 'PERSHING' THEN SUM(MONTO_MON_USD)
                                  ELSE SUM(monto_promedio_compra)
                                  END, --SUM(ROUND(monto_promedio_compra,0)),
                                  CASE ORIGEN
                                  WHEN 'PERSHING' THEN SUM( Round(MONTO_MON_CTA,@DICIMALES_MOSTRAR) )
                                  ELSE sum(VALOR_MERCADO)
                                  END
                                  --SUM(monto_mon_cta) --SUM(round(monto_mon_cta,0))--round(SUM(monto_mon_cta),0) --24-01-204 SUM(monto_mon_cta)
                          FROM    @SaldosActivos
                          WHERE   dsc_padre_arbol_clase_inst in ('Renta Fija Internacional') and origen <> 'NACIONAL'
                          GROUP   BY dsc_arbol_clase_inst,origen


-------------------------------------------------------------------------------------------------------------------------------------------------8



 SELECT   origen,'RentaFijaFondosInter'      AS InicioBloque,
               TipoInstrumento,
               Nemotecnico = ISNULL(Nemotecnico, ''),
               Moneda = ISNULL(Moneda,''),
               dbo.fn_CC_IncluirMilesCartola(PorcentajeInversion,2) as PorcentajeInversion,
               dbo.fn_CC_IncluirMilesCartola(Cantidad,4) AS Cantidad,
               dbo.fn_CC_IncluirMilesCartola(PrecioCompra,4) AS PrecioCompra,
               dbo.fn_CC_IncluirMilesCartola(MontoCompra,4) as  MontoCompra,
               dbo.fn_CC_IncluirMilesCartola(PrecioActual,4) as PrecioActual,
               dbo.fn_CC_IncluirMilesCartola(ValorMercado,2) as ValorMercado,
               --dbo.fn_CC_IncluirMilesCartola(ValorMercado,@Decimales) as ValorMercado,
               ---23-01-2014
              CASE Moneda
      WHEN 'UF' THEN dbo.fn_CC_IncluirMilesCartola(monto_mon_nemotecnico,@Decimales)
      ELSE dbo.fn_CC_IncluirMilesCartola(Monto_mon_origen,@Decimales)
    END   AS Monto_mon_origen,
               ---
     -- Monto_mon_origen, --nuevo
               dbo.fn_CC_IncluirMilesCartola(Rentabilidad,2) as Rentabilidad,
      cod_producto,
      dbo.fn_CC_IncluirMilesCartola(duration,4) as  duration,
    CODIGO, --10-02-2014
    MontoInvertido        ,
       UtilidadPerdida         ,
       ValorizacionUSD        ,
       ValorizacionPeso
           FROM @RentaFijaFondosInter
         ORDER BY TipoInstrumento, ID_TABLA


 --fondos mutuos mixtos

 INSERT INTO @RentaFondosMixtos (origen, TipoInstrumento,
          Nemotecnico,
          Moneda,
                                  PorcentajeInversion,
          Cantidad,
          PrecioCompra,
                                  MontoCompra,
          PrecioActual,
          ValorMercado,
          Monto_mon_origen,
                                  Rentabilidad,
          cod_producto, --07-02-2014
          CODIGO) --10-02-2014
                           SELECT origen, dsc_arbol_clase_inst,
          CASE codigo
           WHEN 'FFMM' THEN dsc_nemotecnico
           ELSE nemotecnico
             END   AS nemotecnico,
          SIMBOLO_MONEDA,
                                  porcentaje_rama,
          cantidad,
          precio_promedio_compra,
                                  ROUND(monto_promedio_compra,0),
          precio     ,
          ROUND(monto_mon_cta,0),
          Monto_mon_origen,
                                  rentabilidad,
          cod_producto,    --07-02-2014
          CODIGO --10-02-2014
                           FROM   @SaldosActivos
                           WHERE  dsc_padre_arbol_clase_inst in ('FONDOS MUTUOS MIXTOS','Otros Activos')
          --and DSC_ARBOL_CLASE_INST_HOJA not in ('Forwards','Derivados')
   and CODIGO='FFMM'

      INSERT INTO @RentaFondosMixtos (origen, TipoInstrumento,
                                  PorcentajeInversion,
                                  MontoCompra,
                                  ValorMercado)
                          SELECT  origen, dsc_arbol_clase_inst,
                                  100,
                                  SUM(ROUND(monto_promedio_compra,0)),
                                  SUM(monto_mon_cta)
                          FROM    @SaldosActivos
                          WHERE   dsc_padre_arbol_clase_inst in ('FONDOS MUTUOS MIXTOS','Otros Activos')
          and CODIGO='FFMM'
         -- and DSC_ARBOL_CLASE_INST_HOJA not in ('Forwards','Derivados')
                          GROUP   BY dsc_arbol_clase_inst,origen
-------------------------------------------------------------------------------------------------------------------------------------------------9

       SELECT   origen,'RentaFondosMixtos'      AS InicioBloque,
               TipoInstrumento,
               Nemotecnico = ISNULL(Nemotecnico, ''),
               Moneda = ISNULL(Moneda,''),
               PorcentajeInversion,
               Cantidad,
               PrecioCompra,
               MontoCompra,
               PrecioActual,
               ValorMercado,
      Monto_mon_origen, --nuevo
               Rentabilidad,
      cod_producto,
      CODIGO --10-02-2014
           FROM @RentaFondosMixtos
         ORDER BY TipoInstrumento, ID_TABLA

      --***********************************************************************************************
      -- INFORMES DE CAJA
      --***********************************************************************************************
    INSERT INTO @CajasCuentas
   EXEC CSGPI.dbo.Pkg_Cajas_Cuenta$BuscarView @pId_Cuenta =@ID_CUENTA -- @IdCuenta

print '1'
  IF EXISTS(SELECT ID_CAJA_CUENTA FROM @CajasCuentas)
  BEGIN
   SET @CANTIDAD = ISNULL((SELECT COUNT(ID_TABLA) FROM @CajasCuentas), 0)
   SET @CONT = 1


   WHILE @CONT <= @CANTIDAD
   BEGIN
print '1.1'
    SELECT  @DESC_CAJA_CUENTA=DSC_CAJA_CUENTA --12-02-2014
       ,@ID_MONEDA = ID_MONEDA
      ,@ID_CAJA_CUENTA = ID_CAJA_CUENTA
    FROM @CajasCuentas
    WHERE ID_TABLA = @CONT
print '1.2'

select @fecha_desde_caja=CONVERT(VARCHAR(8), DATEADD(DAY,1,@FechaDesde), 112)
    EXEC CSGPI.dbo.Pkg_Saldos_Caja$Buscar_Saldos_Cuenta @SALDO OUTPUT, @ID_CUENTA, @ID_CAJA_CUENTA, @fecha_desde_caja
--print '1.3'
    INSERT INTO @InformesCajas (
     ID_OPERACION,
     FECHA_LIQUIDACION,
     DESCRIPCION,
     DESCRIPCION_CAJA,
     ID_MONEDA,
     MONTO_MOVTO_ABONO,
     MONTO_MOVTO_CARGO,
     SALDO
    )
    VALUES(0, CONVERT(DATETIME,@FechaDesde,112), 'SALDO ANTERIOR', 'SALDO ANTERIOR', @ID_MONEDA, NULL, NULL, @Saldo)
--print '1.4'

 --select @fecha_desde_caja=CONVERT(VARCHAR(8), DATEADD(DAY,1,@FechaDesde), 112)
    INSERT INTO @InformesCajas (
     ID_CIERRE,
     ID_OPERACION,
     FECHA_LIQUIDACION,
     DESCRIPCION,
     DESCRIPCION_CAJA,
     MONEDA,
     MONTO_MOVTO_ABONO,
     MONTO_MOVTO_CARGO

    )  EXEC CSGPI.dbo.PKG_MOVIMIENTOS_CAJA_SECURITY @ID_CAJA_CUENTA, @fecha_desde_caja  , @FechaHasta
print '1.5'
   --*****
   --update @InformesCajas set MONTO_MOVTO_ABONO = isnull(MONTO_MOVTO_ABONO,0),
   --         MONTO_MOVTO_CARGO = isnull(MONTO_MOVTO_CARGO,0)

   --*****

    --12-02-2014



    UPDATE @InformesCajas
    SET DSC_CAJA_CUENTA = @DESC_CAJA_CUENTA
    WHERE DSC_CAJA_CUENTA IS NULL
    --FIN

    UPDATE @InformesCajas
    SET ID_MONEDA = @ID_MONEDA
    WHERE ID_MONEDA IS NULL

    SET @CANTIDAD2 = ISNULL((SELECT COUNT(ID_TABLA) FROM @InformesCajas), 0)
    SET @CONT2 = 1
   --print '2'
    WHILE @CONT2 <= @CANTIDAD2
    BEGIN

     IF (SELECT MONTO_MOVTO_ABONO FROM @InformesCajas WHERE ID_TABLA = @CONT2) IS NOT NULL
     BEGIN
      SET @SALDO_ANTERIOR = ISNULL((SELECT SALDO FROM @InformesCajas WHERE ID_TABLA = (@CONT2 - 1)), 0)

      UPDATE @InformesCajas
      SET SALDO =  MONTO_MOVTO_ABONO - MONTO_MOVTO_CARGO + @SALDO_ANTERIOR
      WHERE ID_TABLA = @CONT2
      AND DSC_CAJA_CUENTA = @DESC_CAJA_CUENTA --NUEVO 12-02-2014
     END
     SET @CONT2 = @CONT2 + 1

    END


    EXEC CSGPI.dbo.Pkg_Saldos_Caja$Buscar_Saldos_Cuenta @SALDO OUTPUT, @ID_CUENTA, @ID_CAJA_CUENTA, @FechaHasta

          EXEC CSGPI.dbo.Pkg_Saldos_Caja$Buscar_Saldos_Cuenta @SALDO_PRIMER OUTPUT, @ID_CUENTA, @ID_CAJA_CUENTA, @FechaDesde
--print 'uno'
--print @SALDO
--print 'dos'


    INSERT INTO @InformesCajas (
     ID_OPERACION,
     FECHA_LIQUIDACION,
     DESCRIPCION,
     DESCRIPCION_CAJA,
     ID_MONEDA,
     MONTO_MOVTO_ABONO,
     MONTO_MOVTO_CARGO,
     SALDO,
     DSC_CAJA_CUENTA
    )
    SELECT 0,
      NULL,
      'TOTALES',
      'TOTALES',
      @ID_MONEDA,
      SUM(MONTO_MOVTO_ABONO),
      SUM(MONTO_MOVTO_CARGO),
      SUM(MONTO_MOVTO_ABONO)-SUM(MONTO_MOVTO_CARGO) + @SALDO_PRIMER,
      @DESC_CAJA_CUENTA
    FROM @InformesCajas
    WHERE ID_MONEDA =  @ID_MONEDA
       AND DSC_CAJA_CUENTA= @DESC_CAJA_CUENTA --12-04-2012

    SET @CONT = @CONT + 1

   END --fin WHILE @CONT <= @CANTIDAD

-------------------------------------------------------------------------------------------------------------------------------------------------10

   SELECT 'InformesCajas'        AS InicioBloque,
     /*CASE ID_MONEDA
      WHEN 1 THEN 'Caja Pesos'
      WHEN 2 THEN 'Caja Dolar'
      ELSE 'Caja Otros'
     END           AS TipoCaja,*/

     CASE DSC_CAJA_CUENTA
      WHEN 'CAJA PESO'  THEN 'Caja Pesos'
      WHEN 'CAJA PESOS' THEN 'Caja Pesos'
      WHEN 'Pesos'      THEN 'Caja Pesos'
      WHEN 'Peso'       THEN 'Caja Pesos'
      WHEN 'CAJA DOLAR' THEN 'Caja Dolar'
      WHEN 'Dólar'      THEN 'Caja Dolar'
      WHEN 'Dólar Internacional' THEN 'Caja Dolar Internacional'
      WHEN 'Dolar'      THEN 'Caja Dolar'
      WHEN 'Dolar Internacional' THEN 'Caja Dolar Internacional'
      ELSE 'Caja Otros'
     END           AS TipoCaja,



     ID_OPERACION        AS NumeroOperacion,
     CONVERT(VARCHAR(8), FECHA_LIQUIDACION, 112) AS FechaOperacion,
     DESCRIPCION       AS Detalle,
     --CAST(MONTO_MOVTO_ABONO AS BIGINT)   AS Ingreso,
     --CAST(MONTO_MOVTO_CARGO AS BIGINT)   AS Egreso,

     MONTO_MOVTO_ABONO   AS Ingreso,
     MONTO_MOVTO_CARGO    AS Egreso,

     CONVERT(NUMERIC(18,2),
     CASE ID_MONEDA
      WHEN 1 THEN ISNULL(ROUND(SALDO,0),0)
      WHEN 2 THEN ISNULL(ROUND(SALDO,2),0)
      ELSE '0'
     END )          AS Saldo,

     ID_MONEDA         AS TipoMoneda,
     DSC_CAJA_CUENTA        as dsc_caja_cuenta

   FROM @InformesCajas
   ORDER BY ID_MONEDA, ID_TABLA

  END --FIN IF INFORME CAJA


--**FIN PROBANDO

-------------------------------------------------------------------------------------------------------------------------------------------------11

         select 'Cuentas'         AS InicioBloque,
             num_cuenta              AS NumCuenta
         from @Cuentas

----------------------transacciones --------------------------------------
 --select @Consolidado ,@ID_CUENTA ,@FechaDesde,@FechaHasta


--SELECT  @Consolidado,@ID_CUENTA ,@FechaDesde,@FechaHasta
print @ID_CUENTA
print @fecha_desde_caja
print @FechaHasta
 INSERT INTO @transaccion
EXEC CSGPI.dbo.PKG_CARTOLA_WEB$DetalleMovimientos
'CTA' ,@ID_CUENTA ,@fecha_desde_caja,@FechaHasta,null

-------------------------------------------------------------------------------------------------------------------------------------------------12

select

CONVERT(VARCHAR(10),FechaOperacion,103) as fechaoperacion
,CONVERT(VARCHAR(10),fechaliquidacion,103) as fechaliquidacion
,descTipoMovimiento
,idOperacion
,factura
,nemotecnico
,cantidad
,moneda
,precio
,monto
,comision
,derechos
,gastos
,iva
 from @transaccion
 --------------------------FORWARDS---------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------13


       SELECT            ID_TABLA               ,
                         origen    ,
       TipoInstrumento   ,
       NumeroContrato ,
       MonedaEmision      ,
       Modalidad   ,
       UnidadActivo    ,
       CantidadNominal  , --INT,
       CONVERT(VARCHAR,FechaInicial, 103)as FechaInicial     ,
       CONVERT(VARCHAR,FechaFinal, 103)as FechaFinal     ,
       PrecioPactado       ,
       Resultado
           FROM @Forwards


      --***********************************************************************************************
-------------------------------------------------------------------------------------------------------------------------------------------------14

Select origen,TipoInstrumento,
    Nemotecnico,
    Moneda,
          PorcentajeInversion,
    Cantidad,
    PrecioCompra,
          MontoCompra,
    PrecioActual,
    ValorMercado,
    Monto_mon_origen,
          Rentabilidad ,
          cod_producto, --07-02-2014
    CODIGO

from @RVOtrosActivosFinv


   END TRY
   BEGIN CATCH
      SET @CodErr = @@ERROR
      SET @MsgErr = 'Error en el Procedimiento sp_CartolaApvFlexibleConsDetalleBal:' + ERROR_MESSAGE()
   END CATCH

END
GO
GRANT EXECUTE ON [sp_CartolaApvFlexibleConsDetalleBal_temp] TO DB_EXECUTESP
GO

