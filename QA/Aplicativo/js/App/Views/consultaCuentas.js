﻿var jsfechaConsulta;
var jsfechaConsultaHasta;
var jsfechaFormato;
var jsfechaFormatoHasta;

function resize() {
    var height;
    height = $("#content").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#Paginador").outerHeight(true) - 146;
    $("#ListaCuentas").setGridHeight(height);
}

function initConsultaCuentas() {
    $("#idSubtituloPaginaText").text("Consulta de Cuentas");
    document.title = "Consulta de Cuentas";

    cargarClientes();

    if (!miInformacionCuenta) {
        $("#dtFechaConsultaHasta").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
        $("#dtFechaConsulta").datepicker('setDate', moment(new Date(localStorage.UltimaFechaCierre)).add(-1, 'months').toDate());
    } else {
        $("#dtFechaConsultaHasta").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
        $("#dtFechaConsulta").datepicker('setDate', moment($("#dtFechaConsultaHasta").datepicker('getDate')).add(-1, 'months').format('L'));
    }

}
function cargarEventHandlersConsultaCuentas() {
    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $("#dtRangoFecha").datepicker();

    $("#ListaCuentas").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID_CUENTA" },
        colNames: ['Cuenta', 'Empresa', 'Fecha Contrato', 'Fecha Operativa', 'Fecha Cierre', 'Última Fecha Cierre Sistema', 'Nombre Cuenta', 'Cliente', 'Rut', 'Teléfono Cliente', 'Dirección Cliente',
            'E-mail Cliente', 'Estado Cliente', 'Estado Cuenta', 'Tipo Contrato', 'Bloqueo Cuenta', 'Nombre Asesor', 'Perfil de Riesgo', 'Moneda'],
        colModel: [
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 80, sorttype: "text", formatter: "text", editable: false, search: true, hidden: false, align: "right" },
            { name: "DSC_EMPRESA", index: "DSC_EMPRESA", sortable: true, width: 230, sorttype: "text", formatter: "text", editable: false, search: true, hidden: false },
            { name: "FECHA_CONTRATO", index: "FECHA_CONTRATO", sortable: true, width: 120, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "FECHA_OPERATIVA", index: "FECHA_OPERATIVA", sortable: true, width: 120, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "FECHA_CIERRE_CUENTA", index: "FECHA_CIERRE_CUENTA", sortable: true, width: 120, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "ULTIMA_FECHA_CIERRE_SISTEMA", index: "ULTIMA_FECHA_CIERRE_SISTEMA", sortable: true, width: 190, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "DSC_CUENTA", index: "DSC_CUENTA", sortable: true, width: 120, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NOMBRE_CLIENTE", index: "NOMBRE_CLIENTE", sortable: true, width: 240, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "RUT_CLIENTE", index: "RUT_CLIENTE", sortable: true, width: 100, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "FONO_CLIENTE", index: "FONO_CLIENTE", sortable: true, width: 130, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "DIRECCION_CLIENTE", index: "DIRECCION_CLIENTE", sortable: true, width: 300, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "EMAIL_CLIENTE", index: "EMAIL_CLIENTE", sortable: true, width: 130, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "ESTADO_CLIENTE", index: "ESTADO_CLIENTE", sortable: true, width: 110, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "ESTADO_CUENTA", index: "ESTADO_CUENTA", sortable: true, width: 110, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "DSC_TIPO_ADMINISTRACION", index: "DSC_TIPO_ADMINISTRACION", sortable: true, width: 110, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "BLOQUEO", index: "BLOQUEO", sortable: true, width: 120, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NOMBRE_ASESOR", index: "NOMBRE_ASESOR", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "DSC_PERFIL_RIESGO", index: "DSC_PERFIL_RIESGO", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "DSC_MONEDA", index: "DSC_MONEDA", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false }
        ],
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        sortname: "NUM_CUENTA",
        sortorder: "asc",
        caption: "Cuentas",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false,
        groupingView: {
            groupField: ['NUM_CUENTA'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        }
    });
    $("#ListaCuentas").jqGrid().bindKeys().scrollingRows = true;
    $("#ListaCuentas").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaCuentas").jqGrid('navGrid', '#Paginador', { add: false, edit: false, del: false, excel: false, search: false });
    $("#ListaCuentas").jqGrid('navButtonAdd', '#Paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaCuentas', 'Cuentas', 'Cuentas', '');
        }
    });

    $("#BtnConsultar").button().click(function () {
        consultaOperaciones();
    });
}

function consultaOperaciones() {
    var jsEstado = $("#DDEstado").val();
    var jsIdCliente = $("#DDCliente").val();
    var jsCreadas = $("#CBCreadas:checked").is(':checked') ? "S" : "N";
    var jsCerradas = $("#CBCerradas:checked").is(':checked') ? "S" : "N";
    jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaFormatoHasta = moment.utc($("#dtFechaConsultaHasta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1)
    jsfechaConsultaHasta = jsfechaFormatoHasta.substring(0, jsfechaFormatoHasta.length - 1)

    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }
    if (jsfechaConsultaHasta === "") {
        alert("No ha indicado fecha de consulta hasta");
        return;
    }

    $("#ListaCuentas").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaCuentas.aspx/ConsultaCuentas",
        data: "{'estado':" + (jsEstado == -1 ? null : "'" + jsEstado + "'") +
        ", 'idCliente':" + (jsIdCliente == -1 ? null : "'" + jsIdCliente + "'") +
        ", 'creadas':'" + jsCreadas +
        "', 'cerradas':'" + jsCerradas +
        "', 'fechaConsulta':'" + jsfechaConsulta +
        "', 'fechaConsultaHasta':'" + jsfechaConsultaHasta + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#ListaCuentas").setGridParam({ data: mydata });
                $("#ListaCuentas").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function cargarClientes() {
    $.ajax({
        url: "ConsultaCuentas.aspx/DatosClientes",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                $("#DDCliente").empty();
                $("#DDCliente").prepend('<option value="' + -1 + '">' + 'Todos' + '</option>');
                $.each(res, function (key, val) {
                    $("#DDCliente").append('<option value="' + val.ID_CLIENTE + '">' + val.NOM_CLIENTE + '</option>');
                });
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

$(document).ready(function () {
    initConsultaCuentas();
    cargarEventHandlersConsultaCuentas();
    $(window).trigger('resize');
});