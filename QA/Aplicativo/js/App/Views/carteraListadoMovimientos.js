﻿
var urlVarIdNemotecnico = getUrlVars()["idNemo"];
var urlVarDescripcion = getUrlVars()["Descripcion"];
var xmlData = "";
var urlVarMercado = getUrlVars()["Mercado"];
var urlVarMoneda = getUrlVars()["Moneda"];
var urlVarSector = getUrlVars()["Sector"];
var urlVarAgrupadoPor = getUrlVars()["AgrupadoPor"];
var urlVarFechaConsulta = getUrlVars()["FechaConsulta"];
var target = "#tabCuenta";
var jsfechaConsulta
var jsfechaFormato
var vColModel = [];
var vColNames = [];
var lista;

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#paginador").outerHeight(true) - 37 - 84 - 23;

    if (ancho < 768) {
        $("#Lista").setGridWidth($("#encSubPantalla").width() - 2);
        $('#Lista').setGridHeight(220);
    } else {
        $("#Lista").setGridWidth($("#encSubPantalla").width() - 2);
        $('#Lista').jqGrid('setGridHeight', height);
    }

}

function initCarteraListadoMovimientos() {

    $("#idSubtituloPaginaText").text("Listado de Movimientos: " + urlVarDescripcion);
    document.title = "Listado de Movimientos: " + urlVarDescripcion;

    $("#btnBuscarCuentas").hide();
    $("#btnBuscarClientes").hide();
    $("#btnBuscarGrupos").hide();
    $("#btnLimpiarCuentas").hide();
    $("#btnLimpiarClientes").hide();
    $("#btnLimpiarGrupos").hide();

    lista = urlVarAgrupadoPor == null ? "#ListaPorCuenta" : urlVarAgrupadoPor;

    if (typeof urlVarFechaConsulta !== "undefined") {
        $(".input-group.date").datepicker('setDate', urlVarFechaConsulta);
    } else {
        jsfechaConsulta = moment($(".input-group.date").datepicker('getDate')).format("L");
        urlVarFechaConsulta = jsfechaConsulta;
    }
    if (typeof urlVarMercado !== "undefined") {
        var urlDescripcionMercado = getUrlVars()["DescripcionMercado"];
        $("#idSubtituloPaginaText").text("Listado Movimiento: " + urlVarDescripcion + " - Mercado: " + urlDescripcionMercado);
        document.title = "Listado Movimiento: " + urlVarDescripcion + " - Mercado: " + urlDescripcionMercado;
    } else {
        urlVarMercado = "";
    }
    if (typeof urlVarMoneda !== "undefined") {
        var urlDescripcionMoneda = getUrlVars()["DescripcionMoneda"];
        $("#idSubtituloPaginaText").text("Listado Movimiento: " + urlVarDescripcion + " - Moneda: " + urlDescripcionMoneda);
        document.title = "Listado Movimiento: " + urlVarDescripcion + " - Moneda: " + urlDescripcionMoneda;
    } else {
        urlVarMoneda = "";
    }
    if (typeof urlVarSector !== "undefined") {
        var urlDescripcionSector = getUrlVars()["DescripcionSector"];
        $("#idSubtituloPaginaText").text("Listado Movimiento: " + urlVarDescripcion + " - Sector: " + urlDescripcionSector);
        document.title = "Listado Movimiento: " + urlVarDescripcion + " - Sector: " + urlDescripcionSector;
    } else {
        urlVarSector = "";
    }

    if (urlVarAgrupadoPor == "#ListaPorCuenta") {
        $("#tabsClienteCuentaGrupo").hide();
        $('.nav-tabs a[href="#tabCuenta"]').tab('show');
    } else if (urlVarAgrupadoPor == "#ListaPorCliente") {
        $("#tabsClienteCuentaGrupo").hide();
        $('.nav-tabs a[href="#tabCliente"]').tab('show');
    } else if (urlVarAgrupadoPor == "#ListaPorGrupo") {
        $("#tabsClienteCuentaGrupo").hide();
        $('.nav-tabs a[href="#tabGrupo"]').tab('show');
    }
}
function cargarEventHandlersCarteraListadoMovimientos() {

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $(".input-group.date").datepicker();

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        target = $(e.target).attr("href") // activated tab
        if (target == "#tabCuenta") {
            $("#contenidoPorCuenta").show();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaPorCuenta";
        }
        if (target == "#tabCliente") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").show();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaPorCliente";
        }
        if (target == "#tabGrupo") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").show();
            lista = "#ListaPorGrupo";
        }
        resize();
    });

    $("#Lista").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records" },
        colNames: ["ID", "Cuenta", "Nro. Operación", "Fecha Trans.", "Operación", "Mov", "Nemotécnico", "Cantidad", "Precio", "Monto"],
        colModel: [
            { name: "ID", index: "ID", sortable: true, width: 150, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: false, width: 100, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "ID_OPERACION", index: "ID_OPERACION", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "FECHA_OPERACION", index: "FECHA_OPERACION", sortable: true, width: 140, sorttype: "date", formatter: 'date', editable: false, search: true, align: "center", hidden: false, searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "DSC_TIPO_OPERACION", index: "DSC_TIPO_OPERACION", sortable: true, width: 160, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "TIPO_MOVIMIENTO", index: "TIPO_MOVIMIENTO", sortable: true, width: 150, sorttype: "text", align: "center", formatter: FormatoIngresoEgreso, editable: false, search: true, hidden: false },
            { name: "NEMOTECNICO", index: "NEMOTECNICO", sortable: true, width: 180, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 100, sorttype: "number", formatter: "number", align: "right", editable: false, search: true, hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "PRECIO", index: "PRECIO", sortable: true, width: 120, sorttype: "number", formatter: "number", align: "right", editable: false, search: true, hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO", index: "MONTO", sortable: true, width: 120, sorttype: "number", formatter: "number", align: "right", editable: false, search: true, hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }],
        pager: "#paginador",
        loadtext: "Cargando datos...",
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        width: "100%",
        styleUI: 'Bootstrap',
        //sortname: "FECHA_OPERACION",
        //sortorder: "asc",
        caption: "Listado Movimientos",
        grouping: true,
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        groupingView: {
            groupField: ['NUM_CUENTA'],
            groupColumnShow: [false],
            groupOrder: ["asc"],
            groupText: ['<b>Cuenta: {0}</b>'],
        },
        onCellSelect: function (rowid) {
            var href = "CarteraDetalleOperacion.aspx?Oper=" + $("#Lista").jqGrid("getCell", rowid, "ID_OPERACION") + "&AgrupadoPor=" + lista
            window.location.href = '../Views/' + href;
        }
    });

    $("#Lista").jqGrid().bindKeys().scrollingRows = true
    $("#Lista").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#Lista").jqGrid('navGrid', '#paginador', { add: false, edit: false, del: false, excel: true, search: false });
    $("#Lista").jqGrid('navButtonAdd', '#paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('Lista', 'ListadoMovimiento', 'ListadoMovimiento', '');
        }
    });
    $("#Lista").setGridWidth($("#DivGrillaLstMov").width());
    $('#Lista').jqGrid('setLabel', "ID_OPERACION", "Nro. Operación");

    $("#BtnConsultar").button().click(function () {
        consultaListadoMovimientos("N");
    });

    $("#porCuenta_NroCuenta").on('change', function () {
        consultaListadoMovimientos("S");
    });
}

function FormatoIngresoEgreso(cellvalue, options, rowObject) {
    if (cellvalue == 'E') {
        return "<i class='glyphicon glyphicon-arrow-down' style='color: #E24B0E;'></i>";
    } else {
        return "<i class='glyphicon glyphicon-arrow-up' style='color: #3E8922;'></i>";
    }
};
function consultaListadoMovimientos(strCargaInicial) {
    jsfechaConsulta = document.getElementById("dtFechaConsulta").value;
    var jsIdCuenta = "";
    var jsIdCliente = "";
    var jsIdGrupo = "";

    if (target == "#tabCuenta") {
        jsIdCuenta = miInformacionCuenta == null ? "" : miInformacionCuenta.IdCuenta;
        if (miInformacionCuenta == null && lista == "#ListaPorCuenta") {
            if (strCargaInicial == "N") {
                alert("Primero debe seleccionar una cuenta");
            }
            return;
        }
    }
    if (target == "#tabCliente") {
        jsIdCliente = miInformacionCliente == null ? "" : miInformacionCliente.IdCliente;
        if (miInformacionCliente == null && lista == "#ListaPorCliente") {
            if (strCargaInicial == "N") {
                alert("Primero debe seleccionar un cliente");
            }
            return;
        }
    }
    if (target == "#tabGrupo") {
        jsIdGrupo = miInformacionGrupo == null ? "" : miInformacionGrupo.IdGrupo;
        if (miInformacionGrupo == null && lista == "#ListaPorGrupo") {
            if (strCargaInicial == "N") {
                alert("Primero debe seleccionar un grupo");
            }
            return;
        }
    }
    if (jsfechaConsulta === "") {
        if (strCargaInicial == "N") {
            alert("No ha indicado fecha de consulta");
        }
        return;
    }
    $("#Lista").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    jsfechaFormato = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString()
    $.ajax({
        url: "CarteraListadoMovimientos.aspx/ConsultaListadoMovimientos",
        data: "{'dfechaConsulta':'" + jsfechaFormato.substring(0, jsfechaFormato.length - 1) +
            "', 'idCuenta':'" + jsIdCuenta +
            "', 'idCliente':'" + jsIdCliente +
            "', 'idGrupo':'" + jsIdGrupo +
            "', 'idNemotecnico':'" + urlVarIdNemotecnico +
            "', 'intMercado':'" + urlVarMercado +
            "', 'intMoneda':'" + urlVarMoneda +
            "','intSector':'" + urlVarSector + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#Lista").setGridParam({ data: mydata });
                $("#Lista").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
            $(window).trigger('resize');
        }
    });
};

$(document).ready(function () {
    initCarteraListadoMovimientos();
    cargarEventHandlersCarteraListadoMovimientos();
    consultaListadoMovimientos("S");
    resize();
});