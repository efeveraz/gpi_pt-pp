VERSION 5.00
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Begin VB.Form Frm_Cartola 
   Caption         =   "Form1"
   ClientHeight    =   9150
   ClientLeft      =   60
   ClientTop       =   390
   ClientWidth     =   11385
   LinkTopic       =   "Form1"
   ScaleHeight     =   9150
   ScaleWidth      =   11385
   StartUpPosition =   3  'Windows Default
   Begin VSPrinter8LibCtl.VSPrinter VsP 
      Height          =   8205
      Left            =   30
      TabIndex        =   1
      Top             =   840
      Width           =   11175
      _cx             =   19711
      _cy             =   14473
      Appearance      =   1
      BorderStyle     =   1
      Enabled         =   -1  'True
      MousePointer    =   0
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoRTF         =   -1  'True
      Preview         =   -1  'True
      DefaultDevice   =   0   'False
      PhysicalPage    =   -1  'True
      AbortWindow     =   -1  'True
      AbortWindowPos  =   0
      AbortCaption    =   "Printing..."
      AbortTextButton =   "Cancel"
      AbortTextDevice =   "on the %s on %s"
      AbortTextPage   =   "Now printing Page %d of"
      FileName        =   ""
      MarginLeft      =   1000
      MarginTop       =   1000
      MarginRight     =   1000
      MarginBottom    =   1000
      MarginHeader    =   0
      MarginFooter    =   0
      IndentLeft      =   0
      IndentRight     =   0
      IndentFirst     =   0
      IndentTab       =   720
      SpaceBefore     =   0
      SpaceAfter      =   0
      LineSpacing     =   100
      Columns         =   1
      ColumnSpacing   =   180
      ShowGuides      =   2
      LargeChangeHorz =   300
      LargeChangeVert =   300
      SmallChangeHorz =   30
      SmallChangeVert =   30
      Track           =   0   'False
      ProportionalBars=   -1  'True
      Zoom            =   43.9393939393939
      ZoomMode        =   3
      ZoomMax         =   400
      ZoomMin         =   10
      ZoomStep        =   25
      EmptyColor      =   -2147483636
      TextColor       =   0
      HdrColor        =   0
      BrushColor      =   0
      BrushStyle      =   0
      PenColor        =   0
      PenStyle        =   0
      PenWidth        =   0
      PageBorder      =   7
      Header          =   ""
      Footer          =   ""
      TableSep        =   "|;"
      TableBorder     =   7
      TablePen        =   0
      TablePenLR      =   0
      TablePenTB      =   0
      NavBar          =   3
      NavBarColor     =   -2147483633
      ExportFormat    =   0
      URL             =   ""
      Navigation      =   3
      NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
      AutoLinkNavigate=   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   9
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   525
      Left            =   4800
      TabIndex        =   0
      Top             =   180
      Width           =   1245
   End
End
Attribute VB_Name = "Frm_Cartola"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const fId_Cuenta = 123

Dim fFecha_Cartola As Date
Dim fID_Moneda_Cuenta As Double
Dim fID_Moneda_UF As Double
Dim fID_Moneda_Dolar As Double


Dim fCursor As Object
Dim fTotal_Activos As Double

Private Enum eCartola_Cols
  eCol_Titulo = 1
  eCol_Porc
  eCol_Monto
End Enum


Private Sub Command1_Click()
Dim lLin As Long
  
  Rem Comienzo de la generaci�n del reporte
  With VsP
    .Clear                      'Limpia el VSPrinter
    '.Orientation = orLandscape  'horizontal
    .StartDoc
    .FontName = "Arial"
    .SpaceBefore = "2pt"
    .SpaceAfter = "2pt"
    '-----------------------------------------------------------------------------------------------------
    Rem Encabezado y Pie de p�gina
    .HdrFontSize = 10
    .HdrFontName = "Arial"
    .Header = "||CSGPI - Creasys Chile"
    .Footer = "||Pagina %d"
    '-----------------------------------------------------------------------------------------------------
    .FontSize = 14
    .FontBold = True
    .Paragraph = "Cartola"
    .FontSize = 12
    .Paragraph = "Fecha Consulta: " & Format(Now, cFormatDate)
    
    .Paragraph = "" 'salto de linea
    .FontBold = False
    .FontSize = 9
    
    Rem Por cada cliente encontrado
    .StartTable
      
   ' create table with four rows and four columns
    .TableCell(tcCols) = 7
    .TableCell(tcRows) = 52

    .TableCell(tcFontName) = "Arial"
    .TableCell(tcFontSize) = 7

    .TableCell(tcColWidth, , 1) = "2500"
    .TableCell(tcColWidth, , 2) = "500"
    .TableCell(tcColWidth, , 3) = "1500"
    
    .TableCell(tcColWidth, , 4) = "300"
    
    .TableCell(tcColWidth, , 5) = "1500"
    .TableCell(tcColWidth, , 6) = "1750"
    .TableCell(tcColWidth, , 7) = "1750"
    
    .TableCell(tcAlign, , 3) = taRightMiddle
    .TableCell(tcAlign, , 2) = taCenterMiddle

    '------------------
    '-- INDICADORES
    '------------------
    lLin = 1: .TableCell(tcText, lLin, 1) = "INDICADORES"
              .TableCell(tcColSpan, lLin, 1) = 3
              .TableCell(tcBackColor, lLin, 1) = vbYellow
              
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "Fecha"
    lLin = lLin + 2: .TableCell(tcText, lLin, 1) = "Valor USD"
    lLin = lLin + 2: .TableCell(tcText, lLin, 1) = "Valor UF"


    '------------------
    '-- PATRIMONIO MERCADO
    '------------------
    lLin = lLin + 2: .TableCell(tcText, lLin, 1) = "PATRIMONIO MERCADO"
                      .TableCell(tcColSpan, lLin, 1) = 3
                      .TableCell(tcBackColor, lLin, 1) = vbYellow
              
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "$"
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "UF"
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "USD"

    
    '------------------
    '-- RENTABILIDAD CARTERA
    '------------------
    lLin = lLin + 2: .TableCell(tcText, lLin, 1) = "RENTABILIDAD CARTERA"
                      .TableCell(tcColSpan, lLin, 1) = 3
                      .TableCell(tcBackColor, lLin, 1) = vbYellow
              
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "MES"
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "12 Meses"
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "Anual"


    '------------------
    '-- FLUJO PATRIMONIAL ANUAL
    '------------------
    lLin = 1: .TableCell(tcText, lLin, 5) = "FLUJO PATRIMONIAL ANUAL"
              .TableCell(tcColSpan, lLin, 5) = 3
              .TableCell(tcBackColor, lLin, 5) = vbYellow
              
    lLin = lLin + 1: .TableCell(tcText, lLin, 5) = "FECHA"
                      .TableCell(tcText, lLin, 6) = "APORTES ($)"
                      .TableCell(tcText, lLin, 7) = "RETIROS ($)"

    lLin = 17
    
    '------------------
    '-- CUENTA INVERSION
    '------------------
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "CUENTA INVERSION"
                      .TableCell(tcColSpan, lLin, 1) = 7
                      '.TableCell(tcBackColor, lLin, 1) = vbYellow

    lLin = 19

    '------------------
    '-- ACTIVOS POR PRODUCTO
    '------------------
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "ACTIVOS POR PRODUCTO"
                      .TableCell(tcColSpan, lLin, 1) = 3
                      .TableCell(tcBackColor, lLin, 1) = vbYellow

    lLin = lLin + 11
    '------------------
    '-- FLUJO DIVIDENDOS Y CUPONES
    '------------------
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "FLUJO DIVIDENDOS Y CUPONES"
                      .TableCell(tcColSpan, lLin, 1) = 3
                      .TableCell(tcBackColor, lLin, 1) = vbYellow

    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "VENCIMIENTO"
                      .TableCell(tcText, lLin, 2) = "%"
                      .TableCell(tcText, lLin, 3) = "MONTO($)"
    
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "0-30 Dias"
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "31-90 Dias"
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "Mas de 90 Dias"
    lLin = lLin + 1: .TableCell(tcText, lLin, 1) = "Total"
    
    
    lLin = 19
    
    '------------------
    '-- ACTIVOS POR MERCADO
    '------------------
    lLin = lLin + 1: .TableCell(tcText, lLin, 5) = "ACTIVOS POR MERCADO"
                      .TableCell(tcColSpan, lLin, 5) = 3
                      .TableCell(tcBackColor, lLin, 5) = vbYellow

    lLin = lLin + 7
   
    
    '------------------
    '-- ACTIVOS POR MONEDA
    '------------------
    lLin = lLin + 1: .TableCell(tcText, lLin, 5) = "ACTIVOS POR MONEDA"
                      .TableCell(tcColSpan, lLin, 5) = 3
                      .TableCell(tcBackColor, lLin, 5) = vbYellow



    Call Sub_CargaActivos(VsP)
    
    Call Sub_Indicadores(VsP)
'

    
    .TableCell(tcRowHeight) = "200"



'      .TableCell(tcAlignCurrency) = False
'
'
'      Const sHeader_Clt = "Indicadores"
'      Const sFormat_Clt = "5000|>1400"
'      .AddTable sFormat_Clt, sHeader_Clt, sRecord, clrHeader, , bAppend
'
'      sRecord = NVL(lReg_Clt("nombre_cliente").Value, "") & "|" & _
'                NVL(lReg_Clt("rut_cliente").Value, "") & "|" & _
'                lSexo & "|" & _
'                NVL(lReg_Clt("fecha_nacimiento").Value, "") & "|" & _
'                NVL(lReg_Clt("email").Value, "") & "|" & _
'                lPais_Clt & "|" & _
'                lTipo_Entidad
'
'
'      For Each lReg_Clt In lCursor_Clt
'        Rem Genera el reporte
'        sRecord = NVL(lReg_Clt("nombre_cliente").Value, "") & "|" & _
'                  NVL(lReg_Clt("rut_cliente").Value, "") & "|" & _
'                  lSexo & "|" & _
'                  NVL(lReg_Clt("fecha_nacimiento").Value, "") & "|" & _
'                  NVL(lReg_Clt("email").Value, "") & "|" & _
'                  lPais_Clt & "|" & _
'                  lTipo_Entidad
'        .AddTable sFormat_Clt, sHeader_Clt, sRecord, clrHeader, , bAppend
'      Next
'
'      TableCell(tcRowHeight, 1) = ""
'
'
'
'
'
'      .TableCell(tcFontBold, 0) = True
      .EndTable
'    Else
'      .Paragraph = "No hay Clientes Vigentes en el sistema."
'    End If
    
    .EndDoc
  End With
End Sub

Private Sub VSPrinter1_Click()

End Sub

Private Sub Form_Load()
  fFecha_Cartola = DateValue("27/09/2006")
End Sub

Private Sub Form_Resize()
On Error Resume Next
  'VsP.Top = 0
  VsP.Left = 0
  VsP.Width = Me.ScaleWidth
  VsP.Height = Me.ScaleHeight - VsP.Top
End Sub

Private Sub Sub_CargaActivos(ByRef pVsp As VSPrinter)
Dim lRec As hFields
'----------------------------------------
Dim lcMoneda As Class_Monedas
Dim lcTipo_Cambio As Class_Tipo_Cambios
'----------------------------------------

Dim lTotal_ActivosUF As Double
Dim lTotal_ActivosDolar As Double


  Set lcMoneda = New Class_Monedas
  lcMoneda.Campo("Cod_Moneda").Valor = "UF"
  If lcMoneda.Buscar Then
    fID_Moneda_UF = lcMoneda.Cursor(1)("id_moneda").Value
  End If
  Set lcMoneda = Nothing
  
  Set lcMoneda = New Class_Monedas
  lcMoneda.Campo("cod_moneda").Valor = "USD"
  If lcMoneda.Buscar Then
    fID_Moneda_Dolar = lcMoneda.Cursor(1)("id_moneda").Value
  End If
  Set lcMoneda = Nothing
  
  
  gDB.Parametros.Clear
  gDB.Procedimiento = "Pkg_Cartola.Sp_Activos"
  Call gDB.Parametros.Add("Pid_Cuenta", ePT_Numero, fId_Cuenta, ePD_Entrada)
  Call gDB.Parametros.Add("Pfecha", ePT_Fecha, fFecha_Cartola, ePD_Entrada)
  Call gDB.Parametros.Add("Pcursor", ePT_Cursor, "", ePD_Ambos)
  
  If gDB.EjecutaSP Then
    Set fCursor = gDB.Parametros("Pcursor").Valor
  Else
    Set fCursor = Nothing
  End If
  
  fTotal_Activos = 0
  If Not fCursor Is Nothing Then
    For Each lRec In fCursor
      fID_Moneda_Cuenta = lRec("id_Moneda_cta").Value
      fTotal_Activos = fTotal_Activos + lRec("Monto_Mon_Cta").Value
    Next
  End If
  
  Set lcTipo_Cambio = New Class_Tipo_Cambios
  lTotal_ActivosUF = lcTipo_Cambio.Cambio_Paridad(fId_Cuenta, fTotal_Activos, fID_Moneda_Cuenta, fID_Moneda_UF, fFecha_Cartola)
  lTotal_ActivosDolar = lcTipo_Cambio.Cambio_Paridad(fId_Cuenta, fTotal_Activos, fID_Moneda_Cuenta, fID_Moneda_Dolar, fFecha_Cartola)
  Set lcTipo_Cambio = Nothing
  
  pVsp.TableCell(tcText, 9, 3) = fTotal_Activos
  pVsp.TableCell(tcText, 10, 3) = lTotal_ActivosUF
  pVsp.TableCell(tcText, 11, 3) = lTotal_ActivosDolar
End Sub

Private Sub Sub_Patrimonio_Mercado(ByRef pVsp As VSPrinter)
Dim lRec As hFields
'----------------------------------------
Dim lMon_Pesos As Double
  
  
  lMon_Pesos = 0
  For Each lRec In fCursor
    Select Case lRec("cod_moneda").Value
      Case "$$"
        lMon_Pesos = lMon_Pesos + lRec("Monto_Mon_Cta").Value
    End Select
  Next
 
  pVsp.TableCell(tcText, 7, 1) = lMon_Pesos
End Sub

Private Sub Sub_Indicadores(ByRef pVsp As VSPrinter)
Dim lUF_Paridad As Double
Dim lUF_Operacion As String
Dim lUSD_Paridad As Double
Dim lUSD_Operacion As String
'----------------------------------------
Dim lcTipo_Cambio As Class_Tipo_Cambios


  Set lcTipo_Cambio = New Class_Tipo_Cambios
  Call lcTipo_Cambio.Busca_Precio_Paridad(fId_Cuenta _
                                      , fID_Moneda_Cuenta _
                                      , fID_Moneda_UF _
                                      , fFecha_Cartola _
                                      , lUF_Paridad _
                                      , lUF_Operacion)
  
  Call lcTipo_Cambio.Busca_Precio_Paridad(fId_Cuenta _
                                      , fID_Moneda_Cuenta _
                                      , fID_Moneda_Dolar _
                                      , fFecha_Cartola _
                                      , lUSD_Paridad _
                                      , lUSD_Operacion)
  Set lcTipo_Cambio = Nothing
                                      

  'Fecha
  pVsp.TableCell(tcText, 2, eCol_Monto) = fFecha_Cartola
  
  
  'Valor Dolar
  pVsp.TableCell(tcText, 4, eCol_Monto) = lUSD_Paridad

  'Valor UF
  pVsp.TableCell(tcText, 6, eCol_Monto) = lUF_Paridad
End Sub
