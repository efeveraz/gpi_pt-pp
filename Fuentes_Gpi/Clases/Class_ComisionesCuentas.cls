VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_ComisionesCuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_ComisionesCuenta"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 35
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_COMISION_CUENTA"
    .AddCampo "ID_COMISION_CUENTA", ePT_Numero, ePD_Ambos
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "COD_INSTRUMENTO", ePT_Caracter, ePD_Entrada
    .AddCampo "FECHA_INI", ePT_Fecha, ePD_Entrada
    .AddCampo "FECHA_TER", ePT_Fecha, ePD_Entrada
    .AddCampo "COMISION_HONORARIOS", ePT_Numero, ePD_Entrada
    .AddCampo "COMISION_ASESORIAS", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MONEDA", ePT_Numero, ePD_Entrada
    .AddCampo "PERIODICIDAD", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar(Optional pEnVista As Boolean = False)
Dim lProc As String

  If pEnVista Then
    lProc = ".BuscarView"
  Else
    lProc = ".Buscar"
  End If
  
  Buscar = fClass_Entidad.Buscar(cfPackage & lProc)
End Function

Public Function BuscarView_Todas(pId_Empresa) As Boolean
  With fClass_Entidad
    .AddCampo "id_empresa", ePT_Numero, ePD_Entrada, pId_Empresa
    
    BuscarView_Todas = .Buscar(cfPackage & ".BuscarView_Todas")
  End With
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function ValidaComisionCuenta(ByRef Presultado As Integer, ByRef pMensaje As String) As Boolean
On Error GoTo ErrProcedure

  ValidaComisionCuenta = False

  With gDB
    .Parametros.Clear
    .Procedimiento = cfPackage & ".VALIDA_COMISION_CUENTA"
    .Parametros.Add "p" & Campo("id_comision_cuenta").Nombre, ePT_Numero, Campo("id_comision_cuenta").Valor, ePD_Entrada
    .Parametros.Add "p" & Campo("id_cuenta").Nombre, ePT_Numero, Campo("id_cuenta").Valor, ePD_Entrada
    .Parametros.Add "p" & Campo("Fecha_Ini").Nombre, ePT_Fecha, Campo("Fecha_Ini").Valor, ePD_Entrada
    .Parametros.Add "p" & Campo("Fecha_Ter").Nombre, ePT_Fecha, Campo("Fecha_Ter").Valor, ePD_Entrada
    .Parametros.Add "p" & Campo("cod_instrumento").Nombre, ePT_Caracter, Campo("cod_instrumento").Valor, ePD_Entrada
    
    .Parametros.Add "Presultado", ePT_Numero, "", ePD_Salida
    .Parametros.Add "Pmensaje", ePT_Caracter, "", ePD_Salida
    
    If Not .EjecutaSP Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ErrProcedure
    Else
      Presultado = .Parametros("Presultado").Valor
      pMensaje = .Parametros("Pmensaje").Valor
    End If
  End With

  ValidaComisionCuenta = True
  
ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Buscar_Valida(pFecha) As Boolean
  fClass_Entidad.AddCampo "Fecha", ePT_Fecha, ePD_Entrada, pFecha
  
  Buscar_Valida = fClass_Entidad.Buscar(cfPackage & ".Buscar_Valida")
End Function

Public Function Buscar_Comisiones_Informe(pId_Cuenta As String, _
                                    ByVal pFecha_Ini As Date, _
                                    ByVal pFecha_Fin As Date) As Boolean
                                    
    Dim lClass_Entidad As Class_Entidad

    Set lClass_Entidad = New Class_Entidad

    With lClass_Entidad
        .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada
        .AddCampo "fecha_ini", ePT_Fecha, ePD_Entrada
        .AddCampo "fecha_fin", ePT_Fecha, ePD_Entrada

        .Campo("id_cuenta").Valor = pId_Cuenta
        .Campo("fecha_ini").Valor = pFecha_Ini
        .Campo("fecha_fin").Valor = pFecha_Fin

        Buscar_Comisiones_Informe = .Buscar(cfPackage & ".COMISIONES_CUENTA_INFORME")

        fClass_Entidad.Errnum = lClass_Entidad.Errnum
        fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
        
        Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
        
    End With

    Set lClass_Entidad = Nothing
    
End Function


Public Function Buscar_Comisiones_Porcentuales(pId_Cuenta As String, _
                                    ByVal pFecha_Ini As Date, _
                                    ByVal pFecha_Fin As Date) As Boolean
                                    
    Dim lClass_Entidad As Class_Entidad

    Set lClass_Entidad = New Class_Entidad

    With lClass_Entidad
        .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada
        .AddCampo "fecha_ini", ePT_Fecha, ePD_Entrada
        .AddCampo "fecha_fin", ePT_Fecha, ePD_Entrada

        .Campo("id_cuenta").Valor = pId_Cuenta
        .Campo("fecha_ini").Valor = pFecha_Ini
        .Campo("fecha_fin").Valor = pFecha_Fin

        Buscar_Comisiones_Porcentuales = .Buscar(cfPackage & ".COMISIONES_PORCENTUALES")

        fClass_Entidad.Errnum = lClass_Entidad.Errnum
        fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
        
        Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
        
    End With

    Set lClass_Entidad = Nothing
    
End Function


Public Function Buscar_Comisiones_Fijas(pId_Cuenta As String, _
                                    ByVal pFecha_Ini As Date, _
                                    ByVal pFecha_Fin As Date) As Boolean
                                    
    Dim lClass_Entidad As Class_Entidad

    Set lClass_Entidad = New Class_Entidad

    With lClass_Entidad
        .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada
        .AddCampo "fecha_ini", ePT_Fecha, ePD_Entrada
        .AddCampo "fecha_fin", ePT_Fecha, ePD_Entrada

        .Campo("id_cuenta").Valor = pId_Cuenta
        .Campo("fecha_ini").Valor = pFecha_Ini
        .Campo("fecha_fin").Valor = pFecha_Fin

        Buscar_Comisiones_Fijas = .Buscar(cfPackage & ".COMISIONES_FIJAS")

        fClass_Entidad.Errnum = lClass_Entidad.Errnum
        fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
        
        Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
        
    End With

    Set lClass_Entidad = Nothing
    
End Function

'---------------------------------------------------------------------------------------
' Procedure : Buscar_Comisiones_Cobradas
' Author    : jvidal
' Date      : 24/09/2008
' Purpose   : Ejecutar el Procedimiento almacenado para el informe de Comisiones Cobradas
'---------------------------------------------------------------------------------------
'Public Function Buscar_Comisiones_Cobradas(pId_Cuenta As String, _
'                                    ByVal pFecha_Ini As Date, _
'                                    ByVal pFecha_Fin As Date) As Boolean
Public Function Buscar_Comisiones_Cobradas(pId_Cuenta As String, _
                                    ByVal pFecha_Ini As Date) As Boolean
                                    
    Dim lClass_Entidad As Class_Entidad

    Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
        '.AddCampo "id_empresa", ePT_Numero, ePD_Entrada, Fnt_EmpresaActual
        .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
        .AddCampo "fecha_desde", ePT_Fecha, ePD_Entrada, pFecha_Ini
        
        Buscar_Comisiones_Cobradas = .Buscar(cfPackage & ".COMISIONES_COBRADAS")

        fClass_Entidad.Errnum = lClass_Entidad.Errnum
        fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
        
        Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
        
    End With

    Set lClass_Entidad = Nothing
    
End Function

