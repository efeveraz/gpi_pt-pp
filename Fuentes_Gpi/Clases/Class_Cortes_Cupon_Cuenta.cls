VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Cortes_Cupon_Cuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Cortes_Cupon_Cuenta"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 70
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_CORTE_CUPON_CUENTA"
    .AddCampo "ID_CORTE_CUPON_CUENTA", ePT_Numero, ePD_Ambos
    .AddCampo "ID_CORTE_CUPON", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MOV_ACTIVO", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MOV_CAJA", ePT_Numero, ePD_Entrada
    .AddCampo "FCH_CORTE_CUPON", ePT_Fecha, ePD_Entrada
    .AddCampo "DSC_CORTE_CUPON_CUENTA", ePT_Caracter, ePD_Entrada
    .AddCampo "INTERES", ePT_Numero, ePD_Entrada
    .AddCampo "AMORTIZACION", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar(Optional pId_Cuenta) As Boolean
  With fClass_Entidad
    If Not IsMissing(pId_Cuenta) Then
      .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
    End If
  End With

  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Guardar() As Boolean
Dim lcMov_Caja        As Class_Mov_Caja
Dim lcMov_Activo      As Class_Mov_Activos
Dim lcCajas_Cuenta    As Class_Cajas_Cuenta
Dim lcCargo_Abono     As Class_Cargos_Abonos
'------------------------------------------------------------
Dim lId_Caja_Cuenta   As Double
Dim lFecha_Habil As Date

On Error GoTo ErrProcedure

  Guardar = False
  
  lFecha_Habil = Fnt_Dia_Habil_MasProximo(Me.Campo("FCH_CORTE_CUPON").Valor)
  
  'Busca el id del movimiento de activo
  Set lcMov_Activo = New Class_Mov_Activos
  With lcMov_Activo
    .Campo("id_mov_activo").Valor = Me.Campo("id_mov_activo").Valor
    If Not .Buscar Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  End With
  
  
  'bUSCA LA CAJA CUENTA QUE TENGA LA CUENTA SETIADA PARA LA MONEDA
  Set lcCajas_Cuenta = New Class_Cajas_Cuenta
  With lcCajas_Cuenta
    .Campo("id_cuenta").Valor = lcMov_Activo.Cursor(1)("id_cuenta").Value
    .Campo("id_moneda").Valor = lcMov_Activo.Cursor(1)("id_moneda").Value
    If Not .Buscar Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
    
    lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
  End With
  
  '---------------------------------------------
  '-- Agrega o modifica el movimiento de caja
  '---------------------------------------------
'  Set lcMov_Caja = New Class_Mov_Caja
'  With lcMov_Caja
'    .Campo("id_mov_caja").Valor = Me.Campo("id_mov_caja").Valor
'    .Campo("Id_Caja_Cuenta").Valor = lId_Caja_Cuenta
'    .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_CorteCupon
'    .Campo("Fecha_Movimiento").Valor = Me.Campo("FCH_CORTE_CUPON").Valor
'    .Campo("Fecha_Liquidacion").Valor = lFecha_Habil 'Me.Campo("FCH_CORTE_CUPON").Valor
'    .Campo("Monto").Valor = Me.Campo("MONTO").Valor
'    .Campo("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Abono
'    .Campo("Dsc_Mov_Caja").Valor = Me.Campo("DSC_CORTE_CUPON_CUENTA").Valor
'
'    If Not .Guardar Then
'      fClass_Entidad.ErrMsg = .ErrMsg
'      fClass_Entidad.Errnum = .Errnum
'      GoTo ExitProcedure
'    End If
'
'    Me.Campo("id_mov_caja").Valor = .Campo("id_mov_caja").Valor
'  End With

    Set lcCargo_Abono = New Class_Cargos_Abonos
    With lcCargo_Abono
        .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_CorteCupon
        .Campo("id_mov_caja").Valor = Me.Campo("id_mov_caja").Valor
        .Campo("Id_Caja_Cuenta").Valor = lId_Caja_Cuenta
        .Campo("Id_Cuenta").Valor = lcMov_Activo.Cursor(1)("id_cuenta").Value
        .Campo("Id_Moneda").Valor = lcMov_Activo.Cursor(1)("id_moneda").Value
        .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente '--Siempre se ingresa en estado "P"endiente
        .Campo("Dsc_Cargo_Abono").Valor = Me.Campo("DSC_CORTE_CUPON_CUENTA").Valor
        .Campo("Fecha_Movimiento").Valor = Me.Campo("FCH_CORTE_CUPON").Valor
        .Campo("Flg_Tipo_Cargo").Valor = gcTipoOperacion_Abono
        .Campo("Monto").Valor = Me.Campo("MONTO").Valor
        .Campo("Retencion").Valor = DateDiff("d", Me.Campo("FCH_CORTE_CUPON").Valor, lFecha_Habil)
        .Campo("Flg_Tipo_Origen").Valor = "A"  '--Automatico
        If Not .Guardar Then
            fClass_Entidad.ErrMsg = .ErrMsg
            fClass_Entidad.Errnum = .Errnum
            GoTo ExitProcedure
        End If
    Me.Campo("id_mov_caja").Valor = .Campo("id_mov_caja").Valor
    End With
  

  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcMov_Caja = Nothing
  Set lcMov_Activo = Nothing
  Set lcCajas_Cuenta = Nothing
  Set lcCargo_Abono = Nothing
End Function

Public Function Borrar() As Boolean
Dim lcCorte_Cupon_Cuenta  As Class_Cortes_Cupon_Cuenta
Dim lcMov_Caja            As Class_Mov_Caja
Dim lcCargo_Abono         As Class_Cargos_Abonos        'Agregado por MMA 10/02/2009
'---------------------------------------------------------
Dim lId_Mov_Caja As Double

On Error GoTo ErrProcedure

  Borrar = False
 
  If IsNull(Me.Campo("id_mov_caja").Valor) Then
    'si el id del movimiento de caja es nulo busco el id en la tabla
    Set lcCorte_Cupon_Cuenta = New Class_Cortes_Cupon_Cuenta
    With lcCorte_Cupon_Cuenta
      .Campo("id_corte_cupon_cuenta").Valor = Me.Campo("id_corte_cupon_cuenta").Valor
      If Not .Buscar Then
        fClass_Entidad.ErrMsg = .ErrMsg
        fClass_Entidad.Errnum = .Errnum
        GoTo ExitProcedure
      End If
      
      If .Cursor.Count <= 0 Then
        'Significa que ya fue eliminado el corte de cupon para la cuenta.
        Borrar = True
        GoTo ExitProcedure
      End If
      
      lId_Mov_Caja = .Cursor(1)("id_mov_caja").Value
    End With
  Else
    lId_Mov_Caja = Me.Campo("id_mov_caja").Valor
  End If
 
  'Primero se elimina el corte de cupon y despues el movimiento de caja para que asi
  'no salte la forein key
  If Not fClass_Entidad.Borrar(cfPackage & ".Borrar") Then
    GoTo ExitProcedure
  End If
 
 'elimina el movimiento de caja
  Set lcMov_Caja = New Class_Mov_Caja
  With lcMov_Caja
    .Campo("id_mov_caja").Valor = lId_Mov_Caja
    If Not .Anular(lId_Mov_Caja) Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  End With

  'Agregado por MMA 10/02/2009
  'elimina Cargo_Abono
  Set lcCargo_Abono = New Class_Cargos_Abonos
  With lcCargo_Abono
    .Campo("id_mov_caja").Valor = lId_Mov_Caja
    If Not .AnularPorMovCaja Then
        fClass_Entidad.ErrMsg = .ErrMsg
        fClass_Entidad.Errnum = .Errnum
        GoTo ExitProcedure
    End If
  End With
  
  Borrar = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcMov_Caja = Nothing
  Set lcCorte_Cupon_Cuenta = Nothing
End Function

Public Function Buscar_Nemos_Cortados_Cartera(pId_Cuenta)
Dim lcClass_Entidad As Class_Entidad

  Set lcClass_Entidad = New Class_Entidad
  With lcClass_Entidad
    Call .AddCampo("id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta)
    Call .AddCampo("fch_corte_cupon", ePT_Fecha, ePD_Entrada, Me.Campo("FCH_CORTE_CUPON").Valor)
  
    Buscar_Nemos_Cortados_Cartera = .Buscar(cfPackage & ".Buscar_Nemos_Cortados_Cartera")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.ErrMsg = .ErrMsg
    fClass_Entidad.Errnum = .Errnum
  End With
  Set lcClass_Entidad = Nothing
End Function

Public Function Ejecuta_x_Cuenta(pId_Cuenta) As Boolean
'Dim lcMoneda              As Class_Monedas
Dim lcMoneda              As Object
'-------------------------------------------------------------
'Dim lcTipo_Cambio         As Class_Tipo_Cambios
Dim lcTipo_Cambio         As Object
Dim lcSaldo_Activo        As Class_Saldo_Activos
Dim lcCortes_Cupon_Cuenta As Class_Cortes_Cupon_Cuenta
Dim lcMov_Caja_Origen     As Class_Mov_Caja_Origen
Dim lRecord_Saldos        As hRecord
Dim lReg                  As hFields
'-------------------------------------------------------------
Dim lId_Moneda_Peso
Dim lInteres As Double
Dim lAmortizacion As Double
Dim lFch_Corte_Cupon As Date
Dim lDsc_Origen_Mov_Caja As String
Dim lDsc_Origen_Mov_Caja_Cuenta As String
Dim lParidad                    As Double
Dim lOperacion                  As String
Dim lFecha_Habil                As Date
  
  
  Ejecuta_x_Cuenta = False
  
'  Set lcTipo_Cambio = New Class_Tipo_Cambios
  Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
  
  lFch_Corte_Cupon = Me.Campo("FCH_CORTE_CUPON").Valor
  
  lFecha_Habil = Fnt_Dia_Habil_MasProximo(lFch_Corte_Cupon)
  
  'Busca el ID del codigo PESOS
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    .Campo("cod_Moneda").Valor = cMoneda_Cod_Peso
    If Not .Buscar Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
          
    If .Cursor.Count <= 0 Then
      fClass_Entidad.ErrMsg = "No esta definido el codigo de moneda """ & cMoneda_Cod_Peso & """."""
      fClass_Entidad.Errnum = -1
      GoTo ExitProcedure
    End If
  
    lId_Moneda_Peso = .Cursor(1)("id_moneda").Value
  End With
  Set lcMoneda = Nothing
  
  'Busca todos los cortes que caen ese dia.
  Set lcCortes_Cupon_Cuenta = New Class_Cortes_Cupon_Cuenta
  With lcCortes_Cupon_Cuenta
    .Campo("FCH_CORTE_CUPON").Valor = lFch_Corte_Cupon
    If Not .Buscar_Nemos_Cortados_Cartera(pId_Cuenta) Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
    
    Set lRecord_Saldos = .Cursor
  End With
  Set lcCortes_Cupon_Cuenta = Nothing
  
  lDsc_Origen_Mov_Caja = ""
  Set lcMov_Caja_Origen = New Class_Mov_Caja_Origen
  With lcMov_Caja_Origen
    .Campo("COD_ORIGEN_MOV_CAJA").Valor = gcOrigen_Mov_Caja_CorteCupon
    If Not .Buscar Then
      fClass_Entidad.Errnum = .Errnum
      fClass_Entidad.ErrMsg = .ErrMsg
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      fClass_Entidad.Errnum = Me.SubTipo_LOG
      fClass_Entidad.ErrMsg = "No existe el Origen del Movimiento de Caja (" & gcOrigen_Mov_Caja_CorteCupon & ")."
      GoTo ExitProcedure
    End If
    
    lDsc_Origen_Mov_Caja = "" & .Cursor(1)("dsc_glosa_abono").Value
  End With
  
  For Each lReg In lRecord_Saldos
    lInteres = lReg("interes").Value * lReg("cantidad").Value
    lAmortizacion = lReg("amortizacion").Value * lReg("cantidad").Value
  
    If Not lReg("id_moneda_nemotecnico").Value = lId_Moneda_Peso Then
      'si la moneda es distinta a la moneda PESO se tiene que multiplicar por la paridad
      lInteres = lcTipo_Cambio.Cambio_Paridad(pId_Cuenta:=pId_Cuenta _
                                            , pMonto:=lInteres _
                                            , pId_Moneda_Origen:=lReg("id_moneda_nemotecnico").Value _
                                            , pId_Moneda_Destino:=lId_Moneda_Peso _
                                            , pFecha:=lFecha_Habil)

      lAmortizacion = lcTipo_Cambio.Cambio_Paridad(pId_Cuenta:=pId_Cuenta _
                                            , pMonto:=lAmortizacion _
                                            , pId_Moneda_Origen:=lReg("id_moneda_nemotecnico").Value _
                                            , pId_Moneda_Destino:=lId_Moneda_Peso _
                                            , pFecha:=lFecha_Habil)
    End If
    
    lDsc_Origen_Mov_Caja_Cuenta = Replace(lDsc_Origen_Mov_Caja, "<<NEMOTECNICO>>", lReg("NEMOTECNICO").Value) 'CAMBIA LOS NEMOTECNICOS
    lDsc_Origen_Mov_Caja_Cuenta = Replace(lDsc_Origen_Mov_Caja_Cuenta, "<<CANTIDAD>>", lReg("CANTIDAD").Value) 'CAMBIA LOS NEMOTECNICOS
    
    Set lcCortes_Cupon_Cuenta = New Class_Cortes_Cupon_Cuenta
    With lcCortes_Cupon_Cuenta
      .Campo("ID_CORTE_CUPON").Valor = lReg("ID_CORTE_CUPON").Value
      .Campo("ID_MOV_ACTIVO").Valor = lReg("ID_MOV_ACTIVO").Value
      .Campo("ID_MOV_CAJA").Valor = Null 'Lo dejo en nulo para que la clase cuando se grabe agregue un movimiento de caja de forma automatica
      .Campo("FCH_CORTE_CUPON").Valor = lFch_Corte_Cupon
      .Campo("INTERES").Valor = lInteres
      .Campo("AMORTIZACION").Valor = lAmortizacion
      .Campo("MONTO").Valor = Round(lInteres + lAmortizacion)
      .Campo("DSC_CORTE_CUPON_CUENTA").Valor = lDsc_Origen_Mov_Caja_Cuenta
      If Not .Guardar Then
        fClass_Entidad.ErrMsg = .ErrMsg
        fClass_Entidad.Errnum = .Errnum
        GoTo ExitProcedure
      End If
    End With
    Set lcCortes_Cupon_Cuenta = Nothing
  Next

  Ejecuta_x_Cuenta = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcSaldo_Activo = Nothing
  Set lcCortes_Cupon_Cuenta = Nothing
  Set lcMov_Caja_Origen = Nothing
  Set lcTipo_Cambio = Nothing
End Function

Public Function Deshacer_Enlaze_Cierre(pId_Cuenta, pFecha_Cierre) As Boolean
Dim lcCortes_Cupon_Cuenta As Class_Cortes_Cupon_Cuenta
Dim lReg                  As hFields
Dim lCursor               As hRecord
'--------------------------------------------------------------------------
Dim lFch_Corte_Cupon As Date
  
On Error GoTo ErrProcedure

  Deshacer_Enlaze_Cierre = False
  
  lFch_Corte_Cupon = pFecha_Cierre + 1

  'Busca todos los cortes que caen ese dia.
  Set lcCortes_Cupon_Cuenta = New Class_Cortes_Cupon_Cuenta
  With lcCortes_Cupon_Cuenta
    .Campo("FCH_CORTE_CUPON").Valor = lFch_Corte_Cupon
    If Not .Buscar(pId_Cuenta) Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  End With
  

  Set lCursor = lcCortes_Cupon_Cuenta.Cursor
   
  For Each lReg In lCursor
    Set lcCortes_Cupon_Cuenta = New Class_Cortes_Cupon_Cuenta
    With lcCortes_Cupon_Cuenta
      .Campo("ID_CORTE_CUPON_CUENTA").Valor = lReg("ID_CORTE_CUPON_CUENTA").Value
      .Campo("id_mov_caja").Valor = lReg("id_mov_caja").Value
    
      If Not .Borrar Then
        fClass_Entidad.ErrMsg = .ErrMsg
        fClass_Entidad.Errnum = .Errnum
        GoTo ExitProcedure
      End If
    End With
    Set lcCortes_Cupon_Cuenta = Nothing
  Next

  Deshacer_Enlaze_Cierre = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lCursor = Nothing
  Set lcCortes_Cupon_Cuenta = Nothing
End Function
Public Function Busca_Cortes_Cupon_Pagados(ByVal pId_Cuenta As Double, _
                                            ByVal pFecha As Date, _
                                            ByRef pValor As Double) As Boolean
'**************************************************************************************
  With gDB
    .Parametros.Clear
    .Parametros.Add "PID_CUENTA", ePT_Numero, pId_Cuenta, ePD_Entrada
    .Parametros.Add "PFECHA", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "PVALOR", ePT_Numero, "", ePD_Salida
    .Procedimiento = "PKG_CORTES_CUPON_CUENTA.TOTAL_PAGADOS"
    If .EjecutaSP Then
      pValor = .Parametros("PVALOR").Valor
      'Fnt_Porcentaje_RV_RF = True
      Busca_Cortes_Cupon_Pagados = True
    Else
      pValor = 0
      'Fnt_Porcentaje_RV_RF = False
      Busca_Cortes_Cupon_Pagados = False
    End If
    .Parametros.Clear
  End With
'********************************************************************
End Function
