VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Perfiles_Riesgo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Perfiles_Riesgo"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLog_Subtipo.eLS_Perfiles_Riesgo
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Perfil_Riesgo"
    .AddCampo "Id_Perfil_Riesgo", ePT_Numero, ePD_Ambos
    .AddCampo "Id_Empresa", ePT_Numero, ePD_Entrada
    .AddCampo "Dsc_Perfil_Riesgo", ePT_Caracter, ePD_Entrada
    .AddCampo "Abr_Perfil_Riesgo", ePT_Caracter, ePD_Entrada
    .AddCampo "Porc_Salto_Cuota", ePT_Numero, ePD_Entrada
    .AddCampo "Flg_Perfil_Default", ePT_Caracter, ePD_Entrada
  End With
End Sub

Public Function Buscar(Optional pEnVista As Boolean = False)
Dim lProc As String

  If pEnVista Then
    lProc = ".BuscarView"
  Else
    lProc = ".Buscar"
  End If
  
  Buscar = fClass_Entidad.Buscar(cfPackage & lProc)
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Borrar()
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function
