VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_AvanceGuion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum eCaracteres
  eC_Guion
  eC_BackSlach
  eC_Pipe
  eC_Slach
End Enum

Dim fGuionActual As eCaracteres

Public Function Avanza() As String
  fGuionActual = fGuionActual + 1
  Avanza = Fnt_QueCaracter
End Function

Private Function Fnt_QueCaracter() As String
  Select Case fGuionActual
    Case eC_Guion
      Fnt_QueCaracter = "-"
    Case eC_BackSlach
      Fnt_QueCaracter = "\"
    Case eC_Pipe
      Fnt_QueCaracter = "|"
    Case eC_Slach
      Fnt_QueCaracter = "/"
    Case Else
      fGuionActual = eC_Guion
      Fnt_QueCaracter = Fnt_QueCaracter()
  End Select
End Function
