VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Valor_Tipo_Cambio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "PKG_VALOR_TIPO_CAMBIO"

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Valor_Tipo_Cambio"
    .AddCampo "Id_Valor_Tipo_Cambio", ePT_Numero, ePD_Ambos
    .AddCampo "Id_Tipo_Cambio", ePT_Numero, ePD_Entrada
    .AddCampo "Fecha", ePT_Fecha, ePD_Entrada
    .AddCampo "Valor", ePT_Numero, ePD_Entrada
    .AddCampo "OrigenConver", ePT_Caracter, ePD_Entrada
    .AddCampo "TipoConver", ePT_Caracter, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Guardar() As Boolean
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

'---------------------------------------------------------------------------------------
' Procedure : TraerIndicadores
' Author    : Administrador
' Date      : 11/07/2008
' Purpose   : Traer los indicadores,
'---------------------------------------------------------------------------------------
Public Function BuscarIndicadores() As Boolean
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "PFecha", ePT_Fecha, Campo("Fecha").Valor, ePD_Entrada
    
    gDB.Procedimiento = cfPackage & ".BuscarIndicadores"
    
    If Not gDB.EjecutaSP Then
        BuscarIndicadores = False
        Exit Function
    End If
    
    Set fClass_Entidad.Cursor = gDB.Parametros("Pcursor").Valor
    
    BuscarIndicadores = True
    
End Function

