VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Solicitud_RentaFija"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "Pkg_Solicitud_Inversion_RentaFija"
Dim fCursor As hRecord
Dim fErrNum As Double
Dim fErrMsg As String

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 34
End Function
'---------------------------------------------------------

Public Sub Sub_CopiarReg2Campos(pReg As hFields)
  Call fClass_Entidad.Sub_CopiarReg2Campos(pReg)
End Sub

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
Attribute Campo.VB_UserMemId = 0
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_SOLICITUD"
    .AddCampo "ID_SOLICITUD", ePT_Numero, ePD_Ambos
    .AddCampo "COD_INSTRUMENTO", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_SUBFAMILIA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_EMISOR", ePT_Numero, ePD_Entrada
    .AddCampo "ID_TRADER", ePT_Numero, ePD_Entrada
    .AddCampo "PLAZO", ePT_Numero, ePD_Entrada
    .AddCampo "TIPO_PLAZO", ePT_Caracter, ePD_Entrada
    .AddCampo "RANGO", ePT_Numero, ePD_Entrada
    .AddCampo "DURATION", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_ORDEN", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MONEDA", ePT_Numero, ePD_Entrada
    .AddCampo "OBSERVACIONES", ePT_Caracter, ePD_Entrada
    .AddCampo "FECHA_SOLICITUD", ePT_Fecha, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Guardar() As Boolean
   Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Property Let CampoPKey(ByVal vData As String)
  fClass_Entidad.CampoPKey = vData
End Property

Friend Property Get CampoPKey() As String
  CampoPKey = fClass_Entidad.CampoPKey
End Property

Public Function Buscar_EntreFecha(ldFechaIni, ldFechaTer, Optional p_buscar As Boolean = False) As Boolean
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "PFecha_Desde", ePT_Fecha, ldFechaIni, ePD_Entrada
    gDB.Parametros.Add "PFecha_Hasta", ePT_Fecha, ldFechaTer, ePD_Entrada
    gDB.Procedimiento = cfPackage & ".buscar_entre"
    
    If Not gDB.EjecutaSP Then
        Buscar_EntreFecha = False
        MsgBox "Problemas al traer las Solictudes de Renta Fija.", vbCritical
        Exit Function
    End If
    Set fClass_Entidad.Cursor = gDB.Parametros("Pcursor").Valor
    If fClass_Entidad.Cursor.Count <= 0 Then
        If p_buscar Then
            MsgBox "No se encontró información Solicitada.", vbExclamation
        End If
        Buscar_EntreFecha = False
        Exit Function
    End If
    Buscar_EntreFecha = True
End Function


