VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Ingresos_Mensual_Cuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Ingresos_Mensual_Cuenta"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 31
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_INGRESO_MENSUAL_CTA"
    .AddCampo "ID_INGRESO_MENSUAL_CTA", ePT_Numero, ePD_Ambos
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_ING_MSN_C_ONCEPTO", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MONEDA", ePT_Numero, ePD_Entrada
    .AddCampo "MES", ePT_Numero, ePD_Entrada
    .AddCampo "ANO", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO", ePT_Numero, ePD_Entrada
    .AddCampo "TIPO_ALCANCE", ePT_Caracter, ePD_Entrada
    .AddCampo "TIPO_INGRESO", ePT_Caracter, ePD_Entrada
    .AddCampo "MARCA_ADICIONAL", ePT_Caracter, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function


Public Function Buscar_X_Empresa(pId_Empresa) As Boolean
Dim lClass_Entidad As New Class_Entidad

  Buscar_X_Empresa = False

  With lClass_Entidad
    .AddCampo "id_empresa", ePT_Numero, ePD_Entrada
    
    .Campo("id_empresa").Valor = pId_Empresa
    
    If .Buscar(cfPackage & ".BUSCARVIEW") Then
      Buscar_X_Empresa = True
      Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
    Else
      fClass_Entidad.Errnum = lClass_Entidad.Errnum
      fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
    End If
  End With
  Set lClass_Entidad = Nothing

End Function

