VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_AporteRetiro_APV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad

Dim fCampos As Parametros
Dim fErrNum As Double
Dim fErrMsg As String
Dim fCursor As hRecord

Const cfPackage = "Pkg_Aporte_Rescate_Cta"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 6
End Function
'---------------------------------------------------------

Public Function Buscar(Optional pEnVista As Boolean = False)
Dim lParam  As DLL_COMUN.Parametro
  Buscar = True
  
  gDB.Parametros.Clear
  For Each lParam In fCampos
    'Primero verifica que ningun campo este en null
    If Not IsNull(lParam.Valor) Then
      'Se les agrega la "p" debido a que son parametros
      Call gDB.Parametros.Add("p" & lParam.Nombre, lParam.tipo, lParam.Valor, lParam.Direccion)
    End If
  Next
  
'---------------------------------------------------------------------
'-- Se agrega el parametro de Cursor
'---------------------------------------------------------------------
  Call gDB.Parametros.Add("Pcursor", ePT_Cursor, "", ePD_Ambos)
'---------------------------------------------------------------------

  If pEnVista Then
    gDB.Procedimiento = cfPackage & ".BuscarView"
  Else
    gDB.Procedimiento = cfPackage & ".Buscar"
  End If
  
  If gDB.EjecutaSP Then
    Set fCursor = gDB.Parametros("Pcursor").Valor
  Else
    'Si existe un problema el proceso devuelve false
    Buscar = False
    fErrNum = gDB.Errnum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Guardar(Optional pAutomatico As Boolean = False, _
                        Optional pEsConfirmacion As Boolean = False, _
                        Optional pFechaMovimiento As String = "") As Boolean
Dim lParam  As DLL_COMUN.Parametro
Dim lId_Mov_Caja As String
Dim lId_Mov_Caja_Ret As String
Dim lReg As hFields
Dim lCod_Estado As String
Dim lCodEstado_R_TS As String
'-------------
Dim lAporte_Rescate_Cuenta As Class_APORTE_RESCATE_CUENTA
Dim lMov_Caja As New Class_Mov_Caja


  Guardar = True

  lId_Mov_Caja = cNewEntidad
  lId_Mov_Caja_Ret = cNewEntidad
  
'  lCod_Estado = "P" 'El estado por omision de un aporte o traspaso de entrada es "P"endiente
  lCodEstado_R_TS = IIf(pEsConfirmacion, "P", "A")
  If fCampos("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Aporte Or _
     fCampos("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Traspaso_Entrada Or _
     pEsConfirmacion Then
    lCod_Estado = "P" 'El estado por omision de un aporte o traspaso de entrada es "P"endiente
  Else
    lCod_Estado = "S" 'El estado por omision de un retiro o traspaso de saliad es "S"olicitado
  End If
  If Not fCampos("id_Apo_Res_Cuenta").Valor = cNewEntidad Then
    Set lAporte_Rescate_Cuenta = New Class_APORTE_RESCATE_CUENTA
    lAporte_Rescate_Cuenta.Campo("id_Apo_Res_Cuenta").Valor = fCampos("id_Apo_Res_Cuenta").Valor
    If lAporte_Rescate_Cuenta.Buscar Then
      For Each lReg In lAporte_Rescate_Cuenta.Cursor
        lId_Mov_Caja = lReg("id_mov_caja").Value
        If fCampos("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Rescate Then
            lId_Mov_Caja_Ret = NVL(lReg("id_mov_caja_retencion").Value, "")
        End If
        If Not pEsConfirmacion Then
            lCod_Estado = lReg("cod_estado").Value
        End If
      Next
    Else
      'Si existe un problema el proceso devuelve false
      Guardar = False
      fErrNum = gDB.Errnum
      fErrMsg = gDB.ErrMsg
      GoTo ErrProcedure
    End If
  End If
  
  
  '---------------------------------------------
  '-- Agrega o modifica el movimiento de caja
  '-- Si es aporte se graba con el monto indicado del aporte
  '-- Si es Retiro, se graban dos l�neas, una indicando el monto con la retenci�n descontada y otra
  '-- l�nea indicando la retenci�n.
  '---------------------------------------------
    If fCampos("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Aporte Or _
        fCampos("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Traspaso_Entrada Then
        With lMov_Caja
          .Campo("id_mov_caja").Valor = lId_Mov_Caja
          .Campo("Id_Caja_Cuenta").Valor = fCampos("Id_Caja_Cuenta").Valor
          .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_APO_RES
          .Campo("Fecha_Movimiento").Valor = fCampos("fecha_movimiento").Valor
          .Campo("Fecha_Liquidacion").Valor = (fCampos("fecha_movimiento").Valor + fCampos("Retencion").Valor)
              
          .Campo("Monto").Valor = fCampos("monto").Valor
          .Campo("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Abono
          .Campo("Dsc_Mov_Caja").Valor = fCampos("Dsc_Apo_Res_Cuenta").Valor
          If pAutomatico Then
            .Campo("Flg_Mov_Automatico").Valor = gcFlg_SI
          Else
            .Campo("Flg_Mov_Automatico").Valor = gcFlg_NO
          End If
          If Not .Guardar Then
            'Si existe un problema el proceso devuelve false
            Guardar = False
            fErrNum = gDB.Errnum
            fErrMsg = gDB.ErrMsg
            GoTo ErrProcedure
          End If
          
          lId_Mov_Caja = .Campo("id_mov_caja").Valor
        End With
Rem Si es un Retiro o un Traspaso de Salida se ingresa como Solicitud por lo que no se graba en MOV_CAJA
Rem cuando se confirmen pasar�n a esta tabla
    Else
        If fCampos("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Traspaso_Salida Then
        With lMov_Caja
          .Campo("id_mov_caja").Valor = lId_Mov_Caja
          .Campo("Id_Caja_Cuenta").Valor = fCampos("Id_Caja_Cuenta").Valor
          .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_APO_RES
          .Campo("Fecha_Movimiento").Valor = Fnt_String2Date(pFechaMovimiento) 'fCampos("fecha_movimiento").Valor
          .Campo("Fecha_Liquidacion").Valor = (fCampos("fecha_movimiento").Valor + fCampos("Retencion").Valor)
              
          .Campo("Monto").Valor = fCampos("monto").Valor
          .Campo("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Cargo
          .Campo("Dsc_Mov_Caja").Valor = fCampos("Dsc_Apo_Res_Cuenta").Valor
          If pAutomatico Then
            .Campo("Flg_Mov_Automatico").Valor = gcFlg_SI
          Else
            .Campo("Flg_Mov_Automatico").Valor = gcFlg_NO
          End If
          
          If Not lMov_Caja.Guardar(pCodEstadoAPV:=lCodEstado_R_TS) Then
            'Si existe un problema el proceso devuelve false
            Guardar = False
            fErrNum = gDB.Errnum
            fErrMsg = gDB.ErrMsg
            GoTo ErrProcedure
          End If
          
          lId_Mov_Caja = .Campo("id_mov_caja").Valor
        End With
                
        Else
            With lMov_Caja
              .Campo("id_mov_caja").Valor = lId_Mov_Caja
              .Campo("Id_Caja_Cuenta").Valor = fCampos("Id_Caja_Cuenta").Valor
              .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_APO_RES
              .Campo("Fecha_Movimiento").Valor = Fnt_String2Date(pFechaMovimiento) 'fCampos("fecha_movimiento").Valor
              .Campo("Fecha_Liquidacion").Valor = (fCampos("fecha_movimiento").Valor + fCampos("Retencion").Valor)
    
              .Campo("Monto").Valor = fCampos("monto").Valor - fCampos("monto_retenido_apv").Valor
              .Campo("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Cargo
              .Campo("Dsc_Mov_Caja").Valor = fCampos("Dsc_Apo_Res_Cuenta").Valor
              If pAutomatico Then
                .Campo("Flg_Mov_Automatico").Valor = gcFlg_SI
              Else
                .Campo("Flg_Mov_Automatico").Valor = gcFlg_NO
              End If
              If Not .Guardar(pCodEstadoAPV:=lCodEstado_R_TS) Then
                'Si existe un problema el proceso devuelve false
                Guardar = False
                fErrNum = gDB.Errnum
                fErrMsg = gDB.ErrMsg
                GoTo ErrProcedure
              End If
    
              lId_Mov_Caja = .Campo("id_mov_caja").Valor
            End With
            'Graba Retencion
            With lMov_Caja
              .Campo("id_mov_caja").Valor = lId_Mov_Caja_Ret
              .Campo("Id_Caja_Cuenta").Valor = fCampos("Id_Caja_Cuenta").Valor
              .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_APO_RES
              .Campo("Fecha_Movimiento").Valor = fCampos("fecha_movimiento").Valor
              .Campo("Fecha_Liquidacion").Valor = (fCampos("fecha_movimiento").Valor + fCampos("Retencion").Valor)
    
              .Campo("Monto").Valor = fCampos("monto_retenido_apv").Valor
              .Campo("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Cargo
'              .Campo("Dsc_Mov_Caja").Valor = "RETENCION LEGAL 15% POR RETIRO APV"
              .Campo("Dsc_Mov_Caja").Valor = "RETENCION IMPUESTO 15% POR RETIRO APV"
              If pAutomatico Then
                .Campo("Flg_Mov_Automatico").Valor = gcFlg_SI
              Else
                .Campo("Flg_Mov_Automatico").Valor = gcFlg_NO
              End If
              If Not .Guardar(pCodEstadoAPV:=lCodEstado_R_TS) Then
                'Si existe un problema el proceso devuelve false
                Guardar = False
                fErrNum = gDB.Errnum
                fErrMsg = gDB.ErrMsg
                GoTo ErrProcedure
              End If
    
              lId_Mov_Caja_Ret = .Campo("id_mov_caja").Valor
            End With
        End If
    End If
  gDB.Parametros.Clear
  gDB.Procedimiento = cfPackage & ".Guardar"
  For Each lParam In fCampos
    'Se les agrega la "p" debido a que son parametros
    Call gDB.Parametros.Add("P" & lParam.Nombre, lParam.tipo, NVL(lParam.Valor, ""), lParam.Direccion)
  Next
  '---------------------------------
  'Solo se puede actualizar el mov_caja mediante los procesos de liquidacion o de eliminacion
  gDB.Parametros("pid_mov_caja").Valor = lId_Mov_Caja
  'APV
  If fCampos("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Rescate Then
    gDB.Parametros("pid_mov_caja_retencion").Valor = lId_Mov_Caja_Ret
  End If
  '---------------------------------
  '---------------------------------
  'Solo se puede actualizar el mov_caja mediante los procesos de liquidacion o de eliminacion
  gDB.Parametros("pcod_estado").Valor = lCod_Estado
  '---------------------------------
  
  If Not gDB.EjecutaSP Then
    'Si existe un problema el proceso devuelve false
    Guardar = False
    fErrNum = gDB.Errnum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If
  
  fCampos("id_Apo_Res_Cuenta").Valor = gDB.Parametros("pid_Apo_Res_Cuenta").Valor
  
ErrProcedure:
  gDB.Parametros.Clear
End Function

Public Function Campo(pCampo As String) As Parametro
  Set Campo = fCampos(pCampo)
End Function

Private Sub Class_Initialize()
  
  Set fCampos = New Parametros
  Set fCursor = Nothing
  
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fCampos
    .Clear
    .Add "id_Apo_Res_Cuenta", ePT_Numero, Null, ePD_Ambos
    .Add "Cod_Medio_Pago", ePT_Caracter, Null, ePD_Entrada
    .Add "Id_Banco", ePT_Numero, Null, ePD_Entrada
    .Add "Id_Caja_Cuenta", ePT_Numero, Null, ePD_Entrada
    .Add "Id_Cuenta", ePT_Numero, Null, ePD_Entrada
    .Add "Dsc_Apo_Res_Cuenta", ePT_Caracter, Null, ePD_Entrada
    .Add "Flg_Tipo_Movimiento", ePT_Caracter, Null, ePD_Entrada
    .Add "Fecha_Movimiento", ePT_Fecha, Null, ePD_Entrada
    .Add "Num_Documento", ePT_Caracter, Null, ePD_Entrada
    .Add "Retencion", ePT_Numero, Null, ePD_Entrada
    .Add "Monto", ePT_Numero, Null, ePD_Entrada
    .Add "Cta_Cte_Bancaria", ePT_Caracter, Null, ePD_Entrada
    .Add "Cod_Estado", ePT_Caracter, Null, ePD_Entrada
    .Add "id_mov_caja", ePT_Numero, Null, ePD_Entrada
    .Add "comision_apv", ePT_Numero, Null, ePD_Entrada
    .Add "monto_retenido_apv", ePT_Numero, Null, ePD_Entrada
    .Add "id_mov_caja_retencion", ePT_Numero, Null, ePD_Entrada
    .Add "id_tipo_ahorro", ePT_Numero, Null, ePD_Entrada
    .Add "total_cuotas_apv", ePT_Numero, Null, ePD_Entrada
    .Add "id_institucion_previsional", ePT_Numero, Null, ePD_Entrada
    .Add "retiro_total_parcial", ePT_Caracter, Null, ePD_Entrada
    .Add "confirmacion_apv", ePT_Caracter, Null, ePD_Entrada
  End With
End Sub

Public Function Errnum() As Double
  Errnum = fErrNum
End Function

Public Function ErrMsg() As String
  ErrMsg = fErrMsg
End Function

Public Function Cursor() As hRecord
  'Set Cursor = fClass_Entidad.Cursor
  Set Cursor = fCursor
End Function

Public Function Eliminar() As Boolean
Dim lParam  As DLL_COMUN.Parametro
Dim lAporte_Rescate_Cuenta As Class_APORTE_RESCATE_CUENTA
Dim lMov_Caja As Class_Mov_Caja
Dim lReg As hFields
Dim lBorrar As hRecord

  Eliminar = True
  
  Set lBorrar = New hRecord
  Call lBorrar.AddField("ID")
  
  '--------------------------------------------
  '-- Primero se debe eliminar el movimiento de caja
  '--------------------------------------------
  gDB.Parametros.Clear
  Set lAporte_Rescate_Cuenta = New Class_APORTE_RESCATE_CUENTA
  With lAporte_Rescate_Cuenta
    For Each lParam In fCampos
      'Primero verifica que ningun campo este en null
      If Not IsNull(lParam.Valor) Then
        'Se les agrega la "p" debido a que son parametros
        .Campo(lParam.Nombre).Valor = lParam.Valor
      End If
    Next
    
    If .Buscar Then
      'Almacena todos los ID de la mov_caja
      For Each lReg In .Cursor
        lBorrar.Add.Fields("ID").Value = lReg("id_mov_caja").Value
        Select Case lReg("cod_estado").Value
          Case cCod_Estado_Liquidado
            Eliminar = False
            fErrNum = -1
            fErrMsg = "No se pueden eliminar el aporte/retiro APV N� " & lReg("Id_Apo_Res_Cuenta").Value & ", por que, ya esta liquidado."
            GoTo ErrProcedure
        End Select
      Next
    End If
  End With
  
  gDB.Parametros.Clear
  For Each lParam In fCampos
    'Primero verifica que ningun campo este en null
    If Not IsNull(lParam.Valor) Then
      'Se les agrega la "p" debido a que son parametros
      Call gDB.Parametros.Add("p" & lParam.Nombre, lParam.tipo, lParam.Valor, lParam.Direccion)
    End If
  Next
  
  gDB.Procedimiento = cfPackage & ".Borrar"
  
  If Not gDB.EjecutaSP Then
    'Si existe un problema el proceso devuelve false
    Eliminar = False
    fErrNum = gDB.Errnum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If


  For Each lReg In lBorrar
    Set lMov_Caja = New Class_Mov_Caja
    With lMov_Caja
      .Campo("id_mov_caja").Valor = lReg("ID").Value
      If Not .Eliminar Then
        Eliminar = False
        fErrNum = gDB.Errnum
        fErrMsg = gDB.ErrMsg
        GoTo ErrProcedure
      End If
    End With
    Set lMov_Caja = Nothing
  Next

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Anular(Pid_Apo_Res_Cuenta) As Boolean
On Error GoTo ErrProcedure
Dim lReg As hFields
  
  Anular = True

  gDB.Parametros.Clear
  gDB.Procedimiento = cfPackage & ".Anular"
  gDB.Parametros.Add "Pid_Apo_Res_Cuenta", ePT_Numero, Pid_Apo_Res_Cuenta, ePD_Entrada
  If Not gDB.EjecutaSP Then
    Anular = False
    fErrNum = gDB.Errnum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If
  gDB.Parametros.Clear

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Anular_X_Mov_Caja(Pid_Mov_Caja) As Boolean
On Error GoTo ErrProcedure
Dim lReg As hFields
Dim lAporte_Rescate_Cuenta As New Class_APORTE_RESCATE_CUENTA
Dim lId_Apo_Res_Cuenta
  
  Anular_X_Mov_Caja = True

  'Se busca el id del aporte/rescate mediante el id de la mov_caja
  With lAporte_Rescate_Cuenta
    .Campo("id_mov_caja").Valor = Pid_Mov_Caja
    If .Buscar Then
      For Each lReg In .Cursor
        lId_Apo_Res_Cuenta = lReg("Id_Apo_Res_Cuenta").Value
      Next
    Else
      Anular_X_Mov_Caja = False
      fErrNum = gDB.Errnum
      fErrMsg = gDB.ErrMsg
      GoTo ErrProcedure
    End If
  End With

  'Ahora anula mediate el Metodo
  If Not Me.Anular(lId_Apo_Res_Cuenta) Then
    Anular_X_Mov_Caja = False
    fErrNum = gDB.Errnum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Liquidar(Pid_Apo_Res_Cuenta) As Boolean
On Error GoTo ErrProcedure
Dim lReg As hFields
  
  Liquidar = True

  gDB.Parametros.Clear
  gDB.Procedimiento = cfPackage & ".Liquidar"
  gDB.Parametros.Add "Pid_Apo_Res_Cuenta", ePT_Numero, Pid_Apo_Res_Cuenta, ePD_Entrada
  If Not gDB.EjecutaSP Then
    Liquidar = False
    fErrNum = gDB.Errnum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If
  gDB.Parametros.Clear

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Liquidar_X_Mov_Caja(Pid_Mov_Caja) As Boolean
On Error GoTo ErrProcedure
Dim lReg As hFields
Dim lAporte_Rescate_Cuenta As New Class_APORTE_RESCATE_CUENTA
Dim lId_Apo_Res_Cuenta
  
  Liquidar_X_Mov_Caja = True

  'Se busca el id del aporte/rescate mediante el id de la mov_caja
  With lAporte_Rescate_Cuenta
    .Campo("id_mov_caja").Valor = Pid_Mov_Caja
    If .Buscar Then
      For Each lReg In .Cursor
        lId_Apo_Res_Cuenta = lReg("Id_Apo_Res_Cuenta").Value
      Next
    Else
      Liquidar_X_Mov_Caja = False
      fErrNum = gDB.Errnum
      fErrMsg = gDB.ErrMsg
      GoTo ErrProcedure
    End If
  End With

  'Ahora anula mediate el Metodo
  If Not Me.Liquidar(lId_Apo_Res_Cuenta) Then
    Liquidar_X_Mov_Caja = False
    fErrNum = gDB.Errnum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function
Public Function Buscar_Descendente()
Dim lParam  As DLL_COMUN.Parametro
  Buscar_Descendente = True
  
  gDB.Parametros.Clear
  For Each lParam In fCampos
    'Primero verifica que ningun campo este en null
    If Not IsNull(lParam.Valor) Then
      'Se les agrega la "p" debido a que son parametros
      Call gDB.Parametros.Add("p" & lParam.Nombre, lParam.tipo, lParam.Valor, lParam.Direccion)
    End If
  Next
  
'---------------------------------------------------------------------
'-- Se agrega el parametro de Cursor
'---------------------------------------------------------------------
  Call gDB.Parametros.Add("Pcursor", ePT_Cursor, "", ePD_Ambos)
'---------------------------------------------------------------------

  gDB.Procedimiento = cfPackage & ".Buscar_Entre_Descendente"
  
  If gDB.EjecutaSP Then
    Set fCursor = gDB.Parametros("Pcursor").Valor
  Else
    'Si existe un problema el proceso devuelve false
    Buscar_Descendente = False
    fErrNum = gDB.Errnum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function
Public Function Buscar_AporteRescatePorFecha(lId_Cuenta, ldFechaIni, ldFechaTer, Optional p_buscar As Boolean = False) As Boolean
    'Dim lCursor As hRecord
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_Cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    gDB.Parametros.Add "PFecha_Movimiento_ini", ePT_Fecha, ldFechaIni, ePD_Entrada
    gDB.Parametros.Add "PFecha_Movimiento_ter", ePT_Fecha, ldFechaTer, ePD_Entrada
    
    gDB.Procedimiento = cfPackage & ".buscarview"
    
    If Not gDB.EjecutaSP Then
        Buscar_AporteRescatePorFecha = False
        MsgBox "Problemas al traer los Aportes y Rescates.", vbCritical
        Exit Function
    End If
    Set fCursor = gDB.Parametros("Pcursor").Valor
    If fCursor.Count <= 0 Then
        If p_buscar Then
            MsgBox "No se encontr� informaci�n Solicitada.", vbExclamation
        End If
        Buscar_AporteRescatePorFecha = False
        Exit Function
    End If
    
    Buscar_AporteRescatePorFecha = True
    
End Function


