VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_BusquedaPorActivos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Busqueda_Por_Activos"

Public Id_Nemotecnico As Integer
Public Fecha_Consulta As Date
Public Cod_Producto   As String
Public Cod_Arbol_clase As String
Public Id_Moneda_Nemo As Integer
Public Dsc_Moneda_Nemo As String
Public Decimales As Integer
'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 35
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_NEMOTECNICO"
    .AddCampo "ID_NEMOTECNICO", ePT_Numero, ePD_Entrada
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "COD_INSTRUMENTO", ePT_Caracter, ePD_Entrada
    .AddCampo "FECHA_CIERRE", ePT_Fecha, ePD_Entrada
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar_Activos()
  Buscar_Activos = fClass_Entidad.Buscar(cfPackage & ".Buscar_Activos")
End Function

Public Function Buscar_Cuentas()
  Buscar_Cuentas = fClass_Entidad.Buscar(cfPackage & ".Buscar_Cuentas_Por_Nemo")
End Function



