VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Regiones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
    
Const cfPackage = "Pkg_Regiones"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 16
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
    Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
    Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
    ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
    Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
    Set fClass_Entidad = New Class_Entidad
    Call LimpiaParam
End Sub

Public Sub LimpiaParam()
    With fClass_Entidad
        .LimpiaParam
        .CampoPKey = "Id_Region"
        .AddCampo "Id_Region", ePT_Numero, ePD_Ambos
        .AddCampo "cod_pais", ePT_Caracter, ePD_Entrada
        .AddCampo "Dsc_Region", ePT_Caracter, ePD_Entrada
    End With
End Sub

Public Function Buscar(Optional pEnVista As Boolean = False) As Boolean
    Dim lProc As String
    Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Borrar() As Boolean
    Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Guardar()
    Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

