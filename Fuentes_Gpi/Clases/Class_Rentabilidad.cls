VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Rentabilidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "PKG_RENTABILIDAD"

Dim fSubTipo_LOG As Double

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = fSubTipo_LOG
End Function
'---------------------------------------------------------

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
End Sub

Private Sub Class_Terminate()
  Set fClass_Entidad = Nothing
End Sub

Public Function MultiCuota_Cuenta(pFecha_Desde, pFecha_Hasta) As Boolean
Dim lClass_Entidad As Class_Entidad
  
  fSubTipo_LOG = eLog_Subtipo.eLS_Calc_MultiCuota
  
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "fecha_desde", ePT_Fecha, ePD_Entrada, pFecha_Desde
    .AddCampo "fecha_hasta", ePT_Fecha, ePD_Entrada, pFecha_Hasta

    MultiCuota_Cuenta = .Buscar(cfPackage & ".MULTICUOTA_CUENTAS")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
End Function

Public Function Volatilidad(ByRef pVolatildiad) As Boolean
Dim lClass_Entidad As Class_Entidad
  
  fSubTipo_LOG = eLog_Subtipo.eLS_Calc_Volatilidad
  
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    Volatilidad = .Buscar(cfPackage & ".Volatilidad")
    
    If Not .Cursor Is Nothing Then
      pVolatildiad = .Cursor(1)("volatilidad").Value
    End If
    
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
End Function

