VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_CortesRF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "Pkg_Cortes_RF"
Dim fCursor As hRecord
Dim fErrNum As Double
Dim fErrMsg As String

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 34
End Function
'---------------------------------------------------------

Public Sub Sub_CopiarReg2Campos(pReg As hFields)
  Call fClass_Entidad.Sub_CopiarReg2Campos(pReg)
End Sub

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Corte"
    .AddCampo "Id_Corte", ePT_Numero, ePD_Ambos
    .AddCampo "Id_Operacion_Detalle_compra", ePT_Numero, ePD_Entrada
    .AddCampo "nominales", ePT_Numero, ePD_Entrada
    .AddCampo "Fecha_Venta", ePT_Fecha, ePD_Entrada
    .AddCampo "Id_Operacion_Detalle_venta", ePT_Numero, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Guardar() As Boolean

  If Not fClass_Entidad.Guardar(cfPackage & ".Guardar") Then
    Guardar = False
    GoTo ErrProcedure
  End If
  
  Guardar = True

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Property Let CampoPKey(ByVal vData As String)
  fClass_Entidad.CampoPKey = vData
End Property

Friend Property Get CampoPKey() As String
  CampoPKey = fClass_Entidad.CampoPKey
End Property

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Buscar()
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function BuscarDisponibles()

  BuscarDisponibles = fClass_Entidad.Buscar(cfPackage & ".BuscarDisponibles")
End Function

Public Function GuardarCorteVenta(pNumeroCortes As Long)
  With fClass_Entidad
    If Not IsMissing(pNumeroCortes) Then
      .AddCampo "NumeroCortes", ePT_Numero, ePD_Entrada, pNumeroCortes
    End If
  End With
  

  If Not fClass_Entidad.Guardar(cfPackage & ".GuardarVentaCorte") Then
    GuardarCorteVenta = False
    GoTo ErrProcedure
  End If
  
  GuardarCorteVenta = True

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Anular(pId_Operacion As Long)
  
  gDB.Parametros.Clear
  Call gDB.Parametros.Add("Pid_Operacion", ePT_Numero, pId_Operacion, ePD_Entrada)
  gDB.Procedimiento = cfPackage & ".Anular"
  If Not gDB.EjecutaSP Then
    Anular = False
    fErrMsg = gDB.ErrMsg
    fErrNum = gDB.Errnum
  End If
  gDB.Parametros.Clear
End Function


Public Function BuscarCorteActual()

  BuscarCorteActual = fClass_Entidad.Buscar(cfPackage & ".BuscarCorteActual")
End Function
