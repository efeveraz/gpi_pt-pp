VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Operaciones_Detalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Rem 17/07/2009 MMardones, agrega en el ingreso de la operaci�n el tipo de
Rem            inversi�n, Si es por Monto o por Nominales
Rem -----------------------------------------------------------------

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Operaciones_Detalle"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLog_Subtipo.eLS_Operaciones_Detalle
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    
    .CampoPKey = "Id_Operacion_Detalle"
    .AddCampo "Id_Operacion_Detalle", ePT_Numero, ePD_Ambos
    .AddCampo "Id_Operacion", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada
    .AddCampo "Cantidad", ePT_Numero, ePD_Entrada
    .AddCampo "Precio", ePT_Numero, ePD_Entrada
    .AddCampo "Precio_Gestion", ePT_Numero, ePD_Entrada
    .AddCampo "Plazo", ePT_Numero, ePD_Entrada
    .AddCampo "Base", ePT_Numero, ePD_Entrada
    .AddCampo "Fecha_Vencimiento", ePT_Fecha, ePD_Entrada
    .AddCampo "Id_Moneda_Pago", ePT_Numero, ePD_Entrada
    .AddCampo "Valor_Divisa", ePT_Numero, ePD_Entrada
    .AddCampo "Monto_Pago", ePT_Numero, ePD_Entrada
    .AddCampo "Comision", ePT_Numero, ePD_Entrada
    .AddCampo "Interes", ePT_Numero, ePD_Entrada
    .AddCampo "Utilidad", ePT_Numero, ePD_Entrada
    .AddCampo "Porc_Comision", ePT_Numero, ePD_Entrada
    .AddCampo "Monto_Bruto", ePT_Numero, ePD_Entrada
    .AddCampo "FLG_MONTO_REFERENCIADO", ePT_Caracter, ePD_Entrada
    .AddCampo "FLG_TIPO_DEPOSITO", ePT_Caracter, ePD_Entrada
    .AddCampo "Fecha_Valuta", ePT_Fecha, ePD_Entrada
    .AddCampo "FLG_Vende_Todo", ePT_Caracter, ePD_Entrada
    .AddCampo "Fecha_Liquidacion", ePT_Fecha, ePD_Entrada
    .AddCampo "Precio_Historico_Compra", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MOV_ACTIVO_COMPRA", ePT_Numero, ePD_Entrada
    .AddCampo "FLG_LIMITE_PRECIO", ePT_Caracter, ePD_Entrada
    .AddCampo "TIPO_INVERSION", ePT_Caracter, ePD_Entrada   '17/07/2009 Agregado por MMardones
  End With
End Sub

Public Function Guardar() As Boolean
  Guardar = False
  
  '------------------------------------------
  '--Verficacion del Monto Bruto
  '------------------------------------------
  If NVL(Campo("monto_bruto").Valor, "") = "" Then
       Campo("monto_bruto").Valor = Campo("monto_pago").Valor
  End If
  '------------------------------------------
  '------------------------------------------
  '17/07/2009 Agregado por MMardones.
  'S�lo para Acciones puede ser "P" el tipo de inversion para el resto
  'siempre ser� "N"
  If NVL(Campo("tipo_inversion").Valor, "") = "" Then
       Campo("tipo_inversion").Valor = "N"
  End If
  '------------------------------------------
  Guardar = fClass_Entidad.Guardar(cfPackage & ".guardar")

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Buscar()
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Buscar_Operaciones_Detalle()
  Buscar_Operaciones_Detalle = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Confirmar() As Boolean
Dim lcMov_Activo As Class_Mov_Activos
Dim lOperacion As New Class_Operaciones

  Confirmar = False
  
  'Se busca la operacion para buscar detalles de esta entidadad
  lOperacion.Campo("id_operacion").Valor = Campo("id_operacion").Valor
  If Not lOperacion.Buscar Then
    fClass_Entidad.Errnum = lOperacion.Errnum
    fClass_Entidad.ErrMsg = lOperacion.ErrMsg
    GoTo ErrProcedure
  End If

  
  'Se crea un nuevo registro en la mov_caja
  Set lcMov_Activo = New Class_Mov_Activos
  With lcMov_Activo
    .Campo("Cod_Estado").Valor = cCod_Estado_Confirmado 'Nace en estado confirmado
    .Campo("Id_Mov_Activo").Valor = cNewEntidad
    .Campo("Id_Cuenta").Valor = lOperacion.Cursor(1)("id_cuenta").Value
    .Campo("Id_Nemotecnico").Valor = Me.Campo("id_nemotecnico").Valor
    .Campo("Id_Operacion_Detalle").Valor = Me.Campo("id_operacion_detalle").Valor
    .Campo("Flg_Tipo_Movimiento").Valor = lOperacion.Cursor(1)("Flg_Tipo_Movimiento").Value
    .Campo("Fecha_Operacion").Valor = lOperacion.Cursor(1)("Fecha_Operacion").Value
    .Campo("Fecha_Movimiento").Valor = lOperacion.Cursor(1)("Fecha_Operacion").Value
    .Campo("Fecha_Confirmacion").Valor = Fnt_FechaServidor
    .Campo("Cantidad").Valor = Campo("cantidad").Valor
    .Campo("Precio").Valor = Campo("precio").Valor
    .Campo("Id_Moneda").Valor = Campo("id_moneda_pago").Valor
    .Campo("Monto").Valor = Campo("monto_pago").Valor
    .Campo("Monto_Total").Valor = Campo("monto_pago").Valor
    .Campo("ID_MOV_ACTIVO_COMPRA").Valor = Campo("ID_MOV_ACTIVO_COMPRA").Valor
    
    .Campo("Tasa").Valor = 0           'La tasa todavia no aplica
    'lcMov_Activo.Campo("Id_Saldo_Activo").valor =campo("").valor 'El ID saldo_activo no aplica
  
    If Not .Guardar Then
      fClass_Entidad.Errnum = .Errnum
      fClass_Entidad.ErrMsg = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
    
  
  Confirmar = True

ErrProcedure:
  Err.Clear
  Set lcMov_Activo = Nothing
End Function

Public Function Borrar() As Boolean
Dim lClass_Entidad  As Class_Entidad
Dim lcMov_Activo    As Class_Mov_Activos
Dim lcMov_Activo_Borrar As Class_Mov_Activos
Dim lReg As hFields
'-------------------------------------------------

On Error GoTo ErrProcedure
  
  Borrar = False
  
  
  '--------------------------------------------------------------------
  '-- ELIMINA LOS MOV_ACTIVOS RELACIONADOS CON ESTE DETALLE DE OPERACION
  '--------------------------------------------------------------------
  Set lcMov_Activo = New Class_Mov_Activos
  With lcMov_Activo
    .Campo("ID_OPERACION_DETALLE").Valor = Me.Campo("ID_OPERACION_DETALLE").Valor
    If Not .Buscar Then
      fClass_Entidad.ErrMsg = lcMov_Activo_Borrar.ErrMsg
      fClass_Entidad.Errnum = lcMov_Activo_Borrar.Errnum
      GoTo ExitProcedure
    End If
    
    For Each lReg In .Cursor
      Set lcMov_Activo_Borrar = New Class_Mov_Activos
      lcMov_Activo_Borrar.Campo("ID_MOV_ACTIVO").Valor = lReg("ID_MOV_ACTIVO").Value
      If Not lcMov_Activo_Borrar.Borrar Then
        fClass_Entidad.ErrMsg = lcMov_Activo_Borrar.ErrMsg
        fClass_Entidad.Errnum = lcMov_Activo_Borrar.Errnum
        GoTo ExitProcedure
      End If
      Set lcMov_Activo_Borrar = Nothing
    Next
  End With
  Set lcMov_Activo = Nothing
  
  '--------------------------------------------------------------------
  '-- ELIMINA EL DETALLE DE OPERACION
  '--------------------------------------------------------------------
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    Call .AddCampo("ID_OPERACION_DETALLE", ePT_Numero, ePD_Entrada, Me.Campo("ID_OPERACION_DETALLE").Valor)
    
    If Not .Borrar(cfPackage & ".BORRAR") Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  End With
  Set lClass_Entidad = Nothing

  Borrar = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcMov_Activo_Borrar = Nothing
  Set lcMov_Activo = Nothing
  Set lClass_Entidad = Nothing
End Function

Public Function Sumar_Desde(pId_Cuenta _
                          , pFecha_Operacion _
                          , pMonto_Sumado _
                          , Optional pCod_Instrumento _
                          , Optional pCod_Producto) As Boolean
Dim lClass_Entidad  As Class_Entidad
  
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    Call .AddCampo("Id_Cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta)
    Call .AddCampo("Fecha_Operacion", ePT_Fecha, eParam_Direc.ePD_Entrada, pFecha_Operacion)
    
    If Not IsMissing(pCod_Instrumento) Then
      Call .AddCampo("COD_INSTRUMENTO", ePT_Caracter, eParam_Direc.ePD_Entrada, pCod_Instrumento)
    End If
    
    If Not IsMissing(pCod_Producto) Then
      Call .AddCampo("COD_PRODUCTO", ePT_Caracter, eParam_Direc.ePD_Entrada, pCod_Producto)
    End If
    
    If .Buscar(cfPackage & ".SUMAR_DESDE") Then
      Sumar_Desde = True
      
      pMonto_Sumado = NVL(.Cursor(1)("MONTO").Value, 0)
    Else
      Sumar_Desde = False
    End If
     
    fClass_Entidad.ErrMsg = .ErrMsg
    fClass_Entidad.Errnum = .Errnum
  End With
  Set lClass_Entidad = Nothing
  
ErrProcedure:
  Err.Clear
End Function
