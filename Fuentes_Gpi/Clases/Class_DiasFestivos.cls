VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Dias_Festivos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Dias_Feriados"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 72
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Empresa"
    .AddCampo "Id_Empresa", ePT_Numero, ePD_Ambos
    .AddCampo "FCH_DIA_FERIADO", ePT_Fecha, ePD_Entrada
    .AddCampo "DSC_DIA_FERIADO", ePT_Caracter, ePD_Entrada
  End With
End Sub

Public Function Buscar(Optional pEnVista As Boolean = False) As Boolean
Dim lProc As String

  If pEnVista Then
    lProc = ".BuscarView"
  Else
    lProc = ".Buscar"
  End If
  
  Buscar = fClass_Entidad.Buscar(cfPackage & lProc)
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Es_Feriado() As Boolean
Dim lcDias_Festivos As Class_Dias_Festivos
Dim lReg As hFields

  Es_Feriado = False
  
  'Limpia las variables
  fClass_Entidad.ErrMsg = ""
  fClass_Entidad.Errnum = 0
  
  Set lcDias_Festivos = New Class_Dias_Festivos
  With lcDias_Festivos
    .Campo("id_empresa").Valor = Me.Campo("id_empresa").Valor
    .Campo("FCH_DIA_FERIADO").Valor = Me.Campo("FCH_DIA_FERIADO").Valor
    If Not .Buscar Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
      
    If .Cursor.Count > 0 Then
      Es_Feriado = True
    End If
  End With
  
ExitProcedure:
  Set lcDias_Festivos = Nothing
End Function

