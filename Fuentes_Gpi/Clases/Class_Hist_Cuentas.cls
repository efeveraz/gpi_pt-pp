VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Hist_Cuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "PKG_HIST_CUENTAS"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 1
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Guardar()
    
'-----------------------------------------------------------------------------------------
'-- COLUMNAS DE AUDITORIA DE LOS REGISTROS QUE SE LLENAN DE FORMA AUTOMATICA
'-- HAF: 17/12/2007
'-----------------------------------------------------------------------------------------
  If IsNull(fClass_Entidad.Campo("ID_USUARIO_INSERT").Valor) Then
    fClass_Entidad.Campo("ID_USUARIO_INSERT").Valor = gID_Usuario
  End If
  If IsNull(fClass_Entidad.Campo("ID_USUARIO_UPDATE").Valor) Then
    fClass_Entidad.Campo("ID_USUARIO_UPDATE").Valor = gID_Usuario
  End If
'-----------------------------------------------------------------------------------------
  
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Sub LimpiaParam()
  Id_Log_Proceso = Null

  With fClass_Entidad
    .LimpiaParam
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Cliente", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Tipo_Estado", ePT_Numero, ePD_Entrada
    .AddCampo "Cod_Estado", ePT_Caracter, ePD_Entrada
    .AddCampo "Cod_Tipo_Administracion", ePT_Caracter, ePD_Entrada
    .AddCampo "Id_Contrato_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Empresa", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Asesor", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Moneda", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Perfil_Riesgo", ePT_Numero, ePD_Entrada
    .AddCampo "Num_Cuenta", ePT_Caracter, ePD_Entrada
    .AddCampo "Abr_Cuenta", ePT_Caracter, ePD_Entrada
    .AddCampo "Dsc_Cuenta", ePT_Caracter, ePD_Entrada
    .AddCampo "Observacion", ePT_Caracter, ePD_Entrada
    .AddCampo "Flg_Bloqueado", ePT_Caracter, ePD_Entrada
    .AddCampo "Obs_Bloqueo", ePT_Caracter, ePD_Entrada
    .AddCampo "Flg_Imp_Instrucciones", ePT_Caracter, ePD_Entrada
    .AddCampo "Fecha_Operativa", ePT_Fecha, ePD_Entrada
    .AddCampo "ID_USUARIO_INSERT", ePT_Numero, ePD_Entrada
    .AddCampo "ID_USUARIO_UPDATE", ePT_Numero, ePD_Entrada
    .AddCampo "FLG_MOV_DESCUBIERTOS", ePT_Caracter, ePD_Entrada
    .AddCampo "PORCEN_RF", ePT_Numero, ePD_Entrada
    .AddCampo "PORCEN_RV", ePT_Numero, ePD_Entrada

  End With
End Sub

Public Function Buscar(Optional pEnVista As Boolean = False)
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

