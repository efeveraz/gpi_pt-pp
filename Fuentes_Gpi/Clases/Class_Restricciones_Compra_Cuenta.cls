VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Restricciones_Compra_Cuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Restriccion_Compra_Cuenta"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 9
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Emisor_Especifico", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar(Optional pEnVista As Boolean = False) As Boolean
Dim lProcedimiento As String

  If pEnVista Then
    lProcedimiento = ".BuscarView"
  Else
    lProcedimiento = ".Buscar"
  End If

  Buscar = fClass_Entidad.Buscar(cfPackage & lProcedimiento)
End Function

Public Function Buscar_Nemotecnico_Nulo() As Boolean
  Buscar_Nemotecnico_Nulo = fClass_Entidad.Buscar(cfPackage & ".Buscar_Nemotecnico_Nulo")
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar_Restriccion_Compra")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borra_Restricion_Compra")
End Function

Public Function Borrar_Nemotecnico_Todos() As Boolean
  Borrar_Nemotecnico_Todos = fClass_Entidad.Borrar(cfPackage & ".Borrar_Nemotecnico_Todos")
End Function
