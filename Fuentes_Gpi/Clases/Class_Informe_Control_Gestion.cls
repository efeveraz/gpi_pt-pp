VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Informe_Control_Gestion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "PKG_INFORME_CONTROL_GESTION"

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
'    .CampoPKey = "Id_Subfamilia"
'    .AddCampo "Id_Subfamilia", ePT_Numero, ePD_Ambos
'    .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada
'    .AddCampo "Id_Valorizador", ePT_Numero, ePD_Entrada
'    .AddCampo "Cod_Subfamilia", ePT_Caracter, ePD_Entrada
'    .AddCampo "Dsc_Subfamilia", ePT_Caracter, ePD_Entrada
'    .AddCampo "Flg_Seriado", ePT_Caracter, ePD_Entrada
'    .AddCampo "Flg_Tabla_Desarrollo", ePT_Caracter, ePD_Entrada
  End With
End Sub

Public Function Distribucion_Patrimonios_Administrados(pFechaInicial _
                                                     , pFechaFinal _
                                                     , ByRef pFecha_MaxCierre) As Boolean
With gDB
    .Parametros.Clear
    .Parametros.Add "pFecha_Inicial", ePT_Fecha, pFechaInicial, ePD_Entrada
    .Parametros.Add "pFecha_Final", ePT_Fecha, pFechaFinal, ePD_Entrada
    .Parametros.Add "pFecha_MaxCierre", ePT_Fecha, "", ePD_Salida
    .Parametros.Add "pCursor", ePT_Cursor, "", ePD_Salida
    .Procedimiento = cfPackage & ".Distribucion_Patrimonios_Administrados"
    If .EjecutaSP Then
      Distribucion_Patrimonios_Administrados = True
      Set fClass_Entidad.Cursor = .Parametros("pcursor").Valor
      pFecha_MaxCierre = .Parametros("pFecha_MaxCierre").Valor
    Else
      Distribucion_Patrimonios_Administrados = False
      fClass_Entidad.Errnum = .Errnum
      fClass_Entidad.ErrMsg = .ErrMsg
    End If
    .Parametros.Clear
  End With

End Function


Public Function Ingresos_Acumulados_Asesor(pFechaInicial _
                                         , pFechaFinal) As Boolean
Dim lcClass_Entidad As New Class_Entidad

  Set lcClass_Entidad = New Class_Entidad
  With lcClass_Entidad
    .AddCampo "Fecha_Inicial", ePT_Fecha, ePD_Entrada, pFechaInicial
    .AddCampo "Fecha_Final", ePT_Fecha, ePD_Entrada, pFechaFinal
    
    Ingresos_Acumulados_Asesor = .Buscar(cfPackage & ".Ingresos_Acumulados_Asesor")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lcClass_Entidad = Nothing
End Function

Public Function Ingresos_Acumulados_Mes(pFechaInicial _
                                      , pFechaFinal) As Boolean
Dim lcClass_Entidad As New Class_Entidad

  Set lcClass_Entidad = New Class_Entidad
  With lcClass_Entidad
    .AddCampo "Fecha_Inicial", ePT_Fecha, ePD_Entrada, pFechaInicial
    .AddCampo "Fecha_Final", ePT_Fecha, ePD_Entrada, pFechaFinal
    
    Ingresos_Acumulados_Mes = .Buscar(cfPackage & ".Ingresos_Acumulados_Mes")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lcClass_Entidad = Nothing
End Function

Public Function BPI(pFechaInicial _
                  , pFechaFinal) As Boolean
Dim lcClass_Entidad As New Class_Entidad

  Set lcClass_Entidad = New Class_Entidad
  With lcClass_Entidad
    .AddCampo "Fecha_Inicial", ePT_Fecha, ePD_Entrada, pFechaInicial
    .AddCampo "Fecha_Final", ePT_Fecha, ePD_Entrada, pFechaFinal
    
    BPI = .Buscar(cfPackage & ".BPI")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lcClass_Entidad = Nothing
End Function



