VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Publicadores_Precio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: Class_Publicadores_Precio.cls $
'    $Author: Gbuenrostro $
'    $Date: 14-02-14 16:22 $
'    $Revision: 4 $
'-------------------------------------------------------------------------------------------------

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Publicadores_Precios"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 39
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_PUBLICADOR_PRECIO"
    Call .AddCampo("ID_PUBLICADOR_PRECIO", ePT_Numero, ePD_Entrada)
    Call .AddCampo("ID_PUBLICADOR", ePT_Numero, ePD_Entrada)
    Call .AddCampo("ID_MONEDA", ePT_Numero, ePD_Entrada)
    Call .AddCampo("Fecha", ePT_Fecha, ePD_Entrada)
    Call .AddCampo("TASA", ePT_Numero, ePD_Entrada)
    Call .AddCampo("DURACION", ePT_Numero, ePD_Entrada)
    Call .AddCampo("Precio", ePT_Numero, ePD_Entrada) ', ePD_Ambos)
    Call .AddCampo("id_nemotecnico", ePT_Numero, ePD_Entrada)
  End With
End Sub

Public Function Buscar_Ultimo_Cta_Nemotecnico(pId_Cuenta As String) As Boolean

  Buscar_Ultimo_Cta_Nemotecnico = True

  gDB.Parametros.Clear
  gDB.Parametros.Add "Pid_Cuenta", ePT_Numero, pId_Cuenta, ePD_Entrada
  gDB.Parametros.Add "P" & Campo("id_nemotecnico").Nombre, ePT_Numero, Campo("id_nemotecnico").Valor, ePD_Entrada
  gDB.Parametros.Add "P" & Campo("fecha").Nombre, ePT_Fecha, Campo("fecha").Valor, ePD_Entrada
  gDB.Parametros.Add "PUltimo_Precio_Cta_Nemo", ePT_Numero, 0, ePD_Salida

  gDB.Procedimiento = cfPackage & ".Ultimo_Precio_Cta_Nemo"

  If gDB.EjecutaSP Then
    Campo("precio").Valor = gDB.Parametros("PUltimo_Precio_Cta_Nemo").Valor
  Else
    'Si existe un problema el proceso devuelve false
    Buscar_Ultimo_Cta_Nemotecnico = False
    fClass_Entidad.Errnum = gDB.Errnum
    fClass_Entidad.ErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Buscar_Ultimo_Tasa_Cta_Nemo(pId_Cuenta As String) As Boolean
  
  Buscar_Ultimo_Tasa_Cta_Nemo = True
  
  gDB.Parametros.Clear
  gDB.Parametros.Add "Pid_Cuenta", ePT_Numero, pId_Cuenta, ePD_Entrada
  gDB.Parametros.Add "p" & Campo("id_nemotecnico").Nombre, ePT_Numero, Campo("id_nemotecnico").Valor, ePD_Entrada
  gDB.Parametros.Add "p" & Campo("fecha").Nombre, ePT_Fecha, Campo("fecha").Valor, ePD_Entrada
  gDB.Parametros.Add "PUltimo_Tasa_Cta_Nemo", ePT_Numero, 0, ePD_Salida
  
  gDB.Procedimiento = cfPackage & ".Ultimo_Tasa_Cta_Nemo"
  
  If gDB.EjecutaSP Then
    Campo("tasa").Valor = gDB.Parametros("PUltimo_Tasa_Cta_Nemo").Valor
  Else
    'Si existe un problema el proceso devuelve false
    Buscar_Ultimo_Tasa_Cta_Nemo = False
    fClass_Entidad.Errnum = gDB.Errnum
    fClass_Entidad.ErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Buscar()
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function BuscarUltimoHabil()
  BuscarUltimoHabil = fClass_Entidad.Buscar(cfPackage & ".BuscarUltimoHabil")
End Function

Public Function BuscarView(Optional pCod_Producto = Null)

  With fClass_Entidad
    If Not IsNull(pCod_Producto) Then
      .AddCampo "Cod_Producto", ePT_Caracter, ePD_Entrada
      .Campo("Cod_Producto").Valor = pCod_Producto
    End If
    
    BuscarView = .Buscar(cfPackage & ".BuscarView")
  End With
  
End Function

Public Function Buscar_X_Cuenta(pFecha, pId_Cuenta, pCod_Instrumento) As Boolean
Dim lClass_Entidad As New Class_Entidad

  Buscar_X_Cuenta = False

  With lClass_Entidad
    .AddCampo "fecha", ePT_Fecha, ePD_Entrada
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "Cod_Instrumento", ePT_Numero, ePD_Entrada
  
    .Campo("fecha").Valor = pFecha
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("Cod_Instrumento").Valor = pCod_Instrumento

    If .Buscar(cfPackage & ".Buscar_X_Cuenta") Then
      Buscar_X_Cuenta = True
      Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
    Else
      fClass_Entidad.Errnum = lClass_Entidad.Errnum
      fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
      Set fClass_Entidad.Cursor = Nothing
    End If
  End With
  Set lClass_Entidad = Nothing
End Function

'Esta funcion entrega el precio del nemotecnico que tiene almacenado en el cursor de si mismo.
'si no existe el nemotecnico se entregara 0, para no producir un error.
Public Function Precio_Nemotecnico(pId_Nemotecnico) As Double
Dim lPrecio As hFields
 
  Set lPrecio = fClass_Entidad.Cursor.Buscar("id_nemotecnico", pId_Nemotecnico)
 
  If lPrecio Is Nothing Then
    Precio_Nemotecnico = 0
  Else
    Precio_Nemotecnico = lPrecio("precio").Value
  End If
End Function

Public Function Tasa_Nemotecnico(pId_Nemotecnico) As Double
Dim lTasa As hFields
 
  Set lTasa = fClass_Entidad.Cursor.Buscar("id_nemotecnico", pId_Nemotecnico)
 
  If lTasa Is Nothing Then
    Tasa_Nemotecnico = 0
  Else
    Tasa_Nemotecnico = lTasa("tasa").Value
  End If
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Guardar() As Boolean
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Buscar_Precio(bEnCartera, pCod_Producto, pId_Publicador, pFecha) As Boolean
    Dim lClass_Entidad As New Class_Entidad
    Dim lbExito As Boolean

    Buscar_Precio = False

    With lClass_Entidad
        .AddCampo "cod_producto", ePT_Caracter, ePD_Entrada
        .AddCampo "id_publicador", ePT_Numero, ePD_Entrada
        .AddCampo "fecha", ePT_Fecha, ePD_Entrada
      
        .Campo("fecha").Valor = pFecha
        .Campo("cod_producto").Valor = pCod_Producto
        .Campo("id_publicador").Valor = pId_Publicador
        
        If bEnCartera Then
            lbExito = .Buscar(cfPackage & ".BUSCAR_PRECIOS_SALDO_ACTIVOS")
        Else
            lbExito = .Buscar(cfPackage & ".BUSCAR_PRECIOS")
        End If
        
        If lbExito Then
          Buscar_Precio = True
          Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
        Else
          fClass_Entidad.Errnum = lClass_Entidad.Errnum
          fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
          
          Set fClass_Entidad.Cursor = Nothing
          
        End If
        
    End With
    
    Set lClass_Entidad = Nothing
End Function

'Public Function Buscar_Precio_Saldo_Activo(pCod_Producto, pId_Publicador, pFecha) As Boolean
'    Dim lClass_Entidad As New Class_Entidad
'
'    Buscar_Precio_Saldo_Activo = False
'
'    With lClass_Entidad
'        .AddCampo "cod_producto", ePT_Caracter, ePD_Entrada
'        .AddCampo "id_publicador", ePT_Numero, ePD_Entrada
'        .AddCampo "fecha", ePT_Fecha, ePD_Entrada
'
'        .Campo("fecha").Valor = pFecha
'        .Campo("cod_producto").Valor = pCod_Producto
'        .Campo("id_publicador").Valor = pId_Publicador
'
'        If .Buscar(cfPackage & ".BUSCAR_PRECIOS_SALDO_ACTIVOS") Then
'          Buscar_Precio_Saldo_Activo = True
'          Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
'        Else
'          fClass_Entidad.Errnum = lClass_Entidad.Errnum
'          fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
'
'          Set fClass_Entidad.Cursor = Nothing
'
'        End If
'
'    End With
'
'    Set lClass_Entidad = Nothing
'End Function
'
