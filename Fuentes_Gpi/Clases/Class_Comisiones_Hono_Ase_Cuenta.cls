VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Comisiones_Hono_Ase_Cuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Comisiones_Hono_Ase_Cuenta"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 74
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_COMISION_HONO_ASE_CUENTA"
    .AddCampo "ID_COMISION_HONO_ASE_CUENTA", ePT_Numero, ePD_Ambos
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_CIERRE", ePT_Numero, ePD_Entrada
    .AddCampo "FECHA_CIERRE", ePT_Fecha, ePD_Entrada
    .AddCampo "COMISION_HONORARIOS", ePT_Numero, ePD_Entrada
    .AddCampo "COMISION_ASESORIAS", ePT_Numero, ePD_Entrada
    .AddCampo "COD_INSTRUMENTO", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_MONEDA", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Ejecuta_x_Cuenta() As Boolean
Rem ----------------------------------------------------------------
Rem Procedimiento modificado para que las comisiones se guarden en
Rem la moneda de la cuenta y no en la de la empresa como hasta ahora
Rem CSM 07/05/2009
Rem ----------------------------------------------------------------
Dim lcComisionCuenta            As Class_ComisionesCuentas
Dim lcComisione_Hono_Ase_Cuenta As Class_Comisiones_Hono_Ase_Cuenta
Dim lcSubCuota_Instrumento      As Class_Subcuota_Instrumentos
Dim lcCuenta      As Object
'Dim lcMov_Caja_Origen           As Class_Mov_Caja_Origen
'Dim lcTipo_Cambio               As Object
'Dim lcEmpresa                   As Class_Empresas
Dim lRecord_Comi                As hRecord
Dim lReg                        As hFields
Dim lReg_SubCuota               As hFields
'-------------------------------------------------------------
Dim lMontoHonorarios            As Double
Dim lMontoAsesorias             As Double
'Dim lId_Moneda_Empresa          As Double
Dim lId_Moneda_Cuenta           As String
Dim lIva                        As Double
'Dim lParidad                    As Double
'Dim lOperacion                  As String
Dim lBase                       As Double
Dim lComi_Hono_Dia              As Double
Dim lComi_Ases_Dia              As Double

  Ejecuta_x_Cuenta = False

  lIva = Fnt_Iva(Me.Campo("fecha_cierre").Valor)

'Busca si la cuenta para el cobro de comision por honorarios es afecta a impuesto  (jgr 180510)
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_Cuenta").Valor = Me.Campo("id_Cuenta").Valor
    If .Buscar(True) Then
        For Each lReg In .Cursor
            lIva = IIf(NVL(lReg("flg_comision_afecta_impuesto").Value, "") = gcFlg_SI, lIva, 0)
        Next
    End If
   End With
  Set lcCuenta = Nothing

'  Set lcEmpresa = New Class_Empresas
'  With lcEmpresa
'    .Campo("id_empresa").Valor = Fnt_EmpresaActual
'    If Not .Buscar Then
'      fClass_Entidad.ErrMsg = .ErrMsg
'      fClass_Entidad.ErrNum = .ErrNum
'      GoTo ExitProcedure
'    End If
'
'    lId_Moneda_Empresa = .Cursor(1)("id_moneda").Value
'  End With

  Set lcComisionCuenta = New Class_ComisionesCuentas
  With lcComisionCuenta
    .Campo("id_cuenta").Valor = Me.Campo("id_Cuenta").Valor
    If Not .Buscar_Valida(Me.Campo("fecha_cierre").Valor) Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If

    Set lRecord_Comi = .Cursor
  End With
  Set lcComisionCuenta = Nothing

  Set lcSubCuota_Instrumento = New Class_Subcuota_Instrumentos
  With lcSubCuota_Instrumento
    .Campo("id_cuenta").Valor = Me.Campo("id_Cuenta").Valor
    .Campo("fecha_cierre").Valor = Me.Campo("fecha_cierre").Valor
    If Not .Buscar Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  End With
  
  If lcSubCuota_Instrumento.Cursor.Count > 0 Then
'    Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
'    With lcTipo_Cambio
'      If Not .Busca_Precio_Paridad(Me.Campo("id_Cuenta").Valor _
'                                 , lcSubCuota_Instrumento.Cursor(1)("id_moneda_cuenta").Value _
'                                 , lId_Moneda_Empresa _
'                                 , Me.Campo("fecha_cierre").Valor _
'                                 , lParidad _
'                                 , lOperacion) Then
'        fClass_Entidad.ErrMsg = .ErrMsg
'        fClass_Entidad.ErrNum = .ErrNum
'        GoTo ExitProcedure
'      End If
'    End With
    lId_Moneda_Cuenta = lcSubCuota_Instrumento.Cursor(1)("id_moneda_cuenta").Value
  End If
  
  For Each lReg In lRecord_Comi
    lMontoHonorarios = 0
    lMontoAsesorias = 0
        
    lBase = lReg("periodicidad").Value
    lComi_Hono_Dia = Fnt_Divide(lReg("comision_honorarios").Value, lBase)
    lComi_Ases_Dia = Fnt_Divide(lReg("comision_asesorias").Value, lBase)
        
    Set lReg_SubCuota = lcSubCuota_Instrumento.Cursor.Buscar("cod_instrumento", lReg("cod_instrumento").Value)
    
    If Not lReg_SubCuota Is Nothing Then
      lMontoHonorarios = lReg_SubCuota("saldo_activo_mon_cuenta").Value * lComi_Hono_Dia
      lMontoAsesorias = lReg_SubCuota("saldo_activo_mon_cuenta").Value * lComi_Ases_Dia
      
      lMontoHonorarios = lMontoHonorarios * (lIva + 1)
    End If
    
    If (lMontoHonorarios > 0) Or (lMontoAsesorias > 0) Then
      'lMontoHonorarios = Fnt_Calcula_TipoCambio_Ope(lMontoHonorarios _
                                                  , lParidad _
                                                  , lOperacion)
    
      'lMontoAsesorias = Fnt_Calcula_TipoCambio_Ope(lMontoAsesorias _
                                                  , lParidad _
                                                  , lOperacion)
    
      Set lcComisione_Hono_Ase_Cuenta = New Class_Comisiones_Hono_Ase_Cuenta
      With lcComisione_Hono_Ase_Cuenta
        .Campo("id_cuenta").Valor = Me.Campo("id_Cuenta").Valor
        .Campo("id_cierre").Valor = Me.Campo("id_cierre").Valor
        .Campo("fecha_cierre").Valor = Me.Campo("fecha_cierre").Valor
        .Campo("comision_honorarios").Valor = NVL(Fnt_Redondear_A_Moneda(lMontoHonorarios, lId_Moneda_Cuenta), 0) ' Round(lMontoHonorarios, 0)
        .Campo("comision_asesorias").Valor = NVL(Fnt_Redondear_A_Moneda(lMontoAsesorias, lId_Moneda_Cuenta), 0) ' Round(lMontoAsesorias, 0)
        .Campo("cod_instrumento").Valor = lReg("cod_instrumento").Value
        .Campo("ID_MONEDA").Valor = lId_Moneda_Cuenta 'lId_Moneda_Empresa
        
        If Not .Guardar Then
          fClass_Entidad.ErrMsg = .ErrMsg
          fClass_Entidad.Errnum = .Errnum
          GoTo ExitProcedure
        End If
  
      End With
    End If
    
    Set lcComisionCuenta = Nothing
  Next

  Ejecuta_x_Cuenta = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcComisionCuenta = Nothing
  Set lcComisione_Hono_Ase_Cuenta = Nothing
  Set lcSubCuota_Instrumento = Nothing
  'Set lcMov_Caja_Origen = Nothing
  'Set lcTipo_Cambio = Nothing
  'Set lcEmpresa = Nothing
End Function

Public Function Deshacer_Enlaze_Cierre()
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, Me.Campo("id_cuenta").Valor
    .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada, Me.Campo("fecha_cierre").Valor
    
    Deshacer_Enlaze_Cierre = .Borrar(cfPackage & ".DESHACER_ENLAZE_CIERRE")
    fClass_Entidad.Errnum = lClass_Entidad.Errnum
    fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Calcular_Periodo(pFecha_Ini, pFecha_Fin) As Boolean
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "id_empresa", ePT_Numero, ePD_Entrada, Fnt_EmpresaActual
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, Me.Campo("id_cuenta").Valor
    .AddCampo "fecha_ini", ePT_Fecha, ePD_Entrada, pFecha_Ini
    .AddCampo "fecha_fin", ePT_Fecha, ePD_Entrada, pFecha_Fin
    
    Calcular_Periodo = .Buscar(cfPackage & ".CALCULAR_PERIODO")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function


Public Function Reporte_Detallado(pFecha_Ini, pFecha_Fin) As Boolean
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, Me.Campo("id_cuenta").Valor
    .AddCampo "fecha_liquidacion", ePT_Fecha, ePD_Entrada, pFecha_Fin
    
    Reporte_Detallado = .Buscar(cfPackage & ".REPORTE_DETALLADO")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Reporte_Detalle_Comisiones_Liquidadas(pFecha, Optional pId_Cuenta = "") As Boolean
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    If pId_Cuenta <> "" Then
        .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
    End If
    .AddCampo "fecha_liquidacion", ePT_Fecha, ePD_Entrada, pFecha
    
    Reporte_Detalle_Comisiones_Liquidadas = .Buscar(cfPackage & ".Reporte_Detalle_Liquidadas")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function
