VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Remesas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "Pkg_Remesas"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 40
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_REMESA"
    .AddCampo "ID_REMESA", ePT_Numero, ePD_Ambos
    .AddCampo "ID_MOV_CAJA_EGRESO", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MOV_CAJA_INGRESO", ePT_Numero, ePD_Entrada
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_TIPO_ESTADO", ePT_Numero, ePD_Entrada
    .AddCampo "COD_ESTADO", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_CAJA_CUENTA_EGRESO", ePT_Numero, ePD_Entrada
    .AddCampo "ID_CAJA_CUENTA_INGRESO", ePT_Numero, ePD_Entrada
    .AddCampo "COD_ORIGEN_MOV_CAJA", ePT_Caracter, ePD_Entrada
    .AddCampo "MONTO_EGRESO", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_INGRESO", ePT_Numero, ePD_Entrada
    .AddCampo "RETENCION", ePT_Numero, ePD_Entrada
    .AddCampo "FECHA_REMESA", ePT_Fecha, ePD_Entrada
    .AddCampo "FACTOR_CONVERSION_CLIENTE", ePT_Numero, ePD_Entrada
    .AddCampo "FACTOR_TRANSFERENCIA", ePT_Numero, ePD_Entrada
    .AddCampo "DSC_REMESA", ePT_Caracter, ePD_Entrada
    .AddCampo "OBS_REMESA", ePT_Caracter, ePD_Entrada
    .AddCampo "UTILIDAD", ePT_Caracter, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Guardar() As Boolean
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Anular(pId_Remesa) As Boolean
  
  Anular = True
  
  gDB.Parametros.Clear
  gDB.Procedimiento = cfPackage & ".Anular"
  gDB.Parametros.Add "pid_remesa", ePT_Numero, pId_Remesa, ePD_Entrada
  If Not gDB.EjecutaSP Then
    Anular = False
    fClass_Entidad.Errnum = gDB.Errnum
    fClass_Entidad.ErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If
  gDB.Parametros.Clear

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function
