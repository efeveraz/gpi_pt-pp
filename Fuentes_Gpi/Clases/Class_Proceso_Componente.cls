VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Proceso_Componente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Proceso_Componente"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 49
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function ErrNum() As Double
  ErrNum = fClass_Entidad.ErrNum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .AddCampo "COD_PROCESO_COMPONENTE", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada
    .AddCampo "COMPONENTE", ePT_Caracter, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Guardar() As Boolean
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function


Public Function IniciaClass(ByRef pMensaje As String, Optional pErrorNoExiste As Boolean = True) As Object
Dim lObjecto As Object

  Set IniciaClass = Nothing

  With Me
    If Not .Buscar Then
      pMensaje = "Problema al buscar la componente del proceso (" & Me.Campo("COD_PROCESO_COMPONENTE").Valor & ")"
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      If pErrorNoExiste Then
        pMensaje = "No existe ningun componente definido para el proceso (" & Me.Campo("COD_PROCESO_COMPONENTE").Valor & ")"
      End If
      GoTo ExitProcedure
    End If
    
    Set lObjecto = Fnt_CreateObject(.Cursor(1)("COMPONENTE").Value)
    
    If lObjecto Is Nothing Then
      pMensaje = "No se pudo crear el componente definido para el proceso (" & Me.Campo("COD_PROCESO_COMPONENTE").Valor & ")"
      fClass_Entidad.ErrMsg = Err.Description
      GoTo ExitProcedure
    End If
  End With
  
  Set IniciaClass = lObjecto
  
ErrProcedure:
  If Not Err.Number = 0 Then
    pMensaje = "Problemas al carga el componente (" & Me.Campo("COD_PROCESO_COMPONENTE").Valor & ")."
    fClass_Entidad.ErrMsg = Err.Description
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

End Function
