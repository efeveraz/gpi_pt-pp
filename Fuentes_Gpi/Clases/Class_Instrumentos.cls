VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Instrumentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Instrumentos"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLog_Subtipo.eLS_Instrumentos
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar(Optional pEnVista As Boolean = False)
  If pEnVista Then
    Buscar = fClass_Entidad.Buscar(cfPackage & ".BuscarView")
  Else
    Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
  End If
End Function

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Cod_Instrumento"
    Call .AddCampo("Cod_Instrumento", ePT_Caracter, ePD_Ambos)
    Call .AddCampo("Cod_Producto", ePT_Caracter, ePD_Entrada)
    Call .AddCampo("Id_Valorizador", ePT_Numero, ePD_Entrada)
    Call .AddCampo("Dsc_Intrumento", ePT_Caracter, ePD_Entrada)
    Call .AddCampo("Id_Instrumento", ePT_Numero, ePD_Entrada)
    Call .AddCampo("Id_Subfamilia", ePT_Numero, ePD_Entrada)
    Call .AddCampo("Formulario", ePT_Caracter, ePD_Entrada)
  End With
End Sub

