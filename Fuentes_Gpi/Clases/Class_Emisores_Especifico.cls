VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Emisores_Especifico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "Pkg_Emisores_Especificos"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 11
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_EMISOR_ESPECIFICO"
    .AddCampo "ID_EMISOR_ESPECIFICO", ePT_Numero, ePD_Ambos
    .AddCampo "ID_EMISOR_GENERAL", ePT_Numero, ePD_Entrada
    .AddCampo "DSC_EMISOR_ESPECIFICO", ePT_Caracter, ePD_Entrada
    .AddCampo "COD_EMISOR_ESPECIFICO", ePT_Caracter, ePD_Entrada
    .AddCampo "COD_SVS_NEMOTECNICO", ePT_Caracter, ePD_Entrada
    .AddCampo "COD_TIPO_EMISOR", ePT_Caracter, ePD_Entrada
    .AddCampo "RUT", ePT_Caracter, ePD_Entrada
    .AddCampo "COD_CLASIFICADOR", ePT_Caracter, ePD_Entrada
    .AddCampo "COD_ACHS", ePT_Caracter, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar(Optional pVista As Boolean = False, Optional pId_Sector As String = "") As Boolean
Dim lProc As String
  
  If pVista Then
    lProc = ".Buscarview"
    
    If Not pId_Sector = "" Then
      Call fClass_Entidad.AddCampo("ID_SECTOR", ePT_Numero, ePD_Entrada, pId_Sector)
    End If
  Else
    lProc = ".Buscar"
  End If
  
  Buscar = fClass_Entidad.Buscar(cfPackage & lProc)
  
End Function

Public Function Guardar() As Boolean
Dim lResult As Boolean
Dim lMensaje As String
  
  With fClass_Entidad
    If .Campo(.CampoPKey).Valor = cNewEntidad Then
      lMensaje = "Se ha insertado"
    Else
      lMensaje = "Se ha actualizado"
    End If
  End With

  lMensaje = lMensaje & " el Emisor Específico " _
            & fClass_Entidad.Campo("DSC_EMISOR_ESPECIFICO").Valor

  lResult = fClass_Entidad.Guardar(cfPackage & ".Guardar")

  If Not lResult Then
    lMensaje = "Problema en la grabación del Emisor Específico (" _
              & fClass_Entidad.Campo("DSC_EMISOR_ESPECIFICO").Valor
  End If

  Call Fnt_AgregarLog(SubTipo_LOG _
                    , IIf(lResult, cEstado_Log_Mensaje, cEstado_Log_Error) _
                    , lMensaje _
                    , pId_Log_Proceso:=Id_Log_Proceso)

  Guardar = lResult

End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function
