VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Mov_Activos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Mov_Activos"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 53
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Public Function Buscar(Optional pId_Cierre)

  If Not IsMissing(pId_Cierre) Then
    fClass_Entidad.AddCampo "id_cierre", ePT_Numero, ePD_Entrada, pId_Cierre
  End If

  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function BuscarView(Optional pId_Cierre, Optional pCod_Instrumento)

  If Not IsMissing(pId_Cierre) Then
    fClass_Entidad.AddCampo "id_cierre", ePT_Numero, ePD_Entrada, pId_Cierre
  End If

  If Not IsMissing(pCod_Instrumento) Then
    fClass_Entidad.AddCampo "cod_instrumento", ePT_Caracter, ePD_Entrada, pCod_Instrumento
  End If

  BuscarView = fClass_Entidad.Buscar(cfPackage & ".BuscarView")
End Function

Public Function BuscarView_2(pFecha_ini, Pfecha_fin)
'-------------------------------------------------------------------------
'HAF: 2007/08/22
'NO SE QUIEN HIZO ESTA WEA PERO DEBO REVISAR SI ESTA BIEN LO QUE SE INTENTO HACER :'(
'-------------------------------------------------------------------------
  
  fClass_Entidad.AddCampo "id_cierre", ePT_Numero, ePD_Entrada, Null
  fClass_Entidad.AddCampo "fecha_ini", ePT_Fecha, ePD_Entrada, pFecha_ini
  fClass_Entidad.AddCampo "fecha_fin", ePT_Fecha, ePD_Entrada, Pfecha_fin
  
  BuscarView_2 = fClass_Entidad.Buscar(cfPackage & ".BuscarView_2")
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Mov_Activo"
    .AddCampo "Id_Mov_Activo", ePT_Numero, ePD_Ambos
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Operacion_Detalle", ePT_Numero, ePD_Entrada
    .AddCampo "Flg_Tipo_Movimiento", ePT_Caracter, ePD_Entrada
    .AddCampo "Fecha_Operacion", ePT_Fecha, ePD_Entrada
    .AddCampo "Fecha_Movimiento", ePT_Fecha, ePD_Entrada
    .AddCampo "Fecha_Confirmacion", ePT_Fecha, ePD_Entrada
    .AddCampo "Cantidad", ePT_Numero, ePD_Entrada
    .AddCampo "Precio", ePT_Numero, ePD_Entrada
    .AddCampo "Tasa", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Moneda", ePT_Numero, ePD_Entrada
    .AddCampo "Cod_Estado", ePT_Caracter, ePD_Entrada
    .AddCampo "Id_Saldo_Activo", ePT_Numero, ePD_Entrada
    .AddCampo "Monto", ePT_Numero, ePD_Entrada
    .AddCampo "Monto_Total", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Mov_Activo_Compra", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar_Pendientes_Cierre(pId_Cuenta, pFecha _
                                        , Optional pId_Nemotecnico = Null _
                                        , Optional pTipo_Movimiento = Null _
                                        , Optional pId_Mov_Activo_Compra = Null)
Dim lClass_Entidad  As Class_Entidad
  
  
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
    .AddCampo "Fecha", ePT_Fecha, ePD_Entrada, pFecha
    
    If Not IsNull(pId_Nemotecnico) Then
      .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada, pId_Nemotecnico
    End If
    If Not IsNull(pId_Mov_Activo_Compra) Then
      .AddCampo "Id_Mov_Activo_Compra", ePT_Numero, ePD_Entrada, pId_Mov_Activo_Compra
    End If
    If Not IsNull(pTipo_Movimiento) Then
      .AddCampo "Flg_Tipo_Movimiento", ePT_Caracter, ePD_Entrada, pTipo_Movimiento
    End If
    
    Buscar_Pendientes_Cierre = .Buscar(cfPackage & ".Movimientos_Pend_Cierre")
    Set fClass_Entidad.Cursor = .Cursor
  End With
  Set lClass_Entidad = Nothing
  
ErrProcedure:
  Err.Clear
End Function

Public Function Deshacer_Enlaze_Cierre(ByVal pId_Cuenta As Double, ByVal pFecha_Cierre As Date)
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada
    
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("fecha_cierre").Valor = pFecha_Cierre
    
    Deshacer_Enlaze_Cierre = .Borrar(cfPackage & ".DESHACER_ENLAZE_CIERRE")
    fClass_Entidad.Errnum = lClass_Entidad.Errnum
    fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Enlazar_Cierre(ByVal pId_Cuenta As Double _
                              , ByVal pFecha_Cierre As Date _
                              , ByVal pId_Saldo_Activo _
                              , ByVal pId_Cierre As Double _
                              , ByVal pId_Nemotecnico As Double)
Dim lClass_Entidad As Class_Entidad
  
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
    .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada, pFecha_Cierre
    .AddCampo "Id_Saldo_Activo", ePT_Numero, ePD_Entrada, pId_Saldo_Activo
    .AddCampo "Id_Cierre", ePT_Numero, ePD_Entrada, pId_Cierre
    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada, pId_Nemotecnico
    
    Enlazar_Cierre = .Borrar(cfPackage & ".ENLAZAR_CIERRE")
    fClass_Entidad.Errnum = lClass_Entidad.Errnum
    fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Buscar_Uncierre(pId_Cuenta _
                              , pFecha_Cierre _
                              , pCod_Instrumento) As Boolean
Dim lClass_Entidad As New Class_Entidad

  Buscar_Uncierre = False

  With lClass_Entidad
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada
    .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada
    
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("fecha_cierre").Valor = pFecha_Cierre
    .Campo("Cod_Instrumento").Valor = pCod_Instrumento
    
    If .Buscar(cfPackage & ".Buscar_Uncierre") Then
      Buscar_Uncierre = True
      Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
    Else
      fClass_Entidad.Errnum = lClass_Entidad.Errnum
      fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
    End If
  End With
  Set lClass_Entidad = Nothing

End Function


Public Function Enlazar_Cierre_MA(ByVal pId_Cuenta As Double _
                              , ByVal pFecha_Cierre As Date _
                              , ByVal pId_Saldo_Activo _
                              , ByVal pId_Cierre As Double _
                              , ByVal pId_Nemotecnico As Double _
                              , ByVal pId_Mov_Activo As Double)
Dim lClass_Entidad As Class_Entidad
  
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
    .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada, pFecha_Cierre
    .AddCampo "Id_Saldo_Activo", ePT_Numero, ePD_Entrada, pId_Saldo_Activo
    .AddCampo "Id_Cierre", ePT_Numero, ePD_Entrada, pId_Cierre
    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada, pId_Nemotecnico
    .AddCampo "Id_Mov_Activo", ePT_Numero, ePD_Entrada, pId_Mov_Activo
    
    Enlazar_Cierre_MA = .Borrar(cfPackage & ".ENLAZAR_CIERRE_MA")
    fClass_Entidad.Errnum = lClass_Entidad.Errnum
    fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Buscar_Uncierre_MA(pId_Cuenta _
                                  , pFecha_Cierre _
                                  , pCod_Instrumento) As Boolean
Dim lClass_Entidad As New Class_Entidad

  Buscar_Uncierre_MA = False

  With lClass_Entidad
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada
    .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada
    
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("fecha_cierre").Valor = pFecha_Cierre
    .Campo("Cod_Instrumento").Valor = pCod_Instrumento
    
    If .Buscar(cfPackage & ".Buscar_Uncierre_MA") Then
      Buscar_Uncierre_MA = True
      Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
    Else
      fClass_Entidad.Errnum = lClass_Entidad.Errnum
      fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
    End If
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Buscar_No_Rentabilidad(pId_Cuenta, ByRef pMov_Activo_Ingreso, ByRef pMov_Activo_Egreso) As Boolean
Dim lClass_Entidad As Class_Entidad

  Buscar_No_Rentabilidad = False

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    Call .AddCampo("id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta)
    Call .AddCampo("Fecha_Movimiento", ePT_Fecha, ePD_Entrada, fClass_Entidad.Campo("Fecha_Movimiento").Valor)
    
    If .Buscar(cfPackage & ".Buscar_No_Rentabilidad") Then
      Buscar_No_Rentabilidad = True
      pMov_Activo_Ingreso = .Cursor(1)("Mov_Activo_Ingreso").Value
      pMov_Activo_Egreso = .Cursor(1)("Mov_Activo_Egreso").Value
    End If
    
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Anular_x_OpDet() As Boolean
'HAF: 22/08/2007
'Este procedimiento anula los mov_activos de las detalle de operaciones
'asi que solo se debe utilizar por ahora el campo "id_operacion_detalle"
  Anular_x_OpDet = fClass_Entidad.Borrar(cfPackage & ".Anular_x_OpDet")
End Function

Public Function Anular_x_Operacion(pId_Operacion) As Boolean
'HAF: 22/08/2007
'Este procedimiento anula los mov_activos de las operaciones
'asi que solo se debe utilizar por ahora el campo "id_operacion"
Dim lClass_Entidad As Class_Entidad
  
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    Call .AddCampo("id_operacion", ePT_Numero, ePD_Entrada, pId_Operacion)
    
    Anular_x_Operacion = .Borrar(cfPackage & ".Anular_x_Operacion")
    
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Buscar_X_Arbol_Clase_Inst(pId_Arbol_Clase_Inst, pId_Cierre) As Boolean
Dim lClass_Entidad As Class_Entidad

  Buscar_X_Arbol_Clase_Inst = False

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    Call .AddCampo("id_arbol_clase_inst", ePT_Numero, ePD_Entrada, pId_Arbol_Clase_Inst)
    Call .AddCampo("id_cuenta", ePT_Numero, ePD_Entrada, fClass_Entidad.Campo("Id_Cuenta").Valor)
    Call .AddCampo("id_empresa", ePT_Numero, ePD_Entrada, Fnt_EmpresaActual)
    Call .AddCampo("id_cierre", ePT_Numero, ePD_Entrada, pId_Cierre)
    
    If .Buscar(cfPackage & ".BUSCAR_X_ARBOL_CLASE_INSTRUMENTO") Then
      Buscar_X_Arbol_Clase_Inst = True
      Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
    End If
    
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Buscar_Pendientes_Cierre_SFecha(pId_Cuenta _
                                                , Optional pId_Nemotecnico = Null _
                                                , Optional pTipo_Movimiento = Null _
                                                , Optional pId_Mov_Activo_Compra = Null)
Dim lClass_Entidad  As Class_Entidad
  
  
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
    
    If Not IsNull(pId_Nemotecnico) Then
      .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada, pId_Nemotecnico
    End If
    If Not IsNull(pId_Mov_Activo_Compra) Then
      .AddCampo "Id_Mov_Activo_Compra", ePT_Numero, ePD_Entrada, pId_Mov_Activo_Compra
    End If
    If Not IsNull(pTipo_Movimiento) Then
      .AddCampo "Flg_Tipo_Movimiento", ePT_Caracter, ePD_Entrada, pTipo_Movimiento
    End If
    
    Buscar_Pendientes_Cierre_SFecha = .Buscar(cfPackage & ".Movimientos_Pend_Cierre_SFecha")
    Set fClass_Entidad.Cursor = .Cursor
  End With
  Set lClass_Entidad = Nothing
  
ErrProcedure:
  Err.Clear
End Function


