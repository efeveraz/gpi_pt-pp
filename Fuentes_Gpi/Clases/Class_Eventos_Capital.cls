VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Eventos_Capital"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function Fnt_Ejec_Eventos_Capital(pFecha As Date, pGrilla As VSFlexGrid) As Boolean
Dim lDividendos As Class_VariacionCapital
Dim lVencimientos As Class_Vencimientos_RF
Dim lCambio_Nem As Class_Cambio_Nemotecnico

  Rem Se inicia la ejecuci�n de eventos de capital
  Call Fnt_Escribe_Grilla(pGrilla, "N", Fnt_SYSDATE & " Inicio Ejecuci�n de Eventos de Capital.")
  
'  Rem Ejecuta los dividendos
'  Set lDividendos = New Class_Dividendos
'  lDividendos.Campo("Fecha").Valor = pFecha
'  Call lDividendos.Fnt_Pago_Dividendos(pGrilla)
  
  Rem Ejecuta Cambio de Nemotecnicos
  Set lCambio_Nem = New Class_Cambio_Nemotecnico
  Call lCambio_Nem.Fnt_Cambio_Nemotecnico(pFecha, pGrilla)
  
  Rem T�rmino de la ejecuci�n de eventos de capital
  Call Fnt_Escribe_Grilla(pGrilla, "N", Fnt_SYSDATE & " T�rmino Ejecuci�n de Eventos de Capital.")
  
End Function

Public Function Ejecuta_x_Cuenta(pId_Cuenta _
                               , pFecha _
                               , pId_Cierre _
                               , pfCuenta _
                               , pGrilla As VSFlexGrid) As Boolean
Dim lcCorte_Cupon               As Class_Cortes_Cupon_Cuenta
Dim lcDividendo                 As Class_VariacionCapital
Dim lcVencimiento_Rf            As Class_Vencimientos_RF
Dim lcComponente_EventoCapital  As Class_Componentes_EventoCapital
'---------------------------------------------------------------------
Dim lcClass As Object
'---------------------------------------------------------------------------
Dim lReg As hFields
'---------------------------------------------------------------------------
Dim lMensaje As String

  Rem Se inicia la ejecuci�n de eventos de capital
  Call Fnt_Escribe_Grilla(pGrilla, "", Space(4) & "Buscando Eventos de Capital...")
  
  Ejecuta_x_Cuenta = False
  
  Rem Se inicia la ejecuci�n de eventos de capital
  Call Fnt_Escribe_Grilla(pGrilla, "", Space(6) & "Buscando Cortes de cupon...")
  Set lcCorte_Cupon = New Class_Cortes_Cupon_Cuenta
  With lcCorte_Cupon
    .Campo("fch_corte_cupon").Valor = (pFecha + 1) 'se busca los cortes de cupon de ma�ana
    If Not .Ejecuta_x_Cuenta(pId_Cuenta) Then
      GoTo ExitProcedure
    End If
  End With
  Set lcCorte_Cupon = Nothing
  
  
  Call Fnt_Escribe_Grilla(pGrilla, "", Space(6) & "Buscando Dividendos...")
  Set lcDividendo = New Class_VariacionCapital
  With lcDividendo
    .Campo("fecha").Valor = pFecha
    If Not .Ejecuta_x_Cuenta(pId_Cuenta) Then
      Call Fnt_Escribe_Grilla(pGrilla, _
                         cGrillaLog_Error, _
                         Space(8) & .ErrMsg)
      GoTo ExitProcedure
    End If
  End With
  Set lcDividendo = Nothing
  
  Call Fnt_Escribe_Grilla(pGrilla, "", Space(6) & "Buscando Vencimientos de RF Local...")
  Set lcVencimiento_Rf = New Class_Vencimientos_RF
  With lcVencimiento_Rf
    If Not lcVencimiento_Rf.Vencimientos_X_Cuenta(pId_Cuenta:=pId_Cuenta _
                                                , pFecha_Cierre:=pFecha _
                                                , pMensaje:=lMensaje) Then
      Call Fnt_Escribe_Grilla(pGrilla, _
                         cGrillaLog_Error, _
                         Space(8) & .ErrMsg)
      GoTo ExitProcedure
    End If
  End With
  
  '--------------------------------------------------------
  ' CALCULO DE COMISIONES SACADO DE ACA -----CSM 06/05/2009
  '--------------------------------------------------------
  
  '-----------------------------------------------------------------------------------------------------------------------------
  '-- CLASE QUE CONTIENE LOS PROCESOS DE EVENTOS DE CAPITAL NUEVOS EN EL SISTEMA.
  '-----------------------------------------------------------------------------------------------------------------------------
  Set lcComponente_EventoCapital = New Class_Componentes_EventoCapital
  With lcComponente_EventoCapital
    .Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
    If Not .Buscar Then
      Call Fnt_Escribe_Grilla(pGrilla, _
                              cGrillaLog_Error, _
                              "Problemas al guardar paridades de los derivados de la cuenta " & pfCuenta("num_cuenta").Value & ", entrego el siguiente mensaje: " & vbLf & vbLf & .ErrMsg _
                        , pConLog:=True, pId_Log_SubTipo:=.SubTipo_LOG, pId_Log_Proceso:=.Id_Log_Proceso)
      GoTo ExitProcedure
    End If
    
    For Each lReg In .Cursor
      Set lcClass = Fnt_CreateObject(DesCifrado(lReg("COMPONENTE").Value))
      
      If Not lcClass Is Nothing Then
        If Not lcClass.Ejecuta_x_Cuenta(pId_Cuenta:=pId_Cuenta _
                                      , pId_Cierre:=pId_Cierre _
                                      , pFecha_Cierre:=pFecha _
                                      , pfCuenta:=pfCuenta _
                                      , pGrilla:=pGrilla _
                                      ) Then
          Call Fnt_Escribe_Grilla(pGrilla, _
                                  cGrillaLog_Error, _
                                  "Problemas al guardar paridades de los derivados de la cuenta " & pfCuenta("num_cuenta").Value & ", entrego el siguiente mensaje: " & vbLf & vbLf & .ErrMsg _
                                  , pConLog:=True, pId_Log_SubTipo:=.SubTipo_LOG, pId_Log_Proceso:=.Id_Log_Proceso)
          GoTo ExitProcedure
        End If
      End If
      
      Set lcClass = Nothing
    Next
  End With
  Set lcComponente_EventoCapital = Nothing
  '-----------------------------------------------------------------------------------------------------------------------------
  
  Ejecuta_x_Cuenta = True
  
  Rem T�rmino de la ejecuci�n de eventos de capital
  'Call Fnt_Escribe_Grilla(pGrilla, "N", Fnt_SYSDATE & " T�rmino Ejecuci�n de Eventos de Capital.")
 
ExitProcedure:
  Set lcCorte_Cupon = Nothing
  Set lcDividendo = Nothing
  
End Function

