VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cls_CartolaADCex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Dim fErrMsg As String
Dim fErrNum As Double
Dim fForm As Frm_Reporte_CartolaADC_Ex

Private Declare Function GetTempPath Lib "kernel32" _
Alias "GetTempPathA" (ByVal nBufferLength As Long, _
   ByVal lpBuffer As String) As Long

Private Declare Function GetTempFileName Lib "kernel32" _
Alias "GetTempFileNameA" (ByVal lpszPath As String, _
   ByVal lpPrefixString As String, ByVal wUnique As Long, _
   ByVal lpTempFileName As String) As Long

Public fId_Entidad As String
Public fId_Cuenta As String
Public fNum_Cuenta As String
Public fNombre As String
Public fPerfil_Riesgo As String
Public fMoneda As String
Public fId_Empresa As String
Public fId_Moneda_Salida As Integer
Public fCuentas As String
Public fId_Cliente As String
Public fId_Grupo As String
Public fDireccion As String
Public fComuna As String
Public fCiudad As String
Public fTelefono As String
Public fRut As String
Public fEjecutivo As String
Public fMailEjecutivo As String
Public fFonoEjecutivo As String
Public fAtencion As String
Public fPiePagina As String
Public fFecha_Creacion As String
Public fFecha_Cartola As String
Public fFecha_Cierre As String
Public fDecimales As Integer
Public fPatrimonio_ant_total As Double
Public fMontoxCobrar_ant As Double
Public fMontoXPagar_Ant As Double
Public fNombreArchivo As String
Public fId_Moneda_Peso As String
Public fId_Moneda_UF As String
Public fId_Moneda_USD As String
Public fFlgComision As String
Public Direccion As String
Public Comuna As String
Public Ciudad As String
Public Telefono As String
Public Rut As String
Public Ejecutivo As String
Public MailEjecutivo As String
Public FonoEjecutivo As String
Public RutEjecutivo As String
Public Mail As String
Public Fecha_Creacion As String
Public Fecha_Proceso As String
Public Fecha_Cartola As String
Public Fecha_Cierre As String
Public Decimales As Integer
Public Patrimonio_total As Double
Public Monto_por_Cobrar As Double
Public Monto_por_Pagar As Double
Public NombreArchivo As String

Public Type Est_Detalle_Cajas
    Descripcion As String
    saldo       As Double

End Type

Public Function SubTipo_LOG() As Double
  'SubTipo_LOG = eLS_Paises
End Function
'---------------------------------------------------------

'---------------------------------------------------------
'---------------------------------------------------------
'-- Para que funcione la componente
'---------------------------------------------------------
'---------------------------------------------------------
  Public Property Set gDB(ByVal vData As Object)
    Set Proce.gDB = vData
  End Property
  
  Public Property Get gDB() As Object
    Set gDB = Proce.gDB
  End Property
  
  Public Property Let gID_Usuario(ByVal vData As Double)
    Proce.gID_Usuario = vData
  End Property
  
  Public Property Get gID_Usuario() As Double
    gID_Usuario = Proce.gID_Usuario
  End Property
  
  Public Property Let ID_EMPRESA(ByVal vData As Double)
    Proce.gId_Empresa = vData
  End Property
  
  Public Property Get ID_EMPRESA() As Double
    ID_EMPRESA = Proce.gId_Empresa
  End Property
    
  Public Property Set MDI_Principal(pMDI_Principal As Object)
    Set Proce.gMDI_Principal = pMDI_Principal
  End Property
  
  Public Property Get MDI_Principal() As Object
    Set MDI_Principal = Proce.gMDI_Principal
  End Property

  Public Function GetConfig() As hRecord
  Dim lConfig As hRecord
  
    Set lConfig = New hRecord
    With lConfig
      .AddField "Config"
      '----------------------------------
      .Add.Fields("Config").Value = cPROCESS_gDB
      .Add.Fields("Config").Value = cPROCESS_gId_Usuario
      .Add.Fields("Config").Value = cPROCESS_Id_Empresa
      .Add.Fields("config").Value = cPROCESS_MDI_Principal
    End With
    
    Set GetConfig = lConfig
  End Function
'---------------------------------------------------------
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
End Sub

Public Function LoadForm(Optional pModoAuto As Boolean = False) As Object
    Set fForm = New Frm_Reporte_CartolaADC_Ex
    Set fForm.fForm_Container = Me.MDI_Principal.New_Form_Container
    Set fForm.fClass = Me
    
    Load fForm
    Set LoadForm = fForm
    Call DockForm_DLL(fForm, fForm.fForm_Container)
End Function

Private Sub Class_Terminate()
On Error Resume Next

  Set fForm = Nothing
End Sub

Public Function Fnt_ObtienePatrimonio(pConsolidado As String, _
                                pId_Entidad As Integer, _
                                Optional pFecha_Consulta As String = "", _
                                Optional ByRef pPatrimonio As Double = 0, _
                                Optional ByRef pCuentaPorCobrar As Double = 0, _
                                Optional ByRef pCuentaPorPagar As Double = 0, _
                                Optional ByRef pSaldoCaja As Double = 0, _
                                Optional ByRef pMontoCaja As Double = 0, _
                                Optional ByRef pSimultaneas As Double = 0, _
                                Optional ByRef pComision As Double = 0) As Double
Dim lCursor As hRecord
Dim lReg As hFields
Dim lSaldoActivo As Double

    lSaldoActivo = 0
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    
    If pFecha_Consulta = "" Then
        gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(fFecha_Cartola), ePD_Entrada
    Else
        gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(pFecha_Consulta), ePD_Entrada
    End If
    
    gDB.Parametros.Add "PCONSOLIDADO", ePT_Caracter, pConsolidado, ePD_Entrada
    gDB.Parametros.Add "PID_ENTIDAD", ePT_Numero, pId_Entidad, ePD_Entrada
    If fId_Moneda_Salida <> 0 Then
        gDB.Parametros.Add "PID_MONEDA_SALIDA", ePT_Numero, fId_Moneda_Salida, ePD_Entrada
    End If
    
    gDB.Procedimiento = "PKG_CARTOLA_CONS$Patrimonio"
    
    If gDB.EjecutaSP Then
        Set lCursor = gDB.Parametros("Pcursor").Valor
        For Each lReg In lCursor
            pPatrimonio = CDbl(NVL(lReg("PATRIMONIO_MONSALIDA").Value, 0))
            pCuentaPorCobrar = CDbl(NVL(lReg("MONTO_X_COBRAR").Value, 0))
            pCuentaPorPagar = CDbl(NVL(lReg("MONTO_PAGAR").Value, 0))
            pMontoCaja = CDbl(NVL(lReg("MONTO_CAJA").Value, 0))
            pSaldoCaja = CDbl(NVL(lReg("SALDO_CAJA").Value, 0))
            pSimultaneas = CDbl(NVL(lReg("SIMULTANEAS").Value, 0))
            pComision = CDbl(NVL(lReg("COMISION").Value, 0))
            lSaldoActivo = CDbl(NVL(lReg("SALDO_ACTIVO").Value, 0))
        Next
    End If
    gDB.Parametros.Clear
    Set lReg = Nothing
    Set lCursor = Nothing
    Fnt_ObtienePatrimonio = lSaldoActivo
End Function


Public Sub Limpiar()
     fId_Entidad = ""
     fId_Cuenta = ""
     fNum_Cuenta = ""
     fNombre = ""
     fPerfil_Riesgo = ""
     fMoneda = ""
     fId_Empresa = ""
     fId_Moneda_Salida = 0
     fCuentas = ""
     fId_Cliente = ""
     fId_Grupo = ""
    
     fDireccion = ""
     fComuna = ""
     fCiudad = ""
     fTelefono = ""
     fRut = ""
     fEjecutivo = ""
     fMailEjecutivo = ""
     fFonoEjecutivo = ""
     fAtencion = ""
     fPiePagina = ""
    
     fFecha_Creacion = ""
     fFecha_Cartola = ""
     fFecha_Cierre = ""
     fDecimales = 0
    
     fPatrimonio_ant_total = 0
     fMontoxCobrar_ant = 0
     fMontoXPagar_Ant = 0
     fNombreArchivo = ""
     fId_Moneda_Peso = ""
     fId_Moneda_UF = ""
     fId_Moneda_USD = ""
     fFlgComision = ""
        
    
End Sub

Public Function CreateTempFile(sPrefix As String) As String
   Dim sTmpPath As String * 512
   Dim sTmpName As String * 576
   Dim nRet As Long

   nRet = GetTempPath(512, sTmpPath)
   If (nRet > 0 And nRet < 512) Then
      nRet = GetTempFileName(sTmpPath, sPrefix, 0, sTmpName)
      If nRet <> 0 Then
         CreateTempFile = Left$(sTmpName, _
            InStr(sTmpName, vbNullChar) - 1)
      End If
   End If
End Function

Public Function BuscarInfoHeader(pConsolidado As String, pId_Entidad As String, pId_Asesor As Integer, ByRef pCursor, pId_Empresa) As Boolean
Dim lResult As Boolean

    lResult = True
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    Call gDB.Parametros.Add("pconsolidado", ePT_Caracter, pConsolidado, ePD_Entrada)
    'Call gDB.Parametros.Add("pid_empresa", ePT_Numero, IIf(Fnt_EmpresaActual <> 0, Fnt_EmpresaActual, 1), ePD_Entrada)
    If pId_Empresa <> "" Then
        Call gDB.Parametros.Add("pid_empresa", ePT_Numero, pId_Empresa, ePD_Entrada)
    End If
    Call gDB.Parametros.Add("pfecha_Cierre", ePT_Fecha, fFecha_Cartola, ePD_Entrada)
    If pId_Entidad <> "" Then
        Call gDB.Parametros.Add("pid_entidad", ePT_Numero, pId_Entidad, ePD_Entrada)
    End If
    If pId_Asesor <> 0 Then
        Call gDB.Parametros.Add("pid_asesor", ePT_Numero, pId_Asesor, ePD_Entrada)
    End If
    gDB.Procedimiento = "PKG_CARTOLA_CONS$BuscarInfoHeader"
      If gDB.EjecutaSP Then
          Set pCursor = gDB.Parametros("PCursor").Valor
      Else
          lResult = False
      End If
      fErrNum = gDB.Errnum
      fErrMsg = gDB.ErrMsg
    
    gDB.Parametros.Clear
    BuscarInfoHeader = lResult
End Function

Public Sub Sub_Limpiar()
  fPatrimonio_ant_total = 0
  fMontoxCobrar_ant = 0
  fMontoXPagar_Ant = 0
  
  fId_Cuenta = ""
  fNum_Cuenta = ""
  fNombre = ""
  fPerfil_Riesgo = ""
  fMoneda = ""
  fId_Empresa = ""
  
  fDireccion = ""
  fComuna = ""
  fCiudad = ""
  fTelefono = ""
  fRut = ""
  fEjecutivo = ""
  
  fMailEjecutivo = ""
  fFonoEjecutivo = ""
  
  fFecha_Creacion = ""
  
  fFecha_Cartola = ""
  fFecha_Cierre = ""
  fDecimales = 0
End Sub

Public Function Fnt_BuscarIndicadores(pFecha, ByRef pCursor As hRecord) As Boolean
Dim lClass_Entidad As Class_Entidad


  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    Set .gDB = gDB
    Call .AddCampo("Fecha", ePT_Fecha, ePD_Entrada, pFecha)
    
    Fnt_BuscarIndicadores = .Buscar("PKG_VALOR_TIPO_CAMBIO.BuscarIndicadores")
    Set pCursor = .Cursor
    fErrNum = lClass_Entidad.Errnum
    fErrMsg = lClass_Entidad.ErrMsg
  End With
  
  Set lClass_Entidad = Nothing
  End Function
