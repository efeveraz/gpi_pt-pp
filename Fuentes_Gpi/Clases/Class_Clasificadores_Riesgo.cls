VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Clasificadores_Riesgo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "Pkg_Clasificadores_Riesgo"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 57
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_CLASIFICADOR_RIESGO"
    .AddCampo "ID_CLASIFICADOR_RIESGO", ePT_Numero, ePD_Ambos
    .AddCampo "DSC_CLASIFICADOR_RIESGO", ePT_Caracter, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Buscar_Instru_Clasif_Riesgo() As Boolean
  Buscar_Instru_Clasif_Riesgo = fClass_Entidad.Buscar(cfPackage & ".Buscar_Instru_Clasif_Riesgo")
End Function

Public Function Buscar_Clasif_Riesgo_Nemo(ByVal pId_Nemotecnico) As Boolean
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada
    
    .Campo("Id_Nemotecnico").Valor = pId_Nemotecnico
    
    Buscar_Clasif_Riesgo_Nemo = .Buscar(cfPackage & ".Buscar_Clasif_Riesgo_Nemo")
    Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
    fClass_Entidad.Errnum = lClass_Entidad.Errnum
    fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Buscar_Clasif_Riesgo_Instru(ByVal pCod_Instrumento) As Boolean
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada
    
    .Campo("Cod_Instrumento").Valor = pCod_Instrumento
    
    Buscar_Clasif_Riesgo_Instru = .Buscar(cfPackage & ".Buscar_Clasif_Riesgo_Instru")
    Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
    fClass_Entidad.Errnum = lClass_Entidad.Errnum
    fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Guardar() As Boolean
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function
