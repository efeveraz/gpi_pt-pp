VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Bloqueo_Cuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "Pkg_Bloqueo_Cuentas"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  'SubTipo_LOG = 10
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()

  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_BLOQUEO_CUENTA"
    .AddCampo "ID_BLOQUEO_CUENTA", ePT_Numero, ePD_Ambos
    .AddCampo "ID_CIERRE", ePT_Numero, ePD_Entrada
    .AddCampo "FECHA_CIERRE", ePT_Fecha, ePD_Entrada
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_BLOQUEO_NIVEL", ePT_Numero, ePD_Entrada
    .AddCampo "COD_ESTADO", ePT_Caracter, ePD_Entrada
   ' .AddCampo "Id_Empresa", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_USUARIO_INSERT", ePT_Numero, ePD_Entrada
    .AddCampo "ID_USUARIO_UPDATE", ePT_Numero, ePD_Entrada
    
    End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar_Bloqueo() As Boolean
Dim lcClass_entidad As Class_Entidad
  
  Set lcClass_entidad = New Class_Entidad
  With lcClass_entidad
    Call .AddCampo("ID_EMPRESA", ePT_Numero, ePD_Entrada, gId_Empresa)
    Call .AddCampo(Me.Campo("FECHA_CIERRE").Nombre, ePT_Fecha, ePD_Entrada, Me.Campo("FECHA_CIERRE").Valor)
    Call .AddCampo(Me.Campo("ID_CUENTA").Nombre, ePT_Numero, ePD_Entrada, Me.Campo("ID_CUENTA").Valor)
    Call .AddCampo(Me.Campo("ID_BLOQUEO_NIVEL").Nombre, ePT_Numero, ePD_Entrada, Me.Campo("ID_BLOQUEO_NIVEL").Valor)
    Call .AddCampo(Me.Campo("COD_ESTADO").Nombre, ePT_Caracter, ePD_Entrada, Me.Campo("COD_ESTADO").Valor)
    
    Buscar_Bloqueo = .Buscar(cfPackage & ".BUSCAR_BLOQUEO")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lcClass_entidad = Nothing
    
End Function

Public Function Guardar() As Boolean
    
  If IsNull(fClass_Entidad.Campo("ID_USUARIO_INSERT").Valor) Then
    fClass_Entidad.Campo("ID_USUARIO_INSERT").Valor = gID_Usuario
  End If

  If IsNull(fClass_Entidad.Campo("ID_USUARIO_UPDATE").Valor) Then
    fClass_Entidad.Campo("ID_USUARIO_UPDATE").Valor = gID_Usuario
  End If
  
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Anular() As Boolean

  If IsNull(fClass_Entidad.Campo("ID_USUARIO_UPDATE").Valor) Then
    fClass_Entidad.Campo("ID_USUARIO_UPDATE").Valor = gID_Usuario
  End If
  
  Anular = fClass_Entidad.Borrar(cfPackage & ".Anular")

End Function

Public Function GuardarMasivo(Optional lId_Empresa = 0) As Boolean
Dim lcClass_entidad As Class_Entidad

  Set lcClass_entidad = New Class_Entidad
  With lcClass_entidad
      Call .AddCampo("Fecha_Cierre", ePT_Fecha, ePD_Entrada, Me.Campo("fecha_cierre").Valor)
      Call .AddCampo("Id_Bloqueo_Nivel", ePT_Numero, ePD_Entrada, Me.Campo("ID_BLOQUEO_NIVEL").Valor)
      Call .AddCampo("Id_Empresa", ePT_Numero, ePD_Entrada, lId_Empresa)
      Call .AddCampo("Cod_Estado", ePT_Caracter, ePD_Entrada, Me.Campo("COD_ESTADO").Valor)
      Call .AddCampo("Id_Usuario_Update", ePT_Numero, ePD_Entrada, gID_Usuario)
   
     GuardarMasivo = .Buscar(cfPackage & ".GUARDAR_MASIVO")
    
    Set fClass_Entidad.Cursor = .Cursor
    If lId_Empresa <> 0 Then
     Call .DelCampo("Id_Empresa")
    End If
  End With
  Set lcClass_entidad = Nothing
End Function



