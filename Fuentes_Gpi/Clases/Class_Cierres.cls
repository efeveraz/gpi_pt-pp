VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Cierres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Cierres"

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .AddCampo "ID_CIERRE", ePT_Numero, ePD_Entrada
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada
    .AddCampo "FECHA_CIERRE", ePT_Fecha, ePD_Entrada
    .AddCampo "COD_ESTADO", ePT_Caracter, ePD_Entrada
    .AddCampo "FECHA_HORA_INICIO", ePT_Fecha, ePD_Entrada
    .AddCampo "FECHA_HORA_TERMINO", ePT_Fecha, ePD_Entrada
  End With
End Sub

Public Function Buscar_Entre(pId_Empresa, pFecha_Desde, pFecha_Hasta) As Boolean
Dim lClass_Entidad As Class_Entidad

  Buscar_Entre = False

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada, pId_Empresa
    .AddCampo "Fecha_Desde", ePT_Fecha, ePD_Entrada, pFecha_Desde
    .AddCampo "Fecha_Hasta", ePT_Fecha, ePD_Entrada, pFecha_Hasta
    
    Buscar_Entre = .Buscar(cfPackage & ".Buscar_Entre")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function
Public Function BuscarUltimo(pId_Empresa, pFecha_Cierre) As Boolean
Dim lClass_Entidad As Class_Entidad
  BuscarUltimo = False
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada, pId_Empresa
    .AddCampo "FECHA_CIERRE", ePT_Fecha, ePD_Entrada, pFecha_Cierre
    BuscarUltimo = .Buscar(cfPackage & ".Buscar_ultimo")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

