VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Subcuota_ACI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Subcuota_ACI"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLog_Subtipo.eLS_Subcuota_Instrumentos
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_SUBCUOTA_ACI"
    .AddCampo "ID_SUBCUOTA_ACI", ePT_Numero, ePD_Ambos
    .AddCampo "ID_CIERRE", ePT_Numero, ePD_Entrada
    .AddCampo "ID_ARBOL_CLASE_INST", ePT_Numero, ePD_Entrada
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MONEDA_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "FECHA_CIERRE", ePT_Fecha, ePD_Entrada
    .AddCampo "SALDO_ACTIVO_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "VALOR_CUOTA_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "TOTAL_CUOTAS_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "RENTABILIDAD_MON_CUENTA", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Guardar() As Boolean
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

'Public Function Borrar() As Boolean
'  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
'End Function

Public Function Buscar_Entre(pFecha_Desde, pFecha_Hasta) As Boolean
Dim lClass_Entidad As Class_Entidad

  Buscar_Entre = False

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada, fClass_Entidad.Campo("ID_CUENTA").Valor
    .AddCampo "Fecha_Desde", ePT_Fecha, ePD_Entrada, pFecha_Desde
    .AddCampo "Fecha_Hasta", ePT_Fecha, ePD_Entrada, pFecha_Hasta
    .AddCampo "ID_arbol_clase_inst", ePT_Caracter, ePD_Entrada, fClass_Entidad.Campo("ID_arbol_clase_inst").Valor
    
    Buscar_Entre = .Buscar(cfPackage & ".Buscar_Entre")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function CalculaSubcuota_X_ACI(ByVal pId_Arbol_Clase_Inst _
                                    , ByVal pId_Cuenta _
                                    , ByVal pFecha_Cierre _
                                    , ByVal pId_Cierre _
                                    , ByRef pfCuenta As hFields _
                                    , ByRef pGrilla As Object)
Dim lReg            As hFields
Dim lReg_Ayer       As hFields
Dim lReg_Hoy        As hFields
Dim lcMov_Activo    As Class_Mov_Activos
Dim lcSaldo_Activos As Class_Saldo_Activos
Dim lcSubCuota_ACI  As Class_Subcuota_ACI
Dim lcSubCuota_Instrumento  As Class_Subcuota_Instrumentos
'Dim lcTipo_Cambio   As Class_Tipo_Cambios
Dim lcTipo_Cambio   As Object
Dim lcGuion         As Class_AvanceGuion
'---------------------------------------------------------------
Dim lMonto_Mov_Activos    As Double
Dim lMonto_Saldos_Activos As Double
Dim lFecha_Ayer           As Date
Dim lMonto_MA             As Double
'---------------------------------------------------------------
Dim pParidad As Double
Dim pOperacion As String
'---------------------------------------------------------------
Dim lContador_Activos      As Double
Dim lLin_Grilla            As Long
Dim lMsg_Grilla            As String

On Error GoTo ErrProcedure
                                        
  CalculaSubcuota_X_ACI = False
                                        
'---------------------------------------------------------------------------------------
  lLin_Grilla = (pGrilla.Rows - 1)
  lMsg_Grilla = GetCell(pGrilla, lLin_Grilla, "TEXTO")
  
  lContador_Activos = 0
  
  Set lcGuion = New Class_AvanceGuion
'---------------------------------------------------------------------------------------
  
  lFecha_Ayer = pFecha_Cierre - 1
                                        
  lMonto_Mov_Activos = 0
  lMonto_Saldos_Activos = 0

'  /*  BUSCA TODOS LOS ULTIMOS MOVIMIENTOS PENDIENTES DE CIERRE */
'  /*  SE SUMAN LOS INGRESOS (COMPRAS) Y SE RESTAN LOS EGRESOS (VENTAS) */
  Set lcMov_Activo = New Class_Mov_Activos
  lcMov_Activo.Campo("id_cuenta").Valor = pId_Cuenta
  If lcMov_Activo.Buscar_X_Arbol_Clase_Inst(pId_Arbol_Clase_Inst:=pId_Arbol_Clase_Inst, _
                                            pId_Cierre:=pId_Cierre) Then
'    Set lcTipo_Cambio = New Class_Tipo_Cambios
    Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
    For Each lReg In lcMov_Activo.Cursor
      Call SetCell(pGrilla, lLin_Grilla, "TEXTO", lMsg_Grilla & lcGuion.Avanza, False)

      'Coloca el monto en negativo si es un egreso, si no, lo deja como esta.
      lMonto_MA = (lReg("Monto").Value * IIf(lReg("FLG_TIPO_MOVIMIENTO").Value = gcTipoOperacion_Ingreso, 1, -1))

      'Busca la paridad para calcular el monto de la operacion en la moneda de la cuenta
      If Not lcTipo_Cambio.Busca_Precio_Paridad(pId_Cuenta:=pId_Cuenta _
                                            , pId_Moneda_Origen:=lReg("id_moneda").Value _
                                            , pId_Moneda_Destino:=pfCuenta("id_moneda").Value _
                                            , pFecha:=pFecha_Cierre _
                                            , pParidad:=pParidad _
                                            , pOperacion:=pOperacion) Then
        Call Fnt_Escribe_Grilla(pGrilla, _
                                "E", _
                                "Problemas al buscar el precio de los tipos de cambios para la cuenta " & pfCuenta("num_cuenta").Value & ", entrego el siguiente mensaje: " & vbLf & vbLf & lcTipo_Cambio.ErrMsg)
        GoTo ExitProcedure
      End If

      lMonto_MA = Fnt_Calcula_TipoCambio_Ope(lMonto_MA, pParidad, pOperacion)

      'NOTA: Aqui falta transformar el monto a la moneda de la cuenta
      lMonto_Mov_Activos = lMonto_Mov_Activos + lMonto_MA
    Next
  End If

'---------------------------------------------------------------------------------------
'/*  BUSCA TODOS LOS SALDOS ACTIVOS DEL CIERRE ANTERIOR */
'---------------------------------------------------------------------------------------
  Set lcSaldo_Activos = New Class_Saldo_Activos
  With lcSaldo_Activos
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("fecha_cierre").Valor = pFecha_Cierre
    If Not .Buscar_X_Arbol_Clase_Inst(pId_Arbol_Clase_Inst:=pId_Arbol_Clase_Inst) Then
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "La cuenta " & pfCuenta("num_Cuenta").Value & " se encontro el siguiente mensaje: " & vbLf & vbLf & .ErrMsg)
      GoTo ExitProcedure
    End If
  End With
  
  'Calcula cual es el monto de los saldos.
  For Each lReg In lcSaldo_Activos.Cursor
    Call SetCell(pGrilla, lLin_Grilla, "TEXTO", lMsg_Grilla & lcGuion.Avanza, False)
    lMonto_Saldos_Activos = lMonto_Saldos_Activos + lReg("Monto_Mon_Cta").Value
  Next
  
  Set lcSubCuota_ACI = New Class_Subcuota_ACI
  With lcSubCuota_ACI
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("ID_arbol_clase_inst").Valor = pId_Arbol_Clase_Inst
    If Not .Buscar_Entre(lFecha_Ayer, pFecha_Cierre) Then
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "La cuenta " & pfCuenta("num_Cuenta").Value & " se encontro el siguiente mensaje: " & vbLf & vbLf & .ErrMsg)
      GoTo ExitProcedure
    End If
  
    'EL PATRIMONIO DEL DIA DEL CIERRE DE AYER
    Set lReg_Ayer = .Cursor.Buscar("fecha_cierre", lFecha_Ayer)
    'EL PATRIMONIO DEL DIA DEL CIERRE
    Set lReg_Hoy = .Cursor.Buscar("fecha_cierre", pFecha_Cierre)
  End With
  Set lcSubCuota_ACI = Nothing
  
  'Calculo de Rentabilidad
  Set lcSubCuota_Instrumento = New Class_Subcuota_Instrumentos
  Call lcSubCuota_Instrumento.Sub_CalculaCuota_HijoZu(lMonto_Saldos_Activos, _
                                                      lMonto_Mov_Activos, _
                                                      lReg_Ayer, _
                                                      lReg_Hoy)
  Set lcSubCuota_Instrumento = Nothing
  
  Set lcSubCuota_ACI = New Class_Subcuota_ACI
  With lcSubCuota_ACI
    .Campo("ID_CIERRE").Valor = pId_Cierre
    .Campo("fecha_cierre").Valor = pFecha_Cierre
    .Campo("ID_ARBOL_CLASE_INST").Valor = pId_Arbol_Clase_Inst
    .Campo("ID_CUENTA").Valor = pId_Cuenta
    .Campo("ID_MONEDA_CUENTA").Valor = pfCuenta("id_moneda").Value
    .Campo("SALDO_ACTIVO_MON_CUENTA").Valor = lMonto_Saldos_Activos
    .Campo("VALOR_CUOTA_MON_CUENTA").Valor = lReg_Hoy("VALOR_CUOTA_MON_CUENTA").Value
    .Campo("TOTAL_CUOTAS_MON_CUENTA").Valor = lReg_Hoy("TOTAL_CUOTAS_MON_CUENTA").Value
    .Campo("RENTABILIDAD_MON_CUENTA").Valor = lReg_Hoy("RENTABILIDAD_MON_CUENTA").Value
    
    If Not .Guardar Then
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "La cuenta " & pfCuenta("num_Cuenta").Value & " se encontro el siguiente mensaje: " & vbLf & vbLf & .ErrMsg)
      GoTo ExitProcedure
    End If
  End With
  
  CalculaSubcuota_X_ACI = True

ErrProcedure:
  If Not Err.Number = 0 Then
    MsgBox "Problema en el calculo de Subcuota ACI." & vbLf & vbLf & Err.Description, vbCritical
    GoTo ExitProcedure
    Resume
  End If
                                        
ExitProcedure:
  Set lcMov_Activo = Nothing
  Set lcSaldo_Activos = Nothing
  Set lcSubCuota_ACI = Nothing
End Function

Public Function Deshacer_Enlaze_Cierre(ByVal pId_Cuenta As Double, ByVal pFecha_Cierre As Date)
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    Call .AddCampo("id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta)
    Call .AddCampo("fecha_cierre", ePT_Fecha, ePD_Entrada, pFecha_Cierre)
    
    Deshacer_Enlaze_Cierre = .Borrar(cfPackage & ".DESHACER_ENLAZE_CIERRE")
    fClass_Entidad.ErrMsg = .ErrMsg
    fClass_Entidad.Errnum = .Errnum
  End With
  Set lClass_Entidad = Nothing
End Function
