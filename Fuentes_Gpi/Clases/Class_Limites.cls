VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Limites"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: Class_Limites.cls $
'    $Author: Gbuenrostro $
'    $Date: 13/11/12 17:48 $
'    $Revision: 5 $
'-------------------------------------------------------------------------------------------------

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad
Public xCursor As hRecord
Const cfPackage = "Pkg_Saldos_Activos"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
    SubTipo_LOG = 37
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
    Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
    Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
    ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
    Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
    Set fClass_Entidad = New Class_Entidad
    Call LimpiaParam
End Sub

Public Sub LimpiaParam()
    With fClass_Entidad
        .LimpiaParam
        .CampoPKey = "Id_Saldo_Activo"
        .AddCampo "Id_Saldo_Activo", ePT_Numero, ePD_Ambos
        '----------------------------------------------------
        '-- nuevos campos para el cierre
        '----------------------------------------------------
        .AddCampo "Id_Cierre", ePT_Numero, ePD_Entrada
        '----------------------------------------------------
        .AddCampo "Fecha_Cierre", ePT_Fecha, ePD_Entrada
        .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada
        .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada
        .AddCampo "ID_ASESOR", ePT_Numero, ePD_Entrada
        .AddCampo "ID_CLIENTE", ePT_Numero, ePD_Entrada
        .AddCampo "ID_PERFIL_RIESGO", ePT_Numero, ePD_Entrada
        .AddCampo "ID_FAMILIA", ePT_Caracter, ePD_Entrada
        .AddCampo "ID_SUB_FAMILIA", ePT_Caracter, ePD_Entrada
        .AddCampo "ID_EXCESO", ePT_Caracter, ePD_Entrada
        .AddCampo "Id_Por_Familia", ePT_Numero, ePD_Entrada
        .AddCampo "Id_Por_Sub_Familia", ePT_Numero, ePD_Entrada
        .AddCampo "Id_Arbol_Clase_Inst", ePT_Caracter, ePD_Entrada


    End With
End Sub

Public Function Trae_Control_Limites(pId_Empresa, _
                                    Optional pIdAsesor = Null, _
                                    Optional pIdCliente = Null, _
                                    Optional pIdCuenta = Null, _
                                    Optional pPerfil = Null, _
                                    Optional pFamilia = Null, _
                                    Optional pSubFamilia = Null, _
                                    Optional pExceso = Null, _
                                    Optional pid_Por_Familia = Null, _
                                    Optional pid_Por_SubFamilia = Null, _
                                    Optional lcod_Tipo_Administracion = Null) As Boolean

Dim lClass_Entidad As New Class_Entidad

Trae_Control_Limites = False

With lClass_Entidad
        Call .AddCampo("FECHA_CIERRE", ePT_Fecha, ePD_Entrada, Me.Campo("FECHA_CIERRE").Valor)
        Call .AddCampo("ID_EMPRESA", ePT_Numero, ePD_Entrada, pId_Empresa)
        Call .AddCampo("ID_ASESOR", ePT_Numero, ePD_Entrada, pIdAsesor)
        Call .AddCampo("ID_CLIENTE", ePT_Numero, ePD_Entrada, pIdCliente)
        Call .AddCampo("ID_CUENTA", ePT_Numero, ePD_Entrada, pIdCuenta)
        Call .AddCampo("ID_PERFIL_RIESGO", ePT_Numero, ePD_Entrada, pPerfil)
        Call .AddCampo("ID_FAMILIA", ePT_Caracter, ePD_Entrada, pFamilia)
        Call .AddCampo("ID_SUB_FAMILIA", ePT_Caracter, ePD_Entrada, pSubFamilia)
        Call .AddCampo("ID_EXCESO", ePT_Caracter, ePD_Entrada, pExceso)
        Call .AddCampo("ID_POR_FAMILIA", ePT_Numero, ePD_Entrada, pid_Por_Familia)
        Call .AddCampo("ID_POR_SUB_FAMILIA", ePT_Numero, ePD_Entrada, pid_Por_SubFamilia)
        Call .AddCampo("COD_TIPO_ADMIN", ePT_Caracter, ePD_Entrada, lcod_Tipo_Administracion)


        Trae_Control_Limites = .Buscar(cfPackage & ".Trae_Control_Limite")

        Set fClass_Entidad.Cursor = .Cursor

        fClass_Entidad.Errnum = .Errnum
        fClass_Entidad.ErrMsg = .ErrMsg
End With

Set lClass_Entidad = Nothing

End Function


