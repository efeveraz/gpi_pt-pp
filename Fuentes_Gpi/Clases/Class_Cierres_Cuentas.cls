VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Cierres_Cuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Cierres_Cuentas"

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function ErrNum() As Double
  ErrNum = fClass_Entidad.ErrNum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .AddCampo "ID_CIERRE", ePT_Numero, ePD_Entrada
    .AddCampo "FECHA_CIERRE", ePT_Fecha, ePD_Entrada
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "COD_ESTADO", ePT_Caracter, ePD_Entrada
    .AddCampo "FECHA_HORA_INICIO", ePT_Fecha, ePD_Entrada
    .AddCampo "FECHA_HORA_TERMINO", ePT_Fecha, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function BuscarUltimo() As Boolean
  BuscarUltimo = fClass_Entidad.Buscar(cfPackage & ".Buscar_ultimo")
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Guardar_ReProcesos()
Dim lClass_Entidad As New Class_Entidad

  Guardar_ReProcesos = False

  With lClass_Entidad
    .AddCampo "Id_Cuenta", ePT_Caracter, ePD_Entrada
    .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada
    .AddCampo "COD_ESTADO", ePT_Caracter, ePD_Entrada
    
    .Campo("id_cuenta").Valor = Campo("Id_cuenta").Valor
    .Campo("fecha_cierre").Valor = Campo("Fecha_Cierre").Valor
    .Campo("COD_ESTADO").Valor = Campo("COD_ESTADO").Valor
    
    If .Guardar(cfPackage & ".Guardar_ReProcesos") Then
      Guardar_ReProcesos = True
    Else
      fClass_Entidad.ErrNum = lClass_Entidad.ErrNum
      fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
    End If
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Tipos_Cambios_Cartera() As Boolean
  Tipos_Cambios_Cartera = fClass_Entidad.Buscar(cfPackage & ".TIPOS_CAMBIOS_CARTERA")
End Function

Public Function Buscar_Totales(ByRef Ptot_Saldos_Activos _
                             , ByRef Ptot_Saldos_Cajas _
                             , ByRef Ptot_X_Cobrar _
                             , ByRef Ptot_X_Pagar _
                             , ByRef Ptot_Com_Dev) As Boolean

  With fClass_Entidad
    Buscar_Totales = .Buscar(cfPackage & ".Buscar_Totales")
    
    If .ErrNum = 0 Then
      Ptot_Saldos_Activos = .Cursor(1)("tot_Saldos_Activos").Value
      Ptot_Saldos_Cajas = .Cursor(1)("tot_Saldos_Cajas").Value
      Ptot_X_Cobrar = .Cursor(1)("TOT_X_COBRAR").Value
      Ptot_X_Pagar = .Cursor(1)("TOT_X_PAGAR").Value
      Ptot_Com_Dev = .Cursor(1)("TOT_COM_DEV").Value
    End If
  End With
End Function

Public Function Primer_Aporte() As Variant
On Error GoTo ErrProcedure
  
  With gDB
    .Parametros.Clear
    .Procedimiento = cfPackage & ".PRIMER_APORTE"
    .Parametros.Add "pid_cuenta", ePT_Numero, Campo("id_cuenta").Valor, ePD_Entrada
    If .EjecutaFunction(ePT_Fecha) Then
      Primer_Aporte = .Parametros("pResultado").Valor
    End If
  End With

ErrProcedure:
  fClass_Entidad.ErrNum = gDB.ErrNum
  fClass_Entidad.ErrMsg = gDB.ErrMsg
End Function

Public Function Borrar_Historia_Cuenta() As Boolean
Dim lClass_Entidad As Class_Entidad

  Borrar_Historia_Cuenta = False

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada, Me.Campo("ID_CUENTA").Valor
    .AddCampo "Fecha_Cierre", ePT_Fecha, ePD_Entrada, Me.Campo("Fecha_Cierre").Valor
    
    Borrar_Historia_Cuenta = .Borrar(cfPackage & ".BORRAR_HISTORIA_CUENTA")
    fClass_Entidad.ErrNum = .ErrNum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

