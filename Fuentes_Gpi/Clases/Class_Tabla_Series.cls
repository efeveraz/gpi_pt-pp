VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Tabla_Series"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "PKG_SERIES_NEMOTECNICO"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLS_Nemotecnico
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "idserie"
    
    .AddCampo "idserie", ePT_Numero, ePD_Ambos
    .AddCampo "codserie", ePT_Caracter, ePD_Entrada
    .AddCampo "tipoamortizacion", ePT_Caracter, ePD_Entrada
    .AddCampo "tasaemision", ePT_Numero, ePD_Entrada
    .AddCampo "tasaefectiva", ePT_Numero, ePD_Entrada
    .AddCampo "fechaemision", ePT_Fecha, ePD_Entrada
    .AddCampo "fechavcto", ePT_Fecha, ePD_Entrada
    .AddCampo "nrocupones", ePT_Numero, ePD_Entrada
    .AddCampo "nroamortizaciones", ePT_Numero, ePD_Entrada
    .AddCampo "diavctocupon", ePT_Numero, ePD_Entrada
    .AddCampo "periodocupon", ePT_Numero, ePD_Entrada
    .AddCampo "anos", ePT_Numero, ePD_Entrada
    .AddCampo "decimalestd", ePT_Numero, ePD_Entrada
    .AddCampo "fechaprimercorte", ePT_Fecha, ePD_Entrada
    .AddCampo "corteminimo", ePT_Numero, ePD_Entrada
    .AddCampo "idemisor", ePT_Numero, ePD_Entrada
    .AddCampo "idmoneda", ePT_Numero, ePD_Entrada
    .AddCampo "idfamilia", ePT_Numero, ePD_Entrada
    .AddCampo "seriebanco", ePT_Caracter, ePD_Entrada
   
  End With
End Sub

Public Function Buscar()
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Serie_por_Nemotecnicos(pNemotecnico As String)
    With fClass_Entidad
        Call .AddCampo("Nemotecnico", ePT_Caracter, ePD_Entrada, pNemotecnico)
        Serie_por_Nemotecnicos = .Buscar(cfPackage & ".Buscar_x_Nemo")
    End With
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function




