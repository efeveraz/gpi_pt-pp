VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Alias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "PKG_TIPOS_CONVERSION"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 76
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function ErrNum() As Double
  ErrNum = fClass_Entidad.ErrNum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .AddCampo "Id_Tipo_Conversion", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Origen", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Entidad", ePT_Numero, ePD_Entrada
    .AddCampo "Valor", ePT_Caracter, ePD_Entrada
  End With
End Sub

Public Function Buscar(Optional pCod_Producto As String = "") As Boolean
  With fClass_Entidad
  
    If Not pCod_Producto = "" Then
      .AddCampo "Cod_Producto", ePT_Caracter, ePD_Entrada
      .Campo("Cod_Producto").Valor = pCod_Producto
    End If
    
    Buscar = .Buscar(cfPackage & ".BuscarView")
    
  End With
End Function
