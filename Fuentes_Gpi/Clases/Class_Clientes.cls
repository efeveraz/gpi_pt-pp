VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Clientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim fCampos As Parametros
Dim fErrNum As Double
Dim fErrMsg As String

Dim fCursor As hRecord

Public Function Buscar(Optional pEnVista As Boolean = False)
Dim lParam  As DLL_COMUN.Parametro
  
  Buscar = True
  
  gDB.Parametros.Clear
  For Each lParam In fCampos
    'Primero verifica que ningun campo este en null
    If Not IsNull(lParam.valor) Then
      'Se les agrega la "p" debido a que son parametros
      Call gDB.Parametros.Add(lParam.Nombre, lParam.Tipo, lParam.valor, lParam.Direccion)
    End If
  Next

  If pEnVista Then
    gDB.Tabla = "View_Clientes"
  Else
    gDB.Tabla = "Clientes"
  End If
  gDB.OrderBy = "Rut_Cliente"
  
  If gDB.EjecutaSelect Then
    Set fCursor = gDB.Parametros("cursor").valor
  Else
    'Si existe un problema el proceso devuelve false
    Buscar = False
    fErrNum = gDB.Errnum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Cursor() As hRecord
  Set Cursor = fCursor
End Function


Private Sub Class_Initialize()
  Set fCampos = New Parametros
  Set fCursor = Nothing
  
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fCampos
    .Clear
    .Add "Id_Cliente", ePT_Numero, Null, ePD_Entrada
    .Add "Id_Tipo_Estado", ePT_Numero, Null, ePD_Entrada
    .Add "Cod_Estado", ePT_Caracter, Null, ePD_Entrada
    .Add "Cod_Pais_Conyuge", ePT_Caracter, Null, ePD_Entrada
    .Add "Cod_Pais", ePT_Caracter, Null, ePD_Entrada
    .Add "Rut_Cliente", ePT_Caracter, Null, ePD_Entrada
    .Add "Tipo_Entidad", ePT_Caracter, Null, ePD_Entrada
    .Add "Paterno", ePT_Caracter, Null, ePD_Entrada
    .Add "Materno", ePT_Caracter, Null, ePD_Entrada
    .Add "Nombres", ePT_Caracter, Null, ePD_Entrada
    .Add "Profesion", ePT_Caracter, Null, ePD_Entrada
    .Add "Sexo", ePT_Caracter, Null, ePD_Entrada
    .Add "Fecha_Nacimiento", ePT_Fecha, Null, ePD_Entrada
    .Add "Estado_Civil", ePT_Caracter, Null, ePD_Entrada
    .Add "Celular", ePT_Caracter, Null, ePD_Entrada
    .Add "Observaciones", ePT_Caracter, Null, ePD_Entrada
    .Add "Email", ePT_Caracter, Null, ePD_Entrada
    .Add "Empleador", ePT_Caracter, Null, ePD_Entrada
    .Add "Cargo", ePT_Caracter, Null, ePD_Entrada
    .Add "Rut_Conyuge", ePT_Caracter, Null, ePD_Entrada
    .Add "Paterno_Conyuge", ePT_Caracter, Null, ePD_Entrada
    .Add "Materno_Conyuge", ePT_Caracter, Null, ePD_Entrada
    .Add "Nombres_Conyuge", ePT_Caracter, Null, ePD_Entrada
    .Add "Copia_Escritura", ePT_Caracter, Null, ePD_Entrada
    .Add "Razon_Social", ePT_Caracter, Null, ePD_Entrada
    .Add "Id_Tipo_Sociedad", ePT_Caracter, Null, ePD_Entrada
    .Add "Giro", ePT_Caracter, Null, ePD_Entrada
    .Add "Fecha_Formacion", ePT_Fecha, Null, ePD_Entrada
    .Add "Lugar", ePT_Caracter, Null, ePD_Entrada
    .Add "Notaria", ePT_Caracter, Null, ePD_Entrada
    .Add "Registro", ePT_Caracter, Null, ePD_Entrada
    .Add "Numero", ePT_Caracter, Null, ePD_Entrada
    .Add "Ano", ePT_Caracter, Null, ePD_Entrada
    .Add "Antecedentes", ePT_Caracter, Null, ePD_Entrada
    .Add "Segmento_Cliente", ePT_Numero, Null, ePD_Entrada
    .Add "Sucursal", ePT_Caracter, Null, ePD_Entrada
    .Add "Nombre_Ejecutivo", ePT_Caracter, Null, ePD_Entrada
  End With
End Sub


Public Function Campo(pCampo As String) As Parametro
  Set Campo = fCampos(pCampo)
End Function

Public Function Errnum() As Double
  Errnum = fErrNum
End Function

Public Function ErrMsg() As String
  ErrMsg = fErrMsg
End Function

