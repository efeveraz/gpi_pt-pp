VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Empresas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad

Rem ------------------------------------------------------
Rem 17/07/2009 MMardones. Agrega campo a Empresas para
Rem   el factor de descuento en un ingreso de operaciones
Rem   por Pesos
Rem ------------------------------------------------------

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "PKG_EMPRESAS"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 27
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_EMPRESA"
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Ambos
'    .AddCampo "DSC_EMPRESA", ePT_Caracter, ePD_Entrada
'    .AddCampo "ABR_EMPRESA", ePT_Caracter, ePD_Entrada
'    .AddCampo "ID_MONEDA", ePT_Numero, ePD_Entrada
    .AddCampo "CTA_CTE", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_BANCO", ePT_Numero, ePD_Entrada
    .AddCampo "LIMITE_CAJA", ePT_Numero, ePD_Entrada
    .AddCampo "FLG_BLOQUEO_CUENTAS", ePT_Caracter, ePD_Entrada
    .AddCampo "FLG_CORTES_RF", ePT_Caracter, ePD_Entrada
    .AddCampo "FACTOR_DCTO_INGRESO_PESO", ePT_Numero, ePD_Entrada   '17/07/2009 Agregado por MMardones
    .AddCampo "RUTA_CARTOLA_CAMP", ePT_Caracter, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

