VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_RelogDB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public LstImgRecord As Object
Public LstImgDB As Object
Public PictureIndicator As Object

Dim fIndex As Integer
Dim fMilesegundos As Long

Public Sub AvanzaDB()
Dim lCntImg As Integer

  If (GetTickCount - fMilesegundos) < 100 Then
    Exit Sub
  End If
  
  lCntImg = 0
  If Not LstImgDB Is Nothing Then
    'lCntImg = mvarLstImgDB.ListImages.Count
    lCntImg = LstImgDB.ListImages.Count
  End If
  
  If PictureIndicator Is Nothing Then
    lCntImg = 0
  End If

  If lCntImg > 0 Then
    fIndex = fIndex + 1
    If fIndex > lCntImg Then
      fIndex = 1
    End If
    
    fMilesegundos = GetTickCount
    On Error Resume Next
    PictureIndicator.Picture = LstImgDB.ListImages(fIndex).Picture
    PictureIndicator.ToolTipText = "�ltima lectura DB el " & Now
    On Error GoTo 0
  End If
End Sub

Public Sub AvanzaRelog()
Dim lCntImg As Integer

  If (GetTickCount - fMilesegundos) < 50 Then
    Exit Sub
  End If
  
  lCntImg = 0
  If Not LstImgRecord Is Nothing Then
    'lCntImg = mvarLstImgRecord.ListImages.Count
    lCntImg = LstImgRecord.ListImages.Count
  End If
  
  If PictureIndicator Is Nothing Then
    lCntImg = 0
  End If

  If lCntImg > 0 Then
    fIndex = fIndex + 1
    If fIndex > lCntImg Then
      fIndex = 1
    End If
    
    fMilesegundos = GetTickCount
    On Error Resume Next
    PictureIndicator.Picture = LstImgRecord.ListImages(fIndex).Picture
    PictureIndicator.ToolTipText = "�ltima lectura DB el " & Now
    On Error GoTo 0
  End If
End Sub

Private Sub Class_Initialize()
  fIndex = 0
  fMilesegundos = 0
  Set PictureIndicator = Nothing
  Set LstImgRecord = Nothing
End Sub

Private Sub Class_Terminate()
  Set PictureIndicator = Nothing
  Set LstImgRecord = Nothing
End Sub
