VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Cargos_Abonos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "Pkg_Cargos_Abonos"
Dim fCursor As hRecord
Dim fErrNum As Double
Dim fErrMsg As String

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 34
End Function
'---------------------------------------------------------

Public Sub Sub_CopiarReg2Campos(pReg As hFields)
  Call fClass_Entidad.Sub_CopiarReg2Campos(pReg)
End Sub


Public Function Buscar()
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
Attribute Campo.VB_UserMemId = 0
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Cargo_Abono"
    .AddCampo "Id_Cargo_Abono", ePT_Numero, ePD_Ambos
    .AddCampo "Cod_Origen_Mov_Caja", ePT_Caracter, ePD_Entrada
    .AddCampo "Id_Mov_Caja", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Caja_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Moneda", ePT_Numero, ePD_Entrada
    .AddCampo "Cod_Estado", ePT_Caracter, ePD_Entrada
    .AddCampo "Dsc_Cargo_Abono", ePT_Caracter, ePD_Entrada
    .AddCampo "Fecha_Movimiento", ePT_Fecha, ePD_Entrada
    .AddCampo "Flg_Tipo_Cargo", ePT_Caracter, ePD_Entrada
    .AddCampo "Monto", ePT_Numero, ePD_Entrada
    .AddCampo "Retencion", ePT_Numero, ePD_Entrada
    .AddCampo "Flg_Tipo_Origen", ePT_Caracter, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Guardar(Optional pAutomatico As Boolean = False) As Boolean
Dim lMov_Caja As New Class_Mov_Caja

  With lMov_Caja
    .Campo("Id_Mov_Caja").Valor = Campo("Id_Mov_Caja").Valor
    .Campo("Id_Caja_Cuenta").Valor = Campo("Id_Caja_Cuenta").Valor
    .Campo("Cod_Estado").Valor = Campo("Cod_Estado").Valor
    .Campo("Cod_Origen_Mov_Caja").Valor = Campo("Cod_Origen_Mov_Caja").Valor
    .Campo("Fecha_Movimiento").Valor = Campo("Fecha_Movimiento").Valor
    .Campo("Fecha_Liquidacion").Valor = (Campo("Fecha_Movimiento").Valor + Campo("retencion").Valor)
    .Campo("Monto").Valor = Campo("Monto").Valor
    .Campo("Flg_Tipo_Movimiento").Valor = Campo("Flg_Tipo_Cargo").Valor
    .Campo("Dsc_Mov_Caja").Valor = Campo("Dsc_Cargo_Abono").Valor
    If pAutomatico Then
      .Campo("Flg_Mov_Automatico").Valor = gcFlg_SI
    Else
      .Campo("Flg_Mov_Automatico").Valor = gcFlg_NO
    End If
    If Not .Guardar Then
      Guardar = False
      fClass_Entidad.Errnum = .Errnum
      fClass_Entidad.ErrMsg = .ErrMsg
      GoTo ErrProcedure
    End If
    
    Campo("Id_Mov_Caja").Valor = .Campo("Id_Mov_Caja").Valor
  End With

  If Not fClass_Entidad.Guardar(cfPackage & ".Guardar") Then
    Guardar = False
    GoTo ErrProcedure
  End If
  
  Guardar = True

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Property Let CampoPKey(ByVal vData As String)
  fClass_Entidad.CampoPKey = vData
End Property

Friend Property Get CampoPKey() As String
  CampoPKey = fClass_Entidad.CampoPKey
End Property

Public Function Anular() As Boolean
Dim lMov_Caja As Class_Mov_Caja
Dim lId_Mov_Caja As String
Dim lRollback As Boolean

  Anular = True
  'gDB.IniciarTransaccion
  lRollback = False
  
  If IsNull(Me.Campo("Id_Mov_Caja").Valor) Then
    Rem Primero busca el ID_Mov_Caja del cargo_abono
    With fClass_Entidad
      If .Buscar(cfPackage & ".Buscar") Then
        lId_Mov_Caja = .Cursor(1)("Id_Mov_Caja").Value
      Else
        lRollback = True
        GoTo ErrProcedure
      End If
    End With
  Else
    lId_Mov_Caja = Me.Campo("Id_Mov_Caja").Valor
  End If
    
  Rem Luego anula el cargo_abono
  With gDB
    .Parametros.Clear
    .Parametros.Add "p" & fClass_Entidad.Campo("id_cargo_abono").Nombre, ePT_Numero, fClass_Entidad.Campo("id_cargo_abono").Valor, ePD_Ambos
    .Procedimiento = cfPackage & ".Anular"
    If Not .EjecutaSP Then
      lRollback = True
      GoTo ErrProcedure
    End If
    .Parametros.Clear
  End With
  
  Rem Por ultimo borra el mov_caja asociado
  Set lMov_Caja = New Class_Mov_Caja
  With lMov_Caja
    '.Campo("id_mov_caja").Valor = lId_Mov_Caja
    If Not .Anular(lId_Mov_Caja) Then
      lRollback = True
      fClass_Entidad.Errnum = .Errnum
      fClass_Entidad.ErrMsg = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  
ErrProcedure:
  If lRollback Then
    Anular = False
    'gDB.RollbackTransaccion
  Else
    'gDB.CommitTransaccion
  End If
  Set lMov_Caja = Nothing
  gDB.Parametros.Clear
  Err.Clear
  
End Function

Public Function Borrar() As Boolean
Dim lcMov_Caja     As Class_Mov_Caja
Dim lcCargo_Abono  As Class_Cargos_Abonos
'-----------------------------------------------
Dim lId_Mov_Caja
Dim lRollback     As Boolean

  Borrar = False
  
  Set lcCargo_Abono = New Class_Cargos_Abonos
  With lcCargo_Abono
    .Campo("ID_CARGO_ABONO").Valor = Me.Campo("ID_CARGO_ABONO").Valor
    If Not .Buscar Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      'SI AL BUSCAR NO ENCUENTRA NINGUN CARGO NO REALIZA NADA MAS, OBVIO
      'HAF: 04/01/2008
      GoTo ExitProcedure
    End If
    
    lId_Mov_Caja = lcCargo_Abono.Cursor(1)("Id_Mov_Caja").Value
  End With
  Set lcCargo_Abono = Nothing
  
  If Not fClass_Entidad.Borrar(cfPackage & ".Borrar") Then
    GoTo ExitProcedure
  End If
  
  'SI TIENE MOV_CAJA (DEBERIA SIEMPRE TENER UNA) SE ELIMINA ESTA PRIMERA ANTES DE ELIMINAR EL CARGO
  'HAF: 04/01/2008
  If Not IsNull(lId_Mov_Caja) Then
    Set lcMov_Caja = New Class_Mov_Caja
    With lcMov_Caja
      .Campo("Id_Mov_Caja").Valor = lId_Mov_Caja
      If Not .Borrar() Then
        fClass_Entidad.ErrMsg = .ErrMsg
        fClass_Entidad.Errnum = .Errnum
        GoTo ExitProcedure
      End If
    End With
    Set lcMov_Caja = Nothing
  End If
    
    
  Borrar = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcMov_Caja = Nothing
  Set lcCargo_Abono = Nothing
End Function
Public Function Buscar_CargaAbonoPorFecha(lId_Cuenta, ldFechaIni, ldFechaTer, Optional p_buscar As Boolean = False) As Boolean
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_Cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    gDB.Parametros.Add "PFecha_Movimiento_ini", ePT_Fecha, ldFechaIni, ePD_Entrada
    gDB.Parametros.Add "PFecha_Movimiento_ter", ePT_Fecha, ldFechaTer, ePD_Entrada
    gDB.Procedimiento = cfPackage & ".buscar"
    If Not gDB.EjecutaSP Then
        Buscar_CargaAbonoPorFecha = False
        MsgBox "Problemas al traer los Cargos y Abonos.", vbCritical
        Exit Function
    End If
    Set fClass_Entidad.Cursor = gDB.Parametros("Pcursor").Valor
    If fClass_Entidad.Cursor.Count <= 0 Then
        If p_buscar Then
            MsgBox "No se encontró información Solicitada.", vbExclamation
        End If
        Buscar_CargaAbonoPorFecha = False
        Exit Function
    End If
    Buscar_CargaAbonoPorFecha = True
End Function

'Agregado por MMA 10/02/2009
Public Function AnularPorMovCaja() As Boolean
Dim lRollback As Boolean

  AnularPorMovCaja = True
  'gDB.IniciarTransaccion
  lRollback = False
  
  With gDB
    .Parametros.Clear
    .Parametros.Add "p" & fClass_Entidad.Campo("id_mov_caja").Nombre, ePT_Numero, fClass_Entidad.Campo("id_mov_caja").Valor, ePD_Entrada
    .Procedimiento = cfPackage & ".AnularPorMovCaja"
    If Not .EjecutaSP Then
      lRollback = True
      GoTo ErrProcedure
    End If
    .Parametros.Clear
  End With
  
  
ErrProcedure:
  If lRollback Then
    AnularPorMovCaja = False
    'gDB.RollbackTransaccion
  Else
    'gDB.CommitTransaccion
  End If
  gDB.Parametros.Clear
  Err.Clear
  
End Function

