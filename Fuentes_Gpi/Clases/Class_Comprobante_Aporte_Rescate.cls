VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Comprobante_Aporte_Rescate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad

Const cfPackage = "PKG_APORTES_RETIROS_1862"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso
' Const Plantilla As String = "ACTA DE ENTREGA.DOC"    ' Nombre del documento

Private Type TLineaDetalle
    Nemotecnico As String
    Cantidad    As String
    Precio      As String
    Total       As String
End Type

Private Type TVariable
    sNombre As String
    sValor  As String
End Type

Public FechaProceso     As String
Public sCorredor        As String
Public sRutCorredor     As String
Public nMontoTotal      As String
Public sMontoScrito     As String
Public sFondo           As String
Public sPlantilla       As String

Private nTotalDetalles      As Integer
Private oDetalleFichas()    As TLineaDetalle

Private nTotalVariables     As Integer
Private oVariables()        As TVariable

Private Sub Class_Initialize()
    Set fClass_Entidad = New Class_Entidad
    Call LimpiaParam
    
    FechaProceso = ""
    sCorredor = ""
    sRutCorredor = ""
    nMontoTotal = ""
    sMontoScrito = ""
    sFondo = ""
    nTotalDetalles = 0
    
    sPlantilla = "ComprobanteRetiroCapital.doc"
    
End Sub

Public Function SubTipo_LOG() As Double
  ' SubTipo_LOG = eLog_Subtipo.eLS_Instrumentos
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
    Errnum = fClass_Entidad.Errnum
End Function

Public Function Buscar(Optional pEnVista As Boolean = False)
End Function

Public Function Buscar_ClientesConComprobantes()
    Buscar_ClientesConComprobantes = fClass_Entidad.Buscar(cfPackage & ".ClientesConComprobantes")
End Function

Public Sub LimpiaParam()
    With fClass_Entidad
        .LimpiaParam
        .CampoPKey = "ID_COMPROBANTE"
        Call .AddCampo("ID_COMPROBANTE", ePT_Numero, ePD_Entrada)
        Call .AddCampo("ID_CLIENTE", ePT_Numero, ePD_Entrada)
        Call .AddCampo("NUMERO_FOLIO", ePT_Numero, ePD_Entrada)
        Call .AddCampo("Fecha", ePT_Fecha, ePD_Entrada)
        Call .AddCampo("Tipo_Movimiento", ePT_Caracter, ePD_Entrada)
        Call .AddCampo("ID_APO_RES_CUENTA", ePT_Numero, ePD_Entrada)
        Call .AddCampo("ID_OPERACION", ePT_Numero, ePD_Entrada)
        Call .AddCampo("COD_ESTADO", ePT_Caracter, ePD_Entrada)
        Call .AddCampo("FLG_FIRMADO", ePT_Caracter, ePD_Entrada)
    End With
End Sub

Public Function Buscar_ComprobantesDelClientePorFecha(lId_Cliente, ldFechaIni, ldFechaTer) As Boolean
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_Empresa", ePT_Numero, gId_Empresa, ePD_Entrada
    gDB.Parametros.Add "Pid_Cliente", ePT_Numero, lId_Cliente, ePD_Entrada
    gDB.Parametros.Add "pfechaIni", ePT_Fecha, ldFechaIni, ePD_Entrada
    gDB.Parametros.Add "pfechaFin", ePT_Fecha, ldFechaTer, ePD_Entrada
    
    gDB.Procedimiento = cfPackage & ".ComprobantesPorFecha"
    
    If Not gDB.EjecutaSP Then
        Buscar_ComprobantesDelClientePorFecha = False
        Exit Function
    End If
    
    Set fClass_Entidad.Cursor = gDB.Parametros("Pcursor").Valor
    
    Buscar_ComprobantesDelClientePorFecha = True
    
End Function

Public Function Buscar_ComprobantesPorIDOperacion(lIdOperacion, lTipoOperacion) As Boolean
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_Empresa", ePT_Numero, gId_Empresa, ePD_Entrada
    gDB.Parametros.Add "pId_Key", ePT_Numero, lIdOperacion, ePD_Entrada
    gDB.Parametros.Add "pTipoOperacion", ePT_Caracter, lTipoOperacion, ePD_Entrada
    
    gDB.Procedimiento = cfPackage & ".ComprobantesPorIDOperacion"
    
    If Not gDB.EjecutaSP Then
        Buscar_ComprobantesPorIDOperacion = False
        Exit Function
    End If
    
    Set fClass_Entidad.Cursor = gDB.Parametros("Pcursor").Valor
    
    Buscar_ComprobantesPorIDOperacion = True
    
End Function


Public Function Buscar_DetalleComprobante() As Boolean
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_Comprobante", ePT_Numero, Campo("ID_COMPROBANTE").Valor, ePD_Entrada
    
    gDB.Procedimiento = cfPackage & ".DetalleComprobante"
    
    If Not gDB.EjecutaSP Then
        Buscar_DetalleComprobante = False
        Exit Function
    End If
    
    Set fClass_Entidad.Cursor = gDB.Parametros("Pcursor").Valor
    
    Buscar_DetalleComprobante = True
    
End Function



'---------------------------------------------------------------------------------------
' Procedimiento : GeneraCarta
' Fecha         : 22/09/2006 16:01
' Autor         : Jorge Vidal Bastias
' Proposito     : Generar la carta
' Logica        : 1. Abre la Plantilla
'                 2. Reemplaza los TAG de la plantilla encerrados en {}
'                 3. Grabar el documento con otro nombre
'                 4. Salir sin Guardar (para conservar la plantilla ;))
'---------------------------------------------------------------------------------------
Public Function GeneraComprobante(ByVal fNombreArchivo As String) As Boolean
    Dim DocWord     As Word.Application
    Dim oTabla      As Word.Table
    '-----------------------------------
    Dim nX          As Integer
    Dim nFila       As Integer
    Dim Existe      As Long
    '-----------------------------------

    Set DocWord = CreateObject("word.application")
    
    With DocWord
        .Application.Visible = False
        .Documents.Open App.Path & "\" & sPlantilla
        
        For nX = 1 To nTotalVariables
            .ActiveDocument.Content.Find.Execute FindText:=oVariables(nX).sNombre, _
                                                replacewith:=oVariables(nX).sValor, _
                                                Replace:=2
        Next
        
        nFila = 1
        Set oTabla = DocWord.ActiveDocument.Tables(3)
        
        For nX = 1 To nTotalDetalles
            If nX <> 1 Then
                oTabla.Rows.Add
            End If
        
            nFila = oTabla.Rows.Count
            
            oTabla.Cell(nFila, 1).Range.Text = oDetalleFichas(nX).Nemotecnico
            oTabla.Cell(nFila, 2).Range.Text = oDetalleFichas(nX).Cantidad
            oTabla.Cell(nFila, 3).Range.Text = oDetalleFichas(nX).Precio
            oTabla.Cell(nFila, 4).Range.Text = oDetalleFichas(nX).Total
        Next
        
        .ActiveDocument.PrintOut (False)
        
        Do While .BackgroundPrintingStatus >= 1
        Loop
        
        '.ActiveDocument.Close wdDoNotSaveChanges
        '.ActiveDocument.Close 0
        
        'If oFile.FileExist(fNombreArchivo$) Then
        '    Kill fNombreArchivo$
        'End If
         
        '.ActiveDocument.SaveAs fNombreArchivo$
        .Quit wdDoNotSaveChanges
        
    End With
    
    Set DocWord = Nothing
    
End Function

Public Sub AddDetalle(Nemotecnico As String, Cantidad As String, Precio As String, Total As String)
    nTotalDetalles = nTotalDetalles + 1
       
    ReDim Preserve oDetalleFichas(nTotalDetalles)
    
    oDetalleFichas(nTotalDetalles).Nemotecnico = Nemotecnico
    oDetalleFichas(nTotalDetalles).Cantidad = Cantidad
    oDetalleFichas(nTotalDetalles).Precio = Precio
    oDetalleFichas(nTotalDetalles).Total = Total
    
End Sub

Public Sub AddVariable(Nombre As String, Valor As String)
    nTotalVariables = nTotalVariables + 1
       
    ReDim Preserve oVariables(nTotalVariables)
    
    oVariables(nTotalVariables).sNombre = Nombre
    oVariables(nTotalVariables).sValor = Valor
    
End Sub

Public Sub CleanDocumento()
    CleanVariables
    
    CleanDetalle
End Sub

Public Sub CleanDetalle()
    nTotalDetalles = 0
    ReDim oDetalleFichas(nTotalDetalles)
End Sub

Public Sub CleanVariables()
    nTotalVariables = 0
    ReDim oVariables(nTotalVariables)
End Sub
