VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_AdministracionCartera"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private Declare Function GetTempPath Lib "kernel32" _
   Alias "GetTempPathA" (ByVal nBufferLength As Long, _
   ByVal lpBuffer As String) As Long

Private Declare Function GetTempFileName Lib "kernel32" _
   Alias "GetTempFileNameA" (ByVal lpszPath As String, _
   ByVal lpPrefixString As String, ByVal wUnique As Long, _
   ByVal lpTempFileName As String) As Long

Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Administracion_Cartera"

Public IdCuenta   As String
Public IdCliente  As Integer
Public NombreCliente As String
Public RutCliente As String
Public Num_Cuenta As String
Public NombreCuenta   As String
Public IDEmpresa  As String
Public IdTipoCuenta As Integer
Public FechaInicio As Date
Public FechaTermino As Date
Public NumeroFolio As Integer

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 2
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function CreateTempFile(sPrefix As String) As String
   Dim sTmpPath As String * 512
   Dim sTmpName As String * 576
   Dim nRet As Long

   nRet = GetTempPath(512, sTmpPath)
   If (nRet > 0 And nRet < 512) Then
      nRet = GetTempFileName(sTmpPath, sPrefix, 0, sTmpName)
      If nRet <> 0 Then
         CreateTempFile = Left$(sTmpName, _
            InStr(sTmpName, vbNullChar) - 1)
      End If
   End If
End Function


Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_TipoCuenta"
    .AddCampo "Id_TipoCuenta", ePT_Numero, ePD_Entrada
    .AddCampo "CtaVigente", ePT_Numero, ePD_Entrada
    .AddCampo "Fecha_Inicio", ePT_Fecha, ePD_Entrada
    .AddCampo "Fecha_Termino", ePT_Fecha, ePD_Entrada
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "NumeroFolio", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function BuscarClientes() As Boolean
  BuscarClientes = fClass_Entidad.Buscar(cfPackage & "$BuscarClientes")
End Function

Public Function BuscarCuentas() As Boolean
  BuscarCuentas = fClass_Entidad.Buscar(cfPackage & "$BuscarCuentas")
End Function

Public Function BuscarMovimientosInstrumentos() As Boolean
  BuscarMovimientosInstrumentos = fClass_Entidad.Buscar(cfPackage & "$BuscarMovimientosInstrumentos")
End Function

Public Function Guardar() As Boolean
  Guardar = fClass_Entidad.Buscar(cfPackage & "$ActualizaFolio")
End Function

