VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Cartola_Campos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Type TCampos
    Nombre As String
    Titulo As String
    Largo As Integer
    Alineacion As TextAlignSettings
    TieneFormato As Boolean
    Formato As String
    Formato_Decimales As Integer
    Totaliza As Boolean
    Total As Double
End Type

Public CrearTabla As Boolean

Public Header_Font      As String
Public Header_FontSize  As Integer
Public Header_BackColor As Long
Public Header_ForeColor As Long

Private nTotalDetalles As Integer

Private oLineas() As TCampos

Public Property Get GetTotalLineas()
    GetTotalLineas = nTotalDetalles
End Property

Public Property Get GetLineas()
    GetLineas = oLineas
End Property

Sub AddColumna(ByVal NombreCampo As String, _
                        ByVal Titulo As String, _
                        ByVal Alineacion As TextAlignSettings, _
                        Optional TieneFormato As Boolean = False, _
                        Optional Formato As String = "", _
                        Optional Decimales As Integer = 2, _
                        Optional ByVal Largo As Integer = 0, _
                        Optional ByVal Totaliza As Boolean = False)

    nTotalDetalles = nTotalDetalles + 1

    ReDim Preserve oLineas(nTotalDetalles)

    oLineas(nTotalDetalles).Nombre = UCase(NombreCampo)
    oLineas(nTotalDetalles).Titulo = Titulo
    oLineas(nTotalDetalles).Largo = Largo
    oLineas(nTotalDetalles).Alineacion = Alineacion
    oLineas(nTotalDetalles).TieneFormato = TieneFormato
    oLineas(nTotalDetalles).Formato = Formato
    oLineas(nTotalDetalles).Formato_Decimales = Decimales
    
    oLineas(nTotalDetalles).Totaliza = Totaliza
    oLineas(nTotalDetalles).Total = 0
    
End Sub

Public Sub PintaEncabezado(ByRef oGrilla As Object)
    Dim nX As Integer
    Dim LInt_Col As Integer
    Dim iFila As Integer
    Dim iTotalColumnas As Integer
    
     
    iTotalColumnas = GetTotalLineas
    With oGrilla
        If CrearTabla Then
            .StartTable
        End If
        
        '  .TableBorder = tbBox
        .TableCell(tcCols) = iTotalColumnas
        
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        
        'If Header_Font <> "" Then
        '    .TableCell(tcFont, iFila, 1, iFila, iTotalColumnas) = Header_Font
        'End If
        
        If Header_FontSize <> 0 Then
            .TableCell(tcFontSize, iFila, 1, iFila, iTotalColumnas) = Header_FontSize
        End If
        
        If Header_BackColor <> 0 Then
            .TableCell(tcBackColor, iFila, 1, iFila, iTotalColumnas) = Header_BackColor
        End If
        
        If Header_ForeColor <> 0 Then
            .TableCell(tcForeColor, iFila, 1, iFila, iTotalColumnas) = Header_ForeColor
        End If
        

        
        For nX = 1 To iTotalColumnas
            LInt_Col = nX - 1
            .TableCell(tcText, iFila, nX) = oLineas(nX).Titulo
            .TableCell(tcAlign, iFila, nX) = taCenterMiddle ' oLineas(nX).Alineacion
            .TableCell(tcColBorderLeft, iFila, nX) = 1

            
            If oLineas(nX).Largo <> 0 Then .TableCell(tcColWidth, iFila, nX) = oLineas(nX).Largo & "mm"
            
        Next
        
    End With
    
End Sub

Public Sub PintaTotales(ByRef oGrilla As Object, oCol() As TCampos)
    Dim nX              As Integer
    Dim LInt_Col        As Integer
    Dim iFila           As Integer
    Dim iTotalColumnas  As Integer
     
    iTotalColumnas = GetTotalLineas
    
    With oGrilla
        iFila = .TableCell(tcRows)
        
        If Header_Font <> "" Then
            .TableCell(tcFont, iFila, 1, iFila, iTotalColumnas) = Header_Font
        End If
        
'        If Header_FontSize <> 0 Then
'            .TableCell(tcFontSize, iFila, 1, iFila, iTotalColumnas) = Header_FontSize
'        End If
        
        If Header_BackColor <> 0 Then
            .TableCell(tcBackColor, iFila, 1, iFila, iTotalColumnas) = Header_BackColor
        End If

        If Header_ForeColor <> 0 Then
            .TableCell(tcForeColor, iFila, 1, iFila, iTotalColumnas) = Header_ForeColor
        End If
        
        For nX = 1 To iTotalColumnas
            If oCol(nX).Totaliza Then
                .TableCell(tcText, iFila, nX) = FormatNumber(oCol(nX).Total, oCol(nX).Formato_Decimales)
                .TableCell(tcAlign, iFila, nX) = taRightMiddle ' oLineas(nX).Alineacion
            End If
        Next
        
    End With
    
End Sub

Function CalculaAnchoGrilla() As Double
    Dim nX              As Integer
    Dim iTotalColumnas  As Integer
    Dim iAncho As Double
     
    iTotalColumnas = GetTotalLineas
    
    For nX = 1 To iTotalColumnas
        iAncho = iAncho + oLineas(nX).Largo
    Next
    CalculaAnchoGrilla = iAncho
End Function


Public Sub CleanDetalle()
    nTotalDetalles = 0
    ReDim oLineas(nTotalDetalles)
End Sub

Private Sub Terminate()
    CleanDetalle
End Sub
