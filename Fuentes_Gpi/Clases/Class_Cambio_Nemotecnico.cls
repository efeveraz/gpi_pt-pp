VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Cambio_Nemotecnico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Const fc_Mercado = "N"

Public Function Fnt_Cambio_Nemotecnico(pFecha As Date, pGrilla As VSFlexGrid) As Boolean
Dim lCursor As hRecord
Dim lReg_Nem As hFields
'---------------------------------------
Dim lId_Nem_Antiguo As String
Dim lNem_Antiguo As String
Dim lNem_Nuevo As String
Dim lHay_Cambio As Boolean
'---------------------------------------
Dim lcNemotecnico As Class_Nemotecnicos

  Fnt_Cambio_Nemotecnico = True
  
  Call Fnt_Escribe_Grilla(pGrilla, "N", "Inicio Cambio Nemot�cnicos.")
  
  Rem busca todos los nemot�cnicos que sean Letras Hipotecarias
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    If .Buscar_Nemo_Subfamilia(pFecha) Then
      Set lCursor = .Cursor
    Else
      Fnt_Cambio_Nemotecnico = False
      Call Fnt_Escribe_Grilla(pGrilla, "E", "Cambio Nemot�cnico: Error en la b�squeda de nemot�cnicos." & vbCr & vbCr & .ErrMsg)
      GoTo ErrProcedure
    End If
  End With
  
  Rem Por cada nemotecnico LH encontrado se les cambia el nombre si corresponde
  If lCursor.Count > 0 Then
    For Each lReg_Nem In lCursor
      lHay_Cambio = False
      Rem Pregunta si al nemotecnico le corresponde cambio de nombre
      Rem PENDIENTE!!!!!!!
      
      If lHay_Cambio Then
        lId_Nem_Antiguo = lReg_Nem("id_nemotecnico").Value
        lNem_Antiguo = lReg_Nem("nemotecnico").Value
        lNem_Nuevo = Fnt_Cambia_Letra_Asterisco(lReg_Nem("nemotecnico").Value, pFecha)
       
        Call Fnt_Procesos_Cambio_Nemo(pGrilla, _
                                      lId_Nem_Antiguo, _
                                      lNem_Nuevo, _
                                      lNem_Antiguo, _
                                      lReg_Nem("Id_cuenta").Value, _
                                      pFecha, _
                                      lReg_Nem("cantidad").Value, _
                                      lReg_Nem("ID_MONEDA_NEMOTECNICO").Value, _
                                      lReg_Nem("num_cuenta").Value, _
                                      lReg_Nem("id_mov_activo").Value)
      End If
    Next
  Else
    Call Fnt_Escribe_Grilla(pGrilla, _
                            "A", _
                            "Cambio Nemot�cnicos: No hay nemot�cnicos en cartera LH para la fecha " & pFecha & ".")
  End If
    
ErrProcedure:
  Call Fnt_Escribe_Grilla(pGrilla, "N", "T�rmino Cambio Nemot�cnicos.")

End Function

Private Function Fnt_Procesos_Cambio_Nemo(ByVal pGrilla As VSFlexGrid, _
                                          ByVal pId_Nem_Antiguo, _
                                          ByVal pNem_Nuevo, _
                                          ByVal pNem_Antiguo, _
                                          ByVal pId_cuenta, _
                                          ByVal pFecha, _
                                          ByVal pCantidad, _
                                          ByVal pId_Moneda, _
                                          ByVal pNum_Cuenta, _
                                          ByVal pId_Mov_Activo) As Boolean
Dim lBonos As Class_Bonos
Dim lcNemotecnico As Class_Nemotecnicos
Dim lPrecio As Class_Publicadores_Precio
Dim lcMov_Activos As Class_Mov_Activos
'---------------------------------------
Dim lReg As hFields
'---------------------------------------
Dim lId_Nem_Nuevo As String
Dim lTasa As Double
Dim lMsg_Error As String
Dim lMonto_Valorizado As Double
Dim lRollback As Boolean
Dim lCantidad_Pendiente As Double
Dim lSaldo_Activo_Cantidad As Double
'---------------------------------------

  Fnt_Procesos_Cambio_Nemo = True

  gDB.IniciarTransaccion
  lRollback = False
  
  Set lBonos = New Class_Bonos
  Set lcNemotecnico = New Class_Nemotecnicos
  
  Rem Clona el nemotecnico antiguo en un nuevo nemotecnico, el cual es guardado en el sistema (si no existe)
  If lcNemotecnico.Fnt_Clonar_Nemotecnico(pId_Nem_Antiguo, _
                                          pNem_Nuevo, _
                                          lId_Nem_Nuevo) Then
    Call Fnt_Escribe_Grilla(pGrilla, "N", "Cambio Nemot�cnicos: Correcto cambio del Nemot�cnico ' " & pNem_Antiguo & "' por el Nemot�cnico '" & pNem_Nuevo & "'.")
  Else
    lRollback = True
    Call Fnt_Escribe_Grilla(pGrilla, "E", "Error Cambio Nemot�cnicos:  " & lcNemotecnico.ErrMsg)
    GoTo ErrProcedure
  End If
  
  Rem Busca la tasa correspondiente a la �ltima desde ingresada desde la pFecha
  Set lPrecio = New Class_Publicadores_Precio
  lPrecio.Campo("id_nemotecnico").Valor = CStr(pId_Nem_Antiguo)
  lPrecio.Campo("fecha").Valor = Fnt_FechaServidor
  If lPrecio.Buscar_Ultimo_Tasa_Cta_Nemo(CStr(pId_cuenta)) Then
    lTasa = lPrecio.Campo("tasa").Valor
  Else
    Call Fnt_Escribe_Grilla(pGrilla, "A", "Cambio Nemot�cnico: Error en la b�squeda de la �ltima tasa del nemot�cnico " & pNem_Antiguo & " para la cuenta " & pId_cuenta & "." & vbCr & "Se valoriza con tasa 0." & vbCr & vbCr & lPrecio.ErrMsg)
    lTasa = 0
  End If
  Set lPrecio = Nothing
  
  Rem A la cantidad que hay en la Saldos_Activos, se debe restar las ventas del d�a.
  lCantidad_Pendiente = 0
  Rem Solo se buscan los movimientos pendientes de Ventas
  Set lcMov_Activos = New Class_Mov_Activos
  If lcMov_Activos.Buscar_Pendientes_Cierre(pId_cuenta:=pId_cuenta _
                                          , pFecha:=Fnt_FechaServidor _
                                          , pId_Nemotecnico:=pId_Nem_Antiguo _
                                          , pTipo_Movimiento:=gcTipoOperacion_Egreso _
                                          , pId_Mov_Activo_Compra:=pId_Mov_Activo) Then
    For Each lReg In lcMov_Activos.Cursor
      lCantidad_Pendiente = lCantidad_Pendiente + lReg("cantidad").Value
    Next
  End If
  
  lSaldo_Activo_Cantidad = pCantidad - lCantidad_Pendiente
  
  Rem Valoriza el Nemotecnico antiguo
  lMonto_Valorizado = lBonos.ValorizaPapel(pId_Nem_Antiguo, _
                                           pFecha, _
                                           lTasa, _
                                           lSaldo_Activo_Cantidad, _
                                           False, _
                                           lMsg_Error)
  If Not lMsg_Error = "" Then
    Call Fnt_Escribe_Grilla(pGrilla, "E", "Cambio Nemot�cnico: " & lMsg_Error)
  End If
  
  Rem Realiza la operaci�n de Egreso del nemotecnico antiguo
  If Fnt_Realiza_Operacion(pId_Nem_Antiguo, _
                           pId_cuenta, _
                           lSaldo_Activo_Cantidad, _
                           lTasa, _
                           pId_Moneda, _
                           lMonto_Valorizado, _
                           pFecha, _
                           0, _
                           pNem_Antiguo, _
                           gcTipoOperacion_Egreso, _
                           pGrilla) Then
    Call Fnt_Escribe_Grilla(pGrilla, "N", "Cambio Nemot�cnico: Cuenta " & pNum_Cuenta & ": Se ha efectuado la Operaci�n de Egreso del Nemot�cnico '" & pNem_Antiguo & "'.")
  Else
    lRollback = True
    GoTo ErrProcedure
  End If
  
  Rem Valoriza el Nemotecnico nuevo
  lMonto_Valorizado = lBonos.ValorizaPapel(lId_Nem_Nuevo, _
                                           pFecha, _
                                           lTasa, _
                                           lSaldo_Activo_Cantidad, _
                                           False, _
                                           lMsg_Error)
  If Not lMsg_Error = "" Then
    Call Fnt_Escribe_Grilla(pGrilla, "E", "Cambio Nemot�cnico: " & lMsg_Error)
  End If
  
  Rem Realiza la operaci�n de Ingreso del nemotecnico nuevo
  If Fnt_Realiza_Operacion(lId_Nem_Nuevo, _
                           pId_cuenta, _
                           lSaldo_Activo_Cantidad, _
                           lTasa, _
                           pId_Moneda, _
                           lMonto_Valorizado, _
                           pFecha, _
                           0, _
                           pNem_Nuevo, _
                           gcTipoOperacion_Egreso, _
                           pGrilla) Then
    Call Fnt_Escribe_Grilla(pGrilla, "N", "Cambio Nemot�cnico: Cuenta " & pNum_Cuenta & ": Se ha efectuado la Operaci�n de Ingreso del Nemot�cnico '" & pNem_Nuevo & "'.")
  Else
    lRollback = True
    GoTo ErrProcedure
  End If
  
  Rem Inserta un nuevo registro en la tabla Rel_Operaciones_Opercaciones que relaciona los nemotecnicos antiguo y nuevo
  If Not Fnt_Rel_Operaciones_Operaciones(CStr(pId_Nem_Antiguo), CStr(lId_Nem_Nuevo), pGrilla) Then
    lRollback = True
    GoTo ErrProcedure
  End If

ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    Fnt_Procesos_Cambio_Nemo = False
  Else
    gDB.CommitTransaccion
  End If
  
End Function

Private Function Fnt_Realiza_Operacion(ByVal pId_Nemotecnico, _
                                  ByVal pId_cuenta, _
                                  ByVal pCantidad, _
                                  ByVal pTasa, _
                                  ByVal pId_Moneda, _
                                  ByVal pMonto, _
                                  ByVal pFecha, _
                                  ByVal pPorc_Comision, _
                                  ByVal pNemotecnico, _
                                  ByVal pTipo_Operacion, _
                                  pGrilla As VSFlexGrid) As Boolean
Dim lOperacion As Class_Operaciones
Dim lOperacion_Detalle As Class_Operaciones_Detalle
Dim lId_Caja_Cuenta As Double
Dim lMercado As String

  Fnt_Realiza_Operacion = True

  If pTipo_Operacion = gcTipoOperacion_Ingreso Then
    Rem Si es una compra
    lId_Caja_Cuenta = Fnt_ValirdarFinanciamiento(pId_cuenta, fc_Mercado, pMonto, pId_Moneda, Fnt_FechaServidor)
    If lId_Caja_Cuenta = -1 Then
      Rem Si el financiamiento tuvo problemas
      GoTo ErrProcedure
    End If
  Else
    Rem Si es una venta
    lId_Caja_Cuenta = Fnt_ValidarCaja(pId_cuenta, fc_Mercado, pId_Moneda)
    If lId_Caja_Cuenta < 0 Then
      Rem Significa que hubo problema con la busqueda de la caja
      GoTo ErrProcedure
    End If
  End If
  
  Set lOperacion_Detalle = New Class_Operaciones_Detalle
  With lOperacion_Detalle
    .Campo("Id_Nemotecnico").Valor = pId_Nemotecnico
    .Campo("Cantidad").Valor = pCantidad
    .Campo("Tasa").Valor = pTasa
    .Campo("Id_Moneda_Pago").Valor = pId_Moneda
    .Campo("Monto_Pago").Valor = pMonto
  End With

  Set lOperacion = New Class_Operaciones
  lOperacion.LimpiaParam
  Call lOperacion.Agregar_Operaciones_Detalle(lOperacion_Detalle)

  With lOperacion
    .Campo("Id_Operacion").Valor = cNewEntidad
    .Campo("Id_Cuenta").Valor = pId_cuenta
    .Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Cambio_Nem
    .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente
    .Campo("Id_Moneda_Operacion").Valor = pId_Moneda
    .Campo("Cod_Producto").Valor = gcPROD_RF_NAC
    .Campo("Cod_Instrumento").Valor = gcINST_BONOS_NAC
    .Campo("Flg_Tipo_Movimiento").Valor = pTipo_Operacion
    .Campo("Fecha_Operacion").Valor = pFecha
    .Campo("Fecha_Vigencia").Valor = pFecha
    .Campo("Fecha_Liquidacion").Valor = pFecha
    .Campo("Dsc_Operacion").Valor = ""
    .Campo("Porc_Comision").Valor = pPorc_Comision
    .Campo("Comision").Valor = 0
    .Campo("Derechos").Valor = 0
    .Campo("Gastos").Valor = 0
    .Campo("Iva").Valor = 0
    .Campo("Monto_Operacion").Valor = pMonto

    If Not .Guardar(lId_Caja_Cuenta) Then
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "Error Cambio Nemot�cnico: Nemot�cnico '" & pNemotecnico & "': " & lOperacion.ErrMsg)
      GoTo ErrProcedure
    End If
  End With
  
  Exit Function
  
ErrProcedure:
  Fnt_Realiza_Operacion = False

End Function

Private Function Fnt_Rel_Operaciones_Operaciones(pId_Nem_Egreso As String, _
                                                 pId_Nem_Ingreso As String, _
                                                 pGrilla As VSFlexGrid) As Boolean
  
  Fnt_Rel_Operaciones_Operaciones = True
  With gDB
    .Parametros.Clear
    .Parametros.Add "PId_Nem_Egreso", ePT_Numero, pId_Nem_Egreso, ePD_Entrada
    .Parametros.Add "PId_Nem_Ingreso", ePT_Numero, pId_Nem_Ingreso, ePD_Entrada
    .Procedimiento = "PKG_REL_OPER_OPER.Guardar"
    If Not .EjecutaSP Then
      Call Fnt_Escribe_Grilla(pGrilla, "E", "Error Cambio Nemot�cnico: Problemas en grabar." & vbCr & vbCr & .ErrMsg)
      Fnt_Rel_Operaciones_Operaciones = False
    End If
    .Parametros.Clear
  End With
  
End Function

Private Function Fnt_Cambia_Letra_Asterisco(pNemotecnico, pFecha) As String
Dim lMes_Actual As Integer
Dim lMes_Nemo As Integer
Dim lAno_Nemo As Integer
Dim lNemo_Nuevo As String
   
  lNemo_Nuevo = pNemotecnico

  If InStr(pNemotecnico, "*") > 0 Then
    If Mid(pNemotecnico, 7, 2) = "**" Then
      lNemo_Nuevo = Mid(pNemotecnico, 1, 6) & " *" & Mid(pNemotecnico, 9, 2)
    Else
      lNemo_Nuevo = Mid(pNemotecnico, 1, 6) & "01" & Mid(pNemotecnico, 9, 2)
    End If
  End If

  If InStr(pNemotecnico, "&") > 0 Then
    If Mid(pNemotecnico, 7, 2) = "&" Then
      lNemo_Nuevo = Mid(pNemotecnico, 1, 6) & " &" & Mid(pNemotecnico, 9, 2)
    Else
      lMes_Nemo = Mid(pNemotecnico, 9, 2)
      'lMes_Actual = TO_CHAR(TO_DATE(pfecha, "dd/mm/yyyy"), "mm")
      lMes_Actual = Format(pFecha, "mm")

      If lMes_Nemo > lMes_Actual Then
        'lAno_Nemo = (TO_CHAR(TO_DATE(pfecha, "dd/mm/yyyy"), "yyyy") - 1)
        lAno_Nemo = Format(pFecha, "yyyy") - 1
      Else
        lAno_Nemo = Format(pFecha, "yyyy")
      End If

      lNemo_Nuevo = Mid(pNemotecnico, 1, 6) & Mid(pNemotecnico, 9, 2) & Mid(lAno_Nemo, 3, 2)
    End If
  End If

  Fnt_Cambia_Letra_Asterisco = lNemo_Nuevo

End Function
