VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Cortes_Cupon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "PKG_CORTES_CUPON"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 70
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_CORTE_CUPON"
    .AddCampo "ID_CORTE_CUPON", ePT_Numero, ePD_Ambos
    .AddCampo "ID_NEMOTECNICO", ePT_Numero, ePD_Entrada
    .AddCampo "NRO_CUPON", ePT_Numero, ePD_Entrada
    .AddCampo "FCH_CORTE_CUPON", ePT_Fecha, ePD_Entrada
    .AddCampo "FLUJO", ePT_Numero, ePD_Entrada
    .AddCampo "INTERES", ePT_Numero, ePD_Entrada
    .AddCampo "AMORTIZACION", ePT_Numero, ePD_Entrada
    .AddCampo "FCH_PROX_CORTE_CUPON", ePT_Fecha, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Guardar()
Dim lcCorte_Cupon As Class_Cortes_Cupon

  'Como no se pueden  repetir los nemotecnicos y los cortes de cupon,
  'primero busco por la combinacion de id_nemotecnico y corte cupon para
  'ver si existe el corte. Solo si el id del corte de cupon es null
  If IsNull(Me.Campo("ID_CORTE_CUPON").Valor) Then
    Set lcCorte_Cupon = New Class_Cortes_Cupon
    With lcCorte_Cupon
      .Campo("ID_NEMOTECNICO").Valor = Me.Campo("ID_NEMOTECNICO").Valor
      .Campo("NRO_CUPON").Valor = Me.Campo("NRO_CUPON").Valor
      If Not .Buscar Then
        fClass_Entidad.ErrMsg = .ErrMsg
        fClass_Entidad.Errnum = .Errnum
        GoTo ExitProcedure
      End If
      
      Me.Campo("ID_CORTE_CUPON").Valor = .Campo("ID_CORTE_CUPON").Valor
    End With
  End If

  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
  
ExitProcedure:
  Set lcCorte_Cupon = Nothing
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function CaptionCargar() As String
  CaptionCargar = "Cortes de Cupon"
End Function

Public Function CargarAutomatica(pFecha) As Boolean
Dim lcProceso_Componente  As Class_Proceso_Componente
Dim lcAutoCargador        As Object
Dim lReg                  As hFields
Dim lcCorte_Cupon         As Class_Cortes_Cupon
Dim lcSaldo_Activo        As Class_Saldo_Activos
'---------------------------------------------
Dim lMensaje              As String

Const cCodigoDLL = "CORTECUPON_CARGAAUTO"

On Error GoTo ErrProcedure

  CargarAutomatica = False

  'Busca el componente que corresponde al conciliador
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = cCodigoDLL
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set lcAutoCargador = .IniciaClass(lMensaje)
    
    If lcAutoCargador Is Nothing Then
      fClass_Entidad.ErrMsg = lMensaje & vbCrLf & .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  End With
  Set lcProceso_Componente = Nothing

  With lcAutoCargador
    Set .gDB = gDB
  
    Set lcSaldo_Activo = New Class_Saldo_Activos
    If Not lcSaldo_Activo.Buscar_Nemotecnicos_SAOP(pFecha_Cierre:=pFecha _
                                                 , pId_Empresa:=Null _
                                                 , pCod_Instrumento:=gcINST_BONOS_NAC) Then
      fClass_Entidad.ErrMsg = lcSaldo_Activo.ErrMsg
      fClass_Entidad.Errnum = lcSaldo_Activo.Errnum
      GoTo ExitProcedure
    End If
                                                
    For Each lReg In lcSaldo_Activo.Cursor
      Call .AgregarNemo(lReg("nemotecnico").Value, lReg("id_nemotecnico").Value)
    Next
    
    If Not .Cargar(pFecha) Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
    
    For Each lReg In .Cursor
      Set lcCorte_Cupon = New Class_Cortes_Cupon
      If Not IsMissing(lReg("AMORTIZACION").Value) Then
        lcCorte_Cupon.Campo("ID_NEMOTECNICO").Valor = lReg("ID_NEMOTECNICO").Value
        lcCorte_Cupon.Campo("NRO_CUPON").Valor = lReg("NRO_CUPON").Value
        lcCorte_Cupon.Campo("FCH_CORTE_CUPON").Valor = lReg("Fecha_Cupon").Value
        lcCorte_Cupon.Campo("INTERES").Valor = lReg("INTERES").Value
        lcCorte_Cupon.Campo("AMORTIZACION").Valor = lReg("AMORTIZACION").Value
        lcCorte_Cupon.Campo("FLUJO").Valor = lReg("AMORTIZACION").Value + lReg("INTERES").Value
        lcCorte_Cupon.Campo("FCH_PROX_CORTE_CUPON").Valor = lReg("Fch_Proximo_CorteCupon").Value
        If Not lcCorte_Cupon.Guardar Then
          fClass_Entidad.ErrMsg = lcCorte_Cupon.ErrMsg
          fClass_Entidad.Errnum = lcCorte_Cupon.Errnum
          GoTo ExitProcedure
        End If
      End If
      Set lcCorte_Cupon = Nothing
    Next
  End With
  
  CargarAutomatica = False

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la carga del conciliador (" & cCodigoDLL & ")." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Set lcProceso_Componente = Nothing
  Set lcAutoCargador = Nothing
  Set lcCorte_Cupon = Nothing
  Set lcSaldo_Activo = Nothing
End Function

