VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Saldos_Caja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Saldos_Caja"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 69
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Saldo_Caja"
    .AddCampo "Id_Saldo_Caja", ePT_Numero, ePD_Ambos
    .AddCampo "Id_Caja_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "Fecha_Cierre", ePT_Fecha, ePD_Entrada
    .AddCampo "Id_Moneda_Caja", ePT_Numero, ePD_Entrada
    .AddCampo "Monto_Mon_Caja", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Moneda_Cta", ePT_Numero, ePD_Entrada
    .AddCampo "Monto_Mon_Cta", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Cierre", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_X_COBRAR_MON_CTA", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_X_PAGAR_MON_CTA", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_X_COBRAR_MON_CAJA", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_X_PAGAR_MON_CAJA", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar(Optional pId_Cuenta = Null) As Boolean

  If Not IsNull(pId_Cuenta) Then
    'Se engrega el campo id_cuenta a la clase
    fClass_Entidad.AddCampo "id_cuenta", ePT_Numero, ePD_Entrada
    fClass_Entidad.Campo("id_cuenta").Valor = pId_Cuenta
  End If
 
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Buscar_Saldos_Cuenta(ByRef pSaldo As Double, _
                                     pId_Cuenta As String) As Boolean
Buscar_Saldos_Cuenta = True
  
  With gDB
    .Parametros.Clear
    .Procedimiento = cfPackage & ".Buscar_Saldos_Cuenta"
    .Parametros.Add "PSaldo", ePT_Numero, 0, ePD_Salida
    .Parametros.Add "pid_cuenta", ePT_Numero, pId_Cuenta, DLL_COMUN.ePD_Entrada
    .Parametros.Add "p" & fClass_Entidad.Campo("id_caja_cuenta").Nombre, ePT_Numero, fClass_Entidad.Campo("id_caja_cuenta").Valor, DLL_COMUN.ePD_Entrada
    .Parametros.Add "p" & fClass_Entidad.Campo("fecha_cierre").Nombre, ePT_Fecha, fClass_Entidad.Campo("fecha_cierre").Valor, DLL_COMUN.ePD_Entrada
    If .EjecutaSP Then
      pSaldo = NVL(.Parametros("PSaldo").Valor, 0)
    Else
      Buscar_Saldos_Cuenta = False
      fClass_Entidad.ErrMsg = gDB.ErrMsg
    End If
    .Parametros.Clear
  End With
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Deshacer_Enlaze_Cierre(ByVal pId_Cuenta As Double, ByVal pFecha_Cierre As Date)
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada
    
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("fecha_cierre").Valor = pFecha_Cierre
    
    Deshacer_Enlaze_Cierre = .Borrar(cfPackage & ".DESHACER_ENLAZE_CIERRE")
    fClass_Entidad.Errnum = lClass_Entidad.Errnum
    fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
  End With
  
  Set lClass_Entidad = Nothing
End Function

Public Function Guardar_Paridades()
  Guardar_Paridades = fClass_Entidad.Borrar(cfPackage & ".Guardar_Paridades")
End Function

Public Function Buscar_Saldo_Informe(pId_Cuenta As String, _
                                    ByVal pFecha_Cierre As Date) As Boolean
                                    
    Dim lClass_Entidad As Class_Entidad

    Set lClass_Entidad = New Class_Entidad

    With lClass_Entidad
        .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada     ', pId_Cuenta
        .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada   ', Me.Campo("fecha_cierre").Valor

        .Campo("id_cuenta").Valor = pId_Cuenta
        .Campo("fecha_cierre").Valor = pFecha_Cierre

        Buscar_Saldo_Informe = .Buscar(cfPackage & ".BUSCAR_SALDO_INFORME")

        fClass_Entidad.Errnum = lClass_Entidad.Errnum
        fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
        Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
    End With

    Set lClass_Entidad = Nothing
    
'    Buscar_Saldo_Informe = True
'
'    With gDB
'        .Parametros.Clear
'        .Procedimiento = cfPackage & ".BUSCAR_SALDO_INFORME"
'        .Parametros.Add "pid_cuenta", ePT_Numero, pId_Cuenta, DLL_COMUN.ePD_Entrada
'        .Parametros.Add "pfecha_cierre", ePT_Fecha, pFecha_Cierre, DLL_COMUN.ePD_Entrada
'        .Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
'        If .EjecutaSP Then
'            Buscar_Saldo_Informe = True
'            Set fClass_Entidad.Cursor = gDB.Parametros("pCursor").Valor
'        Else
'            Buscar_Saldo_Informe = False
'            fClass_Entidad.ErrMsg = gDB.ErrMsg
'        End If
'        .Parametros.Clear
'    End With
    
End Function


