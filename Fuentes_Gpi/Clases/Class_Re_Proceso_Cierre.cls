VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Re_Proceso_Cierre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Const cfPackage = "Pkg_Re_Proceso_Cierres"

Public Function Fnt_Busca_Cuentas_Activos(ByVal pFecha, _
                                          ByVal pCod_Producto, _
                                          ByVal pCod_Instrumento, _
                                          ByVal pId_Emisor_Especifico, _
                                          ByVal pId_Nemotecnico, _
                                          ByVal pCod_Mercado, _
                                          ByRef pCursor As hCollection.hRecord) As Boolean
  
  Fnt_Busca_Cuentas_Activos = True
  With gDB
    .Parametros.Clear
    .Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    .Parametros.Add "Pfecha_cierre", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "Pcod_Producto", ePT_Caracter, pCod_Producto, ePD_Entrada
    .Parametros.Add "Pcod_Instrumento", ePT_Caracter, pCod_Instrumento, ePD_Entrada
    .Parametros.Add "Pid_Emisor_Especifico", ePT_Numero, pId_Emisor_Especifico, ePD_Entrada
    If pId_Nemotecnico = Null Then
      .Parametros.Add "Pid_Nemotecnico", ePT_Numero, pId_Nemotecnico, ePD_Entrada
    End If
    .Parametros.Add "Pcod_mercado", ePT_Caracter, pCod_Mercado, ePD_Entrada
    .Procedimiento = cfPackage & ".Busca_Cuentas_Activos"
    If .EjecutaSP Then
      Set pCursor = .Parametros("Pcursor").Valor
    Else
      Fnt_Busca_Cuentas_Activos = False
      MsgBox .ErrMsg, vbCritical, Frm_Re_Proceso_Cierre.Caption
    End If
    .Parametros.Clear
  End With

End Function

Public Function Fnt_Busca_Cuentas_Propiedades(ByVal pId_Asesor, _
                                              ByVal pId_Cliente, _
                                              ByVal pId_Grupos_Cuentas, _
                                              ByRef pCursor As hCollection.hRecord) As Boolean
  
  Fnt_Busca_Cuentas_Propiedades = True
  With gDB
    .Parametros.Clear
    .Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    .Parametros.Add "Pid_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    .Parametros.Add "Pid_Asesor", ePT_Numero, pId_Asesor, ePD_Entrada
    .Parametros.Add "Pid_Cliente", ePT_Numero, pId_Cliente, ePD_Entrada
    .Parametros.Add "Pid_Grupos_Cuentas", ePT_Numero, pId_Grupos_Cuentas, ePD_Entrada
    .Procedimiento = cfPackage & ".Busca_Cuentas_Propiedades"
    If .EjecutaSP Then
      Set pCursor = .Parametros("Pcursor").Valor
    Else
      Fnt_Busca_Cuentas_Propiedades = False
      MsgBox .ErrMsg, vbCritical, Frm_Re_Proceso_Cierre.Caption
    End If
    .Parametros.Clear
  End With

End Function

Public Function Fnt_Validaciones_ReProceso(ByVal pFecha, _
                                    pGrilla As VSFlexGrid, _
                                    ByRef pId_Cierre As String) As Boolean
Dim lMensaje As String
Dim lCierre As Class_Verificaciones_Cierre

  Fnt_Validaciones_ReProceso = False

  Call Fnt_Escribe_Grilla(pGrilla, "N", "Comienzo de Validaciones para el Re-Proceso de Cierre.")
  
  Rem Verifica que el proceso de cierre del dia indicado exista
  Call Fnt_Busca_Cierre(pFecha, pGrilla, pId_Cierre)
  
  Set lCierre = New Class_Verificaciones_Cierre
  Rem Verifica si se quiere realizar un re-proceso de una fecha anterior al ultimo cierre
  If pFecha < lCierre.Busca_Ultima_FechaCierre Then
    Call Fnt_Escribe_Grilla(pGrilla, "A", _
       "Sr Usuario. Se quiere reprocesar una fecha que no es el �ltimo cierre para la empresa." & vbCr & _
       "El sistema para continuar con la integridad dejar� inoperativas las fechas siguientes" & vbCr & _
       "a la que se quiere reprocesar, con lo que, deber� reprocesar desde la fecha seleccionada a" & vbCr & _
       "la fecha actual.")
  Else
    Call Fnt_Escribe_Grilla(pGrilla, "N", "Fecha de Cierre: La Fecha de Cierre corresponde a la fecha del �ltimo cierre de la empresa.")
  End If
  
  Rem Verifica que esten las paridades y los precios de nemotecnicos para el dia indicado a la clase "cierre"
  Call lCierre.Fnt_Verifica_Paridad(pFecha, pGrilla)
  Call lCierre.Fnt_Verifica_Precio_Nemotecnico(pFecha, pGrilla)
  Call lCierre.Fnt_Verifica_Nemotecnicos_SIN_Arbol(pFecha, pGrilla)
  Call lCierre.Fnt_Verifica_Nemotecnicos_Con_Fecha_Vcto_Incorrectas(pFecha, pGrilla)
  
  Rem Verifica que existan datos en la tabla Bloqueo si la empresa lo permite
  Call lCierre.Fnt_Verifica_Bloqueo(pGrilla)
  
  Call Fnt_Escribe_Grilla(pGrilla, "N", "Fin de Validaciones para el Re-Proceso de Cierre.")
  
  If Not Fnt_Verificacion_Error(pGrilla) Then
    If Fnt_Verificacion_Advertencia(pGrilla) Then
      lMensaje = "Las Verificaciones al ReProceso de Cierre tienen Advertencias." & vbCr & _
                 "Puede continuar con el ReProceso de Cierre, pero puede generar errores de corcondancia de datos." & vbCr & vbCr & _
                 "En el Visor de Sucesos se muestran las advertencias por validaciones." & vbCr & _
                 "Por Favor, lea todas las advertencias antes de realizar el ReProceso de Cierre."
      MsgBox lMensaje, vbExclamation, Frm_Re_Proceso_Cierre.Caption
    End If
    Fnt_Validaciones_ReProceso = True
  Else
    lMensaje = "Las Verificaciones al ReProceso de Cierre tienen Errores, no se puede continuar el ReProceso de cierre." & vbCr & vbCr & _
               "En el Visor de Sucesos se muestran los errores en el ReProceso de Cierre."
    MsgBox lMensaje, vbCritical, Frm_Re_Proceso_Cierre.Caption
  End If
  
End Function

Private Function Fnt_Busca_Cierre(ByVal pFecha, _
                                 pGrilla As VSFlexGrid, _
                                 ByRef pId_Cierre) As Boolean

  Fnt_Busca_Cierre = True
  
  Rem Procedimiento que Busca el id_cierre
  With gDB
    .Parametros.Clear
    .Parametros.Add "Pid_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    .Parametros.Add "Pfecha", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "Pid_Cierre", ePT_Numero, "", ePD_Salida
    .Parametros.Add "PTipo_Error", ePT_Caracter, "", ePD_Salida
    .Parametros.Add "Pmensaje", ePT_Caracter, "", ePD_Salida
    .Procedimiento = cfPackage & ".Busca_Cierre"
    If .EjecutaSP Then
      Rem Si el tipo de error es "E" significa que el cierre No esta realizado para la fecha indicada
      If .Parametros("PTipo_Error").Valor = "E" Then
        Fnt_Busca_Cierre = False
      End If
      Call Fnt_Escribe_Grilla(pGrilla, _
                              .Parametros("PTipo_Error").Valor, _
                              "B�squeda de Cierre: " & .Parametros("Pmensaje").Valor)
      pId_Cierre = .Parametros("Pid_Cierre").Valor
    Else
      Fnt_Busca_Cierre = False
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "Error en B�squeda de Cierre: " & .ErrMsg)
    End If
    .Parametros.Clear
  End With
  
End Function

Private Function Fnt_Verificacion_Error(pGrilla As VSFlexGrid) As Boolean
Dim lTipo_Cierre As String
Dim lFila As Long

  Fnt_Verificacion_Error = False
  With pGrilla
    For lFila = 1 To .Rows - 1
      lTipo_Cierre = GetCell(pGrilla, lFila, "colum_pk")
      If lTipo_Cierre = "E" Then
        Fnt_Verificacion_Error = True
        Exit For
      End If
    Next lFila
  End With
End Function

Private Function Fnt_Verificacion_Advertencia(pGrilla As VSFlexGrid) As Boolean
Dim lTipo_Cierre As String
Dim lFila As Long

  Fnt_Verificacion_Advertencia = False
  With pGrilla
    For lFila = 1 To .Rows - 1
      lTipo_Cierre = GetCell(pGrilla, lFila, "colum_pk")
      If lTipo_Cierre = "A" Then
        Fnt_Verificacion_Advertencia = True
        Exit For
      End If
    Next lFila
  End With
End Function
