VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Restricciones_Rel_Porc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function fnt_GlosaArbol(ByVal pId_Arbol) As String
    Dim lcClass_Arbol_Clase_Inst As Class_Arbol_Clase_Instrumento
    
    fnt_GlosaArbol = ""
  
    Set lcClass_Arbol_Clase_Inst = New Class_Arbol_Clase_Instrumento
    
    With lcClass_Arbol_Clase_Inst
        .Campo("ID_ARBOL_CLASE_INST").Valor = pId_Arbol
        If .Buscar Then
            If .Cursor.Count > 0 Then
                fnt_GlosaArbol = .Cursor(1)("DSC_ARBOL_CLASE_INST").Value
            Else
                fnt_GlosaArbol = ""
            End If
        Else
            MsgBox .ErrMsg & " - " & .Errnum, vbCritical
            Exit Function
        End If
    End With
    
    Set lcClass_Arbol_Clase_Inst = Nothing
                                         
End Function

Public Function Fnt_Restriccion_Rel_Porc(ByVal pId_Cuenta, ByVal pId_Nemotecnico, ByVal pMonto, ByVal pId_Moneda, ByVal pFecha_Operacion) As Boolean
    Dim lFecha_Proceso As Date
    '--------------------------------------
    Dim lId_Perfil_Riesgo As String
    Dim lId_Moneda_Cuenta As String
    Dim lDsc_Perfil_Riesgo As String
    Dim lDsc_Moneda As String
    '--------------------------------------
    Dim lCod_Producto As String
    Dim lCod_Instrumento As String
    Dim lDsc_Producto As String
    Dim lDsc_Instrumento As String
    '--------------------------------------
    Dim lPatrimonio_Cta As Double
    '--------------------------------------
    'Dim lcTipo_Cambio As New Class_Tipo_Cambios
    Dim lcTipo_Cambio As Object
    Dim lMonto_Pariado As Double
    Dim lTipo_Cambio As Double
    Dim lOperacion As String
    '--------------------------------------
    Dim lId_Arbol_Clase_Inst As Long
    Dim lId_Arbol_Clase_Prod As Long
    Dim lPorcentaje     As String
    Dim lMonto_Actual   As Double
    Dim lMonto_Sumando
  
    Fnt_Restriccion_Rel_Porc = True
    
    lFecha_Proceso = Fnt_FechaServidor
    
    Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
    
    Rem Busca el Id_Perfil_Riesgo e Id_Moneda de la Cuenta
    If Not Busca_ID_PerfilRiesgo_ID_Moneda(pId_Cuenta, lId_Perfil_Riesgo, lId_Moneda_Cuenta, lDsc_Perfil_Riesgo, lDsc_Moneda) Then
        GoTo ErrProcedure
    End If
    
    Rem Busca el Cod_Producto y Cod_Instrumento del Nemotecnico
    If Not Buscar_Cod_Prod_Instr(pId_Nemotecnico, lCod_Producto, lCod_Instrumento, lDsc_Producto, lDsc_Instrumento) Then
        GoTo ErrProcedure
    End If
    
    Rem Busca el Patrimonio de la Cuenta
    If Not Buscar_Patrimonio_Cuenta(pId_Cuenta, lPatrimonio_Cta) Then
        GoTo ErrProcedure
    End If
    
    With lcTipo_Cambio
        Rem Parea el Monto ingresado a la moneda de la cuenta
        lMonto_Pariado = .Cambio_Paridad(pId_Cuenta, pMonto, pId_Moneda, lId_Moneda_Cuenta, lFecha_Proceso)
        
        Rem Busca el Tipo de Cambio usado en los calculos
        If Not .Busca_Precio_Paridad(pId_Cuenta, pId_Moneda, lId_Moneda_Cuenta, lFecha_Proceso, lTipo_Cambio, lOperacion) Then
            MsgBox .ErrMsg, vbCritical
            GoTo ErrProcedure
        End If
    End With
    
    Rem Busca primero el Porcentaje de Restricci�n seg�n Perfil de Riesgo e Instrumento
    'If Not Buscar_Porcentaje_Instrumento(lCod_Instrumento, lId_Perfil_Riesgo, lPorcentaje) Then
    If Not Buscar_Porcentaje_Instrumento(pId_Cuenta, pId_Nemotecnico, lId_Arbol_Clase_Inst, lPorcentaje) Then
        GoTo ErrProcedure
    End If
    
    Rem Monto actual que tiene la cuenta en cartera por Instrumentos
    lMonto_Actual = Busca_Monto_Actual(pId_Cuenta, lCod_Instrumento, True)
    
    'Movimientos realizados desde el ultimo cierre de la cuenta por el instrumento.
    If Not Fnt_Sumar_Operaciones_Desde(pId_Cuenta:=pId_Cuenta, pFecha_Desde:=pFecha_Operacion, pCod_Instrumento:=lCod_Instrumento, pMonto_Sumado:=lMonto_Sumando) Then
        GoTo ErrProcedure
    End If
    
    lDsc_Instrumento = IIf(lId_Arbol_Clase_Inst = -1, lDsc_Instrumento, fnt_GlosaArbol(lId_Arbol_Clase_Inst))
    
    Rem Valida la Restriccion para el Instrumento
    If Not Verifica_Patrimonio(pId_Cuenta:=pId_Cuenta, _
                               pMonto_Pariado:=lMonto_Pariado, _
                               pMonto_Actual:=lMonto_Actual + lMonto_Sumando, _
                               pPorcentaje_Decimal:=lPorcentaje, _
                               pPatrimonio:=lPatrimonio_Cta, _
                               pNombre:="Instrumento", _
                               pValor_Nombre:=lDsc_Instrumento, _
                               pDsc_Perfil_Riesgo:=lDsc_Perfil_Riesgo, _
                               pId_Moneda_Cuenta:=lId_Moneda_Cuenta, _
                               pDsc_Moneda:=lDsc_Moneda, _
                               pTipo_Cambio:=lTipo_Cambio) Then
        GoTo ErrProcedure
    End If
    
    Rem Luego busca Porcentaje por Producto
    
    'If Not Buscar_Porcentaje_Producto(lCod_Producto, lId_Perfil_Riesgo, lPorcentaje) Then
    If Not Buscar_Porcentaje_Producto(pId_Cuenta, pId_Nemotecnico, lId_Arbol_Clase_Prod, lPorcentaje) Then
        GoTo ErrProcedure
    End If
      
    Rem Monto actual que tiene la cuenta en cartera por Instrumentos
    lMonto_Actual = Busca_Monto_Actual(pId_Cuenta, lCod_Producto, False)
      
    'Movimientos realizados desde el ultimo cierre de la cuenta por el instrumento.
    If Not Fnt_Sumar_Operaciones_Desde(pId_Cuenta:=pId_Cuenta, _
                                        pFecha_Desde:=pFecha_Operacion, _
                                        pCod_Instrumento:=lCod_Instrumento, _
                                        pMonto_Sumado:=lMonto_Sumando) Then
        GoTo ErrProcedure
    End If
    
    lMonto_Sumando = NVL(lMonto_Sumando, 0)
      
    lDsc_Producto = IIf(lId_Arbol_Clase_Prod = -1, lDsc_Producto, fnt_GlosaArbol(lId_Arbol_Clase_Prod))
    Rem Valida la Restriccion para el Producto
    If Not Verifica_Patrimonio(pId_Cuenta, _
                               lMonto_Pariado, _
                               lMonto_Actual + lMonto_Sumando, _
                               lPorcentaje, _
                               lPatrimonio_Cta, _
                               "Producto", _
                               lDsc_Producto, _
                               lDsc_Perfil_Riesgo, _
                               lId_Moneda_Cuenta, _
                               lDsc_Moneda, _
                               lTipo_Cambio) Then
        GoTo ErrProcedure
    End If
      
    Exit Function
    
ErrProcedure:
    Set lcTipo_Cambio = Nothing
    Fnt_Restriccion_Rel_Porc = False
    
End Function

Private Function Busca_ID_PerfilRiesgo_ID_Moneda(ByVal pId_Cuenta, _
                                                 ByRef pId_Perfil_Riesgo, _
                                                 ByRef pId_Moneda, _
                                                 ByRef pDsc_Perfil_Riesgo, _
                                                 ByRef pDsc_Moneda) As Boolean
    'Dim lcCuenta As Class_Cuentas
    Dim lcCuenta As Object
    '-----------------------------
    'Dim lcMoneda As Class_Monedas
    Dim lcMoneda As Object
    '-----------------------------
  
    Busca_ID_PerfilRiesgo_ID_Moneda = True
  
    '  Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
        .Campo("id_cuenta").Valor = pId_Cuenta
        If .Buscar(True) Then
            If .Cursor.Count > 0 Then
                pId_Perfil_Riesgo = .Cursor(1)("id_perfil_riesgo").Value
                pId_Moneda = .Cursor(1)("id_moneda").Value
                pDsc_Perfil_Riesgo = .Cursor(1)("Dsc_Perfil_Riesgo").Value
            
        '        Set lcMoneda = New Class_Monedas
                Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
                With lcMoneda
                    .Campo("Id_moneda").Valor = pId_Moneda
                    If .Buscar Then
                        If .Cursor.Count > 0 Then
                            pDsc_Moneda = .Cursor(1)("dsc_Moneda").Value
                        End If
                    End If
                End With
                Set lcMoneda = Nothing
            Else
                MsgBox "No hay datos con el Nemot�cnico ingresado.", vbCritical
                Busca_ID_PerfilRiesgo_ID_Moneda = False
            End If
        Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas al cargar datos de la Cuenta.", _
                            .ErrMsg, pConLog:=True)
            Busca_ID_PerfilRiesgo_ID_Moneda = False
        End If
    End With
    Set lcCuenta = Nothing
  
End Function

Private Function Buscar_Cod_Prod_Instr(ByVal pId_Nemotecnico, ByRef pCod_Producto, ByRef pCod_Instrumento, ByRef pDsc_Producto, ByRef pDsc_Instrumento) As Boolean
    Dim lcNemotecnico As Class_Nemotecnicos

    Buscar_Cod_Prod_Instr = True
  
    Set lcNemotecnico = New Class_Nemotecnicos
    With lcNemotecnico
        .Campo("id_nemotecnico").Valor = pId_Nemotecnico
        If .BuscarView Then
            If .Cursor.Count > 0 Then
                pCod_Producto = .Cursor(1).Fields("cod_producto").Value
                pDsc_Producto = .Cursor(1).Fields("dsc_producto").Value
         
                pCod_Instrumento = .Cursor(1).Fields("cod_Instrumento").Value
                pDsc_Instrumento = .Cursor(1).Fields("dsc_Intrumento").Value
            Else
                MsgBox "No hay datos con el Nemot�cnico ingresado.", vbCritical
                Buscar_Cod_Prod_Instr = False
            End If
        Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al cargar datos del Nemot�cnico.", _
                        .ErrMsg, _
                        pConLog:=True)
            Buscar_Cod_Prod_Instr = False
        End If
    End With
    Set lcNemotecnico = Nothing
End Function

Private Function Buscar_Patrimonio_Cuenta(ByVal pId_Cuenta, ByRef pPatrimonio_Cta As Double) As Boolean
    Dim lcPatrimonio_Cta As Class_Patrimonio_Cuentas
  
    Buscar_Patrimonio_Cuenta = True
  
    Set lcPatrimonio_Cta = New Class_Patrimonio_Cuentas
    With lcPatrimonio_Cta
        .Campo("id_cuenta").Valor = pId_Cuenta
        .Campo("RENTABILIDAD_MON_CUENTA").Valor = Null
        .Campo("APORTE_RETIROS_MON_CUENTA").Valor = Null
        If .Buscar_Patrimonio_Ultimo_Cierre Then
            If .Cursor.Count > 0 Then
                pPatrimonio_Cta = .Cursor(1).Fields("patrimonio_mon_cuenta").Value
            '      Else
            '        MsgBox "No hay Patrimonio para la Cuenta.", vbCritical
            '        Buscar_Patrimonio_Cuenta = False
            End If
        Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al cargar el Patrimonio de la Cuenta.", _
                        .ErrMsg, _
                        pConLog:=True)
            Buscar_Patrimonio_Cuenta = False
        End If
    End With
    Set lcPatrimonio_Cta = Nothing
End Function

Private Function Buscar_Porcentaje_Instrumento(ByVal pId_Cuenta, ByVal pId_Nemotecnico, ByRef pId_Arbol_Clase_Inst, ByRef pPorcentaje) As Boolean
    Dim lcClass_Rel_Aci_Emp_Nemo As Class_Rel_ACI_Emp_Nemotecnico
    Dim lcClass_RestriccPorcArbolClase As Class_RestriccPorcArbolClase
    
    Buscar_Porcentaje_Instrumento = True
    
    Set lcClass_Rel_Aci_Emp_Nemo = New Class_Rel_ACI_Emp_Nemotecnico
    With lcClass_Rel_Aci_Emp_Nemo
        .Campo("Id_NEMOTECNICO").Valor = pId_Nemotecnico
      
        If .Busca_Id_Arbol(pId_Cuenta, "H") Then
            If .Cursor.Count > 0 Then
                If Not IsNull(.Cursor(1)("id_arbol").Value) Then
                  pId_Arbol_Clase_Inst = .Cursor(1)("id_arbol").Value
               Else
                  pId_Arbol_Clase_Inst = -1
               End If
            Else
                pId_Arbol_Clase_Inst = -1
            End If
        Else
            MsgBox .ErrMsg & " - " & .Errnum, vbCritical
            Exit Function
        End If
    End With
    Set lcClass_Rel_Aci_Emp_Nemo = Nothing
    
    Set lcClass_RestriccPorcArbolClase = New Class_RestriccPorcArbolClase
    With lcClass_RestriccPorcArbolClase
        .Campo("id_arbol_clase_inst").Valor = pId_Arbol_Clase_Inst
        .Campo("id_cuenta").Valor = pId_Cuenta
        If .Buscar Then
            If .Cursor.Count > 0 Then
                pPorcentaje = .Cursor(1).Fields("porcentaje").Value
            Else
                pPorcentaje = ""
            End If
        Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al cargar datos de Restricciones de Perfiles de Riesgo por Instrumento.", _
                        .ErrMsg, _
                        pConLog:=True)
            Buscar_Porcentaje_Instrumento = False
        End If
    End With

    Set lcClass_RestriccPorcArbolClase = Nothing
  
End Function

'''''Private Function Buscar_Porcentaje_Instrumento(ByVal pCod_Instrumento, _
'''''                                               ByVal pId_Perfil_Riesgo, _
'''''                                               ByRef pPorcentaje) As Boolean
'''''Dim lcRestric_Porc_Instr As Class_Restricc_Porc_Instrumento
'''''
'''''  Buscar_Porcentaje_Instrumento = True
'''''
'''''  Set lcRestric_Porc_Instr = New Class_Restricc_Porc_Instrumento
'''''  With lcRestric_Porc_Instr
'''''    .Campo("cod_instrumento").Valor = pCod_Instrumento
'''''    .Campo("id_perfil_riesgo").Valor = pId_Perfil_Riesgo
'''''    If .Buscar Then
'''''      If .Cursor.Count > 0 Then
'''''        pPorcentaje = .Cursor(1).Fields("porcentaje").Value
'''''      Else
'''''        pPorcentaje = ""
'''''      End If
'''''    Else
'''''      Call Fnt_MsgError(.SubTipo_LOG, _
'''''                        "Problemas al cargar datos de Restricciones de Perfiles de Riesgo por Instrumento.", _
'''''                        .ErrMsg, _
'''''                        pConLog:=True)
'''''      Buscar_Porcentaje_Instrumento = False
'''''    End If
'''''  End With
'''''
'''''End Function


'''''Private Function Buscar_Porcentaje_Producto(ByVal pCod_Producto, _
'''''                                            ByVal pId_Perfil_Riesgo, _
'''''                                            ByRef pPorcentaje) As Boolean
'''''Dim lcRestric_Porc_Prod As Class_Restricc_Porc_Producto
'''''
'''''  Buscar_Porcentaje_Producto = True
'''''
'''''  Set lcRestric_Porc_Prod = New Class_Restricc_Porc_Producto
'''''  With lcRestric_Porc_Prod
'''''    .Campo("cod_producto").Valor = pCod_Producto
'''''    .Campo("id_perfil_riesgo").Valor = pId_Perfil_Riesgo
'''''    If .Buscar Then
'''''      If .Cursor.Count > 0 Then
'''''        pPorcentaje = .Cursor(1).Fields("porcentaje").Value
'''''      Else
'''''        pPorcentaje = ""
'''''      End If
'''''    Else
'''''      Call Fnt_MsgError(.SubTipo_LOG, _
'''''                        "Problemas al cargar datos de Restricciones de Perfiles de Riesgo por Producto.", _
'''''                        .ErrMsg, _
'''''                        pConLog:=True)
'''''      Buscar_Porcentaje_Producto = False
'''''    End If
'''''  End With
'''''
'''''End Function

Private Function Buscar_Porcentaje_Producto(ByVal pId_Cuenta, ByVal pId_Nemotecnico, ByRef pId_Arbol_Clase_Prod, ByRef pPorcentaje) As Boolean

    Dim lcClass_Rel_Aci_Emp_Nemo As Class_Rel_ACI_Emp_Nemotecnico
    Dim lcClass_RestriccPorcArbolClase As Class_RestriccPorcArbolClase

    Buscar_Porcentaje_Producto = True
  
    Set lcClass_Rel_Aci_Emp_Nemo = New Class_Rel_ACI_Emp_Nemotecnico
    With lcClass_Rel_Aci_Emp_Nemo
      .Campo("Id_NEMOTECNICO").Valor = pId_Nemotecnico
      If .Busca_Id_Arbol(pId_Cuenta, "P") Then
          
          If .Cursor.Count > 0 Then
                If Not IsNull(.Cursor(1)("id_arbol").Value) Then
                  pId_Arbol_Clase_Prod = .Cursor(1)("id_arbol").Value
               Else
                  pId_Arbol_Clase_Prod = -1
               End If
          Else
            pId_Arbol_Clase_Prod = -1
          End If
      Else
          MsgBox .ErrMsg & " - " & .Errnum, vbCritical
          Exit Function
      End If
    End With
    
    Set lcClass_Rel_Aci_Emp_Nemo = Nothing

    Set lcClass_RestriccPorcArbolClase = New Class_RestriccPorcArbolClase
    
    With lcClass_RestriccPorcArbolClase
        .Campo("id_arbol_clase_inst").Valor = pId_Arbol_Clase_Prod
        .Campo("id_cuenta").Valor = pId_Cuenta
        
        If .Buscar Then
            If .Cursor.Count > 0 Then
                pPorcentaje = .Cursor(1).Fields("porcentaje").Value
            Else
                pPorcentaje = ""
            End If
        Else
            Call Fnt_MsgError(.SubTipo_LOG, "Problemas al cargar datos de Restricciones de Perfiles de Riesgo por Producto.", _
                        .ErrMsg, pConLog:=True)
            Buscar_Porcentaje_Producto = False
        End If
    End With

    Set lcClass_RestriccPorcArbolClase = Nothing
    
End Function
Private Function Busca_Monto_Actual(ByVal pId_Cuenta, ByVal pCod_Nombre, pEs_Instrumento As Boolean) As Double
    Dim lcSaldos_Activos As Class_Saldo_Activos
    Dim lCod_Instrumento
    Dim lCod_Producto
    Dim lMonto_Total As Double
  
    Busca_Monto_Actual = 0
  
    Set lcSaldos_Activos = New Class_Saldo_Activos
    With lcSaldos_Activos
        .Campo("id_cuenta").Valor = pId_Cuenta
    
        If pEs_Instrumento Then
            lCod_Instrumento = pCod_Nombre
            lCod_Producto = Null
        Else
            lCod_Instrumento = Null
            lCod_Producto = pCod_Nombre
        End If
    
        If .Buscar_Total_Monto_x_Instr_Prod(lMonto_Total, lCod_Instrumento, lCod_Producto) Then
            If .Cursor.Count > 0 Then
                Busca_Monto_Actual = NVL(.Cursor(1).Fields("monto_total").Value, 0)
            End If
        End If
    End With
End Function

Private Function Verifica_Patrimonio(ByVal pId_Cuenta, _
                                     ByVal pMonto_Pariado, _
                                     ByVal pMonto_Actual, _
                                     ByVal pPorcentaje_Decimal, _
                                     ByVal pPatrimonio, _
                                     ByVal pNombre, _
                                     ByVal pValor_Nombre, _
                                     ByVal pDsc_Perfil_Riesgo, _
                                     ByVal pId_Moneda_Cuenta, _
                                     ByVal pDsc_Moneda, _
                                     ByVal pTipo_Cambio) As Boolean
                                     
    Dim lPorcentaje_Actual As Double
    '---------------------------------------
    Dim lMonto_despues_Oper As Double
    Dim lPorc_Decimal_despues_Oper As Double
    Dim lPorcentaje_despues_Oper As Double
    '---------------------------------------
    Dim lPorcentaje_Perfil As Double
    '---------------------------------------
    Dim lMensaje As String

    Verifica_Patrimonio = True
                                
    Rem Hans Alvarez: como la cuenta no tiene patrimonio, se deja operar.
    If pPatrimonio = 0 Then
        Exit Function
    End If
  
    Rem Si el Porcentaje es vacio significa que no hay restricciones. Se deja operar
    If pPorcentaje_Decimal = "" Then
        Exit Function
    End If
  
    pPorcentaje_Decimal = CDbl(pPorcentaje_Decimal)
    '--------------------------
    'pPatrimonio = 10000 'BORRAR
    '--------------------------
  
    Rem Monto actual mas el Monto ingresado
    lMonto_despues_Oper = pMonto_Actual + pMonto_Pariado
  
    Rem Porcentaje en decimales del monto actual mas el monto ingresado con respescto al patrimonio
    lPorc_Decimal_despues_Oper = (lMonto_despues_Oper) / pPatrimonio
  
    Rem Compara en el porcentaje despues de operar con el porcentaje de restriccion del perfil de riesgo
    If lPorc_Decimal_despues_Oper > pPorcentaje_Decimal Then
        Rem Porcentaje decimal del Perfil de Riesgo expresado en porcentajes
        lPorcentaje_Perfil = pPorcentaje_Decimal * 100
        lPorcentaje_Perfil = Format(lPorcentaje_Perfil, Fnt_Formato_Moneda(pId_Moneda_Cuenta))
    
        Rem Porcentaje actual en cartera con respecto al patrimonio
        lPorcentaje_Actual = (pMonto_Actual * 100) / pPatrimonio
        lPorcentaje_Actual = Format(lPorcentaje_Actual, "##0,#0")
      
        Rem Porcentaje decimal despues de operar expresado en porcentaje
        lPorcentaje_despues_Oper = lPorc_Decimal_despues_Oper * 100
        lPorcentaje_despues_Oper = Format(lPorcentaje_despues_Oper, Fnt_Formato_Moneda(pId_Moneda_Cuenta))
         
        lMensaje = "Se est� excediendo en el limite." & vbCr & vbCr & "Su limite por " & pValor_Nombre & ", es de " & lPorcentaje_Perfil & "% "
    
        'lMensaje = "El Monto Total ingresado (" & Format(pMonto_Pariado, Fnt_Formato_Moneda(pId_Moneda_Cuenta)) & _
        '           ") sobrepasa la Restricci�n Porcentual del Perfil de Riesgo " & pDsc_Perfil_Riesgo & _
        '           " relacionado con " & pNombre & " " & pValor_Nombre & ", que corresponde al (" & _
        '           lPorcentaje_Perfil & "%) del Patrimonio de la Cuenta." & vbCr & vbCr & _
        '           "Monto Patrimonio cuenta: " & Format(pPatrimonio, Fnt_Formato_Moneda(pId_Moneda_Cuenta)) & vbCr & _
        '           "Monto " & pValor_Nombre & " actual: " & Format(pMonto_Actual, Fnt_Formato_Moneda(pId_Moneda_Cuenta)) & " (" & lPorcentaje_Actual & "%)" & vbCr & _
        '           "------------------------------------------------------------------------------" & vbCr & _
        '           "Monto " & pValor_Nombre & " si se realiza la operaci�n: " & Format(lMonto_despues_Oper, Fnt_Formato_Moneda(pId_Moneda_Cuenta)) & " (" & lPorcentaje_despues_Oper & "%)" & vbCr & vbCr & _
        '           "Valores pariados a la moneda de la Cuenta: " & pDsc_Moneda
        
        If pTipo_Cambio = 1 Then
            lMensaje = lMensaje & vbCr & vbCr & vbCr & "�Desea continuar con la operaci�n?"
        Else
            lMensaje = lMensaje & vbCr & "Valor Tipo de Cambio usado: " & pTipo_Cambio & vbCr & vbCr & vbCr & "�Desea continuar con la operaci�n?"
        End If
    
        If MsgBox(lMensaje, vbYesNo + vbDefaultButton2 + vbInformation, "Advertencia") = vbNo Then
            Verifica_Patrimonio = False
        End If
    End If
  
End Function

Private Function Fnt_Sumar_Operaciones_Desde(pId_Cuenta, pFecha_Desde, ByRef pMonto_Sumado, Optional pCod_Instrumento _
                                           , Optional pCod_Producto) As Boolean
    Dim lcOperacion_Detalle As Class_Operaciones_Detalle
    Dim lcCierre_Cuenta     As Class_Cierres_Cuentas
    '-----------------------------------------------------------------------
    Dim lUlt_Cierre         As Date
    Dim lMonto_Sumado       As Double

    Fnt_Sumar_Operaciones_Desde = False

    Set lcCierre_Cuenta = New Class_Cierres_Cuentas
    With lcCierre_Cuenta
        .Campo("id_cuenta").Valor = pId_Cuenta
        .Campo("FECHA_CIERRE").Valor = pFecha_Desde
        If Not .BuscarUltimo Then
            Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem, _
                        "Problemas con buscar el ultimo cierre de la cuenta.", _
                        .ErrMsg, _
                        pConLog:=True)
            GoTo ExitProcedure
        End If
    
        If .Cursor.Count > 0 Then
            lUlt_Cierre = .Cursor(1)("fecha_cierre").Value
        Else
            'Si no existe sumo desde el inicio del siglo
            lUlt_Cierre = DateSerial(2000, 1, 1)
        End If
    End With

    Set lcOperacion_Detalle = New Class_Operaciones_Detalle
    With lcOperacion_Detalle
        If Not .Sumar_Desde(pId_Cuenta:=pId_Cuenta _
                      , pFecha_Operacion:=lUlt_Cierre _
                      , pMonto_Sumado:=lMonto_Sumado _
                      , pCod_Instrumento:=pCod_Instrumento _
                      , pCod_Producto:=pCod_Producto) Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al sumas las operaciones de la cuenta.", _
                        .ErrMsg, _
                        pConLog:=True)
            GoTo ExitProcedure
        End If
    End With
  
    pMonto_Sumado = lMonto_Sumado
  
    Fnt_Sumar_Operaciones_Desde = True
  
ExitProcedure:
    Set lcOperacion_Detalle = Nothing
    Set lcCierre_Cuenta = Nothing
End Function
