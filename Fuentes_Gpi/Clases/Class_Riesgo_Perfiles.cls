VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Riesgo_Perfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Saldos_Activos" ' "Pkg_Riesgo_Perfiles" '

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Saldo_Activo"
    .AddCampo "Id_Saldo_Activo", ePT_Numero, ePD_Ambos
    '----------------------------------------------------
    '-- nuevos campos para el cierre
    '----------------------------------------------------
    .AddCampo "Fecha_Cierre", ePT_Fecha, ePD_Entrada
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada
'    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada
'    .AddCampo "Cantidad", ePT_Numero, ePD_Entrada
'    .AddCampo "Precio", ePT_Numero, ePD_Entrada
'    .AddCampo "Tasa", ePT_Numero, ePD_Entrada
'    .AddCampo "Id_Moneda_Cta", ePT_Numero, ePD_Entrada
'    .AddCampo "Monto_Mon_Cta", ePT_Numero, ePD_Entrada
'    .AddCampo "Id_Moneda_Nemotecnico", ePT_Numero, ePD_Entrada
'    .AddCampo "Monto_Mon_Nemotecnico", ePT_Numero, ePD_Entrada
'    .AddCampo "Id_Moneda_Origen", ePT_Numero, ePD_Entrada
'    .AddCampo "Monto_Mon_Origen", ePT_Numero, ePD_Entrada
'    .AddCampo "Valor_Paridad", ePT_Numero, ePD_Entrada
'    .AddCampo "Cantidad_Mantenida", ePT_Numero, ePD_Entrada
'    .AddCampo "Monto_Mantenido", ePT_Numero, ePD_Entrada
'    .AddCampo "Id_Mov_Activo", ePT_Numero, ePD_Entrada
'    .AddCampo "TASA_COMPRA", ePT_Numero, ePD_Entrada
'    .AddCampo "MONTO_VALOR_COMPRA", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Fnt_Consulta_ACI(pId_Empresa, pId_Asesor) As Boolean
Dim lClass_Entidad As New Class_Entidad

  Fnt_Consulta_ACI = False

  With lClass_Entidad
    Call .AddCampo("FECHA_CIERRE", ePT_Fecha, ePD_Entrada, Me.Campo("FECHA_CIERRE").Valor)
    Call .AddCampo("ID_EMPRESA", ePT_Numero, ePD_Entrada, pId_Empresa)
    Call .AddCampo("ID_ASESOR", ePT_Numero, ePD_Entrada, pId_Asesor)

  If .Buscar("PKG_RIESGO_PERFILES" & ".Busca_Consulta_ACI") Then
    Fnt_Consulta_ACI = True
    Set fClass_Entidad.Cursor = .Cursor
  End If

    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing

End Function

'Public Function SubTipo_LOG() As Double
'  SubTipo_LOG = 37
'End Function
''---------------------------------------------------------
'
'Public Function ErrMsg() As String
'  ErrMsg = fClass_Entidad.ErrMsg
'End Function
'
'Public Function Errnum() As Double
'  Errnum = fClass_Entidad.Errnum
'End Function
'
'Public Function Buscar()
'  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
'End Function
'
'Public Function Guardar()
'  'VERIFICACION DE LOS VALORES QUE DEBEN IR DEBIDO A QUE SON NOT NULL
'  With fClass_Entidad
'    .Campo("Cantidad_Mantenida").Valor = NVL(.Campo("Cantidad_Mantenida").Valor, 0)
'    .Campo("monto_Mon_Origen").Valor = NVL(.Campo("monto_Mon_Origen").Valor, 0)
'    .Campo("valor_Paridad").Valor = NVL(.Campo("valor_Paridad").Valor, 0)
'    .Campo("monto_Mantenido").Valor = NVL(.Campo("monto_Mantenido").Valor, 0)
'    .Campo("monto_Mon_Cta").Valor = NVL(.Campo("monto_Mon_Cta").Valor, 0)
'  End With
'
'  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
'End Function
'
'Public Function Buscar_Ultimo_Cierre(Optional pId_Cuenta = Null _
'                                   , Optional pId_Nemotecnico = Null _
'                                   , Optional pCod_Instrumento = Null _
'                                   , Optional pCod_Producto = Null _
'                                   , Optional pId_Mov_Activo = Null _
'                                   , Optional pId_Saldo_Activo = Null) As Boolean
'
'Dim lClass_Entidad As New Class_Entidad
'
'  Buscar_Ultimo_Cierre = False
'
'  With lClass_Entidad
'    If Not IsNull(pId_Cuenta) Then
'      .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
'    End If
'    If Not IsNull(pCod_Instrumento) Then
'      .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada, pCod_Instrumento
'    End If
'    If Not IsNull(pCod_Producto) Then
'      .AddCampo "Cod_Producto", ePT_Caracter, ePD_Entrada, pCod_Producto
'    End If
'    If Not IsNull(pId_Nemotecnico) Then
'      .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada, pId_Nemotecnico
'    End If
'    If Not IsNull(pId_Mov_Activo) Then
'      .AddCampo "Id_Mov_Activo", ePT_Numero, ePD_Entrada, pId_Mov_Activo
'    End If
'    If Not IsNull(pId_Saldo_Activo) Then
'      .AddCampo "Id_Saldo_Activo", ePT_Numero, ePD_Entrada, pId_Saldo_Activo
'    End If
'
'    Buscar_Ultimo_Cierre = .Buscar(cfPackage & ".Buscar_Ultimo_Cierre")
'    Set fClass_Entidad.Cursor = .Cursor
'    fClass_Entidad.Errnum = .Errnum
'    fClass_Entidad.ErrMsg = .ErrMsg
'  End With
'  Set lClass_Entidad = Nothing
'End Function
'
'Public Function Buscar_Ultimo_CierreCuenta(Optional pId_Cuenta = Null _
'                                          , Optional pId_Nemotecnico = Null _
'                                          , Optional pCod_Instrumento = Null _
'                                          , Optional pCod_Producto = Null _
'                                          , Optional pId_Mov_Activo = Null _
'                                          , Optional pId_Saldo_Activo = Null _
'                                          , Optional pCod_Mercado = Null) As Boolean
'Dim lClass_Entidad As New Class_Entidad
'
'  With lClass_Entidad
'    If Not IsNull(pId_Cuenta) Then
'      .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
'    End If
'    If Not IsNull(pCod_Instrumento) Then
'      .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada, pCod_Instrumento
'    End If
'    If Not IsNull(pCod_Producto) Then
'      .AddCampo "Cod_Producto", ePT_Caracter, ePD_Entrada, pCod_Producto
'    End If
'    If Not IsNull(pId_Nemotecnico) Then
'      .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada, pId_Nemotecnico
'    End If
'    If Not IsNull(pId_Mov_Activo) Then
'      .AddCampo "Id_Mov_Activo", ePT_Numero, ePD_Entrada, pId_Mov_Activo
'    End If
'    If Not IsNull(pId_Saldo_Activo) Then
'      .AddCampo "Id_Saldo_Activo", ePT_Numero, ePD_Entrada, pId_Saldo_Activo
'    End If
'    If Not IsNull(pCod_Mercado) Then
'      .AddCampo "Cod_Mercado", ePT_Caracter, ePD_Entrada, pCod_Mercado
'    End If
'
'    Buscar_Ultimo_CierreCuenta = .Buscar(cfPackage & ".Buscar_Ultimo_CierreCuenta")
'    Set fClass_Entidad.Cursor = .Cursor
'    fClass_Entidad.Errnum = .Errnum
'    fClass_Entidad.ErrMsg = .ErrMsg
'  End With
'  Set lClass_Entidad = Nothing
'End Function
'
'Public Function Buscar_al_cierre(pFecha_Cierre _
'                              , Optional pId_Cuenta = Null _
'                              , Optional pId_Nemotecnico = Null _
'                              , Optional pCod_Instrumento = Null _
'                              , Optional pCod_Producto = Null _
'                              , Optional pId_Mov_Activo = Null _
'                              , Optional pNemotecnico = Null) As Boolean
'Dim lClass_Entidad As New Class_Entidad
'
'  Buscar_al_cierre = False
'
'  With lClass_Entidad
'    .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada, pFecha_Cierre
'    .AddCampo "Id_Cuenta", ePT_Caracter, ePD_Entrada, pId_Cuenta
'    .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada, pCod_Instrumento
'    .AddCampo "Cod_Producto", ePT_Caracter, ePD_Entrada, pCod_Producto
'    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada, pId_Nemotecnico
'    .AddCampo "Id_Mov_Activo", ePT_Numero, ePD_Entrada, pId_Mov_Activo
'    .AddCampo "Nemotecnico", ePT_Caracter, ePD_Entrada, pNemotecnico
'
'    Buscar_al_cierre = .Buscar(cfPackage & ".Buscar_al_cierre")
'    Set fClass_Entidad.Cursor = .Cursor
'    fClass_Entidad.Errnum = .Errnum
'    fClass_Entidad.ErrMsg = .ErrMsg
'  End With
'  Set lClass_Entidad = Nothing
'End Function
'
'Public Function Fnt_Buscar_Productos_Activos(pFecha As Date _
'                              , Optional pId_Cuenta = Null _
'                              , Optional pId_Asesor = Null _
'                              , Optional pId_Perfil_Riesgo = Null _
'                              , Optional pId_Tipo_Adm = Null _
'                              , Optional pId_Empresa = Null) As Boolean
'Dim lClass_Entidad As New Class_Entidad
'
'  Fnt_Buscar_Productos_Activos = False
'
'  With lClass_Entidad
'    Call .AddCampo("id_Cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta)
'    Call .AddCampo("id_asesor", ePT_Numero, ePD_Entrada, pId_Asesor)
'    Call .AddCampo("id_perfil_riesgo", ePT_Numero, ePD_Entrada, pId_Perfil_Riesgo)
'    Call .AddCampo("cod_tipo_adm", ePT_Caracter, ePD_Entrada, pId_Tipo_Adm)
'    Call .AddCampo("fecha", ePT_Fecha, ePD_Entrada, pFecha)
'    Call .AddCampo("id_empresa", ePT_Numero, ePD_Entrada, pId_Empresa)
'
'    Fnt_Buscar_Productos_Activos = .Buscar(cfPackage & ".Buscar_Productos_Activos")
'    Set fClass_Entidad.Cursor = .Cursor
'    fClass_Entidad.Errnum = .Errnum
'    fClass_Entidad.ErrMsg = .ErrMsg
'  End With
'  Set lClass_Entidad = Nothing
'
'End Function
'
'Public Function Fnt_Buscar_Saldo_Cuenta_Nemo(pSaldo, pCantidad, pSaldo_Restringido) As Boolean
'
'  Fnt_Buscar_Saldo_Cuenta_Nemo = True
'
'  gDB.Parametros.Clear
'
'  gDB.Parametros.Add "p" & Campo("id_Cuenta").Nombre, ePT_Numero, Campo("id_Cuenta").Valor, ePD_Entrada
'  gDB.Parametros.Add "p" & Campo("id_Nemotecnico").Nombre, ePT_Numero, Campo("id_Nemotecnico").Valor, ePD_Entrada
'  gDB.Parametros.Add "PSaldo", ePT_Numero, pSaldo, ePD_Ambos
'  gDB.Procedimiento = cfPackage & ".Busca_Saldo_Cuenta_Nemo"
'
'  If gDB.EjecutaSP Then
'    If Val(NVL(gDB.Parametros("PSaldo").Valor, "0")) - Val(pCantidad) < pSaldo_Restringido Then
'      Fnt_Buscar_Saldo_Cuenta_Nemo = False
'    End If
'  End If
'  gDB.Parametros.Clear
'
'End Function
'
'Public Function Deshacer_Enlaze_Cierre(ByVal pId_Cuenta As Double, ByVal pFecha_Cierre As Date)
'Dim lClass_Entidad As Class_Entidad
'
'  Set lClass_Entidad = New Class_Entidad
'  With lClass_Entidad
'    Call .AddCampo("id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta)
'    Call .AddCampo("fecha_cierre", ePT_Fecha, ePD_Entrada, pFecha_Cierre)
'
'    Deshacer_Enlaze_Cierre = .Borrar(cfPackage & ".DESHACER_ENLAZE_CIERRE")
'    fClass_Entidad.ErrMsg = .ErrMsg
'    fClass_Entidad.Errnum = .Errnum
'  End With
'  Set lClass_Entidad = Nothing
'End Function
'
'Public Function Guardar_Mon_Nemo()
'  Guardar_Mon_Nemo = fClass_Entidad.Borrar(cfPackage & ".Guardar_Mon_Nemo")
'End Function
'
'Public Function Guardar_Paridades()
'  Guardar_Paridades = fClass_Entidad.Borrar(cfPackage & ".GUARDAR_PARIDADES")
'End Function
'
'Public Function Buscar_Total_Monto_x_Instr_Prod(ByRef pMonto_Total As Double, _
'                                                Optional pCod_Instrumento = Null, _
'                                                Optional pCod_Producto = Null) As Boolean
'Dim lClass_Entidad As Class_Entidad
'
'  Buscar_Total_Monto_x_Instr_Prod = False
'
'  Set lClass_Entidad = New Class_Entidad
'  With lClass_Entidad
'    Call .AddCampo("id_cuenta", ePT_Caracter, ePD_Entrada, Me.Campo("id_cuenta").Valor)
'    Call .AddCampo("cod_instrumento", ePT_Caracter, ePD_Entrada, pCod_Instrumento)
'    Call .AddCampo("cod_producto", ePT_Caracter, ePD_Entrada, pCod_Producto)
'
'    Buscar_Total_Monto_x_Instr_Prod = .Buscar(cfPackage & ".Buscar_Total_Monto_x_Instr_Prod")
'    Set fClass_Entidad.Cursor = .Cursor
'    pMonto_Total = NVL(.Cursor(1)("Monto_Total").Value, 0)
'
'    fClass_Entidad.Errnum = .Errnum
'    fClass_Entidad.ErrMsg = .ErrMsg
'  End With
'  Set lClass_Entidad = Nothing
'End Function
'
'Public Function Buscar_Patrimonios_Instr_Asesor(pFecha_Inicial As Date, _
'                                                pFecha_Final As Date) As Boolean
'Dim lClass_Entidad As Class_Entidad
'
'  Buscar_Patrimonios_Instr_Asesor = False
'
'  Set lClass_Entidad = New Class_Entidad
'
'  With lClass_Entidad
'    Call .AddCampo("Fecha_Inicial", ePT_Fecha, ePD_Entrada, pFecha_Inicial)
'    Call .AddCampo("Fecha_Final", ePT_Fecha, ePD_Entrada, pFecha_Final)
'
'    Buscar_Patrimonios_Instr_Asesor = .Buscar(cfPackage & ".Buscar_Patrimonios_Instr_Asesor")
'    Set fClass_Entidad.Cursor = .Cursor
'    fClass_Entidad.Errnum = .Errnum
'    fClass_Entidad.ErrMsg = .ErrMsg
'  End With
'  Set lClass_Entidad = Nothing
'End Function
'
'Public Function Buscar_Nemotecnicos_Uc(pFecha_Cierre _
'                                , pId_Empresa _
'                                , Optional pId_Cuenta = Null _
'                                , Optional pCod_Instrumento = Null) As Boolean
'Dim lClass_Entidad As Class_Entidad
'
'  Set lClass_Entidad = New Class_Entidad
'  With lClass_Entidad
'    Call .AddCampo("fecha_cierre", ePT_Fecha, ePD_Entrada, pFecha_Cierre)
'    Call .AddCampo("id_empresa", ePT_Numero, ePD_Entrada, pId_Empresa)
'    Call .AddCampo("id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta)
'    Call .AddCampo("Cod_Instrumento", ePT_Caracter, ePD_Entrada, pCod_Instrumento)
'
'    Buscar_Nemotecnicos_Uc = .Buscar(cfPackage & ".Busca_Nemotecnicos_Uc")
'    Set fClass_Entidad.Cursor = .Cursor
'    fClass_Entidad.Errnum = .Errnum
'    fClass_Entidad.ErrMsg = .ErrMsg
'  End With
'  Set lClass_Entidad = Nothing
'End Function
'
'Public Function Buscar_Activos_Productos()
'  Buscar_Activos_Productos = fClass_Entidad.Buscar(cfPackage & ".Buscar_Activos_Productos")
'End Function
'
'Public Function Buscar_Activos_Productos_Resumen()
'  Buscar_Activos_Productos_Resumen = fClass_Entidad.Buscar(cfPackage & ".Buscar_Activos_Productos_Resumen")
'End Function
'
'Public Function Calcula_4_Cierre_IDMA(Optional pCod_Instrumento) As Boolean
'  If Not IsEmpty(pCod_Instrumento) Then
'    Call fClass_Entidad.AddCampo("COD_INSTRUMENTO", ePT_Caracter, ePD_Entrada, pCod_Instrumento)
'  End If
'
'  Calcula_4_Cierre_IDMA = fClass_Entidad.Buscar(cfPackage & ".Calcula_4_Cierre_IDMA")
'
'  If Not IsEmpty(pCod_Instrumento) Then
'    Call fClass_Entidad.DelCampo("COD_INSTRUMENTO")
'  End If
'End Function
'
'Public Function Calcula_4_Cierre(Optional pCod_Instrumento) As Boolean
'  If Not IsEmpty(pCod_Instrumento) Then
'    Call fClass_Entidad.AddCampo("COD_INSTRUMENTO", ePT_Caracter, ePD_Entrada, pCod_Instrumento)
'  End If
'
'  Calcula_4_Cierre = fClass_Entidad.Buscar(cfPackage & ".Calcula_4_Cierre")
'
'  If Not IsEmpty(pCod_Instrumento) Then
'    Call fClass_Entidad.DelCampo("COD_INSTRUMENTO")
'  End If
'End Function
'
'Public Function Buscar_Al_Cierre_Valorizar(pFecha_Cierre _
'                                         , pId_Cuenta _
'                                         , pCod_Instrumento) As Boolean
'Dim lClass_Entidad As New Class_Entidad
'
'  With lClass_Entidad
'    Call .AddCampo("fecha_cierre", ePT_Fecha, ePD_Entrada, pFecha_Cierre)
'    Call .AddCampo("Id_Cuenta", ePT_Caracter, ePD_Entrada, pId_Cuenta)
'    Call .AddCampo("Cod_Instrumento", ePT_Caracter, ePD_Entrada, pCod_Instrumento)
'
'    Buscar_Al_Cierre_Valorizar = .Buscar(cfPackage & ".Buscar_Al_Cierre_Valorizar")
'    Set fClass_Entidad.Cursor = .Cursor
'    fClass_Entidad.Errnum = .Errnum
'    fClass_Entidad.ErrMsg = .ErrMsg
'  End With
'  Set lClass_Entidad = Nothing
'End Function
'
'Public Function Buscar_al_CierreCuenta() As Boolean
'  Buscar_al_CierreCuenta = fClass_Entidad.Buscar(cfPackage & ".Buscar_al_CierreCuenta")
'End Function
'
'Public Function Buscar_saldos_para_cartola(pFecha_Cierre _
'                              , Optional pId_Cuenta = Null _
'                              , Optional pId_Nemotecnico = Null _
'                              , Optional pCod_Instrumento = Null _
'                              , Optional pCod_Producto = Null _
'                              , Optional pId_Mov_Activo = Null _
'                              , Optional pNemotecnico = Null) As Boolean
'Dim lClass_Entidad As New Class_Entidad
'
'  Buscar_saldos_para_cartola = False
'
'  With lClass_Entidad
'    .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada, pFecha_Cierre
'    .AddCampo "Id_Cuenta", ePT_Caracter, ePD_Entrada, pId_Cuenta
'    .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada, pCod_Instrumento
'    .AddCampo "Cod_Producto", ePT_Caracter, ePD_Entrada, pCod_Producto
'    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada, pId_Nemotecnico
'    .AddCampo "Id_Mov_Activo", ePT_Numero, ePD_Entrada, pId_Mov_Activo
'    .AddCampo "Nemotecnico", ePT_Caracter, ePD_Entrada, pNemotecnico
'
'    Buscar_saldos_para_cartola = .Buscar(cfPackage & ".Buscar_saldos_para_cartola")
'    Set fClass_Entidad.Cursor = .Cursor
'    fClass_Entidad.Errnum = .Errnum
'    fClass_Entidad.ErrMsg = .ErrMsg
'  End With
'  Set lClass_Entidad = Nothing
'End Function
'
'Public Function Buscar_Cuentas_X_Instrumento(Optional pCod_Instrumento = Null) As Boolean
'Dim lClass_Entidad As New Class_Entidad
'
'  Buscar_Cuentas_X_Instrumento = False
'
'  With lClass_Entidad
'    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada, Me.Campo("id_nemotecnico").Valor
'    .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada, pCod_Instrumento
'    Buscar_Cuentas_X_Instrumento = .Buscar(cfPackage & ".Buscar_Cuentas_X_Instrumento")
'    Set fClass_Entidad.Cursor = .Cursor
'    fClass_Entidad.Errnum = .Errnum
'    fClass_Entidad.ErrMsg = .ErrMsg
'  End With
'  Set lClass_Entidad = Nothing
'
'End Function
'
'Public Function Buscar_X_Arbol_Clase_Inst(pId_Arbol_Clase_Inst) As Boolean
'Dim lClass_Entidad As Class_Entidad
'
'  Buscar_X_Arbol_Clase_Inst = False
'
'  Set lClass_Entidad = New Class_Entidad
'  With lClass_Entidad
'    Call .AddCampo("id_arbol_clase_inst", ePT_Numero, ePD_Entrada, pId_Arbol_Clase_Inst)
'    Call .AddCampo("id_cuenta", ePT_Numero, ePD_Entrada, fClass_Entidad.Campo("Id_Cuenta").Valor)
'    Call .AddCampo("id_empresa", ePT_Numero, ePD_Entrada, Fnt_EmpresaActual)
'    Call .AddCampo("fecha_cierre", ePT_Fecha, ePD_Entrada, fClass_Entidad.Campo("Fecha_Cierre").Valor)
'
'    If .Buscar(cfPackage & ".BUSCAR_X_ARBOL_CLASE_INSTRUMENTO") Then
'      Buscar_X_Arbol_Clase_Inst = True
'      Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
'    End If
'
'    fClass_Entidad.Errnum = .Errnum
'    fClass_Entidad.ErrMsg = .ErrMsg
'  End With
'  Set lClass_Entidad = Nothing
'End Function
'
