VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Rel_Nemotecnico_Atributo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "PKG_REL_NEMOTECNICO_ATRIBUTOS"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLS_Rel_Nemotec_Valor_Clasific
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada
    .AddCampo "ISIM", ePT_Caracter, ePD_Entrada
    .AddCampo "CUSIP", ePT_Caracter, ePD_Entrada
    .AddCampo "SEDOL", ePT_Caracter, ePD_Entrada
    .AddCampo "N_ACCIONESEMISOR", ePT_Numero, ePD_Entrada
    .AddCampo "FECHA_PREPAGO", ePT_Fecha, ePD_Entrada
    .AddCampo "NEMO_CTAPASO", ePT_Caracter, ePD_Entrada
    .AddCampo "TICKER", ePT_Caracter, ePD_Entrada
    .AddCampo "COD_ACHS", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_INSTRUMENTO_EXT", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function
