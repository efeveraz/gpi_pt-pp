VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Comi_Hono_Ase_Cta_Cargo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Comi_Hono_Ase_Cta_Cargo"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 75
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Guardar(ByVal lRetencion As Long) As Boolean
  Guardar = False
    
  'Primero revisa si tiene los cargos asociados, si no los tienes, los crea.
  'Honorarios.
  If IsNull(Me.Campo("ID_CARGO_ABONO_HONORARIO").Valor) Or IsNull(Me.Campo("ID_CARGO_ABONO_ASESORIA").Valor) Then
    If Not RealizarCargos(lRetencion) Then
      GoTo ExitProcedure
    End If
  End If

  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
  
ExitProcedure:
  
End Function

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_COMI_HONO_ASE_CTA_CARGO"
    .AddCampo "ID_COMI_HONO_ASE_CTA_CARGO", ePT_Numero, ePD_Ambos
    .AddCampo "ID_CARGO_ABONO_HONORARIO", ePT_Numero, ePD_Entrada
    .AddCampo "ID_CARGO_ABONO_ASESORIA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MONEDA", ePT_Numero, ePD_Entrada
    .AddCampo "COMISION_HONORARIOS", ePT_Numero, ePD_Entrada
    .AddCampo "COMISION_ASESORIAS", ePT_Numero, ePD_Entrada
    .AddCampo "COMISION_ASESORIAS_ORIG", ePT_Numero, ePD_Entrada
    .AddCampo "COMISION_HONORARIOS_ORIG", ePT_Numero, ePD_Entrada
    .AddCampo "FCH_COMI_HA_CTA_CARGO", ePT_Fecha, ePD_Entrada
    .AddCampo "FECHA_INI", ePT_Fecha, ePD_Entrada
    .AddCampo "FECHA_TER", ePT_Fecha, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function


Public Function Reporte_Detallado(pFecha_Inicio, pFECHA_TERMINO)
  With fClass_Entidad
    .AddCampo "FECHA_INICIO", ePT_Fecha, ePD_Entrada, pFecha_Inicio
    .AddCampo "FECHA_TERMINO", ePT_Fecha, ePD_Entrada, pFECHA_TERMINO
     Reporte_Detallado = fClass_Entidad.Buscar(cfPackage & ".REPORTE_DETALLE")
     .DelCampo "FECHA_INICIO"
     .DelCampo "FECHA_TERMINO"
  End With
End Function


Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Private Function RealizarCargos(ByVal nRetencion As Long) As Boolean
Dim lcCargo_Abono As Class_Cargos_Abonos
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lcMov_Caja_Origen As Class_Mov_Caja_Origen
'-------------------------------------------------------
Dim lDsc_Origen_Mov_Caja  As String

  RealizarCargos = False
  If Me.Campo("COMISION_HONORARIOS").Valor <> 0 Then
      Set lcCaja_Cuenta = New Class_Cajas_Cuenta
      With lcCaja_Cuenta
        .Campo("id_cuenta").Valor = Me.Campo("ID_CUENTA").Valor
        .Campo("id_moneda").Valor = Me.Campo("id_moneda").Valor
        If Not .Buscar_Caja_Para_Invertir() Then
          fClass_Entidad.ErrMsg = .ErrMsg
          fClass_Entidad.Errnum = .Errnum
          GoTo ExitProcedure
        End If
        
        If .Cursor.Count = 0 Then
          fClass_Entidad.Errnum = Me.SubTipo_LOG
          fClass_Entidad.ErrMsg = "No existe una caja definida para la moneda, es imposible realizar el cargo."
        End If
      End With
      
      'HONORARIOS
      If IsNull(Me.Campo("ID_CARGO_ABONO_HONORARIO").Valor) Then
        lDsc_Origen_Mov_Caja = ""
        Set lcMov_Caja_Origen = New Class_Mov_Caja_Origen
        With lcMov_Caja_Origen
          .Campo("COD_ORIGEN_MOV_CAJA").Valor = gcOrigen_Mov_Caja_Comision_Honorarios
          If Not .Buscar Then
            fClass_Entidad.Errnum = .Errnum
            fClass_Entidad.ErrMsg = .ErrMsg
            GoTo ExitProcedure
          End If
          
          If .Cursor.Count <= 0 Then
            fClass_Entidad.Errnum = Me.SubTipo_LOG
            fClass_Entidad.ErrMsg = "No existe el Origen del Movimiento de Caja (" & .Campo("COD_ORIGEN_MOV_CAJA").Valor & ")."
            GoTo ExitProcedure
          End If
          
          lDsc_Origen_Mov_Caja = "" & .Cursor(1)("dsc_glosa_CARGO").Value
        End With
        Set lcMov_Caja_Origen = Nothing
      
        lDsc_Origen_Mov_Caja = Replace(lDsc_Origen_Mov_Caja, "<<FECHA_INI>>", Me.Campo("FECHA_INI").Valor) 'CAMBIA LA FECHA DE INICIO DEL PERIODO
        lDsc_Origen_Mov_Caja = Replace(lDsc_Origen_Mov_Caja, "<<FECHA_FIN>>", Me.Campo("FECHA_TER").Valor) 'CAMBIA LA FECHA DE INICIO DEL PERIODO
      
        Set lcCargo_Abono = New Class_Cargos_Abonos
        With lcCargo_Abono
          .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_Comision_Honorarios
          .Campo("Id_Caja_Cuenta").Valor = lcCaja_Cuenta.Cursor(1)("id_caja_cuenta").Value
          .Campo("Id_Cuenta").Valor = Me.Campo("id_cuenta").Valor
          .Campo("Id_Moneda").Valor = Me.Campo("id_moneda").Valor
          .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente '--Siempre se ingresa en estado "P"endiente
          .Campo("Dsc_Cargo_Abono").Valor = lDsc_Origen_Mov_Caja
          .Campo("Fecha_Movimiento").Valor = Me.Campo("FCH_COMI_HA_CTA_CARGO").Valor
          .Campo("Flg_Tipo_Cargo").Valor = gcTipoOperacion_Cargo
          .Campo("Monto").Valor = Me.Campo("COMISION_HONORARIOS").Valor
          .Campo("Retencion").Valor = nRetencion
          .Campo("Flg_Tipo_Origen").Valor = "A"  '--Automatico
    
          If Not .Guardar Then
            fClass_Entidad.ErrMsg = .ErrMsg
            fClass_Entidad.Errnum = .Errnum
            GoTo ExitProcedure
          End If
          
          Me.Campo("ID_CARGO_ABONO_HONORARIO").Valor = .Campo("ID_CARGO_ABONO").Valor
        End With
        Set lcCargo_Abono = Nothing
      End If
  End If

  'ASESORIAS
  If Me.Campo("COMISION_ASESORIAS").Valor <> 0 Then
      If IsNull(Me.Campo("ID_CARGO_ABONO_ASESORIA").Valor) Then
        lDsc_Origen_Mov_Caja = ""
        Set lcMov_Caja_Origen = New Class_Mov_Caja_Origen
        With lcMov_Caja_Origen
          .Campo("COD_ORIGEN_MOV_CAJA").Valor = gcOrigen_Mov_Caja_Comision_Asesorias
          If Not .Buscar Then
            fClass_Entidad.Errnum = .Errnum
            fClass_Entidad.ErrMsg = .ErrMsg
            GoTo ExitProcedure
          End If
          
          If .Cursor.Count <= 0 Then
            fClass_Entidad.Errnum = Me.SubTipo_LOG
            fClass_Entidad.ErrMsg = "No existe el Origen del Movimiento de Caja (" & .Campo("COD_ORIGEN_MOV_CAJA").Valor & ")."
            GoTo ExitProcedure
          End If
          
          lDsc_Origen_Mov_Caja = "" & .Cursor(1)("dsc_glosa_CARGO").Value
        End With
        Set lcMov_Caja_Origen = Nothing
      
        lDsc_Origen_Mov_Caja = Replace(lDsc_Origen_Mov_Caja, "<<FECHA_INI>>", Me.Campo("FECHA_INI").Valor) 'CAMBIA LA FECHA DE INICIO DEL PERIODO
        lDsc_Origen_Mov_Caja = Replace(lDsc_Origen_Mov_Caja, "<<FECHA_FIN>>", Me.Campo("FECHA_TER").Valor) 'CAMBIA LA FECHA DE INICIO DEL PERIODO
      
        Set lcCargo_Abono = New Class_Cargos_Abonos
        With lcCargo_Abono
          .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_Comision_Asesorias
          .Campo("Id_Caja_Cuenta").Valor = lcCaja_Cuenta.Cursor(1)("id_caja_cuenta").Value
          .Campo("Id_Cuenta").Valor = Me.Campo("id_cuenta").Valor
          .Campo("Id_Moneda").Valor = Me.Campo("id_moneda").Valor
          .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente '--Siempre se ingresa en estado "P"endiente
          .Campo("Dsc_Cargo_Abono").Valor = lDsc_Origen_Mov_Caja
          .Campo("Fecha_Movimiento").Valor = Me.Campo("FCH_COMI_HA_CTA_CARGO").Valor
          .Campo("Flg_Tipo_Cargo").Valor = gcTipoOperacion_Cargo
          .Campo("Monto").Valor = Me.Campo("COMISION_ASESORIAS").Valor
          .Campo("Retencion").Valor = nRetencion
          .Campo("Flg_Tipo_Origen").Valor = "A"  '--Automatico
    
          If Not .Guardar Then
            fClass_Entidad.ErrMsg = .ErrMsg
            fClass_Entidad.Errnum = .Errnum
            GoTo ExitProcedure
          End If
          
          Me.Campo("ID_CARGO_ABONO_ASESORIA").Valor = .Campo("ID_CARGO_ABONO").Valor
        End With
        Set lcCargo_Abono = Nothing
      End If
  End If

  RealizarCargos = True

ExitProcedure:
  Set lcCargo_Abono = Nothing
  Set lcCaja_Cuenta = Nothing
  Set lcMov_Caja_Origen = Nothing
End Function
