VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Cartola"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private Declare Function GetTempPath Lib "kernel32" _
   Alias "GetTempPathA" (ByVal nBufferLength As Long, _
   ByVal lpBuffer As String) As Long

Private Declare Function GetTempFileName Lib "kernel32" _
   Alias "GetTempFileNameA" (ByVal lpszPath As String, _
   ByVal lpPrefixString As String, ByVal wUnique As Long, _
   ByVal lpTempFileName As String) As Long

Public IdCuenta As String
Public Num_Cuenta As String
Public Nombre As String
Public Perfil_Riesgo As String
Public Moneda As String
Public IDEmpresa As String
Public IdCliente As Integer       'Agregado por MMA para Cartola Consolidada
Public IdMonedaSalida As Integer  'Agregado por MMA para Cartola Consolidada y para Cartola Normal
Public TipoConsolidado As String  'Agregado por MMA para Cartola Consolidada Valores :CLT/GRP
Public IdGrupo As Integer         'Agregado por MMA para Cartola Consolidada
Public FlgComision As String

Public Direccion As String
Public Comuna As String
Public Ciudad As String
Public Telefono As String
Public Rut As String
Public Ejecutivo As String
Public MailEjecutivo As String
Public FonoEjecutivo As String
Public RutEjecutivo As String
Public Mail As String

Public Fecha_Creacion As String
Public Fecha_Proceso As String
Public Fecha_Cartola As String
Public Fecha_Cierre As String
Public Decimales As Integer

Public Patrimonio_total As Double
Public Monto_por_Cobrar As Double
Public Monto_por_Pagar As Double
Public NombreArchivo As String

Public Type Est_Detalle_Cajas
    Descripcion As String
    saldo       As Double
End Type
Public Function TotalCajas(Optional Fecha_Consulta As String = "", _
                           Optional ByRef nMontoTotalCajas As Double = 0) As Double
    Dim lCursor_Cuenta As Object
    Dim lCursor_Cajas As Object
    Dim Reg_Cajas As Object
    Dim Reg_Cuenta  As Object
    
    Dim saldo As Double
    Dim montoxpagar As Double
    Dim montoxcobrar As Double
    Dim suma As Double

    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, IdCuenta, ePD_Entrada
    gDB.Procedimiento = "PKG_CAJAS_CUENTA$Buscarview"
    
    If Not gDB.EjecutaSP Then
        Exit Function
    End If
    Set lCursor_Cuenta = gDB.Parametros("Pcursor").Valor
    For Each Reg_Cuenta In lCursor_Cuenta
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "PID_CAJA_CUENTA", ePT_Numero, Reg_Cuenta("id_caja_cuenta").Value, ePD_Entrada
        If Fecha_Consulta = "" Then
            gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Cartola), ePD_Entrada
        Else
            gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Consulta), ePD_Entrada
        End If
        gDB.Parametros.Add "PID_CUENTA", ePT_Numero, IdCuenta, ePD_Entrada
        If IdMonedaSalida <> 0 Then
            gDB.Parametros.Add "pId_Moneda_Salida", ePT_Numero, IdMonedaSalida, ePD_Entrada
        End If
        gDB.Procedimiento = "PKG_SALDOS_CAJA$BUSCAR"
        If gDB.EjecutaSP Then
            Set lCursor_Cajas = gDB.Parametros("Pcursor").Valor
            gDB.Parametros.Clear
            For Each Reg_Cajas In lCursor_Cajas
                saldo = saldo + CDbl(Reg_Cajas("monto_mon_cta").Value)
            Next
        End If
    Next
    nMontoTotalCajas = saldo
End Function
Public Function Fnt_SaldosForward(ByRef pIdEmpresa, Optional pFecha As String = "", Optional pIdCuenta As String) As Double
Dim lCursor As hRecord
Dim lReg As Object
Dim saldo As Double

    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    If To_Number(pIdCuenta) > 0 Then
        gDB.Parametros.Add "PID_CUENTA", ePT_Caracter, pIdCuenta, ePD_Entrada
    End If
    gDB.Parametros.Add "PID_EMPRESA", ePT_Numero, pIdEmpresa, ePD_Entrada
    If pFecha = "" Then
        gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Cartola), ePD_Entrada
    Else
        gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(pFecha), ePD_Entrada
    End If
    gDB.Procedimiento = "PKG_SALDOS_DERIVADOS$BUSCAR"
    If gDB.EjecutaSP Then
            Set lCursor = gDB.Parametros("Pcursor").Valor
            gDB.Parametros.Clear
            For Each lReg In lCursor
                saldo = saldo + CDbl(lReg("VALOR_MERCADO").Value)
            Next
        End If
    Fnt_SaldosForward = saldo
End Function
'************************
'* El Retorno de esta funci�n es el Total de los Activos a una Fecha Determinada
'************************
Public Function DamePatrimonio(Optional Fecha_Consulta As String = "", _
                                Optional ByRef Patrimonio As Double = 0, _
                                Optional ByRef CuentaPorCobrar As Double = 0, _
                                Optional ByRef CuentaPorPagar As Double = 0, _
                                Optional ByRef ValorCuota As Double = 0, _
                                Optional ByRef Forward As Double = 0, _
                                Optional ByRef Comision As Double = 0) As Double
    Dim lCursor As hRecord
    Dim lReg As hFields
    DamePatrimonio = 0
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, IdCuenta, ePD_Entrada
    
    If Fecha_Consulta = "" Then
        gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Cartola), ePD_Entrada
    Else
        gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Consulta), ePD_Entrada
    End If
    If IdMonedaSalida <> 0 Then
        gDB.Parametros.Add "pId_Moneda_Salida", ePT_Numero, IdMonedaSalida, ePD_Entrada
    End If
    
    gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS$Buscar"
    
    If gDB.EjecutaSP Then
        Set lCursor = gDB.Parametros("Pcursor").Valor
        For Each lReg In lCursor
            Patrimonio_total = CDbl(NVL(lReg("patrimonio_mon_cuenta").Value, 0))
            Monto_por_Cobrar = CDbl(NVL(lReg("monto_x_cobrar_mon_cta").Value, 0))
            Monto_por_Pagar = CDbl(NVL(lReg("monto_x_pagar_mon_cta").Value, 0))
            
            Patrimonio = Patrimonio_total
            CuentaPorCobrar = Monto_por_Cobrar
            CuentaPorPagar = Monto_por_Pagar
            ValorCuota = CDbl(NVL(lReg("valor_cuota_mon_cuenta").Value, 0))
            Forward = CDbl(NVL(lReg("simultaneas").Value, 0))
            DamePatrimonio = CDbl(NVL(lReg("saldo_activo_mon_cuenta").Value, 0))
            Comision = CDbl(NVL(lReg("comision").Value, 0))
            Forward = CDbl(NVL(lReg("Forward").Value, 0))
        Next
    End If
    gDB.Parametros.Clear
    Set lReg = Nothing
    Set lCursor = Nothing
End Function

Private Sub Class_Initialize()
    Limpiar
End Sub

Public Sub Limpiar()
    Patrimonio_total = 0
    Monto_por_Cobrar = 0
    Monto_por_Pagar = 0
    
    IdCuenta = ""
    Num_Cuenta = ""
    Nombre = ""
    Perfil_Riesgo = ""
    Moneda = ""
    IDEmpresa = ""
    
    Direccion = ""
    Comuna = ""
    Ciudad = ""
    Telefono = ""
    Rut = ""
    Ejecutivo = ""
    
    MailEjecutivo = ""
    FonoEjecutivo = ""
    RutEjecutivo = ""

    Mail = ""
    
    Fecha_Creacion = ""
    Fecha_Proceso = ""
    Fecha_Cartola = ""
    Fecha_Cierre = ""
    Decimales = 0
    
End Sub
Public Function Saldos_de_Cajas(ByRef Detalle_Cajas() As Est_Detalle_Cajas, _
                                    Optional Fecha_Consulta As String) As Boolean
                           
    Dim lCursor_Cuenta As Object
    Dim lCursor_Cajas As Object
    Dim Reg_Cajas As Object
    Dim Reg_Cuenta  As Object
    
    Dim saldo As Double
    Dim montoxpagar As Double
    Dim montoxcobrar As Double
    Dim suma As Double
    Dim nFila As Integer
    nFila = 0
    saldo = 0

    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, IdCuenta, ePD_Entrada
    gDB.Procedimiento = "PKG_CAJAS_CUENTA$Buscarview"
    If Not gDB.EjecutaSP Then
        Exit Function
    End If
    Set lCursor_Cuenta = gDB.Parametros("Pcursor").Valor
    For Each Reg_Cuenta In lCursor_Cuenta
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "PID_CAJA_CUENTA", ePT_Numero, Reg_Cuenta("id_caja_cuenta").Value, ePD_Entrada
        If Fecha_Consulta = "" Then
            gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Cartola), ePD_Entrada
        Else
            gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Consulta), ePD_Entrada
        End If
        gDB.Procedimiento = "PKG_SALDOS_CAJA$BUSCAR"
        saldo = 0
        If gDB.EjecutaSP Then
            Set lCursor_Cajas = gDB.Parametros("Pcursor").Valor
            gDB.Parametros.Clear
            For Each Reg_Cajas In lCursor_Cajas
                saldo = saldo + CDbl(Reg_Cajas("monto_mon_cta").Value)
            Next
        End If
        ReDim Preserve Detalle_Cajas(0 To nFila)
        With Detalle_Cajas(nFila)
            .Descripcion = Reg_Cuenta("DSC_CAJA_CUENTA").Value
            .saldo = saldo
        End With
        nFila = nFila + 1
    Next
End Function

Public Function CreateTempFile(sPrefix As String) As String
   Dim sTmpPath As String * 512
   Dim sTmpName As String * 576
   Dim nRet As Long

   nRet = GetTempPath(512, sTmpPath)
   If (nRet > 0 And nRet < 512) Then
      nRet = GetTempFileName(sTmpPath, sPrefix, 0, sTmpName)
      If nRet <> 0 Then
         CreateTempFile = Left$(sTmpName, _
            InStr(sTmpName, vbNullChar) - 1)
      End If
   End If
End Function

'---------------------------------------------------------
' Agregado por MMA. Para Cartolas Consolidadas por Cliente
'---------------------------------------------------------
Public Function TraePatrimonioCliente(Optional Fecha_Consulta As String = "", _
                                Optional ByRef Patrimonio As Double = 0, _
                                Optional ByRef CuentaPorCobrar As Double = 0, _
                                Optional ByRef CuentaPorPagar As Double = 0, _
                                Optional ByRef ValorCuota As Double = 0) As Double
    Dim lCursor As hRecord
    Dim lReg As hFields
    Dim lfecha As Date

    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cliente", ePT_Numero, IdCliente, ePD_Entrada
    
    If Fecha_Consulta = "" Then
        lfecha = CDate(Fecha_Cartola)
    Else
        lfecha = CDate(Fecha_Consulta)
    End If
    gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, lfecha, ePD_Entrada
    gDB.Parametros.Add "pId_Moneda_Salida", ePT_Numero, IdMonedaSalida, ePD_Entrada
    gDB.Parametros.Add "PConsolidado", ePT_Caracter, TipoConsolidado, ePD_Entrada
    gDB.Procedimiento = "PKG_CARTOLA$Patrimonio_Consolidado"
    
    If gDB.EjecutaSP Then
        Set lCursor = gDB.Parametros("Pcursor").Valor
        For Each lReg In lCursor
            Patrimonio_total = lReg("patrimonio_mon_cuenta").Value
            Monto_por_Cobrar = lReg("monto_x_cobrar_mon_cta").Value
            Monto_por_Pagar = lReg("monto_x_pagar_mon_cta").Value
            Valor_Cuota = lReg("valor_cuota_mon_cuenta").Value
                                                     
            
            Patrimonio = Patrimonio + Patrimonio_total
            CuentaPorCobrar = CuentaPorCobrar + Monto_por_Cobrar
            CuentaPorPagar = CuentaPorPagar + Monto_por_Pagar
            ValorCuota = ValorCuota + Valor_Cuota
            TraePatrimonioCliente = CDbl(NVL(lReg("saldo_activo_mon_cuenta").Value, 0))
        Next
    End If
    gDB.Parametros.Clear
    Set lReg = Nothing
    Set lCursor = Nothing
End Function

Public Function TraeTotalCajasConsolidado(Optional Fecha_Consulta As String = "", _
                                     Optional ByRef nMontoTotalCajas As Double = 0) As Double
    Dim lCursor_Cajas As hRecord
    Dim Reg_Cajas As hFields
    
    Dim saldo As Double


    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pId_Negocio", ePT_Numero, IDEmpresa, ePD_Entrada
    If TipoConsolidado = CONSOLIDADO_POR_CLIENTE Then
        gDB.Parametros.Add "Pid_cliente", ePT_Numero, IdCliente, ePD_Entrada
    Else
        gDB.Parametros.Add "Pid_grupo", ePT_Numero, IdGrupo, ePD_Entrada
    End If
    
    If Fecha_Consulta = "" Then
        lfecha = CDate(Fecha_Cartola)
    Else
        lfecha = CDate(Fecha_Consulta)
    End If
    gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, lfecha, ePD_Entrada
    gDB.Parametros.Add "pId_Moneda_Salida", ePT_Numero, IdMonedaSalida, ePD_Entrada
    gDB.Parametros.Add "PConsolidado", ePT_Caracter, "CLT", ePD_Entrada
    gDB.Procedimiento = "PKG_CARTOLA$SaldosCajas_Consolidado"
    If gDB.EjecutaSP Then
        Set lCursor_Cajas = gDB.Parametros("Pcursor").Valor
        gDB.Parametros.Clear
        For Each Reg_Cajas In lCursor_Cajas
            saldo = Reg_Cajas("Monto_Mon_Cta").Value
        Next
    End If
    nMontoTotalCajas = saldo
End Function

Public Sub TraePatriyRentab_Consolidado(ByRef lPatrim As Double, _
                                        ByRef lRentabilidad As Double)
    Dim lCursor As hRecord
    Dim lReg As hFields
    Dim lfecha As Date
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cliente", ePT_Numero, IdCliente, ePD_Entrada
    gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Cartola), ePD_Entrada
    gDB.Parametros.Add "pId_Moneda_Salida", ePT_Numero, IdMonedaSalida, ePD_Entrada
    gDB.Parametros.Add "PConsolidado", ePT_Caracter, TipoConsolidado, ePD_Entrada
    gDB.Procedimiento = "PKG_CARTOLA$Patrimonio_Consolidado"
    
    If gDB.EjecutaSP Then
        Set lCursor = gDB.Parametros("Pcursor").Valor
        For Each lReg In lCursor
                lPatrim = NVL(lReg("PATRIMONIO_MON_CUENTA").Value, 0)
                lRentabilidad = NVL(lReg("RENTABILIDAD_MON_CUENTA").Value, 0)
        Next
    End If
    gDB.Parametros.Clear
    Set lReg = Nothing
    Set lCursor = Nothing
End Sub









