VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Mov_Caja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Mov_Caja"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 68
End Function
'---------------------------------------------------------

Public Function Buscar()
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Buscar_Mov_Caja_Cta_Pendientes(Optional ByVal pId_Cuenta = "") As Boolean
  With fClass_Entidad
    
    If Not pId_Cuenta = "" Then
      .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada
      .Campo("id_cuenta").Valor = pId_Cuenta
    End If
    
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada
    .Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
    
    Buscar_Mov_Caja_Cta_Pendientes = .Buscar(cfPackage & ".BuscarView_MovCajaCtaPend")
  End With
End Function

Public Function BuscarPendientes_Liquidacion(Optional ByVal pId_Cuenta = "") As Boolean
  With fClass_Entidad
  
    If Not pId_Cuenta = "" Then
      .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada
      .Campo("id_cuenta").Valor = pId_Cuenta
    End If
    
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada
    .Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
    
    BuscarPendientes_Liquidacion = .Buscar(cfPackage & ".BuscarView_MovCajaPendLiq")
  End With
End Function

Public Function Cursor() As hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function Guardar(Optional pCodEstadoAPV As String = "")
Dim lCod_Estado  As String
Dim lReg As hFields
Dim lMov_Caja As Class_Mov_Caja
  
  If pCodEstadoAPV <> "" Then    'Si es un Retiro o Tr.de Salida APV debe empezar como "A" por ser una solicitud
    lCod_Estado = pCodEstadoAPV
  Else
    lCod_Estado = cCod_Estado_Pendiente 'El estado por omision de un aporte es "P"endiente
  End If
  
  If Not Me.Campo("id_mov_caja").Valor = cNewEntidad Then
    Set lMov_Caja = New Class_Mov_Caja
    lMov_Caja.Campo("id_mov_caja").Valor = Me.Campo("Id_Mov_Caja").Valor
    If lMov_Caja.Buscar Then
      For Each lReg In lMov_Caja.Cursor
        If pCodEstadoAPV = "" Then
            lCod_Estado = lReg("cod_estado").Value
        End If
      Next
    Else
      'Si existe un problema el proceso devuelve false
      Guardar = False
      fClass_Entidad.Errnum = lMov_Caja.Errnum
      fClass_Entidad.ErrMsg = lMov_Caja.ErrMsg
      GoTo ErrProcedure
    End If
    Set lMov_Caja = Nothing
  End If
  
  Me.Campo("cod_estado").Valor = lCod_Estado
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
  
ErrProcedure:
  Set lMov_Caja = Nothing
    
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  'Set fCursor = Nothing
  
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Mov_Caja"
    .AddCampo "Id_Mov_Caja", ePT_Numero, ePD_Ambos
    .AddCampo "Id_Caja_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "Cod_Estado", ePT_Caracter, ePD_Entrada
    .AddCampo "Cod_Origen_Mov_Caja", ePT_Caracter, ePD_Entrada
    .AddCampo "Id_Saldo_Caja", ePT_Numero, ePD_Entrada
    .AddCampo "Fecha_Movimiento", ePT_Fecha, ePD_Entrada
    .AddCampo "Fecha_Liquidacion", ePT_Fecha, ePD_Entrada
    .AddCampo "Monto", ePT_Numero, ePD_Entrada
    .AddCampo "Flg_Tipo_Movimiento", ePT_Caracter, ePD_Entrada
    .AddCampo "Dsc_Mov_Caja", ePT_Caracter, ePD_Entrada
    '----------------------------------------------------------
    .AddCampo "Id_Cierre", ePT_Numero, ePD_Entrada
    .AddCampo "Fecha_Cierre", ePT_Fecha, ePD_Entrada
    .AddCampo "Flg_Mov_Automatico", ePT_Caracter, ePD_Entrada
  End With
End Sub

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Eliminar() As Boolean
Dim lParam  As DLL_COMUN.Parametro
'Dim lMov_Caja As New Class_Mov_Caja
Dim lReg  As hFields

  Eliminar = True
  
  If Me.Buscar Then
    For Each lReg In fClass_Entidad.Cursor
      Select Case lReg("cod_estado").Value
        Case cCod_Estado_Liquidado
          Eliminar = False
          fClass_Entidad.Errnum = -1
          fClass_Entidad.ErrMsg = "No se pueden eliminar el movimiento N� " & lReg("Id_Apo_Res_Cuenta").Value & ", por que ya esta liquidado."
          GoTo ErrProcedure
      End Select
    Next
  Else
    'Si existe un problema el proceso devuelve false
    Eliminar = False
    fClass_Entidad.Errnum = gDB.Errnum
    fClass_Entidad.ErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If
  
  gDB.Parametros.Clear
  Call gDB.Parametros.Add("p" & Me.Campo("id_mov_caja").Nombre, Me.Campo("id_mov_caja").tipo, Me.Campo("id_mov_caja").Valor, Me.Campo("id_mov_caja").Direccion)
  'Me.Campo("id_mov_caja").Valor
  gDB.Procedimiento = cfPackage & ".Borrar"
  
  If Not gDB.EjecutaSP Then
    'Si existe un problema el proceso devuelve false
    Eliminar = False
    fClass_Entidad.Errnum = gDB.Errnum
    fClass_Entidad.ErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Anular(Pid_Mov_Caja, Optional pId_Mov_Caja_Retencion = "") As Boolean
On Error GoTo ErrProcedure
Dim lMov_Caja As New Class_Mov_Caja
Dim lReg As hFields
  
  Anular = True

  With lMov_Caja
    .Campo("id_mov_caja").Valor = Pid_Mov_Caja
    If .Buscar Then
      For Each lReg In .Cursor
        Select Case lReg("COD_ORIGEN_MOV_CAJA").Value
          Case gcOrigen_Mov_Caja_APO_RES
            'Si la anulacion es de un Aporte/Rescate
            Dim lAporte_Rescate_Cuenta As New Class_APORTE_RESCATE_CUENTA
            With lAporte_Rescate_Cuenta
              If Not .Anular_X_Mov_Caja(Pid_Mov_Caja) Then
                Anular = False
                fClass_Entidad.Errnum = .Errnum
                fClass_Entidad.ErrMsg = .ErrMsg
                GoTo ErrProcedure
              End If
            End With
            Set lAporte_Rescate_Cuenta = Nothing
        End Select
      Next
    Else
      Anular = False
      fClass_Entidad.Errnum = .Errnum
      fClass_Entidad.ErrMsg = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  
  gDB.Parametros.Clear
  gDB.Procedimiento = cfPackage & ".Anular"
  gDB.Parametros.Add "pid_mov_caja", ePT_Numero, Pid_Mov_Caja, ePD_Entrada
  If Not gDB.EjecutaSP Then
    Anular = False
    fClass_Entidad.Errnum = gDB.Errnum
    fClass_Entidad.ErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If
  
  '--------------------------
  'APV. SI ES RETIRO SE DEBE ANULAR TAMBIEN EL REGISTRO QUE INDICA LA RETENCION EFECTUADA
  If pId_Mov_Caja_Retencion <> "" Then
    gDB.Parametros.Clear
    gDB.Procedimiento = cfPackage & ".Anular"
    gDB.Parametros.Add "pid_mov_caja", ePT_Numero, pId_Mov_Caja_Retencion, ePD_Entrada
    If Not gDB.EjecutaSP Then
      Anular = False
      fClass_Entidad.Errnum = gDB.Errnum
      fClass_Entidad.ErrMsg = gDB.ErrMsg
      GoTo ErrProcedure
    End If
  End If
 '--------------------------
  gDB.Parametros.Clear

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Liquidar(Pid_Mov_Caja, pMonto As Double) As Boolean
Dim lMov_Caja As New Class_Mov_Caja
Dim lReg As hFields
  
  On Error GoTo ErrProcedure
  
  Liquidar = True

  With lMov_Caja
    .Campo("id_mov_caja").Valor = Pid_Mov_Caja
    If .Buscar Then
      For Each lReg In .Cursor
        Select Case lReg("COD_ORIGEN_MOV_CAJA").Value
          Case gcOrigen_Mov_Caja_APO_RES
            'Si la anulacion es de un Aporte/Rescate
            Dim lAporte_Rescate_Cuenta As New Class_APORTE_RESCATE_CUENTA
            With lAporte_Rescate_Cuenta
              If Not .Liquidar_X_Mov_Caja(Pid_Mov_Caja) Then
                Liquidar = False
                fClass_Entidad.Errnum = .Errnum
                fClass_Entidad.ErrMsg = .ErrMsg
                GoTo ErrProcedure
              End If
            End With
            Set lAporte_Rescate_Cuenta = Nothing
        End Select
      Next
    Else
      Liquidar = False
      fClass_Entidad.Errnum = .Errnum
      fClass_Entidad.ErrMsg = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  
  gDB.Parametros.Clear
  gDB.Procedimiento = cfPackage & ".Liquidar"
  gDB.Parametros.Add "pid_mov_caja", ePT_Numero, Pid_Mov_Caja, ePD_Entrada
  gDB.Parametros.Add "pMonto", ePT_Numero, pMonto, ePD_Entrada
  If Not gDB.EjecutaSP Then
    Liquidar = False
    fClass_Entidad.Errnum = gDB.Errnum
    fClass_Entidad.ErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If
  gDB.Parametros.Clear

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Buscar_Movimientos_Cuenta(pId_Cuenta As String, pFecha_Desde As Date, pFecha_Hasta As Date)
  With fClass_Entidad
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
    .AddCampo "fecha_desde", ePT_Fecha, ePD_Entrada, pFecha_Desde
    .AddCampo "fecha_hasta", ePT_Fecha, ePD_Entrada, pFecha_Hasta
    
    Buscar_Movimientos_Cuenta = .Buscar(cfPackage & ".Buscar_Movimientos_Cuenta")
  
    .DelCampo "id_cuenta"
    .DelCampo "fecha_desde"
    .DelCampo "fecha_hasta"
  End With
End Function

Public Function Deshacer_Enlaze_Cierre(ByVal pId_Cuenta As Double, ByVal pFecha_Cierre As Date)
  With fClass_Entidad
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
    .Campo("fecha_cierre").Valor = pFecha_Cierre
    
    Deshacer_Enlaze_Cierre = .Borrar(cfPackage & ".DESHACER_ENLAZE_CIERRE")
    
    .DelCampo "Id_Cuenta"
  End With
End Function

Public Function Enlazar_Cierre()
  Enlazar_Cierre = fClass_Entidad.Borrar(cfPackage & ".ENLAZAR_CIERRE")
End Function

Public Function Buscar_No_Rentabilidad(pId_Cuenta, ByRef pMov_Caja_Aporte, ByRef pMov_Caja_Rescate) As Boolean
Dim lClass_Entidad As Class_Entidad

  Buscar_No_Rentabilidad = False

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
    .AddCampo "Fecha_Movimiento", ePT_Fecha, ePD_Entrada, Me.Campo("Fecha_Movimiento").Valor
    
    If .Buscar(cfPackage & ".Buscar_No_Rentabilidad") Then
      Buscar_No_Rentabilidad = True
      'Se tubo que dejar asi por problemas con
      pMov_Caja_Aporte = .Cursor(1)("MOV_CAJA_APORTE").Value
      pMov_Caja_Rescate = .Cursor(1)("MOV_CAJA_RESCATE").Value
    End If
    
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
End Function

Public Function Busca_Flujos_Futuros() As Boolean
  Busca_Flujos_Futuros = fClass_Entidad.Buscar(cfPackage & ".Busca_Flujos_Futuros")
End Function

Public Function Buscar_Fecha_Liquid_FFMM(pId_Operacion_Detalle, ByRef pFecha_Liquidacion) As Boolean
Dim lClass_Entidad As Class_Entidad

  Buscar_Fecha_Liquid_FFMM = False
  pFecha_Liquidacion = ""
  
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "id_operacion_detalle", ePT_Numero, ePD_Entrada, pId_Operacion_Detalle
    
    If .Buscar(cfPackage & ".Buscar_Fecha_Liquid_FFMM") Then
      If .Cursor.Count > 0 Then
        pFecha_Liquidacion = .Cursor(1)("fecha_liquidacion").Value
      End If
      Buscar_Fecha_Liquid_FFMM = True
    End If
    
    .DelCampo "id_operacion_detalle"
    
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
End Function

Public Function Calcula_Fechas_DesCubiertas(pId_Cuenta) As Boolean
  With fClass_Entidad
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta

    Calcula_Fechas_DesCubiertas = .Buscar(cfPackage & ".Calcula_Fechas_Descubiertas")
    
    .DelCampo "id_cuenta"
  End With
End Function

Public Function Buscar_Flujos_Resumen(pId_Cuenta _
                                    , pFecha_Inicio _
                                    , pFecha_Termino) As Boolean
  With fClass_Entidad
    Call .AddCampo("Id_Cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta)
    Call .AddCampo("Fecha_Inicio", ePT_Fecha, ePD_Entrada, pFecha_Inicio)
    Call .AddCampo("Fecha_Termino", ePT_Fecha, ePD_Entrada, pFecha_Termino)

    Buscar_Flujos_Resumen = .Buscar(cfPackage & ".BUSCAR_FLUJOS_RESUMEN")
    
    Call .DelCampo("Id_Cuenta")
    Call .DelCampo("Fecha_Inicio")
    Call .DelCampo("Fecha_Termino")
  End With
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function
