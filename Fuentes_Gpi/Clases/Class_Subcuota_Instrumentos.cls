VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Subcuota_Instrumentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Subcuota_Instrumentos"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLog_Subtipo.eLS_Subcuota_Instrumentos
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_SUBCUOTA_INSTRUMENTO"
    .AddCampo "ID_SUBCUOTA_INSTRUMENTO", ePT_Numero, ePD_Ambos
    .AddCampo "ID_CIERRE", ePT_Numero, ePD_Entrada
    .AddCampo "FECHA_CIERRE", ePT_Fecha, ePD_Entrada
    .AddCampo "COD_INSTRUMENTO", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MONEDA_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "SALDO_ACTIVO_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "VALOR_CUOTA_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "TOTAL_CUOTAS_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "RENTABILIDAD_MON_CUENTA", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Guardar() As Boolean
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Buscar_Entre(pFecha_Desde, pFecha_Hasta) As Boolean
Dim lClass_Entidad As Class_Entidad

  Buscar_Entre = False

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada, fClass_Entidad.Campo("ID_CUENTA").Valor
    .AddCampo "Fecha_Desde", ePT_Fecha, ePD_Entrada, pFecha_Desde
    .AddCampo "Fecha_Hasta", ePT_Fecha, ePD_Entrada, pFecha_Hasta
    .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada, fClass_Entidad.Campo("cod_instrumento").Valor
    
    Buscar_Entre = .Buscar(cfPackage & ".Buscar_Entre")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Deshacer_Enlaze_Cierre(ByVal pId_Cuenta As Double, ByVal pFecha_Cierre As Date)
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "id_cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta
    .AddCampo "fecha_cierre", ePT_Fecha, ePD_Entrada, pFecha_Cierre
    
    Deshacer_Enlaze_Cierre = .Borrar(cfPackage & ".DESHACER_ENLAZE_CIERRE")
    fClass_Entidad.Errnum = lClass_Entidad.Errnum
    fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function CalculaSubcuota_X_Instrumento(ByVal pCod_Instrumento _
                                            , ByVal pId_Cuenta _
                                            , ByVal pFecha_Cierre _
                                            , ByVal pId_Cierre _
                                            , ByRef pfCuenta As hFields _
                                            , ByRef pGrilla As Object)
Dim lReg                    As hFields
Dim lReg_Ayer               As hFields
Dim lReg_Hoy                As hFields
Dim lcMov_Activo            As Class_Mov_Activos
Dim lcSaldo_Activos         As Class_Saldo_Activos
Dim lcSubCuota_Instrumento  As Class_Subcuota_Instrumentos
'Dim lcTipo_Cambio           As Class_Tipo_Cambios
Dim lcTipo_Cambio           As Object
Dim lcGuion                 As Class_AvanceGuion
Dim lDividendos             As Class_VariacionCapital
Dim lCortesCuponCuenta       As Class_Cortes_Cupon_Cuenta
'------------------------------------------------------
Dim lMonto_Mov_Activos    As Double
Dim lMonto_Saldos_Activos As Double
Dim lFecha_Ayer           As Date
Dim lMonto_MA             As Double
'--------
Dim pParidad As Double
Dim pOperacion As String
'---------------------------------------------------------------
Dim lContador_Activos      As Double
Dim lLin_Grilla            As Long
Dim lMsg_Grilla            As String
Dim lValor_Divid_Pag       As Double
Dim lValor_Corte_Pag       As Double

On Error GoTo ErrProcedure
                                        
  CalculaSubcuota_X_Instrumento = False
                                        
'---------------------------------------------------------------------------------------
  lLin_Grilla = (pGrilla.Rows - 1)
  lMsg_Grilla = GetCell(pGrilla, lLin_Grilla, "TEXTO")
  
  lContador_Activos = 0
  
  Set lcGuion = New Class_AvanceGuion
'---------------------------------------------------------------------------------------
  
  lFecha_Ayer = pFecha_Cierre - 1
                                        
  lMonto_Mov_Activos = 0
  lMonto_Saldos_Activos = 0

  '/*  BUSCA TODOS LOS ULTIMOS MOVIMIENTOS PENDIENTES DE CIERRE */
  '/*  SE SUMAN LOS INGRESOS (COMPRAS) Y SE RESTAN LOS EGRESOS (VENTAS) */
  Set lcMov_Activo = New Class_Mov_Activos
  lcMov_Activo.Campo("id_cuenta").Valor = pId_Cuenta
  If lcMov_Activo.BuscarView(pId_Cierre:=pId_Cierre _
                           , pCod_Instrumento:=pCod_Instrumento) Then
'    Set lcTipo_Cambio = New Class_Tipo_Cambios
    Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
    For Each lReg In lcMov_Activo.Cursor
      Call SetCell(pGrilla, lLin_Grilla, "TEXTO", lMsg_Grilla & lcGuion.Avanza, False)
      
      'Coloca el monto en negativo si es un egreso, si no, lo deja como esta.
      lMonto_MA = (lReg("Monto").Value * IIf(lReg("FLG_TIPO_MOVIMIENTO").Value = gcTipoOperacion_Ingreso, 1, -1))
      
      'Busca la paridad para calcular el monto de la operacion en la moneda de la cuenta
      If Not lcTipo_Cambio.Busca_Precio_Paridad(pId_Cuenta:=pId_Cuenta _
                                            , pId_Moneda_Origen:=lReg("id_moneda").Value _
                                            , pId_Moneda_Destino:=pfCuenta("id_moneda").Value _
                                            , pFecha:=pFecha_Cierre _
                                            , pParidad:=pParidad _
                                            , pOperacion:=pOperacion) Then
        Call Fnt_Escribe_Grilla(pGrilla, _
                                "E", _
                                "Problemas al buscar el precio de los tipos de cambios para la cuenta " & pfCuenta("num_cuenta").Value & ", entrego el siguiente mensaje: " & vbLf & vbLf & lcTipo_Cambio.ErrMsg)
        GoTo ExitProcedure
      End If
      
      lMonto_MA = Fnt_Calcula_TipoCambio_Ope(lMonto_MA, pParidad, pOperacion)
    
      'NOTA: Aqui falta transformar el monto a la moneda de la cuenta
      lMonto_Mov_Activos = lMonto_Mov_Activos + lMonto_MA
    Next
  End If

'---------------------------------------------------------------------------------------
'/*  BUSCA TODOS LOS SALDOS ACTIVOS DEL CIERRE ANTERIOR */
'---------------------------------------------------------------------------------------
  
  Set lcSaldo_Activos = New Class_Saldo_Activos
  With lcSaldo_Activos
      If Not .Buscar_al_cierre(pFecha_Cierre:=pFecha_Cierre _
                              , pId_Cuenta:=pId_Cuenta _
                              , pCod_Instrumento:=pCod_Instrumento) Then
        Call Fnt_Escribe_Grilla(pGrilla, _
                                "E", _
                                "La cuenta " & pfCuenta("num_Cuenta").Value & " se encontro el siguiente mensaje: " & vbLf & vbLf & .ErrMsg)
        GoTo ExitProcedure
      End If
  End With
  'Calcula cual es el monto de los saldos.
  For Each lReg In lcSaldo_Activos.Cursor
    Call SetCell(pGrilla, lLin_Grilla, "TEXTO", lMsg_Grilla & lcGuion.Avanza, False)
    lMonto_Saldos_Activos = lMonto_Saldos_Activos + lReg("Monto_Mon_Cta").Value
  Next
  
  If pCod_Instrumento = gcINST_ACCIONES_NAC Then
    '---------------------------------------------------
    'Incorpora los Dividendos PAgados al Saldo Activo
    '---------------------------------------------------
    'lValor_Divid_Pag = 0
    Set lDividendos = New Class_VariacionCapital
    With lDividendos
      If .Busca_Dividendos_Pagados(pId_Cuenta, pFecha_Cierre, lValor_Divid_Pag) Then
        lMonto_Saldos_Activos = lMonto_Saldos_Activos + lValor_Divid_Pag
      End If
    End With
    Set lDividendos = Nothing
  End If
  
  If pCod_Instrumento = gcINST_BONOS_NAC Then
    '---------------------------------------------------
    'Incorpora los Cortes de Cup�n Pagados al Saldo Activo
    '---------------------------------------------------
    'lValor_Divid_Pag = 0
    Set lCortesCuponCuenta = New Class_Cortes_Cupon_Cuenta
    With lCortesCuponCuenta
      If .Busca_Cortes_Cupon_Pagados(pId_Cuenta, pFecha_Cierre, lValor_Corte_Pag) Then
        lMonto_Saldos_Activos = lMonto_Saldos_Activos + lValor_Corte_Pag
      End If
    End With
    Set lCortesCuponCuenta = Nothing
  End If
  
  Set lcSubCuota_Instrumento = New Class_Subcuota_Instrumentos
  With lcSubCuota_Instrumento
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("cod_instrumento").Valor = pCod_Instrumento
    If Not .Buscar_Entre(lFecha_Ayer, pFecha_Cierre) Then
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "La cuenta " & pfCuenta("num_Cuenta").Value & " se encontro el siguiente mensaje: " & vbLf & vbLf & .ErrMsg)
      GoTo ExitProcedure
    End If
  
    'EL PATRIMONIO DEL DIA DEL CIERRE DE AYER
    Set lReg_Ayer = .Cursor.Buscar("fecha_cierre", lFecha_Ayer)
    'EL PATRIMONIO DEL DIA DEL CIERRE
    Set lReg_Hoy = .Cursor.Buscar("fecha_cierre", pFecha_Cierre)
  End With
  Set lcSubCuota_Instrumento = Nothing
  
  'Calculo de Rentabilidad
  Call Sub_CalculaCuota_HijoZu(lMonto_Saldos_Activos, lMonto_Mov_Activos, lReg_Ayer, lReg_Hoy)
  
  Set lcSubCuota_Instrumento = New Class_Subcuota_Instrumentos
  With lcSubCuota_Instrumento
    .Campo("ID_CIERRE").Valor = pId_Cierre
    .Campo("fecha_cierre").Valor = pFecha_Cierre
    .Campo("COD_INSTRUMENTO").Valor = pCod_Instrumento
    .Campo("ID_CUENTA").Valor = pId_Cuenta
    .Campo("ID_MONEDA_CUENTA").Valor = pfCuenta("id_moneda").Value
    .Campo("SALDO_ACTIVO_MON_CUENTA").Valor = lMonto_Saldos_Activos '- lValor_Divid_Pag
    .Campo("VALOR_CUOTA_MON_CUENTA").Valor = lReg_Hoy("VALOR_CUOTA_MON_CUENTA").Value
    .Campo("TOTAL_CUOTAS_MON_CUENTA").Valor = lReg_Hoy("TOTAL_CUOTAS_MON_CUENTA").Value
    .Campo("RENTABILIDAD_MON_CUENTA").Valor = lReg_Hoy("RENTABILIDAD_MON_CUENTA").Value
    
    If Not .Guardar Then
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "La cuenta " & pfCuenta("num_Cuenta").Value & " se encontro el siguiente mensaje: " & vbLf & vbLf & .ErrMsg)
      GoTo ExitProcedure
    End If
  End With
  
  CalculaSubcuota_X_Instrumento = True

ErrProcedure:
  If Not Err.Number = 0 Then
    MsgBox "Problema en el calculo de Subcuota de Acciones." & vbLf & vbLf & Err.Description, vbCritical
    GoTo ExitProcedure
    Resume
  End If
                                        
ExitProcedure:
  Set lcMov_Activo = Nothing
  Set lcSaldo_Activos = Nothing
  Set lcSubCuota_Instrumento = Nothing
End Function

Public Sub Sub_CalculaCuota_HijoZu(pPatrimonio As Double _
                                , pAportes_Retiros As Double _
                                , ByRef pReg_Ayer As hFields _
                                , ByRef pReg_Hoy As hFields)
Dim lTC_Aporte_Rescate As Double
  
On Error GoTo ErrProcedure
  
  If pReg_Ayer Is Nothing Then
    'SI EL REGISTRO DE AYER NO EXISTE SE TIENE QUE CREAR
    Set pReg_Ayer = New hFields
    pReg_Ayer.Add "VALOR_CUOTA_MON_CUENTA", 100
    pReg_Ayer.Add "TOTAL_CUOTAS_MON_CUENTA", 0
  End If
  
  If pReg_Hoy Is Nothing Then
    'SI EL REGISTRO DE AYER NO EXISTE SE TIENE QUE CREAR
    Set pReg_Hoy = New hFields
    pReg_Hoy.Add "VALOR_CUOTA_MON_CUENTA", Null
    pReg_Hoy.Add "TOTAL_CUOTAS_MON_CUENTA", 0
    pReg_Hoy.Add "RENTABILIDAD_MON_CUENTA", Null
  End If
  
  
  If IsNull(pReg_Ayer("VALOR_CUOTA_MON_CUENTA").Value) Then
    'si la valor de la cuota es NULL por cualquier motivo tendracomo si fuera el inicio de la cuenta
    ' con el valor 100
    pReg_Ayer("VALOR_CUOTA_MON_CUENTA").Value = 100
  End If
  
  If pAportes_Retiros >= 0 Then
    'Calcula el total de cuotas de los aportes calculandolo por el valor cuota de ayer
    lTC_Aporte_Rescate = Fnt_Divide(pAportes_Retiros, pReg_Ayer("VALOR_CUOTA_MON_CUENTA").Value)
    '/*  el total del cuotas de hoy se deja como la suma de el total de cuotas de ayer mas el total de cuotas de los aporte y rescates*/
    pReg_Hoy("TOTAL_CUOTAS_MON_CUENTA").Value = pReg_Ayer("TOTAL_CUOTAS_MON_CUENTA").Value + lTC_Aporte_Rescate
    If pReg_Hoy("TOTAL_CUOTAS_MON_CUENTA").Value = 0 Then
      pReg_Hoy("VALOR_CUOTA_MON_CUENTA").Value = Null
      pReg_Hoy("RENTABILIDAD_MON_CUENTA").Value = Null
    Else
      '/*  el calculo del valor de la cuota es el Patrimonio dividido por el total de cuotas */
      pReg_Hoy("VALOR_CUOTA_MON_CUENTA").Value = Fnt_Divide(pPatrimonio, pReg_Hoy("TOTAL_CUOTAS_MON_CUENTA").Value)
      '/*  calcula la rentabilidad del dia en la moneda de la cuenta */
      pReg_Hoy("RENTABILIDAD_MON_CUENTA").Value = (Fnt_Divide(pReg_Hoy("VALOR_CUOTA_MON_CUENTA").Value, NVL(pReg_Ayer("VALOR_CUOTA_MON_CUENTA").Value, 0)) - 1)
    End If
  Else
    'se calcula la nueva cuota condierando los rescates
    pReg_Hoy("VALOR_CUOTA_MON_CUENTA").Value = Fnt_Divide(pPatrimonio - pAportes_Retiros, pReg_Ayer("TOTAL_CUOTAS_MON_CUENTA").Value)
    pReg_Hoy("TOTAL_CUOTAS_MON_CUENTA").Value = Fnt_Divide(pPatrimonio, pReg_Hoy("VALOR_CUOTA_MON_CUENTA").Value)
    pReg_Hoy("RENTABILIDAD_MON_CUENTA").Value = (Fnt_Divide(pReg_Hoy("VALOR_CUOTA_MON_CUENTA").Value, NVL(pReg_Ayer("VALOR_CUOTA_MON_CUENTA").Value, 0)) - 1)
  End If

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problema en Cierre de la cuenta.", Err.Description)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:

End Sub




