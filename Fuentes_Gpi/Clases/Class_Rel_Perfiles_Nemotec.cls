VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Rel_Perfiles_Nemotec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Rel_Perfiles_Nemotec"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 13
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Perfil_Riesgo", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar_Perfiles_Nemotecnico() As Boolean
  Buscar_Perfiles_Nemotecnico = fClass_Entidad.Buscar(cfPackage & ".Buscar_perfiles_nemotecnico")
End Function

Public Function Buscar_Nemo_Perfil_Pertenece(pCod_Instrumento As String) As Boolean
  With fClass_Entidad
    .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada
    
    .Campo("Cod_Instrumento").Valor = pCod_Instrumento
    
    Buscar_Nemo_Perfil_Pertenece = .Buscar(cfPackage & ".Buscar_Nemo_Perfil_Pertenece")
  End With
End Function

Public Function Buscar_Nemo_Perfil_NoPertenece(pCod_Instrumento As String) As Boolean
  With fClass_Entidad
    .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada
    
    .Campo("Cod_Instrumento").Valor = pCod_Instrumento
    
    Buscar_Nemo_Perfil_NoPertenece = .Buscar(cfPackage & ".Buscar_Nemo_Perfil_NoPertenece")
  End With
End Function

Public Function Buscar_Nemo_Vigentes() As Boolean
  Buscar_Nemo_Vigentes = fClass_Entidad.Buscar(cfPackage & ".Buscar_Nemo_Vigentes")
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar_perfiles_nemotecnico")
End Function

Public Function Borrar_Nemotecnico_Perfiles(pCod_Instrumento As String) As Boolean
  With fClass_Entidad
    .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada
    
    .Campo("Cod_Instrumento").Valor = pCod_Instrumento
    
    Borrar_Nemotecnico_Perfiles = .Borrar(cfPackage & ".Borrar_nemotecnico_perfiles")
  End With
End Function

Public Function Borrar_Perfiles_Nemotecnico() As Boolean
  Borrar_Perfiles_Nemotecnico = fClass_Entidad.Borrar(cfPackage & ".Borrar_perfiles_nemotecnico")
End Function
