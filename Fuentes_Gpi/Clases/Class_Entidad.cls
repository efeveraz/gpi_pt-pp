VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Entidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Dim fCampos As Parametros
Dim fErrNum As Double
Dim fErrMsg As String

Dim fCursor As hRecord
'local variable(s) to hold property value(s)
Private fCampoPKey As String 'local copy

Friend Function Campos() As Parametros
  Set Campos = fCampos
End Function

Friend Sub Sub_CopiarReg2Campos(pReg As hFields)
Dim lReg As Parametro

On Error Resume Next

  For Each lReg In fCampos
    lReg.Valor = pReg(lReg.Nombre).Value
  Next
End Sub

Friend Property Let CampoPKey(ByVal vData As String)
Attribute CampoPKey.VB_Description = "Devuelve o establece el campo que se usa como PK en la tabla que se hace referencia. Es el campo con el cual se devolvera cuando se guarda la informacion a la DB"
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CampoPKey = 5
    fCampoPKey = vData
End Property

Friend Property Get CampoPKey() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CampoPKey
    CampoPKey = fCampoPKey
End Property

Friend Function Campo(pCampo As String) As Parametro
On Error Resume Next
  Set Campo = Nothing
  Set Campo = fCampos(pCampo)
End Function

Friend Property Let ErrNum(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CampoPKey = 5
    fErrNum = vData
End Property

Friend Property Get ErrNum() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CampoPKey
    ErrNum = fErrNum
End Property

Friend Property Let ErrMsg(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CampoPKey = 5
    fErrMsg = vData
End Property


Friend Property Get ErrMsg() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CampoPKey
    ErrMsg = fErrMsg
End Property

Friend Property Set Cursor(vData As hRecord)
  Set fCursor = vData
End Property

Friend Property Get Cursor() As hRecord
   Set Cursor = fCursor
End Property

Friend Sub LimpiaParam()
  With fCampos
    .Clear
  End With
End Sub

Friend Sub AddCampo(pNom_Campo As String, pTipo As DLL_COMUN.eParam_Tipos, pDirecion As DLL_COMUN.eParam_Direc, Optional pValor As Variant = Null)
  With fCampos
    .Add pNom_Campo, pTipo, pValor, pDirecion
  End With
End Sub

Friend Sub DelCampo(pNom_Campo As String)
  With fCampos
    .Remove pNom_Campo
  End With
End Sub

Friend Function Buscar(pProcedimiento As String) As Boolean
Dim lParam  As DLL_COMUN.Parametro
Dim lHora_Ini As Long
  
  Buscar = True
  
  gDB.Parametros.Clear
  For Each lParam In fCampos
    Rem Primero verifica que ningun campo este en null
    If Not IsNull(lParam.Valor) Then
      Rem Se les agrega la "p" debido a que son parametros
      Call gDB.Parametros.Add("p" & lParam.Nombre, lParam.tipo, lParam.Valor, lParam.Direccion)
    End If
  Next

'---------------------------------------------------------------------
'-- Se agrega el parametro de Cursor
'---------------------------------------------------------------------
  Call gDB.Parametros.Add("pCursor", ePT_Cursor, "", ePD_Ambos)
'---------------------------------------------------------------------
  
  lHora_Ini = GetTickCount
  gDB.Procedimiento = pProcedimiento
  If gDB.EjecutaSP Then
    Set fCursor = gDB.Parametros("pCursor").Valor
  Else
    Rem Si existe un problema el proceso devuelve false
    Buscar = False
    fErrNum = gDB.ErrNum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If

ErrProcedure:
  MDI_Principal.StatusBar.Panels("FECHASERVIDOR").ToolTipText = "Tiempo DB: " & ((GetTickCount - lHora_Ini) / 1000)

  gDB.Parametros.Clear
  Err.Clear
End Function

Friend Function Guardar(pProcedimiento As String) As Boolean
Dim lCod_Estado  As String
Dim lParam  As DLL_COMUN.Parametro
Dim lReg As hFields
'-----------------------------

  Guardar = False

  gDB.Parametros.Clear
  gDB.Procedimiento = pProcedimiento
  For Each lParam In fCampos
    'Se les agrega la "p" debido a que son parametros
    Call gDB.Parametros.Add("P" & lParam.Nombre, lParam.tipo, NVL(lParam.Valor, ""), lParam.Direccion)
  Next
  
  If Not gDB.EjecutaSP Then
    'Si existe un problema el proceso devuelve false
    fErrNum = gDB.ErrNum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If

  If Not fCampoPKey = "" Then
    fCampos(fCampoPKey).Valor = gDB.Parametros("p" & fCampoPKey).Valor
  End If

  Guardar = True

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Friend Function Borrar(pProcedimiento As String) As Boolean
Rem Funcion agregada CSM 24/08/2006
Dim lCod_Estado  As String
Dim lParam  As DLL_COMUN.Parametro
Dim lReg As hFields
'-----------------------------

  Borrar = False

  gDB.Parametros.Clear
  gDB.Procedimiento = pProcedimiento
  For Each lParam In fCampos
    'Primero verifica que ningun campo este en null
    If Not IsNull(lParam.Valor) Then
      'Se les agrega la "p" debido a que son parametros
      Call gDB.Parametros.Add("p" & lParam.Nombre, lParam.tipo, lParam.Valor, lParam.Direccion)
    End If
  Next
  
  If Not gDB.EjecutaSP Then
    'Si existe un problema el proceso devuelve false
    fErrNum = gDB.ErrNum
    fErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If

  For Each lParam In fCampos
    'Primero verifica que ningun campo este en null
    If Not IsNull(lParam.Valor) Then
      'Se les agrega la "p" debido a que son parametros
      Select Case lParam.Direccion
        Case ePD_Salida, ePD_Ambos
          fCampos(lParam.Nombre).Valor = gDB.Parametros("p" & lParam.Nombre).Valor
      End Select
    End If
  Next

  Borrar = True

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Private Sub Class_Initialize()
  Set fCampos = New Parametros
End Sub
