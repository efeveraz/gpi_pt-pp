VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_VariacionCapital"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Rem ------------------------------------------------------
Rem 03/08/2009. MMardones. El campo Cada_x_cantidad es
Rem             numérico y se estaba pasando como caracter
Rem ------------------------------------------------------

Const cfPackage = "Pkg_VariacionesCapital"

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 36
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Dividendo"
    .AddCampo "Id_Dividendo", ePT_Numero, ePD_Ambos
    .AddCampo "Id_Nemotecnico", ePT_Numero, ePD_Entrada
    .AddCampo "Fecha", ePT_Fecha, ePD_Entrada
    .AddCampo "Monto", ePT_Numero, ePD_Entrada
    .AddCampo "Fecha_Corte", ePT_Fecha, ePD_Entrada
    .AddCampo "Tipo_Variacion", ePT_Caracter, ePD_Entrada
    .AddCampo "Id_nemotecnico_opcion", ePT_Numero, ePD_Entrada
    .AddCampo "Cada_X_cantidad", ePT_Numero, ePD_Entrada        '03/08/2009
    .AddCampo "Entregar_X_cantidad", ePT_Numero, ePD_Entrada
    .AddCampo "Mensaje", ePT_Caracter, ePD_Salida      'Agregado por MMA 29/09/2008
  End With
End Sub
Private Function Fnt_Realiza_Operacion(ByVal pId_Cuenta, _
                                  ByVal pCod_Producto, _
                                  ByVal pId_Moneda, _
                                  ByVal pMonto, _
                                  ByVal pFecha, _
                                  ByVal pNemotecnico, _
                                  pGrilla As VSFlexGrid) As Boolean
Dim lcProductos As Class_Productos
Dim lCargo_Abono As Class_Cargos_Abonos
Dim lId_Caja_Cuenta As Double
Dim lRollback As Boolean
Dim lMercado As String
Dim lMsg_Error As String

  Fnt_Realiza_Operacion = True

  gDB.IniciarTransaccion
  lRollback = False
  
  Rem Busca el mercado del nemotecnico: Nacional o Internacional
  Set lcProductos = New Class_Productos
  With lcProductos
    .Campo("cod_producto").Valor = pCod_Producto
    If .Buscar Then
      If .Cursor.Count > 0 Then
        lMercado = .Cursor(1)("cod_Mercado").Value
      Else
        lRollback = True
        Call Fnt_Escribe_Grilla(pGrilla, "E", "Error Dividendo: Nemotécnico " & pNemotecnico & " no tiene mercado." & vbCr & vbCr & .ErrMsg)
        GoTo ErrProcedure
      End If
    Else
      lRollback = True
      Call Fnt_Escribe_Grilla(pGrilla, "E", "Error Dividendo: Nemotécnico " & pNemotecnico & " no tiene mercado." & vbCr & vbCr & .ErrMsg)
      GoTo ErrProcedure
    End If
  End With
  Set lcProductos = Nothing
  
  Rem Busca la caja de la cuenta
  lId_Caja_Cuenta = Fnt_ValirdarFinanciamiento(pId_Cuenta _
                                              , lMercado _
                                              , pMonto _
                                              , pId_Moneda _
                                              , Fnt_FechaServidor _
                                              , False _
                                              , lMsg_Error)
  If lId_Caja_Cuenta = -1 Then
    lRollback = True
    Call Fnt_Escribe_Grilla(pGrilla, "E", "Error Dividendo: Nemotécnico '" & pNemotecnico & "': Error en la Validacion de la caja de la cuenta " & pId_Cuenta & ". " & lMsg_Error)
    Rem Si hubo problemas se sale del procedimento
    GoTo ErrProcedure
  End If
  
  Rem Se realiza la operación
  Set lCargo_Abono = New Class_Cargos_Abonos
  With lCargo_Abono
    .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_DIVIDENDO
    .Campo("Id_Caja_Cuenta").Valor = lId_Caja_Cuenta
    .Campo("Id_Cuenta").Valor = pId_Cuenta
    .Campo("Id_Moneda").Valor = pId_Moneda
    .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente '--Siempre se ingresa en estado "P"endiente
    .Campo("Dsc_Cargo_Abono").Valor = "PAGO DIVIDENDO: " & pNemotecnico & "."
    .Campo("Fecha_Movimiento").Valor = pFecha
    .Campo("Flg_Tipo_Cargo").Valor = gcTipoOperacion_Cargo
    .Campo("Monto").Valor = pMonto
    .Campo("Retencion").Valor = 0 ' cero dias
    .Campo("Flg_Tipo_Origen").Valor = "A"  '--Automatico (Por mientras todo es manual)
    
    If Not .Guardar Then
      lRollback = True
      Call Fnt_Escribe_Grilla(pGrilla, _
                         "E", _
                         "Error Dividendo: Nemotécnico '" & pNemotecnico & "': " & lCargo_Abono.ErrMsg)
    End If
  End With
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    Fnt_Realiza_Operacion = False
  Else
    gDB.CommitTransaccion
  End If
End Function
Public Function Buscar() As Boolean
    Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function
Public Function Buscar_Dividendos_Futuros(ByVal pId_Nemotecnico As String, _
                                          ByVal pFecha_ini, _
                                          ByVal Pfecha_Ter) As Boolean
Dim lEntidad As Class_Entidad

  Set lEntidad = New Class_Entidad
  With lEntidad
    .AddCampo "id_nemotecnico", ePT_Numero, ePD_Entrada, pId_Nemotecnico
    .AddCampo "fecha_ini", ePT_Fecha, ePD_Entrada, pFecha_ini
    .AddCampo "fecha_ter", ePT_Fecha, ePD_Entrada, Pfecha_Ter
    
    Buscar_Dividendos_Futuros = .Buscar("Pkg_Dividendos.Buscar_Dividendos_Futuros")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Ejecuta_x_Cuenta(pId_Cuenta) As Boolean
Dim lcDividendo           As Class_VariacionCapital
Dim lcSaldo_Activo        As Class_Saldo_Activos
Dim lcMov_Caja_Origen     As Class_Mov_Caja_Origen
Dim lcCaja_Cuenta         As Class_Cajas_Cuenta
Dim lcCargo_Abono         As Class_Cargos_Abonos
Dim lCursorDiv            As hRecord
Dim lCursorCaja           As hRecord
Dim lRegDiv               As hFields
Dim lRegSaldos            As hFields
'-------------------------------------------------------------
Dim lFecha_Dividendo As Date
Dim lDsc_Origen_Mov_Caja As String
Dim lDsc_Origen_Mov_Caja_Cuenta As String
Dim lMonto_Dividendo As Double
  
  Ejecuta_x_Cuenta = False
  
  lFecha_Dividendo = Me.Campo("fecha").Valor
  
  'Busca todos los cortes que caen ese dia.
  Set lcDividendo = New Class_VariacionCapital
  With lcDividendo
    .Campo("Fecha_Corte").Valor = lFecha_Dividendo
    .Campo("Tipo_Variacion").Valor = "DIVIDENDOS"
    If Not .Buscar() Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  
    Set lCursorDiv = .Cursor
  End With
  Set lcDividendo = Nothing

  lDsc_Origen_Mov_Caja = ""
  Set lcMov_Caja_Origen = New Class_Mov_Caja_Origen
  With lcMov_Caja_Origen
    .Campo("COD_ORIGEN_MOV_CAJA").Valor = gcOrigen_Mov_Caja_DIVIDENDO
    If Not .Buscar Then
      fClass_Entidad.Errnum = .Errnum
      fClass_Entidad.ErrMsg = .ErrMsg
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      fClass_Entidad.Errnum = Me.SubTipo_LOG
      fClass_Entidad.ErrMsg = "No existe el Origen del Movimiento de Caja (" & .Campo("COD_ORIGEN_MOV_CAJA").Valor & ")."
      GoTo ExitProcedure
    End If
    
    lDsc_Origen_Mov_Caja = "" & .Cursor(1)("dsc_glosa_abono").Value
  End With


  For Each lRegDiv In lCursorDiv
    Set lcSaldo_Activo = New Class_Saldo_Activos
    With lcSaldo_Activo
      If Not .Buscar_al_cierre(pFecha_Cierre:=lFecha_Dividendo _
                              , pId_Cuenta:=pId_Cuenta _
                              , pId_Nemotecnico:=lRegDiv("id_nemotecnico").Value) Then
        fClass_Entidad.ErrMsg = .ErrMsg
        fClass_Entidad.Errnum = .Errnum
        GoTo ExitProcedure
      End If
    End With
          
    For Each lRegSaldos In lcSaldo_Activo.Cursor
      Set lcCaja_Cuenta = New Class_Cajas_Cuenta
      With lcCaja_Cuenta
        .Campo("id_cuenta").Valor = pId_Cuenta
        If Not .Buscar_Caja_Para_Invertir(lRegDiv("id_NEMOTECNICO").Value) Then
          fClass_Entidad.ErrMsg = .ErrMsg
          fClass_Entidad.Errnum = .Errnum
          GoTo ExitProcedure
        End If
        
        Set lCursorCaja = .Cursor
      End With
      
      lDsc_Origen_Mov_Caja_Cuenta = Replace(lDsc_Origen_Mov_Caja, "<<NEMOTECNICO>>", lRegDiv("NEMOTECNICO").Value) 'CAMBIA LOS NEMOTECNICOS
      
      lMonto_Dividendo = Round(lRegSaldos("cantidad").Value * lRegDiv("monto").Value, lCursorCaja(1)("Decimales").Value)
      
      Set lcCargo_Abono = New Class_Cargos_Abonos
      With lcCargo_Abono
        .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_DIVIDENDO
        .Campo("Id_Caja_Cuenta").Valor = lCursorCaja(1)("id_caja_cuenta").Value
        .Campo("Id_Cuenta").Valor = pId_Cuenta
        .Campo("Id_Moneda").Valor = lCursorCaja(1)("id_moneda").Value
        .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente '--Siempre se ingresa en estado "P"endiente
        .Campo("Dsc_Cargo_Abono").Valor = lDsc_Origen_Mov_Caja_Cuenta
        .Campo("Fecha_Movimiento").Valor = lRegDiv("Fecha_Corte").Value
        .Campo("Flg_Tipo_Cargo").Valor = gcTipoOperacion_Abono
        .Campo("Monto").Valor = lMonto_Dividendo
        .Campo("Retencion").Valor = DateDiff("d", lRegDiv("Fecha_Corte").Value, lRegDiv("Fecha").Value)
        
        .Campo("Flg_Tipo_Origen").Valor = "A"  '--Automatico
        
        If Not .Guardar Then
'          Call Fnt_Escribe_Grilla(pGrilla, _
'                             "E", _
'                             "Error Dividendo: Nemotécnico '" & pNemotecnico & "': " & lcCargo_Abono.ErrMsg)
          fClass_Entidad.ErrMsg = .ErrMsg
          fClass_Entidad.Errnum = .Errnum
          GoTo ExitProcedure
        End If
      End With
      
      Set lcCaja_Cuenta = Nothing
      Set lcSaldo_Activo = Nothing
    Next
  Next

  Ejecuta_x_Cuenta = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcSaldo_Activo = Nothing
  Set lcDividendo = Nothing
  Set lcMov_Caja_Origen = Nothing
  Set lcCaja_Cuenta = Nothing
  Set lcSaldo_Activo = Nothing
End Function

Public Function Deshacer_Enlaze_Cierre(pId_Cuenta, pFecha_Cierre) As Boolean
Dim lcCargo_Abono As Class_Cargos_Abonos
Dim lreg          As hFields
Dim lCursor       As hRecord
'--------------------------------------------------------------------------
Dim lFch_Corte_Cupon As Date
  
On Error GoTo ErrProcedure

  Deshacer_Enlaze_Cierre = False
  
  'Busca todos los cortes que caen ese dia.
  Set lcCargo_Abono = New Class_Cargos_Abonos
  With lcCargo_Abono
    .Campo("ID_CUENTA").Valor = pId_Cuenta
    .Campo("FECHA_MOVIMIENTO").Valor = pFecha_Cierre
    .Campo("COD_ORIGEN_MOV_CAJA").Valor = gcOrigen_Mov_Caja_DIVIDENDO
    .Campo("Flg_Tipo_Origen").Valor = "A" '--CARGOS/ABONOS AUTOMATICOS
    If Not .Buscar Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  End With

  Set lCursor = lcCargo_Abono.Cursor
   
   
'--------------------------------------------------------------------------------
'-- ESTA OMITIDO POR PROBLEMAS EN BUSCAR LOS DIVIDENDOS YA CURSADOS.
'--------------------------------------------------------------------------------
'--------------------------------------------------------------------------------
'-- SE DESCOMENTO, EN SECURITY SE ESTABN DUPLICANDO LOS DIVIDENDOS.
'-- NO SE HA ENCONTRADO ALGUNA RAZON PARA COMENTAR ESTA PARTE
'-- haf: 29/01/2008
'--------------------------------------------------------------------------------
  For Each lreg In lCursor
    Set lcCargo_Abono = New Class_Cargos_Abonos
    With lcCargo_Abono
      .Campo("ID_CARGO_ABONO").Valor = lreg("ID_CARGO_ABONO").Value
      .Campo("id_mov_caja").Valor = lreg("id_mov_caja").Value

      If Not .Anular Then
        fClass_Entidad.ErrMsg = .ErrMsg
        fClass_Entidad.Errnum = .Errnum
        GoTo ExitProcedure
      End If
    End With
    Set lcCargo_Abono = Nothing
  Next

  Deshacer_Enlaze_Cierre = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lCursor = Nothing
  Set lcCargo_Abono = Nothing
End Function
Public Function Busca_Dividendos_Pagados(ByVal pId_Cuenta As Double, _
                                            ByVal pFecha As Date, _
                                            ByRef pValor As Double) As Boolean
'**************************************************************************************
  With gDB
    .Parametros.Clear
    .Parametros.Add "PID_CUENTA", ePT_Numero, pId_Cuenta, ePD_Entrada
    .Parametros.Add "PFECHA", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "PVALOR", ePT_Numero, "", ePD_Salida
    .Procedimiento = "PKG_DIVIDENDOS.TOTAL_PAGADOS"
    If .EjecutaSP Then
      pValor = .Parametros("PVALOR").Valor
      'Fnt_Porcentaje_RV_RF = True
      Busca_Dividendos_Pagados = True
    Else
      pValor = 0
      'Fnt_Porcentaje_RV_RF = False
      Busca_Dividendos_Pagados = False
    End If
    .Parametros.Clear
  End With
'********************************************************************
End Function

Public Function Ejecuta_x_Cuenta_Opciones_Acciones(pId_Cuenta) As Boolean
Dim lcDividendo           As Class_VariacionCapital
Dim lcSaldo_Activo        As Class_Saldo_Activos
Dim lcMov_Caja_Origen     As Class_Mov_Caja_Origen
Dim lcCaja_Cuenta         As Class_Cajas_Cuenta
Dim lcCargo_Abono         As Class_Cargos_Abonos
Dim lCursorDiv            As hRecord
Dim lCursorCaja           As hRecord
Dim lRegDiv               As hFields
Dim lRegSaldos            As hFields
'-------------------------------------------------------------
Dim lFecha_Dividendo As Date
Dim lDsc_Origen_Mov_Caja As String
Dim lDsc_Origen_Mov_Caja_Cuenta As String
Dim lMonto_Dividendo As Double
  
  Ejecuta_x_Cuenta_Opciones_Acciones = False
  
  lFecha_Dividendo = Me.Campo("fecha").Valor
  
  'Busca todos los cortes que caen ese dia.
  Set lcDividendo = New Class_VariacionCapital
  With lcDividendo
    .Campo("Fecha_Corte").Valor = lFecha_Dividendo
    .Campo("Tipo_Variacion").Valor = "OPCIONES"
    If Not .Buscar() Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  
    Set lCursorDiv = .Cursor
  End With
  Set lcDividendo = Nothing

  lDsc_Origen_Mov_Caja = ""
  Set lcMov_Caja_Origen = New Class_Mov_Caja_Origen
  With lcMov_Caja_Origen
    .Campo("COD_ORIGEN_MOV_CAJA").Valor = gcOrigen_Mov_Caja_DIVIDENDO
    If Not .Buscar Then
      fClass_Entidad.Errnum = .Errnum
      fClass_Entidad.ErrMsg = .ErrMsg
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      fClass_Entidad.Errnum = Me.SubTipo_LOG
      fClass_Entidad.ErrMsg = "No existe el Origen del Movimiento de Caja (" & .Campo("COD_ORIGEN_MOV_CAJA").Valor & ")."
      GoTo ExitProcedure
    End If
    
    lDsc_Origen_Mov_Caja = "" & .Cursor(1)("dsc_glosa_abono").Value
  End With


  For Each lRegDiv In lCursorDiv
    Set lcSaldo_Activo = New Class_Saldo_Activos
    With lcSaldo_Activo
      If Not .Buscar_al_cierre(pFecha_Cierre:=lFecha_Dividendo _
                              , pId_Cuenta:=pId_Cuenta _
                              , pId_Nemotecnico:=lRegDiv("id_nemotecnico").Value) Then
        fClass_Entidad.ErrMsg = .ErrMsg
        fClass_Entidad.Errnum = .Errnum
        GoTo ExitProcedure
      End If
    End With
          
    For Each lRegSaldos In lcSaldo_Activo.Cursor
      Set lcCaja_Cuenta = New Class_Cajas_Cuenta
      With lcCaja_Cuenta
        .Campo("id_cuenta").Valor = pId_Cuenta
        If Not .Buscar_Caja_Para_Invertir(lRegDiv("id_NEMOTECNICO").Value) Then
          fClass_Entidad.ErrMsg = .ErrMsg
          fClass_Entidad.Errnum = .Errnum
          GoTo ExitProcedure
        End If
        
        Set lCursorCaja = .Cursor
      End With
      
      lDsc_Origen_Mov_Caja_Cuenta = Replace(lDsc_Origen_Mov_Caja, "<<NEMOTECNICO>>", lRegDiv("NEMOTECNICO").Value) 'CAMBIA LOS NEMOTECNICOS
      
      lMonto_Dividendo = Round(lRegSaldos("cantidad").Value * lRegDiv("monto").Value, lCursorCaja(1)("Decimales").Value)
      
      Set lcCargo_Abono = New Class_Cargos_Abonos
      With lcCargo_Abono
        .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_DIVIDENDO
        .Campo("Id_Caja_Cuenta").Valor = lCursorCaja(1)("id_caja_cuenta").Value
        .Campo("Id_Cuenta").Valor = pId_Cuenta
        .Campo("Id_Moneda").Valor = lCursorCaja(1)("id_moneda").Value
        .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente '--Siempre se ingresa en estado "P"endiente
        .Campo("Dsc_Cargo_Abono").Valor = lDsc_Origen_Mov_Caja_Cuenta
        .Campo("Fecha_Movimiento").Valor = lRegDiv("Fecha_Corte").Value
        .Campo("Flg_Tipo_Cargo").Valor = gcTipoOperacion_Abono
        .Campo("Monto").Valor = lMonto_Dividendo
        .Campo("Retencion").Valor = DateDiff("d", lRegDiv("Fecha_Corte").Value, lRegDiv("Fecha").Value)
        
        .Campo("Flg_Tipo_Origen").Valor = "A"  '--Automatico
        
        If Not .Guardar Then
'          Call Fnt_Escribe_Grilla(pGrilla, _
'                             "E", _
'                             "Error Dividendo: Nemotécnico '" & pNemotecnico & "': " & lcCargo_Abono.ErrMsg)
          fClass_Entidad.ErrMsg = .ErrMsg
          fClass_Entidad.Errnum = .Errnum
          GoTo ExitProcedure
        End If
      End With
      
      Set lcCaja_Cuenta = Nothing
      Set lcSaldo_Activo = Nothing
    Next
  Next

  Ejecuta_x_Cuenta_Opciones_Acciones = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcSaldo_Activo = Nothing
  Set lcDividendo = Nothing
  Set lcMov_Caja_Origen = Nothing
  Set lcCaja_Cuenta = Nothing
  Set lcSaldo_Activo = Nothing
End Function
Public Function Deshacer_Enlaze_Cierre_Opciones(pId_Cuenta, pFecha) As Boolean
Dim lcOperaciones As Class_Operaciones
Dim lreg As hFields

On Error GoTo ErrProcedure

  Deshacer_Enlaze_Cierre_Opciones = False

  '--------------------------------------------------------------------
  '-- ELIMINA LAS OPERACION DE CUSTODIA SUSCRIPCION
  '--------------------------------------------------------------------
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("ID_CUENTA").Valor = pId_Cuenta
    .Campo("COD_TIPO_OPERACION").Valor = "CUST_SUSOP"
    .Campo("FECHA_OPERACION").Valor = pFecha
    .Campo("FLG_TIPO_MOVIMIENTO").Valor = gcTipoOperacion_Ingreso
    If Not .Buscar_Desde_2 Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  
    For Each lreg In .Cursor
      If Not gRelogDB Is Nothing Then
        gRelogDB.AvanzaRelog
      End If
      
      If Not Fnt_Elimina_Operaciones(lreg) Then
        GoTo ExitProcedure
      End If
    Next
  End With
  Set lcOperaciones = Nothing
  Deshacer_Enlaze_Cierre_Opciones = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcOperaciones = Nothing
End Function
Private Function Fnt_Elimina_Operaciones(pReg As hFields) As Boolean
Dim lcOperacion As Class_Operaciones
  
  Fnt_Elimina_Operaciones = False
  
  '--------------------------------------------------------------------
  '-- ELIMINA LOS DETALLE DE OPERACION
  '--------------------------------------------------------------------
  Set lcOperacion = New Class_Operaciones
  With lcOperacion
    .Campo("ID_OPERACION").Valor = pReg("ID_OPERACION").Value
    If Not .Borrar Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
    
  End With
  Set lcOperacion = Nothing
  
  Fnt_Elimina_Operaciones = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcOperacion = Nothing
End Function
Public Function Genera_Opciones_Acciones(ByVal pId_Cuenta As Double, _
                                            ByVal pFecha As Date, _
                                            ByRef pHayOpcion As Double) As Boolean
'**************************************************************************************
  With gDB
    .Parametros.Clear
    .Parametros.Add "PID_CUENTA", ePT_Numero, pId_Cuenta, ePD_Entrada
    .Parametros.Add "PFecha_Cierre", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "PHayOpcion", ePT_Numero, pHayOpcion, ePD_Salida
    .Procedimiento = "PKG_DIVIDENDOS.Genera_Opciones_Acciones"
    If .EjecutaSP Then
      Genera_Opciones_Acciones = True
      pHayOpcion = .Parametros("pHayOpcion").Valor
    Else
      Genera_Opciones_Acciones = False
      pHayOpcion = 0
    End If
    .Parametros.Clear
  End With
'********************************************************************
ErrProcedure:
  If Not Err.Number = 0 Then
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
End Function

Public Function Caduca_Opciones_Acciones(ByVal pId_Cuenta As Double, _
                                            ByVal pFecha As Date, _
                                            ByRef pHayOpcion As Double) As Boolean
'**************************************************************************************
  With gDB
    .Parametros.Clear
    .Parametros.Add "PID_CUENTA", ePT_Numero, pId_Cuenta, ePD_Entrada
    .Parametros.Add "PFecha_Cierre", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "PHayOpcion", ePT_Numero, pHayOpcion, ePD_Salida
    .Procedimiento = "PKG_DIVIDENDOS$CADUCA_OPCIONES_ACCIONES"
    If .EjecutaSP Then
      Caduca_Opciones_Acciones = True
      pHayOpcion = NVL(.Parametros("pHayOpcion").Valor, 0)
    Else
      Caduca_Opciones_Acciones = False
      pHayOpcion = 0
    End If
    .Parametros.Clear
  End With
'********************************************************************
ErrProcedure:
  If Not Err.Number = 0 Then
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
End Function
