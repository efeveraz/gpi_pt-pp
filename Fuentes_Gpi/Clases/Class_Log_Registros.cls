VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Log_Registros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "PKG_LOG_REGISTRO"

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_LOG_REGISTRO"
    .AddCampo "ID_LOG_REGISTRO", ePT_Numero, ePD_Ambos
    .AddCampo "ID_LOG_SUBTIPO", ePT_Numero, ePD_Entrada
    .AddCampo "ID_LOG_PROCESO", ePT_Numero, ePD_Entrada
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_USUARIO", ePT_Numero, ePD_Entrada
    .AddCampo "COD_ESTADO", ePT_Caracter, ePD_Entrada
    .AddCampo "GLS_LOG_REGISTRO", ePT_Caracter, ePD_Entrada
    .AddCampo "ERR_LOG_REGISTRO", ePT_Caracter, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Guardar() As Boolean
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Buscar_Ultimos()
  With fClass_Entidad
    'Revisa la empresa si la ingreso, si no, se la agregamos
    'NOTA: SI POR ALGUN MOTIVO MAS ADELANTE SE NECESITA QUE LA EMPRESA SE PASE UN NULL
    '      ESTE CAMPO DEBE LLENARCE CON EL VALOR "", ASI NO SE CUMPLE LA CONDICION DEL NULL ;-)
    If IsNull(.Campo("ID_EMPRESA").Valor) Then
      .Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
    End If
    
    Buscar_Ultimos = .Buscar(cfPackage & ".BUSCAR_ULTIMOS")
    
  End With
End Function

Public Function Busca_Logs_Cierres(pId_Empresa _
                                 , pFecha_Desde _
                                 , pFecha_Hasta) As Boolean
Dim lClass_Entidad As Class_Entidad

  Busca_Logs_Cierres = False

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada, pId_Empresa
    .AddCampo "Fecha_Desde", ePT_Fecha, ePD_Entrada, pFecha_Desde
    .AddCampo "Fecha_Hasta", ePT_Fecha, ePD_Entrada, pFecha_Hasta
    
    Busca_Logs_Cierres = .Buscar(cfPackage & ".BUSCA_LOGS_CIERRES")
    
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = .Errnum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

