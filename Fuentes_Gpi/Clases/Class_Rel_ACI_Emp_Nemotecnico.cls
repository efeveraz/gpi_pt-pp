VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Rel_ACI_Emp_Nemotecnico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Rel_ACI_Emp_Nemotecnico"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 17
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .AddCampo "ID_NEMOTECNICO", ePT_Numero, ePD_Entrada
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_ARBOL_CLASE_INST", ePT_Numero, ePD_Entrada
    .AddCampo "ID_ACI_TIPO", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Busca_Id_Arbol(ByVal pId_Cuenta, ByVal pRelPadreHijo) As Boolean
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    
    Call .AddCampo("Id_Cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta)
    Call .AddCampo(Me.Campo("Id_Nemotecnico").Nombre, ePT_Numero, ePD_Entrada, Me.Campo("Id_Nemotecnico").Valor)
    Call .AddCampo("Rel_Padre_Hijo", ePT_Caracter, ePD_Entrada, pRelPadreHijo)
    Call .AddCampo("Id_Empresa", ePT_Numero, ePD_Entrada, Fnt_EmpresaActual)
    
    Busca_Id_Arbol = .Buscar(cfPackage & ".Buscar_Id_Arbol")
    
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.Errnum = lClass_Entidad.Errnum
    fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
  End With
  
  Set lClass_Entidad = Nothing

End Function

