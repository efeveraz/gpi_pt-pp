VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Usuarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Usuarios"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 7
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
Attribute Campo.VB_UserMemId = 0
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Private Sub Class_Terminate()
  Set fClass_Entidad = Nothing
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Usuario"
    .AddCampo "Id_Usuario", ePT_Numero, ePD_Ambos
    .AddCampo "Id_Asesor", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Empresa", ePT_Numero, ePD_Entrada
    .AddCampo "Dsc_Usuario", ePT_Caracter, ePD_Entrada
    .AddCampo "Password", ePT_Caracter, ePD_Entrada
    .AddCampo "Id_Tipo_Estado", ePT_Numero, ePD_Entrada
    .AddCampo "Cod_Estado", ePT_Caracter, ePD_Entrada
    .AddCampo "Email", ePT_Caracter, ePD_Entrada
    .AddCampo "FechaVigencia", ePT_Caracter, ePD_Entrada

  End With
End Sub

Public Function Buscar() As Boolean

  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
  
End Function

Public Sub Busca_Password()

  Call gDB.parametros.Clear
  Call gDB.parametros.Add("PUsuario", ePT_Caracter, Campo("dsc_usuario").Valor, ePD_Entrada)
  Call gDB.parametros.Add("Ppassword", ePT_Caracter, "", ePD_Salida)
  gDB.Procedimiento = cfPackage & ".Busca_Password"
  
  If gDB.EjecutaSP Then
    Campo("password").Valor = NVL(gDB.parametros("ppassword").Valor, "")
  Else
    MsgBox gDB.ErrMsg, vbCritical
    Campo("password").Valor = ""
  End If
  Call gDB.parametros.Clear
  
End Sub

Public Function Buscar_Asesor_Usuarios() As Boolean
  Buscar_Asesor_Usuarios = fClass_Entidad.Buscar(cfPackage & ".Buscar_Asesor_Usuario")
End Function

Public Function Buscar_Usuarios() As Boolean

  Buscar_Usuarios = fClass_Entidad.Buscar(cfPackage & ".Buscar_Usuarios")
  
End Function

Public Function Buscar_Usuarios_Empresas() As Boolean

  Buscar_Usuarios_Empresas = fClass_Entidad.Buscar(cfPackage & ".Buscar_Empresas")
  
End Function

Public Function Guardar()

  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
  
End Function

Public Function Anular() As Boolean
  
  Anular = True
  
  With fClass_Entidad
    gDB.parametros.Clear
    gDB.parametros.Add "p" & .Campo("id_usuario").Nombre, ePT_Numero, .Campo("id_usuario").Valor, ePD_Entrada
    gDB.Procedimiento = cfPackage & ".Anular"
    If Not gDB.EjecutaSP Then
      Anular = False
      .ErrMsg = gDB.ErrMsg
    End If
    gDB.parametros.Clear
  End With
  
End Function

Public Function Bloquear() As Boolean
  
  Bloquear = True
  
  With fClass_Entidad
    gDB.parametros.Clear
    gDB.parametros.Add "p" & .Campo("id_usuario").Nombre, ePT_Numero, .Campo("id_usuario").Valor, ePD_Entrada
    gDB.Procedimiento = cfPackage & ".Bloquear"
    If Not gDB.EjecutaSP Then
      Bloquear = False
      .ErrMsg = gDB.ErrMsg
    End If
    gDB.parametros.Clear
  End With
  
End Function

Public Function Reset() As Boolean
  
  Reset = True
  
  With fClass_Entidad
    gDB.parametros.Clear
    gDB.parametros.Add "p" & .Campo("id_usuario").Nombre, ePT_Numero, .Campo("id_usuario").Valor, ePD_Entrada
    gDB.Procedimiento = cfPackage & ".ResetClave"
    If Not gDB.EjecutaSP Then
      Reset = False
      .ErrMsg = gDB.ErrMsg
    End If
    gDB.parametros.Clear
  End With
  
End Function

Public Function GetIntentosClave() As Integer

    GetIntentosClave = 8
    'GetLargoMinimoClave = LargoClave(password)

    Call gDB.parametros.Clear
    Call gDB.parametros.Add("pIdNegocio", ePT_Numero, 0, ePD_Entrada)
    Call gDB.parametros.Add("pCodigo", ePT_Caracter, "N_INTENTO_CLAVE", ePD_Entrada)
    Call gDB.parametros.Add("pResultado", ePT_Caracter, "", ePD_Salida)
    gDB.Procedimiento = "Sis_ParametrosConsultaValor"
  
    If gDB.EjecutaSP Then
        GetIntentosClave = gDB.parametros("pResultado").Valor
    End If
    Call gDB.parametros.Clear

End Function

Public Function GetFechaVigencia() As Integer

    GetFechaVigencia = 8
    'GetLargoMinimoClave = LargoClave(password)

    Call gDB.parametros.Clear
    Call gDB.parametros.Add("pIdNegocio", ePT_Numero, 0, ePD_Entrada)
    Call gDB.parametros.Add("pCodigo", ePT_Caracter, "N_DIAS_VENCI", ePD_Entrada)
    Call gDB.parametros.Add("pResultado", ePT_Caracter, "", ePD_Salida)
    gDB.Procedimiento = "Sis_ParametrosConsultaValor"
  
    If gDB.EjecutaSP Then
        GetFechaVigencia = gDB.parametros("pResultado").Valor
    End If
    Call gDB.parametros.Clear

End Function

Public Function Fnt_ActualizarContraseña(ByVal contraseña As String) As Boolean

    Fnt_ActualizarContraseña = True
    Dim lResultado As String
    'GetLargoMinimoClave = LargoClave(password)

    Call gDB.parametros.Clear
    Call gDB.parametros.Add("pIdUsuario", ePT_Numero, gID_Usuario, ePD_Entrada)
    Call gDB.parametros.Add("pClave", ePT_Caracter, contraseña, ePD_Entrada)
    Call gDB.parametros.Add("pResultado", ePT_Caracter, "", ePD_Salida)
    gDB.Procedimiento = "PKG_USUARIOS$ActualizaClave"
  
    If gDB.EjecutaSP Then
      lResultado = gDB.parametros("pResultado").Valor
      If lResultado <> "OK" Then
        Fnt_ActualizarContraseña = False
      End If
    End If
    Call gDB.parametros.Clear

End Function

Public Function Fnt_GuardaClaveInicial(ByVal lUsuario As Long, ByVal lPassword As String) As Boolean
Dim lResultado As String
  
  Fnt_GuardaClaveInicial = True
  
  Call gDB.parametros.Clear
  Call gDB.parametros.Add("PID_USUARIO", ePT_Numero, lUsuario, ePD_Entrada)
  Call gDB.parametros.Add("PPASSWORD", ePT_Caracter, lPassword, ePD_Entrada)
  Call gDB.parametros.Add("pResultado", ePT_Caracter, "", ePD_Salida)
  gDB.Procedimiento = "PKG_USUARIOS$GuardaClaveInicial"
  
  If gDB.EjecutaSP Then
    lResultado = gDB.parametros("pResultado").Valor
    If lResultado <> "OK" Then
      Fnt_GuardaClaveInicial = False
      'gDB.ErrMsg = lResultado
    End If
  Else
    Fnt_GuardaClaveInicial = False
    'gDB.ErrMsg = lResultado
  End If
  Call gDB.parametros.Clear
''
End Function
