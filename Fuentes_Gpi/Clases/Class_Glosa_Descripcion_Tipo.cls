VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Glosa_Descripcion_Tipo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Glosa_Descripcion_Tipo"
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 35
End Function

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar(Optional pEnVista As Boolean = False) As Boolean
Dim lParam  As DLL_COMUN.Parametro
Dim lProcedimiento As String
  
  If pEnVista Then
    lProcedimiento = "BuscarView"
  Else
    lProcedimiento = "Buscar"
  End If
  lProcedimiento = cfPackage & "$" & lProcedimiento
  
  Buscar = fClass_Entidad.Buscar(lProcedimiento)

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function


Public Sub LimpiaParam()
  Id_Log_Proceso = Null

  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_GLOSA_DESCRIPCION_TIPO"
    .AddCampo "ID_GLOSA_DESCRIPCION_TIPO", ePT_Numero, ePD_Ambos
    .AddCampo "COD_GLOSA_DESCRIPCION_TIPO", ePT_Caracter, ePD_Entrada
    .AddCampo "DSC_GLOSA_DESCRIPCION_TIPO", ePT_Caracter, ePD_Entrada
  End With
End Sub

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function
