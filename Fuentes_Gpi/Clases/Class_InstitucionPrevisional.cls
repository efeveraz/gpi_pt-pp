VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_InstitucionPrevisional"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "Pkg_Institucion_Previsional"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 10
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "id_institucion"
    .AddCampo "id_institucion", ePT_Numero, ePD_Ambos
    .AddCampo "id_grupo_institucion", ePT_Numero, ePD_Entrada
    .AddCampo "cod_institucion", ePT_Caracter, ePD_Entrada
    .AddCampo "dsc_institucion", ePT_Caracter, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar() As Boolean

  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
  
End Function

Public Function Guardar() As Boolean
Dim lResult As Boolean
Dim lMensaje As String
  
  With fClass_Entidad
    If .Campo(.CampoPKey).Valor = cNewEntidad Then
      lMensaje = "Se ha insertado"
    Else
      lMensaje = "Se ha actualizado"
    End If
  End With

  lMensaje = lMensaje & " el C�digo " _
            & fClass_Entidad.Campo("dsc_institucion").Valor

  lResult = fClass_Entidad.Guardar(cfPackage & ".Guardar")

  If Not lResult Then
    lMensaje = "Problema en la grabaci�n del Instituci�n Previsional (" _
              & fClass_Entidad.Campo("dsc_institucion").Valor
  End If

  Call Fnt_AgregarLog(Me.SubTipo_LOG _
                    , IIf(lResult, cEstado_Log_Mensaje, cEstado_Log_Error) _
                    , lMensaje _
                    , pId_Log_Proceso:=Id_Log_Proceso)

  Guardar = lResult

End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function




