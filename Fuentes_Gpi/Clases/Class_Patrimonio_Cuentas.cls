VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Patrimonio_Cuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "PKG_PATRIMONIO_CUENTAS"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLog_Subtipo.eLS_Patrimonio_Cuentas
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function ErrNum() As Double
  ErrNum = fClass_Entidad.ErrNum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_PATRIMONIO_CUENTA"
    .AddCampo "ID_PATRIMONIO_CUENTA", ePT_Numero, ePD_Ambos
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "FECHA_CIERRE", ePT_Fecha, ePD_Entrada
    .AddCampo "ID_MONEDA_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "SALDO_CAJA_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "SALDO_ACTIVO_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "PATRIMONIO_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MONEDA_EMPRESA", ePT_Numero, ePD_Entrada
    .AddCampo "SALDO_CAJA_MON_EMPRESA", ePT_Numero, ePD_Entrada
    .AddCampo "SALDO_ACTIVO_MON_EMPRESA", ePT_Numero, ePD_Entrada
    .AddCampo "PATRIMONIO_MON_EMPRESA", ePT_Numero, ePD_Entrada
    .AddCampo "VALOR_CUOTA_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "TOTAL_CUOTAS_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "RENTABILIDAD_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "APORTE_RETIROS_MON_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_CIERRE", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_X_COBRAR_MON_CTA", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_X_PAGAR_MON_CTA", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_X_COBRAR_MON_EMPRESA", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_X_PAGAR_MON_EMPRESA", ePT_Numero, ePD_Entrada
    .AddCampo "COMI_DEVENG_MON_CTA", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar("PKG_PATRIMONIO.Buscar")
End Function

Public Function Buscar_Patrimonio() As Boolean
  Buscar_Patrimonio = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Buscar_Patrimonio_Ultimo_Cierre() As Boolean
  Buscar_Patrimonio_Ultimo_Cierre = fClass_Entidad.Buscar(cfPackage & ".Buscar_Patrimonio_Ultimo_Cierre")
End Function

Public Function Guardar()
  
  'Valores por defecto
  If IsNull(Campo("RENTABILIDAD_MON_CUENTA").Valor) Then
    Campo("RENTABILIDAD_MON_CUENTA").Valor = 0
  End If
  If IsNull(Campo("APORTE_RETIROS_MON_CUENTA").Valor) Then
    Campo("APORTE_RETIROS_MON_CUENTA").Valor = 0
  End If
  
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Borrar_Historia_Cuenta() As Boolean
Dim lClass_Entidad As Class_Entidad

  Borrar_Historia_Cuenta = False

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "Fecha_Cierre", ePT_Fecha, ePD_Entrada
    
    .Campo("ID_CUENTA").Valor = fClass_Entidad.Campo("ID_CUENTA").Valor
    .Campo("Fecha_Cierre").Valor = fClass_Entidad.Campo("Fecha_Cierre").Valor
    
    Borrar_Historia_Cuenta = .Borrar(cfPackage & ".BORRAR_HISTORIA_CUENTA")
    fClass_Entidad.ErrNum = .ErrNum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Buscar_Entre(pFecha_Desde, pFecha_Hasta) As Boolean
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    Call .AddCampo("ID_CUENTA", ePT_Numero, ePD_Entrada, fClass_Entidad.Campo("ID_CUENTA").Valor)
    Call .AddCampo("Fecha_Desde", ePT_Fecha, ePD_Entrada, pFecha_Desde)
    Call .AddCampo("Fecha_Hasta", ePT_Fecha, ePD_Entrada, pFecha_Hasta)
    
    Buscar_Entre = .Buscar(cfPackage & ".Buscar_Entre")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.ErrNum = .ErrNum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Guardar_VCuotas() As Boolean
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    .AddCampo "ID_PATRIMONIO_CUENTA", ePT_Numero, ePD_Entrada, fClass_Entidad.Campo("ID_PATRIMONIO_CUENTA").Valor
    .AddCampo "VALOR_CUOTA_MON_CUENTA", ePT_Numero, ePD_Entrada, fClass_Entidad.Campo("VALOR_CUOTA_MON_CUENTA").Valor
    .AddCampo "TOTAL_CUOTAS_MON_CUENTA", ePT_Numero, ePD_Entrada, fClass_Entidad.Campo("TOTAL_CUOTAS_MON_CUENTA").Valor
    .AddCampo "RENTABILIDAD_MON_CUENTA", ePT_Numero, ePD_Entrada, fClass_Entidad.Campo("RENTABILIDAD_MON_CUENTA").Valor
    .AddCampo "APORTE_RETIROS_MON_CUENTA", ePT_Numero, ePD_Entrada, fClass_Entidad.Campo("APORTE_RETIROS_MON_CUENTA").Valor
    
    Guardar_VCuotas = .Guardar(cfPackage & ".Guardar_VCuotas")
    fClass_Entidad.ErrNum = .ErrNum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Busca_Saltos_Cuotas(pFecha_Desde _
                                  , pFecha_Hasta _
                                  , Optional pId_Perfil) As Boolean
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    Call .AddCampo("Fecha_Desde", ePT_Fecha, ePD_Entrada, pFecha_Desde)
    Call .AddCampo("Fecha_Hasta", ePT_Fecha, ePD_Entrada, pFecha_Hasta)
    
    Call .AddCampo("ID_CUENTA", ePT_Numero, ePD_Entrada, fClass_Entidad.Campo("ID_CUENTA").Valor)
    Call .AddCampo("ID_EMPRESA", ePT_Numero, ePD_Entrada, Fnt_EmpresaActual)
    If Not IsMissing(pId_Perfil) Then
      Call .AddCampo("ID_PERFIL", ePT_Numero, ePD_Entrada, pId_Perfil)
    End If
    
    Busca_Saltos_Cuotas = .Buscar(cfPackage & ".Busca_Saltos_Cuotas")
    Set fClass_Entidad.Cursor = .Cursor
    fClass_Entidad.ErrNum = .ErrNum
    fClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function

