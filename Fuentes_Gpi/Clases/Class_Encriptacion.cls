VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Encriptacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Function LargoClave() As Integer
  
  Reset = True
  
  With fClass_Entidad
    gDB.Parametros.Clear
    gDB.Parametros.Add "p" & .Campo("id_usuario").Nombre, ePT_Numero, .Campo("id_usuario").valor, ePD_Entrada
    gDB.Procedimiento = cfPackage & ".ResetClave"
    If Not gDB.EjecutaSP Then
      Reset = False
      .ErrMsg = gDB.ErrMsg
    End If
    gDB.Parametros.Clear
  End With
  LargoClave = 0
End Function
Public Function Fnt_GeneraClave() As String
Dim LargoMinimo As Integer
Dim Password()
'Dim Password() As string = GetLargoMinimoClave()
Dim Caracteres As String
Dim CaracteresEspeciales As String
Dim tipoCaracter As Integer
Dim passwordValidacion As String
Dim indiceValidacion As Integer
tipoCaracter = 0

Const LetraMayuscula = 1
Const LetraMinuscula = 2
Const CaracterEspecial = 3
Const Numero = 4


   On Error GoTo Fnt_GeneraClave_Error

    LargoMinimo = GetLargoMinimoClave()
    ReDim Password(LargoMinimo)
    Caracteres = "abcdefghijklmnopqrstuvwxyz"
    CaracteresEspeciales = "!#$%&+-.<=>?@_|~"
    
    indiceValidacion = LargoMinimo - 4
    
    For i = 0 To LargoMinimo
    tipoCaracter = 0
'
        If i >= indiceValidacion Then
            'passwordValidacion = passwordValidacion & Password(i)
            If (Not ContieneLetraMayuscula(passwordValidacion)) Then
                tipoCaracter = LetraMayuscula
            ElseIf (Not ContieneLetraMinuscula(passwordValidacion)) Then
                tipoCaracter = LetraMinuscula
            ElseIf (Not ContieneNumeros(passwordValidacion)) Then
                tipoCaracter = Numero
            ElseIf (Not ContieneCaracteresEspeciales(passwordValidacion)) Then
                tipoCaracter = CaracterEspecial
            End If
        Else
            tipoCaracter = Int((4 * Rnd) + 1)
        End If
'
        indiceCaracter = Int(((Len(Caracteres)) * Rnd) + 1) 'Rnd.Next(0, Caracteres.Length)
        letra = Mid(Caracteres, indiceCaracter, 1) 'Caracteres.ElementAt(indiceCaracter).ToString()
'
        If tipoCaracter = LetraMayuscula Then
            Password(i) = UCase(letra) 'letra.ToUpper()
        ElseIf tipoCaracter = LetraMinuscula Then
            Password(i) = LCase(letra) 'letra.ToLower()
        ElseIf tipoCaracter = CaracterEspecial Then
            indiceCaracter = Int(((Len(CaracteresEspeciales)) * Rnd) + 1) 'Rnd.Next(0, CaracteresEspeciales.Length)
            Password(i) = Mid(CaracteresEspeciales, indiceCaracter, 1) 'CaracteresEspeciales.ElementAt(indiceCaracter).ToString()
        ElseIf tipoCaracter = Numero Then
            Password(i) = Int((9 * Rnd) + 1) 'Rnd.Next(0, 10).ToString()
        End If
        passwordValidacion = passwordValidacion & Password(i)
    Next

    Fnt_GeneraClave = passwordValidacion

   On Error GoTo 0
   Exit Function

Fnt_GeneraClave_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure Fnt_GeneraClave of M�dulo de clase Class_Encriptacion"

End Function


 Public Function ValidarPassword(Usuario As String, Password As String) As String
        Dim reglasNoCumplidas As String
        Dim LargoMinimo As Integer
        
        
        Dim numeroClavesRecordar As Integer
        
        LargoMinimo = GetLargoMinimoClave()
        numeroClavesRecordar = GetNumeroClavesRecordar()

        If Len(Password) < LargoMinimo Then
            reglasNoCumplidas = reglasNoCumplidas & "- Debe tener al menos " & LargoMinimo & " car�cteres" & vbNewLine
        End If

        If (Not ContieneNumeros(Password)) Then
            reglasNoCumplidas = reglasNoCumplidas & "- Debe contener al menos un d�gito" & vbNewLine
        End If
'
        If (Not ContieneLetraMinuscula(Password)) Then
            reglasNoCumplidas = reglasNoCumplidas & "- Debe contener al menos un car�cter en min�scula" & vbNewLine
        End If
'
        If (Not ContieneLetraMayuscula(Password)) Then
            reglasNoCumplidas = reglasNoCumplidas & "- Debe contener al menos un car�cter en may�scula" & vbNewLine
        End If
'
        If (Not ContieneCaracteresEspeciales(Password)) Then
            reglasNoCumplidas = "- Debe contener al menos un car�cter especial ( ! # $ % & + - . < = > ? @ _ | ~ )" & vbNewLine
        End If

        'If (numeroClavesRecordar >= 1 And EsClaveRepetida(Usuario)) Then
'            If numeroClavesRecordar = 1 Then
'                reglasNoCumplidas = reglasNoCumplidas & "- NO debe ser igual la �ltima contrase�a" '+ Char(13)
'            Else
'                reglasNoCumplidas = reglasNoCumplidas & "- NO debe ser igual a una de las {0} �ltimas contrase�as" '+ Char(13)
'            End If
        'End If

        ValidarPassword = reglasNoCumplidas
    End Function
    
Public Function GetLargoMinimoClave() As Integer
    GetLargoMinimoClave = 8
    'GetLargoMinimoClave = LargoClave(password)

    Call gDB.Parametros.Clear
    Call gDB.Parametros.Add("pIdNegocio", ePT_Numero, 0, ePD_Entrada)
    Call gDB.Parametros.Add("pCodigo", ePT_Caracter, "N_LARGO_CLAVES", ePD_Entrada)
    Call gDB.Parametros.Add("pResultado", ePT_Caracter, "", ePD_Salida)
    gDB.Procedimiento = "Sis_ParametrosConsultaValor"
  
    If gDB.EjecutaSP Then
        GetLargoMinimoClave = gDB.Parametros("pResultado").valor
    End If
    Call gDB.Parametros.Clear

End Function
Public Function GetNumeroClavesRecordar() As Integer
    GetNumeroClavesRecordar = 8
    'GetLargoMinimoClave = LargoClave(password)

    Call gDB.Parametros.Clear
    Call gDB.Parametros.Add("pIdNegocio", ePT_Numero, 0, ePD_Entrada)
    Call gDB.Parametros.Add("pCodigo", ePT_Caracter, "N_CLAVES", ePD_Entrada)
    Call gDB.Parametros.Add("pResultado", ePT_Caracter, "", ePD_Salida)
    gDB.Procedimiento = "Sis_ParametrosConsultaValor"
  
    If gDB.EjecutaSP Then
        GetNumeroClavesRecordar = gDB.Parametros("pResultado").valor
    End If
    Call gDB.Parametros.Clear

End Function

Public Function ContieneNumeros(Password As String) As Boolean
ContieneNumeros = False
Dim caracter As String
Dim i As Integer

For i = 1 To Len(Password)
    caracter = Mid(Password, i, 1)
    If IsNumeric(caracter) Then
        ContieneNumeros = True
        Exit Function
    Else
        ContieneNumeros = False
    End If
Next i

End Function

Public Function ContieneLetraMinuscula(Password As String) As Boolean
ContieneLetraMinuscula = False
Dim caracter As String
Dim i As Integer

For i = 1 To Len(Password)
    caracter = Mid(Password, i, 1)
    If Not IsNumeric(caracter) Then
        If caracter = LCase(caracter) Then
            ContieneLetraMinuscula = True
            Exit Function
        Else
            ContieneLetraMinuscula = False
        End If
    End If
Next i
End Function

Public Function ContieneLetraMayuscula(Password As String) As Boolean
ContieneLetraMayuscula = False
Dim caracter As String
Dim i As Integer

For i = 1 To Len(Password)
    caracter = Mid(Password, i, 1)
    If Not IsNumeric(caracter) Then
        If caracter = UCase(caracter) Then
            ContieneLetraMayuscula = True
            Exit Function
        Else
            ContieneLetraMayuscula = False
        End If
    End If
Next i
End Function

Public Function ContieneCaracteresEspeciales(Password As String) As Boolean
ContieneCaracteresEspeciales = False
'! # $ % & + - . < = > ? @ _ | ~
Dim caracter As String
Dim i As Integer

For i = 1 To Len(Password)
    caracter = Mid(Password, i, 1)
    If Not IsNumeric(caracter) Then
        Select Case caracter
        Case "!"
            ContieneCaracteresEspeciales = True
        Case "#"
            ContieneCaracteresEspeciales = True
        Case "$"
            ContieneCaracteresEspeciales = True
        Case "%"
            ContieneCaracteresEspeciales = True
        Case "&"
            ContieneCaracteresEspeciales = True
        Case "+"
            ContieneCaracteresEspeciales = True
        Case "-"
            ContieneCaracteresEspeciales = True
        Case "."
            ContieneCaracteresEspeciales = True
        Case "<"
            ContieneCaracteresEspeciales = True
        Case "="
            ContieneCaracteresEspeciales = True
        Case ">"
            ContieneCaracteresEspeciales = True
        Case "?"
            ContieneCaracteresEspeciales = True
        Case "@"
            ContieneCaracteresEspeciales = True
        Case "_"
            ContieneCaracteresEspeciales = True
        Case "|"
            ContieneCaracteresEspeciales = True
        Case "~"
            ContieneCaracteresEspeciales = True
        Case Else
            ContieneCaracteresEspeciales = False
        End Select
    End If
Next i
End Function
