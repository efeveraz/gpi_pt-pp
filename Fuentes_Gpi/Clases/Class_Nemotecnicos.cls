VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Nemotecnicos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Nemotecnicos"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLS_Nemotecnico
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Guardar()

  If NVL(fClass_Entidad.Campo("ID_NEMOTECNICO").Valor, cNewEntidad) = cNewEntidad Then
    If IsNull(fClass_Entidad.Campo("ID_USUARIO_INSERT").Valor) Then
      fClass_Entidad.Campo("ID_USUARIO_INSERT").Valor = gID_Usuario
    End If
  Else
    If IsNull(fClass_Entidad.Campo("ID_USUARIO_UPDATE").Valor) Then
      fClass_Entidad.Campo("ID_USUARIO_UPDATE").Valor = gID_Usuario
    End If
  End If

  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_NEMOTECNICO"
    
    .AddCampo "ID_NEMOTECNICO", ePT_Numero, ePD_Ambos
    .AddCampo "COD_INSTRUMENTO", ePT_Caracter, ePD_Entrada
    .AddCampo "NEMOTECNICO", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_MERCADO_TRANSACCION", ePT_Numero, ePD_Entrada
    .AddCampo "ID_EMISOR_ESPECIFICO", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MONEDA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_TIPO_ESTADO", ePT_Numero, ePD_Entrada
    .AddCampo "COD_ESTADO", ePT_Caracter, ePD_Entrada
    .AddCampo "DSC_NEMOTECNICO", ePT_Caracter, ePD_Entrada
    .AddCampo "TASA_EMISION", ePT_Numero, ePD_Entrada
    .AddCampo "TIPO_TASA", ePT_Caracter, ePD_Entrada
    .AddCampo "PERIODICIDAD", ePT_Caracter, ePD_Entrada
    .AddCampo "FECHA_VENCIMIENTO", ePT_Fecha, ePD_Entrada
    .AddCampo "CORTE_MINIMO_PAPEL", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_EMISION", ePT_Numero, ePD_Entrada
    .AddCampo "LIQUIDEZ", ePT_Caracter, ePD_Entrada
    .AddCampo "BASE", ePT_Numero, ePD_Entrada
    .AddCampo "COD_PAIS", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_EMISOR_ESPECIFICO_ORIGEN", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MONEDA_TRANSACCION", ePT_Numero, ePD_Entrada
    .AddCampo "ID_SUBFAMILIA", ePT_Numero, ePD_Entrada
    .AddCampo "FLG_FUNGIBLE", ePT_Caracter, ePD_Entrada
    .AddCampo "FECHA_EMISION", ePT_Fecha, ePD_Entrada
    .AddCampo "FLG_TIPO_CUOTA_INGRESO", ePT_Caracter, ePD_Entrada
    .AddCampo "FLG_TIPO_CUOTA_EGRESO", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_USUARIO_INSERT", ePT_Numero, ePD_Entrada
    .AddCampo "ID_USUARIO_UPDATE", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function BuscarPerfilRiesgo(pId_Cuenta As String)
Dim lTabla As String

  lTabla = ".Buscar_Nemo_Perfil"

  With fClass_Entidad
    Call .AddCampo("Id_Cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta)

    BuscarPerfilRiesgo = .Buscar(cfPackage & lTabla)
  End With
End Function

Public Function Buscar()
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Buscar_Nemo_Subfamilia(pFecha As Date)
  With fClass_Entidad
    Call .AddCampo("fecha_cierre", ePT_Fecha, ePD_Entrada, pFecha)
    
    Buscar_Nemo_Subfamilia = .Buscar(cfPackage & ".Buscar_Nemo_Subfamilia")
  End With
End Function

Public Function BuscarView(Optional pCod_Producto = Null)
  With fClass_Entidad
    If Not IsNull(pCod_Producto) Then
      Call .AddCampo("Cod_Producto", ePT_Caracter, ePD_Entrada, pCod_Producto)
    End If
    
    BuscarView = .Buscar(cfPackage & ".BuscarView")
  End With
End Function

Public Function Cupones_por_Nemotecnicos(pNemotecnico As String)
    With fClass_Entidad
        .Campo("nemotecnico").Valor = pNemotecnico
        'Call .AddCampo("Nemotecnico", ePT_Caracter, ePD_Entrada, pNemotecnico)
        Cupones_por_Nemotecnicos = .Buscar("PKG_CUPONES_NEMOTECNICO$Buscar")
    End With
End Function


Public Function BuscarGrilla(Optional pCod_Producto = Null)

  With fClass_Entidad
    If Not IsNull(pCod_Producto) Then
      Call .AddCampo("Cod_Producto", ePT_Caracter, ePD_Entrada, pCod_Producto)
    End If
    
    BuscarGrilla = .Buscar(cfPackage & ".BuscarGrilla")
  End With
End Function


Public Function BuscarAyuda(Optional pCod_Instrumento = Null, _
                            Optional pDesc_Emisor_Especifico = Null, _
                            Optional pCodOperacion = Null, _
                            Optional pId_Cuenta = Null, _
                            Optional pFecha_Movimiento = Null, _
                            Optional pNemotecnico = Null, _
                            Optional pCod_Producto = Null, _
                            Optional pVigente = Null, _
                            Optional pCod_Subfamilia = Null, _
                            Optional pCod_Isin = Null)
'Esta wa' de procedimiento tiene que meterle mano para que quede en el standart
Dim lClass_Entidad As Class_Entidad
   
  
  BuscarAyuda = False

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    If Not IsNull(pCod_Instrumento) Then
      .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada, pCod_Instrumento
    End If
      
    If Not IsNull(pDesc_Emisor_Especifico) Then
      .AddCampo "Desc_Emisor_Especifico", ePT_Caracter, ePD_Entrada, pDesc_Emisor_Especifico
    End If
    
    If Not IsNull(pId_Cuenta) Then
      .AddCampo "Id_Cuenta", ePT_Caracter, ePD_Entrada, pId_Cuenta
    End If
      
    If Not IsNull(pFecha_Movimiento) Then
      .AddCampo "Fecha_Movimiento", ePT_Fecha, ePD_Entrada, pFecha_Movimiento
    End If
      
    If Not IsNull(pNemotecnico) Then
      .AddCampo "nemotecnico", ePT_Caracter, ePD_Entrada, pNemotecnico
    End If
      
    If Not IsNull(pCod_Producto) Then
      .AddCampo "cod_producto", ePT_Caracter, ePD_Entrada, pCod_Producto
    End If
    
    If Not IsNull(pVigente) Then
      .AddCampo "Vigente", ePT_Caracter, ePD_Entrada, pVigente
    End If
    
    If pCodOperacion = gcTipoOperacion_Ingreso Then
      If Not IsNull(pCod_Subfamilia) Then
         .AddCampo "Cod_SubFamilia", ePT_Caracter, ePD_Entrada, pCod_Subfamilia
      End If
    End If
    
    If Not IsNull(pCod_Isin) Then
      .AddCampo "cod_isin", ePT_Caracter, ePD_Entrada, pCod_Isin
    End If
    
    If RescataEstadoProductoCierre("FUNGIR_RF") = 0 Then
      If Not .Buscar(cfPackage & IIf(pCodOperacion = gcTipoOperacion_Ingreso, ".BuscarAyuda", ".BuscarAyuda2")) Then
         fClass_Entidad.Errnum = lClass_Entidad.Errnum
         fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
         GoTo ExitProcedure
      End If
    Else
      If RescataCuentaSinFungibilidad(pId_Cuenta) = 0 Then
         If Not .Buscar(cfPackage & IIf(pCodOperacion = gcTipoOperacion_Ingreso, ".BuscarAyuda", ".BuscarAyuda2_II")) Then
            fClass_Entidad.Errnum = lClass_Entidad.Errnum
            fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
            GoTo ExitProcedure
         End If
      Else
         If Not .Buscar(cfPackage & IIf(pCodOperacion = gcTipoOperacion_Ingreso, ".BuscarAyuda", ".BuscarAyuda2")) Then
            fClass_Entidad.Errnum = lClass_Entidad.Errnum
            fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
            GoTo ExitProcedure
         End If
      End If
    End If
        
    Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
  End With
  Set lClass_Entidad = Nothing
  
  BuscarAyuda = True
  
ExitProcedure:

End Function

Private Function RescataEstadoProductoCierre(lProductoCierre As String) As Integer
Dim lReg As hFields

  RescataEstadoProductoCierre = 0
  gDB.Parametros.Clear
  gDB.Procedimiento = "PKG_PROCESO_CIERRE$Verifica_Estado"
  Call gDB.Parametros.Add("pcursor", ePT_Cursor, "", eParam_Direc.ePD_Ambos)
  Call gDB.Parametros.Add("pProductoCierre", ePT_Caracter, lProductoCierre, eParam_Direc.ePD_Entrada)
 
  If gDB.EjecutaSP Then
     For Each lReg In gDB.Parametros("pCursor").Valor
         RescataEstadoProductoCierre = lReg("Estado").Value
     Next
  End If
  gDB.Parametros.Clear
End Function

Public Function RescataCuentaSinFungibilidad(lId_Cuenta) As Boolean
Dim lReg As hFields

  RescataCuentaSinFungibilidad = False
  gDB.Parametros.Clear
  gDB.Procedimiento = "PKG_BUSCA_CTA_SIN_FUNGIR_RF"
  Call gDB.Parametros.Add("pcursor", ePT_Cursor, "", eParam_Direc.ePD_Ambos)
  Call gDB.Parametros.Add("pId_Cuenta", ePT_Numero, lId_Cuenta, eParam_Direc.ePD_Entrada)
 
  If gDB.EjecutaSP Then
     For Each lReg In gDB.Parametros("pCursor").Valor
        If lReg("EXISTE").Value = "S" Then
           RescataCuentaSinFungibilidad = True
        End If
     Next
  End If
  gDB.Parametros.Clear
End Function

Public Function BuscarVigentes(Optional pCod_Producto = Null, _
                               Optional pCod_Instrumento = Null)
Dim lClass_Entidad As New Class_Entidad

  BuscarVigentes = False

  With lClass_Entidad
    If Not IsNull(pCod_Producto) Then
      .AddCampo "Cod_Producto", ePT_Caracter, ePD_Entrada, pCod_Producto
    End If
    
    If Not IsNull(pCod_Instrumento) Then
      .AddCampo "Cod_Instrumento", ePT_Caracter, ePD_Entrada, pCod_Instrumento
    End If
    
    If .Buscar(cfPackage & ".BuscarVigentes") Then
      BuscarVigentes = True
      Set fClass_Entidad.Cursor = lClass_Entidad.Cursor
    Else
      fClass_Entidad.Errnum = lClass_Entidad.Errnum
      fClass_Entidad.ErrMsg = lClass_Entidad.ErrMsg
    End If
  End With
  Set lClass_Entidad = Nothing
End Function

Public Function Buscar_Nemotecnico(ByVal pCod_Producto, _
                                   Optional pMostrar_Msg As Boolean = True) As Boolean
  Buscar_Nemotecnico = False

  gDB.Parametros.Clear
  With fClass_Entidad
    gDB.Parametros.Add "p" & .Campo("cod_Instrumento").Nombre, ePT_Caracter, .Campo("cod_Instrumento").Valor, ePD_Entrada
    gDB.Parametros.Add "p" & .Campo("Nemotecnico").Nombre, ePT_Caracter, .Campo("Nemotecnico").Valor, ePD_Entrada
    gDB.Parametros.Add "Pcod_Producto", ePT_Caracter, pCod_Producto, ePD_Entrada
    gDB.Parametros.Add "p" & .Campo("id_Nemotecnico").Nombre, ePT_Numero, "", ePD_Salida
    gDB.Procedimiento = cfPackage & ".Buscar_Nemotecnico"
    If gDB.EjecutaSP Then
      .Campo("id_Nemotecnico").Valor = gDB.Parametros("p" & .Campo("id_Nemotecnico").Nombre).Valor
      Buscar_Nemotecnico = True
    Else
      .Campo("id_Nemotecnico").Valor = ""
      .ErrMsg = gDB.ErrMsg
      .Errnum = gDB.Errnum
      If pMostrar_Msg Then MsgBox .Errnum & vbCr & .ErrMsg, vbCritical
    End If
  End With
  gDB.Parametros.Clear
  
End Function

Public Function Fnt_Generacion_Nemo_SVS(ByVal PId_Moneda_Deposito, _
                                        ByVal pId_Moneda_Pago) As String
  With Me
    gDB.Parametros.Clear
    gDB.Parametros.Add "p" & .Campo("id_Emisor_Especifico").Nombre, ePT_Numero, .Campo("id_Emisor_Especifico").Valor, ePD_Entrada
    gDB.Parametros.Add "p" & .Campo("Fecha_Vencimiento").Nombre, ePT_Fecha, .Campo("Fecha_Vencimiento").Valor, ePD_Entrada
    gDB.Parametros.Add "PId_Moneda_Deposito", ePT_Numero, PId_Moneda_Deposito, ePD_Entrada
    gDB.Parametros.Add "PId_Moneda_Pago", ePT_Numero, pId_Moneda_Pago, ePD_Entrada
    gDB.Parametros.Add "p" & .Campo("Nemotecnico").Nombre, ePT_Caracter, "", ePD_Salida
    gDB.Parametros.Add "PMgs", ePT_Caracter, "", ePD_Salida
    gDB.Procedimiento = cfPackage & ".Generacion_Nemotecnico_SVS"
    If gDB.EjecutaSP Then
      Fnt_Generacion_Nemo_SVS = NVL(gDB.Parametros("p" & .Campo("Nemotecnico").Nombre).Valor, "")
      If Not gDB.Parametros("PMgs").Valor = "" Then
        MsgBox gDB.Parametros("PMgs").Valor, vbExclamation
      End If
    Else
      Fnt_Generacion_Nemo_SVS = ""
      MsgBox gDB.Errnum & vbCr & gDB.ErrMsg, vbCritical
    End If
  End With
  gDB.Parametros.Clear
  
End Function

Public Function Fnt_Clonar_Nemotecnico(ByVal pId_Nemo_origen, _
                                       ByVal pNemo_Nuevo, _
                                       ByRef pId_Nemo_nuevo) As Boolean
Dim lCursor As hRecord
Dim lReg As hFields
Dim pId_Nemo As String

  Fnt_Clonar_Nemotecnico = True
  
  With Me
    Rem Verifica que el nemotecnico que se vaya a ingresar no est� en el sistema.
    .Campo("nemotecnico").Valor = pNemo_Nuevo
    If .Buscar_Nemotecnico("", False) Then
      If Not .Campo("id_nemotecnico").Valor = "0" Then
        Rem El nemotecnico ya esta en el sistema, no se debe insertar de nuevo.
        pId_Nemo_nuevo = .Campo("id_nemotecnico").Valor 'pId_Nemo
        GoTo ErrProcedure
      End If
    Else
      Fnt_Clonar_Nemotecnico = False
      GoTo ErrProcedure
    End If
  
    Rem Busca los datos del nemotecnico a clonar
    .LimpiaParam
    .Campo("id_nemotecnico").Valor = pId_Nemo_origen
    If .Buscar Then
      Set lCursor = .Cursor
    Else
      Fnt_Clonar_Nemotecnico = False
      GoTo ErrProcedure
    End If
  End With
  
  Rem Se ingresan los datos del nuevo nemotecnico: se cambia el id_nemotecnico y el nemotecnico
  For Each lReg In lCursor
    With Me
      .LimpiaParam
      .Campo("id_Nemotecnico").Valor = cNewEntidad
      .Campo("cod_Instrumento").Valor = lReg("cod_Instrumento").Value
      .Campo("id_subfamilia").Valor = lReg("id_subfamilia").Value
      .Campo("nemotecnico").Valor = pNemo_Nuevo
      .Campo("id_Mercado_Transaccion").Valor = lReg("id_Mercado_Transaccion").Value
      .Campo("id_Emisor_Especifico").Valor = lReg("id_Emisor_Especifico").Value
      .Campo("id_Emisor_Especifico_Origen").Valor = lReg("id_Emisor_Especifico_Origen").Value
      .Campo("id_Moneda").Valor = lReg("id_Moneda").Value
      .Campo("id_Moneda_transaccion").Valor = lReg("id_Moneda_transaccion").Value
      .Campo("id_Tipo_Estado").Valor = lReg("id_Tipo_Estado").Value
      .Campo("cod_Estado").Valor = lReg("cod_Estado").Value
      .Campo("dsc_Nemotecnico").Valor = lReg("dsc_Nemotecnico").Value
      .Campo("tasa_Emision").Valor = lReg("tasa_Emision").Value
      .Campo("tipo_Tasa").Valor = lReg("tipo_Tasa").Value
      .Campo("periodicidad").Valor = lReg("periodicidad").Value
      .Campo("fecha_Vencimiento").Valor = lReg("fecha_Vencimiento").Value
      .Campo("corte_Minimo_Papel").Valor = lReg("corte_Minimo_Papel").Value
      .Campo("monto_Emision").Valor = lReg("monto_Emision").Value
      .Campo("liquidez").Valor = lReg("Dias_liquidez").Value
      .Campo("base").Valor = lReg("base").Value
      .Campo("cod_Pais").Valor = lReg("cod_Pais").Value
      .Campo("flg_Fungible").Valor = lReg("flg_Fungible").Value
    
      If Not .Guardar() Then
        'Si existe un problema el proceso devuelve false
        Fnt_Clonar_Nemotecnico = False
        GoTo ErrProcedure
      End If
  
      pId_Nemo_nuevo = .Campo("Id_Nemotecnico").Valor
    End With
  Next
  
ErrProcedure:
  Exit Function
  
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Buscar_Solo_Nemo(Optional pCod_Producto) As Boolean
  With fClass_Entidad
    If Not IsEmpty(pCod_Producto) Then
      Call .AddCampo("Cod_Producto", ePT_Caracter, ePD_Entrada, pCod_Producto)
    End If
    
    Buscar_Solo_Nemo = .Buscar(cfPackage & ".Buscar_Solo_Nemo")
    
    If Not IsEmpty(pCod_Producto) Then
      Call .DelCampo("Cod_Producto")
    End If
  End With
End Function

Public Function Llena_Combo() As Boolean
  Llena_Combo = fClass_Entidad.Buscar(cfPackage & ".Llena_Combo")
End Function

Public Function Buscar_Vencidos(Optional pCod_Producto) As Boolean
  If Not IsEmpty(pCod_Producto) Then
    Call fClass_Entidad.AddCampo("Cod_Producto", ePT_Caracter, ePD_Entrada, pCod_Producto)
  End If

  Buscar_Vencidos = fClass_Entidad.Buscar(cfPackage & ".Buscar_Vencidos")

  If Not IsEmpty(pCod_Producto) Then
    Call fClass_Entidad.DelCampo("Cod_Producto")
  End If
End Function

 
 

Public Function BuscarDisponibles(Optional Tipo_f) As Integer
  
  If Tipo_f = 2 Then
    With fClass_Entidad
      Call .AddCampo("Id_empresa", ePT_Caracter, ePD_Entrada, Fnt_EmpresaActual)
      BuscarDisponibles = .Buscar(cfPackage & ".BuscarDisponibles_II")
    End With
   Else
    With fClass_Entidad
      Call .AddCampo("Id_empresa", ePT_Caracter, ePD_Entrada, Fnt_EmpresaActual)
      BuscarDisponibles = .Buscar(cfPackage & ".BuscarDisponibles")
    End With
   End If
  
End Function

Public Function GenerarCodAchs() As Boolean
  GenerarCodAchs = fClass_Entidad.Buscar(cfPackage & ".GENERAR_CODIGO_ACHS")
End Function

