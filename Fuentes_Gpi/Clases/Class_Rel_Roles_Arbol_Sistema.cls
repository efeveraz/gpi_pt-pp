VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Rel_Roles_Arbol_Sistema"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Rel_Roles_Arbol_Sistema"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLog_Subtipo.eLS_Rel_Roles_Arbol_Sistema
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .AddCampo "Id_Rol", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Arbol_Sistema", ePT_Numero, ePD_Entrada
    .AddCampo "Flg_Tipo_Permiso", ePT_Caracter, ePD_Entrada
  End With
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Buscar_Permiso(pCod_Arbol_Sistema _
                             , pId_Usuario) As Boolean
  With fClass_Entidad
    Call .AddCampo("COD_ARBOL_SISTEMA", ePT_Caracter, ePD_Entrada, pCod_Arbol_Sistema)
    Call .AddCampo("ID_USUARIO", ePT_Numero, ePD_Entrada, pId_Usuario)
                             
    Buscar_Permiso = .Buscar(cfPackage & ".BUSCAR_PERMISO")
    
    Call .DelCampo("COD_ARBOL_SISTEMA")
    Call .DelCampo("ID_USUARIO")
  End With
End Function

