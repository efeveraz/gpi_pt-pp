VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Comisiones_Cuenta_Fija"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Comisiones_Cuenta_Fija"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLog_Subtipo.eLS_Comisiones_Cuentas_Fija
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Guardar()
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_COMISION_CUENTA_FIJA"
    .AddCampo "ID_COMISION_CUENTA_FIJA", ePT_Numero, ePD_Ambos
    .AddCampo "ID_CUENTA", ePT_Numero, ePD_Entrada
    .AddCampo "ID_MONEDA", ePT_Numero, ePD_Entrada
    .AddCampo "COD_INSTRUMENTO", ePT_Caracter, ePD_Entrada
    .AddCampo "FECHA_INI", ePT_Fecha, ePD_Entrada
    .AddCampo "FECHA_TER", ePT_Fecha, ePD_Entrada
    .AddCampo "MONTO_COMISION_HONORARIO", ePT_Numero, ePD_Entrada
    .AddCampo "MONTO_COMISION_ASESORIA", ePT_Numero, ePD_Entrada
    .AddCampo "PERIODICIDAD", ePT_Numero, ePD_Entrada
  End With
End Sub

Public Function Buscar()
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Validar(ByRef Presultado As Integer, ByRef pMensaje As String) As Boolean
On Error GoTo ErrProcedure

  Validar = False

  With gDB
    .Parametros.Clear
    .Procedimiento = cfPackage & ".VALIDAR"
    .Parametros.Add "p" & Campo("id_comision_cuenta_fija").Nombre, ePT_Numero, Campo("id_comision_cuenta_fija").Valor, ePD_Entrada
    .Parametros.Add "p" & Campo("id_cuenta").Nombre, ePT_Numero, Campo("id_cuenta").Valor, ePD_Entrada
    .Parametros.Add "p" & Campo("Fecha_Ini").Nombre, ePT_Fecha, Campo("Fecha_Ini").Valor, ePD_Entrada
    .Parametros.Add "p" & Campo("Fecha_Ter").Nombre, ePT_Fecha, Campo("Fecha_Ter").Valor, ePD_Entrada
    .Parametros.Add "p" & Campo("cod_instrumento").Nombre, ePT_Caracter, Campo("cod_instrumento").Valor, ePD_Entrada
    
    .Parametros.Add "Presultado", ePT_Numero, "", ePD_Salida
    .Parametros.Add "Pmensaje", ePT_Caracter, "", ePD_Salida
    
    If Not .EjecutaSP Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    Else
      Presultado = .Parametros("Presultado").Valor
      pMensaje = .Parametros("Pmensaje").Valor
    End If
  End With

  Validar = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al validar la comisi�n.", Err.Description)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Buscar_Valida(pfecha) As Boolean
  fClass_Entidad.AddCampo "Fecha", ePT_Fecha, ePD_Entrada, pfecha
  
  Buscar_Valida = fClass_Entidad.Buscar(cfPackage & ".Buscar_Valida")
End Function


