VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Sucursales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "PKG_SUCURSALES"

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "COD_SUCURSAL"
    .AddCampo "COD_SUCURSAL", ePT_Caracter, ePD_Entrada
    .AddCampo "DSC_SUCURSAL", ePT_Caracter, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Guardar() As Boolean
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function
