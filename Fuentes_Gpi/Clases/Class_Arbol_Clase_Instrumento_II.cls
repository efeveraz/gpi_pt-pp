VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Arbol_Clase_Instrumento_II"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "PKG_ARBOL_CLASE_INSTRUMENTO"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLog_Subtipo.eLS_Arbol_Clase_Instrumento
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "ID_ARBOL_CLASE_INST"
    .AddCampo "ID_ARBOL_CLASE_INST", ePT_Numero, ePD_Ambos
    .AddCampo "ID_EMPRESA", ePT_Numero, ePD_Entrada
    .AddCampo "DSC_ARBOL_CLASE_INST", ePT_Caracter, ePD_Entrada
    .AddCampo "ID_PADRE_ARBOL_CLASE_INST", ePT_Numero, ePD_Entrada
    .AddCampo "ORDEN", ePT_Numero, ePD_Entrada
    .AddCampo "ID_ACI_TIPO", ePT_Numero, ePD_Entrada
     
    
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar_II")
End Function

Public Function Buscar_Consulta() As Boolean
  Buscar_Consulta = fClass_Entidad.Buscar(cfPackage & ".Buscar_Consulta_II")
End Function

Public Function Guardar() As Boolean
  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar_II")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar_II")
End Function





