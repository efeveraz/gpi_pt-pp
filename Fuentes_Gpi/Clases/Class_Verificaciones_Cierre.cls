VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Verificaciones_Cierre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Const cfPackage = "Pkg_Proceso_PreCierre"

Public Function Busca_Ultima_FechaCierre() As Date

  With gDB
    .Parametros.Clear
    .Parametros.Add "Pid_empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    .Parametros.Add "Pfecha", ePT_Fecha, "", ePD_Salida
    .Procedimiento = cfPackage & ".Busca_Ultima_FechaCierre"
    If .EjecutaSP Then
      Busca_Ultima_FechaCierre = .Parametros("Pfecha").Valor
    Else
      Busca_Ultima_FechaCierre = DateSerial(2007, 1, 1)
      MsgBox .Errnum & .ErrMsg, vbCritical
    End If
    .Parametros.Clear
  End With

End Function

Public Function Verificacion_Cierre(ByVal pFecha As Date, _
                                    pGrilla As VSFlexGrid) As Boolean
Dim lMensaje As String

  Verificacion_Cierre = False

  Call Fnt_Escribe_Grilla(pGrilla, "N", "Comienzo de Verificaciones para el Cierre.")
  
  Rem Verifica que el proceso de cierre anterior exista y que no exista el cierre para el día indicado
  Call Fnt_Verifica_Cierre_Anterior(pFecha, pGrilla)
  Call Fnt_Verifica_Cierre_Actual(pFecha, pGrilla)
  
  Rem Verifica que esten las paridades y los precios de nemotecnicos para el dia indicado
  Call Fnt_Verifica_Paridad(pFecha, pGrilla)
  Call Fnt_Verifica_Precio_Nemotecnico(pFecha, pGrilla)
  Call Fnt_Verifica_Nemotecnicos_SIN_Arbol(pFecha, pGrilla)
  Call Fnt_Verifica_Nemotecnicos_Con_Fecha_Vcto_Incorrectas(pFecha, pGrilla)
  
  Rem Verifica que existan datos en la tabla Bloqueo si la empresa lo permite
  Call Fnt_Verifica_Bloqueo(pGrilla)
  
  Call Fnt_Escribe_Grilla(pGrilla, "N", "Fin de Verificaciones para el Cierre.")
  
  If Not Fnt_Verificacion_Error(pGrilla) Then
'    If Fnt_Verificacion_Advertencia(pGrilla) Then
'      lMensaje = "Las Verificaciones al Proceso de Cierre tienen Advertencias." & vbCr & _
'                 "Puede continuar con el proceso de cierre, pero puede generar errores de corcondancia de datos." & vbCr & vbCr & _
'                 "En el Visor de Sucesos se muestran las advertencias en el Proceso de Cierre." & vbCr & _
'                 "Por Favor, lea todas las advertencias antes de realizar el Cierre de día."
'      MsgBox lMensaje, vbExclamation, Frm_Proceso_Cierre.Caption
'    End If
    Verificacion_Cierre = True
  Else
    lMensaje = "Las Verificaciones al Proceso de Cierre tienen Errores, no se puede continuar el proceso de cierre." & vbCr & vbCr & _
               "En el Visor de Sucesos se muestran los errores en el Proceso de Cierre."
    MsgBox lMensaje, vbCritical, Frm_Proceso_Cierre.Caption
  End If
  
End Function

Public Function Verificacion_Cierre_Pendiente(ByVal pFecha As Date, _
                                    pGrilla As VSFlexGrid) As Boolean
Dim lMensaje As String

  Verificacion_Cierre_Pendiente = False

  Call Fnt_Escribe_Grilla(pGrilla, "N", "Comienzo de Verificaciones para eliminar Cierres Pendientes.")
  
  Rem Verifica que el proceso de cierre anterior exista y que no exista el cierre para el día indicado
  Call Fnt_Verifica_Cierre_Anterior_Pendiente(pFecha, pGrilla)
  Call Fnt_Verifica_Cierre_Actual_Pendiente(pFecha, pGrilla)
  
  Rem Verifica que esten las paridades y los precios de nemotecnicos para el dia indicado
  Call Fnt_Verifica_Paridad(pFecha, pGrilla)
  Call Fnt_Verifica_Precio_Nemotecnico(pFecha, pGrilla)
  
  Call Fnt_Escribe_Grilla(pGrilla, "N", "Fin de Verificaciones para el Cierre.")
  
  If Not Fnt_Verificacion_Error(pGrilla) Then
    If Fnt_Verificacion_Advertencia(pGrilla) Then
      lMensaje = "Las Verificaciones al Proceso de Cierre tienen Advertencias." & vbCr & _
                 "Puede continuar con el proceso de cierre, pero puede generar errores de corcondancia de datos." & vbCr & vbCr & _
                 "En el Visor de Sucesos se muestran las advertencias en el Proceso de Cierre." & vbCr & _
                 "Por Favor, lea todas las advertencias antes de realizar el Cierre de día."
      MsgBox lMensaje, vbExclamation, Frm_Proceso_Cierre.Caption
    End If
    Verificacion_Cierre_Pendiente = True
  Else
    lMensaje = "Las Verificaciones al Proceso de Cierre tienen Errores, no se puede continuar el proceso de cierre." & vbCr & vbCr & _
               "En el Visor de Sucesos se muestran los errores en el Proceso de Cierre."
    MsgBox lMensaje, vbCritical, Frm_Proceso_Cierre.Caption
  End If
  
End Function

Private Function Fnt_Verifica_Cierre_Anterior(ByVal pFecha, pGrilla As VSFlexGrid) As Boolean
  
  Fnt_Verifica_Cierre_Anterior = True
  With gDB
    .Parametros.Clear
    .Parametros.Add "PId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    .Parametros.Add "Pfecha", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "PTipo_Error", ePT_Caracter, "", ePD_Salida
    .Parametros.Add "Pmsgresult", ePT_Caracter, "", ePD_Salida
    .Procedimiento = cfPackage & ".Verifica_Cierre_Anterior"
    If .EjecutaSP Then
      If .Parametros("PTipo_Error").Valor = "E" Then
        Fnt_Verifica_Cierre_Anterior = False
      End If
      Call Fnt_Escribe_Grilla(pGrilla, _
                              .Parametros("PTipo_Error").Valor, _
                              "Verificación Cierre Anterior: " & .Parametros("Pmsgresult").Valor)
    Else
      Fnt_Verifica_Cierre_Anterior = False
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "Verificación Cierre Anterior: " & .ErrMsg)
    End If
    .Parametros.Clear
  End With
  
End Function

Private Function Fnt_Verifica_Cierre_Anterior_Pendiente(ByVal pFecha, pGrilla As VSFlexGrid) As Boolean
  
  Fnt_Verifica_Cierre_Anterior_Pendiente = True
  With gDB
    .Parametros.Clear
    .Parametros.Add "PId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    .Parametros.Add "Pfecha", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "PTipo_Error", ePT_Caracter, "", ePD_Salida
    .Parametros.Add "Pmsgresult", ePT_Caracter, "", ePD_Salida
    .Procedimiento = cfPackage & ".Verifica_Cierre_Anterior_Pendiente"
    If .EjecutaSP Then
      If .Parametros("PTipo_Error").Valor = "E" Then
        Fnt_Verifica_Cierre_Anterior_Pendiente = False
      End If
      Call Fnt_Escribe_Grilla(pGrilla, _
                              .Parametros("PTipo_Error").Valor, _
                              "Verificación Cierre Anterior: " & .Parametros("Pmsgresult").Valor)
    Else
      Fnt_Verifica_Cierre_Anterior_Pendiente = False
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "Verificación Cierre Anterior: " & .ErrMsg)
    End If
    .Parametros.Clear
  End With
  
End Function

Private Function Fnt_Verifica_Cierre_Actual(ByVal pFecha, pGrilla As VSFlexGrid) As Boolean
  
  Fnt_Verifica_Cierre_Actual = True
  With gDB
    .Parametros.Clear
    .Parametros.Add "PId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    .Parametros.Add "Pfecha", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "PTipo_Error", ePT_Caracter, "", ePD_Salida
    .Parametros.Add "Pmsgresult", ePT_Caracter, "", ePD_Salida
    .Procedimiento = cfPackage & ".Verifica_Cierre_Actual"
    If .EjecutaSP Then
      If .Parametros("PTipo_Error").Valor = "E" Then
        Fnt_Verifica_Cierre_Actual = False
      End If
      Call Fnt_Escribe_Grilla(pGrilla, _
                              .Parametros("PTipo_Error").Valor, _
                              "Verificación Cierre Actual: " & .Parametros("Pmsgresult").Valor)
    Else
      Fnt_Verifica_Cierre_Actual = False
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "Verificación Cierre Actual: " & .ErrMsg)
    End If
    .Parametros.Clear
  End With
  
End Function

Private Function Fnt_Verifica_Cierre_Actual_Pendiente(ByVal pFecha, pGrilla As VSFlexGrid) As Boolean
  
  Fnt_Verifica_Cierre_Actual_Pendiente = True
  With gDB
    .Parametros.Clear
    .Parametros.Add "PId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    .Parametros.Add "Pfecha", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "PTipo_Error", ePT_Caracter, "", ePD_Salida
    .Parametros.Add "Pmsgresult", ePT_Caracter, "", ePD_Salida
    .Procedimiento = cfPackage & ".Verifica_Cierre_Actual_Pendiente"
    If .EjecutaSP Then
      If .Parametros("PTipo_Error").Valor = "E" Then
        Fnt_Verifica_Cierre_Actual_Pendiente = False
      End If
      Call Fnt_Escribe_Grilla(pGrilla, _
                              .Parametros("PTipo_Error").Valor, _
                              "Verificación Cierre Actual: " & .Parametros("Pmsgresult").Valor)
    Else
      Fnt_Verifica_Cierre_Actual_Pendiente = False
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "Verificación Cierre Actual: " & .ErrMsg)
    End If
    .Parametros.Clear
  End With
  
End Function

Public Function Fnt_Verifica_Paridad(ByVal pFecha As Date, pGrilla As VSFlexGrid) As Boolean
  
  Fnt_Verifica_Paridad = True
  With gDB
    .Parametros.Clear
    .Parametros.Add "Pfecha", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "PTipo_Error", ePT_Caracter, "", ePD_Salida
    .Parametros.Add "Pmsgresult", ePT_Caracter, "", ePD_Salida
    .Procedimiento = cfPackage & ".Verifica_Paridad"
    If .EjecutaSP Then
      If .Parametros("PTipo_Error").Valor = "A" Then
        Fnt_Verifica_Paridad = False
      End If
      Call Fnt_Escribe_Grilla(pGrilla, _
                              .Parametros("PTipo_Error").Valor, _
                              "Verificación Paridad Divisas: " & .Parametros("Pmsgresult").Valor)
    Else
      Fnt_Verifica_Paridad = False
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "A", _
                              "Verificación Paridad Divisas: " & .ErrMsg)
    End If
    .Parametros.Clear
  End With
  
End Function

Public Function Fnt_Verifica_Precio_Nemotecnico(ByVal pFecha As Date, pGrilla As VSFlexGrid) As Boolean
Dim lMsg As String
Dim lTipoError As String
Dim lReg  As hCollection.hFields
Dim lCursor As hRecord
  Fnt_Verifica_Precio_Nemotecnico = True
'  With gDB
'    .Parametros.Clear
'    .Parametros.Add "Pfecha", ePT_Fecha, pFecha, ePD_Entrada
'    .Parametros.Add "PTipo_Error", ePT_Caracter, "", ePD_Salida
'    .Parametros.Add "Pmsgresult", ePT_Caracter, "", ePD_Salida
'    .Procedimiento = cfPackage & ".Verifica_Precio_Nemotecnico"
'    If .EjecutaSP Then
'      If .Parametros("PTipo_Error").Valor = "A" Then
'        Fnt_Verifica_Precio_Nemotecnico = False
'      End If
'      Call Fnt_Escribe_Grilla(pGrilla, _
'                              .Parametros("PTipo_Error").Valor, _
'                              "Verificación Precios Nemotécnicos: " & .Parametros("Pmsgresult").Valor)
'    Else
'      Fnt_Verifica_Precio_Nemotecnico = False
'      Call Fnt_Escribe_Grilla(pGrilla, _
'                              "A", _
'                              "Verificación Precios Nemotécnicos: " & .ErrMsg)
'    End If
'    .Parametros.Clear
'  End With
  
    With gDB
      .Parametros.Clear
      .Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
      .Parametros.Add "Pfecha_cierre", ePT_Fecha, pFecha, ePD_Entrada
      .Parametros.Add "PID_EMPRESA", ePT_Numero, gId_Empresa, ePD_Entrada
      .Procedimiento = cfPackage & ".Verifica_Precio_Cierre_Producto"
      If .EjecutaSP Then
        Set lCursor = gDB.Parametros("Pcursor").Valor
        For Each lReg In lCursor
           If To_Number(lReg("Nemos").Value) = 0 Then
              lMsg = "OK"
              lTipoError = "N"
           Else
              lTipoError = "A"
              lMsg = "Faltan " & lReg("nemos").Value & " precios para el día de hoy"
           End If
           Call Fnt_Escribe_Grilla(pGrilla, _
                                lTipoError, _
                                "Verificación Precios " & lReg("dsc_producto").Value & " : " & lMsg, _
                                pCodProducto:=lReg("cod_producto").Value, _
                                pCantidadNemo:=To_Number(lReg("Nemos").Value))
        Next
      Else
        Fnt_Verifica_Precio_Nemotecnico = False
        Call Fnt_Escribe_Grilla(pGrilla, _
                                "A", _
                                "Verificación Precios Nemotécnicos: " & .ErrMsg)
      End If
      .Parametros.Clear
    End With
  
End Function

Private Function Fnt_Verificacion_Error(pGrilla As VSFlexGrid) As Boolean
Dim lTipo_Cierre As String
Dim lFila As Long

  Fnt_Verificacion_Error = False
  With pGrilla
    For lFila = 1 To .Rows - 1
      lTipo_Cierre = GetCell(pGrilla, lFila, "colum_pk")
      If lTipo_Cierre = "E" Then
        Fnt_Verificacion_Error = True
        Exit For
      End If
    Next lFila
  End With
End Function

Private Function Fnt_Verificacion_Advertencia(pGrilla As VSFlexGrid) As Boolean
Dim lTipo_Cierre As String
Dim lFila As Long

  Fnt_Verificacion_Advertencia = False
  With pGrilla
    For lFila = 1 To .Rows - 1
      lTipo_Cierre = GetCell(pGrilla, lFila, "colum_pk")
      If lTipo_Cierre = "A" Then
        Fnt_Verificacion_Advertencia = True
        Exit For
      End If
    Next lFila
  End With
End Function

Public Function Fnt_Verifica_Nemotecnicos_SIN_Arbol(ByVal pFecha, pGrilla As VSFlexGrid) As Boolean
    Dim lcNemoDisponibles As Class_Nemotecnicos
    Fnt_Verifica_Nemotecnicos_SIN_Arbol = True
  
    Rem Carga los nemosdisponibles
    Set lcNemoDisponibles = New Class_Nemotecnicos
    With lcNemoDisponibles
        If .BuscarDisponibles Then
            If .Cursor.Count > 0 Then
                Fnt_Verifica_Nemotecnicos_SIN_Arbol = False
                Call Fnt_Escribe_Grilla(pGrilla, _
                                            "E", _
                                            "Verificación Nemotécnicos SIN Clasificar en Arbol :" & "Hay Nemotécnicos SIN CLASIFICAR EN ARBOL DE ACTIVOS")
            Else
                Call Fnt_Escribe_Grilla(pGrilla, _
                                            "N", _
                                            "Verificación Nemotécnicos SIN Clasificar en Arbol : OK")
            End If
        Else
            Fnt_Verifica_Nemotecnicos_SIN_Arbol = False
            Call Fnt_Escribe_Grilla(pGrilla, _
                                        "E", _
                                        "Verificación Nemotécnicos SIN Clasificar en Arbol: " & .ErrMsg)
        
        End If
    End With
    Set lcNemoDisponibles = Nothing
End Function

Public Function Fnt_Verifica_Bloqueo(pGrilla As VSFlexGrid) As Boolean
Dim lcEmpresa As Class_Empresas
Dim lcBloqueo_Nivel As Class_Bloqueo_Niveles
Dim lFlg_Bloqueo_Cta As String
  
  Fnt_Verifica_Bloqueo = True
  
  Set lcEmpresa = New Class_Empresas
  With lcEmpresa
    .Campo("id_empresa").Valor = gId_Empresa
    If .Buscar Then
      If .Cursor.Count > 0 Then
        lFlg_Bloqueo_Cta = .Cursor(1)("FLG_BLOQUEO_CUENTAS").Value
      Else
        Fnt_Verifica_Bloqueo = False
        Call Fnt_Escribe_Grilla(pGrilla, _
                                "E", _
                                "Verificación Bloqueos de Cuentas: No Existen Empresas definidas en el sistema.")
      End If
    Else
      Fnt_Verifica_Bloqueo = False
      Call Fnt_Escribe_Grilla(pGrilla, _
                              "E", _
                              "Verificación Bloqueos de Cuentas: " & .ErrMsg)
    End If
  End With
  
  If lFlg_Bloqueo_Cta = gcFlg_SI Then
    Set lcBloqueo_Nivel = New Class_Bloqueo_Niveles
    With lcBloqueo_Nivel
      If .Buscar Then
        If .Cursor.Count > 0 Then
          Call Fnt_Escribe_Grilla(pGrilla, _
                                  "N", _
                                  "Verificación Bloqueos de Cuentas: El Cierre considerará los Bloqueos de Cuentas.")
        Else
          Fnt_Verifica_Bloqueo = False
          Call Fnt_Escribe_Grilla(pGrilla, _
                                  "E", _
                                  "Verificación Bloqueos de Cuentas: No hay datos en la tablas de Bloqueos de Cuentas.")
        End If
      Else
        Fnt_Verifica_Bloqueo = False
        Call Fnt_Escribe_Grilla(pGrilla, _
                                "E", _
                                "Verificación Bloqueos de Cuentas: " & .ErrMsg)
      End If
    End With
  ElseIf lFlg_Bloqueo_Cta = gcFlg_NO Then
    Call Fnt_Escribe_Grilla(pGrilla, _
                            "N", _
                            "Verificación Bloqueos de Cuentas: El Cierre NO considerará los Bloqueos de Cuentas.")
  End If
  
End Function
Public Function Fnt_Verifica_Nemotecnicos_Con_Fecha_Vcto_Incorrectas(ByVal pFecha, pGrilla As VSFlexGrid) As Boolean
Dim lClass_Entidad As Class_Entidad
Fnt_Verifica_Nemotecnicos_Con_Fecha_Vcto_Incorrectas = True

Set lClass_Entidad = New Class_Entidad
With lClass_Entidad
    Call .AddCampo("Fecha_Cierre", ePT_Fecha, ePD_Entrada, pFecha)
    
    If .Buscar("PKG_SALDO_ACTIVOS.buscar_fecha_vcto_malas") Then
        If .Cursor.Count > 0 Then
            Fnt_Verifica_Nemotecnicos_Con_Fecha_Vcto_Incorrectas = False
            Call Fnt_Escribe_Grilla(pGrilla, _
                                        "E", _
                                        "Nemotécnico con Fecha Vencimiento Incorrecta :" & .Cursor(1)("Nemotecnico").Value)
        Else
            Call Fnt_Escribe_Grilla(pGrilla, _
                              "N", _
                              "Nemotécnico con Fecha Vencimiento Incorrecta : OK")
        End If
    End If
    lClass_Entidad.Errnum = .Errnum
    lClass_Entidad.ErrMsg = .ErrMsg
  End With
  Set lClass_Entidad = Nothing
End Function


