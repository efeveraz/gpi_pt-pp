VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Temp_Tabla"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad
Const cfPackage = "PKG_TEMP_TABLA"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 41
End Function
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .AddCampo "SESION", ePT_Numero, ePD_Entrada
    .AddCampo "VALOR_NUMERICO", ePT_Numero, ePD_Entrada
    .AddCampo "VALOR_DECIMAL", ePT_Numero, ePD_Entrada
  End With
End Sub

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Function Buscar() As Boolean
  Buscar = fClass_Entidad.Buscar(cfPackage & ".Buscar")
End Function

Public Function Borrar() As Boolean
  Borrar = fClass_Entidad.Borrar(cfPackage & ".Borrar")
End Function

Public Function Guardar() As Boolean

  If IsNull(fClass_Entidad.Campo("sesion").Valor) Then
    'Si la sesion se ha dejado en nula se agrega por defaul la que corresponde
    fClass_Entidad.Campo("sesion").Valor = gDB.SessionID
  End If

  Guardar = fClass_Entidad.Guardar(cfPackage & ".Guardar")
End Function

Public Function Limpiar()
  If IsNull(fClass_Entidad.Campo("sesion").Valor) Then
    'Si la sesion se ha dejado en nula se agrega por defaul la que corresponde
    fClass_Entidad.Campo("sesion").Valor = gDB.SessionID
  End If
  
  Limpiar = Me.Borrar
End Function
