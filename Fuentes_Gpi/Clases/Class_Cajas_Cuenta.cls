VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Cajas_Cuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Cajas_Cuenta"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = 3
End Function
'---------------------------------------------------------
Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  Call LimpiaParam
End Sub

Public Sub LimpiaParam()
  With fClass_Entidad
    .LimpiaParam
    .CampoPKey = "Id_Caja_Cuenta"
    .AddCampo "Id_Caja_Cuenta", ePT_Numero, ePD_Ambos
    .AddCampo "Id_Cuenta", ePT_Numero, ePD_Entrada
    .AddCampo "Id_Moneda", ePT_Numero, ePD_Entrada
    .AddCampo "Cod_Mercado", ePT_Caracter, ePD_Entrada
    .AddCampo "Dsc_Caja_Cuenta", ePT_Caracter, ePD_Entrada
  End With
End Sub

Public Function Buscar_Mov_Caja_Cuentas()
  Buscar_Mov_Caja_Cuentas = fClass_Entidad.Buscar(cfPackage & ".Buscar_Mov_Caja_Cuentas")
End Function

Public Function Buscar(Optional pEnVista As Boolean = False) As Boolean
Dim lParam  As DLL_COMUN.Parametro
Dim lProcedimiento As String
  
  If pEnVista Then
    lProcedimiento = "BuscarView"
  Else
    lProcedimiento = "Buscar"
  End If
  lProcedimiento = cfPackage & "." & lProcedimiento
  
  Buscar = fClass_Entidad.Buscar(lProcedimiento)

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Guardar()
Dim lResult As Boolean
Dim lMensaje As String

  With fClass_Entidad
    If .Campo(.CampoPKey).Valor = cNewEntidad Then
      lMensaje = "Se ha agregado"
    Else
      lMensaje = "Se ha actualizado"
    End If
    
    lMensaje = lMensaje & " la caja cuenta '" _
              & .Campo("Dsc_Caja_Cuenta").Valor & "'"
    
    lResult = .Guardar(cfPackage & ".Guardar")
    
    If Not lResult Then
      lMensaje = "Problema en la grabación de la caja cuenta (" _
                & .Campo("Dsc_Caja_Cuenta").Valor
    End If
    
    Call Fnt_AgregarLog(SubTipo_LOG _
                      , IIf(lResult, cEstado_Log_Mensaje, cEstado_Log_Error) _
                      , lMensaje _
                      , pId_Log_Proceso:=Id_Log_Proceso _
                      , pErr_Log_Registro:=.ErrMsg)
                      
                     
    Guardar = lResult
  End With
End Function

Public Function BuscarFinanciamiento(pMonto, pFecha_Liquidacion) As Boolean
Dim lReg As hFields
Dim lSaldoDisponible As Double
  
  If Not Buscar Then
    'Si existe un problema el proceso devuelve false
    BuscarFinanciamiento = False
    fClass_Entidad.Errnum = gDB.Errnum
    fClass_Entidad.ErrMsg = gDB.ErrMsg
    GoTo ErrProcedure
  End If
  
  Select Case Cursor.Count
    Case 0
      BuscarFinanciamiento = False
      fClass_Entidad.Errnum = eFinanciamiento_Caja.eFC_NoCajaCuenta
      fClass_Entidad.ErrMsg = "No existe una caja para la cuenta para poder financiar."
      GoTo ErrProcedure
    Case 1
      Campo("id_caja_cuenta").Valor = Cursor(1)("id_caja_cuenta").Value
      lSaldoDisponible = Saldo_Disponible(pFecha_Liquidacion)
      
      If pMonto > lSaldoDisponible Then
        BuscarFinanciamiento = False
        fClass_Entidad.Errnum = eFinanciamiento_Caja.eFC_NoAlcanza
        fClass_Entidad.ErrMsg = "El saldo disponible (" & Format(lSaldoDisponible, "#,##0.00") & ") no alcazan para financiar el monto solicitado (" & Format(pMonto, "#,##0.00") & ")"
        GoTo ErrProcedure
      End If
    Case Is >= 2
      BuscarFinanciamiento = False
      fClass_Entidad.Errnum = eFinanciamiento_Caja.eFC_MuchasCuentas
      fClass_Entidad.ErrMsg = "Existen mas de una caja en al misma moneda y mercado para la cuenta."
      GoTo ErrProcedure
  End Select

  BuscarFinanciamiento = True

ErrProcedure:
  gDB.Parametros.Clear
  Err.Clear
End Function

Public Function Saldo_Disponible(pFecha) As Double
  Call gDB.Parametros.Clear
  Call gDB.Parametros.Add("p" & Campo("id_caja_cuenta").Nombre, ePT_Numero, Campo("id_caja_cuenta").Valor, ePD_Entrada)
  Call gDB.Parametros.Add("pfecha", ePT_Fecha, pFecha, ePD_Entrada)
  Call gDB.Parametros.Add("pid_empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada)
  Call gDB.Parametros.Add("pSaldo_Caja_Cuenta", ePT_Numero, 0, ePD_Salida)
  gDB.Procedimiento = cfPackage & ".Fnt_Saldo_Caja_Cuenta"
  
  If gDB.EjecutaSP Then
    Saldo_Disponible = NVL(gDB.Parametros("pSaldo_Caja_Cuenta").Valor, 0)
  Else
    Call Fnt_MsgError(Me.SubTipo_LOG _
                      , "Problemas al obtener el saldo disponible para la caja cuenta (" & Campo("id_caja_cuenta").Valor & ")." _
                      , gDB.ErrMsg _
                      , pConLog:=True)
    Saldo_Disponible = 0
  End If
  Call gDB.Parametros.Clear
End Function

Public Function Borrar() As Boolean
Dim lResult As Boolean
Dim lMensaje As String
  
  'Llena el cursor con los registros que interesan
  If Not Buscar Then
    Borrar = False
    Exit Function
  End If
  
  With fClass_Entidad
    If .Cursor.Count > 0 Then
      lMensaje = "Se elimino la caja cuenta '" _
                & .Campo("Dsc_Caja_Cuenta").Valor & "'."
    Else
      lMensaje = "El id de la caja cuenta (" & .Campo(.CampoPKey).Valor & ") no existe."
    End If
    Set .Cursor = Nothing
    
    lResult = .Borrar(cfPackage & ".Borrar")
    
    If lResult Then
      lMensaje = "Problema en la grabación de la caja cuenta (" _
                & .Campo("Dsc_Caja_Cuenta").Valor
    End If
    
    Call Fnt_AgregarLog(SubTipo_LOG _
                      , IIf(lResult, cEstado_Log_Mensaje, cEstado_Log_Error) _
                      , lMensaje _
                      , pId_Log_Proceso:=Id_Log_Proceso _
                      , pErr_Log_Registro:=.ErrMsg)
                      
    Borrar = lResult
  End With
End Function

Public Function Buscar_Caja_Para_Invertir(Optional pId_Nemotecnico) As Boolean
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lcNemotecnico As Class_Nemotecnicos
Dim lCod_Mercado
Dim lId_Moneda
  
On Error GoTo ErrProcedure
  
  Buscar_Caja_Para_Invertir = False
  
  lCod_Mercado = Me.Campo("cod_mercado").Valor
  lId_Moneda = Me.Campo("Id_Moneda").Valor
  
  If Not IsMissing(pId_Nemotecnico) Then
    Set lcNemotecnico = New Class_Nemotecnicos
    With lcNemotecnico
      .Campo("id_nemotecnico").Valor = pId_Nemotecnico
       
      If Not .BuscarView Then
        fClass_Entidad.Errnum = .Errnum
        fClass_Entidad.ErrMsg = .ErrMsg
        GoTo ExitProcedure
      End If
      
      lCod_Mercado = .Cursor(1)("cod_mercado").Value
      lId_Moneda = .Cursor(1)("ID_MONEDA_TRANSACCION").Value
    End With
  End If
  
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = Me.Campo("id_Cuenta").Valor
    .Campo("id_moneda").Valor = lId_Moneda
    .Campo("cod_mercado").Valor = lCod_Mercado
    
    If Not .Buscar Then
      fClass_Entidad.Errnum = .Errnum
      fClass_Entidad.ErrMsg = .ErrMsg
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      fClass_Entidad.Errnum = -1
      fClass_Entidad.ErrMsg = "No existe una caja para poder realizar la operación."
      GoTo ExitProcedure
    End If
    
    Set fClass_Entidad.Cursor = .Cursor
  End With
  
  Buscar_Caja_Para_Invertir = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcCaja_Cuenta = Nothing
  Set lcNemotecnico = Nothing
End Function


Public Function Saldo_Disponible_PorCuenta(pFecha, pId_cuenta) As Double
  Call gDB.Parametros.Clear
  Call gDB.Parametros.Add("pid_cuenta", ePT_Numero, pId_cuenta, ePD_Entrada)
  Call gDB.Parametros.Add("pfecha", ePT_Fecha, pFecha, ePD_Entrada)
  Call gDB.Parametros.Add("pid_empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada)
  Call gDB.Parametros.Add("pSaldo_Caja_Cuenta", ePT_Numero, 0, ePD_Salida)
  gDB.Procedimiento = cfPackage & ".Fnt_SaldoCaja_Por_Cuenta"
  
  If gDB.EjecutaSP Then
    Saldo_Disponible_PorCuenta = NVL(gDB.Parametros("pSaldo_Caja_Cuenta").Valor, 0)
  Else
    Call Fnt_MsgError(Me.SubTipo_LOG _
                      , "Problemas al obtener el saldo disponible para la cuenta." _
                      , gDB.ErrMsg _
                      , pConLog:=True)
    Saldo_Disponible_PorCuenta = 0
  End If
  Call gDB.Parametros.Clear
End Function




