VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cls_Imp_Saldos_Inv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Class_Entidad

Dim fClass_Entidad As Class_Entidad

Dim fErrMsg As String
Dim fErrNum As Double

Dim fForm As Frm_Imp_Saldos_Inv

Public Property Let Dsc_Producto(ByVal vData As String)
    'fForm.Txt_pro.Text = vData
End Property

Public Property Get Dsc_Producto() As String
    'Dsc_Producto = fForm.Txt_Publicador.Text
End Property

Public Property Let Dsc_Publicador(ByVal vData As String)
    'fForm.Txt_Publicador.Text = vData
End Property

Public Property Get Dsc_Publicador() As String
    'Dsc_Publicador = fForm.Txt_Publicador.Text
End Property

Public Property Let Cod_Producto(ByVal vData As String)
    fForm.fCod_Producto = vData
End Property

Public Property Get Cod_Producto() As String
    Cod_Producto = fForm.fCod_Producto
End Property

Public Property Let Fecha(ByVal vData As Date)
    'fForm.Txt_Fecha.Text = vData
End Property

Public Property Get Fecha() As Date
    'Fecha = fForm.Txt_Fecha.Text
End Property

Public Property Let Id_Publicador(ByVal vData As Double)
    fForm.fId_Publicador = vData
End Property

Public Property Get Id_Publicador() As Double
    Id_Publicador = fForm.fId_Publicador
End Property

'---------------------------------------------------------
'---------------------------------------------------------
'-- Para que funcione la componente
'---------------------------------------------------------
'---------------------------------------------------------
  Public Property Set gDB(ByVal vData As Object)
    'Set fClass_Entidad.gDB = vData
    Set Proce.gDB = vData
  End Property
  
  Public Property Get gDB() As Object
    'Set gDB = fClass_Entidad.gDB
    Set gDB = Proce.gDB
  End Property
  
  Public Property Let gID_Usuario(ByVal vData As Double)
    Proce.gID_Usuario = vData
  End Property
  
  Public Property Get gID_Usuario() As Double
    gID_Usuario = Proce.gID_Usuario
  End Property
  
  Public Property Let Id_Empresa(ByVal vData As Double)
    Proce.gId_Empresa = vData
  End Property
  
  Public Property Get Id_Empresa() As Double
    Id_Empresa = Proce.gId_Empresa
  End Property
    
  Public Property Set MDI_Principal(pMDI_Principal As Object)
    Set Proce.gMDI_Principal = pMDI_Principal
  End Property
  
  Public Property Get MDI_Principal() As Object
    Set MDI_Principal = Proce.gMDI_Principal
  End Property

  Public Function GetConfig() As hRecord
  Dim lConfig As hRecord
  
    Set lConfig = New hRecord
    With lConfig
      .AddField "Config"
      '----------------------------------
      .Add.Fields("Config").Value = cPROCESS_gDB
      .Add.Fields("Config").Value = cPROCESS_gId_Usuario
      .Add.Fields("Config").Value = cPROCESS_Id_Empresa
      .Add.Fields("config").Value = cPROCESS_MDI_Principal
    End With
    
    Set GetConfig = lConfig
  End Function
'---------------------------------------------------------
'---------------------------------------------------------

Public Function Campo(pCampo As String) As DLL_COMUN.Parametro
  Set Campo = fClass_Entidad.Campo(pCampo)
End Function

Public Function Cursor() As hCollection.hRecord
  Set Cursor = fClass_Entidad.Cursor
End Function

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
  
  Set fClass_Entidad.gDB = Me.gDB
End Sub

Public Function LoadForm(Optional pModoAuto As Boolean = False) As Object
    If pModoAuto Then
        Set fForm = New Frm_Imp_Saldos_Inv
        Set fForm.fClass = Me
        Set LoadForm = fForm
    Else
        Set fForm = New Frm_Imp_Saldos_Inv
        Set fForm.fForm_Container = Me.MDI_Principal.New_Form_Container
        Set fForm.fClass = Me
    
        Load fForm
        Set LoadForm = fForm
        Call DockForm_DLL(fForm, fForm.fForm_Container)
    End If
End Function

'Public Function Fnt_Conectar_gDB_PSH() As Object
'Dim lDB_PSH As Object
'
'  Set lDB_PSH = CreateObject("DLL_DB.Class_Conect")
'  With lDB_PSH
'    .AppPath = App.Path & "\"
'    .ArchivoConfiguracion = "PSH_BDD.INI"
'    .Conectar
'  End With
'
'  Set Fnt_Conectar_gDB_PSH = lDB_PSH
'End Function

Public Function Fnt_CargaSaldos_Inv(pFechaProceso As Date, _
                                    pDB As Object, _
                                    ByRef pCodResultado, _
                                    ByRef pMsgResultado, _
                                    ByRef pCursor) As Boolean
                                     
Dim lClase_Entidad  As Class_Entidad
Dim lResult         As Boolean
Dim lCursor         As hRecord

    lResult = True
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "PFECHA_PROCESO", ePT_Fecha, pFechaProceso, ePD_Entrada
    'gDB.Parametros.Add "pCodErr", ePT_Numero, pCodResultado, ePD_Salida
    'gDB.Parametros.Add "pMsgErr", ePT_Caracter, pMsgResultado, ePD_Salida
    gDB.Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
    gDB.Procedimiento = "PKG_INVERSIS$Importador_Saldos"

    If Not gDB.EjecutaSP Then
      lResult = False
      fClass_Entidad.Errnum = gDB.Errnum
      fClass_Entidad.ErrMsg = gDB.ErrMsg
      pCodResultado = gDB.Errnum
      pMsgResultado = gDB.ErrMsg
    Else
      Set pCursor = gDB.Parametros("Pcursor").Valor
    End If
    Fnt_CargaSaldos_Inv = lResult

End Function

Public Function Fnt_TestImportador_INV(pFECHA_CIERRE As Date, _
                                       PCUENTA_EXTERNA As String, _
                                       PCODIGO_ISIN As String, _
                                       PCOD_EMISOR As String, _
                                       PDSC_EMISOR As String, _
                                       PTIPO_PRODUCTO As String, _
                                       PTIPO_ACTIVO As String, _
                                       PDESCRIPCION_VALOR As String, _
                                       PCODIGO_DIVISA_COTIZACION As String, _
                                       PDSC_DIVISA_COTIZACION As String, _
                                       PTOTAL_TITULOS As Double, _
                                       PTOTAL_NOMINAL As Double, _
                                       PCOSTE_MEDIO_POSICION As Double, _
                                       PPRECIO_MERCADO_COTIZACION As Double, _
                                       PPRECIO As Double, _
                                       pFECHA_VENCIMIENTO As Date, _
                                       PVALOR As Double, _
                                       PRATIO_CONVERSION_CONTRA_EURO As Double, _
                                       ByRef pCodResultado, _
                                       ByRef pMsgResultado, _
                                       ByRef pCursor) As Boolean
Dim lClase_Entidad  As Class_Entidad
Dim lResult         As Boolean
Dim lCursor         As hRecord

    lResult = True
    gDB.Parametros.Clear
    gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, pFECHA_CIERRE, ePD_Entrada
    gDB.Parametros.Add "PCUENTA_EXTERNA", ePT_Caracter, PCUENTA_EXTERNA, ePD_Entrada
    gDB.Parametros.Add "PCODIGO_ISIN", ePT_Caracter, PCODIGO_ISIN, ePD_Entrada
    gDB.Parametros.Add "PCOD_EMISOR", ePT_Caracter, PCOD_EMISOR, ePD_Entrada
    gDB.Parametros.Add "PDSC_EMISOR", ePT_Caracter, PDSC_EMISOR, ePD_Entrada
    gDB.Parametros.Add "PTIPO_PRODUCTO", ePT_Caracter, PTIPO_PRODUCTO, ePD_Entrada
    gDB.Parametros.Add "PTIPO_ACTIVO", ePT_Caracter, PTIPO_ACTIVO, ePD_Entrada
    gDB.Parametros.Add "PDESCRIPCION_VALOR", ePT_Caracter, PDESCRIPCION_VALOR, ePD_Entrada
    gDB.Parametros.Add "PCODIGO_DIVISA_COTIZACION", ePT_Caracter, PCODIGO_DIVISA_COTIZACION, ePD_Entrada
    gDB.Parametros.Add "PDSC_DIVISA_COTIZACION", ePT_Caracter, PDSC_DIVISA_COTIZACION, ePD_Entrada
    gDB.Parametros.Add "PTOTAL_TITULOS", ePT_Numero, PTOTAL_TITULOS, ePD_Entrada
    gDB.Parametros.Add "PTOTAL_NOMINAL", ePT_Numero, PTOTAL_NOMINAL, ePD_Entrada
    gDB.Parametros.Add "PCOSTE_MEDIO_POSICION", ePT_Numero, PCOSTE_MEDIO_POSICION, ePD_Entrada
    gDB.Parametros.Add "PPRECIO_MERCADO_COTIZACION", ePT_Numero, PPRECIO_MERCADO_COTIZACION, ePD_Entrada
    gDB.Parametros.Add "PPRECIO", ePT_Numero, PPRECIO, ePD_Entrada
    gDB.Parametros.Add "PFECHA_VENCIMIENTO", ePT_Fecha, pFECHA_VENCIMIENTO, ePD_Entrada
    gDB.Parametros.Add "PVALOR", ePT_Numero, PVALOR, ePD_Entrada
    gDB.Parametros.Add "PRATIO_CONVERSION_CONTRA_EURO", ePT_Numero, PRATIO_CONVERSION_CONTRA_EURO, ePD_Entrada
    gDB.Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS_INT$TestImportador_INV"
    
    If Not gDB.EjecutaSP Then
      lResult = False
      fClass_Entidad.Errnum = gDB.Errnum
      fClass_Entidad.ErrMsg = gDB.ErrMsg
      pMsgResultado = gDB.ErrMsg
    Else
      Set pCursor = gDB.Parametros("Pcursor").Valor
      If pCursor.Count > 0 Then
         pMsgResultado = IIf(IsNull(pCursor(1)("msgresultado").Value), "", pCursor(1)("msgresultado").Value)
         pCodResultado = IIf(IsNull(pCursor(1)("codresultado").Value), "", pCursor(1)("codresultado").Value)
      Else
         pMsgResultado = "Inconsistencia de datos"
         pCodResultado = "ERROR"
      End If
    End If
    Fnt_TestImportador_INV = lResult

End Function

Public Function Fnt_GrabarSaldosActivosInt(pID_SALDO_ACTIVO_INT, _
                                           pFECHA_CIERRE, _
                                           pORIGEN, _
                                           pID_CUENTA, _
                                           pID_NEMOTECNICO, _
                                           pMONEDA, _
                                           pFECHA_VENCIMIENTO, _
                                           pCANTIDAD, _
                                           pPRECIO_MERCADO, _
                                           pPRECIO_COMPRA, _
                                           pPRECIO_PROMEDIO_COMPRA, _
                                           pVALOR_MERCADO_MON_ORIGEN, _
                                           pVALOR_MERCADO_MON_USD, _
                                           pMONTO_INVERTIDO, _
                                           ByRef pCodResultado, _
                                           ByRef pMsgResultado) As Boolean
                                                 
Dim lResult         As Boolean
Dim lClass_Entidad  As New Class_Entidad
Dim lCursor         As hRecord

    lResult = True
    gDB.Parametros.Clear
    gDB.Parametros.Add "pID_SALDO_ACTIVO_INT", ePT_Numero, pID_SALDO_ACTIVO_INT, ePD_Salida
    gDB.Parametros.Add "pFECHA_CIERRE", ePT_Fecha, pFECHA_CIERRE, ePD_Entrada
    gDB.Parametros.Add "pORIGEN", ePT_Caracter, pORIGEN, ePD_Entrada
    gDB.Parametros.Add "pID_CUENTA", ePT_Numero, pID_CUENTA, ePD_Entrada
    gDB.Parametros.Add "pID_NEMOTECNICO", ePT_Numero, pID_NEMOTECNICO, ePD_Entrada
    gDB.Parametros.Add "pMONEDA", ePT_Caracter, pMONEDA, ePD_Entrada
    gDB.Parametros.Add "pFECHA_VENCIMIENTO", ePT_Fecha, pFECHA_VENCIMIENTO, ePD_Entrada
    gDB.Parametros.Add "pCANTIDAD", ePT_Numero, pCANTIDAD, ePD_Entrada
    gDB.Parametros.Add "pPRECIO_MERCADO", ePT_Numero, pPRECIO_MERCADO, ePD_Entrada
    gDB.Parametros.Add "pPRECIO_COMPRA", ePT_Numero, pPRECIO_COMPRA, ePD_Entrada
    gDB.Parametros.Add "pPRECIO_PROMEDIO_COMPRA", ePT_Numero, pPRECIO_PROMEDIO_COMPRA, ePD_Entrada
    gDB.Parametros.Add "pVALOR_MERCADO_MON_ORIGEN", ePT_Numero, pVALOR_MERCADO_MON_ORIGEN, ePD_Entrada
    gDB.Parametros.Add "pVALOR_MERCADO_MON_USD", ePT_Numero, pVALOR_MERCADO_MON_USD, ePD_Entrada
    gDB.Parametros.Add "pMONTO_INVERTIDO", ePT_Numero, pMONTO_INVERTIDO, ePD_Entrada
    
    gDB.Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS_INT$Guardar"

    If Not gDB.EjecutaSP Then
       lResult = False
       fClass_Entidad.Errnum = gDB.Errnum
       fClass_Entidad.ErrMsg = gDB.ErrMsg
       pMsgResultado = gDB.ErrMsg
    Else
       Set lCursor = gDB.Parametros("Pcursor").Valor
       pMsgResultado = IIf(IsNull(lCursor(1)("msgresultado").Value), "", lCursor(1)("msgresultado").Value)
       pCodResultado = IIf(IsNull(lCursor(1)("codresultado").Value), "", lCursor(1)("codresultado").Value)
       pID_SALDO_ACTIVO_INT = gDB.Parametros.Item("pID_SALDO_ACTIVO_INT").Valor
       lResult = True
    End If
    Fnt_GrabarSaldosActivosInt = lResult
End Function

Public Function Fnt_BorrarSaldosActivosInt(ByVal pFechaProceso As Date, _
                                           ByVal pORIGEN As String, _
                                           ByRef pCodResultado, _
                                           ByRef pMsgResultado) As Boolean
Dim lClase_Entidad  As Class_Entidad
Dim lResult         As Boolean
Dim lCursor         As hRecord

    lResult = True
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "pFechaProceso", ePT_Fecha, pFechaProceso, ePD_Entrada
    gDB.Parametros.Add "pOrigen", ePT_Caracter, pORIGEN, ePD_Entrada
    gDB.Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS_INT$Borrar"

    If Not gDB.EjecutaSP Then
      lResult = False
      fClass_Entidad.Errnum = gDB.Errnum
      fClass_Entidad.ErrMsg = gDB.ErrMsg
      pMsgResultado = gDB.ErrMsg
    Else
      Set lCursor = gDB.Parametros("Pcursor").Valor
      pMsgResultado = IIf(IsNull(lCursor(1)("msgresultado").Value), "", lCursor(1)("msgresultado").Value)
      pCodResultado = IIf(IsNull(lCursor(1)("codresultado").Value), "", lCursor(1)("codresultado").Value)
      lResult = True
    End If
    Fnt_BorrarSaldosActivosInt = lResult
End Function

Private Sub Class_Terminate()
On Error Resume Next

  Set fForm = Nothing
End Sub
