VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Vencimientos_RF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Class_Entidad
Dim fClass_Entidad As Class_Entidad

Const cfPackage = "Pkg_Vencimientos_RF"

'---------------------------------------------------------
'-- Para el uso de los LOGz
'---------------------------------------------------------
Public Id_Log_Proceso

Public Function SubTipo_LOG() As Double
  SubTipo_LOG = eLS_Vencimiento_RF_Local
End Function
'---------------------------------------------------------

Public Function ErrMsg() As String
  ErrMsg = fClass_Entidad.ErrMsg
End Function

Public Function Errnum() As Double
  Errnum = fClass_Entidad.Errnum
End Function


'Public Function Fnt_Vencimientos_RF(pFecha As Date, pGrilla As VSFlexGrid) As Boolean
'Dim lSaldos_Activos As Class_Saldo_Activos
'Dim lPrecio As Class_Publicadores_Precio
'Dim lMov_Activos As Class_Mov_Activos
'Dim lOperaciones_detalle As Class_Operaciones_Detalle
'Dim lTasa As Double
'Dim lCursor_Nem As hRecord
'Dim lReg As hFields
'Dim lReg_Mov As hFields
'Dim lReg_Op As hFields
'Dim lCuenta As String
'Dim lCantidad As Double
'Dim lMonto_Valorizado As Double
'Dim lMsg_Error As String
'Dim lId_Operacion_detalle As String
'Dim lFlg_Monto_Referenciado As String
'
'  Fnt_Vencimientos_RF = True
'
'  Call Fnt_Escribe_Grilla(pGrilla, "N", "Inicio Vencimientos RF.")
'
'  Rem busca todos los nemotecnicos en cartera
'  Set lSaldos_Activos = New Class_Saldo_Activos
'  If lSaldos_Activos.Fnt_Buscar_Nemotecnicos_Cartera(pFecha, , gcPROD_RF_NAC) Then
'    Set lCursor_Nem = lSaldos_Activos.Cursor
'  Else
'    Fnt_Vencimientos_RF = False
'    Call Fnt_Escribe_Grilla(pGrilla, "E", "Vencimientos: Error en la b�squeda de nemot�cnicos RF en cartera." & vbCr & vbCr & lSaldos_Activos.ErrMsg)
'    GoTo ErrProcedure
'  End If
'
'  Rem Por cada nemotecnicos se calculan los vencimientos
'  If lCursor_Nem.Count > 0 Then
'    For Each lReg In lCursor_Nem
'      lCuenta = NVL(lReg("ID_CUENTA").Value, -1)
'      lCantidad = NVL(lReg("Cantidad").Value, -1)
'      If lCantidad > 0 Then
'
'        Rem busca la �ltima tasa para valorizar
'        Set lPrecio = New Class_Publicadores_Precio
'        With lPrecio
'          .Campo("id_nemotecnico").Valor = lReg("id_nemotecnico").Value
'          .Campo("fecha").Valor = Fnt_FechaServidor
'          If .Buscar_Ultimo_Tasa_Cta_Nemo(lCuenta) Then
'            lTasa = .Campo("tasa").Valor
'          Else
'            Call Fnt_Escribe_Grilla(pGrilla, "A", "Vencimiento: Error en la b�squeda de la �ltima tasa del nemot�cnico " & lReg("nemotecnico").Value & " para la cuenta " & lCuenta & "." & vbCr & "Se valoriza con tasa 0." & vbCr & vbCr & lPrecio.ErrMsg)
'            lTasa = 0
'          End If
'        End With
'        Set lPrecio = Nothing
'
'        Rem Busca el flg_monto_referenciado en operaciones detalle para el valorizador de DEPOSITOS.
'        If lReg("cod_instrumento").Value = gcINST_DEPOSITOS_NAC Then
'          Rem busca el id_operacion_detalle segun el id_mov_activo de la saldos activos
'          Set lMov_Activos = New Class_Mov_Activos
'          lMov_Activos.Campo("id_mov_activo").Valor = lReg("id_mov_activo").Value
'          If lMov_Activos.Buscar() Then
'            For Each lReg_Mov In lMov_Activos.Cursor
'              lId_Operacion_detalle = NVL(lReg_Mov("id_operacion_detalle").Value, "")
'            Next
'          Else
'            Call Fnt_Escribe_Grilla(pGrilla, "A", "Vencimiento: Error en la b�squeda de la operaci�n detalle." & vbCr & vbCr & lMov_Activos.ErrMsg)
'          End If
'
'          Rem busca el flg_monto_referenciado con el id_operaciones_detalle
'          Set lOperaciones_detalle = New Class_Operaciones_Detalle
'          lOperaciones_detalle.Campo("id_operaciones_detalle").Valor = lId_Operacion_detalle
'          If lOperaciones_detalle.Buscar() Then
'            For Each lReg_Op In lOperaciones_detalle.Cursor
'              lFlg_Monto_Referenciado = NVL(lReg_Op("flg_monto_referenciado").Value, "")
'            Next
'          Else
'            Call Fnt_Escribe_Grilla(pGrilla, "A", "Vencimiento: Error en la b�squeda de la operaci�n detalle." & vbCr & vbCr & lOperaciones_detalle.ErrMsg)
'          End If
'        End If
'
'        If Fnt_Valorizar(lReg("cod_instrumento").Value, _
'                         lReg("id_nem").Value, _
'                         lReg("Cantidad").Value, _
'                         lTasa, _
'                         pFecha, _
'                         lFlg_Monto_Referenciado, _
'                         lMonto_Valorizado, _
'                         lMsg_Error) Then
'          If Fnt_Realiza_Operacion(lReg("id_Nemotecnico").Value, _
'                                   lCuenta, _
'                                   lReg("cod_producto").Value, _
'                                   lReg("cod_instrumento").Value, _
'                                   lCantidad, _
'                                   lTasa, _
'                                   lReg("ID_MONEDA_NEMOTECNICO").Value, _
'                                   lMonto_Valorizado, _
'                                   pFecha, _
'                                   0, _
'                                   lReg("nemotecnico").Value, _
'                                   pGrilla) Then
'            Call Fnt_Escribe_Grilla(pGrilla, _
'                                    "N", _
'                                    "Vencimientos: Nemot�cnico '" & lReg("nemotecnico").Value & "': Se ha efectuado el vencimiento de la cuenta: " & lReg("num_cuenta").Value)
'          End If
'        Else
'          Call Fnt_Escribe_Grilla(pGrilla, _
'                                  "E", _
'                                  "Vencimientos: Nemot�cnico '" & lReg("nemotecnico").Value & "': Error al valorizar la cuenta " & lReg("num_cuenta").Value & vbCr & vbCr & lMsg_Error)
'        End If
'      End If
'      gDB.Parametros.Clear
'    Next
'  Else
'    Call Fnt_Escribe_Grilla(pGrilla, _
'                            "A", _
'                            "Vencimientos: No hay nemot�cnicos vencidos para la fecha " & pFecha & ".")
'  End If
'
'ErrProcedure:
'  Call Fnt_Escribe_Grilla(pGrilla, "N", "T�rmino Vencimientos RF.")
'
'End Function

'Private Function Fnt_Valorizar(pCod_Instrumento As String, _
'                               pId_Nemotecnico As String, _
'                               pCantidad As Double, _
'                               pTasa As Double, _
'                               pFecha_Calculo, _
'                               pFlg_Monto_Referenciado As String, _
'                               ByRef pValorizacion, _
'                               ByRef pMsg_Error As String) As Boolean
'Dim lcClase       As Object
'Dim lTipo_Calculo As Integer
'
'  Fnt_Valorizar = True
'  Select Case pCod_Instrumento
'    Case gcINST_BONOS_NAC
'      '--------------- BONOS ----------------------------------------------------
'      Set lcClase = New Class_Bonos
'      pValorizacion = lcClase.ValorizaPapel(pId_Nemotecnico:=pId_Nemotecnico, _
'                                            pFecha:=Fnt_FechaServidor, _
'                                            pTasa:=pTasa, _
'                                            pNominales:=pCantidad, _
'                                            pMostrar_Mensaje:=False, _
'                                            pMsg_Error:=pMsg_Error)
'      Rem Si pMsg_Error no es vacio significa que hay un error la valorizacion
'      If Not pMsg_Error = "" Then
'        Fnt_Valorizar = False
'      End If
'    Case gcINST_DEPOSITOS_NAC
'      '-------------- DEPOSITOS -------------------------------------------------
'      If pFlg_Monto_Referenciado = "I" Then
'        lTipo_Calculo = 1
'      ElseIf pFlg_Monto_Referenciado = "F" Then
'        lTipo_Calculo = 2
'      End If
'      Set lcClase = New Class_Depositos
'      pValorizacion = lcClase.ValorizaPapel(pNemotecnico:=pId_Nemotecnico, _
'                                            pFecha:=pFecha_Calculo, _
'                                            pTasa:=pTasa, _
'                                            pNominales:=pCantidad, _
'                                            pTipo_Calculo:=lTipo_Calculo, _
'                                            pMostrar_Mensaje:=False, _
'                                            pMsg_Error:=pMsg_Error, _
'                                            pId_Nemotecnico:=pId_Nemotecnico)
'      Rem Si pMsg_Error no es vacio significa que hay un error la valorizacion
'      If Not pMsg_Error = "" Then
'        Fnt_Valorizar = False
'      End If
'    Case gcINST_PACTOS_NAC
'      '-------------- DEPOSITOS -------------------------------------------------
'      If pFlg_Monto_Referenciado = "I" Then
'        lTipo_Calculo = 1
'      ElseIf pFlg_Monto_Referenciado = "F" Then
'        lTipo_Calculo = 2
'      End If
'      Set lcClase = New Class_Pactos
'      pValorizacion = lcClase.ValorizaPapel(pNemotecnico:=pId_Nemotecnico, _
'                                            pFecha:=pFecha_Calculo, _
'                                            pTasa:=pTasa, _
'                                            pNominales:=pCantidad, _
'                                            pTipo_Calculo:=lTipo_Calculo, _
'                                            pMostrar_Mensaje:=False, _
'                                            pMsg_Error:=pMsg_Error, _
'                                            pId_Nemotecnico:=pId_Nemotecnico)
'      Rem Si pMsg_Error no es vacio significa que hay un error la valorizacion
'      If Not pMsg_Error = "" Then
'        Fnt_Valorizar = False
'      End If
'  End Select
'
'End Function

Private Function Fnt_Realiza_Operacion(ByVal pId_Nemotecnico, _
                                      ByVal pId_Cuenta, _
                                      ByVal pCod_Producto, _
                                      ByVal pCod_Instrumento, _
                                      ByVal pCantidad, _
                                      ByVal pTasa, _
                                      ByVal pId_Moneda, _
                                      ByVal pMonto, _
                                      ByVal pFecha, _
                                      ByVal pPorc_Comision, _
                                      ByVal pNemotecnico, _
                                      ByVal pId_Mov_Activo_Compra) As Boolean
Dim lOperacion As Class_Operaciones
Dim lOperacion_Detalle As Class_Operaciones_Detalle
Dim lId_Caja_Cuenta As Double
Dim lCod_Tipo_Operacion As String
Dim lDecimales As Integer

  Fnt_Realiza_Operacion = False

  Rem Como se debe ingresar una venta en mov_activos se valida la caja
  lId_Caja_Cuenta = Fnt_ValidarCaja(pId_Cuenta, cMercado_Nacional, pId_Moneda, lDecimales)
  If lId_Caja_Cuenta < 0 Then
    fClass_Entidad.ErrMsg = "No hay caja disponible para la moneda del nemotecnico " & pNemotecnico
    Rem Si el financiamiento tuvo problemas
    GoTo ExitProcedure
  End If
  pMonto = Round(pMonto, lDecimales)
  Set lOperacion_Detalle = New Class_Operaciones_Detalle
  With lOperacion_Detalle
    .Campo("Id_Nemotecnico").Valor = pId_Nemotecnico
    .Campo("Cantidad").Valor = pCantidad
    .Campo("PRECIO").Valor = pTasa
    .Campo("Id_Moneda_Pago").Valor = pId_Moneda
    .Campo("Monto_Pago").Valor = pMonto
    .Campo("ID_MOV_ACTIVO_COMPRA").Valor = pId_Mov_Activo_Compra
    .Campo("FLG_Vende_Todo").Valor = cFlg_No_Vende_Todo
  End With

  Select Case pCod_Instrumento
    Case gcINST_DEPOSITOS_NAC, gcINST_PACTOS_NAC, gcINST_DEPOSITOS_DAP 'JGR 080509
     lCod_Tipo_Operacion = gcOPERACION_Vencimiento_IIF
    Case gcINST_BONOS_NAC
      lCod_Tipo_Operacion = gcOPERACION_Vencimiento_RF
  End Select

  Set lOperacion = New Class_Operaciones
  lOperacion.LimpiaParam
  Call lOperacion.Agregar_Operaciones_Detalle(lOperacion_Detalle)
  With lOperacion
    .Campo("Id_Operacion").Valor = cNewEntidad
    .Campo("Id_Cuenta").Valor = pId_Cuenta
    .Campo("Cod_Tipo_Operacion").Valor = lCod_Tipo_Operacion
    .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente
    .Campo("Id_Moneda_Operacion").Valor = pId_Moneda
    .Campo("Cod_Producto").Valor = pCod_Producto
    .Campo("Cod_Instrumento").Valor = pCod_Instrumento
    .Campo("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Egreso
    .Campo("Fecha_Operacion").Valor = pFecha
    .Campo("Fecha_Vigencia").Valor = pFecha
    .Campo("Fecha_Liquidacion").Valor = pFecha
    .Campo("Dsc_Operacion").Valor = ""
    .Campo("Porc_Comision").Valor = pPorc_Comision
    .Campo("Comision").Valor = 0
    .Campo("Derechos").Valor = 0
    .Campo("Gastos").Valor = 0
    .Campo("Iva").Valor = 0
    .Campo("Monto_Operacion").Valor = pMonto
    .Campo("FLG_TIPO_ORIGEN").Valor = cTipo_Origen_Automatico

    If Not .Guardar(lId_Caja_Cuenta) Then
      fClass_Entidad.ErrMsg = "Problemas al guardar la operacion de Vencimiento Nemot�cnico." & lOperacion.ErrMsg
      GoTo ExitProcedure
    End If
  End With

  Fnt_Realiza_Operacion = True
  
ExitProcedure:

End Function

Public Function Vencimientos_X_Cuenta(pId_Cuenta _
                                    , pFecha_Cierre _
                                    , ByRef pMensaje As String) As Boolean
Dim lcSaldos_Activos      As Class_Saldo_Activos
Dim lcMov_Activo          As Class_Mov_Activos
Dim lcOperacion_Detalle   As Class_Operaciones_Detalle
Dim lcNemotecnico         As Class_Nemotecnicos
'Dim lcTipo_Cambio         As Class_Tipo_Cambios
Dim lcTipo_Cambio         As Object
Dim lCursor               As hRecord
Dim lCursor_Vencidos      As hRecord
Dim lReg                  As hFields
Dim lRegNemotecnico       As hFields
'--------------------------------------------------------------
Dim lId_Cuenta            As String
Dim lTasa                 As Double
Dim lCantidad As Double
Dim lMonto_Valorizado As Double
Dim lMsg_Error As String
Dim lId_Operacion_detalle As String
Dim lFlg_Monto_Referenciado As String
'--------------------------------------------------------------
Dim lFecha_Vencimiento As Date

  Vencimientos_X_Cuenta = False
  fClass_Entidad.ErrMsg = ""
  
  pMensaje = ""
  
  lFecha_Vencimiento = pFecha_Cierre + 1
  
  
'  Set lcTipo_Cambio = New Class_Tipo_Cambios
  Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
  
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("FECHA_VENCIMIENTO").Valor = lFecha_Vencimiento
    If Not .Buscar_Vencidos(gcPROD_RF_NAC) Then
      fClass_Entidad.ErrMsg = "Problema en la b�squeda de nemot�cnicos RF vencidos." & vbCr & vbCr & .ErrMsg
      GoTo ExitProcedure
    End If
    
    Set lCursor_Vencidos = .Cursor
  End With
  
  
  Rem busca todos los nemotecnicos en cartera
  Set lcSaldos_Activos = New Class_Saldo_Activos
  If Not lcSaldos_Activos.Buscar_al_cierre(pId_Cuenta:=pId_Cuenta _
                                          , pFecha_Cierre:=pFecha_Cierre _
                                          , pCod_Producto:=gcPROD_RF_NAC) Then
    fClass_Entidad.ErrMsg = "Problema en la b�squeda de nemot�cnicos RF en cartera." & vbCr & vbCr & lcSaldos_Activos.ErrMsg
    GoTo ExitProcedure
  End If
  
  Set lCursor = lcSaldos_Activos.Cursor
  
  For Each lReg In lCursor
    'Primero revisa si el papel que esta en cartera esta vencida
    Set lRegNemotecnico = lCursor_Vencidos.Buscar("id_nemotecnico", lReg("id_nemotecnico").Value)
    If Not lRegNemotecnico Is Nothing Then
      lId_Cuenta = lReg("ID_CUENTA").Value
      
      Rem Busca el flg_monto_referenciado en operaciones detalle para el valorizador de DEPOSITOS.
      Select Case lReg("cod_instrumento").Value
        Case gcINST_DEPOSITOS_NAC, gcINST_PACTOS_NAC, gcINST_DEPOSITOS_DAP 'JGR 080509
          Rem busca el id_operacion_detalle segun el id_mov_activo de la saldos activos
          Set lcMov_Activo = New Class_Mov_Activos
          lcMov_Activo.Campo("id_mov_activo").Valor = lReg("id_mov_activo").Value
          If Not lcMov_Activo.Buscar() Then
            fClass_Entidad.ErrMsg = "Problemas en la b�squeda de la operaci�n detalle." & vbCr & vbCr & lcMov_Activo.ErrMsg
            GoTo ExitProcedure
          End If
          
          lId_Operacion_detalle = lcMov_Activo.Cursor(1)("id_operacion_detalle").Value
          lTasa = lcMov_Activo.Cursor(1)("precio").Value
          
          Rem busca el flg_monto_referenciado con el id_operaciones_detalle
          Set lcOperacion_Detalle = New Class_Operaciones_Detalle
          lcOperacion_Detalle.Campo("id_operacion_detalle").Valor = lId_Operacion_detalle
          If Not lcOperacion_Detalle.Buscar() Then
            fClass_Entidad.ErrMsg = "Problemas en la b�squeda de la operaci�n detalle." & vbCr & vbCr & lcOperacion_Detalle.ErrMsg
            GoTo ExitProcedure
          End If
          
          lFlg_Monto_Referenciado = "" & lcOperacion_Detalle.Cursor(1)("flg_monto_referenciado").Value
          
          lMonto_Valorizado = lcTipo_Cambio.Cambio_Paridad(pId_Cuenta:=pId_Cuenta _
                                                         , pMonto:=lReg("Cantidad").Value _
                                                         , pId_Moneda_Origen:=lRegNemotecnico("ID_MONEDA").Value _
                                                         , pId_Moneda_Destino:=lRegNemotecnico("ID_MONEDA_TRANSACCION").Value _
                                                         , pFecha:=lFecha_Vencimiento)
        Case gcINST_BONOS_NAC
          lMonto_Valorizado = lReg("MONTO_MON_NEMOTECNICO").Value
      End Select
        
      If Fnt_Realiza_Operacion(pId_Nemotecnico:=lReg("id_Nemotecnico").Value, _
                               pId_Cuenta:=lId_Cuenta, _
                               pCod_Producto:=lReg("cod_producto").Value, _
                               pCod_Instrumento:=lReg("cod_instrumento").Value, _
                               pCantidad:=lReg("Cantidad").Value, _
                               pTasa:=lTasa, _
                               pId_Moneda:=lReg("ID_MONEDA_NEMOTECNICO").Value, _
                               pMonto:=lMonto_Valorizado, _
                               pFecha:=lFecha_Vencimiento, _
                               pPorc_Comision:=0, _
                               pNemotecnico:=lReg("nemotecnico").Value, _
                               pId_Mov_Activo_Compra:=lReg("id_mov_activo").Value) Then
                               
          If Not Trim(pMensaje) = "" Then
            pMensaje = pMensaje & vbCr
          End If
          
          pMensaje = pMensaje & "Nemot�cnico '" & lReg("nemotecnico").Value & "': Se ha efectuado el vencimiento."
        End If
      gDB.Parametros.Clear
    End If
  Next
  
  Vencimientos_X_Cuenta = True
  
ExitProcedure:
  
  Set lcTipo_Cambio = Nothing
End Function

Private Sub Class_Initialize()
  Set fClass_Entidad = New Class_Entidad
End Sub

Public Function Deshacer_Enlaze_Cierre(pId_Cuenta, pFecha) As Boolean
Dim lcOperaciones As Class_Operaciones
Dim lReg As hFields

On Error GoTo ErrProcedure

  Deshacer_Enlaze_Cierre = False

  '--------------------------------------------------------------------
  '-- ELIMINA LAS OPERACION DE BONOS
  '--------------------------------------------------------------------
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("ID_CUENTA").Valor = pId_Cuenta
    .Campo("COD_TIPO_OPERACION").Valor = gcOPERACION_Vencimiento_RF
    .Campo("FECHA_OPERACION").Valor = pFecha
    If Not .Buscar_Desde Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  
    For Each lReg In .Cursor
      If Not gRelogDB Is Nothing Then
        gRelogDB.AvanzaRelog
      End If
      
      If Not Fnt_Elimina_Operaciones(lReg) Then
        GoTo ExitProcedure
      End If
    Next
  End With
  Set lcOperaciones = Nothing
  
  '--------------------------------------------------------------------
  '-- ELIMINA LAS OPERACION DE DEPOSITOS Y PACTOS
  '--------------------------------------------------------------------
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("ID_CUENTA").Valor = pId_Cuenta
    .Campo("COD_TIPO_OPERACION").Valor = gcOPERACION_Vencimiento_IIF
    .Campo("FECHA_OPERACION").Valor = pFecha
    If Not .Buscar_Desde Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
  
    For Each lReg In .Cursor
      If Not gRelogDB Is Nothing Then
        gRelogDB.AvanzaRelog
      End If
      
      If Not Fnt_Elimina_Operaciones(lReg) Then
        GoTo ExitProcedure
      End If
    Next
  End With
  Set lcOperaciones = Nothing
  
  
  Deshacer_Enlaze_Cierre = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcOperaciones = Nothing
End Function

Private Function Fnt_Elimina_Operaciones(pReg As hFields) As Boolean
Dim lcOperacion As Class_Operaciones
  
  Fnt_Elimina_Operaciones = False
  
  '--------------------------------------------------------------------
  '-- ELIMINA LOS DETALLE DE OPERACION
  '--------------------------------------------------------------------
  Set lcOperacion = New Class_Operaciones
  With lcOperacion
    .Campo("ID_OPERACION").Valor = pReg("ID_OPERACION").Value
    If Not .Borrar Then
      fClass_Entidad.ErrMsg = .ErrMsg
      fClass_Entidad.Errnum = .Errnum
      GoTo ExitProcedure
    End If
    
  End With
  Set lcOperacion = Nothing
  
  Fnt_Elimina_Operaciones = True

ErrProcedure:
  If Not Err.Number = 0 Then
    fClass_Entidad.ErrMsg = Err.Description
    fClass_Entidad.Errnum = Err.Number
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcOperacion = Nothing
End Function


