Attribute VB_Name = "Proce_Operaciones"
Option Explicit

'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: Proce_Operaciones.bas $
'    $Author: Mgutierrez $
'    $Date: 28/11/17 13:51 $
'    $Revision: 9 $
'-------------------------------------------------------------------------------------------------

Public Function Fnt_ValirdarFinanciamiento(pId_Cuenta, _
                                           pCod_Mercado, _
                                           pMonto, _
                                           pId_Moneda, _
                                           pFecha_Liquidacion, _
                                           Optional pMostrar_Msg As Boolean = True, _
                                           Optional ByRef pMsg_Error As String = "") As Double
Dim lCaja_Cuenta As Class_Cajas_Cuenta

  Set lCaja_Cuenta = New Class_Cajas_Cuenta
  With lCaja_Cuenta
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("cod_mercado").Valor = pCod_Mercado
    .Campo("id_moneda").Valor = pId_Moneda
    If .BuscarFinanciamiento(pMonto, pFecha_Liquidacion) Then
      Fnt_ValirdarFinanciamiento = .Campo("id_caja_cuenta").Valor
    Else
      pMsg_Error = .ErrMsg
      If pMostrar_Msg Then MsgBox pMsg_Error, vbCritical
      Fnt_ValirdarFinanciamiento = cNewEntidad
    End If
  End With
  Set lCaja_Cuenta = Nothing
End Function


Public Function Fnt_CheckeaFinanciamiento(pId_Cuenta, _
                                          pCod_Mercado, _
                                          pMonto, _
                                          pId_Moneda, _
                                          pFecha_Liquidacion, _
                                          Optional pMostrar_Msg As Boolean = True, _
                                          Optional ByRef pMsg_Error As String = "", _
                                          Optional ByRef pNum_Error = 0, _
                                          Optional ByRef pId_Caja As Double = 0) As Double
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
'----------------------------------------
Dim lRespuesta As String

  Fnt_CheckeaFinanciamiento = cNewEntidad

  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    If pId_Caja <> 0 Then
        .Campo("Id_Caja_Cuenta").Valor = pId_Caja
    End If
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("cod_mercado").Valor = pCod_Mercado
    .Campo("id_moneda").Valor = pId_Moneda
    Call .BuscarFinanciamiento(pMonto, pFecha_Liquidacion)
    pNum_Error = .Errnum
    pMsg_Error = .ErrMsg
  End With
    
  Select Case lcCaja_Cuenta.Errnum
    Case 0
      Fnt_CheckeaFinanciamiento = lcCaja_Cuenta.Campo("id_caja_cuenta").Valor
    Case eFinanciamiento_Caja.eFC_NoAlcanza
      Fnt_CheckeaFinanciamiento = lcCaja_Cuenta.Campo("id_caja_cuenta").Valor
      
      'Si la planta no alcansa se verifica a la cuenta para saber si esta puede
      'operar sin plata (o sea, pedir fiado).
'      Set lcCuenta = New Class_Cuentas
      Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
      With lcCuenta
        .Campo("id_cuenta").Valor = pId_Cuenta
        If Not .Buscar Then
          pNum_Error = .Errnum
          pMsg_Error = .ErrMsg
          GoTo ExitProcedure
        End If
        
        lRespuesta = vbNo
        If .Cursor(1)("FLG_MOV_DESCUBIERTOS").Value = gcFlg_SI Then
          'Si la cuenta tiene permitido invertir sin plata.
          lRespuesta = MsgBox(lcCaja_Cuenta.ErrMsg & vbCr & vbCr & "¿Desea Continuar?", vbQuestion + vbYesNo, "Inversión Descubierta")
        Else
          Call MsgBox(lcCaja_Cuenta.ErrMsg, vbCritical + vbOKOnly, "Inversión Descubierta")
        End If
      End With
      
      If lRespuesta = vbYes Then
        'Si la respuesta del usuario es invertir de todas maneras se
        pNum_Error = eFinanciamiento_Caja.eFC_InversionDescubierta
      End If
  End Select
  
ExitProcedure:
  Set lcCaja_Cuenta = Nothing
  Set lcCuenta = Nothing
End Function

Public Function Fnt_ValidarCaja(pId_Cuenta, pCod_Mercado, pId_Moneda, Optional ByRef pDecimales) As Double
Dim lCaja_Cuenta  As New Class_Cajas_Cuenta

  With lCaja_Cuenta
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("cod_mercado").Valor = pCod_Mercado
    .Campo("id_moneda").Valor = pId_Moneda
    If .Buscar Then
      Select Case .Cursor.Count
        Case 1
          Fnt_ValidarCaja = .Cursor(1)("id_caja_cuenta").Value
          pDecimales = .Cursor(1)("Decimales").Value
        Case 0
          MsgBox "No existe caja para la cuenta.", vbCritical
          Fnt_ValidarCaja = -1
        Case Is > 1
          MsgBox "Existe mas de una caja para la cuenta.", vbCritical
          Fnt_ValidarCaja = -1
      End Select
    Else
      MsgBox .ErrMsg, vbCritical
      Fnt_ValidarCaja = -1
    End If
  End With
End Function

Public Function Fnt_Cuenta_Unica(Num_Cuenta) As Boolean
'Dim lcCuenta As New Class_Cuentas
Dim lcCuenta As Object

Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  With lcCuenta
    .Campo("num_cuenta").Valor = Num_Cuenta
    If .Buscar Then
      If .Cursor.Count > 0 Then
         Fnt_Cuenta_Unica = False
      Else
         Fnt_Cuenta_Unica = True
      End If
    Else
      MsgBox .ErrMsg, vbCritical
      Fnt_Cuenta_Unica = False
    End If
  End With

End Function

Public Function Fnt_CompruebaRestriccionCompra(pId_Cuenta, pId_Emisor, pId_Nemotecnico) As Boolean
Dim lReg As hFields
Dim lCant As Long
Dim lID
Dim lcRestriccion_Compra As Class_Restricciones_Compra_Cuenta

  Fnt_CompruebaRestriccionCompra = True
  
  Set lcRestriccion_Compra = New Class_Restricciones_Compra_Cuenta
  With lcRestriccion_Compra
    .Campo("id_Cuenta").Valor = pId_Cuenta
    .Campo("id_Emisor_Especifico").Valor = pId_Emisor
    .Campo("id_Nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      For Each lReg In .Cursor
        Fnt_CompruebaRestriccionCompra = False
      Next
    End If
  End With
  Set lcRestriccion_Compra = Nothing
  
End Function

Public Function Fnt_CompruebaRestriccionVenta(pId_Cuenta, pId_Emisor, pId_Nemotecnico, pCantidad) As Boolean
Dim lReg As hFields
Dim lReg_Nemo As hFields
Dim lCant As Long
Dim lID
Dim lcRestricciones_Venta As Class_Restricciones_Venta_Cuenta
Dim lcSaldos_Activos As Class_Saldo_Activos
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Dim lcSaldos_FFMM As Class_FondosMutuos
Dim lcSaldos_Acciones As Class_Acciones
'Dim lcSaldos_Acciones As Class_Acciones
Dim lcSaldos_Bonos As Class_Bonos
Dim lcSaldos_Depositos As Class_Depositos
Dim lcSaldos_Pactos As Class_Pactos
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Dim lcNemotenicos As Class_Nemotecnicos
Dim Cod_Instrumento As String
  Fnt_CompruebaRestriccionVenta = True
  
  Set lcRestricciones_Venta = New Class_Restricciones_Venta_Cuenta
  With lcRestricciones_Venta
    .Campo("id_Cuenta").Valor = pId_Cuenta
    .Campo("id_Emisor_Especifico").Valor = pId_Emisor
    .Campo("id_Nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      For Each lReg In .Cursor
        If NVL(lReg("Flg_Restringir_Todo").Value, "") = "S" Then
          Fnt_CompruebaRestriccionVenta = False
        Else
          'Aqui se chequea que el monto disponible sea menor o igual al monto de la operacion
          'si es que existe la restricción (ex Sp_Saldo_Cuenta_Nemo)
          Cod_Instrumento = ""
          Set lcNemotenicos = New Class_Nemotecnicos
          With lcNemotenicos
              .Campo("id_nemotecnico").Valor = pId_Nemotecnico
              If .Buscar Then
                For Each lReg_Nemo In .Cursor
                  Cod_Instrumento = Trim(lReg_Nemo("cod_instrumento").Value)
                Next
              End If
          End With
          Set lcNemotenicos = Nothing
          
          Select Case Cod_Instrumento
              Case gcINST_ACCIONES_NAC
                  Set lcSaldos_Acciones = New Class_Acciones
                  With lcSaldos_Acciones
                        If .Saldo_Activo_Cantidad(pId_Cuenta, pId_Nemotecnico) < pCantidad Then
                          Fnt_CompruebaRestriccionVenta = False
                        End If
                  End With
              Case gcINST_BONOS_NAC
                  Set lcSaldos_Bonos = New Class_Bonos
                  With lcSaldos_Bonos
                        If Not .Saldo_Activo_Cantidad(pId_Cuenta, pId_Nemotecnico, 0) < pCantidad Then
                          Fnt_CompruebaRestriccionVenta = False
                        End If
                  End With
                       
              Case gcINST_DEPOSITOS_NAC, gcINST_DEPOSITOS_DAP 'jgr 080509
                  Set lcSaldos_Depositos = New Class_Depositos
                  With lcSaldos_Depositos
                        If Not .Saldo_Activo_Cantidad(pId_Cuenta, pId_Nemotecnico, 0) < pCantidad Then
                          Fnt_CompruebaRestriccionVenta = False
                        End If
                  End With
              
              Case gcINST_PACTOS_NAC
                  Set lcSaldos_Pactos = New Class_Pactos
                  With lcSaldos_Pactos
                        If Not .Saldo_Activo_Cantidad(pId_Cuenta, pId_Nemotecnico, 0) < pCantidad Then
                          Fnt_CompruebaRestriccionVenta = False
                        End If
                  End With
              
              Case gcINST_FFMM_RF_NAC, gcINST_FFMM_RV_NAC, gcINST_FFMM_FIP_NAC, gcINST_FFMM_CA_NAC, gcINST_FFMM_FI_NAC  'JGR 190509
                Set lcSaldos_FFMM = New Class_FondosMutuos
                With lcSaldos_FFMM
                      If Not .Saldo_Activo_Cantidad(pId_Cuenta, pId_Nemotecnico) < pCantidad Then
                         Fnt_CompruebaRestriccionVenta = False
                      End If
                End With
                
         End Select
          
          
          
'*************************************************************************************************
'          Set lcSaldos_Activos = New Class_Saldo_Activos
'          With lcSaldos_Activos
'            .Campo("id_Cuenta").Valor = pId_Cuenta
'            .Campo("id_Nemotecnico").Valor = pId_Nemotecnico
'            If Not .Fnt_Buscar_Saldo_Cuenta_Nemo(0, pCantidad, Val(lReg("CANTIDAD_RESTRINGIR").Value)) Then
'              Fnt_CompruebaRestriccionVenta = False
'            End If
'          End With
'          Set lcSaldos_Activos = Nothing
'*************************************************************************************************
        End If
      Next
    End If
  End With
  Set lcRestricciones_Venta = Nothing
  
End Function

Public Function Fnt_Valida_Perfil_Cuenta_Instrm_Nemo(pGrilla As VSFlexGrid, _
                                                     pId_Cuenta, _
                                                     pCod_Instrumento) As Boolean
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lId_Perfil_Riesgo As String
Dim lDsc_Perfil_Riesgo As String
'--------------------------------------
Dim lLinea As Integer
Dim lcRel_PRiesgo_Instrumento As Class_Rel_PRiesgo_Instrumento
Dim lMensaje As String
'--------------------------------------
Dim lcRel_Perfiles_Nemotec As Class_Rel_Perfiles_Nemotec
Dim lId_Nemotecnico As String
'--------------------------------------
'Dim lcInstrumento As Class_Instrumentos
'Dim lDsc_Instrumento As String

  Fnt_Valida_Perfil_Cuenta_Instrm_Nemo = True
  
  Rem Busca el Perfil de Riesgo de la Cuenta
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cuenta").Valor = pId_Cuenta
    If .Buscar(True) Then
      If .Cursor.Count > 0 Then
        lId_Perfil_Riesgo = .Cursor(1)("id_perfil_riesgo").Value
        lDsc_Perfil_Riesgo = .Cursor(1)("DSC_PERFIL_RIESGO").Value
      Else
        MsgBox "La Cuenta no tiene asociado un Perfil de Riesgo.", vbCritical
        Set lcCuenta = Nothing
        GoTo ErrProcedure
      End If
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar datos de la Cuenta.", _
                        .ErrMsg, _
                        pConLog:=True)
      Set lcCuenta = Nothing
      GoTo ErrProcedure
    End If
  End With
  '---------------------------------------------------------------------------------------
  
  Rem Busca en Perfil de Riesgo/Instrumento si existe la relacion
  Set lcRel_PRiesgo_Instrumento = New Class_Rel_PRiesgo_Instrumento
  With lcRel_PRiesgo_Instrumento
    .Campo("id_perfil_riesgo").Valor = lId_Perfil_Riesgo
    .Campo("cod_instrumento").Valor = pCod_Instrumento
    If .Buscar Then
      If .Cursor.Count > 0 Then
        Rem Si existe la relacion entonces sale de la validacion como verdadero
        Rem Los nemotecnicos estan asociados al perfil de riesgo a traves del Instrumento
        Set lcRel_PRiesgo_Instrumento = Nothing
        Exit Function
'      Else
'        Rem Busca el nombre del Instrumento para el mensaje
'          Set lcInstrumento = New Class_Instrumentos
'          With lcInstrumento
'            .Campo("cod_instrumento").Valor = pCod_Instrumento
'            If .Buscar Then
'              If .Cursor.Count > 0 Then
'                lDsc_Instrumento = .Cursor(1)("dsc_intrumento").Value
'              End If
'            End If
'          End With
'          Set lcInstrumento = Nothing
'
'        lMensaje = "El Instrumento " & lDsc_Instrumento & _
'                   " no esta asignado al Perfil de Riesgo " & _
'                   lDsc_Perfil_Riesgo & ", que está asociado a la cuenta." & _
'                   vbCr & vbCr & "¿Desea continuar con la operación?"
'        If MsgBox(lMensaje, vbQuestion + vbYesNo) = vbNo Then
'          Set lcRel_PRiesgo_Instrumento = Nothing
'          GoTo ErrProcedure
'        End If
      End If
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Relación Perfil de Riesgo/Instrumentos.", _
                        .ErrMsg, _
                        pConLog:=True)
      Set lcRel_PRiesgo_Instrumento = Nothing
      GoTo ErrProcedure
    End If
  End With
  Set lcRel_PRiesgo_Instrumento = Nothing
  '---------------------------------------------------------------------------------------
  
  Rem Si no encontró la relacion Perfil de Riesgo/Instrumento o el usuario permite la
  Rem operación busca por cada Nemotécnico de la grilla en Perfil de Riesgo/Nemotecnico
  Rem si existe la relacion
  For lLinea = 1 To (pGrilla.Rows - 1)
    lMensaje = ""
    lId_Nemotecnico = GetCell(pGrilla, lLinea, "id_nemotecnico")
    Set lcRel_Perfiles_Nemotec = New Class_Rel_Perfiles_Nemotec
    With lcRel_Perfiles_Nemotec
      .Campo("id_nemotecnico").Valor = lId_Nemotecnico
      .Campo("id_perfil_riesgo").Valor = lId_Perfil_Riesgo
      If .Buscar_Perfiles_Nemotecnico Then
        If Not .Cursor.Count > 0 Then
          Rem No hay relacion entre el Perfil de Riesgo de la Cuenta y los Instrumentos
          lMensaje = "El Nemotécnico " & GetCell(pGrilla, lLinea, "dsc_nemotecnico") & _
                     " no esta asignado al Perfil de Riesgo " & _
                     lDsc_Perfil_Riesgo & " asociado a la cuenta." & _
                     vbCr & vbCr & "¿Desea continuar con la operación?"
          If MsgBox(lMensaje, vbQuestion + vbYesNo) = vbNo Then
            Set lcRel_Perfiles_Nemotec = Nothing
            GoTo ErrProcedure
          End If
        End If
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar Relación Perfil de Riesgo/Nemotécnicos.", _
                          .ErrMsg, _
                          pConLog:=True)
        Set lcRel_Perfiles_Nemotec = Nothing
        GoTo ErrProcedure
      End If
    End With
    Set lcRel_Perfiles_Nemotec = Nothing
  Next lLinea
  
  Exit Function
  
ErrProcedure:
  Fnt_Valida_Perfil_Cuenta_Instrm_Nemo = False
  
End Function

Public Function Fnt_EnvioEMAIL_Trader(pId_Operacion, Optional pCC As String = "") As Boolean
Dim lcOperacion         As Class_Operaciones
Dim lcRepositorio_Ascii As Class_Repositorio_Ascii
Dim lcTreader           As Class_Traders
Dim lcTipo_Operacion    As Class_Tipos_Operaciones
'Dim lcCuenta            As Class_Cuentas
Dim lcCuenta            As Object
Dim lcNemotecnico       As Class_Nemotecnicos
Dim lcCliente           As Object
Dim lcBoletaOperacion   As Object
Dim lReg                As hFields
'---------------------------------------------------------
Dim lEmailPiePagina     As String
Dim lEMailCuerpo        As String
Dim lEMailDetalle       As String
Dim lEMailDet_Reem      As String
Dim lEMail              As String
Dim lCamposReemplazo    As hRecord
Dim lDestinatarios      As hRecord
Dim lCaracterPos        As Long
'---------------------------------------------------------
Dim lDsc_Trader As String
Dim lDsc_Operacion As String
Dim lDsc_TipoOperacion As String
Dim lRut_Cliente As String
Dim lNombreCliente As String
Dim lNemotecnico  As String
'---------------------------------------------------------
Dim lArchivoPDF As String
Dim lrArchivosPDFs As hRecord

'-------------------------------------------------------------------------------------
Const cMsgNoPlanilla = "No esta definido la plantilla para enviar el E-Mail"
Const cMsgProblMail = "Problemas en el envio de email."
'-------------------------------------------------------------------------------------
Const cDetalleInstruccion = "<<DETALLE_INSTRUCCION>>"
Const cDetalleInstruccionFIN = "<</DETALLE_INSTRUCCION>>"
'-------------------------------------------------------------------------------------

  Fnt_EnvioEMAIL_Trader = False
  
  'BUSCA LA OPERACION
  Set lcOperacion = New Class_Operaciones
  With lcOperacion
    .Campo("id_operacion").Valor = pId_Operacion
    If Not .BuscaConDetalles Then
      Call Fnt_MsgError(.SubTipo_LOG, cMsgProblMail, .ErrMsg)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      Call MsgBox("No existe la operación (" & pId_Operacion & ") buscada.", vbInformation, "Envio de E-Mail")
      GoTo ExitProcedure
    End If
  End With
  
  If IsNull(lcOperacion.Campo("id_trader").Valor) Then
    'si es null significa que no se ha elejido el trader para la operacion.
    GoTo NoError
  End If
  
  'BUSCA EL TRADER
  Set lcTreader = New Class_Traders
  With lcTreader
    .Campo("id_trader").Valor = lcOperacion.Campo("id_trader").Valor
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, cMsgProblMail, .ErrMsg)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      'si no existe el trader no significa que tenga un error
      GoTo NoError
    End If
  
    lEMail = Trim("" & .Cursor(1)("EMAIL_TRADER").Value)
    lDsc_Trader = lcTreader.Cursor(1)("DSC_TRADER").Value
    
    If lEMail = "" Then
      'si no existe el email del trader no significa que tenga un error
      GoTo NoError
    End If
  End With
  Set lcTreader = Nothing
  
  'busca la descripcion del tipo de la operacion
  Set lcTipo_Operacion = New Class_Tipos_Operaciones
  With lcTipo_Operacion
    .Campo("Cod_Tipo_Operacion").Valor = lcOperacion.Campo("Cod_Tipo_Operacion").Valor
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, cMsgProblMail, .ErrMsg)
      GoTo ExitProcedure
    End If
    
    lDsc_Operacion = .Cursor(1)("Dsc_Tipo_Operacion").Value
    
    lDsc_TipoOperacion = .Cursor(1)(IIf(lcOperacion.Campo("FLG_TIPO_MOVIMIENTO").Valor = gcTipoOperacion_Ingreso, "DSC_TIPO_MOV_INGRESO", "DSC_TIPO_MOV_EGRESO"))
  End With
  Set lcTipo_Operacion = Nothing
  
  'busca el nombre del cliente
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cuenta").Valor = lcOperacion.Campo("id_cuenta").Valor
    If Not .Buscar(True) Then
      Call Fnt_MsgError(.SubTipo_LOG, cMsgProblMail, .ErrMsg)
      GoTo ExitProcedure
    End If
    
    lRut_Cliente = .Cursor(1)("rut_cliente").Value
    lNombreCliente = .Cursor(1)("Nombre_cliente").Value
  End With
  
  'BUSCA LA PLANILLA PARA EL EMAIL
  Set lcRepositorio_Ascii = New Class_Repositorio_Ascii
  With lcRepositorio_Ascii
    .Campo("COD_REPOSITORIO").Valor = cRep_ASCII_Operaciones
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, cMsgProblMail, .ErrMsg)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      Call MsgBox(cMsgNoPlanilla, vbInformation, "Envio de E-Mail")
      GoTo ExitProcedure
    End If
    
    lEMailCuerpo = .Cursor(1)("TEXTO_REPOSITORIO").Value
    
    If Trim(lEMailCuerpo) = "" Then
      Call MsgBox(cMsgNoPlanilla, vbInformation, "Envio de E-Mail")
      GoTo ExitProcedure
    End If
  End With
  Set lcRepositorio_Ascii = Nothing
  
  Set lCamposReemplazo = Nothing
  Call Fnt_AgregaCampoReemplazo(lCamposReemplazo, "<<NOM_TRADER>>", lDsc_Trader)
  Call Fnt_AgregaCampoReemplazo(lCamposReemplazo, "<<DSC_OPERACION>>", lDsc_Operacion)
  Call Fnt_AgregaCampoReemplazo(lCamposReemplazo, "<<DSC_TIPO_OPERACION>>", lDsc_TipoOperacion)
  Call Fnt_AgregaCampoReemplazo(lCamposReemplazo, "<<RUT_CLIENTE>>", lRut_Cliente)
  Call Fnt_AgregaCampoReemplazo(lCamposReemplazo, "<<NOM_CLIENTE>>", lNombreCliente)
  
  lEMailCuerpo = Fnt_ReemplazarTexto(lEMailCuerpo, lCamposReemplazo)
  
  'AQUI COLOCA EL DETALLE DE LA INSTRUCCION
  lCaracterPos = InStr(1, lEMailCuerpo, cDetalleInstruccion, vbBinaryCompare)
  lEMailDetalle = Mid(lEMailCuerpo, lCaracterPos + Len(cDetalleInstruccion)) 'BUSCA EL DETALLE DEL EMAIL PARA LAS OPERACIONES
  lEMailCuerpo = Mid(lEMailCuerpo, 1, lCaracterPos - 1) 'DEJA EL CUERPO GUARDADO SIN DETALLE NI PIE DE PAGINA
  
  lCaracterPos = InStr(1, lEMailDetalle, cDetalleInstruccionFIN)
  lEmailPiePagina = Mid(lEMailDetalle, lCaracterPos + Len(cDetalleInstruccionFIN)) 'CAPTURA EL PIE DE PAGINA
  lEMailDetalle = Mid(lEMailDetalle, 1, lCaracterPos - 1) 'DEJA SOLO EL PIE DE PAGINA
  
  For Each lReg In lcOperacion.Cursor
    lNemotecnico = ""
    
    Set lcNemotecnico = New Class_Nemotecnicos
    With lcNemotecnico
      .Campo("id_nemotecnico").Valor = lReg("id_nemotecnico").Value
      If Not .Buscar Then
        Call Fnt_MsgError(.SubTipo_LOG, cMsgProblMail, .ErrMsg)
        GoTo ExitProcedure
      End If
      
      lNemotecnico = .Cursor(1)("nemotecnico").Value
    End With
    Set lcNemotecnico = Nothing
    
    Set lCamposReemplazo = Nothing
    Call Fnt_AgregaCampoReemplazo(lCamposReemplazo, "<<LIN_NEMOTECNICO>>", lNemotecnico, 35)
    Call Fnt_AgregaCampoReemplazo(lCamposReemplazo, "<<LIN_CANTIDAD>>", Format(lReg("cantidad").Value, "#,##0.####"), 15, vbAlignRight)
    Call Fnt_AgregaCampoReemplazo(lCamposReemplazo, "<<LIN_PRECIO>>", Format(lReg("precio").Value, "#,##0.####"), 15, vbAlignRight)
    Call Fnt_AgregaCampoReemplazo(lCamposReemplazo, "<<LIN_MONTO>>", Format(lReg("monto_pago").Value, Fnt_Formato_Moneda(lReg("id_moneda_pago").Value)), 20, vbAlignRight)
    
    'Cambia los valores del detalle de las operaciones
    lEMailDet_Reem = Fnt_ReemplazarTexto(lEMailDetalle, lCamposReemplazo)
    
    'Agrega el detalle al cuerpo
    lEMailCuerpo = lEMailCuerpo & lEMailDet_Reem
  Next
  
  'se agrega el pie de pagina
  lEMailCuerpo = lEMailCuerpo & lEmailPiePagina
  
  Set lDestinatarios = Nothing
  Call Fnt_AgregarDestinatario(pRegDestinatarios:=lDestinatarios, pDisplayName:=lDsc_Trader, pEmail:=lEMail)
  
  
  Set lrArchivosPDFs = Nothing
  
  'Generacion de
  Set lcBoletaOperacion = Fnt_IniciaFormulario(pCod_Proceso_Componente:=cPC_ReporteOperacion _
                                              , pMostrarError:=False)
  If Not lcBoletaOperacion Is Nothing Then
    lArchivoPDF = lcBoletaOperacion.Generar_PDF(pId_Operacion:=lcOperacion.Campo("id_operacion").Valor _
                                              , pArchivoPDF:=lArchivoPDF)
     'lcBoletaOperacion.Hide
    Call Prc_MAPI.Fnt_AgregarArchivoAdjunto(lrArchivosPDFs, lArchivoPDF)
  End If
   
  
  'Modificado por MMA 12/11/2008. Se agrega mail con Copia
  Call Prc_MAPI.Fnt_Enviar("Ingreso de Operacion", lEMailCuerpo, lDestinatarios, True, lrArchivosPDFs, pCC)
  
NoError:
  Fnt_EnvioEMAIL_Trader = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en el envio de email.", Err.Description)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcOperacion = Nothing
  Set lcRepositorio_Ascii = Nothing
  Set lcTreader = Nothing
  Set lcCliente = Nothing
  Set lcTipo_Operacion = Nothing
  Set lCamposReemplazo = Nothing
  Set lDestinatarios = Nothing
  Set lcNemotecnico = Nothing
End Function


Public Function Fnt_EnvioContraparte(pId_Operacion, ByRef pErrOrden As String, Optional pTipoLiquidacion As String = "CN") As Boolean
Dim lcOperacion         As Class_Operaciones
Dim lcRel_Conversion    As Object
'------------------------------------------------------------------------
Dim lCodClase           As String
Dim lClase              As Object
Dim lMensaje            As String
Dim lId_Tipo_Conversion As Double
Dim lExterno As String

Const cMsgErr = "Problemas con el envio a la contraparte."

  Fnt_EnvioContraparte = False
  
  'BUSCA LA OPERACION
  Set lcOperacion = New Class_Operaciones
  With lcOperacion
    .Campo("id_operacion").Valor = pId_Operacion
    If Not .BuscaConDetalles Then
      Call Fnt_MsgError(.SubTipo_LOG, cMsgErr, .ErrMsg)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      Call MsgBox("No existe la operación (" & pId_Operacion & ") buscada.", vbInformation, "Envio de E-Mail")
      GoTo ExitProcedure
    End If
  End With

  lCodClase = "ENVIO_CONTRA_"

  'Agrega el tipo de operacion
  lCodClase = lCodClase & lcOperacion.Campo("COD_TIPO_OPERACION").Valor & "_"
    
  'agrega el tipo de instrumento
  lCodClase = lCodClase & lcOperacion.Campo("cod_instrumento").Valor

  lMensaje = ""
  Set lClase = Fnt_IniciaComponente(pCod_Proceso_Componente:=lCodClase _
                                  , pMensaje:=lMensaje _
                                  , pErrorNoExiste:=False)
  If lClase Is Nothing Then
    If Not lMensaje = "" Then
      MsgBox lMensaje, vbExclamation, "Inicio de componente"
      GoTo ErrProcedure
    End If
    
    GoTo NoError
  End If
  
  If lCodClase = "ENVIO_CONTRA_INST_ACCIONES_NAC" Then
        lExterno = "EXT"
        With lClase
          Set .gDB = gDB
          
          If Not .IngresoOperacion(lcOperacion, pTipoLiquidacion, lExterno) Then
          End If
          
          If Mid(.ErrMsg, 1, 11) = "Error Magic" Then
             pErrOrden = .ErrMsg
             GoTo ExitProcedure
          End If
          
          GoTo NoError
        
        End With
  
  Else
        lExterno = "EXT"
        With lClase
          Set .gDB = gDB
          
          If Not .IngresoOperacion(lcOperacion, pExterno:=lExterno) Then
          End If
          
          If Mid(.ErrMsg, 1, 11) = "Error Magic" Then
             pErrOrden = .ErrMsg
             GoTo ExitProcedure
          End If
          
          GoTo NoError
          
        End With
    End If
  
  
NoError:
  Fnt_EnvioContraparte = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en el envio de la oepración a la contraparte.", Err.Description)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If
  

ExitProcedure:
  Set lcOperacion = Nothing
End Function

Public Function Fnt_CreaNemotecnico_Deposito(pCodigoEmisor As String _
                                             , pFecha_Vencimiento As Date _
                                             , PId_Moneda_Deposito As Double _
                                             , pId_Moneda_Pago As Double _
                                             , pTasa_Emision As Double _
                                             , pFecha_Emision As Date _
                                             , ByRef pId_Nemotecnico As Double _
                                             , ByRef pNemotecnico As String _
                                             , ByRef pMensaje As String _
                                             , Optional pGrilla As VSFlexGrid = Nothing) As Boolean
Dim lcNemotecnico       As Class_Nemotecnicos
Dim lcEmisor_Especifico As Class_Emisores_Especifico
'------------------------------------------------------
Dim lId_Emisor_Especifico As String
Dim lNemotecnico          As String
Dim lId_Subfamilia        As Double
Dim lBase                 As Double
  
  Fnt_CreaNemotecnico_Deposito = False
  pMensaje = "No se pudo crear el nemotecnico."
  
  Select Case PId_Moneda_Deposito
    Case 1 'peso
      lId_Subfamilia = 16
      lBase = 30
    Case 4 'uf
      lId_Subfamilia = 26
      lBase = 360
    Case 2 'dolar
      lId_Subfamilia = 27
      lBase = 30
  End Select
  
  
  Set lcEmisor_Especifico = New Class_Emisores_Especifico
  With lcEmisor_Especifico
    .Campo("COD_SVS_NEMOTECNICO").Valor = pCodigoEmisor
    If Not .Buscar() Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en grabar el Nemotécnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      pMensaje = "No existe una emisor para el codigo """ & pCodigoEmisor & """, es imposible realizar la operación."
      GoTo ExitProcedure
    End If
    
    lId_Emisor_Especifico = .Cursor(1)("id_emisor_especifico").Value
  End With
  
  'GENERACION DEL NEMOTECNICO
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
    .Campo("Fecha_Vencimiento").Valor = pFecha_Vencimiento
    lNemotecnico = .Fnt_Generacion_Nemo_SVS(PId_Moneda_Deposito, pId_Moneda_Pago)
  End With
  Set lcNemotecnico = Nothing
  
  'Busca primero el nemotecnico antes de crearlo
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("Cod_Instrumento").Valor = gcINST_DEPOSITOS_NAC
    .Campo("nemotecnico").Valor = lNemotecnico
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en buscar el Nemotécnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
  
    If .Cursor.Count > 0 Then
      pId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
    Else
      Call Fnt_Escribe_Grilla(pGrilla, "I", Space(8) & "Creando el nemotécnico """ & lNemotecnico & """.")
      
      'CREA EL NEMOTECNICO
      Set lcNemotecnico = New Class_Nemotecnicos
      With lcNemotecnico
        .Campo("id_Nemotecnico").Valor = cNewEntidad
        .Campo("cod_Instrumento").Valor = gcINST_DEPOSITOS_NAC
        .Campo("nemotecnico").Valor = lNemotecnico
        .Campo("id_subfamilia").Valor = lId_Subfamilia
        .Campo("nemotecnico").Valor = lNemotecnico
        .Campo("id_Mercado_Transaccion").Valor = cMercado_Transac_Santiago
        .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
        .Campo("id_Emisor_Especifico_Origen").Valor = lId_Emisor_Especifico
        .Campo("id_Moneda").Valor = PId_Moneda_Deposito
        .Campo("id_Moneda_transaccion").Valor = pId_Moneda_Pago
        .Campo("id_Tipo_Estado").Valor = cTEstado_Nemotecnico
        .Campo("cod_Estado").Valor = cCod_Estado_Vigente
        .Campo("dsc_Nemotecnico").Valor = lNemotecnico
        .Campo("tasa_Emision").Valor = pTasa_Emision
        .Campo("tipo_Tasa").Valor = ""
        .Campo("periodicidad").Valor = ""
        .Campo("fecha_Vencimiento").Valor = pFecha_Vencimiento
        .Campo("corte_Minimo_Papel").Valor = 1 '
        .Campo("monto_Emision").Valor = ""
        .Campo("liquidez").Valor = ""
        .Campo("base").Valor = lBase
        .Campo("cod_Pais").Valor = gcPais_Chile
        .Campo("flg_Fungible").Valor = ""
        .Campo("fecha_emision").Valor = pFecha_Emision
        
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en grabar el Nemotécnico.", _
                            .ErrMsg, _
                            pConLog:=True)
          GoTo ExitProcedure
        End If
        
        pId_Nemotecnico = .Campo("id_nemotecnico").Valor
      End With
    End If
  End With

  pMensaje = ""
  Fnt_CreaNemotecnico_Deposito = True

ExitProcedure:
  Set lcNemotecnico = Nothing
  Set lcEmisor_Especifico = Nothing
  pNemotecnico = lNemotecnico
End Function

Public Function Fnt_CreaNemotecnico_Pacto(pCodigoEmisor As String _
                                        , pFecha_Vencimiento As Date _
                                        , PId_Moneda_Deposito As Double _
                                        , pId_Moneda_Pago As Double _
                                        , pTasa_Emision As Double _
                                        , pFecha_Emision As Date _
                                        , ByRef pId_Nemotecnico As Double _
                                        , ByRef pNemotecnico As String _
                                        , ByRef pMensaje As String _
                                        , Optional pGrilla As VSFlexGrid = Nothing) As Boolean
Dim lcNemotecnico       As Class_Nemotecnicos
Dim lcEmisor_Especifico As Class_Emisores_Especifico
'------------------------------------------------------
Dim lId_Emisor_Especifico As String
Dim lNemotecnico          As String
Dim lId_Subfamilia        As Double
Dim lBase                 As Double
  
  Fnt_CreaNemotecnico_Pacto = False
  pMensaje = "No se pudo crear el nemotecnico."
  
  Select Case PId_Moneda_Deposito
    Case 1 'peso
      lId_Subfamilia = 16
      lBase = 30
    Case 4 'uf
      lId_Subfamilia = 26
      lBase = 360
    Case 2 'dolar
      lId_Subfamilia = 27
      lBase = 30
  End Select
  
  
  Set lcEmisor_Especifico = New Class_Emisores_Especifico
  With lcEmisor_Especifico
    .Campo("COD_SVS_NEMOTECNICO").Valor = pCodigoEmisor
    If Not .Buscar() Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en grabar el Nemotécnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      pMensaje = "No existe una emisor para el codigo """ & pCodigoEmisor & """, es imposible realizar la operación."
      GoTo ExitProcedure
    End If
    
    lId_Emisor_Especifico = .Cursor(1)("id_emisor_especifico").Value
  End With
  
  'GENERACION DEL NEMOTECNICO
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
    .Campo("Fecha_Vencimiento").Valor = pFecha_Vencimiento
    lNemotecnico = "P" & .Fnt_Generacion_Nemo_SVS(PId_Moneda_Deposito, pId_Moneda_Pago)
  End With
  Set lcNemotecnico = Nothing
  
  'Busca primero el nemotecnico antes de crearlo
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("Cod_Instrumento").Valor = gcINST_PACTOS_NAC
    .Campo("nemotecnico").Valor = lNemotecnico
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en buscar el Nemotécnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
  
    If .Cursor.Count > 0 Then
      pId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
    Else
      Call Fnt_Escribe_Grilla(pGrilla, "I", Space(8) & "Creando el nemotécnico """ & lNemotecnico & """.")
      
      'CREA EL NEMOTECNICO
      Set lcNemotecnico = New Class_Nemotecnicos
      With lcNemotecnico
        .Campo("id_Nemotecnico").Valor = cNewEntidad
        .Campo("cod_Instrumento").Valor = gcINST_PACTOS_NAC
        .Campo("nemotecnico").Valor = lNemotecnico
        .Campo("id_subfamilia").Valor = lId_Subfamilia
        .Campo("nemotecnico").Valor = lNemotecnico
        .Campo("id_Mercado_Transaccion").Valor = cMercado_Transac_Santiago
        .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
        .Campo("id_Emisor_Especifico_Origen").Valor = lId_Emisor_Especifico
        .Campo("id_Moneda").Valor = PId_Moneda_Deposito
        .Campo("id_Moneda_transaccion").Valor = pId_Moneda_Pago
        .Campo("id_Tipo_Estado").Valor = cTEstado_Nemotecnico
        .Campo("cod_Estado").Valor = cCod_Estado_Vigente
        .Campo("dsc_Nemotecnico").Valor = lNemotecnico
        .Campo("tasa_Emision").Valor = pTasa_Emision
        .Campo("tipo_Tasa").Valor = ""
        .Campo("periodicidad").Valor = ""
        .Campo("fecha_Vencimiento").Valor = pFecha_Vencimiento
        .Campo("corte_Minimo_Papel").Valor = 1 '
        .Campo("monto_Emision").Valor = ""
        .Campo("liquidez").Valor = ""
        .Campo("base").Valor = lBase
        .Campo("cod_Pais").Valor = gcPais_Chile
        .Campo("flg_Fungible").Valor = ""
        .Campo("fecha_emision").Valor = pFecha_Emision
        
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en grabar el Nemotécnico.", _
                            .ErrMsg, _
                            pConLog:=True)
          GoTo ExitProcedure
        End If
        
        pId_Nemotecnico = .Campo("id_nemotecnico").Valor
      End With
    End If
  End With

  pMensaje = ""
  Fnt_CreaNemotecnico_Pacto = True

ExitProcedure:
  Set lcNemotecnico = Nothing
  Set lcEmisor_Especifico = Nothing
  pNemotecnico = lNemotecnico
End Function

Public Function Fnt_CreaNemotecnico_Bonos(pNemotecnico As String _
                                        , pCodigoEmisor As String _
                                        , pFecha_Vencimiento As Date _
                                        , pId_Moneda As Double _
                                        , pId_Moneda_Transaccion As Double _
                                        , pTasa_Emision As Double _
                                        , pFecha_Emision As Date _
                                        , ByRef pId_Nemotecnico As Double _
                                        , ByRef pMensaje As String _
                                        , Optional pGrilla As VSFlexGrid = Nothing) As Boolean
Dim lcNemotecnico       As Class_Nemotecnicos
Dim lcEmisor_Especifico As Class_Emisores_Especifico
'------------------------------------------------------
Dim lId_Emisor_Especifico As String
Dim lNemotecnico          As String
Dim lId_Subfamilia        As Double
Dim lBase                 As Double
  
  Fnt_CreaNemotecnico_Bonos = False
  pMensaje = "No se pudo crear el nemotecnico."
  
  lNemotecnico = pNemotecnico
  
  'Busca primero el nemotecnico antes de crearlo
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("Cod_Instrumento").Valor = gcINST_BONOS_NAC
    .Campo("nemotecnico").Valor = lNemotecnico
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en buscar el Nemotécnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
  
    If .Cursor.Count > 0 Then
      pId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
    Else
      Call Fnt_Escribe_Grilla(pGrilla, "I", Space(8) & "Creando el nemotécnico """ & lNemotecnico & """.")
      
      Set lcEmisor_Especifico = New Class_Emisores_Especifico
      With lcEmisor_Especifico
        .Campo("COD_SVS_NEMOTECNICO").Valor = pCodigoEmisor
        If Not .Buscar() Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en grabar el Nemotécnico.", _
                            .ErrMsg, _
                            pConLog:=True)
          GoTo ExitProcedure
        End If
        
        If .Cursor.Count <= 0 Then
          pMensaje = "No existe una emisor para el codigo """ & pCodigoEmisor & """, es imposible realizar la operación."
          GoTo ExitProcedure
        End If
        
        lId_Emisor_Especifico = .Cursor(1)("id_emisor_especifico").Value
      End With
      
      'CREA EL NEMOTECNICO
      Set lcNemotecnico = New Class_Nemotecnicos
      With lcNemotecnico
        .Campo("id_Nemotecnico").Valor = cNewEntidad
        .Campo("cod_Instrumento").Valor = gcINST_BONOS_NAC
        .Campo("nemotecnico").Valor = lNemotecnico
        .Campo("id_subfamilia").Valor = Null
        .Campo("nemotecnico").Valor = lNemotecnico
        .Campo("id_Mercado_Transaccion").Valor = cMercado_Transac_Santiago
        .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
        .Campo("id_Emisor_Especifico_Origen").Valor = lId_Emisor_Especifico
        .Campo("id_Moneda").Valor = pId_Moneda
        .Campo("id_Moneda_transaccion").Valor = pId_Moneda_Transaccion
        .Campo("id_Tipo_Estado").Valor = cTEstado_Nemotecnico
        .Campo("cod_Estado").Valor = cCod_Estado_Vigente
        .Campo("dsc_Nemotecnico").Valor = lNemotecnico
        .Campo("tasa_Emision").Valor = pTasa_Emision
        .Campo("tipo_Tasa").Valor = ""
        .Campo("periodicidad").Valor = ""
        .Campo("fecha_Vencimiento").Valor = pFecha_Vencimiento
        .Campo("corte_Minimo_Papel").Valor = 1 '
        .Campo("monto_Emision").Valor = ""
        .Campo("liquidez").Valor = ""
        .Campo("base").Valor = Null
        .Campo("cod_Pais").Valor = gcPais_Chile
        .Campo("flg_Fungible").Valor = ""
        .Campo("fecha_emision").Valor = pFecha_Emision
        
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en grabar el Nemotécnico.", _
                            .ErrMsg, _
                            pConLog:=True)
          GoTo ExitProcedure
        End If
        
        pId_Nemotecnico = .Campo("id_nemotecnico").Valor
      End With
    End If
  End With

  pMensaje = ""
  Fnt_CreaNemotecnico_Bonos = True

ExitProcedure:
  Set lcNemotecnico = Nothing
  Set lcEmisor_Especifico = Nothing
  pNemotecnico = lNemotecnico
End Function


Public Function Fnt_Form_BoletaOperacion() As Boolean
Dim lcClase As Object
  
  Fnt_Form_BoletaOperacion = False

  Set lcClase = Fnt_IniciaFormulario(pCod_Proceso_Componente:=cPC_ReporteOperacion)
  If lcClase Is Nothing Then
    GoTo ExitProcedure
  End If
  
  Fnt_Form_BoletaOperacion = True

ExitProcedure:

End Function

Public Function Fnt_Validad_Operacion(pCod_Tipo_Operacion _
                                    , pFlg_Tipo_Movimiento _
                                    , pId_Cuenta _
                                    , pId_Emisor _
                                    , pId_Nemotecnico _
                                    , pFecha_Operacion _
                                    , Optional pCantidad _
                                    , Optional pId_Moneda _
                                    , Optional pMonto _
                                    , Optional ByRef pMsgError) As Boolean

  Fnt_Validad_Operacion = False

  Select Case pFlg_Tipo_Movimiento
    Case gcTipoOperacion_Ingreso
      Select Case pCod_Tipo_Operacion
        Case gcOPERACION_Directa, gcOPERACION_Instruccion
          'Validacion de Restriccion de Compra
          If Not Fnt_CompruebaRestriccionCompra(pId_Cuenta:=pId_Cuenta _
                                              , pId_Emisor:=pId_Emisor _
                                              , pId_Nemotecnico:=pId_Nemotecnico) Then
            pMsgError = "Existen Nemotécnicos que presentan restricción de compra asociada a la Cuenta," & vbCr & vbCr & "No se puede operar."
            GoTo ExitProcedure
          End If
      End Select
      
      'las validaciones de ingreso de instrucciones son solo para las compras
      Select Case pCod_Tipo_Operacion
        Case gcOPERACION_Instruccion
          If Not Fnt_Val_RestriccPorc_Inversion(pId_Cuenta:=pId_Cuenta _
                                              , pId_Nemotecnico:=pId_Nemotecnico _
                                              , pFecha_Operacion:=pFecha_Operacion _
                                              , pId_Moneda:=pId_Moneda _
                                              , pMonto:=pMonto _
                                              , pMsgError:=pMsgError) Then
            GoTo ExitProcedure
          End If
      End Select
      
    Case gcTipoOperacion_Egreso
      Select Case pCod_Tipo_Operacion
        Case gcOPERACION_Directa, gcOPERACION_Instruccion
          'Validacion de Restriciones de Venta
          If Not Fnt_CompruebaRestriccionVenta(pId_Cuenta:=pId_Cuenta _
                                             , pId_Emisor:=pId_Emisor _
                                             , pId_Nemotecnico:=pId_Nemotecnico _
                                             , pCantidad:=pCantidad) Then
            pMsgError = "Existen Nemotécnicos que presentan restricción de Venta asociada a la Cuenta," & vbCr & vbCr & "No se puede operar."
            GoTo ExitProcedure
          End If
      End Select
  End Select

  Fnt_Validad_Operacion = True

ExitProcedure:

End Function

Public Function Fnt_Val_RestriccPorc_Inversion(pId_Cuenta _
                                              , pId_Nemotecnico _
                                              , pFecha_Operacion _
                                              , pId_Moneda _
                                              , pMonto _
                                              , Optional ByRef pMsgError) As Boolean
Dim lcRestricc_Porc_Producto As Class_Restricc_Porc_Producto

  Fnt_Val_RestriccPorc_Inversion = False

  Set lcRestricc_Porc_Producto = New Class_Restricc_Porc_Producto
  With lcRestricc_Porc_Producto
    If Not .Validar_Operacion(pId_Cuenta:=pId_Cuenta _
                            , pId_Nemotecnico:=pId_Nemotecnico _
                            , pId_Moneda:=pId_Moneda _
                            , pFecha_Operacion:=pFecha_Operacion _
                            , pMonto:=pMonto) Then
      pMsgError = .ErrMsg
      GoTo ExitProcedure
    End If
    
    If Not .Errnum = 0 Then
      pMsgError = .ErrMsg
      GoTo ExitProcedure
    End If
  End With
  
  Fnt_Val_RestriccPorc_Inversion = True
  
ExitProcedure:

  Set lcRestricc_Porc_Producto = New Class_Restricc_Porc_Producto
End Function

Public Function Fnt_Enlaza_VentasCompras_RF(pCod_Instrumento _
                                          , pId_Cuenta _
                                          , pId_Nemotecnico _
                                          , pCantidad _
                                          , pFecha_Movimiento _
                                          , ByRef phActivos As hRecord _
                                          , Optional pId_Mov_Activo = Null) As Boolean
Dim lcSaldo_Activo  As Class_Saldo_Activos
Dim lcMov_Activos   As Class_Mov_Activos
Dim lcClase         As Object
Dim lReg            As hFields
Dim lhActivos       As hRecord
'----------------------------------------------
Dim lSaldo_Cantidad  As Double
Dim lCant_Pend_Mov_Activos As Double

  Fnt_Enlaza_VentasCompras_RF = False
  
  Set lhActivos = New hRecord
  With lhActivos
    .AddField "Id_Mov_Activo"
    .AddField "Cantidad"
    .AddField "Asignado", 0
    .AddField "TotalCantidad", 0
  End With
  
  Select Case pCod_Instrumento
    Case gcINST_BONOS_NAC
      Set lcClase = New Class_Bonos
    Case gcINST_DEPOSITOS_NAC
      Set lcClase = New Class_Depositos
    Case gcINST_PACTOS_NAC
      Set lcClase = New Class_Pactos
  End Select
  
  Set lcSaldo_Activo = New Class_Saldo_Activos
  With lcSaldo_Activo
    If Not .Buscar_al_cierre(pFecha_Cierre:=pFecha_Movimiento - 1 _
                           , pId_Cuenta:=pId_Cuenta _
                           , pId_Nemotecnico:=pId_Nemotecnico _
                           , pId_Mov_Activo:=pId_Mov_Activo, pCod_Instrumento:=pCod_Instrumento) Then
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count > 0 Then
        For Each lReg In .Cursor
           If pCod_Instrumento = "BONOS_NAC" Then '*** RACV
              With lhActivos.Add
                  .Fields("Id_Mov_Activo").Value = lReg("id_mov_activo").Value
                  .Fields("Cantidad").Value = lReg("Cantidad").Value
                  .Fields("TotalCantidad").Value = .Fields("TotalCantidad").Value + lReg("Cantidad").Value
              End With
           Else
              lSaldo_Cantidad = lcClase.Saldo_Activo_Cantidad(pId_Cuenta:=pId_Cuenta _
                                                             , pId_Nemotecnico:=pId_Nemotecnico _
                                                             , pId_Mov_Activo:=lReg("id_mov_activo").Value)
           
              If lSaldo_Cantidad > 0 Then
                With lhActivos.Add
                  .Fields("Id_Mov_Activo").Value = lReg("id_mov_activo").Value
                  .Fields("Cantidad").Value = lSaldo_Cantidad
                End With
              End If
           End If
        Next
    Else
        Set lcMov_Activos = New Class_Mov_Activos
        If lcMov_Activos.Buscar_Pendientes_Cierre(pId_Cuenta:=pId_Cuenta _
                                                , pFecha:=pFecha_Movimiento _
                                                , pId_Nemotecnico:=pId_Nemotecnico _
                                                , pId_Mov_Activo_Compra:=Null) Then
          For Each lReg In lcMov_Activos.Cursor
            Select Case lReg("FLG_TIPO_MOVIMIENTO").Value
              Case gcTipoOperacion_Ingreso
                lCant_Pend_Mov_Activos = lCant_Pend_Mov_Activos + lReg("cantidad").Value
                pId_Mov_Activo = lReg("id_mov_activo").Value
              Case gcTipoOperacion_Egreso
                lCant_Pend_Mov_Activos = lCant_Pend_Mov_Activos - lReg("cantidad").Value
            End Select
          Next
          If lCant_Pend_Mov_Activos > 0 Then
            With lhActivos.Add
              .Fields("Id_Mov_Activo").Value = pId_Mov_Activo
              .Fields("Cantidad").Value = lCant_Pend_Mov_Activos
            End With
          End If
        End If
    End If
  End With
  Set lcSaldo_Activo = Nothing
  
'  If pCod_Instrumento = "BONOS_NAC" Then
'
'  Else
     lSaldo_Cantidad = pCantidad
     For Each lReg In lhActivos
        If lReg("cantidad").Value >= lSaldo_Cantidad Then
           lReg("asignado").Value = lSaldo_Cantidad
        Else
           lReg("asignado").Value = lReg("cantidad").Value
        End If
          
        lSaldo_Cantidad = lSaldo_Cantidad - lReg("asignado").Value
          
        If lSaldo_Cantidad <= 0 Then
           Exit For
        End If
      Next
'  End If
  Set phActivos = lhActivos

  Fnt_Enlaza_VentasCompras_RF = True
ExitProcedure:
  Set lcSaldo_Activo = Nothing
  Set lcClase = Nothing
End Function

Public Function Fnt_VerificaCreacion_Nemo(pNemotecnico _
                                         , pFecha_Movimiento _
                                         , pTasa _
                                         , ByRef pId_nemotécnico _
                                         , ByRef PId_Moneda_Deposito _
                                         , ByRef pId_Moneda_Pago _
                                         , ByRef pFecha_Vencimiento _
                                         , ByRef pCod_Instrumento _
                                         , ByRef pBase _
                                         , ByRef pMsgError As String) As Boolean
Dim lcPactos          As Class_Pactos

'-------------------------------------
'Dim lcMoneda          As Class_Monedas
Dim lcMoneda          As Object
'-------------------------------------

Dim lcEmisor_Especifico As Class_Emisores_Especifico
'---------------------------------------------------
Dim lNemotecnico
Dim lId_Emisor_Especifico
Dim lFecha_Emision
  
  Fnt_VerificaCreacion_Nemo = False
  
  'Verifica si el nemotécnico se puede crear o no
  Select Case Left(pNemotecnico, 2)
    Case "P$", "PU", "PO"
      pCod_Instrumento = gcINST_PACTOS_NAC
     
      
    Case "D$", "DU", "DO"
      pCod_Instrumento = IIf(pCod_Instrumento = gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_NAC) 'JGR 080509 'gcINST_DEPOSITOS_NAC
    
    Case Else
      pMsgError = "No se puede crear el nemotécnico """ & pNemotecnico & """."
      GoTo ExitProcedure
  
  End Select
  
  'Busca el emisor para asi poder crear el nemotécnico que corresponde a Magic
  Set lcEmisor_Especifico = New Class_Emisores_Especifico
  With lcEmisor_Especifico
    .Campo("COD_SVS_nemotecnico").Valor = Trim(Mid(pNemotecnico, 3, 4))
    If Not .Buscar Then
      pMsgError = "Problemas en buscar el codigo SVS del emisor." & .ErrMsg
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      pMsgError = "El codigo del emisor no esta definido en el sistema, cree un emisor especifico con el codigo de SVS correspondiente."
      GoTo ExitProcedure
    End If
    
    lId_Emisor_Especifico = .Cursor(1)("id_emisor_especifico").Value
  End With
  Set lcEmisor_Especifico = Nothing
  
  pFecha_Vencimiento = DateSerial("20" & Mid(pNemotecnico, 11, 2), Mid(pNemotecnico, 9, 2), Mid(pNemotecnico, 7, 2))
  
  'Genera el nemotécnico
  Select Case pCod_Instrumento
    Case gcINST_DEPOSITOS_NAC, gcINST_DEPOSITOS_DAP 'JGR 080509
      Set lNemotecnico = New Class_Nemotecnicos
      With lNemotecnico
        .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
        .Campo("Fecha_Vencimiento").Valor = pFecha_Vencimiento
        lNemotecnico = .Fnt_Generacion_Nemo_SVS(PId_Moneda_Deposito, pId_Moneda_Pago)
      End With
      Set lNemotecnico = Nothing
    Case gcINST_PACTOS_NAC
      Set lcPactos = New Class_Pactos
      lNemotecnico = lcPactos.Fnt_Genera_Nemo_SVS_Pactos(lId_Emisor_Especifico _
                                                       , pFecha_Vencimiento _
                                                       , PId_Moneda_Deposito _
                                                       , pId_Moneda_Pago)
      Set lcPactos = Nothing
  End Select
  
  'Busca la moneda del nemotécnico
  PId_Moneda_Deposito = Null

'----------------------------------
'  Set lcMoneda = New Class_Monedas
  Set lcMoneda = CreateObject(cDLL_Monedas)
'----------------------------------

  With lcMoneda
    .Campo("cod_moneda").Valor = cMoneda_Cod_Peso
    If Not .Buscar Then
      pMsgError = "Problemas en buscar la moneda del nemotécnico." & .ErrMsg
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      pMsgError = "Por raro que paresca no esta la moneda definida en el sistema, favor de crear."
      GoTo ExitProcedure
    End If
    
    pId_Moneda_Pago = .Cursor(1)("id_moneda").Value
  
    Select Case Mid(pNemotecnico, 2, 1)
      Case "$"
        '.Campo("cod_moneda").Valor = cMoneda_Cod_Peso
        PId_Moneda_Deposito = pId_Moneda_Pago
        pBase = 30
      Case "U"
        .Campo("cod_moneda").Valor = cMoneda_Cod_UF
        pBase = 360
      Case "O"
        .Campo("cod_moneda").Valor = cMoneda_Cod_Dolar
        pBase = 30
    End Select
    
    If IsNull(PId_Moneda_Deposito) Then
      If Not .Buscar Then
        pMsgError = "Problemas en buscar la moneda del nemotécnico." & .ErrMsg
        GoTo ExitProcedure
      End If
      
      If .Cursor.Count <= 0 Then
        pMsgError = "Por raro que paresca no esta la moneda definida en el sistema, favor de crear."
        GoTo ExitProcedure
      End If
      
      PId_Moneda_Deposito = .Cursor(1)("id_moneda").Value
    End If
  End With
  Set lcMoneda = Nothing
  
  lFecha_Emision = pFecha_Movimiento
  
  Set lNemotecnico = New Class_Nemotecnicos
  With lNemotecnico
    .Campo("cod_instrumento").Valor = IIf(pCod_Instrumento = gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_NAC) 'JGR 080509 'gcINST_DEPOSITOS_NAC
    .Campo("nemotecnico").Valor = pNemotecnico
    .Campo("cod_Instrumento").Valor = pCod_Instrumento
    .Campo("id_Mercado_Transaccion").Valor = "15" 'fc_Mercado_Transaccion 'Este campo deberia aceptar nulos
    .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
    .Campo("id_Emisor_Especifico_Origen").Valor = lId_Emisor_Especifico
    .Campo("id_Moneda").Valor = PId_Moneda_Deposito
    .Campo("id_Moneda_transaccion").Valor = pId_Moneda_Pago
    .Campo("id_Tipo_Estado").Valor = cTEstado_Nemotecnico
    .Campo("cod_Estado").Valor = cCod_Estado_Vigente
    .Campo("dsc_nemotecnico").Valor = "Ingreso por Importación"
    .Campo("tasa_Emision").Valor = pTasa
    .Campo("tipo_Tasa").Valor = ""
    .Campo("periodicidad").Valor = ""
    .Campo("fecha_Vencimiento").Valor = pFecha_Vencimiento
    .Campo("corte_Minimo_Papel").Valor = 1 'Depositos no tiene corte
    .Campo("Monto_Emision").Valor = ""
    .Campo("liquidez").Valor = ""
    .Campo("base").Valor = pBase
    .Campo("cod_Pais").Valor = gcPais_Chile
    .Campo("flg_Fungible").Valor = ""
    .Campo("fecha_emision").Valor = lFecha_Emision
    If Not .Guardar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en grabar el Nemotécnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
    pId_nemotécnico = .Campo("id_nemotecnico").Valor
  End With
  
  Fnt_VerificaCreacion_Nemo = True
  
ExitProcedure:

End Function

