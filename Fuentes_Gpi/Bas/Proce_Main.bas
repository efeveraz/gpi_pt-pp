Attribute VB_Name = "Proce_Main"
Option Explicit
Dim fBandera As String


Public Sub Sub_ConfiguracionRegional()
  'Configuraci�n del n�mero
  SetLocaleInfo LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, "."
  SetLocaleInfo LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, ","
  'Configuraci�n de la moneda
  SetLocaleInfo LOCALE_USER_DEFAULT, LOCALE_SMONDECIMALSEP, "."
  SetLocaleInfo LOCALE_USER_DEFAULT, LOCALE_SMONTHOUSANDSEP, ","
  DoEvents
End Sub



Public Sub Main()
    Dim lCont As Byte

    On Error GoTo ErrProcedure
  
    fBandera = "Iniciando el sistema."
    gTipo_Menu = "A"
    If App.PrevInstance Then
        MsgBox "La aplicaci�n ya se encuentra activa", vbInformation, App.FileDescription
        End
    End If
  
'  lCont = 0
'  Do While Not Mid(Format(0, "0.0"), 2, 1) = "."
'    'Configura el formato del numero y de la moneda
'    Call Sub_ConfiguracionRegional
'    lCont = lCont + 1
'
'    If lCont = 3 Then
'      MsgBox "El formato decimal definido en su PC " & vbCrLf & vbTab & _
'      "no corresponde al formato requerido para Gesti�n de Inversiones", vbCritical, "Formato Decimal"
'      End
'    End If
'  Loop


'**********************************************************************************************
'   Actualizar las dll. Descomentar la llamada a frmActualizarDLL.Inicial
''**********************************************************************************************
    'frmActualizarDLL.Inicial 'Comentado por Demora de Regsitro
    
    'Solucion que usa archivo de Versiones para indicar cual es la DLL que se instala en cierta version
    frmActualizarDLL.Mantener_DLLs
'**********************************************************************************************
'   FINAL Actualizar las dll.
'**********************************************************************************************

  fBandera = "Buscando el ASCII del decimal"
  'Busca el caracter para la separacion decimal
  Call Sub_CualASCIIDecimal
  
  fBandera = "Iniciando conecci�n a DB"
  Set gDB = New DLL_DB.Class_Conect
  fBandera = "Conectando DB"
  With gDB
    .AppPath = App.Path & "\"
    .Conectar
  End With
  
  fBandera = "Mostrando MDI"
  MDI_Principal.Show
  fBandera = "Mostrando Login"
  
    Dim lParametro
    Dim Usuario As String
    Dim Negocio As Integer
    
    lParametro = Trim(Command)
    '|
    'lParametro = "fcatrian|1"
   
    
    If Len(lParametro) = 0 Then
        
        End
    Else
        Usuario = Mid(lParametro, 1, (InStr(1, lParametro, "|", 1)) - 1)
        Negocio = Mid(lParametro, (InStr(1, lParametro, "|", 1)) + 1, (Len(lParametro) - Len(Usuario)))

        If Not Frm_Login_Sistema.Fnt_MostrarLauncher(Usuario, Negocio) Then
            'Si cancelo la operacion o ocurrio cualquier error que no se puede continuar
            Unload MDI_Principal
            End
        End If
    End If
    'Call Sub_CargaForm
  
'  If Not Frm_Login_Sistema.Fnt_MostrarLauncher Then
'    'Si cancelo la operacion o ocurrio cualquier error que no se puede continuar
'    Unload MDI_Principal
'    End
'  End If
'*********************************************************************************************
  fBandera = "Cargando valores del sistema"
  If Not Fnt_CargaValoresSistema Then
    'Si existe cualquier error que no se puede continuar
    Unload MDI_Principal
    End
  End If
  
  fBandera = "Registrando Usuario"
  Call Sub_RegistrarUsuario
  
  'Load MDI_Principal
  With MDI_Principal
    fBandera = "Cargando Men�"
    .Sub_CargaMenu
    fBandera = "Mostrando Men�"
    .Show

    fBandera = "Mostrando datos de conexi�n"
    .StatusBar.Panels("SESSIONID").Text = "SID: " & gDB.SessionID
    .StatusBar.Panels("SESSIONID").ToolTipText = .StatusBar.Panels("SESSIONID").Text
    .StatusBar.Panels("CONNECT").Text = gDsc_Usuario & "@" & gDB.Server & "." & gDB.Database
    .StatusBar.Panels("CONNECT").ToolTipText = .StatusBar.Panels("CONNECT").Text
    
    .StatusBar.Panels("EMPRESA").Text = gDsc_Empresa
    .StatusBar.Panels("EMPRESA").ToolTipText = .StatusBar.Panels("EMPRESA").Text
    
    
    fBandera = "Iniciando Relog"
    'Set gDB.LstImgRecord = New hRecord
    'gDB.LstImgRecord.AddField "picture", New stdole.StdPicture
    Set gRelogDB = New Class_RelogDB
    Set gRelogDB.LstImgRecord = .ImgClocks
    Set gRelogDB.LstImgDB = .ImgDB
    Set gRelogDB.PictureIndicator = .StatusBar.Panels("STATUSDB")
    Set gDB.LstImgRecord = gRelogDB
    
    'Se ejecuta para obtener las fecha del servidor y colocarla en el MDI
    '.StatusBar.Panels("TEXTO").Text = FMT_DATE(Format(Now, "dd/mm/yyyy"))
    '.StatusBar.Panels("TEXTO").ToolTipText = .StatusBar.Panels("TEXTO").Text
  End With
  
  If gTipo_Menu = "A" Then
    fBandera = "Arbol Menu Visible"
    MDI_Principal.Frame_Menu.Visible = True
  Else
    fBandera = "Men� persiana visible"
    MDI_Principal.menu_persiana.Visible = True
  End If
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call MsgBox("Problemas en el inicio del sistema." & vbCr & vbCr & Err.Description, vbCritical, Title:=fBandera)
    End
    GoTo ExitProcedure
    End
    Resume
  End If

ExitProcedure:

End Sub

Private Function Fnt_CargaMonedas_Matrix() As Boolean
'Dim lcMonedas As Class_Monedas
Dim lcMoneda As Object
  
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)

  With lcMoneda
    If .Buscar Then
      Set gmMatrixMonedas = .Cursor
      Fnt_CargaMonedas_Matrix = True
    Else
      MsgBox "Problemas al cargar la matriz de Monedas." & vbLf & vbLf & .ErrMsg, vbCritical, MDI_Principal.Caption
      Fnt_CargaMonedas_Matrix = False
    End If
  End With
  
  Set lcMoneda = Nothing
End Function

Private Sub Sub_RegistrarUsuario()
Dim lcLog_Sistema_Activo As Class_Log_Sistema_Activo

  Set lcLog_Sistema_Activo = New Class_Log_Sistema_Activo
  With lcLog_Sistema_Activo
    .Campo("SESIONID").Valor = gDB.SessionID
    .Campo("ID_USUARIO").Valor = gID_Usuario
    
    If Not .Guardar Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_Inicio_Sistema _
                      , "No se pudo registrar el usuario al ingresar al sistema." _
                      , .ErrMsg _
                      , True)
    End If
  End With
  Set lcLog_Sistema_Activo = Nothing
End Sub


Public Function Fnt_CargaValoresSistema() As Boolean

  Fnt_CargaValoresSistema = False
  
  gStrPictureEmpresa = App.Path & "\creasys chico 2.jpg"
  gStrPictureEmpresaGrande = App.Path & "\Creasys grande.jpg"
  gUbicacionDLLs = App.Path & "\DLL\"

  Call Fnt_FechaServidor

  If Not Fnt_CargaMonedas_Matrix Then
    'Si existe cualquier error que no se puede continuar
    GoTo ExitProcedure
  End If
  
  Fnt_CargaValoresSistema = True
  
  
ExitProcedure:

End Function

