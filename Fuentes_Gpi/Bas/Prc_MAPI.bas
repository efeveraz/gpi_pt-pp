Attribute VB_Name = "Prc_MAPI"
Option Explicit

Public Const chrArchivoAdjunto = "ARCHIVO"

Public Function Fnt_Enviar(pAsunto As String _
                        , pMensaje As String _
                        , pDestinatarios As hRecord _
                        , Optional pMostrar As Boolean = False _
                        , Optional pArchivosAdjuntos As hRecord = Nothing _
                        , Optional pCC As String = "" _
                        ) As Boolean
Dim lReg As hFields
Dim lrArchivo As hFields
                        
On Error GoTo ErrProcedure
                        
  Fnt_Enviar = False
  
  'Inicia la sesion de E-Mail.
  MDI_Principal.MAPISession.SignOn
  
  With MDI_Principal.MAPIMessages
    'enrtega el ID de la sesion de email
    .SessionID = MDI_Principal.MAPISession.SessionID
    
    'Redacta un mensaje
    .Compose
    .AddressResolveUI = True
        
    For Each lReg In pDestinatarios
      .RecipIndex = (lReg.Index - 1)
      .RecipType = lReg("TipoRecipiente").Value 'mapToList
      .RecipAddress = lReg("Email").Value '"clientes@santander.cl"
      .ResolveName
      
      If Not lReg("DisplayName").Value = "" Then
        If .RecipDisplayName = lReg("Email").Value Then
          .RecipDisplayName = lReg("DisplayName").Value
        End If
      End If
    Next
                
    'Si env�a con copia a alg�n destinatario
    If pCC <> "" Then
        With MDI_Principal.MAPIMessages
            .RecipIndex = .RecipCount
            .RecipType = 2
            .RecipDisplayName = pCC
        End With
    End If
    
    
    'asunto
    .MsgSubject = pAsunto
    'texto del email.
    .MsgNoteText = pMensaje
    
    If Not pArchivosAdjuntos Is Nothing Then
     'Coloca el archivo adjunto
      For Each lrArchivo In pArchivosAdjuntos
        .AttachmentPathName = lrArchivo.Index
        .AttachmentPathName = lrArchivo("archivo").Value
      Next
    End If
    
    'envia el email.
    Call .Send(pMostrar)
  End With
  
  'MDI_Principal.MAPISession.SignOff

  Fnt_Enviar = True

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en el envio de email.", Err.Description)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  MDI_Principal.MAPISession.SignOff
End Function

Public Function Fnt_AgregarDestinatario(ByRef pRegDestinatarios As hRecord _
                                      , pEmail As String _
                                      , Optional pDisplayName As String = "" _
                                      , Optional pTipoRecipiente As MSMAPI.RecipTypeConstants = mapToList) As hFields
Dim lReg As hFields
                                      
  If pRegDestinatarios Is Nothing Then
    Set pRegDestinatarios = New hRecord
    With pRegDestinatarios
      .AddField "EMail"
      .AddField "DisplayName"
      .AddField "TipoRecipiente"
    End With
  End If

  Set lReg = pRegDestinatarios.Add
  lReg("EMail").Value = pEmail
  lReg("DisplayName").Value = pDisplayName
  lReg("TipoRecipiente").Value = pTipoRecipiente

  Set Fnt_AgregarDestinatario = lReg
End Function

Public Function Fnt_AgregarArchivoAdjunto(ByRef pArchivosAdjuntos As hRecord _
                                        , pPathArchivoAdjunto As String) As hFields
Dim lReg As hFields
                                      
  If pArchivosAdjuntos Is Nothing Then
    Set pArchivosAdjuntos = New hRecord
    With pArchivosAdjuntos
      .AddField chrArchivoAdjunto
    End With
  End If

  Set lReg = pArchivosAdjuntos.Add
  lReg(chrArchivoAdjunto).Value = pPathArchivoAdjunto

  Set Fnt_AgregarArchivoAdjunto = lReg
End Function


