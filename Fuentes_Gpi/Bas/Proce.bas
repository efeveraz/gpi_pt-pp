Attribute VB_Name = "Proce"
Option Explicit
Dim gASCIIDecimal As String
Const vb_vs_numerico As Integer = 1
Const vb_vs_alfanumerico As Integer = 2
Const vb_vs_decimal As Integer = 3
Const vb_vs_fecha As Integer = 4

Public Function Fnt_Matrix_Text(pText, Optional pSplitField = vbTab)
Dim lmMatrixFinal()
Dim lmMatrixReg
Dim i

  lmMatrixReg = Split(pText, vbCrLf)
  ReDim lmMatrixFinal(0 To 0)

  For i = LBound(lmMatrixReg) To UBound(lmMatrixReg)
    ReDim Preserve lmMatrixFinal(0 To i + 1)
    lmMatrixFinal(i + 1) = Split(lmMatrixReg(i), pSplitField)
  Next

  Fnt_Matrix_Text = lmMatrixFinal

End Function

Public Function ValidaRut(ByVal Rut As String) As Boolean
   Dim i As Integer, L As Integer, Valor As Integer, suma As Integer, Resultado As Integer
   Dim Resto As String

   ValidaRut = False
   
   If Not Trim(Rut) = "" Then
      If InStr(Rut, "-") > 0 Then
         Rut = Mid(Rut, 1, InStr(Rut, "-") - 1) & Mid(Rut, InStr(Rut, "-") + 1, 1)
      End If
      
      If Not IsNumeric(Mid(Rut, Len(Rut), 1)) Then
          Rut = Mid(Rut, 1, Len(Rut) - 1) & UCase(Mid(Rut, Len(Rut), 1))
      End If
      
      Valor = 2
      L = Len(Rut) - 1
      For i = L To 1 Step -1
         suma = suma + Val(Mid(Rut, i, 1)) * Valor
         Valor = Valor + 1
         If Valor > 7 Then
            Valor = 2
         End If
      Next i
   
      Resultado = suma Mod 11
      Resto = 11 - Resultado
      If Resto = 11 Then
         Resto = "0"
      End If
      If Resto = 10 Then
         Resto = "K"
      End If
   
      If Not UCase(Resto) = UCase(Mid(Rut, Len(Rut), 1)) Then
         ValidaRut = False
      Else
         ValidaRut = True
      End If
   Else
      ValidaRut = True
   End If
End Function

Public Sub Sub_ConfiguraToolBar(ByRef pToolbar As MSComctlLib.Toolbar, ByRef pBarraProceso As MSComctlLib.ProgressBar, ByRef pForm As Form)
Dim lLargos As Double
Dim lButton As MSComctlLib.Button
  
On Error Resume Next
  
  With pToolbar
    lLargos = 0
    For Each lButton In .Buttons
      If lButton.Visible Then
        lLargos = lLargos + lButton.Width
      End If
    Next
    
    .Buttons("STATUSBAR").Width = .Buttons("STATUSBAR").Width + (pForm.ScaleWidth - lLargos - 50)
    
    If Not pBarraProceso Is Nothing Then
      pBarraProceso.Top = 20
      pBarraProceso.Left = .Buttons("STATUSBAR").Left + 20
      pBarraProceso.Width = .Buttons("STATUSBAR").Width - 20
      pBarraProceso.Height = .Buttons("STATUSBAR").Height - 40
    End If
  End With
  
On Error GoTo 0
End Sub

Public Function Fnt_ExisteVentanaKey(ByRef pForm As String, ByVal pkey As String) As Boolean
Dim lForm As Form

  Fnt_ExisteVentanaKey = False

  For Each lForm In Forms
    If UCase(lForm.Name) = UCase(pForm) Then
      If lForm.fKey = pkey Then
        Fnt_ExisteVentanaKey = True
        Exit For
      End If
    End If
  Next
End Function

Public Function Fnt_ExisteVentana(ByRef pForm As String) As Boolean
Dim lForm As Form

  Fnt_ExisteVentana = False

  For Each lForm In Forms
    If UCase(lForm.Name) = UCase(pForm) Then
      Fnt_ExisteVentana = True
      Exit For
    End If
  Next
End Function

Public Function Fnt_ComboSelected_KEY(ByRef pCombo As Object) As String
Dim lLinea As Long

  Fnt_ComboSelected_KEY = ""
  
  If (TypeOf pCombo Is TDBCombo) Or (TypeOf pCombo Is TDBList) Then
    If (Not NVL(pCombo.SelectedItem, "") = "") And (Not pCombo.Text = "") Then
      lLinea = pCombo.SelectedItem
      If pCombo.Text = pCombo.Columns(0).ValueItems(lLinea).DisplayValue Then
        Fnt_ComboSelected_KEY = pCombo.Columns(0).ValueItems(lLinea).Value
      End If
    End If
  Else
    Fnt_ComboSelected_KEY = Fnt_BuscaKey_In_TextCombo(pCombo, pCombo.Text)
  End If
End Function


Public Function Fnt_BuscaKey_In_TextCombo(ByRef pCombo As MSComctlLib.ImageCombo, ByVal pText As String) As String
Dim lcombo As ComboItem

  Fnt_BuscaKey_In_TextCombo = ""
  
  For Each lcombo In pCombo.ComboItems
    If lcombo.Text = pText Then
      Fnt_BuscaKey_In_TextCombo = Mid(lcombo.Key, 2)
      Exit For
    End If
  Next
End Function

Public Function Fnt_PreguntaIsNull(pControl As Control, Optional pCaption)
Dim lCaption As String

  Fnt_PreguntaIsNull = False

  If Trim("" & pControl.Text) = "" Then
    If Not IsMissing(pCaption) Then
      lCaption = pCaption
    Else
      lCaption = pControl.Caption
    End If
    
    Fnt_PreguntaIsNull = True
    
    MsgBox "Debe ingresar """ & lCaption & """.", vbInformation
    pControl.SetFocus
  End If
End Function

Public Function Fnt_PreguntaIsNothing(pControl As Control, Optional pCaption)
Dim lCaption As String
Dim lResult As Boolean

  Fnt_PreguntaIsNothing = False
  
  lResult = False
  If TypeOf pControl Is TDBCombo Then
    If IsNull(pControl.SelectedItem) Then
      lResult = True
    End If
  Else
    If pControl.SelectedItem Is Nothing Then
      lResult = True
    End If
  End If
  
  If lResult Then
    If Not IsMissing(pCaption) Then
      lCaption = pCaption
    Else
      lCaption = pControl.Caption
    End If
    
    MsgBox "Debe ingresar """ & lCaption & """.", vbInformation
    pControl.SetFocus
  End If
  
  Fnt_PreguntaIsNothing = lResult
End Function


Public Function Fnt_EmpresaActual() As Long
  Fnt_EmpresaActual = gId_Empresa
End Function

Public Sub Sub_FormControl_Enabled(ByRef pControles As Object _
                                  , ByRef pContainer As Object _
                                  , ByVal pEnabled As Boolean)
Dim lControl As Control
On Error Resume Next
  
  For Each lControl In pControles
    With lControl
'      Debug.Print .Name
      If .Container Is pContainer Then
        .Enabled = pEnabled
'        Call Sub_FormControl_Enabled(pControles, lControl, pEnabled)
      End If
    End With
  Next
End Sub

Public Sub Sub_FormControl_Color(ByRef pControles As Object)
Dim lControl As Control
On Error Resume Next

  For Each lControl In pControles
    With lControl
      If TypeOf lControl Is TextBox Then
        If .Tag = "OBLI" Then
          .BackColor = fColorOBligatorio
        ElseIf .Locked Then
          .BackColor = fColorNoEdit
        ElseIf Fnt_ValorTag(.Tag, "OBLI") = "S" Then
          .BackColor = fColorOBligatorio
        Else
          .BackColor = vbWhite
        End If
      ElseIf TypeOf lControl Is hControl2.hTextLabel Then
        If .Locked Then
          .BackColorTxt = fColorNoEdit
        ElseIf .Tag = "OBLI" Then
          .BackColorTxt = fColorOBligatorio
        ElseIf Fnt_ValorTag(.Tag, "OBLI") = "S" Then
          .BackColorTxt = fColorOBligatorio
        Else
          .BackColor = vbWhite
        End If
      ElseIf TypeOf lControl Is TDBCombo Then
        If .Tag = "OBLI" Then
          .BackColor = fColorOBligatorio
        ElseIf .Locked Then
          .BackColor = fColorNoEdit
        ElseIf Fnt_ValorTag(.Tag, "OBLI") = "S" Then
          .BackColor = fColorOBligatorio
        Else
          .BackColor = vbWhite
        End If
      ElseIf TypeOf lControl Is ImageCombo Then
        If .Tag = "OBLI" Then
          .BackColor = fColorOBligatorio
        ElseIf Fnt_ValorTag(.Tag, "OBLI") = "S" Then
          .BackColor = fColorOBligatorio
        Else
          .BackColor = vbWhite
        End If
      ElseIf TypeOf lControl Is VSFlexGrid Then
        .BackColorAlternate = fColorLineaAlter
      End If
    End With
  Next
End Sub

Public Function Fnt_Form_Validar(ByRef pControles As Object, Optional pContainer As Object = Nothing) As Boolean
Dim lControl  As Control
Dim lResult   As Boolean
Dim lCaption  As String

On Error Resume Next
  
  lResult = True
  
  For Each lControl In pControles
    With lControl
      If (pContainer Is Nothing) Or (.Container Is pContainer) Then
        Select Case .Tag
          Case "OBLI"
            If Not Fnt_NotNullControl(lControl, lControl.Name) Then
              lResult = False
              Exit For
            End If
          Case ""
            'No se realiza nada debido a que no hay verificacion
          Case Else
            If Fnt_ValorTag(.Tag, "OBLI") = "S" Then
              If Not Fnt_NotNullControl(lControl, Fnt_ValorTag(.Tag, "CAPTION")) Then
                lResult = False
                Exit For
              End If
            End If
        End Select
      End If
    End With
  Next
  
  Fnt_Form_Validar = lResult
End Function

Public Function Fnt_NotNullControl(ByRef pControl As Object, Optional pCaption As String) As Boolean
Dim lResult  As Boolean
Dim lLinea   As Long

  lResult = True

  If TypeOf pControl Is hControl2.hTextLabel Then
    If Trim(pControl.Text) = "" Then
      lResult = False
      pCaption = pControl.Caption
    End If
  ElseIf TypeOf pControl Is TextBox Then
    If Trim(pControl.Text) = "" Then
      lResult = False
      pCaption = pCaption
    End If
  ElseIf TypeOf pControl Is ImageCombo Then
    Rem Cambiado por CSM. Volver al original si no resulta en todos los casos.
    Rem -------------------------------------------
    Rem ---------- C�digo Antiguo -----------------
    Rem -------------------------------------------
    '    If Trim(pControl.Text) = "" Then
    '      lResult = False
    '      pCaption = pCaption
    '    ElseIf pControl.SelectedItem Is Nothing Then
    '      lResult = False
    '      pCaption = pCaption
    '    End If
    Rem -------------------------------------------
    Rem ---------- Nuevo C�digo -------------------
    Rem -------------------------------------------
    If pControl.SelectedItem Is Nothing Then
      If Trim(pControl.Text) = "" Then
        lResult = False
        pCaption = pCaption
      End If
    End If
    Rem -------------------------------------------
  ElseIf TypeOf pControl Is TDBCombo Then
    If Trim(pControl.Text) = "" Or IsNull(pControl.SelectedItem) Then
      lResult = False
      pCaption = pCaption
    Else
      lLinea = pControl.SelectedItem
      If Not pControl.Text = pControl.Columns(0).ValueItems(lLinea).DisplayValue Then
        lResult = False
        pCaption = pCaption
      End If
    End If
  End If
  
  If Not lResult Then
    MsgBox "Debe ingresar """ & pCaption & """.", vbInformation
    pControl.SetFocus
  End If
  
  Fnt_NotNullControl = lResult
End Function

Public Sub Sub_Tama�oForm(pForm As Form, Optional pWidth, Optional pHeight)
  With pForm
    If Not IsMissing(pWidth) Then
      .Width = pWidth
    End If
    
    If Not IsMissing(pHeight) Then
      .Height = pHeight
    End If
  End With
End Sub

Public Function Fnt_FechaServidor() As Date
Dim lFecha As Date

  With gDB
    .Parametros.Clear
    .Procedimiento = "Fnt_Fechas_Servidor"
    .Parametros.Add "PFecha_servidor", ePT_Fecha, "", ePD_Salida
    If .EjecutaSP() Then
      lFecha = .Parametros("PFecha_servidor").Valor
      Fnt_FechaServidor = DateSerial(Year(lFecha), Month(lFecha), Day(lFecha)) 'hago esta transformacion para solamente obtener la fecha y no la hora.
      With MDI_Principal.StatusBar.Panels("FECHASERVIDOR")
        .Text = "Fecha Serv: " & Format(lFecha, cFormatDate)
        '.ToolTipText = .Text
      End With
    Else
      Call Fnt_MsgError(eLS_ErrSystem, "No se puede objtener la fecha del servidor", .ErrMsg, pConLog:=True)
    End If
  End With
End Function

Public Function Fnt_SYSDATE() As Date
Dim lFecha As Date

  With gDB
    .Parametros.Clear
    .Procedimiento = "Fnt_SYSDATE"
    .Parametros.Add "PFecha_servidor", ePT_Fecha, "", ePD_Salida
    If .EjecutaSP() Then
      lFecha = .Parametros("PFecha_servidor").Valor
      Fnt_SYSDATE = lFecha
    Else
      Call Fnt_MsgError(eLS_ErrSystem, "No se puede objtener la fecha completa del servidor", .ErrMsg, pConLog:=True)
    End If
  End With
End Function

' Mostrar el formulario indicado, dentro de picDock
Public Sub DockForm(ByVal pForm As Object, _
                     ByVal picDock As Object, _
                     Optional ByVal Ajustar As Boolean = True)
  ' Hacer el formulario indicado, un hijo del picDock
  ' Si Ajustar es True, se ajustar� al tama�o del contenedor,
  ' si Ajustar es False, se quedar� con el tama�o actual.
  Call SetParent(pForm.hwnd, picDock.hwnd)
  'PosDockForm pForm.hWnd, picDock, ajustar
  'Call ShowWindow(pForm.hWnd, NORMAL_eSW)
  'pForm.Show

  pForm.Mostrar
  Call pForm.Move(MDI_Principal.Frame_Menu.Width + 10, 0)
  pForm.SetFocus
End Sub

' Posicionar el formulario indicado dentro de picDock
Public Sub PosDockForm(ByVal formhWnd As Long, _
                        ByVal picDock As PictureBox, _
                        Optional ByVal Ajustar As Boolean = True)
    ' Posicionar el formulario indicado en las coordenadas del picDock
    ' Si Ajustar es True, se ajustar� al tama�o del contenedor,
    ' si Ajustar es False, se quedar� con el tama�o actual.
    Dim nWidth As Long, nHeight As Long
    Dim wndPl As WINDOWPLACEMENT
    '
    If Ajustar Then
        nWidth = picDock.ScaleWidth \ Screen.TwipsPerPixelX
        nHeight = picDock.ScaleHeight \ Screen.TwipsPerPixelY
    Else
        ' el tama�o del formulario que se va a posicionar
        Call GetWindowPlacement(formhWnd, wndPl)
        With wndPl.rcNormalPosition
            nWidth = .Right - .Left
            nHeight = .Bottom - .Top
        End With
    End If
    Call MoveWindow(formhWnd, 0, 0, nWidth, nHeight, True)
End Sub

Public Function Fnt_Divide(pValor1, pValor2)
On Error Resume Next
  Fnt_Divide = 0
  
  Fnt_Divide = pValor1 / pValor2
Err.Clear
End Function

Public Sub Sub_Bloquea_Puntero(ByVal pForm As Form)
  pForm.MousePointer = vbArrowHourglass
  Screen.MousePointer = vbArrowHourglass
End Sub

Public Sub Sub_Desbloquea_Puntero(ByVal pForm As Form)
  pForm.MousePointer = vbDefault
  Screen.MousePointer = vbDefault
End Sub

Public Function Fnt_Escribe_Grilla(pGrilla As VSFlexGrid _
                                 , ByVal pTipo_Error _
                                 , ByVal pMensaje _
                                 , Optional pAutoWith As Boolean = True _
                                 , Optional pConLog As Boolean = False _
                                 , Optional pId_Log_SubTipo _
                                 , Optional pId_Log_Proceso = Null _
                                 , Optional pCodProducto As String = "" _
                                 , Optional pCantidadNemo As Long = 0) As Long
Dim lLinea As Long

  If pGrilla Is Nothing Then
    'se coloco este codigo pq funciones para eliminar cierres de forma automatica no usan
    'grillas para informar el estado en que se encuentran.
    lLinea = -1
    Exit Function
  End If

  With pGrilla
    lLinea = .Rows
    
    Fnt_Escribe_Grilla = lLinea
    
    .AddItem ""
    Call SetCell(pGrilla, lLinea, "colum_pk", pTipo_Error)
    Call SetCell(pGrilla, lLinea, "TEXTO", pMensaje, pAutoWith)
    If pCodProducto <> "" Then
       Call SetCell(pGrilla, lLinea, "cod_producto", pCodProducto, pAutoWith)
       Call SetCell(pGrilla, lLinea, "cantidad_nemos", pCantidadNemo, pAutoWith)
    End If
    
    Debug.Print pMensaje
    
    Rem Para colocar el icono correspondiente
    .Col = .ColIndex("FOTO")
    .Row = lLinea
    
    Select Case pTipo_Error
      Case "N"
        .CellPicture = MDI_Principal.ImageListGlobal16.ListImages(cBoton_Aceptar).Picture
      Case cGrillaLog_Alert
        .CellPicture = MDI_Principal.ImageListGlobal16.ListImages(cGrilla_Warning).Picture
      Case cGrillaLog_Error
        .CellPicture = MDI_Principal.ImageListGlobal16.ListImages(cGrilla_Error).Picture
      Case Else
        '.CellPicture = MDI_Principal.ImageListGlobal16.ListImages(cBoton_Aceptar).Picture
    End Select
    
    If pConLog Then
      If IsMissing(pId_Log_SubTipo) Then
        pId_Log_SubTipo = eLog_Subtipo.eLS_LogVarios
      End If

      Call Fnt_AgregarLog(pId_Log_SubTipo:=pId_Log_SubTipo _
                        , pCod_Estado:=cEstado_Log_Mensaje _
                        , pGls_Log_Registro:=pMensaje _
                        , pId_Log_Proceso:=pId_Log_Proceso)
    End If
    
    Call .ShowCell(.Row, .Col)
    
    .Refresh
  End With

End Function

Public Function Fnt_Valida_Rut(pRut As hControl2.hTextLabel) As Boolean
  
  Fnt_Valida_Rut = True

  If Not Trim(pRut.Text) = "" Then
    If Not ValidaRut(pRut.Text) Then
      MsgBox "RUT No V�lido.", vbOKOnly + vbExclamation
      pRut.SetFocus
      Fnt_Valida_Rut = False
      Exit Function
    End If
    If InStr(pRut.Text, "-") = 0 Then
      pRut.Text = Mid(Trim(pRut.Text), 1, Len(Trim(pRut.Text)) - 1) & "-" & Mid(Trim(pRut.Text), Len(Trim(pRut.Text)), 1)
    End If
  End If
End Function

Public Function Fnt_SeparaString(ByVal pCadena As String, Optional ByVal pDelimitador As String = ";") As Variant
Dim lMatrix

  lMatrix = Split(pCadena, pDelimitador)
  
  Fnt_SeparaString = lMatrix
End Function

Public Function Fnt_SeparaTags(ByVal pCadena As String) As Variant
Dim lMatrix
Dim lMatrixTag
Dim lLargo As Long
Dim lLinea As Long
Dim lPos As Long

  lMatrix = Fnt_SeparaString(pCadena)

  lLargo = UBound(lMatrix) + 1

  If lLargo > 0 Then
    ReDim lMatrixTag(1 To lLargo, 1 To 2)
  
    For lLinea = 1 To lLargo
      lPos = InStr(1, lMatrix(lLinea - 1), "=")
        
      If lPos = 0 Then
        lMatrixTag(lLinea, 1) = lMatrix(lLinea - 1)
        lMatrixTag(lLinea, 2) = ""
      Else
        lMatrixTag(lLinea, 1) = Left(lMatrix(lLinea - 1), lPos - 1)
        lMatrixTag(lLinea, 2) = Mid(lMatrix(lLinea - 1), lPos + 1)
      End If
    Next
  End If
  
  Fnt_SeparaTags = lMatrixTag
End Function

Public Function Fnt_ValorTag(ByVal pCadena As String, ByVal pTag As String) As String
Dim lMatrix
Dim lLinea As Long

  Fnt_ValorTag = ""
  lMatrix = Fnt_SeparaTags(pCadena)

  If IsArray(lMatrix) Then
    For lLinea = 1 To UBound(lMatrix)
      If lMatrix(lLinea, 1) = pTag Then
        Fnt_ValorTag = lMatrix(lLinea, 2)
        Exit For
      End If
    Next
  End If
End Function

Public Function Fnt_Calcula_TipoCambio_Ope(ByVal pMonto As Double, ByVal pParidad As Double, ByVal pOperacion As String) As Double
'Esta funcion entrega el resultado del monto y la paridad dependiendo del tipo de operacion que se tiene que realizar
  Select Case pOperacion
    Case cFlg_TipoCambio_Multi
      Fnt_Calcula_TipoCambio_Ope = pMonto * pParidad
    Case cFlg_TipoCambio_Divi
      Fnt_Calcula_TipoCambio_Ope = Fnt_Divide(pMonto, pParidad)
    Case Else
      Fnt_Calcula_TipoCambio_Ope = 0
  End Select
End Function

Public Function Fnt_Calcula_TipoCambio_Matrix(ByVal pMonto As Double _
                                            , ByVal pId_Moneda_Origen _
                                            , ByVal pId_Moneda_Destino _
                                            , ByRef pMatrix As hRecord) As Double
Dim lReg As hFields
  
  If pId_Moneda_Origen = pId_Moneda_Destino Then
    'Si las monedas que se estan comparando son iguales se entrega de inmediato la paridad 1
    'no es necesario que se ingrese el recorrido del tipo de cambio.
    Fnt_Calcula_TipoCambio_Matrix = pMonto
    Exit Function
  End If
  
  Fnt_Calcula_TipoCambio_Matrix = 0
  
  For Each lReg In pMatrix
    If lReg("id_mon_origen").Value = pId_Moneda_Origen Then
      If lReg("id_mon_destino").Value = pId_Moneda_Destino Then
        Fnt_Calcula_TipoCambio_Matrix = Fnt_Calcula_TipoCambio_Ope(pMonto, lReg("paridad").Value, lReg("operacion").Value)
      End If
    End If
  Next
End Function

Public Function Fnt_Formato_Moneda(ByVal pId_Moneda) As String
Dim lReg As hFields
Dim lResult As String
Dim lDecimales As String
Dim lId_Moneda As Double
  
  lResult = "#,##0"
  lDecimales = ""
  
  lId_Moneda = To_Number(pId_Moneda)
  
  Set lReg = gmMatrixMonedas.Buscar("ID_MONEDA", lId_Moneda)
  If Not lReg Is Nothing Then
    lDecimales = String(lReg("dicimales_mostrar").Value, "0")
    If Not lDecimales = "" Then
      lDecimales = ("." & lDecimales)
    End If
  End If
  
  Fnt_Formato_Moneda = lResult & lDecimales
End Function

Public Function Fnt_Formato_Decimales(pDecimales) As String
Dim lResult As String
Dim lDecimales As String
  
  lResult = "#,##0"
  lDecimales = ""
  
  lDecimales = String(pDecimales, "0")
  If Not lDecimales = "" Then
    lDecimales = ("." & lDecimales)
  End If

  Fnt_Formato_Decimales = lResult & lDecimales
End Function

Public Function Fnt_Redondear_A_Moneda(pValor, pId_Moneda) As Double
Dim lReg As hFields
Dim lDecimales As Variant
Dim lId_Moneda As Double
  
  lDecimales = Null
  
  lId_Moneda = To_Number(pId_Moneda)
  
  Set lReg = gmMatrixMonedas.Buscar("ID_MONEDA", lId_Moneda)
  If Not lReg Is Nothing Then
    lDecimales = NVL(lReg("dicimales_mostrar").Value, 0)
  End If
  
  If IsNull(lDecimales) Then
    Fnt_Redondear_A_Moneda = pValor
  Else
    Fnt_Redondear_A_Moneda = Round(pValor, lDecimales)
  End If
End Function

Public Function Fnt_Calcula_Dia_Habil(pFecha_Inicio As Date, pDias As Long) As Date
'-------------------------
Rem Funcion que entrega cual es el "pDias" h�bil
'--------------------------
Dim lDia_Semana As Integer
Dim lDia As Long
Dim lDias_Habiles As Long
Dim lSabado As Boolean
  
  If pDias = 0 Then
    Fnt_Calcula_Dia_Habil = Fnt_Dia_Habil_MasProximo(pFecha_Inicio)
    Exit Function
  End If
  
  lDias_Habiles = 0
  lSabado = False
  For lDia = 1 To pDias
    
    lDias_Habiles = lDias_Habiles + 1
    
    Rem Dia de la semana de la fecha inicio + lDia
    lDia_Semana = DateTime.Weekday(pFecha_Inicio + lDias_Habiles)
    
    Rem Verifica que el dia sea sabado
    If lDia_Semana = vbSaturday Then
      lDias_Habiles = lDias_Habiles + 2
      lSabado = True
    Rem Verifica que el dia sea sabado
    ElseIf lDia_Semana = vbSunday Then
      If Not lSabado Then
        lDias_Habiles = lDias_Habiles + 1
      End If
    Rem Si el dia no es sabado ni domingo verifica q no sea feriado
    ElseIf Fnt_Verifica_Feriado(pFecha_Inicio + lDias_Habiles) Then
      lDias_Habiles = lDias_Habiles + 1
    End If
    
  Next
  
  Fnt_Calcula_Dia_Habil = pFecha_Inicio + lDias_Habiles
  
End Function

Public Function Fnt_Dia_Habil_MasProximo(pFecha As Date) As Date
'-------------------------
Rem Funcion que entrega el dia h�bil mas proximo
'--------------------------
Dim lDia_Semana As Integer
Dim lFecha As Date

  lDia_Semana = DateTime.Weekday(pFecha)

  Rem Pregunta si la fecha es sabado
  If lDia_Semana = vbSaturday Then
    Rem Pregunta si el lunes es feriado
    If Fnt_Verifica_Feriado(pFecha + 2) Then
      lFecha = Fnt_Dia_Habil_MasProximo(pFecha + 3)
    Else
      lFecha = pFecha + 2
    End If

  Rem Pregunta si la fecha es domingo
  ElseIf lDia_Semana = vbSunday Then
    Rem Pregunta si el lunes es feriado
    If Fnt_Verifica_Feriado(pFecha + 1) Then
      lFecha = Fnt_Dia_Habil_MasProximo(pFecha + 2)
    Else
      lFecha = pFecha + 1
    End If

  Rem si la fecha es dia de semana
  Else

    If Fnt_Verifica_Feriado(pFecha) Then
      lFecha = Fnt_Dia_Habil_MasProximo(pFecha + 1)
    Else
      lFecha = pFecha
    End If

  End If

  Fnt_Dia_Habil_MasProximo = lFecha
  
End Function

Public Function Fnt_DiasHabiles_EntreFechas(pFecha_Inicio As Date, pFecha_Final As Date) As Long
Dim lDias_Totales As Long
Dim lDias_Feriados As Long
Dim lDia As Long
Dim lDia_Semana As Long

  lDias_Totales = pFecha_Final - pFecha_Inicio
  
  For lDia = 0 To lDias_Totales
    lDia_Semana = DateTime.Weekday(pFecha_Inicio + lDia)
    
    If lDia_Semana = vbSaturday Then
      lDias_Feriados = lDias_Feriados + 1
    ElseIf lDia_Semana = vbSunday Then
      lDias_Feriados = lDias_Feriados + 1
    ElseIf Fnt_Verifica_Feriado(pFecha_Inicio) Then
      lDias_Feriados = lDias_Feriados + 1
    End If
  Next
  
  Fnt_DiasHabiles_EntreFechas = lDias_Totales - lDias_Feriados
  
End Function

Public Function Fnt_Verifica_Feriado(pFecha) As Boolean
Dim lcDias_Festivos As Class_Dias_Festivos
Dim lReg As hFields
Dim lDia_Semana  As VBA.VbDayOfWeek

  Fnt_Verifica_Feriado = False
  
  Rem Dia de la semana de la fecha inicio + lDia
  lDia_Semana = DateTime.Weekday(pFecha)
  
  Select Case lDia_Semana
    Case vbSaturday, vbSunday
      Fnt_Verifica_Feriado = True
      GoTo ExitProcedure
  End Select
  
  
  Set lcDias_Festivos = New Class_Dias_Festivos
  With lcDias_Festivos
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    .Campo("fch_dia_feriado").Valor = pFecha
    
    Fnt_Verifica_Feriado = .Es_Feriado
        
    If Not .Errnum = 0 Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas con comprobar el d�a feriado.", .ErrMsg, pConLog:=True)
    End If
  End With
  Set lcDias_Festivos = Nothing
  
ExitProcedure:

End Function

Public Function Fnt_AgregarLog(pId_Log_SubTipo _
                              , pCod_Estado _
                              , pGls_Log_Registro _
                              , Optional pErr_Log_Registro = "" _
                              , Optional pId_Log_Proceso = Null) As Boolean
Dim lcLog_Registro As Class_Log_Registros

  Fnt_AgregarLog = False

  Set lcLog_Registro = New Class_Log_Registros
  With lcLog_Registro
    .Campo("ID_LOG_REGISTRO").Valor = Null
    .Campo("ID_LOG_SUBTIPO").Valor = pId_Log_SubTipo
    .Campo("ID_LOG_PROCESO").Valor = pId_Log_Proceso
    .Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
    .Campo("ID_USUARIO").Valor = gID_Usuario
    .Campo("COD_ESTADO").Valor = pCod_Estado
    .Campo("GLS_LOG_REGISTRO").Valor = pGls_Log_Registro
    .Campo("ERR_LOG_REGISTRO").Valor = pErr_Log_Registro
    
    If Not .Guardar Then
      MsgBox "Problemas al guardar log." & vbLf & vbLf & .ErrMsg, vbCritical + vbOKOnly
      Exit Function
    End If
  End With
  
  Fnt_AgregarLog = True
End Function

Public Function Fnt_MsgError(ByVal pId_Log_SubTipo As Double _
                            , ByVal pMensaje As String _
                            , ByVal pErrMsg As String _
                            , Optional ByVal pConLog As Boolean = False _
                            , Optional ByVal pId_Log_Proceso = Null _
                            , Optional ByVal pCod_Estado As String = cEstado_Log_Error) As VbMsgBoxResult
Dim lcMsgError As Object
                            
                            
  'Fnt_MsgError = MsgBox(pMensaje & vbLf & vbLf & pErrMsg, vbCritical + vbOKOnly)
  
  Set lcMsgError = CreateObject("CSDLLMSGERROR.MsgError")
  Call lcMsgError.Mostrar(pMensaje, pErrMsg)
  Set lcMsgError = Nothing
  
  If pConLog Then
    If (Not pMensaje = "") And (Not pErrMsg = "") Then
      Call Fnt_AgregarLog(pId_Log_SubTipo _
                        , pCod_Estado _
                        , pMensaje _
                        , pErrMsg _
                        , pId_Log_Proceso)
    End If
  End If
End Function

Public Sub Sub_ComboSelectedItem(pCombo As Object, pkey, Optional pIndex = 0)
Dim a As Integer

  If TypeOf pCombo Is TDBCombo Then
    pCombo.SelectedItem = Fnt_FindValueItem(pCombo, pIndex, pkey)
    If Not IsNull(pCombo.SelectedItem) Then
      a = To_Number(pCombo.SelectedItem)
      pCombo.Text = pCombo.Columns(pIndex).ValueItems(a).DisplayValue
    Else
      pCombo.Text = ""
    End If
  Else
    Call DLL_COMUN.Sub_ComboSelectedItem(pCombo, pkey)
  End If
End Sub

Public Function Fnt_CreateObject(pClass As String) As Object
Dim lArchivo As String
Dim lConfig  As Object
Dim lReg     As hFields

On Error GoTo ErrProcedure

  Set Fnt_CreateObject = Nothing

  On Error Resume Next
  Set Fnt_CreateObject = CreateObject(pClass)
  
  If Fnt_CreateObject Is Nothing Then
    'si la variable esta en nothing significa que no se pudo crear el objecto
    'por lo mismo, el sistema va a intentar registrarlo en el SO.
    
    lArchivo = Left(pClass, InStr(1, pClass, ".") - 1)
    
    'Agregamos el ".dll" al archivo y el path, asumiendo que el parth esta finalizado con "\"
    lArchivo = """" & gUbicacionDLLs & lArchivo & ".dll"""
    
    Shell "RegSvr32.exe " & lArchivo & " /s", vbHide
    
    DoEvents
    
    'y nuevamente intento inicar la clase
    Set Fnt_CreateObject = CreateObject(pClass)
  End If
  
  Set lConfig = Nothing
  On Error Resume Next
  Set lConfig = Fnt_CreateObject.GetConfig
  On Error GoTo ErrProcedure
  If Not lConfig Is Nothing Then
    For Each lReg In lConfig
      Select Case lReg("config").Value
        Case cPROCESS_FORM_REP_GEN
          Set Fnt_CreateObject.Form_Reporte_Generico = New Frm_Reporte_Generico
        Case cPROCESS_Id_Empresa
          Fnt_CreateObject.Id_Empresa = Fnt_EmpresaActual
        Case cPROCESS_gDB
          Set Fnt_CreateObject.gDB = gDB
        Case cPROCESS_gId_Usuario
          Fnt_CreateObject.gID_Usuario = gID_Usuario
        Case cPROCESS_FORM_BUSCADOR_CLTE_CTA
          Set Fnt_CreateObject.Form_Buscador_Clte_Cta = New Frm_Busca_Cuentas
        Case cPROCESS_MDI_Principal
          Set Fnt_CreateObject.MDI_Principal = MDI_Principal
        Case cPROCESS_AvanzaGuion
          Set Fnt_CreateObject.gcAvanzaGuion = New Class_AvanceGuion
      End Select
    Next
  End If
  
  
ErrProcedure:

End Function

Public Sub ColocarEn(pObjetoMovible As Object, pObjetoReferncia As Object, pUbicacion As eFormPos)
  Select Case pUbicacion
    Case eFormPos.eFP_Abajo
      pObjetoMovible.Top = pObjetoReferncia.Height
      pObjetoMovible.Left = pObjetoReferncia.Left
    Case eFormPos.eFP_Arriva
      pObjetoMovible.Top = pObjetoReferncia.Top - pObjetoMovible.Height
      pObjetoMovible.Left = pObjetoReferncia.Left
  End Select
End Sub

'Public Sub Sub_AjustaBands(pCollBar As CoolBar)
'Dim lBand     As Band
'Dim lWidth    As Single
'Dim lAncho As Single
'
'  If pCollBar.Orientation = cc3OrientationHorizontal Then
'    lWidth = pCollBar.Width
'  Else
'    lWidth = pCollBar.Height
'  End If
'
'  lAncho = lWidth / pCollBar.Bands.Count
'  For Each lBand In pCollBar.Bands
'    lBand.Width = lAncho
'  Next
'End Sub

Public Sub Sub_CambiaCheck(pGrilla As VSFlexGrid, pValor As Boolean, Optional pColum As String = "CHK")
Dim lLinea As Long
Dim lCol As Long
  
  lCol = pGrilla.ColIndex(pColum)
  For lLinea = 1 To pGrilla.Rows - 1
    pGrilla.Cell(flexcpChecked, lLinea, lCol) = IIf(pValor, flexChecked, flexUnchecked)
  Next
End Sub

Public Sub Sub_CambiaCheck_Inverso(pGrilla As VSFlexGrid, Optional pColum As String = "CHK")
Dim lLinea As Long
Dim lCol As Long
  
  lCol = pGrilla.ColIndex(pColum)
  For lLinea = 1 To pGrilla.Rows - 1
    pGrilla.Cell(flexcpChecked, lLinea, lCol) = IIf(pGrilla.Cell(flexcpChecked, lLinea, lCol) = flexChecked, flexUnchecked, flexChecked)
  Next
End Sub

Public Sub Sub_IniciaFormulario(pClass As String)
Dim lcClass_Form  As Object
Dim lcForm        As Object
Dim lConfig       As hRecord
Dim lReg          As hFields
'------------------------------------------------
Dim lClass As String
Dim lMensaje   As String
  
  
  lClass = DLL_COMUN.DesCifrado(pClass)
  
  Set lcClass_Form = Fnt_CreateObject(lClass)
  
  If lcClass_Form Is Nothing Then
    lMensaje = "No se pudo crear el componente definido para el proceso (" & lClass & ")"
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrFormulario, lMensaje, Err.Description, pConLog:=True)
    GoTo ExitProcedure
  End If
  
  'MsgBox "antes load form"
  Set lcForm = lcClass_Form.LoadForm
  
  'MsgBox "antes dock"
'  Call DockForm(lcForm, MDI_Principal)
  
  On Error GoTo ErrProcedure
  If Not lcForm.fForm_Container Is Nothing Then
    GoTo ExitProcedure
  End If
  
ErrProcedure:
  'MsgBox "antes dock"
  Call DockForm(lcForm, MDI_Principal)
  
ExitProcedure:

End Sub

Public Function Fnt_Iva(pFecha) As Double
Dim lcIva As Class_Iva

  Fnt_Iva = 0

  Rem Valor Iva del Sistema
  Set lcIva = New Class_Iva
  With lcIva
    If .Buscar_Valida(pFecha) Then
      If .Cursor.Count > 0 Then
        'si es que llega a enviar mas de un registro solo te toma el primero que encuentre
        Fnt_Iva = .Cursor(1)("valor").Value
      End If
    End If
  End With
  
  Set lcIva = Nothing
End Function

Public Function Fnt_AgregaCampoReemplazo(pCamposReemplazo As hRecord, pNom_Campo As String, pText_Reemplazo, Optional pLargo, Optional pAling As AlignConstants = vbAlignLeft) As hFields
Dim lReg As hFields

  If pCamposReemplazo Is Nothing Then
    Set pCamposReemplazo = New hRecord
    With pCamposReemplazo
      Call .AddField("Nom_Campo")
      Call .AddField("Text_Campo")
      Call .AddField("Largo")
      Call .AddField("Aling")
    End With
  End If
  
  Set lReg = pCamposReemplazo.Add
  lReg("Nom_Campo").Value = pNom_Campo
  lReg("Text_Campo").Value = pText_Reemplazo
  lReg("Largo").Value = pLargo
  lReg("Aling").Value = pAling
  
  Set Fnt_AgregaCampoReemplazo = lReg
End Function

Public Function Fnt_ReemplazarTexto(pTexto As String, pCamposReemplazo As hRecord) As String
Dim lReg    As hFields
Dim lResult As String
Dim lTexto  As String

  lResult = pTexto

  For Each lReg In pCamposReemplazo
    If IsMissing(lReg("Largo").Value) Then
      lTexto = lReg("Text_Campo").Value
    Else
      lTexto = Space(lReg("Largo").Value)
      
      Select Case lReg("Aling").Value
        Case vbAlignRight
          RSet lTexto = Left(lReg("Text_Campo").Value, lReg("Largo").Value)
        Case Else
          LSet lTexto = Left(lReg("Text_Campo").Value, lReg("Largo").Value)
      End Select
    End If
  
    lResult = Replace(lResult, lReg("Nom_Campo").Value, lTexto)
  Next
  
  Fnt_ReemplazarTexto = lResult
End Function

Public Function Fnt_VerificaEMAIL(pEmail, Optional pConMensaje = True) As Boolean
Dim lMensaje As String

  If Trim(pEmail) = "" Then
    Fnt_VerificaEMAIL = True
    Exit Function
  End If
  
  Fnt_VerificaEMAIL = False
  
  'revisa si tiene el arroba
  If InStr(1, pEmail, "@") <= 0 Then
    lMensaje = "Email Invalido: No tiene @"
    GoTo ExitProcedure
  End If
  
  Fnt_VerificaEMAIL = True

ExitProcedure:
  If pConMensaje Then
    If Not Trim(lMensaje) = "" Then
      MsgBox lMensaje, vbExclamation, "Validaci�n de Email"
    End If
  End If
End Function

Public Function Fnt_IniciaComponente(pCod_Proceso_Componente As String, ByRef pMensaje As String, Optional pErrorNoExiste As Boolean = True) As Object
Dim lcProceso_Componente  As Class_Proceso_Componente

  Set Fnt_IniciaComponente = Nothing
  
  'Busca el componente que corresponde al conciliador
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = pCod_Proceso_Componente
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set Fnt_IniciaComponente = .IniciaClass(pMensaje:=pMensaje, pErrorNoExiste:=pErrorNoExiste)
  End With
  
ExitProcedure:
  Set lcProceso_Componente = Nothing
End Function

Public Function Fnt_IniciaFormulario(pCod_Proceso_Componente As String _
                                   , Optional ByRef pMensaje As String _
                                   , Optional pErrorNoExiste As Boolean = True _
                                   , Optional pMostrarError As Boolean = True) As Object
Dim lcProceso_Componente  As Class_Proceso_Componente
Dim lcComponente          As Object
Dim lcForm        As Object
Dim lConfig       As hRecord
Dim lReg          As hFields
'------------------------------------------------
Dim lClass     As String
  
  
'  lClass = DLL_COMUN.DesCifrado(pClass)
  lClass = pCod_Proceso_Componente
  
  'Busca el componente que corresponde al conciliador
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = pCod_Proceso_Componente
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set lcComponente = .IniciaClass(pMensaje:=pMensaje, pErrorNoExiste:=pErrorNoExiste)
  End With
  
  If lcComponente Is Nothing Then
    If pMostrarError Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_ErrFormulario, pMensaje, Err.Description, pConLog:=True)
    End If
    GoTo ExitProcedure
  End If
  
  'MsgBox "antes load form"
  Set lcForm = lcComponente.LoadForm
  
  'MsgBox "antes dock"
  Call DockForm(lcForm, MDI_Principal)

  Set Fnt_IniciaFormulario = lcComponente

ExitProcedure:

End Function

Public Function Fnt_Busca_Id_Tipo_Conversion(pTabla, Optional pShowError As Boolean = True) As Double
Dim lcTipo_Conversion As Object

Const cMsgError = "Problemas en buscar el tipo de conversi�n."

  Fnt_Busca_Id_Tipo_Conversion = cNewEntidad

  Set lcTipo_Conversion = CreateObject(cDLL_Tipos_Conversion)
  With lcTipo_Conversion
    Set .gDB = gDB
    .Campo("tabla").Valor = pTabla
    If Not .Buscar Then
      If pShowError Then
        Call Fnt_MsgError(.SubTipo_LOG, cMsgError _
                          , .ErrMsg _
                          , pConLog:=True)
      End If
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      If pShowError Then
        MsgBox "No esta definido el tipo de conversion para " & pTabla & ".", vbInformation, "X-P"
      End If
      GoTo ExitProcedure
    End If
    
    Fnt_Busca_Id_Tipo_Conversion = .Cursor(1)("id_tipo_conversion").Value
  End With

  
ErrProcedure:
  If Not Err.Number = 0 Then
    If pShowError Then
      Call Fnt_MsgError(eLS_ErrSystem, cMsgError, Err.Description)
    End If
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcTipo_Conversion = Nothing
End Function

Public Function Fnt_Busca_Id_Origen(pCOD_ORIGEN, Optional pShowError As Boolean = True) As Double
Dim lcOrigenes As Object

Const cMsgError = "Problemas en buscar el tipo de conversi�n."

  Fnt_Busca_Id_Origen = cNewEntidad

  Set lcOrigenes = CreateObject(cDLL_Origenes)
  With lcOrigenes
    Set .gDB = gDB
    .Campo("cod_origen").Valor = pCOD_ORIGEN
    If Not .Buscar Then
      If pShowError Then
        Call Fnt_MsgError(.SubTipo_LOG, cMsgError _
                          , .ErrMsg _
                          , pConLog:=True)
      End If
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      If pShowError Then
        MsgBox "No esta definido el tipo de conversion para " & pCOD_ORIGEN & ".", vbInformation, "X-P"
      End If
      GoTo ExitProcedure
    End If
    
    Fnt_Busca_Id_Origen = .Cursor(1)("id_origen").Value
  End With

  
ErrProcedure:
  If Not Err.Number = 0 Then
    If pShowError Then
      Call Fnt_MsgError(eLS_ErrSystem, cMsgError, Err.Description)
    End If
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcOrigenes = Nothing
End Function

Public Function Fnt_String2Double(pNro_String As String) As Double
Rem FUNCION QUE TRANSFORMA UN NUMERO STRING DEL ESTILO "200.265,5445"
Rem A UN DOUBLE 200265.5445
Dim lString As String

On Error GoTo ErrProcedure
  
  lString = Replace(pNro_String, ".", "")
  lString = Replace(lString, ",", ".")
  
  Fnt_String2Double = lString
  
  Exit Function
  
ErrProcedure:
  Fnt_String2Double = 0
End Function

' Mostrar el formulario indicado, dentro de picDock
Public Sub AnexaFormulario(ByVal pForm As Object, _
                           ByVal picDock As Object, _
                           Optional ByVal pAjustar As Boolean = True)
  ' Hacer el formulario indicado, un hijo del picDock
  ' Si Ajustar es True, se ajustar� al tama�o del contenedor,
  ' si Ajustar es False, se quedar� con el tama�o actual.
  Call SetParent(pForm.hwnd, picDock.hwnd)
  'PosDockForm pForm.hWnd, picDock, ajustar
  'pForm.Show

  pForm.Show
  Call pForm.Move(0, 0)
  If pAjustar Then
    Call pForm.Move(0, 0, picDock.Width, picDock.Height)
  End If
End Sub

Public Function CargaFormularioAlias() As Object
Dim lcFormularioAlias As Object

  Set lcFormularioAlias = Fnt_CreateObject(cDLL_Alias_Formulario)
  Set CargaFormularioAlias = lcFormularioAlias.LoadForm
End Function

Public Sub Sub_StatusBar(pTexto As String)
  With MDI_Principal.StatusBar.Panels("TEXTO")
    .Text = pTexto
    .ToolTipText = pTexto
  End With
End Sub

Public Sub Sub_Elije_EstadoDefault(pCombo As TDBCombo)
Dim lValueItem As TrueDBList80.ValueItem
Dim lColumn    As TrueDBList80.Column
Dim lTag_Colum

  With pCombo
    For Each lColumn In .Columns
      If lColumn.Caption = gcCombo_ColumTag Then
        lTag_Colum = lColumn.ColIndex
        Exit For
      End If
    Next
    
    If Not IsMissing(lTag_Colum) Then
      For Each lValueItem In lColumn.ValueItems
        If lValueItem.Value = gcFlg_SI Then
          Call Sub_ComboSelectedItem(pCombo, lValueItem.DisplayValue)
          Exit For
        End If
      Next
    End If
  End With
End Sub

Public Sub Sub_EsASCIINumero(ByRef pASCII)
  Select Case pASCII
    Case 8, 45, 48 To 59, Asc(gASCIIDecimal)
      'esto es solo para ser mas entendible la comparacion
    Case Else
      pASCII = 0
  End Select
End Sub

Public Sub Sub_CualASCIIDecimal()
Dim lNum_String As String

  lNum_String = Format(3.5, "0.0")

  gASCIIDecimal = Mid(lNum_String, 2, 1)
End Sub

Public Function Fnt_CargaFormPermisos(pCod_Arbol_Sistema, pContainer As Object)
Dim lcRel_Roles_Arbol_Sistema As Class_Rel_Roles_Arbol_Sistema
Dim lControl As VB.Control
'-----------------------------------------------------
Dim lFlg_Tipo_Permiso
  
On Error GoTo ErrProcedure
  
  Set lcRel_Roles_Arbol_Sistema = New Class_Rel_Roles_Arbol_Sistema
  With lcRel_Roles_Arbol_Sistema
    If Not .Buscar_Permiso(pCod_Arbol_Sistema:=pCod_Arbol_Sistema _
                         , pId_Usuario:=gID_Usuario) Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas en la busqueda de las relaciones de Roles/Arbol Sistema", .ErrMsg, pConLog:=True)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      MsgBox "No tiene permisos para ver esta opci�n.", vbCritical
      GoTo ExitProcedure
    End If
  End With
  
  lFlg_Tipo_Permiso = lcRel_Roles_Arbol_Sistema.Cursor(1)("FLG_TIPO_PERMISO").Value
  Fnt_CargaFormPermisos = lFlg_Tipo_Permiso
  
  Select Case lFlg_Tipo_Permiso
    Case gcFlg_TipoPermiso_SoloLectura
      For Each lControl In pContainer.Controls
        If Not Fnt_ValorTag(lControl.Tag, "SOLOLECTURA") = "N" Then
          If TypeOf lControl Is hTextLabel Then
            lControl.Locked = True
          ElseIf TypeOf lControl Is TextBox Then
            lControl.Locked = True
          ElseIf TypeOf lControl Is TDBCombo Then
            lControl.Locked = True
          ElseIf TypeOf lControl Is MSComctlLib.Toolbar Then
            On Error Resume Next
            With lControl
              .Buttons("ADD").Visible = False
              .Buttons("UPDATE").Caption = "Ver"
              .Buttons("DELETE").Visible = False
              .Buttons("ANULAR").Visible = False
              .Buttons("DEL").Visible = False
              .Buttons("UP").Visible = False
              .Buttons("SAVE").Visible = False
              .Buttons("ADD_APO").Visible = False
              .Buttons("ADD_RES").Visible = False
              .Buttons("DOWN").Visible = False
              .Buttons("IMPORT").Visible = False
              .Buttons("LIQUIDAR").Visible = False
            End With
            On Error GoTo ErrProcedure
          ElseIf TypeOf lControl Is VSFlexGrid Then
            lControl.Editable = False
          ElseIf TypeOf lControl Is DTPicker Then
            lControl.Enabled = False
          ElseIf TypeOf lControl Is CheckBox Then
            lControl.Enabled = False
          ElseIf TypeOf lControl Is TDBText Then
            lControl.Enabled = False
          ElseIf TypeOf lControl Is MonthView Then
            lControl.Enabled = False
          ElseIf TypeOf lControl Is OptionButton Then
            lControl.Enabled = False
          ElseIf TypeOf lControl Is TDBCalendar Then
            lControl.Locked = True
          ElseIf TypeOf lControl Is ListView Then
            lControl.Enabled = False
          Else
            Debug.Print TypeName(lControl)
          End If
        End If
      Next
  End Select

  Call Sub_FormControl_Color(pContainer.Controls)

ErrProcedure:

ExitProcedure:


End Function

Public Sub Sub_Exportar_Archivo_Plano(pGrilla As VSFlexGrid, ByRef pArchivoPlano)
Dim lArchivo_Salida As Byte
Dim lNombreArchivo As String
Dim lFila As Double
  
On Error GoTo ErrProcedure
  
  If pGrilla.Rows = 1 Then Exit Sub
  
  With MDI_Principal.CommonDialog
    .CancelError = True
    '.InitDir = "C:\Mis documentos\"
    .Filter = "Texto (*.txt)|*.txt"
    .Flags = cdlOFNExplorer + cdlOFNHideReadOnly + _
             cdlOFNNoReadOnlyReturn + cdlOFNOverwritePrompt + cdlOFNPathMustExist
    .Filename = pArchivoPlano
    .ShowSave
    lNombreArchivo = .Filename
  End With
  
  If lNombreArchivo = "" Then GoTo ErrProcedure
  
  lArchivo_Salida = FreeFile
  Open lNombreArchivo For Output As #lArchivo_Salida
    For lFila = 1 To pGrilla.Rows - 1
      Print #lArchivo_Salida, GetCell(pGrilla, lFila, "TEXTO")
    Next
  Close #lArchivo_Salida
  
  pArchivoPlano = lNombreArchivo
  
  If MsgBox("�Abrir el archivo guardado?", vbYesNo, "Abrir archivo") = vbYes Then
    Call Shell("cmd /c """ & pArchivoPlano & """")
  End If
  
ErrProcedure:
  Rem Usuario presiona el boton Cancelar del Command Dialog
  Rem o presiona No o Cancelar en el dialogo "ya existe archivo...desea reemplazarlo"
  If Err.Number = 32755 Or Err.Number = 1004 Then
    Exit Sub
  End If
  
  If Not Err.Number = 0 Then
    MsgBox Err.Description
  End If
End Sub

Public Function Fnt_Agregar_Log_Proceso(pId_Log_SubTipo)
Dim lcLog_Proceso As Class_Log_Procesos

  Fnt_Agregar_Log_Proceso = Null

  Set lcLog_Proceso = New Class_Log_Procesos
  With lcLog_Proceso
    .Campo("ID_LOG_SUBTIPO").Valor = pId_Log_SubTipo
    If Not .Guardar Then
      GoTo ExitProcedure
    End If
    
    Fnt_Agregar_Log_Proceso = .Campo("ID_LOG_PROCESO").Valor
  End With
  
ExitProcedure:

End Function

Public Sub Sub_StatusWindows(pStatusBar As StatusBar _
                            , Optional pPrimera _
                            , Optional pSegunda _
                            , Optional pTercera _
                            , Optional pCuarta)
  If Not IsMissing(pPrimera) Then
    pStatusBar.Panels("PRIMERA").Text = pPrimera
  End If
  
  If Not IsMissing(pSegunda) Then
    pStatusBar.Panels("SEGUNDA").Text = pSegunda
  End If

  If Not IsMissing(pTercera) Then
    pStatusBar.Panels("TERCERA").Text = pTercera
  End If

  If Not IsMissing(pCuarta) Then
    pStatusBar.Panels("CUARTA").Text = pCuarta
  End If
End Sub

Public Function Fnt_Techo_Numero(pNumero As Double) As Double
Dim lDecimal As Double
  
  lDecimal = pNumero - Int(pNumero)
   
  If lDecimal > 0 Then
    Fnt_Techo_Numero = Int(pNumero) + 1
  Else
    Fnt_Techo_Numero = pNumero
  End If
   
End Function
Public Sub PubPrdObtienePosicion(Formulario As Form)
   Dim strResultado As String
   strResultado = GetSetting("CsGpi", "Pantallas", Formulario.Name, Formulario.Top & "/" & Formulario.Left)
   If Trim(strResultado) <> "" Then
      Formulario.Top = Mid(strResultado, 1, InStr(strResultado, "/") - 1)
      Formulario.Left = Mid(strResultado, InStr(strResultado, "/") + 1)
   End If
End Sub

Public Sub PubPrdGrabaPosicion(ByVal Formulario As Form)
   Dim lngResultado As Long
   Call SaveSetting("CsGpi", "Pantallas", Formulario.Name, Formulario.Top & "/" & Formulario.Left)
End Sub

Function UltimoDiaDelMes(Fecha_Proceso As String) As String
    UltimoDiaDelMes = DateAdd("d", -Day(Fecha_Proceso), Fecha_Proceso)
End Function

Public Sub RentabilidadCartera(ByVal Fecha As Date, _
                               ByVal IdCuenta As Integer, _
                               ByRef rentabilidad_mensual_PESOS As String, _
                               ByRef rentabilidad_mensual_UF As String, _
                               ByRef rentabilidad_mensual_USD As String, _
                               ByRef rentabilidad_mensual_CTA As String, _
                               ByRef rentabilidad_anual_PESOS As String, _
                               ByRef rentabilidad_anual_UF As String, _
                               ByRef rentabilidad_anual_USD As String, _
                               ByRef rentabilidad_anual_CTA As String, _
                               ByRef rentabilidad_ult_12_meses_PESOS As String, _
                               ByRef rentabilidad_ult_12_meses_UF As String, _
                               ByRef rentabilidad_ult_12_meses_USD As String, _
                               ByRef rentabilidad_ult_12_meses_CTA As String, _
                               ByRef volatilidad_mensual As String, _
                               ByRef volatilidad_anual As String, _
                               ByRef volatilidad_ult_12_meses As String)
    Dim lCursor As hRecord
    Dim lReg As hFields
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, IdCuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, Fecha, ePD_Entrada
    gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS$RENTABILIDADES"
    If Not gDB.EjecutaSP Then
        rentabilidad_mensual_PESOS = "N/A"
        rentabilidad_mensual_UF = "N/A"
        rentabilidad_mensual_USD = "N/A"
        rentabilidad_mensual_CTA = "N/A"
        rentabilidad_anual_PESOS = "N/A"
        rentabilidad_anual_UF = "N/A"
        rentabilidad_anual_USD = "N/A"
        rentabilidad_anual_CTA = "N/A"
        rentabilidad_ult_12_meses_PESOS = "N/A"
        rentabilidad_ult_12_meses_UF = "N/A"
        rentabilidad_ult_12_meses_USD = "N/A"
        rentabilidad_ult_12_meses_CTA = "N/A"
        volatilidad_mensual = "N/A"
        volatilidad_anual = "N/A"
        volatilidad_ult_12_meses = "N/A"
        Exit Sub
    End If
    Set lCursor = gDB.Parametros("Pcursor").Valor
    gDB.Parametros.Clear
    For Each lReg In lCursor
        If lReg("hay_valor_cuota_mes").Value = "NO" Then
            rentabilidad_mensual_PESOS = "N/A"
            rentabilidad_mensual_UF = "N/A"
            rentabilidad_mensual_USD = "N/A"
            rentabilidad_mensual_CTA = "N/A"
            volatilidad_mensual = "N/A"
        Else
            rentabilidad_mensual_PESOS = FormatNumber(lReg("rentabilidad_mensual_$$"), 2, True) & "%"
            rentabilidad_mensual_UF = FormatNumber(lReg("rentabilidad_mensual_UF"), 2, True) & "%"
            rentabilidad_mensual_USD = FormatNumber(lReg("rentabilidad_mensual_DO"), 2, True) & "%"
            rentabilidad_mensual_CTA = FormatNumber(lReg("rentabilidad_mensual"), 2, True) & "%"
            volatilidad_mensual = FormatNumber(lReg("volatilidad_mensual"), 2, True) & "%"
        End If
        If lReg("hay_valor_cuota_ano_calendario").Value = "NO" Then
            rentabilidad_anual_PESOS = "N/A"
            rentabilidad_anual_UF = "N/A"
            rentabilidad_anual_USD = "N/A"
            rentabilidad_anual_CTA = "N/A"
            volatilidad_anual = "N/A"
        Else
            rentabilidad_anual_PESOS = FormatNumber(lReg("rentabilidad_anual_$$"), 2, True) & "%"
            rentabilidad_anual_UF = FormatNumber(lReg("rentabilidad_anual_UF"), 2, True) & "%"
            rentabilidad_anual_USD = FormatNumber(lReg("rentabilidad_anual_DO"), 2, True) & "%"
            rentabilidad_anual_CTA = FormatNumber(lReg("rentabilidad_anual"), 2, True) & "%"
            volatilidad_anual = FormatNumber(lReg("volatilidad_anual"), 2, True) & "%"
        End If
        If lReg("hay_valor_cuota_ult_12_meses").Value = "NO" Then
            rentabilidad_ult_12_meses_PESOS = "N/A"
            rentabilidad_ult_12_meses_UF = "N/A"
            rentabilidad_ult_12_meses_USD = "N/A"
            rentabilidad_ult_12_meses_CTA = "N/A"
            volatilidad_ult_12_meses = "N/A"
        Else
            rentabilidad_ult_12_meses_PESOS = FormatNumber(lReg("rentabilidad_ult_12_meses_$$"), 2, True) & "%"
            rentabilidad_ult_12_meses_UF = FormatNumber(lReg("rentabilidad_ult_12_meses_UF"), 2, True) & "%"
            rentabilidad_ult_12_meses_USD = FormatNumber(lReg("rentabilidad_ult_12_meses_DO"), 2, True) & "%"
            rentabilidad_ult_12_meses_CTA = FormatNumber(lReg("rentabilidad_ult_12_meses"), 2, True) & "%"
            volatilidad_ult_12_meses = FormatNumber(lReg("volatilidad_ult_12_meses"), 2, True) & "%"
        End If
    Next
    Set lCursor = Nothing
    Set lReg = Nothing
End Sub

Public Sub Sub_LimpiarCheckArbol(pArbol As TreeView)
Dim lcNodo As Node
  
  For Each lcNodo In pArbol.Nodes
    If lcNodo.Checked Then
      lcNodo.Checked = False
    End If
  Next
End Sub

Public Sub Sub_Interactivo(Optional pgRelogDB As Object = Nothing)
Static pTime As Long

  If (pTime + 1000) < GetTickCount Then
    DoEvents
    pTime = GetTickCount
  End If
  
  If Not pgRelogDB Is Nothing Then
    pgRelogDB.AvanzaRelog
  End If
End Sub

Public Function Fnt_LetraMayuscula(KeyAscii As Integer) As Integer
  Fnt_LetraMayuscula = Asc(UCase(Chr(KeyAscii)))
End Function

Public Function Fnt_MesNombre(pNumeroMes) As String
Dim laMeses
  laMeses = Array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
  Fnt_MesNombre = laMeses(pNumeroMes)
End Function
'---------------------------------------------------------------------------------------
' Procedure : DeterminaTipoCustodia
' Author    : Administrador
' Date      : 18/06/2008
' Purpose   :
'---------------------------------------------------------------------------------------
'
Function DeterminaTipoCustodia(sChkAporteRetiro) As String
    If sChkAporteRetiro = "NO" Then
        DeterminaTipoCustodia = gcOPERACION_Custodia_NoCapital
    Else
        DeterminaTipoCustodia = gcOPERACION_Custodia
    End If
End Function
Function PrimerDiaMes(Fecha_Proceso As String) As String
    PrimerDiaMes = DateAdd("d", -Day(Fecha_Proceso) + 1, Fecha_Proceso)
End Function
Function UltimoDiaDelMesEnCurso(Fecha_Proceso As String) As String
    UltimoDiaDelMesEnCurso = DateAdd("d", -Day(DateAdd("m", 1, Fecha_Proceso)), DateAdd("m", 1, Fecha_Proceso))
End Function
Function Fnt_Lee_Mail_BackOffice(pId_empresa) As String
Dim lcAlias  As Object
Dim lMail
Dim cOrigen_Dsc As String
Dim cTipo_Conversion_Dsc As String

    cOrigen_Dsc = "BACKOFFICE"
    cTipo_Conversion_Dsc = "MAIL"

    lMail = Null
    Set lcAlias = CreateObject(cDLL_Alias)
    Set lcAlias.gDB = gDB
    lMail = lcAlias.AliasCSBPI2SYSTEM(pCodigoSYSTEM:=cOrigen_Dsc _
                                         , pCodigoCSBPI:=cTipo_Conversion_Dsc _
                                         , pId_Entidad:=pId_empresa)
    If IsNull(lMail) Then
        lMail = ""
    End If
ExitProcedure:
  Fnt_Lee_Mail_BackOffice = lMail
End Function

Public Function Buscar_Id_Moneda(pCod_Moneda As String) As String
Dim lcMonedas As Object
  
  Set lcMonedas = Fnt_CreateObject(cDLL_Monedas)
  With lcMonedas
    .Campo("COD_MONEDA").Valor = pCod_Moneda
    If .Buscar Then
      Buscar_Id_Moneda = .Cursor(1)("ID_MONEDA").Value
    End If
  End With
  
End Function

Public Function Fnt_Buscar_Id_Periodicidad(pCod_Periodicidad As String) As Integer
Dim lcPeriodicidad As Class_Comi_Fija_Periodicidad
Dim lReg As hCollection.hFields

  Set lcPeriodicidad = New Class_Comi_Fija_Periodicidad
  With lcPeriodicidad
    If .Buscar Then
      If lcPeriodicidad.Cursor.Count > 0 Then
        For Each lReg In lcPeriodicidad.Cursor
          If lReg("DSC_COMISION_FIJA_PERIODICIDAD").Value = pCod_Periodicidad Then
            Fnt_Buscar_Id_Periodicidad = lReg("DIAS").Value
            Exit For
          End If
        Next
      End If
    End If
  End With
  Set lcPeriodicidad = Nothing
  
End Function



Public Function Fnt_Obtiene_Permiso_Formulario(pCod_Arbol_Sistema) As String
Dim lcRel_Roles_Arbol_Sistema As Class_Rel_Roles_Arbol_Sistema
'-----------------------------------------------------
Dim lFlg_Tipo_Permiso
  
On Error GoTo ErrProcedure
  
    Set lcRel_Roles_Arbol_Sistema = New Class_Rel_Roles_Arbol_Sistema
    With lcRel_Roles_Arbol_Sistema
        If Not .Buscar_Permiso(pCod_Arbol_Sistema:=pCod_Arbol_Sistema _
                             , pId_Usuario:=gID_Usuario) Then
            Call Fnt_MsgError(.SubTipo_LOG, "Problemas en la busqueda de las relaciones de Roles/Arbol Sistema", .ErrMsg, pConLog:=True)
            GoTo ExitProcedure
        End If
        
        If .Cursor.Count <= 0 Then
            MsgBox "No tiene permisos para ver esta opci�n.", vbCritical
            GoTo ExitProcedure
        End If
    End With

    lFlg_Tipo_Permiso = lcRel_Roles_Arbol_Sistema.Cursor(1)("FLG_TIPO_PERMISO").Value
    Fnt_Obtiene_Permiso_Formulario = lFlg_Tipo_Permiso

ErrProcedure:

ExitProcedure:

End Function
'---------------------------------------------------------------------------------------
' Procedimiento : validaPorcentajeFamilias
' Autor         : JOVB
' Fecha         : 20-08-2009
' Proposito     : Validar que la sumatoria de los porcentajes de las Familias no sobrepase el 100%
'---------------------------------------------------------------------------------------
Public Function validaPorcentaje(ByVal pGrilla As VSFlexGrid, _
                                  ByVal cellEditBuffer As String, _
                                  ByVal intIdPadre As Long, _
                                  ByVal intRow As Long) As Double
    Dim intFila As Integer
    Dim lngSumaPorcentaje As Double
    
    lngSumaPorcentaje = CDbl("0" & cellEditBuffer)
    
    For intFila = 1 To pGrilla.Rows - 1
        If GetCell(pGrilla, intFila, "id_padre_arbol_clase_inst") = intIdPadre Then
            If intRow <> intFila Then
                lngSumaPorcentaje = lngSumaPorcentaje + CLng("0" & GetCell(pGrilla, intFila, "porcentaje"))
            End If
        End If
    Next
    
    validaPorcentaje = lngSumaPorcentaje
    
End Function
Public Sub Sub_Tama�oMinimo(pControl, pHeight, pWidth)
On Error Resume Next

  If pControl.Height < pHeight Then
    pControl.Height = pHeight
  End If
  
  If pControl.Width < pWidth Then
    pControl.Width = pWidth
  End If
End Sub
Public Function Fnt_Entrega_IdCuenta(ByVal pNum_Cuenta, Optional pId_empresa As Integer = 0) As String
Dim lcCuenta As Object

Fnt_Entrega_IdCuenta = ""

Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
With lcCuenta
    .Campo("num_cuenta").Valor = pNum_Cuenta
    If pId_empresa <> 0 Then
       .Campo("Id_Empresa").Valor = pId_empresa
    End If
    If .Buscar_Vigentes Then
        If .Cursor.Count = 1 Then
            Fnt_Entrega_IdCuenta = .Cursor(1)("id_cuenta").Value
        End If
    End If
End With
Set lcCuenta = Nothing
End Function
Public Function Fnt_Entrega_IdCliente(ByVal pRut_Cliente) As String
Dim lcCliente As Object

Fnt_Entrega_IdCliente = ""

Set lcCliente = Fnt_CreateObject(cDLL_Clientes)
With lcCliente
    .Campo("rut_cliente").Valor = pRut_Cliente
    If .Buscar(True) Then
        If .Cursor.Count = 1 Then
            Fnt_Entrega_IdCliente = .Cursor(1)("id_cliente").Value
        End If
    End If
End With
Set lcCliente = Nothing
End Function
Public Sub Sub_Entrega_DatosCuenta(ByVal pId_Cuenta, Optional pRut_Cliente As String = "", Optional pNombre_Cliente As String = "", Optional pNum_Cuenta As String = "", _
                                                     Optional pPerfilRiesgo As String = "", Optional pMonedaCuenta As String = "", Optional pAsesor As String = "", _
                                                     Optional pIdMoneda As String = "", Optional pfechaOperativa As String = "", Optional PError As String = "")
Dim lcCuenta As Object
Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
With lcCuenta
    .Campo("id_cuenta").Valor = pId_Cuenta
    If .Buscar_Vigentes Then
        If .Cursor.Count = 1 Then
            pNum_Cuenta = .Cursor(1)("num_cuenta").Value
            pRut_Cliente = .Cursor(1)("rut_cliente").Value
            pNombre_Cliente = .Cursor(1)("nombre_cliente").Value
            pPerfilRiesgo = .Cursor(1)("dsc_perfil_riesgo").Value
            pMonedaCuenta = .Cursor(1)("dsc_moneda").Value
            pAsesor = .Cursor(1)("Nombre_asesor").Value
            pIdMoneda = .Cursor(1)("id_moneda")
            pfechaOperativa = .Cursor(1)("Fecha_operativa").Value
        End If
    Else
            PError = .ErrMsg
    End If
End With
Set lcCuenta = Nothing
End Sub
Public Sub Sub_Entrega_DatosCliente(ByVal pId_Cliente, Optional pRut_Cliente As Object = Nothing, Optional pNombre_Cliente As Object = Nothing)
Dim lcCliente As Object

Set lcCliente = Fnt_CreateObject(cDLL_Clientes)
With lcCliente
    .Campo("id_cliente").Valor = pId_Cliente
    If .Buscar(True) Then
        If .Cursor.Count > 0 Then
            If Not pRut_Cliente Is Nothing Then
                pRut_Cliente.Text = .Cursor(1)("rut_cliente").Value
            End If
            If Not pNombre_Cliente Is Nothing Then
                pNombre_Cliente.Text = .Cursor(1)("nombre_cliente").Value
            End If
        End If
    End If
End With
Set lcCliente = Nothing
End Sub
Public Sub Sub_Entrega_DatosNemotecnico(ByVal pId_Nemotecnico, ByRef pNemotecnico As String)
Dim lcNemotecnico As Class_Nemotecnicos
pNemotecnico = ""

Set lcNemotecnico = New Class_Nemotecnicos
With lcNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
        If .Cursor.Count = 1 Then
            pNemotecnico = .Cursor(1)("nemotecnico").Value
        End If
    End If
End With
Set lcNemotecnico = Nothing
End Sub
Public Sub Sub_ValidarColumnas(KeyAscii As Integer, ByVal intColumna As Integer, ByVal TipoValor)
Dim inti As Integer
    Select Case TipoValor
        Case vb_vs_numerico
            If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> vbKeyReturn _
                    And KeyAscii <> vbKeyLeft And KeyAscii <> vbKeyRight And KeyAscii <> vbKeyUp _
                    And KeyAscii <> vbKeyDown Then
                KeyAscii = False
            End If
        Case vb_vs_alfanumerico
            KeyAscii = Asc(UCase(Chr(KeyAscii)))
        Case vb_vs_decimal
            If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> vbKeyReturn _
                    And KeyAscii <> vbKeyLeft And KeyAscii <> vbKeyRight And KeyAscii <> vbKeyUp _
                    And KeyAscii <> vbKeyDown And KeyAscii <> 46 Then
                KeyAscii = False
              End If
          Case vb_vs_fecha
    End Select
End Sub


