Attribute VB_Name = "Prc_ComponentOne"
Option Explicit

'Public Sub Sub_Grilla2VsPrinter(ByRef pVsPrinter As VsPrinter _
'                              , ByRef pGrilla As VSFlexGrid _
'                              , Optional pTitulo4Pagina As Boolean = True _
'                              , Optional pNoEndTable As Boolean = False _
'                              , Optional pLineaNoLinea As Boolean = True _
'                              , Optional pHeaders _
'                              , Optional pConcideraCabecera As Boolean = False _
'                              , Optional pLineasCabezeras _
'                              , Optional pLineas1raHoja _
'                              , Optional pLineasOtrasHojas)
'Dim lVSFormat As String
'Dim lVSHeader As String
'Dim lVSRecord As String
''---------
'Dim lCantColum  As Long
'Dim lLinea      As Long
'Dim lColum      As Long
'Dim lText       As String
'Dim lAncho      As Double
''----------------------------
'Dim lHojas      As Long
'Dim lhCabeceras As hRecord
'Dim lfCabecera  As hFields
'Dim lLinea_Table As Long
'
'  lCantColum = 0
'  lVSFormat = ""
'  lVSHeader = ""
'
'  Set lhCabeceras = New hRecord
'  With lhCabeceras
'    .AddField "Texto"
'  End With
'
'  With pGrilla
'    lLinea = 0
'    For lColum = 0 To .Cols - 1
'      If Not .ColHidden(lColum) Then
'        If Not lVSHeader = "" Then
'          lVSHeader = lVSHeader & "|"
'          lVSFormat = lVSFormat & "|"
'        End If
'
'        lVSHeader = lVSHeader & .TextMatrix(lLinea, lColum)
'        lVSFormat = lVSFormat & Fnt_Formats_Grilla2VsPrinter(pGrilla, lColum)
'
'        lCantColum = lCantColum + 1
'      End If
'    Next
'
'    If Not IsMissing(pHeaders) Then
'      lVSHeader = pHeaders
'    End If
'
'    pVsPrinter.StartTable
'    pVsPrinter.TableCell(tcAlignCurrency) = False
'
'    lHojas = 1
'
'    lCantColum = 0
'    lLinea_Table = 0
'    For lLinea = IIf(pConcideraCabecera, 0, 1) To (.Rows - 1) '.FixedRows To (.Rows - 1)
'      lLinea_Table = lLinea_Table + 1
'      lVSRecord = ""
'      lCantColum = 0
'      For lColum = 0 To .Cols - 1
'        If Not .ColHidden(lColum) Then
'          lCantColum = lCantColum + 1
'          If Not lVSRecord = "" Then
'            lVSRecord = lVSRecord & "|"
'          End If
'
'          lText = String(.RowOutlineLevel(lLinea), vbTab)
'
'          If .ColDataType(lColum) = flexDTBoolean Then
'            If .Cell(flexcpChecked, lLinea, lColum) = flexChecked Then
'              lText = lText & "X"
'            End If
'          Else
'            lText = lText & .Cell(flexcpTextDisplay, lLinea, lColum)
'          End If
'
'          lVSRecord = lVSRecord & lText
'        End If
'      Next
'
'      If Not IsMissing(pLineasCabezeras) Then
'        If (lLinea + 1) <= pLineasCabezeras Then
'          lhCabeceras.Add.Fields("texto").Value = lVSRecord
'        End If
'      End If
'
'      If Not IsMissing(pLineas1raHoja) Then
'        If (lHojas = 1) And (((lLinea + 1) Mod pLineas1raHoja) = 0) Then
'          For Each lfCabecera In lhCabeceras
'            Call pVsPrinter.AddTable(lVSFormat, lVSHeader, lfCabecera("texto").Value, pGrilla.BackColorFixed, vbRed, pTitulo4Pagina)
'          Next
'          pVsPrinter.TableCell(tcBackColor, lLinea_Table, 1, lLinea_Table + pLineas1raHoja, lCantColum) = pGrilla.BackColorFixed
'          lLinea_Table = lLinea_Table + pLineasCabezeras
'          lHojas = lHojas + 1
'        End If
'      End If
'
'      If Not IsMissing(pLineasOtrasHojas) Then
'        If (Not lHojas = 1) And (lLinea > pLineas1raHoja) And (((lLinea - pLineas1raHoja + 1) Mod pLineasOtrasHojas) = 0) Then
'          For Each lfCabecera In lhCabeceras
'            Call pVsPrinter.AddTable(lVSFormat, lVSHeader, lfCabecera("texto").Value, pGrilla.BackColorFixed, vbRed, pTitulo4Pagina)
'          Next
'          pVsPrinter.TableCell(tcBackColor, lLinea_Table, 1, lLinea_Table + pLineasOtrasHojas, lCantColum) = pGrilla.BackColorFixed
'          lLinea_Table = lLinea_Table + pLineasCabezeras
'          lHojas = lHojas + 1
'        End If
'      End If
'
'
'
'      Call pVsPrinter.AddTable(lVSFormat, lVSHeader, lVSRecord, pGrilla.BackColorFixed, , pTitulo4Pagina)
'    Next
'
'    If lLinea = 0 Then
'      If pLineaNoLinea Then
'        For lColum = 0 To .Cols - 1
'          If Not lVSRecord = "" Then
'            lVSRecord = lVSRecord & "|"
'          End If
'          lVSRecord = lVSRecord & " "
'        Next
'        Call pVsPrinter.AddTable(lVSFormat, lVSHeader, lVSRecord, pGrilla.BackColorFixed, , pTitulo4Pagina)
'      End If
'    End If
'
'    For lLinea = 0 To (.FixedRows - 1)
'      'Configura los caption de la grilla
'      pVsPrinter.TableCell(tcFontBold, lLinea) = True
'      pVsPrinter.TableCell(tcFontSize, lLinea) = 7
'      pVsPrinter.TableCell(tcBackColor, lLinea) = pGrilla.BackColorFixed
'    Next
'
'    'Ajusta los arnchos a los textos
'    For lLinea = IIf(pConcideraCabecera, 1, 0) To pVsPrinter.TableCell(tcRows)
'      For lColum = 1 To pVsPrinter.TableCell(tcCols)
'        lAncho = pVsPrinter.TextWidth(pVsPrinter.TableCell(tcText, lLinea, lColum))
'        If lAncho > pVsPrinter.TableCell(tcColWidth, col1:=lColum) Then
'          pVsPrinter.TableCell(tcColWidth, col1:=lColum) = lAncho
'        End If
'      Next
'    Next
'
'    pVsPrinter.FontSize = 6
'    pVsPrinter.Align = vbAlignRight
'    pVsPrinter.Paragraph = "Cantidad: " & Format(.Rows - 1, "#,##0")
'
'    If Not pNoEndTable Then
'      pVsPrinter.EndTable
'    End If
'  End With
'End Sub

Public Function Fnt_Formats_Grilla2VsPrinter(pGrilla As VSFlexGrid, pCol As Long) As String
Dim lFormat As String
Dim lAling As String

  lFormat = ""
  lAling = ""
  With pGrilla
    Select Case .FixedAlignment(pCol)
      Case flexAlignRightCenter, flexAlignRightBottom, flexAlignRightTop
        lAling = ">"
    End Select
    
    'lFormat = lFormat & lAling & "=+~" & .ColWidth(pCol)
    lFormat = lFormat & lAling & "+~" & .ColWidth(pCol)
  End With
  
  Fnt_Formats_Grilla2VsPrinter = lFormat
End Function


Public Function Fnt_FindValueItem(pCombo As TDBCombo, pIndexColum, pValue)
Dim i As Long

  Fnt_FindValueItem = Null
  
  For i = 0 To pCombo.Columns(pIndexColum).ValueItems.Count - 1
    ' If pCombo.Columns(pIndexColum).ValueItems(i).Value = Trim(pValue) Then
    If Trim(pCombo.Columns(pIndexColum).ValueItems(i).Value) = Trim(pValue) Then
      Fnt_FindValueItem = Trim(Str(i))
      Exit For
    End If
  Next
End Function

Public Function Fnt_FindValue4Display(pCombo As TDBCombo, pDisplay, Optional pIndexColum = 0)
Dim lEncontrado

  Fnt_FindValue4Display = ""
  
  With pCombo.Columns(pIndexColum)
    lEncontrado = .Find(pDisplay, dblSeekEQ, True)
    If Not IsNull(lEncontrado) Then
      Fnt_FindValue4Display = .ValueItems(.Find(pDisplay, dblSeekEQ, True)).Value
    End If
  End With
End Function

Public Sub UnionCell(ByRef pGrilla As VSFlexGrid, ByVal P_Row As Long, ByVal P_Col1 As Long, ByVal P_Col2 As Long, ByVal P_Texto As String, ByVal P_Color As String, Optional pNegrita As Boolean = False)
   Dim r1&, c1&, r2&, c2&
   
   With pGrilla
      .MergeCells = flexMergeFree
      .MergeRow(P_Row) = True
      .Cell(flexcpText, P_Row, P_Col1, P_Row, P_Col2) = P_Texto
      .GetMergedRange P_Row, P_Col1, r1, r2, c1, c2
      .Cell(flexcpBackColor, P_Row, P_Col1, P_Row, P_Col2) = P_Color
      If pNegrita Then
         .Cell(flexcpFontBold, P_Row, P_Col1, P_Row, P_Col2) = True
      End If
   End With
End Sub

Public Sub SetCell(ByRef pGrilla As VSFlexGrid, ByVal P_Row As Long, ByVal P_Col As String, ByVal P_Caption As String, Optional pAutoSize As Boolean = True)
Dim lCol As Long
 
  With pGrilla
    lCol = .ColIndex(P_Col)
    If lCol = -1 Then
      MsgBox "La columna """ & P_Col & """ no existe en la grilla " & pGrilla.Name & ".", vbCritical
    Else
      Call gRelogDB.AvanzaRelog
        
      .TextMatrix(P_Row, lCol) = P_Caption
      If pAutoSize Then
        Call .AutoSize(lCol)
      End If
    End If
  End With
End Sub

Public Function GetCell(ByRef pGrilla As VSFlexGrid, ByVal P_Row As Long, ByVal P_Col As String) As Variant
  Dim lCol As Long
 
  With pGrilla
    lCol = .ColIndex(P_Col)
    GetCell = .TextMatrix(P_Row, lCol)
  End With
End Function

Public Sub Sub_LimpiarTDBCombo(ByRef pCombo As TDBCombo)
  With pCombo
    'Limpia los datos del control
    .Clear
    .Font.Charset = 0
    'Deja sin columnas el control
    While Not .Columns.Count = 0
      .Columns.Remove 0
    Wend
    
  End With
End Sub

Public Sub Sub_AjustaColumnas_Grilla(pGrilla As VSFlexGrid)
Dim lCol As Long

  For lCol = 0 To pGrilla.Cols - 1
    Call pGrilla.AutoSize(lCol)
  Next
End Sub

Public Sub Sub_hRecord2Grilla(pCursor As hRecord _
                            , pGrilla As VSFlexGrid _
                            , Optional pColum_PK _
                            , Optional pAutoSize As Boolean = False)
Dim lreg As hFields
Dim lRemCamp As hFields
Dim lCol As hRecord
'------------------------------------
Dim lLinea As Long
Dim lKey As String
Dim lCampo As Variant
  
  If pCursor.Count <= 0 Then
    Exit Sub
  End If
  
  Set lCol = New hRecord
  With lCol
    .AddField "Campo"
    .AddField "Colum"
  End With
  
  For lLinea = 0 To pGrilla.Cols - 1
    lKey = UCase(pGrilla.ColKey(lLinea))
    
    If Not IsMissing(pColum_PK) Then
      If lKey = "COLUM_PK" Then
        lKey = pColum_PK
      End If
    End If
    
    With lCol.Add
      .Fields("campo").Value = lKey
      .Fields("colum").Value = lLinea
    End With
  Next
  
  For Each lreg In pCursor
    Call Sub_Interactivo(gRelogDB)
    lLinea = pGrilla.Rows
    pGrilla.AddItem ""
    For Each lRemCamp In lCol
      lCampo = SiNoExisteCampo(lreg, lRemCamp("campo").Value, Empty)
      If Not IsEmpty(lCampo) Then
         pGrilla.TextMatrix(lLinea, lRemCamp("colum").Value) = "" & lCampo
      End If
    Next
  Next
  If pAutoSize Then
    Call Sub_AjustaColumnas_Grilla(pGrilla)
  End If
End Sub

Public Function SiNoExisteCampo(phFields As hFields, pCampo As String, pValor)
Dim lValor As Variant
Dim i As Integer
  'On Error GoTo ExitProcedure
  lValor = pValor
'  lValor = phFields(pCampo).Value
    For i = 1 To phFields.Count
        If UCase(phFields(i).Name) = UCase(pCampo) Then
            lValor = phFields(pCampo).Value
            Exit For
        End If
    Next i
    SiNoExisteCampo = lValor
'ExitProcedure:

  'SiNoExisteCampo = lValor
'  On Error GoTo 0
End Function
