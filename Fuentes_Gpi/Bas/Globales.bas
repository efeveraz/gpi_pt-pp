Attribute VB_Name = "Globales"
Option Explicit

'------------------------------------------------------------------------------
' APIS para obtener los milisegundos
'------------------------------------------------------------------------------
Public Declare Function GetTickCount Lib "kernel32" () As Long

'------------------------------------------------------------------------------
' APIS para incluir las ventanas en un PictureBox
'------------------------------------------------------------------------------
'
' Para hacer ventanas hijas
Public Declare Function SetParent Lib "user32" _
    (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
'
' Para mostrar una ventana seg�n el handle (hwnd)
' ShowWindow() Commands
Public Enum eShowWindow
    HIDE_eSW = 0&
    SHOWNORMAL_eSW = 1&
    NORMAL_eSW = 1&
    SHOWMINIMIZED_eSW = 2&
    SHOWMAXIMIZED_eSW = 3&
    MAXIMIZE_eSW = 3&
    SHOWNOACTIVATE_eSW = 4&
    SHOW_eSW = 5&
    MINIMIZE_eSW = 6&
    SHOWMINNOACTIVE_eSW = 7&
    SHOWNA_eSW = 8&
    RESTORE_eSW = 9&
    SHOWDEFAULT_eSW = 10&
    MAX_eSW = 10&
End Enum

Public Declare Function ShowWindow Lib "user32" _
    (ByVal hwnd As Long, ByVal nCmdShow As eShowWindow) As Long
'
' Para posicionar una ventana seg�n su hWnd
Public Declare Function MoveWindow Lib "user32" _
    (ByVal hwnd As Long, ByVal x As Long, ByVal y As Long, _
    ByVal nWidth As Long, ByVal nHeight As Long, ByVal bRepaint As Long) As Long
'
' Para cambiar el tama�o de una ventana y asignar los valores m�ximos y m�nimos del tama�o
Private Type POINTAPI
    x As Long
    y As Long
End Type
Private Type RECTAPI
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type
Public Type WINDOWPLACEMENT
    Length As Long
    Flags As Long
    ShowCmd As Long
    ptMinPosition As POINTAPI
    ptMaxPosition As POINTAPI
    rcNormalPosition As RECTAPI
End Type
Public Declare Function GetWindowPlacement Lib "user32" _
    (ByVal hwnd As Long, ByRef lpwndpl As WINDOWPLACEMENT) As Long

'-------------------------------------------------------------------------------------------------------
Public Const LOCALE_SDECIMAL = &HE 'separador decimal
Public Const LOCALE_STHOUSAND = &HF 'separador de miles
 
Public Const LOCALE_SMONDECIMALSEP = &H16 'separador decimal en las monedas
Public Const LOCALE_SMONTHOUSANDSEP = &H17 'separador de miles en las monedas
 
Public Const LOCALE_USER_DEFAULT = &H400 'presentar informaci�n del usuario

Public Const LOCALE_SLIST As Long = &HC
 
Declare Function SetLocaleInfo Lib "kernel32" Alias "SetLocaleInfoA" (ByVal Locale As Long, _
     ByVal LCType As Long, ByVal lpLCData As String) As Long

'-------------------------------------------------------------------------------------------------------
Global gDB As DLL_DB.Class_Conect

Global gRelogDB As Class_RelogDB


'----------------------------------------------------------------------------
'-- Constante para la configuracion del sistema
'----------------------------------------------------------------------------
Global gStrPictureEmpresa As String
Global gStrPictureEmpresaGrande As String
Global gUbicacionDLLs As String


'----------------------------------------------------------------------------
'-- Matriz para las monedas.
'----------------------------------------------------------------------------
Global gmMatrixMonedas As hRecord


'----------------------------------------------------------------------------
'-- Usuario del sistema
'----------------------------------------------------------------------------
Global gID_Usuario As Double
Global gDsc_Usuario As String
Global gPassword As String
Global gId_Empresa As Double
Global gDsc_Empresa As String
Global gTipo_Menu As String

'----------------------------------------------------------------------------
'-- Constante para el Pais
'----------------------------------------------------------------------------
'Public Const gcPais_Chile = "CHI"

'----------------------------------------------------------------------------
'-- Agregada por Hans Muller, para pruebas de apertura de HTML con browser
'----------------------------------------------------------------------------
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" ( _
   ByVal hwnd As Long, ByVal lpOperacion As String, ByVal lpFile As String, _
   ByVal lpParametros As String, ByVal lpDirectorio As String, ByVal nShowCmd As Long) As Long

'----------------------------------------------------------------------------
'-- Constante para Tipo de Permisos
'----------------------------------------------------------------------------
Public Const cTPermiso_Lectura = "L"
'Public Const cTPermiso_Total = "T"

'----------------------------------------------------------------------------
'-- Constante para Tipo de Cuota
'----------------------------------------------------------------------------
Public Const cTCuota_Conocida = "C"
Public Const cTCuota_Desconocida = "D"

'----------------------------------------------------------------------------
'-- Constante para mensaje de error del cierre
'----------------------------------------------------------------------------
Public Const cMsgErrCierreCuenta = "    Se encontro el siguiente mensaje: " & vbLf & vbLf

'----------------------------------------------------------------------------
'-- Constante Tipos de Conversion
'----------------------------------------------------------------------------
Public Const cTipo_Conversion_Instrumento = 1
Public Const cTipo_Conversion_Sectores = 2
Public Const cTipo_Conversion_Moneda = 3
Public Const cTipo_Conversion_Nemotecnico = 4
Public Const cTipo_Conversion_Oper_Detalle_FFMM = 5
Public Const cTipo_Conversion_Oper_Detalle_RV = 7
Public Const cTipo_Conversion_Oper_Detalle_RF = 8
'Public Const cTipo_Conversion_Oper_Detalle_FFMM_SEC = 10
'Public Const cTipo_Conversion_Oper_Detalle_RV_SEC = 11
'Public Const cTipo_Conversion_Oper_Detalle_RF_SEC = 12

'----------------------------------------------------------------------------
'-- Constante Origenes
'----------------------------------------------------------------------------
Public Const cOrigen_Super_AFP = 1
Public Const cOrigen_SVS = 2
'Public Const cOrigen_BAC_Fondos = 3
Public Const cOrigen_BAC_Fondos_BBVA = "BAC_FONDOS_BBVA"
Public Const cOrigen_CDS_Fact_RV = "CDS_FACT_RV"
Public Const cOrigen_CDS_Mov_FFMM = "CDS_QRY_FM_MOVCUO"
Public Const cOrigen_Participe_FFMM = "CODIGO_PARTICIPE"
Public Const cOrigen_Sec_Mov_FFMM = "AD_QRY_FM_MOVCUO"
Public Const cOrigen_Sec_Mov_RV = "AD_QRY_CB_MOVTO_ACCIONES"
Public Const cOrigen_Sec_Mov_RF = "AD_QRY_CB_MOVTO_RFIJA"
Public Const cOrigen_Sec_Planes = "SEC_PLANES"
Public Const cOrigen_BBVA_Gestion_Bolsa = "BBVA_GESTION_BOLSA"

'BCI
Public Const cOrigen_Bolsa = "BOLSA"
Public Const cOrigen_BAC_Fondos = "BAC_FONDOS"

'Security
Public Const cOrigen_Magic_Valores = "MAGIC_VALORES"
Public Const cOrigen_Magic_FFMM = "MAGIC_FFMM"

'----------------------------------------------------------------------------
'-- Constante Repositorio ASCII
'----------------------------------------------------------------------------
Public Const cRep_ASCII_Operaciones = "OPERACIONES"

'----------------------------------------------------------------------------
'-- Constante Estado para los Cierres
'----------------------------------------------------------------------------
Public Const cCierre_Estado_Cerrado = "C"
Public Const cCierre_Estado_Pendiente = "P"
Public Const cCierre_Estado_Reprocesando = "R"

'----------------------------------------------------------------------------
'-- Codigo de Mercados de Transaccion
'----------------------------------------------------------------------------
Public Const cMercado_Transac_Santiago = 15

'----------------------------------------------------------------------------
'-- Define estructura y arreglo para almacernar clase menu para cargar formularios dinammicos
'----------------------------------------------------------------------------
Public Type Est_Clase_Menu
    ID_Arbol_Sistema    As Integer
    Opc_Menu            As String
    Clase               As String
End Type

Global gClases_menu() As Est_Clase_Menu

'----------------------------------------------------------------------------
'-- Define constantes para la cartola consolidada
'----------------------------------------------------------------------------

Public Const CONSOLIDADO_POR_CLIENTE As String = "CLT"
Public Const CONSOLIDADO_POR_GRUPO As String = "GRP"

'----------------------------------------------------------------------------
'-- Define constante para tipos de cuentas
'----------------------------------------------------------------------------

Public Const cTIPOCUENTA_ADC As String = "ADMCARTERA"
Public Const cTIPOCUENTA_APV As String = "APV"
Public Const cTIPOCUENTA_FFMM As String = "FFMM"

'----------------------------------------------------------------------------
'-- Define constante Niveles de Bloqueo
'----------------------------------------------------------------------------
Public Const cBLOQUEO_NIVEL_Por_Cierre = 100
Public Const cBLOQUEO_NIVEL_Por_Usuario = 200
Public Const cBLOQUEO_NIVEL_Variacion_VC_Perfil = 210
Public Const cBLOQUEO_NIVEL_Saldo_Caja_Positivo = 300
Public Const cBLOQUEO_NIVEL_Saldo_Caja_Negativo = 400
Public Const cBLOQUEO_NIVEL_Saldos_Activos = 500

'----------------------------------------------------------------------------
'-- Define constante Codigo de Bloqueo
'----------------------------------------------------------------------------
Public Const cBLOQUEO_CODIGO_Vigente = "V"
Public Const cBLOQUEO_CODIGO_Procesado = "P"
Public Const cBLOQUEO_CODIGO_Anulado = "A"
