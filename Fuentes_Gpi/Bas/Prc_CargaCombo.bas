Attribute VB_Name = "Prc_CargaCombo"
Option Explicit

Public ErrMsg As String
Public Errnum As Double
Public Cursor As hRecord


'Public Sub Sub_CargaCombo_GENERICO(pCombo As ImageCombo _
'                                 , pTabla As String _
'                                 , pCampoPK As String _
'                                 , pCampoDSC As String _
'                                 , Optional ByVal pTag _
'                                 , Optional pCampoFiltro As String = "" _
'                                 , Optional pCampoFiltro_Valor As Variant = "" _
'                                 , Optional pTipo As DLL_COMUN.eParam_Tipos = ePT_Caracter _
'                                 , Optional pTodos As Boolean = False _
'                                 , Optional pBlanco As Boolean = False)
'Dim lReg As hFields
'
'  gDB.Parametros.Clear
'  gDB.Tabla = pTabla
'  If Not (pCampoFiltro = "") Then
'    gDB.Parametros.Add pCampoFiltro, pTipo, pCampoFiltro_Valor, ePD_Entrada
'  End If
'
'  gDB.OrderBy = pCampoDSC
'
'  With pCombo
'    Set .SelectedItem = Nothing
'    .Text = ""
'    .ComboItems.Clear
'
'    If pTodos Then
'      Call .ComboItems.Add(Key:=cCmbKALL, Text:="Todos")
'    End If
'
'    If pBlanco Then
'      Call .ComboItems.Add(Key:=cCmbKBLANCO, Text:="")
'    End If
'
'    If gDB.EjecutaSelect Then
'      With .ComboItems
'        For Each lReg In gDB.Parametros("Cursor").Valor
'          With .Add(Key:="K" & lReg(pCampoPK).Value, Text:=lReg(pCampoDSC).Value)
'            If Not pTag = "" Then
'              .Tag = lReg(pTag).Value
'            End If
'          End With
'        Next
'      End With
'    End If
'  End With
'  gDB.Parametros.Clear
'End Sub

Public Sub Sub_CargaCombo_GENERICO_Clase(pCombo As ImageCombo _
                                 , pClase As Object _
                                 , pCampoPK As String _
                                 , pCampoDSC As String _
                                 , Optional ByVal pTag _
                                 , Optional pCampoFiltro As String = "" _
                                 , Optional pCampoFiltro_Valor As Variant = "" _
                                 , Optional pSegundoCampoDSC As String = "" _
                                 , Optional pTodos As Boolean = False _
                                 , Optional pBlanco As Boolean = False)
Dim lReg As hFields
Dim lTexto As String
  
  If Not (pCampoFiltro = "") Then
    pClase.Campo(pCampoFiltro).Valor = pCampoFiltro_Valor
  End If
  
  With pCombo
    Set .SelectedItem = Nothing
    .Text = ""
    .ComboItems.Clear
    
    If pTodos Then
      Call .ComboItems.Add(Key:=cCmbKALL, Text:="Todos")
    End If
    
    If pBlanco Then
      Call .ComboItems.Add(Key:=cCmbKBLANCO, Text:="")
    End If
    
    If pClase.Buscar Then
      With .ComboItems
        For Each lReg In pClase.Cursor
          lTexto = ""
          If Not pSegundoCampoDSC = "" Then
            lTexto = lReg(pCampoDSC).Value & " - " & lReg(pSegundoCampoDSC).Value
          Else
            lTexto = lReg(pCampoDSC).Value
          End If
          With .Add(Key:="K" & lReg(pCampoPK).Value, Text:=lTexto)
            If Not pTag = "" Then
              .Tag = lReg(pTag).Value
            End If
          End With
        Next
      End With
    End If
  End With
  
End Sub

Public Sub Sub_CargaCombo_Cuentas_Vigentes(pCombo As TDBCombo, _
                                           Optional ByVal pTag, _
                                           Optional pTodos As Boolean = False, _
                                           Optional pFiltroPorUsuarioCuenta As Boolean = True, _
                                           Optional pId_Asesor As String = "")
'     Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
'                                      New Class_Cuentas, _
'                                      "id_cuenta", _
'                                      "num_cuenta", _
'                                      pTag:=pTag, _
'                                      pSegundoCampoDSC:="Abr_cuenta", _
'                                      pTodos:=pTodos, _
'                                      pCampoFiltro:="id_empresa", _
'                                      pCampoFiltro_Valor:=Fnt_EmpresaActual)

Dim lReg As hCollection.hFields
'Dim lCuentas As Class_Cuentas
Dim lcCuenta As Object
Dim lTag_Colum As Long
Dim lTexto As String
Dim lCampoPK As String

  With pCombo
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True

    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
    End With
    
    If Not IsMissing(pTag) Then
      With .Columns.Add(.Columns.Count)
        .Visible = False 'Oculta la primera columna al usuario
        .Caption = gcCombo_ColumTag
        lTag_Colum = .ColIndex
      End With
    End If
    
    If pTodos Then
      Call .AddItem("Todos")
      With .Columns(0).ValueItems
        .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      End With
    End If
    
    lCampoPK = "id_cuenta"
    
'    Set lCuentas = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    lcCuenta.Campo("id_empresa").Valor = Fnt_EmpresaActual
    If pFiltroPorUsuarioCuenta Then
        lcCuenta.Campo("id_usuario").Valor = gID_Usuario
    End If
Rem Modificado por MMA. 21/01/09 Agrega Filtro por Id_Asesor.
    If pId_Asesor <> "" Then
        lcCuenta.Campo("id_asesor").Valor = pId_Asesor
    End If

    If lcCuenta.Buscar_Vigentes Then
      For Each lReg In lcCuenta.Cursor
        lTexto = lReg("num_cuenta").Value & " - " & lReg("Abr_cuenta").Value
      
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
        
        Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(lCampoPK).Value, lTexto))
        
        Call .AddItem(lTexto)
        
        If Not IsMissing(pTag) Then
          Call .Columns(lTag_Colum).ValueItems.Add(Fnt_AgregaValueItem(lReg(pTag).Value, lReg(lCampoPK).Value))
        End If
      Next
    Else
      Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo.", lcCuenta.ErrMsg, pConLog:=True)
    End If
  End With
End Sub

Public Sub Sub_CargaCombo_Productos(pCombo As TDBCombo, Optional ByVal pTag, Optional pTodos As Boolean = False, Optional pBlanco As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                    New Class_Productos, _
                                    "cod_producto", _
                                    "dsc_producto", _
                                    pTag:=pTag, _
                                    pTodos:=pTodos, _
                                    pBlanco:=pBlanco)
  
End Sub

Public Sub Sub_CargaCombo_Bloqueos_Nivel(pCombo As TDBCombo, Optional ByVal pTag, Optional pTodos As Boolean = False, Optional pBlanco As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      New Class_Bloqueo_Niveles, _
                                      "id_bloqueo_nivel", _
                                      "dsc_nivel", _
                                      pTag:=pTag, _
                                      pTodos:=pTodos, _
                                      pBlanco:=pBlanco)
  
End Sub

Public Sub Sub_CargaCombo_Estados(pCombo As TDBCombo, pTipo_Estado As Double, Optional pTodos As Boolean = False) ', Optional ByVal pTag
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                    New Class_Estados, _
                                    "cod_estado", _
                                    "dsc_estado", _
                                    pTag:="FLG_DEFAULT", _
                                    pCampoFiltro:="id_Tipo_Estado", _
                                    pCampoFiltro_Valor:=pTipo_Estado, _
                                    pTodos:=pTodos)
  
End Sub


Public Sub Sub_CargaCombo_Estados_Checklist(pCombo As TDBCombo, PEmpresa As String, Optional pTodos As Boolean = False)
  
  Dim lcEstados_Checklist As Object

  Set lcEstados_Checklist = Fnt_CreateObject(cDLL_Checklist_Estados)
  Set lcEstados_Checklist.gDB = gDB
  
  lcEstados_Checklist.Campo("Id_empresa").Valor = PEmpresa
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                    lcEstados_Checklist, _
                                    "id_checklist_estado", _
                                    "dsc_checklist_estado", _
                                    pTodos:=pTodos)
  
End Sub


Public Sub Sub_CargaCombo_Checklist(pCombo As TDBCombo, PEmpresa As String, Optional pTodos As Boolean = False)


  Dim lc_Checklist As Object

  Set lc_Checklist = Fnt_CreateObject(cDLL_Checklist_Cliente)
  Set lc_Checklist.gDB = gDB
  
  lc_Checklist.Campo("Id_empresa").Valor = PEmpresa
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                    lc_Checklist, _
                                    "id_checklist", _
                                    "dsc_checklist", _
                                    pTodos:=pTodos)
  
                                     
End Sub


Public Sub Sub_CargaCombo_Representantes(pCombo As TDBCombo, pId_Cliente, Optional ByVal pTag, Optional pBlanco As Boolean = False)
Dim lcRepresentante As Object

  Set lcRepresentante = Fnt_CreateObject(cDLL_Representantes)
  Set lcRepresentante.gDB = gDB
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      lcRepresentante, _
                                      "id_representante", _
                                      "nombre", _
                                      pTag:=pTag, _
                                      pCampoFiltro:="id_cliente", _
                                      pCampoFiltro_Valor:=pId_Cliente, _
                                      pBlanco:=pBlanco)

'Dim lReg As hFields
'Dim lcClientes As Object
'
'  With pCombo
'    Set .SelectedItem = Nothing
'    .Text = ""
'    .ComboItems.Clear
'
'    If pBlanco Then
'      Call .ComboItems.Add(Key:=cCmbKBLANCO, Text:="")
'    End If
'
'    Set lcClientes = Fnt_CreateObject(cDLL_Representantes)
'    Set lcClientes.gDB = gDB
'    lcClientes.Campo("id_cliente").Valor = pId_Cliente
'    If lcClientes.Buscar() Then
'      With .ComboItems
'        For Each lReg In lcClientes.Cursor
'          With .Add(Key:="K" & lReg("Id_Representante").Value, Text:=lReg("nombre").Value)
'            If Not pTag = "" Then
'              .Tag = lReg(pTag).Value
'            End If
'          End With
'        Next
'      End With
'    End If
'  End With
  
  'Call Sub_CargaCombo_GENERICO(pCombo _
                              , "REPRESENTANTES" _
                              , "id_representante" _
                              , "nombre" _
                              , pTag _
                              , "id_cliente" _
                              , pId_Cliente _
                              , ePT_Numero)
End Sub

Public Sub Sub_CargaCombo_Clientes(pCombo As TDBCombo, Optional ByVal pTag, Optional pBlanco As Boolean = False, Optional pTodos As Boolean = False)
Dim lcClientes As Object

  Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
  Set lcClientes.gDB = gDB

  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      lcClientes, _
                                      "id_cliente", _
                                      "rut_cliente", _
                                      pTag:=pTag, _
                                      pSegundoCampoDSC:="nombre_cliente", _
                                      pTodos:=pTodos, _
                                      pBlanco:=pBlanco)

'Dim lReg As hFields
'Dim lcClientes As Object
'
'  With pCombo
'    Set .SelectedItem = Nothing
'    .Text = ""
'    .ComboItems.Clear
'
'    If pBlanco Then
'      Call .ComboItems.Add(Key:=cCmbKBLANCO, Text:="")
'    End If
'
'    Set lcClientes = CreateObject("CSGCLNT0001.Class_Clientes")
'    Set lcClientes.gDB = gDB
'
'    If lcClientes.Buscar(True) Then
'      With .ComboItems
'        For Each lReg In lcClientes.Cursor
'          With .Add(Key:="K" & lReg("id_cliente").Value, Text:=lReg("rut_cliente").Value & " - " & lReg("nombre_cliente").Value)
'            If Not pTag = "" Then
'              .Tag = lReg(pTag).Value
'            End If
'          End With
'        Next
'      End With
'    End If
'  End With
  
End Sub

Public Sub Sub_CargaCombo_Asesor(pCombo As TDBCombo, Optional ByVal pTag, Optional pTodos As Boolean = False, Optional pBlanco As Boolean = False)
                                     
Dim lClass_Entidad As Class_Entidad
Dim lEncontrado As Boolean
Dim pId_Empresa As String

  pId_Empresa = Fnt_EmpresaActual

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
     
    .AddCampo "id_empresa", ePT_Numero, ePD_Entrada, pId_Empresa
    
    lEncontrado = .Buscar("PKG_ASESORES.Buscar")
    Set Cursor = .Cursor
    Errnum = lClass_Entidad.Errnum
    ErrMsg = lClass_Entidad.ErrMsg
  End With
      
  Set lClass_Entidad = Nothing
      
  If lEncontrado Then
    Call Sub_CargaCombo_GENERICO_TCursor(pCombo _
                                   , Cursor _
                                   , "ID_ASESOR" _
                                   , "RUT" _
                                   , pTag:=pTag _
                                   , pSegundoCampoDSC:="NOMBRE" _
                                   , pTodos:=pTodos _
                                   , pBlanco:=pBlanco)
  End If
                                     
End Sub

Public Sub Sub_CargaCombo_Asesor_Cliente(pCombo As TDBCombo, pId_Cliente, Optional ByVal pTag, Optional pTodos As Boolean = False, Optional pBlanco As Boolean = False)

Dim lClass_Entidad As Class_Entidad
Dim lEncontrado As Boolean
Dim pId_Empresa As String

  pId_Empresa = Fnt_EmpresaActual

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
     
    .AddCampo "id_empresa", ePT_Numero, ePD_Entrada, pId_Empresa
    Rem CGarcia 28/11/2009
    Rem Se comenta el parámetro para anular relación Asesor/Cliente
    '.AddCampo "id_cliente", ePT_Numero, ePD_Entrada, pId_Cliente
    
    
    lEncontrado = .Buscar("PKG_REL_ASESOR_EMPRESA.BuscarView")
    Set Cursor = .Cursor
    Errnum = lClass_Entidad.Errnum
    ErrMsg = lClass_Entidad.ErrMsg
  End With
      
  Set lClass_Entidad = Nothing
      
  If lEncontrado Then
    Call Sub_CargaCombo_GENERICO_TCursor(pCombo _
                                   , Cursor _
                                   , "ID_ASESOR" _
                                   , "RUT" _
                                   , pTag:=pTag _
                                   , pSegundoCampoDSC:="NOMBRE" _
                                   , pTodos:=pTodos _
                                   , pBlanco:=pBlanco)
  End If
                                     
End Sub

Public Sub Sub_CargaCombo_Tipos_Administracion(pCombo As TDBCombo, Optional ByVal pTag, Optional pTodos As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Tipo_Administracion _
                                   , "cod_tipo_administracion" _
                                   , "dsc_tipo_administracion" _
                                   , pTag:=pTag _
                                   , pTodos:=pTodos)

End Sub

Public Sub Sub_CargaCombo_Grupos_Cuentas(pCombo As TDBCombo, Optional ByVal pTag, Optional pTodos As Boolean = False, Optional pBlanco As Boolean = False)

  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Grupos_Cuentas _
                                   , "id_grupo_cuenta" _
                                   , "dsc_grupo_cuenta" _
                                   , pTag:=pTag _
                                   , pTodos:=pTodos _
                                   , pBlanco:=pBlanco _
                                   , pCampoFiltro:="id_empresa" _
                                   , pCampoFiltro_Valor:=Fnt_EmpresaActual)

End Sub


Public Sub Sub_CargaCombo_Perfil_Riesgo(pCombo As TDBCombo, Optional ByVal pTag, Optional pTodos As Boolean = False, Optional pBlanco As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Perfiles_Riesgo _
                                   , "id_perfil_riesgo" _
                                   , "dsc_perfil_riesgo" _
                                   , pTag:=pTag _
                                   , pTodos:=pTodos _
                                   , pBlanco:=pBlanco _
                                   , pCampoFiltro:="id_empresa" _
                                   , pCampoFiltro_Valor:=Fnt_EmpresaActual)
                              
End Sub

Public Sub Sub_CargaCombo_Paises(pCombo As TDBCombo, Optional ByVal pTag)

  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Paises _
                                   , "cod_pais" _
                                   , "dsc_pais" _
                                   , pTag:=pTag)
                              
End Sub

Public Sub Sub_CargaCombo_Mercados(pCombo As TDBCombo, Optional ByVal pTag, _
                                    Optional pTodos As Boolean = False, Optional pBlanco As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Mercados _
                                   , "cod_mercado" _
                                   , "desc_mercado" _
                                   , pTag:=pTag _
                                   , pTodos:=pTodos _
                                   , pBlanco:=pBlanco)
  
End Sub

Public Sub Sub_CargaCombo_Instrumentos(pCombo As TDBCombo, Optional pCod_Producto As String = "", Optional ByVal pTag, Optional pTodos As Boolean = False, Optional pBlanco As Boolean = False)
Dim lcampo As String

  If pCod_Producto = "" Then
    lcampo = ""
  Else
    lcampo = "cod_producto"
  End If

  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Instrumentos _
                                   , "cod_Instrumento" _
                                   , "dsc_Intrumento" _
                                   , pTag:=pTag _
                                   , pCampoFiltro:=lcampo _
                                   , pCampoFiltro_Valor:=pCod_Producto _
                                   , pTodos:=pTodos _
                                   , pBlanco:=pBlanco)

End Sub

Public Sub Sub_CargaCombo_Instrumentos_SVS(pCombo As TDBCombo, Optional pCod_Instrumento As String = "", Optional ByVal pTag, Optional pTodos As Boolean = False, Optional pBlanco As Boolean = False)
Dim lcampo As String
  
  If pCod_Instrumento = "" Then
    lcampo = ""
  Else
    lcampo = "cod_instrumento"
  End If
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Instrumentos_Externos _
                                   , "id_instrumento_svs" _
                                   , "cod_instrumento_svs" _
                                   , pTag:=pTag _
                                   , pCampoFiltro:=lcampo _
                                   , pCampoFiltro_Valor:=pCod_Instrumento _
                                   , pSegundoCampoDSC:="dsc_instrumento_svs" _
                                   , pBlanco:=pBlanco _
                                   , pFlac:=True)

End Sub

Public Sub Sub_CargaCombo_Mercados_Transaccion(pCombo As TDBCombo, Optional ByVal pTag)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Mercados_Transaccion _
                                   , "id_Mercado_Transaccion" _
                                   , "dsc_Mercado_Transaccion" _
                                   , pTag:=pTag)
                              
End Sub

Public Sub Sub_CargaCombo_Monedas(pCombo As TDBCombo, Optional ByVal pTag, Optional ByVal pFlg_Es_Moneda_Pago As String)
Dim lcampo As String

  If pFlg_Es_Moneda_Pago = "" Then
    lcampo = ""
  Else
    lcampo = "FLG_ES_MONEDA_PAGO"
  End If
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , Fnt_CreateObject(cDLL_Monedas) _
                                   , "id_moneda" _
                                   , "dsc_moneda" _
                                   , pTag:=pTag _
                                   , pCampoFiltro:=lcampo _
                                   , pCampoFiltro_Valor:=pFlg_Es_Moneda_Pago)
                              
End Sub

Public Sub Sub_CargaCombo_Cajas_Cuenta(pCombo As TDBCombo, ByVal pId_Cuenta As String, Optional ByVal pTag, Optional pTodos As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Cajas_Cuenta _
                                   , "id_caja_cuenta" _
                                   , "dsc_caja_cuenta" _
                                   , pTag:=pTag _
                                   , pCampoFiltro:="id_Cuenta" _
                                   , pCampoFiltro_Valor:=pId_Cuenta _
                                   , pTodos:=pTodos)
                              
End Sub

Public Sub Sub_CargaCombo_Medios_Pago(pCombo As TDBCombo, Optional ByVal pTag)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Medios_Pago _
                                   , "cod_medio_pago" _
                                   , "dsc_medio_pago" _
                                   , pTag:=pTag)
                              
End Sub

Public Sub Sub_CargaCombo_Glosa_Descripcion(pCombo As TDBCombo, lId_tipo_glosa As Double, Optional ByVal pTag, Optional sCEgreso As String, Optional sCIngreso As String)
  
  Call Sub_CargaCombo_GENERICO_Glosa(pCombo _
                                   , New Class_Glosa_Descripcion _
                                   , "id_glosa_descripcion" _
                                   , "dsc_glosa_descripcion" _
                                   , pTag:=pTag _
                                   , pCampoFiltro:="id_glosa_descripcion_tipo" _
                                   , pCampoFiltro_Valor:=lId_tipo_glosa _
                                   , sCEgreso:=sCEgreso _
                                   , sCIngreso:=sCIngreso)
                                   
                              
End Sub

Public Sub Sub_CargaCombo_Contrapartes(pCombo As TDBCombo, Optional ByVal pTag)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Contrapartes _
                                   , "Id_Contraparte" _
                                   , "dsc_Contraparte" _
                                   , pTag:=pTag)
  
End Sub

Public Sub Sub_CargaCombo_Bancos(pCombo As TDBCombo, Optional ByVal pTag, Optional pBlanco As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Bancos _
                                   , "Id_Banco" _
                                   , "dsc_banco" _
                                   , pTag:=pTag _
                                   , pBlanco:=pBlanco)
                              
End Sub

Public Sub Sub_CargaCombo_Clasificacion_Riesgo_Codigos(pCombo As TDBCombo, ByVal pId_Clasificador_Riesgo As String, Optional ByVal pTag)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Clasificacion_Riesgo_Cod _
                                   , "Cod_Valor_Clasificacion" _
                                   , "Cod_Valor_Clasificacion" _
                                   , pTag:=pTag _
                                   , pCampoFiltro:="Id_Clasificador_Riesgo" _
                                   , pCampoFiltro_Valor:=pId_Clasificador_Riesgo)
                              
End Sub

Public Sub Sub_CargaCombo_Clasificacion_Riesgo(pCombo As TDBCombo, Optional ByVal pTag)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Clasificadores_Riesgo _
                                   , "id_clasificador_riesgo" _
                                   , "dsc_clasificador_riesgo" _
                                   , pTag:=pTag)
                              
End Sub

Public Sub Sub_CargaCombo_Emisores_Generales(pCombo As TDBCombo, Optional ByVal pTag)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Emisores_Generales _
                                   , "ID_EMISOR_GENERAL" _
                                   , "DSC_EMISOR_GENERAL" _
                                   , pTag:=pTag)
  
End Sub

Public Sub Sub_CargaCombo_Emisores_Especifico(pCombo As TDBCombo, Optional ByVal pTag, Optional pTodos As Boolean = False, Optional pBlanco As Boolean = False, Optional pIdEmisor As Long = 0, Optional pCampo As String = "")
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Emisores_Especifico _
                                   , "Id_Emisor_Especifico" _
                                   , "dsc_Emisor_Especifico" _
                                   , pTag:=pTag _
                                   , pCampoFiltro_Valor:=pIdEmisor _
                                   , pCampoFiltro:=pCampo _
                                   , pTodos:=pTodos _
                                   , pBlanco:=pBlanco)
End Sub

Public Sub Sub_CargaCombo_Sectores(pCombo As TDBCombo, Optional ByVal pTag)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Sectores _
                                   , "Id_Sector" _
                                   , "dsc_Sector" _
                                   , pTag:=pTag)
                          
End Sub

Public Sub Sub_CargaCombo_Tipos_Contrapartes(pCombo As TDBCombo, Optional ByVal pTag)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Tipos_Contrapartes _
                                   , "id_tipo_contraparte" _
                                   , "dsc_tipo_contraparte" _
                                   , pTag:=pTag)
End Sub

Public Sub Sub_CargaCombo_Publicadores(pCombo As TDBCombo, Optional ByVal pTag)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Publicadores _
                                   , "id_publicador" _
                                   , "dsc_publicador" _
                                   , pTag:=pTag)
End Sub

Public Sub Sub_CargaCombo_Tipos_Liquidacion(pCombo As ImageCombo, pCod_Producto, Optional ByVal pTag)
  Call Sub_CargaCombo_GENERICO_Clase(pCombo _
                                   , New Class_Tipos_Liquidacion _
                                   , "id_tipo_liquidacion" _
                                   , "dsc_tipo_liquidacion" _
                                   , pTag:=pTag _
                                   , pCampoFiltro:="cod_producto" _
                                   , pCampoFiltro_Valor:=pCod_Producto)
End Sub

'Public Sub Sub_CargaCombo_Calsificacion_Riesgo(pCombo As ImageCombo, Optional ByVal pTag)
'
'  Call Sub_CargaCombo_GENERICO(pCombo _
'                              , "clasificadores_riesgo" _
'                              , "ID_CLASIFICADOR_RIESGO" _
'                              , "DSC_CLASIFICADOR_RIESGO" _
'                              , pTag)
'End Sub


Public Sub Sub_CargaCombo_Subfamilias(pCombo As TDBCombo, ByVal pCod_Instrumento As String, Optional ByVal pTag)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_SubFamilias _
                                   , "ID_SUBFAMILIA" _
                                   , "DSC_SUBFAMILIA" _
                                   , pTag:=pTag _
                                   , pCampoFiltro:="COD_INSTRUMENTO" _
                                   , pCampoFiltro_Valor:=pCod_Instrumento)
End Sub

Public Sub Sub_CargaCombo_Nemotecnicos(pCombo As TDBCombo, Optional pCod_Instrumento As String, Optional ByVal pTag, Optional pTodos As Boolean = False, Optional pBlanco As Boolean = False)
Dim lcampo As String

  If pCod_Instrumento = "" Then
    lcampo = ""
  Else
    lcampo = "COD_INSTRUMENTO"
  End If
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                   , New Class_Nemotecnicos _
                                   , "id_nemotecnico" _
                                   , "DSC_nemotecnico" _
                                   , pTag:=pTag _
                                   , pCampoFiltro:=lcampo _
                                   , pCampoFiltro_Valor:=pCod_Instrumento _
                                   , pTodos:=pTodos _
                                   , pBlanco:=pBlanco)
End Sub

Public Sub Sub_CargaCombo_Empresa(pCombo As TDBCombo, Optional ByVal pTag, Optional pBlanco As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                    , New Class_Empresas _
                                    , "id_empresa" _
                                    , "DSC_empresa" _
                                    , pTag:=pTag _
                                    , pBlanco:=pBlanco)

End Sub

Public Sub Sub_CargaCombo_Usuario_Empresas(pCombo As TDBCombo, _
                                           Optional ByVal pTag, _
                                           Optional pTodos As Boolean = False, _
                                           Optional pFiltroPorUsuarioCuenta As Boolean = True)
Dim lcUsuario As Class_Usuarios
Dim lReg As hFields
Dim lTexto As String
  
  With pCombo
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    
    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
    End With
  
    If pTodos Then
      Call .AddItem("Todos")
      With .Columns(0).ValueItems
        .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      End With
    End If
    
    Set lcUsuario = New Class_Usuarios
    lcUsuario.Campo("Dsc_Usuario").Valor = gDsc_Usuario
    If lcUsuario.Buscar_Usuarios_Empresas() Then
      For Each lReg In lcUsuario.Cursor
        lTexto = ""
        
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
        If Not Fnt_FindValue4Display(pCombo, lTexto) = "" Then
            Call Sub_ComboSelectedItem(pCombo, Fnt_FindValue4Display(pCombo, lTexto))
         End If
        lTexto = lReg("DSC_empresa").Value
        .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("id_empresa").Value, lTexto)
        Call .AddItem(lTexto)
      Next
    End If
    Set lcUsuario = Nothing
    
  End With
  
  
End Sub

Public Sub Sub_CargaCombo_Mov_Caja_Origen(pCombo As TDBCombo, Optional ByVal pTag)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                    , New Class_Mov_Caja_Origen _
                                    , "COD_ORIGEN_MOV_CAJA" _
                                    , "DSC_ORIGEN_MOV_CAJA" _
                                    , pTag:=pTag)
End Sub



Public Sub Sub_CargaCombo_Cta_Vigentes(pCombo As TDBCombo, Optional ByVal pId_Cuenta = "", Optional ByVal pTag, Optional pTodos As Boolean = False)
Dim lMov_Caja As Class_Mov_Caja
Dim lReg As hFields
Dim lTexto As String
  
  With pCombo
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    '.SelectedItem = Null
    
    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
'      .Translate = True
    End With
    
'    With .Columns(1)
'      .Visible = False 'Oculta la primera columna al usuario
'      .Caption = "key"
'    End With
  
    If pTodos Then
      Call .AddItem("Todos")
      With .Columns(0).ValueItems
        .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      End With
    End If
    
    Set lMov_Caja = New Class_Mov_Caja
    If lMov_Caja.Buscar_Mov_Caja_Cta_Pendientes(pId_Cuenta) Then
      For Each lReg In lMov_Caja.Cursor
        lTexto = ""
        
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
          
        lTexto = lReg("num_cuenta").Value & " - " & lReg("dsc_cuenta").Value
          
        .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("id_cuenta").Value, lTexto)
          
  '      Call .AddItem(lReg(pCampoPK).Value)
        Call .AddItem(lTexto)
          
  '      Call .AddItem(lTexto & .AddItemSeparator & "K" & lReg(pCampoPK).Value)
  '      If Not pTag = "" Then
  '        .Tag = lReg(pTag).Value
  '      End If
      Next
    End If
    Set lMov_Caja = Nothing
    
'    With .ComboItems
'      For Each lReg In lMov_Caja.Cursor
'        With .Add(Key:="K" & lReg("id_cuenta").Value, Text:=lReg("num_cuenta").Value & " - " & lReg("dsc_cuenta").Value)
'          If Not pTag = "" Then
'            .Tag = lReg(pTag).Value
'          End If
'        End With
'      Next
'    End With
    
  End With
  
  
End Sub

Public Sub Sub_CargaCombo_Sucursales(pCombo As TDBCombo, Optional ByVal pTag, Optional ByVal pBlanco As Boolean = False)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                    , New Class_Sucursales _
                                    , "COD_SUCURSAL" _
                                    , "DSC_SUCURSAL" _
                                    , pTag:=pTag _
                                    , pBlanco:=pBlanco)
End Sub

Public Sub Sub_CargaCombo_EjecutivosSucursal(pCombo As TDBCombo _
                                              , Optional ByVal pCod_Sucursal As String = "" _
                                              , Optional ByVal pTag _
                                              , Optional ByVal pBlanco As Boolean = False)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                    , New Class_Ejecutivos_Sucursal _
                                    , "COD_EJECUTIVO" _
                                    , "DSC_EJECUTIVO" _
                                    , pTag:=pTag _
                                    , pBlanco:=pBlanco _
                                    , pCampoFiltro:="Cod_Sucursal" _
                                    , pCampoFiltro_Valor:=pCod_Sucursal)
End Sub

Public Sub Sub_CargaCombo_Tipo_Referido(pCombo As TDBCombo, Optional ByVal pTag, Optional ByVal pBlanco As Boolean = False)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                    , New Class_Tipo_Referido _
                                    , "ID_TIPO_REFERIDO" _
                                    , "DSC_TIPO_REFERIDO" _
                                    , pTag:=pTag _
                                    , pBlanco:=pBlanco)
End Sub

Public Sub Sub_CargaCombo_Contrato_Nupcial(pCombo As TDBCombo, Optional ByVal pTag _
                                          , Optional ByVal pBlanco As Boolean = False)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                    , New Class_Contrato_Nupcial _
                                    , "ID_CONTRATO_NUPCIAL" _
                                    , "DSC_CONTRATO_NUPCIAL" _
                                    , pTag:=pTag _
                                    , pBlanco:=pBlanco)
End Sub

Public Sub Sub_CargaCombo_Ingr_Mens_Concepto(pCombo As TDBCombo, Optional ByVal pTag)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                    , New Class_Ingresos_Mensual_Concepto _
                                    , "ID_ING_MSN_C_ONCEPTO" _
                                    , "DSC_ING_MSN_C_ONCEPTO" _
                                    , pTag:=pTag)
End Sub

Public Sub Sub_CargaCombo_Tipos_Operaciones(pCombo As TDBCombo, Optional ByVal pTag, Optional pTodos As Boolean = False)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo _
                                    , New Class_Tipos_Operaciones _
                                    , "cod_tipo_operacion" _
                                    , "DSC_tipo_operacion" _
                                    , pTag:=pTag _
                                    , pTodos:=pTodos)
End Sub

Public Sub Sub_CargaCombo_Clientes_Ctas_Ctes_Cta(pCombo As TDBCombo, pId_Cuenta, Optional pTodos As Boolean = False, Optional ByVal pTag)
Dim lcClientes_Ctas_Ctes As Object 'Class_Clientes_Ctas_Ctes
Dim lReg As hFields

  Set lcClientes_Ctas_Ctes = Fnt_CreateObject(cDLL_Clientes_Ctas_Ctes) 'New Class_Clientes_Ctas_Ctes
  Set lcClientes_Ctas_Ctes.gDB = gDB
  If lcClientes_Ctas_Ctes.Buscar_Cuenta(pId_Cuenta) Then
    With pCombo
      .SelectedItem = ""
      .Text = ""
      .ClearFields
      Call .Columns.Remove(1)
      .Columns(0).ValueItems.Clear

      If pTodos Then
        .Columns(0).Add Fnt_AgregaValueItem(cCmbKALL, "Todas")
        Call .AddItem(cCmbKALL)
      End If

      For Each lReg In lcClientes_Ctas_Ctes.Cursor
        With .Columns(0).ValueItems
          .Add Fnt_AgregaValueItem(lReg("ID_CTA_CTE_CLIENTE").Value, lReg("NUMERO_CTA_CTE").Value & " - " & lReg("DSC_BANCO").Value)
          .Translate = True
        End With
        
        Call .AddItem(lReg("NUMERO_CTA_CTE").Value & " - " & lReg("DSC_BANCO").Value)
        'Call .AddItem(lReg("ID_CTA_CTE_CLIENTE").Value)
      Next
    End With
  End If
End Sub



'Public Sub Sub_CargaCombo_GENERICO_TClase(pCombo As TDBCombo _
'                                 , pClase As Object _
'                                 , pCampoPK As String _
'                                 , pCampoDSC As String _
'                                 , Optional ByVal pTag _
'                                 , Optional pCampoFiltro As String = "" _
'                                 , Optional pCampoFiltro_Valor As Variant = "" _
'                                 , Optional pSegundoCampoDSC As String = "" _
'                                 , Optional pTodos As Boolean = False _
'                                 , Optional pBlanco As Boolean = False)
'Dim lReg As hFields
'Dim lTexto As String
'Dim lresult As Boolean
'Dim lNumError As Long
'Dim lTag_Colum As Integer
'
'  If Not (pCampoFiltro = "") Then
'    pClase.Campo(pCampoFiltro).Valor = pCampoFiltro_Valor
'  End If
'
'  With pCombo
'    .Text = ""
'    .ClearFields
'    .Clear
'    .EmptyRows = True
'    '.SelectedItem = Null
'
'    Call .Columns.Remove(1)
'    With .Columns(0).ValueItems
'      .Clear
'      .Translate = False
''      .Translate = True
'    End With
'
'    If Not IsMissing(pTag) Then
'      With .Columns.Add(.Columns.Count)
'        .Visible = False 'Oculta la primera columna al usuario
'        .Caption = gcCombo_ColumTag
'        lTag_Colum = .ColIndex
'      End With
'    End If
'
'    If pTodos Then
'      Call .AddItem("Todos")
'      With .Columns(0).ValueItems
'        .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
'      End With
'    End If
'
'
'    If pBlanco Then
''      Call .AddItem("")
''      With .Columns(0).ValueItems
''        .Add Fnt_AgregaValueItem(cCmbKBLANCO, "")
''        .Translate = True
''      End With
'    End If
'
'    lresult = False
'
'    On Error Resume Next
'    lresult = pClase.Llena_Combo
'    lNumError = Err.Number
'    On Error GoTo 0
'
'    Select Case lNumError
'      Case 438 'No existe Procedimiento en la clase
'        lresult = pClase.Buscar
'      Case Else
'        'No hace nada
'    End Select
'
'    If lresult Then
'      For Each lReg In pClase.Cursor
'        lTexto = ""
'
'        If Not pSegundoCampoDSC = "" Then
'          lTexto = lReg(pCampoDSC).Value & " - " & lReg(pSegundoCampoDSC).Value
'        Else
'          lTexto = lReg(pCampoDSC).Value
'        End If
'
'        If Not gRelogDB Is Nothing Then
'          gRelogDB.AvanzaRelog
'        End If
'
'        Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(pCampoPK).Value, lTexto))
'
''        Call .AddItem(lReg(pCampoPK).Value)
'        Call .AddItem(lTexto)
'
'        If Not IsMissing(pTag) Then
'          Call .Columns(lTag_Colum).ValueItems.Add(Fnt_AgregaValueItem(lReg(pTag).Value, lReg(pCampoPK).Value))
'        End If
'      Next
'    Else
'      Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo.", pClase.ErrMsg, pConLog:=True)
'    End If
'  End With
'
'End Sub

Public Sub Sub_CargaCombo_GENERICO_TClase(pCombo As TDBCombo _
                                 , pClase As Object _
                                 , pCampoPK As String _
                                 , pCampoDSC As String _
                                 , Optional ByVal pTag _
                                 , Optional pCampoFiltro As String = "" _
                                 , Optional pCampoFiltro_Valor As Variant = "" _
                                 , Optional pSegundoCampoDSC As String = "" _
                                 , Optional pTodos As Boolean = False _
                                 , Optional pBlanco As Boolean = False _
                                 , Optional pFlac As Boolean = False)
Dim lReg As hFields
Dim lTexto As String
Dim lResult As Boolean
Dim lNumError As Long
Dim lTag_Colum As Integer
Dim lText_Ant As String
  
  If Not (pCampoFiltro = "") Then
    pClase.Campo(pCampoFiltro).Valor = pCampoFiltro_Valor
  End If
  
  With pCombo
    lText_Ant = .Text
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    .Font.Charset = 0
    '.SelectedItem = Null
    
    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
'      .Translate = True
    End With
    
    If Not IsMissing(pTag) Then
      With .Columns.Add(.Columns.Count)
        .Visible = False 'Oculta la primera columna al usuario
        .Caption = gcCombo_ColumTag
        lTag_Colum = .ColIndex
      End With
    End If
        
    If pTodos Then
      Call .AddItem("Todos")
      With .Columns(0).ValueItems
        .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      End With
    End If
    
    If pFlac Then
        If pBlanco Then
          Call .AddItem("")
          With .Columns(0).ValueItems
            .Add Fnt_AgregaValueItem(cCmbKBLANCO, "")
            .Translate = True
          End With
        End If
    End If
    
    lResult = False
    
    On Error Resume Next
    lResult = pClase.Llena_Combo
    lNumError = Err.Number
    On Error GoTo 0
    
    Select Case lNumError
      Case 438 'No existe Procedimiento en la clase
            lResult = pClase.Buscar
      Case Else
        'No hace nada
    End Select
    
    If lResult Then
      .Columns(0).ValueItems.Translate = False
      
      For Each lReg In pClase.Cursor
        lTexto = ""
        
        If Not pSegundoCampoDSC = "" Then
          lTexto = lReg(pCampoDSC).Value & " - " & lReg(pSegundoCampoDSC).Value
        Else
          lTexto = lReg(pCampoDSC).Value
        End If
                
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
        
            
        Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(pCampoPK).Value, lTexto))
 
        Call .AddItem(lTexto)
        
        If Not IsMissing(pTag) Then
          Call .Columns(lTag_Colum).ValueItems.Add(Fnt_AgregaValueItem(lReg(pTag).Value, lReg(pCampoPK).Value))
        End If
      Next
    Else
      Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo.", pClase.ErrMsg, pConLog:=True)
    End If
    
    If Not Fnt_FindValue4Display(pCombo, lText_Ant) = "" Then
      Call Sub_ComboSelectedItem(pCombo, Fnt_FindValue4Display(pCombo, lText_Ant))
    End If
  End With
End Sub

Public Sub Sub_CargaCombo_GENERICO_Glosa(pCombo As TDBCombo _
                                 , pClase As Object _
                                 , pCampoPK As String _
                                 , pCampoDSC As String _
                                 , Optional ByVal pTag _
                                 , Optional pCampoFiltro As String = "" _
                                 , Optional pCampoFiltro_Valor As Variant = "" _
                                 , Optional pSegundoCampoDSC As String = "" _
                                 , Optional sCEgreso As String = "" _
                                 , Optional sCIngreso As String = "")
                                 
Dim lReg As hFields
Dim lTexto As String
Dim lResult As Boolean
Dim lNumError As Long
Dim lTag_Colum As Integer
  
  If Not (pCampoFiltro = "") Then
    pClase.Campo(pCampoFiltro).Valor = pCampoFiltro_Valor
  End If
  
  With pCombo
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    '.SelectedItem = Null
    
    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
'      .Translate = True
    End With
    
    If Not IsMissing(pTag) Then
      With .Columns.Add(.Columns.Count)
        .Visible = False 'Oculta la primera columna al usuario
        .Caption = gcCombo_ColumTag
        lTag_Colum = .ColIndex
      End With
    End If
        
    
    
    lResult = False
    
    On Error Resume Next
    lResult = pClase.Llena_Combo
    lNumError = Err.Number
    On Error GoTo 0
    
    Select Case lNumError
      Case 438 'No existe Procedimiento en la clase
        lResult = pClase.Buscar
      Case Else
        'No hace nada
    End Select
    
    If lResult Then
      For Each lReg In pClase.Cursor
        lTexto = ""
        
        If Not pSegundoCampoDSC = "" Then
          lTexto = lReg(pCampoDSC).Value & " - " & lReg(pSegundoCampoDSC).Value
       
        Else
          lTexto = lReg(pCampoDSC).Value
        End If
        'lTexto = "HOLA"
        lTexto = Replace(lTexto, "<<CAJA1>>", sCEgreso)
        lTexto = Replace(lTexto, "<<CAJA2>>", sCIngreso)
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
        
        Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(pCampoPK).Value, lTexto))
        
'        Call .AddItem(lReg(pCampoPK).Value)
        Call .AddItem(lTexto)
        
        If Not IsMissing(pTag) Then
          Call .Columns(lTag_Colum).ValueItems.Add(Fnt_AgregaValueItem(lReg(pTag).Value, lReg(pCampoPK).Value))
        End If
      Next
    Else
      Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo.", pClase.ErrMsg, pConLog:=True)
    End If
  End With
  
End Sub

Public Sub Sub_CargaCombo_Moneda_Serie(pCombo As TDBCombo, _
                                        Optional ByVal pTag, _
                                        Optional pTodos As Boolean = False, _
                                        Optional pBlanco As Boolean = False)

Dim lClass_Entidad As Class_Entidad
Dim lEncontrado As Boolean

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    lEncontrado = .Buscar("PKG_MONEDA_SERIE.Buscar")
    Set Cursor = .Cursor
    Errnum = lClass_Entidad.Errnum
    ErrMsg = lClass_Entidad.ErrMsg
  End With
  
  Set lClass_Entidad = Nothing
  
  
  If lEncontrado Then
      Call Sub_CargaCombo_GENERICO_TCursor(pCombo _
                                     , Cursor _
                                     , "IdMoneda" _
                                     , "descripcion" _
                                     , pTag:=pTag _
                                     , pBlanco:=pBlanco)
  End If
  
  
End Sub

Public Sub Sub_CargaCombo_Familia_Serie(pCombo As TDBCombo, _
                                        Optional ByVal pTag, _
                                        Optional pTodos As Boolean = False, _
                                        Optional pBlanco As Boolean = False)

Dim lClass_Entidad As Class_Entidad
Dim lEncontrado As Boolean

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    lEncontrado = .Buscar("PKG_Familia_Serie.Buscar")
    Set Cursor = .Cursor
    Errnum = lClass_Entidad.Errnum
    ErrMsg = lClass_Entidad.ErrMsg
  End With
  
  Set lClass_Entidad = Nothing
  
  
  If lEncontrado Then
      Call Sub_CargaCombo_GENERICO_TCursor(pCombo _
                                     , Cursor _
                                     , "Idfamilia" _
                                     , "familia" _
                                     , pTag:=pTag _
                                     , pBlanco:=pBlanco)
  End If
  
  
End Sub

Public Sub Sub_CargaCombo_Arbol_Clase_Instrumento(pCombo As TDBCombo, _
                                                  Optional ByVal pTag, _
                                                  Optional pTodos As Boolean = False, _
                                                  Optional pBlanco As Boolean = False)
                            
Dim lClass_Entidad As Class_Entidad
Dim lEncontrado As Boolean
Dim pId_Empresa As String

pId_Empresa = Fnt_EmpresaActual

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
     
    .AddCampo "id_empresa", ePT_Numero, ePD_Entrada
    .Campo("id_empresa").Valor = pId_Empresa

    lEncontrado = .Buscar("PKG_ARBOL_CLASE_INSTRUMENTO.Buscar_Familia")
    Set Cursor = .Cursor
    Errnum = lClass_Entidad.Errnum
    ErrMsg = lClass_Entidad.ErrMsg
  End With
      
  Set lClass_Entidad = Nothing
      
  If lEncontrado Then
      Call Sub_CargaCombo_GENERICO_TCursor(pCombo _
                                     , Cursor _
                                     , "ID_ARBOL_CLASE_INST" _
                                     , "DSC_ARBOL_CLASE_INST" _
                                     , pTag:=pTag _
                                     , pTodos:=True _
                                     , pBlanco:=pBlanco)
  End If



End Sub

Public Sub Sub_CargaCombo_Arbol_Clase_SubInstrumento(pCombo As TDBCombo, _
                                                        Optional ByVal pId_Arbol_Clase_Inst As String, _
                                                        Optional ByVal pTag, _
                                                        Optional pTodos As Boolean = False, _
                                                        Optional pBlanco As Boolean = False)
                            

Dim lClass_Entidad As Class_Entidad
Dim lEncontrado As Boolean
Dim pId_Empresa As String

pId_Empresa = Fnt_EmpresaActual



  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
     
    .AddCampo "Id_Empresa", ePT_Numero, ePD_Entrada
    .Campo("id_empresa").Valor = pId_Empresa
    .AddCampo "Id_Arbol_Clase_Inst", ePT_Caracter, ePD_Entrada
    .Campo("Id_Arbol_Clase_Inst").Valor = IIf(pId_Arbol_Clase_Inst = "" Or pId_Arbol_Clase_Inst = "KALL&~", Null, pId_Arbol_Clase_Inst)
       
    lEncontrado = .Buscar("PKG_ARBOL_CLASE_INSTRUMENTO.Buscar_SubFamilia")
    Set Cursor = .Cursor
    Errnum = lClass_Entidad.Errnum
    ErrMsg = lClass_Entidad.ErrMsg
  End With
      
  Set lClass_Entidad = Nothing
      
  If lEncontrado Then
      Call Sub_CargaCombo_GENERICO_TCursor(pCombo _
                                     , Cursor _
                                     , "ID_ARBOL_CLASE_INST" _
                                     , "DSC_ARBOL_CLASE_INST" _
                                     , pTag:=pTag _
                                     , pTodos:=True _
                                     , pBlanco:=pBlanco _
                                     , pCampoFiltro:="ID_PADRE_ARBOL_CLASE_INST", _
                                       pCampoFiltro_Valor:=pId_Arbol_Clase_Inst)
  End If



End Sub

Public Sub Sub_CargaCombo_GENERICO_TCursor(pCombo As TDBCombo _
                                 , pCursor As Object _
                                 , pCampoPK As String _
                                 , pCampoDSC As String _
                                 , Optional ByVal pTag = "" _
                                 , Optional pCampoFiltro As String = "" _
                                 , Optional pCampoFiltro_Valor = "" _
                                 , Optional pSegundoCampoDSC As String = "" _
                                 , Optional pTodos As Boolean = False _
                                 , Optional pBlanco As Boolean = False)
Dim lReg As hFields
Dim lCursor  As hRecord
Dim lTexto As String
Dim lResult As Boolean
Dim lNumError As Long

  With pCombo
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    '.SelectedItem = Null

    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
    End With

    If pTodos Then
      Call .AddItem("Todos")
      With .Columns(0).ValueItems
        .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      End With
    End If
    Set lCursor = pCursor

    For Each lReg In lCursor
      lTexto = ""

      If Not pSegundoCampoDSC = "" Then
        lTexto = lReg(pCampoDSC).Value & " - " & lReg(pSegundoCampoDSC).Value
      Else
        lTexto = lReg(pCampoDSC).Value
      End If

      .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg(pCampoPK).Value, lTexto)

      Call .AddItem(lTexto)
    Next

  End With

End Sub

Public Function Fnt_AgregaValueItem(pValue As String, pDisplay As String) As ValueItem
Dim lValueItem As ValueItem
  
  Set lValueItem = New ValueItem
  With lValueItem
    .Value = pValue
    .DisplayValue = pDisplay
  End With
  
  Set Fnt_AgregaValueItem = lValueItem
End Function

Public Sub Sub_CargaCombo_Traders(pCombo As TDBCombo, _
                                  pId_Contraparte, _
                                  Optional ByVal pTag)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      New Class_Traders, _
                                      "id_trader", _
                                      "dsc_trader", _
                                      pTag:=pTag, _
                                      pCampoFiltro:="id_contraparte", _
                                      pCampoFiltro_Valor:=pId_Contraparte)
End Sub


Public Sub Sub_CargaCombo_Rel_Contrapartes_Instrum(pCombo As TDBCombo, _
                                                    pCod_Instrumento, _
                                                    Optional ByVal pTag)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      New Class_Rel_Contrapartes_Instrum, _
                                      "ID_CONTRAPARTE", _
                                      "DSC_CONTRAPARTE", _
                                      pTag:=pTag, _
                                      pCampoFiltro:="cod_instrumento", _
                                      pCampoFiltro_Valor:=pCod_Instrumento)
End Sub

Public Sub Sub_CargaCombo_Operaciones(pCombo As TDBCombo, _
                                      Optional pId_Cuenta = "", _
                                      Optional ByVal pTag)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      New Class_Operaciones, _
                                      "id_Operacion", _
                                      "id_operacion", _
                                      pTag:=pTag, _
                                      pCampoFiltro:="id_cuenta", _
                                      pCampoFiltro_Valor:=pId_Cuenta)
End Sub

Public Sub Sub_CargaCombo_Comi_Fija_Periodicidad(pCombo As TDBCombo, _
                                                 Optional ByVal pTag)
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      New Class_Comi_Fija_Periodicidad, _
                                      "DIAS", _
                                      "DSC_COMISION_FIJA_PERIODICIDAD", _
                                      pTag:=pTag)
End Sub

Public Sub Sub_CargaCombo_Cuentas(pCombo As TDBCombo, _
                                  Optional ByVal pTag)

  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      Fnt_CreateObject(cDLL_Cuentas), _
                                      "id_Cuenta", _
                                      "num_cuenta", _
                                      pTag:=pTag, _
                                      pCampoFiltro:="id_empresa", _
                                      pCampoFiltro_Valor:=Fnt_EmpresaActual, _
                                      pSegundoCampoDSC:="abr_cuenta")
End Sub

Rem -------------------------------------------------------
Rem Agregado por MMA. Carga Combo Tipos de Cuentas
Rem -------------------------------------------------------
Public Sub Sub_CargaCombo_Tipo_Cuentas(pCombo As TDBCombo, _
                                       Optional ByVal pTag, _
                                       Optional pTodos As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      New Class_TipoCuentas, _
                                      "id_tipoCuenta", _
                                      "descripcion_corta", _
                                      pTag:=pTag)
                              
End Sub


Rem -------------------------------------------------------
Rem Agregado por MMA. Carga Combo Tipos de Ahorro para APV
Rem -------------------------------------------------------
Public Sub Sub_CargaCombo_TiposAhorro_APV(pCombo As TDBCombo, _
                                       Optional ByVal pTag, _
                                       Optional pTodos As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      New Class_TiposAhorroAPV, _
                                      "id_tipo_ahorro", _
                                      "descripcion_larga", _
                                      pTag:=pTag)
                              
End Sub

Public Sub Sub_CargaCombo_TiposAhorro_Cuenta(pCombo As TDBCombo, _
                                       Optional ByVal pTag, _
                                       Optional pTodos As Boolean = False, _
                                       Optional pId_Cuenta As String = "")
  
'
'
'
'Dim lcRelTipoAhorroCuenta As Class_Rel_TipoAhorro_Cuenta
'
'    Set lcRelTipoAhorroCuenta = New Class_Rel_TipoAhorro_Cuenta
'
'    If pId_Cuenta <> "" Then
'      lcRelTipoAhorroCuenta.Campo("Id_cuenta").Valor = pId_Cuenta
'    End If
'
'    Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
'                                      lcRelTipoAhorroCuenta, _
'                                      "id_tipo_ahorro", _
'                                      "descripcion_larga", _
'                                      pTodos:=pTodos)
  
End Sub

Public Sub Sub_CargaCombo_InstitucionPrevisional(pCombo As TDBCombo, Optional ByVal pIdGrupoInstitucion As String = "")
Dim oInstitucionPrevisional As Class_InstitucionPrevisional
Dim lCampoPK As String
Dim lReg            As hCollection.hFields
Dim lTexto As String

    With pCombo
        .Text = ""
        .ClearFields
        .Clear
        .EmptyRows = True
        .Font.Charset = 0

        Call .Columns.Remove(1)

        .Columns(0).ValueItems.Clear
        .Columns(0).ValueItems.Translate = False

        lCampoPK = "id_institucion"

        Set oInstitucionPrevisional = New Class_InstitucionPrevisional

        If pIdGrupoInstitucion <> "" Then
            oInstitucionPrevisional.Campo("id_grupo_institucion").Valor = pIdGrupoInstitucion
        End If

        If oInstitucionPrevisional.Buscar Then
            For Each lReg In oInstitucionPrevisional.Cursor
                lTexto = lReg("dsc_institucion").Value

                If Not gRelogDB Is Nothing Then
                    gRelogDB.AvanzaRelog
                End If

                Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(lCampoPK).Value, lTexto))

                Call .AddItem(lTexto)
            Next
        Else
            Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo de Instituciones Previsionales.", oInstitucionPrevisional.ErrMsg, pConLog:=True)
        End If

    End With

    Set oInstitucionPrevisional = Nothing
End Sub

Public Sub Sub_CargaCombo_GrupoInstitucionPrevisional(pCombo As TDBCombo, _
                                       Optional ByVal pTag, _
                                       Optional pTodos As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      New Class_GrupoInstitucionPrevisional, _
                                      "id_Grupo_institucion", _
                                      "dsc_grupo_institucion", _
                                      pTag:=pTag)
                              
End Sub

Public Sub Sub_CargaCombo_Cuentas_APV(pCombo As TDBCombo, _
                                           Optional ByVal pTag, _
                                           Optional pTodos As Boolean = False, _
                                           Optional pFiltroPorUsuarioCuenta As Boolean = True, _
                                           Optional pId_Cliente As String = "", _
                                           Optional pId_Cuenta As String = "", _
                                           Optional pPnt_Cmb_Cts As Integer)

Dim lReg As hCollection.hFields
'Dim lCuentas As Class_Cuentas
Dim lcCuenta As Object
Dim lTag_Colum As Long
Dim lTexto As String
Dim lCampoPK As String
Dim Cont As Integer

  With pCombo
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True

    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
    End With
    
    If Not IsMissing(pTag) Then
      With .Columns.Add(.Columns.Count)
        .Visible = False 'Oculta la primera columna al usuario
        .Caption = gcCombo_ColumTag
        lTag_Colum = .ColIndex
      End With
    End If
    
    If pTodos Then
      Call .AddItem("Todos")
      With .Columns(0).ValueItems
        .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      End With
    End If
    
    lCampoPK = "id_cuenta"
    
'    Set lCuentas = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
'Agrega filtro por APV
    If pId_Cliente <> "" Then
        lcCuenta.Campo("id_cliente").Valor = pId_Cliente
    End If
    If lcCuenta.Buscar_Cuentas_APV Then
      For Each lReg In lcCuenta.Cursor
        lTexto = lReg("num_cuenta").Value & " - " & lReg("Abr_cuenta").Value
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
        Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(lCampoPK).Value, lTexto))
        Call .AddItem(lTexto)
        Cont = Cont + 1
        
        ''Agregado para rescatar el indice
        If pId_Cuenta = lReg("num_cuenta").Value Then
            pPnt_Cmb_Cts = Cont
        End If
        
        If Not IsMissing(pTag) Then
          Call .Columns(lTag_Colum).ValueItems.Add(Fnt_AgregaValueItem(lReg(pTag).Value, lReg(lCampoPK).Value))
        End If
       
      Next
    Else
      Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo.", lcCuenta.ErrMsg, pConLog:=True)
    End If
  End With
End Sub

Public Sub Sub_CargaCombo_Clientes_APV(pCombo As TDBCombo, Optional ByVal pTag, Optional pBlanco As Boolean = False, Optional pTodos As Boolean = False)
Dim lcClientes As Object
Dim lReg As hCollection.hFields
Dim lTag_Colum As Long
Dim lTexto As String
Dim lCampoPK As String

  With pCombo
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True

    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
    End With
    
    If Not IsMissing(pTag) Then
      With .Columns.Add(.Columns.Count)
        .Visible = False 'Oculta la primera columna al usuario
        .Caption = gcCombo_ColumTag
        lTag_Colum = .ColIndex
      End With
    End If
    
    If pTodos Then
      Call .AddItem("Todos")
      With .Columns(0).ValueItems
        .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      End With
    End If
    
    lCampoPK = "id_cliente"
    
    Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
    If lcClientes.Buscar_Clientes_APV Then
      For Each lReg In lcClientes.Cursor
        lTexto = lReg("rut_cliente").Value & " - " & lReg("nombre_cliente").Value
      
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
        
        Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(lCampoPK).Value, lTexto))
        
        Call .AddItem(lTexto)
        
        If Not IsMissing(pTag) Then
          Call .Columns(lTag_Colum).ValueItems.Add(Fnt_AgregaValueItem(lReg(pTag).Value, lReg(lCampoPK).Value))
        End If
      Next
    Else
      Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo.", lcClientes.ErrMsg, pConLog:=True)
    End If
  End With
End Sub

Public Sub Sub_CargaCombo_Arbol_Clase_Inst_Tipo(pCombo As TDBCombo, _
                                                Optional ByVal pTag, _
                                                Optional pTodos As Boolean = False)
  
    If TipoArbol_Buscar() Then
    
        Call Sub_CargaCombo_GENERICO_TCursor(pCombo _
                                            , Cursor _
                                            , "ID_ACI_TIPO" _
                                            , "DSC_ACI_TIPO" _
                                            , pTag:=pTag)
    End If
                              
End Sub

Public Function TipoArbol_Buscar() As Boolean
Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    TipoArbol_Buscar = .Buscar("PKG_ARBOL_CLASE_INSTRUMENTO_TIPO.BUSCAR")
    Set Cursor = .Cursor
    Errnum = lClass_Entidad.Errnum
    ErrMsg = lClass_Entidad.ErrMsg
  End With
  
  Set lClass_Entidad = Nothing
End Function
