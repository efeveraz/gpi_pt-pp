Attribute VB_Name = "general_funciones"
Option Explicit


Public Const SWP_NOSIZE = 1
Public Const SWP_NOMOVE = 2
Public Const FLAGS = SWP_NOMOVE Or SWP_NOSIZE
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2
Public Const STILL_ACTIVE = &H103
Public Const PROCESS_QUERY_INFORMATION = &H400
Public mLogin As String
Private hndProc     As Long
Private pAppEjec    As String

Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Declare Function WNetGetUser Lib "mpr" Alias "WNetGetUserA" (ByVal lpName As String, ByVal lpUserName As String, lpnLength As Long) As Long
Public Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Public Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Public Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long


Public Function cargarImagenes(TB As Toolbar, imgLst As ImageList, ParamArray Imagenes())
    
    Rem Carga la toolbar TB, con las imagenes
        
    Dim lngI As Long
    Dim lngJ As Long
    
    On Error Resume Next
    imgLst.ListImages.Clear
    For lngI = LBound(Imagenes) To UBound(Imagenes)
        imgLst.ListImages.Add , Imagenes(lngI), LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\" & Imagenes(lngI) & ".bmp")
    Next
    On Error GoTo 0
    Set TB.ImageList = imgLst
    For lngI = 1 To TB.Buttons.Count
        If TB.Buttons.Item(lngI).Style = tbrDefault Then
            TB.Buttons.Item(lngI).Image = Imagenes(lngJ)
            lngJ = lngJ + 1
        End If
    Next
    TB.Refresh

End Function
Public Sub Trae_CMarco_Vigente(vntCMarco As Variant, lngEntidadCli As Long, lngCodigoProd As Long)

    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim intInd As Integer
    
    On Error GoTo LabelError
    Rem Factor40
    Screen.MousePointer = 11
    
    ReDim vntCMarco(1) As Variant
    For intInd = 0 To 1
        vntCMarco(intInd) = ""
    Next
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_contrato_marco.sp_fme_cmarco_vig_ver"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_entidad_cli", adNumeric, adParamInput, 10, lngEntidadCli)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeProducto", adNumeric, adParamInput, 10, lngCodigoProd)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Contrato Marco"
        GoTo LabelSalir
    End If
    
    If Not recRegistros.EOF Then
        vntCMarco(0) = recRegistros.Fields("ide_contrato_marco").Value
        vntCMarco(1) = recRegistros.Fields("dsc_directorio_rms").Value
    End If
    
    
LabelSalir:
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If

End Sub



Public Function Nro_Pagare_Duplicado(lngPagare As Long) As Boolean
Rem Factor40
    Dim Cmd As New ADODB.Command
    
   On Error GoTo Errores
   
      Screen.MousePointer = vbHourglass
   
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_pagares.sp_fme_nro_pagare_duplicado"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_nro_pagare", adNumeric, adParamInput, 10, lngPagare)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_duplicado", adChar, adParamOutput, 1)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
     Cmd.Execute
    
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Pagar�s"
        GoTo Salir
    End If
    
    Nro_Pagare_Duplicado = Trim(Cmd.Parameters("p_c_duplicado").Value) = "S"
    
    
    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Pagar�s"
    'Resume 0
    
Salir:
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal

End Function

Public Function Nro_Pagare_Sgte() As Long
Rem Factor40
    Dim Cmd As New ADODB.Command
    
   On Error GoTo Errores
   
    Screen.MousePointer = vbHourglass
   
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_pagares.sp_fme_nro_pagare_sugerir"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_nro_pagare", adNumeric, adParamOutput, 10)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Pagar�s"
        GoTo Salir
    End If
    
    Nro_Pagare_Sgte = Cmd.Parameters("p_n_nro_pagare").Value
    
    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Pagar�s"
    'Resume 0
    
Salir:
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal

End Function



Public Function pathRegSvr32() As String
Rem Factor40
    Dim strPath As String
    strPath = Environ("windir")
    If LCase(Dir(strPath & "\system\regsvr32.exe")) = "regsvr32.exe" Then
        strPath = strPath & "\system"
    ElseIf LCase(Dir(strPath & "\system32\regsvr32.exe")) = "regsvr32.exe" Then
        strPath = strPath & "\system32"
    Else
        strPath = ""
    End If
    pathRegSvr32 = strPath
End Function

Function LLamarDll(strNombre As String, strDirectorio As String) As Object
Rem Factor40
Rem RM: Generando la union de las Tareas, LLamando a la aplicaci�n ddl
Rem Crea la Referencia
Dim clsRet As Object
Dim strPathRegsvr32 As String
On Error Resume Next
        strNombre = Trim(UCase(strNombre))
        strDirectorio = Trim(UCase(strDirectorio))
        strPathRegsvr32 = pathRegSvr32()
        Set clsRet = CreateObject(strNombre & ".INICIO")
        Rem Si hubo un error al Crear el Objeto, se registra.
        If Err.Number > 0 Then
            Rem Registra la Dll
            If Dir(strPathRegsvr32 & "\regsvr32.exe") <> "" Then
                Shell strPathRegsvr32 & "\regsvr32 " & gstrRuta & "\..\" & strDirectorio & "\" & strNombre & ".dll /s", vbHide
            Else
                MsgBox "No se encontr� Regsvr32.exe", vbCritical, "Error al Ubicar Regsvr32"
            End If
            DoEvents: DoEvents: DoEvents: DoEvents
            Set clsRet = CreateObject(strNombre & ".INICIO")
        End If
        
        Rem No se encontr� (o no est� registrada) la dll
        If clsRet Is Nothing Then
            MsgBox "La clase no ha sido definida", vbCritical, "Atenci�n"
            Exit Function
        End If
        Set clsRet.adoConnection = gAdoCon
        
        Rem Paso de Parametros
        Set LLamarDll = clsRet
        
'        clsRet.show
'        Set clsRet = Nothing
End Function

Public Sub RetocaGrilla(Grilla As Object, intIniFila As Integer, intFinFila As Integer, intIniColumna As Integer, intFinColumna As Integer, blnColorFondo As Boolean, varColorFondo As Variant, blnColorFuente As Boolean, varColorFuente As Variant, bnlNegrilla As Boolean, bnlAplicaBorde As Boolean)
 Rem Factor40
    Rem Selecciona Bloque de celda
    Grilla.Col = intIniColumna
    Grilla.Row = intIniFila
    Grilla.Row2 = intFinFila
    Grilla.Col2 = intFinColumna
    Rem Activa bloque
    Grilla.BlockMode = True
    
    Rem Define color de fondo
    If blnColorFondo Then
        Grilla.BackColor = varColorFondo
    End If
    
    Rem Define color de fuente
    If blnColorFuente Then
        Grilla.ForeColor = varColorFuente
    End If
    Rem Define Negrilla
    Grilla.FontBold = bnlNegrilla
    
    If bnlAplicaBorde Then
        Rem Aplicando Borde
        Grilla.CellBorderType = 16
        Grilla.CellBorderStyle = 1
        Grilla.Action = 16
    End If
    Rem Desactiva bloque
    Grilla.BlockMode = False
    
End Sub

Public Sub Carga_Fiadores(grdGrilla As Object, lngEntidad As Long)
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    
    Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
   
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_entidades.sp_fme_fiadores_ver"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad_cli", adNumeric, adParamInput, 10, lngEntidad)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad_fia", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_rut", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Fiadores"
        GoTo Salir
    End If
    
    grdGrilla.MaxRows = 0
    Do While Not recRegistros.EOF
        grdGrilla.MaxRows = grdGrilla.MaxRows + 1
        grdGrilla.Row = grdGrilla.MaxRows
        
        grdGrilla.Col = 1: grdGrilla.Text = "" & recRegistros.Fields("ide_entidad_cliente").Value
        grdGrilla.Col = 2: grdGrilla.Text = "" & recRegistros.Fields("ide_entidad_fiador").Value
        grdGrilla.Col = 3: grdGrilla.Text = RTrim("" & recRegistros.Fields("rut_entidad").Value) & "-" & recRegistros.Fields("ide_dig_rut").Value
        grdGrilla.Col = 4: grdGrilla.Text = "" & recRegistros.Fields("nomfiador").Value
        grdGrilla.Col = 5: grdGrilla.Text = "" & recRegistros.Fields("mrc_tipo_persona").Value
        grdGrilla.Col = 6: grdGrilla.Text = "" & recRegistros.Fields("tipopersona").Value
        grdGrilla.Col = 7: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_activ_econom").Value
        grdGrilla.Col = 8: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_activ_econom").Value, garrActividadesEco)
        grdGrilla.Col = 9: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_segmento").Value
        grdGrilla.Col = 10: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_segmento").Value, garrSegmentos)
        grdGrilla.Col = 11: grdGrilla.Text = "" & recRegistros.Fields("cod_estado").Value
        grdGrilla.Col = 12: grdGrilla.Text = "" & recRegistros.Fields("fiaestado").Value
        grdGrilla.Col = 13: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_mod_estado").Value
        grdGrilla.Col = 14: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_mod_estado").Value
        grdGrilla.Col = 15: grdGrilla.Text = "" & recRegistros.Fields("mrc_bloqueo").Value
        grdGrilla.Col = 16: grdGrilla.Text = IIf("" & recRegistros.Fields("mrc_bloqueo").Value = 0, "NO", "SI")
        grdGrilla.Col = 17: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_bloqueo").Value
        grdGrilla.Col = 18: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_bloqueo").Value
        grdGrilla.Col = 19: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_creacion").Value
        grdGrilla.Col = 20: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_creacion").Value
        grdGrilla.Col = 21: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_modifica").Value
        grdGrilla.Col = 22: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_modifica").Value
        grdGrilla.Col = 23: grdGrilla.Text = "" & recRegistros.Fields("nom_razon_social").Value
        grdGrilla.Col = 24: grdGrilla.Text = "" & recRegistros.Fields("nom_fantasia").Value
        grdGrilla.Col = 25: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_pais_origen").Value
        grdGrilla.Col = 26: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_pais_origen").Value, garrPaises)
        grdGrilla.Col = 27: grdGrilla.Text = Format("" & recRegistros.Fields("mnt_prom_vta_mensual").Value, "#,##0.0000")
        grdGrilla.Col = 28: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_mon_provtamen").Value
        grdGrilla.Col = 29: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_mon_provtamen").Value, garrMonedas)
        grdGrilla.Col = 30: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_grupo_econom").Value
        grdGrilla.Col = 31: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_grupo_econom").Value, garrGruposEco)
        grdGrilla.Col = 32: grdGrilla.Text = "" & recRegistros.Fields("cod_notaria_const").Value
        grdGrilla.Col = 33: grdGrilla.Text = "" & recRegistros.Fields("notaria_const").Value
        grdGrilla.Col = 34: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_comuna_const").Value
        grdGrilla.Col = 35: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_comuna_const").Value, garrComunas)
        grdGrilla.Col = 36: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_region_const").Value
        grdGrilla.Col = 37: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_region_const").Value, garrRegiones)
        grdGrilla.Col = 38: grdGrilla.Text = "" & recRegistros.Fields("fch_constitucion").Value
        grdGrilla.Col = 39: grdGrilla.Text = "" & recRegistros.Fields("nom_apellido_paterno").Value
        grdGrilla.Col = 40: grdGrilla.Text = "" & recRegistros.Fields("nom_apellido_materno").Value
        grdGrilla.Col = 41: grdGrilla.Text = "" & recRegistros.Fields("nom_nombres").Value
        grdGrilla.Col = 42: grdGrilla.Text = "" & recRegistros.Fields("mrc_sexo").Value
        grdGrilla.Col = 43: grdGrilla.Text = "" & recRegistros.Fields("sexo").Value
        grdGrilla.Col = 44: grdGrilla.Text = "" & recRegistros.Fields("fch_nacimiento").Value
        grdGrilla.Col = 45: grdGrilla.Text = "" & recRegistros.Fields("mrc_estado_civil").Value
        grdGrilla.Col = 46: grdGrilla.Text = "" & recRegistros.Fields("estadocivil").Value
        grdGrilla.Col = 47: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_profesion").Value
        grdGrilla.Col = 48: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_profesion").Value, garrProfesiones)
        grdGrilla.Col = 49: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_nacionalidad").Value
        grdGrilla.Col = 50: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_nacionalidad").Value, garrNacionalidades)
        grdGrilla.Col = 51: grdGrilla.Text = "" & recRegistros.Fields("gls_cargo_empresa").Value
        grdGrilla.Col = 52: grdGrilla.Text = Format("" & recRegistros.Fields("mnt_ingreso_liquido").Value, "#,##0.0000")
        grdGrilla.Col = 53: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_mon_ingliq").Value
        grdGrilla.Col = 54: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_mon_ingliq").Value, garrMonedas)
        recRegistros.MoveNext
    Loop
    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Fiadores"
Resume 0

Salir:
    recRegistros.Close
    Set Cmd = Nothing
    If grdGrilla.MaxRows > 0 Then
        grdGrilla.Row = 1
    End If
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Avales(grdGrilla As Object, lngEntidad As Long)
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    
    Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
   
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_entidades.sp_fme_avales_ver"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad_cli", adNumeric, adParamInput, 10, lngEntidad)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad_ava", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_rut", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Avales"
        GoTo Salir
    End If
    
    grdGrilla.MaxRows = 0
    Do While Not recRegistros.EOF
        grdGrilla.MaxRows = grdGrilla.MaxRows + 1
        grdGrilla.Row = grdGrilla.MaxRows
        
        grdGrilla.Col = 1: grdGrilla.Text = "" & recRegistros.Fields("ide_entidad_cliente").Value
        grdGrilla.Col = 2: grdGrilla.Text = "" & recRegistros.Fields("ide_entidad_aval").Value
        grdGrilla.Col = 3: grdGrilla.Text = RTrim("" & recRegistros.Fields("rut_entidad").Value) & "-" & recRegistros.Fields("ide_dig_rut").Value
        grdGrilla.Col = 4: grdGrilla.Text = "" & recRegistros.Fields("nomaval").Value
        grdGrilla.Col = 5: grdGrilla.Text = "" & recRegistros.Fields("mrc_tipo_persona").Value
        grdGrilla.Col = 6: grdGrilla.Text = "" & recRegistros.Fields("tipopersona").Value
        grdGrilla.Col = 7: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_activ_econom").Value
        grdGrilla.Col = 8: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_activ_econom").Value, garrActividadesEco)
        grdGrilla.Col = 9: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_segmento").Value
        grdGrilla.Col = 10: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_segmento").Value, garrSegmentos)
        grdGrilla.Col = 11: grdGrilla.Text = "" & recRegistros.Fields("cod_estado").Value
        grdGrilla.Col = 12: grdGrilla.Text = "" & recRegistros.Fields("avaestado").Value
        grdGrilla.Col = 13: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_mod_estado").Value
        grdGrilla.Col = 14: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_mod_estado").Value
        grdGrilla.Col = 15: grdGrilla.Text = "" & recRegistros.Fields("mrc_bloqueo").Value
        grdGrilla.Col = 16: grdGrilla.Text = IIf("" & recRegistros.Fields("mrc_bloqueo").Value = 0, "NO", "SI")
        grdGrilla.Col = 17: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_bloqueo").Value
        grdGrilla.Col = 18: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_bloqueo").Value
        grdGrilla.Col = 19: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_creacion").Value
        grdGrilla.Col = 20: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_creacion").Value
        grdGrilla.Col = 21: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_modifica").Value
        grdGrilla.Col = 22: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_modifica").Value
        grdGrilla.Col = 23: grdGrilla.Text = "" & recRegistros.Fields("nom_razon_social").Value
        grdGrilla.Col = 24: grdGrilla.Text = "" & recRegistros.Fields("nom_fantasia").Value
        grdGrilla.Col = 25: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_pais_origen").Value
        grdGrilla.Col = 26: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_pais_origen").Value, garrPaises)
        grdGrilla.Col = 27: grdGrilla.Text = Format("" & recRegistros.Fields("mnt_prom_vta_mensual").Value, "#,##0.0000")
        grdGrilla.Col = 28: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_mon_provtamen").Value
        grdGrilla.Col = 29: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_mon_provtamen").Value, garrMonedas)
        grdGrilla.Col = 30: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_grupo_econom").Value
        grdGrilla.Col = 31: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_grupo_econom").Value, garrGruposEco)
        grdGrilla.Col = 32: grdGrilla.Text = "" & recRegistros.Fields("cod_notaria_const").Value
        grdGrilla.Col = 33: grdGrilla.Text = "" & recRegistros.Fields("notaria_const").Value
        grdGrilla.Col = 34: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_comuna_const").Value
        grdGrilla.Col = 35: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_comuna_const").Value, garrComunas)
        grdGrilla.Col = 36: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_region_const").Value
        grdGrilla.Col = 37: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_region_const").Value, garrRegiones)
        grdGrilla.Col = 38: grdGrilla.Text = "" & recRegistros.Fields("fch_constitucion").Value
        grdGrilla.Col = 39: grdGrilla.Text = "" & recRegistros.Fields("nom_apellido_paterno").Value
        grdGrilla.Col = 40: grdGrilla.Text = "" & recRegistros.Fields("nom_apellido_materno").Value
        grdGrilla.Col = 41: grdGrilla.Text = "" & recRegistros.Fields("nom_nombres").Value
        grdGrilla.Col = 42: grdGrilla.Text = "" & recRegistros.Fields("mrc_sexo").Value
        grdGrilla.Col = 43: grdGrilla.Text = "" & recRegistros.Fields("sexo").Value
        grdGrilla.Col = 44: grdGrilla.Text = "" & recRegistros.Fields("fch_nacimiento").Value
        grdGrilla.Col = 45: grdGrilla.Text = "" & recRegistros.Fields("mrc_estado_civil").Value
        grdGrilla.Col = 46: grdGrilla.Text = "" & recRegistros.Fields("estadocivil").Value
        grdGrilla.Col = 47: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_profesion").Value
        grdGrilla.Col = 48: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_profesion").Value, garrProfesiones)
        grdGrilla.Col = 49: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_nacionalidad").Value
        grdGrilla.Col = 50: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_nacionalidad").Value, garrNacionalidades)
        grdGrilla.Col = 51: grdGrilla.Text = "" & recRegistros.Fields("gls_cargo_empresa").Value
        grdGrilla.Col = 52: grdGrilla.Text = Format("" & recRegistros.Fields("mnt_ingreso_liquido").Value, "#,##0.0000")
        grdGrilla.Col = 53: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_mon_ingliq").Value
        grdGrilla.Col = 54: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_mon_ingliq").Value, garrMonedas)
        recRegistros.MoveNext
    Loop
    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Avales"
'Resume 0

Salir:
    recRegistros.Close
    Set Cmd = Nothing
    If grdGrilla.MaxRows > 0 Then
        grdGrilla.Row = 1
    End If
   Screen.MousePointer = vbNormal
    
End Sub


Public Sub Carga_CtasCtes(grdGrilla As Object, lngEntidad As Long)

    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command

   Screen.MousePointer = vbHourglass

   On Error GoTo Errores

    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_entidades.sp_fme_ctasctes_ver"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad", adNumeric, adParamInput, 10, lngEntidad)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute

    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Ctas. Ctes."
        GoTo Salir
    End If

    grdGrilla.MaxRows = 0
    Do While Not recRegistros.EOF
        grdGrilla.MaxRows = grdGrilla.MaxRows + 1
        grdGrilla.Row = grdGrilla.MaxRows
                
        grdGrilla.Col = 1: grdGrilla.Text = "" & recRegistros.Fields("ide_entidad").Value
        grdGrilla.Col = 2: grdGrilla.Text = "" & recRegistros.Fields("nro_cta_cte").Value
        grdGrilla.Col = 3: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_banco").Value
        grdGrilla.Col = 4: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_banco").Value, garrBancos)
        grdGrilla.Col = 5: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_sucursal").Value
        grdGrilla.Col = 6: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_sucursal").Value, garrSucursales)
        grdGrilla.Col = 7: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_plaza").Value
        grdGrilla.Col = 8: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_plaza").Value, garrPlazas)
        grdGrilla.Col = 9: grdGrilla.Text = "" & recRegistros.Fields("mrc_vigencia_cta").Value
        grdGrilla.Col = 10: grdGrilla.Text = IIf("" & recRegistros.Fields("mrc_vigencia_cta").Value = 0, "NO", "SI")
        grdGrilla.Col = 11: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_creacion").Value
        grdGrilla.Col = 12: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_creacion").Value
        grdGrilla.Col = 13: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_modifica").Value
        grdGrilla.Col = 14: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_modifica").Value
        recRegistros.MoveNext
   
    Loop

    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Ctas. Ctes."

Salir:

    recRegistros.Close
    Set Cmd = Nothing
    If grdGrilla.MaxRows > 0 Then
        grdGrilla.Row = 1
    End If
   Screen.MousePointer = vbNormal
    
End Sub
Public Sub Carga_Direcciones(grdSuc As Object, intCol As Integer, grdGrilla As Object, lngEntidad As Long)
'Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command

   Screen.MousePointer = vbHourglass

   On Error GoTo Errores

    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_entidades.sp_fme_direcciones_ver"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad", adNumeric, adParamInput, 10, lngEntidad)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute

    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Direcciones"
        GoTo Salir
    End If

    grdGrilla.MaxRows = 0
    Do While Not recRegistros.EOF
        grdGrilla.MaxRows = grdGrilla.MaxRows + 1
        grdGrilla.Row = grdGrilla.MaxRows
                
        grdGrilla.Col = 1: grdGrilla.Text = "" & recRegistros.Fields("ide_entidad").Value
        grdGrilla.Col = 2: grdGrilla.Text = "" & recRegistros.Fields("nro_correlativo").Value
        grdGrilla.Col = 3: grdGrilla.Text = "" & recRegistros.Fields("gls_direccion").Value
        grdGrilla.Col = 4: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_comuna").Value
        grdGrilla.Col = 5: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_comuna").Value, garrComunas)
        grdGrilla.Col = 6: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_region").Value
        grdGrilla.Col = 7: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_region").Value, garrRegiones)
        grdGrilla.Col = 8: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_pais").Value
        grdGrilla.Col = 9: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_pais").Value, garrPaises)
        grdGrilla.Col = 10: grdGrilla.Text = "" & recRegistros.Fields("dsc_cod_postal").Value
        grdGrilla.Col = 11: grdGrilla.Text = "" & recRegistros.Fields("dsc_email").Value
        grdGrilla.Col = 12: grdGrilla.Text = "" & recRegistros.Fields("nro_telefono1").Value
        grdGrilla.Col = 13: grdGrilla.Text = "" & recRegistros.Fields("nro_telefono2").Value
        grdGrilla.Col = 14: grdGrilla.Text = "" & recRegistros.Fields("nro_fax").Value
        grdGrilla.Col = 15: grdGrilla.Text = "" & recRegistros.Fields("gls_observacion").Value
        grdGrilla.Col = 16: grdGrilla.Text = "" & recRegistros.Fields("cod_tipo_direccion").Value
        grdGrilla.Col = 17: grdGrilla.Text = "" & recRegistros.Fields("tipodireccion").Value
        grdGrilla.Col = 18: grdGrilla.Text = "" & recRegistros.Fields("nro_sucursal").Value
        grdGrilla.Col = 19: grdGrilla.Text = Busca_Grilla_Glosa("" & recRegistros.Fields("nro_sucursal").Value, grdSuc, intCol)
        grdGrilla.Col = 20: grdGrilla.Text = "" & recRegistros.Fields("mrc_est_vigencia").Value
        grdGrilla.Col = 21: grdGrilla.Text = IIf("" & recRegistros.Fields("mrc_est_vigencia").Value = 0, "NO", "SI")
        grdGrilla.Col = 22: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_creacion").Value
        grdGrilla.Col = 23: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_creacion").Value
        grdGrilla.Col = 24: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_modifica").Value
        grdGrilla.Col = 25: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_modifica").Value
        recRegistros.MoveNext
    Loop

    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Direcciones"
'Resume 0

Salir:
    recRegistros.Close
    Set Cmd = Nothing
    If grdGrilla.MaxRows > 0 Then
        grdGrilla.Row = 1
    End If
   Screen.MousePointer = vbNormal
    
End Sub
Public Sub Carga_RepLegal(grdDir As Object, intCol As Integer, grdGrilla As Object, lngEntidad As Long)
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command

   Screen.MousePointer = vbHourglass

   On Error GoTo Errores

    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_entidades.sp_fme_replegal_ver"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad_rtda", adNumeric, adParamInput, 10, lngEntidad)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad_rep", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_rut", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute

    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Rep. Legal"
        GoTo Salir
    End If

    grdGrilla.MaxRows = 0
    Do While Not recRegistros.EOF
        grdGrilla.MaxRows = grdGrilla.MaxRows + 1
        grdGrilla.Row = grdGrilla.MaxRows
         
        grdGrilla.Col = 1: grdGrilla.Text = "" & recRegistros.Fields("ide_entidad_representada").Value
        grdGrilla.Col = 2: grdGrilla.Text = "" & recRegistros.Fields("ide_entidad_representante").Value
        grdGrilla.Col = 3: grdGrilla.Text = RTrim("" & recRegistros.Fields("rut_entidad").Value) & "-" & recRegistros.Fields("ide_dig_rut").Value
        grdGrilla.Col = 4: grdGrilla.Text = "" & recRegistros.Fields("nomreplegal").Value
        grdGrilla.Col = 5: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_activ_econom").Value
        grdGrilla.Col = 6: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_activ_econom").Value, garrActividadesEco)
        grdGrilla.Col = 7: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_segmento").Value
        grdGrilla.Col = 8: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_segmento").Value, garrSegmentos)
        grdGrilla.Col = 9: grdGrilla.Text = "" & recRegistros.Fields("cod_escrit_notaria").Value
        grdGrilla.Col = 10: grdGrilla.Text = "" & recRegistros.Fields("nomnotaria").Value
        grdGrilla.Col = 11: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_escrit_comuna").Value
        grdGrilla.Col = 12: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_escrit_comuna").Value, garrComunas)
        grdGrilla.Col = 13: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_escrit_region").Value
        grdGrilla.Col = 14: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_escrit_region").Value, garrRegiones)
        grdGrilla.Col = 15: grdGrilla.Text = "" & recRegistros.Fields("fch_escritura").Value
        grdGrilla.Col = 16: grdGrilla.Text = "" & recRegistros.Fields("fch_inicio").Value
        grdGrilla.Col = 17: grdGrilla.Text = "" & recRegistros.Fields("fch_termino").Value
        grdGrilla.Col = 18: grdGrilla.Text = "" & recRegistros.Fields("nro_corr_dir_asoc").Value
        grdGrilla.Col = 19: grdGrilla.Text = Busca_Grilla_Glosa("" & recRegistros.Fields("nro_corr_dir_asoc").Value, grdDir, intCol)
        grdGrilla.Col = 20: grdGrilla.Text = "" & recRegistros.Fields("gls_observaciones").Value
        grdGrilla.Col = 21: grdGrilla.Text = "" & recRegistros.Fields("cod_estado").Value
        grdGrilla.Col = 22: grdGrilla.Text = "" & recRegistros.Fields("repestado").Value
        grdGrilla.Col = 23: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_mod_estado").Value
        grdGrilla.Col = 24: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_mod_estado").Value
        grdGrilla.Col = 25: grdGrilla.Text = "" & recRegistros.Fields("mrc_bloqueo").Value
        grdGrilla.Col = 26: grdGrilla.Text = IIf("" & recRegistros.Fields("mrc_bloqueo").Value = 0, "NO", "SI")
        grdGrilla.Col = 27: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_bloqueo").Value
        grdGrilla.Col = 28: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_bloqueo").Value
        grdGrilla.Col = 29: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_creacion").Value
        grdGrilla.Col = 30: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_creacion").Value
        grdGrilla.Col = 31: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_modifica").Value
        grdGrilla.Col = 32: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_modifica").Value
        grdGrilla.Col = 33: grdGrilla.Text = "" & recRegistros.Fields("nom_apellido_paterno").Value
        grdGrilla.Col = 34: grdGrilla.Text = "" & recRegistros.Fields("nom_apellido_materno").Value
        grdGrilla.Col = 35: grdGrilla.Text = "" & recRegistros.Fields("nom_nombres").Value
        grdGrilla.Col = 36: grdGrilla.Text = "" & recRegistros.Fields("mrc_sexo").Value
        grdGrilla.Col = 37: grdGrilla.Text = "" & recRegistros.Fields("sexo").Value
        grdGrilla.Col = 38: grdGrilla.Text = "" & recRegistros.Fields("fch_nacimiento").Value
        grdGrilla.Col = 39: grdGrilla.Text = "" & recRegistros.Fields("mrc_estado_civil").Value
        grdGrilla.Col = 40: grdGrilla.Text = "" & recRegistros.Fields("estadocivil").Value
        grdGrilla.Col = 41: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_profesion").Value
        grdGrilla.Col = 42: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_profesion").Value, garrProfesiones)
        grdGrilla.Col = 43: grdGrilla.Text = "" & recRegistros.Fields("cod_tc_nacionalidad").Value
        grdGrilla.Col = 44: grdGrilla.Text = Busca_Corp_Glosa("" & recRegistros.Fields("cod_tc_nacionalidad").Value, garrNacionalidades)
        grdGrilla.Col = 45: grdGrilla.Text = "" & recRegistros.Fields("gls_cargo_empresa").Value
        recRegistros.MoveNext
    Loop
    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Rep.Legal"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
    If grdGrilla.MaxRows > 0 Then
        grdGrilla.Row = 1
    End If
    Screen.MousePointer = vbNormal
End Sub

Public Sub Carga_Sucursales(grdGrilla As Object, lngEntidad As Long)
'Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command

   Screen.MousePointer = vbHourglass

   On Error GoTo Errores

    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_entidades.sp_fme_sucursales_ver"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad", adNumeric, adParamInput, 10, lngEntidad)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute

    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Sucursales"
        GoTo Salir
    End If

    grdGrilla.MaxRows = 0
    Do While Not recRegistros.EOF
        grdGrilla.MaxRows = grdGrilla.MaxRows + 1
        grdGrilla.Row = grdGrilla.MaxRows
                
        grdGrilla.Col = 1: grdGrilla.Text = "" & recRegistros.Fields("ide_entidad").Value
        grdGrilla.Col = 2: grdGrilla.Text = "" & recRegistros.Fields("nro_sucursal").Value
        grdGrilla.Col = 3: grdGrilla.Text = "" & recRegistros.Fields("nom_sucursal").Value
        grdGrilla.Col = 4: grdGrilla.Text = "" & recRegistros.Fields("cod_tipo_sucursal").Value
        grdGrilla.Col = 5: grdGrilla.Text = "" & recRegistros.Fields("tiposucursal").Value
        grdGrilla.Col = 6: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_creacion").Value
        grdGrilla.Col = 7: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_creacion").Value
        grdGrilla.Col = 8: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_modifica").Value
        grdGrilla.Col = 9: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_modifica").Value
        recRegistros.MoveNext
   
    Loop

    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Sucursales"

Salir:
    recRegistros.Close
    Set Cmd = Nothing
    If grdGrilla.MaxRows > 0 Then
        grdGrilla.Row = 1
    End If
   Screen.MousePointer = vbNormal
    
End Sub



Public Function fin_tipoTasa(ByVal lngPzoPromPond As Long) As Long

    Rem ==============================================
    Rem Retorna si la tasa debe ser simple o compuesta
    Rem ===============================================
    If lngPzoPromPond >= 30 Then
        Rem Compuesta
        fin_tipoTasa = 1
    Else
        Rem Simple
        fin_tipoTasa = 2
    End If
     
End Function

Public Function fin_periodos(ByVal dtFecIni As Date, ByVal dtFecFin As Date)
    Rem =========================================
    Rem Retorna la cantidad de Periodos Mensuales
    Rem =========================================
    Dim lngPeriodos As Long
    
    lngPeriodos = Abs((DateDiff("d", dtFecFin, dtFecIni) + 1) / 30) + 0.49
    If lngPeriodos > 12 Then
        fin_periodos = 12
    Else
        fin_periodos = lngPeriodos
    End If
     
End Function




Public Sub EjecutaApp(strShellApp As String, lngShellArg1 As Long, strNomApp As String)
    Rem Ejecuta una aplicaci�n dada,
    Rem la cual m�s tarde puede ser controlada
    Rem a trav�s de AppEjecutandose()
    Dim hndId As Long
    If AppEjecutandose() Then
        MsgBox "Ya se est� ejecutando " & Trim(pAppEjec) & ", en otra ventana.", vbInformation, "Factor 3.0"
    Else
        pAppEjec = strNomApp
        hndId = Shell(strShellApp, lngShellArg1)
        hndProc = OpenProcess(PROCESS_QUERY_INFORMATION, False, hndId)
    End If
    
End Sub
Public Function AppEjecutandose() As Boolean
    Rem Indica Si la aplicaci�n ejecutada con
    Rem EjecutaApp continua en ejecutandose.

    Dim retVal As Long
    GetExitCodeProcess hndProc, retVal
    If retVal = STILL_ACTIVE Then
        AppEjecutandose = True
    Else
        AppEjecutandose = False
    End If
End Function

Public Sub Pos_Cbo_Corp(cboCombo As Object, ByVal vntItem As Variant, arrArreglo() As Variant)
    Rem Factor40
    Dim lngInd As Long
    
    cboCombo.ListIndex = 0
    
    If vntItem = 0 Then Exit Sub
    
    For lngInd = 0 To UBound(arrArreglo)
        
        If Trim(arrArreglo(lngInd, 0)) = Trim(vntItem) Then
            cboCombo.ListIndex = lngInd
            Exit For
        End If
        
    Next

End Sub

Public Sub Pos_Cbo(cboCombo As Object, ByVal lngItem As Long)
    Rem Factor40
    Dim lngIndice As Long
    
    If lngItem = 0 Then
        cboCombo.ListIndex = -1
        Exit Sub
    End If
    
    cboCombo.ListIndex = 0
    For lngIndice = 0 To cboCombo.ListCount - 1
        
        If cboCombo.ItemData(lngIndice) = lngItem Then
            cboCombo.ListIndex = lngIndice
            Exit Sub
        End If
        
    Next
    
    cboCombo.ListIndex = -1

End Sub


Public Function Busca_Grilla_Glosa(lngCodigo As Long, grdGrilla As Object, intCol As Integer) As String
    Dim lngFila As Long
    
    On Error GoTo Errores

    Screen.MousePointer = vbHourglass
    Busca_Grilla_Glosa = ""
    
    If lngCodigo = 0 Then GoTo Salir
    
    grdGrilla.Col = intCol
    
    For lngFila = 1 To grdGrilla.MaxRows
        grdGrilla.Row = lngFila
        
        If grdGrilla.Text = lngCodigo Then
            grdGrilla.Col = intCol + 1
            Busca_Grilla_Glosa = grdGrilla.Text
            GoTo Salir
        End If
        
    Next
    

Salir:
   Screen.MousePointer = vbNormal
    Exit Function
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Buscando Glosa en Grilla"
End Function

Public Sub Carga_Combo_de_Grilla(cboCombo As Object, grdGrilla As Object, intCol As Integer)
    Dim lngFila As Long
    
    On Error GoTo Errores

    cboCombo.Clear
    
    cboCombo.AddItem "NO"
    cboCombo.ItemData(cboCombo.NewIndex) = 0
    
    For lngFila = 1 To grdGrilla.MaxRows
        
        grdGrilla.Row = lngFila
        grdGrilla.Col = intCol: cboCombo.AddItem grdGrilla.Text
        grdGrilla.Col = intCol - 1: cboCombo.ItemData(cboCombo.NewIndex) = grdGrilla.Text
    
    Next
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Error"
    
Salir:
   Screen.MousePointer = vbNormal


End Sub
Public Function TipoPersona(Rut As String) As Integer
    '// 1 --> Persona Natural
    '// 2 --> Persona Juridica

    If Val(Rut) >= 1 And Val(Rut) <= 49999999 Then
       TipoPersona = 1: Exit Function
    End If

    If Val(Rut) >= 50000000 And Val(Rut) <= 799999999 Then
       TipoPersona = 2: Exit Function
    End If

    If Val(Rut) >= 800000000 And Val(Rut) <= 849999999 Then
        TipoPersona = 1: Exit Function
    End If

    If Val(Rut) >= 850000000 And Val(Rut) <= 999999999 Then
        TipoPersona = 2: Exit Function
    End If
End Function

Public Function SpreadToMdb(sprd, Reporte, Optional lngFilIni As Long, Optional lngNumCol As Long) As Boolean
    Dim con         As New ADODB.Connection
    Dim strSql      As String
    Dim lngRow      As Long
    Dim lngI        As Long
    Dim lngJ        As Long
    Dim strNomCol   As String
    Dim rs          As New ADODB.Recordset
    Dim lngCantCol  As Long
    
    If lngFilIni = 0 Then
        lngFilIni = 1
    End If
    
    If lngNumCol = 0 Then
        lngCantCol = sprd.MaxCols
    Else
        lngCantCol = lngNumCol
    End If
    
    SpreadToMdb = True
    Screen.MousePointer = vbHourglass
    On Error Resume Next
    If gstrRuta <> "" Then
        FileCopy gstrRuta & "\Spread.mdb", "C:\Windows\Temp\Spread.mdb"
    Else
        FileCopy App.Path & "\Spread.mdb", "C:\Windows\Temp\Spread.mdb"
    End If
    On Error GoTo Err
    con.Provider = "Microsoft.Jet.OLEDB.4.0"
    con.Open "Data Source=C:\windows\temp\spread.mdb;Persist Security Info=False"
    
    Rem Borra Tabla Temporal
    On Error Resume Next
    strSql = " DROP TABLE " & Reporte
    con.Execute strSql
    
    Rem Crea la Tabla Temporal
    On Error GoTo Err
    strSql = ""
    strSql = strSql & " CREATE TABLE " & Reporte & " ("
    
    For lngI = 1 To lngCantCol
        sprd.Row = 0
        sprd.Col = lngI
        'If Not sprd.ColHidden Then
            strNomCol = sprd.Text
            strNomCol = Replace(strNomCol, ".", "", 1)
            sprd.Row = sprd.MaxRows
            Select Case sprd.CellType
                Case 0
                    Rem Date
                    strSql = strSql & " [" & strNomCol & "] Date,"
                Case 1
                    Rem Edit, Label
                    strSql = strSql & " [" & strNomCol & "] Text (" & sprd.TypeEditLen & "),"
                Case 2
                    Rem Float
                    strSql = strSql & " [" & strNomCol & "] Double ,"
                Case 3
                    Rem Integer
                    strSql = strSql & " [" & strNomCol & "] Long,"
                Case 5
                    strSql = strSql & " [" & strNomCol & "] LongText,"
                Case Else
                    strSql = strSql & " [" & strNomCol & "] Text (" & sprd.TypeEditLen & "),"
            End Select
        'End If
    Next lngI
    strSql = Left(strSql, Len(strSql) - 1) & ")"
    con.Execute strSql
    
    Rem Puebla la Tabla Temporal
    rs.Open Reporte, con, adOpenDynamic, adLockOptimistic
    For lngI = lngFilIni To sprd.MaxRows
        sprd.Row = lngI
        rs.AddNew
        For lngJ = 1 To lngCantCol
            sprd.Col = lngJ
            'If Not sprd.ColHidden Then
                If sprd.Value = "" And (sprd.CellType = 2 Or sprd.CellType = 3) Then
                    rs(lngJ - 1) = 0
                ElseIf sprd.Value = "" And sprd.CellType = 0 Then
                    rs(lngJ - 1) = Null
                ElseIf sprd.Value = "" Then
                    rs(lngJ - 1) = Null
                Else
                    rs(lngJ - 1) = sprd.Text
                End If
            'End If
        Next
        rs.Update
    Next lngI
    rs.Close
    con.Close
    Set rs = Nothing
    Set con = Nothing
    
    Screen.MousePointer = vbDefault
    Exit Function
    
Err:
    MsgBox "Error Nro. " & Err.Number & ". " & Err.Description
    SpreadToMdb = False
    'Resume 0
End Function

''// PROCEDIMIENTO QUE CENTRA UN FORMUALRIO
Sub Centrar(frm As Form)
    frm.Top = (Screen.Height - frm.Height) / 2
    frm.Left = (Screen.Width - frm.Width) / 2
End Sub



Public Function DefDecimales(ByVal Valor As Double, _
                      ByVal Decimales As Double, _
                      ByVal Truncado As Byte) As Double
  'Valor corresponde al valor que va a ser redondeado o truncado
  'Decimales# es la cantidad de decimales que se desa dejar
  'Truncado, valor booleano que indica:
  '1 = true (truncado) ; 2 = false (redondeado)
Dim X As Double
  X = 0.5
  If Valor < 0 Then
     X = -0.5
  End If
  If Truncado Then
     DefDecimales = Fix(Valor * (10 ^ Decimales#)) / (10 ^ Decimales#)
  Else
     DefDecimales = Fix((Valor * (10 ^ Decimales#)) + X) / (10 ^ Decimales#)
  End If

End Function

Public Function InteresPeriodo(ByVal Tasa As Single, _
                        ByVal TipoInt As Integer, _
                        ByVal PerInt As String, _
                        ByVal Fecant As Date, _
                        ByVal FecTope As Date) As Double
    If TipoInt = 2 Then
       If PerInt = "M" Then
          InteresPeriodo = (((Tasa / 30) * DateDiff("d", Fecant, FecTope)) / 100) + 1
       ElseIf PerInt = "A" Then
          InteresPeriodo = (((Tasa / 360) * DateDiff("d", Fecant, FecTope)) / 100) + 1
       ElseIf PerInt = "D" Then
          InteresPeriodo = ((Tasa * DateDiff("d", Fecant, FecTope)) / 100) + 1
       End If
    Else
       If PerInt = "M" Then
          InteresPeriodo = ((((Tasa / 100) + 1) ^ (1 / 30)) ^ DateDiff("d", Fecant, FecTope))
       ElseIf PerInt = "A" Then
          InteresPeriodo = ((((Tasa / 100) + 1) ^ (1 / 360)) ^ DateDiff("d", Fecant, FecTope))
       ElseIf PerInt = "D" Then
          InteresPeriodo = (((Tasa / 100) + 1) ^ DateDiff("d", Fecant, FecTope))
       End If
    End If

End Function



Public Function EliminaBlancos(stTextoOriginal As String) As String
Dim stTexto As String
Dim i, Palabra

    stTexto = stTextoOriginal
    i = InStr(Trim(stTexto), " ")
    If i = 0 Then
        If Trim(stTexto) = "" Then
            EliminaBlancos = ""
        Else
            EliminaBlancos = UCase(Mid(stTexto, 1, 1)) + LCase$(Mid(stTexto, 2, Len(stTexto) - 1))
        End If
        Exit Function
    End If
    Palabra = ""
    While i <> 0
        Palabra = Palabra + UCase(Mid(stTexto, 1, 1)) + LCase(Mid(stTexto, 2, i - 1))
        stTexto = Trim(Mid(stTexto, i, Len(stTexto)))
        i = InStr(stTexto, " ")
    Wend
    Palabra = Palabra + UCase(Mid(stTexto, 1, 1)) + LCase(Mid(stTexto, 2, Len(stTexto) - 1))
    EliminaBlancos = Palabra
End Function


Public Sub MsgErr(Optional lngIcono As Long, Optional titulo As String)
    
    ' <lngErr> use las mismas constantes de MsgBox (respecto al icono)
    
    Dim strErrTec   As String  ' <-- Descripci�n T�cnica del Error.-
    Dim strErrUsr   As String  ' <-- Descripci�n para los Usuarios.-
    Dim strMsg      As String
    Dim intI        As Integer
    Dim strTitulo   As String
    Dim strTmp      As String
    
    
    ' Establece el t�tulo de la ventana
    ' propietaria.-
    strTitulo = App.EXEName
    If Not Screen.ActiveForm Is Nothing Then
        strTitulo = Screen.ActiveForm.Caption
    End If
    
    If titulo <> "" Then
        strTitulo = titulo
    End If
    
    
    ' Establece el �cono "Exclamaci�n" por default.-
    If lngIcono = 0 Then lngIcono = vbExclamation
    
    If Err > 0 Then
        If Err.Number > 2000 And Err.Number < 3000 Then
            Rem Errores Internos de Factoring
            strErrTec = Err.Description
            strErrUsr = Err.Description
            strTitulo = Err.Source
        Else
            Rem Se trata de un error VB.-
            strErrTec = Err.Description & " ( " & Err.Number & " )"
            strErrUsr = strErrTec
        End If
    Else
        Rem Se trata de una error de Datos.-
        If Not gAdoCon Is Nothing Then
            strErrTec = "Usuario :" & LoginNet() & Chr(13) & Chr(10)
            strErrTec = strErrTec & "Fecha   :" & Format(Date, "dd/mm/yyyy") & Chr(13) & Chr(10)
            strErrTec = strErrTec & "Hora    :" & Format(Time, "HH:MM:SS") & Chr(13) & Chr(10)
            strErrTec = strErrTec & "App     :" & App.EXEName & Chr(13) & Chr(10)
            If Not Screen.ActiveForm Is Nothing Then
                strErrTec = strErrTec & "Form    :" & Screen.ActiveForm.Name & Chr(13) & Chr(10)
            End If
            If Not Screen.ActiveControl Is Nothing Then
                strErrTec = strErrTec & "Control :" & Screen.ActiveControl.Name & Chr(13) & Chr(10)
            End If
            For intI = 0 To gAdoCon.Errors.Count - 1
                strErrTec = strErrTec & Chr(13) & Chr(10)
                strErrTec = strErrTec & "Error   :" & intI + 1 & " de " & gAdoCon.Errors.Count & Chr(13) & Chr(10)
                strErrTec = strErrTec & "Descrip :" & gAdoCon.Errors(intI).Description & Chr(13) & Chr(10)
                strErrTec = strErrTec & "N�mero  :" & gAdoCon.Errors(intI).NativeError & Chr(13) & Chr(10)
                strErrTec = strErrTec & "---------------------"
                Select Case gAdoCon.Errors(intI).NativeError
                    Case 1
                        strErrUsr = strErrUsr + "Registro Duplicado" + Chr(13)
                    Case 1400
                        strErrUsr = strErrUsr + "Faltan datos por completar" + Chr(13)
                    Case 942
                        strErrUsr = strErrUsr + "Tabla o Vista  no existe, o no posee los permisos necesarios" + Chr(13)
                    Case 933
                        strErrUsr = strErrUsr + "Error en sentencia SQL" + Chr(13)
                    Case 904
                        strErrUsr = strErrUsr + "Error en sentencia SQL. Nombre de Columna Incorrecto" + Chr(13)
                    Case 1722
                        strErrUsr = strErrUsr + "Error en sentencia SQL. se comparan campos de tipos diferentes" + Chr(13)
                    Case 917
                        strErrUsr = strErrUsr + "Error en sentencia SQL. Falta la coma" + Chr(13)
                    Case 3251
                         strErrUsr = strErrUsr + "La operaci�n requerida, no se ecuentra disponible por el proveedor + Chr(13)"
                    Case Else
                        strErrUsr = strErrTec
                End Select
            Next
        End If
    End If


    If Err.HelpContext = 10000 Then
        Rem Error a trav�s de RAISE (factoring)
        MsgBox Err.Description, lngIcono, strTitulo
    Else
        Rem Otros Errores
        If Trim(strErrTec) <> "" Then
            Open "C:\" & App.EXEName & ".log" For Output As #10
            Print #10, strErrTec
            Close #10
            If bufferMsg() <> "" Then
                strErrUsr = "Problemas al Intentar: " & bufferMsg() & Chr(13) & Chr(13) & strErrUsr & Chr(13)
            End If
            strErrUsr = strErrUsr + Chr(13) + "Se gener� archivo  C:\" & App.EXEName & ".LOG"
            MsgBox strErrUsr, lngIcono, strTitulo
        End If
    End If

End Sub

Public Function bufferMsg(Optional strMsg As String = "/ausente/") As String
    Static msg As String
    If strMsg = "/ausente/" Then
        bufferMsg = msg
    ElseIf strMsg = "" Then
        msg = ""
    Else
        msg = strMsg
    End If
End Function


Public Function LoginNet() As String
    Dim strUserName As String * 255
    Dim lngpos As Long
    Dim strAux As String
    Dim varAux As Variant
       
    If gAdoCon.State <> 1 Then
        lngpos = InStr(1, Command$, ";")
        LoginNet = Mid(Command$, 1, lngpos - 1)
    Else
        lngpos = InStr(1, gAdoCon, "UID")
        If lngpos = 0 Then
            varAux = Split(UCase(gAdoCon), "USER ID=")
            varAux = Split(varAux(1), ";")
            LoginNet = varAux(0)
        Else
            LoginNet = Split(Split(Mid(gAdoCon, InStr(1, gAdoCon, "UID")), "=")(1), ";")(0)
        End If
    End If
    Exit Function
    
    
    If mLogin <> "" Then
        LoginNet = mLogin
    Else
        If WNetGetUser("", strUserName, 255) = 0 Then
            LoginNet = UCase(Left(strUserName, InStr(strUserName, Chr(0)) - 1))
        Else
            MsgBox "No es Posible Establecer Usuario (API WNetGetUser)", vbExclamation, "Conexi�n"
        End If
    End If
End Function

Public Sub SetLoginNet(ByVal strLogin As String)
    mLogin = strLogin
End Sub


Function Busca_Cod_Glosa(cboCombo As Object, glosa As String) As Long
    Dim strSql As String
    Dim i As Integer
    
    Busca_Cod_Glosa = 0
    
    
    For i = 0 To cboCombo.ListCount - 1
        cboCombo.ListIndex = i
        If UCase(Trim(cboCombo.Text)) = UCase(Trim(glosa)) Then
         Busca_Cod_Glosa = cboCombo.ItemData(cboCombo.ListIndex)
         Exit For
        End If
    Next
    
End Function


Public Sub Puebla_Ejecutivos(cboCombo As Object, Optional ByVal lngEjecutivo As Long, Optional strTipo As String)
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_entidades.sp_fme_ejecutivos_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_rut", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_tipo", adVarChar, adParamInput, 3, strTipo)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Param. Bancos"
        GoTo Salir
    End If
    
    cboCombo.Clear
    Do While Not recRegistros.EOF
        
        cboCombo.AddItem recRegistros.Fields("nomejecutivo").Value
        cboCombo.ItemData(cboCombo.NewIndex) = recRegistros.Fields("ide_entidad_ejecutivo").Value
        
        If Not IsMissing(lngEjecutivo) Then
            If recRegistros.Fields("ide_entidad_ejecutivo").Value = lngEjecutivo Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        
        recRegistros.MoveNext
    Loop
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Ejecutivos"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_Bancos()
Rem Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_replicas_corp.sp_fme_banco_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codigo", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Bancos Corporativos"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrBancos(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrBancos(lngInd, 0) = recRegistros.Fields("cod_tc_banco").Value
        garrBancos(lngInd, 1) = recRegistros.Fields("gls_descripcion").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Bancos Corporativos"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub
Public Sub Carga_Arr_Mdas_Local()
Rem Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_monedas.sp_fme_monedas_ver"
                
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codigo", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Monedas"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrMonedas(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrMonedas(lngInd, 0) = recRegistros.Fields("cod_tc_moneda").Value
        garrMonedas(lngInd, 1) = recRegistros.Fields("gls_descripcion").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Monedas"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub




Public Sub Carga_Corp_Nacionalidades()
Rem Factor40
    
Exit Sub

    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_parametros.sp_fme_prmtro_banco"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_codigogral", adVarChar, adParamInput, 10, "9")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_codigopar", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Param. Bancos"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrNacionalidades(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrNacionalidades(lngInd, 0) = recRegistros.Fields("codigo_par").Value
        garrNacionalidades(lngInd, 1) = recRegistros.Fields("glosa_par").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Param Bancos"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_OficinasSII()
Rem Factor40

Exit Sub

    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_parametros.sp_fme_prmtro_banco"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_codigogral", adVarChar, adParamInput, 10, "13")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_codigopar", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Param. Bancos"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrOficinasSII(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrOficinasSII(lngInd, 0) = recRegistros.Fields("codigo_par").Value
        garrOficinasSII(lngInd, 1) = recRegistros.Fields("glosa_par").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Param Bancos"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_Paises()
Rem Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_replicas_corp.sp_fme_pais_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codigo", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Paises Corporativos"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrPaises(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrPaises(lngInd, 0) = recRegistros.Fields("cod_tc_pais").Value
        garrPaises(lngInd, 1) = recRegistros.Fields("gls_descripcion").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Paises Corporativos"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_Plazas()
Rem Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_replicas_corp.sp_fme_plaza_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codigo", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Plazas Corporativas"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrPlazas(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrPlazas(lngInd, 0) = recRegistros.Fields("cod_tc_plaza").Value
        garrPlazas(lngInd, 1) = recRegistros.Fields("gls_descripcion").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Plazas Corporativas"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_Profesiones()
Rem Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_replicas_corp.sp_fme_profesion_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codigo", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Profesiones Corporativas"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrProfesiones(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrProfesiones(lngInd, 0) = recRegistros.Fields("cod_tc_profesion").Value
        garrProfesiones(lngInd, 1) = recRegistros.Fields("gls_descripcion").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Profesiones Corporativas"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_Regiones()
Rem Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_replicas_corp.sp_fme_region_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codigo", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Regiones Corporativas"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrRegiones(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrRegiones(lngInd, 0) = recRegistros.Fields("cod_tc_region").Value
        garrRegiones(lngInd, 1) = recRegistros.Fields("gls_descripcion").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Regiones Corporativas"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_Segmentos()
Rem Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_replicas_corp.sp_fme_segmento_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codigo", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Segmentos Corporativos"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrSegmentos(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrSegmentos(lngInd, 0) = recRegistros.Fields("cod_tc_segmento").Value
        garrSegmentos(lngInd, 1) = recRegistros.Fields("gls_descripcion").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Segmentos Corporativos"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_Sucursales()
Rem Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_replicas_corp.sp_fme_sucursal_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codigo", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Sucursales Corporativas"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrSucursales(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrSucursales(lngInd, 0) = recRegistros.Fields("cod_tc_sucursal").Value
        garrSucursales(lngInd, 1) = recRegistros.Fields("gls_descripcion").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Sucursales Corporativas"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_Comunas()
Rem Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_replicas_corp.sp_fme_comuna_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codigo", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Comunas Corporativas"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrComunas(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrComunas(lngInd, 0) = recRegistros.Fields("cod_tc_comuna").Value
        garrComunas(lngInd, 1) = recRegistros.Fields("gls_descripcion").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Comunas Corporativas"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_EjecutivosBco()
Rem Factor40

Exit Sub

    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_parametros.sp_fme_prmtro_banco"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_codigogral", adVarChar, adParamInput, 10, "6")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_codigopar", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Param. Bancos"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrEjecutivosBco(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrEjecutivosBco(lngInd, 0) = recRegistros.Fields("codigo_par").Value
        garrEjecutivosBco(lngInd, 1) = recRegistros.Fields("glosa_par").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Param Bancos"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_ActividadesEco()
Rem Factor40
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_replicas_corp.sp_fme_actividadeseco_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codigo", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Actividad  Econ�mica"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrActividadesEco(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrActividadesEco(lngInd, 0) = recRegistros.Fields("cod_tc_grupo_econom").Value
        garrActividadesEco(lngInd, 1) = recRegistros.Fields("gls_descripcion").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Actividad  Econ�mica"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Corp_GruposEco()
Rem Factor40
    
Exit Sub

    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros  As Long
    Dim lngInd  As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_parametros.sp_fme_prmtro_banco"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_codigogral", adVarChar, adParamInput, 10, "7")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_codigopar", adVarChar, adParamInput, 10, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Param. Bancos"
        GoTo Salir
    End If
    
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrGruposEco(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrGruposEco(lngInd, 0) = recRegistros.Fields("codigo_par").Value
        garrGruposEco(lngInd, 1) = recRegistros.Fields("glosa_par").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Param Bancos"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Public Sub Carga_Combo_Par(cboCombo As Object, ByVal lngTipo As Long, ByVal lngSTipo As Long, Optional strEmpresa As String, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
    Rem Factor40
    
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngEmpresa As Long
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
   
   If strEmpresa = Empty Then
        lngEmpresa = gLngEmpresa
    Else
        lngEmpresa = Val(strEmpresa)
   End If
   
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.pkg_fme_parametros.sp_fme_prmtros_ver"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, lngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigotipo", adNumeric, adParamInput, 10, lngTipo)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigosubtipo", adNumeric, adParamInput, 10, lngSTipo)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigoparametro", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Par�metros"
        GoTo Salir
    End If
        
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem "Todos"
        End If
    End If
    
    Do While Not recRegistros.EOF
        
        cboCombo.AddItem recRegistros.Fields("gls_descripcion").Value
        cboCombo.ItemData(cboCombo.NewIndex) = recRegistros.Fields("ide_unico_param").Value
        
        If Not IsMissing(lngSeleccion) Then
            If recRegistros.Fields("ide_unico_param").Value = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        
        recRegistros.MoveNext
    Loop
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Par�metros"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
End Sub
Public Sub Carga_Combo_Corp(cboCombo As Object, arrArreglo() As Variant, Optional ByVal strSeleccion As String, Optional blnTodo As Boolean)
Rem Factor40
   Dim lngInd As Long
   Dim lngX As Long
   On Error GoTo Errores
    
   Screen.MousePointer = vbHourglass
   
    cboCombo.Clear
    lngX = 0
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem "Todos", 0
            lngX = 1
        End If
    End If
    
    For lngInd = lngX To UBound(arrArreglo)
        cboCombo.AddItem arrArreglo(lngInd, 1)
        
        If Not IsMissing(strSeleccion) Then
            If arrArreglo(lngInd, 0) = strSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        
    Next

    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Param Bancos"
    
Salir:
   Screen.MousePointer = vbNormal
'Resume 0
End Sub

Public Function Busca_Corp_Glosa(strCodigo As String, arrArreglo() As Variant) As String
Rem Factor40
   Dim lngInd As Long
   
   On Error GoTo Errores
    
   Screen.MousePointer = vbHourglass
   
    If strCodigo = "" Then GoTo Salir
    
    For lngInd = 0 To UBound(arrArreglo)
        
        If arrArreglo(lngInd, 0) = strCodigo Then
            Busca_Corp_Glosa = arrArreglo(lngInd, 1)
            Exit For
        End If
        
    Next
    
Salir:
   Screen.MousePointer = vbNormal
    Exit Function
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Buscando Glosa Param Bancos"
End Function

Public Function Glosa_Prmtro(lngTipo As Long, lngSubTipo As Long, strNomCorto As String) As String
Rem Factor40
  Dim Cmd As New ADODB.Command

  On Error GoTo Errores
  
  Cmd.ActiveConnection = gAdoCon
  Cmd.CommandType = adCmdStoredProc
  Cmd.CommandText = "dbo_factor.pkg_fme_parametros.fn_fme_glosa_prmtro2"
  
  Cmd.Parameters.Append Cmd.CreateParameter("p_retorno", adVarChar, adParamReturnValue, 50, Null)
  Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
  Cmd.Parameters.Append Cmd.CreateParameter("p_n_tipo", adNumeric, adParamInput, 10, lngTipo)
  Cmd.Parameters.Append Cmd.CreateParameter("p_n_subtipo", adNumeric, adParamInput, 10, lngSubTipo)
  Cmd.Parameters.Append Cmd.CreateParameter("p_v_nomcorto", adVarChar, adParamInput, 20, strNomCorto)
  Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
  Cmd.Execute

  If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
    MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Usuario MiddleWare"
  Else
    Glosa_Prmtro = RTrim(Cmd.Parameters("p_retorno").Value)
  End If
  GoTo Salir

Errores:
  MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Usuario MiddleWare"


Salir:
  Set Cmd = Nothing

End Function

'Public Function Busca_Cod_Estado(lngEmpresa As Long, lngTipo As Long, lngSubTipo As Long, srtNombre As String) As Char
'Rem Factor40
'    Dim recRegistros As New ADODB.Recordset
'    Dim Cmd As New ADODB.Command
'
'   Screen.MousePointer = vbHourglass
'
'   On Error GoTo Errores
'
'    Cmd.ActiveConnection = gAdoCon
'    Cmd.CommandType = adCmdStoredProc
'    Cmd.CommandText = "dbo_factor.pkg_fme_parametros.ide_unico_parametro"
'
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, lngEmpresa)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_tipo", adNumeric, adParamInput, 10, lngTipo)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_subtipo", adNumeric, adParamInput, 10, lngSubTipo)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nomcorto", adVarChar, adParamInput, 10, srtNombre)
'    Busca_Cod_Estado = Cmd.Execute
'
'    GoTo Salir
'
'Errores:
'    Busca_TC_Clientes = ""
'
'Salir:
'    recRegistros.Close
'    Set Cmd = Nothing
'   Screen.MousePointer = vbNormal
'
'End Function

Public Sub Carga_Combo_Tasas(cboCombo As Object, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    Rem Factor40
    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.PKG_FME_TASAS.SP_FME_TASAS_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 80, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem "Todos"
        End If
    End If
    
    While Not recRegistros.EOF
        cboCombo.AddItem recRegistros.Fields("GLS_DESCRIPCION").Value
        cboCombo.ItemData(cboCombo.NewIndex) = recRegistros.Fields("IDE_TASA").Value
        If Not IsMissing(lngSeleccion) Then
            If recRegistros.Fields("IDE_TASA").Value = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        recRegistros.MoveNext
    Wend
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    
LabelSalir:
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub
Public Sub Carga_Combo_Gastos(cboCombo As Object, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    Rem Factor40
    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.PKG_FME_GASTOS.SP_FME_GASTOS_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 40, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem "Todos"
        End If
    End If
    
    While Not recRegistros.EOF
        cboCombo.AddItem recRegistros.Fields("GLS_DESCRIPCION").Value
        cboCombo.ItemData(cboCombo.NewIndex) = recRegistros.Fields("IDE_GASTO").Value
        If Not IsMissing(lngSeleccion) Then
            If recRegistros.Fields("IDE_GASTO").Value = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        recRegistros.MoveNext
    Wend
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0

    
LabelSalir:
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Sub Carga_Combo_Plazos(cboCombo As Object, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    Rem Factor40
    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.PKG_FME_PLAZOS.SP_FME_PLAZOS_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 40, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem "Todos"
        End If
    End If
    
    While Not recRegistros.EOF
        cboCombo.AddItem recRegistros.Fields("GLS_DESCRIPCION").Value
        cboCombo.ItemData(cboCombo.NewIndex) = recRegistros.Fields("IDE_PLAZO").Value
        If Not IsMissing(lngSeleccion) Then
            If recRegistros.Fields("IDE_PLAZO").Value = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        recRegistros.MoveNext
    Wend
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    
    
LabelSalir:
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
    End If
End Sub

Public Sub Carga_Combo_Comisiones(cboCombo As Object, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    Rem Factor40
    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.PKG_FME_COMISIONES.SP_FME_COMISIONES_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 40, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem "Todos"
        End If
    End If
    
    While Not recRegistros.EOF
        cboCombo.AddItem recRegistros.Fields("GLS_DESCRIPCION").Value
        cboCombo.ItemData(cboCombo.NewIndex) = recRegistros.Fields("IDE_COMISION").Value
        If Not IsMissing(lngSeleccion) Then
            If recRegistros.Fields("IDE_COMISION").Value = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        recRegistros.MoveNext
    Wend
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    
    
LabelSalir:
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Sub Carga_Combo_Documentos(cboCombo As Object, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    Rem Factor40
    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.PKG_FME_TIPO_DOCTOS.SP_FME_DOCUMENTOS_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 40, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem "Todos"
        End If
    End If
    
    While Not recRegistros.EOF
        cboCombo.AddItem recRegistros.Fields("GLS_DESCRIPCION").Value
        cboCombo.ItemData(cboCombo.NewIndex) = recRegistros.Fields("IDE_TIPO_DOC").Value
        If Not IsMissing(lngSeleccion) Then
            If recRegistros.Fields("IDE_TIPO_DOC").Value = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        recRegistros.MoveNext
    Wend
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    
    
LabelSalir:
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Sub Carga_Combo_Lineas(cboCombo As Object, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean, Optional strClase As String)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    Rem Factor40
    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.PKG_FME_LINEAS.SP_FME_LINEAS_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 80, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_NombreCortoClase", adVarChar, adParamInput, 8, Trim(strClase))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem "Todos"
        End If
    End If
    
    While Not recRegistros.EOF
        cboCombo.AddItem recRegistros.Fields("GLS_DESCRIPCION").Value
        cboCombo.ItemData(cboCombo.NewIndex) = recRegistros.Fields("IDE_LINEA").Value
        If Not IsMissing(lngSeleccion) Then
            If recRegistros.Fields("IDE_LINEA").Value = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        recRegistros.MoveNext
    Wend
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    
    Screen.MousePointer = 0
LabelSalir:
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Sub Carga_Combo_CamposEntrada(cboCombo As Object, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    Rem Factor40
    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.PGK_FME_CAMPOS_GENERICOS.SP_FME_CAMPOS_VER"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 40, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem "Todos"
        End If
    End If
    
    While Not recRegistros.EOF
        cboCombo.AddItem recRegistros.Fields("GLS_COLUMNA").Value
        cboCombo.ItemData(cboCombo.NewIndex) = recRegistros.Fields("NRO_CORR_COLUMNA").Value
        If Not IsMissing(lngSeleccion) Then
            If recRegistros.Fields("NRO_CORR_COLUMNA").Value = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        recRegistros.MoveNext
    Wend
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0

    
LabelSalir:
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Sub Carga_Combo_ContratosMarco(cboCombo As Object, lngIdeCliente As Long, lngIdeProducto As Long, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    Rem Factor40
    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.PKG_FME_CONTRATO_MARCO.SP_FME_CMARCO_VIG_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_entidad_cli", adNumeric, adParamInput, 10, lngIdeCliente)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeProducto", adNumeric, adParamInput, 10, lngIdeProducto)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Contratos Marcos"
        GoTo LabelSalir
    End If
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem "Todos"
        End If
    End If
    
    While Not recRegistros.EOF
        cboCombo.AddItem recRegistros.Fields("IDE_CONTRATO_MARCO").Value
        cboCombo.ItemData(cboCombo.NewIndex) = recRegistros.Fields("IDE_CONTRATO_MARCO").Value
        If Not IsMissing(lngSeleccion) Then
            If recRegistros.Fields("IDE_CONTRATO_MARCO").Value = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        recRegistros.MoveNext
    Wend
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    
    
LabelSalir:
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Function TraeIdeUnicoParam(intTipo As Integer, intSubTipo As Integer, strNombreCorto As String) As Long
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    Rem Factor40
    Screen.MousePointer = 11
    
    TraeIdeUnicoParam = 0
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.PKG_FME_PARAMETROS.SP_FME_INVOCA_FN"
    
    Rem p_n_nroFuncion = 1 FN_FME_GLOSA_PRMTRO
    Rem                  2 FN_FME_NOMCORTO_PRMTRO
    Rem                  3 FN_FME_IDE_UNICO_PRMTRO
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_nroFuncion", adNumeric, adParamInput, 2, 3)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_tipo", adNumeric, adParamInput, 10, Val(intTipo))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_subtipo", adNumeric, adParamInput, 10, Val(intSubTipo))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_unico", adChar, adParamOutput, 8)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_glosa", adVarChar, adParamOutput, 40)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nomcorto", adVarChar, adParamOutput, 20)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Cmd.Execute

    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser rescatada. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbExclamation, "Rescata ide �nico"
        GoTo LabelSalir
    Else
        'TraeIdeUnicoParam = Cmd.Parameters("p_c_ide_unico").Value
    End If

LabelSalir:
    Screen.MousePointer = 0
    Set Cmd = Nothing
    Exit Function
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Function
Public Sub Carga_Combo_Sucursales(cboCombo As Object, lngIdeCliente As Long, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    Rem Factor40
    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.PKG_FME_ENTIDADES.SP_FME_SUCURSALES_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad", adNumeric, adParamInput, 10, lngIdeCliente)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute

    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Sucursales"
        GoTo LabelSalir
    End If
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem "Todos"
        End If
    End If
    
    While Not recRegistros.EOF
        cboCombo.AddItem recRegistros.Fields("NOM_SUCURSAL").Value
        cboCombo.ItemData(cboCombo.NewIndex) = recRegistros.Fields("NRO_SUCURSAL").Value
        If Not IsMissing(lngSeleccion) Then
            If recRegistros.Fields("NRO_SUCURSAL").Value = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
        recRegistros.MoveNext
    Wend
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    
    
LabelSalir:
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Sub Carga_CboFijo_EstCivil(cboCombo As Object, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
    Rem Factor40
    
   Dim strLista(5)
   Dim intI As Integer
   
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
   
   strLista(0) = "TODOS"
   strLista(1) = "SOLTERO"
   strLista(2) = "CASADO"
   strLista(3) = "DIVORCIADO"
   strLista(4) = "VIUDO"
   
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem strLista(0)
        End If
    End If
    
    For intI = 1 To 4
        
        cboCombo.AddItem strLista(intI)
        cboCombo.ItemData(cboCombo.NewIndex) = intI
        
        If Not IsMissing(lngSeleccion) Then
            If intI = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
    Next
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Error"
    
Salir:
   Screen.MousePointer = vbNormal
End Sub
Public Sub Carga_CboFijo_Sexo(cboCombo As Object, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
    Rem Factor40
    
   Dim strLista(3)
   Dim intI As Integer
   
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
   
   strLista(0) = "TODOS"
   strLista(1) = "MASCULINO"
   strLista(2) = "FEMENINO"
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem strLista(0)
        End If
    End If
    
    For intI = 1 To 2
        
        cboCombo.AddItem strLista(intI)
        cboCombo.ItemData(cboCombo.NewIndex) = intI
        
        If Not IsMissing(lngSeleccion) Then
            If intI = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
    Next
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Error"
    
Salir:
   Screen.MousePointer = vbNormal
End Sub

Public Sub Carga_CboFijo_TipoPersona(cboCombo As Object, Optional ByVal lngSeleccion As Long, Optional blnTodo As Boolean)
    Rem Factor40
    
   Dim strLista(3)
   Dim intI As Integer
   
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
   
   strLista(0) = "TODOS"
   strLista(1) = "NATURAL"
   strLista(2) = "JURIDICA"
    
    cboCombo.Clear
    
    If Not IsMissing(blnTodo) Then
        If blnTodo Then
            cboCombo.AddItem strLista(0)
        End If
    End If
    
    For intI = 1 To 2
        
        cboCombo.AddItem strLista(intI)
        cboCombo.ItemData(cboCombo.NewIndex) = intI
        
        If Not IsMissing(lngSeleccion) Then
            If intI = lngSeleccion Then
                cboCombo.ListIndex = cboCombo.NewIndex
            End If
        End If
    Next
    
    If cboCombo.ListIndex = -1 And cboCombo.ListCount > 0 Then cboCombo.ListIndex = 0
    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Error"
    
Salir:
   Screen.MousePointer = vbNormal
End Sub

Public Sub Carga_Contactos(grdGrilla As Object, lngEntidad As Long)
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
   
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_entidades.sp_fme_contactos_ver"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad", adNumeric, adParamInput, 10, lngEntidad)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Contactos"
        GoTo Salir
    End If

    grdGrilla.MaxRows = 0
    Do While Not recRegistros.EOF
        grdGrilla.MaxRows = grdGrilla.MaxRows + 1
        grdGrilla.Row = grdGrilla.MaxRows
        
        grdGrilla.Col = 1: grdGrilla.Text = "" & recRegistros.Fields("ide_entidad").Value
        grdGrilla.Col = 2: grdGrilla.Text = "" & recRegistros.Fields("nro_contacto").Value
        grdGrilla.Col = 3: grdGrilla.Text = RTrim("" & recRegistros.Fields("rut_contacto").Value) & "-" & recRegistros.Fields("ide_dig_rut").Value
        grdGrilla.Col = 4: grdGrilla.Text = "" & recRegistros.Fields("nomcontacto").Value
        grdGrilla.Col = 5: grdGrilla.Text = "" & recRegistros.Fields("nom_apellido_paterno").Value
        grdGrilla.Col = 6: grdGrilla.Text = "" & recRegistros.Fields("nom_apellido_materno").Value
        grdGrilla.Col = 7: grdGrilla.Text = "" & recRegistros.Fields("nom_nombres").Value
        grdGrilla.Col = 8: grdGrilla.Text = "" & recRegistros.Fields("gls_cargo").Value
        grdGrilla.Col = 9: grdGrilla.Text = "" & recRegistros.Fields("dsc_email").Value
        grdGrilla.Col = 10: grdGrilla.Text = "" & recRegistros.Fields("nro_telefono").Value
        grdGrilla.Col = 11: grdGrilla.Text = "" & recRegistros.Fields("nro_fax").Value
        grdGrilla.Col = 12: grdGrilla.Text = "" & recRegistros.Fields("cod_tipo_contacto").Value
        grdGrilla.Col = 13: grdGrilla.Text = "" & recRegistros.Fields("tipocontacto").Value
        grdGrilla.Col = 14: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_creacion").Value
        grdGrilla.Col = 15: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_creacion").Value
        grdGrilla.Col = 16: grdGrilla.Text = "" & recRegistros.Fields("cod_usr_modifica").Value
        grdGrilla.Col = 17: grdGrilla.Text = "" & recRegistros.Fields("fch_hra_modifica").Value
        recRegistros.MoveNext
    Loop

    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Contactos"
'Resume 0

Salir:
    recRegistros.Close
    Set Cmd = Nothing
    If grdGrilla.MaxRows > 0 Then
        grdGrilla.Row = 1
    End If
   Screen.MousePointer = vbNormal
    
End Sub


'// BUSCA UN ITEM DENTRO DE UN COMBO
Function BuscaItemCbo(ByVal Cbo As Object, ByVal ItemData As Long) As String
Dim i As Integer

    BuscaItemCbo = " "
    For i = 0 To Cbo.ListCount - 1
        If Cbo.ItemData(i) = ItemData Then
            BuscaItemCbo = Cbo.List(i)
            Cbo.ListIndex = i
            Exit For
        End If
    Next i
End Function

'// BUSCA UN ITEMDATA DENTRO DE UN COMBO
Function BuscaItemDataCbo(ByVal Cbo As Object, ByVal Item As String) As Long
Dim J As Integer
    
    BuscaItemDataCbo = 0
    For J = 0 To Cbo.ListCount - 1
        If UCase(Cbo.List(J)) = UCase(Item) Then
            Cbo.ListIndex = J
            BuscaItemDataCbo = Cbo.ItemData(J)
            Exit For
        End If
    Next J

End Function

Rem RECUPERAMOS EL ANCHO SI LA MATRIZ CONTIENE DATOS
Sub RecuperarAnchoCols(ByVal sprd As Object, ByRef Cols() As Long)
Dim i As Integer
With sprd
    For i = 1 To .MaxCols
        .ColWidth(i) = Cols(i)
    Next i
End With
End Sub

Rem RUTINA QUE GUARDA EL ANCHO DE LAS COLUMNAS DE UN SPREAD
Sub GuardarAnchoCols(ByVal sprd As Object, ByRef Cols() As Long)
Dim i As Integer
With sprd
    For i = 1 To .MaxCols
        ReDim Preserve Cols(i)
        Cols(i) = .ColWidth(i)
    Next i
End With

End Sub

Public Sub SincronizaMovimiemtoHorizontal(ByRef sprdOrg As Object, _
                                           ByRef sprdDtno As Object)
'// Realiza un Scroll Horizontalmente en el spread sprdDtno
'// de acuerda al Scroll realizado en el spread sprdOrg
    sprdDtno.LeftCol = sprdOrg.LeftCol
    
End Sub
'// Suma los valores de las columnas especificadas
'// en el array Cols() y que pertenecen  al spread
'// sprdOrg,  dejando  los  totales  en  el  array
'// SumTotales,  para  finalmente  dejarlos en  el
'// spread sprdDtno()
Public Sub CalculaTotales(ByRef sprdOrg As Object, _
                           ByRef sprdDtno As Object, _
                           ByRef Cols() As Integer)
Dim i As Integer
Dim J As Integer
Dim SumTotales() As Currency

    If UBound(Cols()) > 0 Then
        ReDim SumTotales(UBound(Cols()))
    Else
        Exit Sub
    End If
    For i = 1 To sprdOrg.MaxRows
        sprdOrg.Row = i
        For J = 1 To UBound(Cols())
            sprdOrg.Col = Cols(J): SumTotales(J) = SumTotales(J) + CCur(sprdOrg.Text)
        Next J
    Next i
    For i = 1 To UBound(Cols())
        sprdDtno.Col = Cols(i): sprdDtno.Text = SumTotales(i)
    Next i
    
End Sub

'// Ajusta el ancho de las columnas del spread sprdDtno
'// de acuerdo al ancho de las columnas del spread sprdOrg
Public Sub AjustarCol(ByRef sprdOrg As Object, ByRef sprdDtno As Object)
Dim i As Integer
    For i = 1 To sprdOrg.MaxCols
        sprdDtno.ColWidth(i) = sprdOrg.ColWidth(i)
    Next i
End Sub


Function ValidarRut(ByVal Rut As String) As Boolean
Rem    ENTRADA:    UN STRING QUE CONTIEN UN RUT
Rem    SALIDAS:    TRUE  STRING VALIDO COMMO RUT
Rem                FALSE STRING NO VALIDO COMO RUT

Dim Digito As String
Dim RutAux As String
    
    ValidarRut = True
    Rut = Trim(Rut)
    Rem SI RUT = BLANCO ENTONCE SALIR
    If Rut = "" Then
        Rut = ""
        ValidarRut = False
        Exit Function
    End If
    Digito = UCase(Right(Rut, 1))
    If Not InStr(Rut, "-") = 0 Then
        RutAux = Trim(Left(Rut, Len(Rut) - 2))
    Else
        RutAux = Trim(Left(Rut, Len(Rut) - 1))
    End If
    If Len(RutAux) = 0 Then
        ValidarRut = False
    End If
    If Not IsNumeric(RutAux) Then
        ValidarRut = False
    Else
        If Not Digito = TraeDigVerificador(RutAux) Then ValidarRut = False
    End If
    
End Function

Function TraeDigVerificador(ByVal Rut As String) As String
Rem  Autor:        SJO 09/12/96
Rem  Modificador:  Nenett Far�as Moore 20/03/2000

Dim Suma As Long
Dim Largo As Byte
Dim Numero As Long
Dim Modulo As Integer
Dim i As Integer
    
    Rut = Trim(Rut)
    Largo = Len(Rut)
    Suma = 0
    For i = 1 To Largo
        Numero = CInt(Mid(Rut, Largo - i + 1, 1))
        If i > 6 Then
            Suma = Suma + Numero * (i - 5)
        Else
            Suma = Suma + Numero * (i + 1)
         End If

    Next i

     Modulo = 11 - (Suma Mod 11)

     If Modulo = 10 Then
        TraeDigVerificador = "K"
     ElseIf Modulo% = 11 Then
        TraeDigVerificador = "0"
     Else
        TraeDigVerificador = Trim(CStr(Modulo))
     End If

End Function

Rem ELIMINA LOS ESPACIOS EN BLANCO QUE SON INNECESARIOS EN UNA CADENA
Function FormatearCadena(ByVal nombre As String) As String
Dim NombreAux As String
Dim pos As Integer, Largo As String

nombre = LTrim(nombre)
nombre = RTrim(nombre)
If Len(nombre) = 0 Then
    FormatearCadena = ""
    Exit Function
End If
Do While Not Len(nombre) = 0
    Largo = Len(nombre)
    pos = InStr(nombre, " ")
    If Not pos = 0 Then
        NombreAux = NombreAux & " " & Left(nombre, pos - 1)
        nombre = LTrim(Right(nombre, Largo - pos))
    Else
        NombreAux = NombreAux & " " & nombre
        nombre = ""
    End If
Loop
FormatearCadena = LTrim(NombreAux)
End Function

Public Sub PrintYX(Y As Integer, X As Integer, Valor As Variant)
    Printer.CurrentY = Y
    Printer.CurrentX = X
    Printer.Print Valor
End Sub

Sub RprintYX(Y As Integer, X As Integer, Valor As Variant, nLargo)
    Dim nDif As Double
    nDif = Printer.TextWidth(String(nLargo, "0")) - Printer.TextWidth(Trim(Valor))
    Printer.CurrentY = Y
    Printer.CurrentX = X
    If nDif > 0 Then
        Printer.CurrentX = Printer.CurrentX + nDif
    End If
    Printer.Print Trim(Valor)
End Sub

Public Sub OrdenaCol(ByVal sprd As Object, ByVal Col As Long, Optional ByVal Row As Long)
    
    Rem ---------------------------------------------------------------
    Rem Ordena en forma ascendente los datos contenidos en la columna
    Rem seleccionada, dentro de una grilla "Spread"
    Rem ---------------------------------------------------------------
    Rem 26/05/2000, PRS.
    Rem 26/05/2000, CC.
    Rem 20/06/2000, CC. "Row opcional"
    
    If IsMissing(Row) Then Row = sprd.ActiveRow
    
    If Row = 0 Then
        Rem Desactiva el redibujo
        sprd.ReDraw = False
            
        Rem Activa bloque
        'sprd.Action = 2
          
        Rem selecciona bloque
        sprd.Row = -1
        sprd.Col = -1
        
        Rem Setea orden por ...
        sprd.SortBy = 0: Rem Fila
        
        Rem Setea tipo de orden
        sprd.SortKey(1) = Col
        sprd.SortKeyOrder(1) = 1: Rem SS_SORT_ORDER_ASCENDING
        
        Rem Selecciona tipo de action
        sprd.Action = 25: Rem Sort
        
        Rem Desactiva Columna
        sprd.Col = Col
        sprd.Col2 = Col
        sprd.Action = 14
        
        Rem Refresca la grilla
        sprd.Refresh
        
        Rem Activa el redibujo
        sprd.ReDraw = True
        
    
    End If

End Sub
Public Sub Bloquea_Controles(ByVal objForm As Form)
    Dim objControl As Object
    For Each objControl In objForm
        If TypeOf objControl Is TextBox Or _
            TypeOf objControl Is ComboBox Then
            objControl.Locked = True
            objControl.BackColor = &HC0C0C0
        End If
        If TypeOf objControl Is CheckBox Then
            objControl.Enabled = False
        End If
    Next
End Sub

Public Function fin_TasaPer(ByVal lngTipoTasa As Long, ByVal lngPzoProm As Long, ByVal lngMoneda As Long, ByVal dtFecha As Date) As Double

    Rem =======================================================
    Rem Retorna la Tasa del Per�odo (i)
    Rem <lngMonConv> es s�lo por compatibilidad, pero no se usa
    Rem =======================================================
    
    Dim adoCmd  As New ADODB.Command
    Dim adoPar  As New ADODB.Parameter
  
    adoCmd.CommandText = "dbo_factor.PCK_FINAN.TasaPer"
    adoCmd.CommandType = adCmdStoredProc
    adoCmd.ActiveConnection = gAdoCon
    
    adoCmd.Parameters.Append adoCmd.CreateParameter("pReturn", adDouble, adParamReturnValue, 0, 0)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pTipoTasa", adDouble, adParamInput, 0, lngTipoTasa)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pPzoProm", adDouble, adParamInput, 0, lngPzoProm)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pMoneda", adDouble, adParamInput, 0, lngMoneda)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pFecha", adDate, adParamInput, 8, dtFecha)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pBusquedaExacta", adDouble, adParamInput, 0, 1)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pEvento", adChar, adParamOutput, 250, "xxx")
    
    adoCmd.Execute
    
    If Val(adoCmd.Parameters(6)) <> 0 Then
        Err.Raise 10000, "fin_TasaPer", adoCmd.Parameters(6), "", 10000
    End If
    
    fin_TasaPer = adoCmd.Parameters(0)

Salir:
    Set adoCmd = Nothing
End Function

Public Function fin_Paridad(curMonto As Currency, lngMoneda As Long, dFecha As Date, Optional lngMonConv As Long) As Currency

    Rem =======================================================
    Rem Retorna la Paridad
    Rem <lngMonConv> es s�lo por compatibilidad, pero no se usa
    Rem =======================================================
    
    Dim adoCmd  As New ADODB.Command
    Dim adoPar  As New ADODB.Parameter
    Dim strEvento As String
  
    If lngMoneda = 999 Then
        fin_Paridad = curMonto
        GoTo Salir
    End If
  
    adoCmd.CommandText = "dbo_factor.PCK_FINAN.mtoParidad"
    adoCmd.CommandType = adCmdStoredProc
    adoCmd.ActiveConnection = gAdoCon
    adoCmd.Parameters.Append adoCmd.CreateParameter("pMoneda", adDouble, adParamInput, 0, lngMoneda)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pMonto", adDouble, adParamInput, 0, curMonto)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pFecha", adDate, adParamInput, 0, dFecha)
    adoCmd.Parameters.Append adoCmd.CreateParameter("strEvento", adChar, adParamOutput, 0, strEvento)

    adoCmd.Execute
    fin_Paridad = curMonto * adoCmd.Parameters(0)
    

Salir:
    Set adoCmd = Nothing
End Function


Public Function fin_Interes(ByVal dblTasa As Double, ByVal lngTipoInt As Long, ByVal strPerInt As String, ByVal dtPerIni As Date, ByVal dtPerFin As Date) As Double

    Rem =========================================
    Rem Devuelve el inter�s seg�n la tasa,
    Rem tipo de interes (simple/comuesto)
    Rem periodo interes (d/m/a)
    Rem y per�odo.
    Rem =========================================

    Dim adoCmd  As New ADODB.Command
    adoCmd.ActiveConnection = gAdoCon
    adoCmd.CommandText = "dbo_factor.PCK_FINAN.Interes"
    adoCmd.CommandType = adCmdStoredProc
    adoCmd.Parameters.Append adoCmd.CreateParameter("pReturn", adDouble, adParamReturnValue, 0, 0)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pTasa", adDouble, adParamInput, 0, dblTasa)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pTipoInt", adDouble, adParamInput, 0, lngTipoInt)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pDesde", adDate, adParamInput, 8, Format(dtPerIni, "dd/mm/yyyy"))
    adoCmd.Parameters.Append adoCmd.CreateParameter("pHasta", adDate, adParamInput, 8, Format(dtPerFin, "dd/mm/yyyy"))
    adoCmd.Parameters.Append adoCmd.CreateParameter("pPeriodo", adChar, adParamInput, 1, strPerInt)
    adoCmd.Parameters.Append adoCmd.CreateParameter("pEvento", adChar, adParamOutput, 250, "xxx")
    adoCmd.Execute
    fin_Interes = adoCmd.Parameters(0)
    
Salir:
    Set adoCmd = Nothing


End Function

Public Function HayCambiosEnControles(frm As Form, strMarca As String) As Boolean
Dim lngInd As Long
Dim blnCambio As Boolean
Dim o As Object
On Error GoTo LabelError

    HayCambiosEnControles = False
    lngInd = 0
    For Each o In frm.Controls
        If UCase(Trim(o.Tag)) = UCase(Trim(strMarca)) Then
            If o <> garrControles(lngInd, 1) Then
                HayCambiosEnControles = True
                Exit For
            End If
            lngInd = lngInd + 1
        End If
    Next o
    
LabelSalir:
    Exit Function
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Function

Public Sub LimpiarControles(frm As Form, strMarca As String)
Dim o As Object
On Error GoTo LabelError

    For Each o In frm.Controls
        If UCase(Trim(o.Tag)) = UCase(Trim(strMarca)) Then
            If TypeOf o Is TextBox Then
                o.Text = ""
                o.DataChanged = False
            ElseIf TypeOf o Is ComboBox Then
                o.ListIndex = -1
                o.DataChanged = False
            ElseIf TypeOf o Is CheckBox Then
                o.Value = 0
                o.DataChanged = False
            ElseIf TypeOf o Is OptionButton Then
                o.Value = False
            ElseIf TypeOf o Is DTPicker Then
                o = Format(Date, "dd/mm/yyyy")
                o.DataChanged = False
            ElseIf TypeOf o Is Label Then
                If o.BorderStyle <> 0 Then
                    o.Caption = ""
                End If
                o.DataChanged = False
            End If
            'o.Enabled = IIf(intDisable = 1, False, True)
        End If
    Next o
    
LabelSalir:
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Sub HabilitarTabs(stab As Object, strListaHabil As String, intPosicion As Integer)
Dim intI As Integer
Dim strValorLista As String
Dim intCantLista As Integer
Dim strCaracter As String
On Error GoTo LabelError
    
    For intI = 1 To Len(strListaHabil)
        strCaracter = Mid(strListaHabil, intI, 1)
        If strCaracter = ";" Then intCantLista = intCantLista + 1
    Next intI
    
    For intI = 0 To Val(stab.Tabs - 1)
        stab.TabEnabled(intI) = False
    Next
    
    For intI = 0 To intCantLista - 1
        strValorLista = Split(strListaHabil, ";")(intI)
        stab.TabEnabled(Val(strValorLista)) = True
    Next intI
        
    stab.Tab = intPosicion
    
LabelSalir:
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, , "Error"
        Err.Clear
        GoTo LabelSalir
    End If
End Sub

Public Sub AlmacenarValoresCtls(frm As Form, strMarca As String)
Dim lngInd As Long
Dim lngRgtros As Long
Dim o As Object
On Error GoTo LabelError
    
    Rem Contar Cantidad de controles Marcados
    For Each o In frm.Controls
        If UCase(Trim(o.Tag)) = UCase(Trim(strMarca)) Then
                lngRgtros = lngRgtros + 1
        End If
    Next o
    
    Rem Dimensionamos el Arreglo
    lngRgtros = lngRgtros - 1
    ReDim garrControles(0 To lngRgtros, 0 To 1)
    Set o = Nothing
    
    Rem Guardamos los Valores iniciales de los Controles en el Arreglo
    lngInd = 0
    For Each o In frm.Controls
        If UCase(Trim(o.Tag)) = UCase(Trim(strMarca)) Then
            garrControles(lngInd, 0) = o.Name
            garrControles(lngInd, 1) = o
            lngInd = lngInd + 1
        End If
    Next o

LabelSalir:
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
    End If
End Sub

Function Es_Control(Valor As Integer) As Integer
    If Valor = 13 Or Valor = 8 Then
       Es_Control = True
    Else
       Es_Control = False
    End If
End Function

Function Es_Numero(strTexto As String, intValor As Integer, blnEsDecimal As Boolean, Optional intEnteros As Integer) As Integer
    
    If blnEsDecimal Then
        If (intValor >= Asc("0") And intValor <= Asc("9")) Or (intValor = 13 Or intValor = 8) Or intValor = Asc(gstrSepDec) Then
            If intValor = Asc(gstrSepDec) And InStr(strTexto, gstrSepDec) > 0 Then
                Es_Numero = False
            Else
                If InStr((strTexto & Chr(intValor) & gstrSepDec), gstrSepDec) > intEnteros And intValor <> 8 Then
                    Es_Numero = False
                Else
                    Es_Numero = True
                End If
            End If
        Else
            Es_Numero = False
        End If
    
    Else
        If (intValor >= Asc("0") And intValor <= Asc("9")) Or (intValor = 13 Or intValor = 8) Then
           Es_Numero = True
        Else
           Es_Numero = False
        End If
    End If
End Function

Public Function Trae_Ide_Entidad(mlngRut As Long) As Long
Rem Factor40
    Dim Cmd As New ADODB.Command
    Dim strCodigo As String
    
   Screen.MousePointer = vbHourglass
   

   On Error GoTo Errores
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_entidades.sp_fme_ide_entidad"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_rut", adNumeric, adParamInput, 10, mlngRut)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_entidad", adNumeric, adParamOutput, 10)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Entidad"
    Else
        Trae_Ide_Entidad = Cmd.Parameters("p_n_entidad").Value
    End If
    GoTo Salir

Errores:
    Trae_Ide_Entidad = 0
    MsgBox "Se ha producido un error al cargar el c�digo de entidad", vbInformation, "Entidad"
    
Salir:
    'recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Function

Public Sub PosicionaGrilla(Grilla As Object, varCodigo As Variant, lngColumnaABuscar As Long)
Dim lngI As Long
On Error GoTo LabelError
    
    Grilla.Col = lngColumnaABuscar
    For lngI = 1 To Grilla.MaxRows
        Grilla.Row = lngI
        If Trim(Grilla.Text) = Trim(varCodigo) Then
            Exit For
        End If
    Next lngI
    Grilla.Row = lngI
    Grilla.Col = 1
    Grilla.Action = 0
    
LabelSalir:
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub


Function Pos_Cbo_Glosa(ByVal Cbo As Object, ByVal Item As String) As Long
Rem Factor40
Dim J As Integer
Dim blnEncontro As Boolean
    
    blnEncontro = False
    
    Pos_Cbo_Glosa = 0
    For J = 0 To Cbo.ListCount - 1
        If UCase(Cbo.List(J)) = UCase(Item) Then
            Cbo.ListIndex = J
            Pos_Cbo_Glosa = Cbo.ItemData(J)
            blnEncontro = True
            Exit For
        End If
    Next J
    
    If Not blnEncontro Then Cbo.ListIndex = -1
    
End Function
