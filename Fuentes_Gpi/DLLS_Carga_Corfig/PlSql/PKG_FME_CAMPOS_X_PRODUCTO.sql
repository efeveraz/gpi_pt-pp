CREATE OR REPLACE PACKAGE pkg_fme_campos_x_producto
IS
   TYPE micursor IS REF CURSOR;

   PROCEDURE sp_fme_corr_filler_ver (
      p_n_empresa       IN       NUMBER,
      p_n_ideproducto   IN       NUMBER,
      p_n_correlativo   OUT      NUMBER,
      p_v_descerror     OUT      VARCHAR2
   );

   PROCEDURE sp_fme_campos_ver (
      p_n_empresa       IN       NUMBER,
      p_n_ideproducto   IN       NUMBER,
      p_c_codtipoarch   IN       CHAR,
      p_c_codseccion    IN       CHAR,
      p_n_codigo        IN       NUMBER,
      p_v_nombre        IN       VARCHAR2,
      p_c_cursor        OUT      micursor,
      p_v_descerror     OUT      VARCHAR2
   );

   PROCEDURE sp_fme_campos_orden_ver (
      p_n_empresa       IN       NUMBER,
      p_n_ideproducto   IN       NUMBER,
      p_c_codtipoarch   IN       CHAR,
      p_c_cursor        OUT      micursor,
      p_v_descerror     OUT      VARCHAR2
   );

   PROCEDURE sp_fme_campo_totaliza (
      p_n_empresa       IN       NUMBER,
      p_n_ideproducto   IN       NUMBER,
      p_c_codtipoarch   IN       CHAR,
      p_c_seccion       IN       CHAR,
      p_n_corr_campo    IN       NUMBER,
      p_n_resultado     OUT      NUMBER,
      p_v_descerror     OUT      VARCHAR2
   );

   PROCEDURE sp_fme_campos_grabar (
      p_n_empresa           IN       NUMBER,
      p_n_ideproducto       IN       NUMBER,
      p_c_codtipoarch       IN       CHAR,
      p_c_codseccion        IN       CHAR,
      p_v_glosa             IN       VARCHAR2,
      p_n_idecolumna        IN       NUMBER,
      p_c_flagobligatorio   IN       CHAR,
      p_c_flagordena        IN       CHAR,
      p_c_flagtotaliza      IN       CHAR,
      p_c_flagconvertir     IN       CHAR,
      p_v_usuario           IN       CHAR,
      p_n_accion            IN       NUMBER,
      p_n_codigo            IN OUT   NUMBER,
      p_v_descerror         OUT      VARCHAR2
   );

   PROCEDURE sp_fme_campos_borrar (
      p_n_empresa     IN       NUMBER,
      p_n_codigo      IN       NUMBER,
      p_v_descerror   OUT      VARCHAR2
   );
END pkg_fme_campos_x_producto;
/
CREATE OR REPLACE PACKAGE BODY pkg_fme_campos_x_producto
AS
   PROCEDURE sp_fme_corr_filler_ver (
      p_n_empresa       IN       NUMBER,
      p_n_ideproducto   IN       NUMBER,
      p_n_correlativo   OUT      NUMBER,
      p_v_descerror     OUT      VARCHAR2
   )
   IS
   BEGIN
      p_v_descerror := '0010 Buscando Correlativo campo filler';

      BEGIN
         SELECT nro_corr_campo
           INTO p_n_correlativo
           FROM CSBPI.ta_campo_archivo_remesa
          WHERE ide_empresa = p_n_empresa
            AND ide_producto = p_n_ideproducto
            AND cod_tipo_archivo = 0
            AND cod_ide_seccion = 0
            AND cod_ide_campo = -99;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_n_correlativo := -1;
      END;

      IF p_n_correlativo = -1
      THEN
         p_v_descerror := '0020 No existe Filler';
      ELSE
         p_v_descerror := '9999 Termino';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
                RTRIM (p_v_descerror)
             || ' >> '
             || SQLCODE
             || ' '
             || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_corr_filler_ver;

-----------------------------------------------------------------

   PROCEDURE sp_fme_campos_ver (
      p_n_empresa       IN       NUMBER,
      p_n_ideproducto   IN       NUMBER,
      p_c_codtipoarch   IN       CHAR,
      p_c_codseccion    IN       CHAR,
      p_n_codigo        IN       NUMBER,
      p_v_nombre        IN       VARCHAR2,
      p_c_cursor        OUT      micursor,
      p_v_descerror     OUT      VARCHAR2
   )
   AS
   BEGIN
      p_v_descerror := '0000 Inicio';

      IF p_n_codigo > 0
      THEN
         p_v_descerror := '0010 Buscando con Criterio Codigo';
         OPEN p_c_cursor FOR
            SELECT a.ide_empresa, a.ide_producto, a.cod_tipo_archivo,
                   a.cod_ide_seccion, b.nom_corto, a.cod_ide_campo,
	  			   pkg_fme_parametros.fn_fme_glosa_prmtro(a.cod_ide_campo ) nom_campo,
                   c.gls_columna, a.nro_corr_campo, a.nro_corr_columna,
                   a.gls_campo, a.flg_obligatorio, a.flg_ind_ordena,
                   a.flg_ind_totaliza, a.flg_convertir, a.fch_hra_creacion,
                   a.cod_usr_creacion, a.fch_hra_modifica,
                   a.cod_usr_modifica
              FROM CSBPI.ta_campo_archivo_remesa a,
                   CSBPI.ta_parametro b,
                   CSBPI.ta_columna_remesa c
             WHERE a.ide_empresa = p_n_empresa
               AND a.nro_corr_campo = p_n_codigo
               AND a.cod_ide_seccion = b.ide_unico_param(+)
               AND a.ide_empresa = c.ide_empresa
               AND a.nro_corr_columna = c.nro_corr_columna;
      ELSIF p_v_nombre IS NOT NULL
      THEN
         p_v_descerror := '0020 Buscando con Criterio Nombre';
         OPEN p_c_cursor FOR
            SELECT a.ide_empresa, a.ide_producto, a.cod_tipo_archivo,
                   a.cod_ide_seccion, b.nom_corto, a.cod_ide_campo,
				   pkg_fme_parametros.fn_fme_glosa_prmtro(a.cod_ide_campo ) nom_campo,
                   c.gls_columna, a.nro_corr_campo, a.nro_corr_columna,
                   a.gls_campo, a.flg_obligatorio, a.flg_ind_ordena,
                   a.flg_ind_totaliza, a.flg_convertir, a.fch_hra_creacion,
                   a.cod_usr_creacion, a.fch_hra_modifica,
                   a.cod_usr_modifica
              FROM CSBPI.ta_campo_archivo_remesa a,
                   CSBPI.ta_parametro b,
                   CSBPI.ta_columna_remesa c
             WHERE a.ide_empresa = p_n_empresa
               AND a.ide_producto = p_n_ideproducto
               AND a.cod_tipo_archivo = p_c_codtipoarch
               AND a.cod_ide_seccion =
                      DECODE (p_c_codseccion,
                              '0', a.cod_ide_seccion,
                              p_c_codseccion
                             )
               AND a.gls_campo LIKE p_v_nombre || '%'
               AND a.cod_ide_seccion = b.ide_unico_param(+)
               AND a.ide_empresa = c.ide_empresa
               AND a.nro_corr_columna = c.nro_corr_columna;
      ELSE
         p_v_descerror := '0030 Buscando sin Criterios';
         OPEN p_c_cursor FOR
            SELECT   a.ide_empresa, a.ide_producto, a.cod_tipo_archivo,
                     a.cod_ide_seccion, b.nom_corto, a.cod_ide_campo,
					 pkg_fme_parametros.fn_fme_glosa_prmtro(a.cod_ide_campo ) nom_campo,
                     c.gls_columna, a.nro_corr_campo, a.nro_corr_columna,
                     a.gls_campo, a.flg_obligatorio, a.flg_ind_ordena,
                     a.flg_ind_totaliza, a.flg_convertir, a.fch_hra_creacion,
                     a.cod_usr_creacion, a.fch_hra_modifica,
                     a.cod_usr_modifica
                FROM CSBPI.ta_campo_archivo_remesa a,
                     CSBPI.ta_parametro b,
                     CSBPI.ta_columna_remesa c
               WHERE a.ide_empresa = p_n_empresa
                 AND a.ide_producto = p_n_ideproducto
                 AND a.cod_tipo_archivo = p_c_codtipoarch
                 AND a.cod_ide_seccion =
                        DECODE (p_c_codseccion,
                                '0', a.cod_ide_seccion,
                                p_c_codseccion
                               )
                 AND a.cod_ide_seccion = b.ide_unico_param(+)
                 AND a.ide_empresa = c.ide_empresa
                 AND a.nro_corr_columna = c.nro_corr_columna
            ORDER BY a.cod_ide_seccion, a.gls_campo;
      END IF;

      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;
         p_v_descerror :=
                RTRIM (p_v_descerror)
             || ' >> '
             || SQLCODE
             || ' '
             || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_campos_ver;

----------------------------------------------------------------------------

   PROCEDURE sp_fme_campos_orden_ver (
      p_n_empresa       IN       NUMBER,
      p_n_ideproducto   IN       NUMBER,
      p_c_codtipoarch   IN       CHAR,
      p_c_cursor        OUT      micursor,
      p_v_descerror     OUT      VARCHAR2
   )
   IS
      l_v_ide_unico_secc   CHAR (8);
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_v_ide_unico_secc :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 7,
                                                     'DET');
      p_v_descerror := '0010 Abriendo cursor';
      OPEN p_c_cursor FOR
         SELECT cod_ide_campo, nro_corr_campo, gls_campo, flg_ind_totaliza
           FROM CSBPI.ta_campo_archivo_remesa
          WHERE ide_empresa = p_n_empresa
            AND ide_producto = p_n_ideproducto
            AND cod_tipo_archivo = p_c_codtipoarch
            AND cod_ide_seccion = l_v_ide_unico_secc
            AND (flg_ind_ordena = 1 OR flg_ind_totaliza = 1);
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;
         p_v_descerror :=
                RTRIM (p_v_descerror)
             || ' >> '
             || SQLCODE
             || ' '
             || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_campos_orden_ver;

----------------------------------------------------------------------------

   PROCEDURE sp_fme_campo_totaliza (
      p_n_empresa       IN       NUMBER,
      p_n_ideproducto   IN       NUMBER,
      p_c_codtipoarch   IN       CHAR,
      p_c_seccion       IN       CHAR,
      p_n_corr_campo    IN       NUMBER,
      p_n_resultado     OUT      NUMBER,
      p_v_descerror     OUT      VARCHAR2
   )
   IS
      l_c_ind_tot   CHAR (8);
   BEGIN
      p_v_descerror := '0010 Creando Consulta';

      BEGIN
         SELECT flg_ind_totaliza
           INTO l_c_ind_tot
           FROM CSBPI.ta_campo_archivo_remesa
          WHERE ide_empresa = p_n_empresa
            AND ide_producto = p_n_ideproducto
            AND cod_tipo_archivo = p_c_codtipoarch
            AND cod_ide_seccion = p_c_seccion
            AND nro_corr_campo = p_n_corr_campo;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_c_ind_tot := '0';
      END;

      p_n_resultado := TO_NUMBER (l_c_ind_tot);
      p_v_descerror := '9999 Termino';
   END sp_fme_campo_totaliza;

----------------------------------------------------------------------------

   PROCEDURE sp_fme_campos_grabar (
      p_n_empresa           IN       NUMBER,
      p_n_ideproducto       IN       NUMBER,
      p_c_codtipoarch       IN       CHAR,
      p_c_codseccion        IN       CHAR,
      p_v_glosa             IN       VARCHAR2,
      p_n_idecolumna        IN       NUMBER,
      p_c_flagobligatorio   IN       CHAR,
      p_c_flagordena        IN       CHAR,
      p_c_flagtotaliza      IN       CHAR,
      p_c_flagconvertir     IN       CHAR,
      p_v_usuario           IN       CHAR,
      p_n_accion            IN       NUMBER,
      p_n_codigo            IN OUT   NUMBER,
      p_v_descerror         OUT      VARCHAR2
   )
   AS
      l_n_ide            NUMBER;
      l_d_fechahora      DATE;
      l_codcampofisico   NUMBER;
      l_n_cantreg        NUMBER (1);
   --Rem Definicion de tipo accion
   --Rem 1= Grabar; 2 = Modificar (p_n_Accion)

   BEGIN
      p_v_descerror := '0000 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      p_v_descerror :=
          '0010 Buscando codigo del campo fisico en la tabla CSBPI.ta_COLUMA_REMESA';

      SELECT cod_ide_campo
        INTO l_codcampofisico
        FROM CSBPI.ta_columna_remesa
       WHERE ide_empresa = p_n_empresa AND nro_corr_columna = p_n_idecolumna;

      p_v_descerror := '0020 Consultando si existe el registro';
      l_n_cantreg := 1;

      IF p_n_accion = 1
      THEN
         SELECT COUNT (*)
           INTO l_n_cantreg
           FROM CSBPI.ta_campo_archivo_remesa
          WHERE ide_empresa = p_n_empresa
            AND ide_producto = p_n_ideproducto
            AND cod_tipo_archivo = p_c_codtipoarch
            AND cod_ide_seccion = p_c_codseccion
            AND nro_corr_columna = p_n_idecolumna;

         IF l_n_cantreg > 0
         THEN
            p_v_descerror := '1111 ya existe el Campo';
            RETURN;
         END IF;
      END IF;

      p_v_descerror := '0030 Actualizando campo archivo por Producto';

      UPDATE CSBPI.ta_campo_archivo_remesa
         SET gls_campo = p_v_glosa,
             nro_corr_columna = p_n_idecolumna,
             cod_ide_campo = l_codcampofisico,
             flg_obligatorio = p_c_flagobligatorio,
             flg_ind_ordena = p_c_flagordena,
             flg_ind_totaliza = p_c_flagtotaliza,
             flg_convertir = p_c_flagconvertir,
             fch_hra_modifica = l_d_fechahora,
             cod_usr_modifica = p_v_usuario
       WHERE ide_empresa = p_n_empresa AND nro_corr_campo = p_n_codigo;

      IF SQL%ROWCOUNT <= 0
      THEN
         p_v_descerror :=
                 '0040 Siguiente Ide de secuencia campo archivo por Producto';

         SELECT CSBPI.sc_fme_campos_x_producto.NEXTVAL
           INTO l_n_ide
           FROM DUAL;

         p_v_descerror := '0050 Creando nuevo campo archivo por Producto';

         INSERT INTO CSBPI.ta_campo_archivo_remesa
                     (ide_empresa, ide_producto, cod_tipo_archivo,
                      cod_ide_seccion, cod_ide_campo, nro_corr_campo,
                      nro_corr_columna, gls_campo, flg_obligatorio,
                      flg_ind_ordena, flg_ind_totaliza, flg_convertir,
                      fch_hra_creacion, cod_usr_creacion, fch_hra_modifica,
                      cod_usr_modifica
                     )
              VALUES (p_n_empresa, p_n_ideproducto, p_c_codtipoarch,
                      p_c_codseccion, l_codcampofisico, l_n_ide,
                      p_n_idecolumna, p_v_glosa, p_c_flagobligatorio,
                      p_c_flagordena, p_c_flagtotaliza, p_c_flagconvertir,
                      l_d_fechahora, p_v_usuario, NULL,
                      NULL
                     );

         p_n_codigo := l_n_ide;
      END IF;
	  
	  COMMIT;
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
                RTRIM (p_v_descerror)
             || ' >> '
             || SQLCODE
             || ' '
             || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_campos_grabar;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_campos_borrar (
      p_n_empresa     IN       NUMBER,
      p_n_codigo      IN       NUMBER,
      p_v_descerror   OUT      VARCHAR2
   )
   AS
      l_n_cantreg   NUMBER (11);
   BEGIN
      p_v_descerror := '0000 Inicio';
      p_v_descerror :=
         '0010 Verificando si existe un campo Archivo por Producto en la estructura de achivo por Cliente';

      SELECT COUNT (*)
        INTO l_n_cantreg
        FROM CSBPI.ta_formato_detalle_archivo
       WHERE ide_empresa = p_n_empresa AND nro_corr_campo = p_n_codigo;

      IF l_n_cantreg > 0
      THEN
         p_v_descerror :=
                '1111 Existe a lo menos un registro de estructura de achivo por Cliente,'
             || CHR (13)
             || 'ocupando el campo archivo por cliente, que desea eliminar'
             || CHR (13)
             || 'Favor de eliminar los registros relacionados, antes de eliminar el campo archivo por producto';
         RETURN;
      END IF;

      p_v_descerror := '0020 Eliminando el registro';

      DELETE FROM CSBPI.ta_campo_archivo_remesa
            WHERE ide_empresa = p_n_empresa AND nro_corr_campo = p_n_codigo;

      COMMIT;
	  p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
                RTRIM (p_v_descerror)
             || ' >> '
             || SQLCODE
             || ' '
             || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_campos_borrar;
END pkg_fme_campos_x_producto;
/

