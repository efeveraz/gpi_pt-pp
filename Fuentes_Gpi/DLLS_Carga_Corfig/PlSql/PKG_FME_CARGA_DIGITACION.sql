CREATE OR REPLACE PACKAGE pkg_fme_carga_digitacion
IS
   TYPE micursor IS REF CURSOR;

   PROCEDURE sp_fme_glosa_estructura (
      p_n_empresa        IN       NUMBER,
      p_n_ide_cliente    IN       NUMBER,
      p_n_ide_producto   IN       NUMBER,
      p_n_ide_sucursal   IN       NUMBER,
      p_c_tipo_arch      IN       CHAR,
      p_c_cursor         OUT      micursor,
      p_v_descerror      OUT      VARCHAR2
   );

   PROCEDURE sp_fme_registros_remesas_ver (
      p_n_empresa         IN       NUMBER,
      p_n_codigo          IN       NUMBER,
      p_n_ide_cliente     IN       NUMBER,
      p_n_ide_producto    IN       NUMBER,
      p_n_ide_contrato    IN       NUMBER,
      p_n_ide_sucursal    IN       NUMBER,
      p_c_tipo_formato    IN       CHAR,
      p_c_origen          IN       CHAR,
      p_c_flgdigitando    IN       CHAR,
      p_d_fecharecepini   IN       DATE,
      p_d_fecharecepfin   IN       DATE,
      p_c_codestado       IN       CHAR,
      p_c_cursor          OUT      micursor,
      p_v_descerror       OUT      VARCHAR2
   );

   PROCEDURE sp_fme_errores_remesas_ver (
      p_n_empresa     IN       NUMBER,
      p_n_numremesa   IN       NUMBER,
      p_c_cursor      OUT      micursor,
      p_v_descerror   OUT      VARCHAR2
   );

   PROCEDURE sp_fme_estructura_cliente_ver (
      p_n_empresa        IN       NUMBER,
      p_n_ide_cliente    IN       NUMBER,
      p_n_ide_producto   IN       NUMBER,
      p_n_ide_sucursal   IN       NUMBER,
      p_c_cursor         OUT      micursor,
      p_v_descerror      OUT      VARCHAR2
   );

   PROCEDURE sp_fme_archivo_cliente_ver (
      p_n_empresa         IN       NUMBER,
      p_n_codigo          IN       NUMBER,
      p_n_contratomarco   IN       NUMBER,
      p_n_tiposeccion     IN       NUMBER,
      p_n_nrolinea        IN       NUMBER,
      p_c_cursor          OUT      micursor,
      p_v_descerror       OUT      VARCHAR2
   );

   PROCEDURE sp_fme_registro_archivo_grabar (
      p_n_empresa                 IN       NUMBER,
      p_n_ideremesa               IN       NUMBER,
      p_n_nrolinea                IN       NUMBER,
      p_n_cod_corredor            IN       NUMBER,
      p_v_nom_corredor            IN       VARCHAR2,
      p_v_rut_cliente             IN       VARCHAR2,
      p_v_nom_cliente             IN       VARCHAR2,
      p_v_gls_cta_ctdia_clte      IN       VARCHAR2,
      p_v_nemotecnico             IN       VARCHAR2,
      p_n_mnt_nominal             IN       NUMBER,
      p_n_mnt_precio              IN       NUMBER,
      p_n_mnt_monto               IN       NUMBER,
      p_n_mnt_drcho_bolsa         IN       NUMBER,
      p_n_mnt_gastos              IN       NUMBER,
      p_n_mnt_comision            IN       NUMBER,
      p_n_mnt_total               IN       NUMBER,
      p_v_nro_factura             IN       VARCHAR2,
      p_d_fch_fecha               IN       DATE,
      p_v_gls_tipo_liquidac       IN       VARCHAR2,
      p_c_mrc_tipo_opera          IN       CHAR,
      p_v_cod_clasif_riesgo_afp   IN       VARCHAR2,
      p_n_duracion_dias_afp       IN       NUMBER,
      p_v_cod_instrumento_afp     IN       VARCHAR2,
      p_v_cod_moneda_afp          IN       VARCHAR2,
      p_n_mnt_factor_precio_afp   IN       NUMBER,
      p_n_tasa_mercado_afp        IN       NUMBER,
      p_n_tasa_adicional_afp      IN       NUMBER,
      p_v_usuario                 IN       CHAR,
      p_v_descerror               OUT      VARCHAR2
   );

   PROCEDURE sp_fme_encabezado_grabar (
      p_n_empresa         IN       NUMBER,
      p_c_ide_tipoarch    IN       CHAR,
      p_c_ide_entidad     IN       CHAR,
      p_c_tipoformato     IN       CHAR,
      p_v_obs             IN       VARCHAR2,
      p_c_tipoorigen      IN       CHAR,
      p_v_rutaarchivo     IN       VARCHAR2,
      p_c_digitacion      IN       CHAR,
      p_n_nrodoctos       IN       NUMBER,
      p_n_mtodoctos       IN       NUMBER,
      p_n_nrodoctosacep   IN       NUMBER,
      p_n_ideremesa       IN OUT   NUMBER,
      p_v_usuario         IN       CHAR,
      p_v_descerror       OUT      VARCHAR2
   );

   PROCEDURE sp_fme_est_digit_grabar (
      p_n_empresa      IN       NUMBER,
      p_n_numremesa    IN       NUMBER,
      p_c_estado_dig   IN       CHAR,
      p_v_usuario      IN       CHAR,
      p_v_descerror    OUT      VARCHAR2
   );

   PROCEDURE sp_fme_rms_para_aceptacion (
      p_n_empresa     IN       NUMBER,
      p_n_numremesa   IN       NUMBER,
      p_v_usuario     IN       CHAR,
      p_v_descerror   OUT      VARCHAR2
   );

   PROCEDURE sp_fme_archivo_borrar (
      p_n_empresa        IN       NUMBER,
      p_n_ide_contrato   IN       NUMBER,
      p_n_ideremesa      IN       NUMBER,
      p_v_usuario        IN       CHAR,
      p_v_descerror      OUT      VARCHAR2
   );

   PROCEDURE sp_fme_registro_archivo_borrar (
      p_n_empresa        IN       NUMBER,
      p_n_ide_contrato   IN       NUMBER,
      p_n_ideremesa      IN       NUMBER,
      p_n_nrolinea       IN       NUMBER,
      p_v_descerror      OUT      VARCHAR2
   );
END pkg_fme_carga_digitacion;
/
CREATE OR REPLACE PACKAGE BODY pkg_fme_carga_digitacion
AS
   PROCEDURE sp_fme_glosa_estructura (
      p_n_empresa        IN       NUMBER,
      p_n_ide_cliente    IN       NUMBER,
      p_n_ide_producto   IN       NUMBER,
      p_n_ide_sucursal   IN       NUMBER,
      p_c_tipo_arch      IN       CHAR,
      p_c_cursor         OUT      micursor,
      p_v_descerror      OUT      VARCHAR2
   )
   AS
      l_c_ide_estado   CHAR (8);
      l_c_tipo_arch    CHAR (8);
      l_n_sucursal     NUMBER;
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_c_ide_estado :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 1,
                                                     'ELI');
      l_c_tipo_arch :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa,
                                                     1,
                                                     6,
                                                     p_c_tipo_arch
                                                    );
      p_v_descerror := '0020 Verificando Numero de Sucursal';

      BEGIN
         SELECT nro_sucursal
           INTO l_n_sucursal
           FROM csbpi.ta_formato_encab_archivo
          WHERE ide_empresa = p_n_empresa
            AND ide_producto = p_n_ide_producto
            AND ide_entidad_cliente = p_n_ide_cliente
            AND nro_sucursal = p_n_ide_sucursal
            AND cod_tipo_archivo = l_c_tipo_arch
            AND cod_estado <> l_c_ide_estado;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_n_sucursal := 0;
      END;

      p_v_descerror := '0030 Buscando glosas de Estructura Archivo Cliente';

      OPEN p_c_cursor FOR
         SELECT cod_tipo_archivo,
                pkg_fme_parametros.fn_fme_glosa_prmtro
                                              (cod_tipo_archivo)
                                                                gls_tipo_arch,
                pkg_fme_parametros.fn_fme_nomcorto_prmtro
                                             (cod_tipo_archivo)
                                                               ncor_tipo_arch,
                cod_tipo_formato,
                pkg_fme_parametros.fn_fme_glosa_prmtro
                                              (cod_tipo_formato)
                                                                gls_tipo_fmto,
                pkg_fme_parametros.fn_fme_nomcorto_prmtro
                                             (cod_tipo_formato)
                                                               ncor_tipo_fmto,
                des_separador
           FROM csbpi.ta_formato_encab_archivo
          WHERE ide_empresa = p_n_empresa
            AND ide_producto = p_n_ide_producto
            AND ide_entidad_cliente = p_n_ide_cliente
            AND nro_sucursal = l_n_sucursal
            AND cod_tipo_archivo = l_c_tipo_arch
            AND cod_estado <> l_c_ide_estado;

      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;

         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_glosa_estructura;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_registros_remesas_ver (
      p_n_empresa         IN       NUMBER,
      p_n_codigo          IN       NUMBER,
      p_n_ide_cliente     IN       NUMBER,
      p_n_ide_producto    IN       NUMBER,
      p_n_ide_contrato    IN       NUMBER,
      p_n_ide_sucursal    IN       NUMBER,
      p_c_tipo_formato    IN       CHAR,
      p_c_origen          IN       CHAR,
      p_c_flgdigitando    IN       CHAR,
      p_d_fecharecepini   IN       DATE,
      p_d_fecharecepfin   IN       DATE,
      p_c_codestado       IN       CHAR,
      p_c_cursor          OUT      micursor,
      p_v_descerror       OUT      VARCHAR2
   )
   AS
      l_c_idunicoestado    CHAR (8);
      l_c_est_rms_sv       CHAR (8);
      l_c_est_rms_ve       CHAR (8);
      l_c_codtipoarchivo   CHAR (8);
      l_c_tipoforma        CHAR (8);
      l_c_estadorem        CHAR (8);
      l_d_fecharecepini    DATE;
      l_d_fecharecepfin    DATE;
   BEGIN
      p_v_descerror := '0005 Rescatando codigo unico estado eliminado';
      l_c_idunicoestado :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 1,
                                                     'ELI');
      p_v_descerror := '0010 Rescatando codigo estado rmsa sin validar';
      l_c_est_rms_sv :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa,
                                                     1,
                                                     12,
                                                     'CSV'
                                                    );
      p_v_descerror :=
                     '0015 Rescatando codigo estado rmsa validada con errores';
      l_c_est_rms_ve :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa,
                                                     1,
                                                     12,
                                                     'CVCE'
                                                    );
      p_v_descerror := '0015 Rescatando codigo tipo archivo entrada';
      l_c_codtipoarchivo :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 6, 'ENT');
      l_c_tipoforma := SUBSTR (p_c_tipo_formato, 1, 8);
      l_c_estadorem := SUBSTR (p_c_codestado, 1, 8);

      IF p_n_codigo > 0
      THEN
         p_v_descerror := '0020 Buscando con Criterio Codigo';

         OPEN p_c_cursor FOR
            SELECT   a.ide_empresa, a.ide_producto,
                     pkg_fme_parametros.fn_fme_glosa_prmtro
                                                 (a.ide_producto)
                                                                gls_producto,
                     a.ide_entidad,
                     pkg_fme_parametros.fn_fme_glosa_prmtro
                                                   (a.ide_entidad)
                                                                 gls_entidad,
                     a.ide_entidad, a.ide_remesa, a.cod_tipo_archivo,
                     pkg_fme_parametros.fn_fme_glosa_prmtro
                                            (a.cod_tipo_archivo)
                                                               desc_tipoarch,
                     a.cod_tipo_formato,
                     pkg_fme_parametros.fn_fme_glosa_prmtro
                                           (a.cod_tipo_formato)
                                                              desc_tipoforma,
                     a.gls_observacion, a.cod_tipo_origen,
                     pkg_fme_parametros.fn_fme_glosa_prmtro
                                             (a.cod_tipo_origen)
                                                               desc_tipoorig,
                     a.gls_ruta_archivo, a.flg_fin_digitacion,
                     a.fch_recepcion, a.nro_documentos, a.mto_documentos,
                     a.nro_doctos_aceptados, a.cod_estado_remesa,
                     pkg_fme_parametros.fn_fme_glosa_prmtro
                                          (a.cod_estado_remesa)
                                                              desc_estadorem,
                     a.fch_hra_mod_estado, a.cod_usr_mod_estado,
                     a.fch_hra_creacion, a.cod_usr_creacion,
                     a.fch_hra_modifica, a.cod_usr_modifica
                FROM csbpi.ta_docto_encab_recep a                         --,
               WHERE a.ide_empresa = p_n_empresa
                 AND a.ide_remesa = p_n_codigo
                 AND a.cod_estado_remesa IN (l_c_est_rms_sv, l_c_est_rms_ve)
                 AND a.cod_tipo_archivo = l_c_codtipoarchivo
            ORDER BY 3;
      ELSIF p_n_ide_cliente > 0
      THEN
         p_v_descerror := '0030 Buscando con Criterio Nombre como Principal';

         --Verificamos si se aplica criterio fecha de recepcion.
         IF p_d_fecharecepini IS NULL AND p_d_fecharecepfin IS NULL
         THEN
            --No aplica Criterio fecha Recepcion.
            p_v_descerror :=
               '0040 Buscando con Criterio Nombre y no Conciderando Criterio fecha recepcion';

            OPEN p_c_cursor FOR
               SELECT   a.ide_empresa, a.ide_producto,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                                 (a.ide_producto)
                                                                gls_producto,
                        a.ide_entidad,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                                   (a.ide_entidad)
                                                                 gls_entidad,
                        a.ide_remesa, a.cod_tipo_archivo,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                            (a.cod_tipo_archivo)
                                                               desc_tipoarch,
                        a.cod_tipo_formato,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                           (a.cod_tipo_formato)
                                                              desc_tipoforma,
                        a.gls_observacion, a.cod_tipo_origen,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                             (a.cod_tipo_origen)
                                                               desc_tipoorig,
                        a.gls_ruta_archivo, a.flg_fin_digitacion,
                        a.fch_recepcion, a.nro_documentos, a.mto_documentos,
                        a.nro_doctos_aceptados, a.cod_estado_remesa,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                          (a.cod_estado_remesa)
                                                              desc_estadorem,
                        a.fch_hra_mod_estado, a.cod_usr_mod_estado,
                        a.fch_hra_creacion, a.cod_usr_creacion,
                        a.fch_hra_modifica, a.cod_usr_modifica
                   FROM csbpi.ta_docto_encab_recep a                      --,
                  WHERE a.ide_empresa = p_n_empresa
                    AND a.cod_estado_remesa IN
                                             (l_c_est_rms_sv, l_c_est_rms_ve)
                    AND a.cod_tipo_formato =
                           DECODE (l_c_tipoforma,
                                   0, a.cod_tipo_formato,
                                   l_c_tipoforma
                                  )
                    AND a.cod_tipo_origen =
                           DECODE (p_c_origen,
                                   0, a.cod_tipo_origen,
                                   p_c_origen
                                  )
                    AND a.flg_fin_digitacion =
                           DECODE (p_c_origen,
                                   0, a.flg_fin_digitacion,
                                   p_c_flgdigitando
                                  )
                    AND a.cod_estado_remesa =
                           DECODE (l_c_estadorem,
                                   0, a.cod_estado_remesa,
                                   l_c_estadorem
                                  )
                    AND a.cod_tipo_archivo = l_c_codtipoarchivo
               ORDER BY 3;
         ELSE
            --Aplica Criterio fecha Recepcion.
            p_v_descerror :=
                         '0050 Estableciendo tope de fechas para la busqueda';

            IF p_d_fecharecepini IS NULL
            THEN
               l_d_fecharecepini := TO_DATE ('01/01/1900', 'DD/MM/YYYY');
            ELSE
               l_d_fecharecepini := p_d_fecharecepini;
            END IF;

            IF p_d_fecharecepfin IS NULL
            THEN
               l_d_fecharecepfin := TO_DATE ('01/01/3000', 'DD/MM/YYYY');
            ELSE
               l_d_fecharecepfin := p_d_fecharecepfin;
            END IF;

            p_v_descerror :=
               '0060 Buscando con Criterio Nombre y Conciderando Criterio fecha recepcion';

            OPEN p_c_cursor FOR
               SELECT   a.ide_empresa, a.ide_producto,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                                 (a.ide_producto)
                                                                 gls_producto,
                        a.ide_entidad,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                                   (a.ide_entidad)
                                                                  gls_entidad,
                        a.ide_remesa, a.cod_tipo_archivo,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                            (a.cod_tipo_archivo)
                                                                desc_tipoarch,
                        a.cod_tipo_formato,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                           (a.cod_tipo_formato)
                                                               desc_tipoforma,
                        a.gls_observacion, a.cod_tipo_origen,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                             (a.cod_tipo_origen)
                                                                desc_tipoorig,
                        a.gls_ruta_archivo, a.flg_fin_digitacion,
                        a.fch_recepcion, a.nro_documentos, a.mto_documentos,
                        a.nro_doctos_aceptados, a.cod_estado_remesa,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                          (a.cod_estado_remesa)
                                                               desc_estadorem,
                        a.fch_hra_mod_estado, a.cod_usr_mod_estado,
                        a.fch_hra_creacion, a.cod_usr_creacion,
                        a.fch_hra_modifica, a.cod_usr_modifica
                   FROM csbpi.ta_docto_encab_recep a
                  WHERE a.ide_empresa = p_n_empresa
                    AND a.cod_estado_remesa IN
                                             (l_c_est_rms_sv, l_c_est_rms_ve)
                    AND a.cod_tipo_formato =
                           DECODE (l_c_tipoforma,
                                   0, a.cod_tipo_formato,
                                   l_c_tipoforma
                                  )
                    AND a.cod_tipo_origen =
                           DECODE (p_c_origen,
                                   0, a.cod_tipo_origen,
                                   p_c_origen
                                  )
                    AND a.flg_fin_digitacion =
                           DECODE (p_c_origen,
                                   0, a.flg_fin_digitacion,
                                   p_c_flgdigitando
                                  )
                    AND a.fch_recepcion >= l_d_fecharecepini
                    AND a.fch_recepcion <= l_d_fecharecepfin
                    AND a.cod_estado_remesa =
                           DECODE (l_c_estadorem,
                                   0, a.cod_estado_remesa,
                                   l_c_estadorem
                                  )
                    AND a.cod_tipo_archivo = l_c_codtipoarchivo
               ORDER BY 3;
         END IF;
      ELSE
         p_v_descerror := '0070 Buscando sin Criterios';

         --Verificamos si se aplica criterio fecha de recepcion.
         IF p_d_fecharecepini IS NULL AND p_d_fecharecepfin IS NULL
         THEN
            --No aplica Criterio fecha Recepcion.
            p_v_descerror :=
               '0080 Buscando sin criterios y no aplicando criterio fecha recepcion';

            OPEN p_c_cursor FOR
               SELECT   a.ide_empresa, a.ide_producto,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                                 (a.ide_producto)
                                                                gls_producto,
                        a.ide_entidad,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                                   (a.ide_entidad)
                                                                 gls_entidad,
                        a.ide_remesa, a.cod_tipo_archivo,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                            (a.cod_tipo_archivo)
                                                               desc_tipoarch,
                        a.cod_tipo_formato,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                           (a.cod_tipo_formato)
                                                              desc_tipoforma,
                        a.gls_observacion, a.cod_tipo_origen,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                             (a.cod_tipo_origen)
                                                               desc_tipoorig,
                        a.gls_ruta_archivo, a.flg_fin_digitacion,
                        a.fch_recepcion, a.nro_documentos, a.mto_documentos,
                        a.nro_doctos_aceptados, a.cod_estado_remesa,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                          (a.cod_estado_remesa)
                                                              desc_estadorem,
                        a.fch_hra_mod_estado, a.cod_usr_mod_estado,
                        a.fch_hra_creacion, a.cod_usr_creacion,
                        a.fch_hra_modifica, a.cod_usr_modifica
                   FROM csbpi.ta_docto_encab_recep a                      --,
                  WHERE a.ide_empresa = p_n_empresa
                    AND a.cod_estado_remesa IN
                                             (l_c_est_rms_sv, l_c_est_rms_ve)
                    AND a.cod_tipo_formato =
                           DECODE (l_c_tipoforma,
                                   0, a.cod_tipo_formato,
                                   l_c_tipoforma
                                  )
                    AND a.cod_tipo_origen =
                           DECODE (p_c_origen,
                                   0, a.cod_tipo_origen,
                                   p_c_origen
                                  )
                    AND a.flg_fin_digitacion =
                           DECODE (p_c_origen,
                                   0, a.flg_fin_digitacion,
                                   p_c_flgdigitando
                                  )
                    AND a.cod_estado_remesa =
                           DECODE (l_c_estadorem,
                                   0, a.cod_estado_remesa,
                                   l_c_estadorem
                                  )
                    AND a.cod_tipo_archivo = l_c_codtipoarchivo
               ORDER BY 3;
         ELSE
            --Aplica Criterio fecha Recepcion.
            p_v_descerror :=
                         '0090 Estableciendo tope de fechas para la busqueda';

            IF p_d_fecharecepini IS NULL
            THEN
               l_d_fecharecepini := TO_DATE ('01/01/1900', 'DD/MM/YYYY');
            ELSE
               l_d_fecharecepini := p_d_fecharecepini;
            END IF;

            IF p_d_fecharecepfin IS NULL
            THEN
               l_d_fecharecepfin := TO_DATE ('01/01/2999', 'DD/MM/YYYY');
            ELSE
               l_d_fecharecepfin := p_d_fecharecepfin;
            END IF;

            p_v_descerror :=
               '0100 Buscando sin criterios y aplicando criterio fecha recepcion';

            OPEN p_c_cursor FOR
               SELECT   a.ide_empresa, a.ide_producto,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                                 (a.ide_producto)
                                                                 gls_producto,
                        a.ide_entidad,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                                   (a.ide_entidad)
                                                                  gls_entidad,
                        a.ide_remesa, a.cod_tipo_archivo,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                            (a.cod_tipo_archivo)
                                                                desc_tipoarch,
                        a.cod_tipo_formato,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                           (a.cod_tipo_formato)
                                                               desc_tipoforma,
                        a.gls_observacion, a.cod_tipo_origen,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                             (a.cod_tipo_origen)
                                                                desc_tipoorig,
                        a.gls_ruta_archivo, a.flg_fin_digitacion,
                        a.fch_recepcion, a.nro_documentos, a.mto_documentos,
                        a.nro_doctos_aceptados, a.cod_estado_remesa,
                        pkg_fme_parametros.fn_fme_glosa_prmtro
                                          (a.cod_estado_remesa)
                                                               desc_estadorem,
                        a.fch_hra_mod_estado, a.cod_usr_mod_estado,
                        a.fch_hra_creacion, a.cod_usr_creacion,
                        a.fch_hra_modifica, a.cod_usr_modifica
                   FROM csbpi.ta_docto_encab_recep a                       --,
                  WHERE a.ide_empresa = p_n_empresa
                    AND a.cod_estado_remesa IN
                                             (l_c_est_rms_sv, l_c_est_rms_ve)
                    AND a.cod_tipo_formato =
                           DECODE (l_c_tipoforma,
                                   0, a.cod_tipo_formato,
                                   l_c_tipoforma
                                  )
                    AND a.cod_tipo_origen =
                           DECODE (p_c_origen,
                                   0, a.cod_tipo_origen,
                                   p_c_origen
                                  )
                    AND a.flg_fin_digitacion =
                           DECODE (p_c_origen,
                                   0, a.flg_fin_digitacion,
                                   p_c_flgdigitando
                                  )
                    AND a.fch_recepcion >= l_d_fecharecepini
                    AND a.fch_recepcion <= l_d_fecharecepfin
                    AND a.cod_estado_remesa =
                           DECODE (l_c_estadorem,
                                   0, a.cod_estado_remesa,
                                   l_c_estadorem
                                  )
                    AND a.cod_tipo_archivo = l_c_codtipoarchivo
               ORDER BY 3;
         END IF;
      END IF;

      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;

         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_registros_remesas_ver;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_errores_remesas_ver (
      p_n_empresa     IN       NUMBER,
      p_n_numremesa   IN       NUMBER,
      p_c_cursor      OUT      micursor,
      p_v_descerror   OUT      VARCHAR2
   )
   AS
   BEGIN
      p_v_descerror := '0000 Inicio';

      OPEN p_c_cursor FOR
         SELECT   a.ide_empresa, a.ide_remesa, a.nro_linea, a.cod_corredor,
                  a.nom_corredor, a.rut_cliente, a.nom_cliente,
                  a.gls_cta_ctdia_clte, a.gls_nemotecnico, a.mnt_nominal,
                  a.mnt_precio, a.mnt_monto, a.mnt_drcho_bolsa, a.mnt_gastos,
                  a.mnt_comision, a.mnt_total, a.nro_factura, a.fch_fecha,
                  a.gls_tipo_liquidac, a.mrc_tipo_opera,
				  a.cod_clasif_riesgo_afp, a.duracion_dias_afp    , a.cod_instrumento_afp  ,
				  a.cod_moneda_afp       ,a.mnt_factor_precio_afp,  a.tasa_mercado_afp     ,
				  a.tasa_adicional_afp   ,
                                                        /*a.nro_linea, a.rut_emisor, a.ide_dv_emisor, a.nom_emisor,
                                                             a.rut_receptor, a.ide_dv_receptor, a.nom_receptor,
                                                             a.gls_direccion, b.gls_descripcion gls_comuna,
                                                             a.gls_ciudad, c.gls_descripcion cod_pais, a.gls_cod_postal,
                                                             a.gls_telefono, a.gls_num_fax, a.gls_casilla,
                                                             g.gls_descripcion cod_tipo_dcto, a.nro_documento,
                                                             a.nro_cuota, d.gls_descripcion cod_banco,
                                                             e.gls_descripcion cod_plaza, a.gls_ctacte,
                                                             pkg_fme_parametros.fn_fme_glosa_prmtro
                                                                                        (a.cod_forma_pago)
                                                                                                         cod_forma_pago,
                                                             a.mnt_documento, f.gls_descripcion cod_moneda,
                                                             a.fch_emision, a.fch_vencimiento, a.fch_pago,
                                                             a.gls_observacion, a.gls_referencia_1, a.gls_referencia_2,
                                                             a.gls_referencia_3, a.mrc_clasif_emisor, a.mnt_original,
                                                             a.mnt_iva, a.mnt_neto, a.mnt_exento,*/
                                                        h.cod_error,
                  pkg_fme_parametros.fn_fme_glosa_prmtro
                                                      (h.cod_error)
                                                                  desc_error
             FROM csbpi.ta_docto_detalle_recep a,
                                                 /*CSBPI.ta_tc_comuna b,
                                                 CSBPI.ta_tc_pais c,
                                                 CSBPI.ta_tc_banco d,
                                                 CSBPI.ta_tc_plaza e,
                                                 CSBPI.ta_moneda f,
                                                 CSBPI.ta_tipo_documento g,*/
                                                 csbpi.ta_remesa_error h
            WHERE a.ide_empresa = p_n_empresa
              AND a.ide_remesa = p_n_numremesa
              --AND TRIM (a.gls_comuna) = TRIM (b.cod_tc_comuna(+))
              --AND TRIM (a.cod_pais) = TRIM (c.cod_tc_pais(+))
              --AND TRIM (a.cod_banco) = TRIM (d.cod_tc_banco(+))
              --AND TRIM (a.cod_plaza) = TRIM (e.cod_tc_plaza(+))
              --AND TRIM (a.cod_moneda) = TRIM (f.cod_tc_moneda(+))
              --AND TRIM (a.cod_tipo_dcto) = TRIM (g.ide_tipo_doc(+))
              --AND a.ide_empresa = g.ide_empresa
              AND a.ide_empresa = h.ide_empresa
              AND a.ide_remesa = h.ide_remesa
              AND a.nro_linea = h.nro_linea
         ORDER BY a.nro_linea;

      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;

         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_errores_remesas_ver;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_estructura_cliente_ver (
      p_n_empresa        IN       NUMBER,
      p_n_ide_cliente    IN       NUMBER,
      p_n_ide_producto   IN       NUMBER,
      p_n_ide_sucursal   IN       NUMBER,
      p_c_cursor         OUT      micursor,
      p_v_descerror      OUT      VARCHAR2
   )
   AS
      l_c_tipo_archivo      CHAR (8);
      l_c_tipo_formato      CHAR (8);
      l_v_tipo_formato      VARCHAR2 (40);
      l_v_tipo_formato_nc   VARCHAR2 (20);
      l_c_idunicoestado     CHAR (8);
      l_n_correncab         NUMBER;
      l_n_sucursal          NUMBER;
      l_c_separador         CHAR (10);
	  l_c_sepdecimal		CHAR (1);
   BEGIN
      p_v_descerror := '0000 Inicio';
      p_v_descerror := '0010 Rescatando codigo unico estado eliminado';
      l_c_idunicoestado :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 1,
                                                     'ELI');
      l_c_tipo_archivo :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 6,
                                                     'ENT');
      p_v_descerror := '0015 Verificando Numero de Sucursal';

      BEGIN
         SELECT nro_sucursal
           INTO l_n_sucursal
           FROM csbpi.ta_formato_encab_archivo
          WHERE ide_empresa = p_n_empresa
            AND ide_producto = p_n_ide_producto
            AND ide_entidad_cliente = p_n_ide_cliente
            AND nro_sucursal = p_n_ide_sucursal
            AND cod_tipo_archivo = l_c_tipo_archivo
            AND cod_estado <> l_c_idunicoestado;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_n_sucursal := 0;
      END;

      p_v_descerror :=
                     '0020 Buscando Correlativo de Estructura Archivo Cliente';

      BEGIN
         SELECT nro_corr_encab, cod_tipo_formato, des_separador, des_simbolo_decimal 
           INTO l_n_correncab, l_c_tipo_formato, l_c_separador, l_c_sepdecimal
           FROM csbpi.ta_formato_encab_archivo
          WHERE ide_empresa = p_n_empresa
            AND ide_producto = p_n_ide_producto
            AND ide_entidad_cliente = p_n_ide_cliente
            AND nro_sucursal = l_n_sucursal
            AND cod_tipo_archivo = l_c_tipo_archivo
            AND cod_estado <> l_c_idunicoestado
            AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_n_correncab := 0;
            l_c_tipo_formato := 0;
            l_c_separador := 0;
      END;

      l_v_tipo_formato :=
                     pkg_fme_parametros.fn_fme_glosa_prmtro (l_c_tipo_formato);
      l_v_tipo_formato_nc :=
                  pkg_fme_parametros.fn_fme_nomcorto_prmtro (l_c_tipo_formato);

      OPEN p_c_cursor FOR
         SELECT   a.ide_empresa, a.nro_corr_encab, a.cod_ide_seccion,
                  b.nom_corto, a.cod_ide_campo, d.gls_descripcion,
                  pkg_fme_parametros.fn_fme_nomcorto_prmtro
                                               (a.cod_ide_campo)
                                                                nom_cto_campo,
                  a.nro_corr_detalle, a.nro_corr_campo, c.gls_campo,
                  c.flg_obligatorio, a.cod_tipo_campo,
                  pkg_fme_parametros.fn_fme_nomcorto_prmtro
                                            (a.cod_tipo_campo)
                                                              nom_cto_tip_cpo,
                  a.vlr_posicion, a.vlr_precision, a.vlr_decimales,
                  a.mrc_alineacion, a.mrc_relleno, a.flg_convertir,
                  a.gls_formato, l_v_tipo_formato tipo_formato,
                  l_v_tipo_formato_nc nom_cto_tip_fmto,
                  l_c_tipo_formato ide_unico_tip_fmto,
                  l_c_separador separador, l_c_sepdecimal sepdecimal, a.cod_estado, a.fch_hra_mod_estado,
                  a.cod_usr_mod_estado, a.mrc_bloqueo, a.fch_hra_bloqueo,
                  a.cod_usr_bloqueo, a.fch_hra_creacion, a.cod_usr_creacion,
                  a.fch_hra_modifica, a.cod_usr_modifica
             FROM csbpi.ta_formato_detalle_archivo a,
                  csbpi.ta_parametro b,
                  csbpi.ta_campo_archivo_remesa c,
                  csbpi.ta_parametro d
            WHERE a.ide_empresa = p_n_empresa
              AND a.nro_corr_encab = l_n_correncab
              AND a.cod_ide_seccion = b.ide_unico_param
              AND a.nro_corr_campo = c.nro_corr_campo
              AND a.cod_ide_campo = d.ide_unico_param(+)
              AND a.cod_estado <> l_c_idunicoestado
         ORDER BY a.cod_ide_seccion, a.vlr_posicion;

      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;

         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_estructura_cliente_ver;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_archivo_cliente_ver (
      p_n_empresa         IN       NUMBER,
      p_n_codigo          IN       NUMBER,
      p_n_contratomarco   IN       NUMBER,
      p_n_tiposeccion     IN       NUMBER,
      p_n_nrolinea        IN       NUMBER,
      p_c_cursor          OUT      micursor,
      p_v_descerror       OUT      VARCHAR2
   )
   AS
      l_c_idunicoestado   CHAR (8);
   --p_n_TipoSeccion = 1 Encabezado y Total.
   --                = 2 Detalle.
   BEGIN
      p_v_descerror := '0000 Inicio';
      p_v_descerror := '0010 Rescatando codigo unico estado';
      l_c_idunicoestado :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa,
                                                     1,
                                                     12,
                                                     'ELI'
                                                    );

      IF p_n_tiposeccion = 1
      THEN
         p_v_descerror :=
            '0020 Buscando Archivo Cliente Correspondiente al Encabezado y Total';

         OPEN p_c_cursor FOR
            SELECT ide_empresa,
                               --ide_contrato_marco,
                               ide_remesa,
                                          --nro_sucursal_cli,
                                          cod_tipo_archivo, cod_tipo_formato,
                   gls_observacion, cod_tipo_origen, gls_ruta_archivo,
                   flg_fin_digitacion, fch_recepcion, nro_documentos,
                   mto_documentos, nro_doctos_aceptados, cod_estado_remesa,
                   fch_hra_mod_estado, cod_usr_mod_estado, fch_hra_creacion,
                   cod_usr_creacion, fch_hra_modifica, cod_usr_modifica
              FROM csbpi.ta_docto_encab_recep
             WHERE ide_empresa = p_n_empresa
                                            --AND ide_contrato_marco = p_n_contratomarco
                   AND ide_remesa = p_n_codigo;
      ELSE
         IF p_n_nrolinea <= 0
         THEN
            p_v_descerror :=
               '0030 Buscando todo el Archivo Cliente Correspondiente al Detalle';

            OPEN p_c_cursor FOR
               SELECT   a.ide_empresa, a.ide_remesa, a.nro_linea,
                        a.cod_corredor, a.nom_corredor, a.rut_cliente,
                        a.nom_cliente, a.gls_cta_ctdia_clte,
                        a.gls_nemotecnico, a.mnt_nominal, a.mnt_precio,
                        a.mnt_monto, a.mnt_drcho_bolsa, a.mnt_gastos,
                        a.mnt_comision, a.mnt_total, a.nro_factura,
                        a.fch_fecha, a.gls_tipo_liquidac, a.mrc_tipo_opera,
                        a.cod_clasif_riesgo_afp, a.duracion_dias_afp    , a.cod_instrumento_afp  ,
				  a.cod_moneda_afp       ,a.mnt_factor_precio_afp,  a.tasa_mercado_afp     ,
				  a.tasa_adicional_afp   ,
                        /*a.ide_empresa, a.ide_contrato_marco, a.ide_remesa,
                           a.nro_linea, a.rut_emisor, a.ide_dv_emisor,
                           a.nom_emisor, a.rut_receptor, a.ide_dv_receptor,
                           a.nom_receptor, a.gls_direccion,
                           b.gls_descripcion gls_comuna, a.gls_ciudad,
                           c.gls_descripcion cod_pais, a.gls_cod_postal,
                           a.gls_telefono, a.gls_num_fax, a.gls_casilla,
                           g.gls_descripcion cod_tipo_dcto, a.nro_documento,
                           a.nro_cuota, d.gls_descripcion cod_banco,
                           e.gls_descripcion cod_plaza, a.gls_ctacte,
                           pkg_fme_parametros.fn_fme_glosa_prmtro
                                                (a.cod_forma_pago)
                                                                 cod_forma_pago,
                           a.mnt_documento, f.gls_descripcion cod_moneda,
                           a.fch_emision, a.fch_vencimiento, a.fch_pago,
                           a.gls_observacion, a.gls_referencia_1,
                           a.gls_referencia_2, a.gls_referencia_3,
                           a.mrc_clasif_emisor, a.mnt_original, a.mnt_iva,
                           a.mnt_neto, a.mnt_exento,*/
                        a.cod_estado_dcto, a.fch_hra_mod_estado,
                        a.cod_usr_mod_estado, a.fch_hra_creacion,
                        a.cod_usr_creacion, a.fch_hra_modifica,
                        a.cod_usr_modifica
                   FROM csbpi.ta_docto_detalle_recep a                    --,
                  --CSBPI.ta_tc_comuna b,
                  --CSBPI.ta_tc_pais c,
                  --CSBPI.ta_tc_banco d,
                  --CSBPI.ta_tc_plaza e,
                  --CSBPI.ta_moneda f,
                  --CSBPI.ta_tipo_documento g
               WHERE     /*TRIM (a.gls_comuna) = TRIM (b.cod_tc_comuna(+))
                     --AND TRIM (a.cod_pais) = TRIM (c.cod_tc_pais(+))
                     --AND TRIM (a.cod_banco) = TRIM (d.cod_tc_banco(+))
                     --AND TRIM (a.cod_plaza) = TRIM (e.cod_tc_plaza(+))
                     --AND TRIM (a.cod_moneda) = TRIM (f.cod_tc_moneda(+))
                     --AND TRIM (a.cod_tipo_dcto) = TRIM (g.ide_tipo_doc(+))
                     --AND a.ide_empresa = g.ide_empresa
                     AND*/ a.ide_empresa = p_n_empresa
                    --AND a.ide_contrato_marco = p_n_contratomarco
                    AND a.ide_remesa = p_n_codigo
                    AND a.cod_estado_dcto <> l_c_idunicoestado
               ORDER BY a.nro_linea;
         ELSE
            p_v_descerror :=
               '0040 Buscando un registro del  Archivo Cliente Correspondiente al Detalle';

            OPEN p_c_cursor FOR
               SELECT ide_empresa, ide_remesa, nro_linea, cod_corredor,
                      nom_corredor, rut_cliente, nom_cliente,
                      gls_cta_ctdia_clte, gls_nemotecnico, mnt_nominal,
                      mnt_precio, mnt_monto, mnt_drcho_bolsa, mnt_gastos,
                      mnt_comision, mnt_total, nro_factura, fch_fecha,
                      gls_tipo_liquidac, mrc_tipo_opera,
					  cod_clasif_riesgo_afp, duracion_dias_afp    , cod_instrumento_afp  ,
				  cod_moneda_afp       ,mnt_factor_precio_afp,  tasa_mercado_afp     ,
				  tasa_adicional_afp   ,
                                                        /*ide_empresa, ide_contrato_marco, ide_remesa, nro_linea,
                                                          rut_emisor, ide_dv_emisor, nom_emisor, rut_receptor,
                                                          ide_dv_receptor, nom_receptor, gls_direccion,
                                                          gls_comuna, gls_ciudad, cod_pais, gls_cod_postal,
                                                          gls_telefono, gls_num_fax, gls_casilla, cod_tipo_dcto,
                                                          nro_documento, nro_cuota, cod_banco, cod_plaza,
                                                          gls_ctacte, cod_forma_pago, mnt_documento, cod_moneda,
                                                          fch_emision, fch_vencimiento, fch_pago,
                                                          gls_observacion, gls_referencia_1, gls_referencia_2,
                                                          gls_referencia_3, mrc_clasif_emisor, mnt_original, mnt_iva,
                                                          mnt_neto, mnt_exento, */
                                                        cod_estado_dcto,
                      fch_hra_mod_estado, cod_usr_mod_estado,
                      fch_hra_creacion, cod_usr_creacion, fch_hra_modifica,
                      cod_usr_modifica
                 FROM csbpi.ta_docto_detalle_recep
                WHERE ide_empresa = p_n_empresa
                  --   AND ide_contrato_marco = p_n_contratomarco
                  AND ide_remesa = p_n_codigo
                  AND nro_linea = p_n_nrolinea
                  AND cod_estado_dcto <> l_c_idunicoestado;
         END IF;
      END IF;

      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;

         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_archivo_cliente_ver;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_registro_archivo_grabar (
      p_n_empresa                 IN       NUMBER,
      p_n_ideremesa               IN       NUMBER,
      p_n_nrolinea                IN       NUMBER,
      p_n_cod_corredor            IN       NUMBER,
      p_v_nom_corredor            IN       VARCHAR2,
      p_v_rut_cliente             IN       VARCHAR2,
      p_v_nom_cliente             IN       VARCHAR2,
      p_v_gls_cta_ctdia_clte      IN       VARCHAR2,
      p_v_nemotecnico             IN       VARCHAR2,
      p_n_mnt_nominal             IN       NUMBER,
      p_n_mnt_precio              IN       NUMBER,
      p_n_mnt_monto               IN       NUMBER,
      p_n_mnt_drcho_bolsa         IN       NUMBER,
      p_n_mnt_gastos              IN       NUMBER,
      p_n_mnt_comision            IN       NUMBER,
      p_n_mnt_total               IN       NUMBER,
      p_v_nro_factura             IN       VARCHAR2,
      p_d_fch_fecha               IN       DATE,
      p_v_gls_tipo_liquidac       IN       VARCHAR2,
      p_c_mrc_tipo_opera          IN       CHAR,
      p_v_cod_clasif_riesgo_afp   IN       VARCHAR2,
      p_n_duracion_dias_afp       IN       NUMBER,
      p_v_cod_instrumento_afp     IN       VARCHAR2,
      p_v_cod_moneda_afp          IN       VARCHAR2,
      p_n_mnt_factor_precio_afp   IN       NUMBER,
      p_n_tasa_mercado_afp        IN       NUMBER,
      p_n_tasa_adicional_afp      IN       NUMBER,
      p_v_usuario                 IN       CHAR,
      p_v_descerror               OUT      VARCHAR2
   )
   AS
      l_n_numerolinea        NUMBER (10);
      l_n_cantreg            NUMBER (10);
      l_n_mtodoctos          NUMBER (19, 4);
      l_d_fechahora          DATE;
      l_c_idunicoestadoing   CHAR (8);
      l_c_idunicoestadoeli   CHAR (8);
      l_b_swichingresa       BOOLEAN;
      l_n_idetipodocto       NUMBER (10);
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      p_v_descerror := '0010 Rescatando codigo unico estado Ingresado';
      l_c_idunicoestadoing :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 15,
                                                     'ING');
      p_v_descerror := '0020 Rescatando codigo unico estado';
      l_c_idunicoestadoeli :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 15,
                                                     'ELI');
      p_v_descerror := '0040 Actualizando Registro del Archivo Cliente';


      UPDATE csbpi.ta_docto_detalle_recep
         SET cod_corredor = p_n_cod_corredor,
             nom_corredor = p_v_nom_corredor,
             rut_cliente = p_v_rut_cliente,
             nom_cliente = p_v_nom_cliente,
             gls_cta_ctdia_clte = p_v_gls_cta_ctdia_clte,
			 gls_nemotecnico=p_v_nemotecnico,
             mnt_nominal = p_n_mnt_nominal,
             mnt_precio = p_n_mnt_precio,
             mnt_monto = p_n_mnt_monto,
             mnt_drcho_bolsa = p_n_mnt_drcho_bolsa,
             mnt_gastos = p_n_mnt_gastos,
             mnt_comision = p_n_mnt_comision,
             mnt_total = p_n_mnt_total,
             nro_factura = p_v_nro_factura,
             fch_fecha = p_d_fch_fecha,
             gls_tipo_liquidac = p_v_gls_tipo_liquidac,
             mrc_tipo_opera = p_c_mrc_tipo_opera,
			 cod_clasif_riesgo_afp=p_v_cod_clasif_riesgo_afp, 
			 duracion_dias_afp    =p_n_duracion_dias_afp, 
			 cod_instrumento_afp  =p_v_cod_instrumento_afp,
			 cod_moneda_afp       =p_v_cod_moneda_afp,
			 mnt_factor_precio_afp=p_n_mnt_factor_precio_afp,  
			 tasa_mercado_afp     =p_n_tasa_mercado_afp,
			 tasa_adicional_afp   =p_n_tasa_adicional_afp			 
       WHERE ide_empresa = p_n_empresa
         AND ide_remesa = p_n_ideremesa
         AND nro_linea = p_n_nrolinea;

      IF SQL%ROWCOUNT <= 0
      THEN
         l_b_swichingresa := TRUE;
      ELSE
         l_b_swichingresa := FALSE;
      END IF;

      p_v_descerror := '0050 Buscando Monto total doctos y Cantidad de doctos';

      BEGIN
         SELECT SUM (mnt_total), COUNT (*)
           INTO l_n_mtodoctos, l_n_cantreg
           FROM csbpi.ta_docto_detalle_recep
          WHERE ide_empresa = p_n_empresa
            AND ide_remesa = p_n_ideremesa
            AND cod_estado_dcto <> l_c_idunicoestadoeli;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_n_mtodoctos := 0;
            l_n_cantreg := 0;
      END;

      IF l_b_swichingresa = TRUE
      THEN
         p_v_descerror := '0060 Buscando siguiente Numero de Linea';

         BEGIN
            SELECT MAX (nro_linea)
              INTO l_n_numerolinea
              FROM csbpi.ta_docto_detalle_recep
             WHERE ide_empresa = p_n_empresa
                   AND ide_remesa = p_n_ideremesa;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_n_numerolinea := 0;
         END;

         IF l_n_numerolinea IS NULL
         THEN
            l_n_numerolinea := 0;
         END IF;

         l_n_numerolinea := l_n_numerolinea + 1;
         p_v_descerror :=
               '0070 Creando nuevo registro en el archivo cliente ('
            || l_n_numerolinea
            || ')';

			
         INSERT INTO csbpi.ta_docto_detalle_recep
                     (ide_empresa, ide_remesa, nro_linea,
                      cod_corredor, nom_corredor, rut_cliente,
                      nom_cliente, gls_cta_ctdia_clte,
					  gls_nemotecnico,
                      mnt_nominal, mnt_precio, mnt_monto,
                      mnt_drcho_bolsa, mnt_gastos, mnt_comision,
                      mnt_total, nro_factura, fch_fecha,
                      gls_tipo_liquidac, mrc_tipo_opera,
					cod_clasif_riesgo_afp, 
					duracion_dias_afp     ,
					cod_instrumento_afp  ,
					cod_moneda_afp       ,
					mnt_factor_precio_afp , 
					tasa_mercado_afp     ,
					tasa_adicional_afp   	,		 
                      cod_estado_dcto, fch_hra_mod_estado,
                      cod_usr_mod_estado, fch_hra_creacion, cod_usr_creacion
                     )
              VALUES (p_n_empresa, p_n_ideremesa, l_n_numerolinea,
                      p_n_cod_corredor, p_v_nom_corredor, p_v_rut_cliente,
                      p_v_nom_cliente, p_v_gls_cta_ctdia_clte,
					  p_v_nemotecnico,
                      p_n_mnt_nominal, p_n_mnt_precio, p_n_mnt_monto,
                      p_n_mnt_drcho_bolsa, p_n_mnt_gastos, p_n_mnt_comision,
                      p_n_mnt_total, p_v_nro_factura, p_d_fch_fecha,
                      p_v_gls_tipo_liquidac, p_c_mrc_tipo_opera,
					p_v_cod_clasif_riesgo_afp, 
					p_n_duracion_dias_afp, 
					p_v_cod_instrumento_afp,
					p_v_cod_moneda_afp,
					p_n_mnt_factor_precio_afp,  
					p_n_tasa_mercado_afp,
					p_n_tasa_adicional_afp,			 
                      l_c_idunicoestadoing, l_d_fechahora,
                      p_v_usuario, l_d_fechahora, p_v_usuario
                     );

         l_n_cantreg := l_n_cantreg + 1;
         l_n_mtodoctos := l_n_mtodoctos + p_n_mnt_total;
      END IF;

      p_v_descerror := '0080 Actualizamos encabezado';

      UPDATE csbpi.ta_docto_encab_recep
         SET nro_documentos = l_n_cantreg,
             mto_documentos = l_n_mtodoctos
       WHERE ide_empresa = p_n_empresa
             AND ide_remesa = p_n_ideremesa;
	  
	  COMMIT;		 
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_registro_archivo_grabar;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_encabezado_grabar (
      p_n_empresa         IN       NUMBER,
      p_c_ide_tipoarch    IN       CHAR,
      p_c_ide_entidad     IN       CHAR,
      p_c_tipoformato     IN       CHAR,
      p_v_obs             IN       VARCHAR2,
      p_c_tipoorigen      IN       CHAR,
      p_v_rutaarchivo     IN       VARCHAR2,
      p_c_digitacion      IN       CHAR,
      p_n_nrodoctos       IN       NUMBER,
      p_n_mtodoctos       IN       NUMBER,
      p_n_nrodoctosacep   IN       NUMBER,
      p_n_ideremesa       IN OUT   NUMBER,
      p_v_usuario         IN       CHAR,
      p_v_descerror       OUT      VARCHAR2
   )
   AS
      l_c_idunicoestadoing   CHAR (8);
      l_c_tipo_archivo       CHAR (8);
      l_d_fechahora          DATE;
      l_c_tipoorigen         CHAR (8);
      l_n_ide                NUMBER (10);
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      p_v_descerror := '0010 Rescatando codigo unico estado Ingresado';
      l_c_idunicoestadoing :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 12,
                                                     'CSV');
      l_c_tipo_archivo :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 6, 'ENT');
      l_c_tipoorigen :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa,
                                                     1,
                                                     13,
                                                     p_c_tipoorigen
                                                    );
      p_v_descerror := '0030 Siguiente Ide de secuencia Producto';

      SELECT csbpi.sc_fme_remesa.NEXTVAL
        INTO l_n_ide
        FROM DUAL;

      p_v_descerror := '0020 Creando nuevo registro de Remesa';

      INSERT INTO csbpi.ta_docto_encab_recep
                  (ide_empresa, ide_producto, ide_entidad, ide_remesa,
                   cod_tipo_archivo, cod_tipo_formato, gls_observacion,
                   cod_tipo_origen, gls_ruta_archivo, flg_fin_digitacion,
                   fch_recepcion, nro_documentos, mto_documentos,
                   nro_doctos_aceptados, cod_estado_remesa,
                   fch_hra_mod_estado, cod_usr_mod_estado, fch_hra_creacion,
                   cod_usr_creacion, fch_hra_modifica, cod_usr_modifica
                  )
           VALUES (p_n_empresa, p_c_ide_tipoarch, p_c_ide_entidad, l_n_ide,
                   l_c_tipo_archivo, p_c_tipoformato, p_v_obs,
                   l_c_tipoorigen, p_v_rutaarchivo, p_c_digitacion,
                   l_d_fechahora, p_n_nrodoctos, p_n_mtodoctos,
                   p_n_nrodoctosacep, l_c_idunicoestadoing,
                   l_d_fechahora, p_v_usuario, l_d_fechahora,
                   p_v_usuario, NULL, NULL
                  );

      p_n_ideremesa := l_n_ide;
	  COMMIT;
	  
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_encabezado_grabar;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_est_digit_grabar (
      p_n_empresa      IN       NUMBER,
      p_n_numremesa    IN       NUMBER,
      p_c_estado_dig   IN       CHAR,
      p_v_usuario      IN       CHAR,
      p_v_descerror    OUT      VARCHAR2
   )
   AS
      l_d_fechahora   DATE;
   BEGIN
      p_v_descerror := '0010 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      p_v_descerror := '0020 Actualizando estado de Digitacisn';

      UPDATE csbpi.ta_docto_encab_recep
         SET flg_fin_digitacion = p_c_estado_dig,
             fch_hra_modifica = l_d_fechahora,
             cod_usr_modifica = p_v_usuario
       WHERE ide_empresa = p_n_empresa AND ide_remesa = p_n_numremesa;
	  
	  COMMIT; 
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_est_digit_grabar;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_rms_para_aceptacion (
      p_n_empresa     IN       NUMBER,
      p_n_numremesa   IN       NUMBER,
      p_v_usuario     IN       CHAR,
      p_v_descerror   OUT      VARCHAR2
   )
   AS
      l_d_fechahora   DATE;
      l_c_est_ing     CHAR (8);
      l_ide_remesa    NUMBER;
   BEGIN
      p_v_descerror := '0010 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      p_v_descerror := '0020 Rescatando codigo estado ingresado';
      l_c_est_ing :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 15,
                                                     'ING');
      p_v_descerror := '0030 Buscando documentos en estado de ingresado';

      BEGIN
         SELECT ide_remesa
           INTO l_ide_remesa
           FROM csbpi.ta_docto_detalle_recep
          WHERE ide_empresa = p_n_empresa
            AND ide_remesa = p_n_numremesa
            AND cod_estado_dcto = l_c_est_ing
            AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_ide_remesa := 0;
      END;

      IF l_ide_remesa = 0
      THEN
         p_v_descerror :=
               '0040 Actualizando esdtado de remesa a Aceptacion con Errores';
         pkg_fme_valida_remesa.sp_fme_act_est_remesa (p_n_empresa,
                                                      p_n_numremesa,
                                                      3,
                                                      p_v_usuario,
                                                      p_v_descerror
                                                     );
      ELSE
         p_v_descerror :=
            '0050 La Remesa no puede ser enviada a aceptacisn. Existen Documentos sin Validar';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_rms_para_aceptacion;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_archivo_borrar (
      p_n_empresa        IN       NUMBER,
      p_n_ide_contrato   IN       NUMBER,
      p_n_ideremesa      IN       NUMBER,
      p_v_usuario        IN       CHAR,
      p_v_descerror      OUT      VARCHAR2
   )
   AS
      l_d_fechahora       DATE;
      l_c_idunicoestado   CHAR (8);
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      p_v_descerror := '0010 Rescatando codigo unico estado';
      l_c_idunicoestado :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 12,
                                                     'ELI');
      p_v_descerror := '0020 Marcando el Encabezado con estado eliminado';

      UPDATE csbpi.ta_docto_encab_recep
         SET cod_estado_remesa = l_c_idunicoestado,
             fch_hra_mod_estado = l_d_fechahora,
             cod_usr_mod_estado = p_v_usuario
       WHERE ide_empresa = p_n_empresa AND ide_remesa = p_n_ideremesa;

      p_v_descerror := '0030 Eliminando todo el detalle de la remesa';

      DELETE FROM csbpi.ta_docto_detalle_recep
            WHERE ide_empresa = p_n_empresa AND ide_remesa = p_n_ideremesa;
	  
	  COMMIT;
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_archivo_borrar;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_registro_archivo_borrar (
      p_n_empresa        IN       NUMBER,
      p_n_ide_contrato   IN       NUMBER,
      p_n_ideremesa      IN       NUMBER,
      p_n_nrolinea       IN       NUMBER,
      p_v_descerror      OUT      VARCHAR2
   )
   AS
      l_c_idunicoestado   CHAR (8);
      l_n_cantreg         NUMBER (10);
      l_n_mtodoctos       NUMBER (19, 4);
   BEGIN
      p_v_descerror := '0000 Inicio';
      p_v_descerror := '0010 Rescatando codigo unico estado';
      l_c_idunicoestado :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa,
                                                     1,
                                                     15,
                                                     'ELI'
                                                    );
      p_v_descerror := '0020 Marcando el registro con estado eliminado';

      UPDATE csbpi.ta_docto_detalle_recep
         SET cod_estado_dcto = l_c_idunicoestado
       WHERE ide_empresa = p_n_empresa
         --AND ide_contrato_marco = p_n_ide_contrato
         AND ide_remesa = p_n_ideremesa
         AND nro_linea = p_n_nrolinea;

      p_v_descerror := '0020 Buscamos Mnoto total doctos y cantidad doctos';

      BEGIN
         SELECT SUM (mnt_total), COUNT (*)
           INTO l_n_mtodoctos, l_n_cantreg
           FROM csbpi.ta_docto_detalle_recep
          WHERE ide_empresa = p_n_empresa
            --AND ide_contrato_marco = p_n_ide_contrato
            AND ide_remesa = p_n_ideremesa
            AND cod_estado_dcto <> l_c_idunicoestado;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_n_mtodoctos := 0;
            l_n_cantreg := 0;
      END;

      p_v_descerror := '0030 Actualizamos encabezado';

      UPDATE csbpi.ta_docto_encab_recep
         SET nro_documentos = l_n_cantreg,
             mto_documentos = l_n_mtodoctos
       WHERE ide_empresa = p_n_empresa
                                      --AND ide_contrato_marco = p_n_ide_contrato
             AND ide_remesa = p_n_ideremesa;
	  
	  COMMIT;		 
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_registro_archivo_borrar;
----------------------------------------------------------------------------
END pkg_fme_carga_digitacion;
/

