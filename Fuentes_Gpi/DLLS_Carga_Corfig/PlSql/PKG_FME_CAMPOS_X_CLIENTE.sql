CREATE OR REPLACE PACKAGE pkg_fme_campos_x_cliente
IS
   TYPE micursor IS REF CURSOR;

   PROCEDURE sp_fme_formato_cliente (
      p_n_empresa        IN       NUMBER,
      p_n_producto       IN       NUMBER,
      p_n_entidad_cli    IN       NUMBER,
      p_n_sucursal       IN       NUMBER,
      p_c_archivo        IN       CHAR,
      p_c_ordenamiento   IN       CHAR,
      p_c_cursor         OUT      micursor,
      p_v_descerror      OUT      VARCHAR2
   );

   PROCEDURE sp_fme_formato_borrar (
      p_n_empresa        IN       NUMBER,
      p_n_producto       IN       NUMBER,
      p_n_entidad_cli    IN       NUMBER,
      p_n_sucursal       IN       NUMBER,
      p_c_archivo        IN       CHAR,
      p_c_formato        IN       CHAR,
      p_c_conver         IN       CHAR,
      p_n_corr_borrado   OUT      NUMBER,
      p_v_descerror      OUT      VARCHAR2
   );

   PROCEDURE sp_fme_formato_encab_grabar (
      p_n_empresa          IN       NUMBER,
      p_n_producto         IN       NUMBER,
      p_n_entidad_cli      IN       NUMBER,
      p_n_sucursal         IN       NUMBER,
      p_c_archivo          IN       CHAR,
      p_c_formato          IN       CHAR,
      p_n_corr_encab_ent   IN       NUMBER,
      p_c_separador        IN       CHAR,
	  p_c_Decimal		   IN       CHAR,
      p_v_usuario          IN       VARCHAR2,
      p_n_corr_encab_sal   OUT      NUMBER,
      p_v_descerror        OUT      VARCHAR2
   );

   PROCEDURE sp_fme_formato_detalle_grabar (
      p_n_empresa          IN       NUMBER,
      p_n_corr_encab       IN       NUMBER,
      p_c_ide_seccion      IN       CHAR,
      p_c_ide_campo        IN       CHAR,
      p_n_nro_corr_campo   IN       NUMBER,
      p_c_cod_tipo_campo   IN       CHAR,
      p_n_posicion         IN       NUMBER,
      p_n_precision        IN       NUMBER,
      p_n_decimales        IN       NUMBER,
      p_c_alineacion       IN       CHAR,
      p_c_relleno          IN       CHAR,
      p_v_formato          IN       VARCHAR2,
      p_c_convierte        IN       CHAR,
      p_v_usuario          IN       VARCHAR2,
      p_v_descerror        OUT      VARCHAR2
   );

   PROCEDURE sp_fme_formato_orden_grabar (
      p_n_empresa       IN       NUMBER,
      p_n_corr_encab    IN       NUMBER,
      p_c_ide_seccion   IN       CHAR,
      p_c_ide_campo     IN       CHAR,
      p_n_orden         IN       NUMBER,
      p_c_tipo_orden    IN       CHAR,
      p_c_totaliza      IN       CHAR,
      p_v_usuario       IN       VARCHAR2,
      p_v_descerror     OUT      VARCHAR2
   );

   PROCEDURE sp_fme_conversion_mostrar (
      p_n_empresa     IN       NUMBER,
      p_n_producto    IN       NUMBER,
      p_n_entidad     IN       NUMBER,
      p_n_sucursal    IN       NUMBER,
      p_c_tipo_arch   IN       CHAR,
      p_c_tipo_fmto   IN       CHAR,
      p_c_ide_campo   IN       CHAR,
      p_c_cursor      OUT      micursor,
      p_v_descerror   OUT      VARCHAR2
   );

   PROCEDURE sp_fme_conversion_ver (
      p_n_empresa      IN       NUMBER,
      p_n_corr_encab   IN       NUMBER,
      p_c_ide_campo    IN       CHAR,
      p_c_cursor       OUT      micursor,
      p_v_descerror    OUT      VARCHAR2
   );

   PROCEDURE sp_fme_conversion_grabar (
      p_n_empresa        IN       NUMBER,
      p_n_corr_encab     IN       NUMBER,
      p_c_ide_campo      IN       CHAR,
      p_v_descampo_ent   IN       VARCHAR2,
      p_c_codcampo_sal   IN       CHAR,
      p_v_descampo_sal   IN       VARCHAR2,
      p_v_usuario        IN       VARCHAR2,
      p_v_descerror      OUT      VARCHAR2
   );

   PROCEDURE sp_fme_conversion_borrar (
      p_n_empresa      IN       NUMBER,
      p_n_corr_encab   IN       NUMBER,
      p_c_ide_campo    IN       CHAR,
      p_v_usuario      IN       VARCHAR2,
      p_v_descerror    OUT      VARCHAR2
   );
END pkg_fme_campos_x_cliente;
/
CREATE OR REPLACE PACKAGE BODY pkg_fme_campos_x_cliente
AS
   PROCEDURE sp_fme_formato_cliente (
      p_n_empresa        IN       NUMBER,
      p_n_producto       IN       NUMBER,
      p_n_entidad_cli    IN       NUMBER,
      p_n_sucursal       IN       NUMBER,
      p_c_archivo        IN       CHAR,
      p_c_ordenamiento   IN       CHAR,
      p_c_cursor         OUT      micursor,
      p_v_descerror      OUT      VARCHAR2
   )
   IS
      l_v_ide_unico        CHAR (8);
      l_v_ide_unico_secc   CHAR (8);
      l_n_sucursal         NUMBER;
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_v_ide_unico :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 1,
                                                     'ELI');
      l_v_ide_unico_secc :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 7,
                                                     'DET');
      p_v_descerror := '0005 Verificando Numero de Sucursal';

      BEGIN
         SELECT nro_sucursal
           INTO l_n_sucursal
           FROM CSBPI.ta_formato_encab_archivo
          WHERE ide_empresa = p_n_empresa
            AND ide_producto = p_n_producto
            AND ide_entidad_cliente = p_n_entidad_cli
            AND nro_sucursal = p_n_sucursal
            AND cod_tipo_archivo = p_c_archivo
            AND cod_estado <> l_v_ide_unico;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_n_sucursal := 0;
      END;

      IF p_c_ordenamiento = 'N'
      THEN
         p_v_descerror := '0010 Abriendo Cursor Enc-Det_Tot';

         OPEN p_c_cursor FOR
            SELECT   cab.ide_empresa, cab.ide_producto,
                     cab.ide_entidad_cliente, cab.nro_sucursal,
                     cab.cod_tipo_archivo, cab.cod_tipo_formato,
                     cab.nro_corr_encab, cab.des_separador, cab.des_simbolo_decimal, cab.cod_estado,
                     cab.mrc_bloqueo, det.cod_ide_campo,
                     det.nro_corr_detalle, det.nro_corr_campo,
                     det.cod_ide_seccion, det.cod_tipo_campo,
                     pkg_fme_parametros.fn_fme_nomcorto_prmtro
                                          (det.cod_ide_seccion)
                                                              nomcor_seccion,
                     pkg_fme_parametros.fn_fme_glosa_prmtro
                                               (det.cod_tipo_campo)
                                                                  tipo_campo,
                     pkg_fme_parametros.fn_fme_nomcorto_prmtro
                                        (det.cod_tipo_campo)
                                                           nomcor_tipo_campo,
                     det.vlr_posicion, det.vlr_precision, det.vlr_decimales,
                     det.mrc_alineacion, det.mrc_relleno, det.gls_formato,
                     det.flg_convertir detconvertir, det.cod_estado,
                     det.mrc_bloqueo, cam.nro_corr_campo,
                     cam.nro_corr_columna, cam.gls_campo, cam.cod_ide_campo,
                     cam.flg_obligatorio, cam.flg_ind_ordena,
                     cam.flg_ind_totaliza, cam.flg_convertir camconvertir,
					 pkg_fme_parametros.fn_fme_glosa_prmtro(cam.cod_ide_campo ) nom_campo
					 
                FROM CSBPI.ta_formato_encab_archivo cab,
                     CSBPI.ta_formato_detalle_archivo det,
                     CSBPI.ta_campo_archivo_remesa cam
               WHERE cab.ide_empresa = det.ide_empresa
                 AND cab.nro_corr_encab = det.nro_corr_encab
                 AND det.ide_empresa = cam.ide_empresa
                 AND det.nro_corr_campo = cam.nro_corr_campo
                 AND cab.ide_empresa = p_n_empresa
                 AND cab.ide_producto = p_n_producto
                 AND cab.ide_entidad_cliente = p_n_entidad_cli
                 AND cab.nro_sucursal = l_n_sucursal
                 /*AND cab.nro_sucursal =
                        DECODE (p_n_sucursal,
                                0, cab.nro_sucursal,
                                p_n_sucursal
                               ) */
                 AND cab.cod_tipo_archivo = p_c_archivo
--               AND cab.cod_tipo_formato = p_c_formato
                 AND cab.cod_estado <> l_v_ide_unico
            ORDER BY det.cod_ide_seccion, det.vlr_posicion;
      ELSIF p_c_ordenamiento = 'S'
      THEN
         p_v_descerror := '0020 Abriendo Cursor Ordenamiento';

         OPEN p_c_cursor FOR
            SELECT   cam.nro_corr_campo, cam.gls_campo, cam.flg_ind_totaliza,
                     ord.flg_totaliza, det.cod_ide_campo, ord.mrc_tipo_orden
                FROM CSBPI.ta_orden_archivo ord,
                     CSBPI.ta_formato_encab_archivo cab,
                     CSBPI.ta_formato_detalle_archivo det,
                     CSBPI.ta_campo_archivo_remesa cam
               WHERE ord.ide_empresa = det.ide_empresa
                 AND ord.nro_corr_encab = det.nro_corr_encab
                 AND ord.nro_corr_detalle = det.nro_corr_detalle
                 AND det.ide_empresa = cab.ide_empresa
                 AND det.nro_corr_encab = cab.nro_corr_encab
                 AND det.ide_empresa = cam.ide_empresa
                 AND det.nro_corr_campo = cam.nro_corr_campo
                 AND cab.ide_empresa = p_n_empresa
                 AND cab.ide_producto = p_n_producto
                 AND cab.ide_entidad_cliente = p_n_entidad_cli
                 AND cab.nro_sucursal = l_n_sucursal
                 /*AND cab.nro_sucursal =
                        DECODE (p_n_sucursal,
                                0, cab.nro_sucursal,
                                p_n_sucursal
                               ) */
                 AND cab.cod_tipo_archivo = p_c_archivo
                 --AND cab.cod_tipo_formato = p_c_formato
                 AND cam.cod_ide_seccion = l_v_ide_unico_secc
                 AND (cam.flg_ind_ordena = 1 OR cam.flg_ind_totaliza = 1)
            ORDER BY ord.nro_orden;
      END IF;

      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;

         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_formato_cliente;

---------------------------------------------------------------------------------------
   PROCEDURE sp_fme_formato_borrar (
      p_n_empresa        IN       NUMBER,
      p_n_producto       IN       NUMBER,
      p_n_entidad_cli    IN       NUMBER,
      p_n_sucursal       IN       NUMBER,
      p_c_archivo        IN       CHAR,
      p_c_formato        IN       CHAR,
      p_c_conver         IN       CHAR,
      p_n_corr_borrado   OUT      NUMBER,
      p_v_descerror      OUT      VARCHAR2
   )
   IS
      l_n_corr_encab   NUMBER;
   BEGIN
      p_v_descerror := '0010 Buscando el correlativo a Borrar';

      BEGIN
         SELECT nro_corr_encab
           INTO l_n_corr_encab
           FROM CSBPI.ta_formato_encab_archivo
          WHERE ide_empresa = p_n_empresa
            AND ide_producto = p_n_producto
            AND ide_entidad_cliente = p_n_entidad_cli
            AND nro_sucursal = p_n_sucursal
            AND cod_tipo_archivo = p_c_archivo
            AND cod_tipo_formato = p_c_formato;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_n_corr_encab := 0;
      END;

      p_v_descerror := '0020 Borrando orden';

      DELETE FROM CSBPI.ta_orden_archivo
            WHERE ide_empresa = p_n_empresa
                  AND nro_corr_encab = l_n_corr_encab;

      p_v_descerror := '0030 Borrando detalle';

      DELETE FROM CSBPI.ta_formato_detalle_archivo
            WHERE ide_empresa = p_n_empresa
                  AND nro_corr_encab = l_n_corr_encab;

      p_v_descerror := '0040 Borrando encabezado';

      DELETE FROM CSBPI.ta_formato_encab_archivo
            WHERE ide_empresa = p_n_empresa
                  AND nro_corr_encab = l_n_corr_encab;

      IF p_c_conver = 'S'
      THEN
         p_v_descerror := '0050 Borrando conversion';

         DELETE FROM CSBPI.ta_conversion_archivo
               WHERE ide_empresa = p_n_empresa
                 AND nro_corr_encab = l_n_corr_encab;
      END IF;

      p_n_corr_borrado := l_n_corr_encab;
	  
	  COMMIT;
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_formato_borrar;

---------------------------------------------------------------------------------------
   PROCEDURE sp_fme_formato_encab_grabar (
      p_n_empresa          IN       NUMBER,
      p_n_producto         IN       NUMBER,
      p_n_entidad_cli      IN       NUMBER,
      p_n_sucursal         IN       NUMBER,
      p_c_archivo          IN       CHAR,
      p_c_formato          IN       CHAR,
      p_n_corr_encab_ent   IN       NUMBER,
      p_c_separador        IN       CHAR,
      p_c_Decimal		   IN       CHAR,
	  p_v_usuario          IN       VARCHAR2,
      p_n_corr_encab_sal   OUT      NUMBER,
      p_v_descerror        OUT      VARCHAR2
   )
   IS
      l_d_fechahora    DATE;
      l_c_estado_apr   CHAR (8);
      l_n_corr_encab   NUMBER;
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      l_c_estado_apr :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 1, 'APR');
      p_v_descerror := '0010 Obteniendo correlativo del encabezado';

      IF p_n_corr_encab_ent = 0
      THEN
         SELECT sc_fme_campos_x_cliente.NEXTVAL
           INTO l_n_corr_encab
           FROM DUAL;
      ELSE
         l_n_corr_encab := p_n_corr_encab_ent;
      END IF;

      p_v_descerror := '0020 Insertando encabezado ' || l_n_corr_encab;

      INSERT INTO CSBPI.ta_formato_encab_archivo 
                  (ide_empresa, ide_producto, ide_entidad_cliente,
                   nro_sucursal, cod_tipo_archivo, cod_tipo_formato,
                   nro_corr_encab, des_separador, des_simbolo_decimal, 
				   mrc_bloqueo, cod_estado,fch_hra_mod_estado, cod_usr_mod_estado,
				   fch_hra_creacion,cod_usr_creacion
                  )
           VALUES (p_n_empresa, p_n_producto, p_n_entidad_cli,
                   p_n_sucursal, p_c_archivo, p_c_formato,
                   l_n_corr_encab, p_c_separador, p_c_Decimal, 0, l_c_estado_apr,
                   l_d_fechahora, p_v_usuario, l_d_fechahora,
                   p_v_usuario
                  );

      p_n_corr_encab_sal := l_n_corr_encab;
	  
	  COMMIT;
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_formato_encab_grabar;

---------------------------------------------------------------------------------------
   PROCEDURE sp_fme_formato_detalle_grabar (
      p_n_empresa          IN       NUMBER,
      p_n_corr_encab       IN       NUMBER,
      p_c_ide_seccion      IN       CHAR,
      p_c_ide_campo        IN       CHAR,
      p_n_nro_corr_campo   IN       NUMBER,
      p_c_cod_tipo_campo   IN       CHAR,
      p_n_posicion         IN       NUMBER,
      p_n_precision        IN       NUMBER,
      p_n_decimales        IN       NUMBER,
      p_c_alineacion       IN       CHAR,
      p_c_relleno          IN       CHAR,
      p_v_formato          IN       VARCHAR2,
      p_c_convierte        IN       CHAR,
      p_v_usuario          IN       VARCHAR2,
      p_v_descerror        OUT      VARCHAR2
   )
   IS
      l_d_fechahora      DATE;
      l_c_estado_apr     CHAR (8);
      l_n_corr_detalle   NUMBER;
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      l_c_estado_apr :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa, 1, 1, 'APR');
      p_v_descerror := '0010 Buscando el siguiente correlativo ';

      SELECT NVL (MAX (nro_corr_detalle), 0) + 1
        INTO l_n_corr_detalle
        FROM CSBPI.ta_formato_detalle_archivo
       WHERE ide_empresa = p_n_empresa AND nro_corr_encab = p_n_corr_encab;

      p_v_descerror := '0020 Insertando registro por seccion';

      INSERT INTO CSBPI.ta_formato_detalle_archivo
                  (ide_empresa, nro_corr_encab, cod_ide_seccion,
                   cod_ide_campo, nro_corr_detalle, nro_corr_campo,
                   cod_tipo_campo, vlr_posicion, vlr_precision,
                   flg_convertir, vlr_decimales, mrc_alineacion, mrc_relleno,
                   gls_formato, mrc_bloqueo, cod_estado, fch_hra_mod_estado,
                   cod_usr_mod_estado, fch_hra_creacion, cod_usr_creacion
                  )
           VALUES (p_n_empresa, p_n_corr_encab, p_c_ide_seccion,
                   p_c_ide_campo, l_n_corr_detalle, p_n_nro_corr_campo,
                   p_c_cod_tipo_campo, p_n_posicion, p_n_precision,
                   p_c_convierte, p_n_decimales, p_c_alineacion, p_c_relleno,
                   p_v_formato, 0, l_c_estado_apr, l_d_fechahora,
                   p_v_usuario, l_d_fechahora, p_v_usuario
                  );
	  
	  COMMIT;
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_formato_detalle_grabar;

---------------------------------------------------------------------------------------
   PROCEDURE sp_fme_formato_orden_grabar (
      p_n_empresa       IN       NUMBER,
      p_n_corr_encab    IN       NUMBER,
      p_c_ide_seccion   IN       CHAR,
      p_c_ide_campo     IN       CHAR,
      p_n_orden         IN       NUMBER,
      p_c_tipo_orden    IN       CHAR,
      p_c_totaliza      IN       CHAR,
      p_v_usuario       IN       VARCHAR2,
      p_v_descerror     OUT      VARCHAR2
   )
   IS
      l_d_fechahora      DATE;
      l_n_corr_detalle   NUMBER;
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      p_v_descerror := '0010 Buscando el correlativo detalle del campo';

      SELECT nro_corr_detalle
        INTO l_n_corr_detalle
        FROM CSBPI.ta_formato_detalle_archivo
       WHERE ide_empresa = p_n_empresa
         AND nro_corr_encab = p_n_corr_encab
         AND cod_ide_seccion = p_c_ide_seccion
         AND cod_ide_campo = p_c_ide_campo;

      p_v_descerror := '0020 Insertando registro de orden';

      INSERT INTO CSBPI.ta_orden_archivo
                  (ide_empresa, nro_corr_encab, nro_corr_detalle, nro_orden,
                   mrc_tipo_orden, flg_totaliza, fch_hra_creacion,
                   cod_usr_creacion
                  )
           VALUES (p_n_empresa, p_n_corr_encab, l_n_corr_detalle, p_n_orden,
                   p_c_tipo_orden, p_c_totaliza, l_d_fechahora,
                   p_v_usuario
                  );
	  
	  COMMIT; 			  
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_formato_orden_grabar;

---------------------------------------------------------------------------------------
   PROCEDURE sp_fme_conversion_mostrar (
      p_n_empresa     IN       NUMBER,
      p_n_producto    IN       NUMBER,
      p_n_entidad     IN       NUMBER,
      p_n_sucursal    IN       NUMBER,
      p_c_tipo_arch   IN       CHAR,
      p_c_tipo_fmto   IN       CHAR,
      p_c_ide_campo   IN       CHAR,
      p_c_cursor      OUT      micursor,
      p_v_descerror   OUT      VARCHAR2
   )
   IS
      l_c_tipo_arch   CHAR (8);
   BEGIN
      p_v_descerror := '0010 Inicio';
      l_c_tipo_arch :=
         pkg_fme_parametros.fn_fme_ide_unico_prmtro (p_n_empresa,
                                                     1,
                                                     6,
                                                     p_c_tipo_arch
                                                    );
      p_v_descerror := '0020 Abriendo Cursor de conversion';

      OPEN p_c_cursor FOR
         SELECT   b.cod_ide_campo, b.des_campo_entrada, b.cod_campo_salida,
                  b.des_campo_salida
             FROM CSBPI.ta_formato_encab_archivo a, CSBPI.ta_conversion_archivo b
            WHERE a.ide_empresa = b.ide_empresa
              AND a.nro_corr_encab = b.nro_corr_encab
              AND a.ide_empresa = p_n_empresa
              AND a.ide_producto = p_n_producto
              AND a.ide_entidad_cliente = p_n_entidad
              AND a.nro_sucursal = p_n_sucursal
              AND a.cod_tipo_archivo = l_c_tipo_arch
              AND a.cod_tipo_formato = p_c_tipo_fmto
              AND b.cod_ide_campo = p_c_ide_campo
              AND b.des_campo_entrada IS NOT NULL
         ORDER BY b.des_campo_entrada;

      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;

         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_conversion_mostrar;

---------------------------------------------------------------------------------------
   PROCEDURE sp_fme_conversion_ver (
      p_n_empresa      IN       NUMBER,
      p_n_corr_encab   IN       NUMBER,
      p_c_ide_campo    IN       CHAR,
      p_c_cursor       OUT      micursor,
      p_v_descerror    OUT      VARCHAR2
   )
   IS
   BEGIN
      p_v_descerror := '0010 Abriendo Cursor de conversion';

      OPEN p_c_cursor FOR
         SELECT ide_empresa, nro_corr_encab, cod_ide_campo, nro_corr_conver,
                des_campo_entrada, cod_campo_salida, des_campo_salida
           FROM CSBPI.ta_conversion_archivo
          WHERE ide_empresa = p_n_empresa
            AND nro_corr_encab = p_n_corr_encab
            AND cod_ide_campo = p_c_ide_campo;

      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;

         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_conversion_ver;

---------------------------------------------------------------------------------------
   PROCEDURE sp_fme_conversion_grabar (
      p_n_empresa        IN       NUMBER,
      p_n_corr_encab     IN       NUMBER,
      p_c_ide_campo      IN       CHAR,
      p_v_descampo_ent   IN       VARCHAR2,
      p_c_codcampo_sal   IN       CHAR,
      p_v_descampo_sal   IN       VARCHAR2,
      p_v_usuario        IN       VARCHAR2,
      p_v_descerror      OUT      VARCHAR2
   )
   IS
      l_d_fechahora     DATE;
      l_n_corr_conver   NUMBER;
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      p_v_descerror := '0010 Actualizando conversion';

      UPDATE CSBPI.ta_conversion_archivo
         SET des_campo_entrada = p_v_descampo_ent,
             des_campo_salida = p_v_descampo_sal,
             fch_hra_modifica = l_d_fechahora,
             cod_usr_modifica = p_v_usuario
       WHERE ide_empresa = p_n_empresa
         AND nro_corr_encab = p_n_corr_encab
         AND cod_ide_campo = p_c_ide_campo
         AND cod_campo_salida = p_c_codcampo_sal;

      IF SQL%ROWCOUNT = 0
      THEN
         p_v_descerror := '0020 Buscando el siguiente correlativo ';

         SELECT NVL (MAX (nro_corr_conver), 0) + 1
           INTO l_n_corr_conver
           FROM CSBPI.ta_conversion_archivo
          WHERE ide_empresa = p_n_empresa AND nro_corr_encab = p_n_corr_encab;

         p_v_descerror := '0030 Insertando conversion';

         INSERT INTO CSBPI.ta_conversion_archivo
                     (ide_empresa, nro_corr_encab, cod_ide_campo,
                      nro_corr_conver, des_campo_entrada, cod_campo_salida,
                      des_campo_salida, fch_hra_creacion, cod_usr_creacion
                     )
              VALUES (p_n_empresa, p_n_corr_encab, p_c_ide_campo,
                      l_n_corr_conver, p_v_descampo_ent, p_c_codcampo_sal,
                      p_v_descampo_sal, l_d_fechahora, p_v_usuario
                     );
      END IF;

      p_v_descerror := '0040 cambiando estado de conversion en campo';

      UPDATE CSBPI.ta_formato_detalle_archivo
         SET flg_convertir = 1,
             cod_usr_modifica = p_v_usuario,
             fch_hra_modifica = l_d_fechahora
       WHERE ide_empresa = p_n_empresa
         AND nro_corr_encab = p_n_corr_encab
         AND cod_ide_campo = p_c_ide_campo;
	  
	  COMMIT;
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_conversion_grabar;

---------------------------------------------------------------------------------------
   PROCEDURE sp_fme_conversion_borrar (
      p_n_empresa      IN       NUMBER,
      p_n_corr_encab   IN       NUMBER,
      p_c_ide_campo    IN       CHAR,
      p_v_usuario      IN       VARCHAR2,
      p_v_descerror    OUT      VARCHAR2
   )
   IS
      l_d_fechahora   DATE;
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      p_v_descerror := '0010 Borrando conversion de campo';

      DELETE FROM CSBPI.ta_conversion_archivo
            WHERE ide_empresa = p_n_empresa
              AND nro_corr_encab = p_n_corr_encab
              AND cod_ide_campo = p_c_ide_campo;

      p_v_descerror := '0020 cambiando estado de conversion en campo';

      UPDATE CSBPI.ta_formato_detalle_archivo
         SET flg_convertir = 0,
             cod_usr_modifica = p_v_usuario,
             fch_hra_modifica = l_d_fechahora
       WHERE ide_empresa = p_n_empresa
         AND nro_corr_encab = p_n_corr_encab
         AND cod_ide_campo = p_c_ide_campo;
	  
	  COMMIT;	
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_conversion_borrar;
---------------------------------------------------------------------------------------
END pkg_fme_campos_x_cliente;
/

