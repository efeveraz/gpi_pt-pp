CREATE OR REPLACE PACKAGE pkg_fme_parametros
IS
   TYPE micursor IS REF CURSOR;
   
   PROCEDURE sp_fme_prmtros_ver (
      p_n_empresa           IN       NUMBER,
      p_n_codigotipo        IN       NUMBER,
      p_n_codigosubtipo     IN       NUMBER,
      p_n_codigoparametro   IN       NUMBER,
      p_c_cursor            OUT      micursor,
      p_v_descerror         OUT      VARCHAR2
   );
   
   PROCEDURE sp_fme_subtipo_prmtros_ver (
      p_n_empresa         IN       NUMBER,
      p_n_codigotipo      IN       NUMBER,
      p_n_codigosubtipo   IN       NUMBER,
      p_c_cursor          OUT      micursor,
      p_v_descerror       OUT      VARCHAR2
   );
   
   PROCEDURE sp_fme_prmtros_grabar (
      p_n_empresa           IN       NUMBER,
      p_n_codigotipo        IN       NUMBER,
      p_n_codigosubtipo     IN       NUMBER,
      p_n_codigoparametro   IN       NUMBER,
      p_v_descripcion       IN       VARCHAR2,
      p_v_nomcorto          IN       VARCHAR2,
      p_c_sistema           IN       CHAR,
      p_v_codigoant         IN       VARCHAR2,
      p_v_codigobco         IN       VARCHAR2,
      p_n_bloqueo           IN       NUMBER,
      p_v_usuario           IN       VARCHAR2,
      p_v_descerror         OUT      VARCHAR2
   );
   
   PROCEDURE sp_fme_prmtros_borrar (
      p_n_empresa           IN       NUMBER,
      p_n_codigotipo        IN       NUMBER,
      p_n_codigosubtipo     IN       NUMBER,
      p_n_codigoparametro   IN       NUMBER,
      p_v_descerror         OUT      VARCHAR2
   );
   
   FUNCTION fn_fme_glosa_prmtro (p_n_ide_unico NUMBER)
      RETURN VARCHAR2;
   
   FUNCTION fn_fme_nomcorto_prmtro (p_n_ide_unico NUMBER)
      RETURN VARCHAR2;

   FUNCTION fn_fme_ide_unico_prmtro (
      p_n_empresa    IN   NUMBER,
      p_n_tipo       IN   NUMBER,
      p_n_subtipo    IN   NUMBER,
      p_v_nomcorto   IN   VARCHAR2) RETURN CHAR;
	  
   FUNCTION fn_Monedas (p_n_dato NUMBER)
      RETURN VARCHAR2;
	  
END pkg_fme_parametros;
/
CREATE OR REPLACE PACKAGE BODY pkg_fme_parametros
AS

----------------------------------------------------------------------------

PROCEDURE sp_fme_prmtros_ver (
      p_n_empresa           IN       NUMBER,
      p_n_codigotipo        IN       NUMBER,
      p_n_codigosubtipo     IN       NUMBER,
      p_n_codigoparametro   IN       NUMBER,
      p_c_cursor            OUT      micursor,
      p_v_descerror         OUT      VARCHAR2
   )
   IS
   BEGIN
      IF p_n_codigoparametro > 0
      THEN
         p_v_descerror := '0010 Buscando un parametro';

         OPEN p_c_cursor FOR
            SELECT ide_tipo_param, ide_subtipo_param, ide_parametro,
                   ide_unico_param, gls_descripcion, nom_corto, mrc_sistema,
                   cod_anterior, cod_banco, mrc_bloqueo, fch_hra_bloqueo,
                   cod_usr_bloqueo, fch_hra_creacion, cod_usr_creacion,
                   fch_hra_modifica, cod_usr_modifica
              FROM CSBPI.ta_parametro
             WHERE ide_empresa = p_n_empresa
               AND ide_tipo_param = p_n_codigotipo
               AND ide_subtipo_param = p_n_codigosubtipo
               AND ide_parametro = p_n_codigoparametro
			   AND IDE_UNICO_PARAM <> 0;
      ELSE
         p_v_descerror := '0020 Buscando todos los parametros de un subtipo';

         OPEN p_c_cursor FOR
            SELECT   ide_tipo_param, ide_subtipo_param, ide_parametro,
                     ide_unico_param, gls_descripcion, nom_corto,
                     mrc_sistema, cod_anterior, cod_banco, mrc_bloqueo,
                     fch_hra_bloqueo, cod_usr_bloqueo, fch_hra_creacion,
                     cod_usr_creacion, fch_hra_modifica, cod_usr_modifica
                FROM CSBPI.ta_parametro
               WHERE ide_empresa = p_n_empresa
                 AND ide_tipo_param = p_n_codigotipo
                 AND ide_subtipo_param = p_n_codigosubtipo
				 AND IDE_UNICO_PARAM <> 0  
            ORDER BY ide_parametro;
      END IF;

      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;

         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_prmtros_ver;

-----------------------------------------------------------------------------  
   
PROCEDURE sp_fme_subtipo_prmtros_ver (
      p_n_empresa         IN       NUMBER,
      p_n_codigotipo      IN       NUMBER,
      p_n_codigosubtipo   IN       NUMBER,
      p_c_cursor          OUT      micursor,
      p_v_descerror       OUT      VARCHAR2
   )
   IS
   BEGIN
      IF p_n_codigosubtipo > 0
      THEN
         p_v_descerror := '0010 Buscando un subtipo';

         OPEN p_c_cursor FOR
            SELECT  IDE_EMPRESA, 
					IDE_TIPO_PARAM, 
					IDE_SUBTIPO_PARAM, 
					IDE_PARAMETRO, 
					IDE_UNICO_PARAM, 
					GLS_DESCRIPCION, 
					NOM_CORTO, 
					COD_ANTERIOR, 
					COD_BANCO, 
					MRC_SISTEMA, 
					MRC_BLOQUEO, 
					FCH_HRA_BLOQUEO, 
					COD_USR_BLOQUEO, 
					FCH_HRA_CREACION, 
					COD_USR_CREACION, 
					FCH_HRA_MODIFICA, 
					COD_USR_MODIFICA
              FROM CSBPI.TA_PARAMETRO
             WHERE ide_empresa = p_n_empresa
               AND ide_tipo_param = p_n_codigotipo
               AND ide_subtipo_param = p_n_codigosubtipo
			   AND IDE_PARAMETRO <> 0;
      ELSE
         p_v_descerror := '0020 Buscando todos los subtipos.';
         OPEN p_c_cursor FOR
            SELECT  IDE_EMPRESA, 
					IDE_TIPO_PARAM, 
					IDE_SUBTIPO_PARAM, 
					IDE_PARAMETRO, 
					IDE_UNICO_PARAM, 
					GLS_DESCRIPCION, 
					NOM_CORTO, 
					COD_ANTERIOR, 
					COD_BANCO, 
					MRC_SISTEMA, 
					MRC_BLOQUEO, 
					FCH_HRA_BLOQUEO, 
					COD_USR_BLOQUEO, 
					FCH_HRA_CREACION, 
					COD_USR_CREACION, 
					FCH_HRA_MODIFICA, 
					COD_USR_MODIFICA
                FROM CSBPI.TA_PARAMETRO
               WHERE IDE_EMPRESA = p_n_empresa
                 AND IDE_TIPO_PARAM = p_n_codigotipo
				 AND IDE_PARAMETRO = 0 
            ORDER BY GLS_DESCRIPCION;
      END IF;

      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         OPEN p_c_cursor FOR
            SELECT *
              FROM DUAL;

         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_subtipo_prmtros_ver;
   
   ----------------------------------------------------------------------------
   PROCEDURE sp_fme_prmtros_grabar (
      p_n_empresa           IN       NUMBER,
      p_n_codigotipo        IN       NUMBER,
      p_n_codigosubtipo     IN       NUMBER,
      p_n_codigoparametro   IN       NUMBER,
      p_v_descripcion       IN       VARCHAR2,
      p_v_nomcorto          IN       VARCHAR2,
      p_c_sistema           IN       CHAR,
      p_v_codigoant         IN       VARCHAR2,
      p_v_codigobco         IN       VARCHAR2,
      p_n_bloqueo           IN       NUMBER,
      p_v_usuario           IN       VARCHAR2,
      p_v_descerror         OUT      VARCHAR2
   )
   IS
      l_n_ide         NUMBER;
      l_n_ideSec      NUMBER;
	  l_d_fechahora   DATE;
   BEGIN
      p_v_descerror := '0000 Inicio';
      l_d_fechahora :=
         TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS'),
                  'DD/MM/YYYY HH:MI:SS'
                 );
      p_v_descerror := '0010 Actualizando un parametro';

      UPDATE CSBPI.ta_parametro
         SET gls_descripcion = p_v_descripcion,
             nom_corto = p_v_nomcorto,
             mrc_sistema = p_c_sistema,
             cod_anterior = p_v_codigoant,
             cod_banco = p_v_codigobco,
             mrc_bloqueo = p_n_bloqueo,
             cod_usr_bloqueo = DECODE (p_n_bloqueo, 0, NULL, p_v_usuario),
             fch_hra_bloqueo = DECODE (p_n_bloqueo, 0, NULL, l_d_fechahora),
             cod_usr_modifica = p_v_usuario,
             fch_hra_modifica = l_d_fechahora
       WHERE ide_empresa = p_n_empresa
         AND ide_tipo_param = p_n_codigotipo
         AND ide_subtipo_param = p_n_codigosubtipo
         AND ide_unico_param = p_n_codigoparametro
		 AND ide_unico_param <> 0;

      IF SQL%ROWCOUNT = 0
      THEN
         p_v_descerror := '0020 Buscando el ultimo parametro';

         SELECT NVL (MAX (ide_parametro), 0) + 1
           INTO l_n_ide
           FROM CSBPI.ta_parametro
          WHERE ide_empresa = p_n_empresa
            AND ide_tipo_param = p_n_codigotipo
            AND ide_subtipo_param = p_n_codigosubtipo;
			
		 SELECT sc_fme_parametro.NEXTVAL INTO l_n_ideSec FROM DUAL;
		  
         p_v_descerror := '0030 Insertando un nuevo parametro';

         INSERT INTO CSBPI.ta_parametro
                     (ide_empresa, ide_tipo_param, ide_subtipo_param,
                      ide_parametro, ide_unico_param, gls_descripcion,
                      nom_corto, mrc_sistema, cod_anterior,
                      cod_banco, mrc_bloqueo,
                      fch_hra_bloqueo,
                      cod_usr_bloqueo,
                      fch_hra_creacion, cod_usr_creacion
                     )
              VALUES (p_n_empresa, p_n_codigotipo, p_n_codigosubtipo,
                      l_n_ide, l_n_ideSec, p_v_descripcion,
                      p_v_nomcorto, p_c_sistema, p_v_codigoant,
                      p_v_codigobco, p_n_bloqueo,
                      DECODE (p_n_bloqueo, 0, NULL, l_d_fechahora),
                      DECODE (p_n_bloqueo, 0, NULL, p_v_usuario),
                      l_d_fechahora, p_v_usuario
                     );	
					 
      	IF p_n_codigosubtipo = 10 THEN
			INSERT INTO CSBPI.TA_CAMPO_ARCHIVO_REMESA
				(IDE_EMPRESA, 
				 IDE_PRODUCTO, 
				 COD_TIPO_ARCHIVO, 
				 COD_IDE_SECCION, 
				 NRO_CORR_CAMPO, 
				 NRO_CORR_COLUMNA, 
				 GLS_CAMPO, 
				 COD_IDE_CAMPO, 
				 FLG_OBLIGATORIO, 
				 FLG_IND_ORDENA, 
				 FLG_IND_TOTALIZA, 
				 FLG_CONVERTIR, 
				 FCH_HRA_CREACION, 
				 COD_USR_CREACION 
                )
			 VALUES
			    (p_n_empresa,
				 l_n_ideSec,
                 0,
				 0,
				 l_n_ideSec,
				 1,
				 'FILLER',
				 -99,
				 0,
				 0,
				 0,
				 0,
				 l_d_fechahora, 
				 p_v_usuario);
		END IF; 
	  END IF;
	  COMMIT;
	  
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_prmtros_grabar;

----------------------------------------------------------------------------
   PROCEDURE sp_fme_prmtros_borrar (
      p_n_empresa           IN       NUMBER,
      p_n_codigotipo        IN       NUMBER,
      p_n_codigosubtipo     IN       NUMBER,
      p_n_codigoparametro   IN       NUMBER,
      p_v_descerror         OUT      VARCHAR2
   )
   IS
   BEGIN
      p_v_descerror := '0010 Borrando un parametro';

      DELETE      CSBPI.ta_parametro
            WHERE ide_empresa = p_n_empresa
              AND ide_tipo_param = p_n_codigotipo
              AND ide_subtipo_param = p_n_codigosubtipo
              AND IDE_UNICO_PARAM = p_n_codigoparametro
			  AND MRC_SISTEMA <> 1;
			  
	  COMMIT;	
      p_v_descerror := '9999 Termino';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_v_descerror :=
               RTRIM (p_v_descerror)
            || ' >> '
            || SQLCODE
            || ' '
            || SUBSTR (SQLERRM, 1, 700);
   END sp_fme_prmtros_borrar;

-----------------------------------------------------------------------------  

   FUNCTION fn_fme_glosa_prmtro (p_n_ide_unico NUMBER)
      RETURN VARCHAR2
   IS
      l_v_glosa   VARCHAR2 (50);
   BEGIN
      BEGIN
         SELECT gls_descripcion
           INTO l_v_glosa
           FROM CSBPI.ta_parametro
          WHERE ide_unico_param = p_n_ide_unico;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_v_glosa := NULL;
      END;

      RETURN l_v_glosa;
   END fn_fme_glosa_prmtro;	 
   
----------------------------------------------------------------------------
   FUNCTION fn_fme_ide_unico_prmtro (
      p_n_empresa    IN   NUMBER,
      p_n_tipo       IN   NUMBER,
      p_n_subtipo    IN   NUMBER,
      p_v_nomcorto   IN   VARCHAR2
   )
      RETURN CHAR
   IS
      l_n_ide   CHAR (8);
   BEGIN
      BEGIN
         SELECT ide_unico_param
           INTO l_n_ide
           FROM CSBPI.ta_parametro
          WHERE ide_empresa = p_n_empresa
            AND ide_tipo_param = p_n_tipo
            AND ide_subtipo_param = p_n_subtipo
            AND nom_corto = p_v_nomcorto;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_n_ide := NULL;
      END;

      RETURN l_n_ide;
   END fn_fme_ide_unico_prmtro;
   
----------------------------------------------------------------------------
   FUNCTION fn_fme_nomcorto_prmtro (p_n_ide_unico NUMBER)
      RETURN VARCHAR2
   IS
      l_v_nomcorto   VARCHAR2 (20);
   BEGIN
      BEGIN
         SELECT nom_corto
           INTO l_v_nomcorto
           FROM CSBPI.ta_parametro
          WHERE ide_unico_param = p_n_ide_unico;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_v_nomcorto := NULL;
      END;

      RETURN l_v_nomcorto;
   END fn_fme_nomcorto_prmtro;


   FUNCTION fn_Monedas (p_n_dato NUMBER)
      RETURN VARCHAR2
   IS
      dscmoneda   VARCHAR2(20);
   BEGIN
      BEGIN
         SELECT dsc_moneda
           INTO dscmoneda
           FROM CSBPI.Monedas
          WHERE ID_MONEDA = p_n_dato;

      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            dscmoneda := NULL;
      END;

      RETURN dscmoneda;
   END fn_Monedas;

END pkg_fme_parametros;
/

