CREATE OR REPLACE PACKAGE PKG_FME_VALIDA_REMESA IS
	TYPE micursor IS REF CURSOR;

	PROCEDURE SP_FME_ACT_EST_REMESA(
	p_n_empresa        IN  NUMBER,
	p_n_NumRemesa      IN  NUMBER,
	p_n_Estado		   IN  NUMBER,
	p_c_usuario        IN  CHAR,
	p_v_DescError      OUT VARCHAR2
	);

END PKG_FME_VALIDA_REMESA;
/
CREATE OR REPLACE PACKAGE BODY PKG_FME_VALIDA_REMESA AS

PROCEDURE SP_FME_ACT_EST_REMESA(
p_n_empresa        IN  NUMBER,
p_n_NumRemesa      IN  NUMBER,
p_n_Estado		   IN  NUMBER,
p_c_usuario        IN  CHAR,
p_v_DescError      OUT VARCHAR2) AS

--rem p_n_Estado   1 = CARGA O DIGIT. SIN VALIDAD
--                 2 = CARGA O DIGIT. VALIDADA CON ERRRORES
--                 3 = PARA ACEPTACION CON ERRORES
--				   4 = PARA ACEPTACION SIN ERRORES
--                 5 = ELIMINADA
l_c_IdEst_Rem   CHAR(8);
l_d_fechahora   DATE;
BEGIN

	p_v_DescError :='0010 Inicio (SP_FME_ACT_EST_REMESA)';
	l_d_fechahora := TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY HH:MI:SS'),'DD/MM/YYYY HH:MI:SS');

	IF p_n_Estado = 1 THEN
		p_v_DescError := '0020 Rescatando ide estado Remesa Carga o Digit. Sin Validar (SP_FME_ACT_EST_REMESA)';
		l_c_IdEst_Rem := Pkg_Fme_Parametros.FN_FME_IDE_UNICO_PRMTRO (p_n_empresa,1,11,'CSV');
	ELSIF p_n_Estado = 2 THEN
		p_v_DescError := '0030 Rescatando ide estado Remesa Carga o Digit.Validada con Err (SP_FME_ACT_EST_REMESA)';
		l_c_IdEst_Rem := Pkg_Fme_Parametros.FN_FME_IDE_UNICO_PRMTRO (p_n_empresa,1,11,'CVCE');
	ELSIF p_n_Estado = 3 THEN
	    p_v_DescError := '0040 Rescatando ide estado Remesa Para Aceptacion con Err (SP_FME_ACT_EST_REMESA)';
		l_c_IdEst_Rem := Pkg_Fme_Parametros.FN_FME_IDE_UNICO_PRMTRO (p_n_empresa,1,11,'PACE');
	ELSIF p_n_Estado = 4 THEN
		p_v_DescError := '0050 Rescatando ide estado Remesa Para Aceptacion sin Err (SP_FME_ACT_EST_REMESA)';
		l_c_IdEst_Rem := Pkg_Fme_Parametros.FN_FME_IDE_UNICO_PRMTRO (p_n_empresa,1,11,'PASE');
	ELSIF p_n_Estado = 5 THEN
		p_v_DescError := '0060 Rescatando ide estado Remesa Eliminada (SP_FME_ACT_EST_REMESA)';
		l_c_IdEst_Rem := Pkg_Fme_Parametros.FN_FME_IDE_UNICO_PRMTRO (p_n_empresa,1,11,'ELI');
	END IF;

	p_v_DescError := '0070 Actualizando estado Remesa (SP_FME_ACT_EST_REMESA)';
	UPDATE TA_DOCTO_ENCAB_RECEP SET
		COD_ESTADO_REMESA = l_c_IdEst_Rem,
		FCH_HRA_MOD_ESTADO = l_d_fechahora,
		COD_USR_MOD_ESTADO = p_c_usuario
	WHERE
		IDE_EMPRESA = p_n_empresa
	AND	IDE_REMESA = p_n_NumRemesa;
	
	COMMIT;
    p_v_DescError := '9999 Termino (SP_FME_ACT_EST_REMESA)';

	EXCEPTION
        WHEN OTHERS THEN
		p_v_DescError := RTRIM (p_v_DescError) || ' >> ' ||
			                 SQLCODE || ' ' || SUBSTR (SQLERRM, 1, 700);
END SP_FME_ACT_EST_REMESA;

END PKG_FME_VALIDA_REMESA;
/

