CREATE OR REPLACE PACKAGE Pkg_Fme_Campos_Genericos IS
   TYPE micursor IS REF CURSOR;

	PROCEDURE SP_FME_CAMPOS_VER(
    p_n_empresa           IN       NUMBER,
	p_n_codigo            IN       NUMBER,
	p_v_nombre            IN       VARCHAR2,
	p_c_cursor            OUT      micursor,
    p_v_DescError         OUT      VARCHAR2
   	);

	PROCEDURE SP_FME_CAMPOS_GRABAR(
    p_n_empresa        IN       NUMBER,
	p_v_Glosa          IN       VARCHAR2,
	p_c_FlagConvierte  IN       CHAR,
	p_c_CodCampoDB     IN       CHAR,
	p_v_usuario        IN       CHAR,
	p_n_codigo         IN OUT   NUMBER,
	p_v_DescError      OUT      VARCHAR2
    );

	PROCEDURE SP_FME_CAMPOS_BORRAR(
    p_n_empresa    IN       NUMBER,
    p_n_codigo     IN       NUMBER,
    p_v_DescError  OUT      VARCHAR2
    );

END Pkg_Fme_Campos_Genericos;
/
CREATE OR REPLACE PACKAGE BODY Pkg_Fme_Campos_Genericos AS
    PROCEDURE SP_FME_CAMPOS_VER (
	p_n_empresa           IN       NUMBER,
	p_n_codigo            IN       NUMBER,
	p_v_nombre            IN       VARCHAR2,
	p_c_cursor            OUT      micursor,
    p_v_DescError         OUT      VARCHAR2) AS

	BEGIN
	    p_v_DescError := '0000 Inicio';

		IF p_n_codigo > 0 THEN
			p_v_descerror := '0010 Buscando con Criterio Codigo';
            OPEN p_c_cursor FOR
            SELECT
		        IDE_EMPRESA,
				NRO_CORR_COLUMNA,
				GLS_COLUMNA,
				COD_IDE_CAMPO,
				Pkg_Fme_Parametros.FN_FME_GLOSA_PRMTRO(COD_IDE_CAMPO) CAMPO_DB,
				FLG_CONVERTIR,
				FCH_HRA_CREACION,
				COD_USR_CREACION,
				FCH_HRA_MODIFICA,
				COD_USR_MODIFICA
            FROM
				CSBPI.TA_COLUMNA_REMESA
            WHERE
				IDE_EMPRESA  = p_n_empresa AND
				NRO_CORR_COLUMNA  = p_n_codigo AND
				NRO_CORR_COLUMNA  <> 1;
				
		ELSIF p_v_nombre IS NOT NULL  THEN
			p_v_descerror := '0020 Buscando con Criterio Glosa';
         	OPEN p_c_cursor FOR
            SELECT
				IDE_EMPRESA,
				NRO_CORR_COLUMNA,
				GLS_COLUMNA,
				COD_IDE_CAMPO,
				Pkg_Fme_Parametros.FN_FME_GLOSA_PRMTRO(COD_IDE_CAMPO) CAMPO_DB,
				FLG_CONVERTIR,
				FCH_HRA_CREACION,
				COD_USR_CREACION,
				FCH_HRA_MODIFICA,
				COD_USR_MODIFICA
            FROM
				CSBPI.TA_COLUMNA_REMESA
            WHERE
				IDE_EMPRESA  = p_n_empresa AND
				GLS_COLUMNA LIKE p_v_nombre || '%' AND
				NRO_CORR_COLUMNA  <> 1				
            ORDER BY
				GLS_COLUMNA;
        ELSE
			p_v_descerror := '0030 Buscando sin Criterios';

		    OPEN p_c_cursor FOR
           	SELECT
		    	IDE_EMPRESA,
				NRO_CORR_COLUMNA,
				GLS_COLUMNA,
				COD_IDE_CAMPO,
				Pkg_Fme_Parametros.FN_FME_GLOSA_PRMTRO(COD_IDE_CAMPO) CAMPO_DB,
				FLG_CONVERTIR,
				FCH_HRA_CREACION,
				COD_USR_CREACION,
				FCH_HRA_MODIFICA,
				COD_USR_MODIFICA
            FROM
				CSBPI.TA_COLUMNA_REMESA
           	WHERE
				IDE_EMPRESA  = p_n_empresa AND
				NRO_CORR_COLUMNA  <> 1	
			ORDER BY
				GLS_COLUMNA;
		END IF;

		p_v_DescError := '9999 Termino';
	EXCEPTION
        WHEN OTHERS THEN
			OPEN p_c_cursor FOR SELECT * FROM DUAL;
			p_v_DescError := RTRIM (p_v_DescError) || ' >> ' ||
			                 SQLCODE || ' ' || SUBSTR (SQLERRM, 1, 700);
	END SP_FME_CAMPOS_VER;
	----------------------------------------------------------------------------
    PROCEDURE SP_FME_CAMPOS_GRABAR(
    p_n_empresa        IN       NUMBER,
	p_v_Glosa          IN       VARCHAR2,
	p_c_FlagConvierte  IN       CHAR,
	p_c_CodCampoDB     IN       CHAR,
	p_v_usuario        IN       CHAR,
	p_n_codigo         IN OUT   NUMBER,
	p_v_DescError      OUT      VARCHAR2) AS

	l_n_ide           NUMBER;
	l_d_fechahora     DATE;

	BEGIN
		 p_v_DescError := '0000 Inicio';
		 l_d_fechahora := TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY HH:MI:SS'),'DD/MM/YYYY HH:MI:SS');

		 p_v_descerror := '0010 Actualizando campo generico';
		 UPDATE CSBPI.TA_COLUMNA_REMESA
         SET GLS_COLUMNA = p_v_Glosa,
		 	 FLG_CONVERTIR = p_c_FlagConvierte,
             COD_IDE_CAMPO = p_c_CodCampoDB,
			 FCH_HRA_MODIFICA = l_d_fechahora,
			 COD_USR_MODIFICA = p_v_usuario
         WHERE
		 	 IDE_EMPRESA = p_n_empresa AND
			 NRO_CORR_COLUMNA = p_n_codigo;

		 IF SQL%ROWCOUNT <= 0 THEN
		 	 p_v_descerror := '0020 Siguiente Ide de secuencia Campo Generico';
             SELECT CSBPI.SC_FME_CAMPO_GENERICO.NEXTVAL
             INTO l_n_ide
             FROM DUAL;

		 	 p_v_descerror := '0040 Creando nuevo Campo Generico';
         	 INSERT INTO CSBPI.TA_COLUMNA_REMESA(
				 IDE_EMPRESA,
				 NRO_CORR_COLUMNA,
				 GLS_COLUMNA,
				 COD_IDE_CAMPO,
				 FLG_CONVERTIR,
				 FCH_HRA_CREACION,
				 COD_USR_CREACION,
				 FCH_HRA_MODIFICA,
				 COD_USR_MODIFICA)
             VALUES(
			     p_n_empresa,
				 l_n_ide,
				 p_v_Glosa,
				 p_c_CodCampoDB,
				 p_c_FlagConvierte,
			 	 l_d_fechahora,
			 	 p_v_usuario,
				 NULL,
				 NULL);

	     p_n_codigo := l_n_ide;
    END IF;
	COMMIT;
	
	p_v_DescError := '9999 Termino';
	EXCEPTION
        WHEN OTHERS THEN
		    p_v_descerror := RTRIM (p_v_descerror) || ' >> ' ||
			                 SQLCODE || ' ' || SUBSTR (SQLERRM, 1, 700);
    END SP_FME_CAMPOS_GRABAR;
----------------------------------------------------------------------------
	PROCEDURE SP_FME_CAMPOS_BORRAR(
    p_n_empresa      IN       NUMBER,
    p_n_codigo       IN       NUMBER,
    p_v_DescError    OUT      VARCHAR2) AS

	l_n_CantReg       NUMBER(11);

	BEGIN
	    p_v_DescError := '0000 Inicio';

		p_v_DescError := '0010 Verificando si existe un campo generico en la estructura de achivo por producto';
-- 		SELECT COUNT(*) INTO l_n_CantReg
-- 		FROM  CSBPI.TA_CAMPO_ARCHIVO_REMESA
-- 		WHERE IDE_EMPRESA = p_n_empresa AND
-- 			  NRO_CORR_COLUMNA = p_n_codigo;
-- 
-- 		IF l_n_CantReg > 0 THEN
-- 			p_v_DescError := '1111 Existe a lo menos un registro de estructura de achivo por producto,' || CHR(13) || 'ocupando el campo generico que desea eliminar' || CHR(13) || 'Favor de eliminar los registros relacionados, antes de eliminar el campo generico';
-- 			RETURN;
-- 		END IF;



		p_v_DescError := '0020 Eliminando el registro';
		DELETE
		FROM CSBPI.TA_COLUMNA_REMESA
		WHERE
			IDE_EMPRESA      = p_n_empresa AND
			NRO_CORR_COLUMNA = p_n_codigo;
		
		COMMIT;
			
		p_v_DescError := '9999 Termino';
	EXCEPTION
        WHEN OTHERS THEN
		p_v_DescError := RTRIM (p_v_DescError) || ' >> ' ||
			                 SQLCODE || ' ' || SUBSTR (SQLERRM, 1, 700);
    END SP_FME_CAMPOS_BORRAR;


END Pkg_Fme_Campos_Genericos;
/

