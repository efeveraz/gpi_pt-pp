Attribute VB_Name = "general_seguridad"
Option Explicit
Public gstrUsuario  As String     ' C�digo del Usuario Conectado
Public glngRut         As Long     ' Rut del Usuario Conectado
Public gLngEmpresa  As Long     ' C�digo de la Empresa
Public gTop         As Long     ' Posici�n Superior para el Formulario
Public gLeft        As Long     ' Posici�n Izquierda para el Formulario
Public gstrRuta     As String   ' Ruta Dll, mismo que App.path
Public gstrRutaMenu As String   ' Ruta Dll, mismo que App.path
Public gstrNomApl   As String   ' Nombre DLL, mismo que App.ExeName
Public gstrSepDec   As String   ' S�mbolo decimal
Public gAdoCon      As New ADODB.Connection
Public glngUsuario  As Long
Public gstrRutaRec  As String   ' Ruta Dll, mismo que App.path
Public glngOpcionMenu As Long   ' Ide. de la opci�n de men�


Public Sub valuar(ByVal strVariable As String, ByVal varValor As Variant)
Rem Factor40
    Select Case Trim(LCase(strVariable))
        
        Rem C�digo del usuario conectado (nombre de usuario)
        Case "gstrusuario"
            gstrUsuario = varValor
        
        Rem Rut del usuario conectado
        Case "glngrut"
            glngRut = varValor  'gRut = varValor
        
        Rem Identificador �nico de la empresa
        Case "glngempresa"
            gLngEmpresa = varValor
        
        Rem Posici�n superior para la aplicaci�n
        Case "gtop"
            gTop = varValor
        
        Rem Posici�n izquierda para la aplicaci�n
        Case "gleft"
            gLeft = varValor
        
        Rem Ruta de la aplicaci�n cargada (.dll)
        Case "gstrruta"
            gstrRuta = varValor
        
        Rem Ruta donde se encuentra el men�
        Rem ejemplo: 'K:\FACTOR40\FMMENUEM'
        Case "gstrrutamenu"
            gstrRutaMenu = varValor
        
        Rem Nombre de la Aplicaci�n.
        Case "gstrnomapl"
            gstrNomApl = varValor
        
        Rem Separador Decimal.
        Case "gstrsepdec"
            gstrSepDec = varValor   'gstrSetDec = varValor
        
        Rem Objeto conexi�n ADO
        Case "gadocon"
            Set gAdoCon = varValor
        
        Rem Ide �nico del Usuario.
        Case "glngusuario"
            glngUsuario = varValor
            
        Rem Ruta del directorio de Recursos
        Rem ejemplo: 'K:\FACTOR40\FMRECURS'
        Case "gstrrutarec"
            gstrRutaRec = varValor
            
        Rem Identificaci�n de la opci�n de men�
        Case "glngopcionmenu"
            glngOpcionMenu = varValor
            
        Case Else
            MsgBox "Error Fatal. Llamada incorrecta: VALUAR(" & strVariable & "," & varValor & ") ", vbCritical, "Error de Sistema"
    End Select

End Sub

