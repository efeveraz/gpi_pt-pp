VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMantCamposGen 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenci�n Campos Gen�ricos para Archivo"
   ClientHeight    =   3750
   ClientLeft      =   4575
   ClientTop       =   1710
   ClientWidth     =   5985
   Icon            =   "frmMantCamposGen.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3750
   ScaleWidth      =   5985
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraIngreso 
      Height          =   2295
      Left            =   60
      TabIndex        =   7
      Top             =   480
      Width           =   5835
      Begin VB.ComboBox cboCampo 
         Height          =   315
         ItemData        =   "frmMantCamposGen.frx":000C
         Left            =   1620
         List            =   "frmMantCamposGen.frx":000E
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Tag             =   "INGCAMP"
         Top             =   1020
         Width           =   2715
      End
      Begin VB.TextBox txtGlosa 
         Height          =   310
         Left            =   1620
         MaxLength       =   40
         TabIndex        =   0
         Tag             =   "INGCAMP"
         Top             =   660
         Width           =   4095
      End
      Begin VB.CheckBox chkConvierte 
         Alignment       =   1  'Right Justify
         Caption         =   "Convierte"
         Height          =   255
         Left            =   90
         TabIndex        =   2
         Tag             =   "INGCAMP"
         Top             =   1380
         Width           =   1710
      End
      Begin VB.Label lblCodigoIng2 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   1620
         TabIndex        =   12
         Tag             =   "INGCAMP"
         Top             =   300
         Width           =   1095
      End
      Begin VB.Label lblCodigoIng 
         Caption         =   "C�digo"
         Height          =   240
         Left            =   120
         TabIndex        =   11
         Top             =   345
         Width           =   540
      End
      Begin VB.Label lblCampo 
         Caption         =   "Campo"
         Height          =   240
         Left            =   120
         TabIndex        =   10
         Top             =   1050
         Width           =   1260
      End
      Begin VB.Label lblGlosa 
         Caption         =   "Nombre"
         Height          =   240
         Left            =   120
         TabIndex        =   9
         Top             =   720
         Width           =   720
      End
      Begin VB.Label lblLista 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Datos del Campo Gen�rico"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   0
         Left            =   0
         TabIndex        =   8
         Top             =   0
         Width           =   5805
      End
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   5
      Top             =   3480
      Width           =   5985
      _ExtentX        =   10557
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7929
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   5985
      _ExtentX        =   10557
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Grabar"
            Object.ToolTipText     =   "Grabar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Volver"
            Object.ToolTipText     =   "Volver"
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraBotones 
      Height          =   735
      Index           =   0
      Left            =   60
      TabIndex        =   13
      Top             =   2730
      Width           =   5835
      Begin VB.CommandButton cmdGrabar 
         Caption         =   "&Grabar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   75
         TabIndex        =   3
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   4275
         TabIndex        =   4
         Top             =   210
         Width           =   1425
      End
   End
End
Attribute VB_Name = "frmMantCamposGen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cboCampo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub chkConvierte_Click()
Dim blnPermiteConv As Boolean
    
    blnPermiteConv = True
    Select Case Trim(UCase(cboCampo.Text))
        Case "GLS_COMUNA"
        Case "COD_PAIS"
        Case "COD_TIPO_DOCTO"
        Case "COD_BANCO"
        Case "COD_PLAZA"
        Case "COD_FORMA_PAGO"
        Case "COD_MONEDA"
        Case Else
            blnPermiteConv = False
    End Select
    
    If Not blnPermiteConv Then
        If chkConvierte.Value <> 0 Then
            MsgBox "Campo seleccionado no permite Conversi�n", vbExclamation, Me.Caption
            chkConvierte.Value = 0
            cboCampo.SetFocus
        End If
    End If
     
End Sub

Private Sub chkConvierte_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cmdGrabar_Click()
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    staEstado.Panels(1) = "Grabando..."
    Screen.MousePointer = 11
    
    Rem Validaciones
    If Trim(txtGlosa) = "" Then
        MsgBox "Debe ingresar Nombre campo gen�rico", vbExclamation, "Campos Gen�ricos"
        txtGlosa.SetFocus
        GoTo LabelSalir
    End If
    If cboCampo.ListIndex = -1 Then
        MsgBox "Debe seleccionar campo base datos, asociado al campo gen�rico", vbExclamation, "Campos Gen�ricos"
        cboCampo.SetFocus
        GoTo LabelSalir
    End If
    Rem ***********************************************************************
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.PKG_FME_CAMPOS_GENERICOS.SP_FME_CAMPOS_GRABAR"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_Glosa", adVarChar, adParamInput, 40, Trim(txtGlosa.Text))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_FlagConvierte", adChar, adParamInput, 1, Val(chkConvierte.Value))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodCampoDB", adChar, adParamInput, 8, cboCampo.ItemData(cboCampo.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adChar, adParamInput, 10, gstrUsuario)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInputOutput, 10, Val(lblCodigoIng2))
    Cmd.Parameters.Append Cmd.CreateParameter("p_V_DescError", adVarChar, adParamOutput, 1000)
    Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser Grabada. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbExclamation, "Campos Gen�ricos"
        GoTo LabelSalir
    Else
        lblCodigoIng2 = Cmd.Parameters("p_n_codigo").Value
    End If
    glngCodigo = lblCodigoIng2
    MsgBox "informaci�n Grabada Exitosamente.", vbInformation, "Campos Gen�ricos"
    
    gblnswichRefresca = True
    If Not gblnModifica = True Then
        Call LimpiarControles(Me, "INGCAMP")
        frmCamposGenericos.cmdBuscar_Click
        Call PosicionaGrilla(frmCamposGenericos.grdCampos, glngCodigo, 2)
    Else
        Unload Me
        GoTo LabelSalir
    End If
    Call AlmacenarValoresCtls(Me, "INGCAMP")
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdVolver_Click()
    If HayCambiosEnControles(Me, "INGCAMP") Then
        If MsgBox("Existe Informaci�n sin Grabar." & Chr(13) & "�Desea volver sin grabar los cambios?", vbYesNo + vbInformation, Me.Caption) <> vbYes Then
            Exit Sub
        Else
            gblnswichRefresca = True
        End If
    End If
    Unload Me
End Sub

Private Sub Form_Load()
    Call Carga_Imagenes
    Rem LLena Combos
    Call Carga_Combo_Par(cboCampo, 1, 5)
End Sub
Private Sub Carga_Imagenes()
        
    tlbAcciones.ImageList = frmCamposGenericos.ilsIconos
    
    tlbAcciones.Buttons("Grabar").Image = 6
    tlbAcciones.Buttons("Volver").Image = 7
    
End Sub

Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)
On Error GoTo LabelError

    Select Case UCase(Trim(Button.Key))
        Case "GRABAR"
            Call cmdGrabar_Click
        Case "VOLVER"
            Call cmdVolver_Click
    End Select
    
LabelSalir:
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, , "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub txtGlosa_GotFocus()
    txtGlosa.SelStart = 0
    txtGlosa.SelLength = Len(txtGlosa)
End Sub

Private Sub txtGlosa_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub txtGlosa_LostFocus()
    txtGlosa.Text = UCase(txtGlosa.Text)
End Sub


