VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "ss32x25.ocx"
Begin VB.Form frmCamposGenericos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenci�n Campos Gen�ricos para Archivo"
   ClientHeight    =   7185
   ClientLeft      =   1005
   ClientTop       =   1875
   ClientWidth     =   9570
   Icon            =   "frmCamposGenericos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7185
   ScaleWidth      =   9570
   Begin VB.Frame fraLista 
      Height          =   4785
      Index           =   4
      Left            =   60
      TabIndex        =   15
      Top             =   1440
      Width           =   9435
      Begin FPSpread.vaSpread grdCampos 
         Height          =   4275
         Left            =   60
         TabIndex        =   2
         Top             =   435
         Width           =   9270
         _Version        =   131077
         _ExtentX        =   16351
         _ExtentY        =   7541
         _StockProps     =   64
         DisplayRowHeaders=   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   11
         MaxRows         =   0
         OperationMode   =   2
         ScrollBarExtMode=   -1  'True
         SelectBlockOptions=   0
         SpreadDesigner  =   "frmCamposGenericos.frx":000C
         VisibleCols     =   8
         VScrollSpecial  =   -1  'True
      End
      Begin VB.Label lblLista 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Lista de Campos"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   4
         Left            =   0
         TabIndex        =   16
         Top             =   90
         Width           =   9405
      End
   End
   Begin VB.Frame fraSeleccion 
      Height          =   975
      Index           =   4
      Left            =   60
      TabIndex        =   11
      Top             =   480
      Width           =   9435
      Begin VB.TextBox txtNombre 
         Height          =   310
         Left            =   3060
         MaxLength       =   40
         TabIndex        =   1
         Top             =   405
         Width           =   4995
      End
      Begin VB.TextBox txtCodigo 
         Height          =   310
         Left            =   780
         MaxLength       =   10
         TabIndex        =   0
         Top             =   405
         Width           =   1035
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   8340
         TabIndex        =   8
         Top             =   420
         Width           =   975
      End
      Begin VB.Label lblNombre 
         Caption         =   "Nombre"
         Height          =   240
         Left            =   2265
         TabIndex        =   14
         Top             =   450
         Width           =   615
      End
      Begin VB.Label lblCodigo 
         Caption         =   "C�digo"
         Height          =   240
         Left            =   120
         TabIndex        =   13
         Top             =   450
         Width           =   540
      End
      Begin VB.Label lblSeleccion 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Criterio de Selecci�n"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   4
         Left            =   0
         TabIndex        =   12
         Top             =   90
         Width           =   9405
      End
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   9
      Top             =   6915
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14253
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Agregar"
            Object.ToolTipText     =   "Agregar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Modificar"
            Object.ToolTipText     =   "Modificar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Eliminar"
            Object.ToolTipText     =   "Eliminar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Consultar"
            Object.ToolTipText     =   "Consultar"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Salir"
            Object.ToolTipText     =   "Salir"
         EndProperty
      EndProperty
      Begin MSComctlLib.ImageList ilsIconos 
         Left            =   2220
         Top             =   60
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
   Begin VB.Frame fraBotones 
      Height          =   735
      Index           =   4
      Left            =   60
      TabIndex        =   17
      Top             =   6180
      Width           =   9435
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   2955
         TabIndex        =   5
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdSalir 
         Caption         =   "&Salir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   7875
         TabIndex        =   7
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   75
         TabIndex        =   3
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   1515
         TabIndex        =   4
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   4400
         TabIndex        =   6
         Top             =   210
         Width           =   1425
      End
   End
End
Attribute VB_Name = "frmCamposGenericos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAgregar_Click()
    
        
    gblnswichRefresca = False
    gblnModifica = False
    frmMantCamposGen.fraIngreso.Enabled = True
    frmMantCamposGen.cmdGrabar.Enabled = True
    Call LimpiarControles(frmMantCamposGen, "INGCAMP")
    Call AlmacenarValoresCtls(frmMantCamposGen, "INGCAMP")
    frmMantCamposGen.Show 1
    
End Sub

Public Sub cmdBuscar_Click()
Dim strAux As String
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError

    staEstado.Panels(1) = "Buscando..."
    Screen.MousePointer = 11
    
    If Trim(txtNombre) <> "" Then txtCodigo.Text = ""
    
    Rem Validaci�n
    strAux = Trim(UCase(Replace(txtNombre, "'", "")))
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.PKG_FME_CAMPOS_GENERICOS.SP_FME_CAMPOS_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, Val(txtCodigo))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 40, strAux)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser cargada. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbInformation, "Campos Genericos"
        GoTo LabelSalir
    End If
    
    grdCampos.MaxRows = 0
    While Not recRegistros.EOF
        strAux = "" & recRegistros.Fields("COD_IDE_CAMPO").Value
        If Val(strAux) <> 0 Then
            grdCampos.MaxRows = grdCampos.MaxRows + 1
            grdCampos.Row = grdCampos.MaxRows
                    
            grdCampos.Col = 1: grdCampos.Text = "" & recRegistros.Fields("IDE_EMPRESA").Value
            grdCampos.Col = 2: grdCampos.Text = "" & recRegistros.Fields("NRO_CORR_COLUMNA").Value
            grdCampos.Col = 3: grdCampos.Text = "" & recRegistros.Fields("GLS_COLUMNA").Value
            If IsNull(recRegistros.Fields("FLG_CONVERTIR").Value) Then
                grdCampos.Col = 4: grdCampos.Text = ""
            ElseIf Val(recRegistros.Fields("FLG_CONVERTIR").Value) = 1 Then
                grdCampos.Col = 4: grdCampos.Text = "SI"
            Else
                grdCampos.Col = 4: grdCampos.Text = "NO"
            End If
            
            grdCampos.Col = 5: grdCampos.Text = "" & recRegistros.Fields("COD_IDE_CAMPO").Value
            grdCampos.Col = 6: grdCampos.Text = "" & recRegistros.Fields("CAMPO_DB").Value
            grdCampos.Col = 7: grdCampos.Text = "" & recRegistros.Fields("FCH_HRA_CREACION").Value
            grdCampos.Col = 8: grdCampos.Text = "" & recRegistros.Fields("COD_USR_CREACION").Value
            grdCampos.Col = 9: grdCampos.Text = "" & recRegistros.Fields("FCH_HRA_MODIFICA").Value
            grdCampos.Col = 10: grdCampos.Text = "" & recRegistros.Fields("COD_USR_MODIFICA").Value
        End If
        recRegistros.MoveNext
    Wend
    If frmMantCamposGen.ActiveControl Is Nothing Then
        grdCampos.SetFocus
    End If
    
    If grdCampos.MaxRows > 0 Then grdCampos.Row = 1

LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdConsultar_Click()
On Error GoTo LabelError

    If grdCampos.MaxRows = 0 Or grdCampos.Row = 0 Then Exit Sub
    
    
    gblnswichRefresca = False
    frmMantCamposGen.fraIngreso.Enabled = False
    frmMantCamposGen.cmdGrabar.Enabled = False
    
    grdCampos.Row = grdCampos.ActiveRow
    grdCampos.Col = 2: frmMantCamposGen.lblCodigoIng2 = Trim(grdCampos.Text)
    glngCodigo = Val(grdCampos.Text)
    grdCampos.Col = 3: frmMantCamposGen.txtGlosa = Trim(grdCampos.Text)
    grdCampos.Col = 5
    Call Pos_Cbo(frmMantCamposGen.cboCampo, Val(grdCampos.Text))
    
    grdCampos.Col = 4
    If UCase(Trim(grdCampos.Text)) = "NO" Then
        frmMantCamposGen.chkConvierte.Value = 0
    Else
        frmMantCamposGen.chkConvierte.Value = 1
    End If
    frmMantCamposGen.tlbAcciones.Buttons("Grabar").Visible = False
    Call AlmacenarValoresCtls(frmMantCamposGen, "INGCAMP")
    frmMantCamposGen.Show 1

LabelSalir:
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdEliminar_Click()
Dim strNombre As String
Dim lngCodigo As Long
Dim Cmd As New ADODB.Command
On Error GoTo LabelError

    If grdCampos.MaxRows = 0 Or grdCampos.Row = 0 Then Exit Sub
    
    
    staEstado.Panels(1) = "Eliminando..."
    Screen.MousePointer = 11
    
    grdCampos.Row = grdCampos.ActiveRow
    grdCampos.Col = 2: lngCodigo = Val(grdCampos.Text)
    grdCampos.Col = 3: strNombre = Trim(grdCampos.Text)
    
    
    If MsgBox("�Est� seguro de Eliminar campo gen�rico?" & Chr(13) & "C�digo  : " & lngCodigo & Chr(13) & "Nombre : " & strNombre, vbYesNo + vbQuestion, Me.Caption) <> vbYes Then
        GoTo LabelSalir
    Else
        Rem llamada a Procedimiento
        Cmd.ActiveConnection = gAdoCon
        Cmd.CommandType = adCmdStoredProc
        Cmd.CommandText = "CSBPI.PKG_FME_CAMPOS_GENERICOS.SP_FME_CAMPOS_BORRAR"
        
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, lngCodigo)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
        Cmd.Execute
    
        If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
            MsgBox "El registro no pudo ser Eliminado. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbExclamation, "Campos gen�ricos"
            GoTo LabelSalir
        End If
    
        MsgBox "Registro Eliminado Exitosamente.", vbInformation, "Campos gen�ricos"
        Call cmdBuscar_Click
        
        
    End If

LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdModificar_Click()
On Error GoTo LabelError
    
    If grdCampos.MaxRows = 0 Or grdCampos.Row = 0 Then Exit Sub
        
    gblnswichRefresca = False
    gblnModifica = True
    
    frmMantCamposGen.fraIngreso.Enabled = True
    frmMantCamposGen.cmdGrabar.Enabled = True
    
    grdCampos.Row = grdCampos.ActiveRow
    grdCampos.Col = 2: frmMantCamposGen.lblCodigoIng2 = Trim(grdCampos.Text)
    glngCodigo = Val(grdCampos.Text)
    grdCampos.Col = 3: frmMantCamposGen.txtGlosa = Trim(grdCampos.Text)
    grdCampos.Col = 5
    Call Pos_Cbo(frmMantCamposGen.cboCampo, Val(grdCampos.Text))
    
    grdCampos.Col = 4
    If UCase(Trim(grdCampos.Text)) = "NO" Then
        frmMantCamposGen.chkConvierte.Value = 0
    Else
        frmMantCamposGen.chkConvierte.Value = 1
    End If
    
    Call AlmacenarValoresCtls(frmMantCamposGen, "INGCAMP")
    frmMantCamposGen.Show 1
    If gblnswichRefresca Then
        Call cmdBuscar_Click
    End If
    
    Call PosicionaGrilla(grdCampos, glngCodigo, 2)

LabelSalir:
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call Carga_Imagenes
    Me.Top = 400
    Me.Left = 3090
End Sub

Private Sub Carga_Imagenes()
        
    ilsIconos.ListImages.Clear
    ilsIconos.ListImages.Add 1, "Agregar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\agregar.bmp")
    ilsIconos.ListImages.Add 2, "Modificar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\editar.bmp")
    ilsIconos.ListImages.Add 3, "Eliminar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\eliminar.bmp")
    ilsIconos.ListImages.Add 4, "Consultar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\consultar.bmp")
    ilsIconos.ListImages.Add 5, "Salir", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\salir.bmp")
    ilsIconos.ListImages.Add 6, "Grabar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\grabar.bmp")
    ilsIconos.ListImages.Add 7, "Volver", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\volver.bmp")
    
    
    tlbAcciones.ImageList = ilsIconos
    
    tlbAcciones.Buttons("Agregar").Image = 1
    tlbAcciones.Buttons("Modificar").Image = 2
    tlbAcciones.Buttons("Eliminar").Image = 3
    tlbAcciones.Buttons("Consultar").Image = 4
    tlbAcciones.Buttons("Salir").Image = 5


End Sub
Private Sub Form_Unload(Cancel As Integer)
    Unload Me
End Sub

Private Sub grdCampos_DblClick(ByVal Col As Long, ByVal Row As Long)
    If grdCampos.MaxRows = 0 Or grdCampos.Row = 0 Then Exit Sub
    Call cmdModificar_Click
End Sub

Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)
On Error GoTo LabelError
        
    Select Case UCase(Trim(Button.Key))
        Case "AGREGAR"
            Call cmdAgregar_Click
        Case "MODIFICAR"
            Call cmdModificar_Click
        Case "ELIMINAR"
            Call cmdEliminar_Click
        Case "CONSULTAR"
            Call cmdConsultar_Click
        Case "SALIR"
            Unload Me
    End Select
    
LabelSalir:
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, , "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub txtCodigo_GotFocus()
    txtCodigo.Text = ""
    txtNombre.Text = ""
    grdCampos.MaxRows = 0
End Sub

Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
    If Not Es_Numero(txtCodigo, KeyAscii, False) Then
        KeyAscii = 0
        Exit Sub
    End If
    If KeyAscii = 13 Then cmdBuscar_Click
End Sub

Private Sub txtCodigo_LostFocus()
    txtCodigo.Text = UCase(txtCodigo.Text)
End Sub

Private Sub txtNombre_GotFocus()
    txtNombre.SelStart = 0
    txtNombre.SelLength = Len(txtNombre)
End Sub

Private Sub txtNombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        txtCodigo.Text = ""
        Call cmdBuscar_Click
    End If
End Sub

Private Sub txtNombre_LostFocus()
    txtNombre.Text = UCase(txtNombre.Text)
End Sub


