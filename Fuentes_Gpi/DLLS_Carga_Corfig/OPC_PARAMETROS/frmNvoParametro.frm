VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmNvoParametro 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Par�metro"
   ClientHeight    =   4140
   ClientLeft      =   2760
   ClientTop       =   9510
   ClientWidth     =   7050
   Icon            =   "frmNvoParametro.frx":0000
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4140
   ScaleWidth      =   7050
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraParametros 
      Height          =   1935
      Left            =   45
      TabIndex        =   14
      Top             =   1260
      Width           =   6975
      Begin VB.CheckBox chkSistema 
         Alignment       =   1  'Right Justify
         Caption         =   "Par�m. de Sistema"
         Height          =   195
         Left            =   4905
         TabIndex        =   21
         Top             =   1500
         Width           =   1725
      End
      Begin VB.TextBox txtCodigoBco 
         Height          =   285
         Left            =   5190
         MaxLength       =   8
         TabIndex        =   4
         Top             =   1080
         Width           =   1440
      End
      Begin VB.TextBox txtCodigoAnt 
         Height          =   285
         Left            =   1725
         MaxLength       =   10
         TabIndex        =   3
         Top             =   1485
         Width           =   2115
      End
      Begin VB.TextBox txtDescripcion 
         Height          =   285
         Left            =   1740
         MaxLength       =   40
         TabIndex        =   1
         Top             =   675
         Width           =   4890
      End
      Begin VB.TextBox txtCodigo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1740
         TabIndex        =   0
         Top             =   285
         Width           =   975
      End
      Begin VB.TextBox txtNomCorto 
         Height          =   285
         Left            =   1725
         MaxLength       =   20
         TabIndex        =   2
         Top             =   1080
         Width           =   2115
      End
      Begin VB.CheckBox chkBlock 
         Alignment       =   1  'Right Justify
         Caption         =   "Registro Bloqueado"
         Enabled         =   0   'False
         Height          =   195
         Left            =   4815
         TabIndex        =   15
         Top             =   315
         Width           =   1815
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Datos del Par�metro"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Left            =   0
         TabIndex        =   23
         Top             =   0
         Width           =   6960
      End
      Begin VB.Label lblCodigoBco 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo Banco"
         Height          =   195
         Left            =   4050
         TabIndex        =   20
         Top             =   1095
         Width           =   1005
      End
      Begin VB.Label lblCodigoAnt 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo Anterior"
         Height          =   195
         Left            =   270
         TabIndex        =   19
         Top             =   1500
         Width           =   1080
      End
      Begin VB.Label lblDescripcion 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         Height          =   195
         Left            =   285
         TabIndex        =   18
         Top             =   690
         Width           =   840
      End
      Begin VB.Label lblCodigo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo"
         Height          =   195
         Left            =   285
         TabIndex        =   17
         Top             =   300
         Width           =   495
      End
      Begin VB.Label lblNomCorto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre Corto"
         Height          =   195
         Left            =   270
         TabIndex        =   16
         Top             =   1095
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Height          =   810
      Left            =   45
      TabIndex        =   12
      Top             =   420
      Width           =   6975
      Begin VB.TextBox txtDescripcionSubtipo 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   285
         Left            =   2640
         MaxLength       =   50
         TabIndex        =   9
         Top             =   345
         Width           =   4230
      End
      Begin VB.TextBox txtCodigoSubtipo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1440
         TabIndex        =   8
         Top             =   345
         Width           =   1170
      End
      Begin VB.Label lblParametros 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "SubTipo Seleccionado"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Left            =   0
         TabIndex        =   22
         Top             =   0
         Width           =   6960
      End
      Begin VB.Label lblSubtipo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "SubTipo"
         Height          =   195
         Left            =   285
         TabIndex        =   13
         Top             =   375
         Width           =   600
      End
   End
   Begin VB.Frame fraOpcion 
      Height          =   735
      Left            =   45
      TabIndex        =   11
      Top             =   3135
      Width           =   6975
      Begin VB.CommandButton cmdGrabar 
         Caption         =   "Grabar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   100
         TabIndex        =   5
         Top             =   200
         Width           =   1425
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "Volver"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   5440
         TabIndex        =   6
         Top             =   200
         Width           =   1425
      End
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   7050
      _ExtentX        =   12435
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Grabar"
            Object.ToolTipText     =   "Grabar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Volver"
            Object.ToolTipText     =   "Volver"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   10
      Top             =   3870
      Width           =   7050
      _ExtentX        =   12435
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9808
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmNvoParametro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public sOperationTipo As String

Private Sub cmdGrabar_Click()
Dim Cmd As New ADODB.Command
On Error GoTo Errores
    
    staEstado.Panels(1) = "Procesando"
    Screen.MousePointer = vbHourglass
    
    If Not Validado Then GoTo Salir
   
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.pkg_fme_parametros.sp_fme_prmtros_grabar"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigotipo", adNumeric, adParamInput, 10, 1)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigosubtipo", adNumeric, adParamInput, 10, Val(txtCodigoSubtipo))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigoparametro", adNumeric, adParamInput, 10, Val(txtCodigo))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descripcion", adVarChar, adParamInput, 40, txtDescripcion.Text)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nomcorto", adVarChar, adParamInput, 20, txtNomCorto.Text)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_sistema", adNumeric, adParamInput, 10, chkSistema.Value)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_codigoant", adVarChar, adParamInput, 10, txtCodigoAnt.Text)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_codigobco", adVarChar, adParamInput, 10, txtCodigoBco.Text)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_bloqueo", adNumeric, adParamInput, 10, chkBlock.Value)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adVarChar, adParamInput, 10, gstrUsuario)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser grabada" & Chr(13) & Cmd.Parameters("p_v_descerror").Value, vbInformation, "Par�metros"
    Else
        MsgBox "La informaci�n ha sido grabada", vbInformation, "Par�metros"
        frmParametros.Cargar_Parametros (Val(txtCodigoSubtipo))
        PosicionaGrilla frmParametros.grdParametros, txtDescripcion.Text, 2
    End If
    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Par�metros"
    
Salir:
    If txtCodigo = Empty Then Limpiar_Textos Else cmdVolver_Click
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
   
   staEstado.Panels(1) = "Listo"

End Sub
Private Sub Limpiar_Textos()
    txtCodigo.Text = ""
    txtDescripcion.Text = ""
    txtNomCorto.Text = ""
    txtCodigoAnt.Text = ""
    txtCodigoBco.Text = ""
    chkBlock.Value = 0
    chkSistema.Value = 0
End Sub

Private Function Validado() As Boolean
    Validado = True
    If txtDescripcion.Text = Empty Then
        MsgBox "Debe ingresar una Descripci�n", vbInformation, "Grabar"
        Validado = False
    End If
End Function


Private Sub cmdVolver_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.BackColor = gColorFondo
    
    Carga_Imagenes
    
    staEstado.Panels(2) = "V " & App.Major & "." & App.Minor
End Sub

Private Sub Carga_Imagenes()
        
    tlbAcciones.ImageList = frmParametros.ilsIconos
    tlbAcciones.Buttons("Grabar").Image = 5
    tlbAcciones.Buttons("Volver").Image = 6
    
End Sub

Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)
    txtDescripcion.Text = UCase(txtDescripcion.Text)
    txtNomCorto.Text = UCase(txtNomCorto.Text)
    txtCodigoAnt.Text = UCase(txtCodigoAnt.Text)
    txtCodigoBco.Text = UCase(txtCodigoBco.Text)
    
    Select Case Button.Key
        Case "Grabar"
            cmdGrabar_Click
        Case "Volver"
            cmdVolver_Click
    End Select

End Sub

Private Sub txtCodigoAnt_GotFocus()
    txtCodigoAnt.SelStart = 0
    txtCodigoAnt.SelLength = Len(txtCodigoAnt)
End Sub

Private Sub txtCodigoAnt_LostFocus()
    txtCodigoAnt.Text = UCase(txtCodigoAnt.Text)
End Sub


Private Sub txtCodigoBco_GotFocus()
    txtCodigoBco.SelStart = 0
    txtCodigoBco.SelLength = Len(txtCodigoBco)
End Sub

Private Sub txtCodigoBco_LostFocus()
    txtCodigoBco.Text = UCase(txtCodigoBco.Text)
End Sub


Private Sub txtDescripcion_GotFocus()
    txtDescripcion.SelStart = 0
    txtDescripcion.SelLength = Len(txtDescripcion)
End Sub

Private Sub txtDescripcion_LostFocus()
    txtDescripcion.Text = UCase(txtDescripcion.Text)
End Sub

Private Sub txtNomCorto_GotFocus()
    txtNomCorto.SelStart = 0
    txtNomCorto.SelLength = Len(txtNomCorto)
End Sub

Private Sub txtNomCorto_LostFocus()
    txtNomCorto.Text = UCase(txtNomCorto.Text)
End Sub


