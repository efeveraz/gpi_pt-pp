VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "ss32x25.ocx"
Begin VB.Form frmParametros 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenci�n de Par�metros"
   ClientHeight    =   6435
   ClientLeft      =   2205
   ClientTop       =   1800
   ClientWidth     =   9540
   Icon            =   "frmParametro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6435
   ScaleWidth      =   9540
   Begin VB.Frame fraSuibtipos 
      Height          =   4110
      Left            =   60
      TabIndex        =   8
      Top             =   1335
      Width           =   9435
      Begin FPSpread.vaSpread grdParametros 
         Height          =   3690
         Left            =   75
         TabIndex        =   9
         Top             =   300
         Width           =   9285
         _Version        =   131077
         _ExtentX        =   16378
         _ExtentY        =   6509
         _StockProps     =   64
         DisplayRowHeaders=   0   'False
         EditEnterAction =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   13
         MaxRows         =   0
         OperationMode   =   3
         ScrollBarExtMode=   -1  'True
         SelectBlockOptions=   0
         SpreadDesigner  =   "frmParametro.frx":000C
         StartingColNumber=   0
         UserResize      =   0
         VisibleCols     =   13
      End
      Begin VB.Label lblParametros 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Par�metros"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Left            =   0
         TabIndex        =   10
         Top             =   0
         Width           =   9465
      End
   End
   Begin VB.Frame fraTipos 
      Height          =   930
      Left            =   60
      TabIndex        =   7
      Top             =   435
      Width           =   9435
      Begin VB.ComboBox cmbSubtipos 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   825
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   435
         Width           =   4095
      End
      Begin VB.Label lblCodigoSubTipo 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   5760
         TabIndex        =   15
         Top             =   435
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         Height          =   195
         Left            =   5160
         TabIndex        =   14
         Top             =   480
         Width           =   675
      End
      Begin VB.Label lblSeleccion 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Criterios de Selecci�n"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Left            =   0
         TabIndex        =   13
         Top             =   90
         Width           =   9465
      End
      Begin VB.Label lblSubtipo 
         Caption         =   "SubTipo"
         Height          =   255
         Left            =   105
         TabIndex        =   12
         Top             =   480
         Width           =   945
      End
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9540
      _ExtentX        =   16828
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Agregar"
            Object.ToolTipText     =   "Agregar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Modificar"
            Object.ToolTipText     =   "Modificar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Eliminar"
            Object.ToolTipText     =   "Eliminar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Salir"
            Object.ToolTipText     =   "Salir"
         EndProperty
      EndProperty
      Begin MSComctlLib.ImageList ilsIconos 
         Left            =   4050
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
   Begin VB.Frame fraOpcion 
      Height          =   735
      Left            =   60
      TabIndex        =   5
      Top             =   5415
      Width           =   9435
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   3015
         TabIndex        =   3
         Top             =   200
         Width           =   1425
      End
      Begin VB.CommandButton cmdSalir 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   7900
         TabIndex        =   4
         Top             =   200
         Width           =   1425
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   100
         TabIndex        =   1
         Top             =   200
         Width           =   1425
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   1560
         TabIndex        =   2
         Top             =   200
         Width           =   1425
      End
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   6
      Top             =   6165
      Width           =   9540
      _ExtentX        =   16828
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14200
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub Carga_Imagenes()
    
    ilsIconos.ListImages.Clear
    ilsIconos.ListImages.Add 1, "Agregar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\agregar.bmp")
    ilsIconos.ListImages.Add 2, "Modificar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\editar.bmp")
    ilsIconos.ListImages.Add 3, "Eliminar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\eliminar.bmp")
    ilsIconos.ListImages.Add 4, "Salir", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\salir.bmp")
    ilsIconos.ListImages.Add 5, "Grabar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\grabar.bmp")
    ilsIconos.ListImages.Add 6, "Volver", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\volver.bmp")
    
    
    tlbAcciones.ImageList = ilsIconos
    tlbAcciones.Buttons("Agregar").Image = 1
    tlbAcciones.Buttons("Modificar").Image = 2
    tlbAcciones.Buttons("Eliminar").Image = 3
    tlbAcciones.Buttons("Salir").Image = 4
    
End Sub

Public Sub Cargar_Parametros(lngSubTipo As Long)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    staEstado.Panels(1) = "Buscando"
    Screen.MousePointer = vbHourglass
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.pkg_fme_parametros.sp_fme_subtipo_prmtros_ver"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigotipo", adNumeric, adParamInput, 10, 1)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigosubtipo", adNumeric, adParamInput, 10, lngSubTipo)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser cargada" & Chr(13) & Cmd.Parameters("p_v_descerror").Value, vbInformation, "Par�metros"
        GoTo LabelSalir
    End If
    
    grdParametros.MaxRows = 0
    Do While Not recRegistros.EOF
        grdParametros.MaxRows = grdParametros.MaxRows + 1
        grdParametros.Row = grdParametros.MaxRows

        grdParametros.Col = 1: grdParametros.Text = "" & recRegistros.Fields("IDE_UNICO_PARAM").Value
        grdParametros.Col = 2: grdParametros.Text = "" & recRegistros.Fields("gls_descripcion").Value
        grdParametros.Col = 3: grdParametros.Text = "" & recRegistros.Fields("nom_corto").Value
        grdParametros.Col = 4: grdParametros.Text = IIf("" & recRegistros.Fields("mrc_sistema").Value = "0", "NO", "SI")
        grdParametros.Col = 5: grdParametros.Text = "" & recRegistros.Fields("cod_anterior").Value
        grdParametros.Col = 6: grdParametros.Text = "" & recRegistros.Fields("cod_banco").Value
        grdParametros.Col = 7: grdParametros.Text = IIf("" & recRegistros.Fields("mrc_bloqueo").Value = "0", "NO", "SI")
        grdParametros.Col = 8: grdParametros.Text = "" & recRegistros.Fields("cod_usr_bloqueo").Value
        grdParametros.Col = 9: grdParametros.Text = "" & recRegistros.Fields("fch_hra_bloqueo").Value
        grdParametros.Col = 10: grdParametros.Text = "" & recRegistros.Fields("cod_usr_creacion").Value
        grdParametros.Col = 11: grdParametros.Text = "" & recRegistros.Fields("fch_hra_creacion").Value
        grdParametros.Col = 12: grdParametros.Text = "" & recRegistros.Fields("cod_usr_modifica").Value
        grdParametros.Col = 13: grdParametros.Text = "" & recRegistros.Fields("fch_hra_modifica").Value
        
        recRegistros.MoveNext
    Loop
    
    grdParametros.Col = 1
    grdParametros.Action = 0
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub Cargar_SubTipos()
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    staEstado.Panels(1) = "Buscando"
    Screen.MousePointer = vbHourglass
   
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.pkg_fme_parametros.sp_fme_subtipo_prmtros_ver"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigotipo", adNumeric, adParamInput, 10, 1)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigosubtipo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser cargada" & Chr(13) & Cmd.Parameters("p_v_descerror").Value, vbInformation, "SubTipos"
        GoTo LabelSalir
    End If

    
    cmbSubtipos.Clear
    Do While Not recRegistros.EOF
        cmbSubtipos.AddItem recRegistros.Fields("gls_descripcion").Value
        cmbSubtipos.ItemData(cmbSubtipos.NewIndex) = Val("" & recRegistros.Fields("ide_subtipo_param").Value)
        recRegistros.MoveNext
    Loop
    
    'If recRegistros.RecordCount > 0 Then cmbSubtipos.ListIndex = 0
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub



Private Sub cmbSubtipos_Click()
    lblCodigoSubTipo = cmbSubtipos.ItemData(cmbSubtipos.ListIndex)
    Call Cargar_Parametros(cmbSubtipos.ItemData(cmbSubtipos.ListIndex))
    
End Sub

Private Sub cmbTipos_Click()
    grdParametros.MaxRows = 0
    Cargar_SubTipos

End Sub

Private Sub cmdAgregar_Click()

    If cmbSubtipos.ListCount = 0 Or cmbSubtipos.ListIndex = -1 Then
        MsgBox "Seleccione SubTipo.", vbInformation, "Agregar"
        cmbSubtipos.SetFocus
        Exit Sub
    End If
    
    frmNvoParametro.Caption = "Agregar Par�metro"
    
    frmNvoParametro.txtDescripcionSubtipo = cmbSubtipos.Text
    
    frmNvoParametro.txtCodigoSubtipo = cmbSubtipos.ItemData(cmbSubtipos.ListIndex)
    frmNvoParametro.txtDescripcionSubtipo = cmbSubtipos.Text
    
    frmNvoParametro.Show vbModal
    
End Sub

Private Sub cmdEliminar_Click()
Dim Cmd As New ADODB.Command
   
      
   If grdParametros.MaxRows < 1 Then
        MsgBox "No existe informacion a  Eliminar", vbInformation, "Eliminar"
        GoTo Salir
   End If
    grdParametros.Row = grdParametros.ActiveRow
    
    grdParametros.Col = 4
    If grdParametros.Text = "SI" Then
        MsgBox "No se puede Eliminar un Par�metro de Sistema", vbInformation, "Eliminar"
        GoTo Salir
    End If
    
    grdParametros.Col = 2
    If MsgBox("� Est� seguro que desea eliminar el Par�metro " & grdParametros.Text & " ?", vbQuestion + vbYesNo + vbDefaultButton2, "Eliminar") = vbNo Then
        GoTo Salir
    End If
    
    staEstado.Panels(1) = "Procesando"
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
   
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.pkg_fme_parametros.sp_fme_prmtros_borrar"
    
    grdParametros.Col = 1
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigotipo", adNumeric, adParamInput, 10, 1)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigosubtipo", adNumeric, adParamInput, 10, cmbSubtipos.ItemData(cmbSubtipos.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigoparametro", adNumeric, adParamInput, 10, grdParametros.Text)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
            MsgBox "La informaci�n no pudo ser borrada" & Chr(13) & Cmd.Parameters("p_v_descerror").Value, vbInformation, "Par�metros"
    Else
        grdParametros.Action = 5
        grdParametros.MaxRows = grdParametros.MaxRows - 1
        MsgBox "La informaci�n ha sido borrada", vbInformation, "Par�metros"
    End If
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Par�metros"
    
Salir:
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    staEstado.Panels(1) = "Listo"

End Sub

Private Sub cmdModificar_Click()
Dim lngRow As Long
    
    
    If grdParametros.MaxRows = 0 Or grdParametros.Row = 0 Then Exit Sub
    
    grdParametros.Row = grdParametros.ActiveRow
    
    If cmbSubtipos.ListCount = 0 Or cmbSubtipos.ListIndex = -1 Then
        MsgBox "Seleccione SubTipo.", vbInformation, "Agregar"
        cmbSubtipos.SetFocus
        Exit Sub
    End If
       
    grdParametros.Col = 4
    If grdParametros.Text = "SI" Then
        MsgBox "No se puede Modificar un Par�metro de Sistema", vbInformation, "Modificar"
        Exit Sub
    End If
    
    
    If grdParametros.ActiveRow > 0 Then
        lngRow = grdParametros.ActiveRow
        grdParametros.Row = grdParametros.ActiveRow
        
        frmNvoParametro.Caption = "Modificar Par�metro"
                
        frmNvoParametro.txtCodigoSubtipo = Val(cmbSubtipos.ItemData(cmbSubtipos.ListIndex))
        frmNvoParametro.txtDescripcionSubtipo = cmbSubtipos.Text
                
        grdParametros.Col = 1: frmNvoParametro.txtCodigo.Text = Trim(grdParametros.Text)
        grdParametros.Col = 2: frmNvoParametro.txtDescripcion.Text = grdParametros.Text
        grdParametros.Col = 3: frmNvoParametro.txtNomCorto.Text = grdParametros.Text
        grdParametros.Col = 4: frmNvoParametro.chkSistema = IIf(grdParametros.Text = "NO", 0, 1)
        grdParametros.Col = 5: frmNvoParametro.txtCodigoAnt.Text = grdParametros.Text
        grdParametros.Col = 6: frmNvoParametro.txtCodigoBco.Text = grdParametros.Text
        grdParametros.Col = 7: frmNvoParametro.chkBlock.Value = IIf(grdParametros.Text = "NO", 0, 1)
        
        frmNvoParametro.fraParametros.Enabled = frmNvoParametro.chkSistema = 0
        frmNvoParametro.cmdGrabar.Enabled = frmNvoParametro.chkSistema = 0
        
        frmNvoParametro.Show vbModal
        
    End If


End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    Call Carga_Imagenes
    staEstado.Panels(2) = "V " & App.Major & "." & App.Minor
    
    Call Cargar_SubTipos
    
    Me.Top = 400
    Me.Left = 3090
    
End Sub

Private Sub grdParametros_DblClick(ByVal Col As Long, ByVal Row As Long)
    cmdModificar_Click
End Sub

Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "Agregar"
            cmdAgregar_Click
        Case "Modificar"
            cmdModificar_Click
        Case "Eliminar"
            cmdEliminar_Click
        Case "Salir"
            cmdSalir_Click
    End Select
End Sub


