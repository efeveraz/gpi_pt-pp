VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "ss32x25.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConvertir 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Conversión de datos de entrada"
   ClientHeight    =   7050
   ClientLeft      =   8925
   ClientTop       =   1380
   ClientWidth     =   8070
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Convertir.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7050
   ScaleWidth      =   8070
   Begin VB.Frame Frame3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3375
      Left            =   60
      TabIndex        =   15
      Top             =   2535
      Width           =   7935
      Begin FPSpread.vaSpread grdDatos 
         Height          =   2895
         Left            =   120
         TabIndex        =   16
         Top             =   360
         Width           =   7695
         _Version        =   131077
         _ExtentX        =   13573
         _ExtentY        =   5106
         _StockProps     =   64
         DisplayRowHeaders=   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   5
         MaxRows         =   0
         ScrollBarExtMode=   -1  'True
         SpreadDesigner  =   "Convertir.frx":000C
         UserResize      =   1
         VisibleCols     =   500
         VisibleRows     =   500
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Código de Datos a Convertir"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Left            =   0
         TabIndex        =   22
         Top             =   0
         Width           =   7900
      End
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   60
      TabIndex        =   4
      Top             =   525
      Width           =   7935
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Información el Campo de Entrada"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Left            =   0
         TabIndex        =   23
         Top             =   0
         Width           =   7905
      End
      Begin VB.Label lblCampo 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   1560
         TabIndex        =   18
         Top             =   1500
         Width           =   2535
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Campo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   5
         Left            =   120
         TabIndex        =   17
         Top             =   1545
         Width           =   585
      End
      Begin VB.Label lblSucursal 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   5160
         TabIndex        =   14
         Top             =   780
         Width           =   2535
      End
      Begin VB.Label lblSeccion 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   5160
         TabIndex        =   13
         Top             =   1140
         Width           =   2535
      End
      Begin VB.Label lblFormatoArch 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   1560
         TabIndex        =   12
         Top             =   1140
         Width           =   2535
      End
      Begin VB.Label lblProducto 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   1560
         TabIndex        =   11
         Top             =   780
         Width           =   2535
      End
      Begin VB.Label lblCliente 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   1560
         TabIndex        =   10
         Top             =   420
         Width           =   6135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Sucursal"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   4
         Left            =   4320
         TabIndex        =   9
         Top             =   870
         Width           =   720
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Sección"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   3
         Left            =   4320
         TabIndex        =   8
         Top             =   1185
         Width           =   645
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Formato Archivo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   1185
         Width           =   1365
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   825
         Width           =   750
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   465
         Width           =   585
      End
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   60
      TabIndex        =   3
      Top             =   5985
      Width           =   7935
      Begin VB.CommandButton cmdModificar 
         Caption         =   "&Modificar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   1550
         TabIndex        =   21
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   2985
         TabIndex        =   1
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   6360
         TabIndex        =   2
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton CmdGrabar 
         Caption         =   "&Aceptar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   120
         TabIndex        =   0
         Top             =   210
         Width           =   1425
      End
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   19
      Top             =   6780
      Width           =   8070
      _ExtentX        =   14235
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11607
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   20
      Top             =   0
      Width           =   8070
      _ExtentX        =   14235
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Aceptar"
            Object.ToolTipText     =   "Grabar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Modificar"
            Object.ToolTipText     =   "Modificar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Eliminar"
            Object.ToolTipText     =   "Eliminar"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Volver"
            Object.ToolTipText     =   "Volver"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmConvertir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Public gsConvertir      As String
    Public gsIdeEnti        As String
    Public gsRut            As String
    Public gsNombre         As String
    Public gsIdeProducto    As String
    Public gsNomProducto    As String
    Public gsIdeSeccion     As String
    Public gsNomSeccion     As String
    Public gsIdeFormato     As String
    Public gsNomFormato     As String
    Public gsIdeSucursal    As String
    Public gsNomSucursal    As String
    Public gsIdeCampo       As String
    Public gsNomCampo       As String
    Public gsTipoInforme    As String
'
Private Sub EliminarConversionDatoEnt(blnPorBoton As Boolean)
    Dim Cmd As New ADODB.Command

    On Error GoTo Errores
           
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_campos_x_cliente.sp_fme_conversion_borrar"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_encab", adNumeric, adParamInput, 10, glngCorrEncab)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_campo", adChar, adParamInput, 8, gsIdeCampo)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adVarChar, adParamInput, 10, gstrUsuario)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Cmd.Execute
     
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
         MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Borrando Conversión"
         GoTo Salir
    End If

    If blnPorBoton Then
        grdDatos.MaxRows = 0
        gsConvertir = "NO"
        Me.Hide
    End If

    
Salir:
    Set Cmd = Nothing
    Screen.MousePointer = vbNormal
    Exit Sub
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Borrando Conversión" '

End Sub

Private Sub TraeConversionDato()
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
           
    Screen.MousePointer = vbHourglass
    
    On Error GoTo Errores
           
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_campos_x_cliente.sp_fme_conversion_ver"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_encab", adNumeric, adParamInput, 10, glngCorrEncab)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_campo", adChar, adParamInput, 8, gsIdeCampo)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
     
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
         MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Detalle Conversión"
         GoTo Salir
    End If

    grdDatos.MaxRows = 0
    While Not recRegistros.EOF
        grdDatos.MaxRows = grdDatos.MaxRows + 1
        grdDatos.Row = grdDatos.MaxRows
        grdDatos.Col = 2: grdDatos.Text = "" & recRegistros.Fields("des_campo_entrada")
        grdDatos.Col = 3: grdDatos.Text = Format("" & recRegistros.Fields("cod_campo_salida"), "#0")
        grdDatos.Col = 4: grdDatos.Text = "" & recRegistros.Fields("des_campo_salida")
        grdDatos.Col = 5: grdDatos.Text = Format("" & recRegistros.Fields("cod_ide_campo"), "#0")
        recRegistros.MoveNext
    Wend

    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
    CmdGrabar.Enabled = grdDatos.MaxRows > 0
    cmdEliminar.Enabled = grdDatos.MaxRows > 0
    Screen.MousePointer = vbNormal
    Exit Sub
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Detalle Conversión" '
'Resume 0
End Sub

Private Sub TraeDatosDelCampoDeSalida()
    Dim Cmd As New ADODB.Command
    Dim strCampoSal     As String
    Dim lArrGeneral() As Variant
    Dim lngInd As Long
           
    Screen.MousePointer = vbHourglass
    
    On Error GoTo Errores
           
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    
    Cmd.CommandText = "dbo_factor.pkg_fme_parametros.sp_fme_invoca_fn"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_funcion", adNumeric, adParamInput, 1, 1)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_tipo", adNumeric, adParamInput, 1, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_subtipo", adNumeric, adParamInput, 1, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_unico", adChar, adParamInputOutput, 8, gsIdeCampo)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_glosa", adVarChar, adParamInputOutput, 40, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nomcorto", adVarChar, adParamInputOutput, 20, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Cmd.Execute

    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
         MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Campos Conversión"
         GoTo Salir
    End If

    strCampoSal = Cmd.Parameters("p_v_glosa").Value
    
    Dim Cbo As ComboBox
    
    Select Case UCase(strCampoSal)
        Case "GLS_COMUNA"
            ReDim lArrGeneral(0 To UBound(garrComunas, 1), 0 To 1)
            For lngInd = 0 To UBound(garrComunas, 1)
                lArrGeneral(lngInd, 0) = garrComunas(lngInd, 0)
                lArrGeneral(lngInd, 1) = garrComunas(lngInd, 1)
            Next
        
        Case "COD_PAIS"
            ReDim lArrGeneral(0 To UBound(garrPaises, 1), 0 To 1)
            For lngInd = 0 To UBound(garrPaises, 1)
                lArrGeneral(lngInd, 0) = garrPaises(lngInd, 0)
                lArrGeneral(lngInd, 1) = garrPaises(lngInd, 1)
            Next
        
        Case "COD_TIPO_DCTO"
            Carga_Combo_Documentos Cbo
            ReDim lArrGeneral(0 To Cbo.ListCount - 1, 0 To 1)
            For lngInd = 0 To Cbo.ListCount - 1
                Cbo.ListIndex = lngInd
                lArrGeneral(lngInd, 0) = Cbo.ItemData(lngInd)
                lArrGeneral(lngInd, 1) = Cbo.Text
            Next
        
        Case "COD_BANCO"
            ReDim lArrGeneral(0 To UBound(garrBancos, 1), 0 To 1)
            For lngInd = 0 To UBound(garrBancos, 1)
                lArrGeneral(lngInd, 0) = garrBancos(lngInd, 0)
                lArrGeneral(lngInd, 1) = garrBancos(lngInd, 1)
            Next
            
        Case "COD_PLAZA"
            ReDim lArrGeneral(0 To UBound(garrPlazas, 1), 0 To 1)
            For lngInd = 0 To UBound(garrPlazas, 1)
                lArrGeneral(lngInd, 0) = garrPlazas(lngInd, 0)
                lArrGeneral(lngInd, 1) = garrPlazas(lngInd, 1)
            Next
            
        Case "COD_FORMA_PAGO"
            Carga_Combo_Par Cbo, 1, 12
            ReDim lArrGeneral(0 To Cbo.ListCount - 1, 0 To 1)
            For lngInd = 0 To Cbo.ListCount - 1
                Cbo.ListIndex = lngInd
                lArrGeneral(lngInd, 0) = Cbo.ItemData(lngInd)
                lArrGeneral(lngInd, 1) = Cbo.Text
            Next
        
        Case "COD_MONEDA"
            ReDim lArrGeneral(0 To UBound(garrMonedas, 1), 0 To 1)
            For lngInd = 0 To UBound(garrMonedas, 1)
                lArrGeneral(lngInd, 0) = garrMonedas(lngInd, 0)
                lArrGeneral(lngInd, 1) = garrMonedas(lngInd, 1)
            Next
            
        Case Else
            gsConvertir = "NO"
            GoTo Salir
    End Select
    
    grdDatos.MaxRows = 0
    grdDatos.ReDraw = False
    For lngInd = 0 To UBound(lArrGeneral, 1)
        grdDatos.MaxRows = grdDatos.MaxRows + 1
        grdDatos.Row = grdDatos.MaxRows
        'grdDatos.Col = 2: grdDatos.Text = rec![PAR_GLS]
        grdDatos.Col = 3: grdDatos.Text = Format(lArrGeneral(lngInd, 0), "#0")
        grdDatos.Col = 4: grdDatos.Text = lArrGeneral(lngInd, 1)
        grdDatos.Col = 5: grdDatos.Text = strCampoSal
    Next
    grdDatos.ReDraw = True

Salir:

    Set Cmd = Nothing
    Screen.MousePointer = vbNormal
    CmdGrabar.Enabled = grdDatos.MaxRows > 0
    Exit Sub
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Campos Conversión"

End Sub

Private Sub cmdEliminar_Click()
    If MsgBox("Está Seguro de Eliminar la conversión del Campo", vbQuestion & vbYesNo, "eliminar") = vbYes Then
        Call EliminarConversionDatoEnt(True)
    End If
End Sub

Private Sub CmdGrabar_Click()
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    
    Dim i                                As Long
    Dim K                               As Long
    Dim IdeCampoEnt           As String
    Dim IdeDatoCampoEnt   As String
    Dim DesDatoCampoEnt   As String
    Dim IdeCampoSal           As String
    Dim IdeDatoCampoSal   As String
    Dim DesDatoCampoSal   As String
    Dim blnOk                        As Boolean

    If Not grdDatos.MaxRows > 0 Then
        Exit Sub
    Else
        For i = 1 To grdDatos.MaxRows
            grdDatos.Row = i
            grdDatos.Col = 2: DesDatoCampoEnt = Trim(grdDatos.Text)
            blnOk = False
            If DesDatoCampoEnt <> "" Then
                blnOk = True
                Exit For
            End If
        Next i
    End If
    
    If Not blnOk Then
        MsgBox "No hay información para convertir los Datos de Entrada", vbCritical, "Convertir datos de Entrada"
        Exit Sub
    End If
    
    Call EliminarConversionDatoEnt(False)
    
    
    If glngCorrEncab = 0 Then
        ' Agrega encabezado de  configuración.-
        
        Cmd.ActiveConnection = gAdoCon
        Cmd.CommandType = adCmdStoredProc
        Cmd.CommandText = "dbo_factor.pkg_fme_campos_x_cliente.sp_fme_formato_encab_grabar"
                
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_producto", adNumeric, adParamInput, 10, frmConfigura.cboProducto.ItemData(frmConfigura.cboProducto.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_entidad_cli", adNumeric, adParamInput, 10, frmConfigura.cboEntidad.ItemData(frmConfigura.cboEntidad.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_sucursal", adNumeric, adParamInput, 10, frmConfigura.cboEntidad.ItemData(frmConfigura.cboEntidad.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_archivo", adChar, adParamInput, 8, frmConfigura.cboArchivo.ItemData(frmConfigura.cboArchivo.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_formato", adChar, adParamInput, 8, frmConfigura.cboFormato.ItemData(frmConfigura.cboFormato.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_encab_ent", adNumeric, adParamInput, 10, 0)
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_separador", adChar, adParamInput, 10, frmConfigura.cboSeparador.Text)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adVarChar, adParamInput, 10, gstrUsuario)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_encab_sal", adNumeric, adParamOutput, 10)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
        Cmd.Execute
    
        If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
            MsgBox "La información no pudo ser grabada" & Chr(13) & Cmd.Parameters("p_v_descerror").Value, vbInformation, "Grabando Conversión"
           ' gAdoCon.RollbackTrans
            GoTo Salir
        End If
                    
        glngCorrEncab = Cmd.Parameters("p_n_corr_encab_sal").Value
    
    End If
    
    
    IdeCampoEnt = gsIdeCampo
    
    For i = 1 To grdDatos.MaxRows
    
        grdDatos.Row = i
        grdDatos.Col = 1: IdeDatoCampoEnt = Trim(grdDatos.Text)
        grdDatos.Col = 2: DesDatoCampoEnt = Trim(grdDatos.Text)
        grdDatos.Col = 3: IdeDatoCampoSal = Trim(grdDatos.Text)
        grdDatos.Col = 4: DesDatoCampoSal = Trim(grdDatos.Text)
        grdDatos.Col = 5: IdeCampoSal = Trim(grdDatos.Text)
    
        Set recRegistros = Nothing
        Set Cmd = Nothing
        
        Cmd.ActiveConnection = gAdoCon
        Cmd.CommandType = adCmdStoredProc
        Cmd.CommandText = "dbo_factor.pkg_fme_campos_x_cliente.sp_fme_conversion_grabar"
        
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_encab", adNumeric, adParamInput, 10, glngCorrEncab)
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_campo", adChar, adParamInput, 8, IdeCampoEnt)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_descampo_ent", adVarChar, adParamInput, 40, Left(DesDatoCampoEnt, 40))
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_codcampo_sal", adChar, adParamInput, 8, IdeDatoCampoSal)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_descampo_sal", adVarChar, adParamInput, 20, Left(DesDatoCampoSal, 20))
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adVarChar, adParamInput, 10, gstrUsuario)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
        Cmd.Execute

        If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
            MsgBox "La información no pudo ser grabada" & Chr(13) & Cmd.Parameters("p_v_descerror").Value, vbInformation, "Conversión"
            GoTo Salir
        End If
        gsConvertir = "SI"
    Next i

Salir:
    Screen.MousePointer = vbNormal
    Me.Hide
End Sub

Private Sub cmdModificar_Click()
    If grdDatos.MaxRows = 0 Then
        MsgBox "No existe información para Modificar", vbInformation, "Modificar"
        Exit Sub
    End If
    
    Load frmEdicion
    
    grdDatos.Row = grdDatos.ActiveRow
    
    frmEdicion.lblCampo = lblCampo
    frmEdicion.lblFila = grdDatos.Row
    grdDatos.Col = 2: frmEdicion.txtDesDatoEnt = grdDatos.Text
    grdDatos.Col = 3: frmEdicion.lblCodDatoSal = grdDatos.Text
    grdDatos.Col = 4: frmEdicion.lblDesDatoSal = grdDatos.Text
        
    frmEdicion.Show vbModal
End Sub

Private Sub cmdVolver_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    
    Carga_Imagenes
    staEstado.Panels(2) = "V " & App.Major & "." & App.Minor
    
    Me.Left = 2295 + 150
    Me.Top = 960 + 150
    lblCliente = gsNombre
    lblProducto = gsNomProducto
    lblFormatoArch = gsNomFormato
    lblSucursal = gsNomSucursal
    lblSeccion = gsNomSeccion
    lblCampo = gsNomCampo
    

    
    If UCase(gsConvertir) = "SI" Then
        TraeConversionDato
    Else
        TraeDatosDelCampoDeSalida
    End If
    
End Sub

Private Sub Carga_Imagenes()
        
    tlbAcciones.ImageList = frmConfigura.ilsIconos
    tlbAcciones.Buttons("Aceptar").Image = 1
    tlbAcciones.Buttons("Modificar").Image = 2
    tlbAcciones.Buttons("Eliminar").Image = 3
    tlbAcciones.Buttons("Volver").Image = 7
    
End Sub
Private Sub grdDatos_DblClick(ByVal Col As Long, ByVal Row As Long)
    cmdModificar_Click
End Sub


Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "Aceptar"
            CmdGrabar_Click
        Case "Modificar"
            cmdModificar_Click
        Case "Eliminar"
            cmdEliminar_Click
        Case "Volver"
            cmdVolver_Click
    End Select
End Sub


