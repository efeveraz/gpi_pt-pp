VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "ss32x25.ocx"
Begin VB.Form frmPreview 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Vista previa de la secci�n"
   ClientHeight    =   4200
   ClientLeft      =   2340
   ClientTop       =   1395
   ClientWidth     =   9405
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPreview.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   9405
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   23
      TabIndex        =   2
      Top             =   3360
      Width           =   9345
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   7800
         TabIndex        =   0
         Top             =   210
         Width           =   1425
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ejemplo del Formato"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3375
      Left            =   23
      TabIndex        =   1
      Top             =   0
      Width           =   9345
      Begin FPSpread.vaSpread sprdEjemplo 
         Height          =   2895
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   9135
         _Version        =   131077
         _ExtentX        =   16113
         _ExtentY        =   5106
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GridColor       =   16777215
         GridShowHoriz   =   0   'False
         GridShowVert    =   0   'False
         GridSolid       =   0   'False
         MaxCols         =   0
         MaxRows         =   0
         ScrollBarExtMode=   -1  'True
         SpreadDesigner  =   "frmPreview.frx":000C
      End
   End
End
Attribute VB_Name = "frmPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public sprdAsignados  As Object
Public gsSeccion        As String
'

Private Sub Puebla_Ejemplo()
Dim intAncho     As Integer
Dim intCont      As Integer
Dim intCont2     As Integer
Dim intCont3     As Integer
Dim intDec       As Integer
Dim intRow       As Integer
Dim strAtributo  As String
Dim strCampo     As String
Dim Formato      As String
Dim strRell      As String
Dim strTexto     As String
Dim intPrec      As Integer
    
    sprdEjemplo.MaxCols = sprdAsignados.MaxRows
    sprdEjemplo.MaxRows = 1
    For intRow = 1 To sprdAsignados.MaxRows
        sprdAsignados.Row = intRow

        ' Establecer titulos.-
        sprdEjemplo.Row = 0
        sprdAsignados.Col = 1
        sprdEjemplo.Col = intRow
        sprdEjemplo.Text = sprdAsignados.Text
        strCampo = sprdAsignados.Text

        ' Establecer precisi�n.-
        sprdAsignados.Col = 4
        intDec = Val(sprdAsignados.Text)
        sprdAsignados.Col = 3
        sprdEjemplo.ColWidth(intRow) = 12 ' sprdAsignados.Value + intDec
        intAncho = Val(sprdAsignados.Value)
'
        sprdAsignados.Col = 3
        If Not sprdAsignados.Text = "" Then
            intPrec = sprdAsignados.Value
        Else
            sprdAsignados.Value = 0
            intPrec = 0
        End If

        sprdAsignados.Col = 2
        For intCont2 = 1 To 1
            strTexto = ""
            sprdEjemplo.Row = intCont2
            If sprdAsignados.Text = "Fecha" Then
                sprdAsignados.Col = 8
                Formato = sprdAsignados.Text
                sprdEjemplo.Text = Format(Date, Formato)
            Else
                If intPrec > 4 Then intPrec = intPrec - Int(Rnd() * 4)
                If sprdAsignados.Text = "Num�rico" Then
                    For intCont3 = 1 To intPrec - intDec
                        strTexto = strTexto & Trim(Str(Int(Rnd(Timer) * 10)))
                    Next
                    If intDec > 0 Then
                        strTexto = strTexto + "." + String(intDec, "0")
                    End If
                Else
                    strTexto = strTexto & String(intPrec, "A")
                End If
                sprdEjemplo.Row = intCont2
                sprdEjemplo.Text = strTexto
            End If
        Next

        For intCont = 1 To 1
            sprdEjemplo.Row = intCont

            ' Establecer alineaci�n.-
            sprdAsignados.Col = 5
            sprdEjemplo.TypeHAlign = IIf(sprdAsignados.TypeButtonText = "Izquierda", 0, 1)

            ' Establecer Relleno.-
            sprdAsignados.Col = 6
            If sprdAsignados.TypeButtonText = "Ceros" Then
                strRell = "0"
            Else
                strRell = " "
            End If


            If sprdEjemplo.TypeHAlign = 1 Then
                ' a la Izquierda.-
                sprdEjemplo.Text = Right(String(intAncho, strRell) + Trim(sprdEjemplo.Text), intAncho)
            Else
                ' a la derecha.-
                sprdEjemplo.Text = Left(Trim(sprdEjemplo.Text) + String(intAncho, strRell), intAncho)
            End If
        Next

    Next
End Sub

Private Sub cmdVolver_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Left = 2295 + 50
    Me.Top = 960 + 50
    Me.Caption = Me.Caption & " [" & gsSeccion & "]"
    Puebla_Ejemplo
End Sub

