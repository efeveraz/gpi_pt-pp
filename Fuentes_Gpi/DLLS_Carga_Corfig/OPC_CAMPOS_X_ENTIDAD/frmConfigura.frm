VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "ss32x25.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmConfigura 
   Caption         =   "Configuraci�n de Archivo Entidad para Carga "
   ClientHeight    =   7140
   ClientLeft      =   885
   ClientTop       =   2175
   ClientWidth     =   9570
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmConfigura.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7140
   ScaleWidth      =   9570
   Begin VB.Frame fraMantencion 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Left            =   30
      TabIndex        =   39
      Top             =   1665
      Width           =   9495
      Begin TabDlg.SSTab tabSecciones 
         Height          =   3675
         Left            =   120
         TabIndex        =   41
         Top             =   420
         Width           =   9255
         _ExtentX        =   16325
         _ExtentY        =   6482
         _Version        =   393216
         Tabs            =   4
         Tab             =   2
         TabsPerRow      =   6
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "  Encabezado   "
         TabPicture(0)   =   "frmConfigura.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "fraEncabezado"
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "    Detalle    "
         TabPicture(1)   =   "frmConfigura.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraDetalle"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "     Totales     "
         TabPicture(2)   =   "frmConfigura.frx":0044
         Tab(2).ControlEnabled=   -1  'True
         Tab(2).Control(0)=   "Frame1"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Ordenamiento"
         TabPicture(3)   =   "frmConfigura.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "Frame3"
         Tab(3).ControlCount=   1
         Begin VB.Frame Frame3 
            Height          =   3210
            Left            =   -74925
            TabIndex        =   48
            Top             =   345
            Width           =   9120
            Begin VB.CommandButton cmdSubirOrden 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   8775
               Picture         =   "frmConfigura.frx":007C
               Style           =   1  'Graphical
               TabIndex        =   35
               ToolTipText     =   "Subir campo"
               Top             =   1230
               Width           =   260
            End
            Begin VB.CommandButton cmdBajarOrden 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   8775
               Picture         =   "frmConfigura.frx":017E
               Style           =   1  'Graphical
               TabIndex        =   34
               ToolTipText     =   "Bajar campos"
               Top             =   1530
               Width           =   260
            End
            Begin VB.CommandButton cmdSacarTodoOrdena 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":0280
               Style           =   1  'Graphical
               TabIndex        =   33
               ToolTipText     =   "Sacar todos"
               Top             =   1965
               Width           =   300
            End
            Begin VB.CommandButton cmdPonerTodoOrdena 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":0382
               Style           =   1  'Graphical
               TabIndex        =   32
               ToolTipText     =   "Pasar todos"
               Top             =   1665
               Width           =   300
            End
            Begin VB.CommandButton cmdSacarOrdena 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":0484
               Style           =   1  'Graphical
               TabIndex        =   31
               ToolTipText     =   "Sacar un campo"
               Top             =   1170
               Width           =   285
            End
            Begin VB.CommandButton cmdPonerOrdena 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":0586
               Style           =   1  'Graphical
               TabIndex        =   30
               ToolTipText     =   "Pasar un campos"
               Top             =   870
               Width           =   285
            End
            Begin FPSpread.vaSpread grdOrdena 
               Height          =   2415
               Left            =   2880
               TabIndex        =   49
               Top             =   315
               Width           =   5850
               _Version        =   131077
               _ExtentX        =   10319
               _ExtentY        =   4260
               _StockProps     =   64
               AllowDragDrop   =   -1  'True
               BackColorStyle  =   3
               DAutoSizeCols   =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               GrayAreaBackColor=   16777215
               GridColor       =   16777215
               MaxCols         =   5
               MaxRows         =   1
               RetainSelBlock  =   0   'False
               ScrollBarExtMode=   -1  'True
               SpreadDesigner  =   "frmConfigura.frx":0688
               UserResize      =   1
               VisibleCols     =   500
               VisibleRows     =   500
            End
            Begin FPSpread.vaSpread grdDispOrdena 
               Height          =   2400
               Left            =   135
               TabIndex        =   58
               Top             =   315
               Width           =   2295
               _Version        =   131077
               _ExtentX        =   4048
               _ExtentY        =   4233
               _StockProps     =   64
               DisplayColHeaders=   0   'False
               DisplayRowHeaders=   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               GrayAreaBackColor=   16777215
               GridShowHoriz   =   0   'False
               GridShowVert    =   0   'False
               MaxCols         =   3
               MaxRows         =   0
               MoveActiveOnFocus=   0   'False
               OperationMode   =   2
               RetainSelBlock  =   0   'False
               ScrollBarExtMode=   -1  'True
               ScrollBars      =   2
               SelectBlockOptions=   0
               SpreadDesigner  =   "frmConfigura.frx":17C0
               UserResize      =   0
               VisibleCols     =   500
               VisibleRows     =   500
            End
         End
         Begin VB.Frame Frame1 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3210
            Left            =   75
            TabIndex        =   44
            Top             =   345
            Width           =   9120
            Begin VB.CommandButton cmdFillerTot 
               Caption         =   "Filler >>"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   400
               Left            =   120
               TabIndex        =   23
               Top             =   2745
               Width           =   1425
            End
            Begin VB.CommandButton cmdBajarTot 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   8775
               Picture         =   "frmConfigura.frx":1A26
               Style           =   1  'Graphical
               TabIndex        =   28
               ToolTipText     =   "Bajar campos"
               Top             =   1530
               Width           =   260
            End
            Begin VB.CommandButton cmdSubirTot 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   8775
               Picture         =   "frmConfigura.frx":1B28
               Style           =   1  'Graphical
               TabIndex        =   29
               ToolTipText     =   "Subir campo"
               Top             =   1230
               Width           =   260
            End
            Begin VB.CommandButton cmdPonerTot 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":1C2A
               Style           =   1  'Graphical
               TabIndex        =   24
               ToolTipText     =   "Poner un campo"
               Top             =   870
               Width           =   285
            End
            Begin VB.CommandButton cmdSacarTot 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":1D2C
               Style           =   1  'Graphical
               TabIndex        =   25
               ToolTipText     =   "Sacar un campo"
               Top             =   1170
               Width           =   285
            End
            Begin VB.CommandButton cmdPonerTodTot 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":1E2E
               Style           =   1  'Graphical
               TabIndex        =   26
               ToolTipText     =   "Poner todos"
               Top             =   1665
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.CommandButton cmdSacarTodTot 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":1F30
               Style           =   1  'Graphical
               TabIndex        =   27
               ToolTipText     =   "Sacar todos"
               Top             =   1965
               Visible         =   0   'False
               Width           =   300
            End
            Begin FPSpread.vaSpread grdAsignadosTot 
               Height          =   2415
               Left            =   2880
               TabIndex        =   47
               Top             =   315
               Width           =   5850
               _Version        =   131077
               _ExtentX        =   10319
               _ExtentY        =   4260
               _StockProps     =   64
               AllowDragDrop   =   -1  'True
               BackColorStyle  =   3
               DAutoSizeCols   =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               GrayAreaBackColor=   16777215
               GridColor       =   16777215
               MaxCols         =   13
               MaxRows         =   1
               RetainSelBlock  =   0   'False
               ScrollBarExtMode=   -1  'True
               SpreadDesigner  =   "frmConfigura.frx":2032
               UserResize      =   1
               VisibleCols     =   500
               VisibleRows     =   500
            End
            Begin FPSpread.vaSpread grdDispTot 
               Height          =   2400
               Left            =   135
               TabIndex        =   57
               Top             =   315
               Width           =   2295
               _Version        =   131077
               _ExtentX        =   4048
               _ExtentY        =   4233
               _StockProps     =   64
               DisplayColHeaders=   0   'False
               DisplayRowHeaders=   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               GrayAreaBackColor=   16777215
               GridShowHoriz   =   0   'False
               GridShowVert    =   0   'False
               MaxCols         =   6
               MaxRows         =   0
               MoveActiveOnFocus=   0   'False
               OperationMode   =   2
               RetainSelBlock  =   0   'False
               ScrollBarExtMode=   -1  'True
               ScrollBars      =   2
               SelectBlockOptions=   0
               SpreadDesigner  =   "frmConfigura.frx":26DE
               UserResize      =   0
               VisibleCols     =   6
            End
         End
         Begin VB.Frame fraEncabezado 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3210
            Left            =   -74925
            TabIndex        =   43
            Top             =   345
            Width           =   9120
            Begin FPSpread.vaSpread grdDispEncab 
               Height          =   2400
               Left            =   120
               TabIndex        =   55
               Top             =   315
               Width           =   2295
               _Version        =   131077
               _ExtentX        =   4048
               _ExtentY        =   4233
               _StockProps     =   64
               DisplayColHeaders=   0   'False
               DisplayRowHeaders=   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               GrayAreaBackColor=   16777215
               GridShowHoriz   =   0   'False
               GridShowVert    =   0   'False
               MaxCols         =   6
               MaxRows         =   0
               MoveActiveOnFocus=   0   'False
               OperationMode   =   2
               RetainSelBlock  =   0   'False
               ScrollBarExtMode=   -1  'True
               ScrollBars      =   2
               SelectBlockOptions=   0
               SpreadDesigner  =   "frmConfigura.frx":29CB
               UserResize      =   0
               VisibleCols     =   500
               VisibleRows     =   500
            End
            Begin VB.CommandButton cmdFillerEncab 
               Caption         =   "Filler >>"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   400
               Left            =   120
               TabIndex        =   9
               Top             =   2745
               Width           =   1425
            End
            Begin VB.CommandButton cmdSacarTodEncab 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":2CCB
               Style           =   1  'Graphical
               TabIndex        =   13
               ToolTipText     =   "Sacar todos"
               Top             =   1965
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.CommandButton cmdPonerTodEncab 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":2DCD
               Style           =   1  'Graphical
               TabIndex        =   12
               ToolTipText     =   "Poner todos"
               Top             =   1665
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.CommandButton cmdSacarEncab 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":2ECF
               Style           =   1  'Graphical
               TabIndex        =   11
               ToolTipText     =   "Sacar un campo"
               Top             =   1170
               Width           =   285
            End
            Begin VB.CommandButton cmdPonerEncab 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":2FD1
               Style           =   1  'Graphical
               TabIndex        =   10
               ToolTipText     =   "Poner un campo"
               Top             =   870
               Width           =   285
            End
            Begin VB.CommandButton cmdSubirEncab 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   8775
               Picture         =   "frmConfigura.frx":30D3
               Style           =   1  'Graphical
               TabIndex        =   15
               ToolTipText     =   "Subir campo"
               Top             =   1230
               Width           =   260
            End
            Begin VB.CommandButton cmdBajarEncab 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   8775
               Picture         =   "frmConfigura.frx":31D5
               Style           =   1  'Graphical
               TabIndex        =   14
               ToolTipText     =   "Bajar campos"
               Top             =   1530
               Width           =   260
            End
            Begin FPSpread.vaSpread grdAsignadosEncab 
               Height          =   2415
               Left            =   2880
               TabIndex        =   45
               Top             =   315
               Width           =   5850
               _Version        =   131077
               _ExtentX        =   10319
               _ExtentY        =   4260
               _StockProps     =   64
               AllowDragDrop   =   -1  'True
               BackColorStyle  =   3
               DAutoSizeCols   =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               GrayAreaBackColor=   16777215
               GridColor       =   16777215
               MaxCols         =   13
               MaxRows         =   1
               RetainSelBlock  =   0   'False
               ScrollBarExtMode=   -1  'True
               SpreadDesigner  =   "frmConfigura.frx":32D7
               UserResize      =   1
               VisibleCols     =   500
               VisibleRows     =   500
            End
         End
         Begin VB.Frame fraDetalle 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3210
            Left            =   -74925
            TabIndex        =   42
            Top             =   345
            Width           =   9120
            Begin VB.CommandButton cmdFillerDet 
               Caption         =   "Filler >>"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   400
               Left            =   120
               TabIndex        =   16
               Top             =   2745
               Width           =   1425
            End
            Begin VB.CommandButton cmdBajar 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   8775
               Picture         =   "frmConfigura.frx":39B1
               Style           =   1  'Graphical
               TabIndex        =   21
               ToolTipText     =   "Bajar campos"
               Top             =   1530
               Width           =   260
            End
            Begin VB.CommandButton cmdSubir 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   8775
               Picture         =   "frmConfigura.frx":3AB3
               Style           =   1  'Graphical
               TabIndex        =   22
               ToolTipText     =   "Subir campo"
               Top             =   1230
               Width           =   260
            End
            Begin VB.CommandButton cmdPoner 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":3BB5
               Style           =   1  'Graphical
               TabIndex        =   17
               ToolTipText     =   "Pasar un campos"
               Top             =   870
               Width           =   285
            End
            Begin VB.CommandButton cmdSacar 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":3CB7
               Style           =   1  'Graphical
               TabIndex        =   18
               ToolTipText     =   "Sacar un campo"
               Top             =   1170
               Width           =   285
            End
            Begin VB.CommandButton cmdPonerTodos 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":3DB9
               Style           =   1  'Graphical
               TabIndex        =   19
               ToolTipText     =   "Pasar todos"
               Top             =   1665
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.CommandButton cmdSacarTodos 
               Height          =   300
               Left            =   2520
               Picture         =   "frmConfigura.frx":3EBB
               Style           =   1  'Graphical
               TabIndex        =   20
               ToolTipText     =   "Sacar todos"
               Top             =   1965
               Visible         =   0   'False
               Width           =   300
            End
            Begin FPSpread.vaSpread grdAsignados 
               Height          =   2415
               Left            =   2880
               TabIndex        =   46
               Top             =   315
               Width           =   5850
               _Version        =   131077
               _ExtentX        =   10319
               _ExtentY        =   4260
               _StockProps     =   64
               AllowDragDrop   =   -1  'True
               BackColorStyle  =   3
               DAutoSizeCols   =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               GrayAreaBackColor=   16777215
               GridColor       =   16777215
               MaxCols         =   15
               MaxRows         =   1
               RetainSelBlock  =   0   'False
               ScrollBarExtMode=   -1  'True
               SpreadDesigner  =   "frmConfigura.frx":3FBD
               UserResize      =   1
               VisibleCols     =   500
               VisibleRows     =   500
            End
            Begin FPSpread.vaSpread grdDisponibles 
               Height          =   2400
               Left            =   135
               TabIndex        =   56
               Top             =   315
               Width           =   2295
               _Version        =   131077
               _ExtentX        =   4048
               _ExtentY        =   4233
               _StockProps     =   64
               DisplayColHeaders=   0   'False
               DisplayRowHeaders=   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               GrayAreaBackColor=   16777215
               GridShowHoriz   =   0   'False
               GridShowVert    =   0   'False
               MaxCols         =   8
               MaxRows         =   0
               MoveActiveOnFocus=   0   'False
               OperationMode   =   2
               RetainSelBlock  =   0   'False
               ScrollBarExtMode=   -1  'True
               ScrollBars      =   2
               SelectBlockOptions=   0
               SpreadDesigner  =   "frmConfigura.frx":46ED
               UserResize      =   0
               VisibleCols     =   500
               VisibleRows     =   500
            End
         End
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Configuraci�n de Secciones del Archivo Entrada"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Left            =   0
         TabIndex        =   52
         Top             =   45
         Width           =   9465
      End
   End
   Begin VB.Frame fraCriterios 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1140
      Left            =   30
      TabIndex        =   36
      Top             =   510
      Width           =   9495
      Begin VB.ComboBox cboDecimal 
         Height          =   330
         Left            =   7820
         Style           =   2  'Dropdown List
         TabIndex        =   62
         Top             =   300
         Width           =   630
      End
      Begin VB.ComboBox cboEntidad 
         Height          =   330
         Left            =   1035
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   60
         Top             =   680
         Width           =   4110
      End
      Begin VB.ComboBox cboSeparador 
         Height          =   330
         Left            =   7820
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   690
         Width           =   630
      End
      Begin VB.ComboBox cboArchivo 
         Enabled         =   0   'False
         Height          =   330
         Left            =   5955
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   315
         Width           =   1455
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   8535
         TabIndex        =   4
         Top             =   645
         Width           =   870
      End
      Begin VB.ComboBox cboProducto 
         Height          =   330
         Left            =   1035
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   315
         Width           =   4110
      End
      Begin VB.ComboBox cboFormato 
         Height          =   330
         Left            =   5955
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   680
         Width           =   1500
      End
      Begin VB.Label Label5 
         Caption         =   "Dec"
         Height          =   255
         Left            =   7500
         TabIndex        =   63
         Top             =   360
         Width           =   375
      End
      Begin VB.Label lblComun 
         AutoSize        =   -1  'True
         Caption         =   "Entidad"
         Height          =   210
         Index           =   0
         Left            =   80
         TabIndex        =   61
         Top             =   750
         Width           =   525
      End
      Begin VB.Label Label4 
         Caption         =   "Sep."
         Height          =   255
         Left            =   7515
         TabIndex        =   59
         Top             =   750
         Width           =   375
      End
      Begin VB.Label Label3 
         Caption         =   "Direcci�n"
         Height          =   255
         Left            =   5205
         TabIndex        =   54
         Top             =   390
         Width           =   825
      End
      Begin VB.Label lblLista 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Criterios de Selecci�n"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Left            =   0
         TabIndex        =   51
         Top             =   0
         Width           =   9465
      End
      Begin VB.Label lblComun 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Archivo"
         Height          =   210
         Index           =   3
         Left            =   65
         TabIndex        =   38
         Top             =   390
         Width           =   915
      End
      Begin VB.Label Label1 
         Caption         =   "Formato"
         Height          =   255
         Left            =   5205
         TabIndex        =   37
         Top             =   750
         Width           =   735
      End
   End
   Begin VB.Frame fraBotones 
      Height          =   735
      Left            =   30
      TabIndex        =   40
      Top             =   6150
      Width           =   9495
      Begin VB.CommandButton cmdPreview 
         Caption         =   "Prevista"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   2995
         TabIndex        =   7
         ToolTipText     =   "Vista Preliminar"
         Top             =   225
         Width           =   1425
      End
      Begin VB.CommandButton cmdSalir 
         Caption         =   "&Salir"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   7935
         TabIndex        =   8
         Top             =   225
         Width           =   1425
      End
      Begin VB.CommandButton cmdGrabar 
         Caption         =   "Grabar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   120
         TabIndex        =   5
         Top             =   225
         Width           =   1425
      End
      Begin VB.CommandButton cmdEliminaConfig 
         Caption         =   "Eliminar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   1560
         TabIndex        =   6
         Top             =   225
         Width           =   1425
      End
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   50
      Top             =   6870
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13776
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   53
      Top             =   0
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Grabar"
            Object.ToolTipText     =   "Grabar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Eliminar"
            Object.ToolTipText     =   "Eliminar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Prevista"
            Object.ToolTipText     =   "Prevista"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Salir"
            Object.ToolTipText     =   "Salir"
         EndProperty
      EndProperty
      Begin MSComctlLib.ImageList ilsIconos 
         Left            =   4635
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
End
Attribute VB_Name = "frmConfigura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public strSeccEncab As String
Public strSeccDetalle As String
Public strSeccTotal As String
Public lngCorrCpoFiller As Long

Public glngCodEntidad As Long
Public glngCodSucursal As Long
Public glngCodProducto As Long
Public gstrCodArchivo As String

'//Constantes de Secci�n
Const ENCABEZADO = 1
Const DETALLE = 2
Const TOTAL = 3

'//Contantes de Tipo Celda Spread
Const SS_CELL_TYPE_DATE = 0             '0  - Date    Creates date cell
Const SS_CELL_TYPE_EDIT = 1             '1  - Edit    (Default) Creates edit cell
Const SS_CELL_TYPE_FLOAT = 2            '2  - Float   Creates float cell  SS_CELL_TYPE_FLOAT
Const SS_CELL_TYPE_INTEGER = 3          '3  - Integer Creates integer cell
Const SS_CELL_TYPE_PIC = 4              '4  - PIC Creates PIC cell
Const SS_CELL_TYPE_STATIC_TEXT = 5      '5  - Static Text Creates static text cell
Const SS_CELL_TYPE_TIME = 6             '6  - Time    Creates time cell
Const SS_CELL_TYPE_BUTTON = 7           '7  - Button  Creates button cell
Const SS_CELL_TYPE_COMBOBOX = 8         '8  - Combo Box   Creates combo box cell
Const SS_CELL_TYPE_PICTURE = 9          '9  - Picture Creates picture cell
Const SS_CELL_TYPE_CHECKBOX = 10        '10 - Check Box  Creates check box cell
Const SS_CELL_TYPE_OWNER_DRAWN = 11     '11 - Owner-Drawn    Creates owner-drawn cell






'Public tipoinforme As Integer

Private Sub Carga_Cbo_Separador()
    cboSeparador.Clear
    
    cboSeparador.AddItem "Tab"
    cboSeparador.AddItem ";"
    cboSeparador.AddItem "-"
    cboSeparador.ListIndex = 0
End Sub

Private Sub Carga_Cbo_Decimal()
    cboDecimal.Clear
    'aqui
    cboDecimal.AddItem ""
    cboDecimal.AddItem "."
    cboDecimal.AddItem ","
    
    cboDecimal.ListIndex = 0
End Sub

Private Function Codigo_Del_Tipo(strTipo As String) As Long
   Dim lngInd As Long
   
    For lngInd = 0 To UBound(garrTiposDatos)
        If garrTiposDatos(lngInd, 1) = strTipo Then
            Codigo_Del_Tipo = garrTiposDatos(lngInd, 0)
            Exit For
        End If
    Next

End Function

Private Sub Carga_Cbo_Grilla()
   Dim lngInd As Long
   
   On Error GoTo Errores
    
   Screen.MousePointer = vbHourglass
   
    grdAsignadosEncab.Col = 2
    grdAsignados.Col = 2
    grdAsignadosTot.Col = 2
    
    
    grdAsignadosEncab.Action = 26
    grdAsignados.Action = 26
    grdAsignadosTot.Action = 26
    
    grdAsignadosEncab.TypeComboBoxIndex = -1
    grdAsignados.TypeComboBoxIndex = -1
    grdAsignadosTot.TypeComboBoxIndex = -1
    
    For lngInd = 0 To UBound(garrTiposDatos)
        grdAsignadosEncab.TypeComboBoxString = garrTiposDatos(lngInd, 1)
        grdAsignados.TypeComboBoxString = garrTiposDatos(lngInd, 1)
        grdAsignadosTot.TypeComboBoxString = garrTiposDatos(lngInd, 1)
    Next

    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Tipos de Datos"
    
Salir:
   Screen.MousePointer = vbNormal

End Sub



Private Sub Carga_Tipos_Datos()
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros As Long
    Dim lngInd As Long
    Dim strGlosa As String
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
   
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.pkg_fme_parametros.sp_fme_prmtros_ver"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigotipo", adNumeric, adParamInput, 10, 1)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigosubtipo", adNumeric, adParamInput, 10, 9)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigoparametro", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Tipos de Datos"
        GoTo Salir
    End If
        
        
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrTiposDatos(0 To lngRgtros, 0 To 1)
        
    lngInd = 0
    Do While Not recRegistros.EOF
        
        strGlosa = recRegistros.Fields("gls_descripcion").Value
        strGlosa = UCase(Left(strGlosa, 1)) & LCase(Right(strGlosa, Len(strGlosa) - 1))
        
        garrTiposDatos(lngInd, 0) = recRegistros.Fields("ide_unico_param").Value
        garrTiposDatos(lngInd, 1) = strGlosa
        
    
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop

    GoTo Salir
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Tipos de Datos"
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal

End Sub

Private Function EsTotalizable(ByVal CorrCampo As Long) As Boolean
    Dim Cmd As New ADODB.Command
    
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_campos_x_producto.sp_fme_campo_totaliza"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ideproducto", adNumeric, adParamInput, 10, cboProducto.ItemData(cboProducto.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codtipoarch", adChar, adParamInput, 8, cboArchivo.ItemData(cboArchivo.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_seccion", adChar, adParamInput, 8, strSeccDetalle)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_campo", adNumeric, adParamInput, 10, CorrCampo)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_resultado", adNumeric, adParamOutput, 1, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Totalizador"
        GoTo Salir
    End If

    EsTotalizable = "" & Cmd.Parameters("p_n_resultado").Value = 1

Salir:

    Set Cmd = Nothing

End Function

Private Sub LlenarListaOrden(ByVal IdeProducto As String, TipoArchivo As Integer)
    
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
   
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_campos_x_producto.sp_fme_campos_orden_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ideproducto", adNumeric, adParamInput, 10, IdeProducto)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codtipoarch", adChar, adParamInput, 8, TipoArchivo)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Lista de Orden"
        GoTo Salir
    End If
    
    grdOrdena.MaxRows = 0
    grdDispOrdena.MaxRows = 0
    
    Do While Not recRegistros.EOF
    
        grdDispOrdena.MaxRows = grdDispOrdena.MaxRows + 1
        grdDispOrdena.Row = grdDispOrdena.MaxRows
        grdDispOrdena.Col = 1: grdDispOrdena.Text = Trim("" & recRegistros.Fields("gls_campo").Value)
        grdDispOrdena.Col = 2: grdDispOrdena.Text = "" & recRegistros.Fields("cod_ide_campo").Value
        grdDispOrdena.Col = 3: grdDispOrdena.Text = "" & recRegistros.Fields("nro_corr_campo").Value
    
        recRegistros.MoveNext
    
    Loop
    
    If grdDispOrdena.MaxRows > 0 Then grdDispOrdena.Row = 1
    
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Lista de Orden"

Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
End Sub

Private Sub LlenarListasDeCampos(ByVal IdeProducto As String, TipoArchivo As Integer)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
    
   Screen.MousePointer = vbHourglass
   
   On Error GoTo Errores
   
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "csbpi.pkg_fme_campos_x_producto.sp_fme_corr_filler_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ideproducto", adNumeric, adParamInput, 10, IdeProducto)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_correlativo", adNumeric, adParamOutput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Lista de Campos"
        GoTo Salir
    End If
    
    lngCorrCpoFiller = Cmd.Parameters("p_n_correlativo").Value
    Set Cmd = Nothing
    
        
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.pkg_fme_campos_x_producto.sp_fme_campos_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ideproducto", adNumeric, adParamInput, 10, IdeProducto)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codtipoarch", adChar, adParamInput, 8, TipoArchivo)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_codseccion", adChar, adParamInput, 8, "0")
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 1, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Lista de Campos"
        GoTo Salir
    End If
    
    grdAsignadosEncab.MaxRows = 0
    grdAsignados.MaxRows = 0
    grdAsignadosTot.MaxRows = 0
    grdOrdena.MaxRows = 0
    grdDispEncab.MaxRows = 0
    grdDisponibles.MaxRows = 0
    grdDispTot.MaxRows = 0
    
    Do While Not recRegistros.EOF
    
        If ("" & recRegistros.Fields("cod_ide_campo").Value) <> "0" Then
        
            If ("" & recRegistros.Fields("nom_corto").Value) = "ENC" Then
                grdDispEncab.MaxRows = grdDispEncab.MaxRows + 1
                grdDispEncab.Row = grdDispEncab.MaxRows
                grdDispEncab.Col = 1: grdDispEncab.Text = Trim("" & recRegistros.Fields("gls_campo").Value)
                grdDispEncab.Col = 2: grdDispEncab.Text = "" & recRegistros.Fields("cod_ide_campo").Value
                grdDispEncab.Col = 3: grdDispEncab.Text = "" & recRegistros.Fields("nro_corr_campo").Value
                grdDispEncab.Col = 4: grdDispEncab.Text = "" & recRegistros.Fields("flg_obligatorio").Value
                grdDispEncab.Col = 5: grdDispEncab.Text = "" & recRegistros.Fields("flg_convertir").Value
                grdDispEncab.Col = 6: grdDispEncab.Text = "" & recRegistros.Fields("nom_campo").Value
                strSeccEncab = "" & recRegistros.Fields("cod_ide_seccion").Value
    
            ElseIf ("" & recRegistros.Fields("nom_corto").Value) = "DET" Then
                grdDisponibles.MaxRows = grdDisponibles.MaxRows + 1
                grdDisponibles.Row = grdDisponibles.MaxRows
                grdDisponibles.Col = 1: grdDisponibles.Text = Trim("" & recRegistros.Fields("gls_campo").Value)
                grdDisponibles.Col = 2: grdDisponibles.Text = "" & recRegistros.Fields("cod_ide_campo").Value
                grdDisponibles.Col = 3: grdDisponibles.Text = "" & recRegistros.Fields("nro_corr_campo").Value
                grdDisponibles.Col = 4: grdDisponibles.Text = "" & recRegistros.Fields("flg_ind_totaliza").Value
                grdDisponibles.Col = 5: grdDisponibles.Text = "" & recRegistros.Fields("flg_ind_ordena").Value
                grdDisponibles.Col = 6: grdDisponibles.Text = "" & recRegistros.Fields("flg_obligatorio").Value
                grdDisponibles.Col = 7: grdDisponibles.Text = "" & recRegistros.Fields("flg_convertir").Value
                grdDisponibles.Col = 8: grdDisponibles.Text = "" & recRegistros.Fields("nom_campo").Value
                
                strSeccDetalle = "" & recRegistros.Fields("cod_ide_seccion").Value
    
            ElseIf ("" & recRegistros.Fields("nom_corto").Value) = "TOT" Then
                grdDispTot.MaxRows = grdDispTot.MaxRows + 1
                grdDispTot.Row = grdDispTot.MaxRows
                grdDispTot.Col = 1: grdDispTot.Text = Trim("" & recRegistros.Fields("gls_campo").Value)
                grdDispTot.Col = 2: grdDispTot.Text = "" & recRegistros.Fields("cod_ide_campo").Value
                grdDispTot.Col = 3: grdDispTot.Text = "" & recRegistros.Fields("nro_corr_campo").Value
                grdDispTot.Col = 4: grdDispTot.Text = "" & recRegistros.Fields("flg_obligatorio").Value
                grdDispTot.Col = 5: grdDispTot.Text = "" & recRegistros.Fields("flg_convertir").Value
                grdDispTot.Col = 6: grdDispTot.Text = "" & recRegistros.Fields("nom_campo").Value
                
                strSeccTotal = "" & recRegistros.Fields("cod_ide_seccion").Value
                
            End If
       
        End If
        
        recRegistros.MoveNext
    
    Loop
    recRegistros.Close
        
    If grdDispEncab.MaxRows > 0 Then grdDispEncab.Row = 1
    If grdDisponibles.MaxRows > 0 Then grdDisponibles.Row = 1
    If grdDispTot.MaxRows > 0 Then grdDispTot.Row = 1

Salir:
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    Exit Sub


Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Lista de Campos"
    GoTo Salir
Resume 0
    
End Sub

Rem LIMPIA LOS CONTROLES EXISTENTES EN EL FORMULARIO
Private Sub InicializaControles()
    CmdGrabar.Enabled = False
    tlbAcciones.Buttons("Grabar").Enabled = CmdGrabar.Enabled
    cmdEliminaConfig.Enabled = False
    tlbAcciones.Buttons("Eliminar").Enabled = cmdEliminaConfig.Enabled
    cmdPreview.Enabled = False
    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
'    cboFormato.ListIndex = 0
    'lstDispEncab.Clear
    grdDispEncab.MaxRows = 0
    grdDisponibles.MaxRows = 0
    grdDispTot.MaxRows = 0
    grdDispOrdena.MaxRows = 0
    grdAsignados.MaxRows = 0
    grdAsignadosEncab.MaxRows = 0
    grdAsignadosTot.MaxRows = 0
    grdOrdena.MaxRows = 0
    tabSecciones.Tab = 0
    fraMantencion.Enabled = False
'    bent.SetFocus
End Sub

Rem EXISTEN 5 CAMPOS QUE DEBEN EXISTIR OBLIGATORIAMENTE
Rem EN TODA CONFIGURACI�N DE ARCHIVO.
Rem ESTA FUNCI�N SE ENCARGA DE VERIFICAR SI EST�N PRESENTES.
'Private Function CamposMinimos(ByVal IdeProducto As String) As Boolean
'Dim Sql     As String
'Dim recMin  As New ADODB.Recordset
'Dim K       As Integer
'
'    Sql = ""
'    Sql = Sql & " SELECT CAAE_IDE_CAMPO_ENT "
'    Sql = Sql & " FROM   CONFIRMING.CAMPOS_ARCHIVO_ENTRADA_CAAE "
'    Sql = Sql & " WHERE  CAAE_IDE_PRODUCTO    = " & IdeProducto
'    'Sql = Sql & " AND    CAAE_IND_OBLIGATORIO = 1 "
'    Sql = Sql & " AND    CAAE_TIPO_INFORME = " & tipoinforme
'    recMin.Open Sql, gAdoCon, adOpenForwardOnly, adLockReadOnly
'    grdAsignados.Col = 7
'    CamposMinimos = True
'    Do While Not recMin.EOF
'        CamposMinimos = False
'        For K = 1 To grdAsignados.MaxRows
'            grdAsignados.Row = K
'            If recMin(0) = grdAsignados.Value Then
'                CamposMinimos = True
'                Exit For
'            End If
'        Next K
'        If Not CamposMinimos Then
'            MsgBox "Faltan campos que son obligatorios ...", , "Error"
'            Exit Do
'        End If
'        recMin.MoveNext
'    Loop
'    recMin.Close
'End Function

Private Function Obligatorios() As Boolean
    Dim lngInd As Long
    
    Obligatorios = True
    
    grdDispEncab.Col = 4
    For lngInd = 1 To grdDispEncab.MaxRows
        grdDispEncab.Row = lngInd
        If Not grdDispEncab.RowHidden And grdDispEncab.Text = 1 Then
            Obligatorios = False
            Exit Function
        End If
    Next
    
    grdDisponibles.Col = 6
    For lngInd = 1 To grdDisponibles.MaxRows
        grdDisponibles.Row = lngInd
        If Not grdDisponibles.RowHidden And grdDisponibles.Text = 1 Then
            Obligatorios = False
            Exit Function
        End If
    Next
    
    grdDispTot.Col = 4
    For lngInd = 1 To grdDispTot.MaxRows
        grdDispTot.Row = lngInd
        If Not grdDispTot.RowHidden And grdDispTot.Text = 1 Then
            Obligatorios = False
            Exit Function
        End If
    Next
    
    If grdAsignadosEncab.MaxRows > 0 Then
        grdAsignadosEncab.Row = 1: grdAsignadosEncab.Col = 13
        If grdAsignadosEncab.Text <> "REG_CRTL1" Then
            Obligatorios = False
            Exit Function
        End If
    End If
    
    If grdAsignados.MaxRows > 0 And (grdAsignadosEncab.MaxRows > 0 Or grdAsignadosTot.MaxRows > 0) Then
        grdAsignados.Row = 1: grdAsignados.Col = 15
        If grdAsignados.Text <> "REG_CRTL1" Then
            Obligatorios = False
            Exit Function
        End If
    End If
        
    If grdAsignadosTot.MaxRows > 0 Then
        grdAsignadosTot.Row = 1: grdAsignadosTot.Col = 13
        If grdAsignadosTot.Text <> "REG_CRTL1" Then
            Obligatorios = False
            Exit Function
        End If
    End If
    
    Obligatorios = grdAsignados.MaxRows > 0
End Function

Rem MUESTRA LA CONFIGURACI�N SI EXISTE DEL CLIENTE ACTUAL
Private Sub TraeConfiguracion()
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim blnContinuar    As Boolean
    Dim IdeCampoEntAsig As String
    Dim Rut             As String
    Dim i               As Long
    Dim J               As Long
    Dim strGlosa As String

    ' Validaciones.-
    If cboEntidad.Text = "" Then Exit Sub
    
        
    Rut = cboEntidad.ItemData(cboEntidad.ListIndex)

    blnContinuar = Not cboProducto.ListIndex = -1 _
                   And Not cboArchivo.ListIndex = -1 _
                   And Not Rut = "" _
                   And Not cboFormato.ListIndex = -1

    If Not blnContinuar Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    On Error GoTo Errores
    
    

    glngCodEntidad = Rut
    glngCodSucursal = 1
    glngCodProducto = cboProducto.ItemData(cboProducto.ListIndex)
    gstrCodArchivo = cboArchivo.ItemData(cboArchivo.ListIndex)
    
    
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.pkg_fme_campos_x_cliente.sp_fme_formato_cliente"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_producto", adNumeric, adParamInput, 10, cboProducto.ItemData(cboProducto.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_entidad_cli", adNumeric, adParamInput, 10, Rut)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_sucursal", adNumeric, adParamInput, 10, glngCodSucursal)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_archivo", adChar, adParamInput, 8, cboArchivo.ItemData(cboArchivo.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_ordenamiento", adChar, adParamInput, 8, "N")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
     
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
         MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Detalle Archivo"
         GoTo Salir
    End If
     
    If Not recRegistros.EOF Then
        BuscaItemCbo cboFormato, recRegistros.Fields("cod_tipo_formato").Value
        If recRegistros.Fields("des_separador").Value <> "" Then
            cboSeparador.Enabled = True
            cboSeparador.Text = Trim(recRegistros.Fields("des_separador").Value)
        Else
            cboSeparador.ListIndex = -1
            cboSeparador.Enabled = False
        End If
        If recRegistros.Fields("des_simbolo_decimal").Value <> "" Then
            cboDecimal.Text = "" & recRegistros.Fields("des_simbolo_decimal").Value
        Else
            cboDecimal.ListIndex = 0
        End If
    Else
        cboDecimal.ListIndex = 0
        cboSeparador.ListIndex = -1
    End If
     
 
    grdAsignadosEncab.ReDraw = False
    grdAsignados.ReDraw = False
    grdAsignadosTot.ReDraw = False
    
    grdAsignadosEncab.MaxRows = 0
    grdAsignados.MaxRows = 0
    grdAsignadosTot.MaxRows = 0
    grdOrdena.MaxRows = 0
    grdDispOrdena.MaxRows = 0
    
    glngCorrEncab = 0
    
    Do While Not recRegistros.EOF
            
        strGlosa = recRegistros.Fields("tipo_campo").Value
        strGlosa = UCase(Left(strGlosa, 1)) & LCase(Right(strGlosa, Len(strGlosa) - 1))

        glngCorrEncab = recRegistros.Fields("nro_corr_encab").Value

        If "" & recRegistros.Fields("nomcor_seccion").Value = "ENC" Then  'ENC  DET TOT
        
            grdAsignadosEncab.MaxRows = grdAsignadosEncab.MaxRows + 1
            grdAsignadosEncab.Row = grdAsignadosEncab.MaxRows
            grdAsignadosEncab.RowHeight(grdAsignadosEncab.MaxRows) = 10
            

            If Not "" & recRegistros.Fields("cod_ide_campo").Value = "-99" Then
                grdAsignadosEncab.Col = 1:   grdAsignadosEncab.Text = IIf(IsNull(recRegistros.Fields("gls_campo")), "", recRegistros.Fields("gls_campo"))
                grdAsignadosEncab.Col = 2:  grdAsignadosEncab.Text = strGlosa
                grdAsignadosEncab.Col = 3:  grdAsignadosEncab.Text = Val(Format("" & recRegistros.Fields("vlr_precision"), "#0"))
                grdAsignadosEncab.Col = 4:  grdAsignadosEncab.Text = Val(Format("" & recRegistros.Fields("vlr_decimales"), "#0"))
                grdAsignadosEncab.Col = 5:  grdAsignadosEncab.TypeButtonText = IIf("" & recRegistros.Fields("mrc_alineacion") = 2, "Izquierda", "Derecha")
                grdAsignadosEncab.Col = 6:  grdAsignadosEncab.TypeButtonText = IIf("" & recRegistros.Fields("mrc_relleno") = 1, "Espacios", "Ceros")
                grdAsignadosEncab.Col = 7:  grdAsignadosEncab.Text = Val(Format("" & recRegistros.Fields("cod_ide_campo"), "#0"))
                grdAsignadosEncab.Col = 9:  grdAsignadosEncab.TypeButtonText = IIf("" & recRegistros.Fields("detconvertir") = 1, "SI", "NO")
                grdAsignadosEncab.Col = 10:  grdAsignadosEncab.Text = "" & recRegistros.Fields("cod_tipo_campo")
                grdAsignadosEncab.Col = 11:  grdAsignadosEncab.Text = "" & recRegistros.Fields("nro_corr_campo")
                grdAsignadosEncab.Col = 12:  grdAsignadosEncab.Text = IIf("" & recRegistros.Fields("camconvertir") = 1, "S", "N")
                grdAsignadosEncab.Col = 13:  grdAsignadosEncab.Text = "" & recRegistros.Fields("nom_campo").Value
            Else
                grdAsignadosEncab.Col = 1:   grdAsignadosEncab.Text = "FILLER"
                grdAsignadosEncab.Col = 2:  grdAsignadosEncab.Text = strGlosa
                grdAsignadosEncab.Col = 3:  grdAsignadosEncab.Text = Val(Format("" & recRegistros.Fields("vlr_precision"), "#0"))
                grdAsignadosEncab.Col = 4:  grdAsignadosEncab.Text = Val(Format("" & recRegistros.Fields("vlr_decimales"), "#0"))
                grdAsignadosEncab.Col = 5:  grdAsignadosEncab.TypeButtonText = IIf("" & recRegistros.Fields("mrc_alineacion") = 2, "Izquierda", "Derecha")
                grdAsignadosEncab.Col = 6:  grdAsignadosEncab.TypeButtonText = IIf("" & recRegistros.Fields("mrc_relleno") = 1, "Espacios", "Ceros")
                grdAsignadosEncab.Col = 7:  grdAsignadosEncab.Text = Val(Format("" & recRegistros.Fields("cod_ide_campo"), "#0"))
                grdAsignadosEncab.Col = 9:  grdAsignadosEncab.CellType = SS_CELL_TYPE_STATIC_TEXT
                grdAsignadosEncab.Col = 10:  grdAsignadosEncab.Text = "" & recRegistros.Fields("cod_tipo_campo")
                grdAsignadosEncab.Col = 11:  grdAsignadosEncab.Text = "" & recRegistros.Fields("nro_corr_campo")
                grdAsignadosEncab.Col = 12:  grdAsignadosEncab.Text = "N"
                grdAsignadosEncab.Col = 13:  grdAsignadosEncab.Text = "" & recRegistros.Fields("nom_campo").Value
            End If
            
            If "" & recRegistros.Fields("nomcor_tipo_campo") = "FEC" Then
            
                    grdAsignadosEncab.Col = 8
                    grdAsignadosEncab.CellType = SS_CELL_TYPE_COMBOBOX
                    grdAsignadosEncab.TypeComboBoxList = _
                    "ddmmyy" & vbTab & "mmddyy" & vbTab & "yymmdd" & vbTab & _
                    "ddmmyyyy" & vbTab & "mmddyyyy" & vbTab & "yyyymmdd" & vbTab & _
                    "dd-mm-yy" & vbTab & "mm-dd-yy" & vbTab & "yy-mm-dd" & vbTab & _
                    "dd-mm-yyyy" & vbTab & "mm-dd-yyyy" & vbTab & "yyyy-mm-dd" & vbTab & _
                    "dd/mm/yy" & vbTab & "mm/dd/yy" & vbTab & "yy/mm/dd" & vbTab & _
                    "dd/mm/yyyy" & vbTab & "mm/dd/yyyy" & vbTab & "yyyy/mm/dd"
                     grdAsignadosEncab.Text = "" & recRegistros.Fields("gls_formato")
            
            ElseIf "" & recRegistros.Fields("nomcor_tipo_campo") = "RUT" Then
                
                    grdAsignadosEncab.Col = 8
                    grdAsignadosEncab.CellType = SS_CELL_TYPE_COMBOBOX
                    grdAsignadosEncab.TypeComboBoxList = "9999999999" & vbTab & "999999999D" & vbTab & "99999999-D"
                    grdAsignadosEncab.TypeComboBoxEditable = False
                    grdAsignadosEncab.Text = "" & recRegistros.Fields("gls_formato")
            
            End If
            
        ElseIf "" & recRegistros.Fields("nomcor_seccion").Value = "DET" Then  'ENC  DET TOT
            
            grdAsignados.MaxRows = grdAsignados.MaxRows + 1
            grdAsignados.Row = grdAsignados.MaxRows
            grdAsignados.RowHeight(grdAsignados.MaxRows) = 10
            
            If Not "" & recRegistros.Fields("cod_ide_campo").Value = "-99" Then
                grdAsignados.Col = 1:  grdAsignados.Text = "" & recRegistros.Fields("gls_campo")
                grdAsignados.Col = 2: grdAsignados.Text = strGlosa
                grdAsignados.Col = 3: grdAsignados.Text = Val(Format("" & recRegistros.Fields("vlr_precision"), "#0"))
                grdAsignados.Col = 4: grdAsignados.Text = Val(Format("" & recRegistros.Fields("vlr_decimales"), "#0"))
                grdAsignados.Col = 5: grdAsignados.TypeButtonText = IIf("" & recRegistros.Fields("mrc_alineacion") = 2, "Izquierda", "Derecha")
                grdAsignados.Col = 6: grdAsignados.TypeButtonText = IIf("" & recRegistros.Fields("mrc_relleno") = 1, "Espacios", "Ceros")
                grdAsignados.Col = 7: grdAsignados.Text = Val(Format("" & recRegistros.Fields("cod_ide_campo"), "#0"))
                grdAsignados.Col = 9: grdAsignados.TypeButtonText = IIf("" & recRegistros.Fields("detconvertir") = 1, "SI", "NO")
                grdAsignados.Col = 10:  grdAsignados.Text = "" & recRegistros.Fields("cod_tipo_campo")
                grdAsignados.Col = 11: grdAsignados.Text = "" & recRegistros.Fields("nro_corr_campo")
                grdAsignados.Col = 12: grdAsignados.Text = "" & recRegistros.Fields("flg_ind_totaliza")
                grdAsignados.Col = 13: grdAsignados.Text = "" & recRegistros.Fields("flg_ind_ordena")
                grdAsignados.Col = 14: grdAsignados.Text = IIf("" & recRegistros.Fields("camconvertir") = 1, "S", "N")
                grdAsignados.Col = 15: grdAsignados.Text = "" & recRegistros.Fields("nom_campo").Value
                'Carga disponibles para ordenamiento
                If "" & recRegistros.Fields("flg_ind_totaliza") = 1 Or "" & recRegistros.Fields("flg_ind_ordena") = 1 Then
                    grdDispOrdena.MaxRows = grdDispOrdena.MaxRows + 1
                    grdDispOrdena.Row = grdDispOrdena.MaxRows
                    grdDispOrdena.Col = 1: grdAsignados.Col = 1: grdDispOrdena.Text = grdAsignados.Text
                    grdDispOrdena.Col = 2: grdAsignados.Col = 7: grdDispOrdena.Text = grdAsignados.Text
                    grdDispOrdena.Col = 3: grdAsignados.Col = 11: grdDispOrdena.Text = grdAsignados.Text
                End If
                
            Else
                grdAsignados.Col = 1: grdAsignados.Text = "FILLER"
                grdAsignados.Col = 2: grdAsignados.Text = strGlosa
                grdAsignados.Col = 3: grdAsignados.Text = Val(Format("" & recRegistros.Fields("vlr_precision"), "#0"))
                grdAsignados.Col = 4: grdAsignados.Text = Val(Format("" & recRegistros.Fields("vlr_decimales"), "#0"))
                grdAsignados.Col = 5: grdAsignados.TypeButtonText = IIf("" & recRegistros.Fields("mrc_alineacion") = 2, "Izquierda", "Derecha")
                grdAsignados.Col = 6: grdAsignados.TypeButtonText = IIf("" & recRegistros.Fields("mrc_relleno") = 1, "Espacios", "Ceros")
                grdAsignados.Col = 7: grdAsignados.Text = Val(Format("" & recRegistros.Fields("cod_ide_campo"), "#0"))
                grdAsignados.Col = 9: grdAsignados.CellType = SS_CELL_TYPE_STATIC_TEXT
                grdAsignados.Col = 10:  grdAsignados.Text = "" & recRegistros.Fields("cod_tipo_campo")
                grdAsignados.Col = 11: grdAsignados.Text = "" & recRegistros.Fields("nro_corr_campo")
                grdAsignados.Col = 12: grdAsignados.Text = "" & recRegistros.Fields("flg_ind_totaliza")
                grdAsignados.Col = 13: grdAsignados.Text = "" & recRegistros.Fields("flg_ind_ordena")
                grdAsignados.Col = 14: grdAsignados.Text = "N"
                grdAsignados.Col = 15: grdAsignados.Text = "" & recRegistros.Fields("nom_campo").Value
            End If
            
            If "" & recRegistros.Fields("nomcor_tipo_campo") = "FEC" Then
                    
                    grdAsignados.Col = 8
                    grdAsignados.CellType = SS_CELL_TYPE_COMBOBOX
                    grdAsignados.TypeComboBoxList = _
                    "ddmmyy" & vbTab & "mmddyy" & vbTab & "yymmdd" & vbTab & _
                    "ddmmyyyy" & vbTab & "mmddyyyy" & vbTab & "yyyymmdd" & vbTab & _
                    "dd-mm-yy" & vbTab & "mm-dd-yy" & vbTab & "yy-mm-dd" & vbTab & _
                    "dd-mm-yyyy" & vbTab & "mm-dd-yyyy" & vbTab & "yyyy-mm-dd" & vbTab & _
                    "dd/mm/yy" & vbTab & "mm/dd/yy" & vbTab & "yy/mm/dd" & vbTab & _
                    "dd/mm/yyyy" & vbTab & "mm/dd/yyyy" & vbTab & "yyyy/mm/dd"
                     grdAsignados.Text = "" & recRegistros.Fields("gls_formato")
            
            ElseIf "" & recRegistros.Fields("nomcor_tipo_campo") = "RUT" Then
            
                    grdAsignados.Col = 8
                    grdAsignados.CellType = SS_CELL_TYPE_COMBOBOX
                    grdAsignados.TypeComboBoxList = "9999999999" & vbTab & "999999999D" & vbTab & "99999999-D"
                    grdAsignados.TypeComboBoxEditable = False
                    grdAsignados.Text = "" & recRegistros.Fields("gls_formato")
                    
            End If
            If grdDispOrdena.MaxRows > 0 Then grdDispOrdena.Row = 1
        
        
        ElseIf "" & recRegistros.Fields("nomcor_seccion").Value = "TOT" Then  'ENC  DET TOT
        
            grdAsignadosTot.MaxRows = grdAsignadosTot.MaxRows + 1
            grdAsignadosTot.Row = grdAsignados.MaxRows
            grdAsignadosTot.RowHeight(grdAsignadosTot.MaxRows) = 10
            
            If Not "" & recRegistros.Fields("cod_ide_campo").Value = "-99" Then
                grdAsignadosTot.Col = 1: grdAsignadosTot.Text = "" & recRegistros.Fields("gls_campo")
                grdAsignadosTot.Col = 2: grdAsignadosTot.Text = strGlosa
                grdAsignadosTot.Col = 3: grdAsignadosTot.Text = Val(Format("" & recRegistros.Fields("vlr_precision"), "#0"))
                grdAsignadosTot.Col = 4: grdAsignadosTot.Text = Val(Format("" & recRegistros.Fields("vlr_decimales"), "#0"))
                grdAsignadosTot.Col = 5: grdAsignadosTot.TypeButtonText = IIf("" & recRegistros.Fields("mrc_alineacion") = 2, "Izquierda", "Derecha")
                grdAsignadosTot.Col = 6: grdAsignadosTot.TypeButtonText = IIf("" & recRegistros.Fields("mrc_relleno") = 1, "Espacios", "Ceros")
                grdAsignadosTot.Col = 7: grdAsignadosTot.Text = Val(Format("" & recRegistros.Fields("cod_ide_campo"), "#0"))
                grdAsignadosTot.Col = 9: grdAsignadosTot.TypeButtonText = IIf("" & recRegistros.Fields("detconvertir") = 1, "SI", "NO")
                grdAsignadosTot.Col = 10:  grdAsignadosTot.Text = "" & recRegistros.Fields("cod_tipo_campo")
                grdAsignadosTot.Col = 11: grdAsignadosTot.Text = "" & recRegistros.Fields("nro_corr_campo")
                grdAsignadosTot.Col = 12: grdAsignadosTot.Text = IIf("" & recRegistros.Fields("camconvertir") = 1, "S", "N")
                grdAsignadosTot.Col = 13: grdAsignadosTot.Text = "" & recRegistros.Fields("nom_campo").Value
            Else
                grdAsignadosTot.Col = 1: grdAsignadosTot.Text = "FILLER"
                grdAsignadosTot.Col = 2: grdAsignadosTot.Text = strGlosa
                grdAsignadosTot.Col = 3: grdAsignadosTot.Text = Val(Format("" & recRegistros.Fields("vlr_precision"), "#0"))
                grdAsignadosTot.Col = 4: grdAsignadosTot.Text = Val(Format("" & recRegistros.Fields("vlr_decimales"), "#0"))
                grdAsignadosTot.Col = 5: grdAsignadosTot.TypeButtonText = IIf("" & recRegistros.Fields("mrc_alineacion") = 2, "Izquierda", "Derecha")
                grdAsignadosTot.Col = 6: grdAsignadosTot.TypeButtonText = IIf("" & recRegistros.Fields("mrc_relleno") = 1, "Espacios", "Ceros")
                grdAsignadosTot.Col = 7: grdAsignadosTot.Text = Val(Format("" & recRegistros.Fields("cod_ide_campo"), "#0"))
                grdAsignadosTot.Col = 9: grdAsignadosTot.CellType = SS_CELL_TYPE_STATIC_TEXT
                grdAsignadosTot.Col = 10:  grdAsignadosTot.Text = "" & recRegistros.Fields("cod_tipo_campo")
                grdAsignadosTot.Col = 11: grdAsignadosTot.Text = "" & recRegistros.Fields("nro_corr_campo")
                grdAsignadosTot.Col = 12: grdAsignadosTot.Text = "N"
                grdAsignadosTot.Col = 13: grdAsignadosTot.Text = "" & recRegistros.Fields("nom_campo").Value
            End If
            
            If "" & recRegistros.Fields("nomcor_tipo_campo") = "FEC" Then
            
                    grdAsignadosTot.Col = 8
                    grdAsignadosTot.CellType = SS_CELL_TYPE_COMBOBOX
                    grdAsignadosTot.TypeComboBoxList = _
                    "ddmmyy" & vbTab & "mmddyy" & vbTab & "yymmdd" & vbTab & _
                    "ddmmyyyy" & vbTab & "mmddyyyy" & vbTab & "yyyymmdd" & vbTab & _
                    "dd-mm-yy" & vbTab & "mm-dd-yy" & vbTab & "yy-mm-dd" & vbTab & _
                    "dd-mm-yyyy" & vbTab & "mm-dd-yyyy" & vbTab & "yyyy-mm-dd" & vbTab & _
                    "dd/mm/yy" & vbTab & "mm/dd/yy" & vbTab & "yy/mm/dd" & vbTab & _
                    "dd/mm/yyyy" & vbTab & "mm/dd/yyyy" & vbTab & "yyyy/mm/dd"
                     grdAsignadosTot.Text = "" & recRegistros.Fields("gls_formato")
                     
              ElseIf "" & recRegistros.Fields("nomcor_tipo_campo") = "RUT" Then
              
                    grdAsignadosTot.Col = 8
                    grdAsignadosTot.CellType = SS_CELL_TYPE_COMBOBOX
                    grdAsignadosTot.TypeComboBoxList = "9999999999" & vbTab & "999999999D" & vbTab & "99999999-D"
                    grdAsignadosTot.TypeComboBoxEditable = False
                    grdAsignadosTot.Text = "" & recRegistros.Fields("gls_formato")
            
            End If
        
        End If
        
        recRegistros.MoveNext
    
    Loop
    
    Rem Busca campos de ordenamiento
    
    Set recRegistros = Nothing
    Set Cmd = Nothing
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.pkg_fme_campos_x_cliente.sp_fme_formato_cliente"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_producto", adNumeric, adParamInput, 10, cboProducto.ItemData(cboProducto.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_entidad_cli", adNumeric, adParamInput, 10, Rut)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_sucursal", adNumeric, adParamInput, 10, glngCodSucursal)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_archivo", adChar, adParamInput, 8, cboArchivo.ItemData(cboArchivo.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_ordenamiento", adChar, adParamInput, 8, "S")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
     
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
         MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Orden Archivo"
         GoTo Salir
    End If


    Do While Not recRegistros.EOF
        grdOrdena.MaxRows = grdOrdena.MaxRows + 1
        grdOrdena.Row = grdOrdena.MaxRows
        grdOrdena.RowHeight(grdOrdena.MaxRows) = 10
                    
        grdOrdena.Col = 1: grdOrdena.Text = "" & recRegistros.Fields("gls_campo")
        grdOrdena.Col = 2: grdOrdena.Text = Val(Format(recRegistros.Fields("cod_ide_campo"), "#0"))
        grdOrdena.Col = 4: grdOrdena.Text = "" & recRegistros.Fields("nro_corr_campo")
        grdOrdena.Col = 5: grdOrdena.TypeButtonText = IIf("" & recRegistros.Fields("mrc_tipo_orden") = 1, "Asc", "Desc")

        If "" & recRegistros.Fields("flg_ind_totaliza") = 1 Then 'Totalizable
            grdOrdena.Col = 3: grdOrdena.Text = IIf(IsNull(recRegistros.Fields("flg_totaliza")) Or recRegistros.Fields("flg_totaliza") = 0, "", "1")
        Else
            grdOrdena.Col = 3: grdOrdena.CellType = SS_CELL_TYPE_STATIC_TEXT
        End If
        
        recRegistros.MoveNext
    Loop
    
    ' Puebla campos disponibles del Encabezado.-
    grdAsignadosEncab.ReDraw = True
    grdDispEncab.Col = 2
    For i = 1 To grdAsignadosEncab.MaxRows
        grdAsignadosEncab.Row = i
        grdAsignadosEncab.Col = 7
        IdeCampoEntAsig = grdAsignadosEncab.Text
        For J = 1 To grdDispEncab.MaxRows
            grdDispEncab.Row = J
            If IdeCampoEntAsig = Trim(grdDispEncab.Text) Then
                grdDispEncab.RowHidden = True
            End If
        Next J
    Next i

    ' Puebla campos disponibles del Detalle.-
    grdAsignados.ReDraw = True
    grdDisponibles.Col = 2
    For i = 1 To grdAsignados.MaxRows
        grdAsignados.Row = i
        grdAsignados.Col = 7
        IdeCampoEntAsig = grdAsignados.Text
        For J = 1 To grdDisponibles.MaxRows
            grdDisponibles.Row = J
            If IdeCampoEntAsig = Trim(grdDisponibles.Text) Then
                grdDisponibles.RowHidden = True
            End If
        Next J
    Next i

    ' Puebla campos disponibles del Total.-
    grdAsignadosTot.ReDraw = True
    grdDispTot.Col = 2
    For i = 1 To grdAsignadosTot.MaxRows
        grdAsignadosTot.Row = i
        grdAsignadosTot.Col = 7
        IdeCampoEntAsig = grdAsignadosTot.Text
        For J = 1 To grdDispTot.MaxRows
            grdDispTot.Row = J
            If IdeCampoEntAsig = Trim(grdDispTot.Text) Then
                grdDispTot.RowHidden = True
            End If
        Next J
    Next i
  
  
    ' Puebla campos disponibles del Orden.-
    grdDispOrdena.Col = 2
    For i = 1 To grdOrdena.MaxRows
        grdOrdena.Row = i
        grdOrdena.Col = 2
        IdeCampoEntAsig = grdOrdena.Text
        For J = 1 To grdDispOrdena.MaxRows
            grdDispOrdena.Row = J
            If IdeCampoEntAsig = Trim(grdDispOrdena.Text) Then
                grdDispOrdena.RowHidden = True
            End If
        Next J
    Next i
    
    cmdPreview.Enabled = grdAsignadosEncab.MaxRows > 0
    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Lista de Orden" '
'Resume 0
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    
End Sub

Private Sub cboArchivo_Click()
    If gblnTrans And Val(cboArchivo.ItemData(cboArchivo.ListIndex)) <> Val(gstrCodArchivo) Then
        If MsgBox("Existen cambios sin grabar." & Chr(13) & "�Desea abandonar los cambios?", vbInformation + vbYesNo, "Configuraci�n") = vbYes Then
            gAdoCon.RollbackTrans
            gblnTrans = False
        Else
            Pos_Cbo cboArchivo, Val(gstrCodArchivo)
            Exit Sub
        End If
    Else
        
    End If
    
    If grdAsignadosEncab.MaxRows + grdAsignados.MaxRows + grdAsignadosTot.MaxRows > 0 And Val(cboArchivo.ItemData(cboArchivo.ListIndex)) <> Val(gstrCodArchivo) Then
        InicializaControles
    End If

    
    
End Sub


Private Sub cboFormato_Click()

     If InStr(cboFormato.Text, "VARIABLE") = 0 Then
        cboSeparador.ListIndex = -1
        cboSeparador.Enabled = False
    Else
        If cboSeparador.ListCount > 0 Then cboSeparador.ListIndex = 0
        cboSeparador.Enabled = True
    End If
     
End Sub

Private Sub cboNombreEnt_LostFocus()
'    If Not gblnTrans Then Carga_Cbo_Sucursal

End Sub

Private Sub cboRutEnt_Click()
'    If cboRutEnt.ListCount = 0 Then Exit Sub
'    If cboRutEnt.ListIndex = -1 Then cboRutEnt.ListIndex = 0
'
'    If gblnTrans And cboRutEnt.ItemData(cboRutEnt.ListIndex) <> glngCodEntidad Then
'
'        If MsgBox("Existen cambios sin grabar." & Chr(13) & "�Desea abandonar los cambios?", vbInformation + vbYesNo, "Configuraci�n") = vbYes Then
'            gAdoCon.RollbackTrans
'            gblnTrans = False
'        Else
'            Pos_Cbo cboRutEnt, glngCodEntidad
'            Exit Sub
'        End If
'    End If
'
'    cboNombreEnt.ListIndex = cboRutEnt.ListIndex
'    If Not gblnTrans Then cboSucursal.Clear

End Sub

Private Sub cboRutEnt_GotFocus()
'    If gblnTrans Then Exit Sub
'    cboRutEnt.Text = ""
'    cboNombreEnt.ListIndex = -1
'
'    grdDispEncab.MaxRows = 0
'    grdDisponibles.MaxRows = 0
'    grdDispTot.MaxRows = 0
'    grdDispOrdena.MaxRows = 0
'
'    grdAsignadosEncab.MaxRows = 0
'    grdAsignados.MaxRows = 0
'    grdAsignadosTot.MaxRows = 0
'    grdOrdena.MaxRows = 0
    
End Sub

Private Sub cboRutEnt_KeyPress(KeyAscii As Integer)
    
    
'    If gblnTrans Then
'        If MsgBox("Existen cambios sin grabar." & Chr(13) & "�Desea abandonar los cambios?", vbInformation + vbYesNo, "Configuraci�n") = vbYes Then
'            gAdoCon.RollbackTrans
'            gblnTrans = False
'            InicializaControles
'
'        Else
'            KeyAscii = 0
'            'Pos_Cbo cboRutEnt, glngCodEntidad
'            Exit Sub
'        End If
'    End If
'
'    If KeyAscii = 13 Then cmdBusEnt_Click
'
'    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
'        KeyAscii = 0
'    Else
'        cboNombreEnt.Clear
'        If cboRutEnt.ListCount > 0 Then cboRutEnt.Clear
'    End If
'
    
End Sub

Private Sub cboNombreEnt_Click()
'    If cboRutEnt.ListCount = 0 Then Exit Sub
'    If cboRutEnt.ListIndex = -1 Then cboRutEnt.ListIndex = 0
'
'    If gblnTrans And cboRutEnt.ItemData(cboRutEnt.ListIndex) <> glngCodEntidad Then
'        If MsgBox("Existen cambios sin grabar." & Chr(13) & "�Desea abandonar los cambios?", vbInformation + vbYesNo, "Configuraci�n") = vbYes Then
'            gAdoCon.RollbackTrans
'            gblnTrans = False
'        Else
'            Pos_Cbo cboRutEnt, glngCodEntidad
'            Exit Sub
'        End If
'    End If
'
'    cboRutEnt.ListIndex = cboNombreEnt.ListIndex
'    If Not gblnTrans Then cboSucursal.Clear

End Sub

Private Sub cboNombreEnt_KeyPress(KeyAscii As Integer)

'    If gblnTrans Then
'        If MsgBox("Existen cambios sin grabar." & Chr(13) & "�Desea abandonar los cambios?", vbInformation + vbYesNo, "Configuraci�n") = vbYes Then
'            gAdoCon.RollbackTrans
'            gblnTrans = False
'            InicializaControles
'            cboRutEnt.Clear
'            cboNombreEnt.Clear
'        Else
'            KeyAscii = 0
'            'Pos_Cbo cboRutEnt, glngCodEntidad
'            Exit Sub
'        End If
'    End If
'
'
'
'    'If cboNombreEnt.ListCount > 0 Then cboNombreEnt.Clear
End Sub

Private Sub cboSucursal_GotFocus()
'    Carga_Cbo_Sucursal

End Sub


Private Sub cboProducto_Click()
    
    If gblnTrans And cboProducto.ItemData(cboProducto.ListIndex) <> glngCodProducto Then
        If MsgBox("Existen cambios sin grabar." & Chr(13) & "�Desea abandonar los cambios?", vbInformation + vbYesNo, "Configuraci�n") = vbYes Then
            gAdoCon.RollbackTrans
            gblnTrans = False
        Else
            Pos_Cbo cboProducto, glngCodProducto
            Exit Sub
        End If
    End If
    
    If grdAsignadosEncab.MaxRows + grdAsignados.MaxRows + grdAsignadosTot.MaxRows > 0 And cboProducto.ItemData(cboProducto.ListIndex) <> glngCodProducto Then
        InicializaControles
    End If

End Sub

Rem BOT�N QUE MUEVE UNA POSICI�N HACIA ABAJO UN CAMPO
Private Sub cmdBajar_Click()
    If grdAsignados.ActiveRow < 1 Then
        MsgBox "Para mover un campo debe seleccionarlo...", vbExclamation, "Asignar"
        Exit Sub
    Else
        If grdAsignados.ActiveRow = grdAsignados.MaxRows Then Exit Sub
        grdAsignados.Row = grdAsignados.ActiveRow
        grdAsignados.Col = 15
        If grdAsignados.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" Then
            MsgBox "Este Campo no puede ser cambiado de posici�n...", vbExclamation, "Asignar"
            Exit Sub
        End If
    End If
    grdAsignados.Col = 1
    grdAsignados.Row = grdAsignados.ActiveRow
    grdAsignados.Col2 = grdAsignados.MaxCols
    grdAsignados.Row2 = grdAsignados.ActiveRow
    grdAsignados.DestCol = 1
    grdAsignados.DestRow = grdAsignados.ActiveRow + 1
    grdAsignados.Action = 21
    grdAsignados.Row = grdAsignados.ActiveRow + 1
    grdAsignados.Action = 0
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
End Sub

Private Sub cmdBajarEncab_Click()
    If grdAsignadosEncab.ActiveRow < 1 Then
        MsgBox "Para mover un campo debe seleccionarlo...", vbExclamation, "Asignar"
        Exit Sub
    Else
        If grdAsignadosEncab.ActiveRow = grdAsignadosEncab.MaxRows Then Exit Sub
        grdAsignadosEncab.Row = grdAsignadosEncab.ActiveRow
        grdAsignadosEncab.Col = 13
        If grdAsignadosEncab.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" Then
            MsgBox "Este Campo no puede ser cambiado de posici�n...", vbExclamation, "Asignar"
            Exit Sub
        End If
    End If
    grdAsignadosEncab.Col = 1
    grdAsignadosEncab.Row = grdAsignadosEncab.ActiveRow
    grdAsignadosEncab.Col2 = grdAsignadosEncab.MaxCols
    grdAsignadosEncab.Row2 = grdAsignadosEncab.ActiveRow
    grdAsignadosEncab.DestCol = 1
    grdAsignadosEncab.DestRow = grdAsignadosEncab.ActiveRow + 1
    grdAsignadosEncab.Action = 21
    grdAsignadosEncab.Row = grdAsignadosEncab.ActiveRow + 1
    grdAsignadosEncab.Action = 0
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
End Sub

Private Sub cmdBajarOrden_Click()
     If grdOrdena.ActiveRow < 1 Then
        MsgBox "Debe seleccionar un campo asignado", vbExclamation, "Asignar"
        Exit Sub
    End If
    grdOrdena.Col = 1
    grdOrdena.Row = grdOrdena.ActiveRow
    grdOrdena.Col2 = grdOrdena.MaxCols
    grdOrdena.Row2 = grdOrdena.ActiveRow
    grdOrdena.DestCol = 1
    grdOrdena.DestRow = grdOrdena.ActiveRow + 1
    grdOrdena.Action = 21
    grdOrdena.Row = grdOrdena.ActiveRow + 1
    grdOrdena.Action = 0
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    

End Sub

Private Sub cmdBajarTot_Click()
  If grdAsignadosTot.ActiveRow < 1 Then
        MsgBox "Para mover un campo debe seleccionarlo...", vbExclamation, "Asignar"
        Exit Sub
    Else
        If grdAsignadosTot.ActiveRow = grdAsignadosTot.MaxRows Then Exit Sub
        grdAsignadosTot.Row = grdAsignadosTot.ActiveRow
        grdAsignadosTot.Col = 13
        If grdAsignadosTot.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" Then
            MsgBox "Este Campo no puede ser cambiado de posici�n...", vbExclamation, "Asignar"
            Exit Sub
        End If
    End If
    grdAsignadosTot.Col = 1
    grdAsignadosTot.Row = grdAsignadosTot.ActiveRow
    grdAsignadosTot.Col2 = grdAsignadosTot.MaxCols
    grdAsignadosTot.Row2 = grdAsignadosTot.ActiveRow
    grdAsignadosTot.DestCol = 1
    grdAsignadosTot.DestRow = grdAsignadosTot.ActiveRow + 1
    grdAsignadosTot.Action = 21
    grdAsignadosTot.Row = grdAsignadosTot.ActiveRow + 1
    grdAsignadosTot.Action = 0
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
End Sub

Private Sub cmdBuscar_Click()
        
    If gblnTrans Then
        If MsgBox("Existen cambios sin grabar." & Chr(13) & "�Desea abandonar los cambios?", vbInformation + vbYesNo, "Configuraci�n") = vbYes Then
            gAdoCon.RollbackTrans
            gblnTrans = False
        Else
            Exit Sub
        End If
    End If
    
    If cboEntidad.ListIndex = -1 Then
        MsgBox "Seleccione Tipo de Entidad ...", vbExclamation, "Configuraci�n Formato Archivo"
        InicializaControles
        cboEntidad.SetFocus
        Exit Sub
    End If
        
    Screen.MousePointer = vbHourglass
    Call LlenarListasDeCampos(cboProducto.ItemData(cboProducto.ListIndex), cboArchivo.ItemData(cboArchivo.ListIndex))
    grdDispOrdena.MaxRows = 0
    grdOrdena.MaxRows = 0
    Call TraeConfiguracion
    fraMantencion.Enabled = True
    CmdGrabar.Enabled = True
    tlbAcciones.Buttons("Grabar").Enabled = CmdGrabar.Enabled
    cmdEliminaConfig.Enabled = True
    tlbAcciones.Buttons("Eliminar").Enabled = cmdEliminaConfig.Enabled
    tabSecciones.Tab = 0
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdEliminaConfig_Click()
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim blnContinuar As Boolean
    Dim Rut          As String
    Dim strMsg       As String

'    If Not Autorizar(sfeliminar) Then Exit Sub

    Rem MSG
    strMsg = "Est� seguro que desea eliminar esta configuraci�n..."
    
    If MsgBox(strMsg, vbYesNo, "Mensaje") = vbNo Then Exit Sub
    
    Rem VALIDACIONES.
'    Rut = Left(cboEntidad.Text, Len(cboEntidad.Text) - 2)
'    blnContinuar = Not Val(Rut) = 0 _
'                   And Not cboProducto.ListIndex = -1 _
'                   And Not cboSucursal.ListIndex = -1 _
'                   And Not cboFormato.ListIndex = -1
    
    blnContinuar = Not cboEntidad.ListIndex = -1 _
                   And Not cboProducto.ListIndex = -1 _
                   And Not cboFormato.ListIndex = -1
    
    
    'ELIMINAR.
    If blnContinuar Then
        Screen.MousePointer = vbHourglass
        
    
        If Not gblnTrans Then
            gAdoCon.BeginTrans
            gblnTrans = True
        End If
        
        
        Cmd.ActiveConnection = gAdoCon
        Cmd.CommandType = adCmdStoredProc
        Cmd.CommandText = "CSBPI.pkg_fme_campos_x_cliente.sp_fme_formato_borrar"
        
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_producto", adNumeric, adParamInput, 10, cboProducto.ItemData(cboProducto.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_entidad_cli", adNumeric, adParamInput, 10, cboEntidad.ItemData(cboEntidad.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_sucursal", adNumeric, adParamInput, 10, 1)
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_archivo", adChar, adParamInput, 8, cboArchivo.ItemData(cboArchivo.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_formato", adChar, adParamInput, 8, cboFormato.ItemData(cboFormato.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_conver", adChar, adParamInput, 1, "N")
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_borrado", adNumeric, adParamOutput, 10, 0)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
        Cmd.Execute
         
        If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
             MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Borrando Configuraci�n"
            If gblnTrans Then
                gAdoCon.RollbackTrans
                gblnTrans = False
            End If
             
             GoTo Salir
        
        End If
        InicializaControles
        If gblnTrans Then
            gAdoCon.CommitTrans
            gblnTrans = False
        End If
    Else
        MsgBox "Falta informaci�n para eliminar la configuraci�n...", , "Error"
    End If

Salir:
        Screen.MousePointer = vbNormal
End Sub

Private Sub Carga_Imagenes()
        
    ilsIconos.ListImages.Clear
    ilsIconos.ListImages.Add 1, "Aceptar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\aceptar.bmp")
    ilsIconos.ListImages.Add 2, "Modificar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\editar.bmp")
    ilsIconos.ListImages.Add 3, "Eliminar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\eliminar.bmp")
    ilsIconos.ListImages.Add 4, "Prevista", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\prevista.bmp")
    ilsIconos.ListImages.Add 5, "Salir", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\salir.bmp")
    ilsIconos.ListImages.Add 6, "Grabar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\grabar.bmp")
    ilsIconos.ListImages.Add 7, "Volver", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\volver.bmp")
    
    
    tlbAcciones.ImageList = ilsIconos
    
    tlbAcciones.Buttons("Grabar").Image = 6
    tlbAcciones.Buttons("Eliminar").Image = 3
    tlbAcciones.Buttons("Prevista").Image = 4
    tlbAcciones.Buttons("Salir").Image = 5
    
    
End Sub
Private Sub cmdFillerDet_Click()
    ' Agregar hacia Asignados.-
    grdAsignados.MaxRows = grdAsignados.MaxRows + 1
    grdAsignados.RowHeight(grdAsignados.MaxRows) = 10
    grdAsignados.Row = grdAsignados.MaxRows
    grdAsignados.Col = 1
    grdAsignados.Text = "FILLER"
    grdAsignados.Col = 2
    grdAsignados.Text = garrTiposDatos(0, 1)
    grdAsignados.Col = 3
    grdAsignados.Text = 10
    grdAsignados.Col = 4
    grdAsignados.Text = 0
    grdAsignados.Col = 5
    grdAsignados.TypeButtonText = "Izquierda"
    grdAsignados.Col = 6
    grdAsignados.TypeButtonText = "Espacios"
    grdAsignados.Col = 7
    grdAsignados.Text = "-99"
    grdAsignados.Col = 10
    grdAsignados.Text = garrTiposDatos(0, 0)
    grdAsignados.Col = 11
    grdAsignados.Text = lngCorrCpoFiller
    grdAsignados.Col = 14
    grdAsignados.Text = "N"
    
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If

End Sub

Private Sub cmdFillerEncab_Click()
    ' Agregar hacia Asignados.-
    grdAsignadosEncab.MaxRows = grdAsignadosEncab.MaxRows + 1
    grdAsignadosEncab.RowHeight(grdAsignadosEncab.MaxRows) = 10
    grdAsignadosEncab.Row = grdAsignadosEncab.MaxRows
    grdAsignadosEncab.Col = 1
    grdAsignadosEncab.Text = "FILLER"
    grdAsignadosEncab.Col = 2
    grdAsignadosEncab.Text = garrTiposDatos(0, 1)
    grdAsignadosEncab.Col = 3
    grdAsignadosEncab.Text = 10
    grdAsignadosEncab.Col = 4
    grdAsignadosEncab.Text = 0
    grdAsignadosEncab.Col = 5
    grdAsignadosEncab.TypeButtonText = "Izquierda"
    grdAsignadosEncab.Col = 6
    grdAsignadosEncab.TypeButtonText = "Espacios"
    grdAsignadosEncab.Col = 7
    grdAsignadosEncab.Text = "-99"
    grdAsignadosEncab.Col = 10
    grdAsignadosEncab.Text = garrTiposDatos(0, 0)
    grdAsignadosEncab.Col = 11
    grdAsignadosEncab.Text = lngCorrCpoFiller
    grdAsignadosEncab.Col = 12
    grdAsignadosEncab.Text = "N"


    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    

End Sub

Private Sub cmdFillerTot_Click()
    ' Agregar hacia Asignados.-
    grdAsignadosTot.MaxRows = grdAsignadosTot.MaxRows + 1
    grdAsignadosTot.RowHeight(grdAsignadosTot.MaxRows) = 10
    grdAsignadosTot.Row = grdAsignadosTot.MaxRows
    grdAsignadosTot.Col = 1
    grdAsignadosTot.Text = "FILLER"
    grdAsignadosTot.Col = 2
    grdAsignadosTot.Text = garrTiposDatos(0, 1)
    grdAsignadosTot.Col = 3
    grdAsignadosTot.Text = 10
    grdAsignadosTot.Col = 4
    grdAsignadosTot.Text = 0
    grdAsignadosTot.Col = 5
    grdAsignadosTot.TypeButtonText = "Izquierda"
    grdAsignadosTot.Col = 6
    grdAsignadosTot.TypeButtonText = "Espacios"
    grdAsignadosTot.Col = 7
    grdAsignadosTot.Text = "-99"
    grdAsignadosTot.Col = 10
    grdAsignadosTot.Text = garrTiposDatos(0, 0)
    grdAsignadosTot.Col = 11
    grdAsignadosTot.Text = lngCorrCpoFiller
    grdAsignadosTot.Col = 12
    grdAsignadosTot.Text = "N"
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
End Sub

Private Sub CmdGrabar_Click()
' -----------------------------
' Graba la nueva configuraci�n
' -------------------------- cc

Dim Cmd As New ADODB.Command

Dim blnContinuar    As Boolean
Dim CorrEncab As Long
Dim intCont         As Integer
Dim CodCampo        As Long
Dim CorrCampo        As Long
Dim Alineacion      As Long
Dim Decimales       As Long
Dim Precision       As Long
Dim Relleno         As Long
Dim TipoDato        As Long
Dim Convertir       As Long
Dim Formato         As String
Dim Rut             As String
Dim IndTot          As Byte
Dim TipoOrden As String

    
    

On Error GoTo error

Screen.MousePointer = vbHourglass

    Rem Validaciones.-
    Rut = cboEntidad.ItemData(cboEntidad.ListIndex)
    blnContinuar = Not Val(Rut) = 0 _
                   And Not cboProducto.ListIndex = -1 _
                   And Not cboFormato.ListIndex = -1 _
                   Or grdAsignados.MaxRows > 0 _
                   Or grdAsignadosEncab.MaxRows > 0 _
                   Or grdAsignadosTot.MaxRows > 0
                   
    'Grabaci�n.
    If blnContinuar And Obligatorios Then
        ' Elimina configuraci�n anterior.-
        
    
        If Not gblnTrans Then
            gAdoCon.BeginTrans
            gblnTrans = True
        End If
        
        Cmd.ActiveConnection = gAdoCon
        Cmd.CommandType = adCmdStoredProc
        Cmd.CommandText = "CSBPI.pkg_fme_campos_x_cliente.sp_fme_formato_borrar"
        
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_producto", adNumeric, adParamInput, 10, cboProducto.ItemData(cboProducto.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_entidad_cli", adNumeric, adParamInput, 10, cboEntidad.ItemData(cboEntidad.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_sucursal", adNumeric, adParamInput, 10, 1)
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_archivo", adChar, adParamInput, 8, cboArchivo.ItemData(cboArchivo.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_formato", adChar, adParamInput, 8, cboFormato.ItemData(cboFormato.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_conver", adChar, adParamInput, 1, "N")
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_borrado", adNumeric, adParamOutput, 10, 0)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
        Cmd.Execute
         
        If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
             MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Preparando Configuraci�n"
             If gblnTrans Then
                 gAdoCon.RollbackTrans
                 gblnTrans = False
             End If
             GoTo Salir
        End If
        
        CorrEncab = Cmd.Parameters("p_n_corr_borrado").Value
        
       Set Cmd = Nothing
        
        ' Agrega encabezado de  configuraci�n.-
        
        Cmd.ActiveConnection = gAdoCon
        Cmd.CommandType = adCmdStoredProc
        Cmd.CommandText = "CSBPI.pkg_fme_campos_x_cliente.sp_fme_formato_encab_grabar"
                
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_producto", adNumeric, adParamInput, 10, cboProducto.ItemData(cboProducto.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_entidad_cli", adNumeric, adParamInput, 10, cboEntidad.ItemData(cboEntidad.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_sucursal", adNumeric, adParamInput, 10, 1)
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_archivo", adChar, adParamInput, 8, cboArchivo.ItemData(cboArchivo.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_formato", adChar, adParamInput, 8, cboFormato.ItemData(cboFormato.ListIndex))
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_encab_ent", adNumeric, adParamInput, 10, CorrEncab)
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_separador", adChar, adParamInput, 10, cboSeparador.Text)
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_Decimal", adChar, adParamInput, 1, Trim(cboDecimal.Text))
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adVarChar, adParamInput, 10, gstrUsuario)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_encab_sal", adNumeric, adParamOutput, 10)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
        Cmd.Execute
        
        
        If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
            MsgBox "La informaci�n no pudo ser grabada" & Chr(13) & Cmd.Parameters("p_v_descerror").Value, vbInformation, "Encabezado Configuraci�n"
            If gblnTrans Then
                gAdoCon.RollbackTrans
                gblnTrans = False
            End If
            GoTo Salir
        End If
                    
        CorrEncab = Cmd.Parameters("p_n_corr_encab_sal").Value
        
        
        '//GRABAR CONFIGURACION SECCION DE ENCABEZADO
        
        If grdAsignadosEncab.MaxRows > 0 Then
            For intCont = 1 To grdAsignadosEncab.MaxRows
                grdAsignadosEncab.Row = intCont
                
                grdAsignadosEncab.Col = 3: Precision = grdAsignadosEncab.Value
                grdAsignadosEncab.Col = 4: Decimales = grdAsignadosEncab.Text
                grdAsignadosEncab.Col = 5: Alineacion = IIf(Left(grdAsignadosEncab.TypeButtonText, 1) = "D", 1, 2)
                grdAsignadosEncab.Col = 6: Relleno = IIf(Left(grdAsignadosEncab.TypeButtonText, 1) = "E", 1, 2)
                grdAsignadosEncab.Col = 7: CodCampo = grdAsignadosEncab.Value
                grdAsignadosEncab.Col = 8: Formato = Trim(grdAsignadosEncab.Text)
                grdAsignadosEncab.Col = 9: Convertir = IIf(grdAsignadosEncab.TypeButtonText = "SI", 1, 0)
                grdAsignadosEncab.Col = 10: TipoDato = grdAsignadosEncab.Value
                grdAsignadosEncab.Col = 11: CorrCampo = grdAsignadosEncab.Value
                
                Set Cmd = Nothing
                
                Cmd.ActiveConnection = gAdoCon
                Cmd.CommandType = adCmdStoredProc
                Cmd.CommandText = "CSBPI.pkg_fme_campos_x_cliente.sp_fme_formato_detalle_grabar"
                
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_encab", adNumeric, adParamInput, 10, CorrEncab)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_seccion", adChar, adParamInput, 8, strSeccEncab)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_campo", adChar, adParamInput, 8, CodCampo)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_nro_corr_campo", adNumeric, adParamInput, 10, CorrCampo)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_cod_tipo_campo", adChar, adParamInput, 8, TipoDato)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_posicion", adNumeric, adParamInput, 4, intCont)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_precision", adNumeric, adParamInput, 4, Precision)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_decimales", adNumeric, adParamInput, 2, Decimales)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_alineacion", adChar, adParamInput, 1, Alineacion)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_relleno", adChar, adParamInput, 1, Relleno)
                Cmd.Parameters.Append Cmd.CreateParameter("p_v_formato", adVarChar, adParamInput, 30, Formato)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_convierte", adVarChar, adParamInput, 1, Convertir)
                Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adVarChar, adParamInput, 10, gstrUsuario)
                Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
                Cmd.Execute
    
                If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
                    MsgBox "La informaci�n no pudo ser grabada" & Chr(13) & Cmd.Parameters("p_v_descerror").Value, vbInformation, "Configuraci�n Secci�n Encabezado"
                    If gblnTrans Then
                        gAdoCon.RollbackTrans
                        gblnTrans = False
                    End If
                    GoTo Salir
                End If
    
            Next intCont
        End If
        
        '//GRABAR CONFIGURACION SECCION DE DETALLE
        If grdAsignados.MaxRows > 0 Then
            For intCont = 1 To grdAsignados.MaxRows
                grdAsignados.Row = intCont
                
                grdAsignados.Col = 3: Precision = grdAsignados.Value
                grdAsignados.Col = 4: Decimales = grdAsignados.Text
                grdAsignados.Col = 5: Alineacion = IIf(Left(grdAsignados.TypeButtonText, 1) = "D", 1, 2)
                grdAsignados.Col = 6: Relleno = IIf(Left(grdAsignados.TypeButtonText, 1) = "E", 1, 2)
                grdAsignados.Col = 7: CodCampo = grdAsignados.Value
                grdAsignados.Col = 8: Formato = Trim(grdAsignados.Text)
                grdAsignados.Col = 9: Convertir = IIf(grdAsignados.TypeButtonText = "SI", 1, 0)
                grdAsignados.Col = 10: TipoDato = grdAsignados.Value
                grdAsignados.Col = 11: CorrCampo = grdAsignados.Value
                
                Set Cmd = Nothing
                
                Cmd.ActiveConnection = gAdoCon
                Cmd.CommandType = adCmdStoredProc
                Cmd.CommandText = "CSBPI.pkg_fme_campos_x_cliente.sp_fme_formato_detalle_grabar"
                
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_encab", adNumeric, adParamInput, 10, CorrEncab)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_seccion", adChar, adParamInput, 8, strSeccDetalle)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_campo", adChar, adParamInput, 8, CodCampo)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_nro_corr_campo", adNumeric, adParamInput, 10, CorrCampo)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_cod_tipo_campo", adChar, adParamInput, 8, TipoDato)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_posicion", adNumeric, adParamInput, 4, intCont)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_precision", adNumeric, adParamInput, 4, Precision)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_decimales", adNumeric, adParamInput, 2, Decimales)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_alineacion", adChar, adParamInput, 1, Alineacion)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_relleno", adChar, adParamInput, 1, Relleno)
                Cmd.Parameters.Append Cmd.CreateParameter("p_v_formato", adVarChar, adParamInput, 30, Formato)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_convierte", adVarChar, adParamInput, 1, Convertir)
                Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adVarChar, adParamInput, 10, gstrUsuario)
                Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
                Cmd.Execute
    
                If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
                    MsgBox "La informaci�n no pudo ser grabada" & Chr(13) & Cmd.Parameters("p_v_descerror").Value, vbInformation, "Configuraci�n Secci�n Detalle"
                    If gblnTrans Then
                        gAdoCon.RollbackTrans
                        gblnTrans = False
                    End If
                    GoTo Salir
                End If
                
            Next intCont
        End If
        
        '//GRABAR CONFIGURACION SECCION DE TOTALES
        If grdAsignadosTot.MaxRows > 0 Then
            For intCont = 1 To grdAsignadosTot.MaxRows
                grdAsignadosTot.Row = intCont
                
                grdAsignadosTot.Col = 3: Precision = grdAsignadosTot.Value
                grdAsignadosTot.Col = 4: Decimales = grdAsignadosTot.Text
                grdAsignadosTot.Col = 5: Alineacion = IIf(Left(grdAsignadosTot.TypeButtonText, 1) = "D", 1, 2)
                grdAsignadosTot.Col = 6: Relleno = IIf(Left(grdAsignadosTot.TypeButtonText, 1) = "E", 1, 2)
                grdAsignadosTot.Col = 7: CodCampo = grdAsignadosTot.Value
                grdAsignadosTot.Col = 8: Formato = Trim(grdAsignadosTot.Text)
                grdAsignadosTot.Col = 9: Convertir = IIf(grdAsignadosTot.TypeButtonText = "SI", 1, 0)
                grdAsignadosTot.Col = 10: TipoDato = grdAsignadosTot.Value
                grdAsignadosTot.Col = 11: CorrCampo = grdAsignadosTot.Value
                
                Set Cmd = Nothing
                
                Cmd.ActiveConnection = gAdoCon
                Cmd.CommandType = adCmdStoredProc
                Cmd.CommandText = "CSBPI.pkg_fme_campos_x_cliente.sp_fme_formato_detalle_grabar"
                
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_encab", adNumeric, adParamInput, 10, CorrEncab)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_seccion", adChar, adParamInput, 8, strSeccTotal)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_campo", adChar, adParamInput, 8, CodCampo)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_nro_corr_campo", adNumeric, adParamInput, 10, CorrCampo)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_cod_tipo_campo", adChar, adParamInput, 8, TipoDato)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_posicion", adNumeric, adParamInput, 4, intCont)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_precision", adNumeric, adParamInput, 4, Precision)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_decimales", adNumeric, adParamInput, 2, Decimales)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_alineacion", adChar, adParamInput, 1, Alineacion)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_relleno", adChar, adParamInput, 1, Relleno)
                Cmd.Parameters.Append Cmd.CreateParameter("p_v_formato", adVarChar, adParamInput, 30, Formato)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_convierte", adVarChar, adParamInput, 1, Convertir)
                Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adVarChar, adParamInput, 10, gstrUsuario)
                Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
                Cmd.Execute
    
                If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
                    MsgBox "La informaci�n no pudo ser grabada" & Chr(13) & Cmd.Parameters("p_v_descerror").Value, vbInformation, "Configuraci�n Secci�n Total"
                    If gblnTrans Then
                        gAdoCon.RollbackTrans
                        gblnTrans = False
                    End If
                    GoTo Salir
                End If
            
            Next intCont
    
        End If
        
        '//GRABAR CONFIGURACION SECCION DE ORDEN
        If grdOrdena.MaxRows > 0 Then
            
            For intCont = 1 To grdOrdena.MaxRows
               
                grdOrdena.Row = intCont
                grdOrdena.Col = 2: CodCampo = Val(grdOrdena.Text)
                grdOrdena.Col = 3: IndTot = Val(grdOrdena.Text)
                grdOrdena.Col = 5: TipoOrden = IIf(Left(grdOrdena.TypeButtonText, 1) = "A", 1, 2)
                
                Set Cmd = Nothing
                
                Cmd.ActiveConnection = gAdoCon
                Cmd.CommandType = adCmdStoredProc
                Cmd.CommandText = "CSBPI.pkg_fme_campos_x_cliente.sp_fme_formato_orden_grabar"
                
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_corr_encab", adNumeric, adParamInput, 10, CorrEncab)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_seccion", adChar, adParamInput, 8, strSeccDetalle)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_campo", adChar, adParamInput, 8, CodCampo)
                Cmd.Parameters.Append Cmd.CreateParameter("p_n_orden", adNumeric, adParamInput, 10, intCont)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_tipo_orden", adChar, adParamInput, 1, TipoOrden)
                Cmd.Parameters.Append Cmd.CreateParameter("p_c_totaliza", adChar, adParamInput, 1, IndTot)
                Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adVarChar, adParamInput, 10, gstrUsuario)
                Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
                Cmd.Execute
    
                If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
                    MsgBox "La informaci�n no pudo ser grabada" & Chr(13) & Cmd.Parameters("p_v_descerror").Value, vbInformation, "Configuraci�n Secci�n Orden"
                    If gblnTrans Then
                        gAdoCon.RollbackTrans
                        gblnTrans = False
                    End If
                    GoTo Salir
                End If
                
            Next intCont
        End If
        
        If gblnTrans Then
            gAdoCon.CommitTrans
            gblnTrans = False
        End If
        
        glngCorrEncab = CorrEncab
        
        Screen.MousePointer = vbDefault
        MsgBox "Los cambios fueron grabados", vbInformation, "Grabar"
        InicializaControles
        cboEntidad.SetFocus
    Else
        MsgBox "Falta informaci�n en el Criterio de Selecci�n" + vbCr + _
               "o no se ha definido la configuraci�n del archivo ", vbExclamation, "Grabar"
    End If
    
Salir:
    Screen.MousePointer = vbNormal
    'cboRutEnt.SetFocus
    Exit Sub
    Resume 0
error:
    If gblnTrans Then
        gAdoCon.RollbackTrans
        gblnTrans = False
    End If
    
    If Err.Number <> 0 And Not Err.Number = -2147168242 Then

        MsgBox "No se pudo grabar", vbExclamation, "Grabar"
    Else
        MsgBox Err.Description, , "Error"
    End If
End Sub

Private Sub cmdPoner_Click()
    
    ' -----------------
    ' Asigna un campo.-
    ' -------------- cc
    Dim strSql As String
    Dim lngBanco As Long
    Dim lngPlaza As Long
    Dim lngSel As Long
    Dim lngIdeCampo As Long
    Dim i As Integer
    Dim blnIndTotal As Boolean
    Dim blnIndOrden As Boolean
    
    lngSel = -1
    For i = 1 To grdDisponibles.MaxRows
        grdDisponibles.Row = i
        If Not grdDisponibles.RowHidden Then
            lngSel = 1
            Exit For
        End If
    Next
    
    grdDisponibles.Row = grdDisponibles.ActiveRow
    
    If lngSel = -1 Or grdDisponibles.RowHidden Then
        MsgBox "Debe seleccionar un campo disponible", vbExclamation, "Asignar"
        Exit Sub
    End If
    
    ' Agregar hacia Asignados.-
    grdAsignados.MaxRows = grdAsignados.MaxRows + 1
    grdDisponibles.Col = 8
    If grdDisponibles.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" And grdAsignados.MaxRows > 1 Then
        grdAsignados.Row = 1
        grdAsignados.Action = 7
    Else
        grdAsignados.Row = grdAsignados.MaxRows
    End If
    
    grdAsignados.RowHeight(grdAsignados.Row) = 10
    grdAsignados.Col = 1:  grdDisponibles.Col = 1
    grdAsignados.Text = grdDisponibles.Text
    grdAsignados.Col = 2
    grdAsignados.Text = garrTiposDatos(0, 1)
    grdAsignados.Col = 3
    grdAsignados.Text = 10
    grdAsignados.Col = 4
    grdAsignados.Text = 0
    grdAsignados.Col = 5
    grdAsignados.TypeButtonText = "Izquierda"
    grdAsignados.Col = 6
    grdAsignados.TypeButtonText = "Espacios"
    grdAsignados.Col = 7: grdDisponibles.Col = 2
    grdAsignados.Text = grdDisponibles.Text
'    grdAsignados.Col = 9: grdDisponibles.Col = 7
'    grdAsignados.Text = grdDisponibles.Text
    grdAsignados.Col = 10
    grdAsignados.Text = garrTiposDatos(0, 0)
    grdAsignados.Col = 11:  grdDisponibles.Col = 3
    grdAsignados.Text = grdDisponibles.Text
    grdAsignados.Col = 12:  grdDisponibles.Col = 4
    grdAsignados.Text = grdDisponibles.Text
    grdAsignados.Col = 13:  grdDisponibles.Col = 5
    grdAsignados.Text = grdDisponibles.Text
    grdAsignados.Col = 14: grdDisponibles.Col = 7
    grdAsignados.Text = IIf(Val(grdDisponibles.Text) = 1, "S", "N")
    grdAsignados.Col = 15: grdDisponibles.Col = 8
    grdAsignados.Text = grdDisponibles.Text
    
    
    'Carga disponibles para ordenamiento
    grdAsignados.Col = 12: blnIndTotal = grdAsignados.Text = 1
    grdAsignados.Col = 13: blnIndOrden = grdAsignados.Text = 1
    
    If blnIndTotal Or blnIndOrden Then
        grdDispOrdena.MaxRows = grdDispOrdena.MaxRows + 1
        grdDispOrdena.Row = grdDispOrdena.MaxRows
        grdDispOrdena.Col = 1: grdAsignados.Col = 1: grdDispOrdena.Text = grdAsignados.Text
        grdDispOrdena.Col = 2: grdAsignados.Col = 7: grdDispOrdena.Text = grdAsignados.Text
        grdDispOrdena.Col = 3: grdAsignados.Col = 11: grdDispOrdena.Text = grdAsignados.Text
    End If
    If grdDispOrdena.MaxRows > 0 Then grdDispOrdena.Row = 1
    
    ' Quitar desde disponibles.-
    grdDisponibles.Col = 2
    lngIdeCampo = grdDisponibles.Text
    
    grdDisponibles.Row = 1
    For i = 1 To grdDisponibles.MaxRows
        grdDisponibles.Row = i
        If lngIdeCampo = grdDisponibles.Text Then
            grdDisponibles.RowHidden = True
        End If
    Next
    
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If

    Rem PARA GRABAR DEBEN EXISTIR POR LO MENOS LOS 5 CAMPOS M�NIMOS
'    cmdGrabar.Enabled = grdAsignados.MaxRows > 4
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
'
'    cmdPreview.Enabled = grdAsignados.MaxRows > 0
'    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
End Sub

Private Sub cmdPonerEncab_Click()
    ' -----------------
    ' Asigna un campo.-
    ' -------------- cc
    Dim strSql As String
    Dim lngBanco As Long
    Dim lngPlaza As Long
    Dim lngSel As Long
    Dim lngIdeCampo As Long
    Dim i As Integer
    
    
    
    lngSel = -1
    For i = 1 To grdDispEncab.MaxRows
        grdDispEncab.Row = i
        If Not grdDispEncab.RowHidden Then
            lngSel = 1
            Exit For
        End If
    Next
    
    grdDispEncab.Row = grdDispEncab.ActiveRow
    
    If lngSel = -1 Or grdDispEncab.RowHidden Then
        MsgBox "Debe seleccionar un campo disponible", vbExclamation, "Asignar"
        Exit Sub
    End If
    
    ' Agregar hacia Asignados.-
    grdAsignadosEncab.MaxRows = grdAsignadosEncab.MaxRows + 1
    grdDispEncab.Col = 6
    If grdDispEncab.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" And grdAsignadosEncab.MaxRows > 0 Then
        grdAsignadosEncab.Row = 1
        grdAsignadosEncab.Action = 7
    Else
        grdAsignadosEncab.Row = grdAsignadosEncab.MaxRows
    End If
    
    grdAsignadosEncab.RowHeight(grdAsignadosEncab.Row) = 10
    grdAsignadosEncab.Col = 1:    grdDispEncab.Col = 1
    grdAsignadosEncab.Text = grdDispEncab.Text
    grdAsignadosEncab.Col = 2
    grdAsignadosEncab.Text = garrTiposDatos(0, 1)
    grdAsignadosEncab.Col = 3
    grdAsignadosEncab.Text = 10
    grdAsignadosEncab.Col = 4
    grdAsignadosEncab.Text = 0
    grdAsignadosEncab.Col = 5
    grdAsignadosEncab.TypeButtonText = "Izquierda"
    grdAsignadosEncab.Col = 6
    grdAsignadosEncab.TypeButtonText = "Espacios"
    grdAsignadosEncab.Col = 7:    grdDispEncab.Col = 2
    grdAsignadosEncab.Text = grdDispEncab.Text
'   grdAsignadosEncab.Col = 9:    grdDispEncab.Col = 5
'   grdAsignadosEncab.Text = grdDispEncab.Text
    grdAsignadosEncab.Col = 10
    grdAsignadosEncab.Text = garrTiposDatos(0, 0)
    grdAsignadosEncab.Col = 11:  grdDispEncab.Col = 3
    grdAsignadosEncab.Text = grdDispEncab.Text
    grdAsignadosEncab.Col = 12:  grdDispEncab.Col = 5
    grdAsignadosEncab.Text = IIf(Val(grdDispEncab.Text) = 1, "S", "N")
    grdAsignadosEncab.Col = 13: grdDispEncab.Col = 6
    grdAsignadosEncab.Text = grdDispEncab.Text
    
    
    ' Quitar desde disponibles.-
    grdDispEncab.Col = 2
    lngIdeCampo = grdDispEncab.Text
    
    grdDispEncab.Row = 1
    For i = 1 To grdDispEncab.MaxRows
        grdDispEncab.Row = i
        If lngIdeCampo = grdDispEncab.Text Then
            grdDispEncab.RowHidden = True
        End If
    Next
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    


Exit Sub
    Rem PARA GRABAR DEBEN EXISTIR POR LO MENOS LOS 5 CAMPOS M�NIMOS
'    cmdGrabar.Enabled = grdAsignadosEncab.MaxRows > 4
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
'    cmdPreview.Enabled = grdAsignadosEncab.MaxRows > 0
'    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
End Sub

Private Sub cmdPonerOrdena_Click()
   ' -----------------
    ' Asigna un campo.-
    ' -------------- cc
    Dim strSql      As String
    Dim lngBanco    As Long
    Dim lngPlaza    As Long
    Dim lngSel      As Long
    Dim lngIdeCampo As Long
    Dim i As Integer
    
    If grdDispOrdena.MaxRows = 0 Then Exit Sub
    
    lngSel = -1
    For i = 1 To grdDispOrdena.MaxRows
        grdDispOrdena.Row = i
        If Not grdDispOrdena.RowHidden Then
            lngSel = 1
            Exit For
        End If
    Next
    
    grdDispOrdena.Row = grdDispOrdena.ActiveRow
    
    If lngSel = -1 Or grdDispOrdena.RowHidden Then
        MsgBox "Debe seleccionar un campo disponible", vbExclamation, "Asignar"
        Exit Sub
    End If
    
  
    ' Agregar hacia Asignados.-
    grdOrdena.MaxRows = grdOrdena.MaxRows + 1
    grdOrdena.RowHeight(grdOrdena.MaxRows) = 10
    grdOrdena.Row = grdOrdena.MaxRows
    grdOrdena.Col = 1:  grdDispOrdena.Col = 1
    grdOrdena.Text = grdDispOrdena.Text
    grdOrdena.Col = 2:  grdDispOrdena.Col = 2
    grdOrdena.Text = grdDispOrdena.Text
    grdDispOrdena.Col = 3
    If Not EsTotalizable(Val(grdDispOrdena.Text)) Then
        grdOrdena.Col = 3
        grdOrdena.CellType = SS_CELL_TYPE_STATIC_TEXT
    End If
    
    grdOrdena.Col = 4:  grdDispOrdena.Col = 3
    grdOrdena.Text = grdDispOrdena.Text
    
    
    ' Quitar desde disponibles.-
    grdDispOrdena.Col = 2
    lngIdeCampo = grdDispOrdena.Text
    
    grdDispOrdena.Row = 1
    For i = 1 To grdDispOrdena.MaxRows
        grdDispOrdena.Row = i
        If lngIdeCampo = grdDispOrdena.Text Then
            grdDispOrdena.RowHidden = True
        End If
    Next
        
    
    
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    

End Sub

Private Sub cmdPonerTodEncab_Click()
    Dim K As Integer
    Dim i As Integer
    Dim blnVisibles As Boolean
    Dim lngIdeCampo As Long
        
    For i = 1 To grdDispEncab.MaxRows
        grdDispEncab.Row = i
        If Not grdDispEncab.RowHidden Then
            blnVisibles = True
            Exit For
        End If
    Next
    
    If Not blnVisibles Then Exit Sub
    
    For K = 0 To grdDispEncab.MaxRows
        grdDispEncab.Row = K
        If Not grdDispEncab.RowHidden Then
            grdAsignadosEncab.MaxRows = grdAsignadosEncab.MaxRows + 1
            grdDispEncab.Col = 6
            If grdDispEncab.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" And grdAsignadosEncab.MaxRows > 1 Then
                grdAsignadosEncab.Row = 1
                grdAsignadosEncab.Action = 7
            Else
                grdAsignadosEncab.Row = grdAsignadosEncab.MaxRows
            End If
            grdAsignadosEncab.RowHeight(grdAsignadosEncab.Row) = 10
            grdAsignadosEncab.Col = 1: grdDispEncab.Col = 1
            grdAsignadosEncab.Text = grdDispEncab.Text
            grdAsignadosEncab.Col = 2
            grdAsignadosEncab.Text = garrTiposDatos(0, 1)
            grdAsignadosEncab.Col = 3
            grdAsignadosEncab.Text = 10
            grdAsignadosEncab.Col = 4
            grdAsignadosEncab.Text = 0
            grdAsignadosEncab.Col = 5
            grdAsignadosEncab.TypeButtonText = "Izquierda"
            grdAsignadosEncab.Col = 6
            grdAsignadosEncab.TypeButtonText = "Espacios"
            grdAsignadosEncab.Col = 7: grdDispEncab.Col = 2
            grdAsignadosEncab.Text = grdDispEncab.Text
        '   grdAsignadosEncab.Col = 9:    grdDispEncab.Col = 5
        '   grdAsignadosEncab.Text = grdDispEncab.Text
            grdAsignadosEncab.Col = 10
            grdAsignadosEncab.Text = garrTiposDatos(0, 0)
            grdAsignadosEncab.Col = 11:  grdDispEncab.Col = 3
            grdAsignadosEncab.Text = grdDispEncab.Text
            grdAsignadosEncab.Col = 12:  grdDispEncab.Col = 5
            grdAsignadosEncab.Text = IIf(Val(grdDispEncab.Text) = 1, "S", "N")
            grdAsignadosEncab.Col = 13: grdDispEncab.Col = 6
            grdAsignadosEncab.Text = grdDispEncab.Text

            grdDispEncab.Col = 2
            lngIdeCampo = grdDispEncab.Text
        
            grdDispEncab.Row = K
            For i = K To grdDispEncab.MaxRows
                grdDispEncab.Row = i
                If lngIdeCampo = grdDispEncab.Text Then
                    grdDispEncab.RowHidden = True
                End If
            Next
        End If
    Next K
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
    
'    lstDispEncab.Clear
    CmdGrabar.Enabled = True
    tlbAcciones.Buttons("Grabar").Enabled = CmdGrabar.Enabled
    cmdPreview.Enabled = True
    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
End Sub

Private Sub cmdPonerTodoOrdena_Click()
    Dim K       As Integer
    Dim IndTot  As Boolean
    Dim i As Integer
    Dim blnVisibles As Boolean
    Dim lngIdeCampo As Long
        
    For i = 1 To grdDispOrdena.MaxRows
        grdDispOrdena.Row = i
        If Not grdDispOrdena.RowHidden Then
            blnVisibles = True
            Exit For
        End If
    Next
    
    If Not blnVisibles Then Exit Sub

    For K = 0 To grdDispOrdena.MaxRows
        grdDispOrdena.Row = K
        
        If Not grdDispOrdena.RowHidden Then
            grdOrdena.MaxRows = grdOrdena.MaxRows + 1
            grdOrdena.RowHeight(grdOrdena.MaxRows) = 10
            grdOrdena.Row = grdOrdena.MaxRows
            grdOrdena.Col = 1: grdDispOrdena.Col = 1
            grdOrdena.Text = grdDispOrdena.Text
            grdOrdena.Col = 2: grdDispOrdena.Col = 2
            grdOrdena.Text = grdDispOrdena.Text
            grdDispOrdena.Col = 3
            If Not EsTotalizable(Val(grdDispOrdena.Text)) Then
                grdOrdena.Col = 3
                grdOrdena.CellType = SS_CELL_TYPE_STATIC_TEXT
            End If
            
            grdDispOrdena.Col = 2
            lngIdeCampo = grdDispOrdena.Text
        
            grdDispOrdena.Row = K
            For i = K To grdDispOrdena.MaxRows
                grdDispOrdena.Row = i
                If lngIdeCampo = grdDispOrdena.Text Then
                    grdDispOrdena.RowHidden = True
                End If
            Next
        End If
    
    Next K
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    

End Sub

Rem AGREGA TODOS LOS CAMPOS A LA CONFIGURACI�N
Private Sub cmdPonerTodos_Click()
    Dim K As Integer
    Dim i As Integer
    Dim blnVisibles As Boolean
    Dim lngIdeCampo As Long
    Dim blnIndTotal As Boolean
    Dim blnIndOrden As Boolean
    
    For i = 1 To grdDisponibles.MaxRows
        grdDisponibles.Row = i
        If Not grdDisponibles.RowHidden Then
            blnVisibles = True
            Exit For
        End If
    Next
    
    If Not blnVisibles Then Exit Sub

    For K = 0 To grdDisponibles.MaxRows
        grdDisponibles.Row = K
        If Not grdDisponibles.RowHidden Then
            grdAsignados.MaxRows = grdAsignados.MaxRows + 1
            grdDisponibles.Col = 8
            If grdDisponibles.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" And grdAsignados.MaxRows > 1 Then
                grdAsignados.Row = 1
                grdAsignados.Action = 7
            Else
                grdAsignados.Row = grdAsignados.MaxRows
            End If
            grdAsignados.RowHeight(grdAsignados.Row) = 10
            grdAsignados.Col = 1: grdDisponibles.Col = 1
            grdAsignados.Text = grdDisponibles.Text
            grdAsignados.Col = 2
            grdAsignados.Text = garrTiposDatos(0, 1)
            grdAsignados.Col = 3
            grdAsignados.Text = 10
            grdAsignados.Col = 4
            grdAsignados.Text = 0
            grdAsignados.Col = 5
            grdAsignados.TypeButtonText = "Izquierda"
            grdAsignados.Col = 6
            grdAsignados.TypeButtonText = "Espacios"
            grdAsignados.Col = 7: grdDisponibles.Col = 2
            grdAsignados.Text = grdDisponibles.Text
        '   grdAsignados.Col = 9: grdDisponibles.Col = 7
        '   grdAsignados.Text = grdDisponibles.Text
            grdAsignados.Col = 10
            grdAsignados.Text = garrTiposDatos(0, 0)
            grdAsignados.Col = 11:  grdDisponibles.Col = 3
            grdAsignados.Text = grdDisponibles.Text
            grdAsignados.Col = 12:  grdDisponibles.Col = 4
            grdAsignados.Text = grdDisponibles.Text
            grdAsignados.Col = 13:  grdDisponibles.Col = 5
            grdAsignados.Text = grdDisponibles.Text
            grdAsignados.Col = 14: grdDisponibles.Col = 7
            grdAsignados.Text = IIf(Val(grdDisponibles.Text) = 1, "S", "N")
            grdAsignados.Col = 15: grdDisponibles.Col = 8
            grdAsignados.Text = grdDisponibles.Text
    
            'Carga disponibles para ordenamiento
            grdAsignados.Col = 12: blnIndTotal = Val(grdAsignados.Text) = 1
            grdAsignados.Col = 13: blnIndOrden = Val(grdAsignados.Text) = 1
    
            If blnIndTotal Or blnIndOrden Then
                grdDispOrdena.MaxRows = grdDispOrdena.MaxRows + 1
                grdDispOrdena.Row = grdDispOrdena.MaxRows
                grdDispOrdena.Col = 1: grdAsignados.Col = 1: grdDispOrdena.Text = grdAsignados.Text
                grdDispOrdena.Col = 2: grdAsignados.Col = 7: grdDispOrdena.Text = grdAsignados.Text
                grdDispOrdena.Col = 3: grdAsignados.Col = 11: grdDispOrdena.Text = grdAsignados.Text
            End If
    
            grdDisponibles.Col = 2
            lngIdeCampo = grdDisponibles.Text
        
            grdDisponibles.Row = K
            For i = K To grdDisponibles.MaxRows
                grdDisponibles.Row = i
                If lngIdeCampo = grdDisponibles.Text Then
                    grdDisponibles.RowHidden = True
                End If
            Next
        End If
    Next K
    If grdDispOrdena.MaxRows > 0 Then grdDispOrdena.Row = 1

    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If

'    lstDisponibles.Clear
    CmdGrabar.Enabled = True
    tlbAcciones.Buttons("Grabar").Enabled = CmdGrabar.Enabled
    cmdPreview.Enabled = True
    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
End Sub

Private Sub cmdPonerTodTot_Click()
    Dim K As Integer
    Dim i As Integer
    Dim blnVisibles As Boolean
    Dim lngIdeCampo As Long
        
    For i = 1 To grdDispTot.MaxRows
        grdDispTot.Row = i
        If Not grdDispTot.RowHidden Then
            blnVisibles = True
            Exit For
        End If
    Next
    
    If Not blnVisibles Then Exit Sub


    For K = 0 To grdDispTot.MaxRows
        grdDispTot.Row = K
        
        If Not grdDispTot.RowHidden Then
            grdAsignadosTot.MaxRows = grdAsignadosTot.MaxRows + 1
            grdDispTot.Col = 6
            If grdDispTot.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" And grdAsignadosTot.MaxRows > 1 Then
                grdAsignadosTot.Row = 1
                grdAsignadosTot.Action = 7
            Else
                grdAsignadosTot.Row = grdAsignadosTot.MaxRows
            End If
            grdAsignadosTot.RowHeight(grdAsignadosTot.Row) = 10
            grdAsignadosTot.Col = 1: grdDispTot.Col = 1
            grdAsignadosTot.Text = grdDispTot.Text
            grdAsignadosTot.Col = 2
            grdAsignadosTot.Text = garrTiposDatos(0, 1)
            grdAsignadosTot.Col = 3
            grdAsignadosTot.Text = 10
            grdAsignadosTot.Col = 4
            grdAsignadosTot.Text = 0
            grdAsignadosTot.Col = 5
            grdAsignadosTot.TypeButtonText = "Izquierda"
            grdAsignadosTot.Col = 6
            grdAsignadosTot.TypeButtonText = "Espacios"
            grdAsignadosTot.Col = 7: grdDispTot.Col = 2
            grdAsignadosTot.Text = grdDispTot.Text
        '   grdAsignadosTot.Col = 9: grdDispTot.Col = 5
        '   grdAsignadosTot.Text = grdDispTot.Text
            grdAsignadosTot.Col = 10
            grdAsignadosTot.Text = garrTiposDatos(0, 0)
            grdAsignadosTot.Col = 11:  grdDispTot.Col = 3
            grdAsignadosTot.Text = grdDispTot.Text
            grdAsignadosTot.Col = 12: grdDispTot.Col = 5
            grdAsignadosTot.Text = IIf(Val(grdDispTot.Text) = 1, "S", "N")
            grdAsignadosTot.Col = 13: grdDispTot.Col = 6
            grdAsignadosTot.Text = grdDispTot.Text
            
            grdDispTot.Col = 2
            lngIdeCampo = grdDispTot.Text
        
            grdDispTot.Row = K
            For i = K To grdDispTot.MaxRows
                grdDispTot.Row = i
                If lngIdeCampo = grdDispTot.Text Then
                    grdDispTot.RowHidden = True
                End If
            Next
        End If
    
    Next K
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
    
'    lstDispTot.Clear
    CmdGrabar.Enabled = True
    tlbAcciones.Buttons("Grabar").Enabled = CmdGrabar.Enabled
    cmdPreview.Enabled = True
    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
End Sub

Private Sub cmdPonerTot_Click()
 ' -----------------
    ' Asigna un campo.-
    ' -------------- cc
    Dim strSql As String
    Dim lngBanco As Long
    Dim lngPlaza As Long
    Dim lngSel As Long
    Dim lngIdeCampo As Long
    Dim i As Integer
    
    lngSel = -1
    For i = 1 To grdDispTot.MaxRows
        grdDispTot.Row = i
        If Not grdDispTot.RowHidden Then
            lngSel = 1
            Exit For
        End If
    Next
    
    grdDispTot.Row = grdDispTot.ActiveRow
    
    If lngSel = -1 Or grdDispTot.RowHidden Then
        MsgBox "Debe seleccionar un campo disponible", vbExclamation, "Asignar"
        Exit Sub
    End If
    
    ' Agregar hacia Asignados.-
    grdAsignadosTot.MaxRows = grdAsignadosTot.MaxRows + 1
    grdDispTot.Col = 6
    If grdDispTot.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" And grdAsignadosTot.MaxRows > 1 Then
        grdAsignadosTot.Row = 1
        grdAsignadosTot.Action = 7
    Else
        grdAsignadosTot.Row = grdAsignadosTot.MaxRows
    End If
    
    grdAsignadosTot.RowHeight(grdAsignadosTot.Row) = 10
    grdAsignadosTot.Col = 1:  grdDispTot.Col = 1
    grdAsignadosTot.Text = grdDispTot.Text
    grdAsignadosTot.Col = 2
    grdAsignadosTot.Text = garrTiposDatos(0, 1)
    grdAsignadosTot.Col = 3
    grdAsignadosTot.Text = 10
    grdAsignadosTot.Col = 4
    grdAsignadosTot.Text = 0
    grdAsignadosTot.Col = 5
    grdAsignadosTot.TypeButtonText = "Izquierda"
    grdAsignadosTot.Col = 6
    grdAsignadosTot.TypeButtonText = "Espacios"
    grdAsignadosTot.Col = 7: grdDispTot.Col = 2
    grdAsignadosTot.Text = grdDispTot.Text
'    grdAsignadosTot.Col = 9: grdDispTot.Col = 5
'    grdAsignadosTot.Text = grdDispTot.Text
    grdAsignadosTot.Col = 10
    grdAsignadosTot.Text = garrTiposDatos(0, 0)
    grdAsignadosTot.Col = 11:  grdDispTot.Col = 3
    grdAsignadosTot.Text = grdDispTot.Text
    grdAsignadosTot.Col = 12: grdDispTot.Col = 5
    grdAsignadosTot.Text = IIf(Val(grdDispTot.Text) = 1, "S", "N")
    grdAsignadosTot.Col = 13: grdDispTot.Col = 6
    grdAsignadosTot.Text = grdDispTot.Text
    
    
    ' Quitar desde disponibles.-
    grdDispTot.Col = 2
    lngIdeCampo = grdDispTot.Text
    
    grdDispTot.Row = 1
    For i = 1 To grdDispTot.MaxRows
        grdDispTot.Row = i
        If lngIdeCampo = grdDispTot.Text Then
            grdDispTot.RowHidden = True
        End If
    Next
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If

    Rem PARA GRABAR DEBEN EXISTIR POR LO MENOS LOS 5 CAMPOS M�NIMOS
'    cmdGrabar.Enabled = grdAsignadosTot.MaxRows > 4
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
'    cmdPreview.Enabled = grdAsignadosTot.MaxRows > 0
'    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
End Sub

Private Sub cmdSacar_Click()
    
    ' ----------------------------------
    ' Quitar un campo, desde Asignados
    ' ------------------------------- cc
    
    Dim lngCodigo   As Long
    Dim lngSel      As Long
    Dim strSql      As String
    Dim i As Integer
    Dim blnIndTotal As Boolean
    Dim blnIndOrden As Boolean
    
    If Not grdAsignados.MaxRows > 0 Then Exit Sub
    lngSel = grdAsignados.ActiveRow
    
    If lngSel < 1 Then
        MsgBox "Debe seleccionar un campo asignado", vbExclamation, "Asignar"
        GoTo Salir
    End If
    
    ' Poner en disponibles.-
    grdAsignados.Row = lngSel
    'grdAsignados.Col = 1
    'strCampo = grdAsignados.Text
    grdAsignados.Col = 7
    lngCodigo = grdAsignados.Text
    If Not lngCodigo = -99 Then
     '   lstDisponibles.AddItem strCampo
     '   lstDisponibles.ItemData(lstDisponibles.NewIndex) = lngCodigo
     
            grdDisponibles.Col = 2
            grdDisponibles.Row = 1
            For i = 1 To grdDisponibles.MaxRows
                grdDisponibles.Row = i
                If lngCodigo = grdDisponibles.Text Then
                    grdDisponibles.RowHidden = False
                End If
            Next
            
    End If
    
    grdAsignados.Col = 12: blnIndTotal = Val(grdAsignados.Text) = 1
    grdAsignados.Col = 13: blnIndOrden = Val(grdAsignados.Text) = 1
    

    If blnIndTotal Or blnIndOrden Then
        grdAsignados.Col = 1
        grdDispOrdena.Col = 1
        grdOrdena.Col = 1
        For i = 1 To grdDispOrdena.MaxRows
            
            grdDispOrdena.Row = i
            If grdDispOrdena.Text = grdAsignados.Text Then
                grdDispOrdena.Action = 5
                grdDispOrdena.MaxRows = grdDispOrdena.MaxRows - 1
            End If
            
            grdOrdena.Row = i
            If grdOrdena.Text = grdAsignados.Text Then
                grdOrdena.Action = 5
                grdOrdena.MaxRows = grdOrdena.MaxRows - 1
            End If
            
        Next i
    
    End If
    
    
    
    
    ' Quitar desde Asignados.-
    grdAsignados.Action = 5
    grdAsignados.MaxRows = grdAsignados.MaxRows - 1
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
    Rem PARA GRABAR DEBEN EXISTIR POR LO MENOS LOS 5 CAMPOS M�NIMOS
'    cmdGrabar.Enabled = grdAsignados.MaxRows > 4
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
'    cmdPreview.Enabled = grdAsignadosEncab.MaxRows > 0
'    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
Salir:
    Exit Sub
End Sub

Private Sub cmdSacarEncab_Click()
    
    ' ----------------------------------
    ' Quitar un campo, desde Asignados
    ' ------------------------------- cc
    
    Dim lngCodigo   As Long
    Dim lngSel      As Long
    Dim strSql      As String
    Dim i As Integer
    
    If Not grdAsignadosEncab.MaxRows > 0 Then Exit Sub
    lngSel = grdAsignadosEncab.ActiveRow
    
    If lngSel < 1 Then
        MsgBox "Debe seleccionar un campo asignado", vbExclamation, "Asignar"
        GoTo Salir
    End If
    
    ' Poner en disponibles.-
    grdAsignadosEncab.Row = lngSel
'    grdAsignadosEncab.Col = 1
'    strCampo = grdAsignadosEncab.Text
    grdAsignadosEncab.Col = 7
    lngCodigo = grdAsignadosEncab.Text
    If Not lngCodigo = -99 Then
 '       lstDispEncab.AddItem strCampo
 '       lstDispEncab.ItemData(lstDispEncab.NewIndex) = lngCodigo
        
            grdDispEncab.Col = 2
            
            grdDispEncab.Row = 1
            For i = 1 To grdDispEncab.MaxRows
                grdDispEncab.Row = i
                If lngCodigo = grdDispEncab.Text Then
                    grdDispEncab.RowHidden = False
                End If
            Next
        
    End If
    
    
    ' Quitar desde Asignados.-
    grdAsignadosEncab.Action = 5
    grdAsignadosEncab.MaxRows = grdAsignadosEncab.MaxRows - 1
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
    
    Rem PARA GRABAR DEBEN EXISTIR POR LO MENOS LOS 5 CAMPOS M�NIMOS
'    cmdGrabar.Enabled = grdAsignadosEncab.MaxRows > 4
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
Salir:
    Exit Sub
End Sub

Private Sub cmdSacarOrdena_Click()
     ' ----------------------------------
    ' Quitar un campo, desde Asignados
    ' ------------------------------- cc
    
    Dim lngCodigo   As Long
    Dim lngSel      As Long
    Dim strSql      As String
    Dim i As Integer
    
    If Not grdOrdena.MaxRows > 0 Then Exit Sub
    lngSel = grdOrdena.ActiveRow
    
    If lngSel < 1 Then
        MsgBox "Debe seleccionar un campo asignado", vbExclamation, "Asignar"
        GoTo Salir
    End If
    
    
    
    ' Poner en disponibles.-
    grdOrdena.Row = lngSel
    'grdOrdena.Col = 1
    'strCampo = grdOrdena.Text
    grdOrdena.Col = 2
    lngCodigo = grdOrdena.Text
    If Not lngCodigo = -99 Then
'        lstOrdena.AddItem strCampo
'        lstOrdena.ItemData(lstOrdena.NewIndex) = lngCodigo
        
            grdDispOrdena.Col = 2
            grdDispOrdena.Row = 1
            For i = 1 To grdDispOrdena.MaxRows
                grdDispOrdena.Row = i
                If lngCodigo = grdDispOrdena.Text Then
                    grdDispOrdena.RowHidden = False
                End If
            Next
        
    End If
    
    ' Quitar desde Asignados.-
    grdOrdena.Action = 5
    grdOrdena.MaxRows = grdOrdena.MaxRows - 1
    
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
    
Salir:
    Exit Sub
End Sub

Private Sub cmdSacarTodEncab_Click()
Dim K As Integer
    If grdAsignadosEncab.ActiveRow < 1 Then Exit Sub
    
    grdDispEncab.Row = 1
    For K = 1 To grdDispEncab.MaxRows
        grdDispEncab.Row = K
        grdDispEncab.RowHidden = False
    Next
    
    
    ' Quitar desde Asignados.-
    grdAsignadosEncab.MaxRows = 0
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
    
    
'    cmdGrabar.Enabled = False
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
'    cmdPreview.Enabled = False
'    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
End Sub

Private Sub cmdSacarTodoOrdena_Click()
Dim K       As Integer

    If grdOrdena.ActiveRow < 1 Then Exit Sub
    
    
    grdDispOrdena.Row = 1
    For K = 1 To grdDispOrdena.MaxRows
        grdDispOrdena.Row = K
        grdDispOrdena.RowHidden = False
    Next
    
    ' Quitar desde Asignados.-
    grdOrdena.MaxRows = 0
    
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
    
End Sub

Rem SACA TODOS LOSC CAMPOS DE LA CONFIGURACI�N
Private Sub cmdSacarTodos_Click()
Dim K As Integer
    If grdAsignados.ActiveRow < 1 Then Exit Sub
    
    grdDisponibles.Row = 1
    For K = 1 To grdDisponibles.MaxRows
        grdDisponibles.Row = K
        grdDisponibles.RowHidden = False
    Next
    

' Quitar desde Ordenamiento
    grdDispOrdena.MaxRows = 0
    grdOrdena.MaxRows = 0

    ' Quitar desde Asignados.-
    grdAsignados.MaxRows = 0
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
    
'    cmdGrabar.Enabled = False
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
'    cmdPreview.Enabled = False
'    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
End Sub

Private Sub cmdSacarTodTot_Click()
Dim K As Integer
    If grdAsignadosTot.ActiveRow < 1 Then Exit Sub
    
    
    grdDispTot.Row = 1
    For K = 1 To grdDispTot.MaxRows
        grdDispTot.Row = K
        grdDispTot.RowHidden = False
    Next
    
    
'    For K = 1 To grdAsignadosTot.MaxRows
'        ' Poner en disponibles.-
'        grdAsignadosTot.Row = K
'        grdAsignadosTot.Col = 7
'        If Not Val(grdAsignadosTot.Text) = -99 Then
'            grdAsignadosTot.Col = 1
'            lstDispTot.AddItem grdAsignadosTot.Text
'            grdAsignadosTot.Col = 7
'            lstDispTot.ItemData(lstDispTot.NewIndex) = Val(grdAsignadosTot.Text)
'        End If
'    Next K
    
    ' Quitar desde Asignados.-
    grdAsignadosTot.MaxRows = 0
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
'    cmdGrabar.Enabled = False
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
'    cmdPreview.Enabled = False
'    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
End Sub

Private Sub cmdSacarTot_Click()
 ' ----------------------------------
    ' Quitar un campo, desde Asignados
    ' ------------------------------- cc
    
    Dim lngCodigo   As Long
    Dim lngSel      As Long
    Dim strSql      As String
    Dim i As Integer
    
    If Not grdAsignadosTot.MaxRows > 0 Then Exit Sub
    lngSel = grdAsignadosTot.ActiveRow
    
    If lngSel < 1 Then
        MsgBox "Debe seleccionar un campo asignado", vbExclamation, "Asignar"
        GoTo Salir
    End If
    
    ' Poner en disponibles.-
    grdAsignadosTot.Row = lngSel
'    grdAsignadosTot.Col = 1
'    strCampo = grdAsignadosTot.Text
    grdAsignadosTot.Col = 7
    lngCodigo = grdAsignadosTot.Text
    If Not lngCodigo = -99 Then
'        lstDispTot.AddItem strCampo
'        lstDispTot.ItemData(lstDispTot.NewIndex) = lngCodigo
        
            grdDispTot.Col = 2
            grdDispTot.Row = 1
            For i = 1 To grdDispTot.MaxRows
                grdDispTot.Row = i
                If lngCodigo = grdDispTot.Text Then
                    grdDispTot.RowHidden = False
                End If
            Next
        
        
    End If
    
    ' Quitar desde Asignados.-
    grdAsignadosTot.Action = 5
    grdAsignadosTot.MaxRows = grdAsignadosTot.MaxRows - 1
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
    Rem PARA GRABAR DEBEN EXISTIR POR LO MENOS LOS 5 CAMPOS M�NIMOS
'    cmdGrabar.Enabled = grdAsignadosTot.MaxRows > 4
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
'    cmdPreview.Enabled = grdAsignadosTot.MaxRows > 0
'    tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
Salir:
    Exit Sub
End Sub

Private Sub cmdSalir_Click()
    If gblnTrans Then
        If MsgBox("Existen cambios sin grabar." & Chr(13) & "�Desea abandonar los cambios?", vbInformation + vbYesNo, "Configuraci�n") = vbYes Then
            gAdoCon.RollbackTrans
            gblnTrans = False
        Else
            Exit Sub
        End If
    End If
    Unload Me
End Sub


Private Sub cmdSubir_Click()
    Dim RowActual As Long
    Dim RowAnterior As Long
    
    If grdAsignados.ActiveRow < 1 Then
        MsgBox "Para mover un campo debe seleccionarlo...", vbExclamation, "Asignar"
        Exit Sub
    Else
        If grdAsignados.ActiveRow = 1 Then Exit Sub
        grdAsignados.Row = grdAsignados.ActiveRow
        grdAsignados.Col = 15
        If grdAsignados.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" Then
            MsgBox "Este Campo no puede ser cambiado de posici�n...", vbExclamation, "Asignar"
            Exit Sub
        Else
            RowActual = grdAsignados.ActiveRow
            RowAnterior = grdAsignados.ActiveRow - 1
            grdAsignados.Row = RowAnterior
            If grdAsignados.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" Then
                MsgBox "El Campo no puede subir m�s de posici�n...", vbExclamation, "Asignar"
                Exit Sub
            End If
            grdAsignados.Row = RowActual
        End If
    End If
    
    
    grdAsignados.Col = 1
    grdAsignados.Row = grdAsignados.ActiveRow
    grdAsignados.Col2 = grdAsignados.MaxCols
    grdAsignados.Row2 = grdAsignados.ActiveRow
    grdAsignados.DestCol = 1
    grdAsignados.DestRow = grdAsignados.ActiveRow - 1
    grdAsignados.Action = 21
    grdAsignados.Row = grdAsignados.ActiveRow - 1
    grdAsignados.Action = 0
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
End Sub


Private Sub cmdPreview_Click()

    
    Select Case tabSecciones.Tab
        Case 0
            If grdAsignadosEncab.MaxRows > 0 Then
                frmPreview.gsSeccion = "ENCABEZADO"
                Set frmPreview.sprdAsignados = grdAsignadosEncab
                frmPreview.Show vbModal
            End If
        Case 1
            If grdAsignados.MaxRows > 0 Then
                frmPreview.gsSeccion = "DETALLE"
                Set frmPreview.sprdAsignados = grdAsignados
                frmPreview.Show vbModal
            End If
        Case 2
            If grdAsignadosTot.MaxRows > 0 Then
                frmPreview.gsSeccion = "TOTALES"
                Set frmPreview.sprdAsignados = grdAsignadosTot
                frmPreview.Show vbModal
            End If
    End Select
    
End Sub

Private Sub cmdSubirEncab_Click()
    Dim RowActual As Long
    Dim RowAnterior As Long
    
     If grdAsignadosEncab.ActiveRow < 1 Then
        MsgBox "Para mover un campo debe seleccionarlo...", vbExclamation, "Asignar"
        Exit Sub
    Else
        If grdAsignadosEncab.ActiveRow = 1 Then Exit Sub
        grdAsignadosEncab.Row = grdAsignadosEncab.ActiveRow
        grdAsignadosEncab.Col = 13
        If grdAsignadosEncab.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" Then
            MsgBox "Este Campo no puede ser cambiado de posici�n...", vbExclamation, "Asignar"
            Exit Sub
        Else
            RowActual = grdAsignadosEncab.ActiveRow
            RowAnterior = grdAsignadosEncab.ActiveRow - 1
            grdAsignadosEncab.Row = RowAnterior
            If grdAsignadosEncab.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" Then
                MsgBox "El Campo no puede subir m�s de posici�n...", vbExclamation, "Asignar"
                Exit Sub
            End If
            grdAsignadosEncab.Row = RowActual
        End If
    End If
    grdAsignadosEncab.Col = 1
    grdAsignadosEncab.Row = grdAsignadosEncab.ActiveRow
    grdAsignadosEncab.Col2 = grdAsignadosEncab.MaxCols
    grdAsignadosEncab.Row2 = grdAsignadosEncab.ActiveRow
    grdAsignadosEncab.DestCol = 1
    grdAsignadosEncab.DestRow = grdAsignadosEncab.ActiveRow - 1
    grdAsignadosEncab.Action = 21
    grdAsignadosEncab.Row = grdAsignadosEncab.ActiveRow - 1
    grdAsignadosEncab.Action = 0
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
End Sub

Private Sub cmdSubirOrden_Click()
    If grdOrdena.ActiveRow < 1 Then
        MsgBox "Para mover un campo debe seleccionarlo...", vbExclamation, "Asignar"
        Exit Sub
    Else
        If grdOrdena.ActiveRow = 1 Then Exit Sub
    End If
    grdOrdena.Col = 1
    grdOrdena.Row = grdOrdena.ActiveRow
    grdOrdena.Col2 = grdOrdena.MaxCols
    grdOrdena.Row2 = grdOrdena.ActiveRow
    grdOrdena.DestCol = 1
    grdOrdena.DestRow = grdOrdena.ActiveRow - 1
    grdOrdena.Action = 21
    grdOrdena.Row = grdOrdena.ActiveRow - 1
    grdOrdena.Action = 0
    
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
    
End Sub

Private Sub cmdSubirTot_Click()
    Dim RowActual As Long
    Dim RowAnterior As Long
    
     If grdAsignadosTot.ActiveRow < 1 Then
        MsgBox "Para mover un campo debe seleccionarlo...", vbExclamation, "Asignar"
        Exit Sub
    Else
        If grdAsignadosTot.ActiveRow = 1 Then Exit Sub
        grdAsignadosTot.Row = grdAsignadosTot.ActiveRow
        grdAsignadosTot.Col = 13
        If grdAsignadosTot.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" Then
            MsgBox "Este Campo no puede ser cambiado de posici�n...", vbExclamation, "Asignar"
            Exit Sub
        Else
            RowActual = grdAsignadosTot.ActiveRow
            RowAnterior = grdAsignadosTot.ActiveRow - 1
            grdAsignadosTot.Row = RowAnterior
            If grdAsignadosTot.Text = "CODIGO DE REGISTRO (CAMPO CTRL.)" Then
                MsgBox "El Campo no puede subir m�s de posici�n...", vbExclamation, "Asignar"
                Exit Sub
            End If
            grdAsignadosTot.Row = RowActual
        End If
    End If
    
    grdAsignadosTot.Col = 1
    grdAsignadosTot.Row = grdAsignadosTot.ActiveRow
    grdAsignadosTot.Col2 = grdAsignadosTot.MaxCols
    grdAsignadosTot.Row2 = grdAsignadosTot.ActiveRow
    grdAsignadosTot.DestCol = 1
    grdAsignadosTot.DestRow = grdAsignadosTot.ActiveRow - 1
    grdAsignadosTot.Action = 21
    grdAsignadosTot.Row = grdAsignadosTot.ActiveRow - 1
    grdAsignadosTot.Action = 0
    
    If Not gblnTrans Then
        gAdoCon.BeginTrans
        gblnTrans = True
    End If
    
End Sub

Private Sub Form_Load()

   
    Call Carga_Imagenes
    staEstado.Panels(2) = "V " & App.Major & "." & App.Minor

'    Call Carga_Corp_Comunas
'    Call Carga_Corp_Paises
'    Call Carga_Corp_Bancos
'    Call Carga_Corp_Plazas
'    Call Carga_Arr_Mdas_Local
    
    Call Carga_Cbo_Separador
    Call Carga_Cbo_Decimal
    
    Call Carga_Combo_Par(cboArchivo, 1, 6)
    Call Carga_Combo_Par(cboFormato, 1, 8)
    Call Carga_Combo_Par(cboEntidad, 1, 11)
    
    Call Carga_Tipos_Datos
    Call Carga_Cbo_Grilla
    
    Call Carga_Combo_Par(cboProducto, 1, 10)
    Me.Top = 400
    Me.Left = 3090
    
    tabSecciones.Tab = 0
    tabSecciones.TabVisible(3) = False
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim msg As String
    msg = "�Realmente desea salir de la aplicaci�n?"
    If UnloadMode > 0 Then
        If MsgBox(msg, vbYesNo, "Mensaje") = vbYes Then
        Else
            Cancel = True
        End If
    End If
End Sub

Private Sub grdAsignados_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
    grdAsignados.Col = Col
    grdAsignados.Row = Row
    
    If Col = 5 Then
        If grdAsignados.TypeButtonText = "Derecha" Then
            grdAsignados.TypeButtonText = "Izquierda"
        Else
            grdAsignados.TypeButtonText = "Derecha"
        End If
    
    ElseIf Col = 6 Then
        If grdAsignados.TypeButtonText = "Espacios" Then
            grdAsignados.TypeButtonText = "Ceros"
        Else
            grdAsignados.TypeButtonText = "Espacios"
        End If
    
    ElseIf Col = 9 And grdAsignados.CellType = SS_CELL_TYPE_BUTTON And Not cboEntidad.Text = "" Then
    
        grdAsignados.Col = 14
        If grdAsignados.Text = "N" Then
            MsgBox "Este campo no admite conversi�n", vbExclamation, "Conversi�n"
            Exit Sub
        End If

        grdAsignados.Col = 9
        frmConvertir.gsConvertir = grdAsignados.TypeButtonText
        frmConvertir.gsIdeEnti = cboEntidad.ItemData(cboEntidad.ListIndex)
        frmConvertir.gsRut = cboEntidad.Text
        frmConvertir.gsNombre = cboEntidad.Text
        frmConvertir.gsIdeProducto = cboProducto.ItemData(cboProducto.ListIndex)
        frmConvertir.gsNomProducto = cboProducto.List(cboProducto.ListIndex)
        frmConvertir.gsIdeFormato = cboFormato.ItemData(cboFormato.ListIndex)
        frmConvertir.gsNomFormato = cboFormato.List(cboFormato.ListIndex)
        frmConvertir.gsIdeSucursal = 1
        frmConvertir.gsNomSucursal = 1
        frmConvertir.gsTipoInforme = cboArchivo.ItemData(cboArchivo.ListIndex)
        
        frmConvertir.gsIdeSeccion = strSeccDetalle
        frmConvertir.gsNomSeccion = "DETALLE"
        
        grdAsignados.Col = 1: frmConvertir.gsNomCampo = grdAsignados.Text
        grdAsignados.Col = 7: frmConvertir.gsIdeCampo = grdAsignados.Value
        frmConvertir.Show vbModal
        grdAsignados.Col = 9: grdAsignados.TypeButtonText = frmConvertir.gsConvertir
        Unload frmConvertir

    End If
    
    grdAsignados.Col = 1
    grdAsignados.Action = 0
End Sub


Private Sub grdAsignados_Change(ByVal Col As Long, ByVal Row As Long)
    Dim strTipo As String

' Si tipo es fecha, actualizar precisi�n.-
Dim TipoAnt As String

    grdAsignados.Col = Col
    grdAsignados.Row = Row
    If Col = 2 Then
        grdAsignados.Row = Row
        grdAsignados.Col = 2
        strTipo = grdAsignados.Text

        If grdAsignados.Text = "Fecha" Then
            grdAsignados.Col = 9
            If grdAsignados.TypeButtonText = "SI" Then
                grdAsignados.Col = 2
                grdAsignados.Text = grdAsignados.TypeComboBoxString
                MsgBox "Antes de modificar a tipo Fecha, debe eliminar la conversi�n de Datos", vbCritical, "Configuracion Archivo Plano"
                Exit Sub
            End If
            grdAsignados.Col = 3
            grdAsignados.Text = 6
            grdAsignados.Col = 4
            grdAsignados.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignados.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignados.Text = 0
            grdAsignados.Col = 5
            grdAsignados.TypeButtonText = "Izquierda"
            grdAsignados.Col = 6
            grdAsignados.TypeButtonText = "Espacios"
            grdAsignados.Col = 8
            grdAsignados.CellType = SS_CELL_TYPE_COMBOBOX
            grdAsignados.TypeComboBoxList = _
                "ddmmyy" & vbTab & "mmddyy" & vbTab & "yymmdd" & vbTab & _
                "ddmmyyyy" & vbTab & "mmddyyyy" & vbTab & "yyyymmdd" & vbTab & _
                "dd-mm-yy" & vbTab & "mm-dd-yy" & vbTab & "yy-mm-dd" & vbTab & _
                "dd-mm-yyyy" & vbTab & "mm-dd-yyyy" & vbTab & "yyyy-mm-dd" & vbTab & _
                "dd/mm/yy" & vbTab & "mm/dd/yy" & vbTab & "yy/mm/dd" & vbTab & _
                "dd/mm/yyyy" & vbTab & "mm/dd/yyyy" & vbTab & "yyyy/mm/dd"
                grdAsignados.TypeComboBoxEditable = False
                grdAsignados.TypeComboBoxCurSel = 0
        ElseIf grdAsignados.Text = "Numerico" Then
            grdAsignados.Col = 3
            grdAsignados.Text = 10
            grdAsignados.Col = 4
            grdAsignados.CellType = SS_CELL_TYPE_EDIT
            grdAsignados.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignados.Text = 0
            grdAsignados.Col = 5
            grdAsignados.TypeButtonText = "Derecha"
            grdAsignados.Col = 6
            grdAsignados.TypeButtonText = "Ceros"
            grdAsignados.Col = 8
            grdAsignados.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignados.Text = ""
        ElseIf grdAsignados.Text = "Caracter" Then
            grdAsignados.Col = 3
            grdAsignados.Text = 10
            grdAsignados.Col = 4
            grdAsignados.CellType = SS_CELL_TYPE_EDIT
            grdAsignados.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignados.Text = 0
            grdAsignados.Col = 5
            grdAsignados.TypeButtonText = "Izquierda"
            grdAsignados.Col = 6
            grdAsignados.TypeButtonText = "Espacios"
            grdAsignados.Col = 8
            grdAsignados.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignados.Text = ""
        Else
            grdAsignados.Col = 3
            grdAsignados.Text = 10
            grdAsignados.Col = 4
            grdAsignados.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignados.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignados.Text = 0
            grdAsignados.Col = 5
            grdAsignados.TypeButtonText = "Derecha"
            grdAsignados.Col = 6
            grdAsignados.TypeButtonText = "Ceros"
            grdAsignados.Col = 8
            grdAsignados.CellType = SS_CELL_TYPE_COMBOBOX
            grdAsignados.TypeComboBoxList = "9999999999" & vbTab & "999999999D" & vbTab & "99999999-D"
            grdAsignados.TypeComboBoxEditable = False
            grdAsignados.TypeComboBoxCurSel = 0
        End If
        grdAsignados.Col = 10
        grdAsignados.Text = Codigo_Del_Tipo(strTipo)
    ElseIf Col = 8 And grdAsignados.CellType = SS_CELL_TYPE_COMBOBOX Then
            Dim Largo As Long
            Largo = Len(grdAsignados.Text)
            grdAsignados.Col = 3
            grdAsignados.Text = Largo
    ElseIf Col = 9 Then
        grdAsignados.Col = 14
        If Val(grdAsignados.Text) <> 1 Then
            grdAsignados.Col = 9
            grdAsignados.Text = 0
        End If
            
    End If
'    cmdGrabar.Enabled = grdAsignados.MaxRows > 4
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
End Sub

Private Sub grdAsignadosEncab_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
    
    
    grdAsignadosEncab.Col = Col
    grdAsignadosEncab.Row = Row
  If Col = 5 Then
        If grdAsignadosEncab.TypeButtonText = "Derecha" Then
            grdAsignadosEncab.TypeButtonText = "Izquierda"
        Else
            grdAsignadosEncab.TypeButtonText = "Derecha"
        End If
    
    ElseIf Col = 6 Then
        If grdAsignadosEncab.TypeButtonText = "Espacios" Then
            grdAsignadosEncab.TypeButtonText = "Ceros"
        Else
            grdAsignadosEncab.TypeButtonText = "Espacios"
        End If
    
    ElseIf Col = 9 And grdAsignadosEncab.CellType = SS_CELL_TYPE_BUTTON And Not cboEntidad.Text = "" Then
    
        grdAsignadosEncab.Col = 12
        If grdAsignadosEncab.Text = "N" Then
            MsgBox "Este campo no admite conversi�n", vbExclamation, "Conversi�n"
            Exit Sub
        End If

        grdAsignadosEncab.Col = 9
        frmConvertir.gsConvertir = grdAsignadosEncab.TypeButtonText
        frmConvertir.gsIdeEnti = cboEntidad.ItemData(cboEntidad.ListIndex)
        frmConvertir.gsRut = cboEntidad.Text
        frmConvertir.gsNombre = cboEntidad.Text
        frmConvertir.gsIdeProducto = cboProducto.ItemData(cboProducto.ListIndex)
        frmConvertir.gsNomProducto = cboProducto.List(cboProducto.ListIndex)
        frmConvertir.gsIdeFormato = cboFormato.ItemData(cboFormato.ListIndex)
        frmConvertir.gsNomFormato = cboFormato.List(cboFormato.ListIndex)
        frmConvertir.gsIdeSucursal = 1
        frmConvertir.gsNomSucursal = 1
        frmConvertir.gsTipoInforme = cboArchivo.ItemData(cboArchivo.ListIndex)
        frmConvertir.gsIdeSeccion = strSeccEncab
        frmConvertir.gsNomSeccion = "ENCABEZADO"
        
        grdAsignadosEncab.Col = 1: frmConvertir.gsNomCampo = grdAsignadosEncab.Text
        grdAsignadosEncab.Col = 7: frmConvertir.gsIdeCampo = grdAsignadosEncab.Value
        frmConvertir.Show vbModal
        grdAsignadosEncab.Col = 9: grdAsignadosEncab.TypeButtonText = frmConvertir.gsConvertir
        Unload frmConvertir
    
    End If
    grdAsignadosEncab.Col = 1
    grdAsignadosEncab.Action = 0
End Sub



Private Sub grdAsignadosEncab_Change(ByVal Col As Long, ByVal Row As Long)
    Dim strTipo As String

' Si tipo es fecha, actualizar precisi�n.-
    grdAsignadosEncab.Col = Col
    grdAsignadosEncab.Row = Row
    If Col = 2 Then
        strTipo = grdAsignadosEncab.Text
        grdAsignadosEncab.Row = Row
        grdAsignadosEncab.Col = 2
        If grdAsignadosEncab.Text = "Fecha" Then
            grdAsignadosEncab.Col = 9
            If grdAsignadosEncab.TypeButtonText = "SI" Then
                grdAsignadosEncab.Col = 2
                grdAsignadosEncab.Text = grdAsignadosEncab.TypeComboBoxString
                MsgBox "Antes de modificar a tipo Fecha, debe eliminar la conversi�n de Datos", vbCritical, "Configuracion Archivo Plano"
                Exit Sub
            End If
            grdAsignadosEncab.Col = 3
            grdAsignadosEncab.Text = 6
            grdAsignadosEncab.Col = 4
            grdAsignadosEncab.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignadosEncab.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignadosEncab.Text = 0
            grdAsignadosEncab.Col = 5
            grdAsignadosEncab.TypeButtonText = "Izquierda"
            grdAsignadosEncab.Col = 6
            grdAsignadosEncab.TypeButtonText = "Espacios"
            grdAsignadosEncab.Col = 8
            grdAsignadosEncab.CellType = SS_CELL_TYPE_COMBOBOX
            grdAsignadosEncab.TypeComboBoxList = _
                "ddmmyy" & vbTab & "mmddyy" & vbTab & "yymmdd" & vbTab & _
                "ddmmyyyy" & vbTab & "mmddyyyy" & vbTab & "yyyymmdd" & vbTab & _
                "dd-mm-yy" & vbTab & "mm-dd-yy" & vbTab & "yy-mm-dd" & vbTab & _
                "dd-mm-yyyy" & vbTab & "mm-dd-yyyy" & vbTab & "yyyy-mm-dd" & vbTab & _
                "dd/mm/yy" & vbTab & "mm/dd/yy" & vbTab & "yy/mm/dd" & vbTab & _
                "dd/mm/yyyy" & vbTab & "mm/dd/yyyy" & vbTab & "yyyy/mm/dd"
                grdAsignadosEncab.TypeComboBoxEditable = False
                grdAsignadosEncab.TypeComboBoxCurSel = 0
        ElseIf grdAsignadosEncab.Text = "Numerico" Then
            grdAsignadosEncab.Col = 3
            grdAsignadosEncab.Text = 10
            grdAsignadosEncab.Col = 4
            grdAsignadosEncab.CellType = SS_CELL_TYPE_EDIT
            grdAsignadosEncab.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignadosEncab.Text = 0
            grdAsignadosEncab.Col = 5
            grdAsignadosEncab.TypeButtonText = "Derecha"
            grdAsignadosEncab.Col = 6
            grdAsignadosEncab.TypeButtonText = "Ceros"
            grdAsignadosEncab.Col = 8
            grdAsignadosEncab.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignadosEncab.Text = ""
        ElseIf grdAsignadosEncab.Text = "Caracter" Then
            grdAsignadosEncab.Col = 3
            grdAsignadosEncab.Text = 10
            grdAsignadosEncab.Col = 4
            grdAsignadosEncab.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignadosEncab.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignadosEncab.Text = 0
            grdAsignadosEncab.Col = 5
            grdAsignadosEncab.TypeButtonText = "Izquierda"
            grdAsignadosEncab.Col = 6
            grdAsignadosEncab.TypeButtonText = "Espacios"
            grdAsignadosEncab.Col = 8
            grdAsignadosEncab.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignadosEncab.Text = ""
        Else
            grdAsignadosEncab.Col = 3
            grdAsignadosEncab.Text = 10
            grdAsignadosEncab.Col = 4
            grdAsignadosEncab.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignadosEncab.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignadosEncab.Text = 0
            grdAsignadosEncab.Col = 5
            grdAsignadosEncab.TypeButtonText = "Derecha"
            grdAsignadosEncab.Col = 6
            grdAsignadosEncab.TypeButtonText = "Ceros"
            grdAsignadosEncab.Col = 8
            grdAsignadosEncab.CellType = SS_CELL_TYPE_COMBOBOX
            grdAsignadosEncab.TypeComboBoxList = "9999999999" & vbTab & "999999999D" & vbTab & "99999999-D"
            grdAsignadosEncab.TypeComboBoxEditable = False
            grdAsignadosEncab.TypeComboBoxCurSel = 0
        End If
        grdAsignadosEncab.Col = 10
        grdAsignadosEncab.Text = Codigo_Del_Tipo(strTipo)

    ElseIf Col = 8 And grdAsignadosEncab.CellType = SS_CELL_TYPE_COMBOBOX Then
            Dim Largo As Long
            Largo = Len(grdAsignadosEncab.Text)
            grdAsignadosEncab.Col = 3
            grdAsignadosEncab.Text = Largo
    
    
    End If
'    cmdGrabar.Enabled = grdAsignadosEncab.MaxRows > 4
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
End Sub

Private Sub grdAsignadosTot_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
    grdAsignadosTot.Col = Col
    grdAsignadosTot.Row = Row
    
    If Col = 5 Then
        If grdAsignadosTot.TypeButtonText = "Derecha" Then
            grdAsignadosTot.TypeButtonText = "Izquierda"
        Else
            grdAsignadosTot.TypeButtonText = "Derecha"
        End If
    
    ElseIf Col = 6 Then
        If grdAsignadosTot.TypeButtonText = "Espacios" Then
            grdAsignadosTot.TypeButtonText = "Ceros"
        Else
            grdAsignadosTot.TypeButtonText = "Espacios"
        End If
        
    ElseIf Col = 9 And grdAsignadosTot.CellType = SS_CELL_TYPE_BUTTON And Not cboEntidad.Text = "" Then
    
        grdAsignadosTot.Col = 12
        If grdAsignadosTot.Text = "N" Then
            MsgBox "Este campo no admite conversi�n", vbExclamation, "Conversi�n"
            Exit Sub
        End If
    
        grdAsignadosTot.Col = 9
        frmConvertir.gsConvertir = grdAsignadosTot.TypeButtonText
        frmConvertir.gsIdeEnti = cboEntidad.ItemData(cboEntidad.ListIndex)
        frmConvertir.gsRut = cboEntidad.Text
        frmConvertir.gsNombre = cboEntidad.Text
        frmConvertir.gsIdeProducto = cboProducto.ItemData(cboProducto.ListIndex)
        frmConvertir.gsNomProducto = cboProducto.List(cboProducto.ListIndex)
        frmConvertir.gsIdeFormato = cboFormato.ItemData(cboFormato.ListIndex)
        frmConvertir.gsNomFormato = cboFormato.List(cboFormato.ListIndex)
        frmConvertir.gsIdeSucursal = cboEntidad.ItemData(cboEntidad.ListIndex)
        frmConvertir.gsNomSucursal = cboEntidad.List(cboEntidad.ListIndex)
        frmConvertir.gsTipoInforme = cboArchivo.ItemData(cboArchivo.ListIndex)
        
        frmConvertir.gsIdeSeccion = strSeccTotal
        frmConvertir.gsNomSeccion = "TOTAL"
      
        grdAsignadosTot.Col = 1: frmConvertir.gsNomCampo = grdAsignadosTot.Text
        grdAsignadosTot.Col = 7: frmConvertir.gsIdeCampo = grdAsignadosTot.Value
        frmConvertir.Show vbModal
        grdAsignados.Col = 9: grdAsignadosTot.TypeButtonText = frmConvertir.gsConvertir
        Unload frmConvertir
    End If
        
    grdAsignadosTot.Col = 1
    grdAsignadosTot.Action = 0

End Sub

Private Sub grdAsignadosTot_Change(ByVal Col As Long, ByVal Row As Long)
 Dim strTipo As String
 
 ' Si tipo es fecha, actualizar precisi�n.-
    grdAsignadosTot.Col = Col
    grdAsignadosTot.Row = Row
    If Col = 2 Then
        grdAsignadosTot.Row = Row
        grdAsignadosTot.Col = 2
        strTipo = grdAsignadosTot.Text
        
        If grdAsignadosTot.Text = "Fecha" Then
            grdAsignadosTot.Col = 9
            If grdAsignadosTot.TypeButtonText = "SI" Then
                grdAsignadosTot.Col = 2
                grdAsignadosTot.Text = grdAsignadosTot.TypeComboBoxString
                MsgBox "Antes de modificar a tipo Fecha, debe eliminar la conversi�n de Datos", vbCritical, "Configuracion Archivo Plano"
                Exit Sub
            End If
            grdAsignadosTot.Col = 3
            grdAsignadosTot.Text = 6
            grdAsignadosTot.Col = 4
            grdAsignadosTot.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignadosTot.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignadosTot.Text = 0
            grdAsignadosTot.Col = 5
            grdAsignadosTot.TypeButtonText = "Izquierda"
            grdAsignadosTot.Col = 6
            grdAsignadosTot.TypeButtonText = "Espacios"
            grdAsignadosTot.Col = 8
            grdAsignadosTot.CellType = SS_CELL_TYPE_COMBOBOX
            grdAsignadosTot.TypeComboBoxList = _
                "ddmmyy" & vbTab & "mmddyy" & vbTab & "yymmdd" & vbTab & _
                "ddmmyyyy" & vbTab & "mmddyyyy" & vbTab & "yyyymmdd" & vbTab & _
                "dd-mm-yy" & vbTab & "mm-dd-yy" & vbTab & "yy-mm-dd" & vbTab & _
                "dd-mm-yyyy" & vbTab & "mm-dd-yyyy" & vbTab & "yyyy-mm-dd" & vbTab & _
                "dd/mm/yy" & vbTab & "mm/dd/yy" & vbTab & "yy/mm/dd" & vbTab & _
                "dd/mm/yyyy" & vbTab & "mm/dd/yyyy" & vbTab & "yyyy/mm/dd"
                grdAsignadosTot.TypeComboBoxEditable = False
                grdAsignadosTot.TypeComboBoxCurSel = 0
        ElseIf grdAsignadosTot.Text = "Numerico" Then
            grdAsignadosTot.Col = 3
            grdAsignadosTot.Text = 10
            grdAsignadosTot.Col = 4
            grdAsignadosTot.CellType = SS_CELL_TYPE_EDIT
            grdAsignadosTot.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignadosTot.Text = 0
            grdAsignadosTot.Col = 5
            grdAsignadosTot.TypeButtonText = "Derecha"
            grdAsignadosTot.Col = 6
            grdAsignadosTot.TypeButtonText = "Ceros"
            grdAsignadosTot.Col = 8
            grdAsignadosTot.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignadosTot.Text = ""
        ElseIf grdAsignadosEncab.Text = "Caracter" Then
            grdAsignadosTot.Col = 3
            grdAsignadosTot.Text = 10
            grdAsignadosTot.Col = 4
            grdAsignadosTot.CellType = SS_CELL_TYPE_EDIT
            grdAsignadosTot.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignadosTot.Text = 0
            grdAsignadosTot.Col = 5
            grdAsignadosTot.TypeButtonText = "Izquierda"
            grdAsignadosTot.Col = 6
            grdAsignadosTot.TypeButtonText = "Espacios"
            grdAsignadosTot.Col = 8
            grdAsignadosTot.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignadosTot.Text = ""
         Else
            grdAsignadosTot.Col = 3
            grdAsignadosTot.Text = 10
            grdAsignadosTot.Col = 4
            grdAsignadosTot.CellType = SS_CELL_TYPE_STATIC_TEXT
            grdAsignadosTot.TypeHAlign = 1 'Alinear a la Derecha
            grdAsignadosTot.Text = 0
            grdAsignadosTot.Col = 5
            grdAsignadosTot.TypeButtonText = "Derecha"
            grdAsignadosTot.Col = 6
            grdAsignadosTot.TypeButtonText = "Ceros"
            grdAsignadosTot.Col = 8
            grdAsignadosTot.CellType = SS_CELL_TYPE_COMBOBOX
            grdAsignadosTot.TypeComboBoxList = "9999999999" & vbTab & "999999999D" & vbTab & "99999999-D"
            grdAsignadosTot.TypeComboBoxEditable = False
            grdAsignadosTot.TypeComboBoxCurSel = 0
        End If
        grdAsignadosTot.Col = 10
        grdAsignadosTot.Text = Codigo_Del_Tipo(strTipo)
        
    ElseIf Col = 8 And grdAsignadosTot.CellType = SS_CELL_TYPE_COMBOBOX Then
            Dim Largo As Long
            Largo = Len(grdAsignadosTot.Text)
            grdAsignadosTot.Col = 3
            grdAsignadosTot.Text = Largo
    ElseIf Col = 9 Then
        grdAsignadosTot.Col = 12
        If Val(grdAsignadosTot.Text) <> 1 Then
            grdAsignadosTot.Col = 9
            grdAsignadosTot.Text = 0
        End If
            
    End If
'    cmdGrabar.Enabled = grdAsignadosTot.MaxRows > 4
'    tlbAcciones.Buttons("Grabar").Enabled = cmdGrabar.Enabled
End Sub

Private Sub grdOrdena_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
    grdOrdena.Col = Col
    grdOrdena.Row = Row
  If Col = 5 Then
        If grdOrdena.TypeButtonText = "Asc" Then
            grdOrdena.TypeButtonText = "Desc"
        Else
            grdOrdena.TypeButtonText = "Asc"
        End If
  End If

End Sub


Private Sub tabSecciones_Click(PreviousTab As Integer)
    Select Case tabSecciones.Tab
        Case 0
            If grdDispEncab.MaxRows > 0 Then grdDispEncab.Row = 1
            cmdPreview.Enabled = grdAsignadosEncab.MaxRows > 0
            tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
        Case 1
            If grdDisponibles.MaxRows > 0 Then grdDisponibles.Row = 1
            cmdPreview.Enabled = grdAsignados.MaxRows > 0
            tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
        Case 2
            If grdDispTot.MaxRows > 0 Then grdDispTot.Row = 1
            cmdPreview.Enabled = grdAsignadosTot.MaxRows > 0
            tlbAcciones.Buttons("Prevista").Enabled = cmdPreview.Enabled
            
        Case 3
            tabSecciones.Tab = PreviousTab
    End Select
End Sub

Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "Grabar"
            CmdGrabar_Click
        Case "Eliminar"
            cmdEliminaConfig_Click
        Case "Prevista"
            cmdPreview_Click
        Case "Salir"
            cmdSalir_Click
    End Select

End Sub


