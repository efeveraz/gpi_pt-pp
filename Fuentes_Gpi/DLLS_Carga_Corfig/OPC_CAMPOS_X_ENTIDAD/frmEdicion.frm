VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEdicion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Conversi�n"
   ClientHeight    =   4005
   ClientLeft      =   12705
   ClientTop       =   8370
   ClientWidth     =   6150
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4005
   ScaleWidth      =   6150
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   90
      TabIndex        =   11
      Top             =   2925
      Width           =   5955
      Begin VB.CommandButton CmdGrabar 
         Caption         =   "&Aceptar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   120
         TabIndex        =   13
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   4380
         TabIndex        =   12
         Top             =   210
         Width           =   1425
      End
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2265
      Left            =   90
      TabIndex        =   0
      Top             =   540
      Width           =   5955
      Begin VB.TextBox txtDesDatoEnt 
         Height          =   315
         Left            =   2790
         TabIndex        =   5
         Top             =   1620
         Width           =   2535
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "C�digo de Dato a Convertir"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Left            =   0
         TabIndex        =   15
         Top             =   0
         Width           =   5970
      End
      Begin VB.Label lblFila 
         Height          =   240
         Left            =   5085
         TabIndex        =   14
         Top             =   1980
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label lblDesDatoSal 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   2790
         TabIndex        =   8
         Top             =   1215
         Width           =   2535
      End
      Begin VB.Label lblCodDatoSal 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   2790
         TabIndex        =   7
         Top             =   810
         Width           =   2535
      End
      Begin VB.Label lblCampo 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   2790
         TabIndex        =   6
         Top             =   405
         Width           =   2535
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Campo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   615
         TabIndex        =   4
         Top             =   465
         Width           =   1665
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo Dato Salida"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   615
         TabIndex        =   3
         Top             =   870
         Width           =   1560
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n Dato Salida"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   2
         Left            =   615
         TabIndex        =   2
         Top             =   1275
         Width           =   1950
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n Dato Entrada"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   5
         Left            =   615
         TabIndex        =   1
         Top             =   1680
         Width           =   2085
      End
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   6150
      _ExtentX        =   10848
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Aceptar"
            Object.ToolTipText     =   "Aceptar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Volver"
            Object.ToolTipText     =   "Volver"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   10
      Top             =   3735
      Width           =   6150
      _ExtentX        =   10848
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7744
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmEdicion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CmdGrabar_Click()
    
    If txtDesDatoEnt.Text = "" Then
        MsgBox "Debe ingresar un valor en la Descripci�n Dato entrada", vbInformation, "Aceptar"
    Else
        frmConvertir.grdDatos.Row = lblFila
        frmConvertir.grdDatos.Col = 2: frmConvertir.grdDatos.Text = txtDesDatoEnt
        frmConvertir.grdDatos.Col = 3: frmConvertir.grdDatos.Text = lblCodDatoSal
        frmConvertir.grdDatos.Col = 4: frmConvertir.grdDatos.Text = lblDesDatoSal
        Unload Me
    End If

End Sub

Private Sub cmdVolver_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Carga_Imagenes
    staEstado.Panels(2) = "V " & App.Major & "." & App.Minor
End Sub

Private Sub Carga_Imagenes()
        
    tlbAcciones.ImageList = frmConfigura.ilsIconos
    tlbAcciones.Buttons("Aceptar").Image = 1
    tlbAcciones.Buttons("Volver").Image = 7
    
End Sub

Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "Aceptar"
            CmdGrabar_Click
        Case "Volver"
            cmdVolver_Click
    End Select
End Sub


Private Sub txtDesDatoEnt_KeyPress(KeyAscii As Integer)
    If KeyAscii >= 97 And KeyAscii <= 122 Or KeyAscii = 241 Then
        KeyAscii = KeyAscii - 32
    End If
End Sub

