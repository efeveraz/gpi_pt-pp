Attribute VB_Name = "proc_OPC_CAMPOS_TIPO_ARCH"
Option Explicit

Public gblnswichRefresca As Boolean
Public gblnModifica As Boolean
Public glngCodigo As Long
Public gstrseccion As String

Public glngIdeProducto As Long
Public glngIdeTipoArch As Long
Public garrSecciones() As Variant
Public garrCampoSalida() As Variant



Public Sub Carga_Secciones()
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
Dim lngRgtros  As Long
Dim lngInd  As Long
On Error GoTo LabelError

    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.PKG_FME_PARAMETROS.SP_FME_PRMTROS_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigotipo", adNumeric, adParamInput, 10, 1)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigosubtipo", adNumeric, adParamInput, 10, 7)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigoparametro", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_coderror", adNumeric, adParamOutput, 10)
        
    Set recRegistros = Cmd.Execute
    
    If Not recRegistros.EOF Then
        lngRgtros = recRegistros.RecordCount - 1
        ReDim garrSecciones(0 To lngRgtros, 0 To 2)
    
        lngInd = 0
        Do While Not recRegistros.EOF
            
            garrSecciones(lngInd, 0) = recRegistros.Fields("IDE_UNICO_PARAM").Value
            garrSecciones(lngInd, 1) = recRegistros.Fields("GLS_DESCRIPCION").Value
            garrSecciones(lngInd, 2) = recRegistros.Fields("NOM_CORTO").Value
            
            recRegistros.MoveNext
            lngInd = lngInd + 1
        Loop
    Else
        ReDim garrSecciones(0 To 0, 0 To 2)
        garrSecciones(0, 0) = 0
        garrSecciones(0, 1) = ""
    End If
    
LabelSalir:
    Screen.MousePointer = 0
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Sub Carga_CamposEntradas()
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
Dim lngRgtros  As Long
Dim lngInd  As Long
On Error GoTo LabelError

    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.PKG_FME_CAMPOS_GENERICOS.SP_FME_CAMPOS_VER"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 40, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
        
    Set recRegistros = Cmd.Execute
    
    If Not recRegistros.EOF Then
        lngRgtros = recRegistros.RecordCount - 1
        ReDim garrCampoSalida(0 To lngRgtros, 0 To 2)
    
        lngInd = 0
        Do While Not recRegistros.EOF
            
            garrCampoSalida(lngInd, 0) = recRegistros.Fields("NRO_CORR_COLUMNA").Value
            garrCampoSalida(lngInd, 1) = recRegistros.Fields("GLS_COLUMNA").Value
            garrCampoSalida(lngInd, 2) = recRegistros.Fields("FLG_CONVERTIR").Value
            
            recRegistros.MoveNext
            lngInd = lngInd + 1
        Loop
    Else
        ReDim garrCampoSalida(0 To 0, 0 To 2)
        garrCampoSalida(0, 0) = 0
        garrCampoSalida(0, 1) = ""
    End If
LabelSalir:
    Screen.MousePointer = 0
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

