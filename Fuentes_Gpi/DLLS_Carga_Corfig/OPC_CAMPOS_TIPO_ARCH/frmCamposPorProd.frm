VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "ss32x25.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCamposPorProd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenci�n Campos de Tipo Archivo"
   ClientHeight    =   7185
   ClientLeft      =   1320
   ClientTop       =   2115
   ClientWidth     =   9570
   Icon            =   "frmCamposPorProd.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7185
   ScaleWidth      =   9570
   Begin VB.Frame fraLista 
      Height          =   4785
      Index           =   4
      Left            =   60
      TabIndex        =   5
      Top             =   1440
      Width           =   9460
      Begin TabDlg.SSTab tabSeciones 
         Height          =   4275
         Left            =   60
         TabIndex        =   17
         Top             =   420
         Width           =   9345
         _ExtentX        =   16484
         _ExtentY        =   7541
         _Version        =   393216
         TabsPerRow      =   7
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Encabezado"
         TabPicture(0)   =   "frmCamposPorProd.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "grdEncabezado"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Detalle"
         TabPicture(1)   =   "frmCamposPorProd.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDetalle"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Total"
         TabPicture(2)   =   "frmCamposPorProd.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "grdTotal"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).ControlCount=   1
         Begin FPSpread.vaSpread grdEncabezado 
            Height          =   3735
            Left            =   60
            TabIndex        =   18
            Top             =   420
            Width           =   9195
            _Version        =   131077
            _ExtentX        =   16219
            _ExtentY        =   6588
            _StockProps     =   64
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   17
            MaxRows         =   0
            OperationMode   =   2
            ScrollBarExtMode=   -1  'True
            SelectBlockOptions=   0
            SpreadDesigner  =   "frmCamposPorProd.frx":0060
         End
         Begin FPSpread.vaSpread grdDetalle 
            Height          =   3735
            Left            =   -74940
            TabIndex        =   19
            Top             =   420
            Width           =   9195
            _Version        =   131077
            _ExtentX        =   16219
            _ExtentY        =   6588
            _StockProps     =   64
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   17
            MaxRows         =   0
            OperationMode   =   2
            ScrollBarExtMode=   -1  'True
            SelectBlockOptions=   0
            SpreadDesigner  =   "frmCamposPorProd.frx":072B
         End
         Begin FPSpread.vaSpread grdTotal 
            Height          =   3735
            Left            =   -74940
            TabIndex        =   20
            Top             =   420
            Width           =   9195
            _Version        =   131077
            _ExtentX        =   16219
            _ExtentY        =   6588
            _StockProps     =   64
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   17
            MaxRows         =   0
            OperationMode   =   2
            ScrollBarExtMode=   -1  'True
            SelectBlockOptions=   0
            SpreadDesigner  =   "frmCamposPorProd.frx":0D09
         End
      End
      Begin VB.Label lblLista 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Lista de Campos"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   4
         Left            =   0
         TabIndex        =   6
         Top             =   90
         Width           =   9465
      End
   End
   Begin VB.Frame fraSeleccion 
      Height          =   975
      Index           =   4
      Left            =   60
      TabIndex        =   2
      Top             =   480
      Width           =   9460
      Begin VB.ComboBox cboTipoArchivo 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "frmCamposPorProd.frx":13D4
         Left            =   6240
         List            =   "frmCamposPorProd.frx":13D6
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   420
         Width           =   1815
      End
      Begin VB.ComboBox cboProducto 
         Height          =   315
         ItemData        =   "frmCamposPorProd.frx":13D8
         Left            =   1140
         List            =   "frmCamposPorProd.frx":13DA
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   420
         Width           =   4035
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   8370
         TabIndex        =   3
         Top             =   420
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Direcci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5460
         TabIndex        =   16
         Top             =   480
         Width           =   675
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Archivo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   14
         Top             =   480
         Width           =   915
      End
      Begin VB.Label lblSeleccion 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Criterio de B�squeda"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   4
         Left            =   0
         TabIndex        =   4
         Top             =   90
         Width           =   9465
      End
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   0
      Top             =   6915
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14253
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Agregar"
            Object.ToolTipText     =   "Agregar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Modificar"
            Object.ToolTipText     =   "Modificar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Eliminar"
            Object.ToolTipText     =   "Eliminar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Consultar"
            Object.ToolTipText     =   "Consultar"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Salir"
            Object.ToolTipText     =   "Salir"
         EndProperty
      EndProperty
      Begin MSComctlLib.ImageList ilsIconos 
         Left            =   2220
         Top             =   60
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   8
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCamposPorProd.frx":13DC
               Key             =   "Agregar"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCamposPorProd.frx":1A56
               Key             =   "Editar"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCamposPorProd.frx":1B68
               Key             =   "Borrar"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCamposPorProd.frx":1C7A
               Key             =   "Salir"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCamposPorProd.frx":23EC
               Key             =   "Grabar"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCamposPorProd.frx":24FE
               Key             =   "Cancelar"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCamposPorProd.frx":2610
               Key             =   "Volver"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCamposPorProd.frx":2B42
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame fraBotones 
      Height          =   735
      Index           =   4
      Left            =   60
      TabIndex        =   7
      Top             =   6180
      Width           =   9460
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   2955
         TabIndex        =   12
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdSalir 
         Caption         =   "&Salir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   7920
         TabIndex        =   11
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   75
         TabIndex        =   10
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   1515
         TabIndex        =   9
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   4400
         TabIndex        =   8
         Top             =   210
         Width           =   1425
      End
   End
End
Attribute VB_Name = "frmCamposPorProd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cboProducto_Click()
    tabSeciones.Tab = 0
    grdEncabezado.MaxRows = 0
    grdDetalle.MaxRows = 0
    grdTotal.MaxRows = 0
    glngIdeProducto = cboProducto.ItemData(cboProducto.ListIndex)
End Sub

Private Sub cboTipoArchivo_Click()
    glngIdeTipoArch = cboTipoArchivo.ItemData(cboTipoArchivo.ListIndex)
End Sub

Private Sub cmdAgregar_Click()
Dim strIdeUnicoSeccion As String
Dim lngInd As Long
On Error GoTo LabelError

    
    gblnswichRefresca = False
    gblnModifica = False
    frmMantCamposPorProd.fraIngreso.Enabled = True
    frmMantCamposPorProd.cmdGrabar.Enabled = True
    If garrCampoSalida(0, 0) <> 0 And garrCampoSalida(0, 1) <> "" Then
        Call Carga_Combo_Corp(frmMantCamposPorProd.cboCampoSalida, garrCampoSalida)
    End If
    
    Call LimpiarControles(frmMantCamposPorProd, "INGCAMP")
    frmMantCamposPorProd.lblProducto = cboProducto.Text
    frmMantCamposPorProd.lblTipoArchivo = cboTipoArchivo.Text
    Call AlmacenarValoresCtls(frmMantCamposPorProd, "INGCAMP")
    
    Select Case tabSeciones.Tab
        Case 0 'Encabezado
            Rem Rescatando Codigo Unico Parametro Seccion
            For lngInd = 0 To UBound(garrSecciones)
                If Trim(UCase(garrSecciones(lngInd, 2))) = "ENC" Then
                    strIdeUnicoSeccion = garrSecciones(lngInd, 0)
                End If
            Next lngInd
            If garrSecciones(0, 0) <> 0 And garrSecciones(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboSeccion, garrSecciones, strIdeUnicoSeccion)
            End If
        Case 1 'Detalle
            Rem Rescatando Codigo Unico Parametro Seccion
            For lngInd = 0 To UBound(garrSecciones)
                If Trim(UCase(garrSecciones(lngInd, 2))) = "DET" Then
                    strIdeUnicoSeccion = garrSecciones(lngInd, 0)
                End If
            Next lngInd
            If garrSecciones(0, 0) <> 0 And garrSecciones(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboSeccion, garrSecciones, strIdeUnicoSeccion)
            End If
        Case 2 'Total
            Rem Rescatando Codigo Unico Parametro Seccion
            For lngInd = 0 To UBound(garrSecciones)
                If Trim(UCase(garrSecciones(lngInd, 2))) = "TOT" Then
                    strIdeUnicoSeccion = garrSecciones(lngInd, 0)
                End If
            Next lngInd
            If garrSecciones(0, 0) <> 0 And garrSecciones(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboSeccion, garrSecciones, strIdeUnicoSeccion)
            End If
    End Select
    
    Call AlmacenarValoresCtls(frmMantCamposPorProd, "INGCAMP")
    frmMantCamposPorProd.cboSeccion.Enabled = False
    frmMantCamposPorProd.Show 1
    
    
LabelSalir:
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Sub cmdBuscar_Click()
Dim strTipoArch As String
Dim strAux As String
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError

    staEstado.Panels(1) = "Buscando..."
    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.PKG_FME_CAMPOS_X_PRODUCTO.SP_FME_CAMPOS_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeProducto", adNumeric, adParamInput, 10, cboProducto.ItemData(cboProducto.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodTipoArch", adChar, adParamInput, 8, cboTipoArchivo.ItemData(cboTipoArchivo.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodSeccion", adChar, adParamInput, 8, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 40, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser cargada. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbInformation, "Campos de archivo por Producto"
        GoTo LabelSalir
    End If
    
    grdEncabezado.MaxRows = 0
    grdDetalle.MaxRows = 0
    grdTotal.MaxRows = 0
    While Not recRegistros.EOF
        strTipoArch = IIf(IsNull(recRegistros.Fields("NOM_CORTO").Value), "", Trim(recRegistros.Fields("NOM_CORTO").Value))
        
        Select Case UCase(strTipoArch)
            Case "ENC"
                grdEncabezado.MaxRows = grdEncabezado.MaxRows + 1
                grdEncabezado.Row = grdEncabezado.MaxRows
                
                grdEncabezado.Col = 1: grdEncabezado.Text = "" & recRegistros.Fields("IDE_EMPRESA").Value
                grdEncabezado.Col = 2: grdEncabezado.Text = "" & recRegistros.Fields("IDE_PRODUCTO").Value
                grdEncabezado.Col = 3: grdEncabezado.Text = "" & recRegistros.Fields("COD_TIPO_ARCHIVO").Value
                grdEncabezado.Col = 4: grdEncabezado.Text = "" & recRegistros.Fields("COD_IDE_SECCION").Value
                grdEncabezado.Col = 5: grdEncabezado.Text = "" & recRegistros.Fields("NRO_CORR_CAMPO").Value
                grdEncabezado.Col = 6: grdEncabezado.Text = "" & recRegistros.Fields("GLS_CAMPO").Value
                If IsNull(recRegistros.Fields("FLG_OBLIGATORIO").Value) Then
                    grdEncabezado.Col = 7: grdEncabezado.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_OBLIGATORIO").Value) = 1 Then
                    grdEncabezado.Col = 7: grdEncabezado.Text = "SI"
                Else
                    grdEncabezado.Col = 7: grdEncabezado.Text = "NO"
                End If
                grdEncabezado.Col = 8: grdEncabezado.Text = "" & recRegistros.Fields("NRO_CORR_COLUMNA").Value
                grdEncabezado.Col = 9: grdEncabezado.Text = "" & recRegistros.Fields("GLS_COLUMNA").Value
                If IsNull(recRegistros.Fields("FLG_IND_ORDENA").Value) Then
                    grdEncabezado.Col = 10: grdEncabezado.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_IND_ORDENA").Value) = 1 Then
                    grdEncabezado.Col = 10: grdEncabezado.Text = "SI"
                Else
                    grdEncabezado.Col = 10: grdEncabezado.Text = "NO"
                End If
                If IsNull(recRegistros.Fields("FLG_IND_TOTALIZA").Value) Then
                    grdEncabezado.Col = 11: grdEncabezado.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_IND_TOTALIZA").Value) = 1 Then
                    grdEncabezado.Col = 11: grdEncabezado.Text = "SI"
                Else
                    grdEncabezado.Col = 11: grdEncabezado.Text = "NO"
                End If
                If IsNull(recRegistros.Fields("FLG_CONVERTIR").Value) Then
                    grdEncabezado.Col = 12: grdEncabezado.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_CONVERTIR").Value) = 1 Then
                    grdEncabezado.Col = 12: grdEncabezado.Text = "SI"
                Else
                    grdEncabezado.Col = 12: grdEncabezado.Text = "NO"
                End If
                grdEncabezado.Col = 13: grdEncabezado.Text = "" & recRegistros.Fields("FCH_HRA_CREACION").Value
                grdEncabezado.Col = 14: grdEncabezado.Text = "" & recRegistros.Fields("COD_USR_CREACION").Value
                grdEncabezado.Col = 15: grdEncabezado.Text = "" & recRegistros.Fields("FCH_HRA_MODIFICA").Value
                grdEncabezado.Col = 16: grdEncabezado.Text = "" & recRegistros.Fields("COD_USR_MODIFICA").Value
            Case "DET"
                grdDetalle.MaxRows = grdDetalle.MaxRows + 1
                grdDetalle.Row = grdDetalle.MaxRows
                
                grdDetalle.Col = 1: grdDetalle.Text = "" & recRegistros.Fields("IDE_EMPRESA").Value
                grdDetalle.Col = 2: grdDetalle.Text = "" & recRegistros.Fields("IDE_PRODUCTO").Value
                grdDetalle.Col = 3: grdDetalle.Text = "" & recRegistros.Fields("COD_TIPO_ARCHIVO").Value
                grdDetalle.Col = 4: grdDetalle.Text = "" & recRegistros.Fields("COD_IDE_SECCION").Value
                grdDetalle.Col = 5: grdDetalle.Text = "" & recRegistros.Fields("NRO_CORR_CAMPO").Value
                grdDetalle.Col = 6: grdDetalle.Text = "" & recRegistros.Fields("GLS_CAMPO").Value
                If IsNull(recRegistros.Fields("FLG_OBLIGATORIO").Value) Then
                    grdDetalle.Col = 7: grdDetalle.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_OBLIGATORIO").Value) = 1 Then
                    grdDetalle.Col = 7: grdDetalle.Text = "SI"
                Else
                    grdDetalle.Col = 7: grdDetalle.Text = "NO"
                End If
                grdDetalle.Col = 8: grdDetalle.Text = "" & recRegistros.Fields("NRO_CORR_COLUMNA").Value
                grdDetalle.Col = 9: grdDetalle.Text = "" & recRegistros.Fields("GLS_COLUMNA").Value
                If IsNull(recRegistros.Fields("FLG_IND_ORDENA").Value) Then
                    grdDetalle.Col = 10: grdDetalle.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_IND_ORDENA").Value) = 1 Then
                    grdDetalle.Col = 10: grdDetalle.Text = "SI"
                Else
                    grdDetalle.Col = 10: grdDetalle.Text = "NO"
                End If
                If IsNull(recRegistros.Fields("FLG_IND_TOTALIZA").Value) Then
                    grdDetalle.Col = 11: grdDetalle.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_IND_TOTALIZA").Value) = 1 Then
                    grdDetalle.Col = 11: grdDetalle.Text = "SI"
                Else
                    grdDetalle.Col = 11: grdDetalle.Text = "NO"
                End If
                If IsNull(recRegistros.Fields("FLG_CONVERTIR").Value) Then
                    grdDetalle.Col = 12: grdDetalle.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_CONVERTIR").Value) = 1 Then
                    grdDetalle.Col = 12: grdDetalle.Text = "SI"
                Else
                    grdDetalle.Col = 12: grdDetalle.Text = "NO"
                End If
                grdDetalle.Col = 13: grdDetalle.Text = "" & recRegistros.Fields("FCH_HRA_CREACION").Value
                grdDetalle.Col = 14: grdDetalle.Text = "" & recRegistros.Fields("COD_USR_CREACION").Value
                grdDetalle.Col = 15: grdDetalle.Text = "" & recRegistros.Fields("FCH_HRA_MODIFICA").Value
                grdDetalle.Col = 16: grdDetalle.Text = "" & recRegistros.Fields("COD_USR_MODIFICA").Value
            Case "TOT"
                grdTotal.MaxRows = grdTotal.MaxRows + 1
                grdTotal.Row = grdTotal.MaxRows
                
                grdTotal.Col = 1: grdTotal.Text = "" & recRegistros.Fields("IDE_EMPRESA").Value
                grdTotal.Col = 2: grdTotal.Text = "" & recRegistros.Fields("IDE_PRODUCTO").Value
                grdTotal.Col = 3: grdTotal.Text = "" & recRegistros.Fields("COD_TIPO_ARCHIVO").Value
                grdTotal.Col = 4: grdTotal.Text = "" & recRegistros.Fields("COD_IDE_SECCION").Value
                grdTotal.Col = 5: grdTotal.Text = "" & recRegistros.Fields("NRO_CORR_CAMPO").Value
                grdTotal.Col = 6: grdTotal.Text = "" & recRegistros.Fields("GLS_CAMPO").Value
                If IsNull(recRegistros.Fields("FLG_OBLIGATORIO").Value) Then
                    grdTotal.Col = 7: grdTotal.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_OBLIGATORIO").Value) = 1 Then
                    grdTotal.Col = 7: grdTotal.Text = "SI"
                Else
                    grdTotal.Col = 7: grdTotal.Text = "NO"
                End If
                grdTotal.Col = 8: grdTotal.Text = "" & recRegistros.Fields("NRO_CORR_COLUMNA").Value
                grdTotal.Col = 9: grdTotal.Text = "" & recRegistros.Fields("GLS_COLUMNA").Value
                If IsNull(recRegistros.Fields("FLG_IND_ORDENA").Value) Then
                    grdTotal.Col = 10: grdTotal.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_IND_ORDENA").Value) = 1 Then
                    grdTotal.Col = 10: grdTotal.Text = "SI"
                Else
                    grdTotal.Col = 10: grdTotal.Text = "NO"
                End If
                If IsNull(recRegistros.Fields("FLG_IND_TOTALIZA").Value) Then
                    grdTotal.Col = 11: grdTotal.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_IND_TOTALIZA").Value) = 1 Then
                    grdTotal.Col = 11: grdTotal.Text = "SI"
                Else
                    grdTotal.Col = 11: grdTotal.Text = "NO"
                End If
                If IsNull(recRegistros.Fields("FLG_CONVERTIR").Value) Then
                    grdTotal.Col = 12: grdTotal.Text = ""
                ElseIf Val(recRegistros.Fields("FLG_CONVERTIR").Value) = 1 Then
                    grdTotal.Col = 12: grdTotal.Text = "SI"
                Else
                    grdTotal.Col = 12: grdTotal.Text = "NO"
                End If
                grdTotal.Col = 13: grdTotal.Text = "" & recRegistros.Fields("FCH_HRA_CREACION").Value
                grdTotal.Col = 14: grdTotal.Text = "" & recRegistros.Fields("COD_USR_CREACION").Value
                grdTotal.Col = 15: grdTotal.Text = "" & recRegistros.Fields("FCH_HRA_MODIFICA").Value
                grdTotal.Col = 16: grdTotal.Text = "" & recRegistros.Fields("COD_USR_MODIFICA").Value
        End Select
        recRegistros.MoveNext
    Wend
    If frmMantCamposPorProd.ActiveControl Is Nothing Then
        grdEncabezado.SetFocus
    End If
    
    If grdEncabezado.MaxRows > 0 Then grdEncabezado.Row = 1
    tabSeciones.Tab = 0
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdConsultar_Click()
On Error GoTo LabelError
    
    Select Case tabSeciones.Tab
        Case 0
            If grdEncabezado.MaxRows = 0 Or grdEncabezado.Row = 0 Then Exit Sub
        Case 1
            If grdDetalle.MaxRows = 0 Or grdDetalle.Row = 0 Then Exit Sub
        Case 2
            If grdTotal.MaxRows = 0 Or grdTotal.Row = 0 Then Exit Sub
    End Select
    
    
    gblnswichRefresca = False
    frmMantCamposPorProd.fraIngreso.Enabled = False
    frmMantCamposPorProd.cmdGrabar.Enabled = False
    
    frmMantCamposPorProd.lblProducto = cboProducto.Text
    frmMantCamposPorProd.lblTipoArchivo = cboTipoArchivo.Text
    
    Select Case tabSeciones.Tab
        Case 0
            grdEncabezado.Row = grdEncabezado.ActiveRow
            grdEncabezado.Col = 5: frmMantCamposPorProd.lblCodigoIng2 = Trim(grdEncabezado.Text)
            glngCodigo = Val(grdEncabezado.Text)
            grdEncabezado.Col = 4
            If garrSecciones(0, 0) <> 0 And garrSecciones(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboSeccion, garrSecciones, grdEncabezado.Text)
            End If
            grdEncabezado.Col = 6: frmMantCamposPorProd.txtGlosa = Trim(grdEncabezado.Text)
            grdEncabezado.Col = 8
            Call Carga_Combo_Corp(frmMantCamposPorProd.cboCampoSalida, garrCampoSalida, Val(grdEncabezado.Text))
            Rem Obligatorio
            grdEncabezado.Col = 7
            If UCase(Trim(grdEncabezado.Text)) = "NO" Then
                frmMantCamposPorProd.chkObligatorio.Value = 0
            Else
                frmMantCamposPorProd.chkObligatorio.Value = 1
            End If
            Rem Ordena
            grdEncabezado.Col = 10
            If UCase(Trim(grdEncabezado.Text)) = "NO" Then
                frmMantCamposPorProd.chkOrdena.Value = 0
            Else
                frmMantCamposPorProd.chkOrdena.Value = 1
            End If
            Rem Totaliza
            grdEncabezado.Col = 11
            If UCase(Trim(grdEncabezado.Text)) = "NO" Then
                frmMantCamposPorProd.chkTotaliza.Value = 0
            Else
                frmMantCamposPorProd.chkTotaliza.Value = 1
            End If
            Rem Convierte
            grdEncabezado.Col = 12
            If UCase(Trim(grdEncabezado.Text)) = "NO" Then
                frmMantCamposPorProd.chkConvierte.Value = 0
            Else
                frmMantCamposPorProd.chkConvierte.Value = 1
            End If
        Case 1
            grdDetalle.Row = grdDetalle.ActiveRow
            grdDetalle.Col = 5: frmMantCamposPorProd.lblCodigoIng2 = Trim(grdDetalle.Text)
            glngCodigo = Val(grdEncabezado.Text)
            grdDetalle.Col = 4
            If garrSecciones(0, 0) <> 0 And garrSecciones(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboSeccion, garrSecciones, grdDetalle.Text)
            End If
            grdDetalle.Col = 6: frmMantCamposPorProd.txtGlosa = Trim(grdDetalle.Text)
            grdDetalle.Col = 8
            Call Carga_Combo_Corp(frmMantCamposPorProd.cboCampoSalida, garrCampoSalida, Val(grdDetalle.Text))
            Rem Obligatorio
            grdDetalle.Col = 7
            If UCase(Trim(grdDetalle.Text)) = "NO" Then
                frmMantCamposPorProd.chkObligatorio.Value = 0
            Else
                frmMantCamposPorProd.chkObligatorio.Value = 1
            End If
            Rem Ordena
            grdDetalle.Col = 10
            If UCase(Trim(grdDetalle.Text)) = "NO" Then
                frmMantCamposPorProd.chkOrdena.Value = 0
            Else
                frmMantCamposPorProd.chkOrdena.Value = 1
            End If
            Rem Totaliza
            grdDetalle.Col = 11
            If UCase(Trim(grdDetalle.Text)) = "NO" Then
                frmMantCamposPorProd.chkTotaliza.Value = 0
            Else
                frmMantCamposPorProd.chkTotaliza.Value = 1
            End If
            Rem Convierte
            grdDetalle.Col = 12
            If UCase(Trim(grdDetalle.Text)) = "NO" Then
                frmMantCamposPorProd.chkConvierte.Value = 0
            Else
                frmMantCamposPorProd.chkConvierte.Value = 1
            End If
        Case 2
            grdTotal.Row = grdTotal.ActiveRow
            grdTotal.Col = 5: frmMantCamposPorProd.lblCodigoIng2 = Trim(grdTotal.Text)
            glngCodigo = Val(grdEncabezado.Text)
            grdTotal.Col = 4
            If garrSecciones(0, 0) <> 0 And garrSecciones(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboSeccion, garrSecciones, grdTotal.Text)
            End If
            grdTotal.Col = 6: frmMantCamposPorProd.txtGlosa = Trim(grdTotal.Text)
            grdTotal.Col = 8
            Call Carga_Combo_Corp(frmMantCamposPorProd.cboCampoSalida, garrCampoSalida, Val(grdTotal.Text))
            Rem Obligatorio
            grdTotal.Col = 7
            If UCase(Trim(grdTotal.Text)) = "NO" Then
                frmMantCamposPorProd.chkObligatorio.Value = 0
            Else
                frmMantCamposPorProd.chkObligatorio.Value = 1
            End If
            Rem Ordena
            grdTotal.Col = 10
            If UCase(Trim(grdTotal.Text)) = "NO" Then
                frmMantCamposPorProd.chkOrdena.Value = 0
            Else
                frmMantCamposPorProd.chkOrdena.Value = 1
            End If
            Rem Totaliza
            grdTotal.Col = 11
            If UCase(Trim(grdTotal.Text)) = "NO" Then
                frmMantCamposPorProd.chkTotaliza.Value = 0
            Else
                frmMantCamposPorProd.chkTotaliza.Value = 1
            End If
            Rem Convierte
            grdTotal.Col = 12
            If UCase(Trim(grdTotal.Text)) = "NO" Then
                frmMantCamposPorProd.chkConvierte.Value = 0
            Else
                frmMantCamposPorProd.chkConvierte.Value = 1
            End If
    End Select
    
    Call AlmacenarValoresCtls(frmMantCamposPorProd, "INGCAMP")
    frmMantCamposPorProd.tlbAcciones.Buttons("Grabar").Visible = False
    frmMantCamposPorProd.Show 1
    
LabelSalir:
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdEliminar_Click()
Dim lngIdeCampoRemesa As Long
Dim strCampoEntrada As String
Dim strCampoSalida As String
Dim Cmd As New ADODB.Command
On Error GoTo LabelError

    Select Case tabSeciones.Tab
        Case 0
            If grdEncabezado.MaxRows = 0 Or grdEncabezado.Row = 0 Then Exit Sub
        Case 1
            If grdDetalle.MaxRows = 0 Or grdDetalle.Row = 0 Then Exit Sub
        Case 2
            If grdTotal.MaxRows = 0 Or grdTotal.Row = 0 Then Exit Sub
    End Select
    
        
    staEstado.Panels(1) = "Eliminando..."
    Screen.MousePointer = 11
    
    Select Case tabSeciones.Tab
        Case 0
            grdEncabezado.Row = grdEncabezado.ActiveRow
            grdEncabezado.Col = 9: strCampoEntrada = Trim(grdEncabezado.Text)
            grdEncabezado.Col = 6: strCampoSalida = Trim(grdEncabezado.Text)
            grdEncabezado.Col = 5: lngIdeCampoRemesa = Val(grdEncabezado.Text)
        Case 1
            grdDetalle.Row = grdDetalle.ActiveRow
            grdDetalle.Col = 9: strCampoEntrada = Trim(grdDetalle.Text)
            grdDetalle.Col = 6: strCampoSalida = Trim(grdDetalle.Text)
            grdDetalle.Col = 5: lngIdeCampoRemesa = Val(grdDetalle.Text)
        Case 2
            grdTotal.Row = grdTotal.ActiveRow
            grdTotal.Col = 9: strCampoEntrada = Trim(grdTotal.Text)
            grdTotal.Col = 6: strCampoSalida = Trim(grdTotal.Text)
            grdTotal.Col = 5: lngIdeCampoRemesa = Val(grdTotal.Text)
    End Select
    
    If MsgBox("�Est� seguro de Eliminar Registro cuyo campo de" & Chr(13) & "Entrada : " & strCampoEntrada & Chr(13) & "Salida    : " & strCampoSalida, vbYesNo + vbQuestion, Me.Caption) <> vbYes Then
        GoTo LabelSalir
    Else
        Rem llamada a Procedimiento
        Cmd.ActiveConnection = gAdoCon
        Cmd.CommandType = adCmdStoredProc
        Cmd.CommandText = "CSBPI.PKG_FME_CAMPOS_X_PRODUCTO.SP_FME_CAMPOS_BORRAR"
        
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, lngIdeCampoRemesa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
        Cmd.Execute
    
        If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
            MsgBox "El registro no pudo ser Eliminado. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbExclamation, Me.Caption
            GoTo LabelSalir
        End If
    
        MsgBox "Registro Eliminado Exitosamente.", vbInformation, Me.Caption
        Call cmdBuscar_Click
        
    End If

LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdModificar_Click()
Dim intTab As Integer
Dim intConvertir As Integer
On Error GoTo LabelError
    
    Select Case tabSeciones.Tab
        Case 0
            If grdEncabezado.MaxRows = 0 Or grdEncabezado.Row = 0 Then Exit Sub
            intTab = 0
        Case 1
            If grdDetalle.MaxRows = 0 Or grdDetalle.Row = 0 Then Exit Sub
            intTab = 1
        Case 2
            If grdTotal.MaxRows = 0 Or grdTotal.Row = 0 Then Exit Sub
            intTab = 3
    End Select
    
        
    gblnswichRefresca = False
    gblnModifica = True
    
    frmMantCamposPorProd.fraIngreso.Enabled = True
    frmMantCamposPorProd.cmdGrabar.Enabled = True
    
    frmMantCamposPorProd.lblProducto = cboProducto.Text
    frmMantCamposPorProd.lblTipoArchivo = cboTipoArchivo.Text
    
    Select Case tabSeciones.Tab
        Case 0
            grdEncabezado.Row = grdEncabezado.ActiveRow
            grdEncabezado.Col = 5: frmMantCamposPorProd.lblCodigoIng2 = Trim(grdEncabezado.Text)
            glngCodigo = Val(grdEncabezado.Text)
            grdEncabezado.Col = 4
            If garrSecciones(0, 0) <> 0 And garrSecciones(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboSeccion, garrSecciones, grdEncabezado.Text)
            End If
            grdEncabezado.Col = 6: frmMantCamposPorProd.txtGlosa = Trim(grdEncabezado.Text)
            grdEncabezado.Col = 8
            If garrCampoSalida(0, 0) <> 0 And garrCampoSalida(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboCampoSalida, garrCampoSalida, Val(grdEncabezado.Text))
            End If
            Rem Obligatorio
            grdEncabezado.Col = 7
            If UCase(Trim(grdEncabezado.Text)) = "NO" Then
                frmMantCamposPorProd.chkObligatorio.Value = 0
            Else
                frmMantCamposPorProd.chkObligatorio.Value = 1
            End If
            Rem Ordena
            grdEncabezado.Col = 10
            If UCase(Trim(grdEncabezado.Text)) = "NO" Then
                frmMantCamposPorProd.chkOrdena.Value = 0
            Else
                frmMantCamposPorProd.chkOrdena.Value = 1
            End If
            Rem Totaliza
            grdEncabezado.Col = 11
            If UCase(Trim(grdEncabezado.Text)) = "NO" Then
                frmMantCamposPorProd.chkTotaliza.Value = 0
            Else
                frmMantCamposPorProd.chkTotaliza.Value = 1
            End If
            Rem Convierte
            grdEncabezado.Col = 12
            If UCase(Trim(grdEncabezado.Text)) = "NO" Then
                frmMantCamposPorProd.chkConvierte.Value = 0
            Else
                frmMantCamposPorProd.chkConvierte.Value = 1
            End If
        Case 1
            grdDetalle.Row = grdDetalle.ActiveRow
            grdDetalle.Col = 5: frmMantCamposPorProd.lblCodigoIng2 = Trim(grdDetalle.Text)
            glngCodigo = Val(grdEncabezado.Text)
            grdDetalle.Col = 4
            If garrSecciones(0, 0) <> 0 And garrSecciones(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboSeccion, garrSecciones, grdDetalle.Text)
            End If
            grdDetalle.Col = 6: frmMantCamposPorProd.txtGlosa = Trim(grdDetalle.Text)
            grdDetalle.Col = 8
            If garrCampoSalida(0, 0) <> 0 And garrCampoSalida(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboCampoSalida, garrCampoSalida, Val(grdDetalle.Text))
            End If
            Rem Obligatorio
            grdDetalle.Col = 7
            If UCase(Trim(grdDetalle.Text)) = "NO" Then
                frmMantCamposPorProd.chkObligatorio.Value = 0
            Else
                frmMantCamposPorProd.chkObligatorio.Value = 1
            End If
            Rem Ordena
            grdDetalle.Col = 10
            If UCase(Trim(grdDetalle.Text)) = "NO" Then
                frmMantCamposPorProd.chkOrdena.Value = 0
            Else
                frmMantCamposPorProd.chkOrdena.Value = 1
            End If
            Rem Totaliza
            grdDetalle.Col = 11
            If UCase(Trim(grdDetalle.Text)) = "NO" Then
                frmMantCamposPorProd.chkTotaliza.Value = 0
            Else
                frmMantCamposPorProd.chkTotaliza.Value = 1
            End If
            Rem Convierte
            grdDetalle.Col = 12
            If UCase(Trim(grdDetalle.Text)) = "NO" Then
                frmMantCamposPorProd.chkConvierte.Value = 0
            Else
                frmMantCamposPorProd.chkConvierte.Value = 1
            End If
            intConvertir = Trim(garrCampoSalida(frmMantCamposPorProd.cboCampoSalida.ListIndex, 2))
            If intConvertir = 1 Then
                frmMantCamposPorProd.chkConvierte.Enabled = True
            Else
                frmMantCamposPorProd.chkConvierte.Enabled = False
            End If
        Case 2
            grdTotal.Row = grdTotal.ActiveRow
            grdTotal.Col = 5: frmMantCamposPorProd.lblCodigoIng2 = Trim(grdTotal.Text)
            glngCodigo = Val(grdEncabezado.Text)
            grdTotal.Col = 4
            If garrSecciones(0, 0) <> 0 And garrSecciones(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboSeccion, garrSecciones, grdTotal.Text)
            End If
            grdTotal.Col = 6: frmMantCamposPorProd.txtGlosa = Trim(grdTotal.Text)
            grdTotal.Col = 8
            If garrCampoSalida(0, 0) <> 0 And garrCampoSalida(0, 1) <> "" Then
                Call Carga_Combo_Corp(frmMantCamposPorProd.cboCampoSalida, garrCampoSalida, Val(grdTotal.Text))
            End If
            Rem Obligatorio
            grdTotal.Col = 7
            If UCase(Trim(grdTotal.Text)) = "NO" Then
                frmMantCamposPorProd.chkObligatorio.Value = 0
            Else
                frmMantCamposPorProd.chkObligatorio.Value = 1
            End If
            Rem Ordena
            grdTotal.Col = 10
            If UCase(Trim(grdTotal.Text)) = "NO" Then
                frmMantCamposPorProd.chkOrdena.Value = 0
            Else
                frmMantCamposPorProd.chkOrdena.Value = 1
            End If
            Rem Totaliza
            grdTotal.Col = 11
            If UCase(Trim(grdTotal.Text)) = "NO" Then
                frmMantCamposPorProd.chkTotaliza.Value = 0
            Else
                frmMantCamposPorProd.chkTotaliza.Value = 1
            End If
            Rem Convierte
            grdTotal.Col = 12
            If UCase(Trim(grdTotal.Text)) = "NO" Then
                frmMantCamposPorProd.chkConvierte.Value = 0
            Else
                frmMantCamposPorProd.chkConvierte.Value = 1
            End If
    End Select
    frmMantCamposPorProd.cboSeccion.Enabled = False
    
    Call AlmacenarValoresCtls(frmMantCamposPorProd, "INGCAMP")
    frmMantCamposPorProd.Show 1
    If gblnswichRefresca Then
        Call cmdBuscar_Click
    End If
    
    Select Case intTab
        Case 0
            Call PosicionaGrilla(grdEncabezado, glngCodigo, 5)
            tabSeciones.Tab = 0
        Case 1
            Call PosicionaGrilla(grdDetalle, glngCodigo, 5)
            tabSeciones.Tab = 1
        Case 2
            Call PosicionaGrilla(grdTotal, glngCodigo, 5)
            tabSeciones.Tab = 2
    End Select
    

LabelSalir:
    Screen.MousePointer = 0
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call Carga_Imagenes
    Call Carga_Secciones
    Call Carga_CamposEntradas
    Call Carga_Combo_Par(cboTipoArchivo, 1, 6)
    Call Carga_Combo_Par(cboProducto, 1, 10)
    Me.Top = 400
    Me.Left = 3090
End Sub
Private Sub Carga_Imagenes()
        
    ilsIconos.ListImages.Clear
    ilsIconos.ListImages.Add 1, "Agregar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\agregar.bmp")
    ilsIconos.ListImages.Add 2, "Modificar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\editar.bmp")
    ilsIconos.ListImages.Add 3, "Eliminar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\eliminar.bmp")
    ilsIconos.ListImages.Add 4, "Consultar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\consultar.bmp")
    ilsIconos.ListImages.Add 5, "Salir", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\salir.bmp")
    ilsIconos.ListImages.Add 6, "Grabar", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\grabar.bmp")
    ilsIconos.ListImages.Add 7, "Volver", LoadPicture(gstrRutaRec & "\DLLS_Carga_Corfig\img\volver.bmp")
    
    
    tlbAcciones.ImageList = ilsIconos
    
    tlbAcciones.Buttons("Agregar").Image = 1
    tlbAcciones.Buttons("Modificar").Image = 2
    tlbAcciones.Buttons("Eliminar").Image = 3
    tlbAcciones.Buttons("Consultar").Image = 4
    tlbAcciones.Buttons("Salir").Image = 5


End Sub
Private Sub Form_Unload(Cancel As Integer)
        Unload Me
End Sub



Private Sub grdDetalle_DblClick(ByVal Col As Long, ByVal Row As Long)
    If grdDetalle.MaxRows = 0 Or grdDetalle.Row = 0 Then Exit Sub
    Call cmdModificar_Click
End Sub

Private Sub grdEncabezado_DblClick(ByVal Col As Long, ByVal Row As Long)
    If grdEncabezado.MaxRows = 0 Or grdEncabezado.Row = 0 Then Exit Sub
    Call cmdModificar_Click
End Sub

Private Sub grdTotal_DblClick(ByVal Col As Long, ByVal Row As Long)
    If grdTotal.MaxRows = 0 Or grdTotal.Row = 0 Then Exit Sub
    Call cmdModificar_Click
End Sub

Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)
On Error GoTo LabelError
        
    Select Case UCase(Trim(Button.Key))
        Case "AGREGAR"
            Call cmdAgregar_Click
        Case "MODIFICAR"
            Call cmdModificar_Click
        Case "ELIMINAR"
            Call cmdEliminar_Click
        Case "CONSULTAR"
            Call cmdConsultar_Click
        Case "SALIR"
            Unload Me
    End Select
    
LabelSalir:
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, , "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub


Function EnClave(strClave As String) As String
    Dim intI        As Integer
    Dim strUsrPsw   As String
    strClave = Trim(strClave)
    For intI = 1 To Len(Trim$(strClave))
        strUsrPsw = strUsrPsw + Chr(Asc(Mid(strClave, intI, 1)) Xor 20)
    Next
    EnClave = strUsrPsw
End Function
