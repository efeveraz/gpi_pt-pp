VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMantCamposPorProd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenci�n Campos de Tipo Archivo"
   ClientHeight    =   4425
   ClientLeft      =   5760
   ClientTop       =   3510
   ClientWidth     =   6495
   Icon            =   "frmMantCamposPorProd.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4425
   ScaleWidth      =   6495
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   60
      TabIndex        =   15
      Top             =   420
      Width           =   6375
      Begin VB.Label Label4 
         Caption         =   "Direcci�n"
         Height          =   240
         Left            =   4080
         TabIndex        =   20
         Top             =   360
         Width           =   780
      End
      Begin VB.Label lblTipoArchivo 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   4860
         TabIndex        =   19
         Tag             =   "INGCAMP"
         Top             =   300
         Width           =   1395
      End
      Begin VB.Label Label2 
         Caption         =   "Tipo Archivo"
         Height          =   240
         Left            =   120
         TabIndex        =   18
         Top             =   360
         Width           =   900
      End
      Begin VB.Label lblProducto 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   1140
         TabIndex        =   17
         Tag             =   "INGCAMP"
         Top             =   300
         Width           =   2775
      End
      Begin VB.Label lblLista 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Producto Seleccionado"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   1
         Left            =   0
         TabIndex        =   16
         Top             =   0
         Width           =   6345
      End
   End
   Begin VB.Frame fraIngreso 
      Height          =   2295
      Left            =   60
      TabIndex        =   8
      Top             =   1140
      Width           =   6375
      Begin VB.Frame frmFlagDetalle 
         BorderStyle     =   0  'None
         Height          =   435
         Left            =   2700
         TabIndex        =   22
         Top             =   1800
         Width           =   3615
         Begin VB.CheckBox chkConvierte 
            Caption         =   "Convierte"
            Height          =   255
            Left            =   2580
            TabIndex        =   25
            Tag             =   "INGCAMP"
            Top             =   120
            Width           =   990
         End
         Begin VB.CheckBox chkTotaliza 
            Caption         =   "Totaliza"
            Height          =   255
            Left            =   1320
            TabIndex        =   24
            Tag             =   "INGCAMP"
            Top             =   120
            Width           =   1170
         End
         Begin VB.CheckBox chkOrdena 
            Caption         =   "Ordena"
            Height          =   255
            Left            =   60
            TabIndex        =   23
            Tag             =   "INGCAMP"
            Top             =   120
            Width           =   930
         End
      End
      Begin VB.ComboBox cboSeccion 
         Height          =   315
         ItemData        =   "frmMantCamposPorProd.frx":000C
         Left            =   1380
         List            =   "frmMantCamposPorProd.frx":000E
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   780
         Width           =   2535
      End
      Begin VB.ComboBox cboCampoSalida 
         Height          =   315
         ItemData        =   "frmMantCamposPorProd.frx":0010
         Left            =   1380
         List            =   "frmMantCamposPorProd.frx":0012
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Tag             =   "INGCAMP"
         Top             =   1500
         Width           =   2535
      End
      Begin VB.TextBox txtGlosa 
         Height          =   310
         Left            =   1380
         MaxLength       =   40
         TabIndex        =   1
         Tag             =   "INGCAMP"
         Top             =   1140
         Width           =   4875
      End
      Begin VB.CheckBox chkObligatorio 
         Caption         =   "Obligatorio"
         Height          =   255
         Left            =   1380
         TabIndex        =   3
         Tag             =   "INGCAMP"
         Top             =   1920
         Width           =   1170
      End
      Begin VB.Label Label1 
         Caption         =   "Seccion"
         Height          =   240
         Left            =   120
         TabIndex        =   21
         Top             =   810
         Width           =   1260
      End
      Begin VB.Label lblCodigoIng2 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   1380
         TabIndex        =   13
         Tag             =   "INGCAMP"
         Top             =   420
         Width           =   1275
      End
      Begin VB.Label lblCodigoIng 
         Caption         =   "C�digo"
         Height          =   240
         Left            =   120
         TabIndex        =   12
         Top             =   465
         Width           =   540
      End
      Begin VB.Label lblCampo 
         Caption         =   "Campo Salida"
         Height          =   240
         Left            =   120
         TabIndex        =   11
         Top             =   1590
         Width           =   1260
      End
      Begin VB.Label lblGlosa 
         Caption         =   "Campo Entrada"
         Height          =   240
         Left            =   120
         TabIndex        =   10
         Top             =   1200
         Width           =   1200
      End
      Begin VB.Label lblLista 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Datos del Campo"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   0
         Left            =   0
         TabIndex        =   9
         Top             =   60
         Width           =   6345
      End
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   6
      Top             =   4155
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8829
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Grabar"
            Object.ToolTipText     =   "Grabar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Volver"
            Object.ToolTipText     =   "Volver"
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraBotones 
      Height          =   735
      Index           =   0
      Left            =   60
      TabIndex        =   14
      Top             =   3390
      Width           =   6375
      Begin VB.CommandButton cmdGrabar 
         Caption         =   "&Grabar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   75
         TabIndex        =   4
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   4815
         TabIndex        =   5
         Top             =   210
         Width           =   1425
      End
   End
End
Attribute VB_Name = "frmMantCamposPorProd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim mintConvertir As Integer

Private Sub cboCampo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cboCampoSalida_Click()
    If cboCampoSalida.ListIndex = -1 Then Exit Sub
    If frmFlagDetalle.Visible Then
        mintConvertir = Trim(garrCampoSalida(cboCampoSalida.ListIndex, 2))
        If mintConvertir = 1 Then
            chkConvierte.Value = 1
            chkConvierte.Enabled = True
        Else
            chkConvierte.Value = 0
            chkConvierte.Enabled = False
        End If
    End If
    txtGlosa.Text = cboCampoSalida.Text
End Sub

Private Sub cboSeccion_Click()
    If cboSeccion.ListIndex = -1 Then Exit Sub
    gstrseccion = Trim(garrSecciones(cboSeccion.ListIndex, 2))
    If gstrseccion = "DET" Then
        frmFlagDetalle.Visible = True
    Else
        frmFlagDetalle.Visible = False
    End If
End Sub

Private Sub cboSeccion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub Check1_Click()

End Sub

Private Sub Check1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub Check2_Click()

End Sub

Private Sub chkConvierte_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub chkObligatorio_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub chkOrdena_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub chkTotaliza_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cmdGrabar_Click()
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    staEstado.Panels(1) = "Grabando..."
    Screen.MousePointer = 11
    
    Rem Validaciones
    If cboSeccion.ListIndex = -1 Then
        MsgBox "Debe seleccionar Secci�n", vbExclamation, Me.Caption
        cboSeccion.SetFocus
        GoTo LabelSalir
    End If
    If Trim(txtGlosa) = "" Then
        MsgBox "Debe ingresar Nombre campo Salida", vbExclamation, Me.Caption
        txtGlosa.SetFocus
        GoTo LabelSalir
    End If
    If cboCampoSalida.ListIndex = -1 Then
        MsgBox "Debe seleccionar campo de Entrada", vbExclamation, Me.Caption
        cboCampoSalida.SetFocus
        GoTo LabelSalir
    End If
    
    If Mid(Trim(cboCampoSalida.Text), 1, 1) = "*" And gstrseccion = "DET" Then
        If Trim(cboCampoSalida.Text) <> "*COD REG" Then
            MsgBox "Seleccione un campo de Entrada, que no comience con (*)," & Chr(13) & _
            "Exceptuando el *COD REG ya que estos campos solo se pueden," & Chr(13) & "asignar a la secci�n Encabezado y Total.", vbExclamation, Me.Caption
            cboCampoSalida.SetFocus
            GoTo LabelSalir
        End If
    End If
    If Mid(Trim(cboCampoSalida.Text), 1, 1) <> "*" And gstrseccion = "ENC" Or gstrseccion = "TOT" Then
        MsgBox "Seleccione un campo de Entrada, que comience con (*)," & Chr(13) & _
        "Ya que estos campos se pueden," & Chr(13) & "asignar a la secci�n Encabezado y Total.", vbExclamation, Me.Caption
        cboCampoSalida.SetFocus
        GoTo LabelSalir
    End If
    Rem ***********************************************************************
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.PKG_FME_CAMPOS_X_PRODUCTO.SP_FME_CAMPOS_GRABAR"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeProducto", adNumeric, adParamInput, 10, glngIdeProducto)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodTipoArch", adChar, adParamInput, 8, glngIdeTipoArch)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodSeccion", adChar, adParamInput, 8, Val(garrSecciones(cboSeccion.ListIndex, 0)))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_Glosa", adVarChar, adParamInput, 40, Trim(txtGlosa.Text))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeColumna", adNumeric, adParamInput, 10, Val(garrCampoSalida(cboCampoSalida.ListIndex, 0)))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_FlagObligatorio", adChar, adParamInput, 1, Val(chkObligatorio.Value))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_FlagOrdena", adChar, adParamInput, 1, IIf(frmFlagDetalle.Visible, Val(chkOrdena.Value), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_FlagTotaliza", adChar, adParamInput, 1, IIf(frmFlagDetalle.Visible, Val(chkTotaliza.Value), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_FlagConvertir", adChar, adParamInput, 1, IIf(frmFlagDetalle.Visible, Val(chkConvierte.Value), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adChar, adParamInput, 10, gstrUsuario)
    Rem Definicion de tipo accion
    Rem 1= Grabar; 2 = Modificar (p_n_Accion)
    If Not gblnModifica Then 'Estamos Grabando
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_Accion", adNumeric, adParamInput, 1, 1)
    Else 'Estamos Modificando
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_Accion", adNumeric, adParamInput, 1, 2)
    End If
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInputOutput, 10, Val(lblCodigoIng2))
    Cmd.Parameters.Append Cmd.CreateParameter("p_V_DescError", adVarChar, adParamOutput, 1000)
    Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) = "1111" Then
            MsgBox "El registro Campo por Tipo Archivo ya existe." & Chr(13) & "No se puede grabar, solo aplicar la opci�n Modificar ", vbExclamation, Me.Caption
        Else
            MsgBox "La informaci�n no pudo ser Grabada. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbExclamation, Me.Caption
        End If
        GoTo LabelSalir
    Else
        lblCodigoIng2 = Cmd.Parameters("p_n_codigo").Value
    End If
    glngCodigo = lblCodigoIng2
    MsgBox "informaci�n Grabada Exitosamente.", vbInformation, Me.Caption
    
    gblnswichRefresca = True
    If Not gblnModifica = True Then
        Call LimpiarControles(Me, "INGCAMP")
        frmCamposPorProd.cmdBuscar_Click
        Select Case UCase(Trim(garrSecciones(cboSeccion.ListIndex, 2)))
            Case "ENC"
                frmCamposPorProd.tabSeciones.Tab = 0
                Call PosicionaGrilla(frmCamposPorProd.grdEncabezado, glngCodigo, 5)
            Case "DET"
                Call PosicionaGrilla(frmCamposPorProd.grdDetalle, glngCodigo, 5)
                frmCamposPorProd.tabSeciones.Tab = 1
            Case "TOT"
                Call PosicionaGrilla(frmCamposPorProd.grdTotal, glngCodigo, 5)
                frmCamposPorProd.tabSeciones.Tab = 2
        End Select
    Else
        Unload Me
        GoTo LabelSalir
    End If
    
    Call AlmacenarValoresCtls(Me, "INGCAMP")
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdVolver_Click()
    If HayCambiosEnControles(Me, "INGCAMP") Then
        If MsgBox("Existe Informaci�n sin Grabar." & Chr(13) & "�Desea volver sin grabar los cambios?", vbYesNo + vbInformation, Me.Caption) <> vbYes Then
            Exit Sub
        Else
            gblnswichRefresca = True
        End If
    End If
    Unload Me
End Sub

Private Sub Form_Load()
    Call Carga_Imagenes
    txtGlosa.Enabled = False
End Sub

Private Sub Carga_Imagenes()
        
    tlbAcciones.ImageList = frmCamposPorProd.ilsIconos
    
    tlbAcciones.Buttons("Grabar").Image = 6
    tlbAcciones.Buttons("Volver").Image = 7
    
    
End Sub
Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)
On Error GoTo LabelError

    Select Case UCase(Trim(Button.Key))
        Case "GRABAR"
            Call cmdGrabar_Click
        Case "VOLVER"
            Call cmdVolver_Click
    End Select
    
LabelSalir:
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, , "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub txtGlosa_GotFocus()
    txtGlosa.SelStart = 0
    txtGlosa.SelLength = Len(txtGlosa)
End Sub

Private Sub txtGlosa_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

