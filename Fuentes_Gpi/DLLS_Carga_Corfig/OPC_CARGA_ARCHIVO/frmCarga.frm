VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "ss32x25.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmCarga 
   Caption         =   "Carga Archivo Cliente"
   ClientHeight    =   7185
   ClientLeft      =   2160
   ClientTop       =   2400
   ClientWidth     =   9525
   Icon            =   "frmCarga.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7185
   ScaleWidth      =   9525
   Begin VB.Frame fraCarga 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3945
      Left            =   60
      TabIndex        =   31
      Top             =   2220
      Width           =   9435
      Begin TabDlg.SSTab sstabDet 
         Height          =   2955
         Left            =   3120
         TabIndex        =   15
         Top             =   420
         Width           =   6240
         _ExtentX        =   11007
         _ExtentY        =   5212
         _Version        =   393216
         TabOrientation  =   1
         Style           =   1
         TabHeight       =   520
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "frmCarga.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "grdDetalle"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Errores Forma"
         TabPicture(1)   =   "frmCarga.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lblDescErr"
         Tab(1).Control(1)=   "grdErrForma"
         Tab(1).ControlCount=   2
         TabCaption(2)   =   "Errores Fondo"
         TabPicture(2)   =   "frmCarga.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Label11"
         Tab(2).Control(1)=   "grdErrFondo"
         Tab(2).ControlCount=   2
         Begin FPSpread.vaSpread grdErrForma 
            Height          =   2265
            Left            =   -74970
            TabIndex        =   48
            Top             =   -15
            Width           =   6165
            _Version        =   131077
            _ExtentX        =   10874
            _ExtentY        =   3995
            _StockProps     =   64
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridSolid       =   0   'False
            MaxCols         =   0
            MaxRows         =   25
            ScrollBarExtMode=   -1  'True
            SpreadDesigner  =   "frmCarga.frx":0060
            VirtualMaxRows  =   25
            VirtualMode     =   -1  'True
            VirtualOverlap  =   100
            VirtualRows     =   100
            VirtualScrollBuffer=   -1  'True
         End
         Begin FPSpread.vaSpread grdErrFondo 
            Height          =   2265
            Left            =   -75000
            TabIndex        =   54
            Top             =   0
            Width           =   6165
            _Version        =   131077
            _ExtentX        =   10874
            _ExtentY        =   3995
            _StockProps     =   64
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridSolid       =   0   'False
            MaxCols         =   0
            MaxRows         =   25
            ScrollBarExtMode=   -1  'True
            SpreadDesigner  =   "frmCarga.frx":020C
            VirtualMaxRows  =   25
            VirtualMode     =   -1  'True
            VirtualOverlap  =   100
            VirtualRows     =   100
            VirtualScrollBuffer=   -1  'True
         End
         Begin FPSpread.vaSpread grdDetalle 
            Height          =   2565
            Left            =   45
            TabIndex        =   58
            Top             =   0
            Width           =   6120
            _Version        =   131077
            _ExtentX        =   10795
            _ExtentY        =   4524
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridSolid       =   0   'False
            MaxCols         =   0
            MaxRows         =   1
            OperationMode   =   2
            ScrollBarExtMode=   -1  'True
            SelectBlockOptions=   0
            SpreadDesigner  =   "frmCarga.frx":03B8
            VirtualMaxRows  =   25
            VirtualMode     =   -1  'True
            VirtualOverlap  =   100
            VirtualRows     =   100
            VirtualScrollBuffer=   -1  'True
            VisibleCols     =   500
         End
         Begin VB.Label Label11 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   -75000
            TabIndex        =   55
            Top             =   2280
            Width           =   6150
         End
         Begin VB.Label lblDescErr 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   -74970
            TabIndex        =   49
            Top             =   2280
            Width           =   6150
         End
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H00B2B2B2&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   540
         Left            =   90
         TabIndex        =   32
         Top             =   3405
         Width           =   9285
         Begin VB.PictureBox pctAvance 
            Appearance      =   0  'Flat
            AutoRedraw      =   -1  'True
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            FillStyle       =   0  'Solid
            ForeColor       =   &H80000008&
            Height          =   120
            Left            =   5340
            Picture         =   "frmCarga.frx":05A4
            ScaleHeight     =   120
            ScaleWidth      =   2430
            TabIndex        =   33
            Top             =   285
            Visible         =   0   'False
            Width           =   2430
         End
         Begin VB.Label lbl 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Cron�metro"
            ForeColor       =   &H00404040&
            Height          =   210
            Index           =   2
            Left            =   7890
            TabIndex        =   47
            Top             =   45
            Width           =   840
         End
         Begin VB.Label lblCrono 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0 "
            ForeColor       =   &H00404040&
            Height          =   225
            Left            =   7815
            TabIndex        =   46
            Tag             =   "CARGA"
            Top             =   240
            Width           =   1005
         End
         Begin VB.Label lbl 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "0"
            ForeColor       =   &H00404040&
            Height          =   210
            Index           =   1
            Left            =   5910
            TabIndex        =   45
            Top             =   45
            Width           =   450
         End
         Begin VB.Shape Shape1 
            Height          =   225
            Left            =   5310
            Top             =   240
            Width           =   2490
         End
         Begin VB.Label lbl 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "% Avance"
            ForeColor       =   &H00404040&
            Height          =   255
            Index           =   0
            Left            =   6375
            TabIndex        =   44
            Top             =   45
            Width           =   765
         End
         Begin VB.Label lbl 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Err�neos"
            ForeColor       =   &H00404040&
            Height          =   255
            Index           =   5
            Left            =   2325
            TabIndex        =   43
            Top             =   45
            Width           =   660
         End
         Begin VB.Label lblErroneos 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0 "
            ForeColor       =   &H00404040&
            Height          =   225
            Left            =   2115
            TabIndex        =   42
            Tag             =   "CARGA"
            Top             =   240
            Width           =   1005
         End
         Begin VB.Label lbl 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Le�dos"
            ForeColor       =   &H00404040&
            Height          =   255
            Index           =   3
            Left            =   360
            TabIndex        =   41
            Top             =   45
            Width           =   480
         End
         Begin VB.Label lblLeidos 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0 "
            ForeColor       =   &H00404040&
            Height          =   225
            Left            =   75
            TabIndex        =   40
            Tag             =   "CARGA"
            Top             =   240
            Width           =   1005
         End
         Begin VB.Label lbl 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Correctos"
            ForeColor       =   &H00404040&
            Height          =   255
            Index           =   4
            Left            =   1230
            TabIndex        =   39
            Top             =   45
            Width           =   720
         End
         Begin VB.Label lblCorrectos 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0 "
            ForeColor       =   &H00404040&
            Height          =   225
            Left            =   1095
            TabIndex        =   38
            Tag             =   "CARGA"
            Top             =   240
            Width           =   1005
         End
         Begin VB.Label lblTotDoc 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0 "
            ForeColor       =   &H00404040&
            Height          =   225
            Left            =   3135
            TabIndex        =   37
            Tag             =   "CARGA"
            Top             =   240
            Width           =   1140
         End
         Begin VB.Label lbl 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Monto Total"
            ForeColor       =   &H00404040&
            Height          =   210
            Index           =   8
            Left            =   3360
            TabIndex        =   36
            Top             =   45
            Width           =   825
         End
         Begin VB.Label lblTraspasados 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0 "
            ForeColor       =   &H00404040&
            Height          =   225
            Left            =   4290
            TabIndex        =   35
            Tag             =   "CARGA"
            Top             =   240
            Width           =   1005
         End
         Begin VB.Label lbl 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Traspasados"
            ForeColor       =   &H00404040&
            Height          =   255
            Index           =   6
            Left            =   4305
            TabIndex        =   34
            Top             =   45
            Width           =   960
         End
      End
      Begin TabDlg.SSTab sstabEncab 
         Height          =   2985
         Left            =   90
         TabIndex        =   12
         Top             =   405
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   5265
         _Version        =   393216
         TabOrientation  =   1
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "Encabezado"
         TabPicture(0)   =   "frmCarga.frx":3080
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "grdEncab"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Total"
         TabPicture(1)   =   "frmCarga.frx":309C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdTotal"
         Tab(1).ControlCount=   1
         Begin FPSpread.vaSpread grdEncab 
            CausesValidation=   0   'False
            Height          =   2475
            Left            =   0
            TabIndex        =   56
            Top             =   0
            Width           =   2985
            _Version        =   131077
            _ExtentX        =   5265
            _ExtentY        =   4366
            _StockProps     =   64
            ColHeaderDisplay=   0
            DisplayColHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridShowHoriz   =   0   'False
            GridShowVert    =   0   'False
            MaxCols         =   1
            MaxRows         =   1
            RowHeaderDisplay=   0
            ScrollBarExtMode=   -1  'True
            SpreadDesigner  =   "frmCarga.frx":30B8
            UserResize      =   1
         End
         Begin FPSpread.vaSpread grdTotal 
            CausesValidation=   0   'False
            Height          =   2565
            Left            =   -75000
            TabIndex        =   57
            Top             =   0
            Width           =   2985
            _Version        =   131077
            _ExtentX        =   5265
            _ExtentY        =   4524
            _StockProps     =   64
            ColHeaderDisplay=   0
            DisplayColHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridShowHoriz   =   0   'False
            GridShowVert    =   0   'False
            MaxCols         =   1
            MaxRows         =   1
            RowHeaderDisplay=   0
            ScrollBarExtMode=   -1  'True
            SpreadDesigner  =   "frmCarga.frx":32E3
            UserResize      =   1
         End
      End
      Begin VB.Label lbl 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00A26D00&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " Detalle"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00E0E0E0&
         Height          =   255
         Index           =   7
         Left            =   3135
         TabIndex        =   51
         Top             =   90
         Width           =   6225
      End
      Begin VB.Label lbl 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00A26D00&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " Encabezado/Total "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00E0E0E0&
         Height          =   255
         Index           =   9
         Left            =   105
         TabIndex        =   50
         Top             =   90
         Width           =   3000
      End
   End
   Begin VB.Frame fraBotones 
      Height          =   735
      Index           =   4
      Left            =   60
      TabIndex        =   28
      Top             =   6120
      Width           =   9435
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   5825
         TabIndex        =   20
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   4390
         TabIndex        =   19
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   2950
         TabIndex        =   18
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdValidar 
         Caption         =   "Validar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   1515
         TabIndex        =   17
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdCargar 
         Caption         =   "Cargar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   75
         TabIndex        =   16
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   7875
         TabIndex        =   21
         Top             =   210
         Width           =   1425
      End
   End
   Begin VB.Frame fraSeleccion 
      Height          =   1815
      Index           =   4
      Left            =   60
      TabIndex        =   13
      Top             =   420
      Width           =   9435
      Begin MSComDlg.CommonDialog dlgRuta 
         Left            =   75
         Top             =   1605
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.ComboBox cboProducto 
         Height          =   315
         Left            =   7875
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Tag             =   "CARGA"
         Top             =   300
         Width           =   1440
      End
      Begin VB.CommandButton cmdOrigen 
         Caption         =   "..."
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6810
         TabIndex        =   9
         Top             =   1395
         Width           =   320
      End
      Begin VB.TextBox txtDescripcion 
         Height          =   315
         Left            =   1095
         MaxLength       =   255
         TabIndex        =   7
         Tag             =   "CARGA"
         Top             =   1020
         Width           =   8190
      End
      Begin VB.TextBox txtRutaArchivo 
         Height          =   310
         Left            =   1095
         MaxLength       =   255
         TabIndex        =   8
         Tag             =   "CARGA"
         Top             =   1380
         Width           =   5700
      End
      Begin VB.ComboBox cboNombreEnt 
         Height          =   315
         Left            =   3300
         TabIndex        =   1
         Tag             =   "CARGA"
         Top             =   315
         Width           =   3270
      End
      Begin VB.CommandButton cmdBusEnt 
         Height          =   315
         Left            =   6540
         MaskColor       =   &H00FFFFFF&
         Picture         =   "frmCarga.frx":34F0
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   300
         UseMaskColor    =   -1  'True
         Width           =   330
      End
      Begin VB.ComboBox cboRutEnt 
         Height          =   315
         Left            =   1095
         TabIndex        =   0
         Tag             =   "CARGA"
         Top             =   315
         Width           =   1440
      End
      Begin VB.ComboBox cboSucursal 
         Height          =   315
         Left            =   3300
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Tag             =   "CARGA"
         Top             =   660
         Width           =   3270
      End
      Begin VB.Label lblTipoFmto 
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   7875
         TabIndex        =   6
         Top             =   675
         Width           =   1410
      End
      Begin VB.Label lblCMarco 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1095
         TabIndex        =   4
         Top             =   675
         Width           =   1410
      End
      Begin VB.Label Label10 
         Caption         =   "Producto"
         Height          =   240
         Left            =   6915
         TabIndex        =   53
         Top             =   360
         Width           =   720
      End
      Begin VB.Label Label8 
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   8115
         TabIndex        =   10
         Tag             =   "CARGA"
         Top             =   1380
         Width           =   1170
      End
      Begin VB.Label Label7 
         Caption         =   "N� Remesa"
         Height          =   240
         Left            =   7200
         TabIndex        =   52
         Top             =   1440
         Width           =   900
      End
      Begin VB.Label Label6 
         Caption         =   "Observaci�n"
         Height          =   240
         Left            =   105
         TabIndex        =   30
         Top             =   1080
         Width           =   900
      End
      Begin VB.Label Label5 
         Caption         =   "Ruta Arch."
         Height          =   240
         Left            =   120
         TabIndex        =   29
         Top             =   1440
         Width           =   900
      End
      Begin VB.Label lblSeleccion 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Datos de la Carga"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   4
         Left            =   0
         TabIndex        =   27
         Top             =   30
         Width           =   9465
      End
      Begin VB.Label lblNombre 
         Caption         =   "Nombre"
         Height          =   240
         Index           =   0
         Left            =   2580
         TabIndex        =   26
         Top             =   330
         Width           =   615
      End
      Begin VB.Label lblRut 
         Caption         =   "Rut Cliente"
         Height          =   240
         Left            =   120
         TabIndex        =   25
         Top             =   330
         Width           =   840
      End
      Begin VB.Label Label1 
         Caption         =   "N� Contrato"
         Height          =   240
         Left            =   120
         TabIndex        =   24
         Top             =   720
         Width           =   840
      End
      Begin VB.Label Label2 
         Caption         =   "Sucursal"
         Height          =   240
         Left            =   2580
         TabIndex        =   23
         Top             =   720
         Width           =   840
      End
      Begin VB.Label Label4 
         Caption         =   "Tipo Formato"
         Height          =   240
         Left            =   6900
         TabIndex        =   14
         Top             =   720
         Width           =   960
      End
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   11
      Top             =   6915
      Width           =   9525
      _ExtentX        =   16801
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13697
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   22
      Top             =   0
      Width           =   9525
      _ExtentX        =   16801
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Agregar"
            Object.ToolTipText     =   "Agregar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Modificar"
            Object.ToolTipText     =   "Modificar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Eliminar"
            Object.ToolTipText     =   "Eliminar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Consultar"
            Object.ToolTipText     =   "Consultar"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Salir"
            Object.ToolTipText     =   "Salir"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "Volver"
            Object.ToolTipText     =   "Volver"
         EndProperty
      EndProperty
      Begin MSComctlLib.ImageList ilsIconos 
         Left            =   2220
         Top             =   60
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   8
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarga.frx":35F2
               Key             =   "Agregar"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarga.frx":3C6C
               Key             =   "Editar"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarga.frx":3D7E
               Key             =   "Borrar"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarga.frx":3E90
               Key             =   "Salir"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarga.frx":4602
               Key             =   "Grabar"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarga.frx":4714
               Key             =   "Cancelar"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarga.frx":4826
               Key             =   "Volver"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarga.frx":4D58
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "frmCarga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Carga_Archivo_Acc()

End Sub

Private Sub Carga_Archivo_Fijo()
    
''    Dim strArchivo As String
''
''
''    Dim lngCuentaCantDoctos As Long
''    Dim dblSumMtoTotDoctos As Double
''    Dim Reg As String
''    Dim NroRemesa As String
''    Dim NomArch As String
''    Dim NomArch2 As String
''    Dim NomArch3 As String
''    Dim FchaRemesa As Date
''    Dim CantDoctos As Long
''    Dim MtoDocumento As Double
''    Dim MtoTotDoctos As Double
''    Dim CodError As Long
''    Dim DesError As String
''    Dim SubTipo As Long
''    Dim cError As Long
''    Dim i As Long
''
''    On Error GoTo Errores
''
''    strArchivo = txtRutaArchivo.Text
''
''    lngCuentaCantDoctos = 0
''    dblSumMtoTotDoctos = 0
''
''
''    cError = 0
''
''    'Mensajes "Validando Formato Archivo Plano..."
''
''    Open strArchivo For Input As #1
''
''    i = 1
''    Do While Not EOF(1)
''        Line Input #1, Reg
''        If Mid(Reg, 1, 1) = 1 Then                              'Cabecera
''            NroRemesa = RTrim(Mid(Reg, 2, 10))
''            FchaRemesa = Format(DateSerial(Val(Mid(Reg, 16, 4)), Val(Mid(Reg, 14, 2)), Val(Mid(Reg, 12, 2))), "dd/mm/yyyy")
''            CantDoctos = Val(Mid(Reg, 20, 6))
''            MtoTotDoctos = Val(Mid(Reg, 26, 15))
''            'RegCabecera = Reg
''        ElseIf Mid(Reg, 1, 1) = 2 Then                      'Detalle
''            'CodError = ValidarFormato(Reg)
''            cError = cError + CodError
''            'DesError = IIf(CodError > 0, BuscaError(CodError, 18), "")
''            Rem Creando archivo plano con lineas de detalle mas errores de formato que se encuentren
''            Reg = Mid(Reg, 1, 406) & DesError & Space(194 - Len(DesError))
''            Print #2, Reg
''            MtoDocumento = Val(Mid(Reg, 287, 15))
''            'CuentaCantDoctos = CuentaCantDoctos + 1
''            'SumaMtoTotDoctos = SumaMtoTotDoctos + MtoDocumento
''        End If
''        i = i + 1
''    Loop
''    Close #2
''    Close #1
''
''    Rem Averiguando si existe error de cabecera
''    CodError = 0
''''    If ExisteRemesa(NroRemesa) Then
''''        CodError = 4
''''        SubTipo = 15
''''    ElseIf Len(RegCabecera) <> 600 Then
''''        CodError = 41
''''        SubTipo = 18
''''    ElseIf CuentaCantDoctos <> CantDoctos Then
''''        CodError = 1
''''        SubTipo = 15
''''    ElseIf SumaMtoTotDoctos <> MtoTotDoctos Then
''''        SubTipo = 15
''''        CodError = 2
''''    End If
''''
''    Open NomArch2 For Input As #2
''    Open NomArch3 For Output As #3
''
''    Rem Buscar descripcion de error de cabecera
''    DesError = IIf(CodError > 0, BuscaError(CodError, SubTipo), "")
''
''    Rem Creando nuevo archivo plano con lineas de cabecera y detalle mas error de formato si existiera
''    Reg = Mid(RegCabecera, 1, 40) & DesError & Space(560 - Len(DesError))
''    Print #3, Reg
''    While Not EOF(2)
''        Line Input #2, Reg
''        Print #3, Reg
''    Wend
''    Close #2
''    Close #3
''    If CodError <> 4 Then GrabarErrores NomArch3, NroRemesa
''
''    Rem Si no hay errores en el formato se graba remesa y ejecutan los PL
''    If CodError = 0 And cError = 0 Then
''       GrabaRemesa NomArch2, NomArch3, NroRemesa, FchaRemesa
''    Else
''        Rem Copia el archivo3 que contiene la remesa con los errores de formato al archivo feedback de salida
''        CopyFile NomArch3, RutaEnviado & Replace(UCase(Mid(gArch, InStrRev(gArch, "\") + 1, 17)), "RFC", "FFC"), 0
''    End If
''
''Salir:
''    Open NomArch1 & ".Fin" For Output As #3
''    Mensajes "Hora t�rmino proceso "
''    Close #1, #2, #3, #10
''    Kill NomArch2
''    Kill NomArch3
''    Exit Sub
''
''
''Errores:
''    Close #1, #2, #3, #10
''    Kill NomArch2
''    Kill NomArch3
''    Mensajes Err.Description


End Sub

'Public glngCodSucursal As Long

Private Sub Carga_Archivo_Var()

End Sub

Private Sub Carga_Archivo_Xls()

End Sub

Private Sub Carga_Cbo_Sucursal()
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command

    If cboRutEnt.Text = "" Or cboNombreEnt.Text = "" Then Exit Sub
    
   Screen.MousePointer = vbHourglass

   On Error GoTo Errores

    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "DBO_FACTOR.pkg_fme_entidades.sp_fme_sucursales_ver"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad", adNumeric, adParamInput, 10, cboRutEnt.ItemData(cboRutEnt.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute

    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Sucursales"
        GoTo Salir
    End If

    cboSucursal.Clear
    cboSucursal.AddItem ""
    cboSucursal.ItemData(cboSucursal.NewIndex) = 0

    Do While Not recRegistros.EOF
        'Nombre de Sucursal
        cboSucursal.AddItem "" & recRegistros.Fields("nom_sucursal").Value
        'Numero de Sucursal
        cboSucursal.ItemData(cboSucursal.NewIndex) = "" & recRegistros.Fields("nro_sucursal").Value
        
        recRegistros.MoveNext
    Loop
    cboSucursal.ListIndex = 0

    GoTo Salir

Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Sucursales"

Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal

End Sub


Private Sub cboNombreEnt_Click()
    cboRutEnt.ListIndex = cboNombreEnt.ListIndex
    
    If cboNombreEnt.ListIndex = -1 Then Exit Sub
    
    If cboNombreEnt.ItemData(cboNombreEnt.ListIndex) = 1 Then
        MsgBox "Cliente Bloqueado. No se pueden cargar Remesas", vbExclamation, "Bloqueo"
        cboNombreEnt.ListIndex = -1
        cboRutEnt.ListIndex = -1
    End If

    If cboRutEnt.ListIndex <> -1 And cboProducto.ListIndex <> -1 Then
        Dim vntCMarco() As Variant
        Call Trae_CMarco_Vigente(vntCMarco(), cboRutEnt.ItemData(cboRutEnt.ListIndex), cboProducto.ItemData(cboProducto.ListIndex))
        lblCMarco.Caption = vntCMarco(0)
        txtRutaArchivo.Text = vntCMarco(1)
    End If

    cboSucursal.Clear
End Sub

Private Sub Trae_Estructura()
    Dim blnExisteEstructura As Boolean
    
    Call Carga_Estructura(cboRutEnt.ItemData(cboRutEnt.ListIndex), _
                                           cboProducto.ItemData(cboProducto.ListIndex), _
                                           cboSucursal.ItemData(cboSucursal.ListIndex), _
                                           blnExisteEstructura)
    
    If blnExisteEstructura Then
        lblTipoFmto.Caption = garrEstrucArchClie(0, 17)
        
        Call EstableceEstructuraGrilla(grdEncab, grdDetalle, grdTotal)
       ' Call TraeArchivoCliente(grdEncab, grdDetalle, grdTotal, lngIdeRemesa, lngIdeContrato)
    Else
        MsgBox "Estructura del cliente no encontrada", vbInformation, Me.Caption
        GoTo LabelSalir
    End If
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
    
End Sub


Private Sub cboNombreEnt_GotFocus()
    cboNombreEnt.Text = ""
    cboRutEnt.ListIndex = -1
    cboProducto.ListIndex = 0
End Sub

Private Sub cboNombreEnt_KeyPress(KeyAscii As Integer)
    cboRutEnt.Clear
End Sub

Private Sub cboProducto_Click()
    If cboRutEnt.ListIndex <> -1 Then
        Dim vntCMarco() As Variant
        Call Trae_CMarco_Vigente(vntCMarco(), cboRutEnt.ItemData(cboRutEnt.ListIndex), cboProducto.ItemData(cboProducto.ListIndex))
        lblCMarco.Caption = vntCMarco(0)
        txtRutaArchivo.Text = vntCMarco(1)
    End If

    If cboRutEnt.ListIndex >= 0 And cboProducto.ListIndex >= 0 And cboSucursal.ListIndex >= 0 Then
        Trae_Estructura
    End If

End Sub


Private Sub cboRutEnt_Click()
    
    cboNombreEnt.ListIndex = cboRutEnt.ListIndex
    
    If cboRutEnt.ListIndex = -1 Then Exit Sub
    
    If cboNombreEnt.ItemData(cboNombreEnt.ListIndex) = 1 Then
        MsgBox "Cliente Bloqueado. No se pueden cargar Remesas", vbExclamation, "Bloqueo"
        cboNombreEnt.ListIndex = -1
        cboRutEnt.ListIndex = -1
    End If

    If cboRutEnt.ListIndex <> -1 And cboProducto.ListIndex <> -1 Then
        Dim vntCMarco() As Variant
        Call Trae_CMarco_Vigente(vntCMarco(), cboRutEnt.ItemData(cboRutEnt.ListIndex), cboProducto.ItemData(cboProducto.ListIndex))
        lblCMarco.Caption = vntCMarco(0)
        txtRutaArchivo.Text = vntCMarco(1)
    End If

    cboSucursal.Clear
    
End Sub

Private Sub cboRutEnt_GotFocus()
    cboRutEnt.Text = ""
    cboNombreEnt.ListIndex = -1
    lblCMarco.Caption = ""
End Sub

Sub cboRutEnt_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cmdBusEnt_Click
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
        KeyAscii = 0
    Else
        cboNombreEnt.Clear
        If cboRutEnt.ListCount > 0 Then cboRutEnt.Clear
    End If
End Sub

Private Sub cboSucursal_Click()
    If cboRutEnt.ListIndex >= 0 And cboProducto.ListIndex >= 0 And cboSucursal.ListIndex >= 0 Then
        Trae_Estructura
    End If
End Sub

Private Sub cboSucursal_GotFocus()
    'Carga_Cbo_Sucursal
    If cboRutEnt.ListIndex >= 0 Then
        Carga_Combo_Sucursales cboSucursal, cboRutEnt.ItemData(cboRutEnt.ListIndex)
    End If
End Sub


Private Sub cmdBusEnt_Click()
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim mlngRut As Long
    Dim mlngProducto As Long
    Dim mstrNombre As String

    On Error GoTo LabelError

    If cboRutEnt.Text = Empty And cboNombreEnt.Text = Empty Then GoTo LabelSalir
    Screen.MousePointer = vbHourglass
    
    mlngRut = Val(cboRutEnt.Text)
    mstrNombre = (cboNombreEnt.Text)
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "DBO_FACTOR.pkg_fme_entidades.sp_fme_clientes_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_rut", adNumeric, adParamInput, 10, mlngRut)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 80, mstrNombre)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_filtro_esp", adVarChar, adParamInput, 15, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_tipo_filtro", adVarChar, adParamInput, 1, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Clientes"
        GoTo LabelSalir
    End If
    
    cboRutEnt.Clear
    cboNombreEnt.Clear
    
    Do While Not recRegistros.EOF
        cboRutEnt.AddItem RTrim("" & recRegistros.Fields("rut_entidad").Value) & "-" & recRegistros.Fields("ide_dig_rut").Value
        cboRutEnt.ItemData(cboRutEnt.NewIndex) = recRegistros.Fields("ide_entidad").Value
        
        cboNombreEnt.AddItem "" & recRegistros.Fields("nomcliente").Value
        cboNombreEnt.ItemData(cboNombreEnt.NewIndex) = recRegistros.Fields("mrc_bloqueo").Value
        
        recRegistros.MoveNext
    Loop
    If cboRutEnt.ListCount > 0 Then cboRutEnt.ListIndex = 0
    If cboNombreEnt.ListCount > 0 Then cboNombreEnt.ListIndex = 0
    
    Rem Exito
    If cboRutEnt.ListIndex = -1 Then
        mlngRut = 0
    Else
        mlngRut = cboRutEnt.ItemData(cboRutEnt.ListIndex)
    End If
    
    If cboProducto.ListIndex = -1 Then
        mlngProducto = 0
    Else
        mlngProducto = cboProducto.ItemData(cboProducto.ListIndex)
    End If
    
    If cboRutEnt.ListIndex <> -1 And cboProducto.ListIndex <> -1 Then
        Dim vntCMarco() As Variant
        Call Trae_CMarco_Vigente(vntCMarco(), cboRutEnt.ItemData(cboRutEnt.ListIndex), cboProducto.ItemData(cboProducto.ListIndex))
        lblCMarco.Caption = vntCMarco(0)
        txtRutaArchivo.Text = vntCMarco(1)
    End If
    
LabelSalir:
    Screen.MousePointer = 0
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub


Private Sub cmdCargar_Click()
    
    Select Case garrEstrucArchClie(0, 18)
        
        Case "FIJO"
            If LCase(Right(txtRutaArchivo.Text, 4)) <> ".txt" Then
                MsgBox "Debe Seleccionar un Archivo a cargar", vbExclamation, "Error"
            Else
                Carga_Archivo_Fijo
            End If
        
        Case "VAR"
            If LCase(Right(txtRutaArchivo.Text, 4)) <> ".txt" Then
                MsgBox "Debe Seleccionar un Archivo a cargar", vbExclamation, "Error"
            Else
                Carga_Archivo_Var
            End If
        
        Case "XLS"
            If LCase(Right(txtRutaArchivo.Text, 4)) <> ".xls" Then
                MsgBox "Debe Seleccionar un Archivo a cargar", vbExclamation, "Error"
            Else
                Carga_Archivo_Xls
            End If
        
        Case "ACC"
            If LCase(Right(txtRutaArchivo.Text, 4)) <> ".mdb" Then
                MsgBox "Debe Seleccionar un Archivo a cargar", vbExclamation, "Error"
            Else
                Carga_Archivo_Acc
            End If
    
    End Select

End Sub
Private Sub cmdOrigen_Click()
    Dim strMsgT   As String
    Dim blnTodoOk As Boolean
    
    On Error GoTo LabelError

    Rem Seleccionar Directorios
    dlgRuta.CancelError = True
    
    On Error GoTo LabelError1
    
    strMsgT = "| Todos los archivos (*.*)|*.*"
    
    Select Case garrEstrucArchClie(0, 18)
        Case "FIJO"
            dlgRuta.Filter = "Archivos de texto (*.txt)|*.txt" & strMsgT
            dlgRuta.DialogTitle = "Abrir base de datos de Texto Fijo"
        
        Case "VAR"
            dlgRuta.Filter = "Archivos de texto (*.txt)|*.txt" & strMsgT
            dlgRuta.DialogTitle = "Abrir base de datos de Texto Variable"
        
        Case "XLS"
            dlgRuta.Filter = "Archivos de Excel (*.xls)|*.xls" & strMsgT
            dlgRuta.DialogTitle = "Abrir archivo de Excel"
        
        Case "ACC"
            dlgRuta.Filter = "MDB de Microsoft Acces (*.mdb)|*.mdb" & strMsgT
            dlgRuta.DialogTitle = "Abri base de datos de Microsoft Acces"
        
    End Select
    
'    Select Case mstrTipoDB
'        Case gstrMSACCESS
'            dlg.Filter = "MDB de Microsoft Acces (*.mdb)|*.mdb" & strMsgT
'            dlg.DialogTitle = "Abri base de datos de Microsoft Acces"
'        Case gstrDBASEIII, gstrDBASEIV, gstrDBASE5
'            dlg.Filter = "Bases de datos de Dbase (*.dbf)|*.dbf" & strMsgT
'            dlg.DialogTitle = "Abrir base de datos de DBase"
'        Case gstrFOXPRO20, gstrFOXPRO25, gstrFOXPRO26, gstrFOXPRO30
'            dlg.Filter = "Bases de datos de FoxPro (*.dbf)|*.dbf" & strMsgT
'            dlg.DialogTitle = "Abrir base de datos de FoxPro"
'        Case gstrPARADOX3X, gstrPARADOX4X, gstrPARADOX5X
'            dlg.Filter = "Bases de datos de Paradox (*.db)|*.db" & strMsgT
'            dlg.DialogTitle = "Abrir base de datos de Paradox"
'        Case gstrEXCEL50
'            dlg.Filter = "Archivos de Excel (*.xls)|*.xls" & strMsgT
'            dlg.DialogTitle = "Abrir archivo de Excel"
'        Case gstrBTRIEVE
'            dlg.Filter = "Bases de datos de Btrieve (FILE.DDF)|FILE.DDF" & strMsgT
'            dlg.DialogTitle = "Abrir base de datos de BTrieve"
'        Case gstrTEXTFILES
'            dlg.Filter = "Archivos de texto (*.txt)|*.txt" & strMsgT
'            dlg.DialogTitle = "Abrir base de datos de Texto"
'    End Select
    
    dlgRuta.FLAGS = cdlOFNHideReadOnly + cdlOFNLongNames + cdlOFNExplorer + cdlOFNFileMustExist + cdlOFNNoChangeDir
    dlgRuta.InitDir = txtRutaArchivo.Text
    
    
    dlgRuta.ShowOpen
    
    txtRutaArchivo.Text = dlgRuta.FileName
    txtRutaArchivo.Tag = dlgRuta.FileName
    
    'call Inicializar
    Rem -------------------------------------------------------------------------
    Rem Aqu� se carga el archivo de texto formato d.o.s.
    Rem Cuando el archivo no viene en texto formato d.o.s, se realiza lo sgte:
    Rem     si es excel, se convierte a texto formato d.o.s.
    Rem     si es texto, pero con formato unix se convierte a texto formato d.o.s.
    Rem --------------------------------------------------------------------------
    'call cargarArchivo
    
    Exit Sub
LabelError1:
    Rem El usuario Presiono Cancelar
    txtRutaArchivo.SetFocus
    staEstado.Panels.Item(1).Text = "Listo"
    Screen.MousePointer = 0
    Exit Sub

LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1).Text = "Listo"
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, , "Error"
        Err.Clear
        GoTo LabelSalir
    End If
End Sub

Private Sub cmdValidar_Click()

    Dim i As Long
    
    For i = 0 To UBound(garrEstrucArchClie)
    
Debug.Print "(" & i & " ,0) = " & "NOM_CORTO        = " & garrEstrucArchClie(i, 0)
Debug.Print "(" & i & " ,1) = " & "COD_IDE_CAMPO    = " & garrEstrucArchClie(i, 1)
Debug.Print "(" & i & " ,2) = " & "GLS_DESCRIPCION  = " & garrEstrucArchClie(i, 2)
Debug.Print "(" & i & " ,3) = " & "NRO_CORR_DETALLE = " & garrEstrucArchClie(i, 3)
Debug.Print "(" & i & " ,4) = " & "NRO_CORR_CAMPO   = " & garrEstrucArchClie(i, 4)
Debug.Print "(" & i & " ,5) = " & "GLS_CAMPO        = " & garrEstrucArchClie(i, 5)
Debug.Print "(" & i & " ,6) = " & "COD_TIPO_CAMPO   = " & garrEstrucArchClie(i, 6)
Debug.Print "(" & i & " ,7) = " & "VLR_POSICION     = " & garrEstrucArchClie(i, 7)
Debug.Print "(" & i & " ,8) = " & "VLR_PRECISION    = " & garrEstrucArchClie(i, 8)
Debug.Print "(" & i & " ,9) = " & "VLR_DECIMALES    = " & garrEstrucArchClie(i, 9)
Debug.Print "(" & i & ",10) = " & "MRC_ALINEACION   = " & garrEstrucArchClie(i, 10)
Debug.Print "(" & i & ",11) = " & "MRC_RELLENO      = " & garrEstrucArchClie(i, 11)
Debug.Print "(" & i & ",12) = " & "FLG_CONVERTIR    = " & garrEstrucArchClie(i, 12)
Debug.Print "(" & i & ",13) = " & "GLS_FORMATO      = " & garrEstrucArchClie(i, 13)
Debug.Print "(" & i & ",14) = " & "COD_ESTADO       = " & garrEstrucArchClie(i, 14)
Debug.Print "(" & i & ",15) = " & "MRC_BLOQUEO      = " & garrEstrucArchClie(i, 15)
Debug.Print "(" & i & ",16) = " & "FLG_OBLIGATORIO  = " & garrEstrucArchClie(i, 16)
Debug.Print "(" & i & ",17) = " & "Tipo_Formato     = " & garrEstrucArchClie(i, 17)
Debug.Print "(" & i & ",18) = " & "nom_cto_Tip_Fmto = " & garrEstrucArchClie(i, 18)
Debug.Print "(" & i & ",19) = " & "Separador        = " & garrEstrucArchClie(i, 19)
Debug.Print ""
    
    
    Next
    
    



End Sub

Private Sub cmdVolver_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
        Call Carga_Combo_Productos(cboProducto)

    
End Sub

