VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmDigitacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Digitacion Archivo Cliente"
   ClientHeight    =   7260
   ClientLeft      =   4230
   ClientTop       =   2220
   ClientWidth     =   9480
   Icon            =   "frmDigitacion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   9480
   Begin VB.Frame Frame1 
      Height          =   1290
      Left            =   60
      TabIndex        =   4
      Top             =   420
      Width           =   9375
      Begin VB.CheckBox chkDigita 
         Caption         =   "Digitando"
         Height          =   195
         Left            =   6000
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   1010
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "Ln"
         Height          =   240
         Left            =   8220
         TabIndex        =   30
         Top             =   330
         Width           =   240
      End
      Begin VB.Label lblNumRemesa 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   7080
         TabIndex        =   29
         Top             =   315
         Width           =   1095
      End
      Begin VB.Label lblNumLinea 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   8460
         TabIndex        =   28
         Tag             =   "DIG"
         Top             =   315
         Width           =   855
      End
      Begin VB.Label lblNombreCliente 
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   3240
         TabIndex        =   27
         Top             =   315
         Width           =   2730
      End
      Begin VB.Label Label10 
         Caption         =   "Estado"
         Height          =   240
         Left            =   120
         TabIndex        =   26
         Top             =   1010
         Width           =   720
      End
      Begin VB.Label Label11 
         Caption         =   "Origen"
         Height          =   240
         Left            =   4380
         TabIndex        =   25
         Top             =   1010
         Width           =   540
      End
      Begin VB.Label Label12 
         Caption         =   "Producto"
         Height          =   240
         Left            =   120
         TabIndex        =   24
         Top             =   660
         Width           =   720
      End
      Begin VB.Label Label13 
         Caption         =   "Fecha Recep"
         Height          =   240
         Left            =   7140
         TabIndex        =   23
         Top             =   660
         Width           =   1020
      End
      Begin VB.Label Label15 
         Caption         =   "Formato"
         Height          =   240
         Left            =   2580
         TabIndex        =   22
         Top             =   1010
         Width           =   600
      End
      Begin VB.Label Label16 
         Caption         =   "Tipo Archivo"
         Height          =   240
         Left            =   7140
         TabIndex        =   21
         Top             =   1010
         Width           =   900
      End
      Begin VB.Label Label17 
         Caption         =   "Sucursal"
         Height          =   240
         Left            =   2580
         TabIndex        =   20
         Top             =   660
         Width           =   660
      End
      Begin VB.Label Label18 
         Caption         =   "N� Contrato"
         Height          =   240
         Left            =   5100
         TabIndex        =   19
         Top             =   660
         Width           =   840
      End
      Begin VB.Label Label19 
         Caption         =   "Rut Cliente"
         Height          =   240
         Left            =   120
         TabIndex        =   18
         Top             =   330
         Width           =   840
      End
      Begin VB.Label lblRutCliente 
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   1140
         TabIndex        =   17
         Top             =   315
         Width           =   1395
      End
      Begin VB.Label lblSeleccion 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Archivo Seleccionado"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   0
         Left            =   0
         TabIndex        =   16
         Top             =   0
         Width           =   9405
      End
      Begin VB.Label lblNumContrato 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   5940
         TabIndex        =   15
         Top             =   610
         Width           =   1155
      End
      Begin VB.Label lblSucursal 
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   3240
         TabIndex        =   14
         Top             =   610
         Width           =   1830
      End
      Begin VB.Label lblProducto 
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   1140
         TabIndex        =   13
         Top             =   610
         Width           =   1395
      End
      Begin VB.Label lblTipoArchivo 
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   8160
         TabIndex        =   12
         Top             =   920
         Width           =   1155
      End
      Begin VB.Label lblFormato 
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   3240
         TabIndex        =   11
         Top             =   920
         Width           =   1095
      End
      Begin VB.Label lblFechaRecep 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   8160
         TabIndex        =   10
         Top             =   610
         Width           =   1155
      End
      Begin VB.Label lbEstado 
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   1140
         TabIndex        =   9
         Top             =   920
         Width           =   1395
      End
      Begin VB.Label lblOrigen 
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   4920
         TabIndex        =   8
         Top             =   920
         Width           =   1050
      End
      Begin VB.Label Label29 
         Caption         =   "N� Remesa"
         Height          =   240
         Left            =   6060
         TabIndex        =   7
         Top             =   330
         Width           =   840
      End
      Begin VB.Label Label14 
         Caption         =   "Nombre"
         Height          =   240
         Left            =   2580
         TabIndex        =   6
         Top             =   330
         Width           =   600
      End
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   0
      Top             =   6990
      Width           =   9480
      _ExtentX        =   16722
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13618
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraDigitacion 
      Height          =   4335
      Left            =   60
      TabIndex        =   31
      Top             =   1920
      Width           =   9435
      Begin VB.VScrollBar vsbDigitacion 
         Height          =   4260
         Left            =   9180
         TabIndex        =   103
         Top             =   60
         Width           =   255
      End
      Begin VB.PictureBox picDigitacion 
         Height          =   5235
         Left            =   0
         ScaleHeight     =   5175
         ScaleWidth      =   9315
         TabIndex        =   32
         Top             =   60
         Width           =   9375
         Begin VB.TextBox MRC_OPERATORIA 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   7080
            Locked          =   -1  'True
            MaxLength       =   24
            TabIndex        =   105
            Tag             =   "DIG"
            Top             =   360
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Option1"
            Height          =   795
            Left            =   3960
            TabIndex        =   65
            Top             =   5520
            Width           =   3735
         End
         Begin VB.ComboBox COD_TIPO_DCTO 
            Height          =   315
            Left            =   570
            Style           =   2  'Dropdown List
            TabIndex        =   64
            Tag             =   "DIG"
            Top             =   3600
            Visible         =   0   'False
            Width           =   2370
         End
         Begin VB.TextBox NOM_EMISOR 
            Height          =   285
            Left            =   570
            MaxLength       =   80
            TabIndex        =   63
            Tag             =   "DIG"
            Top             =   300
            Visible         =   0   'False
            Width           =   3075
         End
         Begin VB.TextBox RUT_EMISOR 
            BackColor       =   &H8000000E&
            Height          =   285
            Left            =   570
            MaxLength       =   10
            TabIndex        =   62
            Tag             =   "DIG"
            Top             =   15
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.TextBox IDE_DV_EMISOR 
            Height          =   285
            Left            =   1740
            MaxLength       =   1
            TabIndex        =   61
            Tag             =   "DIG"
            Top             =   0
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox RUT_RECEPTOR 
            Height          =   285
            Left            =   570
            MaxLength       =   10
            TabIndex        =   60
            Tag             =   "DIG"
            Top             =   615
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.TextBox NOM_RECEPTOR 
            Height          =   285
            Left            =   570
            MaxLength       =   80
            TabIndex        =   59
            Tag             =   "DIG"
            Top             =   900
            Visible         =   0   'False
            Width           =   3075
         End
         Begin VB.TextBox IDE_DV_RECEPTOR 
            Height          =   285
            Left            =   1740
            MaxLength       =   1
            TabIndex        =   58
            Tag             =   "DIG"
            Top             =   600
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox GLS_DIRECCION 
            Height          =   285
            Left            =   570
            MaxLength       =   120
            TabIndex        =   57
            Tag             =   "DIG"
            Top             =   1200
            Visible         =   0   'False
            Width           =   3075
         End
         Begin VB.TextBox GLS_TELEFONO 
            Height          =   285
            Left            =   570
            MaxLength       =   20
            TabIndex        =   56
            Tag             =   "DIG"
            Top             =   2700
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.TextBox GLS_NUM_FAX 
            Height          =   285
            Left            =   570
            MaxLength       =   20
            TabIndex        =   55
            Tag             =   "DIG"
            Top             =   3000
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.TextBox GLS_CASILLA 
            Height          =   285
            Left            =   570
            MaxLength       =   40
            TabIndex        =   54
            Tag             =   "DIG"
            Top             =   3300
            Visible         =   0   'False
            Width           =   2355
         End
         Begin VB.ComboBox GLS_COMUNA 
            Height          =   315
            Left            =   570
            Style           =   2  'Dropdown List
            TabIndex        =   53
            Tag             =   "DIG"
            Top             =   1500
            Visible         =   0   'False
            Width           =   2370
         End
         Begin VB.ComboBox GLS_CIUDAD 
            Height          =   315
            Left            =   570
            Style           =   2  'Dropdown List
            TabIndex        =   52
            Tag             =   "DIG"
            Top             =   1800
            Visible         =   0   'False
            Width           =   2370
         End
         Begin VB.ComboBox COD_PAIS 
            Height          =   315
            Left            =   570
            Style           =   2  'Dropdown List
            TabIndex        =   51
            Tag             =   "DIG"
            Top             =   2100
            Visible         =   0   'False
            Width           =   2370
         End
         Begin VB.TextBox NRO_DOCUMENTO 
            BackColor       =   &H8000000E&
            Height          =   285
            Left            =   570
            MaxLength       =   20
            TabIndex        =   50
            Tag             =   "DIG"
            Top             =   3900
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.TextBox NRO_CUOTA 
            BackColor       =   &H8000000E&
            Height          =   285
            Left            =   4200
            MaxLength       =   3
            TabIndex        =   49
            Tag             =   "DIG"
            Top             =   0
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.ComboBox COD_BANCO 
            Height          =   315
            Left            =   4200
            Style           =   2  'Dropdown List
            TabIndex        =   48
            Tag             =   "DIG"
            Top             =   300
            Visible         =   0   'False
            Width           =   2370
         End
         Begin VB.ComboBox COD_PLAZA 
            Height          =   315
            Left            =   4200
            Style           =   2  'Dropdown List
            TabIndex        =   47
            Tag             =   "DIG"
            Top             =   600
            Visible         =   0   'False
            Width           =   2370
         End
         Begin VB.TextBox GLS_CTACTE 
            BackColor       =   &H8000000E&
            Height          =   285
            Left            =   4200
            MaxLength       =   12
            TabIndex        =   46
            Tag             =   "DIG"
            Top             =   900
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.ComboBox COD_FORMA_PAGO 
            Height          =   315
            Left            =   4200
            Style           =   2  'Dropdown List
            TabIndex        =   45
            Tag             =   "DIG"
            Top             =   1200
            Visible         =   0   'False
            Width           =   2370
         End
         Begin VB.TextBox MNT_DOCUMENTO 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            Height          =   285
            Left            =   4200
            MaxLength       =   24
            TabIndex        =   44
            Tag             =   "DIG"
            Top             =   1500
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.ComboBox COD_MONEDA 
            Height          =   315
            Left            =   4200
            Style           =   2  'Dropdown List
            TabIndex        =   43
            Tag             =   "DIG"
            Top             =   1800
            Visible         =   0   'False
            Width           =   2370
         End
         Begin VB.TextBox GLS_OBSERVACION 
            Height          =   285
            Left            =   4200
            MaxLength       =   255
            TabIndex        =   42
            Tag             =   "DIG"
            Top             =   3000
            Visible         =   0   'False
            Width           =   3075
         End
         Begin VB.TextBox GLS_REFERENCIA_1 
            Height          =   285
            Left            =   4200
            MaxLength       =   80
            TabIndex        =   41
            Tag             =   "DIG"
            Top             =   3300
            Visible         =   0   'False
            Width           =   3075
         End
         Begin VB.TextBox GLS_REFERENCIA_2 
            Height          =   285
            Left            =   4200
            MaxLength       =   80
            TabIndex        =   40
            Tag             =   "DIG"
            Top             =   3600
            Visible         =   0   'False
            Width           =   3075
         End
         Begin VB.TextBox GLS_REFERENCIA_3 
            Height          =   285
            Left            =   4200
            MaxLength       =   80
            TabIndex        =   39
            Tag             =   "DIG"
            Top             =   3900
            Visible         =   0   'False
            Width           =   3075
         End
         Begin VB.ComboBox COD_CLASIF_EMISOR 
            Height          =   315
            Left            =   7080
            Style           =   2  'Dropdown List
            TabIndex        =   38
            Tag             =   "DIG"
            Top             =   0
            Visible         =   0   'False
            Width           =   2370
         End
         Begin VB.TextBox MNT_ORIGINAL 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            Height          =   285
            Left            =   7080
            MaxLength       =   24
            TabIndex        =   37
            Tag             =   "DIG"
            Top             =   660
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.TextBox MNT_IVA 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            Height          =   285
            Left            =   7080
            MaxLength       =   24
            TabIndex        =   36
            Tag             =   "DIG"
            Top             =   960
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.TextBox MNT_NETO 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            Height          =   285
            Left            =   7080
            MaxLength       =   24
            TabIndex        =   35
            Tag             =   "DIG"
            Top             =   1260
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.TextBox MNT_EXENTO 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            Height          =   285
            Left            =   7080
            MaxLength       =   24
            TabIndex        =   34
            Tag             =   "DIG"
            Top             =   1560
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.TextBox GLS_COD_POSTAL 
            Height          =   285
            Left            =   570
            MaxLength       =   20
            TabIndex        =   33
            Tag             =   "DIG"
            Top             =   2400
            Visible         =   0   'False
            Width           =   1215
         End
         Begin MSComCtl2.DTPicker FCH_EMISION 
            Height          =   285
            Left            =   4200
            TabIndex        =   66
            Tag             =   "DIG"
            Top             =   2115
            Visible         =   0   'False
            Width           =   1275
            _ExtentX        =   2249
            _ExtentY        =   503
            _Version        =   393216
            Format          =   58851329
            CurrentDate     =   38727
         End
         Begin MSComCtl2.DTPicker FCH_VENCIMIENTO 
            Height          =   285
            Left            =   4200
            TabIndex        =   67
            Tag             =   "DIG"
            Top             =   2415
            Visible         =   0   'False
            Width           =   1275
            _ExtentX        =   2249
            _ExtentY        =   503
            _Version        =   393216
            Format          =   58851329
            CurrentDate     =   38727
         End
         Begin MSComCtl2.DTPicker FCH_PAGO 
            Height          =   285
            Left            =   4200
            TabIndex        =   68
            Tag             =   "DIG"
            Top             =   2700
            Visible         =   0   'False
            Width           =   1275
            _ExtentX        =   2249
            _ExtentY        =   503
            _Version        =   393216
            Format          =   58851329
            CurrentDate     =   38727
         End
         Begin VB.Label RUT_EMISOR2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   102
            Tag             =   "DIG"
            Top             =   60
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label NOM_EMISOR2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   101
            Tag             =   "DIG"
            Top             =   360
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label RUT_RECEPTOR2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   100
            Tag             =   "DIG"
            Top             =   660
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label NOM_RECEPTOR2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   99
            Tag             =   "DIG"
            Top             =   960
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_DIRECCION2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   98
            Tag             =   "DIG"
            Top             =   1260
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_COMUNA2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   97
            Tag             =   "DIG"
            Top             =   1560
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_CIUDAD2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   96
            Tag             =   "DIG"
            Top             =   1860
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label COD_PAIS2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   95
            Tag             =   "DIG"
            Top             =   2160
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_COD_POSTAL2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   94
            Tag             =   "DIG"
            Top             =   2460
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_TELEFONO2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   93
            Tag             =   "DIG"
            Top             =   2760
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_NUM_FAX2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   92
            Tag             =   "DIG"
            Top             =   3060
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_CASILLA2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   91
            Tag             =   "DIG"
            Top             =   3360
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label COD_TIPO_DCTO2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   90
            Tag             =   "DIG"
            Top             =   3660
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_OBSERVACION2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   89
            Tag             =   "DIG"
            Top             =   3060
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label FCH_PAGO2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   88
            Tag             =   "DIG"
            Top             =   2760
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label FCH_VENCIMIENTO2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   87
            Tag             =   "DIG"
            Top             =   2460
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label FCH_EMISION2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   86
            Tag             =   "DIG"
            Top             =   2160
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label COD_MONEDA2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   85
            Tag             =   "DIG"
            Top             =   1860
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label MNT_DOCUMENTO2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   84
            Tag             =   "DIG"
            Top             =   1560
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label COD_FORMA_PAGO2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   83
            Tag             =   "DIG"
            Top             =   1260
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_CTACTE2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   82
            Tag             =   "DIG"
            Top             =   960
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label COD_PLAZA2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   81
            Tag             =   "DIG"
            Top             =   660
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label COD_BANCO2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   80
            Tag             =   "DIG"
            Top             =   360
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label NRO_CUOTA2 
            Caption         =   "Campo"
            Height          =   225
            Left            =   3660
            TabIndex        =   79
            Tag             =   "DIG"
            Top             =   60
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label NRO_DOCUMENTO2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   0
            TabIndex        =   78
            Tag             =   "DIG"
            Top             =   3960
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_REFERENCIA_12 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   77
            Tag             =   "DIG"
            Top             =   3360
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_REFERENCIA_22 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   76
            Tag             =   "DIG"
            Top             =   3660
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label GLS_REFERENCIA_32 
            Caption         =   "Campo"
            Height          =   220
            Left            =   3660
            TabIndex        =   75
            Tag             =   "DIG"
            Top             =   3960
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label COD_CLASIF_EMISOR2 
            Caption         =   "Campo"
            Height          =   220
            Left            =   6540
            TabIndex        =   74
            Tag             =   "DIG"
            Top             =   60
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label MRC_OPERATORIA2 
            Caption         =   "Campo"
            Height          =   225
            Left            =   6540
            TabIndex        =   73
            Tag             =   "DIG"
            Top             =   420
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label MNT_ORIGINAL2 
            Caption         =   "Campo"
            Height          =   225
            Left            =   6540
            TabIndex        =   72
            Tag             =   "DIG"
            Top             =   720
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label MNT_IVA2 
            Caption         =   "Campo"
            Height          =   225
            Left            =   6540
            TabIndex        =   71
            Tag             =   "DIG"
            Top             =   1020
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label MNT_NETO2 
            Caption         =   "Campo"
            Height          =   225
            Left            =   6540
            TabIndex        =   70
            Tag             =   "DIG"
            Top             =   1320
            Visible         =   0   'False
            Width           =   1860
         End
         Begin VB.Label MNT_EXENTO2 
            Caption         =   "Campo"
            Height          =   225
            Left            =   6540
            TabIndex        =   69
            Tag             =   "DIG"
            Top             =   1620
            Visible         =   0   'False
            Width           =   1860
         End
      End
   End
   Begin VB.Frame fraBotones 
      Height          =   735
      Index           =   4
      Left            =   60
      TabIndex        =   1
      Top             =   6240
      Width           =   9390
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   7875
         TabIndex        =   3
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdGrabar 
         Caption         =   "&Grabar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   100
         TabIndex        =   2
         Top             =   210
         Width           =   1545
      End
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   106
      Top             =   0
      Width           =   9480
      _ExtentX        =   16722
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Grabar"
            Object.ToolTipText     =   "Grabar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Volver"
            Object.ToolTipText     =   "Volver"
         EndProperty
      EndProperty
      Begin MSComctlLib.ImageList ilsIconos 
         Left            =   1800
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
   Begin VB.Label lblSeleccion 
      Alignment       =   2  'Center
      BackColor       =   &H00A26D00&
      Caption         =   "�rea de Digitaci�n"
      ForeColor       =   &H00E0E0E0&
      Height          =   240
      Index           =   1
      Left            =   60
      TabIndex        =   104
      Top             =   1680
      Width           =   9360
   End
End
Attribute VB_Name = "frmDigitacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAgregarLinea_Click()

End Sub

Private Sub cmdGrabar_Click()
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    Rem Validaciones
    If RUT_EMISOR.Visible And RUT_EMISOR.Tag = "OBLIGATORIO" And Trim(RUT_EMISOR.Text) = "" Then
        MsgBox "Debe ingresar " & RUT_EMISOR2.Caption, vbExclamation, Me.Caption
        RUT_EMISOR.SetFocus
        GoTo LabelSalir
    End If
    If IDE_DV_EMISOR.Visible And IDE_DV_EMISOR.Tag = "OBLIGATORIO" And Trim(IDE_DV_EMISOR.Text) = "" Then
        MsgBox "Debe ingresar Dv " & RUT_EMISOR2.Caption, vbExclamation, Me.Caption
        IDE_DV_EMISOR.SetFocus
        GoTo LabelSalir
    End If
    Rem Valida Rut
    If RUT_EMISOR.Visible And RUT_EMISOR.Tag = "OBLIGATORIO" And Trim(RUT_EMISOR.Text) <> "" Then
        If IDE_DV_EMISOR.Visible And IDE_DV_EMISOR.Tag = "OBLIGATORIO" And Trim(IDE_DV_EMISOR.Text) <> "" Then
            If TraeDigVerificador(RUT_EMISOR.Text) <> Trim(IDE_DV_EMISOR.Text) Then
                MsgBox "Dv ingresado no corresponde al " & RUT_EMISOR2.Caption, vbExclamation, Me.Caption
                IDE_DV_EMISOR.SetFocus
                GoTo LabelSalir
            End If
        End If
    End If
    If NOM_EMISOR.Visible And NOM_EMISOR.Tag = "OBLIGATORIO" And Trim(NOM_EMISOR.Text) = "" Then
        MsgBox "Debe ingresar " & NOM_EMISOR2.Caption, vbExclamation, Me.Caption
        NOM_EMISOR.SetFocus
        GoTo LabelSalir
    End If
    If RUT_RECEPTOR.Visible And RUT_RECEPTOR.Tag = "OBLIGATORIO" And Trim(RUT_RECEPTOR.Text) = "" Then
        MsgBox "Debe ingresar " & RUT_RECEPTOR2.Caption, vbExclamation, Me.Caption
        RUT_RECEPTOR.SetFocus
        GoTo LabelSalir
    End If
    If IDE_DV_RECEPTOR.Visible And IDE_DV_RECEPTOR.Tag = "OBLIGATORIO" And Trim(IDE_DV_RECEPTOR.Text) = "" Then
        MsgBox "Debe ingresar " & RUT_RECEPTOR2.Caption, vbExclamation, Me.Caption
        IDE_DV_RECEPTOR.SetFocus
        GoTo LabelSalir
    End If
    Rem Valida Rut
    If RUT_RECEPTOR.Visible And RUT_RECEPTOR.Tag = "OBLIGATORIO" And Trim(RUT_RECEPTOR.Text) <> "" Then
        If IDE_DV_RECEPTOR.Visible And IDE_DV_RECEPTOR.Tag = "OBLIGATORIO" And Trim(IDE_DV_RECEPTOR.Text) <> "" Then
            If TraeDigVerificador(RUT_RECEPTOR.Text) <> Trim(IDE_DV_RECEPTOR.Text) Then
                MsgBox "Dv ingresado no corresponde al " & RUT_RECEPTOR2.Caption, vbExclamation, Me.Caption
                IDE_DV_RECEPTOR.SetFocus
                GoTo LabelSalir
            End If
        End If
    End If
    If NOM_RECEPTOR.Visible And NOM_RECEPTOR.Tag = "OBLIGATORIO" And Trim(NOM_RECEPTOR.Text) = "" Then
        MsgBox "Debe ingresar " & NOM_RECEPTOR2.Caption, vbExclamation, Me.Caption
        NOM_RECEPTOR.SetFocus
        GoTo LabelSalir
    End If
    If GLS_DIRECCION.Visible And GLS_DIRECCION.Tag = "OBLIGATORIO" And Trim(GLS_DIRECCION.Text) = "" Then
        MsgBox "Debe ingresar " & GLS_DIRECCION2.Caption, vbExclamation, Me.Caption
        GLS_DIRECCION.SetFocus
        GoTo LabelSalir
    End If
    If GLS_COMUNA.Visible And GLS_COMUNA.Tag = "OBLIGATORIO" And GLS_COMUNA.ListIndex = -1 Then
        MsgBox "Debe seleccionar " & GLS_COMUNA2.Caption, vbExclamation, Me.Caption
        GLS_COMUNA.SetFocus
        GoTo LabelSalir
    End If
    If GLS_CIUDAD.Visible And GLS_CIUDAD.Tag = "OBLIGATORIO" And GLS_CIUDAD.ListIndex = -1 Then
        MsgBox "Debe seleccionar " & GLS_CIUDAD2.Caption, vbExclamation, Me.Caption
        GLS_CIUDAD.SetFocus
        GoTo LabelSalir
    End If
    If COD_PAIS.Visible And COD_PAIS.Tag = "OBLIGATORIO" And COD_PAIS.ListIndex = -1 Then
        MsgBox "Debe seleccionar " & COD_PAIS2.Caption, vbExclamation, Me.Caption
        COD_PAIS.SetFocus
        GoTo LabelSalir
    End If
    If GLS_COD_POSTAL.Visible And GLS_COD_POSTAL.Tag = "OBLIGATORIO" And Trim(GLS_COD_POSTAL) = "" Then
        MsgBox "Debe ingresar " & GLS_COD_POSTAL2.Caption, vbExclamation, Me.Caption
        GLS_COD_POSTAL.SetFocus
        GoTo LabelSalir
    End If
    If GLS_TELEFONO.Visible And GLS_TELEFONO.Tag = "OBLIGATORIO" And Trim(GLS_TELEFONO.Text) = "" Then
        MsgBox "Debe ingresar " & GLS_TELEFONO2.Caption, vbExclamation, Me.Caption
        GLS_TELEFONO.SetFocus
        GoTo LabelSalir
    End If
    If GLS_NUM_FAX.Visible And GLS_NUM_FAX.Tag = "OBLIGATORIO" And Trim(GLS_NUM_FAX.Text) = "" Then
        MsgBox "Debe ingresar " & GLS_NUM_FAX2.Caption, vbExclamation, Me.Caption
        GLS_NUM_FAX.SetFocus
        GoTo LabelSalir
    End If
    If GLS_CASILLA.Visible And GLS_CASILLA.Tag = "OBLIGATORIO" And Trim(GLS_CASILLA.Text) = "" Then
        MsgBox "Debe ingresar " & GLS_CASILLA2.Caption, vbExclamation, Me.Caption
        GLS_CASILLA.SetFocus
        GoTo LabelSalir
    End If
    If COD_TIPO_DCTO.Visible And COD_TIPO_DCTO.Tag = "OBLIGATORIO" And COD_TIPO_DCTO.ListIndex = -1 Then
        MsgBox "Debe seleccionar " & COD_TIPO_DCTO2.Caption, vbExclamation, Me.Caption
        COD_TIPO_DCTO.SetFocus
        GoTo LabelSalir
    End If
    If NRO_DOCUMENTO.Visible And NRO_DOCUMENTO.Tag = "OBLIGATORIO" And Trim(NRO_DOCUMENTO.Text) = "" Then
        MsgBox "Debe ingresar " & NRO_DOCUMENTO2.Caption, vbExclamation, Me.Caption
        NRO_DOCUMENTO.SetFocus
        GoTo LabelSalir
    End If
    If NRO_CUOTA.Visible And NRO_CUOTA.Tag = "OBLIGATORIO" And Trim(NRO_CUOTA.Text) = "" Then
        MsgBox "Debe ingresar " & NRO_CUOTA2.Caption, vbExclamation, Me.Caption
        NRO_CUOTA.SetFocus
        GoTo LabelSalir
    End If
    If COD_BANCO.Visible And COD_BANCO.Tag = "OBLIGATORIO" And COD_BANCO.ListIndex = -1 Then
        MsgBox "Debe seleccionar " & COD_BANCO2.Caption, vbExclamation, Me.Caption
        COD_BANCO.SetFocus
        GoTo LabelSalir
    End If
    If COD_PLAZA.Visible And COD_PLAZA.Tag = "OBLIGATORIO" And COD_PLAZA.ListIndex = -1 Then
        MsgBox "Debe seleccionar " & COD_PLAZA2.Caption, vbExclamation, Me.Caption
        COD_PLAZA.SetFocus
        GoTo LabelSalir
    End If
    If GLS_CTACTE.Visible And GLS_CTACTE.Tag = "OBLIGATORIO" And Trim(GLS_CTACTE.Text) = "" Then
        MsgBox "Debe ingresar " & GLS_CTACTE2.Caption, vbExclamation, Me.Caption
        GLS_CTACTE.SetFocus
        GoTo LabelSalir
    End If
    If COD_FORMA_PAGO.Visible And COD_FORMA_PAGO.Tag = "OBLIGATORIO" And COD_FORMA_PAGO.ListIndex = -1 Then
        MsgBox "Debe seleccionar " & COD_FORMA_PAGO2.Caption, vbExclamation, Me.Caption
        COD_FORMA_PAGO.SetFocus
        GoTo LabelSalir
    End If
    If MNT_DOCUMENTO.Visible And MNT_DOCUMENTO.Tag = "OBLIGATORIO" And Trim(MNT_DOCUMENTO.Text) = "" Then
        MsgBox "Debe ingresar " & MNT_DOCUMENTO2.Caption, vbExclamation, Me.Caption
        MNT_DOCUMENTO.SetFocus
        GoTo LabelSalir
    End If
    If COD_MONEDA.Visible And COD_MONEDA.Tag = "OBLIGATORIO" And COD_MONEDA.ListIndex = -1 Then
        MsgBox "Debe seleccionar " & COD_MONEDA2.Caption, vbExclamation, Me.Caption
        COD_MONEDA.SetFocus
        GoTo LabelSalir
    End If
    If GLS_OBSERVACION.Visible And GLS_OBSERVACION.Tag = "OBLIGATORIO" And Trim(GLS_OBSERVACION.Text) = "" Then
        MsgBox "Debe ingresar " & GLS_OBSERVACION2.Caption, vbExclamation, Me.Caption
        GLS_OBSERVACION.SetFocus
        GoTo LabelSalir
    End If
    If GLS_REFERENCIA_1.Visible And GLS_REFERENCIA_1.Tag = "OBLIGATORIO" And Trim(GLS_REFERENCIA_1.Text) = "" Then
        MsgBox "Debe ingresar " & GLS_REFERENCIA_12.Caption, vbExclamation, Me.Caption
        GLS_REFERENCIA_1.SetFocus
        GoTo LabelSalir
    End If
    If GLS_REFERENCIA_2.Visible And GLS_REFERENCIA_2.Tag = "OBLIGATORIO" And Trim(GLS_REFERENCIA_2.Text) = "" Then
        MsgBox "Debe ingresar " & GLS_REFERENCIA_22.Caption, vbExclamation, Me.Caption
        GLS_REFERENCIA_2.SetFocus
        GoTo LabelSalir
    End If
    If GLS_REFERENCIA_3.Visible And GLS_REFERENCIA_3.Tag = "OBLIGATORIO" And Trim(GLS_REFERENCIA_3.Text) = "" Then
        MsgBox "Debe ingresar " & GLS_REFERENCIA_32.Caption, vbExclamation, Me.Caption
        GLS_REFERENCIA_3.SetFocus
        GoTo LabelSalir
    End If
    If COD_CLASIF_EMISOR.Visible And COD_CLASIF_EMISOR.Tag = "OBLIGATORIO" And COD_CLASIF_EMISOR.ListIndex = -1 Then
        MsgBox "Debe seleccionar " & COD_CLASIF_EMISOR2.Caption, vbExclamation, Me.Caption
        COD_CLASIF_EMISOR.SetFocus
        GoTo LabelSalir
    End If
    If MNT_ORIGINAL.Visible And MNT_ORIGINAL.Tag = "OBLIGATORIO" And Trim(MNT_ORIGINAL.Text) = "" Then
        MsgBox "Debe ingresar " & MNT_ORIGINAL2.Caption, vbExclamation, Me.Caption
        MNT_ORIGINAL.SetFocus
        GoTo LabelSalir
    End If
    If MNT_IVA.Visible And MNT_IVA.Tag = "OBLIGATORIO" And Trim(MNT_IVA.Text) = "" Then
        MsgBox "Debe ingresar " & MNT_IVA2.Caption, vbExclamation, Me.Caption
        MNT_IVA.SetFocus
        GoTo LabelSalir
    End If
    If MNT_NETO.Visible And MNT_NETO.Tag = "OBLIGATORIO" And Trim(MNT_NETO.Text) = "" Then
        MsgBox "Debe ingresar " & MNT_NETO2.Caption, vbExclamation, Me.Caption
        MNT_NETO.SetFocus
        GoTo LabelSalir
    End If
    If MNT_EXENTO.Visible And MNT_EXENTO.Tag = "OBLIGATORIO" And Trim(MNT_EXENTO.Text) = "" Then
        MsgBox "Debe ingresar " & MNT_EXENTO2.Caption, vbExclamation, Me.Caption
        MNT_EXENTO.SetFocus
        GoTo LabelSalir
    End If
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "DBO_FACTOR.PKG_FME_CARGA_DIGITACION.SP_FME_REGISTRO_ARCHIVO_GRABAR"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Contrato", adNumeric, adParamInput, 10, Val(lblNumContrato))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeRemesa", adNumeric, adParamInput, 10, Val(lblNumRemesa))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_NroLinea", adNumeric, adParamInput, 10, Val(lblNumLinea))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_RutEmisor", adNumeric, adParamInput, 10, IIf(RUT_EMISOR.Visible, Val(RUT_EMISOR.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_DvEmisor", adChar, adParamInput, 1, IIf(IDE_DV_EMISOR.Visible, Trim(IDE_DV_EMISOR.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_NombreEmisor", adVarChar, adParamInput, 80, IIf(NOM_EMISOR.Visible, Trim(NOM_EMISOR.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_RutReceptor", adNumeric, adParamInput, 10, IIf(RUT_RECEPTOR.Visible, Val(RUT_RECEPTOR.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_DvReceptor", adChar, adParamInput, 1, IIf(IDE_DV_RECEPTOR.Visible, Trim(IDE_DV_RECEPTOR.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_NombreRecep", adVarChar, adParamInput, 80, IIf(NOM_RECEPTOR.Visible, Trim(NOM_RECEPTOR.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_Direcion", adVarChar, adParamInput, 120, IIf(GLS_DIRECCION.Visible, Trim(GLS_DIRECCION.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_GlsComuna", adVarChar, adParamInput, 40, IIf(GLS_COMUNA.Visible, Trim(GLS_COMUNA.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_GlsCiudad", adVarChar, adParamInput, 40, IIf(GLS_CIUDAD.Visible, Trim(GLS_CIUDAD.Text), Null))
    If COD_PAIS.Visible Then
        If COD_PAIS.ListIndex <> -1 Then
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_Cod_Pais", adChar, adParamInput, 8, garrPaises(COD_PAIS.ListIndex, 0))
        Else
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_Cod_Pais", adChar, adParamInput, 8, Null)
        End If
    Else
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_Cod_Pais", adChar, adParamInput, 8, Null)
    End If
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_CodPostal", adVarChar, adParamInput, 20, IIf(GLS_COD_POSTAL.Visible, Trim(GLS_COD_POSTAL.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_GlsTelefono", adVarChar, adParamInput, 20, IIf(GLS_TELEFONO.Visible, Trim(GLS_TELEFONO.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_GlsFax", adVarChar, adParamInput, 20, IIf(GLS_NUM_FAX.Visible, Trim(GLS_NUM_FAX.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_GlsCasilla", adVarChar, adParamInput, 40, IIf(GLS_CASILLA.Visible, Trim(GLS_CASILLA.Text), Null))
    If COD_TIPO_DCTO.Visible Then
        If COD_TIPO_DCTO.ListIndex <> -1 Then
            Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeTipoDocto", adNumeric, adParamInput, 10, garrDocumentos(COD_TIPO_DCTO.ListIndex, 0))
        Else
            Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeTipoDocto", adNumeric, adParamInput, 10, Null)
        End If
    Else
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeTipoDocto", adNumeric, adParamInput, 10, Null)
    End If
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_NroDocto", adVarChar, adParamInput, 20, IIf(NRO_DOCUMENTO.Visible, Trim(NRO_DOCUMENTO.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_NroCuota", adNumeric, adParamInput, 3, IIf(NRO_CUOTA.Visible, Val(NRO_CUOTA.Text), Null))
    If COD_BANCO.Visible Then
        If COD_BANCO.ListIndex <> -1 Then
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodBanco", adChar, adParamInput, 8, garrBancos(COD_BANCO.ListIndex, 0))
        Else
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodBanco", adChar, adParamInput, 8, Null)
        End If
    Else
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodBanco", adChar, adParamInput, 8, Null)
    End If
    If COD_PLAZA.Visible Then
        If COD_PLAZA.ListIndex <> -1 Then
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodPlaza", adChar, adParamInput, 8, garrPlazas(COD_PLAZA.ListIndex, 0))
        Else
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodPlaza", adChar, adParamInput, 8, Null)
        End If
    Else
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodPlaza", adChar, adParamInput, 8, Null)
    End If
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_GlsCtaCte", adVarChar, adParamInput, 12, IIf(GLS_CTACTE.Visible, Trim(GLS_CTACTE.Text), Null))
    If COD_FORMA_PAGO.Visible Then
        If COD_FORMA_PAGO.ListIndex <> -1 Then
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodFormaPago", adChar, adParamInput, 8, COD_FORMA_PAGO.ItemData(COD_FORMA_PAGO.ListIndex))
        Else
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodFormaPago", adChar, adParamInput, 8, Null)
        End If
    Else
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodFormaPago", adChar, adParamInput, 8, Null)
    End If
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_MtoDocto", adNumeric, adParamInput, 20, IIf(MNT_DOCUMENTO.Visible, IIf(Trim(MNT_DOCUMENTO.Text) = "", Null, Format(MNT_DOCUMENTO.Text, "###0.0000")), Null))
    If COD_MONEDA.Visible Then
        If COD_MONEDA.ListIndex <> -1 Then
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodMoneda", adChar, adParamInput, 8, garrMonedas(COD_MONEDA.ListIndex, 0))
        Else
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodMoneda", adChar, adParamInput, 8, Null)
        End If
    Else
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodMoneda", adChar, adParamInput, 8, Null)
    End If
    Cmd.Parameters.Append Cmd.CreateParameter("p_d_FechaEmision", adDate, adParamInput, 10, IIf(FCH_EMISION.Visible, Format(FCH_EMISION, "dd/mm/yyyy"), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_d_FechaVcto", adDate, adParamInput, 10, IIf(FCH_VENCIMIENTO.Visible, Format(FCH_VENCIMIENTO, "dd/mm/yyyy"), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_d_FechaPago", adDate, adParamInput, 10, IIf(FCH_PAGO.Visible, Format(FCH_PAGO, "dd/mm/yyyy"), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_Obs", adVarChar, adParamInput, 255, IIf(GLS_OBSERVACION.Visible, Trim(GLS_OBSERVACION.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_GlsRef1", adVarChar, adParamInput, 80, IIf(GLS_REFERENCIA_1.Visible, Trim(GLS_REFERENCIA_1.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_GlsRef2", adVarChar, adParamInput, 80, IIf(GLS_REFERENCIA_2.Visible, Trim(GLS_REFERENCIA_2.Text), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_GlsRef3", adVarChar, adParamInput, 80, IIf(GLS_REFERENCIA_3.Visible, Trim(GLS_REFERENCIA_3.Text), Null))
    If COD_CLASIF_EMISOR.Visible Then
        If COD_CLASIF_EMISOR.ListIndex <> -1 Then
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodClasifEmisor", adChar, adParamInput, 8, COD_CLASIF_EMISOR.ItemData(COD_CLASIF_EMISOR.ListIndex))
        Else
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodClasifEmisor", adChar, adParamInput, 8, Null)
        End If
    Else
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodClasifEmisor", adChar, adParamInput, 8, Null)
    End If
    If MRC_OPERATORIA.Visible Then
        If COD_TIPO_DCTO.ListIndex <> -1 Then
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodOperatoria", adChar, adParamInput, 1, IIf(UCase(Trim(MRC_OPERATORIA.Text)) = "SUMA", 1, 2))
        Else
            Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodOperatoria", adChar, adParamInput, 1, Null)
        End If
    Else
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodOperatoria", adChar, adParamInput, 1, Null)
    End If
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_MtoOriginal", adNumeric, adParamInput, 20, IIf(MNT_ORIGINAL.Visible, IIf(Trim(MNT_ORIGINAL.Text) = "", Null, Format(MNT_ORIGINAL.Text, "###0.0000")), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_MtoIva", adNumeric, adParamInput, 20, IIf(MNT_IVA.Visible, IIf(Trim(MNT_IVA.Text) = "", Null, Format(MNT_IVA.Text, "###0.0000")), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_MtoNeto", adNumeric, adParamInput, 20, IIf(MNT_NETO.Visible, IIf(Trim(MNT_NETO.Text) = "", Null, Format(MNT_NETO.Text, "###0.0000")), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_MtoExento", adNumeric, adParamInput, 20, IIf(MNT_EXENTO.Visible, IIf(Trim(MNT_EXENTO.Text) = "", Null, Format(MNT_EXENTO.Text, "###0.0000")), Null))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adChar, adParamInput, 10, gstrUsuario)
    Cmd.Parameters.Append Cmd.CreateParameter("p_V_DescError", adVarChar, adParamOutput, 1000)
    Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser Grabada." & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbExclamation, Me.Caption
        GoTo LabelSalir
    End If
    If Not gblnModifica Then
        Call LimpiarControles(frmDigitacion, "DIG")
        Call LimpiarControles(frmDigitacion, "OBLIGATORIO")
    End If
    MsgBox "informaci�n Grabada Exitosamente.", vbInformation, Me.Caption
    
    Rem ESTA UTILIZACION
    Call frmCargaDigitacion.tabSeciones_Click(1)
    gblnswichRefresca = True
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdVolver_Click()
    Unload Me
End Sub

Private Sub COD_ESTADO_DCTO_Change()

End Sub

Private Sub COD_BANCO_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub COD_CLASIF_EMISOR_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub COD_FORMA_PAGO_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub COD_MONEDA_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub MRC_OPERATORIA_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub COD_PAIS_KeyPress(KeyAscii As Integer)
    If KeyAscii = 8 Then COD_PAIS.ListIndex = -1
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub COD_PLAZA_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub COD_TIPO_DCTO_Click()
Dim intOperatoria As Integer
    
    If COD_TIPO_DCTO.ListIndex = -1 Then Exit Sub
    intOperatoria = garrDocumentos(COD_TIPO_DCTO.ListIndex, 2)
    If MRC_OPERATORIA.Visible Then
        If intOperatoria = 1 Then
            MRC_OPERATORIA.Text = "SUMA"
        Else
            MRC_OPERATORIA.Text = "RESTA"
        End If
    End If
End Sub

Private Sub COD_TIPO_DCTO_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub Command1_Click()
    'frmCierreDigita.Show 1
End Sub

Private Sub FCH_EMISION_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub FCH_PAGO_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub FCH_VENCIMIENTO_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub Form_Load()
    
    Call cargarImagenes(tlbAcciones, ilsIconos, "grabar", "volver")
    
    gblnswichRefresca = False
    picDigitacion.Height = 5235
    Rem Se establece como m�ximo valor al ScrollBar la resta entre la altura del formulario
    Rem y la altura del PictureBox
    vsbDigitacion.Max = picDigitacion.ScaleHeight - fraDigitacion.Height
    glngPosScroll = 0
End Sub

Private Sub GLS_CASILLA_GotFocus()
    GLS_CASILLA.SelStart = 0
    GLS_CASILLA.SelLength = Len(GLS_CASILLA)
End Sub

Private Sub GLS_CASILLA_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_COD_POSTAL_GotFocus()
    GLS_COD_POSTAL.SelStart = 0
    GLS_COD_POSTAL.SelLength = Len(GLS_COD_POSTAL)
End Sub

Private Sub GLS_COD_POSTAL_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_COMUNA_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_CTACTE_GotFocus()
    GLS_CTACTE.SelStart = 0
    GLS_CTACTE.SelLength = Len(GLS_CTACTE)
End Sub

Private Sub GLS_CTACTE_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_CIUDAD_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_DIRECCION_GotFocus()
    GLS_DIRECCION.SelStart = 0
    GLS_DIRECCION.SelLength = Len(GLS_DIRECCION)
End Sub

Private Sub GLS_DIRECCION_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_ERROR_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_NUM_FAX_GotFocus()
    GLS_NUM_FAX.SelStart = 0
    GLS_NUM_FAX.SelLength = Len(GLS_NUM_FAX)
End Sub

Private Sub GLS_NUM_FAX_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_OBSERVACION_GotFocus()
    GLS_OBSERVACION.SelStart = 0
    GLS_OBSERVACION.SelLength = Len(GLS_OBSERVACION)
End Sub

Private Sub GLS_OBSERVACION_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_REFERENCIA_1_GotFocus()
    GLS_REFERENCIA_1.SelStart = 0
    GLS_REFERENCIA_1.SelLength = Len(GLS_REFERENCIA_1)
End Sub

Private Sub GLS_REFERENCIA_1_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_REFERENCIA_2_GotFocus()
    GLS_REFERENCIA_2.SelStart = 0
    GLS_REFERENCIA_2.SelLength = Len(GLS_REFERENCIA_2)
End Sub

Private Sub GLS_REFERENCIA_2_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_REFERENCIA_3_GotFocus()
    GLS_REFERENCIA_3.SelStart = 0
    GLS_REFERENCIA_3.SelLength = Len(GLS_REFERENCIA_3)
End Sub

Private Sub GLS_REFERENCIA_3_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub GLS_TELEFONO_GotFocus()
    GLS_TELEFONO.SelStart = 0
    GLS_TELEFONO.SelLength = Len(GLS_TELEFONO)
End Sub

Private Sub GLS_TELEFONO_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub IDE_DV_EMISOR_GotFocus()
    IDE_DV_EMISOR.SelStart = 0
    IDE_DV_EMISOR.SelLength = Len(IDE_DV_EMISOR)
End Sub

Private Sub IDE_DV_EMISOR_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub IDE_DV_RECEPTOR_GotFocus()
    IDE_DV_RECEPTOR.SelStart = 0
    IDE_DV_RECEPTOR.SelLength = Len(IDE_DV_RECEPTOR)
End Sub

Private Sub IDE_DV_RECEPTOR_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub MNT_DOCUMENTO_GotFocus()
    MNT_DOCUMENTO = Format(MNT_DOCUMENTO, "###0.0000")
    MNT_DOCUMENTO.SelStart = 0
    MNT_DOCUMENTO.SelLength = Len(MNT_DOCUMENTO)
End Sub

Private Sub MNT_DOCUMENTO_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
    If Not Es_Numero(MNT_DOCUMENTO, KeyAscii, True, 15) Then
        KeyAscii = 0
    End If
End Sub

Private Sub MNT_DOCUMENTO_LostFocus()
    MNT_DOCUMENTO = Format(MNT_DOCUMENTO, "#,##0.0000")
End Sub

Private Sub MNT_EXENTO_GotFocus()
    MNT_EXENTO = Format(MNT_EXENTO, "###0.0000")
    MNT_EXENTO.SelStart = 0
    MNT_EXENTO.SelLength = Len(MNT_EXENTO)
End Sub

Private Sub MNT_EXENTO_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
    If Not Es_Numero(MNT_EXENTO, KeyAscii, True, 15) Then
        KeyAscii = 0
    End If
End Sub

Private Sub MNT_EXENTO_LostFocus()
    MNT_EXENTO = Format(MNT_EXENTO, "#,##0.0000")
End Sub

Private Sub MNT_IVA_GotFocus()
    MNT_IVA = Format(MNT_IVA, "###0.0000")
    MNT_IVA.SelStart = 0
    MNT_IVA.SelLength = Len(MNT_IVA)
End Sub

Private Sub MNT_IVA_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
    If Not Es_Numero(MNT_IVA, KeyAscii, True, 15) Then
        KeyAscii = 0
    End If
End Sub

Private Sub MNT_IVA_LostFocus()
    MNT_IVA = Format(MNT_IVA, "#,##0.0000")
End Sub

Private Sub MNT_NETO_GotFocus()
    MNT_NETO = Format(MNT_NETO, "###0.0000")
    MNT_NETO.SelStart = 0
    MNT_NETO.SelLength = Len(MNT_NETO)
End Sub

Private Sub MNT_NETO_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
    If Not Es_Numero(MNT_NETO, KeyAscii, True, 15) Then
        KeyAscii = 0
    End If
End Sub

Private Sub MNT_NETO_LostFocus()
    MNT_NETO = Format(MNT_NETO, "#,##0.0000")
End Sub

Private Sub MNT_ORIGINAL_GotFocus()
    MNT_ORIGINAL = Format(MNT_ORIGINAL, "###0.0000")
    MNT_ORIGINAL.SelStart = 0
    MNT_ORIGINAL.SelLength = Len(MNT_ORIGINAL)
End Sub

Private Sub MNT_ORIGINAL_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
    If Not Es_Numero(MNT_ORIGINAL, KeyAscii, True, 15) Then
        KeyAscii = 0
    End If
End Sub

Private Sub MNT_ORIGINAL_LostFocus()
    MNT_ORIGINAL = Format(MNT_ORIGINAL, "#,##0.0000")
End Sub

Private Sub NOM_EMISOR_GotFocus()
    NOM_EMISOR.SelStart = 0
    NOM_EMISOR.SelLength = Len(NOM_EMISOR)
End Sub

Private Sub NOM_EMISOR_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub NOM_RECEPTOR_GotFocus()
    NOM_RECEPTOR.SelStart = 0
    NOM_RECEPTOR.SelLength = Len(NOM_RECEPTOR)
End Sub

Private Sub NOM_RECEPTOR_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
End Sub

Private Sub NRO_CUOTA_GotFocus()
    NRO_CUOTA.SelStart = 0
    NRO_CUOTA.SelLength = Len(NRO_CUOTA)
End Sub

Private Sub NRO_CUOTA_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
    If Not Es_Numero(NRO_CUOTA, KeyAscii, False) Then
        KeyAscii = 0
    End If
End Sub

Private Sub NRO_DOCUMENTO_GotFocus()
    NRO_DOCUMENTO.SelStart = 0
    NRO_DOCUMENTO.SelLength = Len(NRO_DOCUMENTO)
End Sub

Private Sub NRO_DOCUMENTO_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
    If Not Es_Numero(NRO_DOCUMENTO, KeyAscii, False) Then
        KeyAscii = 0
    End If
End Sub

Private Sub RUT_EMISOR_GotFocus()
    RUT_EMISOR.SelStart = 0
    RUT_EMISOR.SelLength = Len(RUT_EMISOR)
End Sub

Private Sub RUT_EMISOR_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
    If Not Es_Numero(RUT_EMISOR, KeyAscii, False) Then
        KeyAscii = 0
    End If
End Sub

Private Sub RUT_RECEPTOR_GotFocus()
    RUT_RECEPTOR.SelStart = 0
    RUT_RECEPTOR.SelLength = Len(RUT_RECEPTOR)
End Sub

Private Sub RUT_RECEPTOR_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
        If glngPosScroll < -700 Then glngPosScroll = 0
        glngPosScroll = glngPosScroll - 50
        picDigitacion.Move 0, glngPosScroll
    End If
    If Not Es_Numero(RUT_RECEPTOR, KeyAscii, False) Then
        KeyAscii = 0
    End If
End Sub

Private Sub VScroll1_Change()

End Sub

Private Sub vsbDigitacion_Change()
    picDigitacion.Move 0, -vsbDigitacion.Value
End Sub

Private Sub vsbDigitacion_Scroll()
    picDigitacion.Move 0, -vsbDigitacion.Value
End Sub
