VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmNvaCarga 
   Caption         =   "Nueva Carga"
   ClientHeight    =   4320
   ClientLeft      =   60
   ClientTop       =   8685
   ClientWidth     =   7680
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4320
   ScaleWidth      =   7680
   Begin VB.Frame fraSeleccion 
      Height          =   2775
      Index           =   4
      Left            =   45
      TabIndex        =   7
      Top             =   495
      Width           =   7545
      Begin VB.ComboBox cboFormato 
         Height          =   315
         Left            =   1110
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1080
         Width           =   2100
      End
      Begin VB.ComboBox cboEntidad 
         Height          =   315
         Left            =   1125
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   720
         Width           =   6300
      End
      Begin VB.TextBox txtRutaArchivo 
         Height          =   310
         Left            =   1110
         MaxLength       =   255
         TabIndex        =   1
         Tag             =   "CARGA"
         Top             =   1425
         Width           =   5895
      End
      Begin VB.TextBox txtDescripcion 
         Height          =   675
         Left            =   1110
         MaxLength       =   255
         MultiLine       =   -1  'True
         TabIndex        =   2
         Tag             =   "CARGA"
         Top             =   1830
         Width           =   6320
      End
      Begin VB.CommandButton cmdOrigen 
         Caption         =   "..."
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7095
         TabIndex        =   8
         Top             =   1440
         Width           =   320
      End
      Begin VB.ComboBox cboProducto 
         Height          =   315
         Left            =   1125
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Tag             =   "CARGA"
         Top             =   375
         Width           =   6300
      End
      Begin MSComDlg.CommonDialog dlgRuta 
         Left            =   120
         Top             =   2550
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label Label1 
         Caption         =   "Formato"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   1155
         Width           =   735
      End
      Begin VB.Label lblComun 
         AutoSize        =   -1  'True
         Caption         =   "Entidad"
         Height          =   210
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Top             =   795
         Width           =   705
      End
      Begin VB.Label lblComun 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Archivo"
         Height          =   210
         Index           =   3
         Left            =   120
         TabIndex        =   13
         Top             =   420
         Width           =   915
      End
      Begin VB.Label lblSeleccion 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Creaci�n de Nueva Carga"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   4
         Left            =   0
         TabIndex        =   11
         Top             =   30
         Width           =   7530
      End
      Begin VB.Label Label5 
         Caption         =   "Ruta Arch."
         Height          =   240
         Left            =   120
         TabIndex        =   10
         Top             =   1485
         Width           =   900
      End
      Begin VB.Label Label6 
         Caption         =   "Observaci�n"
         Height          =   240
         Left            =   120
         TabIndex        =   9
         Top             =   1800
         Width           =   900
      End
   End
   Begin VB.Frame fraBotones 
      Height          =   735
      Index           =   0
      Left            =   60
      TabIndex        =   5
      Top             =   3270
      Width           =   7515
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   5955
         TabIndex        =   4
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdGrabar 
         Caption         =   "&Grabar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   120
         TabIndex        =   3
         Top             =   210
         Width           =   1425
      End
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   7680
      _ExtentX        =   13547
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Grabar"
            Object.ToolTipText     =   "Grabar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Volver"
            Object.ToolTipText     =   "Volver"
         EndProperty
      EndProperty
      Begin MSComctlLib.ImageList ilsIconos 
         Left            =   1665
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   12
      Top             =   4050
      Width           =   7680
      _ExtentX        =   13547
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10443
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmNvaCarga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim gstrCodTipoFmto As String
Dim gstrNCorTipoFmto   As String
Private Function Validado() As Boolean
    Rem Validaciones
    Validado = True
    If Trim(cboEntidad.Text) = "" Then
        MsgBox "Debe ingresar Entidad", vbExclamation, Me.Caption
        cboEntidad.SetFocus
        Validado = False
    ElseIf cboProducto.ListIndex = -1 Then
        MsgBox "Debe seleccionar Producto", vbExclamation, Me.Caption
        cboProducto.SetFocus
        Validado = False
    ElseIf cboFormato.Text = "" Then
        MsgBox "Cliente no cuenta con un Tipo de Formato," & Chr(13) & "para el producto" & cboProducto.Text, vbExclamation, Me.Caption
        Validado = False
    ElseIf LCase(Right(txtRutaArchivo, 4)) <> ".txt" And LCase(Right(txtRutaArchivo, 4)) <> ".xls" Then
        MsgBox "No se ha seleccionado el archivo a carar," & Chr(13) & "o se ha seleccionado otro tipo de archivo.", vbExclamation, Me.Caption
        txtRutaArchivo.SetFocus
        Validado = False
    End If

End Function

'Private Sub cboNombreEnt_Click()
'    cboRutEnt.ListIndex = cboNombreEnt.ListIndex
'
'    If cboNombreEnt.ListIndex = -1 Then Exit Sub
'
'    If cboNombreEnt.ItemData(cboNombreEnt.ListIndex) = 1 Then
'        MsgBox "Cliente Bloqueado. No se pueden cargar Remesas", vbExclamation, "Bloqueo"
'        cboNombreEnt.ListIndex = -1
'        cboRutEnt.ListIndex = -1
'    End If
'
'    If cboRutEnt.ListIndex <> -1 And cboProducto.ListIndex <> -1 Then
'        Dim vntCMarco() As Variant
'        Call Trae_CMarco_Vigente(vntCMarco(), cboRutEnt.ItemData(cboRutEnt.ListIndex), cboProducto.ItemData(cboProducto.ListIndex))
'        lblCMarco.Caption = vntCMarco(0)
'        txtRutaArchivo.Text = vntCMarco(1)
'    End If
'
'    cboSucursal.Clear
'End Sub
'Private Sub cboNombreEnt_GotFocus()
'    cboNombreEnt.Text = ""
'    cboRutEnt.ListIndex = -1
'    cboProducto.ListIndex = 0
'End Sub


'Private Sub cboNombreEnt_KeyPress(KeyAscii As Integer)
'    cboRutEnt.Clear
'End Sub


'Private Sub cboRutEnt_Click()
'
'    cboNombreEnt.ListIndex = cboRutEnt.ListIndex
'
'    If cboRutEnt.ListIndex = -1 Then Exit Sub
'
'    If cboNombreEnt.ItemData(cboNombreEnt.ListIndex) = 1 Then
'        MsgBox "Cliente Bloqueado. No se pueden cargar Remesas", vbExclamation, "Bloqueo"
'        cboNombreEnt.ListIndex = -1
'        cboRutEnt.ListIndex = -1
'    End If
'
'    If cboRutEnt.ListIndex <> -1 And cboProducto.ListIndex <> -1 Then
'        Dim vntCMarco() As Variant
'        Call Trae_CMarco_Vigente(vntCMarco(), cboRutEnt.ItemData(cboRutEnt.ListIndex), cboProducto.ItemData(cboProducto.ListIndex))
'        lblCMarco.Caption = vntCMarco(0)
'        txtRutaArchivo.Text = vntCMarco(1)
'    End If
'
'    cboSucursal.Clear
'
'End Sub
'
'
'Private Sub cboRutEnt_GotFocus()
'    cboRutEnt.Text = ""
'    cboNombreEnt.ListIndex = -1
'    lblCMarco.Caption = ""
'End Sub
'Sub cboRutEnt_KeyPress(KeyAscii As Integer)
'    If KeyAscii = 13 Then cmdBusEnt_Click
'    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
'        KeyAscii = 0
'    Else
'        cboNombreEnt.Clear
'        If cboRutEnt.ListCount > 0 Then cboRutEnt.Clear
'    End If
'End Sub

'Private Sub cboSucursal_Click()
'    If cboRutEnt.ListIndex >= 0 And cboProducto.ListIndex >= 0 And cboSucursal.ListIndex >= 0 Then
'        Trae_Estructura
'    End If
'End Sub

'Private Sub cboSucursal_GotFocus()
'    'Carga_Cbo_Sucursal
'    If cboRutEnt.ListIndex >= 0 Then
'        Carga_Combo_Sucursales cboSucursal, cboRutEnt.ItemData(cboRutEnt.ListIndex)
'    End If
'End Sub


'Private Sub cmdBusEnt_Click()
'    Dim recRegistros As New ADODB.Recordset
'    Dim Cmd As New ADODB.Command
'    Dim mlngRut As Long
'    Dim mlngProducto As Long
'    Dim mstrNombre As String
'
'    On Error GoTo LabelError
'
'    If cboRutEnt.Text = Empty And cboNombreEnt.Text = Empty Then GoTo LabelSalir
'    Screen.MousePointer = vbHourglass
'
'    mlngRut = Val(cboRutEnt.Text)
'    mstrNombre = (cboNombreEnt.Text)
'
'    Cmd.ActiveConnection = gAdoCon
'    Cmd.CommandType = adCmdStoredProc
'    Cmd.CommandText = "DBO_FACTOR.pkg_fme_entidades.sp_fme_clientes_ver"
'
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_rut", adNumeric, adParamInput, 10, mlngRut)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 80, mstrNombre)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_filtro_esp", adVarChar, adParamInput, 15, "")
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_tipo_filtro", adVarChar, adParamInput, 1, "")
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
'    Set recRegistros = Cmd.Execute
'
'    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
'        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Clientes"
'        GoTo LabelSalir
'    End If
'
'    cboRutEnt.Clear
'    cboNombreEnt.Clear
'
'    Do While Not recRegistros.EOF
'        cboRutEnt.AddItem RTrim("" & recRegistros.Fields("rut_entidad").Value) & "-" & recRegistros.Fields("ide_dig_rut").Value
'        cboRutEnt.ItemData(cboRutEnt.NewIndex) = recRegistros.Fields("ide_entidad").Value
'
'        cboNombreEnt.AddItem "" & recRegistros.Fields("nomcliente").Value
'        cboNombreEnt.ItemData(cboNombreEnt.NewIndex) = recRegistros.Fields("mrc_bloqueo").Value
'
'        recRegistros.MoveNext
'    Loop
'    If cboRutEnt.ListCount > 0 Then cboRutEnt.ListIndex = 0
'    If cboNombreEnt.ListCount > 0 Then cboNombreEnt.ListIndex = 0
'
'    Rem Exito
'    If cboRutEnt.ListIndex = -1 Then
'        mlngRut = 0
'    Else
'        mlngRut = cboRutEnt.ItemData(cboRutEnt.ListIndex)
'    End If
'
'    If cboProducto.ListIndex = -1 Then
'        mlngProducto = 0
'    Else
'        mlngProducto = cboProducto.ItemData(cboProducto.ListIndex)
'    End If
'
'    If cboRutEnt.ListIndex <> -1 And cboProducto.ListIndex <> -1 Then
'        Dim vntCMarco() As Variant
'        Call Trae_CMarco_Vigente(vntCMarco(), cboRutEnt.ItemData(cboRutEnt.ListIndex), cboProducto.ItemData(cboProducto.ListIndex))
'        lblCMarco.Caption = vntCMarco(0)
'        txtRutaArchivo.Text = vntCMarco(1)
'    End If
'
'LabelSalir:
'    Screen.MousePointer = 0
'    Set Cmd = Nothing
'    Set recRegistros = Nothing
'    Exit Sub
'LabelError:
'    If Not Err.Number = 0 Then
'        MsgBox Err.Description, vbCritical, "Error"
'        Err.Clear
'        GoTo LabelSalir
'        Resume 0
'    End If
'End Sub
'
'
'
Private Sub cmdGrabar_Click()
    Dim Cmd As New ADODB.Command
    On Error GoTo LabelError
    
    staEstado.Panels(1) = "Grabando..."
    Screen.MousePointer = 11
    
    If Not Validado Then Exit Sub
    
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.PKG_FME_CARGA_DIGITACION.SP_FME_ENCABEZADO_GRABAR"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_Ide_TipoArch", adChar, adParamInput, 8, cboProducto.ItemData(cboProducto.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_Ide_entidad", adChar, adParamInput, 8, cboEntidad.ItemData(cboEntidad.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_TipoFormato", adChar, adParamInput, 8, cboFormato.ItemData(cboFormato.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_Obs", adVarChar, adParamInput, 255, Trim(txtDescripcion.Text))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_TipoOrigen", adChar, adParamInput, 8, "CAR")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_RutaArchivo", adVarChar, adParamInput, 255, txtRutaArchivo.Text)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_Digitacion", adChar, adParamInput, 1, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_NroDoctos", adNumeric, adParamInput, 6, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_MtoDoctos", adNumeric, adParamInput, 20, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_NroDoctosAcep", adNumeric, adParamInput, 20, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeRemesa", adNumeric, adParamInputOutput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adChar, adParamInput, 10, gstrUsuario)
    Cmd.Parameters.Append Cmd.CreateParameter("p_V_DescError", adVarChar, adParamOutput, 1000)
    Cmd.Execute
        
    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser Grabada. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbExclamation, Me.Caption
        GoTo LabelSalir
    End If
    MsgBox "informaci�n Grabada Exitosamente.", vbInformation, Me.Caption
    
    frmCargaDigitacion.txtNumRemesa = ""
    frmCargaDigitacion.cboProducto.ListIndex = 0
    frmCargaDigitacion.cboFormato.ListIndex = 0
    frmCargaDigitacion.cboOrigen.ListIndex = 0
    frmCargaDigitacion.dtpFechaRecepIni.Value = Null
    frmCargaDigitacion.dtpFechaRecepFin.Value = Null
    frmCargaDigitacion.cboEstado.ListIndex = 0
    
    frmCargaDigitacion.Buscar_Remesas
    PosicionaGrilla frmCargaDigitacion.grdRegistroArch, Cmd.Parameters("p_n_IdeRemesa").Value, 2
    frmCargaDigitacion.tabSeciones.Tab = 1
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set Cmd = Nothing
    Unload Me

LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If

End Sub

Private Sub cmdOrigen_Click()
    Dim strMsgT   As String
    Dim blnTodoOk As Boolean
    
    On Error GoTo LabelError

    Rem Seleccionar Directorios
    dlgRuta.CancelError = True
    
    On Error GoTo LabelError1
    
    strMsgT = "| Todos los archivos (*.*)|*.*"
    
    Select Case gstrNCorTipoFmto
        Case "FIJO"
            dlgRuta.Filter = "Archivos de texto (*.txt)|*.txt" & strMsgT
            dlgRuta.DialogTitle = "Abrir base de datos de Texto Fijo"
        
        Case "VAR"
            dlgRuta.Filter = "Archivos de texto (*.txt)|*.txt" & strMsgT
            dlgRuta.DialogTitle = "Abrir base de datos de Texto Variable"
        
        Case "XLS"
            dlgRuta.Filter = "Archivos de Excel (*.xls)|*.xls" & strMsgT
            dlgRuta.DialogTitle = "Abrir archivo de Excel"
        
        Case "ACC"
            dlgRuta.Filter = "MDB de Microsoft Acces (*.mdb)|*.mdb" & strMsgT
            dlgRuta.DialogTitle = "Abri base de datos de Microsoft Acces"
        
    End Select
    
'    Select Case mstrTipoDB
'        Case gstrMSACCESS
'            dlg.Filter = "MDB de Microsoft Acces (*.mdb)|*.mdb" & strMsgT
'            dlg.DialogTitle = "Abri base de datos de Microsoft Acces"
'        Case gstrDBASEIII, gstrDBASEIV, gstrDBASE5
'            dlg.Filter = "Bases de datos de Dbase (*.dbf)|*.dbf" & strMsgT
'            dlg.DialogTitle = "Abrir base de datos de DBase"
'        Case gstrFOXPRO20, gstrFOXPRO25, gstrFOXPRO26, gstrFOXPRO30
'            dlg.Filter = "Bases de datos de FoxPro (*.dbf)|*.dbf" & strMsgT
'            dlg.DialogTitle = "Abrir base de datos de FoxPro"
'        Case gstrPARADOX3X, gstrPARADOX4X, gstrPARADOX5X
'            dlg.Filter = "Bases de datos de Paradox (*.db)|*.db" & strMsgT
'            dlg.DialogTitle = "Abrir base de datos de Paradox"
'        Case gstrEXCEL50
'            dlg.Filter = "Archivos de Excel (*.xls)|*.xls" & strMsgT
'            dlg.DialogTitle = "Abrir archivo de Excel"
'        Case gstrBTRIEVE
'            dlg.Filter = "Bases de datos de Btrieve (FILE.DDF)|FILE.DDF" & strMsgT
'            dlg.DialogTitle = "Abrir base de datos de BTrieve"
'        Case gstrTEXTFILES
'            dlg.Filter = "Archivos de texto (*.txt)|*.txt" & strMsgT
'            dlg.DialogTitle = "Abrir base de datos de Texto"
'    End Select
    
    dlgRuta.FLAGS = cdlOFNHideReadOnly + cdlOFNLongNames + cdlOFNExplorer + cdlOFNFileMustExist + cdlOFNNoChangeDir
    dlgRuta.InitDir = txtRutaArchivo.Text
    
    
    dlgRuta.ShowOpen
    
    txtRutaArchivo.Text = dlgRuta.FileName
    txtRutaArchivo.Tag = dlgRuta.FileName
    
    'call Inicializar
    Rem -------------------------------------------------------------------------
    Rem Aqu� se carga el archivo de texto formato d.o.s.
    Rem Cuando el archivo no viene en texto formato d.o.s, se realiza lo sgte:
    Rem     si es excel, se convierte a texto formato d.o.s.
    Rem     si es texto, pero con formato unix se convierte a texto formato d.o.s.
    Rem --------------------------------------------------------------------------
    'call cargarArchivo
    
    Exit Sub
LabelError1:
    Rem El usuario Presiono Cancelar
    txtRutaArchivo.SetFocus
    staEstado.Panels.Item(1).Text = "Listo"
    Screen.MousePointer = 0
    Exit Sub

LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1).Text = "Listo"
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, , "Error"
        Err.Clear
        GoTo LabelSalir
    End If
End Sub
'Private Sub Carga_Cbo_Sucursal()
'    Dim recRegistros As New ADODB.Recordset
'    Dim Cmd As New ADODB.Command
'
'    If cboRutEnt.Text = "" Or cboNombreEnt.Text = "" Then Exit Sub
'
'   Screen.MousePointer = vbHourglass
'
'   On Error GoTo Errores
'
'    Cmd.ActiveConnection = gAdoCon
'    Cmd.CommandType = adCmdStoredProc
'    Cmd.CommandText = "DBO_FACTOR.pkg_fme_entidades.sp_fme_sucursales_ver"
'
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ide_entidad", adNumeric, adParamInput, 10, cboRutEnt.ItemData(cboRutEnt.ListIndex))
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
'    Set recRegistros = Cmd.Execute
'
'    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
'        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Sucursales"
'        GoTo Salir
'    End If
'
'    cboSucursal.Clear
'    cboSucursal.AddItem ""
'    cboSucursal.ItemData(cboSucursal.NewIndex) = 0
'
'    Do While Not recRegistros.EOF
'        'Nombre de Sucursal
'        cboSucursal.AddItem "" & recRegistros.Fields("nom_sucursal").Value
'        'Numero de Sucursal
'        cboSucursal.ItemData(cboSucursal.NewIndex) = "" & recRegistros.Fields("nro_sucursal").Value
'
'        recRegistros.MoveNext
'    Loop
'    cboSucursal.ListIndex = 0
'
'    GoTo Salir
'
'Errores:
'    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Sucursales"
'
'Salir:
'    recRegistros.Close
'    Set Cmd = Nothing
'   Screen.MousePointer = vbNormal
'
'End Sub
'
'
'
'
Private Sub cmdVolver_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call cargarImagenes(tlbAcciones, ilsIconos, "grabar", "volver")
    
    Call Carga_Combo_Par(cboFormato, 1, 8)
    Call Carga_Combo_Par(cboEntidad, 1, 11)
    
    Call Carga_Combo_Par(cboProducto, 1, 10)

End Sub

'Private Sub Trae_Estructura()
'    Dim recRegistros As New ADODB.Recordset
'    Dim Cmd As New ADODB.Command
'
'    On Error GoTo error
'    Cmd.ActiveConnection = gAdoCon
'    Cmd.CommandType = adCmdStoredProc
'    Cmd.CommandText = "DBO_FACTOR.PKG_FME_CARGA_DIGITACION.sp_fme_glosa_estructura"
'
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Cliente", adNumeric, adParamInput, 10, cboRutEnt.ItemData(cboRutEnt.ListIndex))
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Producto", adNumeric, adParamInput, 10, cboProducto.ItemData(cboProducto.ListIndex))
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Sucursal", adNumeric, adParamInput, 10, cboSucursal.ItemData(cboSucursal.ListIndex))
'    Cmd.Parameters.Append Cmd.CreateParameter("p_c_tipo_arch", adChar, adParamInput, 3, "ENT")
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
'    Set recRegistros = Cmd.Execute
'
'    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
'        MsgBox Cmd.Parameters("p_v_DescError").Value, vbInformation, "Nueva Carga"
'        GoTo Salir
'    End If
'
'    If Not recRegistros.EOF Then
'        gstrCodTipoFmto = "" & recRegistros.Fields("cod_tipo_formato").Value
'        lblTipoFmto.Caption = "" & recRegistros.Fields("gls_tipo_fmto").Value
'        gstrNCorTipoFmto = "" & recRegistros.Fields("ncor_tipo_fmto").Value
'    Else
'        gstrCodTipoFmto = ""
'        lblTipoFmto.Caption = ""
'        gstrNCorTipoFmto = ""
'        MsgBox "Estructura del cliente no encontrada", vbInformation, Me.Caption
'        GoTo Salir
'    End If
'
'Salir:
'    Screen.MousePointer = 0
'    staEstado.Panels(1) = "Listo"
'    Exit Sub
'error:
'    If Not Err.Number = 0 Then
'        MsgBox Err.Description, vbCritical, "Error"
'        Err.Clear
'        GoTo Salir
'        Resume 0
'    End If
'
'End Sub
'
Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Trim(Button.Key))
        Case "GRABAR"
            cmdGrabar_Click
        Case "VOLVER"
            cmdVolver_Click
    End Select


End Sub


Private Sub txtDescripcion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

