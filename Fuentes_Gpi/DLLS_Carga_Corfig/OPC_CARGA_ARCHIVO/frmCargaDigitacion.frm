VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "ss32x25.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCargaDigitacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Carga/Digitaci�n Archivo Entidad"
   ClientHeight    =   7185
   ClientLeft      =   1395
   ClientTop       =   2010
   ClientWidth     =   9570
   Icon            =   "frmCargaDigitacion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7185
   ScaleWidth      =   9570
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   14
      Top             =   6915
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13776
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   12
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "CargarRms"
            Object.ToolTipText     =   "Cargar Nueva Remesa"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "DigitarRms"
            Object.ToolTipText     =   "Digitar Nueva Remesa"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EliminarRms"
            Object.ToolTipText     =   "Eliminar Remesa"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "AgregarDoc"
            Object.ToolTipText     =   "Agregar Documento"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "ModificarDoc"
            Object.ToolTipText     =   "Modificar Documento"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "EliminarDoc"
            Object.ToolTipText     =   "Eliminar Documento"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Validar"
            Object.ToolTipText     =   "Validar"
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "Proceso"
            Object.ToolTipText     =   "Enviar para Aceptaci�n"
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Volver"
            Object.ToolTipText     =   "Volver"
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Salir"
            Object.ToolTipText     =   "Salir"
         EndProperty
      EndProperty
      Begin MSComctlLib.ImageList ilsIconos 
         Left            =   5055
         Top             =   60
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
   Begin TabDlg.SSTab tabSeciones 
      Height          =   6435
      Left            =   60
      TabIndex        =   16
      Top             =   480
      Width           =   9480
      _ExtentX        =   16722
      _ExtentY        =   11351
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   7
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Remesas"
      TabPicture(0)   =   "frmCargaDigitacion.frx":000C
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fraSeleccion(4)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "grdRegistroArch"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraBotones(4)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblLista(4)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Detalle"
      TabPicture(1)   =   "frmCargaDigitacion.frx":0028
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "lblTituloRemesas"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "tabDetErr"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame1"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "fraBotones(0)"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).ControlCount=   4
      Begin VB.Frame fraBotones 
         Height          =   645
         Index           =   0
         Left            =   60
         TabIndex        =   56
         Top             =   5700
         Width           =   9360
         Begin VB.CommandButton cmdProceso 
            Caption         =   "&Procesar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   400
            Left            =   5850
            TabIndex        =   62
            Top             =   165
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.CommandButton cmdValidar 
            Caption         =   "&Validar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   400
            Left            =   4410
            TabIndex        =   61
            Top             =   165
            Width           =   1425
         End
         Begin VB.CommandButton cmdAgregar 
            Caption         =   "&Agregar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   400
            Left            =   60
            TabIndex        =   60
            Top             =   165
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.CommandButton cmdEliminar 
            Caption         =   "&Eliminar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   400
            Left            =   2955
            TabIndex        =   59
            Top             =   165
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.CommandButton cmdVolver 
            Caption         =   "&Volver"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   400
            Left            =   7815
            TabIndex        =   58
            Top             =   165
            Width           =   1425
         End
         Begin VB.CommandButton cmdModificar 
            Caption         =   "&Modificar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   400
            Left            =   1515
            TabIndex        =   57
            Top             =   165
            Visible         =   0   'False
            Width           =   1425
         End
      End
      Begin VB.Frame Frame1 
         Height          =   1395
         Left            =   60
         TabIndex        =   27
         Top             =   360
         Width           =   9375
         Begin VB.CheckBox chkDigita 
            Alignment       =   1  'Right Justify
            Caption         =   "Digitando"
            Height          =   195
            Left            =   120
            TabIndex        =   46
            Top             =   2010
            Width           =   1050
         End
         Begin VB.Label lblFormato 
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   6780
            TabIndex        =   42
            Tag             =   "CARGA"
            Top             =   660
            Width           =   2430
         End
         Begin VB.Label Label14 
            Caption         =   "Rut Cliente"
            Height          =   240
            Left            =   5895
            TabIndex        =   53
            Top             =   1980
            Width           =   810
         End
         Begin VB.Label lblNumRemesa 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   1155
            TabIndex        =   48
            Tag             =   "CARGA"
            Top             =   300
            Width           =   1380
         End
         Begin VB.Label Label29 
            Caption         =   "N� Archivo"
            Height          =   240
            Left            =   120
            TabIndex        =   47
            Top             =   360
            Width           =   840
         End
         Begin VB.Label lblOrigen 
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   7845
            TabIndex        =   45
            Tag             =   "CARGA"
            Top             =   1020
            Width           =   1380
         End
         Begin VB.Label lbEstado 
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   3345
            TabIndex        =   44
            Tag             =   "CARGA"
            Top             =   990
            Width           =   3300
         End
         Begin VB.Label lblFechaRecep 
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   1155
            TabIndex        =   43
            Tag             =   "CARGA"
            Top             =   990
            Width           =   1380
         End
         Begin VB.Label lblProducto 
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   3780
            TabIndex        =   41
            Tag             =   "CARGA"
            Top             =   300
            Width           =   5430
         End
         Begin VB.Label lblSucursal 
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   1995
            TabIndex        =   40
            Tag             =   "CARGA"
            Top             =   1950
            Width           =   1785
         End
         Begin VB.Label lblNumContrato 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   4485
            TabIndex        =   39
            Tag             =   "CARGA"
            Top             =   1920
            Width           =   1320
         End
         Begin VB.Label lblSeleccion 
            Alignment       =   2  'Center
            BackColor       =   &H00A26D00&
            Caption         =   "Remesa Seleccionada"
            ForeColor       =   &H00E0E0E0&
            Height          =   240
            Index           =   0
            Left            =   0
            TabIndex        =   38
            Top             =   10
            Width           =   9405
         End
         Begin VB.Label lblNombreCliente 
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   1155
            TabIndex        =   37
            Tag             =   "CARGA"
            Top             =   645
            Width           =   4395
         End
         Begin VB.Label lblRutCliente 
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   6915
            TabIndex        =   36
            Tag             =   "CARGA"
            Top             =   1965
            Width           =   1740
         End
         Begin VB.Label Label19 
            Caption         =   "Entidad"
            Height          =   240
            Left            =   120
            TabIndex        =   35
            Top             =   690
            Width           =   840
         End
         Begin VB.Label Label18 
            Caption         =   "Contrato"
            Height          =   240
            Left            =   3825
            TabIndex        =   34
            Top             =   1980
            Width           =   840
         End
         Begin VB.Label Label17 
            Caption         =   "Sucursal"
            Height          =   240
            Left            =   1335
            TabIndex        =   33
            Top             =   2010
            Width           =   840
         End
         Begin VB.Label lblTitFormato 
            Caption         =   "Tipo Formato"
            Height          =   240
            Left            =   5640
            TabIndex        =   32
            Top             =   720
            Width           =   960
         End
         Begin VB.Label Label13 
            Caption         =   "Fecha Recep"
            Height          =   240
            Left            =   120
            TabIndex        =   31
            Top             =   1050
            Width           =   1020
         End
         Begin VB.Label Label12 
            Caption         =   "Tipo Archivo"
            Height          =   240
            Left            =   2730
            TabIndex        =   30
            Top             =   360
            Width           =   1230
         End
         Begin VB.Label Label11 
            Caption         =   "Origen"
            Height          =   240
            Left            =   7170
            TabIndex        =   29
            Top             =   1080
            Width           =   540
         End
         Begin VB.Label Label10 
            Caption         =   "Estado"
            Height          =   240
            Left            =   2685
            TabIndex        =   28
            Top             =   1050
            Width           =   720
         End
      End
      Begin VB.Frame fraSeleccion 
         Height          =   1815
         Index           =   4
         Left            =   -74940
         TabIndex        =   19
         Top             =   360
         Width           =   9375
         Begin VB.ComboBox cboEntidad 
            Height          =   315
            Left            =   1200
            Style           =   2  'Dropdown List
            TabIndex        =   86
            Top             =   660
            Width           =   4100
         End
         Begin VB.TextBox txtNumRemesa 
            Height          =   315
            Left            =   1200
            TabIndex        =   0
            Top             =   315
            Width           =   1215
         End
         Begin VB.CommandButton cmdBuscar 
            Caption         =   "&Buscar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   435
            Left            =   8235
            TabIndex        =   8
            Top             =   1140
            Width           =   1035
         End
         Begin VB.ComboBox cboFormato 
            Height          =   315
            Left            =   6660
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   660
            Width           =   2670
         End
         Begin VB.CheckBox chkDigitando 
            Caption         =   "Digitando"
            Height          =   255
            Left            =   6360
            TabIndex        =   7
            Top             =   1395
            Width           =   975
         End
         Begin VB.ComboBox cboProducto 
            Height          =   315
            Left            =   3660
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Top             =   300
            Width           =   5670
         End
         Begin VB.ComboBox cboOrigen 
            Height          =   315
            Left            =   4680
            Style           =   2  'Dropdown List
            TabIndex        =   6
            Top             =   1380
            Width           =   1680
         End
         Begin VB.ComboBox cboEstado 
            Height          =   315
            Left            =   1200
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   1380
            Width           =   2655
         End
         Begin MSComCtl2.DTPicker dtpFechaRecepIni 
            Height          =   315
            Left            =   1200
            TabIndex        =   3
            Top             =   1020
            Width           =   1515
            _ExtentX        =   2672
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            Format          =   58720257
            CurrentDate     =   38727
         End
         Begin MSComCtl2.DTPicker dtpFechaRecepFin 
            Height          =   315
            Left            =   3840
            TabIndex        =   4
            Top             =   1020
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            Format          =   58720257
            CurrentDate     =   38727
         End
         Begin VB.Label lblComun 
            AutoSize        =   -1  'True
            Caption         =   "Entidad"
            Height          =   210
            Index           =   0
            Left            =   120
            TabIndex        =   85
            Top             =   735
            Width           =   585
         End
         Begin VB.Label Label31 
            Caption         =   "N� Archivo"
            Height          =   240
            Left            =   120
            TabIndex        =   49
            Top             =   360
            Width           =   840
         End
         Begin VB.Label lblSeleccion 
            Alignment       =   2  'Center
            BackColor       =   &H00A26D00&
            Caption         =   "Criterio de Selecci�n"
            ForeColor       =   &H00E0E0E0&
            Height          =   240
            Index           =   4
            Left            =   0
            TabIndex        =   26
            Top             =   10
            Width           =   9405
         End
         Begin VB.Label Label4 
            Caption         =   "Tipo Formato"
            Height          =   240
            Left            =   5565
            TabIndex        =   25
            Top             =   720
            Width           =   960
         End
         Begin VB.Label Label5 
            Caption         =   "Recep Desde"
            Height          =   240
            Left            =   120
            TabIndex        =   24
            Top             =   1080
            Width           =   1020
         End
         Begin VB.Label Label6 
            Caption         =   "Recep Hasta"
            Height          =   240
            Left            =   2820
            TabIndex        =   23
            Top             =   1080
            Width           =   1020
         End
         Begin VB.Label Label9 
            Caption         =   "Tipo Archivo"
            Height          =   240
            Left            =   2580
            TabIndex        =   22
            Top             =   360
            Width           =   2520
         End
         Begin VB.Label Label7 
            Caption         =   "Origen"
            Height          =   240
            Left            =   4020
            TabIndex        =   21
            Top             =   1440
            Width           =   540
         End
         Begin VB.Label Label8 
            Caption         =   "Estado"
            Height          =   240
            Left            =   120
            TabIndex        =   20
            Top             =   1440
            Width           =   720
         End
      End
      Begin FPSpread.vaSpread grdRegistroArch 
         Height          =   3255
         Left            =   -74940
         TabIndex        =   9
         Top             =   2400
         Width           =   9330
         _Version        =   131077
         _ExtentX        =   16457
         _ExtentY        =   5741
         _StockProps     =   64
         DisplayRowHeaders=   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   34
         MaxRows         =   1
         OperationMode   =   2
         ScrollBarExtMode=   -1  'True
         SelectBlockOptions=   0
         SpreadDesigner  =   "frmCargaDigitacion.frx":0044
         UserResize      =   1
         VisibleCols     =   8
         VScrollSpecial  =   -1  'True
      End
      Begin VB.Frame fraBotones 
         Height          =   735
         Index           =   4
         Left            =   -74940
         TabIndex        =   18
         Top             =   5640
         Width           =   9360
         Begin VB.CommandButton cmdEliminarArch 
            Caption         =   "&Eliminar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   400
            Left            =   2955
            TabIndex        =   12
            Top             =   210
            Width           =   1425
         End
         Begin VB.CommandButton cmdNuevaCarga 
            Caption         =   "&Cargar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   400
            Left            =   110
            TabIndex        =   10
            Top             =   210
            Width           =   1425
         End
         Begin VB.CommandButton cmdSalir 
            Caption         =   "&Salir"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   400
            Left            =   7815
            TabIndex        =   13
            Top             =   210
            Width           =   1425
         End
         Begin VB.CommandButton cmdNuevaDigita 
            Caption         =   "&Digitar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   400
            Left            =   1530
            TabIndex        =   11
            Top             =   210
            Visible         =   0   'False
            Width           =   1425
         End
      End
      Begin TabDlg.SSTab tabDetErr 
         Height          =   3585
         Left            =   90
         TabIndex        =   50
         Top             =   2100
         Width           =   9300
         _ExtentX        =   16404
         _ExtentY        =   6324
         _Version        =   393216
         TabOrientation  =   1
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "frmCargaDigitacion.frx":0AD0
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraAvances"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "grdDetalle"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "hsbMueveEnc"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "hsbMueveTot"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "grdEncab"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "grdTotal"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Errores"
         TabPicture(1)   =   "frmCargaDigitacion.frx":0AEC
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdErrTotal"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "grdErrEncab"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "grdErrDetalle"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "hsbMueveErrEnc"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).Control(4)=   "hsbMueveErrTot"
         Tab(1).Control(4).Enabled=   0   'False
         Tab(1).Control(5)=   "lvwErrores"
         Tab(1).Control(5).Enabled=   0   'False
         Tab(1).ControlCount=   6
         Begin FPSpread.vaSpread grdTotal 
            CausesValidation=   0   'False
            Height          =   270
            Left            =   0
            TabIndex        =   54
            Top             =   2940
            Width           =   8655
            _Version        =   131077
            _ExtentX        =   15266
            _ExtentY        =   476
            _StockProps     =   64
            BackColorStyle  =   2
            ColHeaderDisplay=   0
            DisplayColHeaders=   0   'False
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridShowHoriz   =   0   'False
            MaxCols         =   0
            MaxRows         =   1
            RowHeaderDisplay=   0
            ScrollBarExtMode=   -1  'True
            ScrollBars      =   0
            SpreadDesigner  =   "frmCargaDigitacion.frx":0B08
            UserResize      =   1
            VisibleCols     =   500
            VisibleRows     =   1
         End
         Begin FPSpread.vaSpread grdEncab 
            CausesValidation=   0   'False
            Height          =   270
            Left            =   0
            TabIndex        =   64
            Top             =   15
            Width           =   8655
            _Version        =   131077
            _ExtentX        =   15266
            _ExtentY        =   476
            _StockProps     =   64
            BackColorStyle  =   2
            ColHeaderDisplay=   0
            DisplayColHeaders=   0   'False
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridShowHoriz   =   0   'False
            MaxCols         =   0
            MaxRows         =   1
            RowHeaderDisplay=   0
            ScrollBarExtMode=   -1  'True
            ScrollBars      =   0
            SpreadDesigner  =   "frmCargaDigitacion.frx":0D44
            UserResize      =   1
            VisibleCols     =   500
            VisibleRows     =   1
         End
         Begin MSComctlLib.ListView lvwErrores 
            Height          =   3195
            Left            =   -67680
            TabIndex        =   71
            Top             =   30
            Width           =   1920
            _ExtentX        =   3387
            _ExtentY        =   5636
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   255
            BackColor       =   12648447
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial Narrow"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "DETALLE DE ERRORES"
               Object.Width           =   17639
            EndProperty
         End
         Begin VB.HScrollBar hsbMueveErrTot 
            Height          =   285
            Left            =   -68220
            TabIndex        =   70
            Top             =   2940
            Width           =   525
         End
         Begin VB.HScrollBar hsbMueveErrEnc 
            Height          =   285
            Left            =   -68250
            TabIndex        =   69
            Top             =   15
            Width           =   525
         End
         Begin VB.HScrollBar hsbMueveTot 
            Height          =   285
            Left            =   8685
            TabIndex        =   68
            Top             =   2940
            Width           =   525
         End
         Begin VB.HScrollBar hsbMueveEnc 
            Height          =   285
            Left            =   8685
            TabIndex        =   67
            Top             =   15
            Width           =   525
         End
         Begin FPSpread.vaSpread grdDetalle 
            Height          =   2580
            Left            =   0
            TabIndex        =   55
            Top             =   330
            Width           =   9225
            _Version        =   131077
            _ExtentX        =   16272
            _ExtentY        =   4551
            _StockProps     =   64
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridSolid       =   0   'False
            MaxCols         =   100
            MaxRows         =   1
            OperationMode   =   2
            ScrollBarExtMode=   -1  'True
            SelectBlockOptions=   0
            SpreadDesigner  =   "frmCargaDigitacion.frx":0F80
            VirtualMaxRows  =   25
            VirtualMode     =   -1  'True
            VirtualOverlap  =   100
            VirtualRows     =   100
            VirtualScrollBuffer=   -1  'True
         End
         Begin FPSpread.vaSpread grdErrDetalle 
            Height          =   2580
            Left            =   -75000
            TabIndex        =   63
            Top             =   330
            Width           =   7290
            _Version        =   131077
            _ExtentX        =   12859
            _ExtentY        =   4551
            _StockProps     =   64
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridSolid       =   0   'False
            MaxCols         =   99
            MaxRows         =   1
            OperationMode   =   2
            ScrollBarExtMode=   -1  'True
            SelectBlockOptions=   0
            SpreadDesigner  =   "frmCargaDigitacion.frx":116E
            VirtualMaxRows  =   25
            VirtualMode     =   -1  'True
            VirtualOverlap  =   100
            VirtualRows     =   100
            VirtualScrollBuffer=   -1  'True
            VisibleCols     =   99
         End
         Begin FPSpread.vaSpread grdErrEncab 
            CausesValidation=   0   'False
            Height          =   270
            Left            =   -75000
            TabIndex        =   65
            Top             =   15
            Width           =   6720
            _Version        =   131077
            _ExtentX        =   11853
            _ExtentY        =   476
            _StockProps     =   64
            BackColorStyle  =   2
            ColHeaderDisplay=   0
            DisplayColHeaders=   0   'False
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridShowHoriz   =   0   'False
            MaxCols         =   0
            MaxRows         =   1
            RowHeaderDisplay=   0
            ScrollBarExtMode=   -1  'True
            ScrollBars      =   0
            SpreadDesigner  =   "frmCargaDigitacion.frx":137D
            UserResize      =   1
            VisibleCols     =   500
            VisibleRows     =   1
         End
         Begin FPSpread.vaSpread grdErrTotal 
            CausesValidation=   0   'False
            Height          =   270
            Left            =   -75000
            TabIndex        =   66
            Top             =   2940
            Width           =   6720
            _Version        =   131077
            _ExtentX        =   11853
            _ExtentY        =   476
            _StockProps     =   64
            BackColorStyle  =   2
            ColHeaderDisplay=   0
            DisplayColHeaders=   0   'False
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridShowHoriz   =   0   'False
            MaxCols         =   0
            MaxRows         =   1
            RowHeaderDisplay=   0
            ScrollBarExtMode=   -1  'True
            ScrollBars      =   0
            SpreadDesigner  =   "frmCargaDigitacion.frx":15B9
            UserResize      =   1
            VisibleCols     =   500
            VisibleRows     =   1
         End
         Begin VB.Frame fraAvances 
            BorderStyle     =   0  'None
            Height          =   3030
            Left            =   8010
            TabIndex        =   72
            Top             =   90
            Visible         =   0   'False
            Width           =   1185
            Begin VB.PictureBox pctAvance 
               Appearance      =   0  'Flat
               AutoRedraw      =   -1  'True
               BackColor       =   &H00C0C0C0&
               BorderStyle     =   0  'None
               FillStyle       =   0  'Solid
               ForeColor       =   &H80000008&
               Height          =   120
               Left            =   135
               Picture         =   "frmCargaDigitacion.frx":17F5
               ScaleHeight     =   120
               ScaleWidth      =   960
               TabIndex        =   73
               Top             =   2235
               Visible         =   0   'False
               Width           =   960
            End
            Begin VB.Label lblCrono 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0 "
               ForeColor       =   &H00404040&
               Height          =   225
               Left            =   45
               TabIndex        =   84
               Tag             =   "CARGA"
               Top             =   2700
               Width           =   1140
            End
            Begin VB.Label lbl 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Cron�metro"
               ForeColor       =   &H00404040&
               Height          =   210
               Index           =   2
               Left            =   240
               TabIndex        =   83
               Top             =   2505
               Width           =   840
            End
            Begin VB.Label lbl 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "% Avance"
               ForeColor       =   &H00404040&
               Height          =   255
               Index           =   0
               Left            =   300
               TabIndex        =   82
               Top             =   1995
               Width           =   765
            End
            Begin VB.Shape Shape1 
               Height          =   225
               Left            =   45
               Top             =   2190
               Width           =   1140
            End
            Begin VB.Label lbl 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Monto Total"
               ForeColor       =   &H00404040&
               Height          =   210
               Index           =   8
               Left            =   210
               TabIndex        =   81
               Top             =   1515
               Width           =   855
            End
            Begin VB.Label lblTotDoc 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0 "
               ForeColor       =   &H00404040&
               Height          =   225
               Left            =   45
               TabIndex        =   80
               Tag             =   "CARGA"
               Top             =   1710
               Width           =   1140
            End
            Begin VB.Label lblErroneos 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0 "
               ForeColor       =   &H00404040&
               Height          =   225
               Left            =   45
               TabIndex        =   79
               Tag             =   "CARGA"
               Top             =   1200
               Width           =   1140
            End
            Begin VB.Label lbl 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Err�neos"
               ForeColor       =   &H00404040&
               Height          =   255
               Index           =   5
               Left            =   285
               TabIndex        =   78
               Top             =   1005
               Width           =   660
            End
            Begin VB.Label lblCorrectos 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0 "
               ForeColor       =   &H00404040&
               Height          =   225
               Left            =   45
               TabIndex        =   77
               Tag             =   "CARGA"
               Top             =   720
               Width           =   1140
            End
            Begin VB.Label lbl 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "V�lidos"
               ForeColor       =   &H00404040&
               Height          =   195
               Index           =   4
               Left            =   300
               TabIndex        =   76
               Top             =   525
               Width           =   510
            End
            Begin VB.Label lblLeidos 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0 "
               ForeColor       =   &H00404040&
               Height          =   225
               Left            =   45
               TabIndex        =   75
               Tag             =   "CARGA"
               Top             =   240
               Width           =   1140
            End
            Begin VB.Label lbl 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Le�dos"
               ForeColor       =   &H00404040&
               Height          =   255
               Index           =   3
               Left            =   390
               TabIndex        =   74
               Top             =   45
               Width           =   480
            End
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   -75000
            TabIndex        =   51
            Top             =   2230
            Width           =   6150
         End
      End
      Begin VB.Label lblTituloRemesas 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00A26D00&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " Detalle"
         ForeColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   105
         TabIndex        =   52
         Top             =   1800
         Width           =   9285
      End
      Begin VB.Label lblLista 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Lista  de Archivos"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   4
         Left            =   -74940
         TabIndex        =   17
         Top             =   2160
         Width           =   9345
      End
   End
End
Attribute VB_Name = "frmCargaDigitacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    

Public Sub Buscar_Remesas()
Dim strAux As String
Dim lngIdCliente As Long
Dim lngIdContrato As Long
Dim lngIdSucursal As Long
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
On Error GoTo LabelError

    staEstado.Panels(1) = "Buscando..."
    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.pkg_fme_carga_digitacion.sp_fme_registros_remesas_ver"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, Val(txtNumRemesa.Text))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Cliente", adNumeric, adParamInput, 10, cboEntidad.ItemData(cboEntidad.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Producto", adNumeric, adParamInput, 10, cboProducto.ItemData(cboProducto.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Contrato", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Sucursal", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_Tipo_Formato", adChar, adParamInput, 8, cboFormato.ItemData(cboFormato.ListIndex))
    If cboOrigen.ListIndex <> 0 Then
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_Origen", adChar, adParamInput, 8, garrOrigen(cboOrigen.ListIndex, 0))
    Else
        Cmd.Parameters.Append Cmd.CreateParameter("p_c_Origen", adChar, adParamInput, 8, 0)
    End If
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_flgDigitando", adChar, adParamInput, 1, Val(chkDigitando.Value))
    Cmd.Parameters.Append Cmd.CreateParameter("p_d_FechaRecepIni", adDate, adParamInput, 10, IIf(IsNull(dtpFechaRecepIni), Null, Format(dtpFechaRecepIni, "dd/mm/yyyy")))
    Cmd.Parameters.Append Cmd.CreateParameter("p_d_FechaRecepFin", adDate, adParamInput, 10, IIf(IsNull(dtpFechaRecepFin), Null, Format(dtpFechaRecepFin, "dd/mm/yyyy")))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_CodEstado", adChar, adParamInput, 8, cboEstado.ItemData(cboEstado.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute

    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser cargada. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbInformation, "Carga/Digitaci�n"
        GoTo LabelSalir
    End If
                    
    grdRegistroArch.MaxRows = 0
    While Not recRegistros.EOF
        grdRegistroArch.MaxRows = grdRegistroArch.MaxRows + 1
        grdRegistroArch.Row = grdRegistroArch.MaxRows
                      
        grdRegistroArch.Col = 1: grdRegistroArch.Text = "" & recRegistros.Fields("IDE_EMPRESA").Value
        grdRegistroArch.Col = 2: grdRegistroArch.Text = "" & recRegistros.Fields("IDE_REMESA").Value
        grdRegistroArch.Col = 3: grdRegistroArch.Text = "" & recRegistros.Fields("IDE_PRODUCTO").Value
        grdRegistroArch.Col = 4: grdRegistroArch.Text = "" & recRegistros.Fields("GLS_producto").Value
        grdRegistroArch.Col = 6: grdRegistroArch.Text = "" & recRegistros.Fields("ide_ENTIDAD").Value
        grdRegistroArch.Col = 7: grdRegistroArch.Text = "" & recRegistros.Fields("GLS_entidad").Value
        grdRegistroArch.Col = 10: grdRegistroArch.Text = "" & recRegistros.Fields("COD_TIPO_ORIGEN").Value
        grdRegistroArch.Col = 11: grdRegistroArch.Text = "" & recRegistros.Fields("DESC_TIPOORIG").Value
        grdRegistroArch.Col = 12
        If Not IsNull(recRegistros.Fields("FLG_FIN_DIGITACION").Value) Then
            If Trim(recRegistros.Fields("FLG_FIN_DIGITACION").Value) = "1" Then
                grdRegistroArch.Text = "SI"
            Else
                grdRegistroArch.Text = "NO"
            End If
        End If
        grdRegistroArch.Col = 13: grdRegistroArch.Text = "" & recRegistros.Fields("COD_TIPO_ARCHIVO").Value
        grdRegistroArch.Col = 14: grdRegistroArch.Text = "" & recRegistros.Fields("DESC_TIPOARCH").Value
        grdRegistroArch.Col = 15: grdRegistroArch.Text = "" & recRegistros.Fields("COD_TIPO_FORMATO").Value
        grdRegistroArch.Col = 16: grdRegistroArch.Text = "" & recRegistros.Fields("DESC_TIPOFORMA").Value
        grdRegistroArch.Col = 17: grdRegistroArch.Text = "" & recRegistros.Fields("GLS_OBSERVACION").Value
        grdRegistroArch.Col = 18: grdRegistroArch.Text = "" & recRegistros.Fields("GLS_RUTA_ARCHIVO").Value
        grdRegistroArch.Col = 19: grdRegistroArch.Text = "" & recRegistros.Fields("FCH_RECEPCION").Value
        If IsDate(grdRegistroArch.Text) Then grdRegistroArch.Text = Format(grdRegistroArch.Text, "dd/mm/yyyy")
        grdRegistroArch.Col = 20: grdRegistroArch.Text = "" & recRegistros.Fields("NRO_DOCUMENTOS").Value
        grdRegistroArch.Col = 21: grdRegistroArch.Text = "" & recRegistros.Fields("MTO_DOCUMENTOS").Value
        grdRegistroArch.Text = Format(grdRegistroArch.Text, "#,##0.0000")
        grdRegistroArch.Col = 22: grdRegistroArch.Text = "" & recRegistros.Fields("NRO_DOCTOS_ACEPTADOS").Value
        grdRegistroArch.Col = 23: grdRegistroArch.Text = "" & recRegistros.Fields("COD_ESTADO_REMESA").Value
        grdRegistroArch.Col = 24: grdRegistroArch.Text = "" & recRegistros.Fields("DESC_ESTADOREM").Value
        grdRegistroArch.Col = 25: grdRegistroArch.Text = "" & recRegistros.Fields("FCH_HRA_MOD_ESTADO").Value
        grdRegistroArch.Col = 26: grdRegistroArch.Text = "" & recRegistros.Fields("COD_USR_MOD_ESTADO").Value
        grdRegistroArch.Col = 27: grdRegistroArch.Text = "" & recRegistros.Fields("FCH_HRA_CREACION").Value
        grdRegistroArch.Col = 28: grdRegistroArch.Text = "" & recRegistros.Fields("COD_USR_CREACION").Value
        grdRegistroArch.Col = 29: grdRegistroArch.Text = "" & recRegistros.Fields("FCH_HRA_MODIFICA").Value
        grdRegistroArch.Col = 30: grdRegistroArch.Text = "" & recRegistros.Fields("COD_USR_MODIFICA").Value
        grdRegistroArch.Col = 31: grdRegistroArch.Text = "" & recRegistros.Fields("IDE_ENTIDAD").Value
'        grdRegistroArch.Col = 33: grdRegistroArch.Text = "" & recRegistros.Fields("flg_acepta_erronea").Value
'        grdRegistroArch.Col = 34: grdRegistroArch.Text = "" & recRegistros.Fields("flg_modifica_rms").Value
        recRegistros.MoveNext
    Wend
    
    If grdRegistroArch.MaxRows > 0 Then
        grdRegistroArch.Row = 1
        Call HabilitarTabs(tabSeciones, "0;1;", 0)
    Else
        Call HabilitarTabs(tabSeciones, "0;", 0)
    End If
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If

End Sub

Private Sub EstadoToolBar()

    tlbAcciones.Buttons("AgregarDoc").Enabled = cmdAgregar.Enabled
    tlbAcciones.Buttons("ModificarDoc").Enabled = cmdModificar.Enabled
    tlbAcciones.Buttons("EliminarDoc").Enabled = cmdEliminar.Enabled
    tlbAcciones.Buttons("Validar").Enabled = cmdValidar.Enabled
    tlbAcciones.Buttons("Proceso").Enabled = cmdProceso.Enabled

End Sub

Public Sub Mostrar_Errores(grdGrilla As vaSpread, lngRow As Long, intCol As Integer)
    Dim arrErrores As Variant
    Dim lngInd As Long
    
    grdGrilla.Row = lngRow
    grdGrilla.Col = intCol

    arrErrores = Split(grdGrilla.Text, vbCrLf)
    lvwErrores.ListItems.Clear
    For lngInd = 0 To UBound(arrErrores)
       Call lvwErrores.ListItems.Add(, , arrErrores(lngInd))
    Next
    
End Sub

'Private Sub cboNombreEnt_Click()
'    cboRutEnt.ListIndex = cboNombreEnt.ListIndex
'End Sub

'Private Sub cboNombreEnt_GotFocus()
''    cboNombreEnt.Text = ""
''    cboRutEnt.ListIndex = -1
'    cboProducto.ListIndex = 0
'End Sub

'Private Sub cboNombreEnt_KeyPress(KeyAscii As Integer)
'    cboRutEnt.Clear
'End Sub

Private Sub cboOrigen_Click()
Dim strOrigen As String
    
    If cboOrigen.ListIndex = -1 Then Exit Sub
    strOrigen = Trim(garrOrigen(cboOrigen.ListIndex, 2))
    
    If Trim(UCase(strOrigen)) = "DIG" Then
        chkDigitando.Enabled = True
        chkDigitando.Value = 0
    Else
        chkDigitando.Enabled = False
        chkDigitando.Value = 0
    End If
    
End Sub

'Private Sub cboRutEnt_Click()
'    cboNombreEnt.ListIndex = cboRutEnt.ListIndex
'End Sub
'
'Private Sub cboRutEnt_GotFocus()
'    cboRutEnt.Text = ""
'    cboNombreEnt.ListIndex = -1
'    CboContrato.Clear
'    cboProducto.ListIndex = 0
'    cboFormato.ListIndex = 0
'    cboOrigen.ListIndex = 0
'    cboEstado.ListIndex = 0
'    dtpFechaRecepIni = Format(Date, "dd/mm/yyyy")
'    dtpFechaRecepFin = Format(Date, "dd/mm/yyyy")
'    dtpFechaRecepIni.Value = Null
'    dtpFechaRecepFin.Value = Null
'    txtNumRemesa.Text = ""
'    grdRegistroArch.MaxRows = 0
'    Call HabilitarTabs(tabSeciones, "0;", 0)
'End Sub

'Private Sub cboRutEnt_KeyPress(KeyAscii As Integer)
'    If KeyAscii = 13 Then cmdBusEnt_Click
'    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
'        KeyAscii = 0
'    Else
'        cboNombreEnt.Clear
'        If cboRutEnt.ListCount > 0 Then cboRutEnt.Clear
'    End If
'End Sub

'Private Sub cmdAgregar_Click()
'    Dim blnExiteEstructura As Boolean
'    Dim lngIdeCliente As Long
'    Dim lngIdeProducto As Long
'    Dim lngIdeSucursal As Long
'    Dim lngIdeRemesa As Long
'    Dim lngIdeContrato As Long
'    Dim strCodTipoArchivo As String
'    Dim strCodTipoFormato As String
'
'    On Error GoTo LabelError
'
'  '  If Not Autorizar(sfagregar) Then Exit Sub
'
'    gblnModifica = False
'    gblnswichRefresca = False
'
'    Call Carga_Combo_Corp(frmDigitacion.GLS_COMUNA, garrComunas)
'    Call Carga_Combo_Corp(frmDigitacion.GLS_CIUDAD, garrComunas)
'    Call Carga_Combo_Corp(frmDigitacion.COD_BANCO, garrBancos)
'    Call Carga_Combo_Corp(frmDigitacion.COD_PLAZA, garrPlazas)
'    Call Carga_Combo_Corp(frmDigitacion.COD_PAIS, garrPaises)
'    Call Carga_Combo_Corp(frmDigitacion.COD_MONEDA, garrMonedas)
'    Call Carga_Combo_Corp(frmDigitacion.COD_TIPO_DCTO, garrDocumentos)
'    Call Carga_Combo_Par(frmDigitacion.COD_FORMA_PAGO, 1, 12)
'
'    Call LimpiarControles(frmDigitacion, "DIG")
'    Call EstableceEstrucFormulario(frmDigitacion, "DIG")
'    frmDigitacion.lblRutCliente = lblRutCliente
'    frmDigitacion.lblNombreCliente = lblNombreCliente
'    frmDigitacion.lblNumRemesa = lblNumRemesa
'    frmDigitacion.lblProducto = lblProducto
'    frmDigitacion.lblSucursal = lblSucursal
'    frmDigitacion.lblNumContrato = lblNumContrato
'    frmDigitacion.lblFechaRecep = lblFechaRecep
'    frmDigitacion.lbEstado = lbEstado
'    frmDigitacion.lblFormato = lblFormato
'    frmDigitacion.lblOrigen = lblOrigen
'    frmDigitacion.chkDigita.Value = chkDigita.Value
'    frmDigitacion.chkDigita.Enabled = False
''   frmDigitacion.lblTipoArchivo = lblTipoArchivo
'    frmDigitacion.Show 1
'    If gblnswichRefresca Then
'        staEstado.Panels(1) = "Buscando..."
'        Screen.MousePointer = 11
'        Call TraeArchivoCliente(frmCargaDigitacion.grdEncab, frmCargaDigitacion.grdDetalle, frmCargaDigitacion.grdTotal, lngIdeRemesa, lngIdeContrato)
'    End If
'LabelSalir:
'    Exit Sub
'LabelError:
'    If Not Err.Number = 0 Then
'        MsgBox Err.Description, vbCritical, "Error"
'        Err.Clear
'        GoTo LabelSalir
'        Resume 0
'    End If
'End Sub
'
Private Sub cmdEliminar_Click()
    Dim lngNumLinea As Long
    Dim Cmd As New ADODB.Command
    
    On Error GoTo LabelError

 '   If Not Autorizar(sfeliminar) Then Exit Sub
    
    If grdDetalle.MaxRows = 0 Or grdDetalle.Row = 0 Then Exit Sub
    
    If MsgBox("�Est� seguro de Eliminar L�nea de archivo seleccionada?", vbYesNo + vbQuestion, Me.Caption) <> vbYes Then
        GoTo LabelSalir
    End If
        
    staEstado.Panels(1) = "Eliminando..."
    Screen.MousePointer = 11
    
    grdDetalle.Row = grdDetalle.ActiveRow
    grdDetalle.Col = grdDetalle.MaxCols
    lngNumLinea = grdDetalle.Text
    
    Rem llamada a Procedimiento
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "DBO_FACTOR.PKG_FME_CARGA_DIGITACION.SP_FME_REGISTRO_ARCHIVO_BORRAR"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Contrato", adNumeric, adParamInput, 10, Val(lblNumContrato))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeRemesa", adNumeric, adParamInput, 10, Val(lblNumRemesa))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_NroLinea", adNumeric, adParamInput, 10, lngNumLinea)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Cmd.Execute

    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "El registro no pudo ser Eliminado. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbExclamation, Me.Caption
        GoTo LabelSalir
    End If

    MsgBox "Registro Eliminado Exitosamente.", vbInformation, Me.Caption
    
    staEstado.Panels(1) = "Buscando..."
    Screen.MousePointer = 11
    Call TraeArchivoCliente(frmCargaDigitacion.grdEncab, frmCargaDigitacion.grdDetalle, frmCargaDigitacion.grdTotal, Val(lblNumRemesa), Val(lblNumContrato))
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub cmdEliminarArch_Click()
    Dim lngRemesa As Long
    Dim strProducto As String
    Dim lngNumContrato As String
    Dim strCliente As String
    Dim Cmd As New ADODB.Command
    
    On Error GoTo LabelError

    
    If grdRegistroArch.MaxRows = 0 Or grdRegistroArch.Row = 0 Then Exit Sub
    'If Not Autorizar(Autorizaciones.sfeliminar) Then Exit Sub
    
    staEstado.Panels(1) = "Eliminando..."
    Screen.MousePointer = 11
    
    grdRegistroArch.Row = grdRegistroArch.ActiveRow
    grdRegistroArch.Col = 2: lngRemesa = Val(grdRegistroArch.Text)
    grdRegistroArch.Col = 4: strProducto = Trim(grdRegistroArch.Text)
    grdRegistroArch.Col = 5: lngNumContrato = Val(grdRegistroArch.Text)
    grdRegistroArch.Col = 6: strCliente = Trim(grdRegistroArch.Text)
    grdRegistroArch.Col = 7
    strCliente = strCliente & "  " & Trim(grdRegistroArch.Text)
    If MsgBox("�Est� seguro de Eliminar el Archivo ?" & Chr(13) & "  N�  : " & lngRemesa & Chr(13) & _
              "  Tipo de Archivo     : " & strProducto & Chr(13) & _
              "  Entidad         : " & strCliente, vbYesNo + vbQuestion, Me.Caption) <> vbYes Then
        GoTo LabelSalir
    Else
        Rem llamada a Procedimiento
        Cmd.ActiveConnection = gAdoCon
        Cmd.CommandType = adCmdStoredProc
        Cmd.CommandText = "csbpi.PKG_FME_CARGA_DIGITACION.SP_FME_ARCHIVO_BORRAR"
        
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Contrato", adNumeric, adParamInput, 10, lngNumContrato)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeRemesa", adNumeric, adParamInput, 10, lngRemesa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adChar, adParamInput, 8, gstrUsuario)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
        Cmd.Execute
    
        If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
            MsgBox "El registro no pudo ser Eliminado. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbExclamation, Me.Caption
            GoTo LabelSalir
        End If
    
        MsgBox "Registro Eliminado Exitosamente.", vbInformation, Me.Caption
        Call cmdBuscar_Click
    End If

LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

'Private Sub cmdModificar_Click()
'    Dim intOperatoria As Integer
'    Dim lngNumLinea As Long
'    Dim recRegistros As New ADODB.Recordset
'    Dim Cmd As New ADODB.Command
'
'    On Error GoTo LabelError
'
' '   If Not Autorizar(sfmodificar) Then Exit Sub
'
'    gblnModifica = True
'    gblnswichRefresca = False
'
'    grdDetalle.Row = grdDetalle.ActiveRow
'    grdDetalle.Col = grdDetalle.MaxCols
'    lngNumLinea = Val(grdDetalle.Text)
'
'    Cmd.ActiveConnection = gAdoCon
'    Cmd.CommandType = adCmdStoredProc
'    Cmd.CommandText = "DBO_FACTOR.PKG_FME_CARGA_DIGITACION.SP_FME_ARCHIVO_CLIENTE_VER"
'
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, Val(lblNumRemesa))
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ContratoMarco", adNumeric, adParamInput, 10, Val(lblNumContrato))
'    Rem p_n_TipoSeccion = 1 Encabezado y Total.
'    Rem                 = 2 Detalle.
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_TipoSeccion", adNumeric, adParamInput, 10, 2)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_NroLinea", adNumeric, adParamInput, 10, lngNumLinea)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
'    Set recRegistros = Cmd.Execute
'
'    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
'        MsgBox "La informaci�n no pudo ser cargada. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbInformation, Me.Caption
'        GoTo LabelSalir
'    End If
'
'    If Not recRegistros.EOF Then
'        Call Carga_Combo_Corp(frmDigitacion.GLS_COMUNA, garrComunas)
'        Call Carga_Combo_Corp(frmDigitacion.GLS_CIUDAD, garrComunas)
'
''        Call LimpiarControles(frmDigitacion, "DIG")
'
'        Call Busca_Cod_Glosa(frmDigitacion.GLS_COMUNA, IIf(IsNull(recRegistros("GLS_COMUNA").Value), 0, Trim(recRegistros("GLS_COMUNA").Value)))
'        Call Busca_Cod_Glosa(frmDigitacion.GLS_CIUDAD, IIf(IsNull(recRegistros("GLS_CIUDAD").Value), 0, Trim(recRegistros("GLS_CIUDAD").Value)))
'        Call Carga_Combo_Corp(frmDigitacion.COD_BANCO, garrBancos, IIf(IsNull(recRegistros("COD_BANCO").Value), 0, Trim(recRegistros("COD_BANCO").Value)))
'        Call Carga_Combo_Corp(frmDigitacion.COD_PLAZA, garrPlazas, IIf(IsNull(recRegistros("COD_PLAZA").Value), 0, Trim(recRegistros("COD_PLAZA").Value)))
'        Call Carga_Combo_Corp(frmDigitacion.COD_PAIS, garrPaises, IIf(IsNull(recRegistros("COD_PAIS").Value), 0, Trim(recRegistros("COD_PAIS").Value)))
'        Call Carga_Combo_Corp(frmDigitacion.COD_MONEDA, garrMonedas, IIf(IsNull(recRegistros("COD_MONEDA").Value), 0, Trim(recRegistros("COD_MONEDA").Value)))
'        Call Carga_Combo_Corp(frmDigitacion.COD_TIPO_DCTO, garrDocumentos, IIf(IsNull(recRegistros("COD_TIPO_DCTO").Value), 0, Trim(recRegistros("COD_TIPO_DCTO").Value)))
'        Call Carga_Combo_Par(frmDigitacion.COD_FORMA_PAGO, 1, 12, , IIf(IsNull(recRegistros("COD_FORMA_PAGO").Value), 0, Trim(recRegistros("COD_FORMA_PAGO").Value)))
'        frmDigitacion.RUT_EMISOR = "" & Trim(recRegistros("RUT_EMISOR").Value)
'        frmDigitacion.IDE_DV_EMISOR = "" & Trim(recRegistros("IDE_DV_EMISOR").Value)
'        frmDigitacion.NOM_EMISOR = "" & Trim(recRegistros("NOM_EMISOR").Value)
'        frmDigitacion.RUT_RECEPTOR = "" & Trim(recRegistros("RUT_RECEPTOR").Value)
'        frmDigitacion.IDE_DV_RECEPTOR = "" & Trim(recRegistros("IDE_DV_RECEPTOR").Value)
'        frmDigitacion.NOM_RECEPTOR = "" & Trim(recRegistros("NOM_RECEPTOR").Value)
'        frmDigitacion.GLS_DIRECCION = "" & Trim(recRegistros("GLS_DIRECCION").Value)
'        frmDigitacion.GLS_COD_POSTAL = "" & Trim(recRegistros("GLS_COD_POSTAL").Value)
'        frmDigitacion.GLS_TELEFONO = "" & Trim(recRegistros("GLS_TELEFONO").Value)
'        frmDigitacion.GLS_NUM_FAX = "" & Trim(recRegistros("GLS_NUM_FAX").Value)
'        frmDigitacion.GLS_CASILLA = "" & Trim(recRegistros("GLS_CASILLA").Value)
'        frmDigitacion.NRO_DOCUMENTO = "" & Trim(recRegistros("NRO_DOCUMENTO").Value)
'        frmDigitacion.NRO_CUOTA = "" & Trim(recRegistros("NRO_CUOTA").Value)
'        frmDigitacion.GLS_CTACTE = "" & Trim(recRegistros("GLS_CTACTE").Value)
'        frmDigitacion.MNT_DOCUMENTO = Format("" & Trim(recRegistros("MNT_DOCUMENTO").Value), "#,##0.0000")
'        If Not IsNull(Trim(recRegistros("FCH_EMISION").Value)) Then
'            frmDigitacion.FCH_EMISION = Trim(recRegistros("FCH_EMISION").Value)
'        End If
'        If Not IsNull(Trim(recRegistros("FCH_VENCIMIENTO").Value)) Then
'            frmDigitacion.FCH_VENCIMIENTO = Trim(recRegistros("FCH_VENCIMIENTO").Value)
'        End If
'        If Not IsNull(Trim(recRegistros("FCH_PAGO").Value)) Then
'            frmDigitacion.FCH_PAGO = Trim(recRegistros("FCH_PAGO").Value)
'        End If
'        frmDigitacion.GLS_OBSERVACION = "" & Trim(recRegistros("GLS_OBSERVACION").Value)
'        frmDigitacion.GLS_REFERENCIA_1 = "" & Trim(recRegistros("GLS_REFERENCIA_1").Value)
'        frmDigitacion.GLS_REFERENCIA_2 = "" & Trim(recRegistros("GLS_REFERENCIA_2").Value)
'        frmDigitacion.GLS_REFERENCIA_3 = "" & Trim(recRegistros("GLS_REFERENCIA_3").Value)
'
'        intOperatoria = IIf(IsNull(recRegistros("MRC_OPERATORIA").Value), 0, Trim(recRegistros("MRC_OPERATORIA").Value))
'        If intOperatoria = 1 Then
'            frmDigitacion.MRC_OPERATORIA = "SUMA"
'        ElseIf intOperatoria = 2 Then
'            frmDigitacion.MRC_OPERATORIA = "RESTA"
'        End If
'        frmDigitacion.MNT_ORIGINAL = Format("" & Trim(recRegistros("MNT_ORIGINAL").Value), "#,##0.0000")
'        frmDigitacion.MNT_IVA = Format("" & Trim(recRegistros("MNT_IVA").Value), "#,##0.0000")
'        frmDigitacion.MNT_NETO = Format("" & Trim(recRegistros("MNT_NETO").Value), "#,##0.0000")
'        frmDigitacion.MNT_EXENTO = Format("" & Trim(recRegistros("MNT_EXENTO").Value), "#,##0.0000")
'
'        Call EstableceEstrucFormulario(frmDigitacion, "DIG")
'
'        frmDigitacion.lblRutCliente = lblRutCliente
'        frmDigitacion.lblNombreCliente = lblNombreCliente
'        frmDigitacion.lblNumRemesa = lblNumRemesa
'        frmDigitacion.lblNumLinea = lngNumLinea
'        frmDigitacion.lblProducto = lblProducto
'        frmDigitacion.lblSucursal = lblSucursal
'        frmDigitacion.lblNumContrato = lblNumContrato
'        frmDigitacion.lblFechaRecep = lblFechaRecep
'        frmDigitacion.lbEstado = lbEstado
'        frmDigitacion.lblFormato = lblFormato
'        frmDigitacion.lblOrigen = lblOrigen
'        frmDigitacion.chkDigita.Value = chkDigita.Value
'        frmDigitacion.chkDigita.Enabled = False
''       frmDigitacion.lblTipoArchivo = lblTipoArchivo
'        frmDigitacion.Show 1
'        If gblnswichRefresca Then
'            staEstado.Panels(1) = "Buscando..."
'            Screen.MousePointer = 11
'            Call TraeArchivoCliente(frmCargaDigitacion.grdEncab, frmCargaDigitacion.grdDetalle, frmCargaDigitacion.grdTotal, Val(lblNumRemesa), Val(lblNumContrato))
'        End If
'    End If
'
'LabelSalir:
'    staEstado.Panels(1) = "Listo"
'    Screen.MousePointer = 0
'    Exit Sub
'LabelError:
'    If Not Err.Number = 0 Then
'        MsgBox Err.Description, vbCritical, "Error"
'        Err.Clear
'        GoTo LabelSalir
'        Resume 0
'    End If
'End Sub
'
Private Sub cmdNuevaCarga_Click()
    frmNvaCarga.Show 1
End Sub

Private Sub cmdBuscar_Click()
    Buscar_Remesas
End Sub


'Private Sub cmdBusEnt_Click()
'Dim recRegistros As New ADODB.Recordset
'Dim Cmd As New ADODB.Command
'Dim mlngRut As Long
'Dim mstrNombre As String
'On Error GoTo LabelError
'
'    If cboRutEnt.Text = Empty And cboNombreEnt.Text = Empty Then GoTo LabelSalir
'    Screen.MousePointer = vbHourglass
'
'    mlngRut = Val(cboRutEnt.Text)
'    mstrNombre = (cboNombreEnt.Text)
'
'    Cmd.ActiveConnection = gAdoCon
'    Cmd.CommandType = adCmdStoredProc
'    Cmd.CommandText = "DBO_FACTOR.pkg_fme_entidades.sp_fme_clientes_ver"
'
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_rut", adNumeric, adParamInput, 10, mlngRut)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 80, mstrNombre)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_filtro_esp", adVarChar, adParamInput, 15, "")
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_tipo_filtro", adVarChar, adParamInput, 1, "")
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
'
'    Set recRegistros = Cmd.Execute
'
'    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
'        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Clientes"
'        GoTo LabelSalir
'    End If
'
'    cboRutEnt.Clear
'    cboNombreEnt.Clear
'
'    Do While Not recRegistros.EOF
'        cboRutEnt.AddItem RTrim("" & recRegistros.Fields("rut_entidad").Value) & "-" & recRegistros.Fields("ide_dig_rut").Value
'        cboRutEnt.ItemData(cboRutEnt.NewIndex) = recRegistros.Fields("ide_entidad").Value
'
'        cboNombreEnt.AddItem "" & recRegistros.Fields("nomcliente").Value
'        cboNombreEnt.ItemData(cboNombreEnt.NewIndex) = recRegistros.Fields("mrc_bloqueo").Value
'
'        recRegistros.MoveNext
'    Loop
'    If cboRutEnt.ListCount > 0 Then cboRutEnt.ListIndex = 0
'    If cboNombreEnt.ListCount > 0 Then cboNombreEnt.ListIndex = 0
'
'    Rem Exito
'    If cboRutEnt.ListIndex <> -1 Then
'        Call Carga_Combo_ContratosMarco(CboContrato, cboRutEnt.ItemData(cboRutEnt.ListIndex), cboProducto.ItemData(cboProducto.ListIndex), True, True)
'        Call Carga_Combo_Sucursales(cboSucursal, cboRutEnt.ItemData(cboRutEnt.ListIndex), , True)
'    End If
'
'LabelSalir:
'    Screen.MousePointer = 0
'    Set Cmd = Nothing
'    Set recRegistros = Nothing
'    Exit Sub
'LabelError:
'    If Not Err.Number = 0 Then
'        MsgBox Err.Description, vbCritical, "Error"
'        Err.Clear
'        GoTo LabelSalir
'        Resume 0
'    End If
'End Sub
'
'Private Sub cmdNuevaDigita_Click()
'
'
'    Call LimpiarControles(frmNvaDigitacion, "NUEVADIG")
'    Call AlmacenarValoresCtls(frmNvaDigitacion, "NUEVADIG")
'    frmNvaDigitacion.Show 1
'End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdValidar_Click()
    Dim intCol As Integer
    Dim strNomArch As String
    
  '  If Not Autorizar(sfarchivo) Then Exit Sub
    
    On Error GoTo error
    
    Screen.MousePointer = vbHourglass
    
    grdErrDetalle.MaxRows = 0
    lvwErrores.ListItems.Clear

    If lblTituloRemesas.Caption = "Detalle de la Remesa" Then
'        Call Validar_BD
    
    Else
        For intCol = 2 To grdEncab.MaxCols Step 2
            grdEncab.Col = intCol
            grdErrEncab.Col = intCol
            grdEncab.Text = ""
            grdErrEncab.Text = ""
        Next
        grdEncab.Col = grdEncab.MaxCols
        grdErrEncab.Col = grdErrEncab.MaxCols
        grdEncab.Text = ""
        grdErrEncab.Text = ""
        
        grdDetalle.MaxRows = 0
        
        For intCol = 2 To grdTotal.MaxCols Step 2
            grdTotal.Col = intCol
            grdErrTotal.Col = intCol
            grdTotal.Text = ""
            grdErrTotal.Text = ""
        Next
        grdTotal.Col = grdTotal.MaxCols
        grdErrTotal.Col = grdErrTotal.MaxCols
        grdTotal.Text = ""
        grdErrTotal.Text = ""
        
        
        grdRegistroArch.Row = grdRegistroArch.ActiveRow
        grdRegistroArch.Col = 18
        strNomArch = grdRegistroArch.Text
        
        
        If garrEstructura(0, 1).Nom_Cto_Tip_Fmto = "XLS" Then
            strNomArch = Replace(LCase(grdRegistroArch.Text), "xls", "tmp")
            Call TransformaXLS(grdRegistroArch.Text, strNomArch)
        End If
        Call Desplegar_Archivo(strNomArch, False)
        Call Validar_Fmto_Archivo(strNomArch)
        
        grdErrEncab.Col = IIf(grdErrEncab.MaxCols > 0, 2, 0)
        grdErrTotal.Col = IIf(grdErrTotal.MaxCols > 0, 2, 0)
        If grdErrEncab.Text = "" And grdErrTotal.Text = "" And grdErrDetalle.MaxRows = 0 Then
            Call Grabar_BD
            Call GrillasFmtoOK(grdEncab, grdTotal)
            Call GrillasFmtoOK(grdErrEncab, grdErrTotal)
'            Call Validar_BD
        End If
    
        If garrEstructura(0, 1).Nom_Cto_Tip_Fmto = "XLS" Then Kill strNomArch
    
        grdErrDetalle.SetFocus
        
    End If
    
Salir:
    EstadoToolBar
    Screen.MousePointer = vbNormal
    Exit Sub

error:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Error"
    GoTo Salir
    Resume 0
End Sub

Private Sub cmdVolver_Click()
    Call HabilitarTabs(tabSeciones, "0;1;", 0)
End Sub

Private Sub cmdProceso_Click()
    
  '  If Not Autorizar(sfmodificar) Then Exit Sub
    
    If gblnEsCarga Then
        
        Call Rms_Para_Aceptacion
        
    Else
        cmdAgregar.Enabled = False
        cmdModificar.Enabled = False
        cmdEliminar.Enabled = False
        cmdValidar.Enabled = True
        cmdProceso.Enabled = False
        chkDigita.Value = 0
        EstadoToolBar
        Rem crear procedimiento que cierra la digitacion
    End If


End Sub

Private Sub Form_Load()
    'Call Carga_Imagenes
    Call cargarImagenes(tlbAcciones, ilsIconos, "cargar", "digitar", "eliminar", "agregar", "editar", "eliminar", "validar", "procesar", "volver", "salir")

    cmdEliminarArch.Left = cmdNuevaDigita.Left
    cmdValidar.Left = cmdAgregar.Left

    Call Carga_Combo_Par(cboEntidad, 1, 11, , , True)
    Call Carga_Combo_Par(cboProducto, 1, 10, , , True)
    Call Carga_Combo_Par(cboFormato, 1, 8, , , True)
    Call Carga_Origen(True)
    Call Carga_Combo_Corp(cboOrigen, garrOrigen, 0, True)
    chkDigitando.Enabled = False
    Call Carga_Combo_Par(cboEstado, 1, 12, , , True)
    dtpFechaRecepIni = Format(Date, "dd/mm/yyyy")
    dtpFechaRecepFin = Format(Date, "dd/mm/yyyy")
    dtpFechaRecepIni.Value = Null
    dtpFechaRecepFin.Value = Null
    Call HabilitarTabs(tabSeciones, "0;", 0)
    Call Carga_PerfilImagenes(1)
    Me.Top = 400
    Me.Left = 3090
     
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Unload Me
End Sub








Private Sub grdDetalle_DblClick(ByVal Col As Long, ByVal Row As Long)
    If grdDetalle.MaxRows = 0 Or grdDetalle.Row = 0 Then Exit Sub
    If Not cmdAgregar.Enabled Then Exit Sub
'    Call cmdModificar_Click
End Sub

Private Sub grdErrNegocio_Advance(ByVal AdvanceNext As Boolean)

End Sub

Private Sub grdErrDetalle_Click(ByVal Col As Long, ByVal Row As Long)
    Dim lngRow As Long
    Dim intCol As Integer
    
    lngRow = Row 'grdErrDetalle.ActiveRow
    intCol = grdErrDetalle.MaxCols
    Mostrar_Errores grdErrDetalle, lngRow, intCol

End Sub


Private Sub grdErrDetalle_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
    grdErrDetalle_Click grdErrDetalle.ActiveCol, NewRow
End Sub


Private Sub grdErrEncab_Click(ByVal Col As Long, ByVal Row As Long)
    Dim lngRow As Long
    Dim intCol As Integer
    
    lngRow = grdErrEncab.ActiveRow
    intCol = grdErrEncab.MaxCols
    Mostrar_Errores grdErrEncab, lngRow, intCol
    
End Sub


Private Sub grdErrTotal_Click(ByVal Col As Long, ByVal Row As Long)
    Dim lngRow As Long
    Dim intCol As Integer
    
    lngRow = grdErrTotal.ActiveRow
    intCol = grdErrTotal.MaxCols
    Mostrar_Errores grdErrTotal, lngRow, intCol
End Sub

Private Sub grdRegistroArch_DblClick(ByVal Col As Long, ByVal Row As Long)
    Call HabilitarTabs(tabSeciones, "0;1;", 1)
End Sub

Private Sub hsbMueveEnc_Change()
    Dim intValor As Integer
    
    intValor = IIf(grdEncab.Col < hsbMueveEnc.Value, 1, -1)

    grdEncab.Col = hsbMueveEnc.Value
    If grdEncab.ColHidden Then
        hsbMueveEnc.Value = IIf(hsbMueveEnc.Value + intValor < 0, 0, hsbMueveEnc.Value + intValor)
    End If
    grdEncab.Action = 0

End Sub



Private Sub hsbMueveErrEnc_Change()
    Dim intValor As Integer
    On Error Resume Next
    
    intValor = IIf(grdErrEncab.Col < hsbMueveErrEnc.Value, 1, -1)

    grdErrEncab.Col = hsbMueveErrEnc.Value
    If grdErrEncab.ColHidden And hsbMueveErrEnc.Value < hsbMueveErrEnc.Max Then
        hsbMueveErrEnc.Value = hsbMueveErrEnc.Value + intValor
    End If
    grdErrEncab.Action = 0
     On Error GoTo 0
End Sub


Private Sub hsbMueveErrTot_Change()
    Dim intValor As Integer
    On Error Resume Next
    
    intValor = IIf(grdErrTotal.Col < hsbMueveErrTot.Value, 1, -1)

    grdErrTotal.Col = hsbMueveErrTot.Value
    If grdErrTotal.ColHidden And hsbMueveErrTot.Value < hsbMueveErrTot.Max Then
        hsbMueveErrTot.Value = hsbMueveErrTot.Value + intValor
    End If
    grdErrTotal.Action = 0
    On Error GoTo 0
End Sub

Private Sub hsbMueveTot_Change()
   Dim intValor As Integer
    
    intValor = IIf(grdTotal.Col < hsbMueveTot.Value, 1, -1)

    grdTotal.Col = hsbMueveTot.Value
    If grdTotal.ColHidden Then
        hsbMueveTot.Value = IIf(hsbMueveTot.Value + intValor < 0, 0, hsbMueveTot.Value + intValor)
    End If
    grdTotal.Action = 0
    
End Sub


Public Sub tabSeciones_Click(PreviousTab As Integer)
    Dim blnModificaRemesa As Boolean
    Dim blnExiteEstructura As Boolean
    Dim lngIdeCliente As Long
    Dim lngIdeProducto As Long
    Dim lngIdeSucursal As Long
    Dim lngIdeRemesa As Long
    Dim lngIdeContrato As Long
    Dim strArchivo As String
    
    On Error GoTo LabelError
    
    
    
    If PreviousTab = 1 Then
        frmCargaDigitacion.Buscar_Remesas
        PosicionaGrilla grdRegistroArch, Val(lblNumRemesa), 2
    End If
    
    Select Case Val(tabSeciones.Tab)
        Case 0
            Call Carga_PerfilImagenes(1)
        Case 1
        
            grdRegistroArch.Row = grdRegistroArch.ActiveRow
            grdRegistroArch.Col = 2: lblNumRemesa = Trim(grdRegistroArch.Text)
            grdRegistroArch.Col = 3: lngIdeProducto = Val(grdRegistroArch.Text)
            grdRegistroArch.Col = 4: lblProducto = Trim(grdRegistroArch.Text)
            grdRegistroArch.Col = 5: lblNumContrato = Trim(grdRegistroArch.Text)
            grdRegistroArch.Col = 6: lblRutCliente = Trim(grdRegistroArch.Text)
            grdRegistroArch.Col = 7: lblNombreCliente = Trim(grdRegistroArch.Text)
            grdRegistroArch.Col = 8: lngIdeSucursal = 1 'Val(grdRegistroArch.Text)
            grdRegistroArch.Col = 9: lblSucursal = Trim(grdRegistroArch.Text)
            grdRegistroArch.Col = 11: lblOrigen = Trim(grdRegistroArch.Text)
            grdRegistroArch.Col = 12: chkDigita.Value = IIf(UCase(Trim(grdRegistroArch.Text)) = "NO", 0, 1)
            grdRegistroArch.Col = 16: lblFormato = Trim(grdRegistroArch.Text)
            grdRegistroArch.Col = 18: gblnEsCarga = grdRegistroArch.Text <> ""
                                                 strArchivo = grdRegistroArch.Text
            grdRegistroArch.Col = 19: lblFechaRecep = Trim(grdRegistroArch.Text)
            grdRegistroArch.Col = 24: lbEstado = Trim(grdRegistroArch.Text)
            grdRegistroArch.Col = 31: lngIdeCliente = Val(grdRegistroArch.Text)
            grdRegistroArch.Col = 33: gblnAceptaErr = Val(grdRegistroArch.Text) = 1
            grdRegistroArch.Col = 34: gblnCorrigeRms = Val(grdRegistroArch.Text) = 1
            lngIdeRemesa = Val(lblNumRemesa)
            lngIdeContrato = Val(lblNumContrato)
            chkDigita.Enabled = False
            
            Call Carga_Estructura(lngIdeCliente, lngIdeContrato, lngIdeProducto, lngIdeSucursal, blnExiteEstructura)
            
            If blnExiteEstructura Then
                Call EstableceEstructuraGrilla(grdEncab, grdDetalle, grdTotal)
                Call EstableceEstructuraGrilla(grdErrEncab, grdErrDetalle, grdErrTotal)
                Call AgregaColumnaInvisible(grdErrEncab, grdErrDetalle, grdErrTotal)
                
                lvwErrores.ListItems.Clear
                hsbMueveEnc.Min = IIf(grdEncab.MaxCols > 0, 1, 0)
                hsbMueveEnc.Max = grdEncab.MaxCols
                hsbMueveTot.Min = IIf(grdTotal.MaxCols > 0, 1, 0)
                hsbMueveTot.Max = grdTotal.MaxCols
                hsbMueveErrEnc.Min = IIf(grdErrEncab.MaxCols > 0, 1, 0)
                hsbMueveErrEnc.Max = grdErrEncab.MaxCols
                hsbMueveErrTot.Min = IIf(grdErrTotal.MaxCols > 0, 1, 0)
                hsbMueveErrTot.Max = grdErrTotal.MaxCols
                
                
                Call TraeArchivoCliente(grdEncab, grdDetalle, grdTotal, lngIdeRemesa, lngIdeContrato)
        
                chkDigita.Visible = Not gblnEsCarga
                lblFormato.Visible = gblnEsCarga
                lblTitFormato.Visible = gblnEsCarga
                If gblnEsCarga Then
                    lblTituloRemesas.Caption = "Detalle del Archivo " & strArchivo
                    tlbAcciones.Buttons("Proceso").ToolTipText = "Enviar para Aceptaci�n"
                    cmdProceso.Caption = "Procesa"
                    cmdAgregar.Enabled = False 'gblnCorrigeRms And grdDetalle.MaxRows > 0
                    cmdModificar.Enabled = gblnCorrigeRms And grdDetalle.MaxRows > 0
                    cmdEliminar.Enabled = gblnCorrigeRms And grdDetalle.MaxRows > 0
                    cmdProceso.Enabled = gblnAceptaErr And grdDetalle.MaxRows > 0
                    'cmdValidar.Enabled = True
                    cmdValidar.Enabled = grdDetalle.MaxRows = 0
                Else
                    lblTituloRemesas.Caption = "Detalle de la Digitaci�n"
                    tlbAcciones.Buttons("Proceso").ToolTipText = "Cerrar la Digitaci�n"
                    cmdProceso.Caption = "Cerrar"
                    cmdAgregar.Enabled = chkDigita.Value = 1
                    cmdModificar.Enabled = chkDigita.Value = 1
                    cmdEliminar.Enabled = chkDigita.Value = 1
                    cmdProceso.Enabled = chkDigita.Value = 1
                    cmdValidar.Enabled = False
                End If
                If grdDetalle.MaxRows > 0 Then lblTituloRemesas.Caption = "Detalle de la Remesa"
            Else
                MsgBox "Estructura del cliente no encontrada", vbInformation, Me.Caption
                tabSeciones.Tab = 0
                GoTo LabelSalir
            End If
            tabDetErr.Tab = 0
            
            Call Carga_PerfilImagenes(2)
    End Select
    
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)

    Select Case Button.Key
        Case "CargarRms"
            cmdNuevaCarga_Click
'        Case "DigitarRms"
'            cmdNuevaDigita_Click
'        Case "EliminarRms"
'            cmdEliminarArch_Click
'        Case "AgregarDoc"
'            cmdAgregar_Click
'        Case "ModificarDoc"
'            cmdModificar_Click
'        Case "EliminarDoc"
'            cmdEliminar_Click
        Case "Validar"
            cmdValidar_Click
'        Case "Proceso"
'            cmdProceso_Click
        Case "Volver"
            cmdVolver_Click
        Case "Salir"
            cmdSalir_Click
    End Select

End Sub

Public Function EnClave(strClave As String) As String
    Dim intI        As Integer
    Dim strUsrPsw   As String
    strClave = Trim(strClave)
    For intI = 1 To Len(Trim$(strClave))
        strUsrPsw = strUsrPsw + Chr(Asc(Mid(strClave, intI, 1)) Xor 20)
    Next
    EnClave = strUsrPsw
End Function

Private Sub txtNumRemesa_GotFocus()
    
    cboEntidad.ListIndex = 0
    cboProducto.ListIndex = 0
    cboFormato.ListIndex = 0
    cboOrigen.ListIndex = 0
    cboEstado.ListIndex = 0
    dtpFechaRecepIni = Format(Date, "dd/mm/yyyy")
    dtpFechaRecepFin = Format(Date, "dd/mm/yyyy")
    dtpFechaRecepIni.Value = Null
    dtpFechaRecepFin.Value = Null
    txtNumRemesa.Text = ""
    grdRegistroArch.MaxRows = 0
    Call HabilitarTabs(tabSeciones, "0;", 0)
End Sub


Private Sub TraeArchivoCliente(Grilla1 As vaSpread, Grilla2 As vaSpread, Grilla3 As vaSpread, lngIdeRemesa As Long, lngIdeContrato As Long)
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngInd As Long
    Dim intCol  As Integer
    Dim lngRow As Long
    
    On Error GoTo LabelError
    
    Rem  Buscando Detalle de Arhivo del cliente
    Rem p_n_TipoSeccion = 1 Encabezado y Total.
    Rem                         = 2 Detalle
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "csbpi.pkg_fme_carga_digitacion.sp_fme_archivo_cliente_ver"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, lngIdeRemesa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_ContratoMarco", adNumeric, adParamInput, 10, lngIdeContrato)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_TipoSeccion", adNumeric, adParamInput, 10, 2)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_NroLinea", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute

    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser cargada. " & Chr(13) & _
                    Cmd.Parameters("p_v_DescError").Value, vbInformation, "Carga Archivo Cliente(Detalle)"
        GoTo LabelSalir
    End If
                    
    Rem Se Recorre cada Uno de los Registros del detalle con todos sus campos
    Grilla2.MaxRows = 0
    
    While Not recRegistros.EOF
        Grilla2.MaxRows = Grilla2.MaxRows + 1
        Grilla2.Row = Grilla2.MaxRows
        
        intCol = 1
        For lngRow = 0 To UBound(garrEstructura)
            For lngInd = 0 To recRegistros.Fields.Count - 1
                Grilla2.Col = intCol
                If garrEstructura(lngRow, 1).Gls_Descripcion = recRegistros(lngInd).Name Then
                    Grilla2.Text = "" & recRegistros(lngInd).Value
                    intCol = intCol + 1
                    Exit For
                ElseIf garrEstructura(lngRow, 1).Gls_Descripcion = "CODIGO DE REGISTRO (CAMPO CTRL.)" Then
                    Grilla2.Text = "2"
                    Grilla2.ColHidden = True
                    intCol = intCol + 1
                    Exit For
                ElseIf Val(garrEstructura(lngRow, 1).Cod_Ide_Campo) = 0 Then 'filler
                    intCol = intCol + 1
                    Exit For
                End If
            Next
        Next
        Rem Se almacena el Numero Fila del registro
        Grilla2.Col = Grilla2.MaxCols
        Grilla2.Text = "" & recRegistros.Fields("nro_linea").Value
        recRegistros.MoveNext
    Wend
        
    If Grilla2.MaxRows > 0 Then
        Grilla1.MaxCols = 0
        Grilla3.MaxCols = 0
        
     '   TraeErrLogicos
    End If
 
LabelSalir:
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub

LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Sub TraeErrLogicos()
    Dim Cmd As New ADODB.Command
    Dim recRegistros As New ADODB.Recordset
    
    On Error GoTo error

    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_carga_digitacion.sp_fme_errores_remesas_ver"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_numremesa", adNumeric, adParamInput, 10, lblNumRemesa.Caption)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute

    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
         MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Leyendo Errores"
         GoTo Salir
    End If
    
    Call Desplegar_Errores(recRegistros)
    
Salir:
    Screen.MousePointer = 0
    Set Cmd = Nothing
    Exit Sub

error:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo Salir
        Resume 0
    End If
End Sub

Private Sub Carga_PerfilImagenes(intTipo As Integer)

    tlbAcciones.Buttons("CargarRms").Visible = intTipo = 1
    tlbAcciones.Buttons("DigitarRms").Visible = False 'intTipo = 1
    tlbAcciones.Buttons("EliminarRms").Visible = intTipo = 1
    tlbAcciones.Buttons("Salir").Visible = intTipo = 1
    tlbAcciones.Buttons("AgregarDoc").Visible = False 'intTipo = 2
    tlbAcciones.Buttons("ModificarDoc").Visible = False 'intTipo = 2
    tlbAcciones.Buttons("EliminarDoc").Visible = False 'intTipo = 2
    tlbAcciones.Buttons("Validar").Visible = intTipo = 2
    tlbAcciones.Buttons("Proceso").Visible = False ' intTipo = 2
    tlbAcciones.Buttons("Volver").Visible = intTipo = 2
    
    EstadoToolBar
    
End Sub

