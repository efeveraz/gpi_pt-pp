Attribute VB_Name = "Proc_OPC_CARGA_ARCHIVO"
Option Explicit

    Public gblnConEncab As Boolean
    Public gblnConDetalle As Boolean
    Public gblnConTotal As Boolean
    Public gblnEsCarga As Boolean
    Public gblnCorrigeRms As Boolean
    Public gblnAceptaErr As Boolean
    
    Public garrEstructura() As Estructura

    Type Estructura
        Nom_Corto As String
        Cod_Ide_Campo As String
        Gls_Descripcion As String
        Nom_Corto_Campo As String
        Nro_Corr_Detalle As Long
        Nro_Corr_Campo As Long
        Gls_Campo As String
        Cod_Tipo_Campo As String
        Nom_Cto_Tip_Cpo As String
        Vlr_Posicion As Long
        Vlr_Precision As Long
        Vlr_Decimales As Long
        Mrc_Alineacion As Integer
        Mrc_Relleno As Integer
        Flg_Convertir As Integer
        Gls_Formato As String
        Cod_Estado As String
        Mrc_Bloqueo As Integer
        Flg_Obligatorio As Integer
        Tipo_Formato As String
        Nom_Cto_Tip_Fmto As String
        Ide_Unico_Tip_Fmto As String
        Separador As String
        SepDecimal As String
        Informacion As Variant
    End Type



Public mstrTipoDB As String
Public Const gstrMSACCESS = "Microsoft Access"
Public Const gstrDBASEIII = "Dbase III;"
Public Const gstrDBASEIV = "Dbase IV;"
Public Const gstrDBASE5 = "Dbase 5.0;"
Public Const gstrFOXPRO20 = "FoxPro 2.0;"
Public Const gstrFOXPRO25 = "FoxPro 2.5;"
Public Const gstrFOXPRO26 = "FoxPro 2.6;"
Public Const gstrFOXPRO30 = "FoxPro 3.0;"
Public Const gstrPARADOX3X = "Paradox 3.X;"
Public Const gstrPARADOX4X = "Paradox 4.X;"
Public Const gstrPARADOX5X = "Paradox 5.X;"
Public Const gstrBTRIEVE = "Btrieve;"
Public Const gstrEXCEL30 = "Excel 3.0;"
Public Const gstrEXCEL40 = "Excel 4.0;"
Public Const gstrEXCEL50 = "Excel 5.0;"
Public Const gstrTEXTFILES = "Texto;"
Public Const gstrSQLDB = "ODBC;"


Public glngCantControles As Long
Public gblnswichRefresca As Boolean
Public gblnModifica As Boolean
Public glngCodigo As Long
Public garrOrigen() As Variant
Public glngPosScroll As Long
Rem Arreglo Que Contentra Estructura del Archivo Cliente
Public garrEstrucArchClie() As Variant
Public garrDocumentos() As Variant

    Public garrConvComuna() As Variant
    Public garrConvPais() As Variant
    Public garrConvTipoDocto() As Variant
    Public garrConvBanco() As Variant
    Public garrConvPlaza() As Variant
    Public garrConvFormaPago() As Variant
    Public garrConvMoneda() As Variant


Public Enum tTipoAccion
    Lista = 1
    AgregarModificar = 2
    SubLista = 3
    Consultar = 4
    SubListaConsultar = 5
End Enum

Public Sub AgregaColumnaInvisible(grdGrilla1 As vaSpread, grdGrilla2 As vaSpread, grdGrilla3 As vaSpread)
                
Rem Agrega una Columna mas a las grilla de errores,
'     la que contendra el detalle de los errores de la fila

    grdGrilla1.MaxCols = grdGrilla1.MaxCols + 1
    grdGrilla1.Col = grdGrilla1.MaxCols
    grdGrilla1.CellType = 1
    grdGrilla1.TypeEditLen = 1000
    grdGrilla1.ColHidden = True

    grdGrilla2.MaxCols = grdGrilla2.MaxCols + 1
    grdGrilla2.Col = grdGrilla2.MaxCols
    grdGrilla2.CellType = 1
    grdGrilla2.TypeEditLen = 1000
    grdGrilla2.ColHidden = True

    grdGrilla3.MaxCols = grdGrilla3.MaxCols + 1
    grdGrilla3.Col = grdGrilla3.MaxCols
    grdGrilla3.CellType = 1
    grdGrilla3.TypeEditLen = 1000
    grdGrilla3.ColHidden = True

End Sub


Public Sub CargaArrayConv(arrArreglo() As Variant, strCodCampo As String, strNomCampo As String, strTipoFmto As String)
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim lngRgtros As Long
    Dim lngInd As Long
    
    On Error GoTo Errores
    
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_campos_x_cliente.sp_fme_conversion_mostrar"
    
                                                                    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    frmCargaDigitacion.grdRegistroArch.Col = 3:  Cmd.Parameters.Append Cmd.CreateParameter("p_n_producto", adNumeric, adParamInput, 10, frmCargaDigitacion.grdRegistroArch.Text)
    frmCargaDigitacion.grdRegistroArch.Col = 31: Cmd.Parameters.Append Cmd.CreateParameter("p_n_entidad", adNumeric, adParamInput, 10, frmCargaDigitacion.grdRegistroArch.Text)
    frmCargaDigitacion.grdRegistroArch.Col = 8:  Cmd.Parameters.Append Cmd.CreateParameter("p_n_sucursal", adNumeric, adParamInput, 10, frmCargaDigitacion.grdRegistroArch.Text)
                                                                    Cmd.Parameters.Append Cmd.CreateParameter("p_c_tipo_arch", adChar, adParamInput, 3, "ENT")
                                                                    Cmd.Parameters.Append Cmd.CreateParameter("p_c_tipo_fmto", adChar, adParamInput, 8, Trim(strTipoFmto))
                                                                    Cmd.Parameters.Append Cmd.CreateParameter("p_c_ide_campo", adChar, adParamInput, 8, Trim(strCodCampo))
                                                                    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Conversiones"
        GoTo Salir
    End If
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim arrArreglo(0 To lngRgtros, 0 To 1)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        arrArreglo(lngInd, 0) = recRegistros.Fields("des_campo_entrada").Value
        arrArreglo(lngInd, 1) = recRegistros.Fields("cod_campo_salida").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
Salir:
    recRegistros.Close
    Set Cmd = Nothing
   Screen.MousePointer = vbNormal
    Exit Sub
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Conversiones"
    GoTo Salir
Resume 0


End Sub

Public Sub Desplegar_Errores(recRegistros As ADODB.Recordset)
    Dim intI As Integer
    Dim intJ As Integer
    Dim lngNroLineaReg As Long
    Dim blnNvoReg As Boolean
    Dim strGlosaError As String
    Dim intItem As Integer
    
    lngNroLineaReg = 0
    strGlosaError = ""
    blnNvoReg = True
    intItem = 0
    While Not recRegistros.EOF
        
        If blnNvoReg Then
            intItem = 0
            For intI = 0 To recRegistros.Fields.Count - 1
                For intJ = 0 To UBound(garrEstructura)
                    If recRegistros.Fields.Item(intI).Name = garrEstructura(intJ, 1).Gls_Descripcion Then
                       garrEstructura(intJ, 1).Informacion = recRegistros.Fields(recRegistros.Fields.Item(intI).Name).Value
                       Exit For
                    End If
                Next
            Next
            blnNvoReg = False
        End If
        intItem = intItem + 1
        
        strGlosaError = strGlosaError & Trim(Str(intItem)) & ".- " & recRegistros.Fields("desc_error").Value & vbCrLf
        
        lngNroLineaReg = recRegistros.Fields("nro_linea")
        recRegistros.MoveNext
        
        If recRegistros.EOF Then
            blnNvoReg = True
        ElseIf lngNroLineaReg <> recRegistros.Fields("nro_linea") Then
            blnNvoReg = True
        End If
        
        If blnNvoReg Then
            Call Mostrar_Reg("DET", _
                                  frmCargaDigitacion.grdErrEncab, _
                                  frmCargaDigitacion.grdErrDetalle, _
                                  frmCargaDigitacion.grdErrTotal _
                                  )
            frmCargaDigitacion.grdErrDetalle.Col = frmCargaDigitacion.grdErrDetalle.MaxCols
            frmCargaDigitacion.grdErrDetalle.Text = StrConv(strGlosaError, vbProperCase)
            strGlosaError = ""
        End If
    Wend
    
    If frmCargaDigitacion.grdErrDetalle.MaxRows > 0 Then
        frmCargaDigitacion.Mostrar_Errores frmCargaDigitacion.grdErrDetalle, 1, frmCargaDigitacion.grdErrDetalle.MaxCols
        GrillasFmtoOK frmCargaDigitacion.grdErrEncab, frmCargaDigitacion.grdErrTotal
    Else
        frmCargaDigitacion.grdErrEncab.MaxCols = 0
        frmCargaDigitacion.grdErrDetalle.MaxRows = 0
        frmCargaDigitacion.grdErrTotal.MaxCols = 0
    End If
        
End Sub

Function FormatOK(strTexto As Variant, strFormat As Variant) As Boolean
    
    Dim intLargo As Integer
    Dim intInd As Integer
    Dim strValor As String
    
    FormatOK = True
    intLargo = Len(strTexto)
    
    If intLargo <> Len(strFormat) Then
        FormatOK = False
        Exit Function
    End If
    
    For intInd = 1 To intLargo
        strValor = Mid(strFormat, intInd, 1)
        If (Asc(strValor) < 48 Or Asc(strValor) > 57) And (Asc(UCase(strValor)) < 65 Or Asc(UCase(strValor)) > 90) Then
            If Mid(strTexto, intInd, 1) <> strValor Then
                 FormatOK = False
                Exit Function
            End If
        End If
    Next
    
    
    For intInd = 1 To intLargo
        strValor = Mid(strTexto, intInd, 1)
        If (Asc(strValor) < 48 Or Asc(strValor) > 57) And (Asc(UCase(strValor)) < 65 Or Asc(UCase(strValor)) > 90) Then
            If Mid(strFormat, intInd, 1) <> strValor Then
                 FormatOK = False
                Exit Function
            End If
        End If
    Next
End Function
 Function FechaOK(strTexto As Variant, strFormat As Variant) As String
    
    Dim intLargo As Integer
    Dim intInd As Integer
    Dim strValor As String
    Dim intDs As Integer
    Dim intMs As Integer
    Dim intYs As Integer
    Dim strDia As String
    Dim strMes As String
    Dim strAno As String
    Dim strFormato As String
        
    intDs = 0
    intMs = 0
    intYs = 0
    
    intLargo = Len(strFormat)
    For intInd = 1 To intLargo
        strValor = Mid(strFormat, intInd, 1)
        If strValor = "d" Then intDs = intDs + 1
        If strValor = "m" Then intMs = intMs + 1
        If strValor = "y" Then intYs = intYs + 1
    Next
 
    strDia = Mid(strTexto, InStr(strFormat, "d"), intDs)
    strMes = Mid(strTexto, InStr(strFormat, "m"), intMs)
    strAno = Mid(strTexto, InStr(strFormat, "y"), intYs)
    
    strFormato = String(intYs, "y") & String(intMs, "m") & String(intDs, "d")
 
    If Format(DateSerial(strAno, strMes, strDia), strFormato) = strAno & strMes & strDia Then
        FechaOK = Format(DateSerial(strAno, strMes, strDia), "dd/mm/yyyy")
    Else
        FechaOK = ""
    End If
    
End Function

Public Sub Grabar_BD()
    Dim Cmd As New ADODB.Command
    Dim arrCampos(23, 4) As Variant
    Dim arrValores As Variant
    Dim strReg As String
    Dim ArchDisp As Byte
    Dim intI As Integer
    Dim intJ As Integer
    Dim lngLinea As Long
    Dim strValor As String
    
Rem Descripcion del arreglo arrcampos
'       Col = 0  Nombre del campo de la Base de Datos
'       Col = 1   Nombre del Par�metro en el Proc. Almacenado
'       Col = 2   Tipo de Dato del Par�metro (que tiene relacion con el tipo del Campo)
'       Col = 3   Largo del Par�metro (que tiene relacion con el tipo del Campo)
'       Col = 4   Valor del Par�metro

    arrCampos(0, 0) = "COD_CORREDOR"
    arrCampos(1, 0) = "NOM_CORREDOR"
    arrCampos(2, 0) = "RUT_CLIENTE"
    arrCampos(3, 0) = "NOM_CLIENTE"
    arrCampos(4, 0) = "GLS_CTA_CTDIA_CLTE"
    arrCampos(5, 0) = "GLS_NEMOTECNICO"
    arrCampos(6, 0) = "MNT_NOMINAL"
    arrCampos(7, 0) = "MNT_PRECIO"
    arrCampos(8, 0) = "MNT_MONTO"
    arrCampos(9, 0) = "MNT_DRCHO_BOLSA"
    arrCampos(10, 0) = "MNT_GASTOS"
    arrCampos(11, 0) = "MNT_COMISION"
    arrCampos(12, 0) = "MNT_TOTAL"
    arrCampos(13, 0) = "NRO_FACTURA"
    arrCampos(14, 0) = "FCH_FECHA"
    arrCampos(15, 0) = "GLS_TIPO_LIQUIDAC "
    arrCampos(16, 0) = "MRC_TIPO_OPERA"
    arrCampos(17, 0) = "COD_CLASIF_RIESGO_AFP"
    arrCampos(18, 0) = "DURACION_DIAS_AFP"
    arrCampos(19, 0) = "COD_INSTRUMENTO_AFP"
    arrCampos(20, 0) = "COD_MONEDA_AFP"
    arrCampos(21, 0) = "MNT_FACTOR_PRECIO_AFP"
    arrCampos(22, 0) = "TASA_MERCADO_AFP"
    arrCampos(23, 0) = "TASA_ADICIONAL_AFP"
    
    arrCampos(0, 1) = "p_n_cod_corredor"
    arrCampos(1, 1) = "p_v_nom_corredor"
    arrCampos(2, 1) = "p_v_rut_cliente"
    arrCampos(3, 1) = "p_v_nom_cliente"
    arrCampos(4, 1) = "p_v_gls_cta_ctdia_clte"
    arrCampos(5, 1) = "p_v_nemotecnico"
    arrCampos(6, 1) = "p_n_mnt_nominal"
    arrCampos(7, 1) = "p_n_mnt_precio"
    arrCampos(8, 1) = "p_n_mnt_monto"
    arrCampos(9, 1) = "p_n_mnt_drcho_bolsa"
    arrCampos(10, 1) = "p_n_mnt_gastos"
    arrCampos(11, 1) = "p_n_mnt_comision"
    arrCampos(12, 1) = "p_n_mnt_total"
    arrCampos(13, 1) = "p_v_nro_factura"
    arrCampos(14, 1) = "p_d_fch_fecha"
    arrCampos(15, 1) = "p_v_gls_tipo_liquidac"
    arrCampos(16, 1) = "p_c_mrc_tipo_opera"
    arrCampos(17, 1) = "p_v_cod_clasif_riesgo_afp"
    arrCampos(18, 1) = "p_n_duracion_dias_afp"
    arrCampos(19, 1) = "p_v_cod_instrumento_afp"
    arrCampos(20, 1) = "p_v_cod_moneda_afp"
    arrCampos(21, 1) = "p_n_mnt_factor_precio_afp"
    arrCampos(22, 1) = "p_n_tasa_mercado_afp"
    arrCampos(23, 1) = "p_n_tasa_adicional_afp"
    
    arrCampos(0, 2) = 131  'NUMBER,
    arrCampos(1, 2) = 200  'VARCHAR2,
    arrCampos(2, 2) = 200  'VARCHAR2,
    arrCampos(3, 2) = 200  'VARCHAR2,
    arrCampos(4, 2) = 200  'VARCHAR2,
    arrCampos(5, 2) = 200  'varchar2
    arrCampos(6, 2) = 131  'NUMBER,
    arrCampos(7, 2) = 131  'NUMBER,
    arrCampos(8, 2) = 131  'NUMBER,
    arrCampos(9, 2) = 131  'NUMBER,
    arrCampos(10, 2) = 131 'NUMBER,
    arrCampos(11, 2) = 131 'NUMBER,
    arrCampos(12, 2) = 131 'NUMBER,
    arrCampos(13, 2) = 200 'VARCHAR2,
    arrCampos(14, 2) = 7   'DATE,
    arrCampos(15, 2) = 200 'VARCHAR2,
    arrCampos(16, 2) = 129 'CHAR,
    arrCampos(17, 2) = 200 'VARCHAR2,
    arrCampos(18, 2) = 131 'NUMBER,
    arrCampos(19, 2) = 200 'VARCHAR2,
    arrCampos(20, 2) = 200 'VARCHAR2,
    arrCampos(21, 2) = 131 'NUMBER,
    arrCampos(22, 2) = 131 'NUMBER,
    arrCampos(23, 2) = 131 'NUMBER,
    
    arrCampos(0, 3) = 10
    arrCampos(1, 3) = 60
    arrCampos(2, 3) = 12
    arrCampos(3, 3) = 60
    arrCampos(4, 3) = 15
    arrCampos(5, 3) = 50
    arrCampos(6, 3) = 19
    arrCampos(7, 3) = 19
    arrCampos(8, 3) = 19
    arrCampos(9, 3) = 19
    arrCampos(10, 3) = 19
    arrCampos(11, 3) = 19
    arrCampos(12, 3) = 19
    arrCampos(13, 3) = 10
    arrCampos(14, 3) = 10
    arrCampos(15, 3) = 5
    arrCampos(16, 3) = 1
    arrCampos(17, 3) = 60
    arrCampos(18, 3) = 5
    arrCampos(19, 3) = 60
    arrCampos(20, 3) = 60
    arrCampos(21, 3) = 19
    arrCampos(22, 3) = 11
    arrCampos(23, 3) = 11


    ArchDisp = FreeFile()
    Open "c:\carga.tmp" For Input As #ArchDisp
    
    lngLinea = 0
    Do While Not EOF(1)
        Line Input #1, strReg
        arrValores = Split(strReg, "~")
        lngLinea = lngLinea + 1
        
        For intI = 0 To 23
            arrCampos(intI, 4) = Null
            For intJ = 0 To UBound(garrEstructura)
                If arrCampos(intI, 0) = garrEstructura(intJ, 1).Gls_Descripcion Then
                        arrCampos(intI, 4) = arrValores(intJ)
                   Exit For
                End If
            Next
        Next
        
        Cmd.ActiveConnection = gAdoCon
        Cmd.CommandType = adCmdStoredProc
        Cmd.CommandText = "csbpi.pkg_fme_carga_digitacion.sp_fme_registro_archivo_grabar"
        
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeRemesa", adNumeric, adParamInput, 10, frmCargaDigitacion.lblNumRemesa.Caption)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_NroLinea", adNumeric, adParamInput, 10, lngLinea)
        For intI = 0 To 23
            Cmd.Parameters.Append Cmd.CreateParameter(arrCampos(intI, 1), arrCampos(intI, 2), adParamInput, arrCampos(intI, 3), arrCampos(intI, 4))
        Next
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adChar, adParamInput, 10, gstrUsuario)
        Cmd.Parameters.Append Cmd.CreateParameter("p_V_DescError", adVarChar, adParamOutput, 1000)
        Cmd.Execute
    
        If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
             MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Grabando Remesa"
             GoTo Salir
        End If
        
       Set Cmd = Nothing
    
    Loop
    
Salir:
    Close #ArchDisp
    
End Sub



Public Sub TransformaXLS(strArchXLS As String, strArchTMP As String)
    Dim lExcel As Object 'Excel.Application
    Dim lLibro As Object 'Excel.Workbook
    Dim lHoja As Object 'Excel.Worksheet
    Dim lngFila As Long
    Dim intCol As Integer
    Dim strTexto As String
    Dim intPosDec As Integer
    Dim strEntero As String
    Dim strDecimal As String
    Dim strReg As String
    Dim ArchDisp As Byte
    Dim intColArr As Integer
    
    On Error GoTo Errores
    
    ArchDisp = FreeFile()
    Open strArchTMP For Output As #ArchDisp
    
    Set lExcel = CreateObject("Excel.Application")
    Set lLibro = lExcel.Workbooks.Open(strArchXLS, , True)
    Set lHoja = lLibro.Worksheets(1)
  
    lngFila = 1
    While lHoja.Cells(lngFila, 1).Value <> ""
        strReg = ""
        
        If gblnConEncab Or gblnConTotal Then
            intColArr = lHoja.Cells(lngFila, 1).Value - 1
        Else
            intColArr = 1
        End If
        
        For intCol = 1 To UBound(garrEstructura)
        
            strEntero = ""
            strDecimal = ""
            If garrEstructura(intCol - 1, intColArr).Nom_Cto_Tip_Cpo = "NUM" And garrEstructura(intCol - 1, intColArr).Vlr_Decimales > 0 Then
                intPosDec = InStr(lHoja.Cells(lngFila, intCol).Value, gstrSepDec)
                If intPosDec > 0 Then
                    strEntero = Left(lHoja.Cells(lngFila, intCol).Value, intPosDec - 1)
                    strDecimal = Mid(lHoja.Cells(lngFila, intCol).Value, intPosDec + 1)
                Else
                    strEntero = lHoja.Cells(lngFila, intCol).Value
                    strDecimal = String(garrEstructura(intCol - 1, intColArr).Vlr_Decimales, "0")
                End If
                
                If Len(strDecimal) > garrEstructura(intCol - 1, intColArr).Vlr_Decimales Then
                    strDecimal = strDecimal & "!"
                Else
                    strDecimal = strDecimal & String(garrEstructura(intCol - 1, intColArr).Vlr_Decimales - Len(strDecimal), "0")
                End If
                strTexto = strEntero & strDecimal & ";"
            Else
                strTexto = lHoja.Cells(lngFila, intCol).Value & ";"
            End If
            
            If strTexto = ";" Then Exit For
            strReg = strReg & strTexto
        Next
        Print #ArchDisp, strReg
        lngFila = lngFila + 1
    Wend
    
    For intCol = 0 To 2
        For lngFila = 0 To UBound(garrEstructura)
            If garrEstructura(lngFila, intCol).Nom_Corto = "" Then Exit For
            garrEstructura(lngFila, intCol).Separador = ";"
        Next
    Next
    
Salir:
    lExcel.DisplayAlerts = False
    lExcel.Quit
    Set lExcel = Nothing
    
    Close #ArchDisp
    Exit Sub

Errores:
    MsgBox Err.Number & vbCr & Err.Description, "Error"
    GoTo Salir
    Resume 0
            
End Sub

Public Sub Rms_Para_Aceptacion()
    Dim Cmd As New ADODB.Command
    Dim lngProd As Long
    
    On Error GoTo error
    
    frmCargaDigitacion.grdRegistroArch.Col = 3
    lngProd = frmCargaDigitacion.grdRegistroArch.Text

    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_carga_digitacion.sp_fme_rms_para_aceptacion"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_numremesa", adNumeric, adParamInput, 10, frmCargaDigitacion.lblNumRemesa.Caption)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adChar, adParamInput, 10, gstrUsuario)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
         MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Enviado a Aceptacion"
    Else
        MsgBox "Remesa enviada para Aceptaci�n", vbInformation, "Remesa"
        frmCargaDigitacion.tabSeciones.Tab = 0
        frmCargaDigitacion.Buscar_Remesas
    End If
        
Salir:
    Screen.MousePointer = 0
    Set Cmd = Nothing
    Exit Sub

error:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo Salir
        Resume 0
    End If

End Sub

'Public Sub Validar_BD()
'    Dim Cmd As New ADODB.Command
'   Dim recRegistros As New ADODB.Recordset
'   Dim lngProd As Long
'
'    On Error GoTo error
'
'    frmCargaDigitacion.grdRegistroArch.Col = 3
'    lngProd = frmCargaDigitacion.grdRegistroArch.Text
'
'
'    Cmd.ActiveConnection = gAdoCon
'    Cmd.CommandType = adCmdStoredProc
'    Cmd.CommandText = "dbo_factor.pkg_fme_valida_remesa.sp_fme_valida_remesa"
'
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_numremesa", adNumeric, adParamInput, 10, frmCargaDigitacion.lblNumRemesa.Caption)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_cmarco", adNumeric, adParamInput, 10, frmCargaDigitacion.lblNumContrato.Caption)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_rutcliente", adNumeric, adParamInput, 10, Val(frmCargaDigitacion.lblRutCliente.Caption))
'    Cmd.Parameters.Append Cmd.CreateParameter("p_n_idproducto", adNumeric, adParamInput, 10, lngProd)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adChar, adParamInput, 10, gstrUsuario)
'    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
'    Set recRegistros = Cmd.Execute
'
'    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
'         MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Validando Remesa"
'         GoTo Salir
'    End If
'
'    If recRegistros.EOF Then
'        MsgBox "Remesa enviada para Aceptaci�n", vbInformation, "Remesa sin Errores"
'        frmCargaDigitacion.tabSeciones.Tab = 0
'        frmCargaDigitacion.Buscar_Remesas
'    Else
'        Call Desplegar_Errores(recRegistros)
'
'        If gblnEsCarga Then
'            frmCargaDigitacion.cmdProceso.Enabled = frmCargaDigitacion.gblnAceptaErr
'            frmCargaDigitacion.cmdAgregar.Enabled = False 'frmCargaDigitacion.gblnCorrigeRms
'            frmCargaDigitacion.cmdModificar.Enabled = frmCargaDigitacion.gblnCorrigeRms
'            frmCargaDigitacion.cmdEliminar.Enabled = frmCargaDigitacion.gblnCorrigeRms
'
'        Else
'            frmCargaDigitacion.cmdProceso.Enabled = True
'            frmCargaDigitacion.cmdAgregar.Enabled = True
'            frmCargaDigitacion.cmdModificar.Enabled = True
'            frmCargaDigitacion.cmdEliminar.Enabled = True
'            frmCargaDigitacion.cmdValidar.Enabled = False
'            frmCargaDigitacion.chkDigita.Value = 1
'        End If
'
'    End If
'
'
'Salir:
'    Screen.MousePointer = 0
'    Set Cmd = Nothing
'    Exit Sub
'
'error:
'    If Not Err.Number = 0 Then
'        MsgBox Err.Description, vbCritical, "Error"
'        Err.Clear
'        GoTo Salir
'        Resume 0
'    End If
'
'End Sub
'
'
Private Sub Mostrar_Reg(strTipo As String, grdEnc As vaSpread, grdDet As vaSpread, grdTot As vaSpread)
    Dim lngFila As Long
    Dim intColGrilla As Integer
    Dim sngColWidth As Single
        
    If strTipo = "ENC" Then
        
        intColGrilla = 2
        For lngFila = 0 To UBound(garrEstructura)
            If garrEstructura(lngFila, 0).Nom_Corto = "" Then Exit For
            grdEnc.Col = intColGrilla
            grdEnc.Text = "" & garrEstructura(lngFila, 0).Informacion
            If garrEstructura(lngFila, 0).Cod_Ide_Campo <> -99 Then 'No Filler
                grdEnc.TwipsToColWidth Printer.TextWidth(grdEnc.Text & "O"), sngColWidth
                grdEnc.ColWidth(intColGrilla) = sngColWidth
            End If
            intColGrilla = intColGrilla + 2
        Next
    
    ElseIf strTipo = "DET" Then

        grdDet.MaxRows = grdDet.MaxRows + 1
        grdDet.Row = grdDet.MaxRows
        intColGrilla = 1
        For lngFila = 0 To UBound(garrEstructura)
            If garrEstructura(lngFila, 1).Nom_Corto = "" Then Exit For
            grdDet.Col = intColGrilla
            grdDet.Text = "" & garrEstructura(lngFila, 1).Informacion
            If garrEstructura(lngFila, 1).Gls_Descripcion = "CODIGO DE REGISTRO (CAMPO CTRL.)" Then
                grdDet.ColHidden = frmCargaDigitacion.lblTituloRemesas.Caption = "Detalle de la Remesa"
            End If
            intColGrilla = intColGrilla + 1
        Next

    ElseIf strTipo = "TOT" Then

        intColGrilla = 2
        For lngFila = 0 To UBound(garrEstructura)
            If garrEstructura(lngFila, 2).Nom_Corto = "" Then Exit For
            grdTot.Col = intColGrilla
            grdTot.Text = "" & garrEstructura(lngFila, 2).Informacion
            If garrEstructura(lngFila, 0).Cod_Ide_Campo <> -99 Then 'No Filler
                grdTot.TwipsToColWidth Printer.TextWidth(grdTot.Text & "O"), sngColWidth
                grdTot.ColWidth(intColGrilla) = sngColWidth
            End If
            intColGrilla = intColGrilla + 2
        Next

    End If

End Sub

Function TextoEsNumero(Nro As Variant) As Boolean
    Dim strNoDecimal As String
    
    TextoEsNumero = False
    strNoDecimal = IIf(gstrSepDec = ",", ".", ",")
    
    Nro = Replace(Replace(UCase(Nro), "D", "X"), "E", "X")
    Nro = Replace(UCase(Nro), strNoDecimal, "X")
    If IsNumeric(Nro) Then TextoEsNumero = True
End Function

Public Function Descomponer(strReg) As String
    Dim lngFila As Long
    Dim intTipoReg As Integer
    Dim intCol As Integer
    Dim lngpos As Long
    Dim arrAux As Variant

    On Error GoTo Errores
 
    If gblnConEncab Or gblnConTotal Then
        intTipoReg = Mid(strReg, 1, 1)
    Else
        intTipoReg = 2
    End If

    intCol = intTipoReg - 1
    
    If garrEstructura(lngFila, intCol).Separador <> "" Then
        arrAux = Split(strReg, Trim(garrEstructura(lngFila, intCol).Separador))
    End If
    
    lngpos = 1
    For lngFila = 0 To UBound(garrEstructura)
    
        If garrEstructura(lngFila, intCol).Nom_Corto = "" Then Exit For
        If garrEstructura(lngFila, intCol).Separador = "" Then
            garrEstructura(lngFila, intCol).Informacion = Mid(strReg, lngpos, garrEstructura(lngFila, intCol).Vlr_Precision)
            lngpos = lngpos + garrEstructura(lngFila, intCol).Vlr_Precision
        Else
            garrEstructura(lngFila, intCol).Informacion = arrAux(lngFila)
        End If
    Next

    Descomponer = IIf(intTipoReg = 1, "ENC", IIf(intTipoReg = 2, "DET", IIf(intTipoReg = 3, "TOT", "")))
Salir:
    Exit Function
    
Errores:
    Descomponer = ""
    MsgBox Err.Number & Chr(13) & Err.Description
    GoTo Salir
    Resume 0
    
End Function

Public Sub Validar_Fmto_Archivo(strArchivo As String)
    
    Dim strReg As String
    Dim strNvoReg As String
    Dim strTipoLinea As String
    Dim ArchDisp1 As Byte
    Dim ArchDisp2 As Byte
    Dim lngFila As Long
    Dim intCol As Integer
    Dim strErrorCpo As String
    Dim strErrorReg As String
    Dim strCampo As String
    Dim strRut As String
    Dim strDV As String
    Dim lngEntero As Long
    Dim lngDecimal  As Long
    Dim dblValor As Double
    Dim dblTotalDoctos As Double
    Dim dblSumaTotalDoctos  As Double
    Dim dblMtoTotalDoctos As Double
    Dim dblSumaMtoTotalDoctos As Double
    Dim strLugarTot As String
    Dim strLugarMto     As String
    Dim strCampoTot As String
    Dim strCampoMto As String
    Dim strErrorRegTot As String
    Dim strErrorRegMto As String
    Dim strFecha  As String
    Dim strNvoValor As String
    Dim strConversion As String
    
    On Error GoTo Errores
    
    ArchDisp1 = FreeFile()
    Open strArchivo For Input As #ArchDisp1
    
    ArchDisp2 = FreeFile()
    Open "c:\carga.tmp" For Output As #ArchDisp2
    
    dblSumaTotalDoctos = 0
    Do While Not EOF(1)
        
        Line Input #1, strReg
        strTipoLinea = Descomponer(strReg)
        
        If UCase(strTipoLinea) = "ENC" Then
            intCol = 0
        ElseIf UCase(strTipoLinea) = "DET" Then
            intCol = 1
            dblSumaTotalDoctos = dblSumaTotalDoctos + 1
        ElseIf UCase(strTipoLinea) = "TOT" Then
            intCol = 2
        End If
        
        strErrorReg = ""
        strNvoReg = ""
        
        For lngFila = 0 To UBound(garrEstructura)
            If garrEstructura(lngFila, intCol).Nom_Corto = "" Then Exit For
                
            strCampo = garrEstructura(lngFila, intCol).Gls_Campo & vbCrLf
            strErrorCpo = ""
            
Rem ---------------------------------------------VALIDAR CARACTER ~---------------------------------------------
            If InStr(garrEstructura(lngFila, intCol).Informacion, "~") > 0 Then
                strErrorCpo = strErrorCpo & "Existe el caracter '~' el cual no es permitido." & vbCrLf
            End If

Rem ---------------------------------------------VALIDAR LARGO---------------------------------------------
            If garrEstructura(lngFila, intCol).Separador = "" Then
                If Len(garrEstructura(lngFila, intCol).Informacion) > garrEstructura(lngFila, intCol).Vlr_Precision Then
                    strErrorCpo = strErrorCpo & "El Largo no corresponde al definido." & vbCrLf
                End If
            End If
Rem ---------------------------------------------VALIDAR TIPO DE DATO CARACTER---------------------------------------------
            If garrEstructura(lngFila, intCol).Nom_Cto_Tip_Cpo = "CAR" Then
                strNvoValor = garrEstructura(lngFila, intCol).Informacion
                
Rem ---------------------------------------------VALIDAR TIPO DE DATO FECHA---------------------------------------------
            ElseIf garrEstructura(lngFila, intCol).Nom_Cto_Tip_Cpo = "FEC" Then
                If Not FormatOK(garrEstructura(lngFila, intCol).Informacion, garrEstructura(lngFila, intCol).Gls_Formato) Then
                    strErrorCpo = strErrorCpo & "El Formato no corresponde al definido." & vbCrLf
                Else
                    strFecha = FechaOK(garrEstructura(lngFila, intCol).Informacion, garrEstructura(lngFila, intCol).Gls_Formato)
                    If strFecha = "" Then
                        strErrorCpo = strErrorCpo & "La Fecha no es valida." & vbCrLf
                    Else
                        strNvoValor = strFecha
                    End If
                End If
                
Rem ---------------------------------------------VALIDAR TIPO DE DATO NUMERICO---------------------------------------------
            ElseIf garrEstructura(lngFila, intCol).Nom_Cto_Tip_Cpo = "NUM" Then
                
                Rem ----------------------VALIDAR SEPARADOR DECIMAL-----------------------------
                If garrEstructura(lngFila, intCol).SepDecimal <> "" Then
                    If garrEstructura(lngFila, intCol).SepDecimal = "," Then
                        If InStr(garrEstructura(lngFila, intCol).Informacion, ".") > 0 Then strErrorCpo = strErrorCpo & "El Separador Decimal no correspnde al definido." & vbCrLf
                    Else
                        If InStr(garrEstructura(lngFila, intCol).Informacion, ",") > 0 Then strErrorCpo = strErrorCpo & "El Separador Decimal no correspnde al definido." & vbCrLf
                    End If
                    
                    garrEstructura(lngFila, intCol).Informacion = Replace(garrEstructura(lngFila, intCol).Informacion, garrEstructura(lngFila, intCol).SepDecimal, gstrSepDec)
                    
                End If
                
                If Not TextoEsNumero(garrEstructura(lngFila, intCol).Informacion) Then
                    If InStr(garrEstructura(lngFila, intCol).Informacion, "!") > 0 Then
                        strErrorCpo = strErrorCpo & "La Cantidad de Decimales no correspnde al definido." & vbCrLf
                    Else
                        strErrorCpo = strErrorCpo & "El Valor no es un N�mero." & vbCrLf
                    End If
                Else
                    If garrEstructura(lngFila, intCol).SepDecimal = "" Then
                        lngEntero = Left(garrEstructura(lngFila, intCol).Informacion, Len(garrEstructura(lngFila, intCol).Informacion) - garrEstructura(lngFila, intCol).Vlr_Decimales)
                        If garrEstructura(lngFila, intCol).Vlr_Decimales > 0 Then
                            lngDecimal = Right(garrEstructura(lngFila, intCol).Informacion, garrEstructura(lngFila, intCol).Vlr_Decimales)
                        Else
                            lngDecimal = 0
                        End If
                        dblValor = CDbl(lngEntero & gstrSepDec & lngDecimal)
                    Else
                        dblValor = garrEstructura(lngFila, intCol).Informacion
                    End If
                    strNvoValor = dblValor
                End If
                
Rem ---------------------------------------------VALIDAR TIPO DE DATO RUT---------------------------------------------
            ElseIf garrEstructura(lngFila, intCol).Nom_Cto_Tip_Cpo = "RUT" Then
                If Not FormatOK(garrEstructura(lngFila, intCol).Informacion, garrEstructura(lngFila, intCol).Gls_Formato) Then
                    strErrorCpo = strErrorCpo & "El Formato no corresponde al definido." & vbCrLf
                Else
                
                    If Right(garrEstructura(lngFila, intCol).Gls_Formato, 2) = "-D" Then
                        strDV = TraeDigVerificador(Mid(garrEstructura(lngFila, intCol).Informacion, 1, Len(garrEstructura(lngFila, intCol).Informacion) - 2))
                     ElseIf Right(garrEstructura(lngFila, intCol).Gls_Formato, 1) = "D" Then
                        strDV = TraeDigVerificador(Mid(garrEstructura(lngFila, intCol).Informacion, 1, Len(garrEstructura(lngFila, intCol).Informacion) - 1))
                    Else
                        strDV = TraeDigVerificador(garrEstructura(lngFila, intCol).Informacion)
                    End If
                
                    If Right(garrEstructura(lngFila, intCol).Gls_Formato, 1) = "D" Then
                        If Right(garrEstructura(lngFila, intCol).Informacion, 1) <> strDV Then
                            strErrorCpo = strErrorCpo & "El D�gito Verificador no corresponde." & vbCrLf
                        End If
                    End If
                    strNvoValor = garrEstructura(lngFila, intCol).Informacion
                End If
            
            End If
                
Rem ---------------------------------------------VALIDAR CAMPOS ESPECIALES---------------------------------------------
            Select Case garrEstructura(lngFila, intCol).Gls_Descripcion
                Case "IDE_DV_EMISOR", "IDE_DV_RECEPTOR", "DIGITO VERIFICADOR (CAMPO CTRL.)"
                    If Right(garrEstructura(lngFila, intCol).Informacion, 1) <> strDV Then
                       strErrorCpo = strErrorCpo & "El D�gito Verificador no corresponde." & vbCrLf
                    End If
                    
                Case "MNT_DOCUMENTO"
                    If InStr(garrEstructura(lngFila, intCol).Informacion, "!") = 0 Then
                        lngEntero = Left(garrEstructura(lngFila, intCol).Informacion, Len(garrEstructura(lngFila, intCol).Informacion) - garrEstructura(lngFila, intCol).Vlr_Decimales)
                        If garrEstructura(lngFila, intCol).Vlr_Decimales > 0 Then
                            lngDecimal = Right(garrEstructura(lngFila, intCol).Informacion, garrEstructura(lngFila, intCol).Vlr_Decimales)
                        Else
                            lngDecimal = 0
                        End If
                        dblSumaMtoTotalDoctos = dblSumaMtoTotalDoctos + CDbl(lngEntero & gstrSepDec & lngDecimal)
                    End If
                    
                Case "NUMERO DE CONTRATO (CAMPO CTRL.)"
                    If Val(garrEstructura(lngFila, intCol).Informacion) <> Val(frmCargaDigitacion.lblNumContrato.Caption) Then
                        strErrorCpo = strErrorCpo & "El Contrato Marco no corresponde al seleccionado." & vbCrLf
                    End If
                
                Case "RUT DEL CLIENTE (CAMPO CTRL.)"
                    If Right(garrEstructura(lngFila, intCol).Gls_Formato, 1) = "D" Then
                        strRut = Mid(garrEstructura(lngFila, intCol).Informacion, 1, InStr(garrEstructura(lngFila, intCol).Gls_Formato, "D") - 1)
                    ElseIf Right(garrEstructura(lngFila, intCol).Gls_Formato, 1) = "-D" Then
                        strRut = Mid(garrEstructura(lngFila, intCol).Informacion, 1, InStr(garrEstructura(lngFila, intCol).Gls_Formato, "-D") - 1)
                    ElseIf Right(garrEstructura(lngFila, intCol).Gls_Formato, 1) = "9" Then
                        strRut = garrEstructura(lngFila, intCol).Informacion
                    End If
                    If Val(strRut) <> Val(frmCargaDigitacion.lblRutCliente.Caption) Then
                        strErrorCpo = strErrorCpo & "El Rut del Cliente no corresponde al seleccionado." & vbCrLf
                    End If
                    
'                Case "NOMBRE DEL CLIENTE (CAMPO CTRL.)"
'                    If Trim(garrEstructura(lngFila, intCol).Informacion) <> Trim(frmCargaDigitacion.lblNombreCliente.Caption) Then
'                        strErrorCpo = strErrorCpo & "El Nombre del Cliente no corresponde al seleccionado." & vbCrLf
'                    End If
                
                Case "TOTAL DE DOCUMENTOS (CAMPO CTRL.)"
                    lngEntero = Left(garrEstructura(lngFila, intCol).Informacion, Len(garrEstructura(lngFila, intCol).Informacion) - garrEstructura(lngFila, intCol).Vlr_Decimales)
                    If garrEstructura(lngFila, intCol).Vlr_Decimales > 0 Then
                        lngDecimal = Right(garrEstructura(lngFila, intCol).Informacion, garrEstructura(lngFila, intCol).Vlr_Decimales)
                    Else
                        lngDecimal = 0
                    End If
                    dblTotalDoctos = CDbl(lngEntero & gstrSepDec & lngDecimal)
                    strLugarTot = garrEstructura(lngFila, intCol).Nom_Corto
                    strCampoTot = garrEstructura(lngFila, intCol).Gls_Campo & vbCrLf
                    
                Case "MONTO TOTAL DE DOCUMENTOS (CAMPO CTRL.)"
                    lngEntero = Left(garrEstructura(lngFila, intCol).Informacion, Len(garrEstructura(lngFila, intCol).Informacion) - garrEstructura(lngFila, intCol).Vlr_Decimales)
                    If garrEstructura(lngFila, intCol).Vlr_Decimales > 0 Then
                        lngDecimal = Right(garrEstructura(lngFila, intCol).Informacion, garrEstructura(lngFila, intCol).Vlr_Decimales)
                    Else
                        lngDecimal = 0
                    End If
                    dblMtoTotalDoctos = CDbl(lngEntero & gstrSepDec & lngDecimal)
                    strLugarMto = garrEstructura(lngFila, intCol).Nom_Corto
                    strCampoMto = garrEstructura(lngFila, intCol).Gls_Campo & vbCrLf
            End Select
        
Rem ---------------------------------------------VALIDAR CONVERSION---------------------------------------------
            If garrEstructura(lngFila, intCol).Flg_Convertir = 1 Then
                Select Case LCase(garrEstructura(lngFila, intCol).Gls_Descripcion)
                    Case "gls_comuna"
                        strConversion = Busca_Conversion(garrEstructura(lngFila, intCol).Informacion, garrConvComuna())
                    Case "cod_pais"
                        strConversion = Busca_Conversion(garrEstructura(lngFila, intCol).Informacion, garrConvPais())
                    Case "cod_tipo_docto"
                        strConversion = Busca_Conversion(garrEstructura(lngFila, intCol).Informacion, garrConvTipoDocto())
                    Case "cod_banco"
                        strConversion = Busca_Conversion(garrEstructura(lngFila, intCol).Informacion, garrConvBanco())
                    Case "cod_plaza"
                        strConversion = Busca_Conversion(garrEstructura(lngFila, intCol).Informacion, garrConvPlaza())
                    Case "cod_forma_pago"
                        strConversion = Busca_Conversion(garrEstructura(lngFila, intCol).Informacion, garrConvFormaPago())
                    Case "cod_moneda"
                        strConversion = Busca_Conversion(garrEstructura(lngFila, intCol).Informacion, garrConvMoneda())
                End Select
                
                If strConversion = "" Then
                    strErrorCpo = strErrorCpo & "No existe conversi�n para esta informaci�n." & vbCrLf
                Else
                    strNvoValor = strConversion
                End If
            End If
                
            strNvoReg = strNvoReg & strNvoValor & "~"
                
            If strErrorCpo <> "" Then strErrorReg = strErrorReg & strCampo & strErrorCpo
                
        Next
        
        If strErrorReg <> "" Then
        
            Call Mostrar_Reg(UCase(strTipoLinea), _
                                          frmCargaDigitacion.grdErrEncab, _
                                          frmCargaDigitacion.grdErrDetalle, _
                                          frmCargaDigitacion.grdErrTotal _
                                          )
        
            If UCase(strTipoLinea) = "ENC" Then
                frmCargaDigitacion.grdErrEncab.Col = frmCargaDigitacion.grdErrEncab.MaxCols
                frmCargaDigitacion.grdErrEncab.Text = strErrorReg
            
            ElseIf UCase(strTipoLinea) = "DET" Then
                frmCargaDigitacion.grdErrDetalle.Col = frmCargaDigitacion.grdErrDetalle.MaxCols
                frmCargaDigitacion.grdErrDetalle.CellType = 1
                frmCargaDigitacion.grdErrDetalle.TypeEditLen = 1000
                frmCargaDigitacion.grdErrDetalle.Text = strErrorReg
            
            ElseIf UCase(strTipoLinea) = "TOT" Then
                frmCargaDigitacion.grdErrTotal.Col = frmCargaDigitacion.grdErrTotal.MaxCols
                frmCargaDigitacion.grdErrTotal.Text = strErrorReg
            End If
        
        Else
            Rem crea un nuevo registro en el archivo temporal
            If UCase(strTipoLinea) = "DET" Then
                Print #ArchDisp2, strNvoReg
            End If
        End If
        
    Loop
    
    Close #ArchDisp1, #ArchDisp2
    

    strErrorRegTot = ""
    If dblTotalDoctos <> dblSumaTotalDoctos Then
        Call Mostrar_Reg(strLugarTot, _
                                  frmCargaDigitacion.grdErrEncab, _
                                  frmCargaDigitacion.grdErrDetalle, _
                                  frmCargaDigitacion.grdErrTotal _
                                  )
        strErrorRegTot = strCampoTot & "La Cantidad Total de Documentos no corresponde." & vbCrLf
    End If
    
    strErrorRegMto = ""
    If dblMtoTotalDoctos <> dblSumaMtoTotalDoctos Then
        Call Mostrar_Reg(strLugarMto, _
                                  frmCargaDigitacion.grdErrEncab, _
                                  frmCargaDigitacion.grdErrDetalle, _
                                  frmCargaDigitacion.grdErrTotal _
                                  )
        strErrorRegMto = strCampoMto & "El Monto Total de Documentos no corresponde." & vbCrLf
    End If

    
    If strLugarTot = "ENC" Then
        frmCargaDigitacion.grdErrEncab.Col = frmCargaDigitacion.grdErrEncab.MaxCols
        frmCargaDigitacion.grdErrEncab.Text = frmCargaDigitacion.grdErrEncab.Text & strErrorRegTot
    ElseIf strLugarTot = "TOT" Then
        frmCargaDigitacion.grdErrTotal.Col = frmCargaDigitacion.grdErrTotal.MaxCols
        frmCargaDigitacion.grdErrTotal.Text = frmCargaDigitacion.grdErrTotal.Text & strErrorRegTot
    End If

    If strLugarMto = "ENC" Then
        frmCargaDigitacion.grdErrEncab.Col = frmCargaDigitacion.grdErrEncab.MaxCols
        frmCargaDigitacion.grdErrEncab.Text = frmCargaDigitacion.grdErrEncab.Text & strErrorRegMto
    ElseIf strLugarMto = "TOT" Then
        frmCargaDigitacion.grdErrTotal.Col = frmCargaDigitacion.grdErrTotal.MaxCols
        frmCargaDigitacion.grdErrTotal.Text = frmCargaDigitacion.grdErrTotal.Text & strErrorRegMto
    End If
    
    If frmCargaDigitacion.grdErrDetalle.MaxRows > 0 Then
        frmCargaDigitacion.Mostrar_Errores frmCargaDigitacion.grdErrDetalle, 1, frmCargaDigitacion.grdErrDetalle.MaxCols
    End If
    
Salir:
    Exit Sub
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbExclamation, "Error"
    GoTo Salir
    Resume 0
End Sub


Public Function Busca_Conversion(strCodigo As Variant, arrArreglo() As Variant) As String
   Dim lngInd As Long
   
   On Error GoTo Errores
   
    Busca_Conversion = ""
    If strCodigo = "" Then GoTo Salir
    
    For lngInd = 0 To UBound(arrArreglo)
        If arrArreglo(lngInd, 0) = Trim(strCodigo) Then
            Busca_Conversion = arrArreglo(lngInd, 1)
            Exit For
        End If
    Next
    
Salir:
   Screen.MousePointer = vbNormal
    Exit Function
    
Errores:
    MsgBox Err.Number & Chr(13) & Err.Description, vbInformation, "Buscando Conversi�n"
    
End Function

Public Sub Desplegar_Archivo(strArchivo As String, blnTodos As Boolean)
    Dim strReg As String
    Dim ArchDisp As Byte
    
    ArchDisp = FreeFile()
    Open strArchivo For Input As #ArchDisp
    
    frmCargaDigitacion.grdDetalle.MaxRows = 0
    Do While Not EOF(1)
        Line Input #1, strReg
        Call Mostrar_Reg(UCase(Descomponer(strReg)), _
                                    frmCargaDigitacion.grdEncab, _
                                    frmCargaDigitacion.grdDetalle, _
                                    frmCargaDigitacion.grdTotal _
                                    )
        If Not blnTodos And frmCargaDigitacion.grdDetalle.Row = 30 Then Exit Do
    Loop
    
    Close #ArchDisp
End Sub

Public Sub Carga_Documentos()
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
Dim lngRgtros  As Long
Dim lngInd  As Long
On Error GoTo LabelError

    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "DBO_FACTOR.PKG_FME_TIPO_DOCTOS.SP_FME_DOCUMENTOS_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigo", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInputOutput, 80, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
        
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser cargada", vbInformation, "Tipos de Documentos"
        GoTo LabelSalir
    End If
    
    lngRgtros = recRegistros.RecordCount - 1
    ReDim garrDocumentos(0 To lngRgtros, 0 To 3)

    lngInd = 0
    Do While Not recRegistros.EOF
        
        garrDocumentos(lngInd, 0) = recRegistros.Fields("IDE_TIPO_DOC").Value
        garrDocumentos(lngInd, 1) = recRegistros.Fields("GLS_DESCRIPCION").Value
        garrDocumentos(lngInd, 2) = recRegistros.Fields("MRC_OPERATORIA").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
LabelSalir:
    Screen.MousePointer = 0
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Sub Carga_Origen(blnOpcionTodosEnCombo As Boolean)
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
Dim lngRgtros  As Long
Dim lngInd  As Long
On Error GoTo LabelError

    Screen.MousePointer = 11
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "CSBPI.PKG_FME_PARAMETROS.SP_FME_PRMTROS_VER"
    
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigotipo", adNumeric, adParamInput, 10, 1)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigosubtipo", adNumeric, adParamInput, 10, 13)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_codigoparametro", adNumeric, adParamInput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_coderror", adNumeric, adParamOutput, 10)
        
    Set recRegistros = Cmd.Execute
    
        
    lngRgtros = recRegistros.RecordCount - 1

    If blnOpcionTodosEnCombo Then
        lngInd = 1
        ReDim garrOrigen(0 To lngRgtros + 1, 0 To 2)
    Else
        lngInd = 0
        ReDim garrOrigen(0 To lngRgtros, 0 To 2)
    End If
    
    Do While Not recRegistros.EOF
        
        garrOrigen(lngInd, 0) = recRegistros.Fields("IDE_UNICO_PARAM").Value
        garrOrigen(lngInd, 1) = recRegistros.Fields("GLS_DESCRIPCION").Value
        garrOrigen(lngInd, 2) = recRegistros.Fields("NOM_CORTO").Value
        
        recRegistros.MoveNext
        lngInd = lngInd + 1
    Loop
    
    
LabelSalir:
    Screen.MousePointer = 0
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub
Public Sub Carga_EstructuraDigitacion(lngIdeCliente As Long, lngIdeProducto As Long, lngIdeSucursal As Long, ByRef blnExiteEstructura As Boolean)
    
    Dim lngRgtros As Long
    Dim lngInd As Long
    Dim recRegistros As New ADODB.Recordset
    Dim Cmd As New ADODB.Command
    Dim intCol As Integer
    Dim lngIndEnc As Long
    Dim lngIndDet As Long
    Dim lngIndTot As Long
    Dim lngFila As Long
    On Error GoTo LabelError

    Screen.MousePointer = 11
    glngCantControles = 0
    blnExiteEstructura = False
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "dbo_factor.pkg_fme_carga_digitacion.sp_fme_estructura_cliente_ver"

    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Cliente", adNumeric, adParamInput, 10, lngIdeCliente)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Producto", adNumeric, adParamInput, 10, lngIdeProducto)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Sucursal", adNumeric, adParamInput, 10, lngIdeSucursal)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser cargada. " & Chr(13) & _
                    Cmd.Parameters("p_v_DescError").Value, vbInformation, "Carga Estructura Archivo Cliente"
        GoTo LabelSalir
    End If
    
    If Not recRegistros.EOF Then
        blnExiteEstructura = True
        lngRgtros = recRegistros.RecordCount - 1
        ReDim garrEstrucArchClie(0 To lngRgtros, 0 To 20)
        ReDim garrEstructura(lngRgtros + 1, 2)
        
        gblnConEncab = False
        gblnConDetalle = False
        gblnConTotal = False
        
        lngInd = 0
        
        While Not recRegistros.EOF
            Rem LLenamos el Arreglo con la estructura
'            garrEstrucArchClie(lngInd, 0) = "" & recRegistros.Fields("NOM_CORTO").Value
'            garrEstrucArchClie(lngInd, 1) = "" & recRegistros.Fields("COD_IDE_CAMPO").Value
'            garrEstrucArchClie(lngInd, 2) = "" & recRegistros.Fields("GLS_DESCRIPCION").Value
'            garrEstrucArchClie(lngInd, 3) = "" & recRegistros.Fields("NRO_CORR_DETALLE").Value
'            garrEstrucArchClie(lngInd, 4) = "" & recRegistros.Fields("NRO_CORR_CAMPO").Value
'            garrEstrucArchClie(lngInd, 5) = "" & recRegistros.Fields("GLS_CAMPO").Value
'            garrEstrucArchClie(lngInd, 6) = "" & recRegistros.Fields("COD_TIPO_CAMPO").Value
'            garrEstrucArchClie(lngInd, 7) = "" & recRegistros.Fields("VLR_POSICION").Value
'            garrEstrucArchClie(lngInd, 8) = "" & recRegistros.Fields("VLR_PRECISION").Value
'            garrEstrucArchClie(lngInd, 9) = "" & recRegistros.Fields("VLR_DECIMALES").Value
'            garrEstrucArchClie(lngInd, 10) = "" & recRegistros.Fields("MRC_ALINEACION").Value
'            garrEstrucArchClie(lngInd, 11) = "" & recRegistros.Fields("MRC_RELLENO").Value
'            garrEstrucArchClie(lngInd, 12) = "" & recRegistros.Fields("FLG_CONVERTIR").Value
'            garrEstrucArchClie(lngInd, 13) = "" & recRegistros.Fields("GLS_FORMATO").Value
'            garrEstrucArchClie(lngInd, 14) = "" & recRegistros.Fields("COD_ESTADO").Value
'            garrEstrucArchClie(lngInd, 15) = "" & recRegistros.Fields("MRC_BLOQUEO").Value
'            garrEstrucArchClie(lngInd, 16) = "" & recRegistros.Fields("FLG_OBLIGATORIO").Value
'            garrEstrucArchClie(lngInd, 17) = "" & recRegistros.Fields("Tipo_Formato").Value
'            garrEstrucArchClie(lngInd, 18) = "" & recRegistros.Fields("nom_cto_Tip_Fmto").Value
'            garrEstrucArchClie(lngInd, 19) = "" & recRegistros.Fields("ide_unico_Tip_fmto").Value
'            garrEstrucArchClie(lngInd, 20) = "" & recRegistros.Fields("Separador").Value
            
            If "" & recRegistros.Fields("NOM_CORTO").Value = "ENC" Then
                    gblnConEncab = True
                    intCol = 0
                    lngFila = lngIndEnc
                    lngIndEnc = lngIndEnc + 1
            ElseIf "" & recRegistros.Fields("NOM_CORTO").Value = "DET" Then
                    gblnConDetalle = True
                    intCol = 1
                    lngFila = lngIndDet
                    lngIndDet = lngIndDet + 1
            ElseIf "" & recRegistros.Fields("NOM_CORTO").Value = "TOT" Then
                    gblnConTotal = True
                    intCol = 2
                    lngFila = lngIndTot
                    lngIndTot = lngIndTot + 1
            End If
            
            garrEstructura(lngFila, intCol).Nom_Corto = "DET"
            garrEstructura(lngFila, intCol).Cod_Ide_Campo = "" & recRegistros.Fields("COD_IDE_CAMPO").Value
            garrEstructura(lngFila, intCol).Gls_Descripcion = "RUT_RECEPTOR"
            garrEstructura(lngFila, intCol).Nom_Corto_Campo = "" & recRegistros.Fields("Nom_Cto_Campo").Value
            garrEstructura(lngFila, intCol).Nro_Corr_Detalle = "" & recRegistros.Fields("NRO_CORR_DETALLE").Value
            garrEstructura(lngFila, intCol).Nro_Corr_Campo = "" & recRegistros.Fields("NRO_CORR_CAMPO").Value
            garrEstructura(lngFila, intCol).Gls_Campo = "" & recRegistros.Fields("GLS_CAMPO").Value
            garrEstructura(lngFila, intCol).Cod_Tipo_Campo = "" & recRegistros.Fields("COD_TIPO_CAMPO").Value
            garrEstructura(lngFila, intCol).Nom_Cto_Tip_Cpo = "" & recRegistros.Fields("Nom_Cto_Tip_Cpo").Value
            garrEstructura(lngFila, intCol).Vlr_Posicion = "" & recRegistros.Fields("VLR_POSICION").Value
            garrEstructura(lngFila, intCol).Vlr_Precision = "" & recRegistros.Fields("VLR_PRECISION").Value
            garrEstructura(lngFila, intCol).Vlr_Decimales = "" & recRegistros.Fields("VLR_DECIMALES").Value
            garrEstructura(lngFila, intCol).Mrc_Alineacion = "" & recRegistros.Fields("MRC_ALINEACION").Value
            garrEstructura(lngFila, intCol).Mrc_Relleno = "" & recRegistros.Fields("MRC_RELLENO").Value
            garrEstructura(lngFila, intCol).Flg_Convertir = "" & recRegistros.Fields("FLG_CONVERTIR").Value
            garrEstructura(lngFila, intCol).Gls_Formato = "" & recRegistros.Fields("GLS_FORMATO").Value
            garrEstructura(lngFila, intCol).Cod_Estado = "" & recRegistros.Fields("COD_ESTADO").Value
            garrEstructura(lngFila, intCol).Mrc_Bloqueo = "" & recRegistros.Fields("MRC_BLOQUEO").Value
            garrEstructura(lngFila, intCol).Flg_Obligatorio = "" & recRegistros.Fields("FLG_OBLIGATORIO").Value
            garrEstructura(lngFila, intCol).Tipo_Formato = "" & recRegistros.Fields("Tipo_Formato").Value
            garrEstructura(lngFila, intCol).Nom_Cto_Tip_Fmto = "" & recRegistros.Fields("nom_cto_Tip_Fmto").Value
            garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto = "" & recRegistros.Fields("ide_unico_Tip_fmto").Value
            garrEstructura(lngFila, intCol).Separador = "" & recRegistros.Fields("Separador").Value
            garrEstructura(lngFila, intCol).Informacion = ""
            
            If garrEstructura(lngFila, intCol).Flg_Convertir = 1 Then
                Select Case LCase(garrEstructura(lngFila, intCol).Gls_Descripcion)
                    Case "gls_comuna"
                        Call CargaArrayConv(garrConvComuna(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                    Case "cod_pais"
                        Call CargaArrayConv(garrConvPais(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                    Case "cod_tipo_docto"
                        Call CargaArrayConv(garrConvTipoDocto(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                    Case "cod_banco"
                        Call CargaArrayConv(garrConvBanco(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                    Case "cod_plaza"
                        Call CargaArrayConv(garrConvPlaza(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                    Case "cod_forma_pago"
                        Call CargaArrayConv(garrConvFormaPago(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                    Case "cod_moneda"
                        Call CargaArrayConv(garrConvMoneda(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                End Select
            End If
            
            lngInd = lngInd + 1
            recRegistros.MoveNext
        Wend
    End If

    Rem BORRAR ESTE CODIGO. SOLAMENTE ES USADO PARA ALA PROGRAMACION
 'BEGIN BORRAR
    Open "C:\factor40\formato.txt" For Output As #100
    For intCol = 0 To 2
        For lngFila = 0 To UBound(garrEstructura)
            If garrEstructura(lngFila, intCol).Nom_Corto = "" Then Exit For


            Print #100, lngFila; ","; intCol; "  Nom_Corto          = "; garrEstructura(lngFila, intCol).Nom_Corto
            Print #100, lngFila; ","; intCol; "  Cod_Ide_Campo      = "; garrEstructura(lngFila, intCol).Cod_Ide_Campo
            Print #100, lngFila; ","; intCol; "  Gls_Descripcion    = "; garrEstructura(lngFila, intCol).Gls_Descripcion
            Print #100, lngFila; ","; intCol; "  Nom_Corto_Campo    = "; garrEstructura(lngFila, intCol).Nom_Corto_Campo
            Print #100, lngFila; ","; intCol; "  Nro_Corr_Detalle   = "; garrEstructura(lngFila, intCol).Nro_Corr_Detalle
            Print #100, lngFila; ","; intCol; "  Nro_Corr_Campo     = "; garrEstructura(lngFila, intCol).Nro_Corr_Campo
            Print #100, lngFila; ","; intCol; "  Gls_Campo          = "; garrEstructura(lngFila, intCol).Gls_Campo
            Print #100, lngFila; ","; intCol; "  Cod_Tipo_Campo     = "; garrEstructura(lngFila, intCol).Cod_Tipo_Campo
            Print #100, lngFila; ","; intCol; "  Nom_Cto_Tip_Cpo    = "; garrEstructura(lngFila, intCol).Nom_Cto_Tip_Cpo
            Print #100, lngFila; ","; intCol; "  Vlr_Posicion       = "; garrEstructura(lngFila, intCol).Vlr_Posicion
            Print #100, lngFila; ","; intCol; "  Vlr_Precision      = "; garrEstructura(lngFila, intCol).Vlr_Precision
            Print #100, lngFila; ","; intCol; "  Vlr_Decimales      = "; garrEstructura(lngFila, intCol).Vlr_Decimales
            Print #100, lngFila; ","; intCol; "  Mrc_Alineacion     = "; garrEstructura(lngFila, intCol).Mrc_Alineacion
            Print #100, lngFila; ","; intCol; "  Mrc_Relleno        = "; garrEstructura(lngFila, intCol).Mrc_Relleno
            Print #100, lngFila; ","; intCol; "  Flg_Convertir      = "; garrEstructura(lngFila, intCol).Flg_Convertir
            Print #100, lngFila; ","; intCol; "  Gls_Formato        = "; garrEstructura(lngFila, intCol).Gls_Formato
            Print #100, lngFila; ","; intCol; "  Cod_Estado         = "; garrEstructura(lngFila, intCol).Cod_Estado
            Print #100, lngFila; ","; intCol; "  Mrc_Bloqueo        = "; garrEstructura(lngFila, intCol).Mrc_Bloqueo
            Print #100, lngFila; ","; intCol; "  Flg_Obligatorio    = "; garrEstructura(lngFila, intCol).Flg_Obligatorio
            Print #100, lngFila; ","; intCol; "  Tipo_Formato       = "; garrEstructura(lngFila, intCol).Tipo_Formato
            Print #100, lngFila; ","; intCol; "  Nom_Cto_Tip_Fmto   = "; garrEstructura(lngFila, intCol).Nom_Cto_Tip_Fmto
            Print #100, lngFila; ","; intCol; "  Ide_Unico_Tip_Fmto = "; garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto
            Print #100, lngFila; ","; intCol; "  Separador          = "; garrEstructura(lngFila, intCol).Separador
            Print #100, ""

        Next
    Next

'END BORRAR

LabelSalir:
    Close #100
    Screen.MousePointer = 0
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub
    
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Public Sub Carga_Estructura(lngIdeCliente As Long, lngIdeContrato As Long, lngIdeProducto As Long, lngIdeSucursal As Long, ByRef blnExiteEstructura As Boolean)
    Dim Cmd As New ADODB.Command
    Dim recRegistros As New ADODB.Recordset
    Dim lngRgtros As Long
    Dim lngInd As Long
    Dim intCol As Integer
    Dim lngIndEnc As Long
    Dim lngIndDet As Long
    Dim lngIndTot As Long
    Dim lngFila As Long
    Dim strClaseProd As String
    Dim strClaseTipDoc  As String
    Dim blnExiste As Boolean
    
    
    On Error GoTo LabelError

    Screen.MousePointer = 11
    glngCantControles = 0
    blnExiteEstructura = False
    gblnConEncab = False
    gblnConDetalle = False
    gblnConTotal = False
    
    
    If gblnEsCarga Then  '----------------------------------------------- CARGA ------------------------------------------------
         
        Cmd.ActiveConnection = gAdoCon
        Cmd.CommandType = adCmdStoredProc
        Cmd.CommandText = "csbpi.pkg_fme_carga_digitacion.sp_fme_estructura_cliente_ver"
    
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Cliente", adNumeric, adParamInput, 10, lngIdeCliente)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Producto", adNumeric, adParamInput, 10, lngIdeProducto)
        Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Sucursal", adNumeric, adParamInput, 10, lngIdeSucursal)
        Cmd.Parameters.Append Cmd.CreateParameter("p_v_DescError", adVarChar, adParamOutput, 1000)
        Set recRegistros = Cmd.Execute
        
        If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
            MsgBox "La informaci�n no pudo ser cargada. " & Chr(13) & _
                        Cmd.Parameters("p_v_DescError").Value, vbInformation, "Carga Estructura Archivo Cliente"
            GoTo LabelSalir
        End If
    
        If Not recRegistros.EOF Then
            blnExiteEstructura = True
            lngRgtros = recRegistros.RecordCount - 1
'            ReDim garrEstrucArchClie(0 To lngRgtros, 0 To 20)
            ReDim garrEstructura(lngRgtros + 1, 2)
            
            lngInd = 0
            
            While Not recRegistros.EOF
                
                If "" & recRegistros.Fields("NOM_CORTO").Value = "ENC" Then
                        gblnConEncab = True
                        intCol = 0
                        lngFila = lngIndEnc
                        lngIndEnc = lngIndEnc + 1
                ElseIf "" & recRegistros.Fields("NOM_CORTO").Value = "DET" Then
                        gblnConDetalle = True
                        intCol = 1
                        lngFila = lngIndDet
                        lngIndDet = lngIndDet + 1
                ElseIf "" & recRegistros.Fields("NOM_CORTO").Value = "TOT" Then
                        gblnConTotal = True
                        intCol = 2
                        lngFila = lngIndTot
                        lngIndTot = lngIndTot + 1
                End If
                
                garrEstructura(lngFila, intCol).Nom_Corto = "" & recRegistros.Fields("NOM_CORTO").Value
                garrEstructura(lngFila, intCol).Cod_Ide_Campo = "" & recRegistros.Fields("COD_IDE_CAMPO").Value
                garrEstructura(lngFila, intCol).Gls_Descripcion = "" & recRegistros.Fields("GLS_DESCRIPCION").Value
                garrEstructura(lngFila, intCol).Nom_Corto_Campo = "" & recRegistros.Fields("Nom_Cto_Campo").Value
                garrEstructura(lngFila, intCol).Nro_Corr_Detalle = "" & recRegistros.Fields("NRO_CORR_DETALLE").Value
                garrEstructura(lngFila, intCol).Nro_Corr_Campo = "" & recRegistros.Fields("NRO_CORR_CAMPO").Value
                garrEstructura(lngFila, intCol).Gls_Campo = "" & recRegistros.Fields("GLS_CAMPO").Value
                garrEstructura(lngFila, intCol).Cod_Tipo_Campo = "" & recRegistros.Fields("COD_TIPO_CAMPO").Value
                garrEstructura(lngFila, intCol).Nom_Cto_Tip_Cpo = "" & recRegistros.Fields("Nom_Cto_Tip_Cpo").Value
                garrEstructura(lngFila, intCol).Vlr_Posicion = "" & recRegistros.Fields("VLR_POSICION").Value
                garrEstructura(lngFila, intCol).Vlr_Precision = "" & recRegistros.Fields("VLR_PRECISION").Value
                garrEstructura(lngFila, intCol).Vlr_Decimales = "" & recRegistros.Fields("VLR_DECIMALES").Value
                garrEstructura(lngFila, intCol).Mrc_Alineacion = "" & recRegistros.Fields("MRC_ALINEACION").Value
                garrEstructura(lngFila, intCol).Mrc_Relleno = "" & recRegistros.Fields("MRC_RELLENO").Value
                garrEstructura(lngFila, intCol).Flg_Convertir = "" & recRegistros.Fields("FLG_CONVERTIR").Value
                garrEstructura(lngFila, intCol).Gls_Formato = "" & recRegistros.Fields("GLS_FORMATO").Value
                garrEstructura(lngFila, intCol).Cod_Estado = "" & recRegistros.Fields("COD_ESTADO").Value
                garrEstructura(lngFila, intCol).Mrc_Bloqueo = "" & recRegistros.Fields("MRC_BLOQUEO").Value
                garrEstructura(lngFila, intCol).Flg_Obligatorio = "" & recRegistros.Fields("FLG_OBLIGATORIO").Value
                garrEstructura(lngFila, intCol).Tipo_Formato = "" & recRegistros.Fields("Tipo_Formato").Value
                garrEstructura(lngFila, intCol).Nom_Cto_Tip_Fmto = "" & recRegistros.Fields("nom_cto_Tip_Fmto").Value
                garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto = "" & recRegistros.Fields("ide_unico_Tip_fmto").Value
                garrEstructura(lngFila, intCol).Separador = "" & recRegistros.Fields("Separador").Value
                garrEstructura(lngFila, intCol).SepDecimal = "" & recRegistros.Fields("SepDecimal").Value
                garrEstructura(lngFila, intCol).Informacion = ""
                
                If garrEstructura(lngFila, intCol).Flg_Convertir = 1 Then
                    Select Case LCase(garrEstructura(lngFila, intCol).Gls_Descripcion)
                        Case "gls_comuna"
                            Call CargaArrayConv(garrConvComuna(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                        Case "cod_pais"
                            Call CargaArrayConv(garrConvPais(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                        Case "cod_tipo_docto"
                            Call CargaArrayConv(garrConvTipoDocto(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                        Case "cod_banco"
                            Call CargaArrayConv(garrConvBanco(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                        Case "cod_plaza"
                            Call CargaArrayConv(garrConvPlaza(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                        Case "cod_forma_pago"
                            Call CargaArrayConv(garrConvFormaPago(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                        Case "cod_moneda"
                            Call CargaArrayConv(garrConvMoneda(), garrEstructura(lngFila, intCol).Cod_Ide_Campo, garrEstructura(lngFila, intCol).Gls_Descripcion, garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto)
                    End Select
                End If
                
                lngInd = lngInd + 1
                recRegistros.MoveNext
            Wend
        End If

    Else '---------------------------------------------------------------- DIGITACION ------------------------------------------------------------
    
'        strClaseProd = Clase_Producto(lngIdeProducto, "CODIGO")
'        strClaseTipDoc = Clase_TipoDoc(lngIdeContrato, lngIdeProducto, "CODIGO")
'        If strClaseProd = Chr(0) Or strClaseTipDoc = Chr(0) Then GoTo LabelSalir
'
'        Cmd.ActiveConnection = gAdoCon
'        Cmd.CommandType = adCmdStoredProc
'        Cmd.CommandText = "dbo_factor.pkg_fme_campos_x_digitacion.sp_fme_plantilla_ver"
'
'        Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
'        Cmd.Parameters.Append Cmd.CreateParameter("p_c_cl_prod", adNumeric, adParamInput, 8, strClaseProd)
'        Cmd.Parameters.Append Cmd.CreateParameter("p_c_cl_tipdoc", adChar, adParamInput, 8, strClaseTipDoc)
'        Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
'        Set recRegistros = Cmd.Execute
'
'        If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
'            MsgBox "La informaci�n no pudo ser cargada. " & Chr(13) & _
'                        Cmd.Parameters("p_v_DescError").Value, vbInformation, "Carga Plantilla Digitaci�n"
'            GoTo LabelSalir
'        End If
'
'        If Not recRegistros.EOF Then
'            blnExiteEstructura = True
'            lngRgtros = recRegistros.RecordCount - 1
'            ReDim garrEstructura(lngRgtros + 1, 2)
'
'            gblnConDetalle = True
'
'            intCol = 1
'            lngFila = 0
'            While Not recRegistros.EOF
'                garrEstructura(lngFila, intCol).Nom_Corto = "DET"
'                garrEstructura(lngFila, intCol).Cod_Ide_Campo = "" & recRegistros.Fields("cod_ide_campo").Value
'                garrEstructura(lngFila, intCol).Gls_Descripcion = "" & recRegistros.Fields("nom_campo").Value
'                garrEstructura(lngFila, intCol).Nom_Corto_Campo = ""
'                garrEstructura(lngFila, intCol).Nro_Corr_Detalle = 0
'                garrEstructura(lngFila, intCol).Nro_Corr_Campo = "" & recRegistros.Fields("nro_corr_columna").Value
'                garrEstructura(lngFila, intCol).Gls_Campo = "" & recRegistros.Fields("glosa").Value
'                garrEstructura(lngFila, intCol).Cod_Tipo_Campo = "" & recRegistros.Fields("cod_tipo_campo").Value
'                garrEstructura(lngFila, intCol).Nom_Cto_Tip_Cpo = "" & recRegistros.Fields("nom_cto_tip_cpo").Value
'                garrEstructura(lngFila, intCol).Vlr_Posicion = "" & recRegistros.Fields("vlr_posicion").Value
'                garrEstructura(lngFila, intCol).Vlr_Precision = "" & recRegistros.Fields("vlr_precision").Value
'                garrEstructura(lngFila, intCol).Vlr_Decimales = 0 & recRegistros.Fields("vlr_decimales").Value
'                garrEstructura(lngFila, intCol).Mrc_Alineacion = "" & recRegistros.Fields("mrc_alineacion").Value
'                garrEstructura(lngFila, intCol).Mrc_Relleno = 0
'                garrEstructura(lngFila, intCol).Flg_Convertir = 0
'                garrEstructura(lngFila, intCol).Gls_Formato = ""
'                garrEstructura(lngFila, intCol).Cod_Estado = ""
'                garrEstructura(lngFila, intCol).Mrc_Bloqueo = 0
'                garrEstructura(lngFila, intCol).Flg_Obligatorio = "" & recRegistros.Fields("flg_obligatorio").Value
'                garrEstructura(lngFila, intCol).Tipo_Formato = ""
'                garrEstructura(lngFila, intCol).Nom_Cto_Tip_Fmto = ""
'                garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto = ""
'                garrEstructura(lngFila, intCol).Separador = ""
'                garrEstructura(lngFila, intCol).Informacion = ""
'
'                lngFila = lngFila + 1
'                recRegistros.MoveNext
'            Wend
'        End If
        
    End If
    
    
'    Rem BORRAR ESTE CODIGO. SOLAMENTE ES USADO PARA ALA PROGRAMACION
' 'BEGIN BORRAR
'
'    Open "C:\factor40\formato.txt" For Output As #100
' If Not blnExiteEstructura Then GoTo LabelSalir
'    For intCol = 0 To 2
'        For lngFila = 0 To UBound(garrEstructura)
'            If garrEstructura(lngFila, intCol).Nom_Corto = "" Then Exit For
'
'
'            Print #100, lngFila; ","; intCol; "  Nom_Corto          = "; garrEstructura(lngFila, intCol).Nom_Corto
'            Print #100, lngFila; ","; intCol; "  Cod_Ide_Campo      = "; garrEstructura(lngFila, intCol).Cod_Ide_Campo
'            Print #100, lngFila; ","; intCol; "  Gls_Descripcion    = "; garrEstructura(lngFila, intCol).Gls_Descripcion
'            Print #100, lngFila; ","; intCol; "  Nom_Corto_Campo    = "; garrEstructura(lngFila, intCol).Nom_Corto_Campo
'            Print #100, lngFila; ","; intCol; "  Nro_Corr_Detalle   = "; garrEstructura(lngFila, intCol).Nro_Corr_Detalle
'            Print #100, lngFila; ","; intCol; "  Nro_Corr_Campo     = "; garrEstructura(lngFila, intCol).Nro_Corr_Campo
'            Print #100, lngFila; ","; intCol; "  Gls_Campo          = "; garrEstructura(lngFila, intCol).Gls_Campo
'            Print #100, lngFila; ","; intCol; "  Cod_Tipo_Campo     = "; garrEstructura(lngFila, intCol).Cod_Tipo_Campo
'            Print #100, lngFila; ","; intCol; "  Nom_Cto_Tip_Cpo    = "; garrEstructura(lngFila, intCol).Nom_Cto_Tip_Cpo
'            Print #100, lngFila; ","; intCol; "  Vlr_Posicion       = "; garrEstructura(lngFila, intCol).Vlr_Posicion
'            Print #100, lngFila; ","; intCol; "  Vlr_Precision      = "; garrEstructura(lngFila, intCol).Vlr_Precision
'            Print #100, lngFila; ","; intCol; "  Vlr_Decimales      = "; garrEstructura(lngFila, intCol).Vlr_Decimales
'            Print #100, lngFila; ","; intCol; "  Mrc_Alineacion     = "; garrEstructura(lngFila, intCol).Mrc_Alineacion
'            Print #100, lngFila; ","; intCol; "  Mrc_Relleno        = "; garrEstructura(lngFila, intCol).Mrc_Relleno
'            Print #100, lngFila; ","; intCol; "  Flg_Convertir      = "; garrEstructura(lngFila, intCol).Flg_Convertir
'            Print #100, lngFila; ","; intCol; "  Gls_Formato        = "; garrEstructura(lngFila, intCol).Gls_Formato
'            Print #100, lngFila; ","; intCol; "  Cod_Estado         = "; garrEstructura(lngFila, intCol).Cod_Estado
'            Print #100, lngFila; ","; intCol; "  Mrc_Bloqueo        = "; garrEstructura(lngFila, intCol).Mrc_Bloqueo
'            Print #100, lngFila; ","; intCol; "  Flg_Obligatorio    = "; garrEstructura(lngFila, intCol).Flg_Obligatorio
'            Print #100, lngFila; ","; intCol; "  Tipo_Formato       = "; garrEstructura(lngFila, intCol).Tipo_Formato
'            Print #100, lngFila; ","; intCol; "  Nom_Cto_Tip_Fmto   = "; garrEstructura(lngFila, intCol).Nom_Cto_Tip_Fmto
'            Print #100, lngFila; ","; intCol; "  Ide_Unico_Tip_Fmto = "; garrEstructura(lngFila, intCol).Ide_Unico_Tip_Fmto
'            Print #100, lngFila; ","; intCol; "  Separador          = "; garrEstructura(lngFila, intCol).Separador
'            Print #100, ""
'
'        Next
'    Next
'
''END BORRAR

LabelSalir:
    Close #100
    Screen.MousePointer = 0
    Set recRegistros = Nothing
    Set Cmd = Nothing
    Exit Sub
    
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub


Sub EstableceEstructuraGrilla(Grilla1 As vaSpread, Grilla2 As vaSpread, Grilla3 As vaSpread)
    Dim lngRow As Long
    Dim intCol As Integer
    Dim sngColWidth As Single
    
    On Error GoTo LabelError
    
    Grilla1.MaxRows = 1
    Grilla1.MaxCols = 0
    Grilla2.MaxRows = 0
    Grilla2.MaxCols = 0
    Grilla3.MaxRows = 1
    Grilla3.MaxCols = 0
    
    Grilla1.Row = 1
    Grilla3.Row = 1
        
    For intCol = 0 To 2
        For lngRow = 0 To UBound(garrEstructura)
            Select Case UCase(garrEstructura(lngRow, intCol).Nom_Corto)
                Case "ENC"
                    Grilla1.MaxCols = Grilla1.MaxCols + 2
                    Grilla1.Col = Grilla1.MaxCols - 1
                    Grilla1.Text = StrConv(garrEstructura(lngRow, intCol).Gls_Campo, vbProperCase)
                    Grilla1.TypeHAlign = 0
                    Grilla1.TwipsToColWidth Printer.TextWidth(Grilla1.Text + "O"), sngColWidth
                    Grilla1.ColWidth(Grilla1.Col) = sngColWidth
                    Grilla1.BackColor = vbButtonFace
                    If garrEstructura(lngRow, intCol).Cod_Ide_Campo = -99 Then 'filler
                        Grilla1.ColHidden = True
                        Grilla1.Col = Grilla1.MaxCols
                        Grilla1.ColHidden = True
                    End If
            
                Case "DET"
                    Grilla2.Row = 0
                    Grilla2.MaxCols = Grilla2.MaxCols + 1
                    Grilla2.Col = Grilla2.MaxCols
                    Grilla2.Text = StrConv(garrEstructura(lngRow, intCol).Gls_Campo, vbProperCase)
                    Grilla2.TwipsToColWidth Printer.TextWidth(Grilla2.Text + "O"), sngColWidth
                    Grilla2.ColWidth(Grilla2.Col) = sngColWidth
                    Grilla2.TypeHAlign = 0
                    
                    If Left(garrEstructura(lngRow, intCol).Gls_Descripcion, 6) <> "IDE_DV" Then
                        glngCantControles = glngCantControles + 1
                    End If
                    
                    If garrEstructura(lngRow, intCol).Cod_Ide_Campo = -99 Then 'filler
                        Grilla2.ColHidden = True
                    End If
            
                Case "TOT"
                    Grilla3.MaxCols = Grilla3.MaxCols + 2
                    Grilla3.Col = Grilla3.MaxCols - 1
                    Grilla3.Text = StrConv(garrEstructura(lngRow, intCol).Gls_Campo, vbProperCase)
                    Grilla3.TypeHAlign = 0
                    Grilla3.TwipsToColWidth Printer.TextWidth(Grilla3.Text + "O"), sngColWidth
                    Grilla3.ColWidth(Grilla3.Col) = sngColWidth
                    Grilla3.BackColor = vbButtonFace
                    If garrEstructura(lngRow, intCol).Cod_Ide_Campo = -99 Then 'filler
                        Grilla3.ColHidden = True
                        Grilla3.Col = Grilla3.MaxCols
                        Grilla3.ColHidden = True
                    End If
            
            End Select
        Next
    Next
        
    Rem Se Incorpora columana que contendra el numero fila
    Grilla2.Row = 0
    Grilla2.MaxCols = Grilla2.MaxCols + 1
    Grilla2.Col = Grilla2.MaxCols
    Grilla2.Text = "Nro Fila"
    Grilla2.ColHidden = True

LabelSalir:
    Exit Sub

LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub
Public Sub GrillasFmtoOK(Grilla1 As vaSpread, Grilla3 As vaSpread)
    Grilla1.MaxCols = 0
    Grilla3.MaxCols = 0
End Sub


'Sub EstableceEstrucFormulario(frm As Form, strMarca As String)
'    Dim intTabIndex As Integer
'    Dim lngInd As Long
'    Dim o As Object
'    Dim intTopCtr As Integer
'    Dim intTopLbl As Integer
'    Dim intLefCtr As Integer
'    Dim intLefLbl As Integer
'
'    On Error GoTo LabelError
'
'    intTopCtr = 115     'Top del Primer Control
'    intTopLbl = 160     'Top del Primer Label
'    intLefCtr = 1470    'Left del Primer Control
'    intLefLbl = 120      'Left del Primer Label
'
'    frmDigitacion.vsbDigitacion.Visible = glngCantControles > 17
'
'    Rem Recorre los campos de la estructura del cliente
'    For lngInd = 0 To UBound(garrEstructura)
'        Rem Ubica solo los registros pertenecientes al detalle
'        If UCase(garrEstructura(lngInd, 1).Nom_Corto) = "DET" Then
'            Rem Recorre los controles ubicando el nombre del control que sea igual al campo fisico
'            For Each o In frm.Controls
'                If UCase(Trim(o.Tag)) = UCase(Trim(strMarca)) Then
'                    Rem asigna al caption del Label el nombre campo entrada
'                    If TypeOf o Is Label Then
'                        If o.Name = UCase(Trim(garrEstructura(lngInd, 1).Gls_Descripcion) & "2") Then
'                            o.Caption = StrConv(garrEstructura(lngInd, 1).Gls_Campo, vbProperCase)
'                            o.Visible = True
'                            o.Left = intLefLbl
'                            o.Top = intTopLbl
'                            If intLefLbl = 4650 Then
'                                intLefLbl = 120
'                                intTopLbl = intTopLbl + 350
'                            Else
'                                intLefLbl = 4650
'                            End If
'                        End If
'                    ElseIf TypeOf o Is TextBox Or TypeOf o Is ComboBox Or TypeOf o Is DTPicker Then
'                        If o.Name = UCase(Trim(garrEstructura(lngInd, 1).Gls_Descripcion)) Then
'                            o.Visible = True
'                            o.TabIndex = intTabIndex
'                            If Left(UCase(Trim(garrEstructura(lngInd, 1).Gls_Descripcion)), 6) = "IDE_DV" Then
'                                If intLefCtr = 1470 Then
'                                    intTopCtr = intTopCtr - 335
'                                    intLefCtr = 6000
'                                Else
'                                    intLefCtr = 1470
'                                End If
'                                o.Left = intLefCtr + 1100
'                            Else
'                                o.Left = intLefCtr
'                            End If
'                            o.Top = intTopCtr
'                            If intLefCtr = 6000 Then
'                                intLefCtr = 1470
'                                intTopCtr = intTopCtr + IIf(TypeOf o Is TextBox, 335, 360)
'                            Else
'                                intLefCtr = 6000
'                            End If
'
'                            Rem Incrementa el correlativo del Tabindex
'                            intTabIndex = intTabIndex + 1
'                            Rem Marca el registro si es obligatorio
'                            If Val(garrEstructura(lngInd, 1).Flg_Obligatorio) = 1 Then
'                                o.Tag = "req"
'                            End If
'                        End If
'                    End If
'                End If
'            Next o
'        End If
'    Next lngInd
'
'LabelSalir:
'    Exit Sub
'LabelError:
'    If Not Err.Number = 0 Then
'        MsgBox Err.Description, vbCritical, "Error"
'        Err.Clear
'        GoTo LabelSalir
'        Resume 0
'    End If
'End Sub
Sub AjustaTollBar(tlb As Toolbar, tAccion As tTipoAccion)
    Select Case tAccion
        Case 1 'Lista
            tlb.Buttons("Agregar").Visible = True
            tlb.Buttons("Agregar").ToolTipText = "Agregar"
            tlb.Buttons("Modificar").Visible = True
            tlb.Buttons("Eliminar").Visible = True
            tlb.Buttons("Consultar").Visible = True
            tlb.Buttons("Salir").Visible = True
            tlb.Buttons("Volver").Visible = False
        Case 2 'Agregar/Modificar
            tlb.Buttons("Agregar").Visible = True
            tlb.Buttons("Agregar").ToolTipText = "Grabar"
            tlb.Buttons("Modificar").Visible = False
            tlb.Buttons("Eliminar").Visible = False
            tlb.Buttons("Consultar").Visible = False
            tlb.Buttons("Salir").Visible = False
            tlb.Buttons("Volver").Visible = True
        Case 3 'SubLista
            tlb.Buttons("Agregar").Visible = True
            tlb.Buttons("Agregar").ToolTipText = "Agregar"
            tlb.Buttons("Modificar").Visible = True
            tlb.Buttons("Eliminar").Visible = True
            tlb.Buttons("Consultar").Visible = True
            tlb.Buttons("Salir").Visible = False
            tlb.Buttons("Volver").Visible = True
        Case 4 'Consultar
            tlb.Buttons("Agregar").Visible = False
            tlb.Buttons("Modificar").Visible = False
            tlb.Buttons("Eliminar").Visible = False
            tlb.Buttons("Salir").Visible = False
            tlb.Buttons("Volver").Visible = True
        Case 5 'SubListaConsultar
            tlb.Buttons("Agregar").Visible = False
            tlb.Buttons("Modificar").Visible = False
            tlb.Buttons("Eliminar").Visible = False
            tlb.Buttons("Consultar").Visible = False
            tlb.Buttons("Salir").Visible = False
            tlb.Buttons("Volver").Visible = True
    End Select
End Sub

