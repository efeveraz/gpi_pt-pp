VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmNvaDigitacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Nueva Digitaci�n"
   ClientHeight    =   4500
   ClientLeft      =   2760
   ClientTop       =   2430
   ClientWidth     =   7650
   Icon            =   "frmNvaDigitacion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   7650
   Begin VB.Frame fraSeleccion 
      Height          =   2865
      Index           =   4
      Left            =   60
      TabIndex        =   5
      Top             =   465
      Width           =   7515
      Begin VB.ComboBox cboSucursal 
         Height          =   315
         Left            =   3720
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Tag             =   "NUEVADIG"
         Top             =   990
         Width           =   3360
      End
      Begin VB.ComboBox cboRutEnt 
         Height          =   315
         Left            =   1095
         TabIndex        =   10
         Tag             =   "NUEVADIG"
         Top             =   555
         Width           =   1620
      End
      Begin VB.CommandButton cmdBusEnt 
         Height          =   315
         Left            =   7080
         MaskColor       =   &H00FFFFFF&
         Picture         =   "frmNvaDigitacion.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   540
         UseMaskColor    =   -1  'True
         Width           =   330
      End
      Begin VB.ComboBox cboNombreEnt 
         Height          =   315
         Left            =   3720
         TabIndex        =   8
         Tag             =   "NUEVADIG"
         Top             =   555
         Width           =   3360
      End
      Begin VB.TextBox txtDescripcion 
         Height          =   720
         Left            =   1095
         MaxLength       =   255
         TabIndex        =   7
         Tag             =   "NUEVADIG"
         Top             =   1800
         Width           =   5925
      End
      Begin VB.ComboBox cboProducto 
         Height          =   315
         Left            =   1095
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Tag             =   "NUEVADIG"
         Top             =   990
         Width           =   1620
      End
      Begin MSComDlg.CommonDialog dlg 
         Left            =   120
         Top             =   2160
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label lblCMarco 
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   3720
         TabIndex        =   21
         Tag             =   "NUEVADIG"
         Top             =   1395
         Width           =   1395
      End
      Begin VB.Label Label2 
         Caption         =   "Sucursal"
         Height          =   240
         Left            =   2760
         TabIndex        =   20
         Top             =   1050
         Width           =   840
      End
      Begin VB.Label Label1 
         Caption         =   "N� Contrato"
         Height          =   240
         Left            =   2760
         TabIndex        =   19
         Top             =   1455
         Width           =   840
      End
      Begin VB.Label lblRut 
         Caption         =   "Rut Cliente"
         Height          =   240
         Left            =   120
         TabIndex        =   18
         Top             =   570
         Width           =   840
      End
      Begin VB.Label lblNombre 
         Caption         =   "Nombre"
         Height          =   240
         Index           =   0
         Left            =   2760
         TabIndex        =   17
         Top             =   570
         Width           =   615
      End
      Begin VB.Label lblSeleccion 
         Alignment       =   2  'Center
         BackColor       =   &H00A26D00&
         Caption         =   "Datos del Encabezado de la Remesa"
         ForeColor       =   &H00E0E0E0&
         Height          =   240
         Index           =   4
         Left            =   0
         TabIndex        =   16
         Top             =   30
         Width           =   7545
      End
      Begin VB.Label Label6 
         Caption         =   "Observaci�n"
         Height          =   240
         Left            =   120
         TabIndex        =   15
         Top             =   1860
         Width           =   945
      End
      Begin VB.Label Label7 
         Caption         =   "N� Remesa"
         Height          =   240
         Left            =   120
         TabIndex        =   14
         Top             =   1455
         Width           =   900
      End
      Begin VB.Label lblRemesa 
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1095
         TabIndex        =   13
         Tag             =   "NUEVADIG"
         Top             =   1395
         Width           =   1605
      End
      Begin VB.Label Label10 
         Caption         =   "Producto"
         Height          =   240
         Left            =   120
         TabIndex        =   12
         Top             =   1050
         Width           =   720
      End
   End
   Begin VB.Frame fraBotones 
      Height          =   735
      Index           =   0
      Left            =   60
      TabIndex        =   4
      Top             =   3420
      Width           =   7515
      Begin VB.CommandButton cmdGrabar 
         Caption         =   "&Grabar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   120
         TabIndex        =   0
         Top             =   210
         Width           =   1425
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   5955
         TabIndex        =   1
         Top             =   210
         Width           =   1425
      End
   End
   Begin MSComctlLib.StatusBar staEstado 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   2
      Top             =   4230
      Width           =   7650
      _ExtentX        =   13494
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10390
            MinWidth        =   3881
            Text            =   "Listo"
            TextSave        =   "Listo"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Text            =   "V 0.00"
            TextSave        =   "V 0.00"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbAcciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   7650
      _ExtentX        =   13494
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Grabar"
            Object.ToolTipText     =   "Grabar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Volver"
            Object.ToolTipText     =   "Volver"
         EndProperty
      EndProperty
      Begin MSComctlLib.ImageList ilsIconos 
         Left            =   1800
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
   End
End
Attribute VB_Name = "frmNvaDigitacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cboNombreEnt_Click()
    cboRutEnt.ListIndex = cboNombreEnt.ListIndex
End Sub

Private Sub cboNombreEnt_GotFocus()
    cboNombreEnt.Text = ""
    cboRutEnt.ListIndex = -1
    cboProducto.ListIndex = 0
End Sub

Private Sub cboNombreEnt_KeyPress(KeyAscii As Integer)
    cboRutEnt.Clear
End Sub

Private Sub cboProducto_Click()
Dim vntCMarco As Variant
Dim lngProducto As Long
    
    If cboRutEnt.ListIndex = -1 Then Exit Sub
    
    If cboProducto.ListIndex = -1 Then
        lngProducto = 0
    Else
        lngProducto = cboProducto.ItemData(cboProducto.ListIndex)
    End If
    
    Call Trae_CMarco_Vigente(vntCMarco, cboRutEnt.ItemData(cboRutEnt.ListIndex), lngProducto)
    lblCMarco = vntCMarco(0)
End Sub

Private Sub cboProducto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cboRutEnt_Click()
    cboNombreEnt.ListIndex = cboRutEnt.ListIndex
End Sub

Private Sub cboRutEnt_GotFocus()
    cboRutEnt.Text = ""
    cboNombreEnt.ListIndex = -1
    cboProducto.ListIndex = -1
    cboSucursal.ListIndex = -1
    lblCMarco = ""
End Sub

Private Sub cboRutEnt_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cmdBusEnt_Click
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
        KeyAscii = 0
    Else
        cboNombreEnt.Clear
        If cboRutEnt.ListCount > 0 Then cboRutEnt.Clear
    End If
End Sub

Private Sub cboSucursal_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cmdBusEnt_Click()
Dim recRegistros As New ADODB.Recordset
Dim Cmd As New ADODB.Command
Dim mlngRut As Long
Dim mlngProducto As Long
Dim mstrNombre As String
Dim vntCMarco() As Variant
On Error GoTo LabelError

    If cboRutEnt.Text = Empty And cboNombreEnt.Text = Empty Then GoTo LabelSalir
    Screen.MousePointer = vbHourglass
    
    mlngRut = Val(cboRutEnt.Text)
    mstrNombre = (cboNombreEnt.Text)
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "DBO_FACTOR.pkg_fme_entidades.sp_fme_clientes_ver"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_rut", adNumeric, adParamInput, 10, mlngRut)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_nombre", adVarChar, adParamInput, 80, mstrNombre)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_filtro_esp", adVarChar, adParamInput, 15, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_tipo_filtro", adVarChar, adParamInput, 1, "")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_descerror", adVarChar, adParamOutput, 1000)
    
    Set recRegistros = Cmd.Execute
    
    If Mid(Cmd.Parameters("p_v_descerror").Value, 1, 4) <> "9999" Then
        MsgBox Cmd.Parameters("p_v_descerror").Value, vbInformation, "Clientes"
        GoTo LabelSalir
    End If
    
    cboRutEnt.Clear
    cboNombreEnt.Clear
    
    Do While Not recRegistros.EOF
        cboRutEnt.AddItem RTrim("" & recRegistros.Fields("rut_entidad").Value) & "-" & recRegistros.Fields("ide_dig_rut").Value
        cboRutEnt.ItemData(cboRutEnt.NewIndex) = recRegistros.Fields("ide_entidad").Value
        
        cboNombreEnt.AddItem "" & recRegistros.Fields("nomcliente").Value
        cboNombreEnt.ItemData(cboNombreEnt.NewIndex) = recRegistros.Fields("mrc_bloqueo").Value
        
        recRegistros.MoveNext
    Loop
    If cboRutEnt.ListCount > 0 Then cboRutEnt.ListIndex = 0
    If cboNombreEnt.ListCount > 0 Then cboNombreEnt.ListIndex = 0
    
    Rem Exito
    If cboRutEnt.ListIndex = -1 Then
        mlngRut = 0
    Else
        mlngRut = cboRutEnt.ItemData(cboRutEnt.ListIndex)
    End If
    
    If cboProducto.ListIndex = -1 Then
        mlngProducto = 0
    Else
        mlngProducto = cboProducto.ItemData(cboProducto.ListIndex)
    End If
    
    If cboRutEnt.ListIndex <> -1 Then
        Call Trae_CMarco_Vigente(vntCMarco, cboRutEnt.ItemData(cboRutEnt.ListIndex), mlngProducto)
        lblCMarco = vntCMarco(0)
        Call Carga_Combo_Sucursales(cboSucursal, cboRutEnt.ItemData(cboRutEnt.ListIndex))
    End If
    
LabelSalir:
    Screen.MousePointer = 0
    Set Cmd = Nothing
    Set recRegistros = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub


Private Sub cmdGrabar_Click()
Dim Cmd As New ADODB.Command
On Error GoTo LabelError
    
    staEstado.Panels(1) = "Grabando..."
    Screen.MousePointer = 11
    
    Rem Validaciones
    If Trim(cboRutEnt.Text) = "" Then
        MsgBox "Debe ingresar Cliente", vbExclamation, Me.Caption
        cboRutEnt.SetFocus
        GoTo LabelSalir
    End If
    If cboProducto.ListIndex = -1 Then
        MsgBox "Debe seleccionar Producto", vbExclamation, Me.Caption
        cboProducto.SetFocus
        GoTo LabelSalir
    End If
    If cboSucursal.ListIndex = -1 Then
        MsgBox "Debe seleccionar Sucursal", vbExclamation, Me.Caption
        cboSucursal.SetFocus
        GoTo LabelSalir
    End If
    If Val(lblCMarco) = 0 Then
        MsgBox "Cliente no cuenta con un Contrato marco," & Chr(13) & "para el producto" & cboProducto.Text, vbExclamation, Me.Caption
        cboProducto.SetFocus
        GoTo LabelSalir
    End If
    Rem ***********************************************************************
    
    Cmd.ActiveConnection = gAdoCon
    Cmd.CommandType = adCmdStoredProc
    Cmd.CommandText = "DBO_FACTOR.PKG_FME_CARGA_DIGITACION.SP_FME_ENCABEZADO_GRABAR"
        
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_empresa", adNumeric, adParamInput, 10, gLngEmpresa)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Contrato", adNumeric, adParamInput, 10, Val(lblCMarco))
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_Ide_Sucursal", adNumeric, adParamInput, 10, cboSucursal.ItemData(cboSucursal.ListIndex))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_TipoFormato", adChar, adParamInput, 8, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_Obs", adVarChar, adParamInput, 255, Trim(txtDescripcion.Text))
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_TipoOrigen", adChar, adParamInput, 8, "DIG")
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_RutaArchivo", adVarChar, adParamInput, 255, Null)
    Cmd.Parameters.Append Cmd.CreateParameter("p_c_Digitacion", adChar, adParamInput, 1, "1")
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_NroDoctos", adNumeric, adParamInput, 6, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_MtoDoctos", adNumeric, adParamInput, 20, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_NroDoctosAcep", adNumeric, adParamInput, 20, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_n_IdeRemesa", adNumeric, adParamInputOutput, 10, 0)
    Cmd.Parameters.Append Cmd.CreateParameter("p_v_usuario", adChar, adParamInput, 10, gstrUsuario)
    Cmd.Parameters.Append Cmd.CreateParameter("p_V_DescError", adVarChar, adParamOutput, 1000)
    Cmd.Execute
    

    
    If Mid(Cmd.Parameters("p_v_DescError").Value, 1, 4) <> "9999" Then
        MsgBox "La informaci�n no pudo ser Grabada. " & Chr(13) & Cmd.Parameters("p_v_DescError").Value, vbExclamation, Me.Caption
        GoTo LabelSalir
    Else
        lblRemesa = Cmd.Parameters("p_n_IdeRemesa").Value
    End If
    'glngCodigo = Val(lblCodigoIng2)
    MsgBox "informaci�n Grabada Exitosamente.", vbInformation, Me.Caption
    
    
    'gblnswichRefresca = True
    
    'If Not gblnModifica = True Then
    '    Call LimpiarControles(Me, "INGCOM")
    'End If
    
    'Call AlmacenarValoresCtls(Me, "INGCOM")
    
LabelSalir:
    Screen.MousePointer = 0
    staEstado.Panels(1) = "Listo"
    Set Cmd = Nothing
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, vbCritical, "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
    Unload Me
End Sub

Private Sub cmdVolver_Click()
    If HayCambiosEnControles(Me, "NUEVADIG") Then
        If MsgBox("Existe Informaci�n sin Grabar." & Chr(13) & "�Desea volver sin grabar los cambios?", vbYesNo + vbInformation, Me.Caption) <> vbYes Then
            Exit Sub
        Else
            gblnswichRefresca = True
        End If
    End If
    Unload Me
End Sub

Private Sub Form_Load()
    Call cargarImagenes(tlbAcciones, ilsIconos, "grabar", "volver")
    Call Carga_Combo_Productos(cboProducto)
End Sub

Private Sub tlbAcciones_ButtonClick(ByVal Button As MSComctlLib.Button)
On Error GoTo LabelError

    Select Case UCase(Trim(Button.Key))
        Case "GRABAR"
            Call cmdGrabar_Click
        Case "VOLVER"
            Unload Me
    End Select

LabelSalir:
    Exit Sub
LabelError:
    If Not Err.Number = 0 Then
        MsgBox Err.Description, , "Error"
        Err.Clear
        GoTo LabelSalir
        Resume 0
    End If
End Sub

Private Sub txtDescripcion_GotFocus()
    txtDescripcion.SelStart = 0
    txtDescripcion.SelLength = Len(txtDescripcion)
End Sub

Private Sub txtDescripcion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub


