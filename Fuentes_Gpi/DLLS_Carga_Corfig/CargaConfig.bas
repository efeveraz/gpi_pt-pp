Attribute VB_Name = "CargaConfig"
Option Explicit
'Public Const LOCALE_USER_DEFAULT = &H400
'Public Const LOCALE_SDECIMAL = &HE   '  Simbolo Separador Decimal (para n�meros)
Public Declare Function GetLocaleInfo Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long) As Long

Public gAdoCon      As New ADODB.Connection
Public gstrRutaRec  As String
Public strRuta As String
Public gstrUsuario As String
Public gLngEmpresa As Long
Public gstrSepDec As String
Public gstrSepList As String



Sub EstableceVariables()
    Rem Establece Ruta del Proyecto
    strRuta = App.Path
    gstrUsuario = "JOPAZO"
    gstrRutaRec = strRuta
    gLngEmpresa = 1
End Sub

Public Function pathRegSvr32() As String
Rem Factor40
    Dim strPath As String
    strPath = Environ("windir")
    If LCase(Dir(strPath & "\system\regsvr32.exe")) = "regsvr32.exe" Then
        strPath = strPath & "\system"
    ElseIf LCase(Dir(strPath & "\system32\regsvr32.exe")) = "regsvr32.exe" Then
        strPath = strPath & "\system32"
    Else
        strPath = ""
    End If
    pathRegSvr32 = strPath
End Function

Function LLamarDll(strNombre As String, strDirectorio As String, gstrRuta As String) As Object
Rem Factor40
Rem RM: Generando la union de las Tareas, LLamando a la aplicaci�n ddl
Rem Crea la Referencia
Dim clsRet As Object
Dim strPathRegsvr32 As String
On Error Resume Next
        strNombre = Trim(UCase(strNombre))
        strDirectorio = Trim(UCase(strDirectorio))
        strPathRegsvr32 = pathRegSvr32()
        Set clsRet = CreateObject(strNombre & ".INICIO")
        Rem Si hubo un error al Crear el Objeto, se registra.
        If Err.Number > 0 Then
            Rem Registra la Dll
            If Not Dir(strPathRegsvr32 & "\regsvr32.exe") = "" Then
                Shell strPathRegsvr32 & "\regsvr32 " & gstrRuta & "\" & strDirectorio & "\" & strNombre & ".dll /s", vbHide
                'Shell strPathRegsvr32 & "\regsvr32 " & gstrRuta & "\..\" & strDirectorio & "\" & strNombre & ".dll /s", vbHide
            Else
                MsgBox "No se encontr� Regsvr32.exe", vbCritical, "Error al Ubicar Regsvr32"
            End If
            DoEvents: DoEvents: DoEvents: DoEvents
            Set clsRet = CreateObject(strNombre & ".INICIO")
        End If
        
        Rem No se encontr� (o no est� registrada) la dll
        If clsRet Is Nothing Then
            MsgBox "La clase no ha sido definida", vbCritical, "Atenci�n"
            Exit Function
        End If
        Set clsRet.ADOConnection = gAdoCon
        
        Rem Paso de Parametros
        Set LLamarDll = clsRet
        
'        clsRet.show
'        Set clsRet = Nothing
End Function

Public Function ConexionAdo(ByVal strLogin As String, ByVal strPwd As String, strServidor As String) As Boolean
Dim strConnect As String
Dim strMsg As String
On Error GoTo error
    
    If Not gAdoCon = "" Then
        ConexionAdo = True
        GoTo Salir
    End If
    
    Rem Primera Conexi�n:
    Rem a trav�s del usuario gen�rico se obtiene el permiso suficiente,
    Rem para accesar la tabla dfe claves y extraer la Clave Real del usuario logeado.
    strConnect = ""
    strConnect = strConnect & " Provider    = msdaora.1;"
    strConnect = strConnect & " User ID     = " & strLogin & ";"
    strConnect = strConnect & " Password    = " & strPwd & ";"
    strConnect = strConnect & " Data Source = " & strServidor & ";"
    strConnect = strConnect & " Persis Security info = true;"
    strConnect = strConnect & " Extend Properties = ''"
    gAdoCon.CursorLocation = adUseClient
    gAdoCon.Open strConnect
    If Not Err.Number = 0 Then
        strMsg = ""
        strMsg = strMsg & "No se pudo establecer la conexi�n a la Base de datos" & Chr(13)
        strMsg = strMsg & "Proveedor para acceso no es el correcto o no est� instalado" & Chr(13)
        strMsg = strMsg & "Comun�quese con Soporte."
        MsgBox strMsg, vbCritical, "Conexi�n"
        GoTo Salir
    End If
    On Error GoTo error
    
    ConexionAdo = True
Salir:

    Screen.MousePointer = vbDefault
    Exit Function
    
error:
    MsgBox Err.Description, vbExclamation, "Conexi�n"
    Set gAdoCon = Nothing
    Resume Salir
    Resume 0
    
End Function

Public Sub obtenerSepDecimal()
    Dim sBuffer As String
    Dim lngRet As Long
    Dim strMsg As String
    sBuffer = String$(100, vbNullChar)
    lngRet = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, sBuffer, 100)
    If Not lngRet = 0 Then
        gstrSepDec = Left(sBuffer, lngRet - 1)
    End If
    
    gstrSepDec = Trim(gstrSepDec)
    
    If gstrSepDec = "" Then
        strMsg = "Falta separador de Decimales en su Configuraci�n Regional."
        strMsg = strMsg & "Favor corrija su configuraci�n regional."
    ElseIf InStr(1, ",.", gstrSepDec) = 0 Then
        strMsg = "Usted est� usando un separador de decimal no permitido (" & gstrSepDec & ")" & Chr(13)
        strMsg = strMsg & "Favor cambiar el separador de decimal en su configuraci�n regional."
    End If
    
    If Not strMsg = "" Then
        MsgBox strMsg, vbExclamation, "Error"
        End
    End If
    
    
End Sub

Public Sub ObtenerSepListas()
    Dim sBuffer As String
    Dim lngRet As Long
    Dim strMsg As String
    sBuffer = String$(100, vbNullChar)
    lngRet = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SLIST, sBuffer, 100)
    
    If Not lngRet = 0 Then
        gstrSepList = Left(sBuffer, lngRet - 1)
    End If
    
    gstrSepList = Trim(gstrSepList)
    
    If gstrSepList = "" Then
        strMsg = "Falta el separador de listas en su Configuraci�n Regional."
        strMsg = strMsg & "Favor corrija su configuraci�n regional."
'    ElseIf InStr(1, ",.", gstrSepDec) = 0 Then
'        strMsg = "Usted est� usando un separador de decimal no permitido (" & gstrSepDec & ")" & Chr(13)
'        strMsg = strMsg & "Favor cambiar el separador de decimal en su configuraci�n regional."
    End If
    
    If Not strMsg = "" Then
        MsgBox strMsg, vbExclamation, "Error"
        End
    End If
    
    
End Sub

Public Sub valuar(ByVal strVariable As String, ByVal varValor As Variant)
Rem Factor40
    Select Case Trim(LCase(strVariable))
        
        Rem C�digo del usuario conectado (nombre de usuario)
        Case "gstrusuario"
            gstrUsuario = varValor
        
        Rem Identificador �nico de la empresa
        Case "glngempresa"
            gLngEmpresa = varValor
        
        Rem Separador Decimal.
        Case "gstrsepdec"
            gstrSepDec = varValor   'gstrSetDec = varValor
        
        Rem Objeto conexi�n ADO
        Case "gadocon"
            Set gAdoCon = varValor
        
        Rem Ruta del directorio de Recursos
        Rem ejemplo: 'K:\FACTOR40\FMRECURS'
        Case "gstrrutarec"
            gstrRutaRec = varValor
            
        Case Else
            MsgBox "Error Fatal. Llamada incorrecta: VALUAR(" & strVariable & "," & varValor & ") ", vbCritical, "Error de Sistema"
    End Select

End Sub

