Attribute VB_Name = "general_variables"
Option Explicit

Public Const gColorFondo = &H80000004

Rem Arreglos de combos corporativos
Public garrBancos() As Variant
Public garrComunas() As Variant
Public garrEjecutivosBco() As Variant
Public garrActividadesEco() As Variant
Public garrGruposEco() As Variant
Public garrMonedas() As Variant
Public garrNacionalidades() As Variant
Public garrOficinasSII() As Variant
Public garrPaises() As Variant
Public garrPlazas() As Variant
Public garrProfesiones() As Variant
Public garrRegiones() As Variant
Public garrSegmentos() As Variant
Public garrSucursales() As Variant

Rem Arreglo que Contendra los Valores iniciales de los controles Para Efecto de Validacion de Cambio
Public garrControles() As Variant

