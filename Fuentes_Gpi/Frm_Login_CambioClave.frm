VERSION 5.00
Begin VB.Form Frm_Login_CambioClave 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizacion de Contraseña"
   ClientHeight    =   1815
   ClientLeft      =   45
   ClientTop       =   495
   ClientWidth     =   4845
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1815
   ScaleWidth      =   4845
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmd_salir 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   1320
      Width           =   1935
   End
   Begin VB.CommandButton cmd_Actualizar 
      Caption         =   "Actualizar"
      Height          =   375
      Left            =   2400
      TabIndex        =   4
      Top             =   1320
      Width           =   2175
   End
   Begin VB.TextBox clave2 
      Height          =   375
      Left            =   2040
      TabIndex        =   3
      Top             =   720
      Width           =   2775
   End
   Begin VB.TextBox clave1 
      Height          =   375
      Left            =   2040
      TabIndex        =   2
      Top             =   240
      Width           =   2775
   End
   Begin VB.Label Label 
      Caption         =   "Repetir Contraseña"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   1695
   End
   Begin VB.Label Label 
      Caption         =   "Contraseña Nueva"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1575
   End
End
Attribute VB_Name = "Frm_login_cambioClave"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmd_Actualizar_Click()
If clave1.Text = clave2.Text Then
    Call Ftn_actualizar(clave1.Text)
Else
    Call MsgBox("Las contraseñas ingresadas son diferentes" _
        & vbCrLf & "favor de ingresar nuevamente las contraseñas" _
        , vbCritical, "Actualización de Contraseña")
End If
End Sub
Private Function Ftn_actualizar(ByRef Clave As String) As Boolean

Dim mensaje As String
Dim ClaveEncriptada As String
Dim ClaveFiltrada As String

    Dim lcEncriptacion As Class_Encriptacion
    Set lcEncriptacion = New Class_Encriptacion
    With lcEncriptacion
      mensaje = .ValidarPassword("", Clave)
      If mensaje <> "" Then
          MsgBox "Error:" & vbNewLine & mensaje
          Exit Function
      End If
    Set lcEncriptacion = Nothing
    End With
    
    'seccion encriptacion
    Set lcUsuario1 = New Class_Usuarios
    With lcUsuario1
    ' llamada a encriptador
        ClaveEncriptada = .ExecEncriptacion("", Clave)
        ClaveFiltrada = Right(ClaveEncriptada, (Len(ClaveEncriptada) - 27))
        ClaveFiltrada = Left(ClaveFiltrada, (Len(ClaveFiltrada) - 2))
        Set lcUsuario1 = Nothing
    End With

    Dim lcUsuario As Class_Usuarios
    Set lcUsuario = New Class_Usuarios

    If lcUsuario.Fnt_ActualizarContraseña(ClaveFiltrada) Then
        Call MsgBox("Se actualizo Correctamente la contraseña. " _
                    & vbCrLf & "Favor de ingresar nuevamente al sistema." _
                    , vbInformation, "Actualización de Contraseña")
        Unload Me
    Else
        Call MsgBox("ocurrio un problema con la actualizacion de la contraseña.", vbExclamation, "Actualización de Contraseña")
        
    End If
    
    Set lcUsuario = Nothing
    
End Function

Private Sub cmd_salir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
Set Me.Icon = Nothing
End Sub
