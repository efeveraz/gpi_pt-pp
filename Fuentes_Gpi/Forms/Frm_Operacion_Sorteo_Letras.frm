VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Operacion_Sorteo_Letras 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sorteo de Letras"
   ClientHeight    =   5490
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   10800
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5490
   ScaleWidth      =   10800
   Begin VB.Frame Frame_Datos 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1245
      Left            =   60
      TabIndex        =   7
      Top             =   420
      Width           =   10665
      Begin VB.CommandButton Cmb_Buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3690
         Picture         =   "Frm_Operacion_Sorteo_Letras.frx":0000
         TabIndex        =   8
         ToolTipText     =   "Presione para buscar Nemotecnico."
         Top             =   300
         Width           =   345
      End
      Begin hControl2.hTextLabel Txt_Nemotecnico 
         Height          =   315
         Left            =   120
         TabIndex        =   0
         Tag             =   "OBLI=S"
         Top             =   300
         Width           =   3525
         _ExtentX        =   6218
         _ExtentY        =   556
         LabelWidth      =   1350
         Caption         =   " Nemot�cnico"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin MSComCtl2.DTPicker Dtp_Fecha_Liquidacion 
         Height          =   345
         Left            =   5730
         TabIndex        =   2
         Tag             =   "OBLI"
         ToolTipText     =   "Permite solo dias h�biles"
         Top             =   720
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   58982401
         CurrentDate     =   38768
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Movimiento 
         Height          =   345
         Left            =   5730
         TabIndex        =   1
         Tag             =   "OBLI"
         ToolTipText     =   "Permite solo dias h�biles"
         Top             =   300
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   58982401
         CurrentDate     =   38768
      End
      Begin VB.Label Lbl_FechaMovimiento 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Movimiento"
         Height          =   345
         Left            =   4260
         TabIndex        =   10
         Top             =   300
         Width           =   1425
      End
      Begin VB.Label Lbl_FechaLiquidacion 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Liquidaci�n"
         Height          =   345
         Left            =   4260
         TabIndex        =   9
         Top             =   720
         Width           =   1425
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Sorteo de Letras"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   60
      TabIndex        =   3
      Top             =   1740
      Width           =   10665
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   120
         TabIndex        =   4
         Top             =   300
         Width           =   9825
         _cx             =   17330
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   9
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Operacion_Sorteo_Letras.frx":030A
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Grilla 
         Height          =   990
         Left            =   10050
         TabIndex        =   11
         Top             =   570
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1746
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "ADD"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Agregar una Cuenta al Sorteo de Letra"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "UPD"
               Object.ToolTipText     =   "Modifica una Cuenta al Sorteo de Letra"
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "DEL"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Elimina una Cuenta al Sorteo de Letra"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   255
            Left            =   9420
            TabIndex        =   12
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   10800
      _ExtentX        =   19050
      _ExtentY        =   635
      ButtonWidth     =   2011
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba Sorteos de Letras"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Importar"
            Key             =   "IMPORT"
            Object.ToolTipText     =   "Importa Sorteo de Letras"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Limpiar"
            Key             =   "CLEAN"
            Object.ToolTipText     =   "Limpia la pantalla"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   6
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Operacion_Sorteo_Letras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fId_Nemotecnico
Dim fTasa_Emision
Dim fId_Moneda_Transaccion As String
Dim fhMatriz As hRecord

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Cmb_Buscar_Click()

  fId_Nemotecnico = frm_Busca_Nemotecnicos.Buscar(pCod_Instrumento:=gcINST_BONOS_NAC, _
                                                  pNemotecnico:=Txt_Nemotecnico.Text, _
                                                  pCodOperacion:=gcTipoOperacion_Ingreso, _
                                                  pCod_Subfamilia:="LH")
                                                        
  If Not IsNull(fId_Nemotecnico) Then
    Call Sub_Busca_Datos_Nemotecnico
  End If
  
End Sub

Private Sub Sub_Busca_Datos_Nemotecnico()
Dim lcNemotecnico As Class_Nemotecnicos
Dim lId_SubFamilia, lCod_SubFamilia
Dim lcSubfamilia As Class_SubFamilias
  
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = fId_Nemotecnico
    If .Buscar Then
        Txt_Nemotecnico.Text = .Cursor(1)("nemotecnico").Value
        fTasa_Emision = .Cursor(1)("tasa_emision").Value
        fId_Moneda_Transaccion = .Cursor(1)("id_moneda_transaccion").Value
        lId_SubFamilia = .Cursor(1)("Id_Subfamilia").Value
        lCod_SubFamilia = ""
        Set lcSubfamilia = New Class_SubFamilias
        With lcSubfamilia
          .Campo("ID_subfamilia").Valor = NVL(lId_SubFamilia, 0)
          If .Buscar Then
            If Not .Cursor.Count = 0 Then
                lCod_SubFamilia = .Cursor(1)("cod_subfamilia").Value
            End If
          End If
        End With
        Set lcSubfamilia = Nothing
        If lCod_SubFamilia <> "LH" Then
            fId_Nemotecnico = 0
            fTasa_Emision = ""
            fId_Moneda_Transaccion = ""
        End If
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcNemotecnico = Nothing
  
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("IMPORT").Image = "document"
      .Buttons("CLEAN").Image = cBoton_Refrescar
      .Buttons("EXIT").Image = cBoton_Salir
  End With

  With Toolbar_Grilla
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("UPD").Image = cBoton_Modificar
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Appearance = ccFlat
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
  If Grilla.Row > 0 Then
    If Not GetCell(Grilla, Grilla.Row, "error") = "" Then
      MsgBox GetCell(Grilla, Grilla.Row, "error"), vbInformation, Me.Caption
    End If
  End If
End Sub

Private Sub Sub_Carga_Matriz(pModificar As Boolean)
Dim lReg As hFields
Dim lKey As String

  Set fhMatriz = New hRecord
  With fhMatriz
    .ClearFields
    .AddField "ID_CUENTA"
    .AddField "CUENTA"
    .AddField "CANTIDAD", 0
    .AddField "SALDO", 0
    .AddField "TASA", 0
    .AddField "MONTO", 0
    .AddField "RUT_CLIENTE"
    .AddField "CLIENTE"
    .AddField "ID_NEMOTECNICO"
    .AddField "NEMOTECNICO"
    .AddField "FECHA_MOVIMIENTO"
    
    Set lReg = .Add
    
    If pModificar Then
      lReg("ID_CUENTA").Value = GetCell(Grilla, Grilla.Row, "id_cuenta")
      lReg("CUENTA").Value = GetCell(Grilla, Grilla.Row, "num_cuenta")
      lReg("CANTIDAD").Value = GetCell(Grilla, Grilla.Row, "cantidad")
      lReg("SALDO").Value = GetCell(Grilla, Grilla.Row, "saldo")
      lReg("MONTO").Value = GetCell(Grilla, Grilla.Row, "monto")
      lReg("RUT_CLIENTE").Value = GetCell(Grilla, Grilla.Row, "rut_cliente")
      lReg("CLIENTE").Value = GetCell(Grilla, Grilla.Row, "cliente")
    Else
      lReg("ID_CUENTA").Value = cNewEntidad
      lReg("CUENTA").Value = ""
      lReg("CANTIDAD").Value = 0
      lReg("SALDO").Value = 0
      lReg("MONTO").Value = 0
      lReg("RUT_CLIENTE").Value = ""
      lReg("CLIENTE").Value = ""
    End If
    
    lReg("TASA").Value = fTasa_Emision
    lReg("ID_NEMOTECNICO").Value = fId_Nemotecnico
    lReg("NEMOTECNICO").Value = Txt_Nemotecnico.Text
    lReg("FECHA_MOVIMIENTO").Value = DTP_Fecha_Movimiento.Value
    
    lKey = lReg("ID_CUENTA").Value
    
    Call Sub_EsperaVentana(lKey, pModificar)
  End With
  
End Sub

Private Sub Sub_EsperaVentana(ByVal pkey, pModificar As Boolean)
Dim lForm As Frm_Sorteo_Letras
Dim lNombre As String
Dim lNom_Form As String
  
  lNom_Form = "Frm_Sorteo_Letras"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lNombre = Me.Name
    
    Set lForm = New Frm_Sorteo_Letras
    Call lForm.Fnt_Modificar(pkey, fhMatriz)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      Call Sub_CargarDatos(pModificar)
    End If
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "SAVE"
      Call Sub_Grabar
    Case "IMPORT"
      Call Sub_Importar
    Case "CLEAN"
      Call Sub_Limpiar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Limpiar()
  
  Txt_Nemotecnico.Text = ""
  Txt_Nemotecnico.Locked = False
  Txt_Nemotecnico.BackColorTxt = fColorOBligatorio
  cmb_buscar.Enabled = True
  
  Grilla.Rows = 1
  
End Sub

Private Sub Sub_CargaForm()
  Grilla.Rows = 1
  Txt_Nemotecnico.Text = ""
  Dtp_Fecha_Liquidacion = Fnt_FechaServidor
  DTP_Fecha_Movimiento = Fnt_FechaServidor
End Sub

Private Sub Sub_CargarDatos(pModificar As Boolean)
Dim lReg As hFields
Dim lLinea As Long
  
  If fhMatriz.Count > 0 Then
    For Each lReg In fhMatriz
      
      If Not pModificar Then
        lLinea = Grilla.Rows
        Call Grilla.AddItem("")
        Call SetCell(Grilla, lLinea, "id_cuenta", lReg("ID_CUENTA").Value)
      Else
        lLinea = Grilla.Row
      End If
      
      Call SetCell(Grilla, lLinea, "num_cuenta", lReg("CUENTA").Value)
      Call SetCell(Grilla, lLinea, "CANTIDAD", lReg("CANTIDAD").Value)
      Call SetCell(Grilla, lLinea, "SALDO", lReg("SALDO").Value)
      Call SetCell(Grilla, lLinea, "TASA", lReg("TASA").Value)
      Call SetCell(Grilla, lLinea, "MONTO", lReg("MONTO").Value)
      Call SetCell(Grilla, lLinea, "RUT_CLIENTE", lReg("RUT_CLIENTE").Value)
      Call SetCell(Grilla, lLinea, "CLIENTE", lReg("CLIENTE").Value)
    Next
    
    Txt_Nemotecnico.Locked = True
    Txt_Nemotecnico.BackColorTxt = fColorNoEdit
    cmb_buscar.Enabled = False
  End If

End Sub

Private Sub Sub_Grabar()
Dim lFila As Long
Dim lId_Cuenta As String
Dim lCantidad As Double
Dim lTasa As Double
Dim lMonto As Double
Dim lMsg_Error As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Not Grilla.Rows > 1 Then
    MsgBox "No hay Sorteos de Letras en la Grilla.", vbCritical, Me.Caption
    GoTo ExitProcedure
  End If
  
  For lFila = 1 To Grilla.Rows - 1
    lId_Cuenta = GetCell(Grilla, lFila, "id_cuenta")
    lCantidad = GetCell(Grilla, lFila, "cantidad")
    lTasa = GetCell(Grilla, lFila, "tasa")
    lMonto = GetCell(Grilla, lFila, "monto")
    If Trim(GetCell(Grilla, lFila, "error")) <> "Sorteo de Letra guardado correctamente." Then
        If Not Fnt_Graba_Sorteo_Letras(pId_Cuenta:=lId_Cuenta, _
                                       pCantidad:=lCantidad, _
                                       pTasa:=lTasa, _
                                       pMonto:=lMonto, _
                                       pMsg_Error:=lMsg_Error) Then
          Call SetCell(Grilla, lFila, "error", lMsg_Error)
          Grilla.Cell(flexcpForeColor, lFila, 1, lFila, Grilla.Cols - 1) = vbRed
        Else
          Call SetCell(Grilla, lFila, "error", "Sorteo de Letra guardado correctamente.")
          Grilla.Cell(flexcpForeColor, lFila, 1, lFila, Grilla.Cols - 1) = vbBlue
        End If
    End If
  Next
  
  Call Grilla.Select(0, 0)
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Fnt_Graba_Sorteo_Letras(pId_Cuenta As String, _
                                         pCantidad As Double, _
                                         pTasa As Double, _
                                         pMonto As Double, _
                                         ByRef pMsg_Error As String) As Boolean
Dim lcBonos As Class_Bonos
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lFecha_Movimiento As Date
Dim lFecha_Liquidacion As Date
Dim lId_Caja_Cuenta As String
Dim lId_Operacion As String
Dim lValorizacion As Double
'-------------------------------
Dim lhActivos As hRecord
Dim lReg_Activo As hFields
  
  Fnt_Graba_Sorteo_Letras = True
  
  lFecha_Movimiento = DTP_Fecha_Movimiento.Value
  lFecha_Liquidacion = Dtp_Fecha_Liquidacion.Value
  
  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("id_moneda").Valor = fId_Moneda_Transaccion
    .Campo("cod_mercado").Valor = cMercado_Nacional
    If .Buscar_Caja_Para_Invertir() Then
      lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
    Else
      pMsg_Error = "Problemas en cargar Cajas de la Cuenta." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  Call gDB.IniciarTransaccion 'Incluye transacci�n
  Set lcBonos = New Class_Bonos
  With lcBonos
    If Not Fnt_Enlaza_VentasCompras_RF(pCod_Instrumento:=gcINST_BONOS_NAC, _
                                       pId_Cuenta:=pId_Cuenta, _
                                       pId_Nemotecnico:=fId_Nemotecnico, _
                                       pCantidad:=pCantidad, _
                                       pFecha_Movimiento:=lFecha_Movimiento, _
                                       phActivos:=lhActivos) Then
      pMsg_Error = "Problemas en buscar el enlaze para el Sorteo de Letras." & .ErrMsg
      GoTo ErrProcedure
    End If
    '-----------------------------------------------------------------------------
    For Each lReg_Activo In lhActivos
     If NVL(lReg_Activo("asignado").Value, 0) <> 0 Then
      lValorizacion = lcBonos.ValorizaPapel(fId_Nemotecnico, lFecha_Movimiento, pTasa, NVL(lReg_Activo("asignado").Value, 0))
      Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=fId_Nemotecnico, _
                                        pCantidad:=lReg_Activo("asignado").Value, _
                                        pTasa:=pTasa, _
                                        pId_Moneda:=fId_Moneda_Transaccion, _
                                        pMonto:=lValorizacion, _
                                        pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                        pUtilidad:=0, _
                                        PTasa_Gestion:=pTasa, _
                                        pTasa_Historico:=pTasa, _
                                        pId_Mov_Activo_Compra:=lReg_Activo("Id_Mov_Activo").Value)
     End If
    Next
    If Not .Realiza_Operacion_Sorteo_Letras(pId_Operacion:=lId_Operacion, _
                                            pId_Cuenta:=pId_Cuenta, _
                                            pId_Moneda_Operacion:=fId_Moneda_Transaccion, _
                                            pFecha_Movimiento:=lFecha_Movimiento, _
                                            pFecha_Liquidacion:=lFecha_Liquidacion, _
                                            pDsc_Operacion:="", _
                                            pMonto_Operacion:=pMonto, _
                                            pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
      pMsg_Error = "Problemas al grabar Sorteo de Letras." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Call gDB.CommitTransaccion 'Fin transacci�n
  Exit Function
  
ErrProcedure:
  Call gDB.RollbackTransaccion
  Fnt_Graba_Sorteo_Letras = False
End Function

Private Sub Sub_Importar()
Dim lForm As Frm_Sorteo_Letras_Importador
Dim lNombre As String
Dim lNom_Form As String
  
  Me.Enabled = False
  
  lNom_Form = "Frm_Sorteo_Letras_Importador"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, "") Then
    lNombre = Me.Name
    
    Set lForm = New Frm_Sorteo_Letras_Importador
    Call lForm.Fnt_Modificar
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, "")
      DoEvents
    Loop
  End If
  
  Me.Enabled = True
  
End Sub

Private Sub Toolbar_Grilla_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "ADD"
    
      Call Sub_Valida_Nemotecnico
      If fId_Nemotecnico = 0 Then
        MsgBox "El Nemotecnico no existe en el sistema.", vbInformation, Me.Caption
        Txt_Nemotecnico.SetFocus
      Else
        Call Sub_Busca_Datos_Nemotecnico
        If fId_Nemotecnico = 0 Then
          MsgBox "El Nemotecnico no es v�lido para Sorteo de Letras.", vbInformation, Me.Caption
          Txt_Nemotecnico.SetFocus
          Exit Sub
        End If
        Call Sub_Carga_Matriz(pModificar:=False)
      End If
      
    Case "UPD"
      
      If Grilla.Row > 0 Then
        Call Sub_Valida_Nemotecnico
        If fId_Nemotecnico = 0 Then
          MsgBox "El Nemotecnico no existe en el sistema.", vbInformation, Me.Caption
          Txt_Nemotecnico.SetFocus
        Else
          Call Sub_Busca_Datos_Nemotecnico
          Call Sub_Carga_Matriz(pModificar:=True)
        End If
      End If
      
    Case "DEL"
      If Grilla.Row > 0 Then
        Grilla.RemoveItem (Grilla.Row)
      End If
  End Select
End Sub

Private Sub Sub_Valida_Nemotecnico()
Dim lcNemotecnico As Class_Nemotecnicos

  If Not Txt_Nemotecnico.Text = "" Then
    Set lcNemotecnico = New Class_Nemotecnicos
    With lcNemotecnico
      .Campo("cod_instrumento").Valor = gcINST_BONOS_NAC
      .Campo("nemotecnico").Valor = Txt_Nemotecnico.Text
      If .Buscar_Nemotecnico(gcPROD_RF_NAC) Then
        fId_Nemotecnico = .Campo("id_nemotecnico").Valor
      Else
        MsgBox .ErrMsg, vbCritical, Me.Caption
      End If
    End With
    Set lcNemotecnico = Nothing
  End If
  
End Sub

