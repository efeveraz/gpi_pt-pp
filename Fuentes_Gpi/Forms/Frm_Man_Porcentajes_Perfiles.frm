VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Man_Porcentajes_Perfiles 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Restricciones Porcentajes Perfiles de Riesgo"
   ClientHeight    =   5025
   ClientLeft      =   1320
   ClientTop       =   2160
   ClientWidth     =   6225
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5025
   ScaleWidth      =   6225
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   90
      TabIndex        =   1
      Top             =   450
      Width           =   6045
      Begin TrueDBList80.TDBCombo Cmb_Perfiles_Riesgo 
         Height          =   345
         Left            =   1560
         TabIndex        =   2
         Tag             =   "OBLI=S;CAPTION=Perfiles de Riesgo;SOLOLECTURA=N"
         Top             =   270
         Width           =   3435
         _ExtentX        =   6059
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Man_Porcentajes_Perfiles.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Perfiles de Riesgo"
         Height          =   345
         Left            =   150
         TabIndex        =   3
         Top             =   270
         Width           =   1395
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Porcentajes por Productos e Instrumentos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   75
      TabIndex        =   0
      Top             =   1230
      Width           =   6075
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3285
         Left            =   135
         TabIndex        =   6
         Top             =   270
         Width           =   5760
         _cx             =   10160
         _cy             =   5794
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   0
         GridLines       =   2
         GridLinesFixed  =   4
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   5
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Porcentajes_Perfiles.frx":00AA
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   1
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   4
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   1
         OwnerDraw       =   6
         Editable        =   2
         ShowComboButton =   2
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   6
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   6225
      _ExtentX        =   10980
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n."
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Porcentajes_Perfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Cmb_Perfiles_Riesgo_ItemChange()
  Call Sub_Limpiar_IDs_Grilla
  Call Sub_CargarDatos
End Sub

Private Sub Sub_Limpiar_IDs_Grilla()
Dim lFila As Long

  For lFila = 1 To Grilla.Rows - 1
    
    If Not GetCell(Grilla, lFila, "id_arbol_clase_inst") = "" Then
      Call SetCell(Grilla, lFila, "id_restriccion", cNewEntidad)
    Else
      Call SetCell(Grilla, lFila, "id_restriccion", cNewEntidad)
    End If
  
'''''    If Not GetCell(Grilla, lFila, "cod_producto") = "" Then
'''''      Call SetCell(Grilla, lFila, "id_restricc_porc_prod", cNewEntidad)
'''''      Call SetCell(Grilla, lFila, "id_restricc_porc_instr", "")
'''''    Else
'''''      Call SetCell(Grilla, lFila, "id_restricc_porc_prod", "")
'''''      Call SetCell(Grilla, lFila, "id_restricc_porc_instr", cNewEntidad)
'''''    End If
    
    Call SetCell(Grilla, lFila, "porcentaje", "")
  
  Next
  
End Sub

Private Sub Form_Load()
With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargarDatos()
    Dim lId_Perfil_Riesgo As String
    Dim lcRestricc_Porc_Producto As Class_Restricc_Porc_Producto
    Dim lcRestricc_Porc_Instrumento As Class_Restricc_Porc_Instrumento
    
    'Dim lCod_Producto As String
    Dim lId_Arbol_Clase_Inst As String
    Dim lCod_Instrumento As String
    Dim lFila As Long

    lId_Perfil_Riesgo = Fnt_ComboSelected_KEY(Cmb_Perfiles_Riesgo)
    
    If Not lId_Perfil_Riesgo = "" Then
        For lFila = 1 To Grilla.Rows - 1
            Rem  Carga los Porcentajes de Restricciones de Perfiles de Riesgo por Productos
            'lCod_Producto = GetCell(Grilla, lFila, "cod_producto")
            
            lId_Arbol_Clase_Inst = GetCell(Grilla, lFila, "id_arbol_clase_inst")
      
            'If Not lCod_Producto = "" Then
            If Not lId_Arbol_Clase_Inst = "" Then
                Set lcRestricc_Porc_Producto = New Class_Restricc_Porc_Producto
                With lcRestricc_Porc_Producto
            '       .Campo("cod_producto").Valor = lCod_Producto
                    .Campo("id_arbol_clase_inst").Valor = lId_Arbol_Clase_Inst
                    .Campo("id_perfil_riesgo").Valor = lId_Perfil_Riesgo
                    If .Buscar Then
                        If .Cursor.Count > 0 Then
                         'Call SetCell(Grilla, lFila, "id_restricc_porc_prod", .Cursor(1).Fields("id_restricc_porc_prod").Value)
                          Call SetCell(Grilla, lFila, "id_restriccion", .Cursor(1).Fields("id_restricc_porc_prod").Value)
                          Call SetCell(Grilla, lFila, "porcentaje", To_Number(.Cursor(1).Fields("porcentaje").Value * 100))
                        End If
                    Else
                      Call Fnt_MsgError(.SubTipo_LOG, _
                                        "No se pudo completar la cargar de Restricciones de Porcentaje por Productos e Instrumentos.", _
                                        .ErrMsg, _
                                        pConLog:=True)
                      Exit For
                    End If
                End With
                Set lcRestricc_Porc_Producto = Nothing
            End If
            '----------------------------------------------------------------------------------
      
'''''      Rem  Carga los Porcentajes de Restricciones de Perfiles de Riesgo por Instrumentos
'''''      lCod_Instrumento = GetCell(Grilla, lFila, "cod_instrumento")
'''''      If Not lCod_Instrumento = "" Then
'''''        Set lcRestricc_Porc_Instrumento = New Class_Restricc_Porc_Instrumento
'''''        With lcRestricc_Porc_Instrumento
'''''          .Campo("cod_instrumento").Valor = lCod_Instrumento
'''''          .Campo("id_perfil_riesgo").Valor = lId_Perfil_Riesgo
'''''          If .Buscar Then
'''''            If .Cursor.Count > 0 Then
'''''              Call SetCell(Grilla, lFila, "id_restricc_porc_instr", .Cursor(1).Fields("id_restricc_porc_instr").Value)
'''''              Call SetCell(Grilla, lFila, "porcentaje", To_Number(.Cursor(1).Fields("porcentaje").Value * 100))
'''''            End If
'''''          Else
'''''            Call Fnt_MsgError(.SubTipo_LOG, _
'''''                              "No se pudo completar la cargar de Restricciones de Porcentaje por Productos e Instrumentos.", _
'''''                              .ErrMsg, _
'''''                              pConLog:=True)
'''''            Exit For
'''''          End If
'''''        End With
'''''        Set lcRestricc_Porc_Instrumento = Nothing
'''''      End If
'''''      '----------------------------------------------------------------------------------------
        Next
    End If
    
End Sub

Private Sub Sub_CargaForm()
    Dim lcProducto As Class_Productos
    Dim lReg As hFields
    Dim lFila As Long


    Dim lcArbol_Clase_Instrumento As Class_Arbol_Clase_Instrumento
    Call Sub_FormControl_Color(Me.Controls)
  
    Rem Carga Combo Perflies Perflies de Riesgo
    Call Sub_CargaCombo_Perfil_Riesgo(Cmb_Perfiles_Riesgo)

    Rem inicializa la grilla
    With Grilla
        Rem Disposici�n
        .Rows = 1
        .FixedCols = 0
        .ExtendLastCol = True
        .ColAlignment(-1) = flexAlignLeftTop
        .Editable = flexEDKbdMouse
        
        Rem Contorno
        .OutlineCol = 0
        .OutlineBar = flexOutlineBarSimpleLeaf
        '.MergeCells = flexMergeOutline
        
        Rem Otras
        .AllowUserResizing = flexResizeColumns
        .AllowSelection = False
        .GridLines = flexGridFlatVert
        
        .Redraw = flexRDNone
    End With
    
    Set lcArbol_Clase_Instrumento = New Class_Arbol_Clase_Instrumento
    With lcArbol_Clase_Instrumento
        .Campo("id_empresa").Valor = Fnt_EmpresaActual
        If .Buscar Then
            For Each lReg In .Cursor
                If IsNull(lReg("id_padre_arbol_clase_inst").Value) Then
                    lFila = Grilla.Rows
                    Call Grilla.AddItem("")
                    
                    Call SetCell(Grilla, lFila, "dsc_arbol_clase_inst", lReg("dsc_arbol_clase_inst").Value)
                    Call SetCell(Grilla, lFila, "id_arbol_clase_inst", lReg("id_arbol_clase_inst").Value)
                    Call SetCell(Grilla, lFila, "id_padre_arbol_clase_inst", NVL(lReg("id_padre_arbol_clase_inst").Value, 0))
                    Call SetCell(Grilla, lFila, "id_restriccion", cNewEntidad)
                    
                    Call SetCell(Grilla, lFila, "porcentaje", "")
                    
                    Grilla.IsSubtotal(lFila) = True
                    Grilla.RowOutlineLevel(lFila) = 0
                    
                    If Not Fnt_BuscaHijos(lReg("id_arbol_clase_inst").Value) Then
                        Exit For
                    End If
                    
                End If
            Next
      Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
      End If
    End With
    Set lcArbol_Clase_Instrumento = Nothing

    Grilla.Redraw = flexRDBuffered
'================================================================

'''''  Set lcProducto = New Class_Productos
'''''  With lcProducto
'''''    If .Buscar Then
'''''      For Each lreg In .Cursor
'''''
'''''        lFila = Grilla.Rows
'''''        Call Grilla.AddItem("")
'''''        Call SetCell(Grilla, lFila, "dsc_prod_inst", lreg("Dsc_producto").Value)
'''''        Call SetCell(Grilla, lFila, "cod_producto", lreg("cod_producto").Value)
'''''        Call SetCell(Grilla, lFila, "cod_instrumento", "")
'''''        Call SetCell(Grilla, lFila, "id_restricc_porc_prod", cNewEntidad)
'''''        Call SetCell(Grilla, lFila, "id_restricc_porc_instr", "")
'''''        Call SetCell(Grilla, lFila, "porcentaje", "")
'''''
'''''        Grilla.IsSubtotal(lFila) = True
'''''        Grilla.RowOutlineLevel(lFila) = 0
'''''
'''''        If Not Fnt_BuscaHijos(lreg("cod_producto").Value) Then
'''''          Exit For
'''''        End If
'''''      Next
'''''    Else
'''''      MsgBox .ErrMsg, vbCritical, Me.Caption
'''''    End If
'''''  End With
'''''  Set lcProducto = Nothing
'''''
'''''  Grilla.Redraw = flexRDBuffered

End Sub

''''''Private Function Fnt_BuscaHijos2(Id_Padre As String) As Boolean
''''''Dim lreg As hFields
''''''Dim lcInstrumentos As Class_Instrumentos
''''''Dim lFila  As Long
''''''
''''''  Fnt_BuscaHijos = True
''''''
''''''  Set lcInstrumentos = New Class_Instrumentos
''''''  With lcInstrumentos
''''''    '-------------- BUSCA LOS HIJOS DE UN NODO ---------------
''''''    .Campo("Cod_Producto").Valor = Id_Padre
''''''    If .Buscar Then
''''''      For Each lreg In .Cursor
''''''
''''''        lFila = Grilla.Rows
''''''        Call Grilla.AddItem("")
''''''        Call SetCell(Grilla, lFila, "dsc_prod_inst", lreg("Dsc_intrumento").Value)
''''''        Call SetCell(Grilla, lFila, "cod_producto", "")
''''''        Call SetCell(Grilla, lFila, "cod_instrumento", lreg("cod_instrumento").Value)
''''''        Call SetCell(Grilla, lFila, "id_restricc_porc_prod", "")
''''''        Call SetCell(Grilla, lFila, "id_restricc_porc_instr", cNewEntidad)
''''''        Call SetCell(Grilla, lFila, "porcentaje", "")
''''''
''''''        Grilla.IsSubtotal(lFila) = True
''''''        Grilla.RowOutlineLevel(lFila) = 1
''''''      Next
''''''    Else
''''''      Fnt_BuscaHijos = False
''''''      MsgBox .ErrMsg, vbCritical, Me.Caption
''''''    End If
''''''  End With
''''''  Set lcInstrumentos = Nothing
''''''
''''''End Function

Private Function Fnt_BuscaHijos(Id_Padre As String) As Boolean
Dim lReg As hFields
Dim lcArbol_Clase_Instrumento As Class_Arbol_Clase_Instrumento
Dim lFila  As Long
  
    Fnt_BuscaHijos = True
    
    Set lcArbol_Clase_Instrumento = New Class_Arbol_Clase_Instrumento
    With lcArbol_Clase_Instrumento
        '-------------- BUSCA LOS HIJOS DE UN NODO ---------------
        .Campo("id_padre_arbol_clase_inst").Valor = Id_Padre
        If .Buscar Then
            For Each lReg In .Cursor
        
                lFila = Grilla.Rows
                Call Grilla.AddItem("")
                Call SetCell(Grilla, lFila, "dsc_arbol_clase_inst", lReg("dsc_arbol_clase_inst").Value)
                Call SetCell(Grilla, lFila, "id_arbol_clase_inst", lReg("id_arbol_clase_inst").Value)
                Call SetCell(Grilla, lFila, "id_padre_arbol_clase_inst", lReg("id_padre_arbol_clase_inst").Value)
                Call SetCell(Grilla, lFila, "id_restriccion", cNewEntidad)
                Call SetCell(Grilla, lFila, "porcentaje", "")
                
                Grilla.IsSubtotal(lFila) = True
                Grilla.RowOutlineLevel(lFila) = 1
            Next
        Else
          Fnt_BuscaHijos = False
          MsgBox .ErrMsg, vbCritical, Me.Caption
        End If
    End With
    Set lcArbol_Clase_Instrumento = Nothing
  
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla.ColIndex("porcentaje") Then
    Cancel = True
  End If
End Sub

Private Sub Grilla_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
  Rem Acepta digitos, ".", y "backspace"
  If Not KeyAscii = 8 And Not KeyAscii = 46 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Grilla_ValidateEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    Dim lngSumaPorcentaje As Double
    Dim lngIdPadre As Long
    
    lngIdPadre = GetCell(Grilla, Row, "id_padre_arbol_clase_inst")
    
    If Not Trim(Grilla.EditText) = "" Then
        If lngIdPadre = 0 Then
            If To_Number(Grilla.EditText) > 100 Then
                MsgBox "Los porcentajes no pueden ser mayores a 100", vbCritical, Me.Caption
                Cancel = True
            End If
        Else
            lngSumaPorcentaje = validaPorcentaje(Grilla, Grilla.EditText, lngIdPadre, Row)
                
            If lngSumaPorcentaje > 100 Then
                MsgBox "Los porcentajes de las Familias no pueden ser mayores a 100", vbCritical, Me.Caption
                Cancel = True
            End If
        End If
    End If

End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Call Sub_Grabar
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
              
        'Call Sub_CargaForm
        Call Sub_CargarDatos
      End If
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Listado de Porcentajes de Perfiles de Riesgo por Productos e Instrumentos" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .TableCell(tcColWidth, col1:=1) = 3000
      .TableCell(tcColWidth, col1:=2) = 2000
      
      .EndTable
      .EndDoc
    End With
  End With
  
  lForm.Show
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Sub_Grabar()
Dim lRollback                   As Boolean
Dim lcRestricc_Porc_Producto    As Class_Restricc_Porc_Producto
Dim lcRestricc_Porc_Instrumento As Class_Restricc_Porc_Instrumento
Dim lId_Perfil_Riesgo           As String
Dim lFila                       As Long

  Call Sub_Bloquea_Puntero(Me)
  
  gDB.IniciarTransaccion
  lRollback = False
  
  If Not Fnt_Form_Validar(Me) Then
    lRollback = True
    GoTo ErrProcedure
  End If
  
  lId_Perfil_Riesgo = Fnt_ComboSelected_KEY(Cmb_Perfiles_Riesgo)
  
  For lFila = 1 To Grilla.Rows - 1
    
    Rem Productos
    'If Not GetCell(Grilla, lFila, "cod_producto") = "" Then
    If Not GetCell(Grilla, lFila, "id_arbol_clase_inst") = "" Then
      
      Set lcRestricc_Porc_Producto = New Class_Restricc_Porc_Producto
      With lcRestricc_Porc_Producto
        
        If Not GetCell(Grilla, lFila, "porcentaje") = "" Then
          .Campo("id_restricc_porc_prod").Valor = GetCell(Grilla, lFila, "id_restriccion")
          '.Campo("cod_producto").Valor = GetCell(Grilla, lFila, "cod_producto")
          .Campo("id_arbol_clase_inst").Valor = GetCell(Grilla, lFila, "id_arbol_clase_inst")
          .Campo("id_perfil_riesgo").Valor = lId_Perfil_Riesgo
          .Campo("porcentaje").Valor = To_Number(GetCell(Grilla, lFila, "porcentaje") / 100)
          
          If Not .Guardar Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en grabar Restricciones de Porcentaje de Perfiles de Riesgo por Productos.", _
                              .ErrMsg, _
                              pConLog:=True)
            lRollback = True
            GoTo ErrProcedure
          End If
        Else
          .Campo("id_restricc_porc_prod").Valor = GetCell(Grilla, lFila, "id_restriccion")
          If Not .Borrar Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en borrar Restricciones de Porcentaje de Perfiles de Riesgo por Productos.", _
                            .ErrMsg, _
                            pConLog:=True)
            lRollback = True
            GoTo ErrProcedure
          End If
        End If
      End With
      Set lcRestricc_Porc_Producto = Nothing
        
    Rem Instrumentos
'''''    ElseIf Not GetCell(Grilla, lFila, "cod_instrumento") = "" Then
'''''
'''''      Set lcRestricc_Porc_Instrumento = New Class_Restricc_Porc_Instrumento
'''''      With lcRestricc_Porc_Instrumento
'''''        If Not GetCell(Grilla, lFila, "porcentaje") = "" Then
'''''          .Campo("id_restricc_porc_instr").Valor = GetCell(Grilla, lFila, "id_restricc_porc_instr")
'''''          .Campo("cod_instrumento").Valor = GetCell(Grilla, lFila, "cod_instrumento")
'''''          .Campo("id_perfil_riesgo").Valor = lId_Perfil_Riesgo
'''''          .Campo("porcentaje").Valor = To_Number(GetCell(Grilla, lFila, "porcentaje") / 100)
'''''          If Not .Guardar Then
'''''            Call Fnt_MsgError(.SubTipo_LOG, _
'''''                              "Problemas en grabar Restricciones de Porcentaje de Perfiles de Riesgo por Instrumentos.", _
'''''                              .ErrMsg, _
'''''                              pConLog:=True)
'''''            lRollback = True
'''''            GoTo ErrProcedure
'''''          End If
'''''        Else
'''''          .Campo("id_restricc_porc_instr").Valor = GetCell(Grilla, lFila, "id_restricc_porc_instr")
'''''          If Not .Borrar Then
'''''            Call Fnt_MsgError(.SubTipo_LOG, _
'''''                            "Problemas en borrar Restricciones de Porcentaje de Perfiles de Riesgo por Instrumentos.", _
'''''                            .ErrMsg, _
'''''                            pConLog:=True)
'''''            lRollback = True
'''''            GoTo ErrProcedure
'''''          End If
'''''        End If
'''''      End With
'''''      Set lcRestricc_Porc_Instrumento = Nothing
      
    End If
  Next
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
    MsgBox "Porcentajes del Perfil de Riesgo '" & Cmb_Perfiles_Riesgo.Text & "' guardados correctamente.", vbInformation, Me.Caption
  End If

  Set lcRestricc_Porc_Producto = Nothing
  'Set lcRestricc_Porc_Instrumento = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents
  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub
