VERSION 5.00
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Cargos_Abonos_Fechas_Anteriores 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cargos y Abonos Fechas Anteriores"
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8295
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   8295
   Begin VB.Frame Frame1 
      Height          =   3885
      Left            =   30
      TabIndex        =   0
      Top             =   360
      Width           =   8235
      Begin VB.Frame Frame2 
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1125
         Left            =   120
         TabIndex        =   20
         Top             =   120
         Width           =   7905
         Begin hControl2.hTextLabel Txt_Rut 
            Height          =   315
            Left            =   90
            TabIndex        =   1
            Top             =   300
            Width           =   3990
            _ExtentX        =   7038
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   1200
            Caption         =   "RUT"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Nombres 
            Height          =   315
            Left            =   90
            TabIndex        =   3
            Top             =   660
            Width           =   7725
            _ExtentX        =   13626
            _ExtentY        =   556
            LabelWidth      =   1300
            Caption         =   "Nombres"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Perfil 
            Height          =   315
            Left            =   4140
            TabIndex        =   2
            Top             =   300
            Width           =   3660
            _ExtentX        =   6456
            _ExtentY        =   556
            LabelWidth      =   1000
            TextMinWidth    =   1200
            Caption         =   "Perfil Riesgo"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
      End
      Begin hControl2.hTextLabel Txt_FechaMovimiento 
         Height          =   315
         Left            =   5280
         TabIndex        =   5
         Top             =   1470
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Fecha"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Cuenta 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Top             =   1470
         Width           =   3885
         _ExtentX        =   6853
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Cuentas"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Observaci�n 
         Height          =   315
         Left            =   120
         TabIndex        =   11
         Tag             =   "OBLI"
         Top             =   2670
         Width           =   7995
         _ExtentX        =   14102
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Observaci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   119
      End
      Begin hControl2.hTextLabel Txt_SaldoCaja 
         Height          =   315
         Left            =   120
         TabIndex        =   9
         Top             =   2280
         Width           =   3870
         _ExtentX        =   6826
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Saldo Caja"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Dias_Retencion 
         Height          =   315
         Left            =   120
         TabIndex        =   13
         Tag             =   "OBLI"
         Top             =   3480
         Width           =   2010
         _ExtentX        =   3545
         _ExtentY        =   556
         LabelWidth      =   1500
         TextMinWidth    =   500
         Caption         =   "Fecha Liquidaci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Moneda 
         Height          =   315
         Left            =   5280
         TabIndex        =   8
         Top             =   1860
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Moneda"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Monto 
         Height          =   315
         Left            =   4350
         TabIndex        =   10
         Tag             =   "OBLI"
         Top             =   2280
         Width           =   3765
         _ExtentX        =   6641
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Monto"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin TrueDBList80.TDBCombo Cmb_Cajas 
         Height          =   345
         Left            =   1440
         TabIndex        =   7
         Tag             =   "OBLI=S;CAPTION=Caja"
         Top             =   1860
         Width           =   3795
         _ExtentX        =   6694
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Cargos_Abonos_Fechas_Anteriores.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Mov_Caja_Origen 
         Height          =   345
         Left            =   1620
         TabIndex        =   12
         Tag             =   "OBLI=S;CAPTION=Origen Movimiento"
         Top             =   3060
         Width           =   3795
         _ExtentX        =   6694
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Cargos_Abonos_Fechas_Anteriores.frx":00AA
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComCtl2.DTPicker Dtp_Fecha 
         Height          =   315
         Left            =   6600
         TabIndex        =   6
         Tag             =   "OBLI"
         Top             =   1470
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   68091905
         CurrentDate     =   38768
      End
      Begin MSComCtl2.DTPicker dtp_fecha_liquidacion 
         Height          =   315
         Left            =   2130
         TabIndex        =   14
         Tag             =   "OBLI"
         Top             =   3480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   68091905
         CurrentDate     =   38768
      End
      Begin VB.Label lbl_fecha 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5280
         TabIndex        =   19
         Top             =   1470
         Width           =   1305
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Caja"
         Height          =   345
         Left            =   120
         TabIndex        =   16
         Top             =   1860
         Width           =   1305
      End
      Begin VB.Label lbl_mov_origen 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Origen Movimiento"
         Height          =   345
         Left            =   120
         TabIndex        =   15
         Top             =   3060
         Width           =   1485
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   17
      Top             =   0
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   18
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Cargos_Abonos_Fechas_Anteriores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fTipo As String ' "Cargo" o "Abono"

Public fKey As String

Private fTipo_Cargo As String
Private fEstado As String
Private fId_Moneda As String

Const fFlg_Tipo_Origen_Manual = "M"

Dim fId_Cuenta As Double
Dim fId_Mov_Caja As String

Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fFlg_Mov_Descubiertos As String
Dim fFlg_Fechas_Anteriores As Boolean

Private Sub Cmb_Cajas_ItemChange()
Dim lCaja_Cuenta    As Class_Cajas_Cuenta
Dim lId_Caja_Cuenta As String
Dim lReg            As hFields

  Call Sub_Bloquea_Puntero(Me)

  lId_Caja_Cuenta = Fnt_FindValue4Display(Cmb_Cajas, Cmb_Cajas.Text)
  
  If Not lId_Caja_Cuenta = "" Then
    Set lCaja_Cuenta = New Class_Cajas_Cuenta
    With lCaja_Cuenta
      .Campo("id_caja_cuenta").Valor = lId_Caja_Cuenta

      If .Buscar(True) Then
        For Each lReg In .Cursor
          Txt_Moneda.Text = lReg("dsc_moneda").Value
          fId_Moneda = lReg("id_moneda").Value
          Txt_Monto.Format = Fnt_Formato_Moneda(lReg("id_moneda").Value)
          Txt_SaldoCaja.Format = Txt_Monto.Format
        Next
      End If
         
      Txt_SaldoCaja.Text = .Saldo_Disponible(Dtp_Fecha.Value)
    End With
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub DTP_Fecha_Change()
Dim lCaja_Cuenta    As Class_Cajas_Cuenta
Dim lId_Caja_Cuenta As String

  lId_Caja_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cajas)
  dtp_fecha_liquidacion.MinDate = Dtp_Fecha.Value
  dtp_fecha_liquidacion.Value = Dtp_Fecha.Value
  Txt_Dias_Retencion.Text = DateDiff("d", Dtp_Fecha.Value, dtp_fecha_liquidacion.Value)
  
  If Not lId_Caja_Cuenta = "" Then
    Set lCaja_Cuenta = New Class_Cajas_Cuenta
    lCaja_Cuenta.Campo("id_caja_cuenta").Valor = lId_Caja_Cuenta
    Txt_SaldoCaja.Text = lCaja_Cuenta.Saldo_Disponible(Dtp_Fecha.Value)
    Set lCaja_Cuenta = Nothing
  End If

End Sub

Private Sub dtp_fecha_liquidacion_Change()
    Txt_Dias_Retencion.Text = DateDiff("d", Dtp_Fecha.Value, dtp_fecha_liquidacion.Value)
End Sub

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub


Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
        MsgBox fTipo & " Ingresado correctamente.", vbInformation, Me.Caption
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        If fKey = cNewEntidad Then
          Call Sub_Limpiar
        Else
          Call Sub_CargarDatos
        End If
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lCargo_Abono As Class_Cargos_Abonos
Dim lRollback As Boolean
  
  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If
  
  gDB.IniciarTransaccion
  lRollback = False
  
  Set lCargo_Abono = New Class_Cargos_Abonos
  With lCargo_Abono
    .Campo("id_cargo_abono").Valor = fKey
    .Campo("Cod_Origen_Mov_Caja").Valor = Fnt_ComboSelected_KEY(Cmb_Mov_Caja_Origen)
    If fKey = cNewEntidad Then
      .Campo("Id_Mov_Caja").Valor = cNewEntidad
    Else
      .Campo("Id_Mov_Caja").Valor = fId_Mov_Caja
    End If
    .Campo("id_Caja_Cuenta").Valor = Fnt_ComboSelected_KEY(Cmb_Cajas)
    .Campo("id_Cuenta").Valor = fId_Cuenta
    .Campo("Dsc_Cargo_Abono").Valor = Txt_Observaci�n.Text
    .Campo("Flg_Tipo_Cargo").Valor = fTipo_Cargo
    .Campo("fecha_Movimiento").Valor = Dtp_Fecha.Value ' IIf(fKey = cNewEntidad, Dtp_Fecha.Value, Fnt_String2Date(Txt_FechaMovimiento.Text))
    .Campo("retencion").Valor = Txt_Dias_Retencion.Text
    .Campo("monto").Valor = Txt_Monto.Text
    .Campo("Id_Moneda").Valor = fId_Moneda
    .Campo("FLG_TIPO_ORIGEN").Valor = fFlg_Tipo_Origen_Manual 'De esta pantalla es siempre Manual
    .Campo("cod_estado").Valor = fEstado
    '.Campo("Id_Tipo_Estado").Valor = cTEstado_Mov_Caja        'Tipo de estado Mov_caja = 4
    If .Guardar Then
      fKey = .Campo("id_cargo_abono").Valor
    Else
      lRollback = True
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  
  If lRollback Then
    Fnt_Grabar = False
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  
End Function
Private Sub Sub_CargaForm()
Dim lReg As hFields
  
  Call Sub_Bloquea_Puntero(Me)
  
  fKey = cNewEntidad
  
  Call Sub_FormControl_Color(Me.Controls)
  Call Sub_CargaCombo_Cajas_Cuenta(Cmb_Cajas, fId_Cuenta)
  Call Sub_CargaCombo_Mov_Caja_Origen(Cmb_Mov_Caja_Origen)
  
  Dtp_Fecha.Value = Fnt_FechaServidor
  dtp_fecha_liquidacion.Value = Fnt_FechaServidor
  Txt_Dias_Retencion.Text = "0"
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lCargo_Abono As Class_Cargos_Abonos
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
  
  Call Sub_Bloquea_Puntero(Me)
  
  Load Me
  DoEvents
  
  fEstado = cCod_Estado_Pendiente
  Set lCargo_Abono = New Class_Cargos_Abonos
  With lCargo_Abono
    .Campo("id_cargo_abono").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        fId_Cuenta = lReg("id_cuenta").Value
        fId_Mov_Caja = lReg("id_mov_caja").Value
          
        Call Sub_ComboSelectedItem(Cmb_Cajas, lReg("Id_Caja_Cuenta").Value)
        Call Sub_ComboSelectedItem(Cmb_Mov_Caja_Origen, lReg("COD_ORIGEN_MOV_CAJA").Value)
        Txt_Observaci�n.Text = lReg("DSC_CARGO_ABONO").Value
        fTipo_Cargo = lReg("Flg_Tipo_Cargo").Value
        Txt_Dias_Retencion.Text = lReg("retencion").Value
        Txt_Monto.Text = lReg("monto").Value
        fEstado = lReg("cod_estado").Value
        fId_Moneda = lReg("id_moneda").Value
        Dtp_Fecha.Value = Format(lReg("FECHA_MOVIMIENTO").Value, cFormatDate)
        dtp_fecha_liquidacion.Value = DateAdd("d", Txt_Dias_Retencion.Text, Dtp_Fecha.Value)
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lCargo_Abono = Nothing
  
  Call Cmb_Cajas_ItemChange
  
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cuenta").Valor = fId_Cuenta
    .Campo("id_Empresa").Valor = Fnt_EmpresaActual
    If .Buscar_Vigentes Then
      If .Cursor.Count > 0 Then
        Txt_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("dsc_cuenta").Value
        fFlg_Mov_Descubiertos = .Cursor(1)("FLG_MOV_DESCUBIERTOS").Value
        
        Txt_Rut.Text = .Cursor(1)("rut_cliente").Value
        Txt_Perfil.Text = .Cursor(1)("DSC_PERFIL_RIESGO").Value
        Txt_Nombres.Text = .Cursor(1)("nombre_cliente").Value

      End If
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function Fnt_ValidarDatos() As Boolean
Dim lCaja_Cuenta    As Class_Cajas_Cuenta
Dim lId_Caja_Cuenta As String
Dim lSaldo_Caja     As Double
'-------------------------------------------------
Dim lResult As Boolean

  Select Case fEstado
    Case cCod_Estado_Liquidado, cCod_Estado_Anulado
      Fnt_ValidarDatos = False
      MsgBox "Solo se pueden modificar cuando est�n ""Pendientes"" de Liquidaci�n", vbExclamation, Me.Caption
      Exit Function
  End Select
  
  lResult = Fnt_Form_Validar(Me.Controls)
  
  If lResult Then
    If To_Number(Txt_Monto.Text) <= 0 Then
      MsgBox "El monto no puede ser cero ni negativo.", vbExclamation, Me.Caption
      lResult = False
      Txt_Monto.SetFocus
    ElseIf fTipo_Cargo = gcTipoOperacion_Cargo Then 'Si es un Cargo (rescate)
      Rem Se debe revisar el saldo de caja.
      
      lId_Caja_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cajas)
      lSaldo_Caja = 0
      
      Set lCaja_Cuenta = New Class_Cajas_Cuenta
      With lCaja_Cuenta
        .Campo("id_caja_cuenta").Valor = lId_Caja_Cuenta
        lSaldo_Caja = .Saldo_Disponible(Dtp_Fecha.Value)
      End With
      
      If To_Number(Txt_Monto.Text) > lSaldo_Caja Then
        If fFlg_Mov_Descubiertos = gcFlg_SI Then
            If MsgBox("El monto ingresado sobrepasa el saldo de la caja." & vbLf & vbLf & _
                    "Saldo : " & Format(lSaldo_Caja, "#,##0.00") & vbLf & _
                    "Monto : " & Format(To_Number(Txt_Monto.Text), "#,##0.00") & vbLf & "�Desea Continuar?" _
                  , vbYesNo + vbQuestion _
                , Me.Caption) = vbNo Then
                lResult = False
            End If
       Else
            MsgBox "El monto ingresado sobrepasa el saldo de la caja." & vbLf & vbLf & _
                    "Saldo : " & Format(lSaldo_Caja, "#,##0.00") & vbLf & _
                    "Monto : " & Format(To_Number(Txt_Monto.Text), "#,##0.00") _
                    , vbExclamation
            lResult = False
        End If
      End If
    End If
  End If
  
  Fnt_ValidarDatos = lResult
End Function

Public Function Fnt_Modificar(pkey, pTipo_Cargo, pId_Cuenta, pCod_Arbol_Sistema, pFlg_Fechas_Anteriores)
  
  fId_Cuenta = pId_Cuenta
  fFlg_Fechas_Anteriores = pFlg_Fechas_Anteriores
  
  Load Me
  
  fKey = pkey
  fTipo_Cargo = pTipo_Cargo
  
'-----------------------------------------------------------------------
'--Setea los controles dependiendo de los permisos
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
'-----------------------------------------------------------------------
  Call Sub_CargarDatos
  
  If Not fFlg_Fechas_Anteriores Then
    Dtp_Fecha.Visible = False
    Txt_FechaMovimiento.Visible = True
    Txt_FechaMovimiento.Text = Dtp_Fecha.Value
  Else
    Dtp_Fecha.Visible = True
    Txt_FechaMovimiento.Visible = False
    'DTP_Fecha.MaxDate = DTP_Fecha.Value
  End If
  dtp_fecha_liquidacion.MinDate = Dtp_Fecha.Value
  Select Case fTipo_Cargo
    Case gcTipoOperacion_Abono
      fTipo = "Abono"
    Case gcTipoOperacion_Cargo
      fTipo = "Cargo"
  End Select
    
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso " & fTipo
  Else
    Me.Caption = "Modificaci�n " & fTipo
    'Dtp_Fecha.Enabled = False
    Dtp_Fecha.Visible = True
    Txt_FechaMovimiento.Visible = True
    Txt_FechaMovimiento.Text = Dtp_Fecha.Value
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Sub_Limpiar()
  Cmb_Cajas.Text = ""
  Txt_Moneda.Text = ""
  Txt_SaldoCaja.Text = ""
  Txt_Monto.Text = ""
  Txt_Observaci�n.Text = ""
  Cmb_Mov_Caja_Origen.Text = ""
  Txt_Dias_Retencion.Text = ""
End Sub
Private Sub Txt_Dias_Retencion_KeyPress(KeyAscii As Integer)
  If Not KeyAscii = 8 And Not KeyAscii = 46 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub
Private Sub Txt_Dias_Retencion_LostFocus()
    dtp_fecha_liquidacion.Value = DateAdd("d", Txt_Dias_Retencion.Text, Dtp_Fecha.Value)
End Sub
