VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_VCuota_Patrimonio 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Valor Cuota y Patrimonio"
   ClientHeight    =   8370
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6900
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   6900
   Begin VB.Frame Frm_Filtros 
      Caption         =   "B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7065
      Left            =   60
      TabIndex        =   5
      Top             =   1230
      Width           =   6765
      Begin VB.Frame Frm_Propiedades_Cuentas 
         Caption         =   "Propiedades de la Cuenta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1935
         Left            =   150
         TabIndex        =   10
         Top             =   270
         Width           =   6525
         Begin MSComctlLib.Toolbar Toolbar_Chequear_Propietario 
            Height          =   330
            Left            =   5070
            TabIndex        =   11
            Top             =   1470
            Width           =   1320
            _ExtentX        =   2328
            _ExtentY        =   582
            ButtonWidth     =   1958
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Chequear"
                  Key             =   "CHK"
                  Object.ToolTipText     =   "Busca las cuentas seg�n los filtros"
               EndProperty
            EndProperty
         End
         Begin TrueDBList80.TDBCombo Cmb_Asesor 
            Height          =   345
            Left            =   1380
            TabIndex        =   12
            Top             =   270
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_VCuota_Patrimonio.frx":0000
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Clientes 
            Height          =   345
            Left            =   1380
            TabIndex        =   13
            Top             =   660
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_VCuota_Patrimonio.frx":00AA
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Grupos_Cuentas 
            Height          =   345
            Left            =   1380
            TabIndex        =   14
            Top             =   1050
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_VCuota_Patrimonio.frx":0154
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Lbl_Asesor 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Asesor"
            Height          =   345
            Left            =   90
            TabIndex        =   17
            Top             =   270
            Width           =   1275
         End
         Begin VB.Label Lbl_Clientes 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Clientes"
            Height          =   345
            Left            =   90
            TabIndex        =   16
            Top             =   660
            Width           =   1275
         End
         Begin VB.Label lbl_GruposCuentas 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Grupos Cuentas"
            Height          =   345
            Left            =   90
            TabIndex        =   15
            Top             =   1050
            Width           =   1275
         End
      End
      Begin VB.Frame Frm_Cuentas 
         Caption         =   "Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4815
         Left            =   150
         TabIndex        =   6
         Top             =   2160
         Width           =   6525
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
            Height          =   4425
            Left            =   90
            TabIndex        =   7
            Top             =   270
            Width           =   5745
            _cx             =   10134
            _cy             =   7805
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   6
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Reporte_VCuota_Patrimonio.frx":01FE
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Chequeo 
            Height          =   660
            Left            =   5940
            TabIndex        =   8
            Top             =   540
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar3 
               Height          =   255
               Left            =   9420
               TabIndex        =   9
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
   End
   Begin VB.Frame Frm_Fecha_Re_Proceso 
      Caption         =   "Fechas Proceso"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   795
      Left            =   60
      TabIndex        =   0
      Top             =   390
      Width           =   6765
      Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
         Height          =   315
         Left            =   1470
         TabIndex        =   1
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   65536001
         CurrentDate     =   37732
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
         Height          =   315
         Left            =   4500
         TabIndex        =   2
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   65536001
         CurrentDate     =   37732
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Hasta"
         Height          =   345
         Left            =   3240
         TabIndex        =   4
         Top             =   270
         Width           =   1215
      End
      Begin VB.Label Lbl_Fecha_Desde 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha_Desde"
         Height          =   345
         Left            =   210
         TabIndex        =   3
         Top             =   270
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   18
      Top             =   0
      Width           =   6900
      _ExtentX        =   12171
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORT"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Genera reporte de Rentabilidad por Perfil"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   19
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_VCuota_Patrimonio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Rem ---------------------------------------------
Rem 10/12/2008. Se agrega reporte a excel
Rem ---------------------------------------------
'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Public fApp_Excel           As Excel.Application      ' Excel application
Public fLibro               As Excel.Workbook      ' Excel workbook
Dim lFilaExcel              As Long

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Form_Load()
  
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORT").Image = cBoton_Modificar
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Chequear_Propietario
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("CHK").Image = cBoton_Agregar_Grilla
  End With
  
  With Toolbar_Chequeo
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
      .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
      .Appearance = ccFlat
  End With
   
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Cuentas_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla_Cuentas.ColIndex("CHK") Then
    Cancel = True
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "REPORT"
      Call Sub_Generar_Reporte
    Case "REFRESH"
      Call Sub_CargaForm
    Case "EXIT"
      Unload Me
  End Select
  
End Sub
Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Generar_Reporte
    Case "EXCEL"
      Call Sub_Crea_Excel
  End Select
End Sub
Private Sub Sub_CargaForm()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lReg As hFields
Dim lLinea As Long
  
  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_FormControl_Color(Me.Controls)

  Grilla_Cuentas.Rows = 1
  
  DTP_Fecha_Hasta.Value = Fnt_FechaServidor
  DTP_Fecha_Desde.Value = DTP_Fecha_Hasta.Value
  
  Rem Carga los combos con el primer elemento vac�o
  Call Sub_CargaCombo_Asesor(Cmb_Asesor, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKBLANCO)
  
  Call Sub_CargaCombo_Clientes(Cmb_Clientes, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Clientes, cCmbKBLANCO)
  
  Call Sub_CargaCombo_Grupos_Cuentas(Cmb_Grupos_Cuentas, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Grupos_Cuentas, cCmbKBLANCO)

  Rem Carga las cuentas habilitadas y de la empresa en la grilla
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("Cod_Estado").Valor = cCod_Estado_Habilitada
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    If .Buscar_Vigentes Then
      Call Sub_hRecord2Grilla(lcCuenta.Cursor, Grilla_Cuentas, "id_cuenta")
    Else
      Call Fnt_MsgError(.SubTipo_LOG _
                      , "Problema con buscar las cuentas vigentes." _
                      , .ErrMsg _
                      , pConLog:=True)
      Err.Clear
    End If
  End With
  Set lcCuenta = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Toolbar_Chequear_Propietario_ButtonClick(ByVal Button As MSComctlLib.Button)
Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "CHK"
      Call Sub_Busca_Cuentas_Propiedades
  End Select
  
End Sub

Private Sub Toolbar_Chequeo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
       Call Sub_CambiaCheck(True)
    Case "SEL_NOTHING"
       Call Sub_CambiaCheck(False)
  End Select
   
End Sub

Private Sub Sub_CambiaCheck(pValor As Boolean)
Dim lLinea As Long
Dim lCol As Long
  
  lCol = Grilla_Cuentas.ColIndex("CHK")
  If pValor Then
    For lLinea = 1 To Grilla_Cuentas.Rows - 1
      Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexChecked
    Next
  Else
    For lLinea = 1 To Grilla_Cuentas.Rows - 1
      Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexUnchecked
    Next
  End If
  
End Sub

Private Sub Sub_Busca_Cuentas_Propiedades()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lCursor As hCollection.hRecord
Dim lReg As hCollection.hFields
Dim lId_Asesor As String
Dim lId_Cliente As String
Dim lId_Grupos_Cuentas As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
  lId_Asesor = IIf(lId_Asesor = cCmbKBLANCO, "", lId_Asesor)
  
  lId_Cliente = Fnt_ComboSelected_KEY(Cmb_Clientes)
  lId_Cliente = IIf(lId_Cliente = cCmbKBLANCO, "", lId_Cliente)
  
  lId_Grupos_Cuentas = Fnt_ComboSelected_KEY(Cmb_Grupos_Cuentas)
  lId_Grupos_Cuentas = IIf(lId_Grupos_Cuentas = cCmbKBLANCO, "", lId_Grupos_Cuentas)
  
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_asesor").Valor = lId_Asesor
    .Campo("Id_Cliente").Valor = lId_Cliente
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar(pEnVista:=False, pId_Grupo_Cuenta:=lId_Grupos_Cuentas) Then
      For Each lReg In .Cursor
        Call Sub_Llena_Grilla_Cuentas(lReg("id_cuenta").Value)
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With

  Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Sub Sub_Llena_Grilla_Cuentas(pId_Cuenta As String)
Dim lFila As Long
Dim lCol As Long
  
  With Grilla_Cuentas
    If .Rows > 0 Then
      lCol = Grilla_Cuentas.ColIndex("CHK")
      For lFila = 1 To .Rows - 1
        If GetCell(Grilla_Cuentas, lFila, "colum_pk") = pId_Cuenta Then
          .Cell(flexcpChecked, lFila, lCol) = flexChecked
          Exit For
        End If
      Next
    End If
  End With
  
End Sub

Private Sub Sub_Generar_Reporte()
Const clrHeader = &HD0D0D0
'------------------------------------------
Const sHeader_Cta = "Fecha|Valor Cuota|Total Cuotas|Patrimonio|Movimientos Aporte/Rescate"
Const sFormat_Cta = ">1200|>1700|>1700|>2400|>2400"
'------------------------------------------
Dim sRecord
Dim bAppend
'------------------------------------------
Dim lForm As Frm_Reporte_Generico
Dim lFila As Long
Dim lCol As Long
'------------------------------------------
Dim lcPatriminio As Class_Patrimonio_Cuentas
Dim lId_Cuenta As String
Dim lId_Cuenta_Aux As String
Dim lNum_Cuenta As String
Dim lAbr_Cuenta As String
Dim lDsc_Cuenta As String
Dim lCursor_Cta As hRecord
Dim lReg_Cta As hFields
Dim lValor_Cuota As String
Dim lTotal_Cuotas As String
Dim lPatrimonio As String
Dim lAporte_Retiros As String
Dim lFecOpe_Cuenta As String
Dim lIndFOcds As String
Dim lblnIndFOcds As Boolean

  Call Sub_Bloquea_Puntero(Me)
  
  If DTP_Fecha_Desde.Value > DTP_Fecha_Hasta.Value Then
    MsgBox "La Fecha Desde no puede ser mayor que la Fecha Hasta.", vbInformation, Me.Caption
    GoTo ErrProcedure
  End If
  
  lCol = Grilla_Cuentas.ColIndex("CHK")
  
  Rem Comienzo de la generaci�n del reporte
  Set lForm = New Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="Reporte Valor Cuota y Patrimonio" _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orPortrait)
     
  With lForm.VsPrinter
    .FontSize = 9
    .Paragraph = "Periodo Consulta: " & DTP_Fecha_Desde.Value & " a " & DTP_Fecha_Hasta.Value
    .FontBold = False
    .Paragraph = "" 'salto de linea
    
    For lFila = 1 To Grilla_Cuentas.Rows - 1
      If Grilla_Cuentas.Cell(flexcpChecked, lFila, lCol) = flexChecked Then
        Grilla_Cuentas.Cell(flexcpFontBold, lFila, 0, lFila, Grilla_Cuentas.Cols - 1) = True
        Call Grilla_Cuentas.ShowCell(lFila, 0)
        Call Grilla_Cuentas.Refresh
        'Call Sub_Interactivo(gRelogDB)
      
        lId_Cuenta = GetCell(Grilla_Cuentas, lFila, "colum_pk")
        lNum_Cuenta = GetCell(Grilla_Cuentas, lFila, "num_cuenta")
        lFecOpe_Cuenta = GetCell(Grilla_Cuentas, lFila, "fecha_operativa")
        lblnIndFOcds = True
        
        Set lcPatriminio = New Class_Patrimonio_Cuentas
        With lcPatriminio
          .Campo("id_cuenta").Valor = lId_Cuenta
          If .Buscar_Entre(DTP_Fecha_Desde, DTP_Fecha_Hasta) Then
            Set lCursor_Cta = .Cursor
          Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            GoTo ErrProcedure
          End If
        End With
        Set lcPatriminio = Nothing
        
        If lCursor_Cta.Count > 0 Then
          If lId_Cuenta_Aux <> "" And lId_Cuenta_Aux <> "" Then
            .NewPage
          End If
          lId_Cuenta_Aux = lId_Cuenta
          lAbr_Cuenta = GetCell(Grilla_Cuentas, lFila, "rut_cliente")
          lDsc_Cuenta = GetCell(Grilla_Cuentas, lFila, "nombre_cliente")
          .DrawLine .MarginLeft, .CurrentY, .PageWidth - .MarginRight, .CurrentY
          .FontBold = True
          .Paragraph = UCase("Informaci�n de la Cuenta ")
          .FontBold = False
          .DrawLine .MarginLeft, .CurrentY, .PageWidth - .MarginRight, .CurrentY
          .StartTable
          .TableCell(tcRows) = 3
          .TableCell(tcCols) = 3
          .TableCell(tcColWidth, , 1) = "60mm"
          .TableCell(tcColWidth, , 2) = "5mm"
          .TableCell(tcColWidth, , 3) = "120mm"
          
          .TableCell(tcFontSize) = "9"
          .TableBorder = tbNone
        
          .TableCell(tcText, 1, 1) = "N�mero"
          .TableCell(tcText, 1, 2) = ":"
          .TableCell(tcText, 1, 3) = lNum_Cuenta
          
          .TableCell(tcText, 2, 1) = "Rut Cliente"
          .TableCell(tcText, 2, 2) = ":"
          .TableCell(tcText, 2, 3) = lAbr_Cuenta
        
          .TableCell(tcText, 3, 1) = "Nombre Cliente"
          .TableCell(tcText, 3, 2) = ":"
          .TableCell(tcText, 3, 3) = lDsc_Cuenta
          
          .EndTable
          
          .StartTable
          
          
          .TableBorder = tbBoxColumns
          If lCursor_Cta.Count > 0 Then
            BarraProceso.max = lCursor_Cta.Count
          End If
          For Each lReg_Cta In lCursor_Cta
            BarraProceso.Value = lReg_Cta.Index
            Call Sub_Interactivo(gRelogDB)
          
            lValor_Cuota = Format(lReg_Cta("valor_cuota_mon_cuenta").Value, "#,##0.0000")
            lPatrimonio = Format(lReg_Cta("patrimonio_mon_cuenta").Value, Fnt_Formato_Moneda(lReg_Cta("id_moneda_cuenta").Value))
            lAporte_Retiros = Format(lReg_Cta("APORTE_RETIROS_MON_CUENTA").Value, Fnt_Formato_Moneda(lReg_Cta("id_moneda_cuenta").Value))
            lTotal_Cuotas = Format(lReg_Cta("total_cuotas_mon_cuenta").Value, "#,##0.0000")
            
            lIndFOcds = "  "
            If (lId_Cuenta < 0) And (lblnIndFOcds) _
                And (Trim(lReg_Cta("fecha_cierre").Value) = Trim(lFecOpe_Cuenta)) Then
                lIndFOcds = "* "
                lblnIndFOcds = False
            End If
            sRecord = lIndFOcds & lReg_Cta("fecha_cierre").Value & "|" & _
                      lValor_Cuota & "|" & _
                      lTotal_Cuotas & "|" & _
                      lPatrimonio & "|" & _
                      lAporte_Retiros
            .AddTable sFormat_Cta, sHeader_Cta, sRecord, clrHeader, , bAppend
          Next
          .TableCell(tcFontBold, 0) = True
          .EndTable
          '.NewPage
          '.Paragraph = ""
          
          Grilla_Cuentas.Cell(flexcpFontBold, lFila, 0, lFila, Grilla_Cuentas.Cols - 1) = False
        Else
          .FontBold = False
          .Paragraph = "No hay datos para la Cuenta '" & lNum_Cuenta & "' y Fechas seleccionadas."
        End If
      End If
    Next
    .EndDoc
  End With
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub
'Agregado por MMA. 10/12/2008
Private Sub Sub_Crea_Excel()
Dim nHojas, i       As Integer
Dim hoja            As Integer
Dim Index           As Integer
    
    Set fApp_Excel = CreateObject("Excel.application")
    fApp_Excel.DisplayAlerts = False
    Set fLibro = fApp_Excel.Workbooks.Add
    
    fApp_Excel.ActiveWindow.DisplayGridlines = False
  
    ' App_Excel.Visible = True
    Call Sub_Bloquea_Puntero(Me)
    
    fLibro.Worksheets.Add
   
    With fLibro
        For i = .Worksheets.Count To 2 Step -1
            .Worksheets(i).Delete
        Next i
        .Sheets(1).Select
        .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).Select
            
        .ActiveSheet.Range("A5").Value = "Reporte Valor Cuota y Patrimonio"
        
        .ActiveSheet.Range("A5:E5").Font.Bold = True
        .ActiveSheet.Range("A5:E5").HorizontalAlignment = xlLeft
        '.ActiveSheet.Range("A5:E5").Merge
        
        .ActiveSheet.Range("A8").Value = "Periodo Consulta   " & DTP_Fecha_Desde.Value & " a " & DTP_Fecha_Hasta.Value
        
        .ActiveSheet.Range("A8:B8").HorizontalAlignment = xlLeft
        .ActiveSheet.Range("A8:B8").Font.Bold = True

        .Worksheets.Item(1).Columns("A:A").ColumnWidth = 10     'Numero Cuenta
        .Worksheets.Item(1).Columns("B:B").ColumnWidth = 15     'Rut Cliente
        .Worksheets.Item(1).Columns("C:C").ColumnWidth = 50     'Nombre Cliente
        .Worksheets.Item(1).Columns("D:D").ColumnWidth = 10     'Fecha
        .Worksheets.Item(1).Columns("E:E").ColumnWidth = 20     'Valor Cuota
        .Worksheets.Item(1).Columns("F:F").ColumnWidth = 20     'Total Cuotas
        .Worksheets.Item(1).Columns("G:G").ColumnWidth = 20     'Patrimonio
        .Worksheets.Item(1).Columns("H:H").ColumnWidth = 25     'Movimientos A/R
        .Worksheets.Item(1).Name = "Valor Cuota y Patrimonio"
    End With
    
    lFilaExcel = 10
    
    Generar_Listado_Excel
    
    fApp_Excel.Visible = True
    fApp_Excel.UserControl = True
    
    
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub


Private Sub Generar_Listado_Excel()
Dim lxHoja              As Worksheet
'----------------------------------------------------
Dim lFilaGrilla         As Integer
Dim iFilaInicio         As Integer
Dim iColGrilla          As Integer
Dim sId_Cuenta          As String
Dim sId_Cuenta_Aux      As String
Dim sNum_Cuenta         As String
Dim sAbr_Cuenta         As String
Dim sDsc_Cuenta         As String
Dim dTotalPatrimonio    As Double
Dim dTotalMovimientos   As Double
Dim dValor_Cuota        As Double
Dim dTotal_Cuotas       As Double
Dim dPatrimonio         As Double
Dim dAporte_Retiros     As Double
Dim sFormatoMoneda      As String
Dim lCol                As Long
'---------------------------------------------
Dim lcPatriminio As Class_Patrimonio_Cuentas
Dim sRecord
Dim lCursor_Cta As hRecord
Dim lReg_Cta As hFields
'---------------------------------------------
Dim lFecOpe_Cuenta As String
Dim lIndFOcds As String
Dim lblnIndFOcds As Boolean
'---------------------------------------------

    Call Sub_Bloquea_Puntero(Me)
    
    If DTP_Fecha_Desde.Value > DTP_Fecha_Hasta.Value Then
      MsgBox "La Fecha Desde no puede ser mayor que la Fecha Hasta.", vbInformation, Me.Caption
      GoTo ErrProcedure
    End If
    
    lCol = Grilla_Cuentas.ColIndex("CHK")
    
    
    Set lxHoja = fLibro.ActiveSheet
    Call ImprimeEncabezado
    
    With lxHoja
        For lFilaGrilla = 1 To Grilla_Cuentas.Rows - 1
            If Grilla_Cuentas.Cell(flexcpChecked, lFilaGrilla, lCol) = flexChecked Then
                Grilla_Cuentas.Cell(flexcpFontBold, lFilaGrilla, 0, lFilaGrilla, Grilla_Cuentas.Cols - 1) = True
                Call Grilla_Cuentas.ShowCell(lFilaGrilla, 0)
                Call Grilla_Cuentas.Refresh
                'Call Sub_Interactivo(gRelogDB)
              
                sId_Cuenta = GetCell(Grilla_Cuentas, lFilaGrilla, "colum_pk")
                sNum_Cuenta = GetCell(Grilla_Cuentas, lFilaGrilla, "num_cuenta")
                lFecOpe_Cuenta = GetCell(Grilla_Cuentas, lFilaGrilla, "fecha_operativa")
                lblnIndFOcds = True
                
                Set lcPatriminio = New Class_Patrimonio_Cuentas
                With lcPatriminio
                  .Campo("id_cuenta").Valor = sId_Cuenta
                  If .Buscar_Entre(DTP_Fecha_Desde, DTP_Fecha_Hasta) Then
                    Set lCursor_Cta = .Cursor
                  Else
                    MsgBox .ErrMsg, vbCritical, Me.Caption
                    GoTo ErrProcedure
                  End If
                End With
                Set lcPatriminio = Nothing
                
                If lCursor_Cta.Count > 0 Then
                    If sId_Cuenta_Aux <> "" And sId_Cuenta_Aux <> sId_Cuenta Then
                        Call ImprimeEncabezado
                        dTotalPatrimonio = 0
                        dTotalMovimientos = 0
                    End If
                    sId_Cuenta_Aux = sId_Cuenta
                    sAbr_Cuenta = GetCell(Grilla_Cuentas, lFilaGrilla, "rut_cliente")
                    sDsc_Cuenta = GetCell(Grilla_Cuentas, lFilaGrilla, "nombre_cliente")
        
                    
                    If lCursor_Cta.Count > 0 Then
                        BarraProceso.max = lCursor_Cta.Count
                    End If
                    iFilaInicio = lFilaExcel + 1
                    For Each lReg_Cta In lCursor_Cta
                        BarraProceso.Value = lReg_Cta.Index
                        
                        lFilaExcel = lFilaExcel + 1
                        .Cells(lFilaExcel, 1).Value = sNum_Cuenta
                        .Cells(lFilaExcel, 2).Value = sAbr_Cuenta
                        .Cells(lFilaExcel, 3).Value = sDsc_Cuenta
        
                        sFormatoMoneda = Fnt_Formato_Moneda(lReg_Cta("id_moneda_cuenta").Value)
                        dValor_Cuota = NVL(lReg_Cta("valor_cuota_mon_cuenta").Value, 0)
                        dTotal_Cuotas = NVL(lReg_Cta("total_cuotas_mon_cuenta").Value, 0)
                        dPatrimonio = NVL(lReg_Cta("patrimonio_mon_cuenta").Value, 0)
                        dAporte_Retiros = NVL(lReg_Cta("APORTE_RETIROS_MON_CUENTA").Value, 0)
                        dTotalPatrimonio = dTotalPatrimonio + dPatrimonio
                        dTotalMovimientos = dTotalMovimientos + dAporte_Retiros
                        
                        lIndFOcds = "  "
                        If (sId_Cuenta < 0) And (lblnIndFOcds) _
                            And (Trim(lReg_Cta("fecha_cierre").Value) = Trim(lFecOpe_Cuenta)) Then
                            lIndFOcds = "* "
                            lblnIndFOcds = False
                        End If
    
                        .Cells(lFilaExcel, 4).Value = lIndFOcds & lReg_Cta("fecha_cierre").Value
                        .Cells(lFilaExcel, 5).Value = dValor_Cuota
                        .Cells(lFilaExcel, 5).NumberFormat = "#,##0.0000"
                        .Cells(lFilaExcel, 6).Value = dTotal_Cuotas
                        .Cells(lFilaExcel, 6).NumberFormat = "#,##0.0000"
                        .Cells(lFilaExcel, 7).Value = dPatrimonio
                        .Cells(lFilaExcel, 7).NumberFormat = sFormatoMoneda
                        .Cells(lFilaExcel, 8).Value = dAporte_Retiros
                        .Cells(lFilaExcel, 8).NumberFormat = sFormatoMoneda
                    Next
                    'Muestra Totales por cuenta
                    lFilaExcel = lFilaExcel + 1
                    .Cells(lFilaExcel, 3).Value = "TOTAL CUENTA " & sId_Cuenta
                    .Cells(lFilaExcel, 7).Value = dTotalPatrimonio
                    .Cells(lFilaExcel, 7).NumberFormat = sFormatoMoneda
                    .Cells(lFilaExcel, 8).Value = dTotalMovimientos
                    .Cells(lFilaExcel, 8).NumberFormat = sFormatoMoneda
                    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 8)).Font.Bold = True
                    
                    .Range(.Cells(iFilaInicio, 1), .Cells(lFilaExcel, 2)).HorizontalAlignment = xlRight
                    .Range(.Cells(iFilaInicio, 3), .Cells(lFilaExcel, 3)).HorizontalAlignment = xlLeft
                    .Range(.Cells(iFilaInicio, 5), .Cells(lFilaExcel, 8)).HorizontalAlignment = xlRight
                    
                    lFilaExcel = lFilaExcel + 1
                    Grilla_Cuentas.Cell(flexcpFontBold, lFilaGrilla, 0, lFilaGrilla, Grilla_Cuentas.Cols - 1) = False
                    
                Else
                    .Cells(lFilaExcel, 1).Value = "No hay datos para la Cuenta '" & sNum_Cuenta & "' y Fechas seleccionadas."
                End If
            End If
        Next

    End With

ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub ImprimeEncabezado()
Dim lxHoja          As Worksheet
Dim i               As Integer

    Set lxHoja = fLibro.ActiveSheet
    lFilaExcel = lFilaExcel + 1
    With lxHoja
        .Cells(lFilaExcel, 1).Value = "Cuenta"
        .Cells(lFilaExcel, 2).Value = "Rut"
        .Cells(lFilaExcel, 3).Value = "Cliente"
        .Cells(lFilaExcel, 4).Value = "Fecha"
        .Cells(lFilaExcel, 5).Value = "Valor Cuota"
        .Cells(lFilaExcel, 6).Value = "Total Cuotas"
        .Cells(lFilaExcel, 7).Value = "Patrimonio"
        .Cells(lFilaExcel, 8).Value = "Movimientos Aportes/Retiros"
                
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 8)).BorderAround
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 8)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 8)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 8)).Font.Bold = True
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 8)).WrapText = True
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 8)).HorizontalAlignment = xlCenter
    End With
                    
End Sub

