VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Sorteo_Letras_Importador 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Importador Sorteo Letras"
   ClientHeight    =   5205
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   10245
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5205
   ScaleWidth      =   10245
   Begin VB.Frame Frame1 
      Caption         =   "Sorteo de Letras"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   60
      TabIndex        =   3
      Top             =   1440
      Width           =   10095
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   120
         TabIndex        =   4
         Top             =   300
         Width           =   9315
         _cx             =   16431
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   10
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Sorteo_Letras_Importador.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   1
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Selecciona 
         Height          =   660
         Left            =   9540
         TabIndex        =   8
         Top             =   570
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_ALL"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Selecciona todos los items"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_NOTHING"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Deselecciona todos los items"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar3 
            Height          =   255
            Left            =   9420
            TabIndex        =   9
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
   End
   Begin VB.Frame Frm_Busca_Archivo 
      Caption         =   "Carga Archivo Plano"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   975
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   10095
      Begin VB.CommandButton Cmb_BuscaFile 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   6360
         TabIndex        =   1
         ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
         Top             =   390
         Width           =   375
      End
      Begin hControl2.hTextLabel Txt_ArchivoPlano 
         Height          =   315
         Left            =   120
         TabIndex        =   2
         Tag             =   "OBLI"
         Top             =   390
         Width           =   6105
         _ExtentX        =   10769
         _ExtentY        =   556
         LabelWidth      =   1100
         TextMinWidth    =   1200
         Caption         =   "Archivo Plano"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin MSComDlg.CommonDialog Cmd_AbreArchivo 
         Left            =   5160
         Top             =   240
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin MSComctlLib.Toolbar Toolbar_Carga_Archivo 
         Height          =   330
         Left            =   7410
         TabIndex        =   7
         Top             =   390
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2487
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Importar      "
               Key             =   "IMPORT"
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   10245
      _ExtentX        =   18071
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   6
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Sorteo_Letras_Importador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fSalir As Boolean

Public Enum eExcel_Sorteo
  eGD_Rut_Cliente = 1
  eGD_Num_Cuenta
  eGD_Nemotecnico
  eGD_Nominales
  eGD_Tasa
  eGD_Monto
  eGD_Fecha_Mov
  eGD_Fecha_Liq
End Enum

Public Function Fnt_Modificar()
  
  Load Me
  
  Do While Not fSalir
    DoEvents
  Loop
  
End Function

Private Sub Cmb_BuscaFile_Click()
On Error GoTo Cmd_BuscarArchivo_Err
    
  With Cmd_AbreArchivo
    .CancelError = True
    '.InitDir = "C:\Mis documentos\"
    .Filter = "Texto (*.xls)|*.xls"
    .Flags = cdlOFNFileMustExist
  
    .ShowOpen
  End With
  
  Rem Se coloca el path completo, que incluye el nombre de archivo
  Txt_ArchivoPlano.Text = Cmd_AbreArchivo.Filename
  
  Exit Sub

Cmd_BuscarArchivo_Err:
  If Err = 32755 Then 'usuario presiona el boton Cancelar del Command Dialog
    Exit Sub
  End If
End Sub

Private Sub Form_Load()
  
  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SAVE").Image = cBoton_Grabar
    .Buttons("EXIT").Image = cBoton_Salir
    .Buttons("REFRESH").Image = cBoton_Original
  End With
  
  With Toolbar_Carga_Archivo
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("IMPORT").Image = "document"
  End With
  
  With Toolbar_Selecciona
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With
  
  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  fSalir = True
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla.ColIndex("chk") <> Col Then Cancel = True
End Sub

Private Sub Grilla_DblClick()
Dim lMensaje As String

  lMensaje = GetCell(Grilla, Grilla.Row, "error")
  If Not lMensaje = "" Then
    MsgBox lMensaje, vbInformation
  End If
  
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "SAVE"
      Call Sub_Grabar
      
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perdera las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargaForm
      End If
  End Select
End Sub

Private Sub Sub_CargaForm()
  Txt_ArchivoPlano.Text = ""
  Grilla.Rows = 1
End Sub

Private Sub Sub_Importar()
Dim lExcel As New Excel.Application
Dim lLibro As Excel.Workbook
Dim lHoja As Excel.Worksheet
'------------------------------
Dim lFila As Long
Dim lRut_Cliente
Dim lNum_Cuenta
Dim lNemotecnico
Dim lNominales
Dim lTasa
Dim lMonto
Dim lFecha_Mov
Dim lFecha_Liq
Dim lLinea As Long

On Error GoTo ErrProcedure
  
  Grilla.Rows = 1
  
  If Txt_ArchivoPlano.Text = "" Then
    MsgBox "Debe ingresar la ruta de la planilla Excel.", vbInformation, Me.Caption
    Exit Sub
  End If
  
  lExcel.Visible = False
  Rem Abre el libro excel
  Set lLibro = lExcel.Workbooks.Open(Txt_ArchivoPlano.Text, ReadOnly:=False, Editable:=True)
  Rem abre la hoja del libro a procesar
  Set lHoja = lLibro.Worksheets(1)

  With lHoja
    Rem Setea la linea de inicio de recorrido del excel
    lFila = 2
    
    Do While Not (Trim(.Cells(lFila, eExcel_Sorteo.eGD_Rut_Cliente)) = "" And _
                  Trim(.Cells(lFila, eExcel_Sorteo.eGD_Num_Cuenta)) = "")
    
      lRut_Cliente = Trim(.Cells(lFila, eExcel_Sorteo.eGD_Rut_Cliente))
      lNum_Cuenta = Trim(.Cells(lFila, eExcel_Sorteo.eGD_Num_Cuenta))
      lNemotecnico = Trim(.Cells(lFila, eExcel_Sorteo.eGD_Nemotecnico))
      lNominales = Trim(.Cells(lFila, eExcel_Sorteo.eGD_Nominales))
      lTasa = Trim(.Cells(lFila, eExcel_Sorteo.eGD_Tasa))
      lMonto = Trim(.Cells(lFila, eExcel_Sorteo.eGD_Monto))
      lFecha_Mov = Trim(.Cells(lFila, eExcel_Sorteo.eGD_Fecha_Mov))
      lFecha_Liq = Trim(.Cells(lFila, eExcel_Sorteo.eGD_Fecha_Liq))
      
      lLinea = Grilla.Rows
      Call Grilla.AddItem("")
      Grilla.Cell(flexcpChecked, lLinea, "chk") = flexChecked
      Call SetCell(Grilla, lLinea, "RUT_CLIENTE", lRut_Cliente)
      Call SetCell(Grilla, lLinea, "num_cuenta", lNum_Cuenta)
      Call SetCell(Grilla, lLinea, "NEMOTECNICO", lNemotecnico)
      Call SetCell(Grilla, lLinea, "CANTIDAD", lNominales)
      Call SetCell(Grilla, lLinea, "TASA", lTasa)
      Call SetCell(Grilla, lLinea, "MONTO", lMonto)
      Call SetCell(Grilla, lLinea, "FECHA_MOV", lFecha_Mov)
      Call SetCell(Grilla, lLinea, "FECHA_LIQ", lFecha_Liq)
      
      lFila = lFila + 1
    Loop
    
  End With
  
  MsgBox "Terminado el proceso de carga de la Planilla Excel.", vbInformation, Me.Caption
  
ErrProcedure:
  If Not Err.Number = 0 Then
    If MsgBox("Error en la carga de planilla Excel." & vbCr & vbCr & Err.Description, vbApplicationModal + vbCritical + vbOKCancel, Me.Caption) = vbRetry Then
      Resume
    End If
  End If
  
  lExcel.DisplayAlerts = False
  lExcel.Quit
End Sub

Private Sub Sub_Grabar()
Dim lFila As Long
Dim lRut_Cliente As String
Dim lNum_Cuenta As String
Dim lNemotecnico As String
Dim lCantidad
Dim lTasa
Dim lMonto
Dim lFecha_Mov
Dim lFecha_Liq
Dim lMsg_Error As String
Dim lId_Operacion As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Not Grilla.Rows > 1 Then
    MsgBox "No hay Sorteos de Letras en la Grilla.", vbInformation, Me.Caption
    GoTo ExitProcedure
  End If
  
  For lFila = 1 To Grilla.Rows - 1
    If Grilla.Cell(flexcpChecked, lFila, Grilla.ColIndex("chk")) = flexChecked Then
      lRut_Cliente = GetCell(Grilla, lFila, "rut_cliente")
      lNum_Cuenta = GetCell(Grilla, lFila, "num_cuenta")
      lNemotecnico = GetCell(Grilla, lFila, "nemotecnico")
      lCantidad = GetCell(Grilla, lFila, "cantidad")
      lTasa = GetCell(Grilla, lFila, "tasa")
      lMonto = GetCell(Grilla, lFila, "monto")
      lFecha_Mov = GetCell(Grilla, lFila, "fecha_mov")
      lFecha_Liq = GetCell(Grilla, lFila, "fecha_liq")
      
      If Fnt_Validaciones(pNemotecnico:=lNemotecnico, _
                          pCantidad:=lCantidad, _
                          pTasa:=lTasa, _
                          pMonto:=lMonto, _
                          pFecha_Mov:=lFecha_Mov, _
                          pFecha_Liq:=lFecha_Liq, _
                          pMsg_Error:=lMsg_Error) Then
        If Not Fnt_Graba_Sorteo_Letras(pRut_Cliente:=lRut_Cliente, _
                                       pNum_Cuenta:=lNum_Cuenta, _
                                       pNemotecnico:=lNemotecnico, _
                                       pCantidad:=CDbl(lCantidad), _
                                       pTasa:=CDbl(lTasa), _
                                       pMonto:=CDbl(lMonto), _
                                       pFecha_Mov:=CDate(lFecha_Mov), _
                                       pFecha_Liq:=CDate(lFecha_Liq), _
                                       pMsg_Error:=lMsg_Error, _
                                       pId_Operacion:=lId_Operacion) Then
          Call SetCell(Grilla, lFila, "error", lMsg_Error)
          Grilla.Cell(flexcpForeColor, lFila, 1, lFila, Grilla.Cols - 1) = vbRed
          Call SetCell(Grilla, lFila, "chk", flexChecked)
        Else
          Call SetCell(Grilla, lFila, "error", "Sorteo de Letra guardado correctamente." & vbCr & "N�mero de Operaci�n: " & lId_Operacion)
          Grilla.Cell(flexcpForeColor, lFila, 1, lFila, Grilla.Cols - 1) = vbBlue
          Call SetCell(Grilla, lFila, "chk", 0)
        End If
      Else
        Call SetCell(Grilla, lFila, "error", lMsg_Error)
        Grilla.Cell(flexcpForeColor, lFila, 1, lFila, Grilla.Cols - 1) = vbRed
        Call SetCell(Grilla, lFila, "chk", flexChecked)
      End If
    End If
  Next
  
  Call Grilla.Select(0, 0)
  
  MsgBox "Grabaci�n de Sorteo de Letras finalizada.", vbInformation
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Fnt_Validaciones(pNemotecnico, _
                                  pCantidad, _
                                  pTasa, _
                                  pMonto, _
                                  pFecha_Mov, _
                                  pFecha_Liq, _
                                  pMsg_Error) As Boolean
  Fnt_Validaciones = True
  
  If pNemotecnico = "" Then
    pMsg_Error = "Nemot�cnico est� vac�o."
    GoTo ErrProcedure
  ElseIf pCantidad = "" Then
    pMsg_Error = "Cantidad est� vac�o."
    GoTo ErrProcedure
  ElseIf pTasa = "" Then
    pMsg_Error = "Tasa est� vac�o."
    GoTo ErrProcedure
  ElseIf pMonto = "" Then
    pMsg_Error = "Monto est� vac�o."
    GoTo ErrProcedure
  ElseIf pFecha_Mov = "" Then
    pMsg_Error = "Fecha Movimiento est� vac�o."
    GoTo ErrProcedure
  ElseIf pFecha_Liq = "" Then
    pMsg_Error = "Fecha Liquidaci�n est� vac�o."
    GoTo ErrProcedure
  End If
  
  If Not IsNumeric(pCantidad) Then
    pMsg_Error = "Cantidad no es un valor num�rico."
    GoTo ErrProcedure
  ElseIf Not IsNumeric(pTasa) Then
    pMsg_Error = "Tasa no es un valor num�rico."
    GoTo ErrProcedure
  ElseIf Not IsNumeric(pMonto) Then
    pMsg_Error = "Monto no es un valor num�rico."
    GoTo ErrProcedure
  End If
  
  If Not IsDate(pFecha_Mov) Then
    pMsg_Error = "Fecha Movimiento no es reconocible como fecha."
    GoTo ErrProcedure
  ElseIf Not IsDate(pFecha_Liq) Then
    pMsg_Error = "Fecha Liquidaci�n no es reconocible como fecha."
    GoTo ErrProcedure
  End If
  
  Exit Function
  
ErrProcedure:
  Fnt_Validaciones = False
End Function

Private Function Fnt_Graba_Sorteo_Letras(pRut_Cliente As String, _
                                         pNum_Cuenta As String, _
                                         pNemotecnico As String, _
                                         pCantidad As Double, _
                                         pTasa As Double, _
                                         pMonto As Double, _
                                         pFecha_Mov As Date, _
                                         pFecha_Liq As Date, _
                                         ByRef pMsg_Error As String, _
                                         ByRef pId_Operacion As String) As Boolean
Dim lcBonos As Class_Bonos
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object

Dim lcNemotecnicos As Class_Nemotecnicos
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lcCliente As Object
'---------------------------------------
Dim lId_Cliente As String
Dim lId_Caja_Cuenta As String
Dim lId_Cuenta As String
Dim lId_Nemotecnico As String
Dim lId_Moneda_Transaccion As String
Dim lRollback As Boolean
'---------------------------------------
Dim lhActivos As hRecord
Dim lReg_Activo As hFields
  
  lRollback = False
  gDB.IniciarTransaccion
  
  If Not pNum_Cuenta = "" Then
    Rem Busca el id de la cuenta por numero de cuenta
'    Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
      .Campo("num_cuenta").Valor = pNum_Cuenta
      If .Buscar() Then
        If .Cursor.Count > 0 Then
          lId_Cuenta = .Cursor(1)("id_cuenta").Value
        Else
          pMsg_Error = "El N�mero de Cuenta no existe en el sistema."
          lRollback = True
          GoTo ErrProcedure
        End If
      Else
        pMsg_Error = "Problemas en buscar la Cuenta." & vbCr & .ErrMsg
        lRollback = True
        GoTo ErrProcedure
      End If
    End With
    Set lcCaja_Cuenta = Nothing
  Else
    Rem busca el id del cliente por el rut del cliente
    Set lcCliente = Fnt_CreateObject(cDLL_Clientes)
    With lcCliente
      Set .gDB = gDB
      .Campo("rut_cliente").Valor = pRut_Cliente
      If .Buscar(True) Then
        If .Cursor.Count > 0 Then
          lId_Cliente = .Cursor(1)("id_cliente").Value
        Else
          pMsg_Error = "Rut de Cliente no existe en el sistema."
          lRollback = True
          GoTo ErrProcedure
        End If
      Else
        pMsg_Error = "Problemas en buscar el Rut del Cliente." & vbCr & .ErrMsg
        lRollback = True
        GoTo ErrProcedure
      End If
    End With
    Set lcCliente = Nothing
    
    Rem Busca el id de la cuenta por id de cliente
'    Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
      .Campo("id_cliente").Valor = lId_Cliente
      If .Buscar() Then
        Select Case .Cursor.Count
          Case 1
            lId_Cuenta = .Cursor(1)("id_cuenta").Value
          Case Is > 1
            pMsg_Error = "Hay m�s de una Cuenta para el Rut ' " & pRut_Cliente & "'. No se puede seguir cursando la operaci�n."
            lRollback = True
            GoTo ErrProcedure
          Case Is <= 0
            pMsg_Error = "El Rut de Cliente no tiene asociada Cuentas en el sistema."
            lRollback = True
            GoTo ErrProcedure
        End Select
      Else
        pMsg_Error = "Problemas en buscar la Cuenta." & vbCr & .ErrMsg
        lRollback = True
        GoTo ErrProcedure
      End If
    End With
    Set lcCaja_Cuenta = Nothing
  End If
  
  Set lcNemotecnicos = New Class_Nemotecnicos
  With lcNemotecnicos
    .Campo("nemotecnico").Valor = pNemotecnico
    Rem Busca el id_nemotecnico del Nemotecnico de la planilla
    If .Buscar_Nemotecnico(pCod_Producto:=Null, pMostrar_Msg:=False) Then
      lId_Nemotecnico = .Campo("id_nemotecnico").Valor
      If Not lId_Nemotecnico = "0" Then
        Rem Busca atributos del Nemotecnico
        If .Buscar Then
          lId_Moneda_Transaccion = .Cursor(1)("id_moneda_transaccion").Value
        Else
        End If
      Else
        pMsg_Error = "El Nemot�cnico no existe en el sistema."
        lRollback = True
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = "Problemas en buscar Nemot�cnico." & vbCr & .ErrMsg
      lRollback = True
      GoTo ErrProcedure
    End If
  End With
  Set lcNemotecnicos = Nothing
  
  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("id_moneda").Valor = lId_Moneda_Transaccion
    .Campo("cod_mercado").Valor = cMercado_Nacional
    If .Buscar_Caja_Para_Invertir() Then
      lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
    Else
      pMsg_Error = "Problemas en cargar Cajas de la Cuenta." & vbCr & .ErrMsg
      lRollback = True
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  
  Set lcBonos = New Class_Bonos
  With lcBonos
    If Not Fnt_Enlaza_VentasCompras_RF(pCod_Instrumento:=gcINST_BONOS_NAC, _
                                       pId_Cuenta:=lId_Cuenta, _
                                       pId_Nemotecnico:=lId_Nemotecnico, _
                                       pCantidad:=pCantidad, _
                                       pFecha_Movimiento:=pFecha_Mov, _
                                       phActivos:=lhActivos) Then
      pMsg_Error = "Problemas en buscar el enlaze para el Sorteo de Letras." & .ErrMsg
      lRollback = True
      GoTo ErrProcedure
    End If
    '-----------------------------------------------------------------------------
    If lhActivos.Count > 0 Then
      For Each lReg_Activo In lhActivos
        Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lId_Nemotecnico, _
                                          pCantidad:=lReg_Activo("asignado").Value, _
                                          pTasa:=pTasa, _
                                          pId_Moneda:=lId_Moneda_Transaccion, _
                                          pMonto:=pMonto, _
                                          pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                          pUtilidad:=0, _
                                          PTasa_Gestion:=pTasa, _
                                          pTasa_Historico:=pTasa, _
                                          pId_Mov_Activo_Compra:=lReg_Activo("Id_Mov_Activo").Value)
      Next
      
      If Not .Realiza_Operacion_Sorteo_Letras(pId_Operacion:=pId_Operacion, _
                                              pId_Cuenta:=lId_Cuenta, _
                                              pId_Moneda_Operacion:=lId_Moneda_Transaccion, _
                                              pFecha_Movimiento:=pFecha_Mov, _
                                              pFecha_Liquidacion:=pFecha_Liq, _
                                              pDsc_Operacion:="", _
                                              pMonto_Operacion:=pMonto, _
                                              pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
        pMsg_Error = "Problemas al grabar Sorteo de Letras." & vbCr & .ErrMsg
        lRollback = True
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = "No existen ventas para el dia anterior para el nemot�cnico '" & pNemotecnico & "'."
      lRollback = True
      GoTo ErrProcedure
    End If
  End With
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If

  Fnt_Graba_Sorteo_Letras = Not lRollback

End Function

Private Sub Toolbar_Carga_Archivo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "IMPORT"
      Call Sub_Importar
  End Select
End Sub

Private Sub Toolbar_Selecciona_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla, False)
  End Select
End Sub

Private Sub Sub_CambiaCheck(pGrilla As VSFlexGrid, pValor As Boolean)
    Dim lLinea As Long
    If pValor Then
        For lLinea = 1 To pGrilla.Rows - 1
            'pGrilla.Cell(flexcpChecked, lLinea, "chk") = flexChecked
            Call SetCell(pGrilla, lLinea, "chk", flexChecked)
        Next
    Else
        For lLinea = 1 To pGrilla.Rows - 1
            'pGrilla.Cell(flexcpChecked, lLinea, "chk") = 0
            Call SetCell(pGrilla, lLinea, "chk", 0)
        Next
    End If
End Sub
