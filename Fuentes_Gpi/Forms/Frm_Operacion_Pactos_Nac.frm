VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Operacion_Pactos_Nac 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pactos Nacionales"
   ClientHeight    =   6795
   ClientLeft      =   45
   ClientTop       =   540
   ClientWidth     =   11535
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6795
   ScaleWidth      =   11535
   Begin VB.Frame Frame2 
      Caption         =   "Cliente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1035
      Left            =   90
      TabIndex        =   60
      Top             =   360
      Width           =   11355
      Begin hControl2.hTextLabel Txt_Rut 
         Height          =   315
         Left            =   150
         TabIndex        =   61
         Top             =   240
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "RUT"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Nombres 
         Height          =   315
         Left            =   3030
         TabIndex        =   62
         Top             =   240
         Width           =   8025
         _ExtentX        =   14155
         _ExtentY        =   556
         LabelWidth      =   1000
         Caption         =   "Nombres"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Perfil 
         Height          =   315
         Left            =   3030
         TabIndex        =   63
         Top             =   600
         Width           =   3300
         _ExtentX        =   5821
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "Perfil Riesgo"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   315
         Left            =   150
         TabIndex        =   64
         Top             =   600
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "Cuenta"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5235
      Left            =   90
      TabIndex        =   26
      Top             =   1470
      Width           =   11355
      _ExtentX        =   20029
      _ExtentY        =   9234
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "Frm_Operacion_Pactos_Nac.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frm_Datos_Compra"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame_Nemotecnico"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Clasificadores de Riesgo"
      TabPicture(1)   =   "Frm_Operacion_Pactos_Nac.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frm_Clasificador"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frm_Clasificador 
         Height          =   4125
         Left            =   -74850
         TabIndex        =   46
         Top             =   450
         Width           =   8715
         Begin VSFlex8LCtl.VSFlexGrid Grilla 
            Height          =   3675
            Left            =   150
            TabIndex        =   47
            Top             =   270
            Width           =   7785
            _cx             =   13732
            _cy             =   6482
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   3
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Operacion_Pactos_Nac.frx":0038
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Clasificadores 
            Height          =   660
            Left            =   8070
            TabIndex        =   48
            Top             =   540
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ADD"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Agregar un Clasificador de Riesgo al Nemot�cnico"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "DEL"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Elimina un Clasificador de Riesgo al Nemot�cnico"
               EndProperty
            EndProperty
         End
      End
      Begin VB.Frame Frame_Nemotecnico 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1065
         Left            =   120
         TabIndex        =   43
         Top             =   390
         Width           =   11100
         Begin VB.CheckBox chkAporteRetiro 
            Caption         =   "� Aporte/Retiro de Capital ?"
            Height          =   255
            Left            =   3720
            TabIndex        =   65
            Top             =   300
            Value           =   1  'Checked
            Width           =   2295
         End
         Begin hControl2.hTextLabel Txt_FechaIngreso_Real 
            Height          =   345
            Left            =   8040
            TabIndex        =   2
            Top             =   210
            Width           =   2880
            _ExtentX        =   5080
            _ExtentY        =   609
            LabelWidth      =   1350
            TextMinWidth    =   1000
            Caption         =   "Fecha Operaci�n"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Num_Operacion 
            Height          =   315
            Left            =   150
            TabIndex        =   0
            Top             =   240
            Width           =   3285
            _ExtentX        =   5794
            _ExtentY        =   556
            LabelWidth      =   1215
            Caption         =   "N� Operaci�n"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin TrueDBList80.TDBCombo Cmb_Contraparte 
            Height          =   345
            Left            =   1380
            TabIndex        =   1
            Top             =   630
            Width           =   4095
            _ExtentX        =   7223
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Nac.frx":0104
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Traders 
            Height          =   345
            Left            =   6840
            TabIndex        =   3
            Top             =   630
            Width           =   4095
            _ExtentX        =   7223
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Nac.frx":01AE
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Operacion 
            Height          =   345
            Left            =   9390
            TabIndex        =   58
            Tag             =   "OBLI"
            Top             =   210
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   609
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   57212929
            CurrentDate     =   38768
         End
         Begin VB.Label lbl_fecha_ingreso 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Operaci�n"
            Height          =   345
            Left            =   8040
            TabIndex        =   59
            Top             =   210
            Width           =   1335
         End
         Begin VB.Label Lbl_Contraparte 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Contraparte"
            Height          =   345
            Left            =   150
            TabIndex        =   56
            Top             =   630
            Width           =   1215
         End
         Begin VB.Label Label1 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Trader"
            Height          =   345
            Left            =   5610
            TabIndex        =   55
            Top             =   630
            Width           =   1215
         End
      End
      Begin VB.Frame Frm_Datos_Compra 
         Caption         =   "Datos Operaci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   3615
         Left            =   120
         TabIndex        =   27
         Top             =   1500
         Width           =   11100
         Begin TrueDBList80.TDBCombo Cmb_Base 
            Height          =   345
            Left            =   5670
            TabIndex        =   10
            Top             =   990
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Nac.frx":0258
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Plazo 
            Height          =   345
            Left            =   5670
            TabIndex        =   9
            Top             =   630
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Nac.frx":0302
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Moneda_Pago 
            Height          =   345
            Left            =   1620
            TabIndex        =   7
            Tag             =   "OBLI=S;CAPTION=Moneda de Pago"
            Top             =   1350
            Visible         =   0   'False
            Width           =   2475
            _ExtentX        =   4366
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Nac.frx":03AC
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Moneda_Pactos 
            Height          =   345
            Left            =   1620
            TabIndex        =   6
            Tag             =   "OBLI=S;CAPTION=Moneda Pactos"
            Top             =   990
            Width           =   2475
            _ExtentX        =   4366
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Nac.frx":0456
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Emisor_Especifico 
            Height          =   345
            Left            =   1620
            TabIndex        =   5
            Tag             =   "OBLI=S;CAPTION=Emisor"
            Top             =   630
            Width           =   2475
            _ExtentX        =   4366
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Nac.frx":0500
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Frame Frame_Datos_Documento 
            Caption         =   "Datos Documento"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   1365
            Left            =   120
            TabIndex        =   50
            Top             =   2130
            Width           =   3975
            Begin MSComCtl2.DTPicker Dtp_Fecha_Valuta 
               Height          =   345
               Left            =   1860
               TabIndex        =   18
               Tag             =   "OBLI"
               Top             =   360
               Width           =   1185
               _ExtentX        =   2090
               _ExtentY        =   609
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Format          =   57212929
               CurrentDate     =   38768
            End
            Begin MSComCtl2.DTPicker Dtp_FechaLiquidacion 
               Height          =   345
               Left            =   2610
               TabIndex        =   20
               Tag             =   "OBLI"
               Top             =   810
               Width           =   1185
               _ExtentX        =   2090
               _ExtentY        =   609
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Format          =   57212929
               CurrentDate     =   38768
            End
            Begin TrueDBList80.TDBCombo Cmb_FechaLiquidacion 
               Height          =   345
               Left            =   1860
               TabIndex        =   19
               Top             =   810
               Width           =   705
               _ExtentX        =   1244
               _ExtentY        =   609
               _LayoutType     =   4
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   1
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=1"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   0   'False
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Operacion_Pactos_Nac.frx":05AA
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Named:id=33:Normal"
               _StyleDefs(35)  =   ":id=33,.parent=0"
               _StyleDefs(36)  =   "Named:id=34:Heading"
               _StyleDefs(37)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(38)  =   ":id=34,.wraptext=-1"
               _StyleDefs(39)  =   "Named:id=35:Footing"
               _StyleDefs(40)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(41)  =   "Named:id=36:Selected"
               _StyleDefs(42)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(43)  =   "Named:id=37:Caption"
               _StyleDefs(44)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(45)  =   "Named:id=38:HighlightRow"
               _StyleDefs(46)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=39:EvenRow"
               _StyleDefs(48)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(49)  =   "Named:id=40:OddRow"
               _StyleDefs(50)  =   ":id=40,.parent=33"
               _StyleDefs(51)  =   "Named:id=41:RecordSelector"
               _StyleDefs(52)  =   ":id=41,.parent=34"
               _StyleDefs(53)  =   "Named:id=42:FilterBar"
               _StyleDefs(54)  =   ":id=42,.parent=33"
            End
            Begin VB.Label Lbl_FechaLiquidacion 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha Liquidaci�n T+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Left            =   210
               TabIndex        =   52
               Top             =   810
               Width           =   1620
            End
            Begin VB.Label lbl_fecha_valuta 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha Valuta Vcto."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Left            =   210
               TabIndex        =   51
               Top             =   360
               Width           =   1620
            End
         End
         Begin VB.Frame Frm_referenciado 
            Caption         =   "Referenciado"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   555
            Left            =   4170
            TabIndex        =   30
            Top             =   1380
            Visible         =   0   'False
            Width           =   2595
            Begin VB.OptionButton Opt_Inicio 
               Caption         =   "Inicio"
               Height          =   225
               Left            =   330
               TabIndex        =   11
               Top             =   240
               Value           =   -1  'True
               Width           =   855
            End
            Begin VB.OptionButton Opt_Final 
               Caption         =   "Final"
               Height          =   285
               Left            =   1440
               TabIndex        =   12
               Top             =   210
               Width           =   705
            End
         End
         Begin VB.Frame Frame_Comisiones 
            Height          =   1365
            Left            =   4170
            TabIndex        =   29
            Top             =   2130
            Width           =   6825
            Begin hControl2.hTextLabel Txt_Porcentaje_Comision 
               Height          =   315
               Left            =   120
               TabIndex        =   21
               Tag             =   "OBLI"
               Top             =   210
               Width           =   2175
               _ExtentX        =   3836
               _ExtentY        =   556
               LabelWidth      =   1400
               TextMinWidth    =   500
               Caption         =   "Comisi�n (%)"
               Text            =   "0,0000"
               Text            =   "0,0000"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.0000"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Gastos 
               Height          =   315
               Left            =   120
               TabIndex        =   24
               Tag             =   "OBLI"
               Top             =   930
               Width           =   3255
               _ExtentX        =   5741
               _ExtentY        =   556
               LabelWidth      =   1400
               TextMinWidth    =   500
               Caption         =   "Gastos"
               Text            =   "0.00"
               Text            =   "0.00"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Derechos 
               Height          =   315
               Left            =   3465
               TabIndex        =   25
               Tag             =   "OBLI"
               Top             =   570
               Width           =   3255
               _ExtentX        =   5741
               _ExtentY        =   556
               LabelWidth      =   1400
               TextMinWidth    =   500
               Caption         =   "Derechos Bolsa"
               Text            =   "0.00"
               Text            =   "0.00"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Iva 
               Height          =   315
               Left            =   3450
               TabIndex        =   23
               Tag             =   "OBLI"
               Top             =   930
               Width           =   3255
               _ExtentX        =   5741
               _ExtentY        =   556
               LabelWidth      =   1400
               TextMinWidth    =   500
               Caption         =   " Iva (%)"
               Text            =   "0.00"
               Text            =   "0.00"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Comision 
               Height          =   315
               Left            =   120
               TabIndex        =   22
               Tag             =   "OBLI"
               Top             =   570
               Width           =   3255
               _ExtentX        =   5741
               _ExtentY        =   556
               LabelWidth      =   1400
               TextMinWidth    =   500
               Caption         =   "Comisi�n a Cobrar"
               Text            =   "0.00"
               Text            =   "0.00"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Porcentaje_Derechos 
               Height          =   315
               Left            =   3450
               TabIndex        =   57
               Tag             =   "OBLI"
               Top             =   210
               Width           =   2055
               _ExtentX        =   3625
               _ExtentY        =   556
               LabelWidth      =   1400
               TextMinWidth    =   500
               Caption         =   "Derechos (%)"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
         End
         Begin VB.Frame Frm_Valorizador 
            Height          =   1965
            Left            =   7350
            TabIndex        =   28
            Top             =   150
            Width           =   3645
            Begin VB.CheckBox Chk_Vende_Todo 
               Caption         =   "Vende Todo"
               Height          =   315
               Left            =   480
               TabIndex        =   15
               Top             =   1020
               Width           =   1185
            End
            Begin hControl2.hTextLabel Txt_Cantidad 
               Height          =   315
               Left            =   120
               TabIndex        =   14
               Tag             =   "OBLI"
               Top             =   630
               Width           =   3375
               _ExtentX        =   5953
               _ExtentY        =   556
               LabelWidth      =   1400
               Caption         =   "Nominales"
               Text            =   "0.0000"
               Text            =   "0.0000"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.0000"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin MSComctlLib.Toolbar Toolbar_Valorizar 
               Height          =   330
               Left            =   2400
               TabIndex        =   53
               Top             =   1020
               Width           =   1110
               _ExtentX        =   1958
               _ExtentY        =   582
               ButtonWidth     =   1799
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   1
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Valorizar"
                     Key             =   "VALORIZA"
                     Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
                     Object.ToolTipText     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
                  EndProperty
               EndProperty
            End
            Begin hControl2.hTextLabel Txt_TasaNominal 
               Height          =   315
               Left            =   120
               TabIndex        =   13
               Tag             =   "OBLI"
               Top             =   270
               Width           =   3375
               _ExtentX        =   5953
               _ExtentY        =   556
               LabelWidth      =   1400
               Caption         =   "Tasa Pactos"
               Text            =   "0.0000"
               Text            =   "0.0000"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.0000"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_MontoOperacion 
               Height          =   315
               Left            =   120
               TabIndex        =   16
               Tag             =   "OBLI"
               Top             =   1440
               Width           =   3375
               _ExtentX        =   5953
               _ExtentY        =   556
               LabelWidth      =   1400
               Caption         =   "Monto Operaci�n"
               Text            =   "0.00"
               Text            =   "0.00"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Tasa_Historica 
               Height          =   315
               Left            =   120
               TabIndex        =   17
               Top             =   1800
               Visible         =   0   'False
               Width           =   3375
               _ExtentX        =   5953
               _ExtentY        =   556
               LabelWidth      =   1400
               Caption         =   "Tasa Hist�rica"
               Text            =   "0.00"
               Text            =   "0.00"
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
         End
         Begin hControl2.hTextLabel Txt_Nemotecnico 
            Height          =   315
            Left            =   120
            TabIndex        =   4
            Tag             =   "OBLI"
            Top             =   270
            Width           =   3975
            _ExtentX        =   7011
            _ExtentY        =   556
            LabelWidth      =   1470
            Caption         =   "Nemot�cnico"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
         End
         Begin MSComCtl2.DTPicker Dtp_Fecha_Vencimiento 
            Height          =   315
            Left            =   5670
            TabIndex        =   8
            Tag             =   "OBLI"
            Top             =   270
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   57212929
            CurrentDate     =   38768
         End
         Begin hControl2.hTextLabel Txt_Fecha_Vencimiento 
            Height          =   315
            Left            =   4170
            TabIndex        =   31
            Top             =   270
            Visible         =   0   'False
            Width           =   3060
            _ExtentX        =   5398
            _ExtentY        =   556
            LabelWidth      =   1470
            TextMinWidth    =   1000
            Caption         =   "Fecha Vencimiento"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Base 
            Height          =   345
            Left            =   4170
            TabIndex        =   32
            Top             =   990
            Visible         =   0   'False
            Width           =   3060
            _ExtentX        =   5398
            _ExtentY        =   609
            LabelWidth      =   1470
            TextMinWidth    =   1000
            Caption         =   "Base en d�as"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Moneda_Pactos 
            Height          =   345
            Left            =   120
            TabIndex        =   33
            Top             =   990
            Visible         =   0   'False
            Width           =   3975
            _ExtentX        =   7011
            _ExtentY        =   609
            LabelWidth      =   1470
            Caption         =   "Moneda Pactos"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Emisor_Especifico 
            Height          =   345
            Left            =   120
            TabIndex        =   34
            Top             =   630
            Visible         =   0   'False
            Width           =   3975
            _ExtentX        =   7011
            _ExtentY        =   609
            LabelWidth      =   1470
            Caption         =   "Emisor"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Moneda_Pago 
            Height          =   345
            Left            =   120
            TabIndex        =   35
            Top             =   1350
            Visible         =   0   'False
            Width           =   3975
            _ExtentX        =   7011
            _ExtentY        =   609
            LabelWidth      =   1470
            Caption         =   "Moneda de Pago"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Plazo 
            Height          =   345
            Left            =   4170
            TabIndex        =   36
            Top             =   630
            Visible         =   0   'False
            Width           =   3060
            _ExtentX        =   5398
            _ExtentY        =   609
            LabelWidth      =   1470
            TextMinWidth    =   1000
            Caption         =   "D�as al Vcto."
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Alignment       =   1
         End
         Begin TrueDBList80.TDBCombo Cmb_Nemotecnico 
            Height          =   345
            Left            =   1620
            TabIndex        =   54
            Tag             =   "OBLI=S;CAPTION=Nemot�cnico"
            Top             =   270
            Width           =   2475
            _ExtentX        =   4366
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Nac.frx":0654
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label lbl_nemotecnico 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Nemot�nico"
            Height          =   315
            Left            =   120
            TabIndex        =   49
            Top             =   270
            Visible         =   0   'False
            Width           =   1485
         End
         Begin VB.Label lbl_emisor_especifico 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Emisor"
            Height          =   345
            Left            =   120
            TabIndex        =   42
            Top             =   630
            Width           =   1470
         End
         Begin VB.Label lbl_fecha_ven 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Vencimiento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4170
            TabIndex        =   41
            Top             =   270
            Width           =   1470
         End
         Begin VB.Label lbl_base 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Base en d�as"
            Height          =   345
            Left            =   4170
            TabIndex        =   40
            Top             =   990
            Width           =   1470
         End
         Begin VB.Label lbl_moneda_Pactos 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Moneda Dep�sito"
            Height          =   345
            Left            =   120
            TabIndex        =   39
            Top             =   990
            Width           =   1470
         End
         Begin VB.Label Lbl_Moneda_Pago 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Moneda de Pago"
            Height          =   345
            Left            =   120
            TabIndex        =   38
            Top             =   1350
            Visible         =   0   'False
            Width           =   1470
         End
         Begin VB.Label lbl_plazo 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "D�as al Vcto."
            Height          =   345
            Left            =   4170
            TabIndex        =   37
            Top             =   630
            Width           =   1470
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   44
      Top             =   0
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   635
      ButtonWidth     =   1588
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
            Object.Width           =   500
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   45
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Operacion_Pactos_Nac"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
'-------------------------------------
Rem PARA CONFIRMACION
Dim fOperaciones As Class_Operaciones
Dim fId_Operacion As String
Dim fForm_Confirmacion As Boolean
'------------------------------------
Rem PARA CONSULTA DE OPERACION
Dim fConsulta_Operacion As Boolean
'------------------------------------
Dim fSalir      As Boolean
Dim fEstadoOK   As Boolean
Dim fOperacion  As String
Dim fId_Cuenta  As String
Dim fId_Cliente As Double
Dim fTipo_Operacion As String
Dim fId_Nemotecnico As String
Dim fValor_Iva As Double
Dim fFecha_Operacion As Date
Dim fDerechosBolsa As Double
Dim lcSubfamilia As Class_SubFamilias
Dim lId_Subfamilia As String
Dim fMonto_Operacion As String
Dim lId_Moneda_Peso

Const fc_Mercado_Transaccion = "15" ' Corresponde a Santiago
Const fc_Mercado = "N"
Const fc_Tipo_Interes = 1 ' esto va en duro => como el inter�s es de tipo simple es 1 (si es compuesto es 2)
Const fc_Id_Sector = 3 'en duro => financiero
Const fc_Corte_Minimo_Pac = 1 'pactos no tiene corte minimo, para no tener problemas va a ser 1.

Private Enum eNem_Colum
  eNem_nemotecnico
  eNem_Descripcion
  eNem_Id_Nemotecnico
  eNem_Id_Mov_Activo
End Enum

Private Enum eTipo
  eT_Normal
  eT_Grande
End Enum

'Esta variable contiene el monto valorizado a la tasa nominal
Dim fMonto_Nominal As Double
'Este es la variable que contiene el codigo del instrumento que fue
'iniciado para operar
Dim fCod_Instrumento As String

Private Sub Cmb_Contraparte_ItemChange()
Dim lId_Contraparte As String
  
  lId_Contraparte = Fnt_FindValue4Display(Cmb_Contraparte, Cmb_Contraparte.Text)
  
  If lId_Contraparte = "" Then
    'como nunca va a existir la contraparte -1 se pasa el parametro
    lId_Contraparte = "-1"
  End If
  
  Call Sub_CargaCombo_Traders(Cmb_Traders, lId_Contraparte)
End Sub

Private Sub Chk_Vende_Todo_Click()
  If Chk_Vende_Todo.Value Then
    Rem ---------------------------------------------
    Txt_Cantidad.Locked = True
    Txt_Cantidad.Text = Txt_Cantidad.Tag
    Rem ---------------------------------------------
    Txt_MontoOperacion.Locked = True
    Rem ---------------------------------------------
    Call Sub_ValorizaPapel
  Else
    Rem ---------------------------------------------
    Txt_Cantidad.Locked = False
    Txt_Cantidad.Text = 0
    Rem ---------------------------------------------
    Txt_MontoOperacion.Locked = False
    Txt_MontoOperacion.Text = 0
  End If
End Sub

Private Sub Cmb_Base_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", "-" y "backspace"
  If Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Cmb_Emisor_Especifico_ItemChange()
  Call Sub_Genera_Nemotecnico_SVS
End Sub

Private Sub Cmb_FechaLiquidacion_Change()
  If Not Cmb_FechaLiquidacion.Text = "" Then
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Cmb_FechaLiquidacion.Text)
  End If
End Sub

Private Sub Cmb_FechaLiquidacion_ItemChange()
  If Not Cmb_FechaLiquidacion.Text = "" Then
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Cmb_FechaLiquidacion.Text)
  End If
End Sub

Private Sub Cmb_FechaLiquidacion_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", "-" y "backspace"
  If Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Cmb_Moneda_Pactos_ItemChange()
  Call Sub_Genera_Nemotecnico_SVS
'  Txt_MontoOperacion.Format = Fnt_Formato_Moneda(Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos))
  Txt_Cantidad.Format = Fnt_Formato_Moneda(Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos))
  If lId_Moneda_Peso = Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos) Then
  'If Cmb_Moneda_Pactos = "PESOS" Then
    Cmb_Base.Text = "30"
  Else
    Cmb_Base.Text = "360"
  End If
End Sub

Private Sub Cmb_Moneda_Pago_ItemChange()
'  Call Sub_Genera_Nemotecnico_SVS
End Sub

Private Sub Cmb_Nemotecnico_LostFocus()
Dim lId_Nemotecnico As String
Dim lId_Mov_Activo As String


  lId_Nemotecnico = Fnt_FindValue4Display(Cmb_Nemotecnico, Cmb_Nemotecnico.Text) ', eNem_Id_Nemotecnico)

  If Not lId_Nemotecnico = "" Then
    lId_Nemotecnico = Cmb_Nemotecnico.Columns(eNem_Id_Nemotecnico).Text
    lId_Mov_Activo = Cmb_Nemotecnico.Columns(eNem_Id_Mov_Activo).Text
    Call Sub_Llena_Nemotecnico(lId_Nemotecnico, lId_Mov_Activo)
  End If
End Sub

Private Sub Sub_Llena_Nemotecnico(pId_Nemotecnico As String, pId_Mov_Activo As String)
Dim lcNemotecnico As Class_Nemotecnicos
Dim lReg As hFields
Dim lId_Emisor_Especifico As String
Dim lId_Moneda_Deposito As String
Dim lId_Moneda_Pago As String
Dim lcPrecio As Class_Publicadores_Precio
Dim lcDepositos As Class_Depositos
Dim lSaldo_Cantidad As Double
Dim lcMov_Activos As Class_Mov_Activos
Dim lId_Operacion_detalle As String
Dim lcOperacion_Detalle As Class_Operaciones_Detalle

  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      For Each lReg In .Cursor
        
        lId_Emisor_Especifico = NVL(lReg("id_emisor_especifico").Value, "")
        If Not lId_Emisor_Especifico = "" Then
          Call Sub_ComboSelectedItem(Cmb_Emisor_Especifico, lId_Emisor_Especifico)
          Txt_Emisor_Especifico.Text = Cmb_Emisor_Especifico.Text
        End If
        
        lId_Moneda_Deposito = NVL(lReg("id_moneda").Value, "")
        If Not lId_Moneda_Deposito = "" Then
          Call Sub_ComboSelectedItem(Cmb_Moneda_Pactos, lId_Moneda_Deposito)
          Call Cmb_Moneda_Pactos_ItemChange
          Txt_Moneda_Pactos.Text = Cmb_Moneda_Pactos.Text
        End If
        
        lId_Moneda_Pago = NVL(lReg("id_moneda_transaccion").Value, "")
        If Not lId_Moneda_Pago = "" Then
          Call Sub_ComboSelectedItem(Cmb_Moneda_Pago, lId_Moneda_Pago)
          Txt_Moneda_Pago.Text = Cmb_Moneda_Pago.Text
        End If
        
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en cargar datos del Nemot�cnico.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcNemotecnico = Nothing
  
  Set lcPrecio = New Class_Publicadores_Precio
  With lcPrecio
    .Campo("Id_Nemotecnico").Valor = pId_Nemotecnico
    .Campo("fecha").Valor = fFecha_Operacion
    If .Buscar_Ultimo_Tasa_Cta_Nemo(fId_Cuenta) Then
      Txt_TasaNominal.Tag = NVL(.Campo("tasa").Valor, 0)
      Txt_TasaNominal.Text = NVL(.Campo("tasa").Valor, 0)
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en cargar datos de Publicadores Precio.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcPrecio = Nothing
  
  Set lcDepositos = New Class_Depositos
  With lcDepositos
    lSaldo_Cantidad = .Saldo_Activo_Cantidad(fId_Cuenta, pId_Nemotecnico, pId_Mov_Activo)
    Txt_Cantidad.Text = lSaldo_Cantidad
  End With
  Set lcDepositos = Nothing
  
  '-------------------------------------------------------------------------
  Rem Busca el plazo de la operacion para valorizar
  Set lcMov_Activos = New Class_Mov_Activos
  With lcMov_Activos
    .Campo("id_mov_activo").Valor = pId_Mov_Activo
    If .Buscar Then
      If .Cursor.Count > 0 Then
        lId_Operacion_detalle = NVL(.Cursor(1)("id_operacion_detalle").Value, "")
      End If
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en cargar el Detalle de la Operaci�n.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcMov_Activos = Nothing
  
  If lId_Operacion_detalle Then
    Set lcOperacion_Detalle = New Class_Operaciones_Detalle
    With lcOperacion_Detalle
      .Campo("id_operacion_detalle").Valor = lId_Operacion_detalle
      If .Buscar Then
        If .Cursor.Count > 0 Then
          Cmb_Plazo.Text = .Cursor(1)("plazo").Value
        End If
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Error en cargar Plazo de la Operaci�n para Valorizar.", _
                          .ErrMsg, _
                          pConLog:=True)
      End If
    End With
    Set lcOperacion_Detalle = Nothing
  End If
  '-------------------------------------------------------------------------
  Call Sub_ValorizaPapel
End Sub
Private Sub Cmb_Plazo_ItemChange()
  If Not Cmb_Plazo.Text = "" Then
    Dtp_Fecha_Vencimiento.Value = Format(DateAdd("d", To_Number(Cmb_Plazo.Text), Format(fFecha_Operacion, cFormatDate)), cFormatDate)
    Dtp_Fecha_Valuta.MinDate = Dtp_Fecha_Vencimiento.Value
    Call Sub_Genera_Nemotecnico_SVS
  End If
End Sub
Private Sub DTP_Fecha_Operacion_Change()
    fFecha_Operacion = DTP_Fecha_Operacion.Value
    Dtp_Fecha_Vencimiento.MinDate = DTP_Fecha_Operacion.Value
    Dtp_Fecha_Vencimiento.Value = Format(fFecha_Operacion + 30, cFormatDate)
    Dtp_Fecha_Valuta.MinDate = fFecha_Operacion
    Dtp_Fecha_Valuta.Value = fFecha_Operacion
    Dtp_FechaLiquidacion.MinDate = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Cmb_FechaLiquidacion.Text)
    Call Sub_Genera_Nemotecnico_SVS
End Sub

Private Sub Dtp_FechaLiquidacion_Change()
  Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(Dtp_FechaLiquidacion.Value)
  Cmb_FechaLiquidacion.Text = Fnt_DiasHabiles_EntreFechas(fFecha_Operacion, Dtp_FechaLiquidacion.Value)
End Sub

Private Sub Form_Initialize()
    fFecha_Operacion = Fnt_FechaServidor
End Sub

Private Sub Grilla_DblClick()
  If Not fConsulta_Operacion Then
    If Grilla.Row > 0 Then
      Call Sub_EsperaVentana(GetCell(Grilla, Grilla.Row, "colum_pk"), _
                             GetCell(Grilla, Grilla.Row, "DSC_CLASIFICADOR_RIESGO"), _
                             GetCell(Grilla, Grilla.Row, "COD_VALOR_CLASIFICACION"), _
                             "U", _
                             Grilla.Row)
    End If
  End If
End Sub

Private Sub Toolbar_Clasificadores_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agregar
    Case "DEL"
      If Grilla.Row > 0 Then
        If MsgBox("�Esta seguro de eliminar el Clasificador de Riesgo?", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
          Grilla.RemoveItem (Grilla.Row)
        End If
      End If
  End Select
End Sub

Private Sub Sub_Agregar()
  Call Sub_EsperaVentana(0, "", "", "I")
End Sub

Private Sub Sub_EsperaVentana(pId_Clasificador_Riesgo As Double, _
                              pDsc_Clasificador_Riesgo As String, _
                              pValor_Clasificador As String, _
                              pTipo As String, _
                              Optional pLineaGrilla As Long)

Dim lForm As Frm_ClasifRiesgoNemotecnico
Dim lNombre As String
Dim lNom_Form As String
Dim lNewpId_Clasificador_Riesgo As Double
Dim lNewpDsc_Clasificador_Riesgo As String
Dim lNewValor_Clasificador As String
Dim lLinea As Long
Dim lExiste_Clasificador As Boolean
Dim lFila As Long
Dim lCod_Instrumento As String
  
  Me.Enabled = False
  lExiste_Clasificador = False
  lNom_Form = "Frm_ClasifRiesgoNemotecnico"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pId_Clasificador_Riesgo) Then
    lNombre = Me.Name
    lCod_Instrumento = gcINST_PACTOS_NAC  'Fnt_ComboSelected_KEY(Cmb_Instrumento)
    
    Set lForm = New Frm_ClasifRiesgoNemotecnico
    Call lForm.Fnt_Modificar(pId_Clasificador_Riesgo _
                           , pDsc_Clasificador_Riesgo _
                           , pValor_Clasificador _
                           , lCod_Instrumento _
                           , lNewpId_Clasificador_Riesgo _
                           , lNewpDsc_Clasificador_Riesgo _
                           , lNewValor_Clasificador _
                           , pCod_Arbol_Sistema:="")
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pId_Clasificador_Riesgo)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      If Not lNewpId_Clasificador_Riesgo = 0 Then
        
        If pTipo = "I" Then
          If Grilla.Rows > 1 Then
            For lFila = 1 To Grilla.Rows - 1
              If lNewpId_Clasificador_Riesgo = GetCell(Grilla, lFila, "colum_pk") Then
                lExiste_Clasificador = True
                Exit For
              End If
            Next
          End If
          If lExiste_Clasificador Then
            MsgBox "El Nemot�cnico no puede tener dos Valores de Clasificaci�n de Riesgo del tipo '" & _
                   GetCell(Grilla, lFila, "DSC_CLASIFICADOR_RIESGO") & "'.", vbExclamation, Me.Caption
          Else
            lLinea = Grilla.Rows
            Grilla.AddItem ""
            SetCell Grilla, lLinea, "colum_pk", lNewpId_Clasificador_Riesgo
            SetCell Grilla, lLinea, "DSC_CLASIFICADOR_RIESGO", lNewpDsc_Clasificador_Riesgo
            SetCell Grilla, lLinea, "COD_VALOR_CLASIFICACION", lNewValor_Clasificador
          End If
        ElseIf pTipo = "U" Then
          SetCell Grilla, pLineaGrilla, "colum_pk", lNewpId_Clasificador_Riesgo
          SetCell Grilla, pLineaGrilla, "DSC_CLASIFICADOR_RIESGO", lNewpDsc_Clasificador_Riesgo
          SetCell Grilla, pLineaGrilla, "COD_VALOR_CLASIFICACION", lNewValor_Clasificador
        End If
      End If
    End If
  End If
  Me.Enabled = True
End Sub

Private Sub Txt_Derechos_LostFocus()
  If Not fMonto_Operacion = "" Then
    Txt_Derechos.Text = Int((fMonto_Operacion * Txt_Porcentaje_Derechos.Text) / 100)
    Call Txt_Iva_LostFocus
  
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub

Private Sub Txt_Gastos_LostFocus()
  If Not fMonto_Operacion = "" Then
    Call Txt_Iva_LostFocus
    
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub

Private Sub Txt_MontoOperacion_LostFocus()
  fMonto_Operacion = Txt_MontoOperacion.Text
  Txt_Comision.Text = Int((fMonto_Operacion * Txt_Porcentaje_Comision.Text) / 100)
  Txt_Derechos.Text = Int((fMonto_Operacion * Txt_Porcentaje_Derechos.Text) / 100)
  Txt_Iva.Text = Int((Int(Txt_Comision.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)) * fValor_Iva)
End Sub

Private Sub Txt_Porcentaje_Comision_LostFocus()
  If Not fMonto_Operacion = "" Then
    Txt_Comision.Text = Int((fMonto_Operacion * Txt_Porcentaje_Comision.Text) / 100)
    Call Txt_Iva_LostFocus
    
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub

Private Sub Txt_Comision_LostFocus()
  If Not fMonto_Operacion = "" Then
    Txt_Comision.Text = Int((fMonto_Operacion * Txt_Porcentaje_Comision.Text) / 100)
    Call Txt_Iva_LostFocus
  
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub

Private Sub Txt_Iva_LostFocus()
  If Not fMonto_Operacion = "" Then
    Txt_Iva.Text = Int((Int(Txt_Comision.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)) * fValor_Iva)
  
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub
Private Sub Cmb_Plazo_Change()
  If Not Cmb_Plazo.Text = "" Then
    Dtp_Fecha_Vencimiento.Value = Format(DateAdd("d", To_Number(Cmb_Plazo.Text), Format(fFecha_Operacion, cFormatDate)), cFormatDate)
    Dtp_Fecha_Valuta.MinDate = Dtp_Fecha_Vencimiento.Value
    Call Sub_Genera_Nemotecnico_SVS
  End If
End Sub

Private Sub Cmb_Plazo_KeyPress(KeyAscii As Integer)
  If Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 57) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Dtp_Fecha_Vencimiento_Change()
  Cmb_Plazo.Text = To_Number(Dtp_Fecha_Vencimiento.Value) - To_Number(Format(fFecha_Operacion, cFormatDate))
  Dtp_Fecha_Valuta.MinDate = Dtp_Fecha_Vencimiento.Value
  Call Sub_Genera_Nemotecnico_SVS
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
'      .Buttons("REFRESH").Image = cBoton_Original
  End With

  With Toolbar_Valorizar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("VALORIZA").Image = "boton_valorizar"
      .Appearance = ccFlat
  End With
  
  With Toolbar_Clasificadores
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("ADD").Image = cBoton_Agregar_Grilla
    .Buttons("DEL").Image = cBoton_Eliminar_Grilla
    .Appearance = ccFlat
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
  
  Call Sub_Limpiar
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Rem PREGUNTA SI LA PANTALLA PADRE ES LA DE "CONFIRMACION DE INSTRUCCIONES"
      If fForm_Confirmacion Then
        If Fnt_Grabar_Confirmacion Then
          fEstadoOK = True
          fSalir = True
        End If
      Else
        If Fnt_Grabar Then
          'Si no hubo problemas al grabar, sale
          fEstadoOK = True
          fSalir = True
          'Unload Me
        End If
      End If
    Case "EXIT"
      fEstadoOK = False
      fSalir = True
      'Unload Me
  End Select
End Sub

Private Sub Sub_CargaForm()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lcIva As Class_Iva
Dim lcEmisores_Especifico As Class_Emisores_Especifico
Dim lReg As hFields
Dim lcComisiones As Class_Comisiones_Instrumentos
'---------------------------------------------------------------
Dim lcRel_Contrapartes_Instrum As Class_Rel_Contrapartes_Instrum
Dim lTexto As String
Dim lcMoneda As Object

  Call Sub_Bloquea_Puntero(Me)
  
  SSTab1.Tab = 0
  
  Call Sub_Carga_Datos_Cliente
  
  Grilla.Rows = 1
  
  Call Sub_Setea_Valores
  Call Sub_Setea_Comisiones
  
  '------------------------------------------------
  '-- Setea el color a mano para el combo de plazo
  Cmb_Plazo.BackColor = fColorOBligatorio
  '------------------------------------------------
  
  '------------------------------------------------
  '-- Carga combo de Emisores Especificos, que solo sean del sector Financiero
  With Cmb_Emisor_Especifico
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    '.SelectedItem = Null
    
    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
'      .Translate = True
    End With
    
    Set lcEmisores_Especifico = New Class_Emisores_Especifico
    If lcEmisores_Especifico.Buscar(True, fc_Id_Sector) Then
      
      For Each lReg In lcEmisores_Especifico.Cursor
        
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
        
        .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("ID_EMISOR_ESPECIFICO").Value, lReg("DSC_EMISOR_ESPECIFICO").Value)
        
        Call .AddItem(lReg("DSC_EMISOR_ESPECIFICO").Value)
      Next
      
    Else
      MsgBox lcEmisores_Especifico.ErrMsg, vbCritical, Me.Caption
    End If
    
  End With
  
  '---------------------------------------------------------------
  Call Sub_CargaCombo_Emisores_Especifico(Cmb_Emisor_Especifico)
  Call Sub_ComboSelectedItem(Cmb_Emisor_Especifico, 246)
  Call Sub_CargaCombo_Monedas(Cmb_Moneda_Pactos)
  Call Sub_CargaCombo_Monedas(Cmb_Moneda_Pago, , "S")
  Cmb_Moneda_Pactos.SelectedItem = 0
  Cmb_Moneda_Pago.SelectedItem = 0
  
  Rem Combo Plazo en d�as
'  With Cmb_Plazo
'    Set .SelectedItem = Nothing
'    .Text = ""
'    .ComboItems.Clear
'    .ComboItems.Add , Text:="30"
'    .ComboItems.Add , Text:="60"
'    .ComboItems.Add , Text:="90"
'  End With
  
  With Cmb_Plazo
    Call .AddItem("30")
    Call .AddItem("60")
    Call .AddItem("90")
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      '.Add Fnt_AgregaValueItem("", "")
      .Add Fnt_AgregaValueItem("30", "30")
      .Add Fnt_AgregaValueItem("60", "60")
      .Add Fnt_AgregaValueItem("90", "90")
      .Translate = True
    End With
    
    .Text = ""
    Cmb_Plazo.SelectedItem = 0
  End With
  
  Rem Combo Base en d�as
'  With Cmb_Base
'    Set .SelectedItem = Nothing
'    .Text = ""
'    .ComboItems.Clear
'    .ComboItems.Add , Text:="30"
'    .ComboItems.Add , Text:="360"
'  End With
  
  With Cmb_Base
    Call .AddItem("30")
    Call .AddItem("360")
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      '.Add Fnt_AgregaValueItem("", "")
      .Add Fnt_AgregaValueItem("30", "30")
      .Add Fnt_AgregaValueItem("360", "360")
      .Translate = True
    End With
    
    .Text = ""
    Cmb_Base.SelectedItem = 0
  End With
  
  '------------------------------------------------
  '-- Carga el ID del Cliente
  '------------------------------------------------
  
'  Set lcCuenta = New Class_Cuentas

  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  lcCuenta.Campo("id_cuenta").Valor = fId_Cuenta
  If lcCuenta.Buscar_Vigentes Then
    If lcCuenta.Cursor.Count > 0 Then
      fId_Cliente = lcCuenta.Cursor(1)("id_cliente").Value
    End If
  End If
  '------------------------------------------------

  '------------------------------------------------
  '-- Carga Contrapartes segun instrumento asociado
  '------------------------------------------------
  'Call Sub_CargaCombo_Contrapartes(Cmb_Contraparte)
  
  With Cmb_Contraparte
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    
    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
    End With
    
    Set lcRel_Contrapartes_Instrum = New Class_Rel_Contrapartes_Instrum
    lcRel_Contrapartes_Instrum.Campo("cod_instrumento").Valor = fCod_Instrumento
    If lcRel_Contrapartes_Instrum.BuscarView Then
      For Each lReg In lcRel_Contrapartes_Instrum.Cursor
        lTexto = ""
        
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
          
        lTexto = lReg("DSC_CONTRAPARTE").Value
          
        .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("ID_CONTRAPARTE").Value, lTexto)
          
        Call .AddItem(lTexto)
          
      Next
    End If
    Set lcRel_Contrapartes_Instrum = Nothing
  End With
  
  '------------------------------------------------
  '-- Carga el dias liquidacion
  '------------------------------------------------
'  With Cmb_FechaLiquidacion.ComboItems
'    .Clear
'    Call .Add(, "K" & "0", "0")
'    Call .Add(, "K" & "1", "1")
'    Call .Add(, "K" & "2", "2")
'    Call .Add(, "K" & "3", "3")
'    Call .Add(, "K" & "4", "4")
'    Call .Add(, "K" & "5", "5")
'  End With
    
  With Cmb_FechaLiquidacion
    Call .AddItem("0")
    Call .AddItem("1")
    Call .AddItem("2")
    Call .AddItem("3")
    Call .AddItem("4")
    Call .AddItem("5")
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem("0", "0")
      .Add Fnt_AgregaValueItem("1", "1")
      .Add Fnt_AgregaValueItem("2", "2")
      .Add Fnt_AgregaValueItem("3", "3")
      .Add Fnt_AgregaValueItem("4", "4")
      .Add Fnt_AgregaValueItem("5", "5")
      .Translate = True
    End With
    
    .Text = ""
    .SelectedItem = 0
  End With
    
  If Not fTipo_Operacion = gcOPERACION_Custodia Then
  
    Rem Valor Iva del Sistema
    Set lcIva = New Class_Iva
    With lcIva
      If .Buscar(True) Then
        fValor_Iva = .Cursor(1)("valor").Value
        Txt_Iva.Caption = " Iva (a " & .Porcentaje_Iva(fValor_Iva) & "%)"
      End If
    End With
    
    '------------------------------------------------
    Rem Comisiones
    Set lcComisiones = New Class_Comisiones_Instrumentos
    With lcComisiones
      .Campo("Id_Cuenta").Valor = fId_Cuenta
      .Campo("COD_INSTRUMENTO").Valor = fCod_Instrumento
      If .Buscar(True) Then
        If .Cursor.Count > 0 Then
          Txt_Porcentaje_Comision.Text = lcIva.Porcentaje_Iva(.Cursor(1)("COMISION").Value)
          Txt_Gastos.Text = Int(.Cursor(1)("GASTOS").Value)
          fDerechosBolsa = NVL(.Cursor(1)("DERECHOS_BOLSA").Value, 0)
        End If
      End If
    End With
    Set lcComisiones = Nothing
    Set lcIva = Nothing
  
  End If
  
  ' Si la operacion es custodia, muestra el Check de Aporte o Retiro
  If fTipo_Operacion = gcOPERACION_Custodia Then
    chkAporteRetiro.Value = 1
    chkAporteRetiro.Visible = True
  Else
    chkAporteRetiro.Value = 0
    chkAporteRetiro.Visible = False
  End If


  '---------------------------------------------
  ' CARGA ID_MONEDA PESOS
  '---------------------------------------------
    Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
    With lcMoneda
      .Campo("cod_Moneda").Valor = cMoneda_Cod_Peso
      If Not .Buscar Then
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Error al Buscar Moneda Pesos.", _
                          .ErrMsg, _
                          pConLog:=True)
      End If
      If .Cursor.Count <= 0 Then
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "No esta definido el codigo de moneda """ & cMoneda_Cod_Peso & """.""", _
                          .ErrMsg, _
                          pConLog:=True)
        
      End If
      lId_Moneda_Peso = .Cursor(1)("id_moneda").Value
    End With
    Set lcMoneda = Nothing

  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Carga_Datos_Cliente()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object

'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cuenta").Valor = fId_Cuenta
    If .Buscar_Vigentes Then
      If .Cursor.Count > 0 Then
        Txt_Rut.Text = "" & .Cursor(1)("rut_cliente").Value
        Txt_Nombres.Text = "" & .Cursor(1)("nombre_cliente").Value
        Txt_Num_Cuenta.Text = "" & .Cursor(1)("num_cuenta").Value
        Txt_Perfil.Text = "" & .Cursor(1)("dsc_perfil_riesgo").Value
      End If
    Else
      MsgBox "Problemas en cargar la Cuenta." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcCuenta = Nothing
  
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hFields
Dim lcSaldos_Activos As Class_Saldo_Activos
Dim lcTipo_Liq As Class_Tipos_Liquidacion

  Call Sub_Bloquea_Puntero(Me)
  
  '------------------------------------------------
  '-- Setea Fechas
  '------------------------------------------------
  fFecha_Operacion = Fnt_FechaServidor
  Txt_FechaIngreso_Real.Text = fFecha_Operacion
  DTP_Fecha_Operacion.Value = fFecha_Operacion
  
  If fTipo_Operacion = gcOPERACION_Custodia Then
    Dtp_FechaLiquidacion.Value = fFecha_Operacion
  Else
  
    'Dtp_FechaLiquidacion.MinDate = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
    
    Set lcTipo_Liq = New Class_Tipos_Liquidacion
    With lcTipo_Liq
      .Campo("cod_instrumento").Valor = fCod_Instrumento
      .Campo("id_empresa").Valor = Fnt_EmpresaActual
      .Campo("tipo_movimiento").Valor = fOperacion
      If .Buscar Then
        If .Cursor.Count > 0 Then
          Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, NVL(.Cursor(1).Fields("retencion").Value, 0))
          Cmb_FechaLiquidacion.Text = NVL(.Cursor(1).Fields("retencion").Value, 0)
        Else
          Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
        End If
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en carga de Tipo Liquidacion.", _
                          .ErrMsg, _
                          pConLog:=True)
        Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
      End If
    End With
    Set lcTipo_Liq = Nothing
    
  End If
  
  Rem Carga Nemotecnicos para venta de depositos
  If fOperacion = gcTipoOperacion_Egreso Then
    Call Sub_LimpiarTDBCombo(Cmb_Nemotecnico)
    With Cmb_Nemotecnico
      With .Columns.Add(eNem_nemotecnico)
        .Caption = "Nemot�cnico"
        .Visible = True
      End With
      With .Columns.Add(eNem_Descripcion)
        .Caption = "Descripci�n"
        .Visible = True
      End With
      With .Columns.Add(eNem_Id_Nemotecnico)
        .Caption = "id_nemotecnico"
        .Visible = False
      End With
      With .Columns.Add(eNem_Id_Mov_Activo)
        .Caption = "id_mov_Activo"
        .Visible = False
      End With
      
      Set lcSaldos_Activos = New Class_Saldo_Activos
      If lcSaldos_Activos.Buscar_Ultimo_CierreCuenta(pId_Cuenta:=fId_Cuenta, pCod_Instrumento:=fCod_Instrumento) Then
        For Each lReg In lcSaldos_Activos.Cursor
          Call .AddItem(lReg("nemotecnico").Value & ";" & lReg("dsc_nemotecnico").Value & ";" & lReg("id_nemotecnico").Value & ";" & lReg("id_mov_activo").Value)
          
          .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("id_nemotecnico").Value, lReg("nemotecnico").Value)
        Next
      Else
        Call Fnt_MsgError(lcSaldos_Activos.SubTipo_LOG, _
                          "Problemas en carga de de Saldos Activos.", _
                          lcSaldos_Activos.ErrMsg, _
                          pConLog:=True)
       End If
    End With
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Setea_Valores()
  Txt_TasaNominal.Text = 0
  Txt_Cantidad.Text = 0
  Txt_MontoOperacion.Text = 0
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Dim pMsgError
  'If Not Fnt_Form_Validar(Me.Controls, Frame_Principal) Then
  Dim lcCuenta As Object
    
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  lcCuenta.Campo("id_cuenta").Valor = fId_Cuenta
  If lcCuenta.Cuenta_Bloqueada(pMsgError) Then
    Select Case fTipo_Operacion
        Case gcOPERACION_Instruccion
            MsgBox "Cuenta Bloqueada. Motivo : " & pMsgError, vbExclamation, Me.Caption
            Fnt_ValidarDatos = False
            Exit Function
        Case gcOPERACION_Directa
            pMsgError = pMsgError & vbCr & vbCr & "�Desea continuar con la operaci�n?"
            If MsgBox("Cuenta Bloqueada. Motivo : " & pMsgError, vbQuestion + vbYesNo, Me.Caption) = vbNo Then
                Fnt_ValidarDatos = False
                Exit Function
            End If
    End Select
  End If
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If Fnt_Verifica_Feriado(fFecha_Operacion) Then
    Fnt_ValidarDatos = False
    MsgBox "Solo se pueden ingresar operaciones en d�as h�biles.", vbExclamation, Me.Caption
    Exit Function
  End If
  
  If Dtp_Fecha_Vencimiento.Value <= DTP_Fecha_Operacion.Value Then
    MsgBox "La Fecha de Vencimiento debe ser mayor a la Fecha de Proceso", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If Cmb_Plazo.Text = "" Then
    MsgBox "Debe ingresar D�as al Vencimiento.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  End If
    
  If Cmb_Base.Text = "" Then
    MsgBox "Debe ingresar Base en D�as.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  End If
    
  If Txt_TasaNominal.Text <= 0 Then
    MsgBox "Tasa Nominal no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
    If Txt_Cantidad.Text <= 0 Then
        MsgBox "Nominales no pueden ser menor a cero.", vbExclamation, Me.Caption
        Fnt_ValidarDatos = False
        Exit Function
    End If
  
  If Txt_MontoOperacion.Text <= 0 Then
    MsgBox "Monto Operacion no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If Txt_Porcentaje_Comision.Text < 0 Then
    MsgBox "Porcentaje Comisi�n no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Txt_Comision.Text < 0 Then
    MsgBox "Comisi�n a Cobrar no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Txt_Iva.Text < 0 Then
    MsgBox "El Valor Iva no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Txt_Gastos.Text < 0 Then
    MsgBox "Gastos no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Txt_Derechos.Text < 0 Then
    MsgBox "Derechos Gastos no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  End If
    
  Fnt_ValidarDatos = True
End Function

Public Function Mostrar(ByRef pFormOri As Form, _
                        pId_Cuenta As String, _
                        pOperacion As String, _
                        pTipo_Operacion As String, _
                        pCod_Instrumento As String, _
                        pNombreTipoOperacion As String, _
                        pOper_Fecha_Anterior As Boolean) As Boolean
  
  If Fnt_Verifica_Feriado(Fnt_FechaServidor) Then
    Mostrar = False
    MsgBox "Solo se pueden ingresar operaciones en d�as habiles.", vbExclamation, Me.Caption
    Unload Me
    Exit Function
  End If

  fId_Cuenta = pId_Cuenta
  fOperacion = pOperacion
  fTipo_Operacion = pTipo_Operacion
  fCod_Instrumento = pCod_Instrumento
  
  If Not pTipo_Operacion = gcOPERACION_Custodia Then
    Lbl_FechaLiquidacion.Visible = True
    Cmb_FechaLiquidacion.Visible = True
    Dtp_FechaLiquidacion.Visible = True
  Else
    Lbl_FechaLiquidacion.Visible = False
    Cmb_FechaLiquidacion.Visible = False
    Dtp_FechaLiquidacion.Visible = False
    
    Frame_Comisiones.Visible = False
    
    Frame_Datos_Documento.Height = Frame_Datos_Documento.Height - lbl_fecha_valuta.Height
    Frame_Datos_Documento.Width = 10875
    
    Frm_Datos_Compra.Height = Frm_Datos_Compra.Height - lbl_fecha_valuta.Height
    
    SSTab1.Height = SSTab1.Height - lbl_fecha_valuta.Height
    Me.Height = Me.Height - lbl_fecha_valuta.Height
    
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_Tasa_Historica.Visible = True
      
      Txt_Tasa_Historica.Top = 180
      Txt_TasaNominal.Top = 510
      Txt_Cantidad.Top = 840
      Toolbar_Valorizar.Top = 1200
      Txt_MontoOperacion.Top = 1560
    End If
  End If
  
  Select Case fOperacion
    Case gcTipoOperacion_Ingreso
      Me.Caption = "Compra de Pactos - " & pNombreTipoOperacion
      lbl_nemotecnico.Visible = False
      Cmb_Nemotecnico.Visible = False
      Cmb_Nemotecnico.Tag = ""
      '----------------------------------
      Txt_Nemotecnico.Visible = True
      Chk_Vende_Todo.Visible = False
      Chk_Vende_Todo.Value = False
    Case gcTipoOperacion_Egreso
      Me.Caption = "Venta de Pactos - " & pNombreTipoOperacion
      lbl_nemotecnico.Visible = True
      Cmb_Nemotecnico.Visible = True
      Txt_Nemotecnico.Visible = False
      '----------------------------------
      Cmb_Emisor_Especifico.Tag = ""
      Cmb_Moneda_Pactos.Tag = ""
      Cmb_Moneda_Pago.Tag = ""
      '------------------------------
      Txt_Emisor_Especifico.Visible = True
      lbl_emisor_especifico.Visible = False
      Cmb_Emisor_Especifico.Visible = False
      Cmb_Emisor_Especifico.Tag = ""
      '-------------------------------------
'      Txt_Moneda_Pago.Visible = True
      Lbl_Moneda_Pago.Visible = False
      Cmb_Moneda_Pago.Visible = False
      Cmb_Moneda_Pago.Tag = ""
      '-------------------------------------
      Txt_Moneda_Pactos.Visible = True
      lbl_moneda_Pactos.Visible = False
      Cmb_Moneda_Pactos.Visible = False
      Cmb_Moneda_Pactos.Tag = ""
      '-------------------------------------
      Chk_Vende_Todo.Visible = True
      Chk_Vende_Todo.Value = False
  End Select
  
  Call Sub_CargarDatos
  
  If pOper_Fecha_Anterior Then
    lbl_fecha_ingreso.Visible = True
    DTP_Fecha_Operacion.Visible = True
    Txt_FechaIngreso_Real.Visible = False
    DTP_Fecha_Operacion.MaxDate = fFecha_Operacion
  Else
    lbl_fecha_ingreso.Visible = False
    DTP_Fecha_Operacion.Visible = False
    Txt_FechaIngreso_Real.Visible = True
  End If
  
  fEstadoOK = False
  fSalir = False
  
  DoEvents
  Call Sub_Genera_Nemotecnico_SVS
  Txt_Cantidad.Format = Fnt_Formato_Moneda(Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos))
  Txt_MontoOperacion.Format = Fnt_Formato_Moneda(Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos))
    
  Me.Show
  
  Do While Not fSalir
    DoEvents
  Loop
  
  Mostrar = fEstadoOK
  
  Unload Me
End Function

Private Sub Form_Unload(Cancel As Integer)
  fSalir = True
End Sub

Private Sub Sub_Limpiar()

  Txt_Num_Operacion.Text = ""
  Txt_FechaIngreso_Real.Text = ""
  
  Cmb_Emisor_Especifico.Text = ""
  Txt_Emisor_Especifico.Text = ""
  
  Cmb_Contraparte.Text = ""
  Cmb_Traders.Text = ""
  '-------------------------------------------------------------
  Txt_MontoOperacion.Text = "0"
  
  Cmb_Moneda_Pago.Text = ""
  Txt_Moneda_Pago.Text = ""
  
  Txt_Cantidad.Text = "0"
  
  Txt_TasaNominal.Text = "0"
  
  Txt_Nemotecnico.Text = ""
  '-------------------------------------------------------------
  Cmb_Moneda_Pactos.Text = ""
  Txt_Moneda_Pactos.Text = ""
  
  Dtp_Fecha_Vencimiento.MinDate = Format(fFecha_Operacion, cFormatDate)
  Dtp_Fecha_Vencimiento.Value = Format(fFecha_Operacion + 30, cFormatDate)
  Txt_Fecha_Vencimiento.Text = ""
  
  Dtp_Fecha_Valuta.MinDate = Format(fFecha_Operacion, cFormatDate)
  Dtp_Fecha_Valuta.Value = Format(fFecha_Operacion, cFormatDate)
  
  Cmb_Plazo.Text = ""
  Txt_Plazo.Text = ""
  
  Cmb_Base.Text = ""
  Txt_Base.Text = ""
  
End Sub

Private Sub Sub_Setea_Comisiones()
  Txt_Porcentaje_Comision.Text = 0
  Txt_Comision.Text = 0
  Txt_Iva.Text = 0
  Txt_Gastos.Text = 0
  Txt_Derechos.Text = 0
End Sub

Private Function Fnt_Grabar() As Boolean
    Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc
    Dim lNemotecnico        As Class_Nemotecnicos
    '-------------------------------------------------------
    Dim lId_Moneda_Pago As String
    Dim lPactos As Class_Pactos
    Dim lId_Nemotecnico As String
    Dim lId_Emisor_Especifico As String
    Dim lPlazo As String
    Dim lBase As String
    Dim lReferenciado As String
    Dim lTipo_Deposito As String
    Dim lId_Contraparte As String
    Dim lFecha_Operacion  As Date
    Dim lFecha_Vigencia As Date
    Dim lFecha_Liquidacion As Date
    Dim lId_Moneda_Deposito As String
    Dim lFlg_Vende_Todo As String
    Dim lTasa_Historico As String
    Dim lId_Trader As String
    Dim lId_Mov_Activo As String
    '---------------------------------
    Dim lId_Caja_Cuenta As Double
    Dim lNum_Error      As Double
    '---------------------------------
    Dim lRollback As Boolean
    '------------------------------------------
    Dim sCC As String   'agregado por MMA 12/11/2008
    Dim sChkAporteRetiro    As String
    sChkAporteRetiro = IIf(chkAporteRetiro.Value = 1, "SI", "NO")
    

    Call Sub_Bloquea_Puntero(Me)
    Me.Enabled = False
    
    gDB.IniciarTransaccion
    lRollback = True
  
    If Not Fnt_ValidarDatos Then
        GoTo ErrProcedure
    End If

    lId_Mov_Activo = ""
    lId_Moneda_Pago = Fnt_ComboSelected_KEY(Cmb_Moneda_Pago)
    lId_Emisor_Especifico = Fnt_ComboSelected_KEY(Cmb_Emisor_Especifico)
    lId_Moneda_Deposito = Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos)
  
  If fOperacion = gcTipoOperacion_Ingreso Then
    
    Select Case fTipo_Operacion
      Case gcOPERACION_Instruccion, gcOPERACION_Directa
        lId_Caja_Cuenta = Fnt_CheckeaFinanciamiento(pId_Cuenta:=fId_Cuenta _
                                                   , pCod_Mercado:=fc_Mercado _
                                                   , pMonto:=To_Number(Txt_MontoOperacion.Text) _
                                                   , pId_Moneda:=lId_Moneda_Pago _
                                                   , pFecha_Liquidacion:=Dtp_FechaLiquidacion.Value _
                                                   , pNum_Error:=lNum_Error)
                                                   
        'VERITICA EL RESULTADO DE LA OPERACION
        Select Case lNum_Error
          Case 0, eFinanciamiento_Caja.eFC_InversionDescubierta
            'SI SON ESTOS VALORES SIGNIFICA QUE LA OPERACION SE PUEDE REALIZAR
          Case Else
            'Si el financiamiento tubo problemas
            GoTo ErrProcedure
        End Select
      Case gcOPERACION_Custodia, gcOPERACION_Custodia_NoCapital
        lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, lId_Moneda_Pago)
        If lId_Caja_Cuenta = cNewEntidad Then
          'Si el financiamiento tubo problemas
          GoTo ErrProcedure
        End If
    End Select
    
    Rem Busca que el nemotecnicos exista en el sistema
    Set lNemotecnico = New Class_Nemotecnicos
    With lNemotecnico
      .Campo("cod_instrumento").Valor = gcINST_PACTOS_NAC
      .Campo("nemotecnico").Valor = Txt_Nemotecnico.Text
      If .Buscar_Nemotecnico(gcPROD_RF_NAC) Then
        lId_Nemotecnico = .Campo("id_nemotecnico").Valor
        
        Rem Si lId_Nemotecnico = "0" se crea el nemotecnico en el sistema (no existe ;))
        If lId_Nemotecnico = "0" Then
          .LimpiaParam
          .Campo("id_Nemotecnico").Valor = cNewEntidad
          .Campo("cod_Instrumento").Valor = gcINST_PACTOS_NAC
          .Campo("id_subfamilia").Valor = lId_Subfamilia
          .Campo("nemotecnico").Valor = Txt_Nemotecnico.Text
          .Campo("id_Mercado_Transaccion").Valor = fc_Mercado_Transaccion
          .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
          .Campo("id_Emisor_Especifico_Origen").Valor = lId_Emisor_Especifico
          .Campo("id_Moneda").Valor = lId_Moneda_Deposito
          .Campo("id_Moneda_transaccion").Valor = lId_Moneda_Pago
          .Campo("id_Tipo_Estado").Valor = cTEstado_Nemotecnico
          .Campo("cod_Estado").Valor = cCod_Estado_Vigente
          .Campo("dsc_Nemotecnico").Valor = Txt_Nemotecnico.Text
          .Campo("tasa_Emision").Valor = To_Number(Txt_TasaNominal.Text)
          .Campo("tipo_Tasa").Valor = ""
          .Campo("periodicidad").Valor = ""
          .Campo("fecha_Vencimiento").Valor = Dtp_Fecha_Vencimiento.Value
          .Campo("corte_Minimo_Papel").Valor = fc_Corte_Minimo_Pac
          .Campo("monto_Emision").Valor = ""
          .Campo("liquidez").Valor = ""
          .Campo("base").Valor = To_Number(Cmb_Base.Text)
          .Campo("cod_Pais").Valor = gcPais_Chile
          .Campo("flg_Fungible").Valor = ""
          .Campo("fecha_emision").Valor = fFecha_Operacion
          If Not .Guardar Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en grabar el Nemot�cnico.", _
                              .ErrMsg, _
                              pConLog:=True)
            GoTo ErrProcedure
          End If
          lId_Nemotecnico = .Campo("id_Nemotecnico").Valor
          'Fnt_Grabar = Sub_Grabar_Clasificadores_Riesgo(lId_Nemotecnico)
        End If
      Else
        GoTo ErrProcedure
      End If
    End With
  
  Else
    Rem Si es una venta
    lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, lId_Moneda_Pago)
    If lId_Caja_Cuenta = -1 Then
      Rem Significa que hubo problema con la busqueda de la caja
      GoTo ErrProcedure
    End If
    lId_Nemotecnico = Cmb_Nemotecnico.Columns(eNem_Id_Nemotecnico).Text
    lId_Mov_Activo = Cmb_Nemotecnico.Columns(eNem_Id_Mov_Activo).Text
  End If
  Rem 22/09/2009 MMardones. El control de restricci�n es s�lo para Ingresos
  If fTipo_Operacion = gcOPERACION_Instruccion And fOperacion = gcTipoOperacion_Ingreso Then

    Rem Valida la Restricci�n Porcentual del Perfil de Riesgo de Instrumentos y Productos
    Set lcRestricc_Rel_Porc = New Class_Restricciones_Rel_Porc
    With lcRestricc_Rel_Porc
      If Not .Fnt_Restriccion_Rel_Porc(pId_Cuenta:=fId_Cuenta, _
                                       pId_Nemotecnico:=lId_Nemotecnico, _
                                       pMonto:=To_Number(Txt_MontoOperacion.Text), _
                                       pId_Moneda:=lId_Moneda_Pago, _
                                       pFecha_Operacion:=DTP_Fecha_Operacion.Value) Then
        GoTo ErrProcedure
      End If
    End With
    Set lcRestricc_Rel_Porc = Nothing
  End If
  
  Rem Si es referenciado al inicio => I (inicio), sino F (final)
  lReferenciado = IIf(Opt_Inicio.Value, "I", "F")
  
  Rem Vende todo
  lFlg_Vende_Todo = IIf(Chk_Vende_Todo.Value, cFlg_Vende_Todo, cFlg_No_Vende_Todo)
  
  Rem Tasa Historica solo para custodia y que sea compra de pacto
  If fTipo_Operacion = gcOPERACION_Custodia And fOperacion = gcTipoOperacion_Ingreso Then
    lTasa_Historico = Txt_Tasa_Historica.Text
  Else
    lTasa_Historico = ""
  End If
  
  Set lPactos = New Class_Pactos
  Rem Agrega el detalle de la operaci�n de pacto
  Call lPactos.Agregar_Operaciones_Detalle(pId_Nemotecnico:=lId_Nemotecnico, _
                                           pCantidad:=To_Number(Txt_Cantidad.Text), _
                                           pTasa:=To_Number(Txt_TasaNominal.Text), _
                                           PTasa_Gestion:="", _
                                           pPlazo:=To_Number(Cmb_Plazo.Text), _
                                           pBase:=To_Number(Cmb_Base.Text), _
                                           pFecha_Vencimiento:=Dtp_Fecha_Vencimiento.Value, _
                                           pId_Moneda_Pago:=lId_Moneda_Pago, _
                                           pMonto_Pago:=To_Number(Txt_MontoOperacion.Text), _
                                           pReferenciado:=lReferenciado, _
                                           pTipo_Deposito:=lTipo_Deposito, _
                                           pFecha_Valuta:=Dtp_Fecha_Valuta.Value, _
                                           pFlg_Vende_Todo:=lFlg_Vende_Todo, _
                                           pTasa_Historica:=lTasa_Historico, _
                                           pId_Mov_Activo_Compra:=lId_Mov_Activo)
  
  lId_Contraparte = Fnt_ComboSelected_KEY(Cmb_Contraparte)
  lId_Trader = Fnt_ComboSelected_KEY(Cmb_Traders)
  lFecha_Operacion = DTP_Fecha_Operacion.Value  ' Fnt_FechaServidor
  lFecha_Vigencia = lFecha_Operacion
  lFecha_Liquidacion = Dtp_FechaLiquidacion.Value
  'lTipo_Precio = "" 'Flg_Limite_Precio=> no va

  Select Case fTipo_Operacion
    Case gcOPERACION_Directa
      If Not lPactos.Realiza_Operacion_Directa(pId_Operacion:=fId_Operacion, _
                                               pId_Cuenta:=fId_Cuenta, _
                                               pDsc_Operacion:="", _
                                               pTipoOperacion:=fOperacion, _
                                               pId_Contraparte:=lId_Contraparte, _
                                               pId_Representante:="", _
                                               pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                               pFecha_Operacion:=lFecha_Operacion, _
                                               pFecha_Liquidacion:=lFecha_Liquidacion, _
                                               pId_Trader:=lId_Trader, _
                                               pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                               pComision:=Txt_Comision.Text, _
                                               pDerechos:=Txt_Derechos.Text, _
                                               pGastos:=Txt_Gastos.Text, _
                                               pIva:=Txt_Iva.Text, _
                                               pMonto_Operacion:=Txt_MontoOperacion.Text, _
                                               pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                               pId_Mov_Activo_Compra:=lId_Mov_Activo, _
                                               pChkAporteRetiro:=sChkAporteRetiro) Then
        Call Fnt_MsgError(lPactos.SubTipo_LOG, _
                          "Problemas en grabar el Pacto Nacional.", _
                          lPactos.ErrMsg, _
                          pConLog:=True)
        GoTo ErrProcedure
      End If
    Case gcOPERACION_Custodia
      If Not lPactos.Realiza_Operacion_Custodia(pId_Operacion:=fId_Operacion, _
                                                pId_Cuenta:=fId_Cuenta, _
                                                pDsc_Operacion:="", _
                                                pTipoOperacion:=fOperacion, _
                                                pId_Contraparte:=lId_Contraparte, _
                                                pId_Representante:="", _
                                                pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                                pFecha_Operacion:=lFecha_Operacion, _
                                                pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                pId_Trader:=lId_Trader, _
                                                pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                pComision:=Txt_Comision.Text, _
                                                pDerechos:=Txt_Derechos.Text, _
                                                pGastos:=Txt_Gastos.Text, _
                                                pIva:=Txt_Iva.Text, _
                                                pMonto_Operacion:=Txt_MontoOperacion.Text, _
                                                pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                                pId_Mov_Activo_Compra:=lId_Mov_Activo, _
                                               pChkAporteRetiro:=sChkAporteRetiro) Then
                                               
        Call Fnt_MsgError(lPactos.SubTipo_LOG, _
                          "Problemas en grabar el Pacto Nacional.", _
                          lPactos.ErrMsg, _
                          pConLog:=True)
        GoTo ErrProcedure
      End If
    Case gcOPERACION_Instruccion
      If Not lPactos.Realiza_Operacion_Instruccion(pId_Operacion:=fId_Operacion, _
                                                   pId_Cuenta:=fId_Cuenta, _
                                                   pDsc_Operacion:="", _
                                                   pTipoOperacion:=fOperacion, _
                                                   pId_Contraparte:=lId_Contraparte, _
                                                   pId_Representante:="", _
                                                   pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                                   pFecha_Operacion:=lFecha_Operacion, _
                                                   pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                   pId_Trader:=lId_Trader, _
                                                   pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                   pComision:=Txt_Comision.Text, _
                                                   pDerechos:=Txt_Derechos.Text, _
                                                   pGastos:=Txt_Gastos.Text, _
                                                   pIva:=Txt_Iva.Text, _
                                                   pMonto_Operacion:=Txt_MontoOperacion.Text, _
                                                   pId_Mov_Activo_Compra:=lId_Mov_Activo, _
                                                   pChkAporteRetiro:=sChkAporteRetiro) Then
                                                   
        Call Fnt_MsgError(lPactos.SubTipo_LOG, _
                          "Problemas en grabar el Pacto Nacional.", _
                          lPactos.ErrMsg, _
                          pConLog:=True)
        GoTo ErrProcedure
      End If
    Case Else
      MsgBox "Operacion no reconocida para operar.", vbCritical, Me.Caption
      GoTo ErrProcedure
  End Select

  lRollback = Not Fnt_Grabar_Clasificadores_Riesgo(lId_Nemotecnico)

ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    Fnt_Grabar = False
  Else
    gDB.CommitTransaccion
    Fnt_Grabar = True
  
    sCC = Fnt_Lee_Mail_BackOffice(gId_Empresa)
    Call Fnt_EnvioEMAIL_Trader(fId_Operacion, sCC)

    If sChkAporteRetiro = "SI" Then
        Frm_AporteRescate_Fechas_Anteriores.ImprimeDocWord fId_Operacion, pTipo:="O"
    End If
    
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Function

Private Function Fnt_Grabar_Clasificadores_Riesgo(pId_Nemotecnico As String) As Boolean
Dim lFila As Long
Dim lcRel_Nemo_Valor_Clasif As Class_Rel_Nemotec_Valor_Clasific

  Fnt_Grabar_Clasificadores_Riesgo = True
  
  Rem Valida que el instrumento tenga asociados los Clasificadores de Riesgo
  If Not Fnt_Valida_Clasif_Instrum Then
    GoTo ErrProcedure
  End If
  
  Rem Elimina todos los clasificadores de riesgo para el nemotecnico
  Set lcRel_Nemo_Valor_Clasif = New Class_Rel_Nemotec_Valor_Clasific
  With lcRel_Nemo_Valor_Clasif
    .Campo("Id_Nemotecnico").Valor = pId_Nemotecnico
    If Not .Borrar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en grabar el Pacto Nacional.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  Set lcRel_Nemo_Valor_Clasif = Nothing
    
  Rem Guarda todos los clasificador del nemotecnico
  If Fnt_Grabar_Clasificadores_Riesgo Then
    If Grilla.Rows > 0 Then
      For lFila = 1 To Grilla.Rows - 1
        Set lcRel_Nemo_Valor_Clasif = New Class_Rel_Nemotec_Valor_Clasific
        With lcRel_Nemo_Valor_Clasif
          .Campo("Id_Nemotecnico").Valor = pId_Nemotecnico
          'CAAL 09-2013 .Campo("Id_Clasificador_Riesgo").Valor = GetCell(Grilla, lFila, "colum_pk")
          .Campo("Cod_Valor_Clasificacion").Valor = GetCell(Grilla, lFila, "COD_VALOR_CLASIFICACION")
          If Not .Guardar Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en grabar el Pacto Nacional.", _
                        .ErrMsg, _
                        pConLog:=True)
            GoTo ErrProcedure
          End If
        End With
        Set lcRel_Nemo_Valor_Clasif = Nothing
      Next
    End If
  End If
  
  Exit Function
  
ErrProcedure:
  Fnt_Grabar_Clasificadores_Riesgo = False
  Set lcRel_Nemo_Valor_Clasif = Nothing
    
End Function

Private Function Fnt_Valida_Clasif_Instrum() As Boolean
Dim lFila As Long
Dim lReg As hFields
Dim lCod_Instrumento As String
Dim lExiste_Rel_Instrum_Clasif As Boolean
Dim lcClasificador_Riesgo As Class_Clasificadores_Riesgo
  
  Fnt_Valida_Clasif_Instrum = True
  lExiste_Rel_Instrum_Clasif = False
  With Grilla
    If .Rows > 1 Then
      lCod_Instrumento = gcINST_PACTOS_NAC  'Fnt_ComboSelected_KEY(Cmb_Instrumento)
      For lFila = 1 To .Rows - 1
        Set lcClasificador_Riesgo = New Class_Clasificadores_Riesgo
        With lcClasificador_Riesgo
          .Campo("Id_Clasificador_Riesgo").Valor = GetCell(Grilla, lFila, "colum_pk")
          If .Buscar_Instru_Clasif_Riesgo Then
            For Each lReg In .Cursor
              If lCod_Instrumento = lReg("COD_INSTRUMENTO").Value Then
                lExiste_Rel_Instrum_Clasif = True
                Exit For
              End If
            Next
            If Not lExiste_Rel_Instrum_Clasif Then
              MsgBox "El Clasificador de Riesgo '" & GetCell(Grilla, lFila, "DSC_CLASIFICADOR_RIESGO") & _
                     "' no est� asociado al Instrumento 'Pactos Nacionales'.", vbExclamation, Me.Caption
              Fnt_Valida_Clasif_Instrum = False
              Exit For
            End If
          Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en grabar el Pacto Nacional.", _
                              .ErrMsg, _
                              pConLog:=True)
            Fnt_Valida_Clasif_Instrum = False
            Exit For
          End If
        End With
        Set lcClasificador_Riesgo = Nothing
      Next
    End If
  End With

End Function

Rem FUNCION QUE ES LLAMADA DESDE LA PANTALLA DE CONFIRMACION DE INSTRUCCIONES
Public Function Confirmar(ByRef pFormOri As Form, _
                          ByVal pId_Operacion As String, _
                          ByVal pId_Cuenta As String, _
                          ByRef pMonto_Total As Double, _
                          ByVal pCod_Imstrumento As String) As Boolean

  fId_Cuenta = pId_Cuenta
  fId_Operacion = pId_Operacion
  fCod_Instrumento = pCod_Imstrumento
  
  Txt_Num_Operacion.Text = pId_Operacion
  Me.Caption = Me.Caption & " - Operaci�n N�: " & pId_Operacion
  
  lbl_fecha_ingreso.Visible = False
  DTP_Fecha_Operacion.Visible = False
  Txt_FechaIngreso_Real.Visible = True
  '------------------------------------
  Toolbar.Buttons(1).Caption = "Confirmar"
  Toolbar.Buttons(1).ToolTipText = "Confirma la operaci�n"
  '-------------------------------------
  Txt_Emisor_Especifico.Visible = True
  lbl_emisor_especifico.Visible = False
  Cmb_Emisor_Especifico.Visible = False
  Cmb_Emisor_Especifico.Tag = ""
  
  Chk_Vende_Todo.Visible = False
  '-------------------------------------
'  Txt_Moneda_Pago.Visible = True
  Lbl_Moneda_Pago.Visible = False
  Cmb_Moneda_Pago.Visible = False
  Cmb_Moneda_Pago.Tag = ""
  '-------------------------------------
  lbl_fecha_ven.Visible = False
  Dtp_Fecha_Vencimiento.Visible = False
  Txt_Fecha_Vencimiento.Visible = True
  '-------------------------------------
  Txt_Nemotecnico.BackColorTxt = fColorNoEdit
  Txt_Nemotecnico.Locked = True
  '-------------------------------------
  Txt_Moneda_Pactos.Visible = True
  lbl_moneda_Pactos.Visible = False
  Cmb_Moneda_Pactos.Visible = False
  Cmb_Moneda_Pactos.Tag = ""
  '-------------------------------------
  Txt_Plazo.Visible = True
  lbl_plazo.Visible = False
  Cmb_Plazo.Visible = False
  Cmb_Plazo.Tag = ""
  '-------------------------------------
  Txt_Base.Visible = True
  lbl_base.Visible = False
  Cmb_Base.Visible = False
  Cmb_Base.Tag = ""
  '-------------------------------------
  Cmb_Nemotecnico.Tag = ""
  '-------------------------------------
  Call Form_Resize
  
  Call Sub_CargarDatos_Confirmacion

  fForm_Confirmacion = True
  fEstadoOK = False
  fSalir = False

  Me.Show

  Do While Not fSalir
    DoEvents
  Loop

  Confirmar = fEstadoOK
  'pMonto_Total = To_Number(Txt_MontoOperacion.Text)

  Unload Me
End Function

Rem CARGA DATOS SEGUN EL NUMERO DE OPERACION PARA LA CONFIRMACION
Private Sub Sub_CargarDatos_Confirmacion()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lDetalle As Class_Operaciones_Detalle
Dim lId_Contraparte As String
Dim lNemotecnico As Class_Nemotecnicos
Dim lNombre_Nemotecnico As String
Dim lTipo_Precio As String
Dim lDsc_Emisor_Especifico As String
Dim lId_Moneda_Pago As String
Dim lId_Moneda_Operacion As String
Dim lBase As String

  Call Sub_Bloquea_Puntero(Me)
  
  Set fOperaciones = New Class_Operaciones
  fOperaciones.Campo("Id_Operacion").Valor = fId_Operacion

  If fOperaciones.BuscaConDetalles Then
    fOperacion = fOperaciones.Campo("Flg_Tipo_Movimiento").Valor
    fFecha_Operacion = fOperaciones.Campo("Fecha_Operacion").Valor
    
    Txt_FechaIngreso_Real.Text = fFecha_Operacion
    
    lId_Contraparte = NVL(fOperaciones.Campo("Id_Contraparte").Valor, "")
    If Not lId_Contraparte = "" Then
      Call Sub_ComboSelectedItem(Cmb_Contraparte, lId_Contraparte)
      Call Sub_CargaCombo_Traders(Cmb_Traders, lId_Contraparte)
      Call Sub_ComboSelectedItem(Cmb_Traders, "" & fOperaciones.Campo("Id_Trader").Valor)
    End If

    Txt_MontoOperacion.Text = NVL(fOperaciones.Campo("Monto_Operacion").Valor, "")
    fMonto_Operacion = NVL(fOperaciones.Campo("Monto_Operacion").Valor, "")
    
    Dtp_FechaLiquidacion.Value = fOperaciones.Campo("Fecha_Liquidacion").Valor
    Dtp_FechaLiquidacion.MinDate = fFecha_Operacion
    Cmb_FechaLiquidacion.Text = Fnt_DiasHabiles_EntreFechas(fFecha_Operacion, Dtp_FechaLiquidacion.Value)
    
    Rem Comisiones
    Txt_Porcentaje_Comision.Text = (NVL(fOperaciones.Campo("Porc_Comision").Valor, 0) * 100)
    Txt_Comision.Text = NVL(fOperaciones.Campo("Comision").Valor, 0)
    Txt_Iva.Text = NVL(fOperaciones.Campo("Iva").Valor, 0)
    Txt_Gastos.Text = NVL(fOperaciones.Campo("Gastos").Valor, 0)
    Txt_Derechos.Text = NVL(fOperaciones.Campo("Derechos").Valor, 0)
    Txt_Porcentaje_Derechos.Text = Fnt_Divide(Txt_Derechos.Text * 100, NVL(fOperaciones.Campo("Monto_Operacion").Valor, 0))
    
    Set lNemotecnico = New Class_Nemotecnicos

    For Each lDetalle In fOperaciones.Detalles

      With lNemotecnico
        fId_Nemotecnico = lDetalle.Campo("Id_Nemotecnico").Valor
        .Campo("id_nemotecnico").Valor = fId_Nemotecnico
        If .BuscarView Then
          For Each lReg In .Cursor
            lNombre_Nemotecnico = NVL(lReg("NEMOTECNICO").Value, "")
            lDsc_Emisor_Especifico = NVL(lReg("dsc_emisor_especifico").Value, "")
            lId_Moneda_Operacion = NVL(lReg("id_moneda").Value, "")
          Next
        End If
      End With
      
      Txt_Nemotecnico.Text = lNombre_Nemotecnico
      Txt_Emisor_Especifico.Text = lDsc_Emisor_Especifico
      
      If Not lId_Moneda_Operacion = "" Then
        Call Sub_ComboSelectedItem(Cmb_Moneda_Pactos, lId_Moneda_Operacion)
        Txt_Moneda_Pactos.Text = Cmb_Moneda_Pactos.Text
      End If
      
      lId_Moneda_Pago = NVL(lDetalle.Campo("Id_Moneda_Pago").Valor, "")
      If Not lId_Moneda_Pago = "" Then
        Call Sub_ComboSelectedItem(Cmb_Moneda_Pago, lId_Moneda_Pago)
        Txt_Moneda_Pago.Text = Cmb_Moneda_Pago.Text
      End If
      
      Txt_Cantidad.Text = NVL(lDetalle.Campo("Cantidad").Valor, "")
      Txt_TasaNominal.Text = NVL(lDetalle.Campo("Precio").Valor, "")
     ' Txt_TasaNominal.Text = NVL(lDetalle.Campo("tasa").Valor, "")
      
      If lDetalle.Campo("Flg_Monto_Referenciado").Valor = "I" Then
        Opt_Inicio.Value = True
        Opt_Final.Value = False
      ElseIf lDetalle.Campo("Flg_Monto_Referenciado").Valor = "F" Then
        Opt_Inicio.Value = False
        Opt_Final.Value = True
      End If
      
      Dtp_Fecha_Vencimiento.MinDate = NVL(lDetalle.Campo("Fecha_Vencimiento").Valor, Format(fFecha_Operacion, cFormatDate))
      Dtp_Fecha_Vencimiento.Value = NVL(lDetalle.Campo("Fecha_Vencimiento").Valor, Format(fFecha_Operacion, cFormatDate))
      Txt_Fecha_Vencimiento.Text = Dtp_Fecha_Vencimiento.Value
      
      Dtp_Fecha_Valuta.MinDate = NVL(lDetalle.Campo("Fecha_Valuta").Valor, Format(fFecha_Operacion, cFormatDate))
      Dtp_Fecha_Valuta.Value = NVL(lDetalle.Campo("Fecha_Valuta").Valor, Format(fFecha_Operacion, cFormatDate))
      
      Cmb_Plazo.Text = NVL(lDetalle.Campo("Plazo").Valor, "")
      Txt_Plazo.Text = Cmb_Plazo.Text
      
      Cmb_Base.Text = NVL(lDetalle.Campo("Base").Valor, "")
      Txt_Base.Text = Cmb_Base.Text
      
      Chk_Vende_Todo.Tag = NVL(lDetalle.Campo("flg_vende_todo").Valor, "")
      
      Txt_Tasa_Historica.Text = NVL(lDetalle.Campo("Precio_Historico_Compra").Valor, "")
      
      Call Sub_Carga_Clasificadores_Riesgo(fId_Nemotecnico)
    Next
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Carga_Clasificadores_Riesgo(pId_Nemotecnico As String)
Dim lReg As hFields
Dim lLinea As Long
Dim lClasificador_Riesgo As Class_Clasificadores_Riesgo
  
  Grilla.Rows = 1
  
  Set lClasificador_Riesgo = New Class_Clasificadores_Riesgo
  With lClasificador_Riesgo
    If .Buscar_Clasif_Riesgo_Nemo(pId_Nemotecnico) Then
      For Each lReg In .Cursor
        lLinea = Grilla.Rows
        Grilla.AddItem ""
        SetCell Grilla, lLinea, "colum_pk", lReg("ID_CLASIFICADOR_RIESGO").Value
        SetCell Grilla, lLinea, "DSC_CLASIFICADOR_RIESGO", lReg("DSC_CLASIFICADOR_RIESGO").Value, True
        SetCell Grilla, lLinea, "COD_VALOR_CLASIFICACION", lReg("COD_VALOR_CLASIFICACION").Value, True
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Clasificadores de Riesgo.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lClasificador_Riesgo = Nothing

End Sub

Private Function Fnt_Grabar_Confirmacion() As Boolean
Dim lId_Caja_Cuenta As Double
Dim lRollback As Boolean
Dim lDetalle As Class_Operaciones_Detalle
Dim lId_Contraparte As String
Dim lId_Moneda_Pago As String
Dim lReferenciado As String
Dim lTipo_Deposito As String
Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc
Dim lId_Trader As String


  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False

  gDB.IniciarTransaccion

  lRollback = True
  
  If Not Fnt_ValidarDatos Then
    GoTo ErrProcedure
  End If

  lId_Moneda_Pago = Fnt_ComboSelected_KEY(Cmb_Moneda_Pago)

  If fOperacion = gcTipoOperacion_Ingreso Then
    Rem Si es una compra
    lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, lId_Moneda_Pago)
    If lId_Caja_Cuenta = -1 Then
      Rem Si el financiamiento tuvo problemas
      GoTo ErrProcedure
    End If
  Else
    Rem Si es una venta
    lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, lId_Moneda_Pago)
    If lId_Caja_Cuenta < 0 Then
      Rem Significa que hubo problema con la busqueda de la caja
      GoTo ErrProcedure
    End If
  End If
  
  Rem Valida la Restricci�n Porcentual del Perfil de Riesgo de Instrumentos y Productos
  Set lcRestricc_Rel_Porc = New Class_Restricciones_Rel_Porc
  With lcRestricc_Rel_Porc
    If Not .Fnt_Restriccion_Rel_Porc(pId_Cuenta:=fId_Cuenta, _
                                     pId_Nemotecnico:=fId_Nemotecnico, _
                                     pMonto:=To_Number(Txt_MontoOperacion.Text), _
                                     pId_Moneda:=lId_Moneda_Pago, _
                                     pFecha_Operacion:=DTP_Fecha_Operacion.Value) Then
      GoTo ErrProcedure
    End If
  End With
  Set lcRestricc_Rel_Porc = Nothing
  
  lId_Contraparte = Fnt_ComboSelected_KEY(Cmb_Contraparte)
  lId_Trader = Fnt_ComboSelected_KEY(Cmb_Traders)
'  lFormaPago = Fnt_ComboSelected_KEY(Cmb_FormaPago)
'  lTipo_Precio = Fnt_ComboSelected_KEY(Cmb_TipoPrecio)
'  lFecha_Operacion = Fnt_FechaServidor
'  lFecha_Vigencia = lFecha_Operacion '+ To_Number(Txt_DiasVigencia.Text)

  Rem Si es referenciado al inicio => I (inicio), sino F (final)
  lReferenciado = IIf(Opt_Inicio.Value, "I", "F")
  
  With fOperaciones
'    .Campo("Id_Operacion").valor = fId_Operacion
'    .Campo("Id_Cuenta").valor = fId_Cuenta
'    .Campo("Cod_Tipo_Operacion").valor = gcOPERACION_Directa
'    .Campo("Cod_Estado").valor = "P"
    .Campo("Id_Contraparte").Valor = lId_Contraparte
'    .Campo("Id_Representante").Valor = lId_representante
'    .Campo("Id_Tipo_Liquidacion").Valor = lFormaPago
'    .campo("Id_Moneda_Operacion").Valor = lId_Moneda_Pago
'    .Campo("Cod_Producto").valor = gcPROD_FFMM_NAC 'Esto va en duro
'    .Campo("Cod_Instrumento").valor = fCod_Instrumento
'    .Campo("Flg_Tipo_Movimiento").valor = fOperacion
'    .Campo("Fecha_Operacion").valor = lFecha_Operacion
'    .campo("Fecha_Vigencia").valor = Txt_FechaVigencia.Text
    .Campo("Fecha_Liquidacion").Valor = Dtp_FechaLiquidacion.Value
'    .Campo("Dsc_Operacion").valor = pDsc_Operacion
    .Campo("Id_Trader").Valor = lId_Trader
    .Campo("Porc_Comision").Valor = (Txt_Porcentaje_Comision.Text / 100)
    .Campo("Comision").Valor = Txt_Comision.Text
    .Campo("Derechos").Valor = Txt_Derechos.Text
    .Campo("Gastos").Valor = Txt_Gastos.Text
    .Campo("Iva").Valor = Txt_Iva.Text
    .Campo("Monto_Operacion").Valor = To_Number(Txt_MontoOperacion.Text)
'    .campo("Flg_Limite_Precio").valor = lTipo_Precio

    For Each lDetalle In fOperaciones.Detalles
      'lDetalle.campo("Id_Nemotecnico").Valor = pId_Nemotecnico ' este se saca de nemotecnicos
      lDetalle.Campo("Cantidad").Valor = To_Number(Txt_Cantidad.Text)
      lDetalle.Campo("Precio").Valor = To_Number(Txt_TasaNominal.Text)
      'lDetalle.Campo("Precio_gestion").Valor = To_Number(Txt_TasaNominal.Text)
      'lDetalle.campo("Plazo").Valor = To_Number(Cmb_Plazo.Text)
      'lDetalle.campo("Base").Valor = To_Number(Cmb_Base.Text)
      'lDetalle.campo("Fecha_Vencimiento").Valor = Dtp_Fecha_Vencimiento.Value
      'lDetalle.campo("Id_Moneda_Pago").Valor = lId_Moneda_Pago
      'lDetalle.Campo("Valor_Divisa").Valor = pValor_Divisa
      lDetalle.Campo("Monto_Pago").Valor = To_Number(Txt_MontoOperacion.Text)
      lDetalle.Campo("Monto_Bruto").Valor = To_Number(Txt_MontoOperacion.Text)
      lDetalle.Campo("FLG_MONTO_REFERENCIADO").Valor = lReferenciado
      lDetalle.Campo("FLG_TIPO_DEPOSITO").Valor = lTipo_Deposito
      lDetalle.Campo("Fecha_Valuta").Valor = Dtp_Fecha_Valuta.Value
      lDetalle.Campo("Flg_Vende_Todo").Valor = Chk_Vende_Todo.Tag
    Next
    
    If Not .Guardar(pConfirmacion:=True) Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en la confirmaci�n de la operaci�n.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
    
    If Not .Confirmar(lId_Caja_Cuenta) Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en la confirmaci�n de la operaci�n.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With

  lRollback = Not Fnt_Grabar_Clasificadores_Riesgo(fId_Nemotecnico)
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    'MsgBox "Error en la confirmaci�n de la operaci�n.", vbCritical
    Fnt_Grabar_Confirmacion = False
  Else
    gDB.CommitTransaccion
    Fnt_Grabar_Confirmacion = True
  End If

  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Function

Private Sub Sub_Genera_Nemotecnico_SVS()
Dim lcPactos As Class_Pactos
Dim lId_Emisor_Especifico As String
Dim lId_Moneda_Deposito As String
Dim lId_Moneda_Pago As String

  If Not Cmb_Emisor_Especifico.Text = "" And _
     Not Cmb_Moneda_Pactos.Text = "" Then
    
    lId_Emisor_Especifico = Fnt_ComboSelected_KEY(Cmb_Emisor_Especifico)
    lId_Moneda_Deposito = Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos)
    lId_Moneda_Pago = Fnt_ComboSelected_KEY(Cmb_Moneda_Pago)
    
    Set lcPactos = New Class_Pactos
    Txt_Nemotecnico.Text = lcPactos.Fnt_Genera_Nemo_SVS_Pactos(lId_Emisor_Especifico, _
                                                               Dtp_Fecha_Vencimiento.Value, _
                                                               lId_Moneda_Deposito, _
                                                               lId_Moneda_Pago)
  End If
  
End Sub

Private Sub Txt_Porcentaje_Derechos_LostFocus()
  If Not fMonto_Operacion = "" Then
    Txt_Derechos.Text = Int((fMonto_Operacion * Txt_Porcentaje_Derechos.Text) / 100)
    Call Txt_Iva_LostFocus
  
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoOperacion.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub

Private Sub Toolbar_Valorizar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "VALORIZA"
      Call Sub_ValorizaPapel
  End Select
End Sub

Private Sub Sub_ValorizaPapel()
Dim lcPactos     As Class_Pactos
Dim lValorizacion   As Double
Dim lCantidad       As Double
Dim lId_Nemotecnico As String
Dim lTasa           As Double

  lCantidad = To_Number(Txt_Cantidad.Text)
  lTasa = To_Number(Txt_TasaNominal.Text)
  lId_Nemotecnico = Txt_Nemotecnico.Text
  Set lcPactos = New Class_Pactos
  lValorizacion = lcPactos.ValorizaPapel(lId_Nemotecnico, fFecha_Operacion, lTasa, lCantidad, 1)
  'Txt_MontoOperacion.Text = lValorizacion

  fMonto_Operacion = lValorizacion
  
  Txt_Comision.Text = Int((lValorizacion * Txt_Porcentaje_Comision.Text) / 100)
  Txt_Derechos.Text = Int((lValorizacion * Txt_Porcentaje_Derechos.Text) / 100)
  Txt_Iva.Text = Int((Int(Txt_Comision.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)) * fValor_Iva)
   
  If fOperacion = gcTipoOperacion_Ingreso Then
    Txt_MontoOperacion.Text = lValorizacion + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
  ElseIf fOperacion = gcTipoOperacion_Egreso Then
    Txt_MontoOperacion.Text = lValorizacion - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
  End If
    
'Dim lcValorizador As Object
'Dim lValorizacion As Double
'Dim lId_Nemotecnico As Double
'Dim lId_Valorizador As String
'Dim lMetodo As String
'Dim lcInstrumentos As Class_Instrumentos
'Dim lcValorizadores As Class_Valorizadores
'Dim lTipo_Calculo As Integer
'Dim lcMoneda As Class_Monedas
'Dim lCod_Moneda As String
'Dim lId_Moneda_Deposito As String
'Dim lVal_Nom_Ini As Double
'Dim lVal_Nom_Fin As Double
'Dim lCod_Subfamilia As String
'
'  Call Sub_Bloquea_Puntero(Me)
'
'  If Not Fnt_ValidaDatos_ValRF Then
'    GoTo ErrProcedure
'  End If
'
'  Set lcMoneda = New Class_Monedas
'  lId_Moneda_Deposito = Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos)
'  With lcMoneda
'    .Campo("id_moneda").Valor = lId_Moneda_Deposito
'    If .Buscar Then
'      If .Cursor.Count > 0 Then
'        lCod_Moneda = .Cursor(1)("cod_moneda").Value
'      End If
'    Else
'      Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Problemas en cargar datos de la Moneda.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      GoTo ErrProcedure
'    End If
'  End With
'  Set lcMoneda = Nothing
'
'  Set lcInstrumentos = New Class_Instrumentos
'  With lcInstrumentos
'    .Campo("cod_instrumento").Valor = gcINST_PACTOS_NAC
'    If .Buscar Then
'      If .Cursor.Count > 0 Then
'        lId_Valorizador = NVL(.Cursor(1)("id_valorizador").Value, -1)
'      End If
'    Else
'      Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Problemas en cargar datos del Instrumento.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      GoTo ErrProcedure
'    End If
'  End With
'  Set lcInstrumentos = Nothing
'
'  Set lcValorizador = New Class_Valorizadores
'  With lcValorizador
'    .Campo("Id_Varlorizador").Valor = lId_Valorizador
'    If .Buscar Then
'      If .Cursor.Count > 0 Then
'        lMetodo = .Cursor(1)("metodo").Value
'      End If
'    Else
'      Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Problemas en cargar datos del Valorizador.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      GoTo ErrProcedure
'    End If
'  End With
'
'  Rem Verifica si Dep�sitos tiene seleccionado un valorizador o se configuro bien el valorizador.
'  If lMetodo = "" Then
'    MsgBox "No existe un m�todo definido para Valorizar Pactos.", vbCritical, "Valorizador"
'    GoTo ErrProcedure
'  End If
'
'  On Error Resume Next
'  Set lcValorizador = CreateObject(lMetodo)
'  On Error GoTo 0
'
'  If lcValorizador Is Nothing Then
'    MsgBox "No se ha podido crear el enlace con el Valorizador de RF." & vbCr & vbCr & Err.Description, vbCritical, "Valorizador"
''    Call Fnt_MsgError(lcValorizadores.SubTipo_LOG, _
''                      "No se ha podido crear el enlace con el Valorizador de RF.", _
''                      Err.Description, _
''                      pConLog:=True)
'    GoTo ErrProcedure
'  End If
'
'  If Opt_Inicio.Value Then
'    lTipo_Calculo = 1
'    lVal_Nom_Ini = To_Number(Txt_Cantidad.Text)
'    lVal_Nom_Fin = 0
'  ElseIf Opt_Final.Value Then
'    lTipo_Calculo = 2
'    lVal_Nom_Ini = 0
'    lVal_Nom_Fin = To_Number(Txt_Cantidad.Text)
'  End If
'
'  With lcValorizador
'    Set .fdb = gDB
'    .FechaEmision = Fnt_FechaServidor
'    .FechaCalculo = Fnt_FechaServidor
'    .FechaVencimiento = Dtp_Fecha_Vencimiento.Value
'    .DiasRef = To_Number(Cmb_Plazo.Text)
'    .TipoInteres = fc_Tipo_Interes 'inter�s es de tipo simple es 1
'    .TasaEmision = To_Number(Txt_TasaNominal.Text)
'    .TasaCalculo = To_Number(Txt_TasaNominal.Text)
'    .TipoCalculo = lTipo_Calculo
'    .CodMoneda = lCod_Moneda
'    .Val_Nom_Ini = lVal_Nom_Ini
'    .Val_Nom_Fin = lVal_Nom_Fin
'    .nemotecnico = Txt_Nemotecnico.Text
'
'    If .Valoriza Then
'      lValorizacion = .MontoValorizado
'    Else
'      'MsgBox "El valorizador ha informado un problema: " & vbCr & vbCr & .ErrMsg, vbCritical, "Valorizador"
'      Call Fnt_MsgError(lcValorizadores.SubTipo_LOG, _
'                        "Problemas en la Valorizacion del Monto.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      GoTo ErrProcedure
'    End If
'  End With
'
'ErrProcedure:
'  Txt_MontoOperacion.Text = lValorizacion
'
'  Set lcValorizadores = Nothing
'
'  Call Sub_Desbloquea_Puntero(Me)

'Dim lcValorizador As Object
'Dim lValorizacion As Double
'Dim lId_Nemotecnico As Double
'Dim lId_Valorizador As String
'Dim lMetodo As String
'Dim lcInstrumentos As Class_Instrumentos
'Dim lcValorizadores As Class_Valorizadores
'Dim lTipo_Calculo As Integer
'Dim lcMoneda As Class_Monedas
'Dim lCod_Moneda As String
'Dim lId_Moneda_Deposito As String
'Dim lVal_Nom_Ini As Double
'Dim lVal_Nom_Fin As Double
'
'  Call Sub_Bloquea_Puntero(Me)
'
'  If Not Fnt_ValidaDatos_ValRF Then
'    GoTo ErrProcedure
'  End If
'
'  Set lcMoneda = New Class_Monedas
'  lId_Moneda_Deposito = Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos)
'  With lcMoneda
'    .Campo("id_moneda").Valor = lId_Moneda_Deposito
'    If .Buscar Then
'      If .Cursor.Count > 0 Then
'        lCod_Moneda = .Cursor(1)("cod_moneda").Value
'      End If
'    Else
'      Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Problemas en cargar datos de la Moneda.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      GoTo ErrProcedure
'    End If
'  End With
'  Set lcMoneda = Nothing
'
'  Set lcInstrumentos = New Class_Instrumentos
'  With lcInstrumentos
'    .Campo("cod_instrumento").Valor = gcINST_PACTOS_NAC
'    If .Buscar Then
'      If .Cursor.Count > 0 Then
'        lId_Valorizador = NVL(.Cursor(1)("id_valorizador").Value, -1)
'      End If
'    Else
'      Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Problemas en cargar datos del Instrumento.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      GoTo ErrProcedure
'    End If
'  End With
'  Set lcInstrumentos = Nothing
'
'  Set lcValorizadores = New Class_Valorizadores
'  With lcValorizadores
'    .Campo("Id_Varlorizador").Valor = lId_Valorizador
'    If .Buscar Then
'      If .Cursor.Count > 0 Then
'        lMetodo = .Cursor(1)("metodo").Value
'      End If
'    Else
'      Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Problemas en cargar datos del Valorizador.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      GoTo ErrProcedure
'    End If
'  End With
'  Set lcValorizadores = Nothing
'
'  Rem Verifica si Pactos tiene seleccionado un valorizador o se configuro bien el valorizador.
'  If lMetodo = "" Then
'    MsgBox "No existe un m�todo definido para Valorizar Pactos.", vbCritical, "Valorizador"
'    GoTo ErrProcedure
'  End If
'
'  On Error Resume Next
'  Set lcValorizador = CreateObject(lMetodo)
'  On Error GoTo 0
'
'  If lcValorizador Is Nothing Then
'    MsgBox "No se ha podido crear el enlace con el Valorizador de RF." & vbCr & vbCr & Err.Description, vbCritical, "Valorizador"
''    Call Fnt_MsgError(lcValorizadores.SubTipo_LOG, _
''                      "No se ha podido crear el enlace con el Valorizador de RF.", _
''                      Err.Description, _
''                      pConLog:=True)
'    GoTo ErrProcedure
'  End If
'
'  If Opt_Inicio.Value Then
'    lTipo_Calculo = 1
'    lVal_Nom_Ini = To_Number(Txt_Cantidad.Text)
'    lVal_Nom_Fin = 0
'  ElseIf Opt_Final.Value Then
'    lTipo_Calculo = 2
'    lVal_Nom_Ini = 0
'    lVal_Nom_Fin = To_Number(Txt_Cantidad.Text)
'  End If
'
'  With lcValorizador
'    Set .fdb = gDB
'    .FechaEmision = Fnt_FechaServidor
'    .FechaCalculo = Fnt_FechaServidor
'    .FechaVencimiento = Dtp_Fecha_Vencimiento.Value
'    .DiasRef = To_Number(Cmb_Plazo.Text)
'    .TipoInteres = fc_Tipo_Interes 'inter�s es de tipo simple es 1
'    .TasaEmision = To_Number(Txt_TasaNominal.Text)
'    .TasaCalculo = To_Number(Txt_TasaNominal.Text)
'    .TipoCalculo = lTipo_Calculo
'    .CodMoneda = lCod_Moneda
'    .Val_Nom_Ini = lVal_Nom_Ini
'    .Val_Nom_Fin = lVal_Nom_Fin
'
'    If .Valoriza Then
'      lValorizacion = .MontoValorizado
'    Else
'      'MsgBox "El valorizador ha informado un problema: " & vbCr & vbCr & .ErrMsg, vbCritical, "Valorizador"
'      Call Fnt_MsgError(lcValorizadores.SubTipo_LOG, _
'                        "Problemas en la Valorizacion del Monto.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      GoTo ErrProcedure
'    End If
'  End With
'
'ErrProcedure:
'  Txt_MontoOperacion.Text = lValorizacion
'
'  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Fnt_ValidaDatos_ValRF() As Boolean
Dim lId_Moneda_Deposito As String

  Fnt_ValidaDatos_ValRF = True
  
  lId_Moneda_Deposito = Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos)
  
  If lId_Moneda_Deposito = "" Then
    MsgBox "Debe seleccionar una Moneda Dep�sito para poder valorizar.", vbCritical, Me.Caption
    GoTo ErrProcedure
  ElseIf Cmb_Plazo.Text = "" Then
    MsgBox "Debe ingresar D�as al Vencimiento para poder valorizar.", vbCritical, Me.Caption
    GoTo ErrProcedure
  ElseIf Not To_Number(Cmb_Plazo.Text) > 0 Then
    MsgBox "D�as al Vencimiento debe ser un valor mayor a cero.", vbCritical, Me.Caption
    GoTo ErrProcedure
  ElseIf Not Dtp_Fecha_Vencimiento.Value > fFecha_Operacion Then
    MsgBox "La Fecha de Vencimiento debe ser mayor o igual a '" & fFecha_Operacion & "'.", vbCritical, Me.Caption
    GoTo ErrProcedure
  ElseIf To_Number(Txt_Cantidad.Text) = 0 Then
    MsgBox "El campo 'Nominales' no puede vac�o ni cero.", vbCritical, Me.Caption
    GoTo ErrProcedure
  End If

  Exit Function
  
ErrProcedure:
  Fnt_ValidaDatos_ValRF = False
  
End Function

Private Sub Txt_TasaNominal_LostFocus()
  If Chk_Vende_Todo.Value Then
    Call Sub_ValorizaPapel
  End If
End Sub

Rem FUNCION QUE ES LLAMADA DESDE LA PANTALLA DE CONSULTA DE OPERACIONES
Public Sub Consulta_Operacion(ByRef pFormOri As Form, _
                                   ByVal pId_Operacion As String, _
                                   ByVal pId_Cuenta As String, _
                                   ByVal pCod_Imstrumento As String, _
                                   ByVal pTipo_Operacion, _
                                   ByVal pOperacion)
  
  fConsulta_Operacion = True
  
  fTipo_Operacion = pTipo_Operacion
  fOperacion = pOperacion
  fId_Cuenta = pId_Cuenta
  fId_Operacion = pId_Operacion
  fCod_Instrumento = pCod_Imstrumento
  
  Txt_Num_Operacion.Text = pId_Operacion
  Me.Caption = Me.Caption & " - Operaci�n N�: " & pId_Operacion
  
  lbl_fecha_ingreso.Visible = False
  DTP_Fecha_Operacion.Visible = False
  Txt_FechaIngreso_Real.Visible = True
  
  If Not fTipo_Operacion = gcOPERACION_Custodia Then
    Lbl_FechaLiquidacion.Visible = True
    Cmb_FechaLiquidacion.Visible = True
    Dtp_FechaLiquidacion.Visible = True
  Else
    Lbl_FechaLiquidacion.Visible = False
    Cmb_FechaLiquidacion.Visible = False
    Dtp_FechaLiquidacion.Visible = False
    
    Frame_Comisiones.Visible = False
    
    Frame_Datos_Documento.Height = Frame_Datos_Documento.Height - lbl_fecha_valuta.Height
    Frame_Datos_Documento.Width = 10875
    
    Frm_Datos_Compra.Height = Frm_Datos_Compra.Height - lbl_fecha_valuta.Height
    
    SSTab1.Height = SSTab1.Height - lbl_fecha_valuta.Height
    Me.Height = Me.Height - lbl_fecha_valuta.Height
    
'    If fOperacion = gcTipoOperacion_Ingreso Then
'      Txt_Tasa_Historica.Visible = True
'      Txt_Tasa_Historica.Locked = True
'      Txt_Tasa_Historica.BackColorTxt = fColorNoEdit
'
'      Txt_Tasa_Historica.Top = 180
'      Txt_TasaNominal.Top = 510
'      Txt_Cantidad.Top = 840
'      Toolbar_Valorizar.Top = 1200
'      Txt_MontoOperacion.Top = 1560
'    End If
  End If
  
  Select Case fOperacion
    Case gcTipoOperacion_Ingreso
      Chk_Vende_Todo.Visible = False
    Case gcTipoOperacion_Egreso
      Chk_Vende_Todo.Visible = True
  End Select
  
  Toolbar.Buttons(1).Visible = False
  Call Form_Resize
  '------------------------------------
  
  Frame_Nemotecnico.Enabled = False
  Cmb_Contraparte.BackColor = fColorNoEdit
  Cmb_Traders.BackColor = fColorNoEdit
  '------------------------------------
  
  Txt_Nemotecnico.Visible = True
  Txt_Nemotecnico.Locked = True
  Txt_Nemotecnico.BackColorTxt = fColorNoEdit
  Cmb_Nemotecnico.Visible = False
  Cmb_Nemotecnico.Tag = ""
  
  Txt_Emisor_Especifico.Visible = True
  lbl_emisor_especifico.Visible = False
  Cmb_Emisor_Especifico.Visible = False
  Cmb_Emisor_Especifico.Tag = ""
  
  Txt_Moneda_Pactos.Visible = True
  lbl_moneda_Pactos.Visible = False
  Cmb_Moneda_Pactos.Visible = False
  Cmb_Moneda_Pactos.Tag = ""
  
'  Txt_Moneda_Pago.Visible = True
  Lbl_Moneda_Pago.Visible = False
  Cmb_Moneda_Pago.Visible = False
  Cmb_Moneda_Pago.Tag = ""
  
  lbl_fecha_ven.Visible = False
  Dtp_Fecha_Vencimiento.Visible = False
  Txt_Fecha_Vencimiento.Visible = True
  
  Txt_Plazo.Visible = True
  lbl_plazo.Visible = False
  Cmb_Plazo.Visible = False
  Cmb_Plazo.Tag = ""
  
  Txt_Base.Visible = True
  lbl_base.Visible = False
  Cmb_Base.Visible = False
  Cmb_Base.Tag = ""
  
  Frm_referenciado.Enabled = False
  '-------------------------------------
  
  Txt_TasaNominal.Locked = True
  Txt_TasaNominal.BackColorTxt = fColorNoEdit
  
  Txt_Cantidad.Locked = True
  Txt_Cantidad.BackColorTxt = fColorNoEdit
  
  Txt_MontoOperacion.Locked = True
  Txt_MontoOperacion.BackColorTxt = fColorNoEdit
  
  Txt_Tasa_Historica.Locked = True
  Txt_Tasa_Historica.BackColorTxt = fColorNoEdit
  
  Chk_Vende_Todo.Enabled = False
  Toolbar_Valorizar.Visible = False
  '-------------------------------------
  
  Frame_Datos_Documento.Enabled = False
  Cmb_FechaLiquidacion.BackColor = fColorNoEdit
  '-------------------------------------
  
  Txt_Porcentaje_Comision.Locked = True
  Txt_Porcentaje_Comision.BackColorTxt = fColorNoEdit
  
  Txt_Porcentaje_Derechos.Locked = True
  Txt_Porcentaje_Derechos.BackColorTxt = fColorNoEdit
  
  Txt_Comision.Locked = True
  Txt_Comision.BackColorTxt = fColorNoEdit
  
  Txt_Iva.Locked = True
  Txt_Iva.BackColorTxt = fColorNoEdit
  
  Txt_Gastos.Locked = True
  Txt_Gastos.BackColorTxt = fColorNoEdit
  
  Txt_Derechos.Locked = True
  Txt_Derechos.BackColorTxt = fColorNoEdit
  '------------------------------------
  
  Toolbar_Clasificadores.Visible = False
  Grilla.Width = 8385
  '-------------------------------------
  
  Call Sub_CargarDatos_Consulta
  
  fSalir = False
    
  Me.Show
  
  Do While Not fSalir
    DoEvents
  Loop
  
  Unload Me
End Sub

Private Sub Sub_CargarDatos_Consulta()
Dim lcOperaciones As Class_Operaciones
Dim lcOperaciones_Detalle As Class_Operaciones_Detalle
Dim lReg As hFields
Dim lcMov_Activos As Class_Mov_Activos
Dim lCursor_Mov_Activos As hRecord
Dim lReg_Mov_Activos As hFields
Dim lcNemotecnico As Class_Nemotecnicos
'---------------------------------------
'Dim lcMoneda As Object
'---------------------------------------
Dim lNombre_Nemotecnico As String
Dim lDsc_Moneda As String
Dim lLinea As Long
Dim lOperacion As String
Dim lId_Contraparte As String
Dim lDsc_Emisor_Especifico As String
Dim lId_Moneda_Operacion As String
Dim lId_Moneda_Pago As String
    
  Rem Carga los datos generales de la operacion
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("id_operacion").Valor = fId_Operacion
    If .Buscar Then
      For Each lReg In .Cursor
        If lReg("cod_estado").Value = cCod_Estado_Pendiente Then
          Call Sub_CargarDatos_Confirmacion
          Exit Sub
        Else
          fOperacion = lReg("Flg_Tipo_Movimiento").Value
          fFecha_Operacion = lReg("Fecha_Operacion").Value
          
          Txt_FechaIngreso_Real.Text = fFecha_Operacion
          
          lId_Contraparte = NVL(lReg("Id_Contraparte").Value, "")
          If Not lId_Contraparte = "" Then
            Call Sub_ComboSelectedItem(Cmb_Contraparte, lId_Contraparte)
            Call Sub_CargaCombo_Traders(Cmb_Traders, lId_Contraparte)
            Call Sub_ComboSelectedItem(Cmb_Traders, "" & lReg("Id_Trader").Value)
          End If
      
          Txt_MontoOperacion.Text = NVL(lReg("Monto_Operacion").Value, "")
          
          Dtp_FechaLiquidacion.Value = lReg("Fecha_Liquidacion").Value
          Dtp_FechaLiquidacion.MinDate = fFecha_Operacion
          Cmb_FechaLiquidacion.Text = Fnt_DiasHabiles_EntreFechas(fFecha_Operacion, Dtp_FechaLiquidacion.Value)
          
          Rem Comisiones
          Txt_Porcentaje_Comision.Text = (NVL(lReg("Porc_Comision").Value, 0) * 100)
          Txt_Comision.Text = NVL(lReg("Comision").Value, 0)
          Txt_Iva.Text = NVL(lReg("Iva").Value, 0)
          Txt_Gastos.Text = NVL(lReg("Gastos").Value, 0)
          Txt_Derechos.Text = NVL(lReg("Derechos").Value, 0)
          Txt_Porcentaje_Derechos.Text = Fnt_Divide(Txt_Derechos.Text * 100, NVL(lReg("Monto_Operacion").Value, 0))
        End If
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar la Operaci�n.", _
                        fOperaciones.ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcOperaciones = Nothing
  
  Set lcOperaciones_Detalle = New Class_Operaciones_Detalle
  With lcOperaciones_Detalle
    .Campo("id_operacion").Valor = fId_Operacion
    If .Buscar Then
      Set lCursor_Mov_Activos = .Cursor
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar la Operaci�n Detalle.", _
                        fOperaciones.ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcOperaciones_Detalle = Nothing
  
  Set lcMov_Activos = New Class_Mov_Activos
  With lcMov_Activos
    For Each lReg In lCursor_Mov_Activos
      .Campo("id_operacion_detalle").Valor = lReg("id_operacion_detalle").Value
      If .Buscar Then
        For Each lReg_Mov_Activos In .Cursor
          Set lcNemotecnico = New Class_Nemotecnicos
          lcNemotecnico.Campo("id_nemotecnico").Valor = lReg_Mov_Activos("Id_Nemotecnico").Value
          If lcNemotecnico.BuscarView Then
            If lcNemotecnico.Cursor.Count > 0 Then
              lNombre_Nemotecnico = lcNemotecnico.Cursor(1)("NEMOTECNICO").Value
              lDsc_Emisor_Especifico = NVL(lcNemotecnico.Cursor(1)("dsc_emisor_especifico").Value, "")
              lId_Moneda_Operacion = NVL(lcNemotecnico.Cursor(1)("id_moneda").Value, "")
            End If
          End If
          Set lcNemotecnico = Nothing
        
          Txt_Nemotecnico.Text = lNombre_Nemotecnico
          Txt_Emisor_Especifico.Text = lDsc_Emisor_Especifico
          
          If Not lId_Moneda_Operacion = "" Then
            Call Sub_ComboSelectedItem(Cmb_Moneda_Pactos, lId_Moneda_Operacion)
            Txt_Moneda_Pactos.Text = Cmb_Moneda_Pactos.Text
          End If
          
          lId_Moneda_Pago = NVL(lReg_Mov_Activos("Id_Moneda").Value, "")
          If Not lId_Moneda_Pago = "" Then
            Call Sub_ComboSelectedItem(Cmb_Moneda_Pago, lId_Moneda_Pago)
            Txt_Moneda_Pago.Text = Cmb_Moneda_Pago.Text
          End If
          
          Txt_Cantidad.Text = NVL(lReg_Mov_Activos("Cantidad").Value, "")
          Txt_TasaNominal.Text = NVL(lReg_Mov_Activos("Precio").Value, "")
          
'          If lReg_Mov_Activos("Flg_Monto_Referenciado").Value = "I" Then
'            Opt_Inicio.Value = True
'            Opt_Final.Value = False
'          ElseIf lReg_Mov_Activos("Flg_Monto_Referenciado").Value = "F" Then
'            Opt_Inicio.Value = False
'            Opt_Final.Value = True
'          End If
          
'          Dtp_Fecha_Vencimiento.MinDate = NVL(lReg_Mov_Activos("Fecha_Vencimiento").Value, Format(Fnt_FechaServidor, cFormatDate))
'          Dtp_Fecha_Vencimiento.Value = NVL(lReg_Mov_Activos("Fecha_Vencimiento").Value, Format(Fnt_FechaServidor, cFormatDate))
'          Txt_Fecha_Vencimiento.Text = Dtp_Fecha_Vencimiento.Value
'
'          Dtp_Fecha_Valuta.Value = NVL(lReg_Mov_Activos("Fecha_Valuta").Value, Format(Fnt_FechaServidor, cFormatDate))
'
'          Cmb_Plazo.Text = NVL(lReg_Mov_Activos("Plazo").Value, "")
'          Txt_Plazo.Text = Cmb_Plazo.Text
'
'          Cmb_Base.Text = NVL(lReg_Mov_Activos("Base").Value, "")
'          Txt_Base.Text = Cmb_Base.Text
'
'          Chk_Vende_Todo.Tag = NVL(lReg_Mov_Activos("flg_vende_todo").Value, "")
'
'          Txt_Tasa_Historica.Text = NVL(lReg_Mov_Activos("Precio_Historico_Compra").Value, "")
          
          Call Sub_Carga_Clasificadores_Riesgo(lReg_Mov_Activos("Id_Nemotecnico").Value)
      
        Next
      End If
    Next
  End With
  Set lcMov_Activos = Nothing
  
End Sub
