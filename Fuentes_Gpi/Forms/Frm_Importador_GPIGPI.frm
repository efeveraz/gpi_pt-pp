VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Importador_GPIGPI 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Importador Clientes/Cuentas CDS"
   ClientHeight    =   8745
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8775
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8745
   ScaleMode       =   0  'User
   ScaleWidth      =   8887.755
   Begin MSComCtl2.DTPicker DtFecha 
      Height          =   255
      Left            =   5880
      TabIndex        =   14
      Top             =   840
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   450
      _Version        =   393216
      Format          =   16908289
      CurrentDate     =   41586
   End
   Begin VB.CheckBox ChkHistorial 
      Caption         =   "Incluir Historial"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   840
      Width           =   2175
   End
   Begin VB.OptionButton OptMaestros 
      Caption         =   "Maestros"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   480
      Width           =   1215
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   8
      Top             =   8490
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15425
            Key             =   "Status"
         EndProperty
      EndProperty
   End
   Begin VB.OptionButton OptClientes 
      Caption         =   "Clientes"
      Height          =   255
      Left            =   1560
      TabIndex        =   3
      Top             =   480
      Width           =   1215
   End
   Begin VB.OptionButton OptCuentas 
      Caption         =   "Cuentas"
      Height          =   255
      Left            =   3000
      TabIndex        =   2
      Top             =   480
      Width           =   1215
   End
   Begin VB.Frame FrmClienteCuenta 
      Caption         =   "Clientes/Cuentas"
      Height          =   7215
      Left            =   120
      TabIndex        =   1
      Top             =   1200
      Width           =   8535
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Clientes 
         Height          =   6855
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   7695
         _cx             =   13573
         _cy             =   12091
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   4
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Importador_GPIGPI.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   1
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
         Height          =   6855
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   7695
         _cx             =   13573
         _cy             =   12091
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   5
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Importador_GPIGPI.frx":009E
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   1
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Nemotecnicos 
         Height          =   660
         Left            =   7920
         TabIndex        =   9
         Top             =   240
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "ALL"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Selecciona todos los Perfiles de Riesgo"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "NALL"
               Object.ToolTipText     =   "Selecciona ninguno de los Perfiles de Riesgo"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar2 
            Height          =   255
            Left            =   9420
            TabIndex        =   10
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Grabar"
            Key             =   "SAVE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Graba los cambios realizados"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   2300
         TabIndex        =   11
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin TrueDBList80.TDBCombo Cmb_Empresa 
      Height          =   255
      Left            =   5880
      TabIndex        =   7
      Top             =   480
      Width           =   2775
      _ExtentX        =   4895
      _ExtentY        =   450
      _LayoutType     =   0
      _RowHeight      =   -2147483647
      _WasPersistedAsPixels=   0
      _DropdownWidth  =   0
      _EDITHEIGHT     =   450
      _GAPHEIGHT      =   53
      Columns(0)._VlistStyle=   0
      Columns(0)._MaxComboItems=   5
      Columns(0).DataField=   "ub_grid1"
      Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
      Columns(1)._VlistStyle=   0
      Columns(1)._MaxComboItems=   5
      Columns(1).DataField=   "ub_grid2"
      Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
      Columns.Count   =   2
      Splits(0)._UserFlags=   0
      Splits(0).ExtendRightColumn=   -1  'True
      Splits(0).AllowRowSizing=   0   'False
      Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
      Splits(0)._ColumnProps(0)=   "Columns.Count=2"
      Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
      Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
      Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
      Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
      Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
      Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
      Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
      Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
      Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
      Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
      Splits.Count    =   1
      Appearance      =   3
      BorderStyle     =   1
      ComboStyle      =   2
      AutoCompletion  =   -1  'True
      LimitToList     =   0   'False
      ColumnHeaders   =   0   'False
      ColumnFooters   =   0   'False
      DataMode        =   5
      DefColWidth     =   0
      Enabled         =   -1  'True
      HeadLines       =   1
      FootLines       =   1
      RowDividerStyle =   0
      Caption         =   ""
      EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
      LayoutName      =   ""
      LayoutFileName  =   ""
      MultipleLines   =   0
      EmptyRows       =   0   'False
      CellTips        =   0
      AutoSize        =   0   'False
      ListField       =   ""
      BoundColumn     =   ""
      IntegralHeight  =   0   'False
      CellTipsWidth   =   0
      CellTipsDelay   =   1000
      AutoDropdown    =   0   'False
      RowTracking     =   -1  'True
      RightToLeft     =   0   'False
      MouseIcon       =   0
      MouseIcon.vt    =   3
      MousePointer    =   0
      MatchEntryTimeout=   2000
      OLEDragMode     =   0
      OLEDropMode     =   0
      AnimateWindow   =   0
      AnimateWindowDirection=   0
      AnimateWindowTime=   200
      AnimateWindowClose=   1
      DropdownPosition=   0
      Locked          =   0   'False
      ScrollTrack     =   -1  'True
      ScrollTips      =   -1  'True
      RowDividerColor =   14215660
      RowSubDividerColor=   14215660
      MaxComboItems   =   10
      AddItemSeparator=   ";"
      _PropDict       =   $"Frm_Importador_GPIGPI.frx":0162
      _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
      _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
      _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
      _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
      _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
      _StyleDefs(5)   =   ":id=0,.fontname=Arial"
      _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HFFFFFF&"
      _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
      _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
      _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
      _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
      _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
      _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
      _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
      _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
      _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
      _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
      _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
      _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
      _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
      _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
      _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
      _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
      _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
      _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
      _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
      _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
      _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
      _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
      _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
      _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
      _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
      _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
      _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
      _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
      _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
      _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
      _StyleDefs(38)  =   "Named:id=33:Normal"
      _StyleDefs(39)  =   ":id=33,.parent=0"
      _StyleDefs(40)  =   "Named:id=34:Heading"
      _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(42)  =   ":id=34,.wraptext=-1"
      _StyleDefs(43)  =   "Named:id=35:Footing"
      _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(45)  =   "Named:id=36:Selected"
      _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
      _StyleDefs(47)  =   "Named:id=37:Caption"
      _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
      _StyleDefs(49)  =   "Named:id=38:HighlightRow"
      _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
      _StyleDefs(51)  =   "Named:id=39:EvenRow"
      _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
      _StyleDefs(53)  =   "Named:id=40:OddRow"
      _StyleDefs(54)  =   ":id=40,.parent=33"
      _StyleDefs(55)  =   "Named:id=41:RecordSelector"
      _StyleDefs(56)  =   ":id=41,.parent=34"
      _StyleDefs(57)  =   "Named:id=42:FilterBar"
      _StyleDefs(58)  =   ":id=42,.parent=33"
   End
   Begin VB.Label LblFecha 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Fecha Contrato"
      Height          =   255
      Left            =   4365
      TabIndex        =   15
      Top             =   840
      Width           =   1455
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Empresa"
      Height          =   255
      Left            =   4365
      TabIndex        =   4
      Top             =   480
      Width           =   1455
   End
End
Attribute VB_Name = "Frm_Importador_GPIGPI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: Frm_Importador_GPIGPI.frm $
'    $Author: Gbuenrostro $
'    $Date: 13/01/14 16:38 $
'    $Revision: 8 $
'-------------------------------------------------------------------------------------------------

Implements Class_Entidad
'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana
Const tiempo As Integer = 4

Dim lId_Empresa          As String
'Public fArchivo_Log As Byte

Dim fhBorrar As hRecord


Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Sub_Grabar()
    Dim lReg    As hFields
    Dim lResult As hRecord
    Dim cfPackage As String
    Dim lFila As Long
    'Dim lNombreArchivo  As String

    
    'lNombreArchivo = App.Path & "\Resultado.txt"
    'fArchivo_Log = FreeFile

    If Me.OptMaestros.Value = True Then
        
        Call Sub_Bloquea_Puntero(Me)
        
        cfPackage = "PKG_IMPORTADOR_GPIGPI$Maestros"
        Me.StatusBar.Panels(1).Text = "Importando Maestros"
        Me.StatusBar.Refresh
        With gDB
            .Parametros.Clear
            .Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
            .Procedimiento = cfPackage
            If Not .EjecutaSP Then
                Call Fnt_MsgError(eLS_ErrSystem, "Problemas durante la importaci�n.", .ErrMsg, pConLog:=True)
                Call Sub_Desbloquea_Puntero(Me)
                Exit Sub
            End If
            
            Set lResult = .Parametros("pCursor").Valor
        End With
        Me.StatusBar.Panels(1).Text = "Resultado de proceso: " & lResult(1).Item("ErrorMessage")
        Me.StatusBar.Refresh
        If lResult(1).Item("ErrorMessage") <> "OK" Then
            Call Fnt_MsgError(eLS_ErrSystem, "Problemas durante la importaci�n.", Err.Description, pConLog:=True)
        Else
          MsgBox ("Proceso completado.")
        End If
    ElseIf Me.OptClientes.Value = True Then
    
        
        Call Sub_Bloquea_Puntero(Me)
        lId_Empresa = Fnt_ComboSelected_KEY(Cmb_Empresa)
        
        'Open lNombreArchivo For Output As #fArchivo_Log
        'Print #fArchivo_Log, "Iniciando importacion de clientes (" & ContarClientes() & " registros)"
        'Close #fArchivo_Log

        For lFila = 1 To Me.Grilla_Clientes.Rows - 1
            If GetCell(Grilla_Clientes, lFila, "CHK") = -1 And GetCell(Grilla_Clientes, lFila, "DSC") = "" Then
                Me.StatusBar.Panels(1).Text = "Tiempo estimado de proceso: " & tiempo * ContarClientes & " minutos."
                Call SetCell(Me.Grilla_Clientes, lFila, "DSC", "Procesando...", pAutoSize:=False)
                Me.Refresh
                Grilla_Clientes.Refresh
                cfPackage = "PKG_IMPORTADOR_GPIGPI$Cuentas"
                
                'Open lNombreArchivo For Append As #fArchivo_Log
                'Print #fArchivo_Log, "Importando cliente " & StrConv(GetCell(Grilla_Clientes, lFila, "NOM_CLIENTE"), vbProperCase)
                'Close #fArchivo_Log

                With gDB
                    .IniciarTransaccion
                    .Parametros.Clear
                    .Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
                    .Parametros.Add "Pid_Cuenta", ePT_Numero, "", ePD_Entrada
                    .Parametros.Add "Pid_Cliente", ePT_Numero, GetCell(Grilla_Clientes, lFila, "ID_CLIENTE"), ePD_Entrada
                    .Parametros.Add "Pid_Empresa", ePT_Numero, lId_Empresa, ePD_Entrada
                    .Parametros.Add "Pflg_Historial", ePT_Numero, Me.ChkHistorial.Value, ePD_Entrada
                    .Parametros.Add "Pfh_Contrato", ePT_Fecha, Me.DtFecha, ePD_Entrada
                    .Procedimiento = cfPackage
                    If Not .EjecutaSP Then
                        Call Sub_Desbloquea_Puntero(Me)
                        MsgBox (.ErrMsg)
                        
                        'Open lNombreArchivo For Append As #fArchivo_Log
                        'Print #fArchivo_Log, .ErrMsg
                        'Close #fArchivo_Log
                        Call SetCell(Me.Grilla_Clientes, lFila, "DSC", "ERR " & .ErrMsg, pAutoSize:=True)
                        gDB.RollbackTransaccion
                    Else
                        gDB.CommitTransaccion
                        Set lResult = .Parametros("pCursor").Valor
                        
                        'Open lNombreArchivo For Append As #fArchivo_Log
                        'Print #fArchivo_Log, lResult(1).Item("ErrorMessage")
                        'Close #fArchivo_Log
                        If lResult(1).Item("ErrorMessage") <> "OK" Then
                            Call SetCell(Me.Grilla_Clientes, lFila, "DSC", "ERR" & lResult(1).Item("ErrorNumber") & lResult(1).Item("ErrorMessage"), pAutoSize:=True)
                        Else
                            Call SetCell(Me.Grilla_Clientes, lFila, "DSC", "Importado", pAutoSize:=False)
                        End If
                    End If
                End With
            End If
        Next
        
        MsgBox ("Proceso completado.")

        'Open lNombreArchivo For Append As #fArchivo_Log
        'Print #fArchivo_Log, "Proceso Finalizado"
        'Close #fArchivo_Log
    ElseIf Me.OptCuentas.Value = True Then
        Call Sub_Bloquea_Puntero(Me)
        lId_Empresa = Fnt_ComboSelected_KEY(Cmb_Empresa)
        
        'Open lNombreArchivo For Output As #fArchivo_Log
        'Print #fArchivo_Log, "Iniciando importacion de cuentas (" & ContarCuentas() & " registros)"
        'Close #fArchivo_Log
        
        For lFila = 1 To Me.Grilla_Cuentas.Rows - 1
            If GetCell(Grilla_Cuentas, lFila, "CHK") = -1 And GetCell(Me.Grilla_Cuentas, lFila, "DSC") = "" Then
                Me.StatusBar.Panels(1).Text = "Tiempo estimado de proceso: " & tiempo * ContarCuentas & " minutos." 'Para mas detalles: " & lNombreArchivo
                Call SetCell(Me.Grilla_Cuentas, lFila, "DSC", "Procesando...", pAutoSize:=False)
                Me.Refresh
                Grilla_Cuentas.Refresh
                cfPackage = "PKG_IMPORTADOR_GPIGPI$Cuentas"

                'Open lNombreArchivo For Append As #fArchivo_Log
                'Print #fArchivo_Log, "Importando cuenta " & StrConv(GetCell(Me.Grilla_Cuentas, lFila, "NUM_CUENTA"), vbProperCase)
                'Close #fArchivo_Log
                
                With gDB
                    .IniciarTransaccion
                    .Parametros.Clear
                    .Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
                    .Parametros.Add "Pid_Cuenta", ePT_Numero, GetCell(Me.Grilla_Cuentas, lFila, "ID_CUENTA"), ePD_Entrada
                    .Parametros.Add "Pid_Cliente", ePT_Numero, "", ePD_Entrada
                    .Parametros.Add "Pid_Empresa", ePT_Numero, lId_Empresa, ePD_Entrada
                    .Parametros.Add "Pflg_Historial", ePT_Numero, Me.ChkHistorial.Value, ePD_Entrada
                    .Parametros.Add "Pfh_Contrato", ePT_Fecha, Me.DtFecha, ePD_Entrada
                    .Procedimiento = cfPackage
                    If Not .EjecutaSP Then
                        Call Sub_Desbloquea_Puntero(Me)
                        MsgBox (.ErrMsg)
                        
                        'Open lNombreArchivo For Append As #fArchivo_Log
                        'Print #fArchivo_Log, .ErrMsg
                        'Close #fArchivo_Log
                        Call SetCell(Me.Grilla_Cuentas, lFila, "DSC", "ERR " & .ErrMsg, pAutoSize:=True)
                        gDB.RollbackTransaccion
                    Else
                        gDB.CommitTransaccion
                        Set lResult = .Parametros("pCursor").Valor
                        
                        'Open lNombreArchivo For Append As #fArchivo_Log
                        'Print #fArchivo_Log, lResult(1).Item("ErrorMessage")
                        'Close #fArchivo_Log
                        If lResult(1).Item("ErrorMessage") <> "OK" Then
                            Call SetCell(Me.Grilla_Cuentas, lFila, "DSC", "ERR" & lResult(1).Item("ErrorNumber") & lResult(1).Item("ErrorMessage"), pAutoSize:=True)
                        Else
                            Call SetCell(Me.Grilla_Cuentas, lFila, "DSC", "Importado", pAutoSize:=False)
                        End If
                    End If
                    
                End With
            End If
        Next
        
        MsgBox ("Proceso completado.")
        
        'Open lNombreArchivo For Append As #fArchivo_Log
        'Print #fArchivo_Log, "Proceso Finalizado"
        'Close #fArchivo_Log
    End If
    
ErrProcedure:
    If Not Err.Number = 0 Then
        Call Fnt_MsgError(eLS_ErrSystem, "Problemas durante la importaci�n.", Err.Description, pConLog:=True)
'        lResult = False
        GoTo ExitProcedure
        Resume
    End If

ExitProcedure:
'    If lResult(1).Item("MENSAJE") <> "ok" Then
'        gDB.CommitTransaccion
'    Else
'        gDB.RollbackTransaccion
'    End If
    
    'Call Sub_CargarDatos
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function ContarCuentas() As Integer
Dim Contador As Integer
Dim lFila As Long

Contador = 0
    For lFila = 1 To Me.Grilla_Cuentas.Rows - 1
        If GetCell(Me.Grilla_Cuentas, lFila, "CHK") = -1 And (GetCell(Me.Grilla_Cuentas, lFila, "DSC") = "" Or GetCell(Me.Grilla_Cuentas, lFila, "DSC") = "Procesando...") Then
            Contador = Contador + 1
        End If
    Next
ContarCuentas = Contador
End Function

Private Function ContarClientes() As Integer
Dim Contador As Integer
Dim lFila As Long

Contador = 0
    For lFila = 1 To Me.Grilla_Clientes.Rows - 1
        If GetCell(Me.Grilla_Clientes, lFila, "CHK") = -1 And (GetCell(Me.Grilla_Clientes, lFila, "DSC") = "" Or GetCell(Me.Grilla_Clientes, lFila, "DSC") = "Procesando...") Then
            Contador = Contador + 1
        End If
    Next
ContarClientes = Contador
End Function

Private Sub ChkHistorial_Click()
    Me.DtFecha.Enabled = Not CBool(Me.ChkHistorial.Value)
    Me.LblFecha.Enabled = Not CBool(Me.ChkHistorial.Value)
End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SAVE").Image = cBoton_Grabar
    .Buttons("EXIT").Image = cBoton_Salir
    .Buttons("REFRESH").Image = cBoton_Original
  End With
  
  Call Sub_CargaCombo_Empresa(Cmb_Empresa)
  Me.Cmb_Empresa.SelectedItem = 0
  
  Set fhBorrar = New hRecord
  With fhBorrar
    Call .AddField("ID_ARBOL_CLASE_INST")
  End With
  Me.Grilla_Clientes.Rows = 1
  Me.Grilla_Cuentas.Rows = 1
  Call Sub_CargarDatos
  Me.OptClientes.Value = True
  Me.OptMaestros.Value = True
  Me.ChkHistorial.Value = 1
  
  With Toolbar_Nemotecnicos
    Set .ImageList = MDI_Principal.ImageListGlobal16
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("ALL").Image = "boton_seleccionar_todos"
    .Buttons("NALL").Image = "boton_seleccionar_ninguno"
  End With
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargarDatos()
Dim lReg                      As hFields
Dim lcursor As hRecord

  Call Sub_Bloquea_Puntero(Me)
  
  Dim cfPackage As String
  cfPackage = "PKG_IMPORTADOR_GPIGPI$ClienteBuscar"
  
  With gDB
    .Parametros.Clear
    .Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
    .Procedimiento = cfPackage
    If Not .EjecutaSP Then
      MsgBox (.ErrMsg)
      Exit Sub
    End If
    Set lcursor = gDB.Parametros("pCursor").Valor
    
    Dim lLinea
    Grilla_Clientes.Rows = 1
    For Each lReg In lcursor
      lLinea = Me.Grilla_Clientes.Rows
      Call Me.Grilla_Clientes.AddItem("")
      Call SetCell(Me.Grilla_Clientes, lLinea, "ID_CLIENTE", Trim(lReg("ID_CLIENTE").Value), pAutoSize:=False)
      Call SetCell(Me.Grilla_Clientes, lLinea, "NOM_CLIENTE", StrConv(lReg("NOM_CLIENTE").Value, vbProperCase), pAutoSize:=False)
    Next
  End With
  
  cfPackage = "PKG_IMPORTADOR_GPIGPI$CuentaBuscar"

  With gDB
    .Parametros.Clear
    .Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
    .Procedimiento = cfPackage
    If Not .EjecutaSP Then
      Exit Sub
    End If
    Set lcursor = gDB.Parametros("pCursor").Valor
    
    Grilla_Cuentas.Rows = 1
    For Each lReg In lcursor
      lLinea = Me.Grilla_Cuentas.Rows
      Call Me.Grilla_Cuentas.AddItem("")
      Call SetCell(Me.Grilla_Cuentas, lLinea, "ID_CUENTA", Trim(lReg("ID_CUENTA").Value), pAutoSize:=False)
      Call SetCell(Me.Grilla_Cuentas, lLinea, "NUM_CUENTA", Trim(lReg("NUM_CUENTA").Value), pAutoSize:=True)
      Call SetCell(Me.Grilla_Cuentas, lLinea, "NOM_CLIENTE", StrConv(lReg("NOM_CLIENTE").Value, vbProperCase), pAutoSize:=False)
    Next
  End With
  
ExitProcedure:
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Clientes_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Col = Grilla_Clientes.ColIndex("NOM_CLIENTE") Then
    Cancel = True
  End If
  If Col = Grilla_Clientes.ColIndex("DSC") Then
    Cancel = True
  End If
End Sub

Private Sub Grilla_Clientes_CellChanged(ByVal Row As Long, ByVal Col As Long)
    Me.StatusBar.Panels(1).Text = "Tiempo estimado de proceso: " & tiempo * ContarClientes & " minutos." ' Para mas detalles: " & App.Path & "\Resultado.txt"
End Sub

Private Sub Grilla_Cuentas_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Col = Grilla_Cuentas.ColIndex("NUM_CUENTA") Then
    Cancel = True
  End If
  If Col = Grilla_Cuentas.ColIndex("NOM_CLIENTE") Then
    Cancel = True
  End If
  If Col = Grilla_Cuentas.ColIndex("DSC") Then
    Cancel = True
  End If
End Sub

Private Sub Grilla_Cuentas_CellChanged(ByVal Row As Long, ByVal Col As Long)
    Me.StatusBar.Panels(1).Text = "Tiempo estimado de proceso: " & tiempo * ContarCuentas & " minutos." 'Para mas detalles: " & App.Path & "\Resultado.txt"
End Sub

Private Sub OptClientes_Click()
    Me.Grilla_Clientes.BackColorBkg = &H80000005
    Me.Grilla_Clientes.BackColor = &H80000005
    
    Me.FrmClienteCuenta.Enabled = True
    Me.Grilla_Clientes.Enabled = True
    Me.Grilla_Clientes.Visible = True
    Me.Grilla_Cuentas.Enabled = False
    Me.Grilla_Cuentas.Visible = False
    Me.ChkHistorial.Enabled = True
    Me.Cmb_Empresa.Enabled = True
    Me.DtFecha.Enabled = Not CBool(Me.ChkHistorial.Value)
    Me.LblFecha.Enabled = Not CBool(Me.ChkHistorial.Value)
    Me.Label1.Enabled = True

End Sub

Private Sub OptCuentas_Click()
    Me.Grilla_Cuentas.BackColorBkg = &H80000005
    Me.Grilla_Cuentas.BackColor = &H80000005

    Me.FrmClienteCuenta.Enabled = True
    Me.Grilla_Clientes.Enabled = False
    Me.Grilla_Clientes.Visible = False
    Me.Grilla_Cuentas.Enabled = True
    Me.Grilla_Cuentas.Visible = True
    Me.ChkHistorial.Enabled = True
    Me.Cmb_Empresa.Enabled = True
    Me.DtFecha.Enabled = Not CBool(Me.ChkHistorial.Value)
    Me.LblFecha.Enabled = Not CBool(Me.ChkHistorial.Value)
    Me.Label1.Enabled = True
End Sub

Private Sub OptMaestros_Click()
    Me.Grilla_Clientes.BackColorBkg = &H8000000F
    Me.Grilla_Clientes.BackColor = &H8000000F
    Me.Grilla_Cuentas.BackColorBkg = &H8000000F
    Me.Grilla_Cuentas.BackColor = &H8000000F
    Me.FrmClienteCuenta.Enabled = False
    Me.Grilla_Clientes.Enabled = False
    Me.Grilla_Cuentas.Enabled = False
    Me.ChkHistorial.Enabled = False
    Me.Cmb_Empresa.Enabled = False
    Me.DtFecha.Enabled = False
    Me.LblFecha.Enabled = False
    Me.Label1.Enabled = False
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Call Sub_Grabar
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Sub Toolbar_Nemotecnicos_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lFila As Long
  
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ALL"
      If Me.OptClientes.Value = True Then
        For lFila = 1 To Me.Grilla_Clientes.Rows - 1
          Call SetCell(Me.Grilla_Clientes, lFila, "CHK", -1, pAutoSize:=False)
        Next
'        Me.StatusBar.Panels(1).Text = "Tiempo estimado de proceso: " & 5 * ContarClientes & " minutos"
      ElseIf Me.OptCuentas.Value = True Then
         For lFila = 1 To Me.Grilla_Clientes.Rows - 1
          Call SetCell(Me.Grilla_Cuentas, lFila, "CHK", -1, pAutoSize:=False)
        Next
'        Me.StatusBar.Panels(1).Text = "Tiempo estimado de proceso: " & 5 * ContarCuentas & " minutos"
      End If
    Case "NALL"
      If Me.OptClientes.Value = True Then
        For lFila = 1 To Me.Grilla_Clientes.Rows - 1
          Call SetCell(Me.Grilla_Clientes, lFila, "CHK", "", pAutoSize:=False)
        Next
'        Me.StatusBar.Panels(1).Text = "Tiempo estimado de proceso: " & 5 * ContarClientes & " minutos"
      ElseIf Me.OptCuentas.Value = True Then
        For lFila = 1 To Me.Grilla_Clientes.Rows - 1
          Call SetCell(Me.Grilla_Cuentas, lFila, "CHK", "", pAutoSize:=False)
        Next
'        Me.StatusBar.Panels(1).Text = "Tiempo estimado de proceso: " & 5 * ContarCuentas & " minutos"
      End If
  End Select
End Sub
