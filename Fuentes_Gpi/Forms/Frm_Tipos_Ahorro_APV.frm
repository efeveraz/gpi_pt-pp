VERSION 5.00
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Tipos_Ahorro_APV 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Datos Tipos de Ahorro APV"
   ClientHeight    =   2100
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6750
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2100
   ScaleWidth      =   6750
   Begin VB.Frame Frame1 
      Caption         =   "Datos Tipos de Ahorro"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1605
      Left            =   60
      TabIndex        =   0
      Top             =   450
      Width           =   6585
      Begin VB.CheckBox Chk_HabilitadoRetiro 
         Caption         =   "Habilitado para Retiro"
         Height          =   255
         Left            =   4320
         TabIndex        =   6
         Top             =   1200
         Width           =   2055
      End
      Begin VB.CheckBox Chk_Acogido42Bis 
         Caption         =   "Acogido a 42-Bis"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1200
         Width           =   2535
      End
      Begin hControl2.hTextLabel Txt_DescripcionCorta 
         Height          =   315
         Left            =   120
         TabIndex        =   1
         Tag             =   "OBLI"
         Top             =   330
         Width           =   4875
         _ExtentX        =   8599
         _ExtentY        =   556
         LabelWidth      =   2000
         TextMinWidth    =   1200
         Caption         =   "Descripci�n Corta"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   59
      End
      Begin hControl2.hTextLabel Txt_DescripcionLarga 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Tag             =   "OBLI"
         Top             =   720
         Width           =   6315
         _ExtentX        =   11139
         _ExtentY        =   556
         LabelWidth      =   2000
         TextMinWidth    =   1200
         Caption         =   "Descripci�n Larga"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   59
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   6750
      _ExtentX        =   11906
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Tipos_Ahorro_APV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fFlg_Tipo_Permiso As String

Private Sub Cmb_Mercado_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pId_Cliente, pCod_Arbol_Sistema)
  fKey = pId_Cliente
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Call Sub_CargarDatos
  
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso Tipos de Ahorro APV"
  Else
    Me.Caption = "Modificaci�n Tipos de Ahorro APV: " & Txt_DescripcionCorta.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Unload Me
      End If
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
    Case "EXIT"
      Unload Me
      
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcTipoAhorro As Class_TiposAhorroAPV

    Fnt_Grabar = True

    If Not Fnt_ValidarDatos Then
      Fnt_Grabar = False
      Exit Function
    End If

  
    Set lcTipoAhorro = New Class_TiposAhorroAPV
    With lcTipoAhorro
      .Campo("id_tipo_ahorro").Valor = fKey
      .Campo("descripcion_corta").Valor = Txt_DescripcionCorta.Text
      .Campo("descripcion_larga").Valor = Txt_DescripcionLarga.Text
      .Campo("ind_acogido_42bis").Valor = IIf(Chk_Acogido42Bis.Value = 1, "S", "N")
      .Campo("habilitado_retiro").Valor = IIf(Chk_HabilitadoRetiro.Value = 1, "S", "N")
      If Not .Guardar Then
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en guardar Tipos de Ahorro APV.", _
                          .ErrMsg, _
                          pConLog:=True)
        Fnt_Grabar = False
      End If
    End With
  
End Function

Private Sub Sub_CargaForm()

  Call Sub_FormControl_Color(Me.Controls)
  Chk_Acogido42Bis.Value = 0
  Chk_HabilitadoRetiro.Value = 0
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lcTipoAhorro As Class_TiposAhorroAPV
  
    Set lcTipoAhorro = New Class_TiposAhorroAPV
    With lcTipoAhorro
      .Campo("id_tipo_ahorro").Valor = fKey
      If .Buscar Then
        For Each lReg In .Cursor
          Txt_DescripcionCorta.Text = NVL(lReg("descripcion_corta").Value, "")
          Txt_DescripcionLarga.Text = NVL(lReg("descripcion_larga").Value, "")
          Chk_Acogido42Bis.Value = IIf(lReg("ind_acogido_42bis").Value = "S", 1, 0)
          Chk_HabilitadoRetiro.Value = IIf(lReg("habilitado_retiro").Value = "S", 1, 0)
        Next
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar Tipos Ahorro APV.", _
                          .ErrMsg, _
                          pConLog:=True)
      End If
    End With
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function


