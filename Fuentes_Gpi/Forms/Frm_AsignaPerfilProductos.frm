VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_AsignaPerfilProductos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asignaci�n de Perfiles a Nemot�cnicos"
   ClientHeight    =   6315
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   10455
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6315
   ScaleWidth      =   10455
   Begin MSComctlLib.ListView Lista 
      Height          =   5175
      Left            =   6330
      TabIndex        =   1
      Top             =   720
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   9128
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Perfiles"
         Object.Width           =   10319
      EndProperty
   End
   Begin MSComctlLib.TreeView Arbol 
      Height          =   5175
      Left            =   60
      TabIndex        =   0
      Top             =   720
      Width           =   6195
      _ExtentX        =   10927
      _ExtentY        =   9128
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   176
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      FullRowSelect   =   -1  'True
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   10455
      _ExtentX        =   18441
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Grabar"
            Key             =   "SAVE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Graba los cambios realizados"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   3690
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin MSComctlLib.Toolbar Toolbar_Perfiles 
      Height          =   660
      Left            =   9900
      TabIndex        =   7
      Top             =   720
      Width           =   450
      _ExtentX        =   794
      _ExtentY        =   1164
      ButtonWidth     =   1138
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ALL"
            Description     =   "Agregar un nemotecnico a la Operaci�n"
            Object.ToolTipText     =   "Selecciona todos los Perfiles de Riesgo"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "NALL"
            Object.ToolTipText     =   "Selecciona ninguno de los Perfiles de Riesgo"
         EndProperty
      EndProperty
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   255
         Left            =   9420
         TabIndex        =   8
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Label Label2 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Instrumentos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   6300
      TabIndex        =   6
      Top             =   390
      Width           =   3525
   End
   Begin VB.Label Lbl_Ubicaci�n 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   60
      TabIndex        =   3
      Top             =   5970
      Width           =   6195
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Instrumentos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   60
      TabIndex        =   2
      Top             =   390
      Width           =   6195
   End
End
Attribute VB_Name = "Frm_AsignaPerfilProductos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Arbol_NodeClick(ByVal Node As MSComctlLib.Node)
Dim lReg As hFields
Dim lcRel_Perfiles_Nemotec As Class_Rel_Perfiles_Nemotec
  
  Call Sub_Limpiar

  If Node.Parent Is Nothing Then
    Call Sub_Carga_Instrumento_PRiesgo(Node)
    Exit Sub
  End If
  
  Call Sub_Bloquea_Puntero(Me)
  
  Lbl_Ubicaci�n.Caption = Node.FullPath
  
  Rem Carga los nemotecnicos de los instrumentos
  Set lcRel_Perfiles_Nemotec = New Class_Rel_Perfiles_Nemotec
  With lcRel_Perfiles_Nemotec
    .Campo("Id_Nemotecnico").Valor = Node.Tag
    If .Buscar_Perfiles_Nemotecnico Then
      For Each lReg In .Cursor
        On Error Resume Next
        Lista.ListItems("P" & lReg("ID_PERFIL_RIESGO").Value).Checked = True
        On Error GoTo 0
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcRel_Perfiles_Nemotec = Nothing
    
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Grabar()
Dim lItem As ListItem
Dim lResult As Boolean
Dim lcRel_Perfiles_Nemotec As Class_Rel_Perfiles_Nemotec
    
  Rem Si no tiene ningun elemento del arbol seleccionado
  If Arbol.SelectedItem Is Nothing Then
    Exit Sub
  End If
    
  Rem Si lo que tiene seleccionado no es un instrumento
  If Arbol.SelectedItem.Parent Is Nothing Then
    Call Sub_Guarda_Instrumentos
    Exit Sub
  End If
  
  Call Sub_Bloquea_Puntero(Me)
  
  Rem Inicia la Transaccion
  gDB.IniciarTransaccion
    
  lResult = True
  
  Rem Elimina todos los perfiles de riesgo con el id_nemotecnico
  Set lcRel_Perfiles_Nemotec = New Class_Rel_Perfiles_Nemotec
  With lcRel_Perfiles_Nemotec
    .Campo("Id_Nemotecnico").Valor = Arbol.SelectedItem.Tag
    If Not .Borrar_Perfiles_Nemotecnico Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      lResult = False
      GoTo ErrProcedure
    End If
  End With
  Set lcRel_Perfiles_Nemotec = Nothing
  
  Rem Guarda los perfiles chequeados
  For Each lItem In Lista.ListItems
    If lItem.Checked Then
      Set lcRel_Perfiles_Nemotec = New Class_Rel_Perfiles_Nemotec
      With lcRel_Perfiles_Nemotec
        .Campo("Id_Nemotecnico").Valor = Arbol.SelectedItem.Tag
        .Campo("Id_Perfil_Riesgo").Valor = lItem.Tag
        If Not .Guardar Then
          MsgBox .ErrMsg, vbCritical, Me.Caption
          lResult = False
          GoTo ErrProcedure
        End If
      End With
      Set lcRel_Perfiles_Nemotec = Nothing
    End If
  Next
    
ErrProcedure:
  If Err Then
    lResult = False
    Resume
  End If
  
  If lResult Then
    gDB.CommitTransaccion
  Else
    gDB.RollbackTransaccion
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Limpiar()
Dim lItem As ListItem
  
  For Each lItem In Lista.ListItems
    lItem.Checked = False
  Next
End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SAVE").Image = cBoton_Grabar
    .Buttons("EXIT").Image = cBoton_Salir
    .Buttons("REFRESH").Image = cBoton_Original
  End With
  
  With Toolbar_Perfiles
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("ALL").Image = "boton_seleccionar_todos"
    .Buttons("NALL").Image = "boton_seleccionar_ninguno"
  End With

  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargarDatos()
Dim lNode As Node
Dim lLinea As Long
Dim lReg As hFields
Dim lReg2 As hFields
Dim lInstrumentos As Class_Instrumentos
Dim lcNemotecnico As Class_Nemotecnicos
Dim lcPerfiles_Riesgo As Class_Perfiles_Riesgo
  
  Call Sub_Bloquea_Puntero(Me)
  
  Arbol.Nodes.Clear
  
  Set lInstrumentos = New Class_Instrumentos
  
  Set Arbol.ImageList = MDI_Principal.ImageListGlobal16
  Rem Carga los Instrumentos
  If lInstrumentos.Buscar Then
    For Each lReg In lInstrumentos.Cursor
      Set lNode = Arbol.Nodes.Add(, , "S" & lReg("COD_INSTRUMENTO").Value, lReg("DSC_INTRUMENTO").Value)
      lNode.Tag = lReg("COD_INSTRUMENTO").Value
      lNode.Bold = True
      lNode.Image = "close_folder"
      
      Rem Carga los nemotecnicos de los instrumentos
      Set lcNemotecnico = New Class_Nemotecnicos
      With lcNemotecnico
        .Campo("cod_Instrumento").Valor = lReg("COD_INSTRUMENTO").Value
        If .BuscarGrilla Then
          For Each lReg2 In .Cursor
            Call gRelogDB.AvanzaRelog
            With Arbol.Nodes.Add(lNode.Key, tvwChild, lNode.Key & "\" & lReg2("ID_NEMOTECNICO").Value, lReg2("NEMOTECNICO").Value)
              .Tag = lReg2("ID_NEMOTECNICO").Value
              .Image = "form_grilla"
            End With
          Next
        Else
          MsgBox .ErrMsg, vbCritical, Me.Caption
        End If
      End With
      Set lcNemotecnico = Nothing
    Next
  End If
  Set lInstrumentos = Nothing
           
  Lista.ListItems.Clear
  Set lReg = Nothing
  
  Rem Carga los perfiles de riesgo asociados a la empresa
  Set lcPerfiles_Riesgo = New Class_Perfiles_Riesgo
  With lcPerfiles_Riesgo
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = 0
        With Lista.ListItems.Add(, "P" & lReg("ID_PERFIL_RIESGO").Value, lReg("DSC_PERFIL_RIESGO").Value)
          .Tag = lReg("ID_PERFIL_RIESGO").Value
        End With
        lLinea = lLinea + 1
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcPerfiles_Riesgo = Nothing
  
  'Set Arbol.SelectedItem = Arbol.Nodes(1)
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Call Sub_Grabar
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Sub Toolbar_Perfiles_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lItem As ListItem
  
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ALL"
      For Each lItem In Lista.ListItems
        lItem.Checked = True
      Next
    Case "NALL"
      Call Sub_Limpiar
  End Select
End Sub

Private Sub Sub_Carga_Instrumento_PRiesgo(pNode As MSComctlLib.Node)
Dim lReg As hFields
Dim lcRel_PRiesgo_Instrumento As Class_Rel_PRiesgo_Instrumento
  
  Rem Carga los nemotecnicos de los instrumentos
  Set lcRel_PRiesgo_Instrumento = New Class_Rel_PRiesgo_Instrumento
  With lcRel_PRiesgo_Instrumento
    .Campo("cod_instrumento").Valor = pNode.Tag
    If .Buscar Then
      For Each lReg In .Cursor
        On Error Resume Next
        Lista.ListItems("P" & lReg("ID_PERFIL_RIESGO").Value).Checked = True
        On Error GoTo 0
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcRel_PRiesgo_Instrumento = Nothing
End Sub

Private Sub Sub_Guarda_Instrumentos()
Dim lItem As ListItem
Dim lResult As Boolean
Dim lcRel_PRiesgo_Instrumento As Class_Rel_PRiesgo_Instrumento
  
  On Error GoTo ErrProcedure
  
  Call Sub_Bloquea_Puntero(Me)
  
  Rem Inicia la Transaccion
  gDB.IniciarTransaccion
    
  lResult = True

  Rem Elimina todos los perfiles de riesgo con el cod_instrumento
  Set lcRel_PRiesgo_Instrumento = New Class_Rel_PRiesgo_Instrumento
  With lcRel_PRiesgo_Instrumento
    .Campo("cod_instrumento").Valor = Arbol.SelectedItem.Tag
    If Not .Borrar(Fnt_EmpresaActual) Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      lResult = False
      GoTo ErrProcedure
    End If
  End With
  Set lcRel_PRiesgo_Instrumento = Nothing
  
  Rem Guarda los perfiles chequeados
  For Each lItem In Lista.ListItems
    If lItem.Checked Then
      Set lcRel_PRiesgo_Instrumento = New Class_Rel_PRiesgo_Instrumento
      With lcRel_PRiesgo_Instrumento
        .Campo("cod_instrumento").Valor = Arbol.SelectedItem.Tag
        .Campo("Id_Perfil_Riesgo").Valor = lItem.Tag
        If Not .Guardar Then
          MsgBox .ErrMsg, vbCritical, Me.Caption
          lResult = False
          GoTo ErrProcedure
        End If
      End With
      Set lcRel_PRiesgo_Instrumento = Nothing
    End If
  Next

ErrProcedure:
  If Not Err.Number = 0 Then
    lResult = False
    Resume
  End If
  
  If lResult Then
    gDB.CommitTransaccion
  Else
    gDB.RollbackTransaccion
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub
