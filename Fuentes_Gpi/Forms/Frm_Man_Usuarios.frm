VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Man_Usuarios 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Usuarios"
   ClientHeight    =   4200
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   13155
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   13155
   Begin VB.Frame Frame1 
      Caption         =   "Usuarios"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   13005
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   120
         TabIndex        =   1
         Top             =   300
         Width           =   12765
         _cx             =   22516
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   6
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Usuarios.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   13155
      _ExtentX        =   23204
      _ExtentY        =   635
      ButtonWidth     =   2725
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   11
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Anular"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Anula un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Bloquear"
            Key             =   "BLOQUEAR"
            Object.ToolTipText     =   "Bloquea un usuario"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un elemento"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reset Clave"
            Key             =   "RESET"
            Description     =   "Resetea una contrase�a"
            Object.ToolTipText     =   "Resetea una contrase�a"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9480
         TabIndex        =   3
         Top             =   0
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Usuarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'=========================================================================================================
'====  Formulario         : Frm_Man Usuarios
'====  Descripci�n        : Este formulario permite Agregar, Modificar y Consultar los
'====                       permisos asociados a los distintos Usuarios.
'====  Desarrollado por   : Hans Muller R.
'====  Fecha Creaci�n     : 24/07/2006
'====  Fecha Modificaci�n :
'====  Base de Datos      : Oracle 8i     Usuario : CSBPI
'=========================================================================================================
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana
Dim accion As Boolean

Private Declare Function ShellExecute Lib "shell32.dll" Alias _
    "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, _
    ByVal lpFile As String, ByVal lpParameters As String, _
    ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
    
Private Const SW_SHOWNORMAL = 1
    
Private Const ERROR_FILE_NOT_FOUND = 2&
Private Const ERROR_PATH_NOT_FOUND = 3&
Private Const ERROR_BAD_FORMAT = 11&
Private Const SE_ERR_ACCESSDENIED = 5
Private Const SE_ERR_ASSOCINCOMPLETE = 27
Private Const SE_ERR_DDEBUSY = 30
Private Const SE_ERR_DDEFAIL = 29
Private Const SE_ERR_DDETIMEOUT = 28
Private Const SE_ERR_DLLNOTFOUND = 32
Private Const SE_ERR_FNF = 2
Private Const SE_ERR_NOASSOC = 31
Private Const SE_ERR_OOM = 8
Private Const SE_ERR_PNF = 3


Private Const PROCESS_QUERY_INFORMATION = &H400
Private Const STATUS_PENDING = &H103&



Private Declare Function OpenProcess Lib "kernel32" _
  (ByVal dwDesiredAccess As Long, _
   ByVal bInheritHandle As Long, _
   ByVal dwProcessId As Long) As Long
  
Private Declare Function GetExitCodeProcess Lib "kernel32" _
  (ByVal hProcess As Long, lpExitCode As Long) As Long
  
Private Declare Function CloseHandle Lib "kernel32" _
  (ByVal hObject As Long) As Long

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("BLOQUEAR").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("RESET").Image = cBoton_Refrescar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With
  BarraProceso.Visible = False
  Call Sub_CargaForm
  Call Sub_CargarDatos

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargarDatos()
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lcUsuarios As Class_Usuarios

  Call Sub_Bloquea_Puntero(Me)

  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If

'----------------------------------------------------------------------------------------
'-- Carga datos de roles correspondientes a la empresa de operaci�n
'----------------------------------------------------------------------------------------
  Set lcUsuarios = New Class_Usuarios
  With lcUsuarios
    .Campo("id_Empresa").Valor = Fnt_EmpresaActual
    If .Buscar_Usuarios Then
      Grilla.Rows = 1
      For Each lReg In .Cursor
        lLinea = Grilla.Rows
        Grilla.Rows = lLinea + 1
        
        SetCell Grilla, lLinea, "colum_pk", NVL(lReg("id_Usuario").Value, ""), False
        SetCell Grilla, lLinea, "dsc_Usuario", NVL(lReg("dsc_Usuario").Value, ""), False
        SetCell Grilla, lLinea, "Est_Usuario", NVL(lReg("dsc_Estado").Value, ""), False
        SetCell Grilla, lLinea, "Id_Tipo_Estado", NVL(lReg("Id_Tipo_Estado").Value, ""), False
        SetCell Grilla, lLinea, "Asesor", NVL(lReg("Nombre").Value, ""), False
        SetCell Grilla, lLinea, "Email", NVL(lReg("EMAIL_USUARIO").Value, ""), False
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcUsuarios = Nothing
  
  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  With Grilla
    If Not Trim(GetCell(Grilla, Row, "colum_pk")) = "" Then
      Cancel = True
    End If
  End With
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "UPDATE"
      Call Grilla_DblClick
    Case "REFRESH"
      Call Sub_CargarDatos
    Case "DEL"
      Call Sub_EliminarItem
    Case "BLOQUEAR"
      Call Sub_Bloquear
    Case "ADD"
      accion = True
      Call Sub_Agregar
    Case "RESET"
      Call Sub_Reset
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Grilla_DblClick()
Dim lKey As String
accion = False
  With Grilla
    If .Row > 0 Then
      lKey = GetCell(Grilla, .Row, "colum_pk")
      
      Call Sub_EsperaVentana(lKey)
      
      
    End If
  End With
  
End Sub

Private Sub Sub_Agregar()
  Call Sub_EsperaVentana(cNewEntidad)
End Sub

Private Sub Sub_EsperaVentana(ByVal pkey)
Dim lForm As Frm_Usuarios
Dim lNombre As String
Dim lNom_Form As String
  
  Me.Enabled = False
  lNom_Form = "Frm_Usuarios"
  
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lNombre = Me.Name
    
    Set lForm = New Frm_Usuarios
    
    If accion = True Then
        lForm.vg_NuevoUsuario = True
    Else
        lForm.vg_NuevoUsuario = False
    End If
    
    Call lForm.Fnt_Modificar(pkey, pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      Call Sub_CargarDatos
    Else
      Exit Sub
    End If
  End If
  Me.Enabled = True
End Sub

Private Sub Sub_CargaForm()
  'Limpia la grilla
  Grilla.Rows = 1
End Sub

Private Sub Sub_EliminarItem()
Dim lID As String
Dim lcUsuario As Class_Usuarios

  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea Anular el Usuario " & Grilla.Text & "?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      lID = GetCell(Grilla, Grilla.Row, "colum_pk")
      If Not lID = "" Then
        Set lcUsuario = New Class_Usuarios
        With lcUsuario
          .Campo("Id_Usuario").Valor = lID
          If .Anular Then
            MsgBox "Usuario Anulado correctamente.", vbInformation, Me.Caption
          Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
          End If
        End With
        Set lcUsuario = Nothing
      End If
      Call Sub_CargarDatos
    End If
  End If
End Sub

Private Sub Sub_Bloquear()
Dim lID As String
Dim lcUsuario As Class_Usuarios

  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea Bloquear el Usuario " & Grilla.Text & "?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      lID = GetCell(Grilla, Grilla.Row, "colum_pk")
      If Not lID = "" Then
        Set lcUsuario = New Class_Usuarios
        With lcUsuario
          .Campo("Id_Usuario").Valor = lID
          If .Bloquear Then
            MsgBox "Usuario Bloqueado correctamente.", vbInformation, Me.Caption
          Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
          End If
        End With
        Set lcUsuario = Nothing
      End If
      Call Sub_CargarDatos
    End If
  End If
End Sub
Private Sub Sub_Reset()
Dim lID As String
Dim lcUsuario As Class_Usuarios

Call Sub_Bloquea_Puntero(Me)

  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea Resetear la clave del Usuario " & Grilla.Text & "?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      lID = GetCell(Grilla, Grilla.Row, "colum_pk")
      If Not lID = "" Then
        Set lcUsuario = New Class_Usuarios
        With lcUsuario
          .Campo("Id_Usuario").Valor = lID
          
            Dim parametros As String
            parametros = GetCell(Grilla, Grilla.Row, "dsc_Usuario") & "|" & gId_Empresa
            Call Exec_EnviaClavexMail(App.Path & "\ServicioMail.exe " & parametros)
            
          
          If .Reset Then
            MsgBox "Clave reseteada correctamente.", vbInformation, Me.Caption
          Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
          End If
        End With
        Set lcUsuario = Nothing
      End If
      Call Sub_CargarDatos
    End If
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Exec_EnviaClavexMail(programa As String)

    Dim handle_Process As Long
    Dim id_process As Long
    Dim lp_ExitCode As Long
      
    ' Abre el proceso con el shell
    id_process = Shell(programa, 0) '0 permite trabajar en segundo plano
      
    ' handle del proceso
    handle_Process = OpenProcess(PROCESS_QUERY_INFORMATION, False, id_process)
      
    ' Mientras lp_ExitCode = STATUS_PENDING, se ejecuta el do
    Do
  
        Call GetExitCodeProcess(handle_Process, lp_ExitCode)
          
        DoEvents
     
    Loop While lp_ExitCode = STATUS_PENDING
      
    ' fin
    ' Cierra
    Call CloseHandle(handle_Process)
  
    'MsgBox "Se cerr� el " & programa, vbInformation
  
End Sub

  
Private Sub Abrir_Archivo(ElArchivo As String)
      
    'Abre el archivo
    Select Case ShellExecute(hwnd, "open", ElArchivo, vbNullString, _
                            vbNullString, SW_SHOWNORMAL)
  
        'Retorno de ShellExecute
          
        Case 0
            MsgBox "Fuera de memoria o de recursos "
        Case ERROR_BAD_FORMAT
            MsgBox "Fromato inv�lido de archivo"
        Case SE_ERR_ACCESSDENIED
            MsgBox " Acceso denegado al Intentar abrir el archivo"
        Case SE_ERR_ASSOCINCOMPLETE
            MsgBox "Extensi�n incompleta o inv�lida del nombre del archivo"
        Case SE_ERR_DDEBUSY
            MsgBox " DDE ocupado "
        Case SE_ERR_DDEFAIL
            MsgBox " Fall en la Transacci�n DDE"
        Case SE_ERR_DDETIMEOUT
            MsgBox "Petici�n DDE fuera de tiempo"
        Case SE_ERR_DLLNOTFOUND
            MsgBox "DLL not Found"
        Case ERROR_FILE_NOT_FOUND, SE_ERR_FNF
            MsgBox "File not found"
        Case SE_ERR_NOASSOC
            MsgBox " El archivo no est� asociado a ninguna aplicaci�n"
        Case SE_ERR_OOM
            MsgBox " Fuera de memoria"
        Case ERROR_PATH_NOT_FOUND, SE_ERR_PNF
            MsgBox " Path not found "
'        Case SE_ERR_SHARE
'            MsgBox "Sharing violation"
    End Select
  
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Listado de Usuarios" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

