VERSION 5.00
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Frm_Valorizador_RF 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Valorizador Masivo Renta Fija"
   ClientHeight    =   1860
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   8265
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1860
   ScaleWidth      =   8265
   Begin VB.Frame Frm_Excel 
      Caption         =   "Valorizar Renta Fija Masiva"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1365
      Left            =   75
      TabIndex        =   0
      Top             =   420
      Width           =   8145
      Begin MSComDlg.CommonDialog Cmd_AbreArchivo 
         Left            =   3000
         Top             =   240
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin hControl2.hTextLabel Txt_Archivo 
         Height          =   315
         Left            =   150
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   540
         Width           =   6390
         _ExtentX        =   11271
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Nombre Archivo"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin MSComctlLib.Toolbar Toolbar_Buscar_Archivo 
         Height          =   330
         Left            =   6660
         TabIndex        =   4
         Top             =   540
         Width           =   1110
         _ExtentX        =   1958
         _ExtentY        =   582
         ButtonWidth     =   1746
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Buscar"
               Key             =   "BUSCAR"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8265
      _ExtentX        =   14579
      _ExtentY        =   635
      ButtonWidth     =   1984
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Valorizar"
            Key             =   "VAL"
            Description     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   2
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Valorizador_RF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Enum eExcel
  eGD_Fecha = 1
  eGD_Nemotecnico
  eGD_Tasa
  eGD_Nominales
  eGD_Valor_Prueba
  eGD_Valor_CSGPI
  eGD_Error = 8
End Enum

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("VAL").Image = cBoton_Modificar
      .Buttons("REFRESH").Image = cBoton_Refrescar
  End With

  With Toolbar_Buscar_Archivo
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("BUSCAR").Image = "boton_grilla_buscar"
  End With
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_Buscar_Archivo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "BUSCAR"
      Call Sub_Busca_Planilla
  End Select
End Sub

Private Sub Sub_Busca_Planilla()

On Error GoTo Cmd_BuscarArchivo_Err

  With Cmd_AbreArchivo
    .CancelError = True
    .InitDir = "C:\Mis documentos\"
    .Filter = "Excel (*.xls)|*.xls"
    .Flags = cdlOFNFileMustExist
    .ShowOpen
    Rem se guarda el path completo, que incluye el nombre de archivo
    Txt_Archivo.Text = .FileName
  End With
  
  Exit Sub

Cmd_BuscarArchivo_Err:
  If Err = 32755 Then 'usuario presiona el boton Cancelar del Command Dialog
    Exit Sub
  End If

End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Txt_Archivo.Text = ""
    Case "EXIT"
      Unload Me
    Case "VAL"
      Call Sub_Cargar_Planilla
  End Select
End Sub

Private Sub Sub_Cargar_Planilla()
Dim ERRORROUTINE As String

  If Txt_Archivo.Text = "" Then
    MsgBox "Debe seleccionar un archivo para cargar.", vbExclamation
    Exit Sub
  End If

  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_Valorizar_Masiva_RF(Txt_Archivo.Text)
      
  Call Sub_Desbloquea_Puntero(Me)
  Exit Sub

Error_Cmd_Cargar_Click:
  MsgBox "Ha ocurrido un error al tratar de cargar los datos.", vbExclamation
  Resume Error_Cmd_Cargar_Click1
    
Error_Cmd_Cargar_Click1:
  ERRORROUTINE = "GeneraInfor"
    
End Sub

Private Sub Sub_Valorizar_Masiva_RF(pNombre_Archivo As String)
Dim lExcel As New Excel.Application
Dim lLibro As Excel.Workbook
Dim lHoja As Excel.Worksheet
'-----------------------------------
Dim lFila As Double
Dim lFecha As String
Dim lNemotecnico As String
Dim lTasa As String
Dim lNominales As String
Dim lMsg_Error As String
'-----------------------------------
Dim lcNemotecnicos As Class_Nemotecnicos
Dim lId_Nemtotecnico As String
'-----------------------------------
Dim lcBonos As Class_Bonos

On Error GoTo ErrProcedure
  
  lExcel.Visible = False
  Rem Abre el libro excel
  Set lLibro = lExcel.Workbooks.Open(pNombre_Archivo, ReadOnly:=False, Editable:=True)
  Rem abre la hoja del libro a procesar
  Set lHoja = lLibro.Worksheets(1)

  With lHoja
    Rem Setea la linea de inicio de recorrido del excel
    lFila = 2
    Do While Not .Cells(lFila, eExcel.eGD_Nemotecnico) = ""
      lId_Nemtotecnico = ""
      lFecha = ""
      lNemotecnico = ""
      lTasa = ""
      lNominales = ""
    
      lFecha = .Cells(lFila, eExcel.eGD_Fecha)
      lNemotecnico = .Cells(lFila, eExcel.eGD_Nemotecnico)
      lTasa = .Cells(lFila, eExcel.eGD_Tasa)
      lNominales = .Cells(lFila, eExcel.eGD_Nominales)
      
      If (Not lFecha = "") And (Not lTasa = "") And (Not lNominales = "") Then
        Set lcNemotecnicos = New Class_Nemotecnicos
        lcNemotecnicos.Campo("nemotecnico").Valor = lNemotecnico
        If lcNemotecnicos.Buscar_Nemotecnico(gcPROD_RF_NAC, False) Then
          lId_Nemtotecnico = lcNemotecnicos.Campo("id_nemotecnico").Valor
          If Not lId_Nemtotecnico = "0" Then
            Rem Valoriza el Nemotecnico
            Set lcBonos = New Class_Bonos
            .Cells(lFila, eExcel.eGD_Valor_CSGPI) = lcBonos.ValorizaPapel(lId_Nemtotecnico, lFecha, lTasa, lNominales, False, lMsg_Error)
                   
            If Not lMsg_Error = "" Then
              .Cells(lFila, eExcel.eGD_Error) = lMsg_Error
            End If
            
          Else
            .Cells(lFila, eExcel.eGD_Error) = "El Nemot�cnico est� vencido en el sistema CSGPI."
          End If
        Else
          .Cells(lFila, eExcel.eGD_Error) = lcNemotecnicos.ErrMsg
        End If
      Else
        .Cells(lFila, eExcel.eGD_Error) = "No se puede Valorizar porque hay datos en blanco en la planilla Excel."
      End If
      
      lFila = lFila + 1
    Loop
  End With
  
  lLibro.Save
  
  MsgBox "Terminado el proceso de carga de la Planilla Excel", vbInformation, Me.Caption
  
ErrProcedure:
  If Not Err.Number = 0 Then
    If MsgBox("Error en la carga de planilla Excel." & vbCr & vbCr & Err.de, vbApplicationModal + vbCritical + vbOKCancel, Me.Caption) = vbRetry Then
      Resume
    End If
  End If
  
  lExcel.DisplayAlerts = False
  lExcel.Quit
  
End Sub

'Private Sub Sub_Valorizar()
'Dim lExcel As Excel.Application
'Dim lLibro As Excel.Workbook
'Dim lHoja As Excel.Worksheet
''-------------------------------
'Dim lNombreArchivo As String
'Dim lFila As Double
'Dim msgError As String
''-------------------------------
'Dim lNemotecnico As Class_Nemotecnicos
'Dim lCursor_Nem As hRecord
'Dim lReg_Nem As hFields
'
'On Error GoTo ErrProcedure
'
'  Me.Enabled = False
'
'  Call Sub_Bloquea_Puntero(Me)
'
'  With Cmd_GrabaArchivo
'    .CancelError = True
'    .InitDir = "C:\Mis documentos\"
'    .Filter = "Excel (*.xls)|*.xls"
'    .Flags = cdlOFNCreatePrompt
'    .FileName = "Valorizador Renta Fija"
'    .ShowSave
'    lNombreArchivo = .FileName
'  End With
'
'  If lNombreArchivo = "" Then GoTo ErrProcedure
'
'  Rem Busca los nemotecnicos en el GPI que sean Renta Fija
'  Set lNemotecnico = New Class_Nemotecnicos
'  With lNemotecnico
'    If .BuscarView(gcPROD_RF_NAC) Then
'      Set lCursor_Nem = .Cursor
'    Else
'      GoTo ErrProcedure
'    End If
'  End With
'
'  Set lExcel = CreateObject("Excel.Application")
'  Set lLibro = lExcel.Workbooks.Add
'  Set lHoja = lLibro.Worksheets(1)
'
'  lExcel.Visible = False
'
'  Rem Cabecera
'  lHoja.Cells(1, 1).Value = "Fecha"
'  lHoja.Cells(1, 2).Value = "Nemot�cnico"
'  lHoja.Cells(1, 3).Value = "Tasa"
'  lHoja.Cells(1, 4).Value = "Nominales"
'  lHoja.Cells(1, 5).Value = "Valor Prueba"
'  lHoja.Cells(1, 6).Value = "Valor CSGPI"
'
'  Rem Carga de Datos
'  lFila = 2
'  For Each lReg_Nem In lCursor_Nem
'    With lHoja
'      .Cells(lFila, eExcel.eGD_Fecha) = Format(Now, cFormatDate)
'      .Cells(lFila, eExcel.eGD_Nemotecnico) = lReg_Nem("nemotecnico").Value
'      .Cells(lFila, eExcel.eGD_Tasa) = "5.0"
'      .Cells(lFila, eExcel.eGD_Nominales) = "1000"
'      .Cells(lFila, eExcel.eGD_Valor_Prueba) = ""
'      .Cells(lFila, eExcel.eGD_Valor_CSGPI) = ""
'    End With
'    lFila = lFila + 1
'  Next
'
'  lHoja.SaveAs lNombreArchivo
'  lLibro.Save
'  Beep
'  lExcel.Visible = True
'  lExcel.DisplayAlerts = True
'
'ErrProcedure:
'  Rem Usuario presiona el boton Cancelar del Command Dialog
'  Rem o presiona No o Cancelar en el dialogo "ya existe archivo...desea reemplazarlo"
'  If Err.Number = 32755 Or Err.Number = 1004 Then
'    Me.Enabled = True
'    Call Sub_Desbloquea_Puntero(Me)
'    Exit Sub
'  End If
'  If Err.Number <> 0 Then
'    msgError = "Error en la exportaci�n de datos a la planilla Excel."
'    If MsgBox(msgError & vbCr & vbCr & Err.Description, vbRetryCancel + vbCritical, Me.Caption) = vbRetry Then
'      Resume
'    End If
'    lExcel.DisplayAlerts = False
'    lExcel.Quit
'  End If
'
'  Me.Enabled = True
'  Call Sub_Desbloquea_Puntero(Me)
'End Sub

