VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Consulta_Ultimos_Cierres 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta Ultimos Cierres"
   ClientHeight    =   4200
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   10155
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   10155
   Begin VB.Frame Frame1 
      Caption         =   "Clientes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   10035
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   120
         TabIndex        =   3
         Top             =   300
         Width           =   9825
         _cx             =   17330
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   5
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   1000
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Consulta_Ultimos_Cierres.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Description     =   "Actualiza los datos de la ventana."
            Object.ToolTipText     =   "Actualiza los datos de la ventana."
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Consulta_Ultimos_Cierres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fTipo_Permiso As String

Public Sub Fnt_MostrarVentana(ByVal pTipo_Permiso As String)
  
  fTipo_Permiso = pTipo_Permiso
  
  Load Me
  
  If pTipo_Permiso = cTPermiso_Lectura Then
    With Toolbar.Buttons
      .Item(1).Enabled = False
      .Item(2).Enabled = False
      .Item(3).Enabled = False
    End With
  End If

End Sub

Private Sub Form_Load()
  
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16

      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With
  
  Call Sub_CargarDatos

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargarDatos()

'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lReg    As hCollection.hFields
Dim lReg_Cierres As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lcCierres_Cuenta As Class_Cierres_Cuentas
Dim pFecha As Date


  Call Sub_FormControl_Color(Me.Controls)

  Call Sub_Bloquea_Puntero(Me)
  
  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Set lcCierres_Cuenta = New Class_Cierres_Cuentas
'  Set lcCuenta = New Class_Cuentas
  
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  Grilla.Rows = 1
  lcCuenta.Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
  'lcCuenta.Campo("cod_estado").Valor = "H"
  If lcCuenta.Buscar(True) Then
  pFecha = Fnt_FechaServidor
    For Each lReg In lcCuenta.Cursor
      lLinea = Grilla.Rows
      
      
      lcCierres_Cuenta.Campo("id_cuenta").Valor = lReg("id_cuenta").Value
      lcCierres_Cuenta.Campo("fecha_cierre").Valor = pFecha
      
      If lcCierres_Cuenta.BuscarUltimo Then
          
          For Each lReg_Cierres In lcCierres_Cuenta.Cursor
            Call Grilla.AddItem("")
            Call SetCell(Grilla, lLinea, "colum_pk", lReg("id_cuenta").Value)
            Call SetCell(Grilla, lLinea, "num_cuenta", lReg("num_cuenta").Value)
            Call SetCell(Grilla, lLinea, "dsc_cuenta", NVL(lReg("dsc_cuenta").Value, ""))
            Call SetCell(Grilla, lLinea, "nombre_cliente", NVL(lReg("nombre_cliente").Value, ""))
            Call SetCell(Grilla, lLinea, "fecha_ultimo_cierre", NVL(lReg_Cierres("fecha_cierre").Value, ""))
          Next
            
      End If
      
    Next
  Else
    MsgBox lcCuenta.ErrMsg, vbCritical, Me.Caption
  End If
  Set lcCuenta = Nothing


  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      Call Sub_CargarDatos
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub


Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generación del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Listado Últimos Cierres" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub



Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub




