VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Operacion_Acciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ingreso de Acciones"
   ClientHeight    =   8295
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11880
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8295
   ScaleWidth      =   11880
   Begin VB.Frame Pnl_Cuenta 
      Caption         =   "Cuenta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1335
      Left            =   120
      TabIndex        =   18
      Top             =   390
      Width           =   4695
      Begin VB.CommandButton Btn_Venta 
         Caption         =   "Venta"
         Height          =   375
         Left            =   2760
         TabIndex        =   2
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton Btn_Compra 
         Caption         =   "Compra"
         Height          =   375
         Left            =   360
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4200
         Picture         =   "Frm_Operacion_Acciones.frx":0000
         TabIndex        =   14
         Top             =   360
         Width           =   375
      End
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   345
         Left            =   120
         TabIndex        =   0
         Tag             =   "OBLI=S;CAPTION=Cuentas"
         Top             =   360
         Width           =   4005
         _ExtentX        =   7064
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Acciones.frx":030A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
   End
   Begin VB.Frame Pnl_Operacion 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   6495
      Left            =   120
      TabIndex        =   16
      Top             =   1770
      Width           =   11655
      Begin VB.Frame Pnl_DetalleOperacion 
         Caption         =   "Detalle Operaci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   3105
         Left            =   120
         TabIndex        =   21
         Top             =   180
         Width           =   11415
         Begin VSFlex8LCtl.VSFlexGrid Grilla 
            Height          =   2325
            Left            =   90
            TabIndex        =   7
            Top             =   240
            Width           =   11200
            _cx             =   19756
            _cy             =   4101
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            SelectionMode   =   0
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   50
            Cols            =   21
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   $"Frm_Operacion_Acciones.frx":03B4
            ScrollTrack     =   0   'False
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin hControl2.hTextLabel Txt_TotalDetalle 
            Height          =   315
            Left            =   7080
            TabIndex        =   41
            Top             =   2640
            Width           =   4095
            _ExtentX        =   7223
            _ExtentY        =   556
            LabelWidth      =   2000
            Caption         =   "Monto Total Detalle"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
      End
      Begin VB.Frame Pnl_DatosOperacion 
         Caption         =   "Datos Operaci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   2655
         Left            =   120
         TabIndex        =   17
         Top             =   3720
         Width           =   11415
         Begin VB.CheckBox chkAporteRetiro 
            Caption         =   "� Aporte/Retiro de Capital ?"
            Height          =   375
            Left            =   4170
            TabIndex        =   24
            Top             =   270
            Value           =   1  'Checked
            Width           =   2415
         End
         Begin hControl2.hTextLabel Txt_Porcentaje_Comision 
            Height          =   315
            Left            =   7620
            TabIndex        =   8
            Tag             =   "OBLI"
            Top             =   360
            Width           =   1350
            _ExtentX        =   2381
            _ExtentY        =   556
            LabelWidth      =   800
            TextMinWidth    =   500
            Caption         =   "Comisi�n"
            Text            =   "0.00%"
            Text            =   "0.00%"
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "##0.00%"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Derechos 
            Height          =   315
            Left            =   9000
            TabIndex        =   11
            Tag             =   "OBLI"
            Top             =   720
            Width           =   1995
            _ExtentX        =   3519
            _ExtentY        =   556
            LabelWidth      =   15
            TextMinWidth    =   500
            Caption         =   ""
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Iva 
            Height          =   315
            Left            =   7620
            TabIndex        =   13
            Tag             =   "OBLI"
            Top             =   1440
            Width           =   3375
            _ExtentX        =   5953
            _ExtentY        =   556
            LabelWidth      =   1400
            TextMinWidth    =   500
            Caption         =   " Iva (%)"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Comision 
            Height          =   315
            Left            =   9000
            TabIndex        =   9
            Tag             =   "OBLI"
            Top             =   360
            Width           =   1995
            _ExtentX        =   3519
            _ExtentY        =   556
            LabelWidth      =   15
            TextMinWidth    =   500
            Caption         =   ""
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Gastos 
            Height          =   315
            Left            =   7620
            TabIndex        =   12
            Tag             =   "OBLI"
            Top             =   1080
            Width           =   3375
            _ExtentX        =   5953
            _ExtentY        =   556
            LabelWidth      =   1400
            TextMinWidth    =   500
            Caption         =   "Gastos"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Porcentaje_Derechos 
            Height          =   315
            Left            =   7620
            TabIndex        =   10
            Tag             =   "OBLI"
            Top             =   720
            Width           =   1350
            _ExtentX        =   2381
            _ExtentY        =   556
            LabelWidth      =   800
            TextMinWidth    =   500
            Caption         =   "Derechos "
            Text            =   "0.00%"
            Text            =   "0.00%"
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "##0.00%"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_MontoTotal 
            Height          =   345
            Left            =   7620
            TabIndex        =   22
            Top             =   2250
            Width           =   3375
            _ExtentX        =   5953
            _ExtentY        =   609
            LabelWidth      =   1400
            Caption         =   "Monto Total"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   16761024
            BackColorTxt    =   16761024
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_MontoNeto 
            Height          =   315
            Left            =   7620
            TabIndex        =   23
            Top             =   1800
            Width           =   3375
            _ExtentX        =   5953
            _ExtentY        =   556
            LabelWidth      =   1400
            Caption         =   "Neto"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Num_Operacion 
            Height          =   315
            Left            =   120
            TabIndex        =   25
            Top             =   2220
            Visible         =   0   'False
            Width           =   3285
            _ExtentX        =   5794
            _ExtentY        =   556
            LabelWidth      =   1400
            Caption         =   "N� Operaci�n"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_DiasVigencia 
            Height          =   315
            Left            =   1470
            TabIndex        =   26
            Top             =   3240
            Visible         =   0   'False
            Width           =   2715
            _ExtentX        =   4789
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   870
            Caption         =   "Dias Vigencia"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0"
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_FechaVigencia 
            Height          =   315
            Left            =   1470
            TabIndex        =   27
            Top             =   2880
            Visible         =   0   'False
            Width           =   3300
            _ExtentX        =   5821
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   1000
            Caption         =   "Fecha Vigencia"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin TrueDBList80.TDBCombo Cmb_Representantes 
            Height          =   345
            Left            =   1530
            TabIndex        =   28
            Top             =   1770
            Width           =   4365
            _ExtentX        =   7699
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Acciones.frx":077C
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Contraparte 
            Height          =   345
            Left            =   1530
            TabIndex        =   29
            Top             =   990
            Width           =   4365
            _ExtentX        =   7699
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Acciones.frx":0826
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Traders 
            Height          =   345
            Left            =   1530
            TabIndex        =   30
            Top             =   1380
            Width           =   4365
            _ExtentX        =   7699
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Acciones.frx":08D0
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin hControl2.hTextLabel Txt_FechaIngreso_Real 
            Height          =   315
            Left            =   120
            TabIndex        =   31
            Top             =   240
            Width           =   3330
            _ExtentX        =   5874
            _ExtentY        =   556
            LabelWidth      =   1400
            Caption         =   "Fecha Operaci�n"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin MSComCtl2.DTPicker Dtp_FechaLiquidacion 
            Height          =   345
            Left            =   2280
            TabIndex        =   32
            Tag             =   "OBLI"
            Top             =   600
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   609
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   71434241
            CurrentDate     =   39883
         End
         Begin TrueDBList80.TDBCombo Cmb_FechaLiquidacion 
            Height          =   345
            Left            =   1530
            TabIndex        =   33
            Top             =   600
            Width           =   705
            _ExtentX        =   1244
            _ExtentY        =   609
            _LayoutType     =   4
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   1
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=1"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Acciones.frx":097A
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Named:id=33:Normal"
            _StyleDefs(35)  =   ":id=33,.parent=0"
            _StyleDefs(36)  =   "Named:id=34:Heading"
            _StyleDefs(37)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(38)  =   ":id=34,.wraptext=-1"
            _StyleDefs(39)  =   "Named:id=35:Footing"
            _StyleDefs(40)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(41)  =   "Named:id=36:Selected"
            _StyleDefs(42)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(43)  =   "Named:id=37:Caption"
            _StyleDefs(44)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(45)  =   "Named:id=38:HighlightRow"
            _StyleDefs(46)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=39:EvenRow"
            _StyleDefs(48)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(49)  =   "Named:id=40:OddRow"
            _StyleDefs(50)  =   ":id=40,.parent=33"
            _StyleDefs(51)  =   "Named:id=41:RecordSelector"
            _StyleDefs(52)  =   ":id=41,.parent=34"
            _StyleDefs(53)  =   "Named:id=42:FilterBar"
            _StyleDefs(54)  =   ":id=42,.parent=33"
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Operacion 
            Height          =   345
            Left            =   1530
            TabIndex        =   38
            Tag             =   "OBLI"
            Top             =   240
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   609
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   71434241
            CurrentDate     =   38768
         End
         Begin VB.Label lbl_fecha_ingreso 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Operaci�n"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   39
            Top             =   240
            Width           =   1395
         End
         Begin VB.Label Label5 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Contraparte"
            Height          =   345
            Left            =   120
            TabIndex        =   37
            Top             =   990
            Width           =   1395
         End
         Begin VB.Label Label1 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Trader"
            Height          =   345
            Left            =   120
            TabIndex        =   36
            Top             =   1380
            Width           =   1395
         End
         Begin VB.Label Label4 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Representantes"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   35
            Top             =   1770
            Width           =   1395
         End
         Begin VB.Label lbl_fecha_liquidacion 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Liquidaci�n"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   34
            Top             =   600
            Width           =   1395
         End
      End
      Begin MSComctlLib.Toolbar Toolbar_Operacion 
         Height          =   330
         Left            =   2880
         TabIndex        =   40
         Top             =   3360
         Width           =   5460
         _ExtentX        =   9631
         _ExtentY        =   582
         ButtonWidth     =   4128
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Ingresar Datos Operaci�n"
               Key             =   "OPERACION"
               Description     =   "Ingresa Datos Operaci�n"
               Object.ToolTipText     =   "Ingresa Datos Operaci�n"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Ingresar Detalle Operaci�n"
               Key             =   "DETALLEOPERACION"
               Description     =   "Ingresar detalle de operaci�n"
               Object.ToolTipText     =   "Ingresar detalle de operaci�n"
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Pnl_Cliente 
      Caption         =   "Cliente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1335
      Left            =   4920
      TabIndex        =   15
      Top             =   390
      Width           =   6855
      Begin hControl2.hTextLabel Txt_Rut 
         Height          =   315
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   2940
         _ExtentX        =   5186
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "RUT"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Nombres 
         Height          =   315
         Left            =   240
         TabIndex        =   5
         Top             =   600
         Width           =   6210
         _ExtentX        =   10954
         _ExtentY        =   556
         LabelWidth      =   1000
         Caption         =   "Nombres"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Perfil 
         Height          =   315
         Left            =   3360
         TabIndex        =   4
         Top             =   240
         Width           =   3060
         _ExtentX        =   5398
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "Perfil Riesgo"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel txtNombreAsesor 
         Height          =   315
         Left            =   240
         TabIndex        =   6
         Top             =   960
         Width           =   6210
         _ExtentX        =   10954
         _ExtentY        =   556
         LabelWidth      =   1000
         Caption         =   "Asesor"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Description     =   "Inicializa valores para un nuevo ingreso"
            Object.ToolTipText     =   "Inicializa valores para un nuevo ingreso"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   20
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Operacion_Acciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim edrow%, edcol%, edKey%

Public fKey As String
Dim fConsulta_Operacion As Boolean
'------------------------------------
Dim fSalir              As Boolean
Dim fEstadoOK           As Boolean
Dim fOperacion          As String
Dim fId_Cuenta          As String
Dim fId_Cliente         As Double
Dim fFormOri            As Form
Dim fTipo_Operacion     As String
Dim fCod_Instrumento    As String
Dim fModif_Grilla       As Boolean
Dim fId_Nemotecnico     As String
Dim fDsc_Nemotecnico    As String
Dim fEmisor             As Long
Dim fValor_Iva          As Double
Dim fFecha_Operacion    As Date
Dim fDsc_Operacion      As String
Dim fTabIndex           As Integer
Dim bEliminaLinea       As Boolean
Dim fId_Moneda_Transaccion As Integer
Dim fTasa_Emision       As Double
Dim fId_Operacion       As String
Dim bOper_Fecha_Anterior As Boolean
Dim lTipoPrecio         As String
Dim lTipoInversion      As String
Dim fRowGrilla          As Long
Dim fColGrilla          As Long
Dim fTipoInversion      As String
Dim fTipoPrecio         As String
Dim bModificaCelda      As Boolean

Const fc_EsMoneda_Pago = "S" ' solo las monedas que sean moneda de pago
Const fc_Mercado = "N"
Const MAX_COLUMNAS = 20
Const c_Id_Moneda = 0
Const c_Id_Nemotecnico = 1
Const c_cod_operacion = 2
Const c_id_emisor = 3
Const c_id_moneda_pago = 4
Const c_precio_historico = 5
Const c_id_mov_activo = 6
Const c_id_operacion_detalle = 7
Const c_tasa_emision = 8
Const c_plazo = 9
Const c_flg_vende_todo = 10
Const c_KEY_TIPO_PRECIO = 11
Const c_nemo = 12
Const c_TipoInversion = 13
Const c_emisor = 14
Const c_moneda = 15
Const c_cantidad_nemo = 16
Const c_cantidad = 17
Const c_tipoprecio = 18
Const c_precio = 19
Const c_monto = 20

Const vb_vs_numerico As Integer = 1
Const vb_vs_alfanumerico As Integer = 2
Const vb_vs_decimal As Integer = 3
Const vb_vs_fecha As Integer = 4
Const vb_vs_tipoinversion As Integer = 5
Const vb_vs_tipoprecio As Integer = 6

Enum Tipos
   Numerico = vb_vs_numerico
   AlfaNumerico = vb_vs_alfanumerico
   Decimales = vb_vs_decimal
   Fecha = vb_vs_fecha
   TipoInversion = vb_vs_tipoinversion
   TipoPrecio = vb_vs_tipoprecio
End Enum

Public Function Mostrar(pTipo_Operacion As String, _
                        pCod_Instrumento As String, _
                        pOper_Fecha_Anterior As Boolean, _
                        Optional pId_Operacion As String = "") As Boolean

  
'    If Fnt_Verifica_Feriado(Fnt_FechaServidor) And Not pOper_Fecha_Anterior Then
'        Mostrar = False
'        MsgBox "Solo se pueden ingresar operaciones en d�as habiles.", vbExclamation, Me.Caption
'        Unload Me
'        Exit Function
'    End If
    
    fTipo_Operacion = pTipo_Operacion
    fCod_Instrumento = gcINST_ACCIONES_NAC
    fId_Operacion = pId_Operacion
    bOper_Fecha_Anterior = pOper_Fecha_Anterior
    
    If fTipo_Operacion = gcOPERACION_Custodia Or _
          fTipo_Operacion = gcOPERACION_Custodia_SusCrip Or _
          fTipo_Operacion = gcOPERACION_Custodia_NoCapital Then

        lbl_fecha_liquidacion.Visible = False
        Cmb_FechaLiquidacion.Visible = False
        Dtp_FechaLiquidacion.Visible = False

    End If
    chkAporteRetiro.Value = False
    
      
    Load Me
'    Mostrar = fEstadoOK
    
End Function

Private Sub Form_Load()
 
    With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SAVE").Image = cBoton_Grabar
        .Buttons("REFRESH").Image = cBoton_Original
        .Buttons("EXIT").Image = cBoton_Salir
    End With
    
    With Toolbar_Operacion
    Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("OPERACION").Image = cBoton_Modificar
        .Buttons("DETALLEOPERACION").Image = cBoton_Agregar_Grilla
    End With
    
    Call Sub_CargaForm
    lTipoPrecio = "Mercado" & Chr$(124) & "L�mite"
    lTipoInversion = "Cantidad" & Chr$(124) & "Monto"
    Grilla.Editable = -1
    
    Toolbar_Operacion.Buttons(1).Enabled = True
    Toolbar_Operacion.Buttons(3).Enabled = False
    bEliminaLinea = False
    fTabIndex = 6
    Me.Top = 1
    Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub


Rem ------------------------------------------------------------
Rem Manejo en Grilla Operaciones
Rem ------------------------------------------------------------

Private Sub Grilla_AfterEdit(ByVal Row As Long, ByVal Col As Long)
    Grilla.TabBehavior = flexTabControls
    fRowGrilla = Row
    fColGrilla = Col
    Select Case Col
        Case c_nemo
'            Grilla.TextMatrix(Row, Col) = UCase(Grilla.TextMatrix(Row, Col))
            fDsc_Nemotecnico = UCase(Grilla.TextMatrix(Row, Col))
            If Fnt_BuscaDatosNemotecnico Then
                fTipoInversion = ""
                Grilla.Col = c_TipoInversion
            Else
                Grilla.Col = c_nemo
            End If
        Case c_TipoInversion
            If fTipoInversion = "M" Then
                'Grilla.TextMatrix(Row, Col) = "Monto"
                Call SetCell(Grilla, fRowGrilla, "tipo_inversion", "Monto", False)
                Grilla.Col = c_tipoprecio
                fTipoInversion = "M"
            Else
                If fTipoInversion = "C" Then
                    'Grilla.TextMatrix(Row, Col) = "Cantidad"
                    Call SetCell(Grilla, fRowGrilla, "tipo_inversion", "Cantidad", False)
                    Grilla.Col = c_cantidad
                    fTipoInversion = "C"
                Else
                    Grilla.Col = c_TipoInversion
                    fTipoInversion = ""
                End If
            End If
        Case c_cantidad
            If bModificaCelda Then
                Call Sub_CalculaValor
            Else
                If fTipoInversion = "C" Then
                    Grilla.Col = c_tipoprecio
                End If
            End If
        Case c_tipoprecio
            Call SetCell(Grilla, fRowGrilla, "KEY_TIPO_PRECIO", fTipoPrecio, False)
            If fTipoPrecio = "L" Then
                Grilla.TextMatrix(Row, Col) = "L�mite"
                Call SetCell(Grilla, fRowGrilla, "dsc_tipo_precio", "L�mite", False)
                Grilla.Col = c_precio
            Else
                If fTipoPrecio = "M" Then
                    Grilla.TextMatrix(Row, Col) = "Mercado"
                    Call SetCell(Grilla, fRowGrilla, "dsc_tipo_precio", "Mercado", False)
                    Call Sub_BuscaPrecioCierre
                    If fTipoInversion = "M" Then
                        Grilla.Col = c_monto
                    Else
                        Call Sub_CalculaValor
                        If fTipoInversion = "" Then
                            Grilla.Col = c_TipoInversion
                        Else
                            Grilla.Col = c_monto
                        End If
                    End If
                End If
            End If
        Case c_precio
            If fTipoInversion = "M" Then
                Grilla.Col = c_monto
            Else
                Call Sub_CalculaValor
                Grilla.Col = c_monto
            End If
        Case c_monto
            Call Sub_CalculaValor
            Grilla.Col = c_nemo
    End Select
    bModificaCelda = False
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    Select Case Col
        Case c_nemo
            Grilla.EditMaxLength = 12
        Case c_TipoInversion
            Grilla.EditMaxLength = 1
        '    Grilla.ColComboList(c_TipoInversion) = lTipoInversion
        '    If Grilla.TextMatrix(Row, Col) = "" Then
        '        Grilla.TextMatrix(Row, Col) = "Cantidad"
        '    End If
        Case c_emisor
            Cancel = True
        Case c_moneda
            Cancel = True
        Case c_cantidad
            Grilla.EditMaxLength = 13
            If fTipoInversion = "" Then
                fTipoInversion = "C"
                Grilla.TextMatrix(Row, c_TipoInversion) = "Cantidad"
            End If
        Case c_precio
            Grilla.EditMaxLength = 13
        Case c_monto
            Grilla.EditMaxLength = 13
            If fTipoInversion = "" Then
                fTipoInversion = "M"
                Grilla.TextMatrix(Row, c_TipoInversion) = "Monto"
            End If
        Case c_tipoprecio
            Grilla.EditMaxLength = 1
        '    Grilla.ColComboList(c_tipoprecio) = lTipoPrecio
        '    If Grilla.TextMatrix(Row, Col) = "" Then
        '        Grilla.TextMatrix(Row, Col) = "Mercado"
        '    End If
    End Select
End Sub

Private Sub Grilla_BeforeRowColChange(ByVal OldRow As Long, ByVal OldCol As Long, ByVal NewRow As Long, ByVal NewCol As Long, Cancel As Boolean)
    If NewRow = OldRow Then Exit Sub
    'If NewCol < OldCol Then Exit Sub
    'If OldRow > NewRow Then Exit Sub
    If Not bModificaCelda Then
        If Grilla.Cell(flexcpText, OldRow, c_nemo) = "" _
            Or Grilla.Cell(flexcpText, OldRow, c_emisor) = "" _
            Or Grilla.Cell(flexcpText, OldRow, c_moneda) = "" _
            Or Grilla.Cell(flexcpText, OldRow, c_cantidad) = "" _
            Or Grilla.Cell(flexcpText, OldRow, c_tipoprecio) = "" _
            Or Grilla.Cell(flexcpText, OldRow, c_precio) = "" _
            Or Grilla.Cell(flexcpText, OldRow, c_monto) = "" Then
            Cancel = True
            bModificaCelda = False
            Exit Sub
        End If
    End If
End Sub

Private Sub Grilla_DblClick()
    fRowGrilla = Grilla.Row
    fColGrilla = Grilla.Col

    bModificaCelda = True
End Sub


Private Sub Grilla_EnterCell()
    Select Case Grilla.Col
        Case c_nemo, c_TipoInversion, c_cantidad, c_tipoprecio, c_precio, c_monto
            Grilla.Editable = flexEDKbdMouse
        Case Else
            Grilla.Editable = flexEDNone
    End Select
End Sub
Private Sub Grilla_KeyDownEdit(ByVal Row As Long, ByVal Col As Long, KeyCode As Integer, ByVal Shift As Integer)
'    If KeyCode = vbKeyTab Then
'        ' tabbing out of the last cell moves on to next control
'        If Row = Grilla.Rows - 1 And Col = Grilla.Cols - 1 Then
'            If (Shift And vbShiftMask) = 0 Then
'                Grilla.TabBehavior = flexTabControls
'                SendKeys "{tab}"
'            End If
'
'        ' tabbing out of the first cell moves on to previous control
'        ElseIf Row = Grilla.FixfRowGrillas And Col = Grilla.FixedCols Then
'            If (Shift And vbShiftMask) <> 0 Then
'                Grilla.TabBehavior = flexTabControls
'                SendKeys "+{tab}"
'            End If
'        End If
'    End If

End Sub

Private Sub Grilla_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
    If KeyAscii <> vbKeyReturn And KeyAscii <> vbKeyBack Then
        Select Case Col
            Case c_nemo
                Call prdValidarColumnas(Me, Grilla, KeyAscii, c_nemo, AlfaNumerico)
            Case c_TipoInversion
                Call prdValidarColumnas(Me, Grilla, KeyAscii, c_TipoInversion, TipoInversion)
            Case c_cantidad
                Call prdValidarColumnas(Me, Grilla, KeyAscii, c_cantidad, Numerico)
            Case c_tipoprecio
                Call prdValidarColumnas(Me, Grilla, KeyAscii, c_tipoprecio, TipoPrecio)
            Case c_precio
                Call prdValidarColumnas(Me, Grilla, KeyAscii, c_precio, Decimales)
        End Select
    End If
End Sub

Private Sub Grilla_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        Call Sub_EliminarLinea
    End If

End Sub


Private Sub grilla_StartEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    Grilla.TabBehavior = flexTabCells
End Sub

Rem ------------------------------------------------------------
Rem Eventos de Campos
Rem ------------------------------------------------------------

Private Sub Txt_Comision_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub
Private Sub Txt_Comision_LostFocus()
    Call Sub_Calcula_MontoTotal
End Sub

Private Sub Txt_Derechos_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub
Private Sub Txt_Derechos_LostFocus()
    Call Sub_Calcula_MontoTotal
End Sub
Private Sub Txt_Gastos_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub
Private Sub Txt_Gastos_LostFocus()
    Call Sub_Calcula_MontoTotal
End Sub
Private Sub Txt_Iva_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub
Private Sub Txt_Iva_LostFocus()
  Call Sub_Calcula_MontoTotal
End Sub


Private Sub Txt_Porcentaje_Comision_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub
Private Sub Txt_Porcentaje_Comision_LostFocus()
  Call Sub_Calcula_MontoTotal
End Sub
Private Sub Txt_Porcentaje_Derechos_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub
Private Sub Txt_Porcentaje_Derechos_LostFocus()
  Call Sub_Calcula_MontoTotal
End Sub




Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
        If Fnt_Grabar Then
            MsgBox "Operaci�n ingresada exitosamente.", vbInformation, Me.Caption
            Unload Me
        End If
    Case "REFRESH"
      If MsgBox("Con esta acci�n inicializar� los valores." & vbLf & "�Realiza un nuevo ingreso?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_Limpiar
      End If
    Case "EXIT"
      Unload Me
  End Select
End Sub
Private Sub Sub_Limpiar()
Dim lRow As Integer
Dim lCol As Integer

    Call Sub_Bloquea_Puntero(Me)
    
    For lRow = 1 To Grilla.Rows - 1
        For lCol = 0 To MAX_COLUMNAS
            Grilla.TextMatrix(lRow, lCol) = ""
        Next
    Next
        
    Me.Caption = "Acciones - " & IIf(fTipo_Operacion = "INST", "Instrucci�n", "Directa")
    
    Toolbar_Operacion.Buttons(1).Enabled = True
    Toolbar_Operacion.Buttons(3).Enabled = False

    Grilla.Editable = flexEDKbdMouse
    fRowGrilla = 0
    bEliminaLinea = False
 
    fId_Moneda_Transaccion = 1
    
    Call Sub_Setea_Comisiones
    Call Sub_Desbloquea_Puntero(Me)
    
    Cmb_Cuentas.Text = ""
    Txt_Rut.Text = ""
    Txt_Nombres.Text = ""
    Txt_Perfil.Text = ""
    txtNombreAsesor.Text = ""
    Pnl_Cliente.Enabled = True
    Pnl_Cuenta.Enabled = True
    Btn_Compra.Enabled = True
    Btn_Venta.Enabled = True
End Sub

Private Sub Toolbar_Operacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "OPERACION"
        Pnl_DetalleOperacion.Enabled = False
        Pnl_DatosOperacion.Enabled = True
        Toolbar_Operacion.Buttons(1).Enabled = False
        Toolbar_Operacion.Buttons(3).Enabled = True
    Case "DETALLEOPERACION"
        Pnl_DatosOperacion.Enabled = False
        Pnl_DetalleOperacion.Enabled = True
        Toolbar_Operacion.Buttons(1).Enabled = True
        Toolbar_Operacion.Buttons(3).Enabled = False
  End Select
End Sub


Private Sub Btn_Compra_click()
    fOperacion = "I"
    Me.Caption = "Compra de Acciones - Instrucci�n"
    fDsc_Operacion = "Compra"
    Btn_Venta.Enabled = False
    Pnl_Cuenta.Enabled = False
    Pnl_Cliente.Enabled = False
    Pnl_Operacion.Enabled = True
    Pnl_DatosOperacion.Enabled = False
    'Grilla.ColHidden(c_cantidad_nemo) = True
    Grilla.Col = c_nemo
    Grilla.Row = 1
    Call grilla_StartEdit(1, c_nemo, False)
End Sub
Private Sub Btn_Venta_click()
    fOperacion = "E"
    Me.Caption = "Venta de Acciones - Instrucci�n"
    fDsc_Operacion = "Venta"
    Btn_Compra.Enabled = False
    Pnl_Cuenta.Enabled = False
    Pnl_Cliente.Enabled = False
    Pnl_Operacion.Enabled = True
    Pnl_DatosOperacion.Enabled = False
    'Grilla.ColHidden(c_cantidad_nemo) = False
    Grilla.Col = c_nemo
    Grilla.Row = 1
    Call grilla_StartEdit(1, c_nemo, False)
End Sub

Private Sub Cmb_Contraparte_ItemChange()
Dim lId_Contraparte As String
  
  lId_Contraparte = Fnt_FindValue4Display(Cmb_Contraparte, Cmb_Contraparte.Text)
  
  If lId_Contraparte = "" Then
    'como nunca va a existir la contraparte -1 se pasa el parametro
    lId_Contraparte = "-1"
  End If
  
  Call Sub_CargaCombo_Traders(Cmb_Traders, lId_Contraparte)
End Sub

Private Sub Cmb_FechaLiquidacion_Change()
  If Not Cmb_FechaLiquidacion.Text = "" Then
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Fnt_FindValue4Display(Cmb_FechaLiquidacion, Cmb_FechaLiquidacion.Text))
  End If
End Sub

Private Sub Cmb_FechaLiquidacion_ItemChange()
  If Not Cmb_FechaLiquidacion.Text = "" Then
    Dtp_FechaLiquidacion.MinDate = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Fnt_FindValue4Display(Cmb_FechaLiquidacion, Cmb_FechaLiquidacion.Text))
    Dtp_FechaLiquidacion.Value = Dtp_FechaLiquidacion.MinDate
  End If
End Sub

Private Sub DTP_Fecha_Operacion_Change()
  fFecha_Operacion = DTP_Fecha_Operacion.Value
    Dtp_FechaLiquidacion.MinDate = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Fnt_FindValue4Display(Cmb_FechaLiquidacion, Cmb_FechaLiquidacion.Text))
End Sub

Private Sub Dtp_FechaLiquidacion_Change()
 ' Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
  Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Fnt_FindValue4Display(Cmb_FechaLiquidacion, Cmb_FechaLiquidacion.Text))
End Sub

Private Sub cmb_buscar_Click()
    fId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)
    
    If fId_Cuenta <> 0 Then
        Call Sub_ComboSelectedItem(Cmb_Cuentas, fId_Cuenta)
        Cmb_Cuentas_LostFocus
    End If
End Sub

Private Sub Cmb_Cuentas_GotFocus()
  Txt_Rut.Text = ""
  Txt_Perfil.Text = ""
  Txt_Nombres.Text = ""
  txtNombreAsesor.Text = ""
End Sub

Private Sub Cmb_Cuentas_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Cmb_Cuentas_LostFocus()
    fId_Cuenta = Fnt_FindValue4Display(Cmb_Cuentas, Cmb_Cuentas.Text)
    If fId_Cuenta <> "" Then
        Call Sub_CargaDatosCliente
        Call Sub_CargaDatos
        
        If bOper_Fecha_Anterior Then
          lbl_fecha_ingreso.Visible = True
          DTP_Fecha_Operacion.Visible = True
          Txt_FechaIngreso_Real.Visible = False
          DTP_Fecha_Operacion.MaxDate = fFecha_Operacion
        Else
          lbl_fecha_ingreso.Visible = False
          DTP_Fecha_Operacion.Visible = False
          Txt_FechaIngreso_Real.Visible = True
        End If
    Else
        Cmb_Cuentas.SetFocus
    End If
End Sub

Private Sub Sub_CargaForm()

    Call Sub_Bloquea_Puntero(Me)

    Call Sub_FormControl_Color(Me.Controls)

    '------------------------------------------------
    '-- Carga Cuentas
    '------------------------------------------------
    Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, "id_cliente")
  
    '------------------------------------------------
    '-- Setea Comisiones
    '------------------------------------------------
    Call Sub_Setea_Comisiones
  
    '------------------------------------------------
    '-- Carga Fecha Liquidaci�n
    '------------------------------------------------
    Call Sub_CargaFechaLiquidacion
    
    '------------------------------------------------
    '-- Carga Contraparte
    '------------------------------------------------
    Call Sub_CargaContraparte
    
    '------------------------------------------------
    '-- Carga Representante
    '------------------------------------------------
    Call Sub_CargaCombo_Representantes(Cmb_Representantes, fId_Cliente)
    
    '------------------------------------------------
    '-- Carga Combo Monedas en Grilla
    '------------------------------------------------
    'Call Sub_CargaCombo_Monedas(Cmb_Moneda)
    
    
    
    Grilla.Editable = flexEDKbdMouse

    Pnl_Operacion.Enabled = False
    
    Call Sub_Desbloquea_Puntero(Me)

End Sub


Private Sub Sub_CargaDatosCliente()
Dim lcCuenta As Object
'-----------------------------------------------
Dim lReg            As hCollection.hFields
Dim lId_Cuenta      As String

    Call Sub_Bloquea_Puntero(Me)
    
    
    If Not fId_Cuenta = "" Then
        'Busca el perfil de la cuenta.
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("id_cuenta").Valor = fId_Cuenta
            If .Buscar_Vigentes Then
                If .Cursor.Count > 0 Then
                    Txt_Perfil.Text = .Cursor(1)("dsc_perfil_riesgo").Value
                    Txt_Perfil.Tag = .Cursor(1)("id_perfil_riesgo").Value
                    fId_Cliente = .Cursor(1)("id_cliente").Value
                    Txt_Rut.Text = "" & .Cursor(1)("rut_cliente").Value
                    Txt_Nombres.Text = "" & .Cursor(1)("nombre_cliente").Value
                    txtNombreAsesor.Text = "" & .Cursor(1)("nombre_Asesor").Value
                End If
            Else
                MsgBox "Problemas en cargar la Cuenta." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            End If
        End With
        Set lcCuenta = Nothing
    End If
    
    Call Sub_Desbloquea_Puntero(Me)

End Sub

'-------------------- Rutinas

Private Sub Sub_Setea_Comisiones()
  '-------------------------------
  Txt_DiasVigencia.Text = 1
  '-------------------------------
  Txt_Porcentaje_Comision.Text = 0
  Txt_Comision.Text = 0
  Txt_Iva.Text = 0
  Txt_Gastos.Text = 0
  Txt_Derechos.Text = 0
  Txt_TotalDetalle.Text = 0
  Txt_MontoTotal.Text = 0
End Sub

Private Sub Sub_CargaFechaLiquidacion()
  With Cmb_FechaLiquidacion
    Call .AddItem("PH")
    Call .AddItem("PM")
    Call .AddItem("CN")
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem("0", "PH")
      .Add Fnt_AgregaValueItem("1", "PM")
      .Add Fnt_AgregaValueItem("2", "CN")
      .Translate = True
    End With
    
    
    'Agregar seleccion de item de combo
    'Si son acciones fCod_Instrumento = gcINST_ACCIONES_NAC se debe buscar valor de TIPO DE LIQUIDACION
    'Si son ffmm ncaionales se debe buscar valor de VALUTA DE NEMOTECNICOS
    .Text = ""
    .SelectedItem = 2
  End With

End Sub

Private Sub Sub_CargaContraparte()
Dim lcRel_Contrapartes_Instrum As Class_Rel_Contrapartes_Instrum
Dim lTexto As String
Dim lReg As hFields

  With Cmb_Contraparte
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
    End With
    
    Set lcRel_Contrapartes_Instrum = New Class_Rel_Contrapartes_Instrum
    lcRel_Contrapartes_Instrum.Campo("cod_instrumento").Valor = fCod_Instrumento
    If lcRel_Contrapartes_Instrum.BuscarView Then
      For Each lReg In lcRel_Contrapartes_Instrum.Cursor
        lTexto = ""
        
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
          
        lTexto = lReg("DSC_CONTRAPARTE").Value
          
        .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("ID_CONTRAPARTE").Value, lTexto)
          
        Call .AddItem(lTexto)
          
      Next
    End If
    Set lcRel_Contrapartes_Instrum = Nothing
  End With

End Sub

Private Sub Sub_CargaIva_Comisiones()
Dim lcIva As Class_Iva
Dim lcComisiones As Class_Comisiones_Instrumentos

    Set lcIva = New Class_Iva
    With lcIva
      If .Buscar(True) Then
        fValor_Iva = .Cursor(1)("valor").Value
        Txt_Iva.Caption = " Iva (a " & .Porcentaje_Iva(fValor_Iva) & "%)"
      End If
    End With


    Set lcComisiones = New Class_Comisiones_Instrumentos
    With lcComisiones
      .Campo("Id_Cuenta").Valor = fId_Cuenta
      .Campo("COD_INSTRUMENTO").Valor = fCod_Instrumento
      If .Buscar(True) Then
        If .Cursor.Count > 0 Then
          Txt_Porcentaje_Comision.Text = lcIva.Porcentaje_Iva(.Cursor(1)("COMISION").Value)
          Txt_Gastos.Text = Int(NVL(.Cursor(1)("GASTOS").Value, 0))
          Txt_Porcentaje_Derechos.Text = lcIva.Porcentaje_Iva(NVL(.Cursor(1)("DERECHOS_BOLSA").Value, 0))
        End If
      End If
    End With
    
    Set lcComisiones = Nothing
    Set lcIva = Nothing
End Sub

Private Sub Sub_CargaDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lNemotecnicos As Class_Nemotecnicos
Dim lSaldos_Activos As Class_Saldo_Activos
Dim lcTipo_Liq As Class_Tipos_Liquidacion

    Load Me
    Call Sub_Bloquea_Puntero(Me)
  
    '------------------------------------------------
    '-- Setea Fechas
    '------------------------------------------------
    fFecha_Operacion = Fnt_FechaServidor
    Txt_FechaIngreso_Real.Text = fFecha_Operacion
    DTP_Fecha_Operacion.Value = fFecha_Operacion
    
    '------------------------------------------------
    '-- Carga Iva y Comisiones
    '------------------------------------------------
    Call Sub_CargaIva_Comisiones

    If fTipo_Operacion = gcOPERACION_Custodia Or fTipo_Operacion = gcOPERACION_Custodia_SusCrip Then
        Dtp_FechaLiquidacion.Value = fFecha_Operacion
    Else
        
        Dtp_FechaLiquidacion.MinDate = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
        
        Set lcTipo_Liq = New Class_Tipos_Liquidacion
        With lcTipo_Liq
          .Campo("cod_instrumento").Valor = fCod_Instrumento
          .Campo("id_empresa").Valor = Fnt_EmpresaActual
          .Campo("tipo_movimiento").Valor = fOperacion
          If .Buscar Then
            If .Cursor.Count > 0 Then
              Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, NVL(.Cursor(1).Fields("retencion").Value, 0))
              Cmb_FechaLiquidacion.Text = NVL(.Cursor(1).Fields("retencion").Value, 0)
            Else
              Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
            End If
          Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en carga de Tipos de Liquidacion.", _
                              .ErrMsg, _
                              pConLog:=True)
            Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
          End If
        End With
        Set lcTipo_Liq = Nothing
      
    End If

    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Calcula_MontoTotal()
Dim lLinea As Long
Dim lTotal As Double
Dim lPorc_Comision As Double
Dim lPorc_Derechos As Double
Dim lComision As Double
Dim lDerechos As Double
Dim lGastos As Double
Dim lIva As Double
    
        
    
    lTotal = 0
        
    For lLinea = 1 To (Grilla.Rows - 1)
        If GetCell(Grilla, lLinea, "monto") <> "" Then
            lTotal = lTotal + NVL(GetCell(Grilla, lLinea, "monto"), 0)
        End If
    Next
    Txt_TotalDetalle.Text = lTotal
    
    Txt_MontoNeto.Text = lTotal
    
    lPorc_Comision = To_Number(Txt_Porcentaje_Comision.Text)
    lPorc_Derechos = To_Number(Txt_Porcentaje_Derechos.Text)
    lGastos = To_Number(Txt_Gastos.Text)
    
    If fOperacion = gcTipoOperacion_Ingreso Then
      
      lComision = Fnt_Techo_Numero((lTotal * lPorc_Comision) / 100)
      lDerechos = Fnt_Techo_Numero((lTotal * lPorc_Derechos) / 100)
      lIva = Fnt_Techo_Numero((lComision + lGastos + lDerechos) * fValor_Iva)
    
      Txt_MontoTotal.Text = lTotal + lComision + lIva + lGastos + lDerechos
    
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      
      lComision = Int((lTotal * lPorc_Comision) / 100)
      lDerechos = Int((lTotal * lPorc_Derechos) / 100)
      lIva = Int((lComision + lGastos + lDerechos) * fValor_Iva)
      
      Txt_MontoTotal.Text = lTotal - (lComision + lIva + lGastos + lDerechos)
    End If
    
    Txt_Comision.Text = lComision
    Txt_Derechos.Text = lDerechos
    Txt_Iva.Text = lIva
   
       
End Sub

Private Function Fnt_BuscaDatosNemotecnico() As Boolean
Dim lcNemotecnico As Class_Nemotecnicos
Dim lId_Moneda    As Integer
Dim lId_Moneda_Tr As Integer
Dim lSaldo_Cantidad As Double
Dim lAcciones As Class_Acciones
Dim lresult As Boolean

    lresult = True
    Set lcNemotecnico = New Class_Nemotecnicos
    With lcNemotecnico
        .Campo("nemotecnico").Valor = fDsc_Nemotecnico
        If .BuscarView Then
            If .Cursor.Count = 0 Then
                MsgBox "Nemot�cnico no existe.", vbInformation, Me.Caption
                Grilla.Col = c_nemo
                lresult = False
            Else
                lId_Moneda = .Cursor(1)("id_moneda").Value
                lId_Moneda_Tr = .Cursor(1)("id_moneda_transaccion").Value
                fId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
                If Not Fnt_Valida_Perfil_Cuenta_Nemo() Then
                    lresult = False
                    GoTo ErrProcedure
                End If
                Call SetCell(Grilla, fRowGrilla, "dsc_nemotecnico", .Cursor(1)("nemotecnico").Value, pAutoSize:=False)
                Call SetCell(Grilla, fRowGrilla, "id_nemotecnico", .Cursor(1)("id_nemotecnico").Value, pAutoSize:=False)
                Call SetCell(Grilla, fRowGrilla, "dsc_emisor", .Cursor(1)("dsc_emisor_especifico").Value, pAutoSize:=False)
                Call SetCell(Grilla, fRowGrilla, "id_moneda", lId_Moneda, pAutoSize:=False)
                Call SetCell(Grilla, fRowGrilla, "dsc_moneda", .Cursor(1)("dsc_moneda").Value, pAutoSize:=False)
                fTasa_Emision = NVL(.Cursor(1)("tasa_emision").Value, 0)
                If fRowGrilla = 1 Then
                    fId_Moneda_Transaccion = lId_Moneda_Tr
                End If
                If lId_Moneda_Tr <> fId_Moneda_Transaccion Then
                    MsgBox "La moneda debe ser la misma que la de los nemot�cnicos ya ingresados", vbInformation, Me.Caption
                    lresult = False
                End If
            End If
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            lresult = False
        End If
    End With
ErrProcedure:
    Set lcNemotecnico = Nothing
    Fnt_BuscaDatosNemotecnico = lresult
End Function

Private Sub Sub_BuscaPrecioCierre()
Dim lPrecio As Class_Publicadores_Precio
Dim lReg As hFields
  
    Call Sub_Bloquea_Puntero(Me)
    Set lPrecio = New Class_Publicadores_Precio
    With lPrecio
      .Campo("id_nemotecnico").Valor = fId_Nemotecnico
      .Campo("fecha").Valor = DTP_Fecha_Operacion.Value
      If .Buscar_Ultimo_Cta_Nemotecnico(fId_Cuenta) Then
          Grilla.TextMatrix(fRowGrilla, c_precio) = NVL(.Campo("precio").Valor, 0)
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en carga de Precio de Publicadores.", _
                          .ErrMsg, _
                          pConLog:=True)
      End If
    End With
    Set lPrecio = Nothing
    Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Function Fnt_Grabar()
Dim lAccion As Class_Acciones
Dim lLinea As Long
Dim lId_Contraparte As String
Dim lTipo_Precio As String
Dim lId_representante As String
Dim lFecha_Operacion  As Date
Dim lFecha_Vigencia As Date
Dim lFecha_Liquidacion As Date
Dim lPrecio_Historico As String
Dim lPrecioLimiteLinea As String
Dim lId_Trader As String
'---------------------------------
Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc
Dim lId_Nemotecnico As String
'---------------------------------
Dim lId_Caja_Cuenta As Double
Dim lNum_Error      As Double
'---------------------------------
Dim lRollback As Boolean
'------------------------------------------
Dim sChkAporteRetiro        As String
'------------------------------------------
Dim sCC             As String       'agregado por MMA 12/11/2008 para envio con copia

    sChkAporteRetiro = IIf(chkAporteRetiro.Value = 0, "NO", "SI")
    
    Call Sub_Bloquea_Puntero(Me)
    Me.Enabled = False
    
    lRollback = True
    Fnt_Grabar = False
    
    gDB.IniciarTransaccion
    
    If Not Fnt_ValidarDatos Then
        GoTo ErrProcedure
    End If

    If fOperacion = gcTipoOperacion_Ingreso Then
        Rem Si es una compra
        Rem Validar si el Instrumento y Nemot�cnicos est�n relacionados al perfil de riesgo que le corresponde a la cuenta
'        If Not Fnt_Valida_Perfil_Cuenta_Instrm_Nemo(Grilla, fId_Cuenta, fCod_Instrumento) Then
'          GoTo ErrProcedure
'        End If
        Select Case fTipo_Operacion
            Case gcOPERACION_Instruccion, gcOPERACION_Directa
                lId_Caja_Cuenta = Fnt_CheckeaFinanciamiento(pId_Cuenta:=fId_Cuenta _
                                                           , pCod_Mercado:=fc_Mercado _
                                                           , pMonto:=Txt_MontoTotal.Text _
                                                           , pId_Moneda:=fId_Moneda_Transaccion _
                                                           , pFecha_Liquidacion:=Dtp_FechaLiquidacion.Value _
                                                           , pNum_Error:=lNum_Error)
                                                           
                'VERIFICA EL RESULTADO DE LA OPERACION
                Select Case lNum_Error
                    Case 0, eFinanciamiento_Caja.eFC_InversionDescubierta
                      'SI SON ESTOS VALORES SIGNIFICA QUE LA OPERACION SE PUEDE REALIZAR
                    Case Else
                      'Si el financiamiento tubo problemas
                      GoTo ErrProcedure
                End Select
              
            Case gcOPERACION_Custodia, gcOPERACION_Custodia_SusCrip, gcOPERACION_Custodia_NoCapital
                lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, fId_Moneda_Transaccion)
                If lId_Caja_Cuenta = cNewEntidad Then
                  'Si el financiamiento tubo problemas
                  GoTo ErrProcedure
                End If
            
        End Select
    Else
        'Si es una venta
        lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, fId_Moneda_Transaccion)
        If lId_Caja_Cuenta = -1 Then
            'Significa que hubo problema con la busqueda de la caja
            GoTo ErrProcedure
        End If
    End If

    
    Set lAccion = New Class_Acciones
  
    For lLinea = 1 To (Grilla.Rows - 1)
        If GetCell(Grilla, lLinea, "dsc_nemotecnico") = "" Then
            Exit For
        Else
            lPrecio_Historico = ""
            If fTipo_Operacion = gcOPERACION_Directa Or fTipo_Operacion = gcOPERACION_Instruccion Then
                lPrecioLimiteLinea = GetCell(Grilla, lLinea, "KEY_TIPO_PRECIO")
            Else
                lPrecioLimiteLinea = ""
            End If
            Call lAccion.Agregar_Operaciones_Detalle(GetCell(Grilla, lLinea, "id_nemotecnico"), _
                                                     GetCell(Grilla, lLinea, "cantidad"), _
                                                     GetCell(Grilla, lLinea, "precio"), _
                                                     GetCell(Grilla, lLinea, "id_moneda"), _
                                                     GetCell(Grilla, lLinea, "monto"), _
                                                     cFlg_No_Vende_Todo, _
                                                     lPrecio_Historico, _
                                                     lPrecioLimiteLinea)
        End If
    Next
      
    lId_Contraparte = Fnt_ComboSelected_KEY(Cmb_Contraparte)
    lId_representante = Fnt_ComboSelected_KEY(Cmb_Representantes)
    lFecha_Operacion = DTP_Fecha_Operacion.Value ' Fnt_FechaServidor
    lId_Trader = Fnt_ComboSelected_KEY(Cmb_Traders)
    lFecha_Vigencia = lFecha_Operacion + To_Number(Txt_DiasVigencia.Text)
    lFecha_Liquidacion = Dtp_FechaLiquidacion.Value

    Select Case fTipo_Operacion
        Case gcOPERACION_Directa
            If Not lAccion.Realiza_Operacion_Directa(pId_Operacion:=fId_Operacion, _
                                                    pId_Cuenta:=fId_Cuenta, _
                                                    pDsc_Operacion:="", _
                                                    pTipoOperacion:=fOperacion, _
                                                    pId_Contraparte:=lId_Contraparte, _
                                                    pId_Representante:=lId_representante, _
                                                    pId_Moneda_Operacion:=fId_Moneda_Transaccion, _
                                                    pFecha_Operacion:=lFecha_Operacion, _
                                                    pFecha_Vigencia:=lFecha_Vigencia, _
                                                    pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                    pId_Trader:=lId_Trader, _
                                                    pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                    pComision:=Txt_Comision.Text, _
                                                    pDerechos_Bolsa:=Txt_Derechos.Text, _
                                                    pGastos:=Txt_Gastos.Text, _
                                                    pIva:=Txt_Iva.Text, _
                                                    pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                    pTipo_Precio:=lTipo_Precio, _
                                                    pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                                    pChkAporteRetiro:=sChkAporteRetiro) Then
                                                    
                Call Fnt_MsgError(lAccion.SubTipo_LOG, _
                                  "Problemas en grabar Acciones.", _
                                  lAccion.ErrMsg, _
                                  pConLog:=True)
                GoTo ErrProcedure
            End If
        Case gcOPERACION_Custodia, gcOPERACION_Custodia_NoCapital
            If Not lAccion.Realiza_Operacion_Custodia(pId_Operacion:=fId_Operacion, _
                                                    pId_Cuenta:=fId_Cuenta, _
                                                    pDsc_Operacion:="", _
                                                    pTipoOperacion:=fOperacion, _
                                                    pId_Contraparte:=lId_Contraparte, _
                                                    pId_Representante:=lId_representante, _
                                                    pId_Moneda_Operacion:=fId_Moneda_Transaccion, _
                                                    pFecha_Operacion:=lFecha_Operacion, _
                                                    pFecha_Vigencia:=lFecha_Vigencia, _
                                                    pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                    pId_Trader:=lId_Trader, _
                                                    pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                    pComision:=Txt_Comision.Text, _
                                                    pDerechos_Bolsa:=Txt_Derechos.Text, _
                                                    pGastos:=Txt_Gastos.Text, _
                                                    pIva:=Txt_Iva.Text, _
                                                    pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                    pTipo_Precio:=lTipo_Precio, _
                                                    pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                                    pChkAporteRetiro:=sChkAporteRetiro) Then
                                                    
                Call Fnt_MsgError(lAccion.SubTipo_LOG, _
                                  "Problemas en grabar Acciones.", _
                                  lAccion.ErrMsg, _
                                  pConLog:=True)
                GoTo ErrProcedure
            End If
        Case gcOPERACION_Custodia_SusCrip
            If Not lAccion.Realiza_Operacion_Custodia_Opciones(pId_Operacion:=fId_Operacion, _
                                                    pId_Cuenta:=fId_Cuenta, _
                                                    pDsc_Operacion:="", _
                                                    pTipoOperacion:=fOperacion, _
                                                    pId_Contraparte:=lId_Contraparte, _
                                                    pId_Representante:=lId_representante, _
                                                    pId_Moneda_Operacion:=fId_Moneda_Transaccion, _
                                                    pFecha_Operacion:=lFecha_Operacion, _
                                                    pFecha_Vigencia:=lFecha_Vigencia, _
                                                    pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                    pId_Trader:=lId_Trader, _
                                                    pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                    pComision:=Txt_Comision.Text, _
                                                    pDerechos_Bolsa:=Txt_Derechos.Text, _
                                                    pGastos:=Txt_Gastos.Text, _
                                                    pIva:=Txt_Iva.Text, _
                                                    pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                    pTipo_Precio:=lTipo_Precio, _
                                                    pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
                Call Fnt_MsgError(lAccion.SubTipo_LOG, _
                                  "Problemas en grabar Acciones.", _
                                  lAccion.ErrMsg, _
                                  pConLog:=True)
                GoTo ErrProcedure
            End If
        Case gcOPERACION_Instruccion
            If Not lAccion.Realiza_Operacion_Instruccion(pId_Operacion:=fId_Operacion, _
                                                        pId_Cuenta:=fId_Cuenta, _
                                                        pDsc_Operacion:="", _
                                                        pTipoOperacion:=fOperacion, _
                                                        pId_Contraparte:=lId_Contraparte, _
                                                        pId_Representante:=lId_representante, _
                                                        pId_Moneda_Operacion:=fId_Moneda_Transaccion, _
                                                        pFecha_Operacion:=lFecha_Operacion, _
                                                        pFecha_Vigencia:=lFecha_Vigencia, _
                                                        pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                        pId_Trader:=lId_Trader, _
                                                        pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                        pComision:=Txt_Comision.Text, _
                                                        pDerechos_Bolsa:=Txt_Derechos.Text, _
                                                        pGastos:=Txt_Gastos.Text, _
                                                        pIva:=Txt_Iva.Text, _
                                                        pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                        pTipo_Precio:=lTipo_Precio, _
                                                        pChkAporteRetiro:=sChkAporteRetiro) Then
                Call Fnt_MsgError(lAccion.SubTipo_LOG, _
                                  "Problemas en grabar Acciones.", _
                                  lAccion.ErrMsg, _
                                  pConLog:=True)
                GoTo ErrProcedure
            End If
        Case Else
          MsgBox "Operacion no reconocida para operar.", vbCritical, Me.Caption
          GoTo ErrProcedure
    End Select

    lRollback = False
    Fnt_Grabar = True

ErrProcedure:
    If lRollback Then
        gDB.RollbackTransaccion
    Else
        'gDB.CommitTransaccion      'si la operaci�n se graba en magic se hace el commit
 '       sCC = Fnt_Lee_Mail_BackOffice(gId_Empresa)
            gDB.CommitTransaccion
 '           Call Fnt_EnvioEMAIL_Trader(fId_Operacion, sCC)

            If sChkAporteRetiro = "SI" Then
                Frm_AporteRescate_Fechas_Anteriores.ImprimeDocWord fId_Operacion, pTipo:="O"
            End If
    End If
    
    Call Sub_Desbloquea_Puntero(Me)
    Me.Enabled = True
End Function

Private Function Fnt_ValidarDatos() As Boolean
Dim lLinea As Long
Dim pMsgError
Dim lcCuenta As Object
    
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    
    lcCuenta.Campo("id_cuenta").Valor = fId_Cuenta
    If lcCuenta.Cuenta_Bloqueada(pMsgError) Then
        Select Case fTipo_Operacion
            Case gcOPERACION_Instruccion
                MsgBox "Cuenta Bloqueada. Motivo : " & pMsgError, vbExclamation, Me.Caption
                Fnt_ValidarDatos = False
                Exit Function
            Case gcOPERACION_Directa
                pMsgError = pMsgError & vbCr & vbCr & "�Desea continuar con la operaci�n?"
                If MsgBox("Cuenta Bloqueada. Motivo : " & pMsgError, vbQuestion + vbYesNo, Me.Caption) = vbNo Then
                    Fnt_ValidarDatos = False
                    Exit Function
                End If
        End Select
    End If
      
    If Not Fnt_Form_Validar(Me.Controls, Pnl_Operacion) Then
        Fnt_ValidarDatos = False
        Exit Function
    ElseIf Not Fnt_Form_Validar(Me.Controls, Pnl_DatosOperacion) Then
        Fnt_ValidarDatos = False
        Exit Function
    End If
      
'    If Fnt_Verifica_Feriado(fFecha_Operacion) Then
'      Fnt_ValidarDatos = False
'      MsgBox "Solo se pueden ingresar operaciones en d�as h�biles.", vbExclamation, Me.Caption
'      Exit Function
'    End If
'
    If (Txt_Porcentaje_Comision.Text < 0) Or (Txt_Porcentaje_Comision.Text > 100) Then
      MsgBox "Porcentaje Comisi�n no puede ser menor a cero ni mayor a cien.", vbExclamation, Me.Caption
      Fnt_ValidarDatos = False
      Txt_Porcentaje_Comision.SetFocus
      Exit Function
    ElseIf Txt_Comision.Text < 0 Then
      MsgBox "Comisi�n a Cobrar no puede ser menor a cero.", vbExclamation, Me.Caption
      Fnt_ValidarDatos = False
      Exit Function
    ElseIf (Txt_Iva.Text < 0) Then
      MsgBox "El Valor Iva no puede ser menor a cero ni mayor a cien.", vbExclamation, Me.Caption
      Fnt_ValidarDatos = False
      Exit Function
    ElseIf Txt_Gastos.Text < 0 Then
      MsgBox "Gastos no puede ser menor a cero.", vbExclamation, Me.Caption
      Fnt_ValidarDatos = False
      Exit Function
    ElseIf Txt_Derechos.Text < 0 Then
      MsgBox "Derechos Gastos no puede ser menor a cero.", vbExclamation, Me.Caption
      Fnt_ValidarDatos = False
      Exit Function
    End If
      
    If Grilla.Rows <= 1 Then
      MsgBox "Para realizar una operaci�n m�nimo debe tener un detalle.", vbExclamation, Me.Caption
      Fnt_ValidarDatos = False
      Exit Function
    End If
    
    If fOperacion = gcTipoOperacion_Ingreso Then
        If fTipo_Operacion = gcOPERACION_Directa Or fTipo_Operacion = gcOPERACION_Instruccion Then
            For lLinea = 1 To (Grilla.Rows - 1)
                If GetCell(Grilla, lLinea, "id_nemotecnico") <> "" Then
                    If Not Fnt_CompruebaRestriccionCompra(fId_Cuenta, fEmisor, GetCell(Grilla, lLinea, "id_nemotecnico")) Then
                      MsgBox "Existen Nemot�cnicos que presentan restricci�n de compra asociada a la Cuenta." & vbCr & vbCr & "No se puede operar.", vbOKOnly + vbCritical, Me.Caption
                      Fnt_ValidarDatos = False
                      Exit Function
                    End If
                End If
            Next
        End If
    Else
      'Verifica las restricciones de Venta asociadas al producto
        If fTipo_Operacion = gcOPERACION_Directa Or fTipo_Operacion = gcOPERACION_Instruccion Then
            For lLinea = 1 To (Grilla.Rows - 1)
                If GetCell(Grilla, lLinea, "id_nemotecnico") <> "" Then
                    If Not Fnt_CompruebaRestriccionVenta(fId_Cuenta, fEmisor, GetCell(Grilla, lLinea, "id_nemotecnico"), GetCell(Grilla, lLinea, "Cantidad")) Then
                        MsgBox "Existen Nemot�cnicos que presentan restricci�n de Venta asociada a la Cuenta." & vbCr & vbCr & "No se puede operar.", vbOKOnly + vbCritical, Me.Caption
                        Fnt_ValidarDatos = False
                        Exit Function
                    End If
                End If
            Next
        End If
    End If
    
    Fnt_ValidarDatos = True
End Function


Private Sub Sub_CalculaValor()
Dim lMonto As Double
Dim lPrecio As Double
Dim lCantidad As Long
Dim lValor As Double
Dim lPorc As Double

    If fTipoInversion <> "" Then
        Call Sub_Bloquea_Puntero(Me)
        If fTipoInversion = "C" Then
            'Calcula Monto
            lCantidad = To_Number(GetCell(Grilla, fRowGrilla, "cantidad"))
            lPrecio = To_Number(GetCell(Grilla, fRowGrilla, "precio"))
            lPrecio = IIf(lPrecio > 0, lPrecio, 1)
            lMonto = Round(lCantidad * lPrecio)
            Call SetCell(Grilla, fRowGrilla, "monto", lMonto, pAutoSize:=False)
        Else
            'Calcula Cantidad
            lPrecio = To_Number(GetCell(Grilla, fRowGrilla, "precio"))
            lPrecio = IIf(lPrecio > 0, lPrecio, 1)
            lMonto = To_Number(GetCell(Grilla, fRowGrilla, "monto"))
            lValor = lMonto Mod lPrecio
            lPorc = lValor * 0.012
            lCantidad = lValor - lPorc
            lMonto = Round(lCantidad * lPrecio)
            Call SetCell(Grilla, fRowGrilla, "cantidad", lCantidad, pAutoSize:=False)
            Call SetCell(Grilla, fRowGrilla, "monto", lMonto, pAutoSize:=False)
        End If
        Call Sub_Calcula_MontoTotal
        Call Sub_Desbloquea_Puntero(Me)
    End If
End Sub

'Private Sub Sub_CargaPanelNemotecnicos()
'Dim lcNemotecnico   As Class_Nemotecnicos
'Dim lcAcciones As Class_Acciones
'Dim lcSaldos_Activos As Class_Saldo_Activos
''--------------------------------
'Dim lCursor
'Dim lId_Nemotecnico As String
'Dim lId_Mov_Activo As String
'Dim lReg As hFields
'Dim lLinea As Integer
'Dim bResult As Boolean
'
'    Call Sub_Bloquea_Puntero(Me)
'    Grilla_Nemo.Rows = 1
'    If fOperacion = gcTipoOperacion_Ingreso Then
'        Grilla_Nemo.ColHidden(11) = True  'Saldo Disponible
'        'Si se estan realizando COMPRAS
'        Set lcNemotecnico = New Class_Nemotecnicos
'        If fTipo_Operacion = gcOPERACION_Directa Then
'            lcNemotecnico.Campo("cod_instrumento").Valor = fCod_Instrumento 'gcINST_ACCIONES_NAC  cambiado CSM 23/04/2006
'            bResult = lcNemotecnico.Buscar()
'            If bResult Then
'                Set lCursor = lcNemotecnico.Cursor
'            Else
'                Call Fnt_MsgError(lcNemotecnico.SubTipo_LOG, _
'                                    "Problemas en carga de Nemot�cnicos.", _
'                                    lcNemotecnico.ErrMsg, _
'                                    pConLog:=True)
'            End If
'        Else
'            lcNemotecnico.Campo("cod_instrumento").Valor = fCod_Instrumento 'gcINST_ACCIONES_NAC  cambiado CSM 23/04/2006
'            bResult = lcNemotecnico.BuscarPerfilRiesgo(fId_Cuenta)
'            If bResult Then
'                Set lCursor = lcNemotecnico.Cursor
'            Else
'                Call Fnt_MsgError(lcNemotecnico.SubTipo_LOG, _
'                                    "Problemas en carga de Nemot�cnicos.", _
'                                    lcNemotecnico.ErrMsg, _
'                                    pConLog:=True)
'            End If
'        End If
'        Set lcNemotecnico = Nothing
'    Else
'        Grilla_Nemo.ColHidden(10) = True  'Emisor
'        Set lcSaldos_Activos = New Class_Saldo_Activos
'        bResult = lcSaldos_Activos.Buscar_Ultimo_CierreCuenta(pId_Cuenta:=fId_Cuenta, _
'                                                              pCod_Instrumento:=fCod_Instrumento)
'        If Not bResult Then
'            MsgBox lcSaldos_Activos.ErrMsg, vbCritical, Me.Caption
'            GoTo ExitProcedure
'        Else
'            Set lCursor = lcSaldos_Activos.Cursor
'        End If
'        Set lcSaldos_Activos = Nothing
'    End If
'
'    If bResult Then
'        For Each lReg In lCursor
'            lLinea = Grilla_Nemo.Rows
'            Call Grilla_Nemo.AddItem("")
'            Call SetCell(Grilla_Nemo, lLinea, "id_nemotecnico", lReg("id_nemotecnico").Value, pAutoSize:=False)
'            Call SetCell(Grilla_Nemo, lLinea, "nemotecnico", lReg("nemotecnico").Value, pAutoSize:=False)
'            Call SetCell(Grilla_Nemo, lLinea, "dsc_nemotecnico", lReg("dsc_nemotecnico").Value, pAutoSize:=False)
'            If fOperacion = gcTipoOperacion_Egreso Then
'                Call SetCell(Grilla_Nemo, lLinea, "id_mov_activo", NVL(lReg("id_mov_activo").Value, 0), pAutoSize:=False)
'                Set lcAcciones = New Class_Acciones
'                lId_Nemotecnico = GetCell(Grilla_Nemo, lLinea, "id_nemotecnico")
'                Call SetCell(Grilla_Nemo, lLinea, "id_moneda", lReg("id_moneda_nemotecnico").Value, pAutoSize:=False)
'                Call SetCell(Grilla_Nemo, lLinea, "cantidad", lcAcciones.Saldo_Activo_Cantidad(fId_Cuenta, lId_Nemotecnico), pAutoSize:=False)
'                Set lcAcciones = Nothing
'            End If
'        Next
'    End If
'
'    Pnl_Nemo.Visible = True
'ExitProcedure:
'
'    Call Sub_Desbloquea_Puntero(Me)
'End Sub
Function Fnt_Valida_Linea(verificar_filas As Integer) As Variant
Dim col_g As Integer
Dim i As Integer

    Fnt_Valida_Linea = True
    
    Grilla.Redraw = False

'If Grilla.Col = c_emisor Then
'   FUNC_VALIDA_LINEA = FUNC_VALIDA_NEMO(Gen_Parametros.Fec_hoy_gen, Gr_Detalle.TextMatrix(Gr_Detalle.Row, c_nemo))
'   If Trim(Datos_Nemotecnico.Emisor) <> "" Then
'      FUNC_VALIDA_LINEA = False
'      Gr_Detalle.Redraw = True
'      Gr_Detalle.SetFocus
'      Exit Function
'   End If
'End If
If Not verificar_filas Then
   If Grilla.Row < 2 Then
      Grilla.Redraw = True
      Exit Function
   End If
   Grilla.Row = Grilla.Row - 1
End If

col_g = Grilla.Col

Const c_cantidad_nemo = 15
Const c_cantidad = 16
Const c_tipoprecio = 17
Const c_precio = 18
Const c_monto = 19

For i = c_nemo To c_monto
    Grilla.Col = i
    If c_cantidad_nemo <> i Then
        If Trim(Grilla.TextMatrix(Grilla.Row, i)) = "" Then
           Fnt_Valida_Linea = False
           Exit For
        End If
    End If
Next i
If Not verificar_filas Then
   Grilla.Row = Grilla.Row + 1
   Grilla.Redraw = True
End If
Grilla.Col = col_g
Grilla.SetFocus

End Function

Sub prdValidarColumnas(ByVal Formulario As Form, ByVal vsGrilla As VSFlexGrid, KeyAscii As Integer, ByVal intColumna As Integer, ByVal intTipos As Tipos)
    Dim inti As Integer
    If vsGrilla.Col = intColumna Then
        Select Case intTipos
            Case vb_vs_numerico
                If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> vbKeyReturn _
                    And KeyAscii <> vbKeyLeft And KeyAscii <> vbKeyRight And KeyAscii <> vbKeyUp _
                    And KeyAscii <> vbKeyDown Then
                    KeyAscii = False
                End If
            Case vb_vs_alfanumerico
                KeyAscii = Asc(UCase(Chr(KeyAscii)))
            Case vb_vs_decimal
                If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> vbKeyReturn _
                    And KeyAscii <> vbKeyLeft And KeyAscii <> vbKeyRight And KeyAscii <> vbKeyUp _
                    And KeyAscii <> vbKeyDown And KeyAscii <> 46 Then
                    KeyAscii = False
                End If
            Case vb_vs_tipoinversion
                KeyAscii = Asc(UCase(Chr(KeyAscii)))
                If Chr(KeyAscii) <> "C" And Chr(KeyAscii) <> "M" Then
                    KeyAscii = False
                Else
                    fTipoInversion = Chr(KeyAscii)
'                    Grilla.TextMatrix(fRowGrilla, c_TipoInversion) = fTipoInversion
'                    Call SetCell(Grilla, fRowGrilla, "tipo_inversion", fTipoInversion, False)
                    Call Grilla_AfterEdit(fRowGrilla, c_TipoInversion)
                End If
            Case vb_vs_tipoprecio
                KeyAscii = Asc(UCase(Chr(KeyAscii)))
                If Chr(KeyAscii) <> "L" And Chr(KeyAscii) <> "M" Then
                    KeyAscii = False
                Else
                    fTipoPrecio = Chr(KeyAscii)
'                    Grilla.TextMatrix(fRowGrilla, c_tipoprecio) = Chr(KeyAscii)
'                    Call SetCell(Grilla, fRowGrilla, "dsc_tipo_precio", Chr(KeyAscii), False)
'                    Call SetCell(Grilla, fRowGrilla, "KEY_TIPO_PRECIO", Chr(KeyAscii), False)
                    Call Grilla_AfterEdit(fRowGrilla, c_tipoprecio)
                End If
            Case vb_vs_fecha
      End Select
   End If
End Sub


Public Function Fnt_Valida_Perfil_Cuenta_Nemo() As Boolean
Dim lId_Perfil_Riesgo As String
Dim lDsc_Perfil_Riesgo As String
'--------------------------------------
Dim lLinea As Integer
Dim lcRel_PRiesgo_Instrumento As Class_Rel_PRiesgo_Instrumento
Dim lMensaje As String
'--------------------------------------
Dim lcRel_Perfiles_Nemotec As Class_Rel_Perfiles_Nemotec
Dim fId_Nemotecnico As String
'--------------------------------------
Dim lresult As Boolean

    lresult = True
  
    lId_Perfil_Riesgo = Txt_Perfil.Tag
    lDsc_Perfil_Riesgo = Txt_Perfil.Text
        
  '---------------------------------------------------------------------------------------
  
    Rem Busca en Perfil de Riesgo/Instrumento si existe la relacion
    Set lcRel_PRiesgo_Instrumento = New Class_Rel_PRiesgo_Instrumento
    With lcRel_PRiesgo_Instrumento
        .Campo("id_perfil_riesgo").Valor = lId_Perfil_Riesgo
        .Campo("cod_instrumento").Valor = fCod_Instrumento
        If .Buscar Then
            If .Cursor.Count > 0 Then
                Rem Si existe la relacion entonces sale de la validacion como verdadero
                Rem Los nemotecnicos estan asociados al perfil de riesgo a traves del Instrumento
                Set lcRel_PRiesgo_Instrumento = Nothing
                GoTo ErrProcedure
            End If
        Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en cargar Relaci�n Perfil de Riesgo/Instrumentos.", _
                              .ErrMsg, _
                              pConLog:=True)
            Set lcRel_PRiesgo_Instrumento = Nothing
            lresult = False
            GoTo ErrProcedure
        End If
    End With
    Set lcRel_PRiesgo_Instrumento = Nothing
  '---------------------------------------------------------------------------------------
  
    Rem Si no encontr� la relacion Perfil de Riesgo/Instrumento o el usuario permite la
    Rem operaci�n busca por cada Nemot�cnico de la grilla en Perfil de Riesgo/Nemotecnico
    Rem si existe la relacion
    lMensaje = ""
    Set lcRel_Perfiles_Nemotec = New Class_Rel_Perfiles_Nemotec
    With lcRel_Perfiles_Nemotec
        .Campo("id_nemotecnico").Valor = fId_Nemotecnico
        .Campo("id_perfil_riesgo").Valor = lId_Perfil_Riesgo
        If .Buscar_Perfiles_Nemotecnico Then
          If Not .Cursor.Count > 0 Then
            Rem No hay relacion entre el Perfil de Riesgo de la Cuenta y los Instrumentos
            lMensaje = "El Nemot�cnico " & fDsc_Nemotecnico & _
                       " no esta asignado al Perfil de Riesgo " & _
                       lDsc_Perfil_Riesgo & " asociado a la cuenta." & _
                       vbCr & vbCr & "�Desea continuar con la operaci�n?"
            If MsgBox(lMensaje, vbQuestion + vbYesNo) = vbNo Then
              Set lcRel_Perfiles_Nemotec = Nothing
              'GoTo ErrProcedure
              lresult = False
            End If
          End If
        Else
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en cargar Relaci�n Perfil de Riesgo/Nemot�cnicos.", _
                            .ErrMsg, _
                            pConLog:=True)
          Set lcRel_Perfiles_Nemotec = Nothing
          'GoTo ErrProcedure
          lresult = False
        End If
    End With
    Set lcRel_Perfiles_Nemotec = Nothing
  
  
ErrProcedure:
  Fnt_Valida_Perfil_Cuenta_Nemo = lresult
  
End Function


Private Sub Sub_EliminarLinea()
Dim lResp As VbMsgBoxResult
Dim lCol As Integer

    lResp = MsgBox("Desea eliminar esta l�nea?", vbYesNo, Me.Caption)
    If lResp = vbYes Then
        For lCol = 0 To MAX_COLUMNAS
            Grilla.TextMatrix(fRowGrilla, lCol) = ""
        Next
        Call Sub_Calcula_MontoTotal
        Grilla.Col = c_nemo
        Grilla.Row = IIf(fRowGrilla = 1, 1, fRowGrilla - 1)
    End If
End Sub

