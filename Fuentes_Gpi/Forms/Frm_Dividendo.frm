VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Dividendo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dividendo"
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5595
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   5595
   Begin VB.Frame Frame1 
      Caption         =   " Variaci�n de Capital "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3765
      Left            =   60
      TabIndex        =   5
      Top             =   420
      Width           =   5475
      Begin VB.CommandButton Cmb_Buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4560
         Picture         =   "Frm_Dividendo.frx":0000
         TabIndex        =   8
         ToolTipText     =   "Presione para buscar Nemotecnico."
         Top             =   2610
         Visible         =   0   'False
         Width           =   345
      End
      Begin MSComCtl2.DTPicker DTP_Fecha 
         Height          =   330
         Left            =   1470
         TabIndex        =   6
         Tag             =   "OBLI"
         Top             =   1080
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Format          =   64159745
         CurrentDate     =   38810
      End
      Begin hControl2.hTextLabel Txt_Monto 
         Height          =   315
         Left            =   150
         TabIndex        =   2
         Tag             =   "OBLI"
         Top             =   1470
         Width           =   5175
         _ExtentX        =   9128
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   500
         Caption         =   "Monto"
         Text            =   "0.0000000000"
         Text            =   "0.0000000000"
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.0000000000"
         Tipo_TextBox    =   1
      End
      Begin hControl2.hTextLabel Txt_Nemotecnico 
         Height          =   315
         Left            =   150
         TabIndex        =   0
         Top             =   300
         Width           =   5175
         _ExtentX        =   9128
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   500
         Caption         =   "Nemot�cnico"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel txt_moneda_nem 
         Height          =   315
         Left            =   150
         TabIndex        =   4
         Top             =   1830
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   500
         Caption         =   "Moneda Nemot."
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Nemotecnico_Opcion 
         Height          =   315
         Left            =   150
         TabIndex        =   7
         Tag             =   "OBLI=S"
         Top             =   2610
         Visible         =   0   'False
         Width           =   4395
         _ExtentX        =   7752
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Opci�n (-Osa)"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin hControl2.hTextLabel txt_Tipo_Variacion 
         Height          =   315
         Left            =   150
         TabIndex        =   11
         Top             =   690
         Width           =   5175
         _ExtentX        =   9128
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   500
         Caption         =   "Tipo Variaci�n"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel txt_cada_x_acciones 
         Height          =   315
         Left            =   150
         TabIndex        =   12
         Tag             =   "OBLI"
         Top             =   3000
         Visible         =   0   'False
         Width           =   5175
         _ExtentX        =   9128
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   500
         Caption         =   "Cada"
         Text            =   "0"
         Text            =   "0"
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0"
         Tipo_TextBox    =   1
      End
      Begin hControl2.hTextLabel txt_Entregar_x_Acciones 
         Height          =   315
         Left            =   150
         TabIndex        =   13
         Tag             =   "OBLI"
         Top             =   3360
         Visible         =   0   'False
         Width           =   5175
         _ExtentX        =   9128
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   500
         Caption         =   "Entregar"
         Text            =   "0.000000000000"
         Text            =   "0.000000000000"
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.000000000000"
         Tipo_TextBox    =   1
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Corte 
         Height          =   330
         Left            =   1470
         TabIndex        =   14
         Tag             =   "OBLI"
         Top             =   2220
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Format          =   64159745
         CurrentDate     =   38810
      End
      Begin VB.Label lbl_fecha_corte 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Cierre"
         Height          =   330
         Left            =   150
         TabIndex        =   10
         Top             =   1080
         Width           =   1305
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Pago"
         Height          =   330
         Left            =   150
         TabIndex        =   9
         Top             =   2220
         Width           =   1305
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   5595
      _ExtentX        =   9869
      _ExtentY        =   635
      ButtonWidth     =   1879
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Dividendo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Dim fId_Nemotecnico As String
Dim fId_Nemotecnico_Opcion As String
Dim fId_Moneda As String

Dim fTipoVariacion As String
Dim fFlg_Tipo_Permiso As String

Private Sub Cmb_TipoVariacion_ItemChange()
End Sub

Private Sub cmb_buscar_Click()
    If fKey = cNewEntidad Then
        fId_Nemotecnico_Opcion = ""
        Txt_Nemotecnico_Opcion.Text = Mid(Txt_Nemotecnico.Text, 1, 3)
        fId_Nemotecnico_Opcion = NVL(frm_Busca_Nemotecnicos.Buscar(pCod_Instrumento:=gcINST_ACCIONES_NAC, _
                                                                pNemotecnico:=Txt_Nemotecnico_Opcion.Text, _
                                                                pCodOperacion:=gcTipoOperacion_Ingreso), 0)
        If Not IsNull(fId_Nemotecnico_Opcion) Then
            Call Sub_Llena_Nemotecnico(fId_Nemotecnico_Opcion)
        End If
    End If
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pId_Dividendo, pId_Nemotecnico, pCod_Arbol_Sistema, pTipo_Variacion)
  fKey = pId_Dividendo
  fId_Nemotecnico = pId_Nemotecnico
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  If UCase(pTipo_Variacion) = "TODAS" Then
    pTipo_Variacion = "Dividendos"
  End If
  If UCase(pTipo_Variacion) = "OPCION" Then
    fTipoVariacion = "Opci�n"
    If fKey = cNewEntidad Then
        Txt_Nemotecnico_Opcion.Locked = False
        Txt_Nemotecnico_Opcion.BackColorTxt = fColorOBligatorio
    Else
        Txt_Nemotecnico_Opcion.Locked = True
        Txt_Nemotecnico_Opcion.BackColorTxt = fColorNoEdit
    End If
    Txt_Nemotecnico_Opcion.Visible = True
    Cmb_Buscar.Visible = True
    txt_cada_x_acciones.Visible = True
    txt_Entregar_x_Acciones.Visible = True
  Else
    fTipoVariacion = "Dividendo"
    Txt_Nemotecnico_Opcion.Visible = False
    Cmb_Buscar.Visible = False
    txt_cada_x_acciones.Visible = False
    txt_Entregar_x_Acciones.Visible = False
  End If
  
  txt_Tipo_Variacion.Text = pTipo_Variacion
      
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de " & Trim(pTipo_Variacion)
  Else
    Call Sub_CargarDatos
    Me.Caption = "Modificaci�n de " & Trim(pTipo_Variacion)
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        If fKey = cNewEntidad Then
          Call Sub_Limpiar
        Else
          Call Sub_CargarDatos
        End If
      End If
  End Select
End Sub

Private Sub Sub_Limpiar()
  DTP_Fecha.Value = Format(Fnt_FechaServidor, cFormatDate)
  Txt_Monto.Text = 0
  DTP_Fecha_Corte.Value = Format(Fnt_FechaServidor, cFormatDate)
  Txt_Nemotecnico_Opcion.Visible = False
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcDividendo As Class_VariacionCapital
  
  Fnt_Grabar = True
  
  If Not Fnt_Valida Then
    Fnt_Grabar = False
    Exit Function
  End If
  
  Set lcDividendo = New Class_VariacionCapital
  With lcDividendo
    .Campo("Id_Dividendo").Valor = IIf(fKey = cNewEntidad, 0, fKey)
    .Campo("Id_Nemotecnico").Valor = fId_Nemotecnico
    .Campo("Fecha").Valor = DTP_Fecha.Value
    .Campo("Monto").Valor = To_Number(Txt_Monto.Text)
    .Campo("Fecha_Corte").Valor = DTP_Fecha_Corte.Value
    .Campo("Tipo_Variacion").Valor = UCase(txt_Tipo_Variacion.Text)
    .Campo("Id_Nemotecnico_opcion").Valor = fId_Nemotecnico_Opcion
    .Campo("cada_x_cantidad").Valor = txt_cada_x_acciones.Text
    .Campo("entregar_x_cantidad").Valor = txt_Entregar_x_Acciones.Text
    If .Guardar Then
      MsgBox "Variaci�n guardada correctamente.", vbInformation, Me.Caption
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      Fnt_Grabar = False
    End If
  End With
  Set lcDividendo = Nothing
  
End Function

Private Sub Sub_CargaForm()
Dim lcNemotecnico As Class_Nemotecnicos
Dim lcMoneda As Object
  
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = fId_Nemotecnico
    If .Buscar Then
      Txt_Nemotecnico.Text = .Cursor(1)("nemotecnico").Value
      fId_Moneda = .Cursor(1)("id_moneda").Value
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcNemotecnico = Nothing
  
'  Set lcMoneda = New Class_Monedas
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    .Campo("id_moneda").Valor = fId_Moneda
    If .Buscar Then
      txt_moneda_nem.Text = .Cursor(1)("dsc_moneda").Value
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcMoneda = Nothing
  
  Call Sub_Limpiar
  
End Sub

Private Sub Sub_CargarDatos()
Dim lcDividendo As Class_VariacionCapital
Dim lcNemotecnico As Class_Nemotecnicos
  Load Me
  Set lcDividendo = New Class_VariacionCapital
  With lcDividendo
    .Campo("id_dividendo").Valor = fKey
    If .Buscar Then
      If .Cursor.Count > 0 Then
        DTP_Fecha.Value = .Cursor(1)("fecha").Value
        Txt_Monto.Text = To_Number(.Cursor(1)("monto").Value)
        DTP_Fecha_Corte.Value = .Cursor(1)("Fecha_corte").Value
        txt_Tipo_Variacion.Text = .Cursor(1)("Tipo_Variacion").Value
        txt_cada_x_acciones.Text = .Cursor(1)("cada_x_cantidad").Value
        txt_Entregar_x_Acciones.Text = .Cursor(1)("entregar_x_cantidad").Value
        If Not NVL(.Cursor(1)("id_nemotecnico_opcion").Value, 0) = 0 Then
            fId_Nemotecnico_Opcion = lcDividendo.Cursor(1)("id_nemotecnico_opcion").Value
            Set lcNemotecnico = New Class_Nemotecnicos
            With lcNemotecnico
              .Campo("id_nemotecnico").Valor = lcDividendo.Cursor(1)("id_nemotecnico_opcion").Value
              If .Buscar Then
                Txt_Nemotecnico_Opcion.Text = .Cursor(1)("nemotecnico").Value
              Else
                MsgBox .ErrMsg, vbCritical, Me.Caption
              End If
            End With
            Set lcNemotecnico = Nothing
        End If
      End If
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcDividendo = Nothing
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function

Private Function Fnt_Valida() As Boolean
  Fnt_Valida = True
  
    If To_Number(Txt_Monto.Text) <= 0 Then
      Fnt_Valida = False
      MsgBox "Debe ingresar un monto v�lido para " & fTipoVariacion & ".", vbExclamation, Me.Caption
      Exit Function
    End If
    If fTipoVariacion = "Opci�n" Then
        If To_Number(txt_cada_x_acciones.Text) <= 0 Then
          Fnt_Valida = False
          MsgBox "Debe ingresar un monto v�lido para cada Acci�n.", vbExclamation, Me.Caption
          txt_cada_x_acciones.SetFocus
          Exit Function
        End If
        
        If To_Number(txt_Entregar_x_Acciones.Text) <= 0 Then
          Fnt_Valida = False
          MsgBox "Debe ingresar un monto v�lido en Entregar.", vbExclamation, Me.Caption
          txt_Entregar_x_Acciones.SetFocus
          Exit Function
        End If
        
        If Txt_Nemotecnico_Opcion.Text = "" Then
          Fnt_Valida = False
          MsgBox "Debe ingresar el Nemotecnico", vbInformation, Me.Caption
          Txt_Nemotecnico_Opcion.SetFocus
          Exit Function
        End If
    End If
End Function

Private Sub Sub_Llena_Nemotecnico(pId_Nemotecnico)
Dim lcNemotecnico As Class_Nemotecnicos
Dim lReg As hFields

  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .BuscarView Then
      For Each lReg In .Cursor
        Txt_Nemotecnico_Opcion.Text = Trim(lReg("nemotecnico").Value)
      Next
    End If
  End With
  Set lcNemotecnico = Nothing
End Sub
