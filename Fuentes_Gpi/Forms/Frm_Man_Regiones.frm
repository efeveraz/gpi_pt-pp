VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Man_Regiones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Regiones"
   ClientHeight    =   4200
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   8355
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   8355
   Begin VB.Frame Frame1 
      Caption         =   "Regiones"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3195
      Left            =   0
      TabIndex        =   0
      Top             =   840
      Width           =   8325
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   2835
         Left            =   120
         TabIndex        =   1
         Top             =   300
         Width           =   8085
         _cx             =   14261
         _cy             =   5001
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Regiones.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   8355
      _ExtentX        =   14737
      _ExtentY        =   635
      ButtonWidth     =   2037
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Eliminar"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Elimina un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un elemento"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   7560
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin TrueDBList80.TDBCombo Cmb_Pais 
      Height          =   345
      Left            =   1560
      TabIndex        =   5
      Tag             =   "OBLI=S;CAPTION=Pa�s"
      Top             =   480
      Width           =   4905
      _ExtentX        =   8652
      _ExtentY        =   609
      _LayoutType     =   0
      _RowHeight      =   -2147483647
      _WasPersistedAsPixels=   0
      _DropdownWidth  =   0
      _EDITHEIGHT     =   609
      _GAPHEIGHT      =   53
      Columns(0)._VlistStyle=   0
      Columns(0)._MaxComboItems=   5
      Columns(0).DataField=   "ub_grid1"
      Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
      Columns(1)._VlistStyle=   0
      Columns(1)._MaxComboItems=   5
      Columns(1).DataField=   "ub_grid2"
      Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
      Columns.Count   =   2
      Splits(0)._UserFlags=   0
      Splits(0).ExtendRightColumn=   -1  'True
      Splits(0).AllowRowSizing=   0   'False
      Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
      Splits(0)._ColumnProps(0)=   "Columns.Count=2"
      Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
      Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
      Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
      Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
      Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
      Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
      Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
      Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
      Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
      Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
      Splits.Count    =   1
      Appearance      =   3
      BorderStyle     =   1
      ComboStyle      =   0
      AutoCompletion  =   -1  'True
      LimitToList     =   0   'False
      ColumnHeaders   =   0   'False
      ColumnFooters   =   0   'False
      DataMode        =   5
      DefColWidth     =   0
      Enabled         =   -1  'True
      HeadLines       =   1
      FootLines       =   1
      RowDividerStyle =   0
      Caption         =   ""
      EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
      LayoutName      =   ""
      LayoutFileName  =   ""
      MultipleLines   =   0
      EmptyRows       =   0   'False
      CellTips        =   0
      AutoSize        =   0   'False
      ListField       =   ""
      BoundColumn     =   ""
      IntegralHeight  =   0   'False
      CellTipsWidth   =   0
      CellTipsDelay   =   1000
      AutoDropdown    =   -1  'True
      RowTracking     =   -1  'True
      RightToLeft     =   0   'False
      MouseIcon       =   0
      MouseIcon.vt    =   3
      MousePointer    =   0
      MatchEntryTimeout=   2000
      OLEDragMode     =   0
      OLEDropMode     =   0
      AnimateWindow   =   3
      AnimateWindowDirection=   5
      AnimateWindowTime=   200
      AnimateWindowClose=   1
      DropdownPosition=   0
      Locked          =   0   'False
      ScrollTrack     =   -1  'True
      ScrollTips      =   -1  'True
      RowDividerColor =   14215660
      RowSubDividerColor=   14215660
      MaxComboItems   =   10
      AddItemSeparator=   ";"
      _PropDict       =   $"Frm_Man_Regiones.frx":0080
      _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
      _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
      _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
      _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
      _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
      _StyleDefs(5)   =   ":id=0,.fontname=Arial"
      _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
      _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
      _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
      _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
      _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
      _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
      _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
      _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
      _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
      _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
      _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
      _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
      _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
      _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
      _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
      _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
      _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
      _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
      _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
      _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
      _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
      _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
      _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
      _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
      _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
      _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
      _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
      _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
      _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
      _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
      _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
      _StyleDefs(38)  =   "Named:id=33:Normal"
      _StyleDefs(39)  =   ":id=33,.parent=0"
      _StyleDefs(40)  =   "Named:id=34:Heading"
      _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(42)  =   ":id=34,.wraptext=-1"
      _StyleDefs(43)  =   "Named:id=35:Footing"
      _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(45)  =   "Named:id=36:Selected"
      _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
      _StyleDefs(47)  =   "Named:id=37:Caption"
      _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
      _StyleDefs(49)  =   "Named:id=38:HighlightRow"
      _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
      _StyleDefs(51)  =   "Named:id=39:EvenRow"
      _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
      _StyleDefs(53)  =   "Named:id=40:OddRow"
      _StyleDefs(54)  =   ":id=40,.parent=33"
      _StyleDefs(55)  =   "Named:id=41:RecordSelector"
      _StyleDefs(56)  =   ":id=41,.parent=34"
      _StyleDefs(57)  =   "Named:id=42:FilterBar"
      _StyleDefs(58)  =   ":id=42,.parent=33"
   End
   Begin VB.Label Label3 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Pa�s"
      Height          =   345
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   1305
   End
End
Attribute VB_Name = "Frm_Man_Regiones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFlg_Tipo_Permiso   As String
Dim fCod_Arbol_Sistema  As String

Public Sub Mostrar(pCod_Arbol_Sistema)
    fCod_Arbol_Sistema = pCod_Arbol_Sistema
    fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
    
    Call Form_Resize
    
    Load Me
    
End Sub

Private Sub Cmb_Pais_Change()
   '  Call Sub_CargarDatos
End Sub

Private Sub Cmb_Pais_LostFocus()
    Call Sub_CargarDatos
End Sub

Private Sub Form_Load()
  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("ADD").Image = cBoton_Agregar_Grilla
        .Buttons("DEL").Image = cBoton_Eliminar_Grilla
        .Buttons("EXIT").Image = cBoton_Salir
        .Buttons("UPDATE").Image = cBoton_Modificar
        .Buttons("REFRESH").Image = cBoton_Refrescar
        .Buttons("PRINTER").Image = cBoton_Imprimir
  End With
  
  Call Sub_CargaCombo_Paises(Cmb_Pais)
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
  
End Sub

Private Sub Sub_CargarDatos()
    Dim lReg    As hCollection.hFields
    Dim lLinea  As Long
    Dim lID     As String
    
    Dim oRegion As Class_Regiones
  
    Call Sub_Bloquea_Puntero(Me)
    
    lID = Fnt_ComboSelected_KEY(Cmb_Pais)
    
    '----------------------------------------------------------------------------------------
    '-- Carga de regiones
    '----------------------------------------------------------------------------------------
    Set oRegion = New Class_Regiones
    If lID <> "" Then
        oRegion.Campo("cod_pais").Valor = lID
        
        If oRegion.Buscar() Then
            With Grilla
                .Rows = 1
                For Each lReg In oRegion.Cursor
                    lLinea = .Rows
                    .Rows = lLinea + 1
                    SetCell Grilla, lLinea, "colum_pk", lReg("id_region").Value
                    SetCell Grilla, lLinea, "pais", lReg("dsc_pais").Value
                    SetCell Grilla, lLinea, "dsc_region", lReg("dsc_region").Value
                Next
            End With
        Else
            MsgBox oRegion.ErrMsg, vbCritical, Me.Caption
        End If
    End If
    
    Set oRegion = Nothing
    
    If Not lID = "" Then
        Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
        If Not Grilla.Row = cNewEntidad Then
            Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
        End If
    End If
    
    Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Form_Resize()
    Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    With Grilla
        If Not Trim(GetCell(Grilla, Row, "colum_pk")) = "" Then
            Cancel = True
        End If
    End With
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents

    Select Case Button.Key
        Case "UPDATE"
            Call Grilla_DblClick
        Case "REFRESH"
            Call Sub_CargarDatos
        Case "DEL"
            Call Sub_EliminarItem
        Case "ADD"
            Call Sub_Agregar
        Case "EXIT"
            Unload Me
        Case "PRINTER"
            Call Sub_Imprimir(ePrinter.eP_Impresora)
    End Select
End Sub

Private Sub Grilla_DblClick()
    Dim lKey As String

    With Grilla
        If .Row > 0 Then
            lKey = GetCell(Grilla, .Row, "colum_pk")
            Call Sub_EsperaVentana(lKey)
        End If
    End With
End Sub

Private Sub Sub_Agregar()
    Call Sub_EsperaVentana(cNewEntidad)
End Sub

Private Sub Sub_EsperaVentana(ByVal pKey)
     Dim lForm       As Frm_Regiones
    Dim lNombre         As String
    Dim lNom_Form       As String
    Dim sIdPais         As String
  
    Me.Enabled = False
    lNom_Form = "Frm_Regiones"
  
    If Not Fnt_ExisteVentanaKey(lNom_Form, pKey) Then
        lNombre = Me.Name
        
        sIdPais = Fnt_ComboSelected_KEY(Cmb_Pais)
    
        Set lForm = New Frm_Regiones
        Call lForm.Fnt_Modificar(pKey, _
                                pCod_Arbol_Sistema:=fCod_Arbol_Sistema, _
                                Cod_Pais:=sIdPais)
    
        Do While Fnt_ExisteVentanaKey(lNom_Form, pKey)
            DoEvents
        Loop
    
        If Fnt_ExisteVentana(lNombre) Then
            Call Sub_CargarDatos
        Else
            Exit Sub
        End If
        
    End If
    
    Me.Enabled = True
End Sub

Private Sub Sub_CargaForm()

  'Limpia la grilla
  Grilla.Rows = 1
  
  Call Sub_FormControl_Color(Me.Controls)

End Sub

Private Sub Sub_EliminarItem()
    Dim lID             As String
    Dim oRegiones       As Class_Regiones
    Dim oComunaCiudad   As Class_Comuna_Ciudad

    If Grilla.Row > 0 Then
        If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
            lID = GetCell(Grilla, Grilla.Row, "colum_pk")
      
            If Not lID = "" Then
                Set oComunaCiudad = New Class_Comuna_Ciudad
                With oComunaCiudad
                    .Campo("ID_REGION").Valor = lID
                    
                    If Not .Borrar Then
                        MsgBox .ErrMsg, vbCritical, Me.Caption
                    End If
                End With
                Set oComunaCiudad = Nothing
        
                Set oRegiones = New Class_Regiones
                With oRegiones
                    .Campo("ID_REGION").Valor = lID
                    If .Borrar Then
                        MsgBox "Regi�n y sus Comunas/Ciudades fueron eliminadas correctamente.", vbInformation + vbOKOnly, Me.Caption
                    Else
                        If .Errnum = 440 Then ' Si la Region tiene hijos asociados
                            MsgBox "La Regi�n no puede ser eliminada, ya que est� asociada a otros componentes.", vbCritical, Me.Caption
                        Else
                          MsgBox .ErrMsg, vbCritical, Me.Caption
                        End If
                    End If
                End With
                Set oRegiones = Nothing
            End If
    
            Call Sub_CargarDatos
        End If
    End If
End Sub
Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
    Dim lForm As Frm_Reporte_Generico
  
    Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Listado de Regiones" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub


Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    Me.SetFocus
    DoEvents

    Select Case ButtonMenu.Key
        Case "SCREEN"
            Call Sub_Imprimir(ePrinter.eP_Pantalla)
        Case "PDF"
            Call Sub_Imprimir(ePrinter.eP_PDF)
    End Select
    
End Sub
