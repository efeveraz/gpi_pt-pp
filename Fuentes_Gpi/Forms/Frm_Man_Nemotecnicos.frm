VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Man_Nemotecnicos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Nemotécnicos"
   ClientHeight    =   7200
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   12885
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   204
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7200
   ScaleWidth      =   12885
   Begin VB.Frame Frame1 
      Caption         =   "Atributos de Nemotécnicos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   6795
      Left            =   15
      TabIndex        =   0
      Top             =   390
      Width           =   12855
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1395
         Left            =   60
         TabIndex        =   7
         Top             =   180
         Width           =   6795
         Begin VB.CommandButton Cmb_Buscar_Nemo 
            Caption         =   "?"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   6360
            Picture         =   "Frm_Man_Nemotecnicos.frx":0000
            TabIndex        =   8
            ToolTipText     =   "Presione para buscar Nemotecnico."
            Top             =   1290
            Visible         =   0   'False
            Width           =   345
         End
         Begin TrueDBList80.TDBCombo Cmb_Productos 
            Height          =   345
            Left            =   1530
            TabIndex        =   9
            Tag             =   "OBLI=S;CAPTION=Producto;SOLOLECTURA=N"
            Top             =   180
            Width           =   5085
            _ExtentX        =   8969
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Man_Nemotecnicos.frx":030A
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin hControl2.hTextLabel Txt_Nemotecnico 
            Height          =   345
            Left            =   90
            TabIndex        =   10
            Top             =   600
            Width           =   6525
            _ExtentX        =   11509
            _ExtentY        =   609
            LabelWidth      =   1425
            Caption         =   " Nemotécnico"
            Text            =   ""
            BackColorTxt    =   16777215
            BackColorTxt    =   16777215
         End
         Begin VB.Label Label7 
            BorderStyle     =   1  'Fixed Single
            Caption         =   " Producto"
            Height          =   345
            Left            =   90
            TabIndex        =   11
            Top             =   180
            Width           =   1425
         End
      End
      Begin VB.Frame Fra_Planilla 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1395
         Left            =   6990
         TabIndex        =   4
         Top             =   180
         Width           =   5805
         Begin VB.OptionButton Opt_Accion 
            Caption         =   "Solo Nemotécnicos Vigentes"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   210
            TabIndex        =   6
            Top             =   250
            Value           =   -1  'True
            Width           =   2865
         End
         Begin VB.OptionButton Opt_Todos 
            Caption         =   "Todos"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   210
            TabIndex        =   5
            Top             =   1050
            Width           =   1065
         End
         Begin MSComctlLib.Toolbar Toolbar_Proceso 
            Height          =   420
            Left            =   4680
            TabIndex        =   12
            Top             =   840
            Width           =   930
            _ExtentX        =   1640
            _ExtentY        =   741
            ButtonWidth     =   1561
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Buscar"
                  Key             =   "BUSC"
                  Description     =   "Valoriza el nemotécnico a la tasa de inversión."
               EndProperty
            EndProperty
         End
         Begin hControl2.hTextLabel Txt_CodIsin 
            Height          =   345
            Left            =   480
            TabIndex        =   13
            Top             =   590
            Width           =   2925
            _ExtentX        =   5159
            _ExtentY        =   609
            LabelWidth      =   750
            Caption         =   " ISIN"
            Text            =   ""
            BackColorTxt    =   16777215
            BackColorTxt    =   16777215
         End
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   5115
         Left            =   60
         TabIndex        =   1
         Top             =   1620
         Width           =   12735
         _cx             =   22463
         _cy             =   9022
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   6
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Nemotecnicos.frx":03B4
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   12885
      _ExtentX        =   22728
      _ExtentY        =   635
      ButtonWidth     =   2037
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Eliminar"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Elimina un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un elemento"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Description     =   "Actualiza los datos de la ventana."
            Object.ToolTipText     =   "Actualiza los datos de la ventana."
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Nemotecnicos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String
Dim lId_Nemotecnico As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Cmb_Buscar_Nemo_Click()
Dim lProductos As String
Dim lNemotecnico As String
lProductos = Fnt_ComboSelected_KEY(Cmb_Productos)
If Not lProductos = "" Then
    'lCod_Instrumento = Fnt_ComboSelected_KEY(Cmb_Instrumento)
    lId_Nemotecnico = NVL(frm_Busca_Nemotecnicos.Buscar(pNemotecnico:=Txt_Nemotecnico.Text, _
                                    pCodOperacion:=gcTipoOperacion_Ingreso, _
                                    pCod_Producto:=lProductos), 0)
    Call Sub_Entrega_DatosNemotecnico(lId_Nemotecnico, lNemotecnico)
    Txt_Nemotecnico.Text = lNemotecnico
Else
    Txt_Nemotecnico.Text = ""
    lId_Nemotecnico = 0
    MsgBox "Primero Debe Ingresar Código de Producot", vbInformation
End If

End Sub

Private Sub Cmb_Productos_ItemChange()
'  Call Sub_CargarDatos
End Sub

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
     .Buttons("ADD").Image = cBoton_Agregar_Grilla
     .Buttons("DEL").Image = cBoton_Eliminar_Grilla
     .Buttons("EXIT").Image = cBoton_Salir
     .Buttons("UPDATE").Image = cBoton_Modificar
     .Buttons("REFRESH").Image = cBoton_Refrescar
     .Buttons("PRINTER").Image = cBoton_Imprimir
  End With
  
  Call Sub_CargaForm
  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
  
  Rem Limpia la grilla
  Grilla.Rows = 1

  With Toolbar_Proceso
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("BUSC").Image = "boton_grilla_buscar"
    .Appearance = ccFlat
  End With

  Call Sub_FormControl_Color(Me.Controls)
  
  Call Sub_CargaCombo_Productos(Cmb_Productos, pBlanco:=True)
  
  With Cmb_Productos
    .Text = ""
  End With
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub


Private Sub Grilla_DblClick()
Dim lKey As String
Dim lForm As Form

  With Grilla
    If .Row > 0 Then
      lKey = GetCell(Grilla, .Row, "colum_pk")
      Call Sub_EsperaVentana(lKey)
    End If
  End With
End Sub



Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agregar
    Case "EXIT"
      Unload Me
    Case "UPDATE"
      Call Grilla_DblClick
    Case "REFRESH"
      Call Sub_CargarDatos
    Case "DEL"
      Call Sub_Eliminar
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Sub_CargarDatos()
Dim lcNemotecnico As Class_Nemotecnicos
'-----------------------------------------
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lId_producto As String
   
  If Fnt_ComboSelected_KEY(Cmb_Productos) = "" Then
    lId_producto = -999 'No existe este ID
  Else
    lId_producto = Fnt_FindValue4Display(Cmb_Productos, Cmb_Productos.Text)
  End If
  
  Me.Enabled = False
  Call Sub_Bloquea_Puntero(Me)

  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Grilla.Rows = 1
  
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    If .BuscarAyuda(pCod_Producto:=lId_producto, pNemotecnico:=Txt_Nemotecnico.Text, pCodOperacion:=gcTipoOperacion_Ingreso, pVigente:=IIf(Opt_Accion.Value = True, "S", "N"), pCod_Isin:=Txt_CodIsin.Text) Then
      If .Cursor.Count > 0 Then
        Call Prc_ComponentOne.Sub_hRecord2Grilla(.Cursor, Grilla, "id_nemotecnico")
      Else
        If lId_producto <> "-999" Then
            MsgBox "No Existen Información Solicitada.", vbInformation + vbOKOnly, Me.Caption
        End If
      End If
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcNemotecnico = Nothing
  
  
  Call Sub_AjustaColumnas_Grilla(Grilla)

  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If

  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)
  
  Set lcNemotecnico = Nothing
End Sub

Private Sub Sub_Agregar()
  Call Sub_EsperaVentana(cNewEntidad)
End Sub

Private Sub Sub_EsperaVentana(pkey)
Dim lForm     As Frm_Nemotecnicos
Dim lNombre   As String
Dim lNom_Form As String
Dim lId_producto As String
  
  Me.Enabled = False
  Call Sub_Bloquea_Puntero(Me)
  lNom_Form = "Frm_Nemotecnicos"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lNombre = Me.Name
    
    If Fnt_ComboSelected_KEY(Cmb_Productos) = "" Then
        lId_producto = -999 'No existe este ID
    Else
        lId_producto = Fnt_FindValue4Display(Cmb_Productos, Cmb_Productos.Text)
    End If
    
    Set lForm = New Frm_Nemotecnicos
    
    Call lForm.Fnt_Modificar(pColum_PK:=pkey _
                           , pCod_Arbol_Sistema:=fCod_Arbol_Sistema _
                           , pCod_Producto:=lId_producto)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      Call Sub_CargarDatos
    Else
      Exit Sub
    End If
  End If
  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Eliminar()
Dim lcNemotecnico As Class_Nemotecnicos
'-----------------------------------------
Dim lColum_PK As String
Dim lResult As Boolean
Dim lElimino As Boolean
Dim lcRel_Nemo_Valor_Clasif As Class_Rel_Nemotec_Valor_Clasific
    
  Call Sub_Bloquea_Puntero(Me)
    
  gDB.IniciarTransaccion
  
  lResult = True
  lElimino = False
  If Grilla.Row > 0 Then
    If MsgBox("¿Seguro que desea eliminar?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      lColum_PK = GetCell(Grilla, Grilla.Row, "colum_pk")
      If Not lColum_PK = "" Then
        Rem Elimina todos los clasificadores de riesgo para el nemotecnico
        Set lcRel_Nemo_Valor_Clasif = New Class_Rel_Nemotec_Valor_Clasific
        With lcRel_Nemo_Valor_Clasif
          .Campo("Id_Nemotecnico").Valor = lColum_PK
          If Not .Borrar Then
            MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            lResult = False
            GoTo ErrProcedure
          End If
        End With
        Set lcRel_Nemo_Valor_Clasif = Nothing
                
        Set lcNemotecnico = New Class_Nemotecnicos
        With lcNemotecnico
          .Campo("id_nemotecnico").Valor = lColum_PK
          If .Borrar Then
            MsgBox "Nemotécnico eliminado correctamente.", vbInformation + vbOKOnly, Me.Caption
          Else
            'JGR 03-Ago-09
            If .Errnum = -2147217873 Then
               MsgBox "El Nemotécnico no ha podido ser eliminado. " & vbCr & "Este está siendo utilizado en el sistema", vbCritical, Me.Caption
            Else
               MsgBox .ErrMsg, vbCritical, Me.Caption
            End If
            lResult = False
            GoTo ErrProcedure
          End If
        End With
        lElimino = True
      End If
    End If
  End If
  
ErrProcedure:
  If lResult Then
    gDB.CommitTransaccion
    If lElimino Then Call Sub_CargarDatos
  Else
    gDB.RollbackTransaccion
  End If
  Set lcNemotecnico = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
Dim lProducto As String

  Set lForm = New Frm_Reporte_Generico
  lProducto = Cmb_Productos.Text
  
  Rem Comienzo de la generación del reporte
  With lForm
    
      .VSPrinter.MarginLeft = 600
      .VSPrinter.MarginRight = 600
    
    Call .Sub_InicarDocumento(pTitulo:="Listado de Nemotécnicos" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
     
    With .VSPrinter
      .FontSize = 10
      .Paragraph = "Producto: " & lProducto
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 6
      
      Call Sub_Grilla2VsPrinter(lForm.VSPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub
Private Sub Toolbar_Proceso_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim lProductos As String
    lProductos = Fnt_ComboSelected_KEY(Cmb_Productos)
    If Not lProductos = "" Then
    Else
       MsgBox "Primero debe elegir un Producto.", vbInformation + vbOKOnly, Me.Caption
       Exit Sub
    End If
    Me.SetFocus
    DoEvents
    Select Case Button.Key
        Case "BUSC"
            Call Sub_CargarDatos
    End Select
End Sub
Private Sub Txt_Nemotecnico_KeyPress(KeyAscii As Integer)
    KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub
