VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Object = "{1BCC7098-34C1-4749-B1A3-6C109878B38F}#1.0#0"; "vspdf8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Cartola_Security_Consolidado 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cartola Consolidada"
   ClientHeight    =   6540
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9810
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6540
   ScaleWidth      =   9810
   Begin VB.Frame Frame1 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1155
      Left            =   120
      TabIndex        =   9
      Top             =   420
      Width           =   9615
      Begin VB.OptionButton optOpciones 
         Caption         =   "Genera Archivo"
         Height          =   210
         Index           =   2
         Left            =   120
         TabIndex        =   13
         Top             =   720
         Width           =   1695
      End
      Begin VB.OptionButton optOpciones 
         Caption         =   "Por Impresora"
         Height          =   210
         Index           =   1
         Left            =   120
         TabIndex        =   12
         Top             =   480
         Width           =   1455
      End
      Begin VB.OptionButton optOpciones 
         Caption         =   "Por Pantalla"
         Height          =   210
         Index           =   0
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Value           =   -1  'True
         Width           =   1455
      End
      Begin VB.CommandButton Cmb_BuscaFile 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8850
         TabIndex        =   10
         ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
         Top             =   600
         Width           =   345
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
         Height          =   345
         Left            =   3645
         TabIndex        =   14
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         Format          =   20840449
         CurrentDate     =   38938
      End
      Begin hControl2.hTextLabel Txt_Directorio 
         Height          =   315
         Left            =   2400
         TabIndex        =   15
         Tag             =   "OBLI"
         Top             =   600
         Width           =   6360
         _ExtentX        =   11218
         _ExtentY        =   556
         LabelWidth      =   1200
         TextMinWidth    =   1200
         Caption         =   "Directorio"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Cartola"
         Height          =   345
         Index           =   0
         Left            =   2400
         TabIndex        =   16
         Top             =   240
         Width           =   1185
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Datos de B�squeda"
      ForeColor       =   &H000000FF&
      Height          =   1575
      Left            =   120
      TabIndex        =   0
      Top             =   1680
      Width           =   9615
      Begin VB.CommandButton Btn_Buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   6120
         Picture         =   "Frm_Reporte_Cartola_Security_Consolidado.frx":0000
         TabIndex        =   1
         Top             =   720
         Width           =   375
      End
      Begin VSPrinter8LibCtl.VSPrinter vp 
         Height          =   615
         Left            =   7080
         TabIndex        =   2
         Top             =   840
         Visible         =   0   'False
         Width           =   1335
         _cx             =   2355
         _cy             =   1085
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         MousePointer    =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoRTF         =   -1  'True
         Preview         =   -1  'True
         DefaultDevice   =   0   'False
         PhysicalPage    =   -1  'True
         AbortWindow     =   -1  'True
         AbortWindowPos  =   0
         AbortCaption    =   "Printing..."
         AbortTextButton =   "Cancel"
         AbortTextDevice =   "on the %s on %s"
         AbortTextPage   =   "Now printing Page %d of"
         FileName        =   ""
         MarginLeft      =   1440
         MarginTop       =   1440
         MarginRight     =   1440
         MarginBottom    =   1440
         MarginHeader    =   0
         MarginFooter    =   0
         IndentLeft      =   0
         IndentRight     =   0
         IndentFirst     =   0
         IndentTab       =   720
         SpaceBefore     =   0
         SpaceAfter      =   0
         LineSpacing     =   100
         Columns         =   1
         ColumnSpacing   =   180
         ShowGuides      =   2
         LargeChangeHorz =   300
         LargeChangeVert =   300
         SmallChangeHorz =   30
         SmallChangeVert =   30
         Track           =   0   'False
         ProportionalBars=   -1  'True
         Zoom            =   -1.15864527629234
         ZoomMode        =   3
         ZoomMax         =   400
         ZoomMin         =   10
         ZoomStep        =   25
         EmptyColor      =   -2147483636
         TextColor       =   0
         HdrColor        =   0
         BrushColor      =   0
         BrushStyle      =   0
         PenColor        =   0
         PenStyle        =   0
         PenWidth        =   0
         PageBorder      =   0
         Header          =   ""
         Footer          =   ""
         TableSep        =   "|;"
         TableBorder     =   7
         TablePen        =   0
         TablePenLR      =   0
         TablePenTB      =   0
         NavBar          =   3
         NavBarColor     =   -2147483633
         ExportFormat    =   0
         URL             =   ""
         Navigation      =   3
         NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
         AutoLinkNavigate=   0   'False
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
      End
      Begin MSComDlg.CommonDialog Cmd_AbreArchivo 
         Left            =   8400
         Top             =   960
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin TrueDBList80.TDBCombo Cmb_GruposCuentas 
         Height          =   345
         Left            =   1320
         TabIndex        =   3
         Tag             =   "OBLI=S;CAPTION=Cuenta"
         Top             =   720
         Width           =   4605
         _ExtentX        =   8123
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Cartola_Security_Consolidado.frx":030A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Moneda 
         Height          =   345
         Left            =   1320
         TabIndex        =   4
         Tag             =   "OBLI=S;CAPTION=Moneda"
         Top             =   1080
         Width           =   3525
         _ExtentX        =   6218
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Cartola_Security_Consolidado.frx":03B4
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Empresa 
         Height          =   345
         Left            =   1320
         TabIndex        =   5
         Tag             =   "OBLI=S;CAPTION=Empresa"
         Top             =   360
         Width           =   4605
         _ExtentX        =   8123
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   2
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   0   'False
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   0
         AnimateWindowDirection=   0
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Cartola_Security_Consolidado.frx":045E
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin hControl2.hTextLabel Txt_NombreCliente 
         Height          =   315
         Left            =   1320
         TabIndex        =   19
         Tag             =   "OBLI"
         Top             =   720
         Width           =   4560
         _ExtentX        =   8043
         _ExtentY        =   556
         LabelWidth      =   15
         TextMinWidth    =   1200
         Caption         =   ""
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin VSPDF8LibCtl.VSPDF8 VSPDF81 
         Left            =   9120
         Top             =   960
         Author          =   ""
         Creator         =   ""
         Title           =   ""
         Subject         =   ""
         Keywords        =   ""
         Compress        =   3
      End
      Begin VB.Label Lbl_TipoConsolidado 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cliente"
         Height          =   330
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   1170
      End
      Begin VB.Label lbl_moneda 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Moneda"
         Height          =   345
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   1185
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Empresa"
         Height          =   330
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   1170
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   17
      Top             =   0
      Width           =   9810
      _ExtentX        =   17304
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   18
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
      Height          =   2625
      Left            =   120
      TabIndex        =   20
      Top             =   3360
      Width           =   9585
      _cx             =   16907
      _cy             =   4630
      Appearance      =   2
      BorderStyle     =   1
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MousePointer    =   0
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      BackColorFixed  =   -2147483633
      ForeColorFixed  =   -2147483630
      BackColorSel    =   65535
      ForeColorSel    =   0
      BackColorBkg    =   -2147483643
      BackColorAlternate=   -2147483643
      GridColor       =   -2147483633
      GridColorFixed  =   -2147483632
      TreeColor       =   -2147483632
      FloodColor      =   192
      SheetBorder     =   -2147483642
      FocusRect       =   2
      HighLight       =   1
      AllowSelection  =   -1  'True
      AllowBigSelection=   -1  'True
      AllowUserResizing=   1
      SelectionMode   =   3
      GridLines       =   10
      GridLinesFixed  =   2
      GridLineWidth   =   1
      Rows            =   2
      Cols            =   7
      FixedRows       =   1
      FixedCols       =   0
      RowHeightMin    =   0
      RowHeightMax    =   0
      ColWidthMin     =   0
      ColWidthMax     =   0
      ExtendLastCol   =   -1  'True
      FormatString    =   $"Frm_Reporte_Cartola_Security_Consolidado.frx":0508
      ScrollTrack     =   -1  'True
      ScrollBars      =   3
      ScrollTips      =   -1  'True
      MergeCells      =   0
      MergeCompare    =   0
      AutoResize      =   -1  'True
      AutoSizeMode    =   0
      AutoSearch      =   2
      AutoSearchDelay =   2
      MultiTotals     =   -1  'True
      SubtotalPosition=   1
      OutlineBar      =   0
      OutlineCol      =   0
      Ellipsis        =   1
      ExplorerBar     =   3
      PicturesOver    =   0   'False
      FillStyle       =   0
      RightToLeft     =   0   'False
      PictureType     =   0
      TabBehavior     =   0
      OwnerDraw       =   0
      Editable        =   2
      ShowComboButton =   1
      WordWrap        =   0   'False
      TextStyle       =   0
      TextStyleFixed  =   0
      OleDragMode     =   0
      OleDropMode     =   0
      ComboSearch     =   3
      AutoSizeMouse   =   -1  'True
      FrozenRows      =   0
      FrozenCols      =   0
      AllowUserFreezing=   0
      BackColorFrozen =   0
      ForeColorFrozen =   0
      WallPaperAlignment=   9
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   24
   End
   Begin MSComctlLib.Toolbar Toolbar_Verificacion 
      Height          =   330
      Left            =   8040
      TabIndex        =   21
      Top             =   6120
      Width           =   1650
      _ExtentX        =   2910
      _ExtentY        =   582
      ButtonWidth     =   2858
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   1
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Generar Reporte"
            Key             =   "REPORTE"
            Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
            Object.ToolTipText     =   "Genera el Reporte de Instruccones"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Frm_Reporte_Cartola_Security_Consolidado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFecha              As Date
Dim lForm               As Frm_Reporte_Cartola
Dim FilaAux             As Variant
Dim iFilaTermino        As Variant
Dim lId_Cuenta          As String
Dim lFormatoMonedaCta   As String

'Dim lNum_Cuenta         As String
'Dim lDsc_Cuenta         As String
'Dim lNom_Cliente        As String
'Dim lDsc_Moneda         As String
Dim lId_Moneda_Cta      As String
Dim nDecimales          As Integer
Dim Moneda_Cuentas      As String
Dim fId_Cliente         As Integer
Dim fId_Grupo As Integer

Dim nPage               As Integer
Dim oCliente            As New Class_Cartola
Dim bImprimioHeader     As Boolean
'----------------------------------------------
Dim iLineasImpresas     As Integer

Dim sTextoIndicadores As String
Dim stextovalores  As String
Dim sTextoValoresAnt As String
Dim sTextoDosPuntos As String
Dim nInterlineado As Integer

'--------------------------------------------------------------------------
' Constantes
'--------------------------------------------------------------------------
Const RColorRelleno = 38     ' 213   ' R
Const GColorRelleno = 7      ' 202   ' G
Const BColorRelleno = 115    ' 195   ' B

Const RColorRelleno_TITULOS = 242  ' R
Const GColorRelleno_TITULOS = 242   ' G
Const BColorRelleno_TITULOS = 242   ' B

Const glb_tamletra_titulo = 8
Const glb_tamletra_subtitulo1 = 7.5
Const glb_tamletra_subtitulo2 = 7.5
Const glb_tamletra_registros = 7.3

Const Font_Name = "Times New Roman"
' Const Font_Name = "Gill Sans MT"

Const COLUMNA_INICIO    As String = "10mm"
Const COLUMNA_DOS       As String = "140mm"
Const CONST_ANCHO_GRILLA As String = "255mm"

Const CONS_NUMERO_LINEAS As Integer = 38   ' Numero de Lineas

Const CONS_POR_PANTALLA As Integer = 0
Const CONS_POR_IMPRESORA As Integer = 1
Const CONS_POR_ARCHIVO As Integer = 2


Public Sub Mostrar(TipoConsolidado As String)
    oCliente.TipoConsolidado = TipoConsolidado
    Call Form_Resize
  
  Load Me
End Sub

Private Sub Btn_Buscar_Click()

    fId_Cliente = NVL(Frm_Busca_Cuentas.BuscarCliente(), 0)
    
    If fId_Cliente <> 0 Then
        oCliente.IdCliente = fId_Cliente
        Call DatosDelCliente
        Txt_NombreCliente.Text = oCliente.Nombre
        Sub_Carga_Grilla
    End If
End Sub

Private Sub Cmb_BuscaFile_Click()
    On Error GoTo Cmd_BuscarArchivo_Err
    
    Dim x As Integer
    'Cheap way to use the common dialog box as a directory-picker
    x = 3

    Cmd_AbreArchivo.CancelError = True    'Do not terminate on error

    On Error Resume Next         'I will hande errors

    Cmd_AbreArchivo.Action = 1            'Present "open" dialog

    'If FileTitle is null, user did not override the default (*.*)
    If Cmd_AbreArchivo.FileTitle <> "" Then x = Len(Cmd_AbreArchivo.FileTitle)

    If Err = 0 Then
        ChDrive Cmd_AbreArchivo.Filename
        Txt_Directorio.Text = Left(Cmd_AbreArchivo.Filename, Len(Cmd_AbreArchivo.Filename) - x)
    Else
        'User pressed "Cancel"
    End If
  Exit Sub

Cmd_BuscarArchivo_Err:
  If Err = 32755 Then 'usuario presiona el boton Cancelar del Command Dialog
    Exit Sub
  End If
End Sub


Private Sub Cmb_GruposCuentas_SelChange(Cancel As Integer)
    fId_Grupo = IIf(Fnt_ComboSelected_KEY(Cmb_GruposCuentas) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_GruposCuentas))
    oCliente.IdGrupo = fId_Grupo
    Sub_Carga_Grilla
End Sub

Private Sub Form_Load()
    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("EXIT").Image = cBoton_Salir
            .Buttons("REFRESH").Image = cBoton_Refrescar
    End With
  
    With Toolbar_Verificacion
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("REPORTE").Image = cBoton_Modificar
    End With
  
    Call Sub_CargaForm
  
    Me.Top = 1
    Me.Left = 1
  
    With Cmd_AbreArchivo
        .Flags = cdlOFNPathMustExist
        .Flags = .Flags Or cdlOFNHideReadOnly
        .Flags = .Flags Or cdlOFNNoChangeDir
        .Flags = .Flags Or cdlOFNExplorer
        .Flags = .Flags Or cdlOFNNoValidate
        .Filename = "*.*"
    End With
    
    Txt_Directorio.Enabled = False
    Cmb_BuscaFile.Enabled = False
  
End Sub

Private Sub Sub_CargaForm()
    Dim lCierre As Class_Verificaciones_Cierre
    
    oCliente.IdCuenta = ""
    
    
    Set lCierre = New Class_Verificaciones_Cierre
  
    fFecha = lCierre.Busca_Ultima_FechaCierre
    DTP_Fecha_Ter.Value = fFecha

    Call Sub_Limpiar
    
    Call ActivaControlIngreso
    If oCliente.TipoConsolidado = CONSOLIDADO_POR_GRUPO Then
        Call Sub_CargaCombo_Grupos_Cuentas(Cmb_GruposCuentas, pBlanco:=True)
    End If
       
    Call Sub_FormControl_Color(Me.Controls)
    
    Call Sub_CargaCombo_Empresa(Cmb_Empresa)
    Call Sub_CargaCombo_Monedas(Cmb_Moneda)
    
End Sub
Private Sub ActivaControlIngreso()
    Select Case oCliente.TipoConsolidado
        Case CONSOLIDADO_POR_CLIENTE
            Cmb_GruposCuentas.Visible = False
            Cmb_GruposCuentas.Enabled = False
            Txt_NombreCliente.Visible = True
            Txt_NombreCliente.Enabled = True
            Btn_Buscar.Visible = True
            Btn_Buscar.Visible = True
            Lbl_TipoConsolidado.Caption = "Cliente"
        Case CONSOLIDADO_POR_GRUPO
            Txt_NombreCliente.Visible = False
            Txt_NombreCliente.Enabled = False
            Btn_Buscar.Visible = False
            Btn_Buscar.Visible = False
            Lbl_TipoConsolidado.Caption = "Grupo Cuenta"
            Cmb_GruposCuentas.Visible = True
            Cmb_GruposCuentas.Enabled = True
            
    End Select
End Sub
Private Sub Form_Resize()
    Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub


Private Sub optOpciones_Click(Index As Integer)
    Dim i As Long
    
    If Index = CONS_POR_ARCHIVO Then
        If optOpciones(Index).Value Then
            Txt_Directorio.Enabled = True
            Cmb_BuscaFile.Enabled = True
        Else
            Txt_Directorio.Enabled = False
            Cmb_BuscaFile.Enabled = False
        End If
    End If
    
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents

    Select Case Button.Key
        Case "REFRESH"
            Call Sub_Limpiar
        Case "EXIT"
            Unload Me
    End Select
End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents
    Dim i As Integer
    Dim flag As Boolean
    flag = False
    nInterlineado = 110
    Select Case Button.Key
        Case "REPORTE"
            If optOpciones(CONS_POR_ARCHIVO).Value And Txt_Directorio.Text = "" Then
                MsgBox "Debe Ingresar la Carpeta de Destino para los PDF", vbExclamation, Me.Caption
                Exit Sub
            End If
            If Not GeneraCartola(DTP_Fecha_Ter.Value) Then
                MsgBox "Error en la Generacion", vbInformation
            End If
            flag = True
            Screen.MousePointer = vbDefault
            If Not optOpciones(CONS_POR_PANTALLA).Value And flag Then
                MsgBox "Generaci�n Terminada", vbExclamation, Me.Caption
            End If
    End Select
End Sub

Private Sub Sub_Limpiar()
    Select Case oCliente.TipoConsolidado
        Case CONSOLIDADO_POR_CLIENTE
            Txt_NombreCliente.Text = ""
        Case CONSOLIDADO_POR_GRUPO
            Call Sub_ComboSelectedItem(Cmb_GruposCuentas, cCmbKALL)
    End Select
    DTP_Fecha_Ter.Value = fFecha
    Call Sub_ComboSelectedItem(Cmb_Moneda, cCmbKALL)
End Sub

Sub FormatoHoja()
    With vp
        .PaperSize = pprLetter
        .MarginLeft = COLUMNA_INICIO   ' "15mm"
        .MarginRight = "15mm"
        .MarginTop = "45mm"
        .MarginBottom = "25mm"
        .MarginFooter = "15mm"
        .MarginHeader = "0mm"
        
        .Font.Name = "Microsoft Sans Serif"
        .Font.Size = glb_tamletra_registros - 1
        
        .LineSpacing = 110
        
        .Orientation = orLandscape
    End With
    
    sTextoIndicadores = ""
    stextovalores = ""
    sTextoValoresAnt = ""
    sTextoDosPuntos = ""
    
End Sub

Public Sub pon_titulo(pTitulo As String, pAlto As Variant, pfont_name As String, pfont_size As Double)
    Dim ant_font_name   As String
    Dim ant_font_size   As Double
    Dim pY              As Variant
    Dim pX              As Variant


    With vp
        'lee valores anteriores
        ant_font_name = .FontName
        ant_font_size = .FontSize

        'pon valores nuevos
        ' .FontName = "Gill Sans MT"   ' pfont_name
        .FontSize = pfont_size

        ' pon titulo
        pY = .CurrentY
        pX = .CurrentX

        Call haz_linea(pY, True)

        .CalcParagraph = pTitulo
        .Paragraph = " " & pTitulo
        
        Call haz_linea(.CurrentY, True)

        'recupera valores anteriores
        .FontName = ant_font_name
        .FontSize = ant_font_size

        '.CurrentX = pX
        '.CurrentY = pY

    End With
End Sub

Function GeneraCartola(Fecha_Cartola As Date) As Boolean
    Dim i As Integer
    Dim hay_cierre As Boolean
    Dim lId_Cliente As Integer
    Dim lId_Moneda_Salida As Integer
    Dim lid_Empresa As Integer
    Dim lId_Grupo As Integer

    nPage = 0
    
    lId_Moneda_Salida = IIf(Fnt_ComboSelected_KEY(Cmb_Moneda) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Moneda))
    lid_Empresa = IIf(Fnt_ComboSelected_KEY(Cmb_Empresa) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Empresa))
    
    Select Case oCliente.TipoConsolidado
        Case CONSOLIDADO_POR_CLIENTE
            If Txt_NombreCliente.Text = "" Then
                MsgBox "Debe seleccionar Cliente", vbInformation
                GeneraCartola = False
                Exit Function
            End If
        Case CONSOLIDADO_POR_GRUPO
            lId_Grupo = IIf(Fnt_ComboSelected_KEY(Cmb_GruposCuentas) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_GruposCuentas))
            If Cmb_GruposCuentas.Text = "" Then
                MsgBox "Debe seleccionar Grupo de Cuenta", vbInformation
                GeneraCartola = False
                Exit Function
            End If
    End Select
    'oCliente.Fecha_Cartola = DTP_Fecha_Ter.Value
    
    oCliente.IdCliente = fId_Cliente
    oCliente.IdGrupo = lId_Grupo
    oCliente.Fecha_Cartola = CDate(Fecha_Cartola)
    oCliente.IdMonedaSalida = lId_Moneda_Salida
    oCliente.IDEmpresa = lid_Empresa

    'Call Sub_Carga_Grilla
    
    GeneraCartola = True
    hay_cierre = GeneraCartola
    
'    If oCliente.TipoConsolidado = CONSOLIDADO_POR_CLIENTE Then
'        Call DatosDelCliente
'    End If
    
    FormatoHoja
    vp.StartDoc
    Call PaginaUno(hay_cierre):     DoEvents
    Call PaginaDos:                 DoEvents
    Call PaginaTres:                DoEvents
    
    
    vp.EndDoc
'********************************************
    Dim sFooter As String
    
    Dim vColorAnt As Variant
    sFooter = ""
    vp.FontName = "Times New Roman"
    vp.FontSize = 5
    vColorAnt = vp.PenColor
   
    For i = 1 To nPage
        vp.StartOverlay i
        
        haz_linea ("200mm")
        
        vp.CurrentX = vp.MarginLeft
        vp.CurrentY = rTwips("201mm")
        
        sFooter = "Apoquindo 3150 Piso 7"
        
        vp.TextAlign = taCenterMiddle
        
        vp.TextColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        vp.Paragraph = sFooter & vbCrLf & "Pagina " & i & "/" & nPage
        
        
        vp.EndOverlay
    Next
    
    If optOpciones(CONS_POR_IMPRESORA).Value Then
        vp.PrintDoc
    ElseIf optOpciones(CONS_POR_ARCHIVO).Value Then
        Dim Carpeta As String
        Dim Directorio As String
        
        If Txt_Directorio.Text <> "" Then
            Carpeta = MonthName(Month(oCliente.Fecha_Cartola)) & Year(oCliente.Fecha_Cartola)
            
            On Error Resume Next
            MkDir Txt_Directorio.Text & Carpeta
            
            Directorio = Txt_Directorio.Text & Carpeta
            
            oCliente.NombreArchivo = Trim(oCliente.Nombre) & "_" & Trim(oCliente.Num_Cuenta) & "_" & Day(oCliente.Fecha_Cartola) & Carpeta & ".pdf"
            VSPDF81.ConvertDocument vp, Directorio & "\" & oCliente.NombreArchivo
        End If
        Set lForm = Nothing
        
    Else
        Set lForm = New Frm_Reporte_Cartola
        Dim PathAndFile As String
        
        PathAndFile = oCliente.CreateTempFile("car")
        
        vp.SaveDoc PathAndFile
        
        lForm.vp.LoadDoc PathAndFile
    End If
    
    vp.KillDoc
    
    Screen.MousePointer = vbDefault

End Function

Sub PaginaDos()
    Call ActivosClaseInstrumento
End Sub

Sub PaginaTres()
    vp.NewPage

    Call Cajas
End Sub

Function Pri_Dia_Mes()

End Function

Sub Cajas()
    Dim lCursor_Caj         As hRecord
    Dim Cursor_Mov_Caja     As hRecord
    Dim lreg_1              As hFields
    Dim lReg_Caj            As hFields
    Dim ultimosaldo         As Double
    Dim anterior            As Double
    
    Dim i                   As Integer
    '--------------------------------------------------
    Dim lCajas_Ctas         As Class_Cajas_Cuenta
    '-------------------------------------------------
    Dim nDecimalesCaja      As Integer
    '--------------------------------------------------
    Dim bImprimio         As Boolean
    Dim sCajaActual         As String
    '--------------------------------------------------
    Dim nTotalIngreso   As Double
    Dim nTotalEgreso    As Double
    Dim nSaldo          As Double
        
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
    If oCliente.TipoConsolidado = CONSOLIDADO_POR_CLIENTE Then
        gDB.Parametros.Add "Pid_cliente", ePT_Numero, oCliente.IdCliente, ePD_Entrada
    Else
        gDB.Parametros.Add "Pid_grupo", ePT_Numero, oCliente.IdGrupo, ePD_Entrada
    End If
    gDB.Parametros.Add "Pfecha_cierre", ePT_Numero, DTP_Fecha_Ter.Value, ePD_Entrada
    gDB.Parametros.Add "Pid_moneda_salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
    gDB.Parametros.Add "PConsolidado", ePT_Caracter, oCliente.TipoConsolidado, ePD_Entrada
    gDB.Procedimiento = "PKG_CARTOLA$Cajas_Consolidado"
    If gDB.EjecutaSP Then
        Set lCursor_Caj = gDB.Parametros("Pcursor").Valor
    Else
       Exit Sub
    End If
    
    bImprimioHeader = False
    
    With vp
        .CurrentY = .CurrentY + rTwips("2mm")
        
        .StartTable
            .TableBorder = tbBox
            .TableCell(tcCols) = 1
            .TableCell(tcRows) = 1
            .TableCell(tcFontSize, 1, 1, 1, 1) = 12
            .TableCell(tcFontSize, 1, 1) = glb_tamletra_subtitulo2
            .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
            .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
            .TableCell(tcText, 1, 1) = "INFORME DE CAJAS:"
            .TableCell(tcColWidth, 1, 1) = CONST_ANCHO_GRILLA
        .EndTable
        
        iLineasImpresas = 1
        
        For Each lReg_Caj In lCursor_Caj
            nDecimalesCaja = lReg_Caj("Decimales").Value
            
            .CurrentY = .CurrentY + rTwips("2mm")
            
            iLineasImpresas = iLineasImpresas + 1
'--------------------------------------------------------------------------------------------------------------------------
            gDB.Parametros.Clear
            gDB.Procedimiento = "Pkg_Saldos_Caja$Buscar_Saldos_Cuenta"
            gDB.Parametros.Add "PID_CAJA_CUENTA", ePT_Numero, lReg_Caj("id_caja_cuenta").Value, ePD_Entrada
            gDB.Parametros.Add "PID_CUENTA", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
            gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, CDate(UltimoDiaDelMes(oCliente.Fecha_Cartola)) + 1, ePD_Entrada
            gDB.Parametros.Add "PSaldo", ePT_Numero, ultimosaldo, ePD_Salida
            
            '.Parametros.Add "PID_CAJA_CUENTA", ePT_Fecha, request.form("idcaja"), ePD_Entrada
            
            If gDB.EjecutaSP Then
                ultimosaldo = gDB.Parametros("Psaldo").Valor
            End If
'--------------------------------------------------------------------------------------------------------------------------
            gDB.Parametros.Clear
            gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
            gDB.Parametros.Add "Pidcajacuenta", ePT_Numero, lReg_Caj("id_caja_cuenta").Value, ePD_Entrada
            gDB.Parametros.Add "Pfecha_inicio", ePT_Fecha, CDate(UltimoDiaDelMes(oCliente.Fecha_Cartola)) + 1, ePD_Entrada
            gDB.Parametros.Add "Pfecha_termino", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
            gDB.Procedimiento = "PKG_MOVIMIENTOS_CAJA_SECURITY"
            
            If Not gDB.EjecutaSP Then
                Exit Sub
            End If
            
            Set Cursor_Mov_Caja = gDB.Parametros("Pcursor").Valor
'----------------------------------------------------------------------------------------------------------------------------
            sCajaActual = NVL(lReg_Caj("dsc_caja_cuenta").Value, "Caja")
            Call EncabezadoCajas(sCajaActual, False)
            
            iLineasImpresas = iLineasImpresas + 1
            
            i = .TableCell(tcRows)
            
            .TableCell(tcText, i, 1) = ""
            .TableCell(tcText, i, 2) = UltimoDiaDelMes(oCliente.Fecha_Cartola)
            .TableCell(tcText, i, 3) = ""
            .TableCell(tcText, i, 4) = "SALDO ANTERIOR"
            .TableCell(tcText, i, 5) = ""
            .TableCell(tcText, i, 6) = ""
            .TableCell(tcText, i, 7) = FormatNumber(ultimosaldo, nDecimalesCaja)
            
            .TableCell(tcAlign, i, 7) = taRightMiddle
            .TableCell(tcFontSize, i, 1, i, 7) = glb_tamletra_registros
            
            iLineasImpresas = iLineasImpresas + 1
            
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            
            bImprimio = False
            nTotalIngreso = 0
            nTotalEgreso = 0
            nSaldo = 0
            
            For Each lreg_1 In Cursor_Mov_Caja
                i = .TableCell(tcRows)
                bImprimio = True
'------------------------------------------------------------------------------------------------------------
                If (iLineasImpresas Mod CONS_NUMERO_LINEAS) = 0 Then
                    .EndTable
                    
                    .NewPage
                    .StartTable
                    
                    Call EncabezadoCajas(sCajaActual, True)
                    
                    iLineasImpresas = iLineasImpresas + 1
                    i = .TableCell(tcRows)
                    
                End If
'------------------------------------------------------------------------------------------------------------
                If lreg_1("id_operacion").Value <> 0 Then
                    .TableCell(tcText, i, 1) = lreg_1("id_operacion").Value
                Else
                    .TableCell(tcText, i, 1) = "-"
                End If
                
                .TableCell(tcText, i, 2) = lreg_1("fecha_liquidacion").Value
                
                .TableCell(tcText, i, 3) = ""
                .TableCell(tcText, i, 4) = Trim(lreg_1("descripcion").Value)
                .TableCell(tcText, i, 5) = FormatNumber(lreg_1("monto_movto_abono").Value, nDecimalesCaja)
                .TableCell(tcAlign, i, 5) = taRightMiddle
                
                .TableCell(tcText, i, 6) = FormatNumber(lreg_1("monto_movto_cargo").Value, nDecimalesCaja)
                .TableCell(tcAlign, i, 6) = taRightMiddle
                
                If anterior <> lreg_1("id_operacion").Value Or anterior = 0 Then
                    ultimosaldo = ultimosaldo + lreg_1("monto_movto_abono").Value - lreg_1("monto_movto_cargo").Value
                End If
                
                .TableCell(tcFontSize, i, 1, i, 7) = glb_tamletra_registros
                .TableCell(tcText, i, 7) = FormatNumber(ultimosaldo, nDecimalesCaja)
                .TableCell(tcAlign, i, 7) = taRightMiddle
                
                nTotalIngreso = nTotalIngreso + lreg_1("monto_movto_abono").Value
                nTotalEgreso = nTotalEgreso + lreg_1("monto_movto_cargo").Value
                nSaldo = nSaldo + ultimosaldo
                 
                
                anterior = lreg_1("id_operacion").Value
                
                iLineasImpresas = iLineasImpresas + 1
                .TableCell(tcRows) = .TableCell(tcRows) + 1
                
            Next ' END OF {For Each lreg_1 In Cursor_Mov_Caja}
            '***********************************************************************
            ' Totales
            '***********************************************************************
            If bImprimio Then
                bImprimio = False
                i = .TableCell(tcRows)
                
                .TableCell(tcBackColor, i) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                .TableCell(tcForeColor, i) = RGB(255, 255, 255)
                .TableCell(tcText, i, 1) = ""
                .TableCell(tcText, i, 2) = ""
                .TableCell(tcText, i, 3) = ""
                .TableCell(tcText, i, 4) = "TOTALES": .TableCell(tcAlign, i, 4) = taRightMiddle
                .TableCell(tcText, i, 5) = FormatNumber(nTotalIngreso, nDecimalesCaja): .TableCell(tcAlign, i, 5) = taRightMiddle
                .TableCell(tcText, i, 6) = FormatNumber(nTotalEgreso, nDecimalesCaja): .TableCell(tcAlign, i, 6) = taRightMiddle
                .TableCell(tcText, i, 7) = FormatNumber(ultimosaldo, nDecimalesCaja): .TableCell(tcAlign, i, 7) = taRightMiddle
                
                .TableCell(tcFontSize, i, 1, i, 7) = glb_tamletra_registros
            End If
            '***********************************************************************
            'Fin totales
            '***********************************************************************
            .EndTable
            
            Set Cursor_Mov_Caja = Nothing
            Set lreg_1 = Nothing
            
        Next  ' END OF {For Each lReg_Caj In lCursor_Caj}
        
        Set lReg_Caj = Nothing
        Set lCursor_Caj = Nothing
        
    End With
    
End Sub

Private Sub EncabezadoCajas(Optional dsc_caja_cuenta As String = "", Optional bContinuacion As Boolean = False)
    Dim iFila As Integer
    
    With vp
        .StartTable
        
        .TableCell(tcCols) = 7
        .TableCell(tcRows) = 1
        
        If dsc_caja_cuenta <> "" Then
            
            .TableBorder = tbBox
            
            .TableCell(tcColSpan, 1, 1) = 7
            
            .TableCell(tcFontSize, 1, 1, 1, 7) = glb_tamletra_titulo
            .TableCell(tcText, 1, 1) = dsc_caja_cuenta & IIf(bContinuacion, " (Continuacion ...)", "")
            
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iLineasImpresas = iLineasImpresas + 1
            
        End If
        
        .TableBorder = tbBoxRows
        iFila = .TableCell(tcRows)
        
        .TableCell(tcRowHeight, iFila) = 250
        
        .TableCell(tcText, iFila, 1) = "N� OPERACION"
        .TableCell(tcColWidth, iFila, 1) = 1350
        .TableCell(tcBackColor, iFila, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 2) = "FECHA LIQ."
        .TableCell(tcColWidth, iFila, 2) = 1305
        .TableCell(tcBackColor, iFila, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 3) = ""
        .TableCell(tcColWidth, iFila, 3) = 0
        .TableCell(tcBackColor, iFila, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 4) = "DETALLE"
        .TableCell(tcColWidth, iFila, 4) = 6300
        .TableCell(tcBackColor, iFila, 4) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 5) = "INGRESO"
        .TableCell(tcAlign, iFila, 5) = taRightMiddle
        .TableCell(tcColWidth, iFila, 5) = 1800
        .TableCell(tcBackColor, iFila, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 6) = "EGRESO"
        .TableCell(tcAlign, iFila, 6) = taRightMiddle
        .TableCell(tcColWidth, iFila, 6) = 1800
        .TableCell(tcBackColor, iFila, 6) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 7) = "SALDO"
        .TableCell(tcAlign, iFila, 7) = taRightMiddle
        .TableCell(tcColWidth, iFila, 7) = 1890
        .TableCell(tcBackColor, iFila, 7) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)

        .TableCell(tcFontSize, iFila, 1, iFila, 7) = glb_tamletra_titulo
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)
                
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        
    End With
    
End Sub
        
Sub AsignaTitulos(ByRef oColumnas As Class_Cartola_Campos, sProducto As String)
    oColumnas.CleanDetalle
    
    Select Case sProducto
        Case gcPROD_RV_NAC
            'MENU DE ACCIONES'
            With oColumnas
                .AddColumna "nemotecnico", "NEMOT�CNICO", taLeftMiddle, False, , , 61
                .AddColumna "DSC_MONEDA", "MONEDA", taCenterMiddle, False, , , 40
                .AddColumna "Porcentaje_inversion", "% " & vbCrLf & "DE INVERSI�N", taRightMiddle, True, , 2, 23, True
                .AddColumna "Nominal", "CANTIDAD", taRightMiddle, True, , 0, 26
                .AddColumna "precio_promedio_compra", "PRECIO PROMEDIO " & vbCrLf & "COMPRA", taRightMiddle, True, , 2, 25
                .AddColumna "precio", "PRECIO ACTUAL", taRightMiddle, True, , 2, 25
                '.AddColumna "valoractual", "VALOR ACTUAL", taRightMiddle, True, , 2, 30
                
                .AddColumna "Valorizacion_Mercado", "VALORIZACI�N" & vbCrLf & "MERCADO", taRightMiddle, True, "", oCliente.Decimales, 30, True
                .AddColumna "rentabilidad", "% RENT.", taRightMiddle, True, , 2, 30
                
            End With
        Case gcPROD_RF_NAC
            'MENU DE RENTA FIJA
            With oColumnas
                .AddColumna "nemotecnico", "NEMOT�CNICO", taLeftMiddle, False, , , 25
                .AddColumna "DSC_EMISOR_ESPECIFICO", "EMISOR", taLeftMiddle, False, , , 15
                .AddColumna "Porcentaje_inversion", "% DE" & vbCrLf & "INVERSI�N", taRightMiddle, False, , 2, 20, True
                .AddColumna "DSC_MONEDA", "MONEDA", taCenterMiddle, True, "", 2, 16
                .AddColumna "Nominal", "VALOR" & vbCrLf & "NOMINAL", taRightMiddle, True, "", 0, 18
                .AddColumna "tasa_cupon", "TASA" & vbCrLf & "CUP�N", taRightMiddle, True, "", 2, 13
                .AddColumna "Fecha_Vencimiento", "FECHA" & vbCrLf & "VCTO.", taCenterMiddle, False, , 0, 20
                .AddColumna "Dias", "DIAS", taRightMiddle, True, , 0, 15
                .AddColumna "Duration", "DURATION", taRightMiddle, True, , 2, 20
                .AddColumna "TASA_COMPRA", "TASA" & vbCrLf & "COMPRA", taRightMiddle, True, , 2, 15
                .AddColumna "precio_Fecha_compra", "PRECIO" & vbCrLf & "COMPRA", taRightMiddle, True, , 2, 20
                .AddColumna "tasa", "TASA" & vbCrLf & "MERCADO", taRightMiddle, True, , 2, 17
                .AddColumna "precio", "PRECIO" & vbCrLf & "MERCADO", taRightMiddle, True, , 2, 20
                .AddColumna "Valorizacion_Mercado", "VALORIZACI�N", taRightMiddle, True, , 2, 26, True
                
            End With
        Case gcPROD_FFMM_NAC
            'MENU DE FONDOS MUTUOS
            With oColumnas
                .AddColumna "DSC_nemotecnico", "NEMOT�CNICO", taLeftMiddle, False, , 0, 75
                .AddColumna "DSC_MONEDA", "MONEDA", taCenterMiddle, False, , 0, 44
                .AddColumna "Porcentaje_inversion", "%" & vbCrLf & "DE INVERSI�N", taRightMiddle, True, , 2, 25, True
                .AddColumna "Nominal", "N�MERO" & vbCrLf & "CUOTAS", taRightMiddle, True, "#,###,##0", 2, 26
                .AddColumna "precio_promedio_compra", "VALOR CUOTA" & vbCrLf & "PROMEDIO COMPRA", taRightMiddle, True, , 2, 30
                .AddColumna "precio", "VALOR CUOTA" & vbCrLf & "ACTUAL", taRightMiddle, True, "#,###,##0.00", 2, 30
                .AddColumna "Valorizacion_Mercado", "VALOR MERCADO", taRightMiddle, True, "#,###,##0.00", 2, 30, True
                
            End With
    End Select

End Sub
        

Sub DatosDelCliente()
    Dim lCursorCuentas
    Dim lReg
    Dim CursorDireccion
    Dim Reg_1
    Dim Reg
    Dim Cursor_1
    Dim Cursor

    Dim IDMoneda As Integer

    Dim Fecha_Servidor As Date

    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cliente", ePT_Numero, oCliente.IdCliente, ePD_Entrada
    gDB.Procedimiento = "PKG_CUENTAS$Buscar"

    If Not gDB.EjecutaSP Then
        GoTo Fin
    End If

    Set lCursorCuentas = gDB.Parametros("Pcursor").Valor
    gDB.Parametros.Clear

'#################### Direcciones Clientes ####################################
     For Each lReg In lCursorCuentas
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Pid_cliente", ePT_Numero, oCliente.IdCliente, ePD_Entrada
        gDB.Procedimiento = "PKG_DIRECCIONES_CLIENTES$Buscar"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set CursorDireccion = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each Reg_1 In CursorDireccion
            oCliente.Direccion = Trim(NVL(Reg_1("direccion").Value, ""))
            oCliente.Comuna = NVL(Reg_1("DSC_COMUNA_CIUDAD").Value, "")
            oCliente.Ciudad = NVL(Reg_1("DSC_REGION").Value, "")
            oCliente.Telefono = NVL(Reg_1("fono").Value, "")
            
            If oCliente.Direccion = "" Then
                oCliente.Direccion = "Sin Direcci�n"
            End If

        Next
'#################### FIN Direcciones Clientes #################################

'#################### Moneda  ####################################
        IDMoneda = oCliente.IdMonedaSalida

        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Pid_Moneda", ePT_Numero, IDMoneda, ePD_Entrada
        gDB.Procedimiento = "PKG_MONEDAS$Buscar"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set Cursor_1 = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each Reg_1 In Cursor_1
            oCliente.Moneda = Reg_1("dsc_moneda").Value
            oCliente.Decimales = Reg_1("dicimales_mostrar").Value
        Next
'#################### FIN Moneda #################################

'PKG_CIERRES_CUENTAS$BUSCAR_ultimo
'#################### Moneda  ####################################
        Fecha_Servidor = Fnt_FechaServidor()
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
        gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Servidor), ePD_Entrada
        gDB.Procedimiento = "PKG_CIERRES_CUENTAS$BUSCAR_ultimo"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set Cursor = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each Reg In Cursor
            'Fecha_cierre = Reg("fecha_cierre").value
            oCliente.Fecha_Cierre = CDate(Reg("fecha_cierre").Value)
        Next

'#################### FIN Moneda #################################
        oCliente.Nombre = lReg("nombre_cliente").Value
        
        oCliente.Rut = formato_rut(lReg("rut_cliente").Value)
        
        oCliente.Num_Cuenta = lReg("num_cuenta").Value
        oCliente.Perfil_Riesgo = lReg("dsc_perfil_riesgo").Value
        oCliente.Ejecutivo = NVL(lReg("dsc_asesor").Value, "Sin Asesor")
        oCliente.Fecha_Creacion = lReg("fecha_operativa").Value
        
        oCliente.MailEjecutivo = NVL(lReg("email").Value, "")        ' mail del Asesor.
        oCliente.FonoEjecutivo = NVL(lReg("fono").Value, "")     ' Fono del Asesor.
        
        oCliente.IDEmpresa = lReg("id_empresa").Value
        
        oCliente.NombreArchivo = ""
    Next
    
Fin:

    Set lCursorCuentas = Nothing
    Set lReg = Nothing
    Set CursorDireccion = Nothing
    Set Reg_1 = Nothing
    Set Reg = Nothing
    Set Cursor_1 = Nothing
    Set Cursor = Nothing

End Sub

Private Sub vp_NewPage()
    nPage = nPage + 1
    
    Call pon_header
    
    bImprimioHeader = True
    
    'Call Indicadores
End Sub

Public Sub pon_header()
    Dim xAnt As Variant
    Dim yAnt As Variant
    Dim xtalign As Variant
    Dim xbrushcolor As Variant
    Dim xpencolor As Variant
    Dim xtextcolor As Variant
    Dim xfontsize As Variant
    Dim xmarginleft As Variant
    Dim xAjusteAnt As Variant
    Dim sColorTextoAnt      As Long
    '-------------------------------------------------------------------
    Dim xValTop As Variant
    Dim sRutaImagen As String
    '-------------------------------------------------------------------
    Dim Xinicio As Variant
    Dim YInicio As Variant
    '-------------------------------------------------------------------
    Dim lreg_vtc            As hCollection.hFields
    Dim lValor_Tipo_Cambio  As New Class_Valor_Tipo_Cambio
    '-------------------------------------------------------------------
    
    vp.FontBold = False
    
    vp.MarginLeft = COLUMNA_INICIO
    
    sRutaImagen = App.Path & "\security.jpg"
    
    xValTop = ""
    
    'Inserta Logo
    On Error Resume Next
    vp.DrawPicture LoadPicture(sRutaImagen), "200mm", "7mm", "65mm", "10mm"
    
    On Error GoTo 0
    
    'Guarda valores anteriores
    xAnt = vp.CurrentX
    yAnt = vp.CurrentY
    xtalign = vp.TextAlign
    xbrushcolor = vp.BrushColor
    xpencolor = vp.PenColor
    xtextcolor = vp.TextColor
    xfontsize = vp.FontSize
    xmarginleft = vp.MarginLeft
    sColorTextoAnt = vp.TextColor
    
    vp.CurrentX = vp.MarginLeft
    vp.CurrentY = "20mm"
        
'Asigna nuevos valores
    vp.TextAlign = taCenterTop
    vp.BrushColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = RGB(255, 255, 255) ' RGB(100, 100, 100)
    
    vp.FontSize = 14
    'Llena texto
    vp.TextBox "BALANCE DE INVERSI�N", vp.MarginLeft, vp.CurrentY, "122.7mm", "7mm", True, False, True
    'Asigna nuevos valores
    vp.TextAlign = taLeftTop
    vp.BrushColor = RGB(242, 242, 242)
    'vp.PenColor = RGB(232, 226, 222)
    vp.TextColor = RGB(0, 62, 134)    ' sColorTextoAnt  ' &H0&
    vp.FontSize = 8
    
    'Llena texto
    Xinicio = vp.MarginLeft
    YInicio = vp.CurrentY
    
    Call vp.DrawRectangle(Xinicio, YInicio, Xinicio + rTwips("123mm"), YInicio + rTwips("24mm"))

    ' vp.MarginLeft = "79mm"
    vp.MarginLeft = Xinicio + rTwips("3mm")
    vp.CurrentX = Xinicio + rTwips("3mm")
    vp.CurrentY = YInicio + rTwips("3mm")
    
    Xinicio = vp.CurrentX
    
    vp.Text = UCase("Se�ores" & vbCrLf & _
              oCliente.Nombre & vbCrLf & _
              "RUT N�: " & oCliente.Rut & vbCrLf & _
              oCliente.Direccion & vbCrLf & _
              oCliente.Comuna & " " & oCliente.Ciudad)
              
' ####################################################  Cuadro Dos
    vp.MarginLeft = COLUMNA_DOS ' + rTwips("150mm")
    Xinicio = vp.MarginLeft
    YInicio = "20mm"
                
    'Asigna nuevos valores
    vp.TextAlign = taCenterTop
    vp.BrushColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    'vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = RGB(0, 62, 134)     ' RGB(100, 100, 100)
    
    vp.FontSize = 14
    'Llena texto
    vp.TextBox " ", Xinicio, YInicio, "124.7mm", "7mm", True, False, True
    
    'Asigna nuevos valores
    vp.TextAlign = taLeftTop
    vp.BrushColor = RGB(242, 242, 242)   ' RGB(232, 226, 222)
    'vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = RGB(0, 62, 134)
    vp.FontSize = 8
    
    'Llena texto
     vp.MarginLeft = Xinicio ' + rTwips("3mm")
    Xinicio = vp.MarginLeft
    YInicio = vp.CurrentY
    
    vp.DrawRectangle Xinicio, _
                    YInicio, _
                    Xinicio + rTwips("125mm"), _
                    YInicio + rTwips("24mm")

    vp.MarginLeft = Xinicio + rTwips("3mm")
    vp.CurrentX = Xinicio + rTwips("3mm")
    vp.CurrentY = YInicio + rTwips("3mm")
    
    vp.Text = UCase("Informe al " & vbCrLf & _
                    " " & vbCrLf & _
                    "Asesor" & vbCrLf & _
                    "Email" & vbCrLf & _
                    "Telefono")
    
    
    vp.MarginLeft = Xinicio + rTwips("22mm")
    vp.CurrentX = Xinicio + rTwips("22mm")
    vp.CurrentY = YInicio + rTwips("3mm")
    
    vp.Text = UCase(": " & oCliente.Fecha_Cartola & vbCrLf & _
              "  " & vbCrLf & _
              ": " & oCliente.Ejecutivo & vbCrLf & _
              ": " & oCliente.MailEjecutivo & vbCrLf & _
              ": " & oCliente.FonoEjecutivo)
              
'********************************************************************************
' Indicadores
'********************************************************************************
'-----------------------------------------------------------------------------
    xAjusteAnt = vp.TextAlign
    
    If sTextoIndicadores = "" Then    ' Es la primera vez que pasa por ac�
    
        sTextoIndicadores = "Valores al"
        sTextoDosPuntos = ":"
        stextovalores = oCliente.Fecha_Cartola
        sTextoValoresAnt = UltimoDiaDelMes(oCliente.Fecha_Cartola)
        
        With lValor_Tipo_Cambio
        
            .Campo("FECHA").Valor = oCliente.Fecha_Cartola
            If .BuscarIndicadores Then
                For Each lreg_vtc In .Cursor
                    sTextoIndicadores = sTextoIndicadores & vbCrLf & Trim(lreg_vtc("DSC_MONEDA").Value)
                    sTextoDosPuntos = sTextoDosPuntos & vbCrLf & ":"
                    stextovalores = stextovalores & vbCrLf & FormatNumber(lreg_vtc("VALOR").Value, 2)
                Next
            End If
                    
            .Campo("FECHA").Valor = UltimoDiaDelMes(oCliente.Fecha_Cartola)
            If .BuscarIndicadores Then
                For Each lreg_vtc In .Cursor
                    sTextoValoresAnt = sTextoValoresAnt & vbCrLf & FormatNumber(lreg_vtc("VALOR").Value, 2)
                Next
            End If
            
        End With
        
        Set lValor_Tipo_Cambio = Nothing
    End If
    
    vp.MarginLeft = Xinicio + rTwips("66mm")
    vp.CurrentX = vp.MarginLeft
    vp.CurrentY = YInicio + rTwips("3mm")
    
    vp.StartTable
    
    vp.TableCell(tcRows) = 1
    vp.TableCell(tcCols) = 4
    vp.TableBorder = tbNone
    vp.TableCell(tcFontSize) = 7.5
    
    vp.TableCell(tcText, 1, 1) = UCase(sTextoIndicadores)
    vp.TableCell(tcText, 1, 2) = sTextoDosPuntos
    vp.TableCell(tcText, 1, 3) = UCase(stextovalores)
    vp.TableCell(tcText, 1, 4) = UCase(sTextoValoresAnt)
    
    vp.TableCell(tcColWidth, 1, 1) = rTwips("21mm")
    vp.TableCell(tcColWidth, 1, 2) = rTwips("4mm")
    vp.TableCell(tcColWidth, 1, 3) = rTwips("17mm")
    vp.TableCell(tcColWidth, 1, 4) = rTwips("17mm")
    
    vp.TableCell(tcColAlign, 1, 2) = taLeftMiddle
    vp.TableCell(tcColAlign, 1, 3, 1, 4) = taRightMiddle
    
    vp.EndTable
        
    
'----------------------------------------------------------------------------
    vp.MarginLeft = COLUMNA_INICIO
    vp.CurrentY = YInicio + rTwips("25mm")
    vp.CurrentX = vp.MarginLeft
    
    vp.TextAlign = xtalign
    vp.BrushColor = xbrushcolor
    vp.PenColor = xpencolor
    vp.TextAlign = xAjusteAnt
    vp.TextColor = sColorTextoAnt
    

End Sub

Private Function rTwips(pMM As Variant)
    Dim i As Integer, s As String, d As Double
    If VarType(pMM) <> vbString Then
        rTwips = pMM
    Else
        i = InStr(1, pMM, "mm")
        If i = 0 Then
            MsgBox "Error de conversion de mm a twips"
            End
        End If
        
        s = Mid$(pMM, 1, i - 1)
        
        s = Replace(s, ".", gstrSepDec)
        s = Replace(s, ",", gstrSepDec)
        
        d = CDbl(s)
        
        ' rTwips = ((((d * 254) / 1440) + 0.5) * 567) / 100 + 0.5
        rTwips = 56.52 * d
        
    End If
End Function

Private Function Twips2mm(cTwips As Variant)
    Twips2mm = (cTwips / 56.52)
End Function

Function formato_rut(rut_bruto)
    Dim rut_salida, dv
    Dim Rut
    Dim f
    Dim rut_salida3
    Dim rut_salida2
    Dim rut_salida1
    
    rut_salida = Trim(rut_bruto)
    dv = Right(rut_salida, 1)
    Rut = Left(rut_salida, Len(rut_salida) - 2)
    f = Len(Rut)
    
    If f > 6 Then
        rut_salida3 = Mid(Rut, f - 2, 3)
        f = f - 3
        rut_salida2 = Mid(Rut, f - 2, 3)
        f = f - 3
        If f < 3 Then
            rut_salida1 = Left(Rut, f)
        End If
        formato_rut = rut_salida1 & "." & rut_salida2 & "." & rut_salida3 & "-" & dv
    Else
        formato_rut = rut_bruto
    End If
End Function

Private Function DameInicioDeMes(sFecha As String) As String
    Dim sFechaTmp As String
    Dim sMes As String
    
    If Month(sFecha) < 10 Then
        sMes = "0" & Month(sFecha)
    Else
        sMes = Month(sFecha)
    End If
    
    sFechaTmp = "01/" & sMes & "/" & Year(sFecha)
    
    DameInicioDeMes = sFechaTmp
    
End Function

Private Function Obtener_Simbolo(Valor As Long) As String
   Dim Simbolo  As String
   Dim r1       As Long
   Dim r2       As Long
   Dim p        As Integer
   Dim Locale   As Long
   
   'Locale = GetUserDefaultLCID()
   r1 = GetLocaleInfo(Locale, Valor, vbNullString, 0)
   
   'buffer
   Simbolo = String$(r1, 0)
   
   'En esta llamada devuelve el s�mbolo en el Buffer
   r2 = GetLocaleInfo(Locale, Valor, Simbolo, r1)
   
   'Localiza el espacio nulo de la cadena para eliminarla
   p = InStr(Simbolo, Chr$(0))
   
   If p > 0 Then
      'Elimina los nulos
      Obtener_Simbolo = Left$(Simbolo, p - 1)
   End If
   
End Function
'
' ############################## PAGINA UNO ###########################################
'
Private Sub PaginaUno(ByRef hay_cierre As Boolean)
    
    vp.CurrentY = vp.CurrentY + rTwips("2mm")
    pon_titulo "", "5mm", "Times New Roman", glb_tamletra_titulo
    vp.CurrentY = vp.CurrentY + rTwips("2mm")
   
    FilaAux = vp.CurrentY
    
    CuadroActivos
    
    RentabilidadCartera
    
    FlujoPatrimonial
    
    FilaAux = vp.CurrentY
    
    DistribucionActivos
    


End Sub

Sub CuadroActivos()
    Dim lCursor As Object
    Dim lCursor_rama As Object
    Dim lReg As Object
    Dim lreg_rama As Object
       
    Dim a As Integer
    Dim suma As Double
    Dim Patrimonio_Actual As Double
    Dim MontoCobrar_Actual  As Double
    Dim MontoPagar_Actual As Double
    Dim MontoTotalCajas_Actual As Double
    
    Dim Patrimonio_Anterior As Double
    Dim MontoCobrar_Anterior  As Double
    Dim MontoPagar_Anterior As Double
    Dim MontoTotalCajas_Anterior As Double
    
    Dim Fecha_Mes_anterior As String
    
    Dim Decimales As Integer
    
    Dim dsc_caja_cuenta  As String
    
    Dim nTotalActivos_Actual As Double
    Dim nTotalActivos_Anterior As Double
    Dim nTotalPasivos_Actual As Double
    Dim nTotalPasivos_Anterior As Double
    
    Dim nTotalActivos_Actual_Para_Porcentajes As Double
    Dim nTotalActivos_Anterior_Para_Porcentajes As Double
    
    Dim Mostrado As Integer
    Dim nTotal_Actual  As Double
    Dim nTotal_Anterior  As Double
    
    Dim iFila As Integer
    Dim cadena As String
    
    Decimales = oCliente.Decimales
    
    Fecha_Mes_anterior = UltimoDiaDelMes(oCliente.Fecha_Cartola)
    
    ' Patrimonio : queda en las variables expuestas
    ' Patrimonio : queda en las variables expuestas
    nTotalActivos_Actual_Para_Porcentajes = oCliente.TraePatrimonioCliente("", Patrimonio_Actual, MontoCobrar_Actual, MontoPagar_Actual)
    nTotalActivos_Anterior_Para_Porcentajes = oCliente.TraePatrimonioCliente(Fecha_Mes_anterior, Patrimonio_Anterior, MontoCobrar_Anterior, MontoPagar_Anterior)
        
    oCliente.TraeTotalCajasConsolidado "", MontoTotalCajas_Actual
    oCliente.TraeTotalCajasConsolidado Fecha_Mes_anterior, MontoTotalCajas_Anterior
    
    a = 1
    suma = 0
    
    nTotalPasivos_Actual = 0
    nTotalPasivos_Anterior = 0
    nTotalActivos_Actual = 0
    nTotalActivos_Anterior = 0
    
    nTotalActivos_Actual_Para_Porcentajes = nTotalActivos_Actual_Para_Porcentajes + MontoTotalCajas_Actual + MontoCobrar_Actual
    nTotalActivos_Anterior_Para_Porcentajes = nTotalActivos_Anterior_Para_Porcentajes + MontoTotalCajas_Anterior + MontoCobrar_Anterior
    
    cadena = ""
    
    With vp
        .CurrentY = .CurrentY + rTwips("2mm")
        FilaAux = vp.CurrentY
        .StartTable
        .TableBorder = tbBox
        .TableCell(tcCols) = 5
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcFontSize, 1, 1, 1, 5) = glb_tamletra_registros
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcText, 1, 1) = "ACTIVOS"
        .TableCell(tcText, 1, 2) = "Valor Actual " & oCliente.Fecha_Cartola ' Fecha
        .TableCell(tcColSpan, 1, 2) = 2
        .TableCell(tcText, 1, 4) = "Valor Anterior " & Fecha_Mes_anterior ' Fecha
        .TableCell(tcColSpan, 1, 4) = 2
        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "38mm"
        .TableCell(tcColWidth, 1, 2) = "27mm"
        .TableCell(tcColWidth, 1, 3) = "15mm"
        .TableCell(tcColWidth, 1, 4) = "28mm"
        .TableCell(tcColWidth, 1, 5) = "15mm"
        ' Alineacion de cada columna
        .TableCell(tcAlign, 1, 1) = taLeftMiddle
        .TableCell(tcAlign, 1, 2) = taRightMiddle
        .TableCell(tcAlign, 1, 3) = taRightMiddle
        .TableCell(tcAlign, 1, 4) = taRightMiddle
        .TableCell(tcAlign, 1, 5) = taRightBottom
        .TableCell(tcRowHeight, 1) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        '************************************************************************
        ' Caja
        '************************************************************************
         dsc_caja_cuenta = "Caja"
        .TableCell(tcText, iFila, 1) = dsc_caja_cuenta
        .TableCell(tcText, iFila, 2) = FormatNumber(MontoTotalCajas_Actual, Decimales)
        .TableCell(tcText, iFila, 3) = FormatPorcentaje(MontoTotalCajas_Actual, nTotalActivos_Actual_Para_Porcentajes)
        .TableCell(tcText, iFila, 4) = FormatNumber(MontoTotalCajas_Anterior, Decimales)
        .TableCell(tcText, iFila, 5) = FormatPorcentaje(MontoTotalCajas_Anterior, nTotalActivos_Anterior_Para_Porcentajes)
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        '************************************************************************
        ' Cuentas por Cobrar
        '************************************************************************
        .TableCell(tcText, iFila, 1) = "Cuentas por Cobrar "
        .TableCell(tcText, iFila, 2) = FormatNumber(MontoCobrar_Actual, Decimales)
        .TableCell(tcText, iFila, 3) = FormatPorcentaje(MontoCobrar_Actual, nTotalActivos_Actual_Para_Porcentajes)
        .TableCell(tcText, iFila, 4) = FormatNumber(MontoCobrar_Anterior, Decimales)
        .TableCell(tcText, iFila, 5) = FormatPorcentaje(MontoCobrar_Anterior, nTotalActivos_Anterior_Para_Porcentajes)
        nTotalActivos_Actual = nTotalActivos_Actual + MontoCobrar_Actual
        nTotalActivos_Anterior = nTotalActivos_Anterior + MontoCobrar_Anterior
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        a = 1
        Mostrado = 0
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Activos %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        gDB.Parametros.Clear
        gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
        If oCliente.TipoConsolidado = CONSOLIDADO_POR_CLIENTE Then
            gDB.Parametros.Add "pid_cliente", ePT_Numero, oCliente.IdCliente, ePD_Entrada
        Else
            gDB.Parametros.Add "pid_grupo", ePT_Numero, oCliente.IdGrupo, ePD_Entrada
        End If
        gDB.Parametros.Add "pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
        gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, DTP_Fecha_Ter.Value, ePD_Entrada
        gDB.Parametros.Add "PId_Moneda_Salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
        gDB.Parametros.Add "PConsolidado", ePT_Caracter, oCliente.TipoConsolidado, ePD_Entrada
        gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_ARBOLES"
        
        If Not gDB.EjecutaSP Then
            Exit Sub
        End If
        Set lCursor = gDB.Parametros("Pcursor").Valor
        gDB.Parametros.Clear
        nTotalActivos_Actual = nTotalActivos_Actual + MontoTotalCajas_Actual
        nTotalActivos_Anterior = nTotalActivos_Anterior + MontoTotalCajas_Anterior
        For Each lReg In lCursor
            gDB.Parametros.Clear
            gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
            gDB.Parametros.Add "PID_ARBOL_CLASE_INST", ePT_Numero, lReg("id").Value, ePD_Entrada
            gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, DTP_Fecha_Ter.Value, ePD_Entrada
            gDB.Parametros.Add "PId_Empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
            If oCliente.TipoConsolidado = CONSOLIDADO_POR_CLIENTE Then
                gDB.Parametros.Add "pid_cliente", ePT_Numero, oCliente.IdCliente, ePD_Entrada
            Else
                gDB.Parametros.Add "pid_grupo", ePT_Numero, oCliente.IdGrupo, ePD_Entrada
            End If
            gDB.Parametros.Add "PId_Moneda_Salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
            gDB.Parametros.Add "PConsolidado", ePT_Caracter, oCliente.TipoConsolidado, ePD_Entrada
            gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ABRE_RAMA"
            If Not gDB.EjecutaSP Then
                Exit Sub
            End If
            Set lCursor_rama = gDB.Parametros("Pcursor").Valor
            gDB.Parametros.Clear
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            .TableCell(tcText, .TableCell(tcRows), 1) = UCase(lReg("DSC_ARBOL_CLASE_INST").Value)
            .TableCell(tcText, .TableCell(tcRows), 2) = FormatNumber(lReg("monto_mon_cta").Value, oCliente.Decimales)
            .TableCell(tcText, .TableCell(tcRows), 3) = FormatPorcentaje(lReg("monto_mon_cta").Value, nTotalActivos_Actual_Para_Porcentajes)
            .TableCell(tcBackColor, .TableCell(tcRows)) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
            .TableCell(tcForeColor, .TableCell(tcRows)) = RGB(255, 255, 255)    ' BLANCO
            .TableCell(tcText, .TableCell(tcRows), 4) = FormatNumber(lReg("MONTO_MON_CTA_ANTERIOR").Value, oCliente.Decimales)
            .TableCell(tcText, .TableCell(tcRows), 5) = FormatPorcentaje(lReg("MONTO_MON_CTA_ANTERIOR").Value, nTotalActivos_Anterior_Para_Porcentajes)
            .TableCell(tcFontSize, .TableCell(tcRows), 1, .TableCell(tcRows), 5) = glb_tamletra_registros
            .TableCell(tcAlign, .TableCell(tcRows), 1, .TableCell(tcRows), 5) = taCenterMiddle
            .TableCell(tcRowHeight, .TableCell(tcRows)) = rTwips("4.6mm")
            For Each lreg_rama In lCursor_rama
                .TableCell(tcRows) = .TableCell(tcRows) + 1
                iFila = .TableCell(tcRows)
                nTotal_Actual = 0
                nTotal_Anterior = 0
                .TableCell(tcText, iFila, 1) = lreg_rama("DSC_ARBOL_CLASE_INST").Value
                .TableCell(tcText, iFila, 2) = FormatNumber(lreg_rama("monto_actual").Value, oCliente.Decimales)
                .TableCell(tcText, iFila, 3) = FormatPorcentaje(lreg_rama("monto_actual").Value, nTotalActivos_Actual_Para_Porcentajes)
                .TableCell(tcText, iFila, 4) = FormatNumber(lreg_rama("monto_anterior").Value, oCliente.Decimales)
                .TableCell(tcText, iFila, 5) = FormatPorcentaje(lreg_rama("monto_anterior").Value, nTotalActivos_Anterior_Para_Porcentajes)
                .TableCell(tcFontSize, iFila, 1, iFila, 5) = glb_tamletra_registros
                .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
                iFila = .TableCell(tcRows)
            Next
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
            nTotalActivos_Actual = nTotalActivos_Actual + lReg("monto_mon_cta").Value
            nTotalActivos_Anterior = nTotalActivos_Anterior + lReg("monto_mon_cta_anterior").Value
         Next
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        .TableCell(tcText, iFila, 1) = "TOTAL ACTIVOS"
        .TableCell(tcText, iFila, 2) = FormatNumber(nTotalActivos_Actual, oCliente.Decimales)
        .TableCell(tcText, iFila, 3) = FormatPorcentaje(nTotalActivos_Actual, nTotalActivos_Actual)
        .TableCell(tcText, iFila, 4) = FormatNumber(nTotalActivos_Anterior, oCliente.Decimales)
        .TableCell(tcText, iFila, 5) = FormatPorcentaje(nTotalActivos_Anterior, nTotalActivos_Anterior)
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcFontSize, 1, 1, iFila, 5) = glb_tamletra_subtitulo1
        .TableCell(tcFontSize, iFila, 1, iFila, 5) = glb_tamletra_subtitulo1 + 1
        .TableCell(tcBackColor, iFila) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        .TableCell(tcAlign, 1, 1, .TableCell(tcRows), 1) = taLeftMiddle
        .TableCell(tcAlign, 1, 2, .TableCell(tcRows), 5) = taRightMiddle
        .EndTable
        '######################## PASIVOS #########################################
        .StartTable
        .TableBorder = tbBox
        .TableCell(tcCols) = 5
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcText, 1, 1) = "PASIVOS"
        .TableCell(tcText, 1, 2) = "Valor Actual " & oCliente.Fecha_Cartola ' Fecha
        .TableCell(tcColSpan, 1, 2) = 2
        .TableCell(tcText, 1, 4) = "Valor Anterior " & Fecha_Mes_anterior ' Fecha
        .TableCell(tcColSpan, 1, 4) = 2
        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "38mm"
        .TableCell(tcColWidth, 1, 2) = "27mm"
        .TableCell(tcColWidth, 1, 3) = "15mm"
        .TableCell(tcColWidth, 1, 4) = "28mm"
        .TableCell(tcColWidth, 1, 5) = "15mm"
        ' Alineacion de cada columna
        .TableCell(tcAlign, 1, 1) = taLeftMiddle
        .TableCell(tcAlign, 1, 2) = taRightMiddle
        .TableCell(tcAlign, 1, 3) = taRightMiddle
        .TableCell(tcAlign, 1, 4) = taRightMiddle
        .TableCell(tcAlign, 1, 5) = taRightMiddle
        .TableCell(tcRowHeight, 1) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        '************************************************************************
        ' Administracion
        '************************************************************************
        .TableCell(tcText, iFila, 1) = "Comisi�n por Adm. "
        .TableCell(tcText, iFila, 2) = "0"
        .TableCell(tcText, iFila, 3) = ""
        .TableCell(tcText, iFila, 4) = "0"
        .TableCell(tcText, iFila, 5) = ""
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        nTotalPasivos_Actual = nTotalPasivos_Actual + 0
        nTotalPasivos_Anterior = nTotalPasivos_Anterior + 0
        '************************************************************************
        ' Cuentas por Pagar
        '************************************************************************
        .TableCell(tcText, iFila, 1) = "Cuentas por Pagar"
        .TableCell(tcText, iFila, 2) = FormatNumber(MontoPagar_Actual, Decimales)
        .TableCell(tcText, iFila, 3) = ""  ' FormatPorcentaje(MontoPagar_Actual, Patrimonio_Actual)
        .TableCell(tcText, iFila, 4) = FormatNumber(MontoPagar_Anterior, Decimales)
        .TableCell(tcText, iFila, 5) = ""  ' FormatPorcentaje(MontoPagar_Anterior, Patrimonio_Anterior)
        .TableCell(tcFontSize, 1, 1, iFila, 5) = glb_tamletra_subtitulo1
        nTotalPasivos_Actual = nTotalPasivos_Actual + MontoPagar_Actual
        nTotalPasivos_Anterior = nTotalPasivos_Anterior + MontoPagar_Anterior
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 2
        iFila = .TableCell(tcRows)
        .TableCell(tcText, iFila, 1) = "TOTAL PASIVOS"
        .TableCell(tcText, iFila, 2) = FormatNumber(nTotalPasivos_Actual, oCliente.Decimales)
        .TableCell(tcText, iFila, 3) = ""
        .TableCell(tcText, iFila, 4) = FormatNumber(nTotalPasivos_Anterior, oCliente.Decimales)
        .TableCell(tcText, iFila, 5) = ""
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcFontSize, iFila, 1, iFila, 5) = glb_tamletra_subtitulo1 + 1
        .TableCell(tcBackColor, iFila) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        .TableCell(tcAlign, 1, 1, .TableCell(tcRows), 1) = taLeftMiddle
        .TableCell(tcAlign, 1, 2, .TableCell(tcRows), 5) = taRightMiddle
        .EndTable
        '************************************************************************
        ' Patrimonio
        '************************************************************************
        .StartTable
        .TableBorder = tbBox
        .TableCell(tcCols) = 5
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcFontSize, 1, 1, 1, 5) = glb_tamletra_subtitulo1 + 1
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "38mm"
        .TableCell(tcColWidth, 1, 2) = "27mm"
        .TableCell(tcColWidth, 1, 3) = "15mm"
        .TableCell(tcColWidth, 1, 4) = "28mm"
        .TableCell(tcColWidth, 1, 5) = "15mm"
        .TableCell(tcText, 1, 1) = "PATRIMONIO"
        Patrimonio_Actual = nTotalActivos_Actual - nTotalPasivos_Actual
        Patrimonio_Anterior = nTotalActivos_Anterior - nTotalPasivos_Anterior
        .TableCell(tcText, 1, 2) = FormatNumber(Patrimonio_Actual, Decimales)
        .TableCell(tcText, 1, 3) = ""
        .TableCell(tcText, 1, 4) = FormatNumber(Patrimonio_Anterior, Decimales)
        .TableCell(tcText, 1, 5) = ""
        .TableCell(tcAlign, 1, 1, .TableCell(tcRows), 1) = taLeftMiddle
        .TableCell(tcAlign, 1, 2, .TableCell(tcRows), 5) = taRightMiddle
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .EndTable
    End With
End Sub

Function SumaTotalClase(ByVal lCursor As hRecord, _
                        ByVal sClase As String, _
                        ByRef nTotal_Actual As Double, _
                        ByRef nTotal_Anterior As Double) As Double
    'Dim lReg As hFields
    
    nTotal_Anterior = 0
    nTotal_Actual = 0
    
'    For Each lReg In lCursor
'        'If sClase = lreg("DSC_ARBOL_CLASE_INST").Value Then
'            'nTotal_Actual = nTotal_Actual + CDbl(lreg("MONTO_MON_CTA").Value)
'            'nTotal_Anterior = nTotal_Anterior + CDbl(lreg("Monto_Anterior").Value)
'        'End If
'    Next
   
End Function
Function SumaTotalActivos(ByVal lCursor As hRecord, _
                        ByRef nTotal_Actual As Double, _
                        ByRef nTotal_Anterior As Double) As Double
    'Dim lReg As hFields
    
    nTotal_Anterior = 0
    nTotal_Actual = 0
    
'    For Each lReg In lCursor
        'nTotal_Actual = nTotal_Actual + CDbl(lReg("Monto_Actual").Value)
        'nTotal_Anterior = nTotal_Anterior + CDbl(lReg("Monto_Anterior").Value)
'    Next
   
End Function

Sub RentabilidadCartera()
    Dim lCursor As hRecord
    Dim lReg As hFields
    Dim iFila As Integer
    
    Dim Xinicio As Variant
    Dim YInicio As Variant
    
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    If oCliente.TipoConsolidado = CONSOLIDADO_POR_CLIENTE Then
       gDB.Parametros.Add "Pid_cliente", ePT_Numero, oCliente.IdCliente, ePD_Entrada
    Else
       gDB.Parametros.Add "Pid_grupo", ePT_Numero, oCliente.IdGrupo, ePD_Entrada
    End If
    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
    gDB.Parametros.Add "Pid_moneda_salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
    gDB.Procedimiento = "PKG_CARTOLA$Rentabilidades_Consolidado"

    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor = gDB.Parametros("Pcursor").Valor

    gDB.Parametros.Clear

    With vp
        vp.MarginLeft = COLUMNA_DOS ' + rTwips("150mm")
        Xinicio = vp.MarginLeft
        YInicio = FilaAux
        'YInicio = 5557 + rTwips("5mm")
        .LineSpacing = nInterlineado
        .CurrentY = YInicio
        FilaAux = vp.CurrentY

        .StartTable
        .TableBorder = tbBox

        .TableCell(tcCols) = 3

        .TableCell(tcRows) = .TableCell(tcRows) + 1
        
        .TableCell(tcFontSize, 1, 1, 1, 3) = glb_tamletra_subtitulo2
        .TableCell(tcBackColor, 1, 1, 1, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcRowHeight, 1) = rTwips("4.6mm")
        .TableCell(tcText, 1, 1) = " RENTABILIDADES "
        .TableCell(tcText, 1, 3) = "En PESOS"
        .TableCell(tcText, 1, 3) = "En UF"
        '.TableCell(tcText, 1, 4) = "En Dolares"
        '.TableCell(tcText, 1, 5) = "Volatilidad"
        
        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "55mm"
        .TableCell(tcColWidth, 1, 2) = "35mm"
        .TableCell(tcColWidth, 1, 3) = "35mm"
        
        .TableCell(tcColAlign, 1, 1, 1, 3) = taLeftTop
        '.TableCell(tcColWidth, 1, 4) = "28mm"
        '.TableCell(tcColWidth, 1, 5) = "15mm"


        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)

        For Each lReg In lCursor
            .TableCell(tcText, iFila, 1) = "Rentabilidad Mensual"
            .TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_mensual_$$"), 2, True) & "%"
            .TableCell(tcText, iFila, 3) = FormatNumber(lReg("rentabilidad_mensual_UF"), 2, True) & "%"
            '.TableCell(tcText, iFila, 4) = lReg("rentabilidad_mensual_DO")
            '.TableCell(tcText, iFila, 5) = lReg("volatilidad")

            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)

            .TableCell(tcText, iFila, 1) = "Rentabilidad Acumulada Anual"
            .TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_anual_$$"), 2, True) & "%"
            .TableCell(tcText, iFila, 3) = FormatNumber(lReg("rentabilidad_anual_UF"), 2, True) & "%"
            '.TableCell(tcText, iFila, 4) = lReg("rentabilidad_anual_DO")
            '.TableCell(tcText, iFila, 5) = lReg("volatilidad")

            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
            
            .TableCell(tcText, iFila, 1) = "Rentabilidad �ltimos 12 meses"
            .TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_ult_12_meses_$$"), 2, True) & "%"
            .TableCell(tcText, iFila, 3) = FormatNumber(lReg("rentabilidad_ult_12_meses_UF"), 2, True) & "%"
            
            '.TableCell(tcText, iFila, 4) = lReg("rentabilidad_ult_12_meses_DO")
            '.TableCell(tcText, iFila, 5) = lReg("volatilidad")
        Next
        
        ' Alineacion de cada columna
        .TableCell(tcAlign, , 1) = taLeftMiddle
        .TableCell(tcAlign, , 2) = taRightMiddle
        .TableCell(tcAlign, , 3) = taRightMiddle
        '.TableCell(tcAlign, , 4) = taRightMiddle
        '.TableCell(tcAlign, , 5) = taRightMiddle
        .TableCell(tcFontSize, 1, 1, 4, 4) = glb_tamletra_registros
        
        .EndTable
        
    End With
    
    Set lCursor = Nothing
    Set lReg = Nothing
End Sub

Sub FlujoPatrimonial()
    Dim lCursor As hRecord
    Dim lReg As hFields
    Dim iFila As Integer
    Dim Xinicio As Variant
    Dim YInicio As Variant
    Dim i As Integer
    Dim j As Integer
    
    Dim lTotal_Aportes As Double
    Dim lTotal_Retiros As Double
    
    Dim lTotal_Aportes_UF As Double
    Dim lTotal_Retiros_UF As Double

    'PKG_CAJAS_CUENTA$Buscar

    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    If oCliente.TipoConsolidado = CONSOLIDADO_POR_CLIENTE Then
        gDB.Parametros.Add "Pid_cliente", ePT_Numero, oCliente.IdCliente, ePD_Entrada
    Else
        gDB.Parametros.Add "Pid_grupo", ePT_Numero, oCliente.IdGrupo, ePD_Entrada
    End If
    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
    gDB.Parametros.Add "Pid_moneda_salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
    gDB.Parametros.Add "PConsolidado", ePT_Caracter, oCliente.TipoConsolidado, ePD_Entrada
    gDB.Procedimiento = "PKG_CARTOLA$AporteRescate_Consolidado"

    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor = gDB.Parametros("Pcursor").Valor
    With vp
        vp.MarginLeft = COLUMNA_DOS ' + rTwips("150mm")
        Xinicio = .MarginLeft
        YInicio = .CurrentY + rTwips("5mm")
        .LineSpacing = nInterlineado
        'YInicio = 3120 + rTwips("5mm")
        .CurrentY = YInicio
        FilaAux = .CurrentY

        .StartTable
        .TableBorder = tbBox

        .TableCell(tcCols) = 3

        .TableCell(tcRows) = .TableCell(tcRows) + 1

        .TableCell(tcFontSize, 1, 1, 3, 3) = glb_tamletra_subtitulo1
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
        
        '.TableCell(tcBackColor, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        '.TableCell(tcBackColor, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)

        .TableCell(tcText, 1, 1) = "FLUJO PATRIMONIAL"
        .TableCell(tcText, 1, 2) = "APORTES"
        .TableCell(tcText, 1, 3) = "RETIROS"
        
        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "55mm"
        .TableCell(tcColWidth, 1, 2) = "35mm"
        .TableCell(tcColWidth, 1, 3) = "35mm"
        .TableCell(tcRowHeight, 1) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        
        lTotal_Aportes_UF = 0
        lTotal_Retiros_UF = 0
        
        i = 1

        For Each lReg In lCursor
            If i = 10 Then Exit For
            .TableCell(tcText, iFila, 1) = lReg("FECHA_MOVIMIENTO")
            If lReg("FLG_TIPO_MOVIMIENTO") = "A" Then
                  .TableCell(tcText, iFila, 2) = FormatNumber(lReg("monto").Value, oCliente.Decimales)
                  .TableCell(tcText, iFila, 3) = "-"
            Else
                  .TableCell(tcText, iFila, 2) = "-"
                  .TableCell(tcText, iFila, 3) = FormatNumber(lReg("monto").Value, oCliente.Decimales)
            End If
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
            i = i + 1
            lTotal_Aportes = lReg("MONTO_TOTAL_APORTES").Value
            lTotal_Retiros = lReg("MONTO_TOTAL_RESCATES").Value
            lTotal_Aportes_UF = lReg("MONTO_TOTAL_APORTES_UF").Value
            lTotal_Retiros_UF = lReg("MONTO_TOTAL_RESCATES_UF").Value
            .TableCell(tcFontSize, iFila, 1, iFila, 3) = glb_tamletra_registros
        Next
        
        For j = i To 10
            .TableCell(tcText, iFila, 1) = ""
            .TableCell(tcText, iFila, 2) = ""
            .TableCell(tcText, iFila, 3) = ""
            
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
        Next
        
        '.TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcText, .TableCell(tcRows), 1) = "TOTAL EN " & Trim(Moneda_Cuentas)
        .TableCell(tcText, .TableCell(tcRows), 2) = FormatNumber(lTotal_Aportes, oCliente.Decimales)
        .TableCell(tcText, .TableCell(tcRows), 3) = FormatNumber(lTotal_Retiros, oCliente.Decimales)
        .TableCell(tcBackColor, .TableCell(tcRows)) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, .TableCell(tcRows)) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 3) = glb_tamletra_registros
        ' Alineacion de cada columna
        .TableCell(tcAlign, , 1) = taLeftMiddle
        .TableCell(tcAlign, , 2) = taRightMiddle
        .TableCell(tcAlign, , 3) = taRightMiddle
        .TableCell(tcRowHeight, .TableCell(tcRows)) = rTwips("4.6mm")
        ' *********************************************************
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcText, .TableCell(tcRows), 1) = "TOTAL EN UF"
        .TableCell(tcText, .TableCell(tcRows), 2) = FormatNumber(lTotal_Aportes_UF, 2)
        .TableCell(tcText, .TableCell(tcRows), 3) = FormatNumber(lTotal_Retiros_UF, 2)
        .TableCell(tcBackColor, .TableCell(tcRows)) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, .TableCell(tcRows)) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 3) = glb_tamletra_registros
        ' Alineacion de cada columna
        .TableCell(tcAlign, , 1) = taLeftMiddle
        .TableCell(tcAlign, , 2) = taRightMiddle
        .TableCell(tcAlign, , 3) = taRightMiddle
        .TableCell(tcRowHeight, .TableCell(tcRows)) = rTwips("4.6mm")
        .EndTable
    End With
    Set lCursor = Nothing
    Set lReg = Nothing
End Sub

Sub DistribucionActivos()
    Dim lCursor As Object
    Dim lReg As Object
    
    Dim Patrimonio_Actual As Double
    Dim MontoCobrar_Actual  As Double
    Dim MontoPagar_Actual As Double
    Dim MontoTotalCajas_Actual As Double
    
    'Dim Patrimonio_Anterior As Double
    'Dim MontoCobrar_Anterior  As Double
    'Dim MontoPagar_Anterior As Double
    'Dim MontoTotalCajas_Anterior As Double
    
    Dim Fecha_Mes_anterior As String
    Dim Mostrado As Integer
    'Dim dsc_producto As String
    Dim prod_anterior As String
    'Dim dsc_intrumento As String
    
    Dim nTotal_Actual  As Double
    Dim nTotal_Anterior  As Double
    
    'Dim nTotalActivos_Actual As Double
    'Dim nTotalActivos_Anterior As Double
    
    'Dim Porc_Monto_mon_cta As Double
    
    'Dim monto_mon_cta As Double
        
    Dim Decimales As Integer
    
    Dim nMargenLeft  As Variant
    'Dim iFilaActual As Variant
    Dim j As Integer
    Dim aux As Double
    Dim aux1 As String
    
    Dim sNombres()
    Dim sValores()
    Dim i As Integer
    
    i = 0
    
    Decimales = oCliente.Decimales
    
    Fecha_Mes_anterior = UltimoDiaDelMes(oCliente.Fecha_Cartola)
    
    ' Patrimonio : queda en las variables expuestas
    oCliente.TraePatrimonioCliente "", Patrimonio_Actual, MontoCobrar_Actual, MontoPagar_Actual
    
    oCliente.TraeTotalCajasConsolidado "", MontoTotalCajas_Actual
    
    
    If MontoTotalCajas_Actual > 0 Then
        ReDim Preserve sNombres(i)
        ReDim Preserve sValores(i)
        sNombres(i) = "CAJA"
        sValores(i) = MontoTotalCajas_Actual
        i = i + 1
    End If
     
    Mostrado = 0
              
    '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Activos %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'    gDB.Parametros.Clear
'    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
'    gDB.Parametros.Add "PFecha_Cartola", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
'    gDB.Parametros.Add "PFecha_Anterior", ePT_Fecha, CDate(Fecha_Mes_anterior), ePD_Entrada
'    gDB.Procedimiento = "PKG_CARTOLA$DAME_ACTIVOS_CLASE"
    
        gDB.Parametros.Clear
        gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
        If oCliente.TipoConsolidado = CONSOLIDADO_POR_CLIENTE Then
            gDB.Parametros.Add "pid_cliente", ePT_Numero, oCliente.IdCliente, ePD_Entrada
        Else
            gDB.Parametros.Add "pid_grupo", ePT_Numero, oCliente.IdGrupo, ePD_Entrada
        End If
        gDB.Parametros.Add "pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
        gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, DTP_Fecha_Ter.Value, ePD_Entrada
        gDB.Parametros.Add "PId_Moneda_Salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
        gDB.Parametros.Add "PConsolidado", ePT_Caracter, oCliente.TipoConsolidado, ePD_Entrada

        gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_ARBOLES"
    
    
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If
    
    Set lCursor = gDB.Parametros("Pcursor").Valor
    
    gDB.Parametros.Clear
     
    'SumaTotalActivos lCursor, nTotalActivos_Actual, nTotalActivos_Anterior
     
    For Each lReg In lCursor
        If Mostrado = 0 Or prod_anterior <> lReg("DSC_ARBOL_CLASE_INST").Value Then
           nTotal_Actual = 0
           nTotal_Anterior = 0
            
           'SumaTotalClase lCursor, lReg("DSC_ARBOL_CLASE_INST").Value, nTotal_Actual, nTotal_Anterior
           
           Mostrado = 1
           prod_anterior = lReg("DSC_ARBOL_CLASE_INST").Value
           
           If CDbl(NVL(lReg("MONTO_MON_CTA").Value, 0)) > 0 Then
              ReDim Preserve sNombres(i)
              ReDim Preserve sValores(i)
              sNombres(i) = lReg("DSC_ARBOL_CLASE_INST").Value
              'sValores(i) = nTotal_Actual
              sValores(i) = lReg("MONTO_MON_CTA").Value
              i = i + 1
           End If
        End If
    Next
    
    If i > 0 Then
        For i = LBound(sValores) To UBound(sValores)
            For j = i + 1 To UBound(sValores) - 1
                If CDbl(sValores(i)) > CDbl(sValores(j)) Then
                    aux = sValores(j)
                    sValores(j) = sValores(i)
                    sValores(i) = aux
    
                    aux1 = sNombres(j)
                    sNombres(j) = sNombres(i)
                    sNombres(i) = aux1
                End If
            Next
        Next
        
        Dim sName As String
        
        sName = fncGraficoTorta(sNombres, sValores, "Composici�n Cartera", 11, False)

        With vp
            .CurrentY = FilaAux + rTwips("5mm")
            FilaAux = .CurrentY
            'FilaAux = 7200
            nMargenLeft = .MarginLeft
            .MarginLeft = COLUMNA_DOS
            .DrawPicture LoadPicture(sName), vp.MarginLeft + rTwips("10mm"), FilaAux, "90mm", "55mm", , False
            .MarginLeft = nMargenLeft
            .CurrentY = FilaAux
        End With
        
    End If
    
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)

End Sub


'############################################ PAGINA UNO ######################################

Sub fncDisclaimer()
    Dim sTxt As String
    vp.FontSize = 8
    If (vp.CurrentY + rTwips("30mm")) > (vp.PageHeight - vp.MarginBottom - rTwips("10mm")) Then
        vp.NewPage
    Else
        vp.CurrentY = (vp.PageHeight - vp.MarginBottom - rTwips("30mm"))
    End If
    vp.BrushColor = vbWhite
    vp.DrawRectangle vp.MarginLeft - rTwips("0mm"), vp.CurrentY, (vp.PageWidth - vp.MarginRight) + rTwips("0mm"), vp.CurrentY + rTwips("30mm")
    sTxt = "AVISO IMPORTANTE"
    vp.Paragraph = sTxt
    sTxt = "" & vbCrLf
    sTxt = sTxt & "El presente documento ha sido elaborado por Corredora Gesti�n de Inversiones S.A., de conformidad con el Mandato para la Administraci�n de Cartera celebrado entre el Cliente y Moneda Gesti�n de Inversiones S.A. el cual se entiende formar parte del presente instrumento."
    sTxt = sTxt & "Corredora Gesti�n de Inversiones S.A. no asume ninguna responsabilidad de cualquier informaci�n impl�cita o expl�cita que pudiera contener este documento."
    sTxt = sTxt & "La informaci�n contenida en el presente instrumento es confidencial y est� dirigida exclusivamente al destinatario. Cualquier uso, reproducci�n, divulgaci�n o distribuci�n por otras personas se encuentra estrictamente prohibida, acarreando su incumplimiento sanciones penales de conformidad a la ley. "
    sTxt = sTxt & vbCrLf
    vp.Paragraph = sTxt
End Sub
Private Sub haz_linea(pY As Variant, Optional bColor As Boolean = False)
    Dim vColorAnt As Variant
    vColorAnt = vp.PenColor
    'If bColor Then
        vp.PenColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    'End If
    
    vp.DrawLine 0, 0, 0, 0
    vp.DrawLine 0, 0, 0, 0
    vp.PenColor = vColorAnt
End Sub
Function Monto_Moneda(ByVal sCodMoneda As String) As String
    Dim s As String
    s = UCase(Trim(sCodMoneda))
    Select Case s
        Case "PESO"
            Monto_Moneda = "$"
        Case "DO"
            Monto_Moneda = "USD"
        Case Else
            Monto_Moneda = s
    End Select
End Function
Function Dame_Decimales(ByVal s As String)
    Select Case s
        Case "PESO"
            Dame_Decimales = 0
        Case Else
            Dame_Decimales = 2
    End Select
End Function

Function fncGraficoTorta(catnom, valores, stitulo, tipo, bLeyenda)
    Dim xColorTitulo As String
    Dim xColor As String
    'Dim aColors()
    Dim cd, c
    Dim Path As String
    Dim sFname As String
    
    
    xColorTitulo = ColorToHex(0, 0, 0)
    xColor = ColorToHex(RColorRelleno, GColorRelleno, BColorRelleno)

'    aColors = Array(ColorToHex(44, 41, 38), _
'                    ColorToHex(18, 17, 15), _
'                    ColorToHex(196, 183, 169), _
'                    ColorToHex(81, 41, 38), _
'                    ColorToHex(248, 245, 241), _
'                    ColorToHex(138, 128, 119), _
'                    ColorToHex(238, 245, 241), _
'                    ColorToHex(189, 184, 179), _
'                    ColorToHex(214, 200, 186), _
'                    ColorToHex(194, 181, 167), _
'                    ColorToHex(196, 183, 169), _
'                    ColorToHex(64, 61, 48) _
'                    )

    
    Set cd = CreateObject("ChartDirector.API")
    
    cd.setLicenseCode ("YT5ZJ59NKAF9ZR3942DAE53E")

    Set c = cd.PieChart(900, 350)

    Call c.setPieSize(450, 140, 250)

    Call c.set3D(40, 75)
    
    'Call c.addLegend(330, 40)
    
    Call c.setLabelFormat("<*size=10*><*block*>{label}<*br*>{percent}%<*/*>")

    Call c.setStartAngle(120, False)

    Call c.SetData(valores, catnom)

    'sTitulo= "<*size=12*><*block*><*color=" & xColorTitulo & "*>" & sTitulo & "<*/*>"

    Call c.addTitle(stitulo, "arial.ttf", 16, xColorTitulo)
    
    Call c.setLabelPos(50, cd.LineColor)

    ' Call c.setColors2(cd.DataColor, aColors)
    ' Call c.setColors(aColors)

    'Path = App.Path & "\" '    App.Path ' Left(Request.ServerVariables("PATH_INFO"), InStrRev(Request.ServerVariables("PATH_INFO"), "/graficos/"))
    
    'MODIFICADO PARA QUE EL SISTEMA DEJE EN UN DIRECTORIO TEMPORAL EL JPG QUE SE GENERA DEBIDO A QUE ESTABA
    'ENSUCIANDO EL DIRECTORIO DE INSTALACION DEL SISTEMA, LO CUAL, ERA INCORRECTO.�
    'HAF: 20070620.
    Path = Environ$("TEMP") & "\"

    sFname = c.makeTmpFile(Path & sFname, cd.JPG)

    fncGraficoTorta = Path & sFname

End Function
Function ColorToHex(r, g, b)
    ColorToHex = "&H" & Hex(r) & Hex(g) & Hex(b)
End Function
Function fncGraficoValorCuota() As String
    'Dim oRs As ADODB.Recordset
    'Dim xSql
    Dim sName As String
    Dim stitulo
    Dim fechainicrent
    Dim fechabaserent
    Dim lCursor_Cuotas As hRecord
    Dim lCampo As hFields
    Dim catnom(), Valor()
    Dim catnomc(), valorC()
    Dim catnom3(), valor3()

    Dim sFechaMenor
    Dim sFechaMayor

    Dim iValorMenor
    Dim iValorMayor

    Dim nRentabilidad_diaria
    Dim iBase As Integer
    Dim i As Integer
    
    fechainicrent = "21/10/2006" ' & Year(glb_fecha_hoy)
    fechabaserent = "21/11/2006"
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pid_cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_inicio", ePT_Numero, fechainicrent, ePD_Entrada
    gDB.Parametros.Add "Pfecha_final", ePT_Numero, fechabaserent, ePD_Entrada
    gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS.RENTABILIDAD_PERIODOS"
    If gDB.EjecutaSP Then
        Set lCursor_Cuotas = gDB.Parametros("pcursor").Valor
        If lCursor_Cuotas.Count > 0 Then
            iBase = 100
            i = 0
            For Each lCampo In lCursor_Cuotas
            
                If NVL(lCampo("rentabilidad_mon_cuenta").Value, 0) = 0 Then
                    nRentabilidad_diaria = 0
                Else
                    nRentabilidad_diaria = CDbl(lCampo("rentabilidad_mon_cuenta").Value)
                End If
                If nRentabilidad_diaria <> 0 Then
                    ReDim Preserve catnom(i)
                    ReDim Preserve Valor(i)
        
                    ReDim Preserve catnomc(i)
                    ReDim Preserve valorC(i)
                    ReDim Preserve catnom3(i)
                    ReDim Preserve valor3(i)
                    If i = 0 Then
                        sFechaMenor = lCampo("fecha_cierre").Value
                        sFechaMayor = lCampo("fecha_cierre").Value
        
                        iValorMenor = iBase
                        iValorMayor = iBase
        
                        catnom(i) = lCampo("fecha_cierre").Value
                        Valor(i) = iBase
                    Else
                        catnom(i) = lCampo("fecha_cierre").Value
                        Valor(i) = Valor(i - 1) * (1 + nRentabilidad_diaria / 100)
                    End If
        
                    If sFechaMenor > lCampo("fecha_cierre").Value Then
                        sFechaMenor = lCampo("fecha_cierre").Value
                    End If
        
                    If sFechaMayor < lCampo("fecha_cierre").Value Then
                        sFechaMayor = lCampo("fecha_cierre").Value
                    End If
        
                    If iValorMenor > Valor(i) Then
                        iValorMenor = Valor(i)
                    End If
        
                    If iValorMayor < Valor(i) Then
                        iValorMayor = Valor(i)
                    End If
                    catnom(i) = Format(lCampo("fecha_cierre").Value, "dd/mm/yy") 'Mid(DameMes(lCampo("fecha_cierre").Value), 1, 3) & "-" & Year(lCampo("fecha_cierre").Value)
                    catnomc(i) = lCampo("fecha_cierre").Value & "-"
                    valorC(i) = CDbl(lCampo("valor_cuota_mon_cuenta").Value)
                    i = i + 1
                End If
            Next
        End If
    Else
        MsgBox gDB.Parametros.Errnum & vbCr & gDB.Parametros.ErrMsg, vbCritical
    End If
    gDB.Parametros.Clear

    ' cantidades = datediff("d", fechainicrent, fechainicrent)

'    Do While Not oRs.EOF
'        If IsNull(oRs("rentabilidad_diaria")) Then
'            nRentabilidad_diaria = 0
'        Else
'            nRentabilidad_diaria = CDbl(oRs("rentabilidad_diaria"))
'        End If
'
'        If nRentabilidad_diaria <> 0 Then
'            ReDim Preserve catnom(i)
'            ReDim Preserve Valor(i)
'
'            ReDim Preserve catnomc(i)
'            ReDim Preserve valorC(i)
'            ReDim Preserve catnom3(i)
'            ReDim Preserve valor3(i)
'
'            If i = 0 Then
'                sFechaMenor = oRs("fecha")
'                sFechaMayor = oRs("fecha")
'
'                iValorMenor = iBase
'                iValorMayor = iBase
'
'                catnom(i) = oRs("fecha")
'                Valor(i) = iBase
'            Else
'                catnom(i) = oRs("fecha")
'                Valor(i) = Valor(i - 1) * (1 + nRentabilidad_diaria / 100)
'            End If
'
'            If sFechaMenor > oRs("fecha") Then
'                sFechaMenor = oRs("fecha")
'            End If
'
'            If sFechaMayor < oRs("fecha") Then
'                sFechaMayor = oRs("fecha")
'            End If
'
'            If iValorMenor > Valor(i) Then
'                iValorMenor = Valor(i)
'            End If
'
'            If iValorMayor < Valor(i) Then
'                iValorMayor = Valor(i)
'            End If
'
'            catnom(i) = Mid(DameMes(oRs("fecha")), 1, 3) & "-" & Year(oRs("fecha"))
'
'            catnomc(i) = oRs("fecha") & "-"
'            valorC(i) = CDbl(oRs("cuota"))
'
'            i = i + 1
'
'        End If
'
'        oRs.MoveNext
'
'    Loop
    
    Dim nSemestres As Double
    'Dim iDiferencia As Double
    Dim bLineaTendencia As Boolean

'    nSemestres = Round((DateDiff("d", sFechaMenor, sFechaMayor) / 365) * 2)
'    nSemestres = DateDiff("m", sFechaMenor, sFechaMayor)
'
'    iDiferencia = (iValorMayor - iValorMenor) / 10
'
'    iValorMenor = iValorMenor - iDiferencia
'    iValorMayor = iValorMayor + iDiferencia

    ' sTitulo = "Valor Cuota - " & fechainicrent & " hasta " & fechabaserent & " "
    stitulo = "VALOR CUOTA/BASE 100"

    bLineaTendencia = False
      

'   sName  = GraficoLineas(catnom, valor, sTitulo, false, i-1, nSemestres, bLineaTendencia, Int(iValorMenor/1.1), INT(iValorMayor*1.1) )
    sName = GraficoLineas(catnom, Valor, stitulo, False, i - 1, nSemestres, bLineaTendencia, iValorMenor, iValorMayor)

    fncGraficoValorCuota = sName

End Function

Function GraficoLineas(Categories1, Vals1, stitulo, leyenda, entries, nSemestres, bLineaTendencia, iValorMenor, iValorMayor) As String
    Dim cd
    Dim c
    Dim xColor          As String
    Dim xColorTitulo    As String
    Dim noOfPoints      As Integer
    Dim Path            As String
    Dim sFname As String
    'Dim sTitulo
    Dim x As Integer
    Dim y As Integer
    
    
    
    

    xColorTitulo = ColorToHex(0, 0, 0)
    xColor = ColorToHex(RColorRelleno, GColorRelleno, BColorRelleno)
    'x = 950
    'y = 350

    noOfPoints = 1 ' UBound(Vals1) - 1

    Set cd = CreateObject("ChartDirector.API")
 
    cd.setLicenseCode ("YT5ZJ59NKAF9ZR3942DAE53E")
    
    x = 350
    y = 220
    Set c = cd.XYChart(x, y) ' , cd.metalColor(&HDEDEDE), -1, 2)

    'Call c.addTitle(stitulo, "arialbd.ttf", 13, &HFFFFFF).setBackground(&HFF0000, &H0, 0)      ' Add a title to the y axis
    Call c.setPlotArea(50, 60, x - 60, y - 90)

'    Set c = cd.XYChart(x, y, &HFFFFFF, 1, 0)
 '   Call c.setPlotArea(100, 60, Int(x * 0.9), Int(y * 0.7), cd.Transparent, cd.Transparent, cd.Transparent, cd.Transparent, cd.Transparent)

    'Call c.yAxis().setLabelStyle("GILB____.TTF", 10)
    'Call c.yAxis().setLabelStyle("GILB____.TTF", 12)
    Call c.yAxis().setLabelStyle("arialbd.ttf", 7, &H555555)
    Call c.yAxis().setLabelFormat("{value|4.}")
    
    Call c.yAxis().setLinearScale(iValorMenor, iValorMayor)

    Call c.xAxis().setLabels(Categories1)

    'Call c.xAxis().setLabelStyle("GILB____.TTF", 12)  ' , 0, 90)
    Call c.xAxis().setLabelStyle("arialbd.ttf", 7, &H555555, 30).setpos(0, -1)
    
    Call c.xAxis().setLabelStep(3)
    
    Call c.addLineLayer(Vals1, 0)

    stitulo = "<*size=14,yoffset=-11*><*block*><*color=" & xColorTitulo & "*>" & stitulo & "<*br*><*/*>"

    Call c.addTitle(stitulo, "GILB____.TTF", 14, xColorTitulo).setBackground(xColor)
    
    'Path = App.Path & "\" '    App.Path ' "." ' Left(Request.ServerVariables("PATH_INFO"), InStrRev(Request.ServerVariables("PATH_INFO"), "/graficos/"))

    'MODIFICADO PARA QUE EL SISTEMA DEJE EN UN DIRECTORIO TEMPORAL EL JPG QUE SE GENERA DEBIDO A QUE ESTABA
    'ENSUCIANDO EL DIRECTORIO DE INSTALACION DEL SISTEMA, LO CUAL, ERA INCORRECTO.�
    'HAF: 20070620.
    Path = Environ$("TEMP") & "\"

    sFname = c.makeTmpFile(Path & sFname, cd.JPG)
    
    GraficoLineas = Path & sFname

End Function


Private Function FormatPorcentaje(Valor As Double, Valor_Total As Double, Optional ByRef nValorAsignado As Double = 0) As String
    If Valor_Total <> 0 Then
        FormatPorcentaje = FormatNumber((Valor / Valor_Total) * 100, 2) & "%"
        nValorAsignado = (Valor / Valor_Total) * 100
    Else
        FormatPorcentaje = FormatNumber(0, 2) & "%"
        nValorAsignado = 0
    End If
End Function

Function FormatNumber(xValor As Variant, iDecimales As Integer, Optional bPorcentaje As Boolean = False) As String
    Dim xFormato        As String
    Dim sDecimalSep     As String
    Dim sMilesSep       As String
        
    sDecimalSep = Obtener_Simbolo(LOCALE_SDECIMAL)
    sMilesSep = Obtener_Simbolo(LOCALE_SMONTHOUSANDSEP)
    
    On Error GoTo PError
        
    If iDecimales = 0 Then
        xFormato = "###,###,##0"
    ElseIf bPorcentaje Then
        xFormato = "##0." & String(iDecimales - 1, "#") & "0"
    Else
        xFormato = "###,###,##0." & String(iDecimales - 1, "#") & "0"
    End If
    
    GoTo Fin
    
PError:
    xValor = "0"
    
Fin:
    FormatNumber = Format(xValor, xFormato)
    On Error GoTo 0
    
End Function

Function DameMes(sFecha)
    Dim iMes As Integer
    Dim aMeses
    iMes = Month(sFecha)
    aMeses = Array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
    DameMes = aMeses(iMes)
End Function
'******************************************************************************************************************
'Private Sub ActivosClaseInstrumento()
'
'    Const clrHeader = &HD0D0D0
'    '------------------------------------------
'    'Const sHeader_SA = "Nemot�cnico|Cantidad|Precio|Valor Mercado|Valor Mercado Cuenta|"
'    'Const sFormat_SA = "2900|>1600|>1100|>2500|>2500|3600"
'    'Const sHeader_Detalle_Caja = "Fecha Mov.|Fecha Liq.|Origen Mov.|Movimientos|Ingreso|Egreso|Saldo"
'    'Const sFormat_Detalle_Caja = "1200|1100|1950|4550|>1800|>1800|>1800"
'    '------------------------------------------
'    Dim sRecord
'    Dim bAppend
'    Dim lLinea As Integer
'    '------------------------------------------
'    Dim lPatrimonio As Class_Patrimonio_Cuentas
'    Dim lReg_Pat As hFields
'    Dim lPatrim As String
'    Dim lRentabilidad As Double
'    '------------------------------------------
'    Dim lCajas_Ctas As Class_Cajas_Cuenta
'    Dim lCursor_Caj As hRecord
'    Dim lCursor_Caja As hRecord
'    Dim lReg_Caj As hFields
'    Dim lReg_Caja As hFields
'    Dim lMonto_Mon_Caja As String
'    '------------------------------------------
'    Dim lSaldos_Activos As Class_Saldo_Activos
'    Dim lCursor_SA As hRecord
'    Dim lReg_SA As hFields
'    Dim lReg_SN As hFields
'    Dim lCursor_SN As hRecord
'
'    Dim lCuenta_Lineas As Integer
'    Dim lTope As Integer
'
'    '------------------------------------------
'    Dim lParidad As Double
'    Dim lOperacion As String
'    Dim lMonto_Mon_Cta As String
'    '------------------------------------------
'    '    Dim lcTipo_Cambio As Class_Tipo_Cambios
'    Dim lcTipo_Cambio As Object
'    '------------------------------------------
'    Dim lInstrumento As Class_Instrumentos
'    Dim lCursor_Ins As hRecord
'    Dim lReg_Ins As hFields
'    Dim lHay_Activos As Boolean
'    '------------------------------------------
'    Dim lSaldos_Caja As Class_Saldos_Caja
'
'    '--------------------------------------
'    Dim lReg As hCollection.hFields
'    '    Dim lcCuenta As Class_Cuentas
'    Dim lcCuenta As Object
'    Dim lcMov_Caja As Class_Mov_Caja
'    Dim lcSaldos_Caja As Class_Saldos_Caja
'    '--------------------------------------
'    Dim lFecDesde As Date
'    Dim lFecHasta As Date
'    Dim lId_Caja_Cuenta As String
'    Dim lTotal As Double
'    Dim lIngreso As Double
'    Dim lEgreso As Double
'    Dim lUltimaCaja As Long
'    Dim nDecimales As Integer
'    Dim iFila As Integer
'    Dim i As Integer
'    Dim anterior As Integer
'    Dim Mostrado As Integer
'    '------------------------------------------
'    Dim Monto As Double
'    Dim Porcentaje As Double
'
'    Dim cont As Integer
'
'    Dim lCodProducto As String
'
'    Dim yAnt As Variant
'
'    Call Sub_Bloquea_Puntero(Me)
'
'    'If Not Fnt_Form_Validar(Me.Controls) Then
'        'GoTo ErrProcedure
'    'End If
'
'    Rem Busca el patrimonio de la cuenta y su rentabilidad
'    Set lPatrimonio = New Class_Patrimonio_Cuentas
'    With lPatrimonio
'        .Campo("id_cuenta").Valor = lId_Cuenta
'        .Campo("FECHA_CIERRE").Valor = DTP_Fecha_Ter.Value
'        'Valores por defecto
'        .Campo("RENTABILIDAD_MON_CUENTA").Valor = Null
'        .Campo("APORTE_RETIROS_MON_CUENTA").Valor = Null
'
'        If .Buscar Then
'            For Each lReg_Pat In .Cursor
'                lPatrim = FormatNumber(lReg_Pat("PATRIMONIO_MON_CUENTA").Value, nDecimales)
'                lRentabilidad = NVL(lReg_Pat("RENTABILIDAD_MON_CUENTA").Value, 0)
'            Next
'        Else
'            MsgBox .ErrMsg, vbCritical, Me.Caption
'            'GoTo ErrProcedure
'        End If
'    End With
'
'    Rem Busca las cajas de la cuenta
'    Set lCajas_Ctas = New Class_Cajas_Cuenta
'
'    With lCajas_Ctas
'        .Campo("id_cuenta").Valor = lId_Cuenta
'        If .Buscar(True) Then
'            Set lCursor_Caj = .Cursor
'            Set lCursor_Caja = .Cursor
'        Else
'            MsgBox .ErrMsg, vbCritical, Me.Caption
'            'GoTo ErrProcedure
'        End If
'    End With
'
'    Rem Busca los Instrumentos del sistema
'    Set lInstrumento = New Class_Instrumentos
'    With lInstrumento
'        If .Buscar Then
'            Set lCursor_Ins = .Cursor
'        Else
'            MsgBox .ErrMsg, vbCritical, Me.Caption
'            'GoTo ErrProcedure
'        End If
'    End With
'
'    Rem Comienzo de la generaci�n del reporte
'    lLinea = 2
'
'    lHay_Activos = False
'
'    anterior = 0
'    gDB.Parametros.Clear
'    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
'    gDB.Parametros.Add "Pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
'    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
'    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA"
'
'    If Not gDB.EjecutaSP Then
'        Exit Sub
'    End If
'
'    Set lCursor_Ins = gDB.Parametros("Pcursor").Valor
'    Dim iAumentoFila As Double
'    With vp
'        .MarginLeft = COLUMNA_INICIO    ' + rTwips("150mm")
'        FilaAux = .CurrentY
'
'        iAumentoFila = Twips2mm(.LineSpacing)
'
'        cont = 0
'        For Each lReg In lCursor_Ins
'
'            Monto = 0
'            Porcentaje = 0
'
'            If CDbl(NVL(lReg("monto_mon_cta_inst").Value, 0)) <> 0 Then
'                If anterior = lReg("id_arbol_clase_inst").Value Then
'                    '.NewPage
'                    '.Paragraph = ""
'                Else
''                    If lReg("codigo").Value = "FFMM" Then
''                        cont = cont + 1
''                        If cont = 1 Then
''                            '.NewPage
''                            .CurrentY = 2510
''                            .CurrentY = vp.CurrentY + rTwips("2mm")
''                            .Paragraph = ""
''                            'pon_titulo UCase("RESUMEN ACTIVOS"), "5mm", Font_Name, glb_tamletra_titulo
''                            .CurrentY = vp.CurrentY + rTwips("2mm")
''                            'MsgBox CurrentY
''
''                            FilaAux = vp.CurrentY
''                            '.CurrentY = 2750
''                        Else
''                            .Paragraph = ""
''                        End If
''                    Else
'                        'If anterior <> 0 Then
'                            ' If .CurrentY > 4000 Or anterior = 0 Then
'                            .NewPage
'                            '.CurrentY = 2510
'                            .CurrentY = vp.CurrentY + rTwips("2mm")
'                          ' .Paragraph = ""
'                            'pon_titulo UCase("RESUMEN ACTIVOS"), "5mm", Font_Name, glb_tamletra_titulo
'                        'End If
'                        .CurrentY = vp.CurrentY + rTwips("2mm")
'                        FilaAux = vp.CurrentY
'
'                        .TableBorder = tbBox
'                        '.CurrentY = 2750
''                    End If
'                End If
'
'                'If .CurrentY > 8000 Then
'                '    .NewPage
'                'End If
'
'' HAY UN ELSE NUEVO PARA CONTROLAR LA DISTORCI�N DE LA CARTOLA.
'
'                If CDbl(NVL(lReg("monto_mon_cta_inst").Value, 0)) Then
'                    '.CurrentY = 3000
'                    'yAnt = .CurrentY
'                    .FontSize = 10
'                    .FontBold = True
'
'                    If anterior <> lReg("id_arbol_clase_inst").Value Then
'                        .Paragraph = "TIPO DE INSTRUMENTO: " & Trim(lReg("dsc_arbol_clase_inst").Value)
'                    Else
'                        '.Paragraph = ""
'                    End If
'
'                    '.CurrentY = yAnt
'                    .FontBold = False
'
'                    If CDbl(NVL(Trim(lReg("monto_mon_cta_hoja").Value), 0)) <> 0 And _
'                        CInt(lReg("NIVEL").Value) = 0 Then
'
'                        .CurrentY = .CurrentY + rTwips("3mm")
'
'                        ' .FontBold = False
'                        .Paragraph = Trim(lReg("dsc_arbol_clase_inst_hoja").Value)
'                        .FontSize = glb_tamletra_registros
'
'                        .StartTable
'                            .TableBorder = tbBox
'                            .TableBorder = tbBoxRows
'
'                            .TableCell(tcCols) = 11
'                            .TableCell(tcRows) = .TableCell(tcRows) + 1
'                            .TableCell(tcFontSize, 1, 1, 1, 3) = glb_tamletra_registros
'
'                            .TableCell(tcText, 1, 2) = ""
'                        '   .TableCell(tcColSpan, 1, 4) = 2
'                            .TableCell(tcText, 1, 4) = ""
'
'                        '   Ancho de cada columna
'                        '   .TableCell(tcColWidth, 1, 1) = "60mm"
'                        '   .TableCell(tcColWidth, 1, 2) = "25mm"
'                        '   .TableCell(tcColWidth, 1, 3) = "25mm"
'                        '   .TableCell(tcColWidth, 1, 4) = "30mm"
'                        '   .TableCell(tcColWidth, 1, 5) = "30mm"
'                        '   .TableCell(tcColWidth, 1, 6) = "30mm"
'                        '   .TableCell(tcColWidth, 1, 7) = "30mm"
'                        '   .TableCell(tcColWidth, 1, 8) = "25mm"
'
'                        lCodProducto = lReg("codigo").Value
'
'                        Select Case lCodProducto
'                            Case "RV"
'                                .TableCell(tcCols) = 8
'                                .TableCell(tcText, 1, 1) = "NEMOTECNICO"
'                                .TableCell(tcText, 1, 2) = "MONEDA"
'                                .TableCell(tcText, 1, 3) = "% DE INVERSION"
'                                .TableCell(tcText, 1, 4) = "CANTIDAD"
'                                .TableCell(tcText, 1, 5) = "PRECIO PROMEDIO" & vbCrLf & "COMPRA"
'                                .TableCell(tcText, 1, 6) = "PRECIO" & vbCrLf & "ACTUAL"
'                                .TableCell(tcText, 1, 7) = "VALOR" & vbCrLf & "MERCADO"
'                                .TableCell(tcText, 1, 8) = "RENTABILIDAD"
'
'                                .TableCell(tcColWidth, 1, 1) = "70mm"
'                                .TableCell(tcColWidth, 1, 2) = "20mm"
'                                .TableCell(tcColWidth, 1, 3) = "25mm"
'                                .TableCell(tcColWidth, 1, 4) = "25mm"
'                                .TableCell(tcColWidth, 1, 5) = "30mm"
'                                .TableCell(tcColWidth, 1, 6) = "25mm"
'                                .TableCell(tcColWidth, 1, 7) = "35mm"
'                                .TableCell(tcColWidth, 1, 8) = "25mm"
'
'                                '.TableCell(tcColWidth, 1, 9) = "20mm"
'                                '.TableCell(tcColWidth, 1, 9) = "25mm"
'                                '.TableCell(tcColWidth, 1, 10) = "25mm"
'
'                            Case "RF"
'                                .TableCell(tcCols) = 13
'                                .TableCell(tcText, 1, 1) = "NEMOTECNICO"
'                                .TableCell(tcText, 1, 2) = "EMISOR"
'                                .TableCell(tcText, 1, 3) = "MONEDA"
'                                .TableCell(tcText, 1, 4) = "% DE " & vbCrLf & "INVERSION"
'                                .TableCell(tcText, 1, 5) = "" & vbCrLf & "NOMINALES"
'                                .TableCell(tcText, 1, 6) = "TASA" & vbCrLf & "CUP�N"
'                                .TableCell(tcText, 1, 7) = "FECHA" & vbCrLf & "VCTO."
'                                .TableCell(tcText, 1, 8) = " "           ' "DIAS"
'                                .TableCell(tcText, 1, 9) = " "           ' "DURATION"
'                                .TableCell(tcText, 1, 10) = "TASA" & vbCrLf & "COMPRA"
'                                 .TableCell(tcText, 1, 11) = ""   ' "PRECIO" & vbCrLf & "MERCADO"    ' "PRECIO" & vbCrLf & "COMPRA"
'                                .TableCell(tcText, 1, 12) = "TASA" & vbCrLf & "MERCADO"
'                                .TableCell(tcText, 1, 13) = "VALORIZACI�N"
'
'                                .TableCell(tcColWidth, 1, 1) = "35mm"
'                                .TableCell(tcColWidth, 1, 2) = "22mm"
'                                .TableCell(tcColWidth, 1, 3) = "22mm"
'                                .TableCell(tcColWidth, 1, 4) = "15mm"
'                                .TableCell(tcColWidth, 1, 5) = "25mm"
'                                .TableCell(tcColWidth, 1, 6) = "15mm"
'                                .TableCell(tcColWidth, 1, 7) = "25mm"
'                                .TableCell(tcColWidth, 1, 8) = "6mm" ' 10mm
'                                .TableCell(tcColWidth, 1, 9) = "6mm" ' 15mm
'                                .TableCell(tcColWidth, 1, 10) = "25mm"
'                                .TableCell(tcColWidth, 1, 11) = "4mm"   ' "25mm"
'                                .TableCell(tcColWidth, 1, 12) = "25mm"
'                                .TableCell(tcColWidth, 1, 13) = "30mm"
'
'                            Case "FFMM"
'                                .TableCell(tcCols) = 8
'                                .TableCell(tcText, 1, 1) = "DESCRIPCI�N"
'                                .TableCell(tcText, 1, 2) = "MONEDA"
'                                .TableCell(tcText, 1, 3) = "% DE INVERSION"
'                                .TableCell(tcText, 1, 4) = "CANTIDAD DE" & vbCrLf & "CUOTAS"
'                                .TableCell(tcText, 1, 5) = "VALOR CUOTA" & vbCrLf & "PROMEDIO COMPRA"
'                                .TableCell(tcText, 1, 6) = "VALOR CUOTA" & vbCrLf & "ACTUAL"
'                                '.TableCell(tcText, 1, 7) = "" ' "VAL. MERCADO " & vbCrLf & "MON. PAPEL"
'                                .TableCell(tcText, 1, 7) = "VALOR ACTUAL " ' & vbCrLf & "MON. CUENTA"
'                                .TableCell(tcText, 1, 8) = "RENTABILIDAD"
'
'                                .TableCell(tcColWidth, 1, 1) = "70mm"
'                                .TableCell(tcColWidth, 1, 2) = "20mm"
'                                .TableCell(tcColWidth, 1, 3) = "25mm"
'                                .TableCell(tcColWidth, 1, 4) = "25mm"
'                                .TableCell(tcColWidth, 1, 5) = "30mm"
'                                .TableCell(tcColWidth, 1, 6) = "25mm"
'                                .TableCell(tcColWidth, 1, 7) = "35mm"
'                                .TableCell(tcColWidth, 1, 8) = "25mm"
'
'                        End Select
'
'                        .TableCell(tcAlign, 1, 1) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 2) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 3) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 4) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 5) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 6) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 7) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 8) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 9) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 10) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 11) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 12) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 13) = taCenterMiddle
'
'                        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'                        .TableCell(tcBackColor, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'
'                        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)  ' Blanco
'                        .TableCell(tcForeColor, 2) = RGB(255, 255, 255)  ' Blanco
'
'                        gDB.Parametros.Clear
'                        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'                        gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
'                        gDB.Parametros.Add "PID_ARBOL_CLASE", ePT_Numero, lReg("id_arbol_clase_inst_hoja").Value, ePD_Entrada
'                        gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
'                        gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO"
'
'                        If Not gDB.EjecutaSP Then
'                            Exit Sub
'                        End If
'
'                        Set lCursor_SA = gDB.Parametros("Pcursor").Valor
'
'                        vp.MarginLeft = COLUMNA_INICIO    ' + rTwips("150mm")
'                        FilaAux = .CurrentY
'
'                        For Each lReg_SA In lCursor_SA
'                            .TableCell(tcRows) = .TableCell(tcRows) + 1
'                            iFila = .TableCell(tcRows)
'
'                            '  If .CurrentY > 8000 Then
'                            '     .NewPage
'                            '  End If
'
'                            Select Case lCodProducto    ' lReg("codigo").Value
'                                Case "RV"
'                                    .TableCell(tcText, iFila, 1) = Trim(lReg_SA("nemotecnico").Value)
'                                    .TableCell(tcText, iFila, 2) = lReg_SA("simbolo_moneda").Value
'                                    .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
'                                    .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("cantidad").Value, 0)
'                                    .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_compra").Value, 2)
'
'                                    '.TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_promedio_compra").Value, 2)
'                                    '.TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio_promedio_compra").Value * lReg_SA("cantidad").Value, 2)
'
'                                    .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio").Value, 2)
'                                    '.TableCell(tcText, iFila, 8) = FormatNumber(lReg_SA("monto_mon_nemotecnico").Value, 2)
'                                    .TableCell(tcText, iFila, 7) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)    ' Valor de Mercado
'                                    .TableCell(tcText, iFila, 8) = IIf(CDbl(lReg_SA("rentabilidad").Value) > 1000, "-", FormatNumber(lReg_SA("rentabilidad").Value, 2) & "%")
'
'                                    .TableCell(tcAlign, iFila, 2) = taCenterMiddle
'                                    .TableCell(tcAlign, iFila, 3) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 4) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 5) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 6) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 7) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 8) = taRightMiddle
'
'                                    Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
'                                    Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
'
'                                Case "FFMM"
'                                    .TableCell(tcText, iFila, 1) = Trim(lReg_SA("DSC_nemotecnico").Value)
'                                    .TableCell(tcText, iFila, 2) = Trim(lReg_SA("simbolo_moneda").Value)
'                                    .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
'                                    .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("cantidad").Value, 4)
'                                    .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_compra").Value, 4)
'                                    .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio").Value, 4)
'                                    '.TableCell(tcText, iFila, 7) = "" ' FormatNumber(lReg_SA("monto_mon_nemotecnico").Value, oCliente.Decimales)   'Decimales_Moneda(Trim(lReg_SA("simbolo_moneda").Value)))
'                                    .TableCell(tcText, iFila, 7) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)
'                                    .TableCell(tcText, iFila, 8) = IIf(CDbl(lReg_SA("rentabilidad").Value) > 1000, "-", FormatNumber(lReg_SA("rentabilidad").Value, 2) & "%")
'
'                                    .TableCell(tcAlign, iFila, 2) = taCenterMiddle
'                                    .TableCell(tcAlign, iFila, 3) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 4) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 5) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 6) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 7) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 8) = taRightMiddle
'                                    '.TableCell(tcAlign, iFila, 9) = taRightMiddle
'
'                                    Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
'                                    Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
'
'                                Case "RF"
'                                    .TableCell(tcText, iFila, 1) = Trim(lReg_SA("nemotecnico").Value)
'                                    .TableCell(tcText, iFila, 2) = Left(Trim(lReg_SA("cod_emisor").Value), 16)
'                                    .TableCell(tcText, iFila, 3) = Trim(lReg_SA("SIMBOLO_MONEDA").Value)
'                                    .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
'                                    .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("cantidad").Value, 0)
'                                    .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("tasa_emision").Value, 2)
'                                    .TableCell(tcText, iFila, 7) = Trim(lReg_SA("fecha_vencimiento").Value)
'                                    .TableCell(tcText, iFila, 8) = " " ' FormatNumber(lReg_SA("dias").Value, 2)
'                                    .TableCell(tcText, iFila, 9) = " " ' FormatNumber(lReg_SA("duration").Value, 2)
'                                    .TableCell(tcText, iFila, 10) = FormatNumber(lReg_SA("tasa_compra").Value, 2)
'                                    .TableCell(tcText, iFila, 11) = ""    ' FormatNumber(lReg_SA("precio_compra").Value, 2)
'                                    .TableCell(tcText, iFila, 12) = FormatNumber(lReg_SA("tasa").Value, 2)
'                                    .TableCell(tcText, iFila, 13) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)
'
'                                    .TableCell(tcAlign, iFila, 2) = taLeftMiddle
'                                    .TableCell(tcAlign, iFila, 3) = taCenterMiddle
'                                    .TableCell(tcAlign, iFila, 4) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 5) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 6) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 7) = taCenterMiddle   ' taRightMiddle
'                                    .TableCell(tcAlign, iFila, 8) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 9) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 10) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 11) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 12) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 13) = taRightMiddle
'
'                                    Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
'                                    Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
'
'                            End Select
'
'                        Next  ' Nemotecnicos (For Each lReg_SA In lCursor_SA)
'                        .TableCell(tcRows) = .TableCell(tcRows) + 1
'                        iFila = iFila + 1
'
'                        Select Case lReg("codigo").Value
'                            Case "RV"
'                                .TableCell(tcText, iFila, 3) = FormatNumber(Porcentaje, 2) & "%"
'                                .TableCell(tcAlign, iFila, 3) = taRightMiddle
'                                .TableCell(tcText, iFila, 6) = "TOTAL"
'                                .TableCell(tcAlign, iFila, 6) = taRightMiddle
'                                .TableCell(tcText, iFila, 7) = FormatNumber(Monto, oCliente.Decimales)
'                                .TableCell(tcAlign, iFila, 7) = taRightMiddle
'                            Case "FFMM"
'                                .TableCell(tcText, iFila, 3) = FormatNumber(Porcentaje, 2) & "%"
'                                .TableCell(tcAlign, iFila, 3) = taRightMiddle
'
'                                .TableCell(tcText, iFila, 6) = "TOTAL"
'                                .TableCell(tcAlign, iFila, 6) = taRightMiddle
'
'                                .TableCell(tcText, iFila, 7) = FormatNumber(Monto, oCliente.Decimales)
'                                .TableCell(tcAlign, iFila, 7) = taRightMiddle
'
'                            Case "RF"
'                                .TableCell(tcText, iFila, 4) = FormatNumber(Porcentaje, 2) & "%"
'                                .TableCell(tcAlign, iFila, 4) = taRightMiddle
'
'                                .TableCell(tcText, iFila, 12) = "TOTAL"
'                                .TableCell(tcAlign, iFila, 12) = taRightMiddle
'
'                                .TableCell(tcText, iFila, 13) = FormatNumber(Monto, oCliente.Decimales)
'                                .TableCell(tcAlign, iFila, 13) = taRightMiddle
'                        End Select
'                        .TableCell(tcBackColor, iFila) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'                        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)   ' Blanco
'                        .EndTable
'                    End If
'' ESTO HAY QUE CORREGIR, SE ME FUE EN COLLERA
''                ElseIf CDbl(NVL(lReg("monto_mon_cta_inst").Value, 0)) <> 0 Then
''                    If anterior <> lReg("id_arbol_clase_inst").Value Then
''                        .Paragraph = "TIPO DE INSTRUMENTO: " & Trim(lReg("dsc_arbol_clase_inst").Value)
''                    Else
''                        .Paragraph = ""
''                    End If
'                End If
'            End If
'
'            If lReg("codigo").Value = "RV" And _
'                anterior <> lReg("id_arbol_clase_inst").Value Then
'                '.NewPage
'                '.Paragraph = ""
'                'Call PaginaDos
'            End If
'
'            anterior = lReg("id_arbol_clase_inst").Value
'        Next ' For Each lReg In lCursor_Ins
'
''        .NewPage
''        .CurrentY = 2510
''        .CurrentY = vp.CurrentY + rTwips("2mm")
''        pon_titulo UCase("MOVIMIENTOS DE FONDOS MUTUOS"), "5mm", Font_Name, glb_tamletra_titulo
''        .CurrentY = vp.CurrentY + rTwips("2mm")
''        FilaAux = vp.CurrentY
''                  For Each lReg In lCursor_Ins
''                      If lReg("codigo").Value = "FFMM" Then
''                    gDB.Parametros.Clear
''                    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
''                    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
''                    gDB.Parametros.Add "PID_ARBOL_CLASE", ePT_Numero, lReg("id_arbol_clase_inst_hoja").Value, ePD_Entrada
''                    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
''                    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO"
''
''                    If Not gDB.EjecutaSP Then
''                    Exit Sub
''                    End If
''
''                    Set lCursor_SA = gDB.Parametros("Pcursor").Valor
''
''                      For Each lReg_SA In lCursor_SA
''                          If .CurrentY > 10000 Then
''                              .NewPage
''                              .CurrentY = 2510
''                              .CurrentY = vp.CurrentY + rTwips("2mm")
''                              pon_titulo UCase("MOVIMIENTOS FONDOS MUTUOS"), "5mm", Font_Name, glb_tamletra_titulo
''                              .CurrentY = vp.CurrentY + rTwips("2mm")
''                              FilaAux = vp.CurrentY
''                          End If
''
''                        .CurrentY = vp.CurrentY + rTwips("2mm")
''                        pon_titulo UCase("MOVIMIENTOS ") & lReg_SA("nemotecnico").Value, "3mm", Font_Name, 8
''                        .CurrentY = vp.CurrentY + rTwips("2mm")
''                        FilaAux = vp.CurrentY
''
''                          gDB.Parametros.Clear
''                          gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
''                          gDB.Parametros.Add "id_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
''                          gDB.Parametros.Add "fecha_ini", ePT_Numero, CDate(oCliente.Fecha_Cartola) - 30, ePD_Entrada
''                          gDB.Parametros.Add "fecha_ter", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
''                          gDB.Parametros.Add "id_nemo", ePT_Numero, lReg_SA("id_nemotecnico").Value, ePD_Entrada
''                          gDB.Procedimiento = "PKG_MOV_ACTIVOS$BUSCAR_MOV_NEMO"
''
''                          If Not gDB.EjecutaSP Then
''                          Exit Sub
''                          End If
''
''                          Set lCursor_SN = gDB.Parametros("Pcursor").Valor
''                          .StartTable
''                          .TableCell(tcRows) = 1
''                          .TableCell(tcCols) = 5
''                          '.TableCell(tcRows) = lCursor_SN.Count
''                          iFila = 1
''                          .TableCell(tcText, iFila, 1) = "FECHA MOVIMIENTO"
''                          .TableCell(tcText, iFila, 2) = "DESCRIPCI�N"
''                          .TableCell(tcText, iFila, 3) = "N�MERO DE  CUOTAS"
''                          .TableCell(tcText, iFila, 4) = "VALOR CUOTA"
''                          .TableCell(tcText, iFila, 5) = "MONTO INVERTIDO"
''
''                          .TableCell(tcAlign, iFila, 1) = taCenterMiddle
''                          .TableCell(tcAlign, iFila, 2) = taCenterMiddle
''                          .TableCell(tcAlign, iFila, 3) = taCenterMiddle
''                          .TableCell(tcAlign, iFila, 4) = taCenterMiddle
''                          .TableCell(tcAlign, iFila, 5) = taCenterMiddle
''
''                          .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
''                          iFila = iFila + 1
''                          For Each lReg_SN In lCursor_SN
''                             .TableCell(tcRows) = .TableCell(tcRows) + 1
''                             .TableCell(tcText, iFila, 1) = lReg_SN("fecha_movimiento").Value
''                             .TableCell(tcText, iFila, 2) = lReg_SN("dsc_movimiento").Value
''                             .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SN("cantidad").Value, 4)
''                             .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SN("precio").Value, 4)
''                             .TableCell(tcText, iFila, 5) = IIf(lReg_SN("flg_tipo_movimiento").Value = "I", FormatNumber(lReg_SN("monto_total").Value, oCliente.Decimales), FormatNumber(CDbl(lReg_SN("monto_total").Value) * -1, oCliente.Decimales))
''
''                             .TableCell(tcAlign, iFila, 1) = taCenterMiddle
''                             .TableCell(tcAlign, iFila, 2) = taLeftMiddle
''                             .TableCell(tcAlign, iFila, 3) = taRightMiddle
''                             .TableCell(tcAlign, iFila, 4) = taRightMiddle
''                             .TableCell(tcAlign, iFila, 5) = taRightMiddle
''
''                             'FormatNumber(lReg_SN("monto_total").Value, 2)
''                             'IIf(lReg_SN("flg_tipo_movimiento").Value = 'I', formatnumber(lreg_sn("monto_total").value,2),0)
''                             iFila = iFila + 1
''
''                          Next
''                          .TableCell(tcColWidth, , 1) = "30mm"
''                          .TableCell(tcColWidth, , 2) = "80mm"
''                          .TableCell(tcColWidth, , 3) = "30mm"
''                          .TableCell(tcColWidth, , 4) = "30mm"
''                          .TableCell(tcColWidth, , 5) = "40mm"
''                          .TableCell(tcFontSize, 1, 1, iFila, 5) = 7
''                          .EndTable
''
''
''                      Next
''                      End If
''
''                      Next
'
'
'        Set lCursor_Ins = Nothing
'        Set lReg = Nothing
'
'    End With
'
'End Sub

'***********************************************************************************************************
Private Sub ActivosClaseInstrumento()
    Dim lLinea As Integer
    '------------------------------------------
    Dim lPatrimonio As Class_Patrimonio_Cuentas
    Dim lReg_Pat As hFields
    Dim lPatrim As String
    Dim lMontoPatrimonio As Double
    Dim lRentabilidad As Double
    '------------------------------------------
    Dim lCajas_Ctas As Class_Cajas_Cuenta
    Dim lCursor_Caj As hRecord
    Dim lCursor_Caja As hRecord
    '------------------------------------------
    Dim lCursor_SA As hRecord
    Dim lReg_SA As hFields
    '------------------------------------------
    Dim lInstrumento As Class_Instrumentos
    Dim lCursor_Ins As hRecord
    Dim lHay_Activos As Boolean
    '--------------------------------------
    Dim lReg As hCollection.hFields
    Dim nDecimales As Integer
    Dim iFila As Integer
    Dim anterior As Integer
    '------------------------------------------
    Dim Monto As Double
    Dim Porcentaje As Double
    Dim cont As Integer
    Dim lCodProducto As String
    Dim sColorTextoAnt      As Long

    Call Sub_Bloquea_Puntero(Me)

    Rem Busca el patrimonio de la cuenta y su rentabilidad
    oCliente.TraePatriyRentab_Consolidado lMontoPatrimonio, lRentabilidad
    lPatrim = FormatNumber(lMontoPatrimonio, nDecimales)


    Rem Busca los Instrumentos del sistema
    Set lInstrumento = New Class_Instrumentos
    With lInstrumento
        If .Buscar Then
            Set lCursor_Ins = .Cursor
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            'GoTo ErrProcedure
        End If
    End With

    Rem Comienzo de la generaci�n del reporte
    lLinea = 2

    lHay_Activos = False

    anterior = 0
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    If oCliente.TipoConsolidado = CONSOLIDADO_POR_CLIENTE Then
        gDB.Parametros.Add "Pid_cliente", ePT_Numero, oCliente.IdCliente, ePD_Entrada
    Else
        gDB.Parametros.Add "Pid_grupo", ePT_Numero, oCliente.IdGrupo, ePD_Entrada
    End If
    gDB.Parametros.Add "Pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
    gDB.Parametros.Add "Pid_moneda_salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
    gDB.Parametros.Add "PConsolidado", ePT_Caracter, oCliente.TipoConsolidado, ePD_Entrada
    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA"

    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor_Ins = gDB.Parametros("Pcursor").Valor
    Dim iAumentoLinea As Double
    
    With vp
        ' .NewPage
        iAumentoLinea = Twips2mm(.LineSpacing)
    
        ' .CurrentY = rTwips("55mm")

        cont = 0
        iLineasImpresas = 0

        For Each lReg In lCursor_Ins
            Monto = 0
            Porcentaje = 0

            If CDbl(NVL(lReg("monto_mon_cta_inst").Value, 0)) <> 0 And _
                    CDbl(NVL(Trim(lReg("monto_mon_cta_hoja").Value), 0)) <> 0 And _
                        CInt(lReg("NIVEL").Value) = 0 Then
                        
                'If CDbl(NVL(Trim(lReg("monto_mon_cta_hoja").Value), 0)) <> 0 And _
                    CInt(lReg("NIVEL").Value) = 0 Then
                    
                    gDB.Parametros.Clear
                    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
                    If oCliente.TipoConsolidado = CONSOLIDADO_POR_CLIENTE Then
                        gDB.Parametros.Add "Pid_cliente", ePT_Numero, oCliente.IdCliente, ePD_Entrada
                    Else
                        gDB.Parametros.Add "Pid_grupo", ePT_Numero, oCliente.IdGrupo, ePD_Entrada
                    End If
                    gDB.Parametros.Add "PID_ARBOL_CLASE", ePT_Numero, lReg("id_arbol_clase_inst_hoja").Value, ePD_Entrada
                    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
                    gDB.Parametros.Add "Pid_moneda_salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
                    gDB.Parametros.Add "PConsolidado", ePT_Caracter, oCliente.TipoConsolidado, ePD_Entrada
                    gDB.Procedimiento = "PKG_CARTOLA$ResumenActivos_Consolidado"

                    If Not gDB.EjecutaSP Then
                        Exit Sub
                    End If

                    Set lCursor_SA = gDB.Parametros("Pcursor").Valor

                    If anterior <> lReg("id_arbol_clase_inst").Value Then
                        .NewPage
                        
                        .FontSize = 10
                        .FontBold = True
                        .Paragraph = "TIPO DE INSTRUMENTO: " & Trim(lReg("dsc_arbol_clase_inst").Value)
                        .FontBold = False
                        
                        iLineasImpresas = .LineSpacing
                    End If
                    
                    anterior = lReg("id_arbol_clase_inst").Value
                                        
                    lCodProducto = lReg("codigo").Value
                    If iLineasImpresas + 220 >= (.LineSpacing * (CONS_NUMERO_LINEAS - 3)) Then
                        .NewPage
                        .FontSize = 10
                        .Paragraph = Trim(lReg("dsc_arbol_clase_inst_hoja").Value)
                        .FontSize = glb_tamletra_registros
                        'Encabezados de Los Productos
                        Call PoneEncabezados(vp, lCodProducto)
                        iLineasImpresas = .LineSpacing
                    Else
                        .FontBold = False
                        .FontSize = 10
                        .Paragraph = Trim(lReg("dsc_arbol_clase_inst_hoja").Value)
                        .FontBold = False
                        .FontSize = glb_tamletra_registros
                        Call PoneEncabezados(vp, lCodProducto)
                    End If

                    vp.MarginLeft = COLUMNA_INICIO    ' + rTwips("150mm")

                    For Each lReg_SA In lCursor_SA
                        iFila = .TableCell(tcRows)
                        
                        If iLineasImpresas >= (.LineSpacing * (CONS_NUMERO_LINEAS - 3)) Then
                            .EndTable
                            .NewPage
                            .FontSize = 10
                            .Paragraph = Trim(lReg("dsc_arbol_clase_inst_hoja").Value) & "( Continuaci�n )"
                            .FontSize = glb_tamletra_registros
                            'Encabezados de Los Productos
                            Call PoneEncabezados(vp, lCodProducto)

                            iLineasImpresas = .LineSpacing
                        End If

                        Select Case lCodProducto    ' lReg("codigo").Value
                            Case "RV"
                                .TableCell(tcText, iFila, 1) = Trim(lReg_SA("nemotecnico").Value)
                                .TableCell(tcText, iFila, 2) = lReg_SA("simbolo_moneda").Value
                                .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
                                .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("cantidad").Value, 0)
                                .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_compra").Value, 2)

                                .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio").Value, 2)
                                .TableCell(tcText, iFila, 7) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)    ' Valor de Mercado
                                .TableCell(tcText, iFila, 8) = IIf(CDbl(lReg_SA("rentabilidad").Value) > 1000, "-", FormatNumber(lReg_SA("rentabilidad").Value, 2) & "%")

                                .TableCell(tcAlign, iFila, 2) = taCenterMiddle
                                .TableCell(tcAlign, iFila, 3) = taRightMiddle
                                .TableCell(tcAlign, iFila, 4) = taRightMiddle
                                .TableCell(tcAlign, iFila, 5) = taRightMiddle
                                .TableCell(tcAlign, iFila, 6) = taRightMiddle
                                .TableCell(tcAlign, iFila, 7) = taRightMiddle
                                .TableCell(tcAlign, iFila, 8) = taRightMiddle

                                Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
                                Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)

                            Case "FFMM"
                                .TableCell(tcText, iFila, 1) = Trim(lReg_SA("DSC_nemotecnico").Value)
                                .TableCell(tcText, iFila, 2) = Trim(lReg_SA("simbolo_moneda").Value)
                                .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
                                .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("cantidad").Value, 4)
                                .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_compra").Value, 4)
                                .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio").Value, 4)
                                .TableCell(tcText, iFila, 7) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)
                                .TableCell(tcText, iFila, 8) = IIf(CDbl(lReg_SA("rentabilidad").Value) > 1000, "-", FormatNumber(lReg_SA("rentabilidad").Value, 2) & "%")

                                .TableCell(tcAlign, iFila, 2) = taCenterMiddle
                                .TableCell(tcAlign, iFila, 3) = taRightMiddle
                                .TableCell(tcAlign, iFila, 4) = taRightMiddle
                                .TableCell(tcAlign, iFila, 5) = taRightMiddle
                                .TableCell(tcAlign, iFila, 6) = taRightMiddle
                                .TableCell(tcAlign, iFila, 7) = taRightMiddle
                                .TableCell(tcAlign, iFila, 8) = taRightMiddle

                                Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
                                Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)

                            Case "RF"
                                .TableCell(tcText, iFila, 1) = Trim(lReg_SA("nemotecnico").Value)
                                .TableCell(tcText, iFila, 2) = Left(Trim(lReg_SA("cod_emisor").Value), 16)
                                .TableCell(tcText, iFila, 3) = Trim(lReg_SA("SIMBOLO_MONEDA").Value)
                                .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
                                .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("cantidad").Value, 0)
                                .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("tasa_emision").Value, 2)
                                .TableCell(tcText, iFila, 7) = Trim(NVL(lReg_SA("fecha_vencimiento").Value, ""))
                                .TableCell(tcText, iFila, 8) = FormatNumber(lReg_SA("tasa_compra").Value, 2)
                                .TableCell(tcText, iFila, 9) = FormatNumber(lReg_SA("tasa").Value, 2)
                                .TableCell(tcText, iFila, 10) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)

                                .TableCell(tcAlign, iFila, 2) = taLeftMiddle
                                .TableCell(tcAlign, iFila, 3) = taCenterMiddle
                                .TableCell(tcAlign, iFila, 4) = taRightMiddle
                                .TableCell(tcAlign, iFila, 5) = taRightMiddle
                                .TableCell(tcAlign, iFila, 6) = taRightMiddle
                                .TableCell(tcAlign, iFila, 7) = taCenterMiddle
                                .TableCell(tcAlign, iFila, 8) = taRightMiddle
                                .TableCell(tcAlign, iFila, 9) = taRightMiddle
                                .TableCell(tcAlign, iFila, 10) = taRightMiddle

                                Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
                                Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
                                
                        End Select

                        iLineasImpresas = iLineasImpresas + .LineSpacing
                        .TableCell(tcRows) = .TableCell(tcRows) + 1
                        
                    Next  ' Nemotecnicos (For Each lReg_SA In lCursor_SA)
                    
                    iLineasImpresas = iLineasImpresas + .LineSpacing
                    iFila = .TableCell(tcRows)

                    Select Case lReg("codigo").Value
                        Case "RV"
                            .TableCell(tcText, iFila, 3) = FormatNumber(Porcentaje, 2) & "%"
                            .TableCell(tcAlign, iFila, 3) = taRightMiddle
                            .TableCell(tcText, iFila, 6) = "TOTAL"
                            .TableCell(tcAlign, iFila, 6) = taRightMiddle
                            .TableCell(tcText, iFila, 7) = FormatNumber(Monto, oCliente.Decimales)
                            .TableCell(tcAlign, iFila, 7) = taRightMiddle

                        Case "FFMM"
                            .TableCell(tcText, iFila, 3) = FormatNumber(Porcentaje, 2) & "%"
                            .TableCell(tcAlign, iFila, 3) = taRightMiddle

                            .TableCell(tcText, iFila, 6) = "TOTAL"
                            .TableCell(tcAlign, iFila, 6) = taRightMiddle

                            .TableCell(tcText, iFila, 7) = FormatNumber(Monto, oCliente.Decimales)
                            .TableCell(tcAlign, iFila, 7) = taRightMiddle

                        Case "RF"
                            .TableCell(tcText, iFila, 4) = FormatNumber(Porcentaje, 2) & "%"
                            .TableCell(tcAlign, iFila, 4) = taRightMiddle

                            .TableCell(tcText, iFila, 9) = "TOTAL"
                            .TableCell(tcAlign, iFila, 9) = taRightMiddle

                            .TableCell(tcText, iFila, 10) = FormatNumber(Monto, oCliente.Decimales)
                            .TableCell(tcAlign, iFila, 10) = taRightMiddle

                    End Select

                    .TableCell(tcBackColor, iFila) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                    .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)   ' Blanco
                    .EndTable
                    
                    .Paragraph = ""
                    
                    iLineasImpresas = iLineasImpresas + .LineSpacing
                    
                End If
                
            'End If

            ' anterior = lReg("id_arbol_clase_inst").Value
        Next                ' For Each lReg In lCursor_Ins

        Set lCursor_Ins = Nothing
        Set lReg = Nothing

    End With

End Sub

Private Sub PoneEncabezados(vp, lCodProducto)
    With vp
        .StartTable
        .TableBorder = tbBox
        .TableBorder = tbBoxRows
        
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcFontSize) = glb_tamletra_registros
    
        .TableCell(tcText, 1, 2) = ""
        .TableCell(tcText, 1, 4) = ""

        Select Case lCodProducto
            Case "RV"
                .TableCell(tcCols) = 8
                .TableCell(tcText, 1, 1) = "NEMOTECNICO"
                .TableCell(tcText, 1, 2) = "MONEDA"
                .TableCell(tcText, 1, 3) = "% DE INVERSION"
                .TableCell(tcText, 1, 4) = "CANTIDAD"
                .TableCell(tcText, 1, 5) = "PRECIO PROMEDIO" & vbCrLf & "COMPRA"
                .TableCell(tcText, 1, 6) = "PRECIO" & vbCrLf & "ACTUAL"
                .TableCell(tcText, 1, 7) = "VALOR" & vbCrLf & "MERCADO"
                .TableCell(tcText, 1, 8) = "RENTABILIDAD"
                
                .TableCell(tcColWidth, 1, 1) = "70mm": .TableCell(tcAlign, 1, 1) = taCenterMiddle
                .TableCell(tcColWidth, 1, 2) = "20mm": .TableCell(tcAlign, 1, 2) = taCenterMiddle
                .TableCell(tcColWidth, 1, 3) = "25mm": .TableCell(tcAlign, 1, 3) = taCenterMiddle
                .TableCell(tcColWidth, 1, 4) = "25mm": .TableCell(tcAlign, 1, 4) = taCenterMiddle
                .TableCell(tcColWidth, 1, 5) = "30mm": .TableCell(tcAlign, 1, 5) = taCenterMiddle
                .TableCell(tcColWidth, 1, 6) = "25mm": .TableCell(tcAlign, 1, 6) = taCenterMiddle
                .TableCell(tcColWidth, 1, 7) = "35mm": .TableCell(tcAlign, 1, 7) = taCenterMiddle
                .TableCell(tcColWidth, 1, 8) = "25mm": .TableCell(tcAlign, 1, 8) = taCenterMiddle
                
            Case "RF"
                .TableCell(tcCols) = 10
                .TableCell(tcText, 1, 1) = "NEMOTECNICO"
                .TableCell(tcText, 1, 2) = "EMISOR"
                .TableCell(tcText, 1, 3) = "MONEDA"
                .TableCell(tcText, 1, 4) = "% DE " & vbCrLf & "INVERSION"
                .TableCell(tcText, 1, 5) = "" & vbCrLf & "NOMINALES"
                .TableCell(tcText, 1, 6) = "TASA" & vbCrLf & "CUP�N"
                .TableCell(tcText, 1, 7) = "FECHA" & vbCrLf & "VCTO."
                .TableCell(tcText, 1, 8) = "TASA" & vbCrLf & "COMPRA"
                .TableCell(tcText, 1, 9) = "TASA" & vbCrLf & "MERCADO"
                .TableCell(tcText, 1, 10) = "VALORIZACI�N"
                
                .TableCell(tcColWidth, 1, 1) = "40mm": .TableCell(tcAlign, 1, 1) = taCenterMiddle
                .TableCell(tcColWidth, 1, 2) = "25mm": .TableCell(tcAlign, 1, 2) = taCenterMiddle
                .TableCell(tcColWidth, 1, 3) = "22mm": .TableCell(tcAlign, 1, 3) = taCenterMiddle
                .TableCell(tcColWidth, 1, 4) = "20mm": .TableCell(tcAlign, 1, 4) = taCenterMiddle
                .TableCell(tcColWidth, 1, 5) = "25mm": .TableCell(tcAlign, 1, 5) = taCenterMiddle
                .TableCell(tcColWidth, 1, 6) = "18mm": .TableCell(tcAlign, 1, 6) = taCenterMiddle
                .TableCell(tcColWidth, 1, 7) = "25mm": .TableCell(tcAlign, 1, 7) = taCenterMiddle
                .TableCell(tcColWidth, 1, 8) = "25mm": .TableCell(tcAlign, 1, 8) = taCenterMiddle
                .TableCell(tcColWidth, 1, 9) = "25mm": .TableCell(tcAlign, 1, 9) = taCenterMiddle
                .TableCell(tcColWidth, 1, 10) = "30mm": .TableCell(tcAlign, 1, 10) = taCenterMiddle
                
        
            Case "FFMM"
                .TableCell(tcCols) = 8
                .TableCell(tcText, 1, 1) = "DESCRIPCI�N"
                .TableCell(tcText, 1, 2) = "MONEDA"
                .TableCell(tcText, 1, 3) = "% DE INVERSION"
                .TableCell(tcText, 1, 4) = "CANTIDAD DE" & vbCrLf & "CUOTAS"
                .TableCell(tcText, 1, 5) = "VALOR CUOTA" & vbCrLf & "PROMEDIO COMPRA"
                .TableCell(tcText, 1, 6) = "VALOR CUOTA" & vbCrLf & "ACTUAL"
                .TableCell(tcText, 1, 7) = "VALOR ACTUAL " ' & vbCrLf & "MON. CUENTA"
                .TableCell(tcText, 1, 8) = "RENTABILIDAD"
                
                .TableCell(tcColWidth, 1, 1) = "70mm": .TableCell(tcAlign, 1, 1) = taCenterMiddle
                .TableCell(tcColWidth, 1, 2) = "20mm": .TableCell(tcAlign, 1, 2) = taCenterMiddle
                .TableCell(tcColWidth, 1, 3) = "25mm": .TableCell(tcAlign, 1, 3) = taCenterMiddle
                .TableCell(tcColWidth, 1, 4) = "25mm": .TableCell(tcAlign, 1, 4) = taCenterMiddle
                .TableCell(tcColWidth, 1, 5) = "30mm": .TableCell(tcAlign, 1, 5) = taCenterMiddle
                .TableCell(tcColWidth, 1, 6) = "25mm": .TableCell(tcAlign, 1, 6) = taCenterMiddle
                .TableCell(tcColWidth, 1, 7) = "35mm": .TableCell(tcAlign, 1, 7) = taCenterMiddle
                .TableCell(tcColWidth, 1, 8) = "25mm": .TableCell(tcAlign, 1, 8) = taCenterMiddle

        End Select
        
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcBackColor, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                           
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)  ' Blanco
        .TableCell(tcForeColor, 2) = RGB(255, 255, 255)  ' Blanco
        
        
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        
        iLineasImpresas = iLineasImpresas + (.LineSpacing * .TableCell(tcRows))

    End With
End Sub


Public Function PadC(ByVal sSrc As String, ByVal iLen As Integer, Optional ByVal sChr As String = " ") As String
    Dim iLeft As Integer
    Dim iRight As Integer
    
    If iLen > Len(sSrc) Then
        If iLen Mod 2 = 0 Then
            'This is an even length output string...
            iLeft = (iLen - Len(sSrc)) \ 2
        Else
            'This is an odd length output string...
            iLeft = ((iLen + 1) - Len(sSrc)) \ 2
        End If
        iRight = iLen - (iLeft + Len(sSrc))

        PadC = String(iLeft, sChr) & sSrc & String(iRight, sChr)
    Else
        PadC = sSrc
    End If

End Function

Public Function PadR(ByVal sSrc As String, ByVal iLen As Integer, Optional ByVal sChr As String = " ") As String
    Dim x As Integer
    x = iLen - Len(sSrc)
    If x > 0 Then
        PadR = sSrc & String(x, sChr)
    Else
        PadR = sSrc
    End If
End Function

Public Function PadL(ByVal sSrc As String, ByVal iLen As Integer, Optional ByVal sChr As String = " ") As String
    Dim x As Integer
    x = iLen - Len(sSrc)
    If x > 0 Then
        PadL = String(x, sChr) & sSrc
    Else
        PadL = sSrc
    End If
End Function

Private Sub Sub_Carga_Grilla()
    Dim lCursor         As Object
    Dim lReg            As Object

    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    If oCliente.TipoConsolidado = CONSOLIDADO_POR_CLIENTE Then
        gDB.Parametros.Add "pId_Cliente", ePT_Numero, oCliente.IdCliente, ePD_Entrada
    Else
        gDB.Parametros.Add "PId_Grupo_Cuenta", ePT_Numero, oCliente.IdGrupo, ePD_Entrada
    End If
    gDB.Procedimiento = "PKG_CARTOLA$BuscarCuentas_xClte_xGrupoCta"
    
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If
    
    Set lCursor = gDB.Parametros("Pcursor").Valor
    Grilla_Cuentas.Rows = 1
    
    Dim lLinea
    For Each lReg In lCursor
        lLinea = Grilla_Cuentas.Rows
        Call Grilla_Cuentas.AddItem("")
        Call SetCell(Grilla_Cuentas, lLinea, "colum_pk", Trim(lReg("id_cuenta").Value), pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "num_cuenta", lReg("num_cuenta").Value, pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "Rut_cliente", lReg("rut_cliente").Value, pAutoSize:=True)
        Call SetCell(Grilla_Cuentas, lLinea, "razon_social", lReg("nombre_cliente").Value, pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "moneda", lReg("Simbolo_Moneda").Value, pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "fecha_cierre", NVL(lReg("Fecha_Cierre_Cuenta").Value, ""), pAutoSize:=False)
    Next
End Sub



