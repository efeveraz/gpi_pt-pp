VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_AsignaActivosANemotecnico 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asignaci�n de Clases de Activos a Nemot�cnicos"
   ClientHeight    =   8115
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14175
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3030
   ScaleMode       =   0  'User
   ScaleWidth      =   14175
   Begin MSComctlLib.ListView Lista 
      Height          =   6975
      Left            =   4320
      TabIndex        =   0
      Top             =   720
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   12303
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nemot�cnicos"
         Object.Width           =   6262
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "ISIM"
         Object.Width           =   2716
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "CUSIP"
         Object.Width           =   2716
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Instrumento"
         Object.Width           =   5186
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Producto"
         Object.Width           =   5186
      EndProperty
   End
   Begin MSComctlLib.TreeView Arbol 
      Height          =   6975
      Left            =   60
      TabIndex        =   1
      Top             =   720
      Width           =   3555
      _ExtentX        =   6271
      _ExtentY        =   12303
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   176
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      FullRowSelect   =   -1  'True
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   14175
      _ExtentX        =   25003
      _ExtentY        =   635
      ButtonWidth     =   2170
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Grabar"
            Key             =   "SAVE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Graba los cambios realizados"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   3690
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin MSComctlLib.Toolbar Toolbar_Transfiere 
      Height          =   660
      Left            =   3840
      TabIndex        =   4
      Top             =   2040
      Width           =   450
      _ExtentX        =   794
      _ExtentY        =   1164
      ButtonWidth     =   1138
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ADD"
            Description     =   "Agregar un nemotecnico a la Operaci�n"
            Object.ToolTipText     =   "Selecciona todos los Perfiles de Riesgo"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DEL"
            Object.ToolTipText     =   "Selecciona ninguno de los Perfiles de Riesgo"
         EndProperty
      EndProperty
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   255
         Left            =   9420
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin MSComctlLib.Toolbar Toolbar_Nemotecnicos 
      Height          =   660
      Left            =   13680
      TabIndex        =   9
      Top             =   720
      Width           =   450
      _ExtentX        =   794
      _ExtentY        =   1164
      ButtonWidth     =   1138
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ALL"
            Description     =   "Agregar un nemotecnico a la Operaci�n"
            Object.ToolTipText     =   "Selecciona todos los Perfiles de Riesgo"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "NALL"
            Object.ToolTipText     =   "Selecciona ninguno de los Perfiles de Riesgo"
         EndProperty
      EndProperty
      Begin MSComctlLib.ProgressBar ProgressBar2 
         Height          =   255
         Left            =   9420
         TabIndex        =   10
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Clases de Activos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   60
      TabIndex        =   8
      Top             =   390
      Width           =   5835
   End
   Begin VB.Label Lbl_Ubicaci�n 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   60
      TabIndex        =   7
      Top             =   7770
      Width           =   13635
   End
   Begin VB.Label Label2 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Nemot�cnicos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   6600
      TabIndex        =   6
      Top             =   390
      Width           =   3525
   End
End
Attribute VB_Name = "Frm_AsignaActivosANemotecnico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana
Const cPrefijo = "K"
Const cRoot = "ROOT"
Dim fNemotecnicos As hRecord

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Arbol_NodeClick(ByVal Node As MSComctlLib.Node)
Dim lcNodo       As Node
Dim lCodigo      As String

  If Not Arbol.SelectedItem Is Nothing Then
    Set lcNodo = Arbol.SelectedItem
    lCodigo = lcNodo.Key
  End If
End Sub




'Private Sub Cmb_Asig_Anemo_Click()
'Call Sub_CargarDatos

'End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long
 

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SAVE").Image = cBoton_Grabar
    .Buttons("EXIT").Image = cBoton_Salir
    .Buttons("REFRESH").Image = cBoton_Original
  End With
  
  With Toolbar_Transfiere
    Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Hacia_Izquierda
      .Buttons("DEL").Image = cBoton_Hacia_Derecha
  End With
  
  
  With Toolbar_Nemotecnicos
    Set .ImageList = MDI_Principal.ImageListGlobal16
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("ALL").Image = "boton_seleccionar_todos"
    .Buttons("NALL").Image = "boton_seleccionar_ninguno"
  End With
  
  Set fNemotecnicos = New hRecord
  With fNemotecnicos
    Call .AddField("ID")
    Call .AddField("ID_NEMOTECNICO")
    Call .AddField("NEMOTECNICO")
    Call .AddField("ISIM")
    Call .AddField("CUSIP")
    Call .AddField("INSTRUMENTO")
    Call .AddField("producto")
  End With
  
  Call Sub_CargarDatos
  
  Toolbar_Transfiere.Buttons("DEL").Enabled = False
  
  Me.Top = 1
  Me.Left = 1
End Sub
Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Call Sub_Grabar
      Call Sub_CargarDatos
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Sub Toolbar_Nemotecnicos_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lItem As ListItem
  
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ALL"
      For Each lItem In Lista.ListItems
        lItem.Checked = True
      Next
    Case "NALL"
      Call Sub_Limpiar
  End Select
End Sub

Private Sub Toolbar_Transfiere_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_AgregarNodo
      Toolbar_Transfiere.Buttons("DEL").Enabled = True
    Case "DEL"
      Call Sub_EliminarNodo
  End Select
End Sub

Private Sub Sub_Limpiar()
Dim lItem As ListItem
  
  For Each lItem In Lista.ListItems
    lItem.Checked = False
  Next
End Sub
Private Sub Sub_CargarDatos()
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_CargaArbolActivos
    Call Sub_CargaNemosDisponibles
    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Sub_CargaArbolActivos()
  
Dim lcArbol_Clase_Instrumento As Class_Arbol_Clase_Instrumento
Dim lReg                      As hFields
'---------------------------------------------------------------
Dim lRelacion
Dim lPadre
Dim lNode As Node
Dim lID_PADRE_ARBOL_CLASE_INST

  
  lID_PADRE_ARBOL_CLASE_INST = Null
  If Not Arbol.SelectedItem Is Nothing Then
    lID_PADRE_ARBOL_CLASE_INST = Arbol.SelectedItem
  End If
  
  'Limpia los que se quieren elimiar
  'Call fhBorrar.LimpiarRegistros
  
  Arbol.Nodes.Clear
  Set lNode = Arbol.Nodes.Add(Key:=cRoot _
                            , Text:="Raiz")
  lNode.Expanded = True
  
  'Set Arbol.ImageList = MDI_Principal.ImageListGlobal16
  
  Rem Carga el Arbol de clasificaciones de instrumentos
  Set lcArbol_Clase_Instrumento = New Class_Arbol_Clase_Instrumento
  lcArbol_Clase_Instrumento.Campo("id_empresa").Valor = Fnt_EmpresaActual
  '---- Se Agrega ACI_tipo para diferenciar los arboles 12/12/2018 Ralvarez
  'Dim ase_tipo As Integer
  'If Cmb_Asig_Anemo.ListIndex = 0 Then ase_tipo = 1
  'If Cmb_Asig_Anemo.ListIndex = 1 Then ase_tipo = 2
  '   lcArbol_Clase_Instrumento.Campo("id_aci_tipo").Valor = 1
 '--------------------------------------------------------------------------
  
  If Not lcArbol_Clase_Instrumento.Buscar Then
    Call Fnt_MsgError(lcArbol_Clase_Instrumento.SubTipo_LOG _
                    , "Problemas al buscar." _
                    , lcArbol_Clase_Instrumento.ErrMsg _
                    , pConLog:=True)
    GoTo ExitProcedure
  End If
  
  
  
  For Each lReg In lcArbol_Clase_Instrumento.Cursor
    If IsNull(lReg("ID_PADRE_ARBOL_CLASE_INST").Value) Then
      lPadre = cRoot
    Else
      lPadre = cPrefijo & lReg("ID_PADRE_ARBOL_CLASE_INST").Value
    End If
    
    Set lNode = Arbol.Nodes.Add(relative:=lPadre _
                             , relationship:=tvwChild _
                             , Key:=cPrefijo & lReg("ID_ARBOL_CLASE_INST").Value _
                             , Text:=lReg("DSC_ARBOL_CLASE_INST").Value)
    lNode.Expanded = True
    'lNode.Bold = True
    'lNode.Image = "close_folder"
  Next
  Set lcArbol_Clase_Instrumento = Nothing
  
  If Not IsNull(lID_PADRE_ARBOL_CLASE_INST) Then
    On Error Resume Next
    Set Arbol.SelectedItem = Arbol.Nodes(lID_PADRE_ARBOL_CLASE_INST)
  End If
  
ExitProcedure:
  Set lcArbol_Clase_Instrumento = Nothing
End Sub

Private Sub Sub_CargaNemosDisponibles()
Dim lNode As Node
Dim lLinea As Long
Dim lReg As hFields
Dim lReg2 As hFields
Dim lInstrumentos As Class_Instrumentos
Dim lcNemotecnico As Class_Nemotecnicos
Dim lcNemoDisponibles As Class_Nemotecnicos
Dim Tipo_f As Integer
    Lista.ListItems.Clear
    Set lReg = Nothing
    Rem Carga los nemosdisponibles
    Set lcNemoDisponibles = New Class_Nemotecnicos
    With lcNemoDisponibles
         Tipo_f = 1
      If .BuscarDisponibles(Tipo_f) Then
        lLinea = 1
        For Each lReg In .Cursor
'          With Lista.ListItems.Add(, "P" & lReg("ID_NEMOTECNICO").Value, lReg("NEMOTECNICO").Value)
'            .Tag = lReg("ID_NEMOTECNICO").Value
'          End With
          Call Lista.ListItems.Add(, "P" & lReg("ID_NEMOTECNICO").Value, lReg("NEMOTECNICO").Value)
          Lista.ListItems.Item(lLinea).Tag = lReg("ID_NEMOTECNICO").Value
          Lista.ListItems.Item(lLinea).SubItems(1) = IIf(IsNull(lReg("ISIM").Value), "", Trim(lReg("ISIM").Value))
          Lista.ListItems.Item(lLinea).SubItems(2) = IIf(IsNull(lReg("CUSIP").Value), "", Trim(lReg("CUSIP").Value))
          Lista.ListItems.Item(lLinea).SubItems(3) = IIf(IsNull(lReg("INSTRUMENTO").Value), "", Trim(lReg("INSTRUMENTO").Value))
          Lista.ListItems.Item(lLinea).SubItems(4) = IIf(IsNull(lReg("producto").Value), "", Trim(lReg("producto").Value))
          lLinea = lLinea + 1
        Next
      Else
        MsgBox .ErrMsg, vbCritical, Me.Caption
      End If
    End With
    Set lcNemoDisponibles = Nothing
    
    'Set Arbol.SelectedItem = Arbol.Nodes(1)
    
End Sub
Private Sub Sub_AgregarNodo()
Dim lcNodo_Select As Node
Dim lCodigo       As String
Dim lId_Arbol     As Integer
Dim lItem         As ListItem
Dim Linea         As Long
    
    If Arbol.SelectedItem Is Nothing Then
      MsgBox "Debe seleccionar Clase Activo", vbInformation
      Exit Sub
    End If
  
    For Each lcNodo_Select In Arbol.Nodes
      If (lcNodo_Select.Selected) And (lcNodo_Select.Children > 0) Then
        MsgBox "No se puede asignar el Nemot�cnico a un Producto.", vbInformation
        Exit Sub
      End If
    Next
    
    Call Sub_Bloquea_Puntero(Me)
  
    Call fNemotecnicos.LimpiarRegistros
    With Arbol
      Set lcNodo_Select = .SelectedItem
      lId_Arbol = Mid(lcNodo_Select.Key, 2)
      Linea = 1
      For Each lItem In Lista.ListItems
          If lItem.Checked Then
              Set .SelectedItem = .Nodes.Add(relative:=lcNodo_Select _
                                       , relationship:=tvwChild _
                                       , Key:="I" & lItem.Tag _
                                       , Text:=lItem.Text)

            Else
               Sub_RegistraNemo lItem.Tag, Trim(lItem.Text), Lista.ListItems.Item(Linea).SubItems(1), Lista.ListItems.Item(Linea).SubItems(2), Lista.ListItems.Item(Linea).SubItems(3), Lista.ListItems.Item(Linea).SubItems(4)
          End If
          Linea = Linea + 1
      Next
    End With
    
    Call Sub_ActualizaListaNemos
    
    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Sub_RegistraNemo(pId_Nemo As Long, pNemo As String, pISIM As String, _
                             pCUSIP As String, pInstrumento As String, pProducto As String)
Dim lReg As hFields
Dim lReg_Nemo As hFields
Dim lNemo_Encontrado As Boolean
Dim indice As Integer

    Set lReg_Nemo = fNemotecnicos.Add
    lReg_Nemo("id_nemotecnico").Value = pId_Nemo
    lReg_Nemo("nemotecnico").Value = pNemo
    lReg_Nemo("ISIM").Value = pISIM
    lReg_Nemo("CUSIP").Value = pCUSIP
    lReg_Nemo("INSTRUMENTO").Value = pInstrumento
    lReg_Nemo("producto").Value = pProducto
  
End Sub

Private Sub Sub_ActualizaListaNemos()
Dim lReg As hFields
Dim lLinea As Long
    Lista.ListItems.Clear
    lLinea = 1
    For Each lReg In fNemotecnicos
        Call Lista.ListItems.Add(, "P" & lReg("ID_NEMOTECNICO").Value, lReg("NEMOTECNICO").Value)
        Lista.ListItems.Item(lLinea).Tag = lReg("ID_NEMOTECNICO").Value
        Lista.ListItems.Item(lLinea).SubItems(1) = IIf(IsNull(lReg("ISIM").Value), "", Trim(lReg("ISIM").Value))
        Lista.ListItems.Item(lLinea).SubItems(2) = IIf(IsNull(lReg("CUSIP").Value), "", Trim(lReg("CUSIP").Value))
        Lista.ListItems.Item(lLinea).SubItems(3) = IIf(IsNull(lReg("INSTRUMENTO").Value), "", Trim(lReg("INSTRUMENTO").Value))
        Lista.ListItems.Item(lLinea).SubItems(4) = IIf(IsNull(lReg("producto").Value), "", Trim(lReg("producto").Value))
        lLinea = lLinea + 1
    Next
End Sub
Private Sub Sub_EliminarNodo()
Dim lcNodo As Node
Dim lhBorrar As hRecord
Dim lReg As hFields


  If Arbol.SelectedItem Is Nothing Then
    Exit Sub
  End If
  Call Sub_Bloquea_Puntero(Me)
  Set lcNodo = Arbol.SelectedItem
   
  Select Case lcNodo.Key
    Case cRoot
        Exit Sub
    Case Else
        If Mid(lcNodo.Key, 1, 1) = "I" Then
          Set lhBorrar = New hRecord
          Call lhBorrar.AddField("ID")
          Call lhBorrar.AddField("NEMO")
          
          If Fnt_EliminaNodoRecursivo(pcNodo:=lcNodo _
                                    , phBorrar:=lhBorrar) Then
             Call Arbol.Nodes.Remove(lcNodo.Index)
            
          End If
          Set lhBorrar = Nothing
        Else
          MsgBox "Clase Activo no puede ser eliminada", vbInformation
        End If
  End Select
  Call Sub_ActualizaListaNemos
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function Fnt_EliminaNodoRecursivo(ByRef pcNodo As Node, ByRef phBorrar As hRecord, Optional pBorrarHijos As VbMsgBoxResult = vbNo) As Boolean
Dim lResult As Boolean
Dim lcNodo  As Node
Dim lCursor As hRecord
Dim lReg    As hFields

Dim Id_Nemotecnico As String
Dim Nemotecnico    As String
Dim ISIM           As String
Dim CUSIP          As String
Dim instrumento    As String
Dim Producto       As String
    
  Fnt_EliminaNodoRecursivo = False
  
  Set lcNodo = pcNodo.Child
  
  Do While Not lcNodo Is Nothing
    If pBorrarHijos = vbNo Then
      pBorrarHijos = MsgBox("La clase """ & pcNodo.Text & """ contiene clases hijas." & vbCr & vbCr & "�Desea eliminarla de todas formas?", vbYesNo + vbQuestion, Me.Caption)
    End If
  
    If pBorrarHijos = vbYes Then
      If Not Fnt_EliminaNodoRecursivo(pcNodo:=lcNodo _
                                    , phBorrar:=phBorrar _
                                    , pBorrarHijos:=pBorrarHijos) Then
        GoTo ExitProcedure
      End If
    Else
      GoTo ExitProcedure
    End If
    
    Set lcNodo = lcNodo.Next
  Loop
  
  If Not pcNodo.Key = "" Then
     Set lReg = Nothing
     Set lCursor = Nothing
     Id_Nemotecnico = Mid(pcNodo.Key, 2)
     If RetornaDatos(Id_Nemotecnico, lCursor) Then
         For Each lReg In lCursor
            ISIM = IIf(IsNull(lReg("ISIM").Value), "", Trim(lReg("ISIM").Value))
            CUSIP = IIf(IsNull(lReg("CUSIP").Value), "", Trim(lReg("CUSIP").Value))
            instrumento = IIf(IsNull(lReg("INSTRUMENTO").Value), "", Trim(lReg("INSTRUMENTO").Value))
            Producto = IIf(IsNull(lReg("producto").Value), "", Trim(lReg("producto").Value))
         Next
     End If
     Sub_RegistraNemo Mid(pcNodo.Key, 2), Trim(pcNodo.Text), ISIM, CUSIP, instrumento, Producto
     Set lCursor = Nothing
  End If
  
  Fnt_EliminaNodoRecursivo = True
  
ExitProcedure:

End Function

Private Sub Sub_Grabar()
Dim lReg As hFields
Dim lcNodo As Node
Dim lId_Arbol As String
Dim lcNodoHijo As Node
Dim lResult As Boolean

    Call Sub_Bloquea_Puntero(Me)
  
    Rem Inicia la Transaccion
    gDB.IniciarTransaccion
      
    lResult = True
  
    For Each lcNodo In Arbol.Nodes
        If Not lcNodo.Parent Is Nothing Then
            If Mid(lcNodo.Key, 1, 1) = "I" Then
                lId_Arbol = Mid(lcNodo.Parent.Key, 2)
                lResult = Fnt_GrabaNemos(Mid(lcNodo.Key, 2), lId_Arbol)
                If Not lResult Then
                    GoTo ErrProcedure
                End If
            End If
        End If
    Next

ErrProcedure:
  If Err Then
    lResult = False
    Resume
  End If
  
  If lResult Then
    gDB.CommitTransaccion
    MsgBox "Proceso de grabaci�n de asignaci�n de clases activos a nemot�cnicos exitosa", vbInformation
  Else
    MsgBox "Error en proceso de grabaci�n de asignaci�n de clases activos a nemot�cnicos", vbInformation
    gDB.RollbackTransaccion
  End If
  
  Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Function Fnt_GrabaNemos(ByRef pId_Nemotecnico, ByVal pId_Arbol As Integer) As Boolean
Dim lcRel_ACI_Emp_Nemotecnico As Class_Rel_ACI_Emp_Nemotecnico
Dim lResult As Boolean

    lResult = True
    Set lcRel_ACI_Emp_Nemotecnico = New Class_Rel_ACI_Emp_Nemotecnico

    With lcRel_ACI_Emp_Nemotecnico
        .Campo("id_nemotecnico").Valor = pId_Nemotecnico
        .Campo("id_empresa").Valor = Fnt_EmpresaActual
        .Campo("ID_ARBOL_CLASE_INST").Valor = pId_Arbol
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG _
                          , "Problemas al guardar la relaci�n de la clase con el nemot�cnico." _
                          , .ErrMsg _
                          , pConLog:=True)
            lResult = False
          GoTo ExitProcedure
        End If
    End With
ExitProcedure:
    Set lcRel_ACI_Emp_Nemotecnico = Nothing
    Fnt_GrabaNemos = lResult
End Function

Private Function RetornaDatos(pId_Nemotecnico, Cursor As hRecord) As Boolean
Dim lCursor   As hRecord
Dim lReg2     As hFields

RetornaDatos = False

  With gDB
    .Parametros.Clear
    .Procedimiento = "PKG_NEMOTECNICOS$BuscarDisponibles"
    .Parametros.Add "pID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    .Parametros.Add "pID_NEMOTECNICO", ePT_Numero, pId_Nemotecnico, ePD_Entrada
    .Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    
    If .EjecutaSP Then
      Set Cursor = .Parametros("Pcursor").Valor
      RetornaDatos = True
    Else
      RetornaDatos = False
    End If
  End With
                                               
End Function
