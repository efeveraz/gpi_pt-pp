VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Comisiones_Cuentas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Comisiones Cuentas"
   ClientHeight    =   3630
   ClientLeft      =   45
   ClientTop       =   540
   ClientWidth     =   7110
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3630
   ScaleWidth      =   7110
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   12
      Tag             =   "OBLI"
      Top             =   0
      Width           =   7110
      _ExtentX        =   12541
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   13
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin C1SizerLibCtl.C1Tab SSTab 
      Height          =   3105
      Left            =   60
      TabIndex        =   11
      Top             =   420
      Width           =   6990
      _cx             =   12330
      _cy             =   5477
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   6579300
      ForeColor       =   -2147483634
      FrontTabColor   =   -2147483635
      BackTabColor    =   6579300
      TabOutlineColor =   -2147483632
      FrontTabForeColor=   -2147483634
      Caption         =   "&General|&Historico"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   6
      Position        =   0
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   0   'False
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   -1  'True
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   0
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   -1  'True
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   2760
         Left            =   15
         ScaleHeight     =   2760
         ScaleWidth      =   6960
         TabIndex        =   16
         Top             =   330
         Width           =   6960
         Begin VB.Frame Frame1 
            Height          =   1665
            Left            =   60
            TabIndex        =   19
            Top             =   1020
            Width           =   6855
            Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
               Height          =   330
               Left            =   4590
               TabIndex        =   6
               Top             =   1155
               Visible         =   0   'False
               Width           =   2130
               _ExtentX        =   3757
               _ExtentY        =   582
               _Version        =   393216
               Enabled         =   0   'False
               CheckBox        =   -1  'True
               CustomFormat    =   "d/MM/yyyy"
               Format          =   59703299
               CurrentDate     =   39167.5069560185
            End
            Begin MSComCtl2.DTPicker DTP_Fecha_Ini 
               Height          =   330
               Left            =   1470
               TabIndex        =   5
               Tag             =   "OBLI"
               Top             =   1170
               Width           =   1530
               _ExtentX        =   2699
               _ExtentY        =   582
               _Version        =   393216
               CustomFormat    =   "d/MM/yyyy"
               Format          =   59703299
               CurrentDate     =   38810
            End
            Begin hControl2.hTextLabel Txt_Comision_honorarios 
               Height          =   315
               Left            =   150
               TabIndex        =   1
               Tag             =   "OBLI"
               ToolTipText     =   "Comisi�n de Honorarios"
               Top             =   240
               Width           =   2820
               _ExtentX        =   4974
               _ExtentY        =   556
               LabelWidth      =   1300
               Caption         =   "Comision ADC"
               Text            =   "0"
               Text            =   "0"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "###,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Comision_Asesorias 
               Height          =   315
               Left            =   150
               TabIndex        =   2
               Tag             =   "OBLI"
               ToolTipText     =   "Comisi�n de Asesor�as"
               Top             =   630
               Visible         =   0   'False
               Width           =   2820
               _ExtentX        =   4974
               _ExtentY        =   556
               LabelWidth      =   1300
               Enabled         =   0   'False
               Caption         =   "Com. Asesor�as"
               Text            =   "0"
               Text            =   "0"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "###,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin TrueDBList80.TDBCombo Cmb_Moneda 
               Height          =   345
               Left            =   4590
               TabIndex        =   3
               Tag             =   "OBLI=S;CAPTION=Moneda"
               Top             =   240
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   0   'False
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Comisiones_Cuentas.frx":0000
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_Periodicidad 
               Height          =   345
               Left            =   4590
               TabIndex        =   4
               Tag             =   "OBLI=S;CAPTION=Periodicidad"
               Top             =   615
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   0   'False
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Comisiones_Cuentas.frx":00AA
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin VB.Label Label5 
               Caption         =   "%"
               Enabled         =   0   'False
               Height          =   195
               Left            =   3000
               TabIndex        =   25
               Top             =   690
               Visible         =   0   'False
               Width           =   225
            End
            Begin VB.Label Label3 
               Caption         =   "%"
               Height          =   195
               Left            =   3000
               TabIndex        =   24
               Top             =   300
               Width           =   225
            End
            Begin VB.Label Label8 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Moneda"
               Height          =   345
               Left            =   3270
               TabIndex        =   23
               Top             =   240
               Width           =   1305
            End
            Begin VB.Label Label2 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Periodicidad"
               Height          =   345
               Left            =   3270
               TabIndex        =   22
               Top             =   615
               Width           =   1305
            End
            Begin VB.Label Label1 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha Inicio"
               Height          =   330
               Left            =   150
               TabIndex        =   21
               Top             =   1170
               Width           =   1305
            End
            Begin VB.Label Label4 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha T�rmino"
               Enabled         =   0   'False
               Height          =   330
               Left            =   3270
               TabIndex        =   20
               Top             =   1155
               UseMnemonic     =   0   'False
               Visible         =   0   'False
               Width           =   1305
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Datos de la Cuenta"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   825
            Left            =   60
            TabIndex        =   17
            Top             =   90
            Width           =   6855
            Begin VB.CommandButton cmb_buscar 
               Caption         =   "?"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Left            =   3045
               Picture         =   "Frm_Comisiones_Cuentas.frx":0154
               TabIndex        =   26
               Top             =   315
               Width           =   375
            End
            Begin hControl2.hTextLabel Txt_Instrumento 
               Height          =   315
               Left            =   3495
               TabIndex        =   0
               Tag             =   "OBLI"
               ToolTipText     =   "Cuenta"
               Top             =   330
               Width           =   3105
               _ExtentX        =   5477
               _ExtentY        =   556
               LabelWidth      =   1300
               Caption         =   "Instrumento"
               Text            =   "0"
               Text            =   "0"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
            End
            Begin TrueDBList80.TDBCombo Cmb_Instrumento 
               Height          =   345
               Left            =   4815
               TabIndex        =   7
               Tag             =   "OBLI=S;CAPTION=Cuenta"
               Top             =   315
               Width           =   1785
               _ExtentX        =   3149
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   0   'False
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Comisiones_Cuentas.frx":045E
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin hControl2.hTextLabel Txt_Num_Cuenta 
               Height          =   345
               Left            =   75
               TabIndex        =   27
               Tag             =   "OBLI=S;CAPTION=Cuenta"
               Top             =   315
               Width           =   2925
               _ExtentX        =   5159
               _ExtentY        =   609
               LabelWidth      =   1245
               TextMinWidth    =   1200
               Caption         =   "N� de Cuenta"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               MaxLength       =   100
            End
            Begin hControl2.hTextLabel Txt_Cuenta 
               Height          =   315
               Left            =   120
               TabIndex        =   28
               Tag             =   "OBLI"
               ToolTipText     =   "Cuenta"
               Top             =   330
               Width           =   2820
               _ExtentX        =   4974
               _ExtentY        =   556
               LabelWidth      =   1300
               Caption         =   "Cuenta"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Tipo_TextBox    =   1
            End
            Begin VB.Label Lbl_Instrumento 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Instrumento"
               Height          =   330
               Left            =   3495
               TabIndex        =   18
               Top             =   345
               Width           =   1305
            End
         End
      End
      Begin VB.PictureBox Picture4 
         BorderStyle     =   0  'None
         Height          =   2760
         Left            =   7605
         ScaleHeight     =   2760
         ScaleWidth      =   6960
         TabIndex        =   15
         Top             =   330
         Width           =   6960
         Begin VB.Frame Frame3 
            Height          =   2655
            Left            =   90
            TabIndex        =   14
            Top             =   30
            Width           =   6795
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Comisiones 
               Height          =   1905
               Left            =   120
               TabIndex        =   8
               Top             =   660
               Width           =   6540
               _cx             =   11536
               _cy             =   3360
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   5
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Comisiones_Cuentas.frx":0508
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
            Begin hControl2.hTextLabel Txt_Cuenta_Historial 
               Height          =   315
               Left            =   120
               TabIndex        =   9
               Tag             =   "OBLI"
               ToolTipText     =   "Cuenta"
               Top             =   210
               Width           =   2985
               _ExtentX        =   5265
               _ExtentY        =   556
               LabelWidth      =   1300
               Caption         =   "Cuenta"
               Text            =   ""
               Locked          =   -1  'True
               BackColorTxt    =   12648447
               BackColorTxt    =   12648447
               Tipo_TextBox    =   1
            End
            Begin hControl2.hTextLabel Txt_Instrumento_Historial 
               Height          =   315
               Left            =   3165
               TabIndex        =   10
               Tag             =   "OBLI"
               ToolTipText     =   "Cuenta"
               Top             =   225
               Width           =   3465
               _ExtentX        =   6112
               _ExtentY        =   556
               LabelWidth      =   1300
               Caption         =   "Instrumento"
               Text            =   ""
               Locked          =   -1  'True
               BackColorTxt    =   12648447
               BackColorTxt    =   12648447
               Tipo_TextBox    =   1
            End
         End
      End
   End
End
Attribute VB_Name = "Frm_Comisiones_Cuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const Const_Moneda = "Pesos"
Const Const_Periocidad = "Anual"

Public fKey As String
Public lId_Cuenta As Integer
Public lNum_Cuenta As String
Public lAbr_Cuenta As String
Public lDesc_Cuenta As String
Public fCod_Instrumento As String
Public fGrilla As VSFlexGrid

Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana

Private Sub cmb_buscar_Click()
lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)
If lId_Cuenta <> 0 Then
    Txt_Num_Cuenta.Text = ""
    Txt_Num_Cuenta.Tag = lId_Cuenta
    Call Lpr_Buscar_Cuenta("", Str(lId_Cuenta))
End If
End Sub

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pColum_PK, pCod_Arbol_Sistema, pGrilla As VSFlexGrid)
  fKey = pColum_PK
  Set fGrilla = pGrilla
'-----------------------------------------------------------------------
'--Setea los controles dependiendo de los permisos
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
'-----------------------------------------------------------------------
  
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de Comisiones de Adm. de Cartera"
    Txt_Num_Cuenta.Text = Frm_Man_Comisiones_Cuentas.Cmb_Cuentas.Text
    Txt_Num_Cuenta.Tag = Fnt_ComboSelected_KEY(Frm_Man_Comisiones_Cuentas.Cmb_Cuentas)
    lId_Cuenta = IIf(Txt_Num_Cuenta.Tag = "", 0, Trim(Txt_Num_Cuenta.Tag))
    lDesc_Cuenta = Txt_Num_Cuenta.Text
    Call Sub_CargarHistorial
  Else
    Me.Caption = "Modificaci�n de Comisiones de Adm. de Cartera"
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
 
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub



Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Rem Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lComisionesCuenta As Class_ComisionesCuentas
Dim lresult As Boolean
Dim lCod_Instrumento As String

    Call Sub_Bloquea_Puntero(Me)
    
    Fnt_Grabar = True
    
    If Not Fnt_ValidarDatos Then
        Fnt_Grabar = False
        GoTo ErrProcedure
    End If
  
    
    lresult = True
    If fKey = cNewEntidad Then
        lCod_Instrumento = Cmb_Instrumento.Text
        If Fnt_ExisteComision(lCod_Instrumento) Then
            If MsgBox("La comisi�n ya existe para este instrumento. Desea continuar?" _
                , vbYesNo + vbQuestion _
                , Me.Caption) = vbYes Then
                DTP_Fecha_Ter.Value = DTP_Fecha_Ini.Value
                lresult = True
            Else
                lresult = False
            End If
        Else
            lresult = True
        End If
    End If
  
    If lresult Then
        Set lComisionesCuenta = New Class_ComisionesCuentas
        With lComisionesCuenta
          .Campo("Id_Comision_Cuenta").Valor = fKey
          If fKey = cNewEntidad Then
            .Campo("Id_Cuenta").Valor = Txt_Num_Cuenta.Tag
            .Campo("cod_Instrumento").Valor = Fnt_ComboSelected_KEY(Cmb_Instrumento)
          Else
            .Campo("Id_Cuenta").Valor = lId_Cuenta
            .Campo("cod_Instrumento").Valor = fCod_Instrumento ' Trim(Txt_Instrumento.Text)
          End If
          .Campo("fecha_Ini").Valor = Format(DTP_Fecha_Ini.Value, cFormatDate)
          .Campo("fecha_Ter").Valor = Format(DTP_Fecha_Ter.Value, cFormatDate)
          .Campo("comision_Honorarios").Valor = (Txt_Comision_honorarios.Text / 100)
          .Campo("comision_Asesorias").Valor = (Txt_Comision_Asesorias.Text / 100)
          .Campo("Id_Moneda").Valor = Fnt_ComboSelected_KEY(Cmb_Moneda)
          .Campo("PERIODICIDAD").Valor = Fnt_ComboSelected_KEY(Cmb_Periodicidad)
          If Not .Guardar Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en grabar el Comisiones Cuentas.", _
                              .ErrMsg, _
                              pConLog:=True)
            Fnt_Grabar = False
          End If
        End With
    End If
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Function

Private Sub Sub_CargaForm()
Dim lReg As hFields

  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_FormControl_Color(Me.Controls)

  Txt_Cuenta_Historial.BackColorTxt = fColorNoEdit
  Txt_Instrumento_Historial.BackColorTxt = fColorNoEdit
  
  DTP_Fecha_Ini.Value = Format(Fnt_FechaServidor, cFormatDate)
  DTP_Fecha_Ter.Value = Format(Fnt_FechaServidor, cFormatDate)
  
  Call Sub_CargaCombo_Comi_Fija_Periodicidad(Cmb_Periodicidad)
  Call Sub_CargaCombo_Monedas(Cmb_Moneda, pFlg_Es_Moneda_Pago:="S")
  Call Sub_ComboSelectedItem(Cmb_Moneda, Buscar_Id_Moneda(cMoneda_Cod_Peso))
  
  Call Sub_ComboSelectedItem(Cmb_Periodicidad, 365)
  
  
  'Call Sub_ComboSelectedItem(Cmb_Periodicidad, Fnt_BuscaKey_In_TextCombo(Cmb_Periodicidad, cPeriocidad_Cod_Anual))
  'Call Sub_CargaCombo_Monedas(Cmb_Moneda, pFlg_Es_Moneda_Pago:="S")
  
  If fKey = cNewEntidad Then
    Me.Caption = "Agregar Comisiones de Asesor�a y Honorarios"
    Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento, "")
    Txt_Cuenta.Visible = False
    Txt_Instrumento.Visible = False
    Lbl_Instrumento.Visible = True
    Cmb_Instrumento.Visible = True
    Cmb_Instrumento.Tag = "OBLI"
  Else
    Me.Caption = "Modificaci�n Comisiones de Asesor�as y Honorarios"
    Txt_Cuenta.Locked = True
    Txt_Cuenta.Visible = True
    Txt_Instrumento.Visible = True
    Txt_Cuenta.BackColorTxt = fColorNoEdit
    Txt_Instrumento.BackColorTxt = fColorNoEdit
    Lbl_Instrumento.Visible = False
    Cmb_Instrumento.Tag = ""
    Cmb_Instrumento.Visible = False
    Call Sub_CargarDatos 'RDG 19/03/2010 Se descomenta esta linea, por alguna razon estada comentada y no traia los datos desde la DB
    Call Sub_CargarHistorial
  End If
  SSTab.CurrTab = 0

  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
'======= SE REFERENCIA CLASE COMISIONES CUENTA ============
Dim lComisionesCuenta As Class_ComisionesCuentas
  
  Call Sub_Bloquea_Puntero(Me)
  
  '======== SE INSTANCIA CLASE COMISIONES CUENTA ===========
  Set lComisionesCuenta = New Class_ComisionesCuentas
  With lComisionesCuenta
    .Campo("id_comision_cuenta").Valor = fKey
    '=== BUSCA DATOS DE LAS COMISIONES X CUENTA =======
    If .Buscar(True) Then
      For Each lReg In .Cursor
        lId_Cuenta = "" & lReg("id_cuenta").Value
        lNum_Cuenta = "" & lReg("num_cuenta").Value
        lAbr_Cuenta = "" & lReg("abr_cuenta").Value
        lDesc_Cuenta = lNum_Cuenta & " - " & lAbr_Cuenta
        Txt_Cuenta.Text = lDesc_Cuenta
        Txt_Num_Cuenta.Tag = "" & lReg("id_cuenta").Value
        Txt_Num_Cuenta.Text = lNum_Cuenta & " - " & lAbr_Cuenta
        DTP_Fecha_Ini.Value = lReg("Fecha_Ini").Value
        DTP_Fecha_Ter.Value = lReg("Fecha_Ter").Value
        Txt_Comision_honorarios.Text = NVL(lReg("Comision_honorarios").Value, 0) * 100
        Txt_Comision_Asesorias.Text = NVL(lReg("Comision_asesorias").Value, 0) * 100
        
        ' Txt_Instrumento.Text = "" & lReg("cod_instrumento").Value
        fCod_Instrumento = Trim("" & lReg("cod_instrumento").Value)
        Txt_Instrumento.Text = Trim("" & lReg("DSC_INTRUMENTO").Value)
        
        
        
        'Call Sub_ComboSelectedItem(Cmb_Moneda, Const_Moneda)
        'Call Sub_ComboSelectedItem(Cmb_Periodicidad, Const_Periocidad)
        ' RDG 19/03/2010 Se asigna seleccion de campos moneda y periodicidad con valores desde la base de datos al modificar
        Call Sub_ComboSelectedItem(Cmb_Moneda, lReg("id_moneda")) 'Const_Moneda)
        Call Sub_ComboSelectedItem(Cmb_Periodicidad, lReg("periodicidad")) 'Const_Periocidad)
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Comisiones Cuentas.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function Fnt_ValidarDatos() As Boolean
Dim lValido As Boolean
Dim lFecha_Ini As String
Dim lFecha_Ter As String
Dim lResultado As Integer
Dim lMensaje As String
'======= SE REFERENCIA CLASE COMISIONES CUENTA ============
Dim lComisionesCuenta As Class_ComisionesCuentas
  
  Call Sub_Bloquea_Puntero(Me)
  
  lValido = Fnt_Form_Validar(Me.Controls)
'  If lValido Then
'    If DTP_Fecha_Ter.Value < DTP_Fecha_Ini.Value Then
'      lValido = False
'      MsgBox "Fecha T�rmino debe ser posterior a fecha de inicio.", vbExclamation, Me.Caption
'    End If
'  End If
  
  If lValido Then
    lFecha_Ini = Format(DTP_Fecha_Ini.Value, cFormatDate)
    lFecha_Ter = DLL_COMUN.NVL(Format(DTP_Fecha_Ter.Value, cFormatDate), "")
    '======== SE INSTANCIA CLASE COMISIONES CUENTA ===========
    Set lComisionesCuenta = New Class_ComisionesCuentas
    With lComisionesCuenta
      .Campo("Id_Comision_Cuenta").Valor = fKey
      .Campo("Id_Cuenta").Valor = lId_Cuenta
      
      ' .Campo("cod_instrumento").Valor = Txt_Instrumento.Text
      .Campo("cod_instrumento").Valor = fCod_Instrumento
      
      .Campo("Fecha_Ini").Valor = lFecha_Ini
      .Campo("Fecha_Ter").Valor = lFecha_Ter
    .Campo("Id_Moneda").Valor = Fnt_ComboSelected_KEY(Cmb_Moneda)
      '=== BUSCA DATOS DE LAS COMISIONES X CUENTA =======
      If Not .ValidaComisionCuenta(lResultado, lMensaje) Then
        Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en validar informaci�n.", _
                        .ErrMsg, _
                        pConLog:=True)
        lValido = False
      Else
        If lResultado = 1 Then
          lValido = True
        Else
          lValido = False
          'MsgBox "Error en la informaci�n ingresada." & vbCr & vbCr & lMensaje, vbCritical, Me.Caption
          Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en la informaci�n ingresada.", _
                        lMensaje, _
                        pConLog:=True)
        End If
      End If
    End With
  End If
  Fnt_ValidarDatos = lValido
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Function

Private Sub Sub_CargarHistorial()
Dim lReg As hCollection.hFields
Dim lLinea As Long
'======= SE REFERENCIA CLASE COMISIONES CUENTA ============
Dim lComisionesCuenta As Class_ComisionesCuentas

  Txt_Cuenta_Historial.Text = lDesc_Cuenta
  Txt_Instrumento_Historial.Text = Txt_Instrumento.Text
  
  Grilla_Comisiones.Rows = 1
  
  '======== SE INSTANCIA CLASE COMISIONES CUENTA ===========
  Set lComisionesCuenta = New Class_ComisionesCuentas
  With lComisionesCuenta
    .Campo("Id_Cuenta").Valor = lId_Cuenta
    .Campo("cod_instrumento").Valor = Trim(fCod_Instrumento)
    '=== BUSCA DATOS DE LAS COMISIONES X CUENTA =======
    If .Buscar Then
      For Each lReg In .Cursor
          lLinea = Grilla_Comisiones.Rows
          Call Grilla_Comisiones.AddItem("")
          
          Call SetCell(Grilla_Comisiones, lLinea, "Fecha_Ini", NVL(lReg("Fecha_ini").Value, ""))
          Call SetCell(Grilla_Comisiones, lLinea, "Fecha_Ter", NVL(lReg("Fecha_ter").Value, ""))
          Call SetCell(Grilla_Comisiones, lLinea, "comision_honorarios", NVL(lReg("comision_honorarios").Value, 0) * 100)
          Call SetCell(Grilla_Comisiones, lLinea, "comision_asesorias", NVL(lReg("comision_asesorias").Value, 0) * 100)
        Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar el historial.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
End Sub

Public Function Fnt_ExisteComision(pDscInstrumento) As Boolean
Dim fila As Long
Dim lexiste As Boolean

    lexiste = False
    For fila = 1 To fGrilla.Rows - 1
        If GetCell(fGrilla, fila, "cod_instrumento") = pDscInstrumento Then
            lexiste = True
            Exit For
        End If
    Next
    Fnt_ExisteComision = lexiste
End Function

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call Lpr_Buscar_Cuenta("TXT")
    End If
End Sub

Private Sub Lpr_Buscar_Cuenta(ByVal Origen As String, Optional p_Id_Cuenta As String)
Dim lcCuenta      As Object

    If Origen = "TXT" Then
        If InStr(1, Txt_Num_Cuenta.Text, "-") > 0 Then
           Txt_Num_Cuenta.Text = Trim(Left(Txt_Num_Cuenta.Text, InStr(1, Txt_Num_Cuenta.Text, "-") - 1))
        End If
        p_Id_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text), 0)
    End If
    If p_Id_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = p_Id_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                    Txt_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                    lDesc_Cuenta = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                    lId_Cuenta = .Cursor(1)("id_cuenta").Value
                    Call Sub_CargarHistorial
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
            Set lcCuenta = Nothing
    End If
End Sub



