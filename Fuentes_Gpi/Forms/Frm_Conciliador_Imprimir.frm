VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Conciliador_Imprimir 
   Appearance      =   0  'Flat
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Imprimir Conciliador"
   ClientHeight    =   2550
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3195
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2550
   ScaleWidth      =   3195
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2025
      Left            =   75
      TabIndex        =   5
      Top             =   450
      Width           =   3045
      Begin VB.CheckBox Chk_SoloContraparte 
         Caption         =   "Solo Contraparte"
         Height          =   195
         Left            =   390
         TabIndex        =   2
         Top             =   1020
         Value           =   1  'Checked
         Width           =   1725
      End
      Begin VB.CheckBox Chk_SoloBPI 
         Caption         =   "Solo CSGPI"
         Height          =   195
         Left            =   390
         TabIndex        =   1
         Top             =   690
         Value           =   1  'Checked
         Width           =   1725
      End
      Begin VB.CheckBox Chk_Diferencias 
         Caption         =   "Diferencias"
         Height          =   195
         Left            =   390
         TabIndex        =   0
         Top             =   390
         Value           =   1  'Checked
         Width           =   1725
      End
      Begin MSComctlLib.Toolbar Toolbar_Imprimir 
         Height          =   330
         Left            =   1035
         TabIndex        =   6
         Top             =   1470
         Width           =   1050
         _ExtentX        =   1852
         _ExtentY        =   582
         ButtonWidth     =   1720
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Imprimir"
               Key             =   "PRINT"
               Description     =   "Valoriza el nemotécnico a la tasa de inversión."
               Object.ToolTipText     =   "Genera el Reporte de Instruccones"
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   3195
      _ExtentX        =   5636
      _ExtentY        =   635
      ButtonWidth     =   1429
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   4
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Conciliador_Imprimir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fImprimir As Boolean

Private Sub Form_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    Call Sub_Imprimir
  End If
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
  End With

  With Toolbar_Imprimir
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("PRINT").Image = cBoton_Imprimir
  End With


  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Mostrar(ByRef pRep_Diferencias _
                      , ByRef pRep_SoloBPI _
                      , ByRef pRep_SoloContraparte _
                      ) As Boolean
  Call Form_Resize
  
  fImprimir = False
  
  Me.Top = 1
  Me.Left = 1
  Me.Show vbModal
  
  pRep_Diferencias = (Chk_Diferencias.Value = vbChecked)
  pRep_SoloBPI = (Chk_SoloBPI.Value = vbChecked)
  pRep_SoloContraparte = (Chk_SoloContraparte.Value = vbChecked)
  
  
  Mostrar = fImprimir
  
  Unload Me
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "EXIT"
      Unload Me
  End Select
End Sub


Private Sub Sub_CargaForm()
  Call Sub_FormControl_Color(Me.Controls)
End Sub

Private Sub Toolbar_Imprimir_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "PRINT"
      Call Sub_Imprimir
  End Select
End Sub


Private Sub Sub_Imprimir()
  If Chk_Diferencias.Value = vbUnchecked And _
     Chk_SoloBPI.Value = vbUnchecked And _
     Chk_SoloContraparte.Value = vbUnchecked Then
    Call MsgBox("Debe seleccionar por lo menos un reporte para imprimir", vbExclamation, Me.Caption)
    Exit Sub
  End If
  
  fImprimir = True
  Me.Hide
End Sub
