VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_ImportaOperacionesBBVA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Importaci�n de Movimientos Diarios"
   ClientHeight    =   9330
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   13155
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9330
   ScaleWidth      =   13155
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   13155
      _ExtentX        =   23204
      _ExtentY        =   635
      ButtonWidth     =   1429
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   11040
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin TabDlg.SSTab Tab_Operaciones 
      Height          =   8865
      Left            =   0
      TabIndex        =   2
      Top             =   420
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   15637
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "Importaci�n Renta Variable"
      TabPicture(0)   =   "Frm_ImportaOperacionesBBVA.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame4"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame_3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Importaci�n Renta Fija"
      TabPicture(1)   =   "Frm_ImportaOperacionesBBVA.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(1)=   "Frame2"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Importaci�n Fondos Mutuos"
      TabPicture(2)   =   "Frm_ImportaOperacionesBBVA.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame3"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "Frame5"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).ControlCount=   2
      Begin VB.Frame Frame5 
         Caption         =   "Operaciones Con Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   5175
         Left            =   -74910
         TabIndex        =   10
         Top             =   330
         Width           =   12915
         Begin VB.Frame Frame7 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   705
            Left            =   90
            TabIndex        =   23
            Top             =   600
            Width           =   12735
            Begin VB.CommandButton Cmb_BuscaFile_FM 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   9330
               TabIndex        =   24
               ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
               Top             =   240
               Width           =   345
            End
            Begin MSComCtl2.DTPicker DTP_Fecha_Proceso_FM 
               Height          =   315
               Left            =   1470
               TabIndex        =   25
               Top             =   240
               Width           =   1365
               _ExtentX        =   2408
               _ExtentY        =   556
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Format          =   66584577
               CurrentDate     =   37732
            End
            Begin hControl2.hTextLabel Txt_ArchivoPlano_FM 
               Height          =   315
               Left            =   2880
               TabIndex        =   26
               Tag             =   "OBLI"
               Top             =   240
               Width           =   6360
               _ExtentX        =   11218
               _ExtentY        =   556
               LabelWidth      =   1200
               TextMinWidth    =   1200
               Caption         =   "Archivo Plano"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
            End
            Begin VB.Label Label2 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha Proceso"
               Height          =   345
               Left            =   210
               TabIndex        =   27
               Top             =   240
               Width           =   1215
            End
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_FM 
            CausesValidation=   0   'False
            Height          =   3675
            Left            =   90
            TabIndex        =   11
            Top             =   1380
            Width           =   12165
            _cx             =   21458
            _cy             =   6482
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   0
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   17
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesBBVA.frx":0054
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Operaciones_FM 
            Height          =   330
            Left            =   90
            TabIndex        =   12
            Top             =   270
            Width           =   4935
            _ExtentX        =   8705
            _ExtentY        =   582
            ButtonWidth     =   2011
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Wrappable       =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "SAVE"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Importar"
                  Key             =   "IMPORT"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Excel"
                  Key             =   "EXCEL"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Limpiar"
                  Key             =   "REFRESH"
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar Toolbar_Selc_Con_Cta_FM 
            Height          =   660
            Left            =   12360
            TabIndex        =   40
            Top             =   1650
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar5 
               Height          =   255
               Left            =   9420
               TabIndex        =   41
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Operaciones SIN Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   3255
         Left            =   -74910
         TabIndex        =   9
         Top             =   5490
         Width           =   12915
         Begin MSComctlLib.Toolbar Toolbar_Selc_Sin_Cta_FM 
            Height          =   660
            Left            =   12360
            TabIndex        =   42
            Top             =   510
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar6 
               Height          =   255
               Left            =   9420
               TabIndex        =   43
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_Sin_Cuenta_FM 
            CausesValidation=   0   'False
            Height          =   2925
            Left            =   90
            TabIndex        =   44
            Top             =   240
            Width           =   12165
            _cx             =   21458
            _cy             =   5159
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   0
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   17
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesBBVA.frx":0328
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Operaciones Con Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   5175
         Left            =   -74910
         TabIndex        =   7
         Top             =   330
         Width           =   12915
         Begin VB.Frame Frame6 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   705
            Left            =   90
            TabIndex        =   18
            Top             =   600
            Width           =   12735
            Begin VB.CommandButton Cmb_BuscaFile_RF 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   9330
               TabIndex        =   19
               ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
               Top             =   240
               Width           =   345
            End
            Begin MSComCtl2.DTPicker DTP_Fecha_Proceso_RF 
               Height          =   315
               Left            =   1470
               TabIndex        =   20
               Top             =   240
               Width           =   1365
               _ExtentX        =   2408
               _ExtentY        =   556
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Format          =   66584577
               CurrentDate     =   37732
            End
            Begin hControl2.hTextLabel Txt_ArchivoPlano_RF 
               Height          =   315
               Left            =   2880
               TabIndex        =   21
               Tag             =   "OBLI"
               Top             =   240
               Width           =   6360
               _ExtentX        =   11218
               _ExtentY        =   556
               LabelWidth      =   1200
               TextMinWidth    =   1200
               Caption         =   "Archivo Plano"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
            End
            Begin VB.Label Label1 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha Proceso"
               Height          =   345
               Left            =   210
               TabIndex        =   22
               Top             =   240
               Width           =   1215
            End
         End
         Begin MSComctlLib.Toolbar Toolbar_Operaciones_RF 
            Height          =   330
            Left            =   90
            TabIndex        =   8
            Top             =   270
            Width           =   4935
            _ExtentX        =   8705
            _ExtentY        =   582
            ButtonWidth     =   2011
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Wrappable       =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "SAVE"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Importar"
                  Key             =   "IMPORT"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Excel"
                  Key             =   "EXCEL"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Limpiar"
                  Key             =   "REFRESH"
               EndProperty
            EndProperty
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_RF 
            CausesValidation=   0   'False
            Height          =   3675
            Left            =   90
            TabIndex        =   30
            Top             =   1380
            Width           =   12165
            _cx             =   21458
            _cy             =   6482
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   26
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesBBVA.frx":05FC
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Selc_Con_Cta_RF 
            Height          =   660
            Left            =   12360
            TabIndex        =   36
            Top             =   1650
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar3 
               Height          =   255
               Left            =   9420
               TabIndex        =   37
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Operaciones SIN Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   3255
         Left            =   -74910
         TabIndex        =   6
         Top             =   5490
         Width           =   12915
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_Sin_Cuenta_RF 
            CausesValidation=   0   'False
            Height          =   2925
            Left            =   90
            TabIndex        =   31
            Top             =   240
            Width           =   12165
            _cx             =   21458
            _cy             =   5159
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   26
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesBBVA.frx":0A91
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Selc_Sin_Cta_RF 
            Height          =   660
            Left            =   12360
            TabIndex        =   38
            Top             =   510
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar4 
               Height          =   255
               Left            =   9420
               TabIndex        =   39
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
      Begin VB.Frame Frame_3 
         Caption         =   "Operaciones Con Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   5175
         Left            =   90
         TabIndex        =   4
         Top             =   330
         Width           =   12915
         Begin VB.Frame Frm_Fecha_Proceso 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   705
            Left            =   90
            TabIndex        =   13
            Top             =   600
            Width           =   12735
            Begin VB.CommandButton Cmb_BuscaFile_RV 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   9330
               TabIndex        =   16
               ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
               Top             =   240
               Width           =   345
            End
            Begin MSComCtl2.DTPicker DTP_Fecha_Proceso_RV 
               Height          =   315
               Left            =   1470
               TabIndex        =   14
               Top             =   240
               Width           =   1365
               _ExtentX        =   2408
               _ExtentY        =   556
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Format          =   66584577
               CurrentDate     =   37732
            End
            Begin hControl2.hTextLabel Txt_ArchivoPlano_RV 
               Height          =   315
               Left            =   2880
               TabIndex        =   17
               Tag             =   "OBLI"
               Top             =   240
               Width           =   6360
               _ExtentX        =   11218
               _ExtentY        =   556
               LabelWidth      =   1200
               TextMinWidth    =   1200
               Caption         =   "Archivo Plano"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
            End
            Begin VB.Label Lbl_Fecha_Proceso 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha Proceso"
               Height          =   345
               Left            =   210
               TabIndex        =   15
               Top             =   240
               Width           =   1215
            End
         End
         Begin MSComctlLib.Toolbar Toolbar_Operaciones_RV 
            Height          =   330
            Left            =   90
            TabIndex        =   5
            Top             =   270
            Width           =   4935
            _ExtentX        =   8705
            _ExtentY        =   582
            ButtonWidth     =   2011
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Wrappable       =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "SAVE"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Importar"
                  Key             =   "IMPORT"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Excel"
                  Key             =   "EXCEL"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Limpiar"
                  Key             =   "REFRESH"
               EndProperty
            EndProperty
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_RV 
            CausesValidation=   0   'False
            Height          =   3675
            Left            =   90
            TabIndex        =   28
            Top             =   1380
            Width           =   12165
            _cx             =   21458
            _cy             =   6482
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   0
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   21
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesBBVA.frx":0F23
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Selc_Con_Cta 
            Height          =   660
            Left            =   12360
            TabIndex        =   32
            Top             =   1650
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar1 
               Height          =   255
               Left            =   9420
               TabIndex        =   33
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Operaciones SIN Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   3255
         Left            =   90
         TabIndex        =   3
         Top             =   5490
         Width           =   12915
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_Sin_Cuenta_RV 
            CausesValidation=   0   'False
            Height          =   2925
            Left            =   90
            TabIndex        =   29
            Top             =   240
            Width           =   12165
            _cx             =   21458
            _cy             =   5159
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   21
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesBBVA.frx":12CD
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Selc_Sin_Cta 
            Height          =   660
            Left            =   12360
            TabIndex        =   34
            Top             =   510
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar2 
               Height          =   255
               Left            =   9420
               TabIndex        =   35
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
   End
End
Attribute VB_Name = "Frm_ImportaOperacionesBBVA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim lcAlias As Object

Dim fKey_RV As String
Dim fKey_RF As String
Dim fKey_FM As String

Dim fMov_Fact_RV As Object
Dim fMov_Fact_RF As Object
Dim fMov_Fact_FM As Object

Const cTipo_Ingreso = "I"
Const cTipo_Rescate = "R"

Public Function Fnt_Mostrar(pCod_Proceso_Componente_RV As String, _
                            pCod_Proceso_Componente_RF As String, _
                            pCod_Proceso_Componente_FM As String)
  Fnt_Mostrar = False

  fKey_RV = pCod_Proceso_Componente_RV
  fKey_RF = pCod_Proceso_Componente_RF
  fKey_FM = pCod_Proceso_Componente_FM
  
  If Not Fnt_CargarDatos Then
    Unload Me
    Exit Function
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
  
  Fnt_Mostrar = True
End Function

Private Function Fnt_CargarDatos() As Boolean
Dim lcProceso_Componente  As Class_Proceso_Componente
Dim lMensaje              As String

On Error GoTo ErrProcedure

  Call Sub_Bloquea_Puntero(Me)
  
  Load Me
  Fnt_CargarDatos = False
  
  Set fMov_Fact_RV = Nothing
  Set fMov_Fact_RF = Nothing
  Set fMov_Fact_FM = Nothing
  
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = fKey_RV
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set fMov_Fact_RV = .IniciaClass(lMensaje)
    
    If fMov_Fact_RV Is Nothing Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                      , lMensaje _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  Set lcProceso_Componente = Nothing
  
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = fKey_RF
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set fMov_Fact_RF = .IniciaClass(lMensaje)
    
    If fMov_Fact_RF Is Nothing Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                      , lMensaje _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  Set lcProceso_Componente = Nothing
  
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = fKey_FM
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set fMov_Fact_FM = .IniciaClass(lMensaje)
    
    If fMov_Fact_FM Is Nothing Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                      , lMensaje _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  Set lcProceso_Componente = Nothing
  
  Fnt_CargarDatos = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la carga de un Proceso Componente." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Function

Private Sub Cmb_BuscaFile_FM_Click()
  If fMov_Fact_FM.Fnt_Busca_Archivo_Plano Then
    Txt_ArchivoPlano_FM.Text = fMov_Fact_FM.fArchivo
  End If
End Sub

Private Sub Cmb_BuscaFile_RF_Click()
  If fMov_Fact_RF.Fnt_Busca_Archivo_Plano Then
    Txt_ArchivoPlano_RF.Text = fMov_Fact_RF.fArchivo
  End If
End Sub

Private Sub Cmb_BuscaFile_RV_Click()
  If fMov_Fact_RV.Fnt_Busca_Archivo_Plano Then
    Txt_ArchivoPlano_RV.Text = fMov_Fact_RV.fArchivo
  End If
End Sub

Private Function Fnt_Nombre_Archivo(pTexto As hTextLabel) As String
Dim lPos_Letra As Long

  For lPos_Letra = Len(pTexto.Text) To 1 Step -1
    If Mid(pTexto.Text, lPos_Letra, 1) = "\" Then
      Fnt_Nombre_Archivo = Mid(pTexto.Text, lPos_Letra + 1, Len(pTexto.Text))
      Exit For
    End If
  Next
  
End Function

Private Sub Form_Load()
Dim lReg  As hCollection.hFields

  Call PubPrdObtienePosicion(Me)
    
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Operaciones_RV
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("IMPORT").Image = cBoton_Aceptar
      .Buttons("EXCEL").Image = cBoton_Excel
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("SAVE").Enabled = False
      .Buttons("EXCEL").Enabled = False
      .Buttons("REFRESH").Enabled = False
      .Appearance = ccFlat
  End With
  
  With Toolbar_Operaciones_RF
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("IMPORT").Image = cBoton_Aceptar
      .Buttons("EXCEL").Image = cBoton_Excel
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("SAVE").Enabled = False
      .Buttons("EXCEL").Enabled = False
      .Buttons("REFRESH").Enabled = False
      .Appearance = ccFlat
  End With
  
  With Toolbar_Operaciones_FM
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("IMPORT").Image = cBoton_Aceptar
      .Buttons("EXCEL").Image = cBoton_Excel
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("SAVE").Enabled = False
      .Buttons("EXCEL").Enabled = False
      .Buttons("REFRESH").Enabled = False
      .Appearance = ccFlat
  End With
  
  With Toolbar_Selc_Con_Cta
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With
  
  With Toolbar_Selc_Sin_Cta
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With
  
  With Toolbar_Selc_Con_Cta_RF
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With
  
  With Toolbar_Selc_Sin_Cta_RF
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With
  
  With Toolbar_Selc_Con_Cta_FM
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With
  
  With Toolbar_Selc_Sin_Cta_FM
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With
  
  Call Sub_Limpia_Objetos
  Tab_Operaciones.Tab = 0
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call PubPrdGrabaPosicion(Me)
End Sub

Private Sub Grilla_Operaciones_FM_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_FM.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_FM_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_FM)
End Sub

Private Sub Grilla_Operaciones_FM_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_FM)
End Sub

Private Sub Grilla_Operaciones_FM_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_FM)
End Sub

Private Sub Grilla_Operaciones_RF_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_RF.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_RF_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_RF)
End Sub

Private Sub Grilla_Operaciones_RF_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_RF)
End Sub

Private Sub Grilla_Operaciones_RF_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_RF)
End Sub

Private Sub Grilla_Operaciones_RV_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_RV.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_RV_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_RV)
End Sub

Private Sub Grilla_Operaciones_RV_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_RV)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_Sin_Cuenta_FM)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_Sin_Cuenta_RF.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_Sin_Cuenta_RF)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_Sin_Cuenta_RF)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_Sin_Cuenta_RF)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_Sin_Cuenta_RV.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_Sin_Cuenta_RV)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_Sin_Cuenta_RV)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_Sin_Cuenta_RV)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Limpia_Objetos()

  Txt_ArchivoPlano_RV.Text = ""
  Txt_ArchivoPlano_RF.Text = ""
  Txt_ArchivoPlano_FM.Text = ""
  
  DTP_Fecha_Proceso_RV.Value = Fnt_FechaServidor
  DTP_Fecha_Proceso_RF.Value = Fnt_FechaServidor
  DTP_Fecha_Proceso_FM.Value = Fnt_FechaServidor
  
  Grilla_Operaciones_RV.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_RV.Rows = 1
  Grilla_Operaciones_RF.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_RF.Rows = 1
  Grilla_Operaciones_FM.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_FM.Rows = 1
End Sub

Private Function Fnt_Importa_RV() As Boolean
Dim lCampo As hFields
Dim lLinea As Long
Dim lTipo_Movimiento As String
Dim lCod_Instrumento As String
Dim lId_Nemotecnico As String
Dim lId_Cuenta
Dim lAlias_Cta As String
Dim lLinea_Oper_sin_cta As Long
Dim lNum_Cuenta As String
Dim lLinea_Oper_con_cta As Long
'----------------------------------
Dim lcNemotecnicos As Class_Nemotecnicos
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
  
  Fnt_Importa_RV = True
  Call Sub_Bloquea_Puntero(Me)
  
  Grilla_Operaciones_RV.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_RV.Rows = 1
  
  With fMov_Fact_RV
    If Not .Carga_Facturacion_RV Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
    
    If .Cursor.Count = 0 Then
      MsgBox "No hay datos en el Archivo Seleccionado.", vbInformation, Me.Caption
      GoTo ErrProcedure
    End If
    
    For Each lCampo In .Cursor
      lTipo_Movimiento = ""
      If lCampo("TIPO_MOVIMIENTO").Value = gcTipoOperacion_Ingreso Then
        lTipo_Movimiento = "Ingreso"
      ElseIf lCampo("TIPO_MOVIMIENTO").Value = gcTipoOperacion_Egreso Then
        lTipo_Movimiento = "Egreso"
      End If
      '------- RESCATA EL ID_NEMOTECNICO
      Set lcNemotecnicos = New Class_Nemotecnicos
      lcNemotecnicos.Campo("nemotecnico").Valor = lCampo("NEMOTECNICO").Value
      lCod_Instrumento = ""
      Rem Busca el id_nemotecnico del Nemotecnico de la planilla
      If lcNemotecnicos.Buscar_Nemotecnico(pCod_Producto:=Null, pMostrar_Msg:=False) Then
        lId_Nemotecnico = lcNemotecnicos.Campo("id_nemotecnico").Valor
        If Not lId_Nemotecnico = "0" Then
          'Rem Busca atributos del Nemotecnico
          If lcNemotecnicos.Buscar Then
            lCod_Instrumento = NVL(lcNemotecnicos.Cursor(1)("cod_instrumento").Value, "")
          Else
          End If
        End If
      Else
      End If
      Set lcNemotecnicos = Nothing
      '------- FIN RESCATA EL ID_NEMOTECNICO
      Rem Busca el Alias de la Cuenta por el "Rut Cliente" concatenado con la "Cuenta"
      lAlias_Cta = NVL(lCampo("RUT_CLIENTE").Value, "") & "/" & NVL(lCampo("CUENTA").Value, "")
      Set lcAlias = CreateObject(cDLL_Alias)
      Set lcAlias.gDB = gDB
      lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_BAC_Fondos_BBVA _
                                           , pCodigoCSBPI:=cTabla_Cuentas _
                                           , pValor:=lAlias_Cta)
      If IsNull(lId_Cuenta) Then
          lLinea_Oper_sin_cta = Grilla_Operaciones_Sin_Cuenta_RV.Rows
          Call Grilla_Operaciones_Sin_Cuenta_RV.AddItem("")
          Grilla_Operaciones_Sin_Cuenta_RV.Cell(flexcpChecked, lLinea_Oper_sin_cta, "chk") = flexChecked
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "colum_pk", NVL(lCampo("NRO_ORDEN").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "fecha", NVL(lCampo("FECHA").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "rut", Trim(lCampo("RUT_CLIENTE").Value))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "id_cuenta", "")
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "num_cuenta", "")
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "tipo_movimiento", lTipo_Movimiento)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "flg_tipo_movimiento", lCampo("TIPO_MOVIMIENTO").Value)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "id_nemotecnico", lId_Nemotecnico)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "nemotecnico", lCampo("NEMOTECNICO").Value)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "Cantidad", lCampo("CANTIDAD").Value)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "precio", lCampo("PRECIO").Value)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "comi", NVL(lCampo("COMISION").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "Dere", NVL(lCampo("DERECHO").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "gast", NVL(lCampo("GASTOS").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "iva", NVL(lCampo("IVA").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "monto_neto", NVL(lCampo("MONTO_NETO").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "fecha_liquidacion", NVL(lCampo("FECHA_LIQUIDACION").Value, ""))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "cod_instru", lCod_Instrumento)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "dsc_error", "")
      Else
          ' ----------- RESCATA NUMERO CUENTA
'          Set lcCuenta = New Class_Cuentas
          Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
          lcCuenta.Campo("id_cuenta").Valor = lId_Cuenta
          If lcCuenta.Buscar Then
              lNum_Cuenta = lcCuenta.Cursor(1)("num_cuenta").Value
          End If
          Set lcCuenta = Nothing
          ' ----------- FIN RESCATA NUMERO CUENTA
          lLinea_Oper_con_cta = Grilla_Operaciones_RV.Rows
          Call Grilla_Operaciones_RV.AddItem("")
          Grilla_Operaciones_RV.Cell(flexcpChecked, lLinea_Oper_con_cta, "chk") = flexChecked
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "colum_pk", NVL(lCampo("NRO_ORDEN").Value, 0))
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "fecha", NVL(lCampo("FECHA").Value, 0))
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "rut", Trim(lCampo("RUT_CLIENTE").Value))
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "id_cuenta", lId_Cuenta)
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "num_cuenta", lNum_Cuenta)
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "tipo_movimiento", lTipo_Movimiento)
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "flg_tipo_movimiento", lCampo("TIPO_MOVIMIENTO").Value)
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "id_nemotecnico", lId_Nemotecnico)
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "nemotecnico", lCampo("NEMOTECNICO").Value)
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "Cantidad", lCampo("CANTIDAD").Value)
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "precio", lCampo("PRECIO").Value)
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "comi", NVL(lCampo("COMISION").Value, 0))
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "Dere", NVL(lCampo("DERECHO").Value, 0))
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "gast", NVL(lCampo("GASTOS").Value, 0))
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "iva", NVL(lCampo("IVA").Value, 0))
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "monto_neto", NVL(lCampo("MONTO_NETO").Value, 0))
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "fecha_liquidacion", lCampo("FECHA_LIQUIDACION").Value)
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "cod_instru", lCod_Instrumento)
          Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "dsc_error", "")
      End If
      
    Next
  End With
  
  Call Sub_Desbloquea_Puntero(Me)
  Exit Function
  
ErrProcedure:
  Fnt_Importa_RV = False
  Call Sub_Desbloquea_Puntero(Me)
    
End Function

Private Sub Toolbar_Operaciones_FM_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lNombre_Hoja As String
Dim lTitulo As String
Dim lMsg_Error As String

  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
        Call Sub_Graba_Operaciones_con_Cta_FM
        
    Case "IMPORT"
      If Fnt_Importa_FM Then
        Tab_Operaciones.TabEnabled(0) = False
        Tab_Operaciones.TabEnabled(1) = False
        With Toolbar_Operaciones_FM
            .Buttons("SAVE").Enabled = True
            .Buttons("IMPORT").Enabled = False
            .Buttons("EXCEL").Enabled = True
            .Buttons("REFRESH").Enabled = True
        End With
      End If
      
    Case "EXCEL"
      lNombre_Hoja = "Importador de Movimientos FM"
      lTitulo = "Importador de Movimientos Fondos Mutuos"
      If Not Fnt_Excel(lNombre_Hoja, _
                       lTitulo, _
                       Txt_ArchivoPlano_FM, _
                       Grilla_Operaciones_FM, _
                       Grilla_Operaciones_Sin_Cuenta_FM, _
                       lMsg_Error) Then
        MsgBox lMsg_Error, vbCritical, Me.Caption
      End If
      
    Case "REFRESH"
        Tab_Operaciones.TabEnabled(0) = True
        Tab_Operaciones.TabEnabled(1) = True
        With Toolbar_Operaciones_FM
            .Buttons("SAVE").Enabled = False
            .Buttons("IMPORT").Enabled = True
            .Buttons("EXCEL").Enabled = False
            .Buttons("REFRESH").Enabled = False
        End With
        Call Sub_Limpia_Objetos
    
  End Select

End Sub

Private Sub Toolbar_Operaciones_RF_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lNombre_Hoja As String
Dim lTitulo As String
Dim lMsg_Error As String

  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Call Sub_Graba_Operaciones_con_Cta_RF
        
    Case "IMPORT"
      If Fnt_Importa_RF Then
        Tab_Operaciones.TabEnabled(0) = False
        Tab_Operaciones.TabEnabled(2) = False
        With Toolbar_Operaciones_RF
          .Buttons("SAVE").Enabled = True
          .Buttons("IMPORT").Enabled = False
          .Buttons("EXCEL").Enabled = True
          .Buttons("REFRESH").Enabled = True
        End With
      End If
      
    Case "EXCEL"
      lNombre_Hoja = "Importador de Movimientos RF"
      lTitulo = "Importador de Movimientos Renta Fija"
      If Not Fnt_Excel(lNombre_Hoja, _
                       lTitulo, _
                       Txt_ArchivoPlano_RF, _
                       Grilla_Operaciones_RF, _
                       Grilla_Operaciones_Sin_Cuenta_RF, _
                       lMsg_Error) Then
        MsgBox lMsg_Error, vbCritical, Me.Caption
      End If
    
    Case "REFRESH"
      Tab_Operaciones.TabEnabled(0) = True
      Tab_Operaciones.TabEnabled(2) = True
      With Toolbar_Operaciones_RF
          .Buttons("SAVE").Enabled = False
          .Buttons("IMPORT").Enabled = True
          .Buttons("EXCEL").Enabled = False
          .Buttons("REFRESH").Enabled = False
      End With
      Call Sub_Limpia_Objetos
      
  End Select
  
End Sub

Private Sub Toolbar_Operaciones_RV_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lNombre_Hoja As String
Dim lTitulo As String
Dim lMsg_Error As String

  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Call Sub_Graba_Operaciones_con_Cta_RV
        
    Case "IMPORT"
      If Fnt_Importa_RV Then
        Tab_Operaciones.TabEnabled(1) = False
        Tab_Operaciones.TabEnabled(2) = False
        With Toolbar_Operaciones_RV
          .Buttons("SAVE").Enabled = True
          .Buttons("IMPORT").Enabled = False
          .Buttons("EXCEL").Enabled = True
          .Buttons("REFRESH").Enabled = True
        End With
      End If
        
    Case "EXCEL"
      lNombre_Hoja = "Importador de Movimientos RV"
      lTitulo = "Importador de Movimientos Renta Variable"
      If Not Fnt_Excel(lNombre_Hoja, _
                       lTitulo, _
                       Txt_ArchivoPlano_RV, _
                       Grilla_Operaciones_RV, _
                       Grilla_Operaciones_Sin_Cuenta_RV, _
                       lMsg_Error) Then
        MsgBox lMsg_Error, vbCritical, Me.Caption
      End If
        
    Case "REFRESH"
      Tab_Operaciones.TabEnabled(1) = True
      Tab_Operaciones.TabEnabled(2) = True
      With Toolbar_Operaciones_RV
        .Buttons("SAVE").Enabled = False
        .Buttons("IMPORT").Enabled = True
        .Buttons("EXCEL").Enabled = False
        .Buttons("REFRESH").Enabled = False
      End With
      Call Sub_Limpia_Objetos
          
  End Select
End Sub

Public Function Fnt_Excel(pNombre_Hoja As String, _
                          pTitulo As String, _
                          pArchivo As hTextLabel, _
                          pGrilla_con_Cta As VSFlexGrid, _
                          pGrilla_sin_Cta As VSFlexGrid, _
                          ByRef pMsg_Error As String) As Boolean
Dim lApp_Excel As Excel.Application
Dim lLibro As Excel.Workbook
Dim lxHoja As Worksheet
Dim lHojas As Integer
Dim lNro_Libro As Integer
Dim lHoja As Integer
Dim lFila As Long
Dim lColumna As Long
Dim lFila_Grilla As Long
Dim lColumna_Grilla As Long
    
  On Error GoTo ErrProcedure
  
  Call Sub_Bloquea_Puntero(Me)
  
  Fnt_Excel = True
  
  If pGrilla_con_Cta.Rows = 1 And pGrilla_sin_Cta.Rows = 1 Then
    Call Sub_Desbloquea_Puntero(Me)
    Exit Function
  End If
  
  Set lApp_Excel = CreateObject("Excel.application")
  Set lLibro = lApp_Excel.Workbooks.Add
  
  For lNro_Libro = lLibro.Worksheets.Count To lLibro.Worksheets.Count - 1 Step -1
      lLibro.Worksheets(lNro_Libro).Delete
  Next lNro_Libro
  
  For lNro_Libro = lLibro.Worksheets.Count To lHojas - 1
      lLibro.Worksheets.Add
  Next lNro_Libro
  
  For lHoja = 1 To lLibro.Worksheets.Count
    lLibro.Sheets(lHoja).Select
    lLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresa).ShapeRange.IncrementLeft 39.75
    lLibro.Worksheets.Item(lHoja).Range("B6").Value = "Banca Patrimonial de Inversiones"
    lLibro.Worksheets.Item(lHoja).Range("B6").Font.Bold = True
    lLibro.Worksheets.Item(lHoja).Columns("A:A").ColumnWidth = 2
  Next lHoja
  
  lLibro.Sheets(1).Select
  lApp_Excel.ActiveWindow.DisplayGridlines = False

  'NOMBRE HOJA
  lLibro.Worksheets.Item(1).Name = pNombre_Hoja

  'TITULO
  lLibro.ActiveSheet.Range("D7").Value = pTitulo
  lLibro.ActiveSheet.Range("D8").Value = "Nombre Archivo: " & Fnt_Nombre_Archivo(pArchivo)
  lLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
  lLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
  lLibro.ActiveSheet.Range("D7:G7").Merge
  lLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
  lLibro.ActiveSheet.Range("D8:G8").Merge

  'ENCABEZADO
  lFila = 10
  lColumna = 2
  lLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Operaciones con Cuentas"
  lLibro.ActiveSheet.Cells(lFila, lColumna).Font.Bold = True
  
  Set lxHoja = lLibro.Sheets(1)
  Call lxHoja.Select
  
  With pGrilla_con_Cta
    If .Rows > 1 Then
      For lFila_Grilla = 0 To .Rows - 1
        lFila = lFila + 1
        For lColumna_Grilla = 1 To .Cols - 1
          If Not .ColHidden(lColumna_Grilla) Then
            lxHoja.Cells(lFila, lColumna).NumberFormat = .ColFormat(lColumna_Grilla)
            lxHoja.Cells(lFila, lColumna).Value = .TextMatrix(lFila_Grilla, lColumna_Grilla)
            lColumna = lColumna + 1
          End If
        Next
        lColumna = 2
      Next
      Call Sub_Formato_Hoja(lApp_Excel, lLibro, "B11")
    Else
      lLibro.ActiveSheet.Cells(lFila + 1, lColumna).Value = "No hay datos"
    End If
  End With
  
  lFila = lFila + 3
  lColumna = 2
  lLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Operaciones sin Cuentas"
  lLibro.ActiveSheet.Cells(lFila, lColumna).Font.Bold = True
  
  With pGrilla_sin_Cta
    If .Rows > 1 Then
      For lFila_Grilla = 0 To .Rows - 1
        lFila = lFila + 1
        For lColumna_Grilla = 1 To .Cols - 1
          If Not .ColHidden(lColumna_Grilla) Then
            lxHoja.Cells(lFila, lColumna).NumberFormat = .ColFormat(lColumna_Grilla)
            lxHoja.Cells(lFila, lColumna).Value = .TextMatrix(lFila_Grilla, lColumna_Grilla)
            lColumna = lColumna + 1
          End If
        Next
        lColumna = 2
      Next
      Call Sub_Formato_Hoja(lApp_Excel, lLibro, "B" & (lFila + 1 - pGrilla_sin_Cta.Rows))
    Else
      lLibro.ActiveSheet.Cells(lFila + 1, lColumna).Value = "No hay datos"
    End If
  End With
  
  lLibro.Sheets(1).Select
  lApp_Excel.Visible = True
  lApp_Excel.UserControl = True
    
  Call Sub_Desbloquea_Puntero(Me)
  
  Exit Function
   
ErrProcedure:
  Fnt_Excel = False
  pMsg_Error = Err.Description
  Err.Clear
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Function

Private Sub Sub_Formato_Hoja(pApp_Excel As Excel.Application, pLibro As Excel.Workbook, ByVal pRango As String)
  pLibro.Sheets(1).Select
  pLibro.Sheets(1).Activate

  pLibro.ActiveSheet.Range(pRango).Select
  pLibro.ActiveSheet.Range(pRango).Activate
  pApp_Excel.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlDown)).Select
  pApp_Excel.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlToRight)).Select
  
  pApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
  
  pApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
  
  pApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
  
  pApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
  
  pApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
  
  pApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
  
  pLibro.ActiveSheet.Range(pRango).Select
  pLibro.ActiveSheet.Range(pRango).Activate
  pApp_Excel.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlToRight)).Select
  pLibro.ActiveSheet.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlToRight)).Activate
  pLibro.ActiveSheet.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlToRight)).Font.Bold = True

  pApp_Excel.Selection.Interior.ColorIndex = 36
  pApp_Excel.Selection.Interior.Pattern = xlSolid
  
  pLibro.ActiveSheet.Columns("B:B").Select
  pApp_Excel.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlToRight)).Select
  pApp_Excel.Selection.EntireColumn.AutoFit
  pLibro.ActiveSheet.Range("A1").Select
    
End Sub

Private Sub Sub_CambiaCheck(pGrilla As VSFlexGrid, pValor As Boolean)
    Dim lLinea As Long
    If pValor Then
        For lLinea = 1 To pGrilla.Rows - 1
            'pGrilla.Cell(flexcpChecked, lLinea, "chk") = flexChecked
            Call SetCell(pGrilla, lLinea, "chk", flexChecked)
        Next
    Else
        For lLinea = 1 To pGrilla.Rows - 1
            'pGrilla.Cell(flexcpChecked, lLinea, "chk") = 0
            Call SetCell(pGrilla, lLinea, "chk", 0)
        Next
    End If
End Sub

Private Sub Grilla_Operaciones_RV_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_RV)
End Sub

Private Sub Sub_Setea_Cta_Nueva(pGrilla As VSFlexGrid)
Dim lId_Cuenta
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
'----------------------------
Dim lId_Cliente As String
Dim lcCliente As Object
'----------------------------
Dim lNum_Cuenta As String
Dim lRut_Cliente As String
Dim lNro_Orden As String
Dim lFila As Long
    
  lId_Cuenta = Frm_Busca_Cuentas.Buscar
  
  If Not IsNull(lId_Cuenta) Then
'    Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
      .Campo("id_cuenta").Valor = lId_Cuenta
      If .Buscar Then
        lId_Cliente = .Cursor(1).Fields("id_cliente").Value
        lNum_Cuenta = .Cursor(1).Fields("num_cuenta").Value
      End If
    End With
    Set lcCuenta = Nothing
    
    Set lcCliente = Fnt_CreateObject(cDLL_Clientes)
    With lcCliente
      Set .gDB = gDB
      .Campo("id_cliente").Valor = lId_Cliente
      If .Buscar(True) Then
        lRut_Cliente = .Cursor(1).Fields("rut_cliente").Value
      End If
    End With
    Set lcCliente = Nothing
    
    lNro_Orden = GetCell(pGrilla, pGrilla.Row, "colum_pk")
    For lFila = 1 To pGrilla.Rows - 1
      If GetCell(pGrilla, lFila, "colum_pk") = lNro_Orden Then
        Call SetCell(pGrilla, lFila, "id_cuenta", lId_Cuenta)
        Call SetCell(pGrilla, lFila, "num_cuenta", lNum_Cuenta)
        Call SetCell(pGrilla, lFila, "rut", lRut_Cliente)
      End If
    Next
  End If
End Sub

Private Sub Sub_Graba_Operaciones_con_Cta_RV()
Dim lFila As Long
Dim lMsg_Error As String
    
  For lFila = 1 To Grilla_Operaciones_RV.Rows - 1
    Rem Busca las operaciones chequeadas
    If Grilla_Operaciones_RV.Cell(flexcpChecked, lFila, Grilla_Operaciones_Sin_Cuenta_RV.ColIndex("chk")) = flexChecked Then
      If Not Fnt_Ope_Directa_Cont_RV(pGrilla:=Grilla_Operaciones_RV, _
                                     pFila:=lFila, _
                                     pNro_Orden:=To_Number(To_Number(GetCell(Grilla_Operaciones_RV, lFila, "colum_pk")))) Then
        'GoTo ErrProcedure
      End If
    End If
  Next lFila
  
  For lFila = 1 To Grilla_Operaciones_Sin_Cuenta_RV.Rows - 1
    Rem Busca las operaciones chequeadas
    If Grilla_Operaciones_Sin_Cuenta_RV.Cell(flexcpChecked, lFila, Grilla_Operaciones_Sin_Cuenta_RV.ColIndex("chk")) = flexChecked Then
      If Not Fnt_Ope_Directa_Cont_RV(pGrilla:=Grilla_Operaciones_Sin_Cuenta_RV, _
                                     pFila:=lFila, _
                                     pNro_Orden:=To_Number(To_Number(GetCell(Grilla_Operaciones_Sin_Cuenta_RV, lFila, "colum_pk")))) Then
        'GoTo ErrProcedure
      End If
    End If
  Next lFila
  
  MsgBox "Grabaci�n de Movimientos Diarios de Renta Variable finalizada.", vbInformation
  
  Tab_Operaciones.TabEnabled(1) = True
  Tab_Operaciones.TabEnabled(2) = True
  
'  With Toolbar_Operaciones_RV
'    .Buttons("SAVE").Enabled = False
'    .Buttons("IMPORT").Enabled = True
'    .Buttons("SEL_ALL").Enabled = False
'    .Buttons("SEL_NOTHING").Enabled = False
'    .Buttons("REFRESH").Enabled = False
'  End With
  
  Call Grilla_Operaciones_RV.Select(0, 0)
  Call Grilla_Operaciones_Sin_Cuenta_RV.Select(0, 0)
  
  'Call Sub_Limpia_Objetos
  
  'Call Toolbar_ButtonClick("REFRESH")
End Sub

Private Function Fnt_Ope_Directa_Cont_RV(pGrilla As VSFlexGrid, _
                                         ByRef pFila As Long, _
                                         ByVal pNro_Orden As Long) As Boolean
Dim lFila As Long
Dim lcAcciones As Class_Acciones
Dim lId_Caja_Cuenta As Double
Dim lId_Nemotecnico As String
Dim lFecha_Movimiento As Date
Dim lFecha_Liquidacion As Date
Dim lId_Cuenta As String
Dim lId_Moneda As String
Dim lFecha As Date
Dim lMsg_Error As String
Dim lNum_Cuenta As String
Dim lFlg_Tipo_Movimiento As String
'---------------------------------------
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
'---------------------------------------
Dim lId_Operacion As String
Dim lcOperaciones As Class_Operaciones
Dim lDetalle As Class_Operaciones_Detalle
Dim lRollback As Boolean
Dim lMonto_Operacion As Double
Dim lSuma_Monto_Neto As Double
'---------------------------------------
Dim lcComisiones As Class_Comisiones_Instrumentos
Dim lPorcentaje_Comision As Double
Dim lGastos As Double
Dim lComision As Double
Dim lDerecho As Double
Dim lIva As Double
'---------------------------------------
Dim lRecord_Detalle As hRecord
Dim lReg As hFields
Dim lCod_Instrumento As String
'---------------------------------------
 
  Set lRecord_Detalle = New hRecord
  With lRecord_Detalle
    .ClearFields
    .AddField "ID_NEMOTECNICO", 0
    .AddField "CANTIDAD", 0
    .AddField "PRECIO", 0
    .AddField "MONTO_DETALLE", 0
    .AddField "ID_MONEDA"
    .AddField "COMISION", 0
    .AddField "DERECHO", 0
    .AddField "GASTOS", 0
    .AddField "IVA", 0
    .LimpiarRegistros
  End With

  lRollback = False
  gDB.IniciarTransaccion
  
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("fecha_operacion").Valor = DTP_Fecha_Proceso_RV.Value
    .Campo("cod_producto").Valor = gcPROD_RV_NAC
    If Not .Anular_Operaciones_Pendientes Then
      lRollback = True
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcOperaciones = Nothing
    
  lFila = pFila
  lId_Cuenta = GetCell(pGrilla, pFila, "id_cuenta")
  lNum_Cuenta = GetCell(pGrilla, pFila, "num_cuenta")
  Rem Ingresa el movimiento
  Do While To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pNro_Orden
    lId_Nemotecnico = GetCell(pGrilla, lFila, "id_nemotecnico")
    lFecha = GetCell(pGrilla, lFila, "fecha")
    '-----------------------------------------------------------------------------------------
    Rem VALIDACIONES
    lMsg_Error = ""
    Rem Valida que no se est� ingresando la operacion de nuevo
    If Not Fnt_Buscar_Rel_Nro_Oper_Detalle(GetCell(pGrilla, lFila, "colum_pk"), lMsg_Error) Then
      lMsg_Error = "El N�mero de Orden '" & GetCell(pGrilla, lFila, "colum_pk") & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
      GoTo ErrProcedure
    Rem La Fecha que viene en el Archivo Plano tiene q ser igual a la Fecha Proceso
    ElseIf Not lFecha = DTP_Fecha_Proceso_RV.Value Then
      lMsg_Error = "La Fecha '" & lFecha & "' no corresponde a la Fecha de Proceso seleccionada." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Si el Id_Cuenta y Num_cuenta es vacio quiere decir que no tiene asociada una cuenta en GPI
    ElseIf lId_Cuenta = "" And lNum_Cuenta = "" Then
      lMsg_Error = "La Orden no tiene asociado una cuenta en el sistema. El campo ""Cuenta"" est� vacio." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Si el id_nemotecnico es "0" quiere decir que no se encontr� el nemotecnico en GPI
    ElseIf lId_Nemotecnico = "0" Then
      lMsg_Error = "El Nemot�cnico '" & GetCell(pGrilla, lFila, "NEMOTECNICO") & "' no est� en el sistema." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem La Fecha de Liquidacion es vacia, no se puede operar
    ElseIf GetCell(pGrilla, lFila, "fecha_liquidacion") = "" Then
      lMsg_Error = "La Fecha de Liquidaci�n no puede estar vacia. No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem La Fecha de Liquidacion es "00/00/0000", no se puede operar
    ElseIf GetCell(pGrilla, lFila, "fecha_liquidacion") = "00/00/0000" Then
      lMsg_Error = "La Fecha de Liquidaci�n no es v�lida. No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Busca la moneda del nemotecnico en CSGPI
    ElseIf Not Fnt_Buscar_Datos_Nemo_RV(lId_Nemotecnico, lId_Moneda, lMsg_Error) Then
      GoTo ErrProcedure
    End If
    '-----------------------------------------------------------------------------
    Set lReg = lRecord_Detalle.Add
    lReg("ID_NEMOTECNICO").Value = lId_Nemotecnico
    lReg("CANTIDAD").Value = To_Number(GetCell(pGrilla, lFila, "Cantidad"))
    lReg("PRECIO").Value = To_Number(GetCell(pGrilla, lFila, "precio"))
    lReg("MONTO_DETALLE").Value = To_Number(GetCell(pGrilla, lFila, "monto_neto"))
    lReg("ID_MONEDA").Value = lId_Moneda
    lReg("COMISION").Value = To_Number(GetCell(pGrilla, lFila, "comi"))
    lReg("DERECHO").Value = To_Number(GetCell(pGrilla, lFila, "dere"))
    lReg("GASTOS").Value = To_Number(GetCell(pGrilla, lFila, "gast"))
    lReg("IVA").Value = To_Number(GetCell(pGrilla, lFila, "iva"))
    lFila = lFila + 1
    
    If lFila > pGrilla.Rows - 1 Then
      Exit Do
    End If
  Loop

  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("id_moneda").Valor = lId_Moneda
    .Campo("cod_mercado").Valor = cMercado_Nacional
    
    If .Buscar_Caja_Para_Invertir() Then
      lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
    Else
      lMsg_Error = "Problemas en cargar Cajas de la Cuenta." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  '-----------------------------------------------------------------------------

  lFecha_Movimiento = DTP_Fecha_Proceso_RV.Value
  lFecha_Liquidacion = GetCell(pGrilla, pFila, "FECHA_LIQUIDACION")
  lCod_Instrumento = GetCell(pGrilla, pFila, "cod_instru")
  lFlg_Tipo_Movimiento = GetCell(pGrilla, pFila, "flg_tipo_movimiento")
  
  Set lcAcciones = New Class_Acciones
  With lcAcciones
    For Each lReg In lRecord_Detalle
      Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                        pCantidad:=lReg("cantidad").Value, _
                                        pPrecio:=lReg("precio").Value, _
                                        pId_Moneda:=lReg("id_moneda").Value, _
                                        pMonto:=lReg("monto_detalle").Value, _
                                        pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                        pPrecio_Historico:="")
      
      lSuma_Monto_Neto = lSuma_Monto_Neto + lReg("monto_detalle").Value
'      lComision = lComision + lReg("COMISION").Value
'      lDerecho = lDerecho + lReg("DERECHO").Value
'      lGastos = lGastos + lReg("GASTOS").Value
'      lIva = lIva + lReg("IVA").Value
      Rem No vienen prorrateados
      lComision = lReg("COMISION").Value
      lDerecho = lReg("DERECHO").Value
      lGastos = lReg("GASTOS").Value
      lIva = lReg("IVA").Value
    Next
    
    Rem Luego, se suman los montos netos de cada detalle con la comision
    If lFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
      lMonto_Operacion = lSuma_Monto_Neto + lComision + lDerecho + lGastos + lIva
    ElseIf lFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
      lMonto_Operacion = lSuma_Monto_Neto - (lComision + lDerecho + lGastos + lIva)
    End If
    
    Rem Comisiones
    Set lcComisiones = New Class_Comisiones_Instrumentos
    With lcComisiones
      .Campo("Id_Cuenta").Valor = lId_Cuenta
      .Campo("COD_INSTRUMENTO").Valor = lCod_Instrumento
      If .Buscar(True) Then
        If .Cursor.Count > 0 Then
          lPorcentaje_Comision = NVL(.Cursor(1)("COMISION").Value, 0)
        End If
      End If
    End With
    Set lcComisiones = Nothing
    '-----------------------------------------------------------------------------
    Rem Flag para que no tome en cuenta la cantidad de activos al ingresar una instruccion desde el Importador de movimientos
    Rem y para que ingrese en campos propios de importacion
    .fImportacion = True
    If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                          pId_Cuenta:=lId_Cuenta, _
                                          pDsc_Operacion:="", _
                                          pTipoOperacion:=lFlg_Tipo_Movimiento, _
                                          pId_Contraparte:="", _
                                          pId_Representante:="", _
                                          pId_Moneda_Operacion:=lId_Moneda, _
                                          pFecha_Operacion:=lFecha_Movimiento, _
                                          pFecha_Vigencia:=lFecha_Movimiento, _
                                          pFecha_Liquidacion:=lFecha_Liquidacion, _
                                          pId_Trader:="", _
                                          pPorc_Comision:=lPorcentaje_Comision, _
                                          pComision:=lComision, _
                                          pDerechos_Bolsa:=lDerecho, _
                                          pGastos:=lGastos, _
                                          pIva:=lIva, _
                                          pMonto_Operacion:=lMonto_Operacion, _
                                          pTipo_Precio:=cTipo_Precio_Mercado) Then
      lMsg_Error = "Problemas al grabar Acciones Nacionales." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcAcciones = Nothing
  '----------------------------------------------------------------------------

  Rem Con el nuevo id_operacion busca los detalles
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Id_Operacion").Valor = lId_Operacion
  
    Rem Busca el detalle de la operacion
    If Not .BuscaConDetalles Then
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  
    Rem Realiza la Confirmaci�n de la Instrucci�n
    If Not .Confirmar(lId_Caja_Cuenta) Then
      lMsg_Error = "Error en la confirmaci�n de la operaci�n." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  
    Rem Por cada id_operacion_detalle encontrado guarda la relacion
    Rem "nro_orden-id_operacion_detalle" en la tabla rel_conversiones
    For Each lDetalle In lcOperaciones.Detalles
      If Not Fnt_Guardar_Rel_Nro_Oper_Detalle(GetCell(pGrilla, pFila, "colum_pk"), _
                                              lDetalle.Campo("id_operacion_detalle").Valor, _
                                              lMsg_Error) Then
        GoTo ErrProcedure
      End If
    Next
  End With
    
  Rem Setea la pFila para que siga con la siguiente linea de la grilla
'  pFila = lFila - 1
  lMsg_Error = "Operaci�n Ingresada correctamente." & vbCr & vbCr & "N�mero Orden: " & pNro_Orden & vbCr & "N�mero Operaci�n CSGPI: " & lId_Operacion
  GoTo ExitProcedure
  
ErrProcedure:
  lRollback = True
  
ExitProcedure:
  lFila = pFila
  lMsg_Error = "Archivo: " & Fnt_Nombre_Archivo(Txt_ArchivoPlano_RV) & vbCr & vbCr & lMsg_Error
  Do While To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pNro_Orden
    Call SetCell(pGrilla, lFila, "dsc_error", "")
    Call SetCell(pGrilla, lFila, "dsc_error", lMsg_Error)
    
    If lRollback Then
      pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbRed
      'Grilla_Operaciones_Sin_Cuenta_RV.Cell(flexcpChecked, lFila, "chk") = flexChecked
      Call SetCell(pGrilla, lFila, "chk", flexChecked)
    Else
      pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbBlue
      Call SetCell(pGrilla, lFila, "chk", 0)
    End If
    
    lFila = lFila + 1
    
    If lFila > pGrilla.Rows - 1 Then
      Exit Do
    End If
  Loop
  
  Rem Setea la pFila para que siga con la siguiente Orden de la grilla
  pFila = lFila - 1
  
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  Set lcAcciones = Nothing
  
  Call Fnt_AgregarLog(pId_Log_SubTipo:=eLog_Subtipo.eLS_Importador_Movimientos _
                    , pCod_Estado:=cEstado_Log_Mensaje _
                    , pGls_Log_Registro:=lMsg_Error _
                    , pId_Log_Proceso:=Fnt_Agregar_Log_Proceso(eLog_Subtipo.eLS_Importador_Movimientos))
                        
  Fnt_Ope_Directa_Cont_RV = Not lRollback
  
End Function

Private Function Fnt_Buscar_Datos_Nemo_RV(pId_Nemotecnico As String, _
                                          ByRef pId_Moneda_Nemo As String, _
                                          ByRef pMsg_Error As String) As Boolean
Dim lcAlias As Class_Alias
Dim lcNemotecnico As Class_Nemotecnicos
  
  Fnt_Buscar_Datos_Nemo_RV = True
  
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pId_Moneda_Nemo = .Cursor(1)("id_moneda_transaccion").Value
      Else
        pMsg_Error = "Nemot�cnico no encontrado en el sistema. No se puede operar la instrucci�n."
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = "Problemas en cargar datos de Nemotecnico." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcNemotecnico = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Buscar_Datos_Nemo_RV = False
End Function

Private Function Fnt_Guardar_Rel_Nro_Oper_Detalle(pNro_Orden As Variant, _
                                                  pId_Operacion_Detalle As String, _
                                                  pMsg_Error As String) As Boolean
Dim lId_Origen As String
Dim lId_Tipo_Conversion As String
Dim lcRel_Conversion As Object

  Fnt_Guardar_Rel_Nro_Oper_Detalle = True
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_BAC_Fondos_BBVA, False)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle, False)
  
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = lId_Origen
    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
    .Campo("valor").Valor = pNro_Orden
    Rem En "id_entidad" se guarda el id_operacion_detalle
    .Campo("id_entidad").Valor = pId_Operacion_Detalle
    If Not .Guardar Then
      pMsg_Error = "Problemas en guardar la relaci�n N�mero de Orden y el Detalle de la Operaci�n." & vbCr & .ErrMsg
      Fnt_Guardar_Rel_Nro_Oper_Detalle = False
    End If
  End With
  Set lcRel_Conversion = Nothing
  
End Function

Private Function Fnt_Buscar_Rel_Nro_Oper_Detalle(pNro_Orden As Variant, pMsg_Error As String) As Boolean
  gDB.Parametros.Clear
  gDB.Procedimiento = "PKG_BBVA.IMPORT_AVAIBLE"
  gDB.Parametros.Add "PID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
  gDB.Parametros.Add "PVALOR", ePT_Caracter, pNro_Orden, ePD_Entrada
  gDB.Parametros.Add "PCOD_INSTRUMENTO", ePT_Caracter, "", ePD_Entrada
  gDB.Parametros.Add "PRETURN", ePT_Caracter, "", ePD_Salida
  If Not gDB.EjecutaSP Then
    pMsg_Error = gDB.ErrMsg
    GoTo ErrProcedure
  End If
  Fnt_Buscar_Rel_Nro_Oper_Detalle = (gDB.Parametros("PRETURN").Valor = gcFlg_SI)
  
  Exit Function
ErrProcedure:
  Fnt_Buscar_Rel_Nro_Oper_Detalle = False

'Dim lId_Origen As String
'Dim lId_Tipo_Conversion As String
'Dim lcRel_Conversion As Object
'
'  Fnt_Buscar_Rel_Nro_Oper_Detalle = False
'
'  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_BAC_Fondos_BBVA)
'  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle)
'
'  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
'  With lcRel_Conversion
'    Set .gDB = gDB
'    .Campo("Id_Origen").Valor = lId_Origen
'    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
'    .Campo("valor").Valor = pNro_Orden
'    If .Buscar Then
'      If .Cursor.Count > 0 Then
'        pMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
'        Fnt_Buscar_Rel_Nro_Oper_Detalle = True
'      End If
'    Else
'      pMsg_Error = "Problemas en buscar la relaci�n N�mero Documento y el Detalle de la Operaci�n." & vbCr & .ErrMsg
'      Fnt_Buscar_Rel_Nro_Oper_Detalle = True
'    End If
'  End With
'  Set lcRel_Conversion = Nothing
End Function

Private Function Fnt_Importa_RF() As Boolean
Dim lCampo As hFields
Dim lLinea As Long
Dim lTipo_Movimiento As String
Dim lCod_Instrumento As String
Dim lId_Nemotecnico As String
Dim lId_Cuenta
Dim lAlias_Cta As String
Dim lLinea_Oper_sin_cta As Long
Dim lNum_Cuenta As String
Dim lLinea_Oper_con_cta As Long
'----------------------------------
Dim lcNemotecnicos As Class_Nemotecnicos
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
  
  Call Sub_Bloquea_Puntero(Me)
  
  Fnt_Importa_RF = True
  
  Grilla_Operaciones_RF.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_RF.Rows = 1
  
  With fMov_Fact_RF
    If Not .Carga_Facturacion_Rf Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
    
    If .Cursor.Count = 0 Then
      MsgBox "No hay datos en el Archivo Seleccionado.", vbInformation, Me.Caption
      GoTo ErrProcedure
    End If
    
    For Each lCampo In .Cursor
      lTipo_Movimiento = ""
      If lCampo("TIPO_MOVIMIENTO").Value = gcTipoOperacion_Ingreso Then
        lTipo_Movimiento = "Ingreso"
      ElseIf lCampo("TIPO_MOVIMIENTO").Value = gcTipoOperacion_Egreso Then
        lTipo_Movimiento = "Egreso"
      End If
      '------- RESCATA EL ID_NEMOTECNICO
      If lCampo("NEMOTECNICO").Value = "PAGARE NR" Or _
         lCampo("NEMOTECNICO").Value = "PAGARE R" Or _
         lCampo("NEMOTECNICO").Value = "PDBC" Or _
         lCampo("NEMOTECNICO").Value = "PRBC" Then
        lId_Nemotecnico = "0"
        lCod_Instrumento = gcINST_DEPOSITOS_NAC
      Else
        Set lcNemotecnicos = New Class_Nemotecnicos
        lcNemotecnicos.Campo("nemotecnico").Valor = lCampo("NEMOTECNICO").Value
        lCod_Instrumento = ""
        Rem Busca el id_nemotecnico del Nemotecnico de la planilla
        If lcNemotecnicos.Buscar_Nemotecnico(pCod_Producto:=Null, pMostrar_Msg:=False) Then
          lId_Nemotecnico = lcNemotecnicos.Campo("id_nemotecnico").Valor
          If Not lId_Nemotecnico = "0" Then
            'Rem Busca atributos del Nemotecnico
            If lcNemotecnicos.Buscar Then
              lCod_Instrumento = NVL(lcNemotecnicos.Cursor(1)("cod_instrumento").Value, "")
            Else
            End If
          End If
        End If
        Set lcNemotecnicos = Nothing
      End If
      '------- FIN RESCATA EL ID_NEMOTECNICO
      Rem Busca el Alias de la Cuenta por el "Rut Cliente" concatenado con la "Cuenta"
      lAlias_Cta = NVL(lCampo("RUT_CLIENTE").Value, "") & "/" & NVL(lCampo("CUENTA").Value, "")
      Set lcAlias = CreateObject(cDLL_Alias)
      Set lcAlias.gDB = gDB
      lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_BAC_Fondos_BBVA _
                                           , pCodigoCSBPI:=cTabla_Cuentas _
                                           , pValor:=lAlias_Cta)
      If IsNull(lId_Cuenta) Then
          lLinea_Oper_sin_cta = Grilla_Operaciones_Sin_Cuenta_RF.Rows
          Call Grilla_Operaciones_Sin_Cuenta_RF.AddItem("")
          Grilla_Operaciones_Sin_Cuenta_RF.Cell(flexcpChecked, lLinea_Oper_sin_cta, "chk") = flexChecked
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "colum_pk", NVL(lCampo("NRO_ORDEN").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "fecha", NVL(lCampo("FECHA").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "rut", Trim(lCampo("RUT_CLIENTE").Value))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "id_cuenta", "")
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "num_cuenta", "")
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "tipo_movimiento", lTipo_Movimiento)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "flg_tipo_movimiento", lCampo("TIPO_MOVIMIENTO").Value)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "id_nemotecnico", lId_Nemotecnico)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "nemotecnico", lCampo("NEMOTECNICO").Value)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "nominales", lCampo("NOMINALES").Value)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "tasa", lCampo("TASA").Value)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "moneda", NVL(lCampo("moneda").Value, ""))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "tipo_reajuste", NVL(lCampo("tipo_reajuste").Value, ""))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "comi", NVL(lCampo("COMISION").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "Dere", NVL(lCampo("DERECHO").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "gast", NVL(lCampo("GASTOS").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "iva", NVL(lCampo("IVA").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "monto_operacion", NVL(lCampo("MONTO_OPERACION").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "moneda_operacion", NVL(lCampo("moneda_operacion").Value, ""))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "fecha_liquidacion", NVL(lCampo("FECHA_LIQUIDACION").Value, ""))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "FECHA_vencimiento", NVL(lCampo("FECHA_vencimiento").Value, ""))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "cod_instru", lCod_Instrumento)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "rut_emisor", NVL(lCampo("RUT_EMISOR").Value, ""))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_RF, lLinea_Oper_sin_cta, "dsc_error", "")
      Else
          ' ----------- RESCATA NUMERO CUENTA
'          Set lcCuenta = New Class_Cuentas
          Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
          lcCuenta.Campo("id_cuenta").Valor = lId_Cuenta
          If lcCuenta.Buscar Then
            lNum_Cuenta = lcCuenta.Cursor(1)("num_cuenta").Value
          Else
          End If
          Set lcCuenta = Nothing
          ' ----------- FIN RESCATA NUMERO CUENTA
          lLinea_Oper_con_cta = Grilla_Operaciones_RF.Rows
          Call Grilla_Operaciones_RF.AddItem("")
          Grilla_Operaciones_RF.Cell(flexcpChecked, lLinea_Oper_con_cta, "chk") = flexChecked
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "colum_pk", NVL(lCampo("NRO_ORDEN").Value, 0))
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "fecha", NVL(lCampo("FECHA").Value, 0))
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "rut", Trim(lCampo("RUT_CLIENTE").Value))
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "id_cuenta", lId_Cuenta)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "num_cuenta", lNum_Cuenta)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "tipo_movimiento", lTipo_Movimiento)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "flg_tipo_movimiento", lCampo("TIPO_MOVIMIENTO").Value)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "id_nemotecnico", lId_Nemotecnico)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "nemotecnico", lCampo("NEMOTECNICO").Value)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "nominales", lCampo("NOMINALES").Value)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "tasa", lCampo("TASA").Value)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "moneda", NVL(lCampo("moneda").Value, ""))
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "tipo_reajuste", NVL(lCampo("tipo_reajuste").Value, ""))
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "comi", NVL(lCampo("COMISION").Value, 0))
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "Dere", NVL(lCampo("DERECHO").Value, 0))
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "gast", NVL(lCampo("GASTOS").Value, 0))
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "iva", NVL(lCampo("IVA").Value, 0))
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "monto_operacion", NVL(lCampo("MONTO_OPERACION").Value, 0))
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "moneda_operacion", NVL(lCampo("moneda_operacion").Value, ""))
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "fecha_liquidacion", lCampo("FECHA_LIQUIDACION").Value)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "fecha_vencimiento", lCampo("fecha_vencimiento").Value)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "cod_instru", lCod_Instrumento)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "RUT_EMISOR", lCampo("RUT_EMISOR").Value)
          Call SetCell(Grilla_Operaciones_RF, lLinea_Oper_con_cta, "dsc_error", "")
      End If
      
    Next
  End With
  
  Call Sub_Desbloquea_Puntero(Me)
  Exit Function
  
ErrProcedure:
  Fnt_Importa_RF = False
  Call Sub_Desbloquea_Puntero(Me)
  
End Function

Private Sub Sub_Graba_Operaciones_con_Cta_RF()
Dim lFila As Long
    
  For lFila = 1 To Grilla_Operaciones_RF.Rows - 1
    Rem Busca las operaciones chequeadas
    If Grilla_Operaciones_RF.Cell(flexcpChecked, lFila, Grilla_Operaciones_RF.ColIndex("chk")) = flexChecked Then
      If Not Fnt_Ope_Directa_Cont_RF(pGrilla:=Grilla_Operaciones_RF, _
                                     pFila:=lFila, _
                                     pNro_Orden:=To_Number(To_Number(GetCell(Grilla_Operaciones_RF, lFila, "colum_pk")))) Then
        'GoTo ErrProcedure
      End If
    End If
  Next lFila
    
  For lFila = 1 To Grilla_Operaciones_Sin_Cuenta_RF.Rows - 1
    Rem Busca las operaciones chequeadas
    If Grilla_Operaciones_Sin_Cuenta_RF.Cell(flexcpChecked, lFila, Grilla_Operaciones_Sin_Cuenta_RF.ColIndex("chk")) = flexChecked Then
      If Not Fnt_Ope_Directa_Cont_RF(pGrilla:=Grilla_Operaciones_Sin_Cuenta_RF, _
                                     pFila:=lFila, _
                                     pNro_Orden:=To_Number(To_Number(GetCell(Grilla_Operaciones_Sin_Cuenta_RF, lFila, "colum_pk")))) Then
        'GoTo ErrProcedure
      End If
    End If
  Next lFila
  
  MsgBox "Grabaci�n de Movimientos Diarios de Renta Fija finalizada.", vbInformation
  
  Tab_Operaciones.TabEnabled(0) = True
  Tab_Operaciones.TabEnabled(2) = True
  
'  With Toolbar_Operaciones_RV
'    .Buttons("SAVE").Enabled = False
'    .Buttons("IMPORT").Enabled = True
'    .Buttons("SEL_ALL").Enabled = False
'    .Buttons("SEL_NOTHING").Enabled = False
'    .Buttons("REFRESH").Enabled = False
'  End With
  
  Call Grilla_Operaciones_RF.Select(0, 0)
  Call Grilla_Operaciones_Sin_Cuenta_RF.Select(0, 0)
  
  'Call Sub_Limpia_Objetos
  
  'Call Toolbar_ButtonClick("REFRESH")
End Sub

Private Function Fnt_Ope_Directa_Cont_RF(pGrilla As VSFlexGrid, ByRef pFila As Long, ByVal pNro_Orden As Long) As Boolean
Dim lcBonos As Class_Bonos
Dim lcDepositos As Class_Depositos
Dim lcPactos As Class_Pactos
'------------------------------------------------
Dim lId_Caja_Cuenta As Double
Dim lId_Nemotecnico As String
Dim lFecha As Date
Dim lFecha_Movimiento As Date
Dim lFecha_Liquidacion As Date
Dim lMoneda_Operacion As String
Dim lMoneda As String
Dim lTipo_Reajuste  As String
Dim lMsg_Error As String
Dim lFlg_Tipo_Movimiento As String
'---------------------------------------
Dim lcCliente As Object
Dim lRut_Cliente As String
Dim lId_Cliente As String
'---------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lId_Cuenta
Dim lNum_Cuenta
'---------------------------------------
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
'---------------------------------------
Dim lId_Operacion As String
Dim lcOperaciones As Class_Operaciones
Dim lDetalle As Class_Operaciones_Detalle
Dim lRollback As Boolean
Dim lMonto_Operacion As Double
Dim lSuma_Monto_Neto As Double
Dim lId_Moneda_Deposito
Dim lId_Moneda_Pago As String
Dim lFecha_Vencimiento As Date
Dim lRut_Emisor As String
'---------------------------------------
Dim lcComisiones As Class_Comisiones_Instrumentos
Dim lPorcentaje_Comision As Double
Dim lGastos As Double
Dim lPorcentaje_Derechos As Double
Dim lComision As Double
Dim lDerechos As Double
Dim lIva As Double
'---------------------------------------
Dim lFila As Long
Dim lRecord_Detalle As hRecord
Dim lhActivos       As hRecord
Dim lReg_Activo     As hFields
Dim lReg As hFields
Dim pFila_Cont As Long
Dim lCod_Instrumento As String
    
  Set lRecord_Detalle = New hRecord
  With lRecord_Detalle
    .ClearFields
    .AddField "id_nemotecnico", 0
    .AddField "NOMINALES", 0
    .AddField "TASA", 0
    .AddField "ID_MONEDA"
    .AddField "COMISION", 0
    .AddField "DERECHO", 0
    .AddField "GASTOS", 0
    .AddField "IVA", 0
    .AddField "MONTO_OPERACION", 0
    .AddField "FECHA_VENCIMIENTO"
    '.AddField "Base"
    .LimpiarRegistros
  End With

  lRollback = False
  gDB.IniciarTransaccion
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Rem Realiza la operacion directa
        
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("fecha_operacion").Valor = DTP_Fecha_Proceso_RF.Value
    .Campo("cod_producto").Valor = gcPROD_RF_NAC
    If Not .Anular_Operaciones_Pendientes Then
      lRollback = True
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcOperaciones = Nothing
    
  lFila = pFila
  lId_Cuenta = GetCell(pGrilla, pFila, "id_cuenta")
  lNum_Cuenta = GetCell(pGrilla, pFila, "num_cuenta")
  lFecha_Movimiento = DTP_Fecha_Proceso_RF.Value
  lCod_Instrumento = GetCell(pGrilla, pFila, "cod_instru")
  
  Rem Ingresa el movimiento
  Do While To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pNro_Orden
    lFecha = GetCell(pGrilla, lFila, "fecha")
    lId_Nemotecnico = GetCell(pGrilla, lFila, "id_nemotecnico")
    lRut_Emisor = GetCell(pGrilla, lFila, "Rut_Emisor")
    lFecha_Vencimiento = To_Number(GetCell(pGrilla, lFila, "Fecha_Vencimiento"))
    lMoneda_Operacion = GetCell(pGrilla, lFila, "Moneda_Operacion") ' Moneda Deposito
    lMoneda = GetCell(pGrilla, lFila, "Moneda") ' Moneda Pago
    lTipo_Reajuste = GetCell(pGrilla, lFila, "Tipo_Reajuste")
    '-----------------------------------------------------------------------------------------
    Rem VALIDACIONES
    lMsg_Error = ""
    Rem Valida que no se est� ingresando la operacion de nuevo
    If Not Fnt_Buscar_Rel_Nro_Oper_Detalle(GetCell(pGrilla, lFila, "colum_pk"), lMsg_Error) Then
      lMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
      GoTo ErrProcedure
    Rem La Fecha que viene en el Archivo Plano tiene q ser igual a la Fecha Proceso
    ElseIf Not lFecha = DTP_Fecha_Proceso_RF.Value Then
      lMsg_Error = "La Fecha '" & lFecha & "' no corresponde a la Fecha de Proceso seleccionada." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Si el Id_Cuenta y Num_cuenta es vacio quiere decir que no tiene asociada una cuenta en GPI
    ElseIf lId_Cuenta = "" And lNum_Cuenta = "" Then
      lMsg_Error = "La Orden no tiene asociado una cuenta en el sistema. El campo ""Cuenta"" est� vacio." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Si el id_nemot�cnico es "0" quiere decir que no se encontr� el nemot�cnico en GPI
    ElseIf lId_Nemotecnico = "0" Then
      Rem Si no encontro el nemotecnico, se crea. Solo para depositos y pactos
      If lCod_Instrumento = gcINST_DEPOSITOS_NAC Or lCod_Instrumento = gcINST_PACTOS_NAC Then
        If Not Fnt_Crea_Nemotecnico(pCod_Instrumento:=lCod_Instrumento, _
                                    pRut_Emisor:=lRut_Emisor, _
                                    pFecha_Emision:=lFecha_Movimiento, _
                                    pFecha_Vencimiento:=lFecha_Vencimiento, _
                                    pMoneda_Deposito:=lTipo_Reajuste, _
                                    pMoneda_Pago:=lMoneda_Operacion, _
                                    pTipo_Reajuste:=lTipo_Reajuste, _
                                    pId_Nemotecnico:=lId_Nemotecnico, _
                                    pNemotecnico:="", _
                                    pMsg_Error:=lMsg_Error) Then
          GoTo ErrProcedure
        End If
      Else
        lMsg_Error = "El Nemot�cnico '" & GetCell(pGrilla, lFila, "NEMOTECNICO") & "' no est� en el sistema." & vbCr & "No se puede operar la instrucci�n."
        GoTo ErrProcedure
      End If
    End If
    Rem La Fecha de Liquidacion es vacia, no se puede operar
    If GetCell(pGrilla, lFila, "fecha_liquidacion") = "" Then
      lMsg_Error = "La Fecha de Liquidaci�n no puede estar vacia. No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem La Fecha de Liquidacion es "00/00/0000", no se puede operar
    ElseIf GetCell(pGrilla, lFila, "fecha_liquidacion") = "00/00/0000" Then
      lMsg_Error = "La Fecha de Liquidaci�n no es v�lida. No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Busca la moneda del nemot�cnico en CSGPI
    ElseIf Not Fnt_Buscar_Datos_Nemo_RF(lId_Nemotecnico, lId_Moneda_Pago, lFecha_Vencimiento, lMsg_Error) Then
      GoTo ErrProcedure
    End If
    '-----------------------------------------------------------------------------
  
    Set lReg = lRecord_Detalle.Add
    lReg("id_nemotecnico").Value = lId_Nemotecnico
    lReg("NOMINALES").Value = To_Number(GetCell(pGrilla, lFila, "NOMINALES"))
    lReg("TASA").Value = To_Number(GetCell(pGrilla, lFila, "TASA"))
    lReg("ID_MONEDA").Value = lId_Moneda_Pago
    lReg("COMISION").Value = To_Number(GetCell(pGrilla, lFila, "comi"))
    lReg("DERECHO").Value = To_Number(GetCell(pGrilla, lFila, "Dere"))
    lReg("GASTOS").Value = To_Number(GetCell(pGrilla, lFila, "gast"))
    lReg("IVA").Value = To_Number(GetCell(pGrilla, lFila, "IVA"))
    lReg("MONTO_OPERACION").Value = To_Number(GetCell(pGrilla, lFila, "monto_operacion"))
    lReg("FECHA_VENCIMIENTO").Value = lFecha_Vencimiento
    'lReg("Base").Value = lBase
    lFila = lFila + 1
    If lFila > pGrilla.Rows - 1 Then
      Exit Do
    End If
  Loop
  
  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("id_moneda").Valor = lId_Moneda_Pago
    .Campo("cod_mercado").Valor = cMercado_Nacional
    If .Buscar_Caja_Para_Invertir() Then
      lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
    Else
      lMsg_Error = "Problemas en cargar Cajas de la Cuenta." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  '-----------------------------------------------------------------------------
  
  Rem Comisiones
  Set lcComisiones = New Class_Comisiones_Instrumentos
  With lcComisiones
    .Campo("Id_Cuenta").Valor = lId_Cuenta
    .Campo("COD_INSTRUMENTO").Valor = lCod_Instrumento
    If .Buscar(True) Then
        If .Cursor.Count > 0 Then
          lPorcentaje_Comision = NVL(.Cursor(1)("COMISION").Value, 0)
        End If
    End If
  End With
  Set lcComisiones = Nothing
  
  Rem Fecha de Liquidacion
  lFecha_Liquidacion = GetCell(pGrilla, pFila, "fecha_liquidacion")
  Rem Tipo Movimiento
  lFlg_Tipo_Movimiento = GetCell(pGrilla, pFila, "flg_tipo_movimiento")
  
  Select Case lCod_Instrumento
    Case gcINST_BONOS_NAC
      Set lcBonos = New Class_Bonos
      With lcBonos
        For Each lReg In lRecord_Detalle
          If GetCell(pGrilla, pFila, "flg_tipo_movimiento") = gcTipoOperacion_Ingreso Then
            Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                              pCantidad:=lReg("nominales").Value, _
                                              pTasa:=lReg("tasa").Value, _
                                              PTasa_Gestion:=lReg("tasa").Value, _
                                              pId_Moneda:=lReg("id_moneda").Value, _
                                              pMonto:=lReg("MONTO_OPERACION").Value, _
                                              pUtilidad:=0, _
                                              pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                              pTasa_Historico:=lReg("tasa").Value, _
                                              pId_Mov_Activo_Compra:=Null)
          Else
            If Not Fnt_Enlaza_VentasCompras_RF(pId_Cuenta:=lId_Cuenta, _
                                               pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                               pCantidad:=lReg("nominales").Value, _
                                               pFecha_Movimiento:=lFecha_Movimiento, _
                                               phActivos:=lhActivos) Then
              lMsg_Error = "Problemas en buscar el enlaze para las ventas de RF." & vbCr & .ErrMsg
              GoTo ErrProcedure
            End If
            
            For Each lReg_Activo In lhActivos
              Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                pCantidad:=lReg_Activo("asignado").Value, _
                                                pTasa:=lReg("tasa").Value, _
                                                pId_Moneda:=lReg("id_moneda").Value, _
                                                pMonto:=lReg("MONTO_OPERACION").Value, _
                                                pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                pUtilidad:=0, _
                                                PTasa_Gestion:=lReg("tasa").Value, _
                                                pTasa_Historico:=lReg("tasa").Value, _
                                                pId_Mov_Activo_Compra:=lReg_Activo("Id_Mov_Activo").Value)
            Next
          End If
          
'          lComision = lComision + lReg("COMISION").Value
'          lDerechos = lDerechos + lReg("DERECHO").Value
'          lGastos = lGastos + lReg("GASTOS").Value
'          lIva = lIva + lReg("IVA").Value
          
          Rem No vienen prorrateados
          lComision = lReg("COMISION").Value
          lDerechos = lReg("DERECHO").Value
          lGastos = lReg("GASTOS").Value
          lIva = lReg("IVA").Value
          
          lSuma_Monto_Neto = lSuma_Monto_Neto + lReg("MONTO_OPERACION").Value
  
          Rem Suma de la operacion
          If lFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
            lMonto_Operacion = lSuma_Monto_Neto + lComision + lDerechos + lGastos + lIva
          Else
            lMonto_Operacion = lSuma_Monto_Neto - (lComision + lDerechos + lGastos + lIva)
          End If
                  
          Rem Flag para que no tome en cuenta la cantidad de activos al ingresar una instruccion desde el Importador de movimientos
          Rem y para que ingrese en campos propios de importacion
          .fImportacion = True
          If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                                pId_Cuenta:=lId_Cuenta, _
                                                pDsc_Operacion:="", _
                                                pTipoOperacion:=lFlg_Tipo_Movimiento, _
                                                pId_Contraparte:="", _
                                                pId_Representante:="", _
                                                pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                                pFecha_Operacion:=lFecha_Movimiento, _
                                                pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                pId_Trader:="", _
                                                pPorc_Comision:=lPorcentaje_Comision, _
                                                pComision:=lComision, _
                                                pDerechos_Bolsa:=lDerechos, _
                                                pGastos:=lGastos, _
                                                pIva:=lIva, _
                                                pMonto_Operacion:=lMonto_Operacion, _
                                                pTipo_Precio:=cTipo_Precio_Mercado) Then
              lMsg_Error = "Problemas al grabar Renta Fija." & vbCr & .ErrMsg
              GoTo ErrProcedure
          End If
          
        Next
        
      End With
      
    Case gcINST_DEPOSITOS_NAC
      Set lcDepositos = New Class_Depositos
      With lcDepositos
        For Each lReg In lRecord_Detalle
          If GetCell(pGrilla, pFila, "flg_tipo_movimiento") = gcTipoOperacion_Ingreso Then
            Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                              pCantidad:=lReg("nominales").Value, _
                                              pTasa:=lReg("tasa").Value, _
                                              PTasa_Gestion:=lReg("tasa").Value, _
                                              pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                              pBase:="", _
                                              pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                              pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                              pMonto_Pago:=lReg("MONTO_OPERACION").Value, _
                                              pReferenciado:="F", _
                                              pTipo_Deposito:="", _
                                              pFecha_Valuta:=lFecha_Liquidacion, _
                                              pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                              pTasa_Historico:=lReg("tasa").Value, _
                                              pId_Mov_Activo_Compra:=Null)
          Else
            If Not Fnt_Enlaza_VentasCompras_RF(pId_Cuenta:=lId_Cuenta, _
                                               pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                               pCantidad:=lReg("nominales").Value, _
                                               pFecha_Movimiento:=lFecha_Movimiento, _
                                               phActivos:=lhActivos) Then
              lMsg_Error = "Problemas en buscar el enlaze para las ventas de RF."
              GoTo ErrProcedure
            End If
          
            For Each lReg_Activo In lhActivos
              Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                pCantidad:=lReg_Activo("asignado").Value, _
                                                pTasa:=lReg("tasa").Value, _
                                                PTasa_Gestion:=lReg("tasa").Value, _
                                                pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                                pBase:="", _
                                                pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                                pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                                pMonto_Pago:=lReg("MONTO_OPERACION").Value, _
                                                pReferenciado:="F", _
                                                pTipo_Deposito:="", _
                                                pFecha_Valuta:=lFecha_Liquidacion, _
                                                pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                pTasa_Historico:=lReg("tasa").Value, _
                                                pId_Mov_Activo_Compra:=lReg_Activo("Id_Mov_Activo").Value)
              Next
          End If
          
'          lComision = lComision + lReg("COMISION").Value
'          lDerechos = lDerechos + lReg("DERECHO").Value
'          lGastos = lGastos + lReg("GASTOS").Value
'          lIva = lIva + lReg("IVA").Value
          
          Rem No vienen prorrateados
          lComision = lReg("COMISION").Value
          lDerechos = lReg("DERECHO").Value
          lGastos = lReg("GASTOS").Value
          lIva = lReg("IVA").Value
          
          lSuma_Monto_Neto = lSuma_Monto_Neto + lReg("MONTO_OPERACION").Value
  
          Rem Suma de la operacion
          If lFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
            lMonto_Operacion = lSuma_Monto_Neto + lComision + lDerechos + lGastos + lIva
          Else
            lMonto_Operacion = lSuma_Monto_Neto - (lComision + lDerechos + lGastos + lIva)
          End If
          
          Rem Flag para que no tome en cuenta la cantidad de activos al ingresar una instruccion desde el Importador de movimientos
          Rem y para que ingrese en campos propios de importacion
          .fImportacion = True
          If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                                pId_Cuenta:=lId_Cuenta, _
                                                pDsc_Operacion:="", _
                                                pTipoOperacion:=lFlg_Tipo_Movimiento, _
                                                pId_Contraparte:="", _
                                                pId_Representante:="", _
                                                pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                                pFecha_Operacion:=lFecha_Movimiento, _
                                                pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                pId_Trader:="", _
                                                pPorc_Comision:=lPorcentaje_Comision, _
                                                pComision:=lComision, _
                                                pDerechos_Bolsa:=lDerechos, _
                                                pGastos:=lGastos, _
                                                pIva:=lIva, _
                                                pMonto_Operacion:=lMonto_Operacion) Then
              lMsg_Error = "Problemas al grabar Renta Fija." & vbCr & .ErrMsg
              GoTo ErrProcedure
          End If
          
        Next
        
      End With
        
    Case gcINST_PACTOS_NAC
      Set lcPactos = New Class_Pactos
      With lcPactos
        For Each lReg In lRecord_Detalle
          If GetCell(pGrilla, pFila, "flg_tipo_movimiento") = gcTipoOperacion_Ingreso Then
            Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                              pCantidad:=lReg("nominales").Value, _
                                              pTasa:=lReg("tasa").Value, _
                                              PTasa_Gestion:=lReg("tasa").Value, _
                                              pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                              pBase:="", _
                                              pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                              pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                              pMonto_Pago:=lReg("MONTO_OPERACION").Value, _
                                              pReferenciado:="F", _
                                              pTipo_Deposito:="", _
                                              pFecha_Valuta:=lFecha_Liquidacion, _
                                              pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                              pTasa_Historica:=lReg("tasa").Value, _
                                              pId_Mov_Activo_Compra:=Null)
          Else
            If Not Fnt_Enlaza_VentasCompras_RF(pId_Cuenta:=lId_Cuenta, _
                                               pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                               pCantidad:=lReg("nominales").Value, _
                                               pFecha_Movimiento:=lFecha_Movimiento, _
                                               phActivos:=lhActivos) Then
              lMsg_Error = "Problemas en buscar el enlaze para las ventas de RF."
              GoTo ErrProcedure
            End If
          
            For Each lReg_Activo In lhActivos
              Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                pCantidad:=lReg_Activo("asignado").Value, _
                                                pTasa:=lReg("tasa").Value, _
                                                PTasa_Gestion:=lReg("tasa").Value, _
                                                pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                                pBase:="", _
                                                pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                                pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                                pMonto_Pago:=lReg("MONTO_OPERACION").Value, _
                                                pReferenciado:="F", _
                                                pTipo_Deposito:="", _
                                                pFecha_Valuta:=lFecha_Liquidacion, _
                                                pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                pTasa_Historica:=lReg("tasa").Value, _
                                                pId_Mov_Activo_Compra:=lReg_Activo("Id_Mov_Activo").Value)
              Next
          End If
        
'          lComision = lComision + lReg("COMISION").Value
'          lDerechos = lDerechos + lReg("DERECHO").Value
'          lGastos = lGastos + lReg("GASTOS").Value
'          lIva = lIva + lReg("IVA").Value
          
          Rem No vienen prorrateados
          lComision = lReg("COMISION").Value
          lDerechos = lReg("DERECHO").Value
          lGastos = lReg("GASTOS").Value
          lIva = lReg("IVA").Value
          
          lSuma_Monto_Neto = lSuma_Monto_Neto + lReg("MONTO_OPERACION").Value
  
          Rem Suma de la operacion
          If lFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
            lMonto_Operacion = lSuma_Monto_Neto + lComision + lDerechos + lGastos + lIva
          Else
            lMonto_Operacion = lSuma_Monto_Neto - (lComision + lDerechos + lGastos + lIva)
          End If
          
          Rem Flag para que no tome en cuenta la cantidad de activos al ingresar una instruccion desde el Importador de movimientos
          Rem y para que ingrese en campos propios de importacion
          .fImportacion = True
          If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                                pId_Cuenta:=lId_Cuenta, _
                                                pDsc_Operacion:="", _
                                                pTipoOperacion:=lFlg_Tipo_Movimiento, _
                                                pId_Contraparte:="", _
                                                pId_Representante:="", _
                                                pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                                pFecha_Operacion:=lFecha_Movimiento, _
                                                pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                pId_Trader:="", _
                                                pPorc_Comision:=lPorcentaje_Comision, _
                                                pComision:=lComision, _
                                                pDerechos:=lDerechos, _
                                                pGastos:=lGastos, _
                                                pIva:=lIva, _
                                                pMonto_Operacion:=lMonto_Operacion) Then
              lMsg_Error = "Problemas al grabar Renta Fija." & vbCr & .ErrMsg
              GoTo ErrProcedure
          End If
              
        Next
        
      End With
        
  End Select
  '------------------------------------------------------------------------------
  
  Rem Con el nuevo id_operacion busca los detalles
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Id_Operacion").Valor = lId_Operacion
  
    Rem Busca el detalle de la operacion
    If Not .BuscaConDetalles Then
      lRollback = True
      GoTo ErrProcedure
    End If
    
    Rem Realiza la Confirmaci�n de la Instrucci�n
    If Not .Confirmar(lId_Caja_Cuenta) Then
      lMsg_Error = "Error en la confirmaci�n de la operaci�n." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  
    lFila = pFila
    For Each lDetalle In lcOperaciones.Detalles
      For lFila = lFila To (pGrilla.Rows - 1)
        If GetCell(pGrilla, lFila, "colum_pk") = pNro_Orden Then
          If Not Fnt_Guardar_Rel_Nro_Oper_Detalle(GetCell(pGrilla, lFila, "colum_pk"), _
                                                  lDetalle.Campo("id_operacion_detalle").Valor, _
                                                  lMsg_Error) Then
            GoTo ErrProcedure
          End If
          lFila = lFila + 1
          Exit For
        End If
      Next
    Next
  End With
  '----------------------------------------------------------------------------

  lMsg_Error = "Operaci�n Ingresada correctamente." & vbCr & vbCr & "N�mero Orden: " & pNro_Orden & vbCr & "N�mero Operaci�n CSGPI: " & lId_Operacion
  GoTo ExitProcedure

ErrProcedure:
  lRollback = True
  
ExitProcedure:
  lMsg_Error = "Archivo: " & Fnt_Nombre_Archivo(Txt_ArchivoPlano_RF) & vbCr & vbCr & lMsg_Error
  For lFila = pFila To (pGrilla.Rows - 1)
    If To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pNro_Orden Then
      Call SetCell(pGrilla, lFila, "dsc_error", lMsg_Error)
      
      If lRollback Then
        pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbRed
        'Grilla_Operaciones_Sin_Cuenta_RV.Cell(flexcpChecked, lFila, "chk") = flexChecked
        Call SetCell(pGrilla, lFila, "chk", flexChecked)
      Else
        pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbBlue
        Call SetCell(pGrilla, lFila, "chk", 0)
        'Call SetCell(pGrilla, lFila, "grabado", gcFlg_SI, pAutoSize:=False)
      End If
    End If
  Next
  
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  Set lcBonos = Nothing
  
  Call Fnt_AgregarLog(pId_Log_SubTipo:=eLog_Subtipo.eLS_Importador_Movimientos _
                    , pCod_Estado:=cEstado_Log_Mensaje _
                    , pGls_Log_Registro:=lMsg_Error _
                    , pId_Log_Proceso:=Fnt_Agregar_Log_Proceso(eLog_Subtipo.eLS_Importador_Movimientos))
                    
  Fnt_Ope_Directa_Cont_RF = Not lRollback
  
End Function

Private Function Fnt_Crea_Nemotecnico(pCod_Instrumento As String, _
                                      pRut_Emisor As String, _
                                      pFecha_Emision As Date, _
                                      pFecha_Vencimiento As Date, _
                                      pMoneda_Deposito As String, _
                                      pMoneda_Pago As String, _
                                      pTipo_Reajuste As String, _
                                      ByRef pId_Nemotecnico As String, _
                                      ByRef pNemotecnico As String, _
                                      ByRef pMsg_Error As String) As Boolean
Dim lcEmisor_Especifico As Class_Emisores_Especifico
Dim lCod_Emisor As String
'----------------------------------------------------
Dim lcAlias As Object
Dim lId_Moneda_Tipo_Reajuste
Dim lId_Moneda_Deposito As Double
Dim lId_Moneda_Pago As Double
Dim lId_Nemotecnico As Double
  
  Fnt_Crea_Nemotecnico = True
  
  Rem Busca el Codigo SVS del Emisor
  lCod_Emisor = ""
  Set lcEmisor_Especifico = New Class_Emisores_Especifico
  With lcEmisor_Especifico
    .Campo("RUT").Valor = pRut_Emisor
    If .Buscar() Then
      If .Cursor.Count > 0 Then
        lCod_Emisor = .Cursor(1)("COD_SVS_NEMOTECNICO").Value
      Else
        pMsg_Error = "Rut Emisor '" & pRut_Emisor & "' no est� asociado a ningun Emisor Especifico en el sistema."
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcEmisor_Especifico = Nothing
  
  Rem Se setea el id_moneda de deposito y pago
  lId_Moneda_Deposito = 0
  lId_Moneda_Pago = 0
  Rem Busca el alias de la moneda pago
  lId_Moneda_Tipo_Reajuste = ""
  Set lcAlias = Fnt_CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  lId_Moneda_Tipo_Reajuste = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_BBVA_Gestion_Bolsa _
                                                     , pCodigoCSBPI:=cTabla_Monedas _
                                                     , pValor:=pTipo_Reajuste)
  Set lcAlias = Nothing
  
  If IsNull(lId_Moneda_Tipo_Reajuste) Then
    pMsg_Error = "Alias de Tipo de Reajuste '" & pTipo_Reajuste & "' no est� en el sistema."
    GoTo ErrProcedure
  End If
  
  If pMoneda_Pago = "NR" Then
    lId_Moneda_Pago = 1 ' Peso
  ElseIf pMoneda_Pago = "R" Then
    lId_Moneda_Pago = lId_Moneda_Tipo_Reajuste
  End If
  
  Rem Busca el alias de la moneda deposito
  Set lcAlias = Fnt_CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  lId_Moneda_Deposito = NVL(lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_BBVA_Gestion_Bolsa _
                                                    , pCodigoCSBPI:=cTabla_Monedas _
                                                    , pValor:=pMoneda_Deposito), 0)
  Set lcAlias = Nothing
  
  If lId_Moneda_Deposito = 0 Then
    pMsg_Error = "La Moneda '" & pMoneda_Deposito & "' no es reconocida por el sistema."
    GoTo ErrProcedure
  ElseIf lId_Moneda_Pago = 0 Then
    pMsg_Error = "La Moneda '" & pMoneda_Pago & "' no es reconocida por el sistema."
    GoTo ErrProcedure
  End If
    
  Select Case pCod_Instrumento
    Case gcINST_DEPOSITOS_NAC
      If Not Fnt_CreaNemotecnico_Deposito(pCodigoEmisor:=lCod_Emisor, _
                                          pFecha_Vencimiento:=pFecha_Vencimiento, _
                                          PId_Moneda_Deposito:=lId_Moneda_Deposito, _
                                          pId_Moneda_Pago:=lId_Moneda_Pago, _
                                          pTasa_Emision:=0, _
                                          pFecha_Emision:=pFecha_Emision, _
                                          pId_Nemotecnico:=lId_Nemotecnico, _
                                          pNemotecnico:=pNemotecnico, _
                                          pMensaje:=pMsg_Error) Then
        GoTo ErrProcedure
      End If
      
    Case gcINST_PACTOS_NAC
      If Not Fnt_CreaNemotecnico_Pacto(pCodigoEmisor:=lCod_Emisor, _
                                       pFecha_Vencimiento:=pFecha_Vencimiento, _
                                       PId_Moneda_Deposito:=lId_Moneda_Deposito, _
                                       pId_Moneda_Pago:=lId_Moneda_Pago, _
                                       pTasa_Emision:=0, _
                                       pFecha_Emision:=pFecha_Emision, _
                                       pId_Nemotecnico:=lId_Nemotecnico, _
                                       pNemotecnico:=pNemotecnico, _
                                       pMensaje:=pMsg_Error) Then
        GoTo ErrProcedure
      End If
  End Select
  
  pId_Nemotecnico = lId_Nemotecnico
  
  Exit Function
  
ErrProcedure:
  Fnt_Crea_Nemotecnico = False
End Function

Private Function Fnt_Buscar_Datos_Nemo_RF(pId_Nemotecnico As String, _
                                          ByRef pId_Moneda_Nemo As String, _
                                          ByRef pFecha_Vencimiento As Date, _
                                          ByRef pMsg_Error As String) As Boolean
Dim lcAlias As Class_Alias
Dim lcNemotecnico As Class_Nemotecnicos
  
  Fnt_Buscar_Datos_Nemo_RF = True
  
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pId_Moneda_Nemo = .Cursor(1)("id_moneda_transaccion").Value
        pFecha_Vencimiento = NVL(.Cursor(1)("fecha_vencimiento").Value, Format(Now, cFormatDate))
      Else
        pMsg_Error = "Nemot�cnico no encontrado en el sistema. No se puede operar la instrucci�n."
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = "Problemas en cargar datos de Nemotecnico." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcNemotecnico = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Buscar_Datos_Nemo_RF = False
End Function

Private Function Fnt_Importa_FM() As Boolean
Dim lCampo As hFields
Dim lLinea As Long
Dim lTipo_Movimiento As String
Dim lCod_Instrumento As String
Dim lId_Nemotecnico As String
Dim lId_Cuenta
Dim lAlias_Cta As String
Dim lLinea_Oper_sin_cta As Long
Dim lNum_Cuenta As String
Dim lLinea_Oper_con_cta As Long
Dim lAlias_Fondo As String
Dim lNemotecnico As String
'----------------------------------
Dim lcNemotecnicos As Class_Nemotecnicos
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
  
  Call Sub_Bloquea_Puntero(Me)
  
  Fnt_Importa_FM = True
  
  Grilla_Operaciones_FM.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_FM.Rows = 1
  
  With fMov_Fact_FM
    If Not .Carga_Facturacion_FM Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
    
    If .Cursor.Count = 0 Then
      MsgBox "No hay datos en el Archivo Seleccionado.", vbInformation, Me.Caption
      GoTo ErrProcedure
    End If
    
    For Each lCampo In .Cursor
      lTipo_Movimiento = ""
      If lCampo("TIPOMOV").Value = gcTipoOperacion_Ingreso Then
        lTipo_Movimiento = "Inversion"
      ElseIf lCampo("TIPOMOV").Value = gcTipoOperacion_Rescate Then
        lTipo_Movimiento = "Rescate"
      End If
      '-----------------------------------------------
      Rem Rescata el alias del nemotecnico segun el codigo del fondo
      lNemotecnico = ""
      lCod_Instrumento = ""
      lAlias_Fondo = NVL(lCampo("FONDO").Value, "")
      Set lcAlias = CreateObject(cDLL_Alias)
      Set lcAlias.gDB = gDB
      lId_Nemotecnico = "" & lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_BAC_Fondos_BBVA _
                                                     , pCodigoCSBPI:=cTabla_Nemotecnicos _
                                                     , pValor:=lAlias_Fondo)
      If Not lId_Nemotecnico = "" Then
        '------- RESCATA EL ID_NEMOTECNICO
        Set lcNemotecnicos = New Class_Nemotecnicos
        Rem Busca el id_nemotecnico del Nemotecnico de la planilla
        lcNemotecnicos.Campo("id_nemotecnico").Valor = lId_Nemotecnico
        If lcNemotecnicos.Buscar Then
          If lcNemotecnicos.Cursor.Count > 0 Then
            'Rem Busca atributos del Nemotecnico
            lNemotecnico = NVL(lcNemotecnicos.Cursor(1)("nemotecnico").Value, "")
            lCod_Instrumento = NVL(lcNemotecnicos.Cursor(1)("cod_instrumento").Value, "")
          End If
        End If
        Set lcNemotecnicos = Nothing
      End If
      Set lcAlias = Nothing
      '---------------------------------------------------------------------------------------
      Rem Busca el Alias de la Cuenta por el "Rut Cliente" concatenado con la "Cuenta"
      lAlias_Cta = NVL(lCampo("RUT_CLIENTE").Value, "") & "/" & NVL(lCampo("CUENTA").Value, "")
      Set lcAlias = CreateObject(cDLL_Alias)
      Set lcAlias.gDB = gDB
      lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_BAC_Fondos_BBVA _
                                           , pCodigoCSBPI:=cTabla_Cuentas _
                                           , pValor:=lAlias_Cta)
      If IsNull(lId_Cuenta) Then
          lLinea_Oper_sin_cta = Grilla_Operaciones_Sin_Cuenta_FM.Rows
          Call Grilla_Operaciones_Sin_Cuenta_FM.AddItem("")
          Grilla_Operaciones_Sin_Cuenta_FM.Cell(flexcpChecked, lLinea_Oper_sin_cta, "chk") = flexChecked
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "colum_pk", NVL(lCampo("FOLIO").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "fecha", NVL(lCampo("FECHAMOV").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "rut", NVL(lCampo("RUT_CLIENTE").Value, ""))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "id_cuenta", "")
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "num_cuenta", "")
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "tipo_movimiento", lTipo_Movimiento)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "flg_tipo_movimiento", NVL(lCampo("TIPOMOV").Value, ""))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "id_nemotecnico", lId_Nemotecnico)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "cod_ffmm", lAlias_Fondo)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "fondo_mutuo", lNemotecnico)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "monto_cuotas", lCampo("MONTO_CUOTAS").Value)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "cuota", lCampo("CUOTA").Value)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "monto", NVL(lCampo("MONTO").Value, 0))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "moneda", NVL(lCampo("MONEDA").Value, ""))
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "cod_instru", lCod_Instrumento)
          Call SetCell(Grilla_Operaciones_Sin_Cuenta_FM, lLinea_Oper_sin_cta, "dsc_error", "")
      Else
          ' ----------- RESCATA NUMERO CUENTA
'          Set lcCuenta = New Class_Cuentas
          Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
          lcCuenta.Campo("id_cuenta").Valor = lId_Cuenta
          If lcCuenta.Buscar Then
              lNum_Cuenta = lcCuenta.Cursor(1)("num_cuenta").Value
          Else
          End If
          Set lcCuenta = Nothing
          ' ----------- FIN RESCATA NUMERO CUENTA
          lLinea_Oper_con_cta = Grilla_Operaciones_FM.Rows
          Call Grilla_Operaciones_FM.AddItem("")
          Grilla_Operaciones_FM.Cell(flexcpChecked, lLinea_Oper_con_cta, "chk") = flexChecked
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "colum_pk", NVL(lCampo("FOLIO").Value, 0))
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "fecha", NVL(lCampo("FECHAMOV").Value, 0))
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "rut", NVL(lCampo("RUT_CLIENTE").Value, ""))
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "id_cuenta", lId_Cuenta)
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "num_cuenta", lNum_Cuenta)
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "tipo_movimiento", lTipo_Movimiento)
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "flg_tipo_movimiento", NVL(lCampo("TIPOMOV").Value, ""))
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "id_nemotecnico", lId_Nemotecnico)
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "cod_ffmm", lAlias_Fondo)
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "fondo_mutuo", lNemotecnico)
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "monto_cuotas", lCampo("MONTO_CUOTAS").Value)
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "cuota", lCampo("CUOTA").Value)
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "monto", NVL(lCampo("MONTO").Value, 0))
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "moneda", NVL(lCampo("MONEDA").Value, ""))
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "cod_instru", lCod_Instrumento)
          Call SetCell(Grilla_Operaciones_FM, lLinea_Oper_con_cta, "dsc_error", "")
      End If
    
    Next
  End With
  
  Call Sub_Desbloquea_Puntero(Me)
  Exit Function
  
ErrProcedure:
  Fnt_Importa_FM = False
  Call Sub_Desbloquea_Puntero(Me)
  
End Function

Private Sub Sub_Graba_Operaciones_con_Cta_FM()
Dim lFila As Long
    
  For lFila = 1 To Grilla_Operaciones_FM.Rows - 1
    Rem Busca las operaciones chequeadas
    If Grilla_Operaciones_FM.Cell(flexcpChecked, lFila, Grilla_Operaciones_Sin_Cuenta_FM.ColIndex("chk")) = flexChecked Then
      If Not Fnt_Ope_Directa_Cont_FM(pGrilla:=Grilla_Operaciones_FM, _
                                     pFila:=lFila, _
                                     pFolio:=To_Number(To_Number(GetCell(Grilla_Operaciones_FM, lFila, "colum_pk")))) Then
        'GoTo ErrProcedure
      End If
    End If
  Next lFila
    
  For lFila = 1 To Grilla_Operaciones_Sin_Cuenta_FM.Rows - 1
    Rem Busca las operaciones chequeadas
    If Grilla_Operaciones_Sin_Cuenta_FM.Cell(flexcpChecked, lFila, Grilla_Operaciones_Sin_Cuenta_FM.ColIndex("chk")) = flexChecked Then
      If Not Fnt_Ope_Directa_Cont_FM(pGrilla:=Grilla_Operaciones_Sin_Cuenta_FM, _
                                     pFila:=lFila, _
                                     pFolio:=To_Number(To_Number(GetCell(Grilla_Operaciones_Sin_Cuenta_FM, lFila, "colum_pk")))) Then
        'GoTo ErrProcedure
      End If
    End If
  Next lFila
  
  MsgBox "Grabaci�n de Movimientos Diarios de Fondos Mutuos finalizada.", vbInformation
  
  Tab_Operaciones.TabEnabled(0) = True
  Tab_Operaciones.TabEnabled(1) = True
  
'  With Toolbar_Operaciones_RV
'    .Buttons("SAVE").Enabled = False
'    .Buttons("IMPORT").Enabled = True
'    .Buttons("SEL_ALL").Enabled = False
'    .Buttons("SEL_NOTHING").Enabled = False
'    .Buttons("REFRESH").Enabled = False
'  End With
  
  Call Grilla_Operaciones_FM.Select(0, 0)
  Call Grilla_Operaciones_Sin_Cuenta_FM.Select(0, 0)
  
  'Call Sub_Limpia_Objetos
  
  'Call Toolbar_ButtonClick("REFRESH")
End Sub

Private Function Fnt_Ope_Directa_Cont_FM(pGrilla As VSFlexGrid, ByRef pFila As Long, ByVal pFolio As Long) As Boolean
Dim lFila As Long
Dim lcFondos As Class_FondosMutuos
Dim lId_Nemotecnico As String
Dim lDias_Liquidez As Long
Dim lFecha_Movimiento As Date
Dim lFecha_Liquidacion As Date
Dim lId_Cuenta As String
Dim lId_Moneda As String
Dim lFecha As Date
Dim lMsg_Error As String
Dim lNum_Cuenta As String
Dim lCod_Instrumento As String
Dim lFlg_Tipo_Movimiento As String
'---------------------------------------
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lId_Caja_Cuenta As Double
'---------------------------------------
Dim lId_Operacion As String
Dim lcOperaciones As Class_Operaciones
Dim lDetalle As Class_Operaciones_Detalle
Dim lRollback As Boolean
Dim lMonto_Operacion As Double
'Dim lSuma_Monto_Neto As Double
'---------------------------------------
Dim lcComisiones As Class_Comisiones_Instrumentos
Dim lPorcentaje_Comision As Double
Dim lGastos As Double
Dim lComision As Double
Dim lDerechos As Double
Dim lIva As Double
'---------------------------------------
Dim lRecord_Detalle As hRecord
Dim lReg As hFields
'---------------------------------------
 
  Set lRecord_Detalle = New hRecord
  With lRecord_Detalle
    .ClearFields
    .AddField "ID_NEMOTECNICO", 0
    .AddField "MONTO_CUOTAS", 0
    .AddField "VALOR_CUOTA", 0
    .AddField "MONTO", 0
    .AddField "ID_MONEDA"
    .AddField "FECHA_LIQUIDACION"
    .LimpiarRegistros
  End With

  lRollback = False
  gDB.IniciarTransaccion
  
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("fecha_operacion").Valor = DTP_Fecha_Proceso_FM.Value
    .Campo("cod_producto").Valor = gcPROD_FFMM_NAC
    If Not .Anular_Operaciones_Pendientes Then
      lRollback = True
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcOperaciones = Nothing
    
  lFila = pFila
  lId_Cuenta = GetCell(pGrilla, pFila, "id_cuenta")
  lNum_Cuenta = GetCell(pGrilla, pFila, "num_cuenta")
  
  Rem Ingresa el movimiento
  Do While To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pFolio
    lId_Nemotecnico = GetCell(pGrilla, lFila, "id_nemotecnico")
    lFecha = GetCell(pGrilla, lFila, "fecha")
    '-----------------------------------------------------------------------------------------
    Rem VALIDACIONES
    lMsg_Error = ""
    Rem Valida que no se est� ingresando la operacion de nuevo
    If Not Fnt_Buscar_Rel_Nro_Oper_Detalle(GetCell(pGrilla, lFila, "colum_pk"), lMsg_Error) Then
      lMsg_Error = "El N�mero de Folio '" & GetCell(pGrilla, lFila, "colum_pk") & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
      GoTo ErrProcedure
    Rem La Fecha que viene en el Archivo Plano tiene q ser igual a la Fecha Proceso
    ElseIf Not lFecha = DTP_Fecha_Proceso_FM.Value Then
      lMsg_Error = "La Fecha '" & lFecha & "' no corresponde a la Fecha de Proceso seleccionada." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Si el Id_Cuenta y Num_cuenta es vacio quiere decir que no tiene asociada una cuenta en GPI
    ElseIf lId_Cuenta = "" And lNum_Cuenta = "" Then
      lMsg_Error = "La Orden no tiene asociado una cuenta en el sistema. El campo ""Cuenta"" est� vacio." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Si el id_nemotecnico es "" quiere decir que no se encontr� el nemotecnico en GPI
    ElseIf lId_Nemotecnico = "" Then
      lMsg_Error = "El Nemot�cnico '" & GetCell(pGrilla, lFila, "fondo_mutuo") & "' no est� en el sistema." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Busca la moneda del nemotecnico en CSGPI
    ElseIf Not Fnt_Buscar_Datos_Nemo_FM(lId_Nemotecnico, lId_Moneda, lDias_Liquidez, lMsg_Error) Then
      GoTo ErrProcedure
    End If
    '-----------------------------------------------------------------------------
    Set lReg = lRecord_Detalle.Add
    lReg("ID_NEMOTECNICO").Value = lId_Nemotecnico
    lReg("MONTO_CUOTAS").Value = To_Number(GetCell(pGrilla, lFila, "monto_cuotas"))
    lReg("VALOR_CUOTA").Value = To_Number(GetCell(pGrilla, lFila, "cuota"))
    lReg("MONTO").Value = To_Number(GetCell(pGrilla, lFila, "monto"))
    lReg("ID_MONEDA").Value = lId_Moneda
    
    lFecha_Liquidacion = lFecha
    If lFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
      lFecha_Liquidacion = Fnt_Calcula_Dia_Habil(lFecha, lDias_Liquidez)
    End If
      
    lReg("FECHA_LIQUIDACION").Value = lFecha_Liquidacion
    lFila = lFila + 1
    
    If lFila > pGrilla.Rows - 1 Then
      Exit Do
    End If
  Loop

  Rem Tipo Movimiento
  If GetCell(pGrilla, pFila, "flg_tipo_movimiento") = cTipo_Ingreso Then
    lFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso
  ElseIf GetCell(pGrilla, pFila, "flg_tipo_movimiento") = cTipo_Rescate Then
    lFlg_Tipo_Movimiento = gcTipoOperacion_Egreso
  End If
  
  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("id_moneda").Valor = lId_Moneda
    .Campo("cod_mercado").Valor = cMercado_Nacional
    
    If .Buscar_Caja_Para_Invertir() Then
      lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
    Else
      lMsg_Error = "Problemas en cargar Cajas de la Cuenta." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  '-----------------------------------------------------------------------------

  lFecha_Movimiento = DTP_Fecha_Proceso_FM.Value
  'lFecha_Liquidacion = GetCell(pGrilla, pFila, "FECHA_LIQUIDACION")
  lCod_Instrumento = GetCell(pGrilla, pFila, "cod_instru")
  
  Set lcFondos = New Class_FondosMutuos
  With lcFondos
    For Each lReg In lRecord_Detalle
      Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                        pcuota:=lReg("monto_cuotas").Value, _
                                        pPrecio:=lReg("valor_cuota").Value, _
                                        pId_Moneda:=lReg("id_moneda").Value, _
                                        pMonto:=lReg("monto").Value, _
                                        pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                        pFecha_Liquidacion:=lReg("fecha_liquidacion").Value, _
                                        pPrecio_Historico:="")
      Rem Se suman los montos netos de cada detalle
      lMonto_Operacion = lMonto_Operacion + lReg("monto").Value
    Next
    
    Rem Comisiones
    Set lcComisiones = New Class_Comisiones_Instrumentos
    With lcComisiones
      .Campo("Id_Cuenta").Valor = lId_Cuenta
      .Campo("COD_INSTRUMENTO").Valor = lCod_Instrumento
      If .Buscar(True) Then
        If .Cursor.Count > 0 Then
          lPorcentaje_Comision = NVL(.Cursor(1)("COMISION").Value, 0)
        End If
      End If
    End With
    Set lcComisiones = Nothing
    '-----------------------------------------------------------------------------
    Rem Flag para que no tome en cuenta la cantidad de activos al ingresar una instruccion desde el Importador de movimientos
    Rem y para que ingrese en campos propios de importacion
    .fImportacion = True
    If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                          pId_Cuenta:=lId_Cuenta, _
                                          pDsc_Operacion:="", _
                                          pTipoOperacion:=lFlg_Tipo_Movimiento, _
                                          pId_Contraparte:="", _
                                          pId_Representante:="", _
                                          pId_Moneda_Operacion:=lId_Moneda, _
                                          pFecha_Operacion:=lFecha_Movimiento, _
                                          pFecha_Vigencia:=lFecha_Movimiento, _
                                          pFecha_Liquidacion:=lFecha_Liquidacion, _
                                          pId_Trader:="", _
                                          pPorc_Comision:=lPorcentaje_Comision, _
                                          pComision:=lComision, _
                                          pDerechos_Bolsa:=lDerechos, _
                                          pGastos:=lGastos, _
                                          pIva:=lIva, _
                                          pMonto_Operacion:=lMonto_Operacion, _
                                          pTipo_Precio:=cTipo_Precio_Mercado, _
                                          pCod_Instrumento:=lCod_Instrumento) Then
      lMsg_Error = "Problemas al grabar Fondos Mutuos Nacionales." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcFondos = Nothing
  '----------------------------------------------------------------------------

  Rem Con el nuevo id_operacion busca los detalles
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Id_Operacion").Valor = lId_Operacion
  
    Rem Busca el detalle de la operacion
    If Not .BuscaConDetalles Then
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  
    Rem Realiza la Confirmaci�n de la Instrucci�n
    If Not .Confirmar(lId_Caja_Cuenta) Then
      lMsg_Error = "Error en la confirmaci�n de la operaci�n." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  
    Rem Por cada id_operacion_detalle encontrado guarda la relacion
    Rem "nro_orden-id_operacion_detalle" en la tabla rel_conversiones
    For Each lDetalle In lcOperaciones.Detalles
      If Not Fnt_Guardar_Rel_Nro_Oper_Detalle(GetCell(pGrilla, pFila, "colum_pk"), _
                                              lDetalle.Campo("id_operacion_detalle").Valor, _
                                              lMsg_Error) Then
        GoTo ErrProcedure
      End If
    Next
  End With
    
  Rem Setea la pFila para que siga con la siguiente linea de la grilla
'  pFila = lFila - 1
  lMsg_Error = "Operaci�n Ingresada correctamente." & vbCr & vbCr & "N�mero Folio: " & pFolio & vbCr & "N�mero Operaci�n CSGPI: " & lId_Operacion
  GoTo ExitProcedure
  
ErrProcedure:
  lRollback = True
  
ExitProcedure:
  lFila = pFila
  lMsg_Error = "Archivo: " & Fnt_Nombre_Archivo(Txt_ArchivoPlano_FM) & vbCr & vbCr & lMsg_Error
  Do While To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pFolio
    Call SetCell(pGrilla, lFila, "dsc_error", "")
    Call SetCell(pGrilla, lFila, "dsc_error", lMsg_Error)
    
    If lRollback Then
      pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbRed
      'Grilla_Operaciones_Sin_Cuenta_RV.Cell(flexcpChecked, lFila, "chk") = flexChecked
      Call SetCell(pGrilla, lFila, "chk", flexChecked)
    Else
      pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbBlue
      Call SetCell(pGrilla, lFila, "chk", 0)
    End If
    
    lFila = lFila + 1
    
    If lFila > pGrilla.Rows - 1 Then
      Exit Do
    End If
  Loop
  
  Rem Setea la pFila para que siga con la siguiente Orden de la grilla
  pFila = lFila - 1
  
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  Set lcFondos = Nothing
  
  Call Fnt_AgregarLog(pId_Log_SubTipo:=eLog_Subtipo.eLS_Importador_Movimientos _
                    , pCod_Estado:=cEstado_Log_Mensaje _
                    , pGls_Log_Registro:=lMsg_Error _
                    , pId_Log_Proceso:=Fnt_Agregar_Log_Proceso(eLog_Subtipo.eLS_Importador_Movimientos))
                    
  Fnt_Ope_Directa_Cont_FM = Not lRollback
  
End Function

Private Function Fnt_Buscar_Datos_Nemo_FM(pId_Nemotecnico As String, _
                                          ByRef pId_Moneda_Nemo As String, _
                                          ByRef pDias_Liquidez As Long, _
                                          ByRef pMsg_Error As String) As Boolean
Dim lcAlias As Class_Alias
Dim lcNemotecnico As Class_Nemotecnicos
  
  Fnt_Buscar_Datos_Nemo_FM = True
  
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pId_Moneda_Nemo = .Cursor(1)("id_moneda_transaccion").Value
        pDias_Liquidez = NVL(.Cursor(1)("DIAS_LIQUIDEZ").Value, 0)
      Else
        pMsg_Error = "Nemot�cnico no encontrado en el sistema. No se puede operar la instrucci�n."
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = "Problemas en cargar datos de Nemotecnico." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcNemotecnico = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Buscar_Datos_Nemo_FM = False
End Function

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_Sin_Cuenta_FM.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_Sin_Cuenta_FM)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_Sin_Cuenta_FM)
End Sub

Private Sub Sub_Check_DesCheck_Mismo_Nro_Orden(pGrilla As VSFlexGrid)
Dim lFil_grilla As Integer
Dim lCol_grilla As Integer
Dim lLinea As Integer
Dim lNro_Orden As Long
    
    lFil_grilla = pGrilla.Row
    lCol_grilla = pGrilla.Col
    If lCol_grilla = 0 And lFil_grilla > 0 Then
        If Val(GetCell(pGrilla, lFil_grilla, "chk")) = 0 Then
            lNro_Orden = Val(GetCell(pGrilla, lFil_grilla, "colum_pk"))
            For lLinea = 1 To pGrilla.Rows - 1
                If lNro_Orden = Val(GetCell(pGrilla, lLinea, "colum_pk")) Then
                    pGrilla.TextMatrix(lLinea, lCol_grilla) = flexChecked
                End If
            Next
        Else
            lNro_Orden = Val(GetCell(pGrilla, lFil_grilla, "colum_pk"))
            For lLinea = 1 To pGrilla.Rows - 1
                If lNro_Orden = Val(GetCell(pGrilla, lLinea, "colum_pk")) Then
                    pGrilla.TextMatrix(lLinea, lCol_grilla) = 0
                End If
            Next
        End If
    End If
End Sub

Private Sub Sub_Mensaje_Error(pGrilla As VSFlexGrid)
Dim lMensaje As String

  lMensaje = GetCell(pGrilla, pGrilla.Row, "dsc_error")
  If Not lMensaje = "" Then
    MsgBox lMensaje, vbInformation
  End If
End Sub

Private Sub Toolbar_Selc_Con_Cta_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_RV, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_RV, False)
  End Select
End Sub

Private Sub Toolbar_Selc_Con_Cta_FM_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_FM, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_FM, False)
  End Select
End Sub

Private Sub Toolbar_Selc_Con_Cta_RF_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_RF, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_RF, False)
  End Select
End Sub

Private Sub Toolbar_Selc_Sin_Cta_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RV, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RV, False)
  End Select
End Sub

Private Sub Toolbar_Selc_Sin_Cta_FM_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_FM, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_FM, False)
  End Select
End Sub

Private Sub Toolbar_Selc_Sin_Cta_RF_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RF, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RF, False)
  End Select
End Sub
