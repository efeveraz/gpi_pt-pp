VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_AporteRescate_Fechas_Anteriores 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Aportes/Rescates Fechas Anteriores"
   ClientHeight    =   5130
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8325
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5130
   ScaleWidth      =   8325
   Begin VB.Frame Frame1 
      Height          =   4755
      Left            =   75
      TabIndex        =   0
      Top             =   370
      Width           =   8235
      Begin VB.Frame Frame2 
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1125
         Left            =   120
         TabIndex        =   26
         Top             =   120
         Width           =   7905
         Begin hControl2.hTextLabel Txt_Rut 
            Height          =   315
            Left            =   90
            TabIndex        =   1
            Top             =   300
            Width           =   3990
            _ExtentX        =   7038
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   1200
            Caption         =   "RUT"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Nombres 
            Height          =   315
            Left            =   90
            TabIndex        =   3
            Top             =   660
            Width           =   7725
            _ExtentX        =   13626
            _ExtentY        =   556
            LabelWidth      =   1300
            Caption         =   "Nombres"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Perfil 
            Height          =   315
            Left            =   4140
            TabIndex        =   2
            Top             =   300
            Width           =   3660
            _ExtentX        =   6456
            _ExtentY        =   556
            LabelWidth      =   1000
            TextMinWidth    =   1200
            Caption         =   "Perfil Riesgo"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
      End
      Begin hControl2.hTextLabel Txt_FechaMovimiento 
         Height          =   315
         Left            =   5280
         TabIndex        =   5
         Top             =   1350
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Fecha"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin VB.Frame Frame_Bancario 
         Enabled         =   0   'False
         Height          =   1245
         Left            =   90
         TabIndex        =   18
         Top             =   3400
         Width           =   8085
         Begin hControl2.hTextLabel Txt_Num_Documento 
            Height          =   345
            Left            =   4230
            TabIndex        =   16
            Top             =   300
            Width           =   3750
            _ExtentX        =   6615
            _ExtentY        =   609
            LabelWidth      =   1300
            Caption         =   "N� Documento"
            Text            =   ""
            MaxLength       =   19
         End
         Begin TrueDBList80.TDBCombo Cmb_Bancos 
            Height          =   345
            Left            =   1440
            TabIndex        =   17
            Top             =   690
            Width           =   2565
            _ExtentX        =   4524
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_AporteRescate_Fechas_Anteriores.frx":0000
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Cta_Cte 
            Height          =   345
            Left            =   1440
            TabIndex        =   15
            Top             =   270
            Width           =   2565
            _ExtentX        =   4524
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_AporteRescate_Fechas_Anteriores.frx":00AA
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Label4 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Banco"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   20
            Top             =   690
            Width           =   1305
         End
         Begin VB.Label Label2 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Cuenta Corriente"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   19
            Top             =   270
            Width           =   1305
         End
      End
      Begin hControl2.hTextLabel Txt_Cuenta 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Top             =   1350
         Width           =   3885
         _ExtentX        =   6853
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Cuentas"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Observaci�n 
         Height          =   315
         Left            =   120
         TabIndex        =   11
         Tag             =   "OBLI"
         Top             =   2670
         Width           =   7995
         _ExtentX        =   14102
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Observaci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   119
      End
      Begin hControl2.hTextLabel Txt_SaldoCaja 
         Height          =   315
         Left            =   120
         TabIndex        =   9
         Top             =   2310
         Width           =   3870
         _ExtentX        =   6826
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Saldo Caja"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Dias_Retencion 
         Height          =   315
         Left            =   4350
         TabIndex        =   13
         Tag             =   "OBLI"
         Top             =   3030
         Width           =   2010
         _ExtentX        =   3545
         _ExtentY        =   556
         LabelWidth      =   1500
         TextMinWidth    =   500
         Caption         =   "Fecha Liquidaci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Moneda 
         Height          =   315
         Left            =   5280
         TabIndex        =   8
         Top             =   1920
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Moneda"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Monto 
         Height          =   315
         Left            =   4350
         TabIndex        =   10
         Tag             =   "OBLI"
         Top             =   2310
         Width           =   3765
         _ExtentX        =   6641
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Monto"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin TrueDBList80.TDBCombo Cmb_Medios_Pago 
         Height          =   345
         Left            =   1440
         TabIndex        =   12
         Tag             =   "OBLI=S;CAPTION=Medio de pago"
         Top             =   3030
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_AporteRescate_Fechas_Anteriores.frx":0154
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Cajas 
         Height          =   345
         Left            =   1440
         TabIndex        =   7
         Tag             =   "OBLI=S;CAPTION=Caja"
         Top             =   1920
         Width           =   3795
         _ExtentX        =   6694
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_AporteRescate_Fechas_Anteriores.frx":01FE
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComCtl2.DTPicker Dtp_Fecha 
         Height          =   315
         Left            =   6600
         TabIndex        =   6
         Tag             =   "OBLI"
         Top             =   1350
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   60358657
         CurrentDate     =   38768
      End
      Begin MSComCtl2.DTPicker dtp_fecha_liquidacion 
         Height          =   315
         Left            =   6360
         TabIndex        =   14
         Tag             =   "OBLI"
         Top             =   3030
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   60358657
         CurrentDate     =   38768
      End
      Begin VB.Label lbl_fecha 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5280
         TabIndex        =   25
         Top             =   1350
         Width           =   1305
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Caja"
         Height          =   345
         Left            =   120
         TabIndex        =   22
         Top             =   1920
         Width           =   1305
      End
      Begin VB.Label Label3 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Medio de pago"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         TabIndex        =   21
         Top             =   3030
         Width           =   1305
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   8325
      _ExtentX        =   14684
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   24
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_AporteRescate_Fechas_Anteriores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Private fTipo_Movimiento As String
Private fEstado As String

Dim fId_Cuenta As Double

Dim fFlg_Tipo_Permiso           As String
Dim fFlg_Mov_Descubiertos       As String
Dim fFlg_Fechas_Anteriores      As Boolean

Private Sub Cmb_Cajas_LostFocus()
    Dim lCaja_Cuenta    As Class_Cajas_Cuenta
    Dim lId_Caja_Cuenta As String
    Dim lReg            As hFields

    lId_Caja_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cajas)
  
    If Not lId_Caja_Cuenta = "" Then
        Set lCaja_Cuenta = New Class_Cajas_Cuenta
        With lCaja_Cuenta
            .Campo("id_caja_cuenta").Valor = lId_Caja_Cuenta

          If .Buscar(True) Then
            For Each lReg In .Cursor
              Txt_Moneda.Text = lReg("dsc_moneda").Value
              Txt_Monto.Format = Fnt_Formato_Moneda(lReg("id_moneda").Value)
              Txt_SaldoCaja.Format = Txt_Monto.Format
            Next
          Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en cargar la 'Moneda', el 'Saldo Caja' y el 'Monto' de la Caja seleccionada.", _
                            .ErrMsg, _
                            pConLog:=True)
          End If
             
          Txt_SaldoCaja.Text = .Saldo_Disponible(DTP_Fecha.Value)
        End With
    Set lCaja_Cuenta = Nothing
  End If
End Sub

Private Sub Cmb_Cta_Cte_LostFocus()
Dim lcClientes_Ctas_Ctes As Object 'Class_Clientes_Ctas_Ctes
Dim lId_Cta_Cte_Cliente As String
  
  lId_Cta_Cte_Cliente = Fnt_ComboSelected_KEY(Cmb_Cta_Cte)
  
  If Not lId_Cta_Cte_Cliente = "" Then
    Set lcClientes_Ctas_Ctes = Fnt_CreateObject(cDLL_Clientes_Ctas_Ctes) 'New Class_Clientes_Ctas_Ctes
    With lcClientes_Ctas_Ctes
      Set .gDB = gDB
      .Campo("ID_CTA_CTE_CLIENTE").Valor = Fnt_ComboSelected_KEY(Cmb_Cta_Cte)
      If Not .Buscar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                        , "Problemas en cargar la 'Cuenta Corriente' de la Cuenta." _
                        , .ErrMsg _
                        , pConLog:=True)
        Exit Sub
      End If
      
      If .Cursor.Count > 0 Then
        Call Sub_ComboSelectedItem(Cmb_Bancos, .Cursor(1)("ID_BANCO").Value)
      End If
    End With
  End If
End Sub

Private Sub Cmb_Medios_Pago_ItemChange()
    ' Frame_Bancario.Enabled = (Fnt_FindValue4Display(Cmb_Medios_Pago, Cmb_Medios_Pago.Text) = "CHE")
    Dim sCodMedioPago As String
    
    sCodMedioPago = Fnt_FindValue4Display(Cmb_Medios_Pago, Cmb_Medios_Pago.Text)
    Frame_Bancario.Enabled = (sCodMedioPago <> "CASH")
    
End Sub

Private Sub DTP_Fecha_Change()
Dim lCaja_Cuenta    As Class_Cajas_Cuenta
Dim lId_Caja_Cuenta As String

  lId_Caja_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cajas)
  dtp_fecha_liquidacion.MinDate = DTP_Fecha.Value
  dtp_fecha_liquidacion.Value = DTP_Fecha.Value
  Txt_Dias_Retencion.Text = DateDiff("d", DTP_Fecha.Value, dtp_fecha_liquidacion.Value)
  
  If Not lId_Caja_Cuenta = "" Then
    Set lCaja_Cuenta = New Class_Cajas_Cuenta
    lCaja_Cuenta.Campo("id_caja_cuenta").Valor = lId_Caja_Cuenta
    Txt_SaldoCaja.Text = lCaja_Cuenta.Saldo_Disponible(DTP_Fecha.Value)
    Set lCaja_Cuenta = Nothing
  End If
  
End Sub

Private Sub dtp_fecha_liquidacion_Change()
    Txt_Dias_Retencion.Text = DateDiff("d", DTP_Fecha.Value, dtp_fecha_liquidacion.Value)
End Sub

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
    
  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
          Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub
Private Function Fnt_Grabar() As Boolean
    Dim MedioPago       As String
    Dim lClase          As Class_APORTE_RESCATE_CUENTA
    Dim sRespuesta      As VbMsgBoxResult
    
    MedioPago = Fnt_ComboSelected_KEY(Cmb_Medios_Pago)
    Fnt_Grabar = True
  
    If Not Fnt_ValidarDatos Then
        Fnt_Grabar = False
        Exit Function
    End If
    
    Set lClase = New Class_APORTE_RESCATE_CUENTA
    
    With lClase
        .Campo("id_Apo_Res_Cuenta").Valor = fKey
        .Campo("cod_Medio_Pago").Valor = MedioPago
        
        If Fnt_ComboSelected_KEY(Cmb_Bancos) = "" Then
            .Campo("id_Banco").Valor = Null
        Else
            .Campo("id_Banco").Valor = Fnt_ComboSelected_KEY(Cmb_Bancos)
        End If
        
        'If Trim(MedioPago) = "CHE" Then
        '    .Campo("id_Banco").Valor = Fnt_ComboSelected_KEY(Cmb_Bancos)
        'Else
        '    .Campo("id_Banco").Valor = Null
        'End If
        
        .Campo("id_Caja_Cuenta").Valor = Fnt_ComboSelected_KEY(Cmb_Cajas)
        .Campo("id_Cuenta").Valor = fId_Cuenta
        .Campo("dsc_Apo_Res_Cuenta").Valor = Txt_Observaci�n.Text
        .Campo("flg_Tipo_Movimiento").Valor = fTipo_Movimiento
        .Campo("fecha_Movimiento").Valor = DTP_Fecha.Value 'IIf(fKey = cNewEntidad, Dtp_Fecha.value, Fnt_String2Date(Txt_FechaMovimiento.Text))
        .Campo("num_Documento").Valor = Txt_Num_Documento.Text
        .Campo("retencion").Valor = Txt_Dias_Retencion.Text
        .Campo("monto").Valor = Txt_Monto.Text
        .Campo("cta_Cte_Bancaria").Valor = Cmb_Cta_Cte.Text
        
        If .Guardar Then
            fKey = .Campo("id_Apo_Res_Cuenta").Valor
            
            If fTipo_Movimiento = gcTipoOperacion_Rescate Then
                sRespuesta = MsgBox("Rescate guardado correctamente." & vbCrLf & _
                                    "� Desea Imprimir la papeleta ?", _
                                    vbYesNo + vbQuestion, "Rescate")
            Else
                sRespuesta = MsgBox("Aporte guardado correctamente." & vbCrLf & _
                                    "� Desea Imprimir la papeleta ?", _
                                    vbYesNo + vbQuestion, "Aporte")
            End If
            
            If sRespuesta = vbYes Then
                ImprimeDocWord fKey
            End If
            
        Else
            Fnt_Grabar = False
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al grabar el " & IIf(fTipo_Movimiento = gcTipoOperacion_Rescate, "Rescate", "Aporte") & "." _
                        , .ErrMsg _
                        , True)
                        
        End If
        
    End With
    
    Set lClase = Nothing
    
End Function

'---------------------------------------------------------------------------------------
' Procedure : ImprimeDocWord
' Author    : Administrador
' Date      : 27/06/2008
' Purpose   : Imprimir el documento para folios
'---------------------------------------------------------------------------------------
Public Sub ImprimeDocWord(ByVal iIdKey As String, Optional pTipo As String = "A")
    Dim DocWord         As New Class_Comprobante_Aporte_Rescate
    '-----------------------------------------------------------
    Dim lCodEjecutivo   As String
    Dim lCodSucursal    As String
    Dim iIdComprobante  As String
    Dim lestado         As String
    Dim lNomEjecutivo   As String
    
    Dim lReg            As hFields
    '-----------------------------------------------------------
    Dim lNemotecnico    As String
    Dim lCantidad       As String
    Dim lPrecio         As String
    Dim lTotal          As String
    '-----------------------------------------------------------
    Dim lFormato        As String
    Dim nTotalDocumento As Double
    
    ' DocWord.sPlantilla = "ComprobanteRetiroCapital.doc"
    Dim lForm       As Frm_Reporte_Generico
    Set lForm = New Frm_Reporte_Generico

    lForm.fTipoSalida = ePrinter.eP_Impresora
    lForm.VSPDF8.Title = "Comprobante Aporte-Rescate"
    
    Dim NUM_FOLIO As String
    Dim TEXTO_APO_RET As String
    Dim Fecha As String
    Dim NOM_CLIENTE As String
    Dim rut_cliente As String
    Dim Direccion As String
    Dim Num_Cuenta As String
    Dim REPLEGAL As String
    Dim FECHACONTRATO As String
    Dim ESTADOCUENTA As String
    Dim OBSERVACION As String
    Dim NOM_EJECUTIVO As String
    Dim Tipo_Movimiento As String
    Dim tipo_entidad As String
    
    With lForm.VsPrinter
        If Not DocWord.Buscar_ComprobantesPorIDOperacion(iIdKey, pTipo) Then
            GoTo Fin
        End If
        '---------------------------------------------------------------------------
        ' Agrega los nombres de TAG y el valor.
        '---------------------------------------------------------------------------
        For Each lReg In DocWord.Cursor
            NUM_FOLIO = lReg("NUMERO_FOLIO").Value
            TEXTO_APO_RET = lReg("TIPO_MOVIMIENTO").Value
            Fecha = lReg("FECHA").Value
            NOM_CLIENTE = lReg("NOMBRE_CLIENTE").Value
            rut_cliente = lReg("RUT_CLIENTE").Value
            Direccion = lReg("direccion").Value
            Num_Cuenta = lReg("num_cuenta").Value
            REPLEGAL = lReg("Representante").Value
            FECHACONTRATO = lReg("fecha_operativa").Value
            ESTADOCUENTA = IIf(Trim(lReg("estadocontrato").Value) = "H", "VIGENTE", "NO VIGENTE")
            OBSERVACION = NVL(lReg("observacion").Value, "")
            tipo_entidad = lReg("tipo_entidad").Value
            
            lestado = lReg("COD_ESTADO").Value
            Tipo_Movimiento = Trim(lReg("TIPO_MOVIMIENTO").Value)

            lCodEjecutivo = NVL(lReg("COD_EJECUTIVO").Value, 0)
            lCodSucursal = NVL(lReg("COD_SUCURSAL").Value, 0)
            
            lFormato = Fnt_Formato_Moneda(lReg("ID_MONEDA").Value)
            
            NOM_EJECUTIVO = lReg("NOMBRE_ASESOR").Value
            
            DocWord.Campo("ID_COMPROBANTE").Valor = lReg("ID_COMPROBANTE").Value
        Next
        .Font.Name = "Arial"
        .Font.Size = 10
        
        .Orientation = orLandscape

        .SpaceBefore = 0
        .SpaceAfter = 0
        .Clear
        .Orientation = orPortrait
        .StartDoc
        .FontBold = True
        .FontSize = 12
        .TextAlign = taRightTop
        .Paragraph = "N� Folio    " & NUM_FOLIO
        .TextAlign = taCenterTop
        .Paragraph = "COMPROBANTE DE " & Tipo_Movimiento & " CAPITAL " & IIf(lestado = "ANULADO", "(ANULADO)", "")
        .TextAlign = taLeftTop
        .FontBold = False
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .FontSize = 10
        .Paragraph = "FECHA" & vbTab & vbTab & ":   " & Fecha
        .Paragraph = "CLIENTE" & vbTab & ":   " & NOM_CLIENTE
        .Paragraph = "RUT" & vbTab & vbTab & ":   " & rut_cliente
        .Paragraph = "EJECUTIVO" & vbTab & ":   " & NOM_EJECUTIVO
        .Paragraph = "DIRECCION" & vbTab & ":   " & Direccion
        .Paragraph = "CUENTA" & vbTab & ":   " & Num_Cuenta
        If tipo_entidad = "J" Then
            .Paragraph = "REP. LEGAL" & vbTab & ":   " & REPLEGAL
        Else
            .Paragraph = ""
        End If
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = "Sr. Cliente se informa a Ud., que con esta fecha se han " & IIf(Trim(Tipo_Movimiento) = "RETIRO", "entregado", "recibido") & " por concepto de " & Tipo_Movimiento & " de Capital los instrumentos que se detallan, los cuales en este  acto " & IIf(Trim(Tipo_Movimiento) = "RETIRO", "dejan de", "pasan a") & " formar parte integra de vuestro patrimonio administrado por " & gDsc_Empresa & "."
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .StartTable
            .TableBorder = tbBox
            .TableCell(tcCols) = 4
            .TableCell(tcRows) = 1
            .TableCell(tcAlign, 1, 1) = taCenterMiddle
            .TableCell(tcAlign, 1, 2) = taCenterMiddle
            .TableCell(tcAlign, 1, 3) = taCenterMiddle
            .TableCell(tcAlign, 1, 4) = taCenterMiddle
            .TableBorder = tbAll
            .TableCell(tcFontSize, 1, 1, 1, 1) = 10
            .TableCell(tcFontBold, 1, 1, 1, 4) = True
            .TableCell(tcFontBold, 2, 1, 2, 4) = False
            
            .TableCell(tcText, 1, 1) = "INSTRUMENTO"
            .TableCell(tcText, 1, 2) = "CANTIDAD"
            .TableCell(tcText, 1, 3) = "PRECIO"
            .TableCell(tcText, 1, 4) = "TOTAL"
            .TableCell(tcColWidth, 1, 1) = "55mm"
            .TableCell(tcColWidth, 1, 2) = "35mm"
            .TableCell(tcColWidth, 1, 3) = "35mm"
            .TableCell(tcColWidth, 1, 4) = "35mm"
            .FontBold = False

            If DocWord.Buscar_DetalleComprobante() Then
                For Each lReg In DocWord.Cursor
                    .TableCell(tcRows) = DocWord.Cursor.Count + 2
                    .TableCell(tcText, lReg.Index + 1, 1) = lReg("NEMOTECNICO").Value
                    .TableCell(tcText, lReg.Index + 1, 2) = FormatNumber(CDbl(lReg("CANTIDAD").Value), 4)
                    .TableCell(tcText, lReg.Index + 1, 3) = FormatNumber(CDbl(NVL(lReg("PRECIO").Value, 0)), 4)
                    .TableCell(tcText, lReg.Index + 1, 4) = Format(lReg("MONTO").Value, lFormato)
                    .TableCell(tcAlign, lReg.Index + 1, 1) = taLeftMiddle
                    .TableCell(tcAlign, lReg.Index + 1, 2, lReg.Index + 1, 4) = taRightMiddle
    
                    nTotalDocumento = nTotalDocumento + lReg("MONTO").Value
                Next
                .TableCell(tcText, DocWord.Cursor.Count + 2, 4) = Format(nTotalDocumento, lFormato)
                .TableCell(tcAlign, DocWord.Cursor.Count + 2, 4) = taRightMiddle
            Else
                .TableCell(tcRows) = 2
            End If
        .EndTable
        
        .FontBold = False
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = "Atentamente,"
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .StartTable
            .TableBorder = tbNone
            .TableCell(tcCols) = 2
            .TableCell(tcRows) = 2
            .TableCell(tcColAlign, 1, 1) = taCenterMiddle
            .TableCell(tcColAlign, 1, 2) = taCenterMiddle
            .TableCell(tcColAlign, 2, 1) = taCenterMiddle
            .TableCell(tcColAlign, 2, 2) = taCenterMiddle
            .TableCell(tcFontSize, 1, 1, 1, 1) = 10
            .TableCell(tcFontBold, 1, 1, 2, 2) = True
            .TableCell(tcColWidth, 1, 1) = "80mm"
            .TableCell(tcColWidth, 1, 2) = "80mm"
            .TableCell(tcText, 1, 1) = "_______________________________________"
            .TableCell(tcText, 1, 2) = "_______________________________________"
            .TableCell(tcText, 2, 1) = "p." & NOM_CLIENTE
            .TableCell(tcText, 2, 2) = "p." & gDsc_Empresa
        .EndTable

        .TextAlign = taLeftTop
        .FontBold = False
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = "FECHA CONTRATO" & vbTab & vbTab & ":   " & FECHACONTRATO
        .Paragraph = "ESTADO" & vbTab & vbTab & vbTab & ":   " & ESTADOCUENTA
        .Paragraph = "OBSERVACION CLIENTE" & vbTab & ":   " & OBSERVACION

        .EndDoc
    End With
    
Fin:

    Set DocWord = Nothing
End Sub

Private Sub Sub_CargaForm()
  
  Call Sub_Bloquea_Puntero(Me)
  
  fKey = cNewEntidad
  
  Call Sub_FormControl_Color(Me.Controls)
  Call Sub_CargaCombo_Cajas_Cuenta(Cmb_Cajas, fId_Cuenta)
  Call Sub_CargaCombo_Medios_Pago(Cmb_Medios_Pago)
  Call Sub_CargaCombo_Bancos(Cmb_Bancos)
  
  DTP_Fecha.Value = Fnt_FechaServidor
  dtp_fecha_liquidacion.Value = Fnt_FechaServidor
  Txt_Dias_Retencion.Text = "0"
   
  Call Sub_Desbloquea_Puntero(Me)
  
  Call Cmb_Medios_Pago_ItemChange
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lAporte_Rescate_Cuenta As Class_APORTE_RESCATE_CUENTA
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
  
  Load Me
  
  'Se tiene que cargar aca debido a que todavia no se sabe ID que le corresponde
  Call Sub_CargaCombo_Clientes_Ctas_Ctes_Cta(Cmb_Cta_Cte, fId_Cuenta)
  
  fEstado = ""
  Set lAporte_Rescate_Cuenta = New Class_APORTE_RESCATE_CUENTA
  With lAporte_Rescate_Cuenta
    .Campo("id_Apo_Res_Cuenta").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        fId_Cuenta = lReg("id_cuenta").Value
          
        Call Sub_ComboSelectedItem(Cmb_Medios_Pago, lReg("cod_medio_pago").Value)
        
        Call Sub_ComboSelectedItem(Cmb_Bancos, NVL(lReg("id_banco").Value, ""))
        
        Call Sub_ComboSelectedItem(Cmb_Cajas, lReg("Id_Caja_Cuenta").Value)
        
        Txt_Observaci�n.Text = lReg("dsc_Apo_Res_Cuenta").Value
        fTipo_Movimiento = lReg("flg_Tipo_Movimiento").Value
        Txt_Num_Documento.Text = "" & lReg("num_Documento").Value
        Txt_Dias_Retencion.Text = lReg("retencion").Value
        Txt_Monto.Text = lReg("monto").Value
        Cmb_Cta_Cte.Text = "" & lReg("cta_Cte_Bancaria").Value
        fEstado = lReg("cod_estado").Value
        DTP_Fecha.Value = Format(lReg("fecha_movimiento").Value, cFormatDate)
        dtp_fecha_liquidacion.Value = DateAdd("d", Txt_Dias_Retencion.Text, DTP_Fecha.Value)
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG _
                        , "Problemas al buscar el Aporte/Rescate." _
                        , .ErrMsg _
                        , pConLog:=True)
    End If
  End With
  Set lAporte_Rescate_Cuenta = Nothing
  
  Call Cmb_Cajas_LostFocus
  Call Cmb_Medios_Pago_ItemChange
  
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cuenta").Valor = fId_Cuenta
    .Campo("id_Empresa").Valor = Fnt_EmpresaActual
    If .Buscar_Vigentes Then
      If .Cursor.Count > 0 Then
        Txt_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("dsc_cuenta").Value
        
        Txt_Rut.Text = .Cursor(1)("rut_cliente").Value
        Txt_Perfil.Text = .Cursor(1)("DSC_PERFIL_RIESGO").Value
        Txt_Nombres.Text = .Cursor(1)("nombre_cliente").Value
        
        fFlg_Mov_Descubiertos = .Cursor(1)("FLG_MOV_DESCUBIERTOS").Value
      End If
    Else
      Call Fnt_MsgError(.SubTipo_LOG _
                        , "Problemas con la busqueda de los datos de la cuenta (" & fId_Cuenta & ")." _
                        , .ErrMsg _
                        , pConLog:=True)
    End If
  End With
  Set lcCuenta = Nothing
  
End Sub
Private Function Fnt_ValidarDatos() As Boolean
    Dim lCaja_Cuenta    As Class_Cajas_Cuenta
    Dim lId_Caja_Cuenta As String
    Dim lSaldo_Caja     As Double
    Dim sCodMedioPago   As String
'-------------------------------------------------
    Dim lResult As Boolean

    Select Case fEstado
        Case cCod_Estado_Liquidado, cCod_Estado_Anulado
            Fnt_ValidarDatos = False
            MsgBox "Solo se pueden modificar cuando est�n ""Pendientes"" de Liquidaci�n.", vbExclamation, Me.Caption
            Exit Function
    End Select
  
    lResult = Fnt_Form_Validar(Me.Controls)
  
    If lResult Then
        If To_Number(Txt_Monto.Text) <= 0 Then
            MsgBox "Debe ingresar un monto v�lido.", vbExclamation, Me.Caption
            lResult = False
            Txt_Monto.SetFocus
            Fnt_ValidarDatos = False
            Exit Function
        End If
        
        If Fnt_ComboSelected_KEY(Cmb_Medios_Pago) <> "CASH" Then
            sCodMedioPago = Fnt_ComboSelected_KEY(Cmb_Medios_Pago)
                           
            If sCodMedioPago = "CAR_CTA" Then
                If Not fTipo_Movimiento = gcTipoOperacion_Aporte Then
'                    If Fnt_ComboSelected_KEY(Cmb_Bancos) = "" Then
'                        MsgBox "Al elegir el aporte en """ & Cmb_Medios_Pago.Text & """ debe ingresar el banco.", vbExclamation, Me.Caption
'                        lResult = False
'                        Cmb_Bancos.SetFocus
'                        Fnt_ValidarDatos = lResult
'                        Exit Function
'
'
'                    ElseIf Trim(Cmb_Cta_Cte.Text) = "" Then
'                        MsgBox "Al elegir el aporte en """ & Cmb_Medios_Pago.Text & """ debe ingresar la cuenta corriente.", vbExclamation, Me.Caption
'                        lResult = False
'                        Cmb_Cta_Cte.SetFocus
'                        Fnt_ValidarDatos = lResult
'                        Exit Function
'
'                    End If
                    
'                Else
                    lResult = False
                    MsgBox "Los """ & Cmb_Medios_Pago.Text & """ son solo para aportes.", vbExclamation, Me.Caption
                    Cmb_Medios_Pago.SetFocus
                    Fnt_ValidarDatos = lResult
                    Exit Function
                End If
                
            ElseIf (sCodMedioPago = "TRAS_FON" Or sCodMedioPago = "ABO_CTA") Then
                If Not fTipo_Movimiento = gcTipoOperacion_Rescate Then
'                    If Fnt_ComboSelected_KEY(Cmb_Bancos) = "" Then
'                        MsgBox "Al elegir el rescate """ & Cmb_Medios_Pago.Text & """ debe ingresar el banco.", vbExclamation, Me.Caption
'                        lResult = False
'                        Cmb_Bancos.SetFocus
'                        Fnt_ValidarDatos = lResult
'                        Exit Function
'                    ElseIf Trim(Cmb_Cta_Cte.Text) = "" Then
'                        MsgBox "Al elegir el rescate """ & Cmb_Medios_Pago.Text & """ debe ingresar la cuenta corriente.", vbExclamation, Me.Caption
'                        lResult = False
'                        Cmb_Cta_Cte.SetFocus
'                        Fnt_ValidarDatos = lResult
'                        Exit Function
'                    End If
'                Else
                    lResult = False
                    MsgBox "Los """ & Cmb_Medios_Pago.Text & """ son solo para Rescates.", vbExclamation, Me.Caption
                    Cmb_Medios_Pago.SetFocus
                    Fnt_ValidarDatos = lResult
                    Exit Function
                End If
            End If
        End If
        
        
        If fTipo_Movimiento = gcTipoOperacion_Rescate Then  'Si es un rescate
            Rem Se debe revisar el saldo de caja.
            
            lId_Caja_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cajas)
            lSaldo_Caja = 0
            
            Set lCaja_Cuenta = New Class_Cajas_Cuenta
            With lCaja_Cuenta
              .Campo("id_caja_cuenta").Valor = lId_Caja_Cuenta
              lSaldo_Caja = .Saldo_Disponible(DTP_Fecha.Value)
            End With
            
            If To_Number(Txt_Monto.Text) > lSaldo_Caja Then
                If fFlg_Mov_Descubiertos = gcFlg_SI Then
                    If MsgBox("El monto ingresado sobrepasa el saldo de la caja." & vbLf & vbLf & _
                          "Saldo : " & Format(lSaldo_Caja, "#,##0.00") & vbLf & _
                          "Monto : " & Format(To_Number(Txt_Monto.Text), "#,##0.00") & vbLf & "�Desea Continuar?" _
                        , vbYesNo + vbQuestion _
                      , Me.Caption) = vbNo Then
                      lResult = False
                    End If
                Else
                    MsgBox "El monto ingresado sobrepasa el saldo de la caja." & vbLf & vbLf & _
                          "Saldo : " & Format(lSaldo_Caja, "#,##0.00") & vbLf & _
                          "Monto : " & Format(To_Number(Txt_Monto.Text), "#,##0.00") _
                          , vbExclamation
                    lResult = False
                End If
            End If
            
            Set lCaja_Cuenta = Nothing
        End If
        
    End If
  
  Fnt_ValidarDatos = lResult
End Function

Public Function Fnt_Modificar(pkey, pTipo_Movimiento, pId_Cuenta, pCod_Arbol_Sistema, pFlg_Fechas_Anteriores)
  fId_Cuenta = pId_Cuenta
  fFlg_Fechas_Anteriores = pFlg_Fechas_Anteriores
  
  Load Me
  
  fKey = pkey
  fTipo_Movimiento = pTipo_Movimiento
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Call Sub_CargarDatos
    
  If Not fFlg_Fechas_Anteriores Then
    DTP_Fecha.Visible = False
    Txt_FechaMovimiento.Visible = True
    Txt_FechaMovimiento.Text = DTP_Fecha.Value
  Else
    DTP_Fecha.Visible = True
    Txt_FechaMovimiento.Visible = False
    'DTP_Fecha.MaxDate = DTP_Fecha.Value
  End If
  dtp_fecha_liquidacion.MinDate = DTP_Fecha.Value
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de "
  Else
    Me.Caption = "Modificaci�n del "
    'DTP_Fecha.Enabled = False
    DTP_Fecha.Visible = True
    Txt_FechaMovimiento.Visible = True
    Txt_FechaMovimiento.Text = DTP_Fecha.Value
  End If
  
  Select Case pTipo_Movimiento
    Case gcTipoOperacion_Aporte
      Me.Caption = Me.Caption & "Aporte"
    Case gcTipoOperacion_Rescate
      Me.Caption = Me.Caption & "Rescate"
  End Select
    
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function
Private Sub Txt_Dias_Retencion_LostFocus()
    dtp_fecha_liquidacion.Value = DateAdd("d", Txt_Dias_Retencion.Text, DTP_Fecha.Value)
End Sub
Private Sub Txt_Monto_Validate(Cancel As Boolean)
  If Txt_Monto.Text < 0 Then
    Cancel = True
    MsgBox "No se permiten valores negativos.", vbExclamation, Me.Caption
  End If
End Sub
