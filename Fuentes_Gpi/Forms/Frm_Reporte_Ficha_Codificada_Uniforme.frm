VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Ficha_Codificada_Uniforme 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ficha Estad�stica Codificada Uniforme"
   ClientHeight    =   9045
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14880
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9045
   ScaleWidth      =   14880
   Begin VB.Frame Frm_Fecha_Re_Proceso 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   795
      Left            =   -15
      TabIndex        =   2
      Top             =   390
      Width           =   14895
      Begin MSComCtl2.DTPicker DTP_Fecha_Consulta 
         Height          =   315
         Left            =   1350
         TabIndex        =   3
         Top             =   320
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   56164353
         CurrentDate     =   37732
      End
      Begin TrueDBList80.TDBCombo Cmb_Empresa 
         Height          =   345
         Left            =   3960
         TabIndex        =   4
         Tag             =   "SOLOLECTURA=N"
         Top             =   320
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Ficha_Codificada_Uniforme.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Lbl_Fecha_Consulta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Consulta"
         Height          =   345
         Left            =   90
         TabIndex        =   6
         Top             =   320
         Width           =   1215
      End
      Begin VB.Label lblInstrumento 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Empresa"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2880
         TabIndex        =   5
         Top             =   320
         Width           =   1065
      End
   End
   Begin VB.Frame Frm_Cuentas 
      Caption         =   "Flujos de Caja"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7725
      Left            =   -15
      TabIndex        =   0
      Top             =   1200
      Width           =   14895
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
         Height          =   7275
         Left            =   60
         TabIndex        =   1
         Top             =   240
         Width           =   14625
         _cx             =   25797
         _cy             =   12832
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         SelectionMode   =   1
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   24
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Reporte_Ficha_Codificada_Uniforme.frx":00AA
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   0
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   14880
      _ExtentX        =   26247
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "SEARCH"
            Description     =   "Busca Cuentas"
            Object.ToolTipText     =   "Carga en Grilla las Cuentas del Nemot�cnico"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORT"
            Object.ToolTipText     =   "Genera reporte de Rentabilidad por Perfil"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   1
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   8
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Ficha_Codificada_Uniforme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: Frm_Reporte_Ficha_Codificada_Uniforme.frm $
'    $Author: Gbuenrostro $
'    $Date: 26-02-14 19:29 $
'    $Revision: 6 $
'-------------------------------------------------------------------------------------------------

Dim fFlg_Tipo_Permiso As String 'Flags para el permiqso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana
Public fApp_Excel           As Excel.Application      ' Excel application
Public fLibro               As Excel.Workbook      ' Excel workbook

Dim iFilaExcel    As Integer
Dim sFormato As String
Dim iTotalColExcel As Integer
Dim lCursor_FlujoCaja    As hRecord

Public Sub Mostrar(pCod_Arbol_Sistema)
    fCod_Arbol_Sistema = pCod_Arbol_Sistema
'  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
    fFlg_Tipo_Permiso = Fnt_Obtiene_Permiso_Formulario(pCod_Arbol_Sistema)
 
    Call Form_Resize
    
    Load Me
End Sub




Private Sub Form_Load()
   Me.Top = 1
   Me.Left = 1
   With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SEARCH").Image = cBoton_Buscar
        .Buttons("REPORT").Image = cBoton_Modificar
        .Buttons("REFRESH").Image = cBoton_Original
        .Buttons("EXIT").Image = cBoton_Salir
   End With

   Call Sub_CargaForm
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEARCH"
        If ValidaEntradaDatos Then
            Call Sub_CargarDatos
        End If
    Case "REFRESH"
      Call Sub_Limpiar
    Case "REPORT"
      If Me.Grilla_Cuentas.Rows > 1 Then
        Call Sub_Crea_Excel
      End If
    Case "EXIT"
      Unload Me
  End Select

End Sub
Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "EXCEL"
      If Me.Grilla_Cuentas.Rows > 1 Then
        Call Sub_Crea_Excel
      End If
  End Select
End Sub
Private Function ValidaEntradaDatos() As Boolean
Dim bOk As Boolean

    bOk = True
    
    'If Trim(Txt_Num_Cuenta.Text) = "" And chk_TodasCtas.Value = 0 Then
    '   MsgBox "Debe seleccionar una cuenta o marcar ""Todas"".", vbExclamation, Me.Caption
    '   bOk = False
    'End If
  
    ValidaEntradaDatos = bOk
End Function
Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_FormControl_Color(Me.Controls)
    
    Call Sub_CargaCombo_Usuario_Empresas(Cmb_Empresa, pTodos:=True)
    Call Sub_ComboSelectedItem(Cmb_Empresa, cCmbKALL)
    
    Grilla_Cuentas.GridLines = flexGridNone
    Call ConfiguraFormulario
    Grilla_Cuentas.Rows = 2
    
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Consulta.Value = lCierre.Busca_Ultima_FechaCierre
    Set lCierre = Nothing
    
    sFormato = "#,##0.0000"
    
    Cmb_Empresa.Enabled = True

    Call Sub_Desbloquea_Puntero(Me)
End Sub

Public Sub ConfiguraFormulario()
  With Me.Grilla_Cuentas
    .AddItem "Cartera" & vbTab _
             & "Empresa" & vbTab _
             & "Nro. Cuenta" & vbTab _
             & "Cuenta" & vbTab _
             & "Renta Variable Nacional" & vbTab _
             & "Renta Variable Nacional" & vbTab _
             & "Renta Variable Nacional" & vbTab _
             & "Renta Fija Nacional" & vbTab _
             & "Renta Fija Nacional" & vbTab _
             & "Renta Fija Nacional" & vbTab _
             & "Renta Fija Nacional" & vbTab _
             & "Renta Fija Nacional" & vbTab _
             & "Renta Fija Nacional" & vbTab _
             & "Renta Fija Internacional" & vbTab _
             & "Renta Fija Internacional" & vbTab _
             & "Renta Variable Internacional" & vbTab _
             & "Renta Variable Internacional" & vbTab _
             & "Otros Activos" & vbTab _
             & "Cuentas por Cobrar" & vbTab _
             & "Cuentas por Pagar" & vbTab _
             & "Total Caja" & vbTab _
             & "Total Activos" & vbTab _
             & "Total Promedio Mensual Activos" & vbTab _
             & "Total Patrimonio", 0
             
    .MergeCells = flexMergeFixedOnly
    ' flexMergeRestrictAll
    .MergeCol(0) = True
    .MergeCol(1) = True
    .MergeCol(2) = True
    .MergeCol(3) = True  'Agregado MMA 12/11/2008
    .MergeCol(18) = True
    .MergeCol(19) = True
    .MergeCol(20) = True
    .MergeCol(21) = True
    .MergeCol(22) = True
    .MergeCol(23) = True
    
    .MergeRow(0) = True
    .MergeRow(1) = True
    
    .FixedRows = 2
  End With
End Sub
Private Sub Form_Resize()
   Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Sub_Limpiar()
Dim lCierre As Class_Verificaciones_Cierre
Dim i As Integer

    Call Sub_ComboSelectedItem(Cmb_Empresa, cCmbKALL)
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Consulta.Value = lCierre.Busca_Ultima_FechaCierre
    Grilla_Cuentas.Rows = 1
    For i = 10 To 12
        Grilla_Cuentas.ColHidden(i) = False
    Next
End Sub
Private Sub Sub_CargarDatos()
Dim lCursor                 As Object
Dim lReg                    As Object
Dim lLinea                  As Integer
Dim dTotalValor             As Double
Dim dTotalCantidad          As Double
Dim iDecimales              As Integer
Dim sFechaDesde             As String
Dim sFechaHasta             As String
Dim lId_Cuenta              As Integer
Dim lId_Asesor              As Integer
Dim lMoneda_Cuenta          As Integer
Dim sTituloT0               As String
Dim sTituloT1               As String
Dim sTituloT2               As String
Dim lDecimalesCuenta        As Integer
Dim dTotalT0                As Double
Dim dTotalT1                As Double
Dim dTotalT2                As Double
'---------------------------------------------
Dim lCajas_Ctas             As Class_Cajas_Cuenta
Dim lReg_Caj                As hFields
Dim lCursor_Caj             As hRecord
Dim dSaldoCaja              As Double
Dim lCaja                   As Integer
Dim lCol                    As Integer
Dim sNombre_Col             As String
Dim sSimbolo                As String
Dim nDecimalesCaja          As Integer
Dim nombre_col              As String
Dim lOtrasCajasActivas      As Integer
Dim i As Integer
'---------------------------------------------

    Call Sub_Bloquea_Puntero(Me)
    Grilla_Cuentas.Rows = 2
    Dim lfecha As Date
    lfecha = Me.DTP_Fecha_Consulta.Value
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_REPORTE_FICHA_ESTAD�STICA_CODIFICADA_UNIFORME$Buscar"
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pFecha", ePT_Fecha, lfecha, ePD_Entrada
    If Fnt_ComboSelected_KEY(Cmb_Empresa) <> cCmbKALL Then
      gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_ComboSelected_KEY(Cmb_Empresa), ePD_Entrada
    End If
    If Not gDB.EjecutaSP Then
        MsgBox "Error " & gDB.ErrMsg
        Exit Sub
    End If
       
    Set lCursor_FlujoCaja = Nothing
    Set lCursor_FlujoCaja = gDB.Parametros("Pcursor").Valor
    If lCursor_FlujoCaja.Count > 0 Then
        For Each lReg In lCursor_FlujoCaja
            lLinea = Grilla_Cuentas.Rows
            Call Grilla_Cuentas.AddItem("")
            Call SetCell(Grilla_Cuentas, lLinea, "CARTERA", Trim(lReg("CARTERA").Value), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "DSC_EMPRESA", lReg("DSC_EMPRESA").Value, pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "NUM_CUENTA", lReg("NUM_CUENTA").Value, pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "DSC_CUENTA", lReg("DSC_CUENTA").Value, pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RV_ACCIONES", FormatNumber(NVL(lReg("RV_ACCIONES").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RV_FONDOS_MUTUOS", FormatNumber(NVL(lReg("RV_FONDOS_MUTUOS").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RV_FONDOS_INVERSI�N", FormatNumber(NVL(lReg("RV_FONDOS_INVERSI�N").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RF_REAJUSTE_USD", FormatNumber(NVL(lReg("RF_REAJUSTE_USD").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RF_REAJUSTE_UF", FormatNumber(NVL(lReg("RF_REAJUSTE_UF").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RF_REAJUSTE_PES", FormatNumber(NVL(lReg("RF_REAJUSTE_PES").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RF_FFMM_CPLAZO", FormatNumber(NVL(lReg("RF_FFMM_CPLAZO").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RF_FFMM_LPLAZO", FormatNumber(NVL(lReg("RF_FFMM_LPLAZO").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RF_FFMM_ESTRUC", FormatNumber(NVL(lReg("RF_FFMM_ESTRUC").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RF_BONOS_INT", FormatNumber(NVL(lReg("RF_BONOS_INT").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RF_FFMM_INT", FormatNumber(NVL(lReg("RF_FFMM_INT").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RV_ACCIONES_INT", FormatNumber(NVL(lReg("RV_ACCIONES_INT").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "RV_FFMM_INT", FormatNumber(NVL(lReg("RV_FFMM_INT").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "FFMM", FormatNumber(NVL(lReg("FFMM").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "CTAS_X_COBRAR", FormatNumber(NVL(lReg("CTAS_X_COBRAR").Value, 0), 0), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "CTAS_X_PAGAR", FormatNumber(NVL(lReg("CTAS_X_PAGAR").Value, 0), 0), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "SALDO_CAJA", FormatNumber(NVL(lReg("SALDO_CAJA").Value, 0), 0), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "TOTAL_ACTIVOS", FormatNumber(NVL(lReg("TOTAL_ACTIVOS").Value, 0), 0), pAutoSize:=False)
            'Call SetCell(Grilla_Cuentas, lLinea, "TOTAL_PROM_MENSUAL", FormatNumber(NVL(lReg("TOTAL_PROM_MENSUAL").Value, 0), 0), pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "TOTAL_PATRIMONIO", FormatNumber(NVL(lReg("TOTAL_PATRIMONIO").Value, 0), 0), pAutoSize:=False)
        Next
    End If
    Me.Grilla_Cuentas.AutoResize = True
    Me.Grilla_Cuentas.AutoSizeMode = flexAutoSizeColWidth
ExitProcedure:
    Set lCursor_Caj = Nothing
    'Set lCursor_FlujoCaja = Nothing
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Crea_Excel()
Dim nHojas, i       As Integer
Dim hoja            As Integer
Dim Index           As Integer
    
    Set fApp_Excel = CreateObject("Excel.application")
    fApp_Excel.DisplayAlerts = False
    Set fLibro = fApp_Excel.Workbooks.Add
    
    fApp_Excel.ActiveWindow.DisplayGridlines = False
  
    ' App_Excel.Visible = True
    Call Sub_Bloquea_Puntero(Me)
    
    fLibro.Worksheets.Add
   
    With fLibro
        For i = .Worksheets.Count To 2 Step -1
            .Worksheets(i).Delete
        Next i
        .Sheets(1).Select
        .ActiveSheet.Pictures.Insert(App.Path & "\Logo Security.jpg").Select
            
        .ActiveSheet.Range("A5").Value = "Ficha Estad�stica Codificada Uniforme"
        
        .ActiveSheet.Range("A5:E5").Font.Bold = True
        .ActiveSheet.Range("A5:E5").HorizontalAlignment = xlLeft
        .ActiveSheet.Range("A5:E5").Merge
        
        .ActiveSheet.Range("A8").Value = "Fecha Consulta   " & DTP_Fecha_Consulta.Value
        
        .ActiveSheet.Range("A8:B8").HorizontalAlignment = xlLeft
        .ActiveSheet.Range("A8:B8").Font.Bold = True
        .ActiveSheet.Range("A8:E8").Merge

        .Worksheets.Item(1).Columns("A:Z").ColumnWidth = 150
        .Worksheets.Item(1).Name = "Ficha Estad�stica Codificada"
    End With
    
    iFilaExcel = 10
    
    Generar_Listado_Excel
    fLibro.ActiveSheet.Columns("A:A").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fApp_Excel.Selection.EntireColumn.AutoFit
    'fLibro.ActiveSheet.Columns("N:Q").Select
    fLibro.Worksheets.Item(1).Columns("N:Q").ColumnWidth = 25
    fApp_Excel.Visible = True
    fApp_Excel.UserControl = True
    fLibro.ActiveSheet.Range("A1").Select
    
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub


Private Sub Generar_Listado_Excel()
Dim lReg                 As hCollection.hFields

Dim lxHoja               As Worksheet
'----------------------------------------------------
Dim bImprimioTitulo      As Boolean
Dim lFilaGrilla          As Integer
Dim iFilaInicio          As Integer
Dim iColGrilla           As Integer
Dim iColExcel            As Integer
Dim lIdCuenta            As String
Dim dTotalCantidad       As Double
Dim dTotalValor          As Double
Dim sNombre_Col          As String
Dim iTotalColGrilla      As Integer
Dim sValor               As String
Dim nDecimales           As Integer
Dim sFormatoValorMercado As String
Dim dblTotal             As Double
Dim sFechaDesde          As String
Dim sFechaHasta          As String
Dim lId_Cuenta           As Integer
Dim lId_Asesor           As Integer
Dim lMoneda_Cuenta       As Integer
Dim iColFinal            As Integer
Dim sFormatoCaja         As String
Dim i                    As Integer
Dim sTituloT0            As String
Dim sTituloT1            As String
Dim sTituloT2            As String
Dim lDecimalesCuenta     As Integer
Dim dTotalT0             As Double
Dim dTotalT1             As Double
Dim dTotalT2             As Double
'---------------------------------------------
Dim lCajas_Ctas          As Class_Cajas_Cuenta
Dim lReg_Caj             As hFields
Dim lCursor_Caj          As hRecord
Dim dSaldoCaja           As Double
Dim lCaja                As Integer
Dim lCol                 As Integer
Dim sSimbolo             As String
Dim nDecimalesCaja       As Integer
Dim nombre_col           As String
Dim lOtrasCajasActivas   As Integer
'---------------------------------------------
    
Dim RV_ACCIONES As Double
Dim RV_FONDOS_MUTUOS As Double
Dim RV_FONDOS_INVERSI�N As Double
Dim RF_REAJUSTE_USD As Double
Dim RF_REAJUSTE_UF As Double
Dim RF_REAJUSTE_PES As Double
Dim RF_FFMM_CPLAZO As Double
Dim RF_FFMM_LPLAZO As Double
Dim RF_FFMM_ESTRUC As Double
Dim RF_BONOS_INT As Double
Dim RF_FFMM_INT As Double
Dim RV_ACCIONES_INT As Double
Dim RV_FFMM_INT As Double
Dim FFMM As Double
Dim TOTAL_ACTIVOS As Double
Dim CTAS_X_COBRAR As Double
Dim CTAS_X_PAGAR As Double
Dim SALDO_CAJA As Double
'Dim TOTAL_PROM_MENSUAL As Double
Dim TOTAL_PATRIMONIO As Double
    
    RV_ACCIONES = 0
    RV_FONDOS_MUTUOS = 0
    RV_FONDOS_INVERSI�N = 0
    RF_REAJUSTE_USD = 0
    RF_REAJUSTE_UF = 0
    RF_REAJUSTE_PES = 0
    RF_FFMM_CPLAZO = 0
    RF_FFMM_LPLAZO = 0
    RF_FFMM_ESTRUC = 0
    FFMM = 0
    TOTAL_ACTIVOS = 0
    CTAS_X_COBRAR = 0
    CTAS_X_PAGAR = 0
    SALDO_CAJA = 0
    'Dim TOTAL_PROM_MENSUAL =0
    TOTAL_PATRIMONIO = 0
    
    'On Error GoTo ErrProcedure
    
    
    bImprimioTitulo = False
    iColExcel = 1
    dTotalT0 = 0
    dTotalT1 = 0
    dTotalT2 = 0
    iTotalColGrilla = Grilla_Cuentas.Cols
    
    Set lxHoja = fLibro.ActiveSheet
        
    If Not bImprimioTitulo Then
        Call ImprimeEncabezado(sTituloT0, sTituloT1, sTituloT2)
        bImprimioTitulo = True
        iFilaInicio = iFilaExcel + 1
    End If
        
'    gDB.Parametros.Clear
'    gDB.Procedimiento = "PKG_REPORTE_FICHA_ESTAD�STICA_CODIFICADA_UNIFORME$Buscar"
'    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'    gDB.Parametros.Add "pFecha", ePT_Caracter, Me.DTP_Fecha_Consulta.Value, ePD_Entrada
'    If Fnt_ComboSelected_KEY(Cmb_Empresa) <> cCmbKALL Then
'      gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_ComboSelected_KEY(Cmb_Empresa), ePD_Entrada
'    End If
'    If Not gDB.EjecutaSP Then
'        MsgBox "Error " & gDB.ErrMsg
'        Exit Sub
'    End If
        
    iFilaInicio = iFilaExcel
    'Set lCursor_FlujoCaja = gDB.Parametros("Pcursor").Valor
    If lCursor_FlujoCaja.Count > 0 Then
        With lxHoja
            For Each lReg In lCursor_FlujoCaja
                iColExcel = 1
                iFilaExcel = iFilaExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("CARTERA").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("DSC_EMPRESA").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = "'" & lReg("NUM_CUENTA").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("DSC_CUENTA").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RV_ACCIONES").Value, 0), 0)
                RV_ACCIONES = RV_ACCIONES + NVL(lReg("RV_ACCIONES").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RV_FONDOS_MUTUOS").Value, 0), 0) 'sformato
                RV_FONDOS_MUTUOS = RV_FONDOS_MUTUOS + NVL(lReg("RV_FONDOS_MUTUOS").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RV_FONDOS_INVERSI�N").Value, 0), 0)
                RV_FONDOS_INVERSI�N = RV_FONDOS_INVERSI�N + NVL(lReg("RV_FONDOS_INVERSI�N").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RF_REAJUSTE_USD").Value, 0), 0)
                RF_REAJUSTE_USD = RF_REAJUSTE_USD + NVL(lReg("RF_REAJUSTE_USD").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RF_REAJUSTE_UF").Value, 0), 0)
                RF_REAJUSTE_UF = RF_REAJUSTE_UF + NVL(lReg("RF_REAJUSTE_UF").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RF_REAJUSTE_PES").Value, 0), 0)
                RF_REAJUSTE_PES = RF_REAJUSTE_PES + NVL(lReg("RF_REAJUSTE_PES").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RF_FFMM_CPLAZO").Value, 0), 0)
                RF_FFMM_CPLAZO = RF_FFMM_CPLAZO + NVL(lReg("RF_FFMM_CPLAZO").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RF_FFMM_LPLAZO").Value, 0), 0)
                RF_FFMM_LPLAZO = RF_FFMM_LPLAZO + NVL(lReg("RF_FFMM_LPLAZO").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RF_FFMM_ESTRUC").Value, 0), 0)
                RF_FFMM_ESTRUC = RF_FFMM_ESTRUC + NVL(lReg("RF_FFMM_ESTRUC").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RF_BONOS_INT").Value, 0), 0)
                RF_BONOS_INT = RF_BONOS_INT + NVL(lReg("RF_BONOS_INT").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RF_FFMM_INT").Value, 0), 0)
                RF_FFMM_INT = RF_FFMM_INT + NVL(lReg("RF_FFMM_INT").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RV_ACCIONES_INT").Value, 0), 0)
                RV_ACCIONES_INT = RV_ACCIONES_INT + NVL(lReg("RV_ACCIONES_INT").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("RV_FFMM_INT").Value, 0), 0)
                RV_FFMM_INT = RV_FFMM_INT + NVL(lReg("RV_FFMM_INT").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("FFMM").Value, 0), 0)
                FFMM = FFMM + NVL(lReg("FFMM").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("CTAS_X_COBRAR").Value, 0), 0)
                CTAS_X_COBRAR = CTAS_X_COBRAR + NVL(lReg("CTAS_X_COBRAR").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("CTAS_X_PAGAR").Value, 0), 0)
                CTAS_X_PAGAR = CTAS_X_PAGAR + NVL(lReg("CTAS_X_PAGAR").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("SALDO_CAJA").Value, 0), 0)
                SALDO_CAJA = SALDO_CAJA + NVL(lReg("SALDO_CAJA").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("TOTAL_ACTIVOS").Value, 0), 0)
                TOTAL_ACTIVOS = TOTAL_ACTIVOS + NVL(lReg("TOTAL_ACTIVOS").Value, 0)
'                iColExcel = iColExcel + 1
'                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("TOTAL_PROM_MENSUAL").Value, 0), 0)
'                TOTAL_PROM_MENSUAL = TOTAL_PROM_MENSUAL + NVL(lReg("TOTAL_PROM_MENSUAL").Value, 0)
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(lReg("TOTAL_PATRIMONIO").Value, 0), 0)
                TOTAL_PATRIMONIO = TOTAL_PATRIMONIO + NVL(lReg("TOTAL_PATRIMONIO").Value, 0)
            Next
            iColExcel = 1
            iFilaExcel = iFilaExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = "TOTALES"
            
            iColExcel = 5
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RV_ACCIONES, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RV_FONDOS_MUTUOS, 0), 0) 'sformato
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RV_FONDOS_INVERSI�N, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RF_REAJUSTE_USD, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RF_REAJUSTE_UF, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RF_REAJUSTE_PES, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RF_FFMM_CPLAZO, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RF_FFMM_LPLAZO, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RF_FFMM_ESTRUC, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RF_BONOS_INT, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RF_FFMM_INT, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RV_ACCIONES_INT, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(RV_FFMM_INT, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(FFMM, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(CTAS_X_COBRAR, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(CTAS_X_PAGAR, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(SALDO_CAJA, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(TOTAL_ACTIVOS, 0), 0)
'            iColExcel = iColExcel + 1
'            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(TOTAL_PROM_MENSUAL, 0), 0)
            iColExcel = iColExcel + 1
            .Cells(iFilaExcel, iColExcel).Value = FormatNumber(NVL(TOTAL_PATRIMONIO, 0), 0)
            
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaExcel, 4)).HorizontalAlignment = xlLeft
            .Range(.Cells(iFilaInicio, 5), .Cells(iFilaExcel, iColExcel)).HorizontalAlignment = xlRight

            .Range(.Cells(iFilaInicio - 1, 5), .Cells(iFilaInicio - 1, iColExcel - 5)).BorderAround
            .Range(.Cells(iFilaInicio - 1, 5), .Cells(iFilaInicio - 1, iColExcel - 5)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(iFilaInicio - 1, 5), .Cells(iFilaInicio - 1, iColExcel - 5)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
            .Range(.Cells(iFilaInicio - 1, 5), .Cells(iFilaInicio - 1, iColExcel - 5)).Font.Bold = True
            .Range(.Cells(iFilaInicio - 1, 5), .Cells(iFilaInicio - 1, iColExcel - 5)).WrapText = True
            .Range(.Cells(iFilaInicio - 1, 5), .Cells(iFilaInicio - 1, iColExcel - 5)).HorizontalAlignment = xlCenter
            
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).BorderAround
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).Font.Bold = True
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).WrapText = True
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).HorizontalAlignment = xlCenter

            .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).BorderAround
            .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
            .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).Font.Bold = True
            .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).WrapText = True

        End With
    End If
ExitProcedure:

End Sub

Private Sub ImprimeEncabezado(ByVal pTituloT0 As String, ByVal pTituloT1 As String, ByVal pTituloT2 As String)
Dim lxHoja          As Worksheet
Dim iColExcel       As Integer
Dim i               As Integer

    Set lxHoja = fLibro.ActiveSheet
    iFilaExcel = iFilaExcel + 1
    iColExcel = 1
    With lxHoja
        .Cells(iFilaExcel, iColExcel).Value = "Cartera"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Empresa"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Nro. Cuenta"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Cuenta"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel - 1, iColExcel).Value = "Renta Variable Nacional"
        .Range("E10:G10").Merge
        .Cells(iFilaExcel, iColExcel).Value = "Acciones"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Fondos Mutuos"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Fondos de inversi�n"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel - 1, iColExcel).Value = "Renta Fija Nacional"
        .Range("H10:M10").Merge
        .Cells(iFilaExcel, iColExcel).Value = "Instrumentos Reajuste US$"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Instrumentos Reajuste UF"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Instrumentos Reajuste $"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Fondos Mutuos Corto Plazo"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Fondos Mutuos Largo Plazo"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Fondos Estructurados"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel - 1, iColExcel).Value = "Renta Fija Internacional"
        .Range("N10:O10").Merge
        .Cells(iFilaExcel, iColExcel).Value = "Bonos"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Fondos Mutuos"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel - 1, iColExcel).Value = "Renta Variable Internacional"
        .Range("P10:Q10").Merge
        .Cells(iFilaExcel, iColExcel).Value = "Acciones"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Fondos Mutuos"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel - 1, iColExcel).Value = "Otros Activos"
        .Cells(iFilaExcel, iColExcel).Value = "Fondos Mutuos Mixtos"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Cuentas por Cobrar"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Cuentas por Pagar"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Total Caja"
'        iColExcel = iColExcel + 1
'        .Cells(iFilaExcel, iColExcel).Value = "Total Promedio Mensual Activos"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Total Activos"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Total Patrimonio"
        
    End With
    
End Sub

Public Function Fnt_Calcula_Proximo_Dia_Habil(pFecha_Inicio As Date, pDias As Long) As Date
'-------------------------
Rem Funcion que entrega cual es el "pDias" h�bil
'--------------------------
Dim lDia As Long
Dim lSabado As Boolean
Dim lfecha As Date
Dim lFechaProxima As Date
  
  If pDias = 0 Then
    lFechaProxima = Fnt_Dia_Habil_MasProximo(pFecha_Inicio)
  Else
    lfecha = pFecha_Inicio
    For lDia = 1 To pDias
      lFechaProxima = Fnt_Dia_Habil_MasProximo(lfecha + 1)
      lfecha = lFechaProxima
    Next
  End If
  Fnt_Calcula_Proximo_Dia_Habil = lFechaProxima
  
End Function

Private Function Fnt_Obtiene_FechaCierre_Virtual(ByVal pId_Cuenta As String, ByRef pFecha As Date) As Boolean
Dim lcCuentas As Object
Dim lresult As Boolean

    lresult = True
    Set lcCuentas = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuentas
        .Campo("id_cuenta").Valor = pId_Cuenta
        If .Buscar Then
            If NVL(.Cursor(1)("fecha_cierre_virtual").Value, "") = "" Then
               lresult = False
            Else
                pFecha = .Cursor(1)("fecha_cierre_virtual").Value
            End If
        End If
    End With
    Fnt_Obtiene_FechaCierre_Virtual = lresult
End Function

