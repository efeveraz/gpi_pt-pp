VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Sector 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sector"
   ClientHeight    =   4725
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6840
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4725
   ScaleWidth      =   6840
   Begin VB.Frame Frame1 
      Caption         =   "Datos Sector"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4215
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   6705
      Begin VB.Frame Frame2 
         Caption         =   "Alias"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   3285
         Left            =   150
         TabIndex        =   3
         Top             =   780
         Width           =   6405
         Begin VSFlex8LCtl.VSFlexGrid Grilla 
            Height          =   2625
            Left            =   150
            TabIndex        =   4
            Top             =   270
            Width           =   5595
            _cx             =   9869
            _cy             =   4630
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   2
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Sector.frx":0000
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Grilla 
            Height          =   660
            Left            =   5820
            TabIndex        =   5
            Top             =   300
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ADD"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Agregar un registro"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "DEL"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Elimina un registro"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar1 
               Height          =   255
               Left            =   9420
               TabIndex        =   6
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
      Begin hControl2.hTextLabel txt_sector 
         Height          =   315
         Left            =   150
         TabIndex        =   7
         Tag             =   "OBLI"
         Top             =   360
         Width           =   6375
         _ExtentX        =   11245
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Descripci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   60
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6840
      _ExtentX        =   12065
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Sector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const CTipoConversion = 2 'Conversion de sectores
Public fKey As String

Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  With Toolbar_Grilla
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Appearance = ccFlat
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pColum_PK, pCod_Arbol_Sistema)
  fKey = pColum_PK
  
  Load Me
  
'-----------------------------------------------------------------------
'--Setea los controles dependiendo de los permisos
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
'-----------------------------------------------------------------------
  
  Call Sub_CargarDatos
  
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de Sector"
  Else
    Me.Caption = "Modificaci�n Sector: " & txt_sector.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
  If Col = 0 Then
    KeyAscii = 0
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
    
  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lTipo_Contraparte As String
Dim lFila As Long
Dim lId_Sector As String
Dim lRollback As Boolean
Dim lValor As String
Dim lOrigen As Long
Dim lcSector As Class_Sectores
Dim lcRel_Conversiones As Object 'Class_Rel_Conversiones

  Fnt_Grabar = True
  
  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If
  
  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
  
  lId_Sector = fKey
  lRollback = False
  gDB.IniciarTransaccion
  
  Set lcSector = New Class_Sectores
  With lcSector
    .Campo("Id_sector").Valor = lId_Sector
    .Campo("Dsc_sector").Valor = txt_sector.Text
    If Not .Guardar Then
      MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      lRollback = True
      GoTo ErrProcedure
    End If
    lId_Sector = .Campo("Id_Sector").Valor
  End With
  Set lcSector = Nothing
  
  Set lcRel_Conversiones = Fnt_CreateObject(cDLL_Rel_Conversiones)  ' New Class_Rel_Conversiones
  With lcRel_Conversiones
    Set .gDB = gDB
    Rem Borra todos los registros de Rel_Conversiones seg�n el id
    .Campo("id_entidad").Valor = lId_Sector
    .Campo("id_tipo_conversion").Valor = CTipoConversion
    If Not .Borrar Then
      MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      lRollback = True
      GoTo ErrProcedure
    End If
  End With
  Set lcRel_Conversiones = Nothing
  
  Rem Graba los instrumentos de la grilla
  If Grilla.Rows > 1 Then
    For lFila = 1 To Grilla.Rows - 1
      lOrigen = Val(Grilla.TextMatrix(lFila, 0))
      lValor = Trim(Grilla.TextMatrix(lFila, 1))
      
      Set lcRel_Conversiones = Fnt_CreateObject(cDLL_Rel_Conversiones) 'New Class_Rel_Conversiones
      With lcRel_Conversiones
        Set .gDB = gDB
        .Campo("id_entidad").Valor = lId_Sector
        .Campo("id_tipo_conversion").Valor = CTipoConversion
        .Campo("id_origen").Valor = lOrigen
        .Campo("Valor").Valor = lValor
        If Not .Guardar Then
          MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
          lRollback = True
          GoTo ErrProcedure
        End If
      End With
      Set lcRel_Conversiones = Nothing
    Next
  End If
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    Fnt_Grabar = False
  Else
    gDB.CommitTransaccion
    Call Sub_CargarDatos
  End If
  
  Set lcSector = Nothing
  Set lcRel_Conversiones = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
  
End Function

Private Sub Sub_CargaForm()
Dim lComboList As String
Dim lReg As hFields

  'Limpia la grilla
  Grilla.Rows = 1

  gDB.Parametros.Clear
  gDB.Procedimiento = "pkg_Origenes.buscar"
  Call gDB.Parametros.Add("Pcursor", DLL_COMUN.ePT_Cursor, "", DLL_COMUN.ePD_Ambos)
  If gDB.EjecutaSP Then
    lComboList = ""
    For Each lReg In gDB.Parametros("pCursor").Valor
      lComboList = lComboList & "|#" & lReg("id_origen").Value
      lComboList = lComboList & ";" & lReg("dsc_origen").Value
    Next
  Else
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
  End If
  gDB.Parametros.Clear
  Grilla.ColComboList(Grilla.ColIndex("dsc_origen")) = lComboList

End Sub

Private Sub Sub_CargarDatos()
Dim lcSectores As Class_Sectores
'----------------------------------------------------
Dim lReg    As hCollection.hFields
Dim lCursor As hRecord
Dim lLinea  As Long
Dim lID     As String
Dim lcRel_Conversiones As Object 'Class_Rel_Conversiones
  
  Load Me
  
  If fKey = cNewEntidad Then
    Grilla.Rows = 1
    Exit Sub
  End If
  
  Set lcSectores = New Class_Sectores
  With lcSectores
    .Campo("ID_SECTOR").Valor = fKey
    If Not .Buscar Then
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      Exit Sub
    End If
    If .Cursor.Count > 1 Then
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      Exit Sub
    End If
    Set lReg = .Cursor(1)
    txt_sector.Text = lReg("dsc_sector")
  End With
  Set lcSectores = Nothing

  Grilla.Rows = 1

  Set lcRel_Conversiones = Fnt_CreateObject(cDLL_Rel_Conversiones) 'New Class_Rel_Conversiones
  With lcRel_Conversiones
    Set .gDB = gDB
    .Campo("ID_ENTIDAD").Valor = fKey
    .Campo("ID_TIPO_CONVERSION").Valor = CTipoConversion
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla.Rows
        Grilla.Rows = lLinea + 1
        SetCell Grilla, lLinea, "dsc_origen", lReg("id_origen").Value
        SetCell Grilla, lLinea, "valor", lReg("valor").Value
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas en cargar.", .ErrMsg, pConLog:=True)
    End If
  End With
  Set lcRel_Conversiones = Nothing

End Sub

Private Function Fnt_ValidarDatos() As Boolean
Dim lFila As Long
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  End If
  
  With Grilla
    If .Rows > 1 Then
      For lFila = 1 To .Rows - 1
        If GetCell(Grilla, lFila, "dsc_origen") = "" Then
          MsgBox "Debe seleccionar un Origen.", vbExclamation, Me.Caption
          GoTo ErrProcedure
        ElseIf GetCell(Grilla, lFila, "Valor") = "" Then
          MsgBox "Debe ingresar un Alias.", vbExclamation, Me.Caption
          GoTo ErrProcedure
        ElseIf Len(GetCell(Grilla, lFila, "Valor")) > 30 Then
          MsgBox "Debe ingresar un Alias menor a 30 caracteres.", vbExclamation, Me.Caption
          GoTo ErrProcedure
        End If
      Next
    End If
  End With
  
  Fnt_ValidarDatos = True
  
  Exit Function
  
ErrProcedure:
  Fnt_ValidarDatos = False
  
End Function

Private Sub Sub_Agregar()
  Grilla.AddItem ""
End Sub

Private Sub Sub_Eliminar()
  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
      With Grilla
        Call .RemoveItem(.Row)
      End With
    End If
  End If
End Sub

Private Sub Toolbar_Grilla_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agregar
    Case "DEL"
      Call Sub_Eliminar
  End Select
End Sub
