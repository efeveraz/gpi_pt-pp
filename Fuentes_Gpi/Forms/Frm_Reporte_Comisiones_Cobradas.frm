VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Comisiones_Cobradas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Comisiones Liquidadas"
   ClientHeight    =   8370
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6900
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   6900
   Begin VB.Frame Frm_Filtros 
      Caption         =   "B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7065
      Left            =   60
      TabIndex        =   3
      Top             =   1230
      Width           =   6765
      Begin VB.Frame Frm_Propiedades_Cuentas 
         Caption         =   "Propiedades de la Cuenta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1935
         Left            =   150
         TabIndex        =   8
         Top             =   270
         Width           =   6525
         Begin MSComctlLib.Toolbar Toolbar_Chequear_Propietario 
            Height          =   330
            Left            =   5070
            TabIndex        =   9
            Top             =   1470
            Width           =   1320
            _ExtentX        =   2328
            _ExtentY        =   582
            ButtonWidth     =   1958
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Chequear"
                  Key             =   "CHK"
                  Object.ToolTipText     =   "Busca las cuentas seg�n los filtros"
               EndProperty
            EndProperty
         End
         Begin TrueDBList80.TDBCombo Cmb_Asesor 
            Height          =   345
            Left            =   1380
            TabIndex        =   10
            Top             =   270
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_Comisiones_Cobradas.frx":0000
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Clientes 
            Height          =   345
            Left            =   1380
            TabIndex        =   11
            Top             =   660
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_Comisiones_Cobradas.frx":00AA
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Grupos_Cuentas 
            Height          =   345
            Left            =   1380
            TabIndex        =   12
            Top             =   1050
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_Comisiones_Cobradas.frx":0154
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Lbl_Asesor 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Asesor"
            Height          =   345
            Left            =   90
            TabIndex        =   15
            Top             =   270
            Width           =   1275
         End
         Begin VB.Label Lbl_Clientes 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Clientes"
            Height          =   345
            Left            =   90
            TabIndex        =   14
            Top             =   660
            Width           =   1275
         End
         Begin VB.Label lbl_GruposCuentas 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Grupos Cuentas"
            Height          =   345
            Left            =   90
            TabIndex        =   13
            Top             =   1050
            Width           =   1275
         End
      End
      Begin VB.Frame Frm_Cuentas 
         Caption         =   "Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4815
         Left            =   150
         TabIndex        =   4
         Top             =   2160
         Width           =   6525
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
            Height          =   4425
            Left            =   90
            TabIndex        =   5
            Top             =   270
            Width           =   5745
            _cx             =   10134
            _cy             =   7805
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   5
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Reporte_Comisiones_Cobradas.frx":01FE
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Chequeo 
            Height          =   660
            Left            =   5940
            TabIndex        =   6
            Top             =   540
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar3 
               Height          =   255
               Left            =   9420
               TabIndex        =   7
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
   End
   Begin VB.Frame Frm_Fecha_Re_Proceso 
      Caption         =   "Filtro de b�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   795
      Left            =   60
      TabIndex        =   0
      Top             =   390
      Width           =   6765
      Begin MSComCtl2.DTPicker DTP_FechaLiquidacion 
         Height          =   315
         Left            =   2040
         TabIndex        =   1
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   65994753
         CurrentDate     =   37732
      End
      Begin VB.Label Lbl_Fecha_Desde 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Liquidaci�n"
         Height          =   345
         Left            =   210
         TabIndex        =   2
         Top             =   270
         Width           =   1815
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   6900
      _ExtentX        =   12171
      _ExtentY        =   635
      ButtonWidth     =   2990
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte Detalle"
            Key             =   "REPORT"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Genera reporte de Rentabilidad por Perfil"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Reporte Resumen"
            Key             =   "REPORTRE"
            Object.ToolTipText     =   "Genera reporte de Rentabilidad por Perfil"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   17
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Comisiones_Cobradas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Const COL_FECHA_LIQUIDACION = 2

Const COL_FECHA_MOVIMIENTO = 3

Const COL_CUENTA = 3
Const COL_RUT_CLIENTE = 4
Const COL_NOMBRE_CLIENTE = 5
Const COL_MONEDA = 6
Const COL_COMISION_HON = 7
Const COL_COMISION_ASE = 8
Const COL_MONTO_COBRADO = 7
Const COL_MONTO_ASESOR = 8
Const COL_NOMBRE_ASESOR = 8



Const COL_MONTO_HON = 7
Const COL_MONTO_ASE = 8




'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Dim fdetalle_resumen As String

'----------------------------------------------------------------------------
Dim App_Excel       As Excel.Application      ' Excel application
Dim lLibro          As Excel.Workbook      ' Excel workbook

Private Type TCuenta
    id_cuenta       As Integer
    Num_Cuenta      As String
    abr_cuenta      As String
    dsc_cuenta      As String
End Type

Dim aCuentas() As TCuenta
Dim bMuestraCuadro As Boolean

Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------
Private Sub Form_Load()
    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("REPORT").Image = cBoton_Excel
            .Buttons("REPORTRE").Image = cBoton_Modificar
            .Buttons("REFRESH").Image = cBoton_Original
            .Buttons("EXIT").Image = cBoton_Salir
    End With
  
    With Toolbar_Chequear_Propietario
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("CHK").Image = cBoton_Agregar_Grilla
    End With
  
    With Toolbar_Chequeo
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
            .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
            .Appearance = ccFlat
    End With
   
    Call Sub_CargaForm
  
    Me.Top = 1
    Me.Left = 1
    
End Sub

Private Sub Form_Resize()
    Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Cuentas_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    If Not Col = Grilla_Cuentas.ColIndex("CHK") Then
        Cancel = True
    End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents
  
    Select Case Button.Key
        Case "REPORT"
            fdetalle_resumen = "Detalle"
            Call Sub_Generar_Excel(fdetalle_resumen)
        Case "REPORTRE"
            fdetalle_resumen = "Resumen"
            Call Sub_Generar_Excel(fdetalle_resumen)
        Case "REFRESH"
            Call Sub_CargaForm
        Case "EXIT"
            Unload Me
    End Select
  
End Sub

Private Sub Sub_CargaForm()
    'Dim lcCuenta As Class_Cuentas
    Dim lcCuenta As Object
    Dim lReg As hFields
    Dim lLinea As Long
  
    Call Sub_Bloquea_Puntero(Me)
  
    Call Sub_FormControl_Color(Me.Controls)

    Grilla_Cuentas.Rows = 1
  
    'DTP_Fecha_Hasta.Value = Fnt_FechaServidor
    DTP_FechaLiquidacion.Value = Fnt_FechaServidor
  
    Rem Carga los combos con el primer elemento vac�o
    Call Sub_CargaCombo_Asesor(Cmb_Asesor, pBlanco:=True)
    Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKBLANCO)
    
    Call Sub_CargaCombo_Clientes(Cmb_Clientes, pBlanco:=True)
    Call Sub_ComboSelectedItem(Cmb_Clientes, cCmbKBLANCO)
    
    Call Sub_CargaCombo_Grupos_Cuentas(Cmb_Grupos_Cuentas, pBlanco:=True)
    Call Sub_ComboSelectedItem(Cmb_Grupos_Cuentas, cCmbKBLANCO)
    
    Rem Carga las cuentas habilitadas y de la empresa en la grilla
    '  Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
        .Campo("Cod_Estado").Valor = cCod_Estado_Habilitada
        .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
        If .Buscar_Vigentes Then
            Call Sub_hRecord2Grilla(lcCuenta.Cursor, Grilla_Cuentas, "id_cuenta")
        Else
            Call Fnt_MsgError(.SubTipo_LOG _
                            , "Problema con buscar las cuentas vigentes." _
                            , .ErrMsg _
                            , pConLog:=True)
            Err.Clear
        End If
    End With
    Set lcCuenta = Nothing
    
    Call Sub_CambiaCheck(True)
    
    Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Toolbar_Chequear_Propietario_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents

    Select Case Button.Key
        Case "CHK"
            Call Sub_Busca_Cuentas_Propiedades
    End Select
  
End Sub

Private Sub Toolbar_Chequeo_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents

    Select Case Button.Key
        Case "SEL_ALL"
            Call Sub_CambiaCheck(True)
        Case "SEL_NOTHING"
            Call Sub_CambiaCheck(False)
    End Select
   
End Sub

Private Sub Sub_CambiaCheck(pValor As Boolean)
    Dim lLinea As Long
    Dim lCol As Long
  
    lCol = Grilla_Cuentas.ColIndex("CHK")
    If pValor Then
        For lLinea = 1 To Grilla_Cuentas.Rows - 1
            Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexChecked
        Next
    Else
        For lLinea = 1 To Grilla_Cuentas.Rows - 1
            Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexUnchecked
        Next
    End If
  
End Sub

Private Sub Sub_Busca_Cuentas_Propiedades()
    'Dim lcCuenta As Class_Cuentas
    Dim lcCuenta As Object
    Dim lCursor As hCollection.hRecord
    Dim lReg As hCollection.hFields
    Dim lId_Asesor As String
    Dim lId_Cliente As String
    Dim lId_Grupos_Cuentas As String
    
    Call Sub_Bloquea_Puntero(Me)
  
    lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
    lId_Asesor = IIf(lId_Asesor = cCmbKBLANCO, "", lId_Asesor)
    
    lId_Cliente = Fnt_ComboSelected_KEY(Cmb_Clientes)
    lId_Cliente = IIf(lId_Cliente = cCmbKBLANCO, "", lId_Cliente)
    
    lId_Grupos_Cuentas = Fnt_ComboSelected_KEY(Cmb_Grupos_Cuentas)
    lId_Grupos_Cuentas = IIf(lId_Grupos_Cuentas = cCmbKBLANCO, "", lId_Grupos_Cuentas)
    
    '  Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
        .Campo("id_asesor").Valor = lId_Asesor
        .Campo("Id_Cliente").Valor = lId_Cliente
        .Campo("id_empresa").Valor = Fnt_EmpresaActual
        
        If .Buscar(pEnVista:=False, pId_Grupo_Cuenta:=lId_Grupos_Cuentas) Then
            For Each lReg In .Cursor
                Call Sub_Llena_Grilla_Cuentas(lReg("id_cuenta").Value)
            Next
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
        End If
    End With
    
    Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Sub Sub_Llena_Grilla_Cuentas(pId_Cuenta As String)
    Dim lFila As Long
    Dim lCol As Long
  
    With Grilla_Cuentas
        If .Rows > 0 Then
            lCol = Grilla_Cuentas.ColIndex("CHK")
            For lFila = 1 To .Rows - 1
                If GetCell(Grilla_Cuentas, lFila, "colum_pk") = pId_Cuenta Then
                    .Cell(flexcpChecked, lFila, lCol) = flexChecked
                    Exit For
                End If
            Next
        End If
    End With
    
End Sub
'---------------------------------------------------------------------------------------
' Procedure : Fnt_Cuentas_Chequeadas
' Author    : jvidal
' Date      : 24/09/2008
' Purpose   : Verifica que por lo menos una de las cuentas est� chequeda.
'---------------------------------------------------------------------------------------
Private Function Fnt_Cuentas_Chequeadas() As Boolean
    Dim lHay_Chequeados As Boolean
    Dim lCol As Long
    Dim lFila As Long

    lHay_Chequeados = False
    lCol = Grilla_Cuentas.ColIndex("CHK")
  
    With Grilla_Cuentas
        For lFila = 1 To .Rows - 1
            If .Cell(flexcpChecked, lFila, lCol) = flexChecked Then
                lHay_Chequeados = True
                Exit For
            End If
        Next
    End With
  
    Fnt_Cuentas_Chequeadas = lHay_Chequeados
    
End Function
'---------------------------------------------------------------------------------------
' Procedure : Sub_Generar_Excel
' Author    : jvidal
' Date      : 24/09/2008
' Purpose   : Generar la planilla Excel de salida del reporte.
'---------------------------------------------------------------------------------------
Private Sub Sub_Generar_Excel(fdetalle_resumen As String)
    Dim lFila           As Long
    Dim lCol            As Long
    Dim lId_Cuenta      As String
    Dim lFecha_Proceso  As Date
    Dim lTiempo         As Long
    
    Dim nFila   As Integer
    Dim i       As Integer
    
    Dim bPrimera As Boolean
    
    ReDim aCuentas(0)
    
    If Not Fnt_Cuentas_Chequeadas() Then
        MsgBox "No hay cuentas chequeadas para mostrar en el informe.", vbCritical, Me.Caption
        Exit Sub
    End If
    
    Call Sub_Bloquea_Puntero(Me)
    Me.Enabled = False
        
    lCol = Grilla_Cuentas.ColIndex("CHK")
    
    For lFila = 1 To Grilla_Cuentas.Rows - 1
        If Grilla_Cuentas.Cell(flexcpChecked, lFila, lCol) = flexChecked Then
            nFila = UBound(aCuentas) + 1
            
            lId_Cuenta = GetCell(Grilla_Cuentas, lFila, "colum_pk")
            nFila = UBound(aCuentas) + 1
            ReDim Preserve aCuentas(nFila)
            
            aCuentas(nFila).id_cuenta = GetCell(Grilla_Cuentas, lFila, "colum_pk")
            aCuentas(nFila).Num_Cuenta = GetCell(Grilla_Cuentas, lFila, "NUM_CUENTA")
        End If
    Next
    
    
    Crea_Excel
    
    
    
    Me.Enabled = True
    Call Sub_Desbloquea_Puntero(Me)
    
End Sub
'---------------------------------------------------------------------------------------
' Procedure : Crea_Excel
' Author    : jvidal
' Date      : 10/01/2008
' Purpose   : Para cada una de las cuentas seleccionadas en la grilla de la aplicacion,
'             Crea una hoja en el libro.
'             Llena la hoja.
'             Hace la aplicacion Excel Visible.
'---------------------------------------------------------------------------------------
Private Sub Crea_Excel()
    Dim i           As Integer
    Dim hoja        As Integer
    Dim nFilaExcel  As Integer
        
    Set App_Excel = CreateObject("Excel.application")
    App_Excel.DisplayAlerts = False
    Set lLibro = App_Excel.Workbooks.Add
    
    App_Excel.Visible = False
    
    For i = 1 To lLibro.Worksheets.Count - 1
        lLibro.Worksheets(i).Delete
    Next
    
    
    'NOMBRE HOJA
    
    If fdetalle_resumen <> "Detalle" Then
        lLibro.Worksheets.Add After:=lLibro.Worksheets(lLibro.Worksheets.Count)
        lLibro.ActiveSheet.Name = "Reporte Comisiones Liquidadas"
    
        lLibro.ActiveSheet.Cells(1, 1).Select
        lLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).ShapeRange.IncrementLeft 29.75
        
        lLibro.ActiveSheet.Range("B6").Font.Bold = True
        lLibro.ActiveSheet.Columns("A:A").ColumnWidth = 2
        nFilaExcel = HeaderXls(i)
        
    End If
    

        
    If fdetalle_resumen = "Detalle" Then
        For i = 1 To UBound(aCuentas)
            Call Sub_Interactivo(gRelogDB)
            lLibro.Worksheets.Add After:=lLibro.Worksheets(lLibro.Worksheets.Count)
            lLibro.ActiveSheet.Name = "Cuenta " & Str(aCuentas(i).Num_Cuenta)
            lLibro.ActiveSheet.Cells(1, 1).Select
            lLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).ShapeRange.IncrementLeft 29.75

    
            lLibro.ActiveSheet.Range("B6").Font.Bold = True
            lLibro.ActiveSheet.Columns("A:A").ColumnWidth = 2
            nFilaExcel = HeaderXls(i)
            
            
            Genera_Informe_Excel i, nFilaExcel
            With lLibro.ActiveSheet
                .Columns("A:A").ColumnWidth = 2
                .Columns("B:B").ColumnWidth = 10
                .Columns("C:C").ColumnWidth = 11
                .Columns("D:D").ColumnWidth = 52
                .Columns("E:E").ColumnWidth = 18
                .Columns("F:F").ColumnWidth = 18
                .Columns("G:G").ColumnWidth = 15
                .Columns("H:H").ColumnWidth = 33
                .Columns("I:I").ColumnWidth = 50
            End With
    
    
        Next
    
    ElseIf fdetalle_resumen = "Resumen" Then
        For i = 1 To UBound(aCuentas)
            Call Sub_Interactivo(gRelogDB)
            Genera_Informe_Excel_Resumen i, nFilaExcel
        Next
        
        lLibro.ActiveSheet.Columns.AutoFit
    End If
    
    
    
    
    lLibro.ActiveSheet.Cells(1, 1).Select
       
    lLibro.Worksheets(1).Delete
    
    App_Excel.Visible = True
    App_Excel.UserControl = True
    
    Set lLibro = Nothing
    Set App_Excel = Nothing
    
    
    
    
End Sub

'---------------------------------------------------------------------------------------
' Procedure : HeaderXls
' Author    : jvidal
' Date      : 08/01/2008
' Purpose   : Header de la hojas de la aplicacion.
'---------------------------------------------------------------------------------------
Function HeaderXls(ByVal hoja As Integer) As Integer
    Dim intFilaInicioDatos As Integer
    
    'lLibro.Worksheets(hoja).Select
    App_Excel.ActiveWindow.DisplayGridlines = False
    
    With lLibro.ActiveSheet
        'TITULO
        If fdetalle_resumen = "Detalle" Then
            .Range("B5").Value = "Reporte Detalle Comisiones Liquidadas"
            .Range("A5:I5").Font.Size = 14
            .Range("A5:I5").Font.Bold = True
            .Range("A5:I5").HorizontalAlignment = xlCenter
            .Range("A5:I5").Merge
            .Range("I8").Value = "Fecha Generaci�n Reporte " & Date
        Else
            .Range("B5").Value = "Reporte Resumen Comisiones Liquidadas"
            .Range("A5:H5").Font.Size = 14
            .Range("A5:H5").Font.Bold = True
            .Range("A5:H5").HorizontalAlignment = xlCenter
            .Range("A5:H5").Merge
            .Range("H8").Value = "Fecha Generaci�n Reporte " & Date
        End If
        
        .Range("B8").Value = "Empresa: " & gDsc_Empresa
        .Range("B8:G8").Merge
        
         intFilaInicioDatos = 10
    End With
    
    HeaderXls = intFilaInicioDatos ' Columna donde comienzan los datos
    
End Function


'---------------------------------------------------------------------------------------
' Procedure : PonerLineaEnExcel
' Author    : jvidal
' Date      : 25/09/2008
' Purpose   : Poner con Datos una linea del Excel
'---------------------------------------------------------------------------------------
Private Sub PonerLineaEnExcel(ByVal intFila As Integer, ByVal lReg As hCollection.hFields, ByVal sFormato As String)
        
    With lLibro.ActiveSheet
    
        If lReg("Item") = "RESUMEN" Then
            .Cells(intFila, 2).Value = lReg("NUM_CUENTA")
            .Cells(intFila, 3).Value = lReg("RUT_CLIENTE")
            .Cells(intFila, 4).Value = lReg("NOMBRE_CLIENTE")
            .Cells(intFila, 5).Value = lReg("Fecha_LIQUIDACION").Value   ' Format(lreg("FECHA_LIQUIDACION"), "General Date")
            .Cells(intFila, 6).Value = lReg("Fecha_Movimiento").Value   ' Format(lreg("FECHA_LIQUIDACION"), "General Date")
            .Cells(intFila, 7).Value = lReg("Monto")
            .Cells(intFila, 8).Value = " "
            .Cells(intFila, 9).Value = lReg("ASESOR")
            .Range(.Cells(intFila, 2), .Cells(intFila, 9)).Font.Bold = True
        Else
            .Cells(intFila, 6).Value = lReg("Fecha_Movimiento").Value   ' Format(lreg("FECHA_LIQUIDACION"), "General Date")
            .Cells(intFila, 7).Value = lReg("Monto")
            .Cells(intFila, 8).Value = lReg("Item")
        End If
        
        .Range(.Cells(intFila, 2), .Cells(intFila, 2)).HorizontalAlignment = xlLeft
        .Range(.Cells(intFila, 3), .Cells(intFila, 3)).HorizontalAlignment = xlRight
        .Range(.Cells(intFila, 4), .Cells(intFila, 4)).HorizontalAlignment = xlLeft
        .Range(.Cells(intFila, 5), .Cells(intFila, 6)).HorizontalAlignment = xlCenter
        .Range(.Cells(intFila, 7), .Cells(intFila, 7)).HorizontalAlignment = xlRight
        .Range(.Cells(intFila, 8), .Cells(intFila, 9)).HorizontalAlignment = xlLeft
        
    End With
End Sub

'---------------------------------------------------------------------------------------
' Procedure : PonerLineaEncabezado
' Author    : Administrador
' Date      : 25/09/2008
' Purpose   : Encabezado de las Columnas
'---------------------------------------------------------------------------------------
Private Sub PonerLineaEncabezado_Detalle(ByVal intFila As Integer, pxHoja As Worksheet)

    
    With pxHoja
        .Cells(intFila, COL_COMISION_HON).HorizontalAlignment = xlCenter
        .Cells(intFila, COL_COMISION_ASE).HorizontalAlignment = xlCenter
        
        .Cells(intFila, 2).Value = "Cuenta"
        .Cells(intFila, 3).Value = "Rut Cliente"
        .Cells(intFila, 4).Value = "Nombre Cliente"
        .Cells(intFila, 5).Value = "Fecha Liquidaci�n"
        .Cells(intFila, 6).Value = "Fecha Movimiento"
        .Cells(intFila, 7).Value = "Monto"
        .Cells(intFila, 8).Value = "Item / Producto"
        .Cells(intFila, 9).Value = "Asesor"
        
        .Range(.Cells(intFila, 2), .Cells(intFila, 9)).Interior.Color = RGB(255, 255, 0)
        .Range(.Cells(intFila, 2), .Cells(intFila, 9)).Font.Bold = True
        .Range(.Cells(intFila, 2), .Cells(intFila, 9)).BorderAround
        .Range(.Cells(intFila, 2), .Cells(intFila, 9)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(intFila, 2), .Cells(intFila, 9)).HorizontalAlignment = xlCenter
        
    End With
End Sub


Private Sub PonerLineaEncabezado_Resumen(ByVal intFila As Integer)
    With lLibro.ActiveSheet
        .Cells(intFila, COL_FECHA_LIQUIDACION).Value = "Fecha Liquidacion"
        .Cells(intFila, COL_FECHA_MOVIMIENTO).Value = "Fecha Movimiento"
        .Cells(intFila, COL_CUENTA).Value = "Cuenta"
        .Cells(intFila, COL_RUT_CLIENTE).Value = "Rut Cliente"
        .Cells(intFila, COL_NOMBRE_CLIENTE).Value = "Nombre Cliente"
        .Cells(intFila, COL_MONEDA).Value = "Moneda"
        .Cells(intFila, COL_MONTO_COBRADO).Value = "Monto"
        .Cells(intFila, COL_NOMBRE_ASESOR).Value = "Asesor"
        
        .Range(.Cells(intFila, COL_FECHA_LIQUIDACION), .Cells(intFila, COL_NOMBRE_ASESOR)).Interior.Color = RGB(255, 255, 0)
        .Range(.Cells(intFila, COL_FECHA_LIQUIDACION), .Cells(intFila, COL_NOMBRE_ASESOR)).Font.Bold = True
        .Range(.Cells(intFila, COL_FECHA_LIQUIDACION), .Cells(intFila, COL_NOMBRE_ASESOR)).BorderAround
        .Range(.Cells(intFila, COL_FECHA_LIQUIDACION), .Cells(intFila, COL_NOMBRE_ASESOR)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(intFila, COL_FECHA_LIQUIDACION), .Cells(intFila, COL_NOMBRE_ASESOR)).HorizontalAlignment = xlCenter
        
    End With
End Sub

'---------------------------------------------------------------------------------------
' Procedure : PonerLineaTotalExcel
' Author    : Administrador
' Date      : 25/09/2008
' Purpose   : Pone la linea de Total
'---------------------------------------------------------------------------------------
Private Sub PonerLineaTotalExcel(ByVal intFila As Integer, ByVal dblTotalase As Double, ByVal dbltotalhon As Double, ByVal strFormato As String, pxHoja As Worksheet)
    With pxHoja
         .Cells(intFila, COL_MONTO_HON).NumberFormat = strFormato
         .Cells(intFila, COL_MONTO_ASE).NumberFormat = strFormato
         .Cells(intFila, COL_RUT_CLIENTE).Value = "Total Moneda PESO"
         .Cells(intFila, 7).Value = dblTotalase
               
        
        .Range(.Cells(intFila, COL_RUT_CLIENTE), .Cells(intFila, COL_MONTO_ASE)).Interior.Color = RGB(204, 255, 204)
        .Range(.Cells(intFila, COL_RUT_CLIENTE), .Cells(intFila, COL_MONEDA)).Merge
        
        .Range(.Cells(intFila, COL_FECHA_LIQUIDACION), .Cells(intFila, COL_CUENTA)).Merge
    End With
End Sub



'---------------------------------------------------------------------------------------
' Procedure : PonerLineaVacia
' Author    : Administrador
' Date      : 25/09/2008
' Purpose   : Poner el Mensaje en la Linea
'---------------------------------------------------------------------------------------
Private Sub PonerLineaVacia(ByVal intFila As Integer, ByVal strMensaje As String, pxHoja As Worksheet)
    With pxHoja
        .Cells(intFila, COL_FECHA_LIQUIDACION).Value = strMensaje
        .Range(.Cells(intFila, COL_FECHA_LIQUIDACION), .Cells(intFila, COL_NOMBRE_ASESOR)).Merge
    End With
    
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Genera_Informe
' Author    : jvidal
' Date      : 10/01/2008
' Purpose   : Realizar el Informe para una cuenta
'---------------------------------------------------------------------------------------
Sub Genera_Informe_Excel(ByVal hoja As Integer, ByRef iFilaExcel As Integer)
Dim lReg                As hCollection.hFields
Dim lcComisionesCobradas As Class_Comisiones_Hono_Ase_Cuenta
Dim lxHoja          As Worksheet
'----------------------------------------------------
Dim intFila           As Integer


Dim iColExcel       As Integer
Dim lIdCuenta       As String

Dim sValor              As String
Dim nDecimales          As Integer
Dim lNumCuentaAnt       As String
Dim sFormato            As String
Dim dbltotalhon            As Double
Dim dblTotalase       As Double
    
    'On Error GoTo ErrProcedure
    
    lIdCuenta = Str(aCuentas(hoja).id_cuenta)
    Set lxHoja = lLibro.ActiveSheet
    
    'Set lcComisionesCobradas = New Class_ComisionesCuentas
    Set lcComisionesCobradas = New Class_Comisiones_Hono_Ase_Cuenta
    With lcComisionesCobradas
        
        
        'If Not .Reporte_Detallado(pFecha_ini:=DTP_Fecha_Desde.Value, Pfecha_fin:=DTP_Fecha_Hasta.Value) Then
        If Not .Reporte_Detalle_Comisiones_Liquidadas(DTP_FechaLiquidacion.Value, lIdCuenta) Then
            MsgBox "Problemas en buscar las cuentas." & vbLf & vbLf & .ErrMsg, vbCritical, "Comisiones Liquidadas"
            GoTo ExitProcedure
        End If
        
        intFila = iFilaExcel
        Set lxHoja = lLibro.ActiveSheet
            
        If .Cursor.Count = 0 Then
            Call PonerLineaVacia(intFila, "No existen comisiones liquidadas para la cuenta " & aCuentas(hoja).Num_Cuenta & "", lxHoja)
            intFila = intFila + 1
        Else
            Call PonerLineaEncabezado_Detalle(intFila, lxHoja)
                    
            intFila = intFila + 1
            
            For Each lReg In lcComisionesCobradas.Cursor
'                lLibro.ActiveSheet.Name = "Cuenta:" & lReg("NUM_CUENTA").Value
                sFormato = "#,##0"
                ' If lReg("DICIMALES_MOSTRAR").Value > 0 Then
                '     sFormato = sFormato & "." & String(lReg("DICIMALES_MOSTRAR").Value, "0")
                ' End If

                Call Sub_Interactivo(gRelogDB)
                Call PonerLineaEnExcel(intFila, lReg, sFormato)
                
                '-----------------------------------------------------------------
                ' Formato
                '-----------------------------------------------------------------
                lxHoja.Cells(intFila, COL_MONTO_HON).NumberFormat = sFormato
                
                intFila = intFila + 1
                
                'dbltotalhon = dbltotalhon + lReg("COMISION_honorarios")
                If lReg("Item") <> "RESUMEN" Then
                    dblTotalase = dblTotalase + lReg("MONTO")
                End If
                lNumCuentaAnt = lReg("NUM_CUENTA").Value
                
            Next
            
            Call PonerLineaTotalExcel(intFila, dblTotalase, dbltotalhon, sFormato, lxHoja)
            
            
            intFila = intFila + 1
            
            Set lcComisionesCobradas = Nothing
        End If
        
    End With
    
    intFila = intFila + 1
    
    Call FormatoFinal(iFilaExcel, intFila, lxHoja, 9)

    'iFilaExcel = intFila
    'lxHoja.Cells(1, 1).Select
    
ErrProcedure:
    If Not Err.Number = 0 Then
        Call Fnt_MsgError(eLS_ErrSystem, "Problemas con el informe de Comisiones de Cuentas.", Err.Description, pConLog:=True)
        Err.Clear
        GoTo ExitProcedure
        Resume
    End If

ExitProcedure:

  Set lcComisionesCobradas = Nothing
  
End Sub

Private Sub FormatoFinal(ByVal iFilaExcel As Integer, ByVal intFila As Integer, pxHoja As Worksheet, pColFinal As Integer)
    Dim iCol As Integer
    
    With pxHoja
        '************************************************************************************
        ' Rayas Verticales
        '************************************************************************************
        For iCol = COL_FECHA_LIQUIDACION To pColFinal
            .Range(.Cells(iFilaExcel + 1, iCol), .Cells(intFila - 2, iCol)).BorderAround
            .Range(.Cells(iFilaExcel + 1, iCol), .Cells(intFila - 2, iCol)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(iFilaExcel + 1, iCol), .Cells(intFila - 2, iCol)).Borders(xlInsideHorizontal).LineStyle = xlNone
        Next

        .Range(.Cells(intFila - 2, COL_FECHA_LIQUIDACION), .Cells(intFila - 2, pColFinal)).BorderAround
        .Range(.Cells(intFila - 2, COL_FECHA_LIQUIDACION), .Cells(intFila - 2, pColFinal)).Borders.Color = RGB(0, 0, 0)
        
    End With
    
End Sub

'******************************************+
' Reporte Resumen
'**********************************

Sub Genera_Informe_Excel_Resumen(ByVal hoja As Integer, ByRef iFilaExcel As Integer)
    Dim lReg                As hCollection.hFields
    Dim ComisionesCobradas    As Class_ComisionesCuentas
    Dim lxHoja          As Worksheet
    '----------------------------------------------------
    Dim intFila           As Integer
    Dim iColExcel       As Integer
    Dim lIdCuenta       As String
    Dim sValor              As String
    Dim nDecimales          As Integer
    Dim sFormato            As String
    Dim dblTotal            As Double
    On Error GoTo ErrProcedure
    
    lIdCuenta = aCuentas(hoja).id_cuenta
    Set lxHoja = lLibro.ActiveSheet
    
    Set ComisionesCobradas = New Class_ComisionesCuentas
    With ComisionesCobradas
        
        'If Not .Buscar_Comisiones_Cobradas(pId_Cuenta:=lIdCuenta, pFecha_Ini:=DTP_Fecha_Desde.Value, pFecha_Fin:=DTP_Fecha_Hasta.Value) Then
        If Not .Buscar_Comisiones_Cobradas(lIdCuenta, DTP_FechaLiquidacion.Value) Then
            MsgBox "Problemas en buscar las cuentas." & vbLf & vbLf & .ErrMsg, vbCritical, "Comisiones Liquidadas"
            GoTo ExitProcedure
        End If
        
        intFila = iFilaExcel
        
        PonerLineaEncabezado_Resumen intFila
        
        intFila = intFila + 1
        
        If ComisionesCobradas.Cursor.Count > 0 Then
            For Each lReg In ComisionesCobradas.Cursor
                sFormato = "#,##0"
                
                If lReg("DECIMALES_MOSTRAR").Value > 0 Then
                    sFormato = sFormato & "." & String(lReg("DECIMALES_MOSTRAR").Value, "0")
                End If

                Call Sub_Interactivo(gRelogDB)
                Call PonerLineaEnExcel_Resumen(intFila, lReg)
                
                '-----------------------------------------------------------------
                ' Formato
                '-----------------------------------------------------------------
                lxHoja.Cells(intFila, COL_MONTO_COBRADO).NumberFormat = sFormato
                
                intFila = intFila + 1
                
                dblTotal = dblTotal + lReg("MONTO")
                
            Next
            
            Call PonerLineaTotalExcel_resumen(intFila, dblTotal, sFormato)
            intFila = intFila + 1
            
            Set ComisionesCobradas = Nothing
        Else
            ' intFila = intFila + 1
            Call PonerLineaVacia(intFila, "No existen comisiones cobradas para la cuenta " & aCuentas(hoja).Num_Cuenta & "", lxHoja)
            intFila = intFila + 1
        End If
        
    End With
    
    intFila = intFila + 1
    
    Call FormatoFinal(iFilaExcel, intFila, lxHoja, COL_NOMBRE_ASESOR)

    iFilaExcel = intFila
    lxHoja.Cells(1, 1).Select
    
ErrProcedure:
    If Not Err.Number = 0 Then
        Call Fnt_MsgError(eLS_ErrSystem, "Problemas con el informe de Comisiones de Cuentas.", Err.Description, pConLog:=True)
        Err.Clear
        GoTo ExitProcedure
        Resume
    End If

ExitProcedure:

  Set ComisionesCobradas = Nothing
  
End Sub

Private Sub PonerLineaEnExcel_Resumen(ByVal intFila As Integer, ByVal lReg As hCollection.hFields)
        
    With lLibro.ActiveSheet
        .Cells(intFila, COL_FECHA_LIQUIDACION).Value = lReg("FECHA_LIQUIDACION")   ' Format(lreg("FECHA_LIQUIDACION"), "General Date")
        .Cells(intFila, COL_FECHA_MOVIMIENTO).Value = lReg("FECHA_MOVIMIENTO")     ' Format(lreg("FECHA_MOVIMIENTO"), "General Date")
        .Cells(intFila, COL_CUENTA).Value = lReg("NUM_CUENTA")
        .Cells(intFila, COL_RUT_CLIENTE).Value = lReg("RUT_CLIENTE")
        .Cells(intFila, COL_NOMBRE_CLIENTE).Value = lReg("NOMBRE_CLIENTE")
        .Cells(intFila, COL_MONEDA).Value = lReg("DSC_MONEDA")
        .Cells(intFila, COL_MONTO_COBRADO).Value = Val(lReg("MONTO"))
        .Cells(intFila, COL_NOMBRE_ASESOR).Value = lReg("NOMBRE_ASESOR")
        
        .Range(.Cells(intFila, COL_FECHA_LIQUIDACION), .Cells(intFila, COL_RUT_CLIENTE)).HorizontalAlignment = xlRight
        .Range(.Cells(intFila, COL_FECHA_LIQUIDACION), .Cells(intFila, COL_FECHA_MOVIMIENTO)).NumberFormat = "m/d/yyyy"
    End With
End Sub

Private Sub PonerLineaTotalExcel_resumen(ByVal intFila As Integer, ByVal dblTotal As Double, ByVal strFormato As String)
    With lLibro.ActiveSheet
        .Cells(intFila, COL_RUT_CLIENTE).Value = "Total Moneda PESO"
        .Cells(intFila, COL_MONTO_COBRADO).Value = dblTotal
        .Cells(intFila, COL_MONTO_COBRADO).NumberFormat = strFormato
        
        .Range(.Cells(intFila, COL_RUT_CLIENTE), .Cells(intFila, COL_MONTO_COBRADO)).Interior.Color = RGB(204, 255, 204)
        .Range(.Cells(intFila, COL_RUT_CLIENTE), .Cells(intFila, COL_MONEDA)).Merge
        
        .Range(.Cells(intFila, COL_FECHA_LIQUIDACION), .Cells(intFila, COL_CUENTA)).Merge
    End With
End Sub
