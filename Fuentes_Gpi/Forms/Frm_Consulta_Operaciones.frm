VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Consulta_Operaciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de Operaciones"
   ClientHeight    =   9045
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   14865
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9045
   ScaleWidth      =   14865
   Begin VB.Frame Frame1 
      Caption         =   "Operaciones"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   5565
      Left            =   60
      TabIndex        =   1
      Top             =   3360
      Width           =   14685
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   5205
         Left            =   90
         TabIndex        =   2
         Tag             =   "SOLOLECTURA=N"
         Top             =   240
         Width           =   13875
         _cx             =   24474
         _cy             =   9181
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   18
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Consulta_Operaciones.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Nemotecnicos 
         Height          =   660
         Left            =   14040
         TabIndex        =   29
         Top             =   240
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "ALL"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Selecciona todos los Perfiles de Riesgo"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "NALL"
               Object.ToolTipText     =   "Selecciona ninguno de los Perfiles de Riesgo"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar2 
            Height          =   255
            Left            =   9420
            TabIndex        =   30
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filtros de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2895
      Left            =   60
      TabIndex        =   0
      Top             =   390
      Width           =   14685
      Begin VB.Frame Frame6 
         Height          =   1095
         Left            =   120
         TabIndex        =   19
         Top             =   1680
         Width           =   14415
         Begin VB.CheckBox chk_TodasCtas 
            Caption         =   "Todas"
            Height          =   360
            Left            =   13095
            TabIndex        =   27
            ToolTipText     =   "Todas las Cuentas"
            Top             =   255
            Width           =   855
         End
         Begin VB.CommandButton cmb_buscar 
            Caption         =   "?"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   12600
            Picture         =   "Frm_Consulta_Operaciones.frx":034B
            TabIndex        =   26
            Top             =   255
            Width           =   375
         End
         Begin TrueDBList80.TDBCombo Cmb_Clientes 
            Height          =   315
            Left            =   1410
            TabIndex        =   20
            Tag             =   "SOLOLECTURA=N"
            Top             =   240
            Width           =   5145
            _ExtentX        =   9075
            _ExtentY        =   556
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   556
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_Operaciones.frx":0655
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Asesores 
            Height          =   315
            Left            =   1410
            TabIndex        =   22
            Tag             =   "SOLOLECTURA=N"
            Top             =   615
            Width           =   5145
            _ExtentX        =   9075
            _ExtentY        =   556
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   556
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_Operaciones.frx":06FF
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Instrumento 
            Height          =   345
            Left            =   8100
            TabIndex        =   23
            Tag             =   "SOLOLECTURA=N"
            Top             =   615
            Width           =   5745
            _ExtentX        =   10134
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_Operaciones.frx":07A9
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin hControl2.hTextLabel Txt_Num_Cuenta 
            Height          =   345
            Left            =   6840
            TabIndex        =   28
            Top             =   255
            Width           =   5730
            _ExtentX        =   10107
            _ExtentY        =   609
            LabelWidth      =   1245
            TextMinWidth    =   1200
            Caption         =   "N� de Cuenta"
            Text            =   ""
            MaxLength       =   100
         End
         Begin VB.Label lbl_asesores 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Asesor"
            Height          =   315
            Left            =   120
            TabIndex        =   25
            Top             =   615
            Width           =   1245
         End
         Begin VB.Label Label8 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Instrumento"
            Height          =   315
            Left            =   6840
            TabIndex        =   24
            Top             =   615
            Width           =   1245
         End
         Begin VB.Label Label5 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Cliente"
            Height          =   315
            Left            =   120
            TabIndex        =   21
            Top             =   240
            Width           =   1245
         End
      End
      Begin VB.Frame Frame5 
         Height          =   735
         Left            =   120
         TabIndex        =   12
         Top             =   960
         Width           =   14415
         Begin TrueDBList80.TDBCombo Cmb_Tipo_Operacion 
            Height          =   315
            Left            =   6330
            TabIndex        =   13
            Tag             =   "SOLOLECTURA=N"
            Top             =   240
            Width           =   3000
            _ExtentX        =   5292
            _ExtentY        =   556
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   556
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=0,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=MS Sans Serif"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_Operaciones.frx":0853
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&,.bold=0,.fontsize=825"
            _StyleDefs(7)   =   ":id=1,.italic=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(8)   =   ":id=1,.fontname=MS Sans Serif"
            _StyleDefs(9)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(10)  =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(11)  =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(12)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(13)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(14)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(15)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(16)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(17)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(18)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(19)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(20)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(21)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(22)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(23)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(24)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(25)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(26)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(27)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(28)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(29)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(30)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(31)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(32)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(33)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(34)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(35)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(36)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(37)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(38)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(39)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(40)  =   "Named:id=33:Normal"
            _StyleDefs(41)  =   ":id=33,.parent=0"
            _StyleDefs(42)  =   "Named:id=34:Heading"
            _StyleDefs(43)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(44)  =   ":id=34,.wraptext=-1"
            _StyleDefs(45)  =   "Named:id=35:Footing"
            _StyleDefs(46)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(47)  =   "Named:id=36:Selected"
            _StyleDefs(48)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(49)  =   "Named:id=37:Caption"
            _StyleDefs(50)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(51)  =   "Named:id=38:HighlightRow"
            _StyleDefs(52)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(53)  =   "Named:id=39:EvenRow"
            _StyleDefs(54)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(55)  =   "Named:id=40:OddRow"
            _StyleDefs(56)  =   ":id=40,.parent=33"
            _StyleDefs(57)  =   "Named:id=41:RecordSelector"
            _StyleDefs(58)  =   ":id=41,.parent=34"
            _StyleDefs(59)  =   "Named:id=42:FilterBar"
            _StyleDefs(60)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Estado 
            Height          =   315
            Left            =   11250
            TabIndex        =   14
            Tag             =   "SOLOLECTURA=N"
            Top             =   240
            Width           =   3000
            _ExtentX        =   5292
            _ExtentY        =   556
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   556
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_Operaciones.frx":08FD
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Tipo_Movimiento 
            Height          =   315
            Left            =   1410
            TabIndex        =   15
            Tag             =   "SOLOLECTURA=N"
            Top             =   240
            Width           =   3000
            _ExtentX        =   5292
            _ExtentY        =   556
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   556
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_Operaciones.frx":09A7
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Label2 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Tipo Operaci�n"
            Height          =   315
            Left            =   5040
            TabIndex        =   18
            Top             =   240
            Width           =   1245
         End
         Begin VB.Label Label3 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Tipo Movimiento"
            Height          =   315
            Left            =   120
            TabIndex        =   17
            Top             =   240
            Width           =   1245
         End
         Begin VB.Label Label4 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Estado"
            Height          =   315
            Left            =   9960
            TabIndex        =   16
            Top             =   240
            Width           =   1245
         End
      End
      Begin VB.Frame Frame4 
         Height          =   735
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   6255
         Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
            Height          =   315
            Left            =   1380
            TabIndex        =   8
            Tag             =   "SOLOLECTURA=N"
            Top             =   240
            Width           =   1665
            _ExtentX        =   2937
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   71892993
            CurrentDate     =   37732
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
            Height          =   315
            Left            =   4380
            TabIndex        =   10
            Tag             =   "SOLOLECTURA=N"
            Top             =   240
            Width           =   1665
            _ExtentX        =   2937
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   71892993
            CurrentDate     =   37732
         End
         Begin VB.Label lbl_fecha_hasta 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Hasta"
            Height          =   315
            Left            =   3120
            TabIndex        =   11
            Top             =   240
            Width           =   1245
         End
         Begin VB.Label lbl_Fecha_desde 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Desde"
            Height          =   315
            Left            =   120
            TabIndex        =   9
            Top             =   240
            Width           =   1245
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "N� Operaci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   735
         Left            =   11350
         TabIndex        =   5
         Top             =   240
         Width           =   3165
         Begin hControl2.hTextLabel Txt_Num_Documento 
            Height          =   315
            Left            =   240
            TabIndex        =   6
            Tag             =   "SOLOLECTURA=N"
            Top             =   270
            Width           =   2700
            _ExtentX        =   4763
            _ExtentY        =   556
            LabelWidth      =   1100
            Caption         =   "N� Operaci�n"
            Text            =   "0"
            Text            =   "0"
            Format          =   "#,##0"
            Tipo_TextBox    =   1
            Alignment       =   1
            MaxLength       =   19
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   14865
      _ExtentX        =   26220
      _ExtentY        =   635
      ButtonWidth     =   2990
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "RESEARCH"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Busca las instrucciones sin confirmar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Anular"
            Key             =   "ANULAR"
            Description     =   "Anula Instrucciones Pendientes"
            Object.ToolTipText     =   "Anula las Instrucciones Pendientes"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Comprobante"
            Key             =   "BOLETA"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   4
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Consulta_Operaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: Frm_Consulta_Operaciones.frm $
'    $Author: Fcatrian $
'    $Date: 1-03-16 15:13 $
'    $Revision: 4 $
'-------------------------------------------------------------------------------------------------

Rem ---------------------------------------------------------------------------
Rem 03/04/2009. Controla fecha de consulta si es el permiso es sololectura
Rem             se considera la fecha de cierre virtual como fecha de consulta
Rem 28/07/2009. MMardones. Se traslada la columna Fecha Operaci�n a la 2da pos.
Rem 05/08/2009. MMardones. Se agrega columna P / N para indicar si las operaciones
Rem             fueron realizadas por Pesos o por Nominal (cambio s�lo en acciones)
Rem ---------------------------------------------------------------------------

Const fc_Mercado = "N"

Dim fOperacion As Boolean
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana
Dim fId_Operacion As String
Dim fFechaCierreVirtual As Date
Private Type datos_cliente
  Cuenta As String
  Nombre As String
  Rut As String
  Ejecutivo As String
End Type

Private Type datos_operacion
  Moneda_fondo As String
  Fecha_Operacion As Date
  Fecha_Vencimiento As Date
  Tipo_Operacion As String
  Mercado As String
  Por_Rueda As String
  Moneda_Operacion As String
  Cta_Operacion As String
  contraparte As String
  Cond_Liquidacion As String
  Monto_Operacion As String
  Iva As String
  Comision As String
  Por_Comision As String
  Derechos As String
  Gastos As String
  Otros As String
  Tipo_Movimiento As String
  Id_Operacion As String
  Cod_Instrumento As String
  Cod_Producto As String
  Cod_Tipo_Operacion As String
  Flg_Tipo_Movimiento As String
  Cod_Estado As String
End Type

Dim glb_cliente As datos_cliente
Dim glb_operacion As datos_operacion
Const cMovimiento_Acciones_Nac = "AD_QRY_CB_MOVTO_ACCIONES"

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
'  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  fFlg_Tipo_Permiso = Fnt_Obtiene_Permiso_Formulario(pCod_Arbol_Sistema)
 If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
    Toolbar.Buttons(2).Visible = False
End If
  
  Call Form_Resize
  

  Load Me
End Sub


Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("RESEARCH").Image = "boton_grilla_buscar"
      .Buttons("ANULAR").Image = cBoton_Cancelar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("BOLETA").Image = cBoton_Modificar
      .Buttons("PRINTER").Image = cBoton_Imprimir
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Nemotecnicos
    Set .ImageList = MDI_Principal.ImageListGlobal16
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("ALL").Image = "boton_seleccionar_todos"
    .Buttons("NALL").Image = "boton_seleccionar_ninguno"
  End With


  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub



Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not (Col = Grilla.ColIndex("CHK")) Then
    Cancel = True
  End If  'Cancel = True
End Sub

Private Sub Grilla_Click()
  With Grilla
    If .Row > 0 Then
        fId_Operacion = GetCell(Grilla, .Row, "colum_pk")
    End If
  End With
End Sub

Private Sub Grilla_DblClick()
Dim lKey As String
Dim lCod_Instrumento As String
Dim lFormulario As String
Dim lId_Cuenta As String
Dim lCod_Tipo_Operacion As String
Dim lFlg_Tipo_Movimiento As String

  With Grilla
    If .Row > 0 Then
      lKey = GetCell(Grilla, .Row, "colum_pk")
      lCod_Instrumento = GetCell(Grilla, .Row, "cod_instrumento")
      lFormulario = GetCell(Grilla, .Row, "formulario")
      lId_Cuenta = GetCell(Grilla, .Row, "id_cuenta")
      lCod_Tipo_Operacion = GetCell(Grilla, .Row, "Cod_Tipo_Operacion")
      lFlg_Tipo_Movimiento = GetCell(Grilla, .Row, "cod_Tipo_Movimiento")
      If (lCod_Instrumento = "RV_INT_INV" Or lCod_Instrumento = "RF_INT_INV" Or lCod_Instrumento = "FFMM_INT_INV") Then
        'no hace nada por ahora
      Else
        Call Sub_EsperaVentana(lKey, lCod_Instrumento, lFormulario, lId_Cuenta, lCod_Tipo_Operacion, lFlg_Tipo_Movimiento)
      End If
    End If
  End With
End Sub


Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "RESEARCH"
      Call Sub_CargarDatos
    Case "ANULAR"
      If MsgBox("�Esta seguro(a) que desea Anular las operaciones seleccionadas?.", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
        Call Sub_Anular
      End If
    Case "REFRESH"
      Call Sub_Limpiar
    Case "BOLETA"
      Call Sub_Generar_Comprobante
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Limpiar()
  
  Grilla.Rows = 1
  
  Call Sub_ComboSelectedItem(Cmb_Asesores, cCmbKALL)
  Call Sub_ComboSelectedItem(Cmb_Instrumento, cCmbKALL)
  Call Sub_ComboSelectedItem(Cmb_Tipo_Operacion, cCmbKALL)
  Call Sub_ComboSelectedItem(Cmb_Tipo_Movimiento, cCmbKALL)
  Call Sub_ComboSelectedItem(Cmb_Estado, cCmbKALL)
  
  DTP_Fecha_Desde.Value = Format(Fnt_FechaServidor, cFormatDate)
  DTP_Fecha_Hasta.Value = Format(Fnt_FechaServidor, cFormatDate)
  
  Txt_Num_Documento.Text = ""
  
  chk_TodasCtas.Value = 0
  Txt_Num_Cuenta.Text = ""
  Txt_Num_Cuenta.Tag = ""
  Txt_Num_Cuenta.Enabled = True
  cmb_buscar.Enabled = True
  
End Sub

Private Sub Sub_CargarDatos()
Dim lId_Operacion As String
Dim lId_Cuenta As String
Dim lId_Cliente As String
Dim lId_Asesor As String
Dim lCod_Instrumento As String
Dim lTipo_Operacion As String
Dim lTipo_Movimiento As String
Dim lFecha_Desde As String
Dim lFecha_Hasta As String
Dim lCod_Estado As String
'-----------------------------------
Dim lcOperaciones As Class_Operaciones
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim sTipoInversion As String        'Agregado 05/08/2009

  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Call Sub_Bloquea_Puntero(Me)
  
  Grilla.Rows = 1
  
 ' If fOperacion Then
  If Val(Txt_Num_Documento.Text) <> 0 Then

    lId_Operacion = Txt_Num_Documento.Text
    lFecha_Desde = ""
    lFecha_Hasta = ""
    lId_Cuenta = ""
    lId_Asesor = ""
    lCod_Instrumento = ""
    lTipo_Operacion = ""
    lTipo_Movimiento = ""
    lCod_Estado = ""
  Else
    If DTP_Fecha_Desde.Value > DTP_Fecha_Hasta.Value Then
      MsgBox "La Fecha Desde no puede ser mayor que la Fecha Hasta", vbExclamation, Me.Caption
      GoTo ErrProcedure
    End If
    
    If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
        If fFechaCierreVirtual > 0 Then
          If DTP_Fecha_Hasta.Value > fFechaCierreVirtual Then
            DTP_Fecha_Hasta.Value = fFechaCierreVirtual
          End If
        End If
    End If
    
    lId_Operacion = ""
    lFecha_Desde = DTP_Fecha_Desde.Value
    lFecha_Hasta = DTP_Fecha_Hasta.Value
    
    lId_Cuenta = IIf(chk_TodasCtas.Value = 1, "", Trim(Txt_Num_Cuenta.Tag))
    lId_Cliente = IIf(Fnt_ComboSelected_KEY(Cmb_Clientes) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Clientes))
    lId_Asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesores) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Asesores))
    lCod_Instrumento = IIf(Fnt_ComboSelected_KEY(Cmb_Instrumento) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Instrumento))
    lTipo_Operacion = IIf(Fnt_ComboSelected_KEY(Cmb_Tipo_Operacion) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Tipo_Operacion))
    lTipo_Movimiento = IIf(Fnt_ComboSelected_KEY(Cmb_Tipo_Movimiento) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Tipo_Movimiento))
    lCod_Estado = IIf(Fnt_ComboSelected_KEY(Cmb_Estado) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Estado))
  End If
    
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Id_Operacion").Valor = lId_Operacion
    .Campo("Cod_Instrumento").Valor = lCod_Instrumento
    .Campo("cod_producto").Valor = ""
    .Campo("Cod_Tipo_Operacion").Valor = lTipo_Operacion
    .Campo("Flg_Tipo_Movimiento").Valor = lTipo_Movimiento
    .Campo("cod_Estado").Valor = lCod_Estado
    If .Fnt_Buscar_Operaciones_Cliente(lFecha_Desde, lFecha_Hasta, lId_Cuenta, lId_Cliente, lId_Asesor) Then
       
      For Each lReg In .Cursor
      
        Select Case lReg("flg_tipo_movimiento").Value
          Case gcTipoOperacion_Ingreso
            lTipo_Movimiento = "Compra/Ingreso"
          Case gcTipoOperacion_Egreso
            lTipo_Movimiento = "Venta/Egreso"
          Case Else
            lTipo_Movimiento = ""
        End Select
        
        lLinea = Grilla.Rows
        Call Grilla.AddItem("")
        Call SetCell(Grilla, lLinea, "id_cuenta", lReg("ID_CUENTA").Value, False)
        Call SetCell(Grilla, lLinea, "num_cuenta", lReg("NUM_CUENTA").Value, False)
        Call SetCell(Grilla, lLinea, "colum_pk", lReg("ID_OPERACION").Value, False)
        Call SetCell(Grilla, lLinea, "cod_instrumento", lReg("COD_INSTRUMENTO").Value, False)
        Call SetCell(Grilla, lLinea, "dsc_instrumento", "" & lReg("DSC_INTRUMENTO").Value, False)
        Call SetCell(Grilla, lLinea, "cod_tipo_operacion", "" & lReg("cod_TIPO_OPERACION").Value, False)
        Call SetCell(Grilla, lLinea, "tipo_operacion", "" & lReg("DSC_TIPO_OPERACION").Value, False)
        Call SetCell(Grilla, lLinea, "cod_tipo_movimiento", "" & lReg("flg_tipo_movimiento").Value, False)
        Call SetCell(Grilla, lLinea, "tipo_Movimiento", lTipo_Movimiento, False)
        Call SetCell(Grilla, lLinea, "cod_estado", "" & lReg("cod_estado").Value, False)
        Call SetCell(Grilla, lLinea, "dsc_estado", "" & lReg("DSC_estado").Value, False)
        Call SetCell(Grilla, lLinea, "monto", "" & lReg("MONTO_OPERACION").Value, False)
        Call SetCell(Grilla, lLinea, "dsc_moneda_operacion", "" & lReg("DSC_MONEDA").Value, False)
        Call SetCell(Grilla, lLinea, "FechaOperacion", lReg("FECHA_OPERACION").Value, False)
        Call SetCell(Grilla, lLinea, "dsc_Asesor", "" & lReg("NOMBRE_Asesor").Value, False)
        Call SetCell(Grilla, lLinea, "formulario", "" & lReg("FORMULARIO").Value, False)
        sTipoInversion = Fnt_BuscaTipoInversionDetalle(lReg("id_operacion").Value)    'Agregado 05/08/2009
        Call SetCell(Grilla, lLinea, "tipo_inversion", sTipoInversion, False)
      Next
      
      Call Sub_AjustaColumnas_Grilla(Grilla)
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Operaciones.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  
  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_CargaForm()
Dim lComboList As String
Dim lReg As hFields

  Call Sub_Bloquea_Puntero(Me)
  

  Call Sub_FormControl_Color(Me.Controls)

  Rem Limpia la grilla
  Grilla.Rows = 1
  Call Sub_CargaCombo_Clientes(Cmb_Clientes, pTodos:=True)
  Call Sub_CargaCombo_Asesor(Cmb_Asesores, pTodos:=True)
  Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento, pTodos:=True)
  Call Sub_CargaCombo_Estados(Cmb_Estado, cTEstado_Operaciones, pTodos:=True)
  
  'Call Sub_CargaCombo_Tipos_Operaciones(Cmb_Tipo_Operacion, pTodos:=True)
  With Cmb_Tipo_Operacion
    Call .AddItem("Todos")
    Call .AddItem("Custodia")
    Call .AddItem("Suscripci�n")
    Call .AddItem("Directa")
    Call .AddItem("Instruccion")
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      .Add Fnt_AgregaValueItem(gcOPERACION_Custodia, "Custodia")
      .Add Fnt_AgregaValueItem(gcOPERACION_Custodia_SusCrip, "Opciones")
      .Add Fnt_AgregaValueItem(gcOPERACION_Directa, "Directa")
      .Add Fnt_AgregaValueItem(gcOPERACION_Instruccion, "Instrucci�n")
      .Translate = True
    End With
  End With
  
  With Cmb_Tipo_Movimiento
    Call .AddItem("Todos")
    Call .AddItem("Compra/Ingreso")
    Call .AddItem("Venta/Egreso")
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      .Add Fnt_AgregaValueItem(gcTipoOperacion_Ingreso, "Compra/Ingreso")
      .Add Fnt_AgregaValueItem(gcTipoOperacion_Egreso, "Venta/Egreso")
      .Translate = True
    End With
  End With
  
  Call Sub_Limpiar
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_EsperaVentana(pkey As String, _
                              pCod_Instrumento As String, _
                              pFormulario As String, _
                              pId_Cuenta As String, _
                              pCod_Tipo_Operacion As String, _
                              pFlg_Tipo_Movimiento As String)
Dim lClass As Object
Dim lForm As Object

  Me.Enabled = False
  
  Set lClass = Nothing
  Select Case UCase(pFormulario)
    Case "CLASS_ACCIONES"
      Set lClass = New Class_Acciones
    Case "CLASS_BONOS"
      Set lClass = New Class_Bonos
    Case "CLASS_FONDOSMUTUOS"
      Set lClass = New Class_FondosMutuos
    Case "CLASS_DEPOSITOS"
      Set lClass = New Class_Depositos
    Case "CLASS_PACTOS"
      Set lClass = New Class_Pactos
'Modulo Internacional agregado por MMA
    Case "CLASS_ACCIONES_INT"
      Set lClass = New Class_Acciones_Int
    Case "CLASS_BONOS_INT"
      Set lClass = New Class_Bonos_Int
    Case "CLASS_FONDOSMUTUOS_INT"
      Set lClass = New Class_FondosMutuos_Int
  End Select
  
  If Not lClass Is Nothing Then
    
    Set lForm = Nothing
    Set lForm = lClass.Form_Consulta_Operacion
    
    If Not lForm Is Nothing Then
      Call Sub_FormControl_Enabled(Me.Controls, Me, False)
      Call lForm.Consulta_Operacion(Me, pkey, pId_Cuenta, pCod_Instrumento, pCod_Tipo_Operacion, pFlg_Tipo_Movimiento)
      Call Sub_FormControl_Enabled(Me.Controls, Me, True)
    End If
  End If
  
  Me.Enabled = True
  
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

Private Sub Toolbar_Nemotecnicos_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim lFila As Long
  
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ALL"
      For lFila = 1 To Me.Grilla.Rows - 1
        Call SetCell(Me.Grilla, lFila, "CHK", -1, pAutoSize:=False)
      Next
    Case "NALL"
      For lFila = 1 To Me.Grilla.Rows - 1
        Call SetCell(Me.Grilla, lFila, "CHK", "", pAutoSize:=False)
      Next
  End Select
End Sub

Private Sub Txt_Num_Documento_KeyPress(KeyAscii As Integer)
'  If KeyAscii = vbKeyReturn Then
'    fOperacion = True
'    Call Sub_CargarDatos
'    fOperacion = False
'  Rem Acepta digitos, ".", "-" y "backspace"
'  ElseIf Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 59) Then
'    KeyAscii = 0
'  End If
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Consulta de Operaciones" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Sub_Anular()
Dim lcOperaciones As Class_Operaciones
'---------------------------------------------
Dim lFila As Long
Dim lRollback As Boolean
Dim Selecciono As Boolean
Dim Pregunta As Boolean

Pregunta = False
Const cNoSelect = "Primero debe seleccionar alguna operaci�n."

  If (ContarRegistros < 1) Then
    Call MsgBox(cNoSelect, vbInformation, Me.Caption)
    GoTo ExitProcedure
  End If
  
  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
  
  For lFila = 1 To Me.Grilla.Rows - 1
    If Not (GetCell(Grilla, lFila, "cod_estado") = cCod_Estado_Anulado) Then
      If GetCell(Me.Grilla, lFila, "CHK") = -1 Then
        If Not Fnt_VerificaAnulacion(lFila, Pregunta) Then
          GoTo ExitProcedure
        End If
        
        gDB.IniciarTransaccion
        lRollback = False
        
        With Grilla
          Set lcOperaciones = New Class_Operaciones
          With lcOperaciones
            .Campo("id_Operacion").Valor = GetCell(Grilla, lFila, "colum_pk")
            .Campo("id_Cuenta").Valor = GetCell(Grilla, lFila, "id_cuenta")
            If Not .Anular Then
              'Si existe un problema el proceso devuelve false
              lRollback = True
              GoTo ErrProcedure
            End If
          End With
        End With
        gDB.CommitTransaccion
      End If
    End If
  Next
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    MsgBox "Se produjo un Error al Anular operaciones." & vbCr & vbCr & gDB.ErrMsg, vbCritical
  Else
    'gDB.CommitTransaccion
    MsgBox "Las operaciones fueron Anuladas con �xito.", vbInformation
    Call Sub_CargarDatos
  End If
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
  Set lcOperaciones = Nothing
End Sub

Private Function Fnt_VerificaAnulacion(lFila As Long, ByRef Pregunta As Boolean) As Boolean
Dim lcOperaciones As Class_Operaciones
Dim lcMov_Activos As Class_Mov_Activos
Dim lcCierre_Cuenta As Class_Cierres_Cuentas
Dim lReg          As hFields
Dim lcCierre      As Class_Cierre
'-------------------------------------------------------
Dim lId_Operacion As Double
Dim lEncontroCierre As Boolean
Dim lFecha_Cierre   As Date
Dim lId_Cuenta      As Double
Dim pId_Cierre      As Double
  
Const cMsgError = "Problemas en la verificacion para la anulaci�n."
  
  Fnt_VerificaAnulacion = False

  lId_Operacion = GetCell(Grilla, lFila, "colum_pk")
  
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("id_Operacion").Valor = lId_Operacion
    If Not .BuscaConDetalles Then
      Call Fnt_MsgError(.SubTipo_LOG, cMsgError, .ErrMsg)
      GoTo ExitProcedure
    End If
    
    lId_Cuenta = .Campo("id_cuenta").Valor
  End With
  
  lEncontroCierre = False
  For Each lReg In lcOperaciones.Cursor
    Set lcMov_Activos = New Class_Mov_Activos
    With lcMov_Activos
      .Campo("id_operacion_detalle").Valor = lReg("id_operacion_detalle").Value
      If Not .Buscar Then
        Call Fnt_MsgError(.SubTipo_LOG, cMsgError, .ErrMsg)
        GoTo ExitProcedure
      End If
          
      If .Cursor.Count >= 1 Then
        'si el detalle de la operacion fue confirmada se verifica si ya fue conciderado en un cierre.
        If Not IsNull(.Cursor(1)("id_cierre").Value) Then
          lEncontroCierre = True
          pId_Cierre = .Cursor(1)("id_cierre").Value
          lFecha_Cierre = .Cursor(1)("fecha_cierre").Value
          Exit For
        End If
      End If
    End With
    Set lcMov_Activos = Nothing
  Next
  
  If lEncontroCierre Then
    If Not Pregunta Then
        If Not MsgBox("La anulaci�n de la operaciones obligara a reprocesar los cierres desde la fecha " & Format(lFecha_Cierre, cFormatDate) & _
                        " hacia adelante." & vbCr & vbCr & "�Desea realizar la anulaci�n de la operaci�n?", vbYesNo, "Anulaci�n de Operaciones ") = vbYes Then
          GoTo ExitProcedure
        End If
        Pregunta = True
    End If
    
    'aqui se eliminan los cierres que tienen enlazado la cuenta
    
    ' Verifica que la cuenta no este cerrada para el dia procesar.
    gDB.IniciarTransaccion
    Set lcCierre_Cuenta = New Class_Cierres_Cuentas
    With lcCierre_Cuenta
        .Campo("id_cuenta").Valor = lId_Cuenta
        .Campo("fecha_cierre").Valor = lFecha_Cierre
        If .Buscar Then
            If .Cursor.Count > 0 Then
                .Campo("id_cierre").Valor = pId_Cierre
                .Campo("COD_ESTADO").Valor = cCierre_Estado_Reprocesando
                If Not .Guardar_ReProcesos Then
                    GoTo ExitProcedure
                End If
            End If
        End If
    End With
    Set lcCierre = New Class_Cierre
    With lcCierre
      If Not .Fnt_PreparaCierreCuenta(lId_Cuenta, lFecha_Cierre, Nothing) Then
        gDB.RollbackTransaccion
        Call Fnt_MsgError(eLS_ErrSystem, "No se puede preparar la ", Err.Description)
        GoTo ExitProcedure
      End If
    End With
    Set lcCierre_Cuenta = Nothing
    Set lcCierre = Nothing
    gDB.CommitTransaccion
  End If

  Fnt_VerificaAnulacion = True

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, cMsgError, Err.Description)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Set lcOperaciones = Nothing
  Set lcMov_Activos = Nothing
  Set lcCierre = Nothing
End Function

Private Sub Sub_Generar_Comprobante()
Dim lId_Operacion As String
Dim lFormulario As Object
Dim lFila As Integer

    If Not Fnt_Form_Validar(Me.Controls) Then
      GoTo ErrProcedure
    End If
    
    Set lFormulario = Fnt_Generar_Comprobante(pTipoSalida:=ePrinter.eP_Pantalla)
    If Not lFormulario Is Nothing Then
      Call ColocarEn(lFormulario, Me, eFP_Abajo)
      

    End If
  
ErrProcedure:

End Sub

Private Function Fnt_Generar_Comprobante(ByVal pTipoSalida As ePrinter _
                                        , Optional ByRef pArchivo As String = "") As Object
'Dim lcOperaciones  As Class_Operaciones
'------------------------------------------------------------
Const clrHeader = &HD0D0D0
Dim sRecord
Dim bAppend
Dim lLinea As Integer
Dim lForm As Frm_Reporte_Generico
Dim lReg As hCollection.hFields
Dim a As Integer
Dim iFila As Integer
Dim Filas As Integer
Dim lFila As Integer
Dim bPrimeraHoja As Boolean
Dim lcOrdenMagic As Object
'-----------------------------------------------------------------------
Dim lOD_Cursor As hRecord 'CURSOR PARA EL DETALLE DE LAS OPERACIONES.

    Set Fnt_Generar_Comprobante = Nothing

    bPrimeraHoja = True
    Call Sub_Bloquea_Puntero(Me)
    Set lForm = New Frm_Reporte_Generico

    Call lForm.Sub_InicarDocumento(pTitulo:="" _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orPortrait _
                               , pImprimeSoloEmpresa:="S")
    With lForm.VsPrinter
     .StartDoc
            Call Sub_CargarDatos_Gen
    
            Select Case Fnt_DiasHabiles_EntreFechas(glb_operacion.Fecha_Operacion _
                                                    , glb_operacion.Fecha_Vencimiento)
                Case 0
                  glb_operacion.Cond_Liquidacion = "PH"  ' Pago Hoy
                Case 1
                  glb_operacion.Cond_Liquidacion = "PM"  ' Pago Ma�ana
                Case 2
                  glb_operacion.Cond_Liquidacion = "CN"  ' Contado Normal
            End Select
                 
            If Not bPrimeraHoja Then
                .NewPage
            Else
               bPrimeraHoja = False
            End If
            .FontName = "Arial"
            .SpaceBefore = "2pt"
            .SpaceAfter = "2pt"
            .HdrFontName = "Arial"
            .HdrFontSize = 10
            .HdrFontBold = True

            .MarginLeft = "15mm"
            .MarginRight = "15mm"
              
            
            .StartTable
              .TableBorder = tbNone
              .TableCell(tcRows) = 1
              .TableCell(tcCols) = 1
              .TableCell(tcFontSize, 1, 1, 2, 5) = 9
              .TableCell(tcAlign, 1, 1) = taCenterMiddle
              .TableCell(tcText, 1, 1) = "COMPROBANTE DE OPERACI�N " & glb_operacion.Tipo_Movimiento
              .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
            .EndTable
                    
            .Paragraph = "" 'salto de linea
            
            .StartTable
              .TableCell(tcRows) = 1
              .TableCell(tcCols) = 2
              .TableCell(tcFontSize, 1, 1) = 7
              .TableCell(tcColWidth, 1, 1) = "90mm"
              .TableCell(tcText, 1, 1) = "ANTECEDENTES GENERALES"
              .TableCell(tcColWidth, 1, 2) = "90mm"
              .TableCell(tcAlign, 1, 2) = taRightMiddle
              .TableCell(tcText, 1, 2) = "N� OP - " & fId_Operacion
            .EndTable
                
            .StartTable
              .TableCell(tcRows) = 6
              .TableCell(tcCols) = 9
              .TableCell(tcFontSize, 1, 1, 6, 9) = 6
              .TableBorder = tbBox
                
              .TableCell(tcColWidth, 1, 1) = "23mm"
              .TableCell(tcColWidth, 1, 2) = "5mm"
              .TableCell(tcColWidth, 1, 3) = "36mm"
              ' 64
              
              
              .TableCell(tcColWidth, 1, 4) = "23mm"
              .TableCell(tcColWidth, 1, 5) = "5mm"
              .TableCell(tcColWidth, 1, 6) = "25mm"
              ' 53
                
              .TableCell(tcColWidth, 1, 7) = "23mm"
              .TableCell(tcColWidth, 1, 8) = "5mm"
              .TableCell(tcColWidth, 1, 9) = "35mm"
              ' 63
              
              
              .TableCell(tcText, 1, 1) = "Fecha Operaci�n"
              .TableCell(tcText, 1, 2) = ":"
              .TableCell(tcText, 1, 3) = glb_operacion.Fecha_Operacion
              
              .TableCell(tcText, 2, 1) = "Cond. Liquidaci�n"
              .TableCell(tcText, 2, 2) = ":"
              .TableCell(tcText, 2, 3) = glb_operacion.Cond_Liquidacion
              
              .TableCell(tcText, 3, 1) = "Cod. Cuenta"
              .TableCell(tcText, 3, 2) = ":"
              .TableCell(tcText, 3, 3) = glb_cliente.Cuenta
              
              .TableCell(tcText, 4, 1) = "Rut Cliente"
              .TableCell(tcText, 4, 2) = ":"
              .TableCell(tcText, 4, 3) = FormatoRut(Trim(glb_cliente.Rut))
              
              .TableCell(tcText, 5, 1) = "Nombre Cliente"
              .TableCell(tcText, 5, 2) = ":"
              .TableCell(tcText, 5, 3) = UCase(glb_cliente.Nombre)
        
              .TableCell(tcText, 6, 1) = "Contraparte"
              .TableCell(tcText, 6, 2) = ":"
              .TableCell(tcText, 6, 3) = UCase(glb_operacion.contraparte)
              
              .TableCell(tcText, 1, 4) = "Fecha Vencimiento"
              .TableCell(tcText, 1, 5) = ":"
              .TableCell(tcText, 1, 6) = glb_operacion.Fecha_Vencimiento
              
              .TableCell(tcText, 2, 4) = "Moneda"
              .TableCell(tcText, 2, 5) = ":"
              .TableCell(tcText, 2, 6) = glb_operacion.Moneda_Operacion
              
              .TableCell(tcText, 3, 4) = "Cta. Movimiento"
              .TableCell(tcText, 3, 5) = ":"
              .TableCell(tcText, 3, 6) = glb_operacion.Cta_Operacion
              
              .TableCell(tcText, 4, 4) = "Ejecutivo"
              .TableCell(tcText, 4, 5) = ":"
              .TableCell(tcText, 4, 6) = UCase(glb_cliente.Ejecutivo)
              
        
              .TableCell(tcText, 1, 7) = "Tipo Operaci�n"
              .TableCell(tcText, 1, 8) = ":"
              .TableCell(tcText, 1, 9) = glb_operacion.Tipo_Operacion
              
              .TableCell(tcText, 2, 7) = "Mercado"
              .TableCell(tcText, 2, 8) = ":"
              .TableCell(tcText, 2, 9) = UCase(glb_operacion.Mercado)
              
              .TableCell(tcText, 3, 7) = "Por Rueda"
              .TableCell(tcText, 3, 8) = ":"
              .TableCell(tcText, 3, 9) = ""
            .EndTable
                
            .StartTable
              .TableCell(tcRows) = 1
              .TableCell(tcCols) = 1
              .TableCell(tcFontSize, 1, 1) = 7
              .TableCell(tcColWidth, 1, 1) = "180mm"
              .TableCell(tcText, 1, 1) = "DETALLE OPERACI�N"
              .TableBorder = tbNone
            .EndTable
                
                
            .StartTable
              .TableCell(tcRows) = 1
              .TableCell(tcCols) = 5
              .TableCell(tcFontSize, 1, 1, 1, 5) = 7
              .TableCell(tcColWidth, 1, 1) = "14mm"
              .TableCell(tcColWidth, 1, 2) = "50mm"
              .TableCell(tcColWidth, 1, 3) = "44mm"
              .TableCell(tcColWidth, 1, 4) = "38mm"
              .TableCell(tcColWidth, 1, 5) = "34mm"
              .TableBorder = tbBoxColumns
              .TableCell(tcAlign, 1, 1, 1, 5) = taCenterMiddle
              
              .TableCell(tcText, 1, 1) = "Orden"
              .TableCell(tcText, 1, 2) = "Instrumento"
              .TableCell(tcText, 1, 3) = "Cantidad o Nominal"
              .TableCell(tcText, 1, 4) = "Precio o Tasa"
              .TableCell(tcText, 1, 5) = "Monto Operaci�n"
            .EndTable
            
            .StartTable
              '.TableCell(tcRows) = 4
              .TableCell(tcCols) = 5
              .TableBorder = tbBoxColumns
              
              .TableCell(tcColWidth, 1, 1) = "14mm"
              .TableCell(tcColWidth, 1, 2) = "50mm"
              .TableCell(tcColWidth, 1, 3) = "44mm"
              .TableCell(tcColWidth, 1, 4) = "38mm"
              .TableCell(tcColWidth, 1, 5) = "34mm"
              
              If Operaciones_Detalle_BuscarView(lOD_Cursor) Then
               ' Set lOD_Cursor = gDB.Cursor
                Filas = lOD_Cursor.Count
                If Filas < 50 Then
                  Filas = 50
                End If
                .TableCell(tcRows) = Filas
                
                a = 1
                glb_operacion.Monto_Operacion = 0
                
                For Each lReg In lOD_Cursor
                     .TableCell(tcText, a, 1) = Fnt_Lee_Orden_Externa(lReg("ID_OPERACION_DETALLE").Value)
                     .TableCell(tcText, a, 2) = lReg("nemotecnico").Value
                     .TableCell(tcText, a, 3) = FormatNumber(lReg("cantidad").Value)
                     .TableCell(tcText, a, 4) = FormatNumber(lReg("precio").Value)
                     .TableCell(tcText, a, 5) = FormatNumber(lReg("monto_pago").Value)
                     glb_operacion.Monto_Operacion = CDbl(glb_operacion.Monto_Operacion) + CDbl(lReg("monto_pago").Value)
                a = a + 1
                Next
                Set lcOrdenMagic = Nothing
              End If
              
            .TableCell(tcFontSize, 1, 1, Filas, 5) = 6
            .TableCell(tcAlign, 1, 2, Filas, 5) = taRightMiddle
            .TableCell(tcAlign, 1, 2) = taLeftMiddle
            .EndTable
            
            .MarginLeft = "122.95mm"
            
            .StartTable
              
              .TableBorder = tbBoxColumns
              .TableCell(tcRows) = 1
              .TableCell(tcCols) = 2
              .TableCell(tcFontSize, 1, 1, 1, 3) = 6
              .TableCell(tcColWidth, 1, 1) = "38mm"
              .TableCell(tcColWidth, 1, 2) = "34mm"
              .TableCell(tcText, 1, 1) = "SUB-TOTAL"
              .TableCell(tcText, 1, 2) = FormatNumber(glb_operacion.Monto_Operacion)
              .TableCell(tcAlign, 1, 2) = taRightMiddle
        
            .EndTable
            
            .Paragraph = "" 'salto de linea
            .StartTable
              .TableCell(tcRows) = 1
              .TableCell(tcCols) = 1
              .TableCell(tcFontSize, 1, 1) = 6
              .TableCell(tcColWidth, 1, 1) = "72mm"
              .TableCell(tcText, 1, 1) = "DETALLE DE GASTOS"
              .TableBorder = tbNone
              
            .EndTable
            
            .StartTable
              .TableCell(tcRows) = 5
              .TableCell(tcCols) = 2
              .TableBorder = tbBoxColumns
              .TableCell(tcFontSize, 1, 1, 5, 2) = 6
              .TableCell(tcColWidth, 1, 1) = "38mm"
              .TableCell(tcColWidth, 1, 2) = "34mm"
              glb_operacion.Por_Comision = CDbl(glb_operacion.Por_Comision) * 100
              .TableCell(tcText, 1, 1) = "Comisi�n Corredor " & "( " & glb_operacion.Por_Comision & "% )"
              .TableCell(tcText, 2, 1) = "Derechos de Bolsa"
              .TableCell(tcText, 3, 1) = "Otros"
              .TableCell(tcText, 4, 1) = "Gastos de Transferencia"
              .TableCell(tcText, 5, 1) = "IVA"
              
              .TableCell(tcAlign, 1, 2) = taRightMiddle
              .TableCell(tcAlign, 2, 2) = taRightMiddle
              .TableCell(tcAlign, 3, 2) = taRightMiddle
              .TableCell(tcAlign, 4, 2) = taRightMiddle
              .TableCell(tcAlign, 5, 2) = taRightMiddle
              
              .TableCell(tcText, 1, 2) = FormatNumber(NVL(glb_operacion.Comision, 0))
              .TableCell(tcText, 2, 2) = FormatNumber(NVL(glb_operacion.Derechos, 0))
              .TableCell(tcText, 3, 2) = FormatNumber(NVL(glb_operacion.Otros, 0))
              .TableCell(tcText, 4, 2) = FormatNumber(NVL(glb_operacion.Gastos, 0))
              .TableCell(tcText, 5, 2) = FormatNumber(NVL(glb_operacion.Iva, 0))
            .EndTable
        
            .StartTable
              .TableBorder = tbBoxColumns
              .TableCell(tcRows) = 1
              .TableCell(tcCols) = 2
              .TableCell(tcFontSize, 1, 1, 1, 3) = 6
              .TableCell(tcColWidth, 1, 1) = "38mm"
              .TableCell(tcColWidth, 1, 2) = "34mm"
              .TableCell(tcText, 1, 1) = "TOTAL"
              .TableCell(tcAlign, 1, 2) = taRightMiddle
              .TableCell(tcText, 1, 2) = FormatNumber(CDbl(glb_operacion.Comision) + CDbl(glb_operacion.Derechos) + CDbl(glb_operacion.Gastos) + CDbl(glb_operacion.Otros) + CDbl(glb_operacion.Iva) + CDbl(glb_operacion.Monto_Operacion))
            .EndTable
        
            .MarginLeft = "25mm"
            
            .StartTable
              .TableBorder = tbNone
              .TableCell(tcRows) = 3
              .TableCell(tcCols) = 1
              .TableCell(tcFontSize, 1, 1, 3, 1) = 6
              .TableCell(tcColWidth, 1, 1) = "60mm"
              .TableCell(tcText, 1, 1) = "_______________________________________"
              .TableCell(tcText, 2, 1) = "     Security Corredores de Bolsa"
              .TableCell(tcText, 3, 1) = "Adm. de Cartera"
              .TableCell(tcAlign, 1, 1, 3, 1) = taCenterMiddle
            .EndTable
                
     '     End If
      .EndDoc
    End With

  Set Fnt_Generar_Comprobante = lForm
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Function

Public Sub Sub_CargarDatos_Gen()
'------------------------------------------
Dim lReg As hFields
Dim lId_Cuenta As Integer 'Double
Dim lCursor As hRecord
'------------------------------------------
Dim lcOperaciones As New Class_Operaciones
Dim lcCuentas As Object
Call Sub_Bloquea_Puntero(Me)
'---------------------------------------
     
  Call Operaciones_BuscarView(lId_Cuenta)
  Call Cuentas_BuscarView(lId_Cuenta)
Call Sub_Desbloquea_Puntero(Me)
End Sub

Function FormatoRut(lrut As String) As String
Dim rut_1 As String
Dim rut_2 As String
Dim rut_c As String
Dim rut_t As String
Dim dv As String
rut_1 = Len(lrut)
rut_c = Left(lrut, rut_1 - 2)
dv = Right(lrut, 1)
rut_2 = Len(rut_c)
While Len(rut_c) > 2
rut_2 = Len(rut_c)
rut_t = "." & Right(rut_c, 3) & rut_t
rut_c = Left(rut_c, rut_2 - 3)
Wend
FormatoRut = rut_c & rut_t & "-" & dv
End Function
Public Function Operaciones_Detalle_BuscarView(ByRef pCursor As hRecord) As Boolean

  Dim lClass_Entidad As Class_Entidad

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
'    Set .gDB = gDB
    
    Call .AddCampo("Id_Operacion", ePT_Numero, ePD_Entrada, fId_Operacion)
    
    Operaciones_Detalle_BuscarView = .Buscar("PKG_OPERACIONES_DETALLE.BuscarView")
    Set pCursor = .Cursor
'    Errnum = lClass_Entidad.Errnum
'    ErrMsg = lClass_Entidad.ErrMsg
  End With
  Set lClass_Entidad = Nothing

End Function

Public Sub Operaciones_BuscarView(ByRef pId_Cuenta As Integer)
Dim lClass_Entidad As Class_Entidad
Dim lReg
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
    'Set .gDB = gDB
    
    Call .AddCampo("Id_Operacion", ePT_Numero, ePD_Entrada, fId_Operacion)
    
    If .Buscar("PKG_OPERACIONES.BuscarView") Then
        If .Cursor.Count > 0 Then
             Set lReg = .Cursor(1)
             pId_Cuenta = lReg("id_cuenta").Value
             glb_operacion.Fecha_Operacion = lReg("Fecha_Operacion").Value
             glb_operacion.Moneda_Operacion = lReg("dsc_moneda").Value
             glb_operacion.Fecha_Vencimiento = lReg("fecha_liquidacion").Value
             glb_operacion.Mercado = Trim(lReg("dsc_producto").Value)
             glb_cliente.Cuenta = Trim(lReg("id_cuenta").Value)
             glb_operacion.Tipo_Operacion = lReg("DSC_TIPO_OPERACION").Value
             glb_operacion.contraparte = "" & lReg("DSC_contraparte").Value
             glb_operacion.Cta_Operacion = Entrega_Caja_Cargo_Abono(NVL(lReg("id_cargo_abono").Value, 0))
            
             'glb_operacion.Monto_Operacion = NVL(lReg("monto_operacion").Value, 0)
             glb_operacion.Comision = NVL(lReg("comision").Value, 0)
             glb_operacion.Derechos = NVL(lReg("derechos").Value, 0)
             glb_operacion.Iva = NVL(lReg("iva").Value, 0)
             glb_operacion.Por_Comision = NVL(lReg("porc_comision").Value, 0)
             glb_operacion.Otros = 0
             glb_operacion.Gastos = NVL(lReg("gastos").Value, 0)
             glb_operacion.Tipo_Movimiento = IIf(lReg("FLG_TIPO_MOVIMIENTO").Value = "I", "COMPRA", "VENTA")
       End If
    Else
        MsgBox "Error al traer Operaciones " & lClass_Entidad.ErrMsg, vbInformation
    End If
  End With
  Set lClass_Entidad = Nothing
End Sub


Public Sub Cuentas_BuscarView(pId_Cuenta As Integer)
Dim lClass_Entidad As Class_Entidad
Dim lCursor As hRecord
Dim lReg
  
     
  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad

    Call .AddCampo("Id_Cuenta", ePT_Numero, ePD_Entrada, pId_Cuenta)

    If .Buscar("PKG_CUENTAS.BuscarView") Then
        Set lReg = .Cursor(1)
        glb_cliente.Nombre = "" & lReg("nombre_cliente").Value
        glb_cliente.Rut = "" & lReg("rut_cliente").Value
        glb_cliente.Ejecutivo = "" & lReg("dsc_asesor").Value
    Else
        MsgBox "Error al traer Cuentas " & lClass_Entidad.ErrMsg, vbInformation
    End If
  End With
  Set lClass_Entidad = Nothing

End Sub


Function Entrega_Caja_Cargo_Abono(lId_cargo_abono As String) As String
Dim lClass_Entidad As Class_Entidad
  
    Set lClass_Entidad = New Class_Entidad
    With lClass_Entidad
      
        Call .AddCampo("id_cargo_abono", ePT_Numero, ePD_Entrada, lId_cargo_abono)
        If .Buscar("Pkg_Cargos_Abonos.Buscar") Then
            If .Cursor.Count > 0 Then
                Entrega_Caja_Cargo_Abono = .Cursor(1).Fields("dsc_caja_cuenta").Value
            End If
        Else
            MsgBox "Error al traer Descripci�n Caja Cuenta" & lClass_Entidad.ErrMsg, vbInformation
        End If
    End With
    Set lClass_Entidad = Nothing

End Function

Public Function Fnt_Lee_Orden_Externa(pId_Operacion_Detalle) As String
Dim lcAlias  As Object
Dim lFolioOrden

    lFolioOrden = Null
    Set lcAlias = CreateObject(cDLL_Alias)
    Set lcAlias.gDB = gDB
        
    lFolioOrden = lcAlias.AliasCSBPI2SYSTEM(pCodigoCSBPI:=cTabla_Operacion_Detalle _
                                         , pCodigoSYSTEM:=cMovimiento_Acciones_Nac _
                                         , pId_Entidad:=pId_Operacion_Detalle)
    If IsNull(lFolioOrden) Then
        lFolioOrden = 0
    End If
      
ExitProcedure:
  Fnt_Lee_Orden_Externa = lFolioOrden
End Function

'Public Function Fnt_VerificaPermiso(pCod_Arbol_Sistema)
'Dim lcRel_Roles_Arbol_Sistema As Class_Rel_Roles_Arbol_Sistema
'Dim lControl As VB.Control
''-----------------------------------------------------
'Dim lFlg_Tipo_Permiso
'
'On Error GoTo ErrProcedure
'
'  Set lcRel_Roles_Arbol_Sistema = New Class_Rel_Roles_Arbol_Sistema
'  With lcRel_Roles_Arbol_Sistema
'    If Not .Buscar_Permiso(pCod_Arbol_Sistema:=pCod_Arbol_Sistema _
'                         , pId_Usuario:=gID_Usuario) Then
'      Call Fnt_MsgError(.SubTipo_LOG, "Problemas en la busqueda de las relaciones de Roles/Arbol Sistema", .ErrMsg, pConLog:=True)
'      GoTo ExitProcedure
'    End If
'
'    If .Cursor.Count <= 0 Then
'      MsgBox "No tiene permisos para ver esta opci�n.", vbCritical
'      GoTo ExitProcedure
'    End If
'  End With
'
'  lFlg_Tipo_Permiso = lcRel_Roles_Arbol_Sistema.Cursor(1)("FLG_TIPO_PERMISO").Value
'  Fnt_VerificaPermiso = lFlg_Tipo_Permiso
'
'End Function

Private Function Fnt_Obtiene_FechaCierre_Virtual(ByVal pId_Cuenta As String, ByRef pFecha As Date) As Boolean
Dim lcCuentas As Object
Dim lResult As Boolean

    lResult = True
    Set lcCuentas = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuentas
        .Campo("id_cuenta").Valor = pId_Cuenta
        If .Buscar Then
            If NVL(.Cursor(1)("fecha_cierre_virtual").Value, "") = "" Then
               lResult = False
            Else
                pFecha = .Cursor(1)("fecha_cierre_virtual").Value
            End If
        End If
    End With
    Fnt_Obtiene_FechaCierre_Virtual = lResult
End Function
'Agregado 05/08/2009
Private Function Fnt_BuscaTipoInversionDetalle(ByVal lId_Operacion As String) As String
Dim lcOperacionesDetalle As Class_Operaciones_Detalle
Dim sTipoInversion As String

    Set lcOperacionesDetalle = New Class_Operaciones_Detalle
    With lcOperacionesDetalle
        .Campo("id_operacion").Valor = lId_Operacion
        If .Buscar Then
            If .Cursor.Count > 0 Then
                sTipoInversion = NVL(.Cursor(1)("tipo_inversion").Value, "N")
            Else
                sTipoInversion = "N"
            End If
        End If
    End With
    Fnt_BuscaTipoInversionDetalle = sTipoInversion
End Function


Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call Lpr_Buscar_Cuenta("TXT")
    End If
End Sub

Private Sub cmb_buscar_Click()
Dim lId_Cuenta As Integer

    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)
    If lId_Cuenta <> 0 Then
        Txt_Num_Cuenta.Text = ""
        Txt_Num_Cuenta.Tag = lId_Cuenta
        Call Lpr_Buscar_Cuenta("", CStr(lId_Cuenta))
    End If
End Sub

Private Sub Lpr_Buscar_Cuenta(ByVal Origen As String, Optional p_Id_Cuenta As String)
Dim lcCuenta      As Object
Dim lFechaCierreVirtual As Date
Dim lFechaCierre        As Date

    If Origen = "TXT" Then
        If InStr(1, Txt_Num_Cuenta.Text, "-") > 0 Then
           Txt_Num_Cuenta.Text = Trim(Left(Txt_Num_Cuenta.Text, InStr(1, Txt_Num_Cuenta.Text, "-") - 1))
        End If
        p_Id_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text), 0)
    End If
    If p_Id_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = p_Id_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                    If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
                        lFechaCierre = DTP_Fecha_Hasta.Value
                        fFechaCierreVirtual = Fnt_FechaServidor
                        DTP_Fecha_Hasta.Value = Fnt_FechaServidor
                        DTP_Fecha_Desde.Value = Fnt_FechaServidor
                        If Fnt_Obtiene_FechaCierre_Virtual(p_Id_Cuenta, fFechaCierreVirtual) Then
                            If DTP_Fecha_Hasta.Value > fFechaCierreVirtual Then
                                DTP_Fecha_Hasta.Value = fFechaCierreVirtual
                                If DTP_Fecha_Hasta.Value < DTP_Fecha_Desde.Value Then
                                    DTP_Fecha_Desde.Value = fFechaCierreVirtual
                                End If
                            End If
                        Else
                            DTP_Fecha_Hasta.Value = lFechaCierre
                        End If
                    End If
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
            Set lcCuenta = Nothing
    End If
End Sub

Private Sub chk_TodasCtas_Click()

If chk_TodasCtas.Value = 1 Then
    Txt_Num_Cuenta.Text = ""
    Txt_Num_Cuenta.Tag = ""
    Txt_Num_Cuenta.Enabled = False
    cmb_buscar.Enabled = False
Else
    Txt_Num_Cuenta.Enabled = True
    cmb_buscar.Enabled = True
End If

End Sub

Private Function ContarRegistros() As Integer
    Dim Contador As Integer
    Dim lFila As Long

    Contador = 0
    For lFila = 1 To Me.Grilla.Rows - 1
        If GetCell(Me.Grilla, lFila, "CHK") = -1 Then
            Contador = Contador + 1
        End If
    Next
    ContarRegistros = Contador
End Function

