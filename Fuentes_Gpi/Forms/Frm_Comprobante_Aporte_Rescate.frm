VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Comprobante_Aporte_Rescate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de Comprobantes Aportes/Retiro"
   ClientHeight    =   6450
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   9360
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6450
   ScaleWidth      =   9360
   Begin VB.Frame Frame1 
      Caption         =   "Comprobantes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4665
      Left            =   0
      TabIndex        =   0
      Top             =   1710
      Width           =   9345
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3915
         Left            =   90
         TabIndex        =   1
         Top             =   630
         Width           =   9165
         _cx             =   16166
         _cy             =   6906
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   27
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Comprobante_Aporte_Rescate.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Chequeo 
         Height          =   330
         Left            =   90
         TabIndex        =   12
         Top             =   240
         Width           =   1890
         _ExtentX        =   3334
         _ExtentY        =   582
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Wrappable       =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_ALL"
               Description     =   "Selecciona todos los items"
               Object.ToolTipText     =   "Selecciona todos los items"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_NOTHING"
               Description     =   "Deselecciona todos los items"
               Object.ToolTipText     =   "Deselecciona todos los items"
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "INV_SEL"
               Description     =   "Cambia la Seleccion de los items"
               Object.ToolTipText     =   "Cambia la Seleccion de los items"
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filtros"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1335
      Left            =   0
      TabIndex        =   4
      Top             =   360
      Width           =   9345
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   5950
         Picture         =   "Frm_Comprobante_Aporte_Rescate.frx":0494
         TabIndex        =   13
         Top             =   300
         Width           =   375
      End
      Begin TrueDBList80.TDBCombo Cmb_Clientes 
         Height          =   345
         Left            =   930
         TabIndex        =   5
         Tag             =   "OBLI=S;CAPTION=Clientes"
         Top             =   300
         Width           =   4965
         _ExtentX        =   8758
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Comprobante_Aporte_Rescate.frx":079E
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
         Height          =   345
         Left            =   3930
         TabIndex        =   7
         Top             =   840
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   609
         _Version        =   393216
         Format          =   65667073
         CurrentDate     =   38938
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ini 
         Height          =   345
         Left            =   1290
         TabIndex        =   8
         Top             =   840
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   609
         _Version        =   393216
         Format          =   65667073
         CurrentDate     =   38938
      End
      Begin MSComctlLib.Toolbar Toolbar_Proceso 
         Height          =   330
         Left            =   5400
         TabIndex        =   11
         Top             =   840
         Width           =   1020
         _ExtentX        =   1799
         _ExtentY        =   582
         ButtonWidth     =   1561
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar"
               Key             =   "BUSC"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
            EndProperty
         EndProperty
      End
      Begin VB.Label Lbl_Fecha_Ini 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Inicio"
         Height          =   345
         Index           =   1
         Left            =   90
         TabIndex        =   10
         Top             =   840
         Width           =   1185
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha T�rmino"
         Height          =   345
         Index           =   0
         Left            =   2730
         TabIndex        =   9
         Top             =   840
         Width           =   1185
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Clientes"
         Height          =   330
         Left            =   120
         TabIndex        =   6
         Top             =   300
         Width           =   795
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   9360
      _ExtentX        =   16510
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESCAR"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   3
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PRINT"
                  Text            =   "a Impresora"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Comprobante_Aporte_Rescate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fTipo_Permiso As String
Dim lId_Cliente As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  Me.Show
End Sub



Private Sub cmb_buscar_Click()
    lId_Cliente = NVL(Frm_Busca_Cuentas.BuscarCliente(), 0)
    
    If lId_Cliente <> 0 Then
        Call Sub_ComboSelectedItem(Cmb_Clientes, lId_Cliente)
    End If
End Sub

Private Sub Form_Load()
    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("REFRESCAR").Image = cBoton_Refrescar
            .Buttons("PRINTER").Image = cBoton_Imprimir
            .Buttons("EXIT").Image = cBoton_Salir
    End With
  
    With Toolbar_Chequeo
        Set .ImageList = MDI_Principal.ImageListGlobal16
           .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
           .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
           .Buttons("INV_SEL").Image = "boton_seleccionar_todos"
           .Appearance = ccFlat
    End With
  
    With Toolbar_Proceso
        Set .ImageList = MDI_Principal.ImageListGlobal16
         .Buttons("BUSC").Image = cBoton_Modificar
         .Appearance = ccFlat
    End With
  
    Call Sub_CargaForm

    Me.Top = 1
    Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub


Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla.ColIndex("CHK") Then
    Cancel = True
  'ElseIf GetCell(Grilla, Row, "flg_total") = gcFlg_SI Then
  '  Cancel = True
  End If
End Sub


Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "REFRESCAR"
      Call Sub_CargaForm
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
    Case "EXIT"
      Unload Me
  End Select
End Sub


Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lLinea      As Long
Dim lCol        As Long
Dim iIdComprobante As Long
        
    lCol = Grilla.ColIndex("CHK")
    
    For lLinea = 1 To Grilla.Rows - 1
        If Grilla.Cell(flexcpChecked, lLinea, lCol) = flexChecked Then
            iIdComprobante = GetCell(Grilla, lLinea, "ID_COMPROBANTE")
            'Agrega pregunta por MMA.
            If MsgBox("Genera Comprobante Aporte Rescate?." _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
                GenerarComprobante lLinea, pTipoSalida
            End If
        End If
    Next
End Sub

Private Sub GenerarComprobante(ByVal lLinea As Long, pTipoSalida As ePrinter)
    Dim DocWord         As New Class_Comprobante_Aporte_Rescate
    '-----------------------------------------------------------
    Dim lCodEjecutivo   As String
    Dim lCodSucursal    As String
    Dim iIdComprobante  As String
    Dim lestado         As String
    Dim lNomEjecutivo   As String
    
    Dim lReg            As hFields
    '-----------------------------------------------------------
    Dim lNemotecnico    As String
    Dim lCantidad       As String
    Dim lPrecio         As String
    Dim lTotal          As String
    '-----------------------------------------------------------
    Dim lFormato        As String
    Const sHeader = "|INSTRUMENTO|CANTIDAD|PRECIO|TOTAL"
    
    Dim nTotalDocumento As Double
    
    Dim lForm       As Frm_Reporte_Generico
    Dim sRutaImagen As String
    Set lForm = New Frm_Reporte_Generico
    
    sRutaImagen = App.Path & "\Logo Security.jpg"
                            
    lForm.fTipoSalida = pTipoSalida
    lForm.VSPDF8.Title = "Comprobante Aporte-Rescate"
    With lForm.VsPrinter
        
        .Font.Name = "Arial"
        .Font.Size = 10
        
        .Orientation = orLandscape

        .SpaceBefore = 0
        .SpaceAfter = 0
        .Clear
        .Orientation = orPortrait
        .StartDoc
        .FontBold = True
        .FontSize = 12
        .TextAlign = taRightTop
        .Paragraph = "N� Folio    " & GetCell(Grilla, lLinea, "NUMERO_FOLIO")
        .TextAlign = taCenterTop
        .Paragraph = "COMPROBANTE DE " & GetCell(Grilla, lLinea, "TIPO_MOVIMIENTO") & " CAPITAL " & IIf(lestado = "ANULADO", "(ANULADO)", "")
        .TextAlign = taLeftTop
        .FontBold = False
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .FontSize = 10
        .Paragraph = "FECHA" & vbTab & vbTab & ":   " & GetCell(Grilla, lLinea, "FECHA")
        .Paragraph = "CLIENTE" & vbTab & ":   " & GetCell(Grilla, lLinea, "NOMBRE_CLIENTE")
        .Paragraph = "RUT" & vbTab & vbTab & ":   " & GetCell(Grilla, lLinea, "RUT_CLIENTE")
        .Paragraph = "EJECUTIVO" & vbTab & ":   " & GetCell(Grilla, lLinea, "NOMBRE_ASESOR")
        .Paragraph = "DIRECCION" & vbTab & ":   " & GetCell(Grilla, lLinea, "direccion")
        .Paragraph = "CUENTA" & vbTab & ":   " & GetCell(Grilla, lLinea, "num_cuenta")
        If GetCell(Grilla, lLinea, "tipo_entidad") = "J" Then
            .Paragraph = "REP. LEGAL" & vbTab & ":   " & GetCell(Grilla, lLinea, "replegal")
        Else
            .Paragraph = ""
        End If
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = "Sr. Cliente se informa a Ud., que con esta fecha se han " & IIf(Trim(GetCell(Grilla, lLinea, "TIPO_MOVIMIENTO")) = "RETIRO", "entregado", "recibido") & " por concepto de " & GetCell(Grilla, lLinea, "TIPO_MOVIMIENTO") & " de Capital los instrumentos que se detallan, los cuales en este  acto " & IIf(Trim(GetCell(Grilla, lLinea, "TIPO_MOVIMIENTO")) = "RETIRO", "dejan de", "pasan a") & " formar parte integra de vuestro patrimonio administrado por " & gDsc_Empresa & "."
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .StartTable
            .TableBorder = tbBox
            .TableCell(tcCols) = 4
            .TableCell(tcRows) = 1
            .TableCell(tcAlign, 1, 1) = taCenterMiddle
            .TableCell(tcAlign, 1, 2) = taCenterMiddle
            .TableCell(tcAlign, 1, 3) = taCenterMiddle
            .TableCell(tcAlign, 1, 4) = taCenterMiddle
            .TableBorder = tbAll
            .TableCell(tcFontSize, 1, 1, 1, 1) = 10
            .TableCell(tcFontBold, 1, 1, 1, 4) = True
            .TableCell(tcFontBold, 2, 1, 2, 4) = False
            
            .TableCell(tcText, 1, 1) = "INSTRUMENTO"
            .TableCell(tcText, 1, 2) = "CANTIDAD"
            .TableCell(tcText, 1, 3) = "PRECIO"
            .TableCell(tcText, 1, 4) = "TOTAL"
            .TableCell(tcColWidth, 1, 1) = "55mm"
            .TableCell(tcColWidth, 1, 2) = "35mm"
            .TableCell(tcColWidth, 1, 3) = "35mm"
            .TableCell(tcColWidth, 1, 4) = "35mm"
            .FontBold = False

            DocWord.Campo("ID_COMPROBANTE").Valor = GetCell(Grilla, lLinea, "ID_COMPROBANTE")
            lFormato = Fnt_Formato_Moneda(GetCell(Grilla, lLinea, "ID_MONEDA"))
            If DocWord.Buscar_DetalleComprobante() Then
                For Each lReg In DocWord.Cursor
                    .TableCell(tcRows) = DocWord.Cursor.Count + 2
                    .TableCell(tcText, lReg.Index + 1, 1) = lReg("NEMOTECNICO").Value
                    .TableCell(tcText, lReg.Index + 1, 2) = FormatNumber(CDbl(lReg("CANTIDAD").Value), 4)
                    .TableCell(tcText, lReg.Index + 1, 3) = FormatNumber(CDbl(NVL(lReg("PRECIO").Value, 0)), 4)
                    .TableCell(tcText, lReg.Index + 1, 4) = Format(lReg("MONTO").Value, lFormato)
                    .TableCell(tcAlign, lReg.Index + 1, 1) = taLeftMiddle
                    .TableCell(tcAlign, lReg.Index + 1, 2, lReg.Index + 1, 4) = taRightMiddle
    
                    nTotalDocumento = nTotalDocumento + lReg("MONTO").Value
                Next
                .TableCell(tcText, DocWord.Cursor.Count + 2, 4) = Format(nTotalDocumento, lFormato)
                .TableCell(tcAlign, DocWord.Cursor.Count + 2, 4) = taRightMiddle
            Else
                .TableCell(tcRows) = 2
            End If
        .EndTable
        
        .FontBold = False
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = "Atentamente,"
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .StartTable
            .TableBorder = tbNone
            .TableCell(tcCols) = 2
            .TableCell(tcRows) = 2
            .TableCell(tcColAlign, 1, 1) = taCenterMiddle
            .TableCell(tcColAlign, 1, 2) = taCenterMiddle
            .TableCell(tcColAlign, 2, 1) = taCenterMiddle
            .TableCell(tcColAlign, 2, 2) = taCenterMiddle
            .TableCell(tcFontSize, 1, 1, 1, 1) = 10
            .TableCell(tcFontBold, 1, 1, 2, 2) = True
            .TableCell(tcColWidth, 1, 1) = "80mm"
            .TableCell(tcColWidth, 1, 2) = "80mm"
            .TableCell(tcText, 1, 1) = "_______________________________________"
            .TableCell(tcText, 1, 2) = "_______________________________________"
            .TableCell(tcText, 2, 1) = "p." & GetCell(Grilla, lLinea, "NOMBRE_CLIENTE")
            .TableCell(tcText, 2, 2) = "p." & gDsc_Empresa
        .EndTable

        .TextAlign = taLeftTop
        .FontBold = False
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = ""
        .Paragraph = "FECHA CONTRATO" & vbTab & vbTab & ":   " & GetCell(Grilla, lLinea, "fechaoperativa")
        .Paragraph = "ESTADO" & vbTab & vbTab & vbTab & ":   " & IIf(Trim(GetCell(Grilla, lLinea, "estadocontrato")) = "H", "VIGENTE", "NO VIGENTE")
        .Paragraph = "OBSERVACION CLIENTE" & vbTab & ":   " & GetCell(Grilla, lLinea, "observacion")

        .EndDoc
    End With
    
    Set DocWord = Nothing
End Sub


Private Function DameNombreEjecutivo(lCodEjecutivo, lCodSucursal)
    DameNombreEjecutivo = "Falta la funcion"
End Function

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "PRINT"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

Private Sub Sub_CargaForm()
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_FormControl_Color(Me.Controls)
  
    Grilla.Rows = 1
    
    ' Este carga todos los clientes,
    ' Call Sub_CargaCombo_Clientes(Cmb_Clientes, pTodos:=False)
    
    
    Call Sub_CargaCombo_Clientes
    
    Call Sub_ComboSelectedItem(Cmb_Clientes, cCmbKALL)
    
    DTP_Fecha_Ter.Value = Fnt_FechaServidor
    DTP_Fecha_Ini.Value = DTP_Fecha_Ter.Value - 30
    
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargaCombo_Clientes()
    Dim lReg As hFields
    Dim oComprobantes As New Class_Comprobante_Aporte_Rescate
    Dim pTag As String
    Dim pBlanco As Boolean
    Dim lTexto As String
    Dim lNomCliente As String
    Dim pCampoPK As String
    Dim lValueItem As ValueItem
    
    pTag = ""
    pBlanco = True
    
    pCampoPK = "id_Cliente"
         
    With Cmb_Clientes
        .Text = ""
        .ClearFields
        .Clear
        .EmptyRows = True
        '.SelectedItem = Null
        Call .Columns.Remove(1)
        With .Columns(0).ValueItems
          .Clear
          .Translate = False
    '      .Translate = True
        End With
    
        If pBlanco Then
            ' Call .ComboItems.Add(Key:=cCmbKBLANCO, Text:="")
        End If
    
        If oComprobantes.Buscar_ClientesConComprobantes() Then
            For Each lReg In oComprobantes.Cursor
                lTexto = lReg("rut_cliente").Value
                lNomCliente = lReg("nombre_cliente").Value
                
                If Not gRelogDB Is Nothing Then
                    gRelogDB.AvanzaRelog
                End If
                
                Set lValueItem = New ValueItem
                With lValueItem
                    .Value = lReg("id_Cliente").Value
                    .DisplayValue = lTexto & " - " & lNomCliente
                End With
                
                 With .Columns(0).ValueItems
                    .Add lValueItem
                    .Translate = True
                End With
                
                Set lValueItem = Nothing
                ' Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(pCampoPK).Value, lTexto))
                ' Call .AddItem(lReg(pCampoPK).Value)
                 Call .AddItem(lTexto & " - " & lNomCliente)
            Next
              
        End If
        
    End With
    
    Set oComprobantes = Nothing
End Sub

Private Sub Toolbar_Proceso_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "BUSC"
      Call Sub_BuscarDatosGrilla
  End Select
End Sub
Private Sub Sub_BuscarDatosGrilla()
    Dim oComprobante As New Class_Comprobante_Aporte_Rescate
    '----------------------------------------------------------------------------
    Dim lReg        As hFields
    '----------------------------------------------------------------------------
    Dim lId_Cliente  As String
    Dim ldFechaIni  As String
    Dim ldFechaFin  As String
    '----------------------------------------------------------------------------
    Dim lLinea      As Long
    Dim lFormato    As String
    Dim lSCodEstado As String
    '----------------------------------------------------------------------------
    Dim lhTotales As hRecord
    Dim lfTotal As hFields

    On Error GoTo ErrProcedure

    Call Sub_Bloquea_Puntero(Me)

    If Not Fnt_Form_Validar(Me.Controls) Then
        Exit Sub
    End If

    If (Me.DTP_Fecha_Ini > Me.DTP_Fecha_Ter) Then
      MsgBox ("La Fecha Inicio es mayor a la Fecha Termino")
      Exit Sub
    End If
    
    lId_Cliente = Fnt_ComboSelected_KEY(Cmb_Clientes)
    ldFechaIni = DTP_Fecha_Ini.Value
    ldFechaFin = DTP_Fecha_Ter.Value
  
    Grilla.Rows = 1
  
    With oComprobante
        If Not .Buscar_ComprobantesDelClientePorFecha(lId_Cliente, ldFechaIni, ldFechaFin) Then
            Call Fnt_MsgError(.SubTipo_LOG, "Problemas al traer los Comprobantes.", .ErrMsg, pConLog:=False)
            GoTo ExitProcedure
        End If
        
        For Each lReg In .Cursor
            lLinea = Grilla.Rows
            Grilla.AddItem ""
            
            Grilla.Cell(flexcpChecked, lLinea, Grilla.ColIndex("chk")) = flexChecked
            
            Call SetCell(Grilla, lLinea, "ID_COMPROBANTE", lReg("ID_COMPROBANTE").Value, False)
            Call SetCell(Grilla, lLinea, "ID_CLIENTE", lReg("ID_CLIENTE").Value, False)
            Call SetCell(Grilla, lLinea, "NUMERO_FOLIO", lReg("NUMERO_FOLIO").Value, False)
            Call SetCell(Grilla, lLinea, "FECHA", lReg("FECHA").Value, False)
            Call SetCell(Grilla, lLinea, "TIPO_MOVIMIENTO", lReg("TIPO_MOVIMIENTO").Value, False)
            Call SetCell(Grilla, lLinea, "ID_APO_RES_CUENTA", NVL(lReg("ID_APO_RES_CUENTA").Value, ""), False)
            Call SetCell(Grilla, lLinea, "ID_OPERACION", NVL(lReg("ID_OPERACION").Value, ""), False)
            
            If lReg("COD_ESTADO").Value = "A" Then
                Call SetCell(Grilla, lLinea, "COD_ESTADO", "ANULADO", False)
            Else
                Call SetCell(Grilla, lLinea, "COD_ESTADO", "VIGENTE", False)
            End If
            
            Call SetCell(Grilla, lLinea, "FLG_FIRMADO", lReg("FLG_FIRMADO").Value, False)
            Call SetCell(Grilla, lLinea, "ID_CUENTA", lReg("ID_CUENTA").Value, False)
            Call SetCell(Grilla, lLinea, "ABR_CUENTA", lReg("ABR_CUENTA").Value, False)
            Call SetCell(Grilla, lLinea, "NUM_CUENTA", lReg("NUM_CUENTA").Value, False)
            Call SetCell(Grilla, lLinea, "DSC_CUENTA", lReg("DSC_CUENTA").Value, False)
            Call SetCell(Grilla, lLinea, "COD_SUCURSAL", NVL(lReg("COD_SUCURSAL").Value, ""), False)
            Call SetCell(Grilla, lLinea, "COD_EJECUTIVO", NVL(lReg("COD_EJECUTIVO").Value, ""), False)
            Call SetCell(Grilla, lLinea, "RUT_CLIENTE", NVL(lReg("RUT_CLIENTE").Value, ""), False)
            Call SetCell(Grilla, lLinea, "RAZON_SOCIAL", NVL(lReg("RAZON_SOCIAL").Value, ""), False)
            Call SetCell(Grilla, lLinea, "NOMBRE_CLIENTE", NVL(lReg("NOMBRE_CLIENTE").Value, ""), False)
            Call SetCell(Grilla, lLinea, "ID_MONEDA", NVL(lReg("ID_MONEDA").Value, ""), False)
            Call SetCell(Grilla, lLinea, "NOMBRE_ASESOR", NVL(lReg("NOMBRE_ASESOR").Value, ""), False)
            
            Call SetCell(Grilla, lLinea, "EstadoContrato", NVL(lReg("EstadoContrato").Value, ""), False)
            Call SetCell(Grilla, lLinea, "observacion", NVL(lReg("observacion").Value, ""), False)
            Call SetCell(Grilla, lLinea, "fechaoperativa", NVL(lReg("FECHA_OPERATIVA").Value, ""), False)
            Call SetCell(Grilla, lLinea, "replegal", NVL(lReg("Representante").Value, ""), False)
            Call SetCell(Grilla, lLinea, "direccion", NVL(lReg("Direccion").Value, ""), False)
            Call SetCell(Grilla, lLinea, "tipo_entidad", NVL(lReg("tipo_entidad").Value, ""), False)
        Next
    End With
    Grilla.SetFocus
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al traer los comprobantes.", Err.Description, pConLog:=False)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
    Set oComprobante = Nothing
    Call Sub_Desbloquea_Puntero(Me)
End Sub

  
Private Sub Toolbar_Chequeo_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim lLinea As Long
    Dim lCol As Long
    
    Me.SetFocus
    DoEvents
  
    If Button.Key = "INV_SEL" Then
        lCol = Grilla.ColIndex("CHK")
    
        For lLinea = 1 To Grilla.Rows - 1
            If Grilla.Cell(flexcpChecked, lLinea, lCol) = flexChecked Then
                Grilla.Cell(flexcpChecked, lLinea, lCol) = flexUnchecked
            Else
                Grilla.Cell(flexcpChecked, lLinea, lCol) = flexChecked
            End If
        Next
    Else
        Call Sub_CambiaCheck(Grilla, (Button.Key = "SEL_ALL"))
    End If
    
End Sub

