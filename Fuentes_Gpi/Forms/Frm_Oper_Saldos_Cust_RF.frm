VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Oper_Saldos_Cust_RF 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Operaci�n de Saldos de Custodia RF"
   ClientHeight    =   4740
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   9345
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4740
   ScaleWidth      =   9345
   Begin VB.Frame Frame2 
      Height          =   915
      Left            =   90
      TabIndex        =   3
      Top             =   420
      Width           =   9165
      Begin VB.CommandButton Cmb_BuscaFile 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   6570
         TabIndex        =   4
         ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
         Top             =   300
         Width           =   375
      End
      Begin hControl2.hTextLabel Txt_ArchivoPlano 
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Tag             =   "OBLI"
         Top             =   300
         Width           =   6360
         _ExtentX        =   11218
         _ExtentY        =   556
         LabelWidth      =   1200
         TextMinWidth    =   1200
         Caption         =   "Archivo Plano"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
   End
   Begin VB.Frame Frnm_Grilla_Sucesos 
      Caption         =   "Visor de Sucesos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3255
      Left            =   90
      TabIndex        =   0
      Top             =   1410
      Width           =   9165
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   2865
         Left            =   120
         TabIndex        =   1
         Top             =   270
         Width           =   7965
         _cx             =   14049
         _cy             =   5054
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Oper_Saldos_Cust_RF.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Detalle 
         Height          =   330
         Left            =   8160
         TabIndex        =   2
         Top             =   540
         Width           =   900
         _ExtentX        =   1588
         _ExtentY        =   582
         ButtonWidth     =   1588
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Detalle"
               Key             =   "DETALLE"
               Object.ToolTipText     =   "Muestra el detalle de los sucesos del Cierre"
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   9345
      _ExtentX        =   16484
      _ExtentY        =   635
      ButtonWidth     =   1588
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   7
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Oper_Saldos_Cust_RF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const fc_Mercado = "N"

Public fKey As String

Dim fSaldos_Cust_RF As Object

Public Function Fnt_Mostrar(pCod_Proceso_Componente As String)
  Fnt_Mostrar = False

  fKey = pCod_Proceso_Componente
  
  If Not Fnt_CargarDatos Then
    Unload Me
    Exit Function
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
  
  Fnt_Mostrar = True
End Function

Private Function Fnt_CargarDatos() As Boolean
Dim lcProceso_Componente  As Class_Proceso_Componente
Dim lMensaje              As String

On Error GoTo ErrProcedure

  Call Sub_Bloquea_Puntero(Me)
  
  Load Me
  Fnt_CargarDatos = False
  
  Set fSaldos_Cust_RF = Nothing
  
  'Busca el componente que corresponde al conciliador
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = fKey
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set fSaldos_Cust_RF = .IniciaClass(lMensaje)
    
    If fSaldos_Cust_RF Is Nothing Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                      , lMensaje _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  Set lcProceso_Componente = Nothing
  
  Fnt_CargarDatos = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la carga del Proceso Componente (" & fKey & ")." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Function

Private Sub Cmb_BuscaFile_Click()
  
  If fSaldos_Cust_RF.Fnt_Busca_Archivo_Plano Then
    Txt_ArchivoPlano.Text = fSaldos_Cust_RF.fArchivo
  End If
  
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = "boton_grabar"
      .Buttons("EXIT").Image = "boton_salir"
  End With

  Txt_ArchivoPlano.Text = ""
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
Dim lMensaje As String

  With Grilla
    If .Row > 0 Then
      lMensaje = GetCell(Grilla, .Row, "TEXTO")
      If Not lMensaje = "" Then
        MsgBox lMensaje, vbInformation, Me.Caption
      End If
    End If
  End With
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        MsgBox "Saldos de Custodia de Renta Fija guardados correctamente.", vbInformation, Me.Caption
      End If
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lCursor As hRecord
Dim lReg As hFields
Dim lFila_Detalle As Long
Dim lSeguir As Boolean
Dim lId_Nemotecnico
Dim lFecha As Date
Dim lRut_Cliente As String
Dim lCuenta As String
Dim lNemotecnico As String
Dim lSaldo As Double
Dim lTasa_Compra As Double
Dim lVal_Tasa_Compra As Double
Dim lTasa_Mercado As Double
Dim lVal_Tasa_Mercado As Double
Dim lId_Cuenta
Dim lId_Moneda As String
Dim lId_Caja_Cuenta As String
Dim lCod_Instrumento As String
'----------------------------
Dim lcAlias As Object
Dim lcNemotecnico As Class_Nemotecnicos
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lcBonos As Class_Bonos
Dim lcDeposito As Class_Depositos

  Fnt_Grabar = True
  Set lCursor = Nothing
  Grilla.Rows = 1
  
  With fSaldos_Cust_RF
    If Not .Fnt_Cargar_Saldos_RF Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
    
    If .Cursor.Count = 0 Then
      MsgBox "No hay datos en el Archivo Seleccionado.", vbInformation, Me.Caption
      GoTo ErrProcedure
    End If
    
    Set lCursor = .Cursor
  End With
    
  '---------------------------------
  Rem Moneda Peso = 1
  lId_Moneda = 1
  '---------------------------------
  
  lFila_Detalle = 1
  Call Fnt_Escribe_Grilla(Grilla, "N", "Inicio Proceso Carga Saldos de Custodia de Renta Fija: " & Now & ".")
  For Each lReg In lCursor
    lSeguir = True
    lFecha = lReg("FECHA").Value
    lRut_Cliente = lReg("RUT_CLIENTE").Value
    lCuenta = lReg("CUENTA").Value
    lNemotecnico = lReg("NEMOTECNICO").Value
    lSaldo = lReg("SALDO").Value
    lTasa_Compra = lReg("TASA_COMPRA").Value
    lVal_Tasa_Compra = lReg("VAL_TASA_COMPRA").Value
    lTasa_Mercado = lReg("TASA_MERCADO").Value
    lVal_Tasa_Mercado = lReg("VAL_TASA_MERCADO").Value
    '------------------------------------------------------------------------------------------------------------------------------
    Rem Busca el Alias de la Cuenta por el Rut Cliente
    Set lcAlias = CreateObject(cDLL_Alias)
    Set lcAlias.gDB = gDB
    lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                         , pCodigoCSBPI:=cTabla_Cuentas _
                                         , pValor:=lRut_Cliente)
    Set lcAlias = Nothing
    If IsNull(lId_Cuenta) Then
      Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": El Rut Cliente " & lRut_Cliente & " no tiene asociado un Alias en una Cuenta de GPI.")
      lSeguir = False
    End If
    '------------------------------------------------------------------------------------------------------------------------------
    If lSeguir Then
      lId_Nemotecnico = ""
      lCod_Instrumento = ""
      Rem Busca el id_nemotecnico del Nemotecnico
      Set lcNemotecnico = New Class_Nemotecnicos
      With lcNemotecnico
        .Campo("nemotecnico").Valor = lNemotecnico
        If .Buscar_Nemotecnico(pCod_Producto:="", pMostrar_Msg:=False) Then
          lId_Nemotecnico = .Campo("id_nemotecnico").Valor
          If lId_Nemotecnico = "0" Then
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": Nemot�cnico " & lNemotecnico & " no existe o est� vencido.")
            lSeguir = False
          Else
            If .Buscar Then
              If .Cursor.Count > 0 Then
                lCod_Instrumento = .Cursor(1)("cod_instrumento").Value
              End If
            Else
              Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": " & .ErrMsg)
              lSeguir = False
            End If
          End If
        Else
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": " & .ErrMsg)
          lSeguir = False
        End If
      End With
      Set lcNemotecnico = Nothing
    End If
    '------------------------------------------------------------------------------------------------------------------------------
    lId_Caja_Cuenta = ""
    Rem Busca la caja para la cuenta
    If lSeguir Then
      Set lcCaja_Cuenta = New Class_Cajas_Cuenta
      With lcCaja_Cuenta
        .Campo("id_cuenta").Valor = lId_Cuenta
        .Campo("id_moneda").Valor = lId_Moneda
        .Campo("cod_mercado").Valor = fc_Mercado
        If .Buscar_Caja_Para_Invertir(lId_Nemotecnico) Then
          If .Cursor.Count > 0 Then
            lId_Caja_Cuenta = NVL(.Cursor(1)("id_caja_cuenta").Value, "")
          End If
        Else
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": " & lcCaja_Cuenta.ErrMsg)
          lSeguir = False
        End If
      End With
      Set lcCaja_Cuenta = Nothing
      
      If lId_Caja_Cuenta = "" Then
        Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": No existe una caja para la cuenta del Rut Cliente " & lRut_Cliente & ".")
        lSeguir = False
      End If
    End If
    '------------------------------------------------------------------------------------------------------------------------------
    If lSeguir Then
      Select Case lCod_Instrumento
        Case gcINST_BONOS_NAC
          Set lcBonos = New Class_Bonos
          With lcBonos
            Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lId_Nemotecnico, _
                                              pCantidad:=lSaldo, _
                                              pTasa:=lTasa_Compra, _
                                              PTasa_Gestion:=lTasa_Compra, _
                                              pId_Moneda:=lId_Moneda, _
                                              pMonto:=lVal_Tasa_Compra, _
                                              pUtilidad:=0, _
                                              pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                              pTasa_Historico:="", _
                                              pId_Mov_Activo_Compra:="")
  
            If Not .Realiza_Operacion_Custodia(pId_Operacion:=cNewEntidad, _
                                               pId_Cuenta:=lId_Cuenta, _
                                               pDsc_Operacion:="", _
                                               pTipoOperacion:=gcTipoOperacion_Ingreso, _
                                               pId_Contraparte:="", _
                                               pId_Representante:="", _
                                               pId_Moneda_Operacion:=lId_Moneda, _
                                               pFecha_Operacion:=lFecha, _
                                               pFecha_Liquidacion:=lFecha, _
                                               pId_Trader:="", _
                                               pPorc_Comision:=0, _
                                               pComision:=0, _
                                               pDerechos_Bolsa:=0, _
                                               pGastos:=0, _
                                               pIva:=0, _
                                               pMonto_Operacion:=lVal_Tasa_Compra, _
                                               pTipo_Precio:=cTipo_Precio_Mercado, _
                                               pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
              Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": " & .ErrMsg)
              GoTo ErrProcedure
              Fnt_Grabar = False
            Else
              Call Fnt_Escribe_Grilla(Grilla, "N", "Fila " & lFila_Detalle & ": Ingreso correcto de la Operaci�n de Custodia.")
            End If
          End With
          
      Case gcINST_DEPOSITOS_NAC
        Set lcDeposito = New Class_Depositos
        With lcDeposito
          Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lId_Nemotecnico, _
                                            pCantidad:=lSaldo, _
                                            pTasa:=lTasa_Compra, _
                                            PTasa_Gestion:=lTasa_Compra, _
                                            pPlazo:="", _
                                            pBase:="", _
                                            pFecha_Vencimiento:="", _
                                            pId_Moneda_Pago:=lId_Moneda, _
                                            pMonto_Pago:=lVal_Tasa_Compra, _
                                            pReferenciado:="", _
                                            pTipo_Deposito:="", _
                                            pFecha_Valuta:="", _
                                            pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                            pTasa_Historico:="", _
                                            pId_Mov_Activo_Compra:="")
                                                      
          If Not .Realiza_Operacion_Custodia(pId_Operacion:=cNewEntidad, _
                                             pId_Cuenta:=lId_Cuenta, _
                                             pDsc_Operacion:="", _
                                             pTipoOperacion:=gcTipoOperacion_Ingreso, _
                                             pId_Contraparte:="", _
                                             pId_Representante:="", _
                                             pId_Moneda_Operacion:=lId_Moneda, _
                                             pFecha_Operacion:=lFecha, _
                                             pFecha_Liquidacion:=lFecha, _
                                             pId_Trader:="", _
                                             pPorc_Comision:=0, _
                                             pComision:=0, _
                                             pDerechos_Bolsa:=0, _
                                             pGastos:=0, _
                                             pIva:=0, _
                                             pMonto_Operacion:=lVal_Tasa_Compra, _
                                             pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": " & .ErrMsg)
            GoTo ErrProcedure
            Fnt_Grabar = False
          End If
        
        End With
      End Select
    End If
    
    lFila_Detalle = lFila_Detalle + 1
  Next
  Call Fnt_Escribe_Grilla(Grilla, "N", "T�rmino Proceso Carga Saldos de Custodia de Renta Fija: " & Now & ".")
  
ErrProcedure:
  Set lcAlias = Nothing
  Set lcNemotecnico = Nothing
  Set lcCaja_Cuenta = Nothing
  Set lcBonos = Nothing
  Set lcDeposito = Nothing
End Function

Private Sub Toolbar_Detalle_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "DETALLE"
      Call Grilla_DblClick
  End Select
End Sub
