VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Man_CortesRFija 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Cortes Renta Fija"
   ClientHeight    =   6420
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11100
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6420
   ScaleWidth      =   11100
   Begin VB.Frame Frame2 
      Caption         =   "Cuenta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1575
      Left            =   120
      TabIndex        =   10
      Top             =   360
      Width           =   10935
      Begin VB.Frame Frame3 
         Height          =   1215
         Left            =   5040
         TabIndex        =   15
         Top             =   240
         Width           =   5775
         Begin hControl2.hTextLabel Txt_Rut 
            Height          =   315
            Left            =   120
            TabIndex        =   16
            Top             =   120
            Width           =   2655
            _ExtentX        =   4683
            _ExtentY        =   556
            LabelWidth      =   795
            TextMinWidth    =   1200
            Caption         =   "RUT"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Nombres 
            Height          =   315
            Left            =   120
            TabIndex        =   17
            Top             =   480
            Width           =   5520
            _ExtentX        =   9737
            _ExtentY        =   556
            LabelWidth      =   795
            Caption         =   "Nombres"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_NombreAsesor 
            Height          =   315
            Left            =   120
            TabIndex        =   18
            Top             =   840
            Width           =   5520
            _ExtentX        =   9737
            _ExtentY        =   556
            LabelWidth      =   795
            Caption         =   "Asesor"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
      End
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4560
         Picture         =   "Frm_Man_CortesRFija.frx":0000
         TabIndex        =   11
         Top             =   360
         Width           =   375
      End
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   345
         Left            =   1170
         TabIndex        =   0
         Tag             =   "OBLI=S;CAPTION=Cuentas"
         Top             =   390
         Width           =   3285
         _ExtentX        =   5794
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Man_CortesRFija.frx":030A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Nemotecnico 
         Height          =   345
         Left            =   1170
         TabIndex        =   1
         Tag             =   "OBLI=S;CAPTION=Cuentas"
         Top             =   840
         Width           =   3765
         _ExtentX        =   6641
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Man_CortesRFija.frx":03B4
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComctlLib.Toolbar Toolbar_Proceso 
         Height          =   330
         Left            =   3840
         TabIndex        =   2
         Top             =   1200
         Width           =   1020
         _ExtentX        =   1799
         _ExtentY        =   582
         ButtonWidth     =   1561
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar"
               Key             =   "BUSC"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
            EndProperty
         EndProperty
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Instrumento"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   13
         Top             =   870
         Width           =   1035
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   12
         Top             =   390
         Width           =   1035
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Corte Definido"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4335
      Left            =   120
      TabIndex        =   8
      Top             =   2040
      Width           =   5535
      Begin VSFlex8LCtl.VSFlexGrid Grilla_CorteActual 
         Height          =   3045
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   5340
         _cx             =   9419
         _cy             =   5371
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   6
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_CortesRFija.frx":045E
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin hControl2.hTextLabel Txt_CorteOriginal 
         Height          =   345
         Left            =   2880
         TabIndex        =   21
         Top             =   3600
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   609
         LabelWidth      =   1000
         TextMinWidth    =   1000
         Caption         =   "Total Corte"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0"
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_CorteVendido 
         Height          =   345
         Left            =   120
         TabIndex        =   22
         Top             =   3840
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   609
         LabelWidth      =   1000
         TextMinWidth    =   1000
         Caption         =   "Vendidos"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0"
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_CorteDisponible 
         Height          =   345
         Left            =   120
         TabIndex        =   23
         Top             =   3480
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   609
         LabelWidth      =   1000
         TextMinWidth    =   1000
         Caption         =   "Disponibles"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0"
         Alignment       =   1
      End
   End
   Begin VB.Frame Pnl_NuevoCorte 
      Caption         =   "Nuevo Corte"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4335
      Left            =   5760
      TabIndex        =   6
      Top             =   2040
      Width           =   5295
      Begin hControl2.hTextLabel Txt_Nominales 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Tag             =   "OBLI"
         Top             =   960
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1000
         Caption         =   "Nominales"
         Text            =   "0,0000"
         Text            =   "0,0000"
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.0000"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_NumeroCorte 
         Height          =   315
         Left            =   120
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   600
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1000
         Caption         =   "# Corte"
         Text            =   "0"
         Text            =   "0"
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_NominalesIngresados 
         Height          =   345
         Left            =   2640
         TabIndex        =   7
         Top             =   3480
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   609
         LabelWidth      =   1000
         TextMinWidth    =   1000
         Caption         =   "Ingresados"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0"
         Alignment       =   1
      End
      Begin MSComctlLib.Toolbar Toolbar_Grilla 
         Height          =   330
         Left            =   840
         TabIndex        =   5
         Top             =   1800
         Width           =   1290
         _ExtentX        =   2275
         _ExtentY        =   582
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "ADD"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Agregar un nemotecnico a la Operaci�n"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "DEL"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
            EndProperty
         EndProperty
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_CorteNuevo 
         Height          =   3195
         Left            =   2640
         TabIndex        =   14
         Top             =   240
         Width           =   2460
         _cx             =   4339
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   2
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_CortesRFija.frx":05AA
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   11100
      _ExtentX        =   19579
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   6000
         TabIndex        =   20
         Top             =   45
         Width           =   1380
         _ExtentX        =   2434
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_CortesRFija"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fFlg_Tipo_Permiso   As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema  As String 'Codigo para el permiso sobre la ventana
Dim fCod_Instrumento    As String
Dim fFilaModificada     As Long
Dim fId_Mov_Activo               As String

Private Enum eNem_Colum
  eNem_nemotecnico
  eNem_Descripcion
  eNem_Id_Nemotecnico
  eNem_Id_Mov_Activo
  eNem_Tasa_Emision
End Enum

Public Sub Mostrar(pCod_Arbol_Sistema)
  
    fCod_Arbol_Sistema = pCod_Arbol_Sistema
    fFlg_Tipo_Permiso = Fnt_Obtiene_Permiso_Formulario(pCod_Arbol_Sistema)
    If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
        Pnl_NuevoCorte.Enabled = False
    End If
  
  Call Form_Resize
    fCod_Instrumento = "DEPOSITO_NAC"

  Load Me
End Sub


Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub
Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  With Toolbar_Proceso
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("BUSC").Image = "boton_grilla_buscar"
        .Appearance = ccFlat
  End With
  
  With Toolbar_Grilla
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Appearance = ccFlat
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub



Private Sub Grilla_CorteNuevo_DblClick()
Dim lfila As Long

    lfila = Grilla_CorteNuevo.Row

    Txt_NumeroCorte.Text = GetCell(Grilla_CorteNuevo, lfila, "numero_corte")
    Txt_Nominales.Text = GetCell(Grilla_CorteNuevo, lfila, "nominales")
    Grilla_CorteNuevo.Cell(flexcpFontBold, lfila, 0, lfila, 1) = True
    fFilaModificada = lfila
End Sub

Private Sub Toolbar_Grilla_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
            Call Sub_AgregaCorte
    Case "DEL"
        Call Sub_EliminaCorte
  End Select
End Sub

Private Sub Txt_Nominales_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        SendKeys "{TAB}"
    End If
End Sub

Private Sub Txt_NumeroCorte_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        SendKeys "{TAB}"
    End If
End Sub

Private Sub Grilla_CorteActual_DblClick()
Dim lfila As Long

    lfila = Grilla_CorteActual.Row
    If GetCell(Grilla_CorteActual, lfila, "fecha_venta") <> "" Then
        MsgBox "Corte vendido, no puede ser modificado.", vbInformation, Me.Caption
    Else
        Txt_NumeroCorte.Text = GetCell(Grilla_CorteActual, lfila, "numero_corte")
        Txt_Nominales.Text = GetCell(Grilla_CorteActual, lfila, "nominales")
        Grilla_CorteActual.Cell(flexcpFontBold, lfila, 1, lfila, 5) = True
    End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
    
  Select Case Button.Key
    Case "SAVE"
        If Fnt_Grabar Then
            'Call Sub_Limpiar
            Call Sub_CargaCortes(fId_Mov_Activo)
        End If
    Case "EXIT"
        Unload Me
    Case "REFRESH"
        If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
                , vbYesNo + vbQuestion _
                , Me.Caption) = vbYes Then
              Call Sub_Limpiar
        End If
  End Select
End Sub
Private Sub Toolbar_Proceso_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim lCuenta As String
    lCuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
    If Not lCuenta = "" Then
    Else
       MsgBox "Primero debe elegir una cuenta.", vbInformation + vbOKOnly, Me.Caption
       Exit Sub
    End If
    Me.SetFocus
    DoEvents
    Select Case Button.Key
        Case "BUSC"
            Call Cmb_Nemotecnico_LostFocus
    End Select
End Sub
Private Sub Sub_Limpiar()
    Grilla_CorteActual.Rows = 1
    Grilla_CorteNuevo.Rows = 1
    Txt_Nominales.Text = 0
    Txt_NumeroCorte.Text = 0
    Txt_CorteDisponible.Text = 0
    Txt_CorteOriginal.Text = 0
    Txt_CorteVendido.Text = 0
    Txt_NominalesIngresados.Text = 0
    fFilaModificada = 0
    fId_Mov_Activo = 0
End Sub

Private Sub Sub_CargaForm()
  
    Call Sub_Bloquea_Puntero(Me)
    
    fKey = cNewEntidad
    
    
    Call Sub_FormControl_Color(Me.Controls)
    Call Sub_CargaCombo_Cuentas(Cmb_Cuentas)
    'Call Sub_CargaCombo_Nemotecnicos_Cuenta
     
    Call Sub_Desbloquea_Puntero(Me)
    

End Sub


Private Sub Cmb_Cuentas_ItemChange()
Dim lId_Cuenta          As Integer
Dim lFechaCierreVirtual As Date
Dim lFechaCierre        As Date

    lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
    If lId_Cuenta <> 0 Then
        Call Cmb_Cuentas_LostFocus
        Call Sub_CargaCombo_Nemotecnicos_Cuenta(lId_Cuenta)
    End If

End Sub
Private Sub Cmb_Cuentas_LostFocus()
    Call Sub_CargarDatos
End Sub

Private Sub Cmb_Buscar_Click()
Dim lId_Cuenta As String
Dim lFechaCierreVirtual As Date
Dim lFechaCierre        As Date

    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)
    If lId_Cuenta <> 0 Then
        Call Sub_ComboSelectedItem(Cmb_Cuentas, lId_Cuenta)
    End If
End Sub

Private Sub Sub_CargaCombo_Nemotecnicos_Cuenta(pId_Cuenta As Integer)
Dim lSaldos_Activos As Class_Saldo_Activos
Dim lReg            As hFields
    Call Sub_LimpiarTDBCombo(Cmb_Nemotecnico)
    With Cmb_Nemotecnico
        With .Columns.Add(eNem_nemotecnico)
            .Caption = "Nemot�cnico"
            .Visible = True
        End With
        With .Columns.Add(eNem_Descripcion)
            .Caption = "Descripci�n"
            .Visible = True
        End With
        With .Columns.Add(eNem_Id_Nemotecnico)
            .Caption = "id_nemotecnico"
            .Visible = False
        End With
        With .Columns.Add(eNem_Id_Mov_Activo)
            .Caption = "id_mov_Activo"
            .Visible = False
        End With
        With .Columns.Add(eNem_Tasa_Emision)
            .Caption = "tasa_emision"
            .Visible = False
        End With
        Set lSaldos_Activos = New Class_Saldo_Activos
        If lSaldos_Activos.Buscar_Ultimo_CierreCuenta(pId_Cuenta:=pId_Cuenta, pCod_Instrumento:=fCod_Instrumento) Then
            For Each lReg In lSaldos_Activos.Cursor
                Call .AddItem(lReg("nemotecnico").Value & ";" & Trim(lReg("dsc_nemotecnico").Value) & ";" & lReg("id_nemotecnico").Value & ";" & lReg("id_mov_activo").Value & ";" & lReg("tasa_emision").Value)
                .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("id_nemotecnico").Value, lReg("nemotecnico").Value)
            Next
        Else
            Call Fnt_MsgError(lSaldos_Activos.SubTipo_LOG, _
                        "Problemas en carga de de Saldos Activos.", _
                        lSaldos_Activos.ErrMsg, _
                        pConLog:=True)
        End If
    End With

End Sub



Private Sub Cmb_Nemotecnico_LostFocus()
Dim lId_Operacion_Detalle_Compra As String
Dim lId_Nemotecnico             As String

  lId_Nemotecnico = Fnt_FindValue4Display(Cmb_Nemotecnico, Cmb_Nemotecnico.Text)  ' , eNem_Id_Nemotecnico)

  If Not lId_Nemotecnico = "" Then
    fId_Mov_Activo = Cmb_Nemotecnico.Columns(eNem_Id_Mov_Activo).Text
    Call Sub_CargaCortes(fId_Mov_Activo)
  End If

End Sub

Private Sub Sub_CargaCortes(pId_Mov_Activo As String)
Dim lId_Operacion_Detalle_Compra As String
Dim lcCortesRF                   As Class_CortesRF
Dim lReg                         As hFields
Dim lfila                        As Long
Dim lDefinidos                   As Double
Dim lVendidos                    As Double
Dim lTotal                       As Double

    Call Sub_Bloquea_Puntero(Me)
    
    Grilla_CorteActual.Rows = 1
    lDefinidos = 0
    lVendidos = 0
    lTotal = 0
    lId_Operacion_Detalle_Compra = Fnt_ObtieneIDCompraCorte(pId_Mov_Activo)
    Set lcCortesRF = New Class_CortesRF
    With lcCortesRF
        .Campo("id_operacion_detalle_compra").Valor = lId_Operacion_Detalle_Compra
        If .BuscarCorteActual Then
            If .Cursor.Count = 0 Then
                MsgBox "No tiene cortes definidos.", vbInformation, Me.Caption
            Else
                For Each lReg In .Cursor
                    lfila = Grilla_CorteActual.Rows
                    Call Grilla_CorteActual.AddItem("")
                    Call SetCell(Grilla_CorteActual, lfila, "id_corte", lReg("id_corte").Value, False)
                    Call SetCell(Grilla_CorteActual, lfila, "id_operacion_detalle_compra", lReg("id_operacion_detalle_compra").Value, False)
                    Call SetCell(Grilla_CorteActual, lfila, "numero_corte", 1, False)
                    Call SetCell(Grilla_CorteActual, lfila, "nominales", lReg("nominales").Value, False)
                    Call SetCell(Grilla_CorteActual, lfila, "fecha_venta", NVL(lReg("fecha_venta").Value, ""), False)
                    Call SetCell(Grilla_CorteActual, lfila, "id_operacion_detalle_venta", NVL(lReg("id_operacion_detalle_venta").Value, ""), False)
                    lTotal = lTotal + (To_Number(lReg("nominales").Value))
                    If NVL(lReg("fecha_venta").Value, "") = "" Then
                        lDefinidos = lDefinidos + (To_Number(lReg("nominales").Value))
                    Else
                        lVendidos = lVendidos + (To_Number(lReg("nominales").Value))
                    End If
                Next
            End If
        End If
    End With
    Txt_CorteOriginal.Text = FormatNumber(lTotal, 4)
    Txt_CorteDisponible.Text = FormatNumber(lDefinidos, 4)
    Txt_CorteVendido.Text = FormatNumber(lVendidos, 4)
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function Fnt_ObtieneIDCompraCorte(pId_Mov_Activo As String) As String
Dim lcMov_Activos   As Class_Mov_Activos
Dim lID             As String

    Set lcMov_Activos = New Class_Mov_Activos
    With lcMov_Activos
        .Campo("id_mov_activo").Valor = pId_Mov_Activo
        If .Buscar Then
            lID = .Cursor(1)("id_operacion_detalle").Value
        Else
            lID = ""
        End If
    End With
    Fnt_ObtieneIDCompraCorte = lID

End Function


Private Function Fnt_Grabar() As Boolean
Dim lresult     As Boolean
Dim sMensaje    As String
Dim lfila    As Long
Dim lID_Corte   As Long
Dim lID_Venta   As Long
Dim lID_Compra   As Long
Dim lcCortesRF  As Class_CortesRF
Dim lNroCortes   As Long
Dim lCuentaCortes As Long

    Call Sub_Bloquea_Puntero(Me)
    
    If To_Number(Txt_NominalesIngresados.Text) <> To_Number(Txt_CorteDisponible.Text) Then
       MsgBox "Nominales ingresados superan los nominales disponibles.", vbInformation, Me.Caption
        lresult = False
        GoTo ExitProcedure
    End If
    
    gDB.IniciarTransaccion
    
    lresult = True
    sMensaje = "Cortes guardados exitosamente"
    
    Set lcCortesRF = New Class_CortesRF
    With lcCortesRF
        lID_Compra = NVL(GetCell(Grilla_CorteActual, 1, "id_operacion_detalle_compra"), 0)
        .Campo("id_operacion_detalle_compra").Valor = lID_Compra
        If Not .Borrar Then
            sMensaje = "Error en Actualizaci�n de Cortes. " & .ErrMsg
            lresult = False
            GoTo ErrProcedure
        Else
            For lfila = 1 To Grilla_CorteNuevo.Rows - 1
                lNroCortes = To_Number(GetCell(Grilla_CorteNuevo, lfila, "numero_corte"))
                For lCuentaCortes = 1 To lNroCortes
                    .Campo("id_corte").Valor = cNewEntidad
                    .Campo("id_operacion_detalle_compra").Valor = lID_Compra
                    .Campo("nominales").Valor = GetCell(Grilla_CorteNuevo, lfila, "nominales")
                    If Not .Guardar Then
                        sMensaje = "Error en Actualizaci�n de Cortes. " & .ErrMsg
                        lresult = False
                      GoTo ErrProcedure
                    End If
                Next
            Next
            For lfila = 1 To Grilla_CorteActual.Rows - 1
                If NVL(GetCell(Grilla_CorteActual, lfila, "fecha_venta"), "") <> "" Then
                    .Campo("id_corte").Valor = cNewEntidad
                    .Campo("id_operacion_detalle_compra").Valor = lID_Compra
                    .Campo("nominales").Valor = GetCell(Grilla_CorteActual, lfila, "nominales")
                    .Campo("fecha_venta").Valor = GetCell(Grilla_CorteActual, lfila, "fecha_venta")
                    .Campo("id_operacion_detalle_venta").Valor = GetCell(Grilla_CorteActual, lfila, "id_operacion_detalle_venta")
                    If Not .Guardar Then
                        sMensaje = "Error en Actualizaci�n de Cortes. " & .ErrMsg
                        lresult = False
                        GoTo ErrProcedure
                    End If
                End If
            Next
        End If
    End With
    Set lcCortesRF = Nothing
    
ErrProcedure:
    If Not lresult Then
        MsgBox sMensaje, vbInformation, Me.Caption
        gDB.RollbackTransaccion
    Else
        MsgBox sMensaje, vbInformation, Me.Caption
        gDB.CommitTransaccion
    End If
  
    Call Sub_Desbloquea_Puntero(Me)

ExitProcedure:
    Fnt_Grabar = lresult
End Function

Private Sub Sub_CargarDatos()
Dim lcCuenta As Object
'-----------------------------------------------
Dim lReg            As hCollection.hFields
Dim lId_Cuenta      As String
Dim lLinea          As Long
                

    Call Sub_Bloquea_Puntero(Me)
  
    lId_Cuenta = Fnt_FindValue4Display(Cmb_Cuentas, Cmb_Cuentas.Text)
  
    If Not lId_Cuenta = "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("id_cuenta").Valor = lId_Cuenta
            If .Buscar_Vigentes Then
                If .Cursor.Count > 0 Then
                    'Txt_Perfil.Text = .Cursor(1)("dsc_perfil_riesgo").Value
                    Txt_Rut.Text = "" & .Cursor(1)("rut_cliente").Value
                    Txt_Nombres.Text = "" & .Cursor(1)("nombre_cliente").Value
                    Txt_NombreAsesor.Text = "" & .Cursor(1)("nombre_Asesor").Value
                End If
            Else
                MsgBox "Problemas en cargar la Cuenta." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            End If
        End With
    End If
    Grilla_CorteNuevo.Rows = 1
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_AgregaCorte()
Dim lLinea As Long
Dim lNominales As Double
Dim lNroCorte  As Double
Dim lDefinidos As Double
Dim bPrimero   As Boolean
Dim bExiste As Boolean
    
    Call Sub_Bloquea_Puntero(Me)
    lNroCorte = Txt_NumeroCorte.Text
    lNominales = CDbl(Txt_Nominales.Text)
        
    If (To_Number(Txt_Nominales.Text) <> 0 And To_Number(Txt_NumeroCorte.Text) <> 0) Then
        bExiste = Fnt_ExisteCorte(lLinea)
        If bExiste Then
            If fFilaModificada = 0 Or fFilaModificada <> lLinea Then
                lNroCorte = lNroCorte + CInt(GetCell(Grilla_CorteNuevo, lLinea, "numero_corte"))
            Else
                lLinea = fFilaModificada
            End If
        Else
            If Grilla_CorteNuevo.Rows = 0 Then
                Grilla_CorteNuevo.Rows = 1
            End If
            lLinea = Grilla_CorteNuevo.Rows
            Call Grilla_CorteNuevo.AddItem("")
        End If
        Call SetCell(Grilla_CorteNuevo, lLinea, "numero_corte", lNroCorte, pAutoSize:=False)
        Call SetCell(Grilla_CorteNuevo, lLinea, "nominales", lNominales, pAutoSize:=False)
        Txt_NumeroCorte.Text = 0
        Txt_Nominales.Text = 0
    End If
    Txt_NumeroCorte.SetFocus
    If bExiste Then
        If (fFilaModificada <> 0 And fFilaModificada <> lLinea) Then
            Grilla_CorteNuevo.RemoveItem (fFilaModificada)
            'Grilla_Corte.Cell(flexcpFontBold, fFilaCorteModif, 0, fFilaCorteModif, 1) = True
        End If
        fFilaModificada = 0
    End If
    
    Call Sub_CalculaNominalesIngresados
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_EliminaCorte()
Dim lLinea As Long
  
  Call Sub_Bloquea_Puntero(Me)
  lLinea = Grilla_CorteNuevo.Row
  If lLinea > 0 Then
    If MsgBox("�Desea eliminar este Corte?.", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
      Call Grilla_CorteNuevo.RemoveItem(lLinea)
      Call Sub_CalculaNominalesIngresados
    End If
  End If
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CalculaNominalesIngresados()
Dim lLinea As Long
Dim lNominales As Double
Dim lDefinidos As Double
Dim lNroCorte As Double

    lDefinidos = 0
    If Grilla_CorteNuevo.Rows > 1 Then
        For lLinea = 1 To Grilla_CorteNuevo.Rows - 1
            lNominales = NVL(GetCell(Grilla_CorteNuevo, lLinea, "nominales"), 0)
            lNroCorte = NVL(GetCell(Grilla_CorteNuevo, lLinea, "numero_corte"), 1)
            If lNominales = 0 Then
                Exit For
            End If
            lDefinidos = lDefinidos + (lNominales * lNroCorte)
        Next
    End If
    Txt_NominalesIngresados.Text = FormatNumber(lDefinidos, 4)
    If To_Number(Txt_NominalesIngresados.Text) > To_Number(Txt_CorteDisponible.Text) Then
        MsgBox "Nominales ingresados superan los nominales disponibles.", vbInformation, Me.Caption
    End If
End Sub

Private Function Fnt_ExisteCorte(ByRef pLinea) As Boolean
Dim lfila As Long
Dim lresult As Boolean

    pLinea = 0
    lresult = False
    For lfila = 1 To Grilla_CorteNuevo.Rows - 1
        If GetCell(Grilla_CorteNuevo, lfila, "nominales") = Txt_Nominales.Text Then
            pLinea = lfila
            lresult = True
            Exit For
        End If
    Next
    Fnt_ExisteCorte = lresult
End Function
