VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Instrucciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte de Instrucciones"
   ClientHeight    =   1800
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   8190
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1800
   ScaleWidth      =   8190
   Begin VB.Frame Frame1 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1275
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   8055
      Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
         Height          =   345
         Left            =   4110
         TabIndex        =   8
         Top             =   270
         Width           =   1605
         _ExtentX        =   2831
         _ExtentY        =   609
         _Version        =   393216
         Format          =   15990785
         CurrentDate     =   38938
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ini 
         Height          =   345
         Left            =   1170
         TabIndex        =   7
         Top             =   270
         Width           =   1605
         _ExtentX        =   2831
         _ExtentY        =   609
         _Version        =   393216
         Format          =   15990785
         CurrentDate     =   38938
      End
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   5850
         TabIndex        =   1
         Top             =   270
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2858
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Reporte"
               Key             =   "REPORTE"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               Object.ToolTipText     =   "Genera el Reporte de Instruccones"
            EndProperty
         EndProperty
      End
      Begin TrueDBList80.TDBCombo Cmb_Estado 
         Height          =   345
         Left            =   1170
         TabIndex        =   9
         Tag             =   "OBLI=S;CAPTION=Estado"
         Top             =   750
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Instrucciones.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Lbl_Fecha_Ini 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Inicio"
         Height          =   345
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   270
         Width           =   975
      End
      Begin VB.Label lbl_estado 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Estado"
         Height          =   330
         Left            =   120
         TabIndex        =   3
         Top             =   750
         Width           =   975
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha T�rmino"
         Height          =   345
         Index           =   0
         Left            =   2850
         TabIndex        =   2
         Top             =   270
         Width           =   1185
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   8190
      _ExtentX        =   14446
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Instrucciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFecha As Date

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
  End With
  
  With Toolbar_Verificacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORTE").Image = cBoton_Modificar
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
'Dim lCierre As Class_Verificaciones_Cierre

  Call Sub_CargaCombo_Estados(Cmb_Estado, cTEstado_Operaciones, pTodos:=True)
  
  'Set lCierre = New Class_Verificaciones_Cierre
  'fFecha = lCierre.Busca_Ultima_FechaCierre
  fFecha = Format(Fnt_FechaServidor, cFormatDate)
  Call Sub_Limpiar
    
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Call Sub_Limpiar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REPORTE"
      Call Sub_Generar_Reporte
  End Select
End Sub

Private Sub Sub_Limpiar()
  
  Call Sub_ComboSelectedItem(Cmb_Estado, cCmbKALL)
  DTP_Fecha_Ini.Value = fFecha
  DTP_Fecha_Ter.Value = fFecha
  
End Sub

Private Sub Sub_Generar_Reporte()
Const clrHeader = &HD0D0D0
'------------------------------------------
Const sHeader_Ope = "N� Instrucci�n|Cuenta|Producto|Instrumento|Estado|Fecha Operaci�n|Tipo de Movimiento|Moneda"
Const sFormat_Ope = "1500|1000|2500|2500|1000|>1500|2000|1000"
Const sHeader_Det = "Nemot�cnico|Cantidad|Tasa/Precio|Monto Instrucci�n|Cantidad Confirmada|Tasa/Precio Confirmado|Monto Confirmado|Fecha Confirmaci�n"
Const sFormat_Det = "2000|>1200|>1300|>1800|>1800|>2000|>2300|>1700"
'------------------------------------------
Dim sRecord
Dim bAppend
Dim lLinea As Integer
'------------------------------------------
Dim lOperaciones As Class_Operaciones
Dim lCursor_Ope As hRecord
Dim lReg_Ope As hFields
'------------------------------------------
Dim lDetalle As Class_Operaciones_Detalle
Dim lCursor_Det As hRecord
Dim lReg_Det As hFields
'------------------------------------------
Dim lMov_Activo As Class_Mov_Activos
Dim lReg_MA As hFields
'------------------------------------------
Dim lNum_Cuenta As String
'------------------------------------------
Dim lDsc_Instrumento As String
Dim lDsc_Producto As String
'------------------------------------------
Dim lNemotecnico As Class_Nemotecnicos
Dim lReg_Nem As hFields
Dim lNemo As String
'------------------------------------------
Dim lCod_Estado As String
Dim lCantidad_Conf As String
Dim lValor_Mercado_Conf As String
Dim lPrecio_Conf As String
Dim lFecha_Conf As String
Dim lFecha_Desde As String
Dim lFecha_Hasta As String
'------------------------------------------
Dim lTipoMovimiento As String
Dim lForm As Frm_Reporte_Generico
'------------------------------------------
Dim lcGuion   As Class_AvanceGuion
Dim lMsg      As String


  Call Sub_Bloquea_Puntero(Me)
  
  Set lcGuion = New Class_AvanceGuion
  BarraProceso.Value = 0
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  ElseIf DTP_Fecha_Ini.Value > DTP_Fecha_Ter.Value Then
    MsgBox "La fecha de inicio es mayor que la fecha de t�rmino.", vbCritical, Me.Caption
    GoTo ErrProcedure
  End If
  
  lCod_Estado = IIf(Fnt_ComboSelected_KEY(Cmb_Estado) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Estado))
  lFecha_Desde = DTP_Fecha_Ini.Value
  lFecha_Hasta = DTP_Fecha_Ter.Value
  
  Call Sub_StatusBar(pTexto:="Buscando Instrucciones...")
  
  Rem Busca las operaciones seg�n los filtros ingresados
  Set lOperaciones = New Class_Operaciones
  With lOperaciones
    .Campo("Id_Operacion").Valor = ""
    .Campo("Cod_Instrumento").Valor = ""
    .Campo("cod_producto").Valor = ""
    .Campo("Cod_Tipo_Operacion").Valor = ""
    .Campo("Flg_Tipo_Movimiento").Valor = ""
    .Campo("cod_Estado").Valor = lCod_Estado
    If .Fnt_Buscar_Operaciones(lFecha_Desde, lFecha_Hasta, "", "") Then
      Set lCursor_Ope = .Cursor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  Rem Comienzo de la generaci�n del reporte
  Set lForm = New Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="Reporte de Instrucciones" _
                               , pTipoSalida:=ePrinter.eP_pantalla _
                               , pOrientacion:=orLandscape)
     
  With lForm.VsPrinter
    .FontSize = 9
    .Paragraph = "Fecha Consulta entre " & DTP_Fecha_Ini.Value & " y " & DTP_Fecha_Ter.Value
    .Paragraph = "Estado: " & Cmb_Estado.Text
    
    .Paragraph = "" 'salto de linea
    .FontBold = False
    .FontSize = 8
    
    Rem Por cada operacion encontrada
    If lCursor_Ope.Count > 0 Then
      BarraProceso.Max = lCursor_Ope.Count
      For Each lReg_Ope In lCursor_Ope
        BarraProceso.Value = BarraProceso.Value + 1
        lMsg = "Leyendo " & Format(BarraProceso.Value, "#,##0") & " de " & Format(BarraProceso.Max, "#,##0")
        Call Sub_StatusBar(pTexto:=lMsg & lcGuion.Avanza)
        Call Sub_Interactivo(pgRelogDB:=gRelogDB)
      
        Rem Limpia las variables
        lNum_Cuenta = ""
        lDsc_Instrumento = ""
        lDsc_Producto = ""
        lCantidad_Conf = 0
        lValor_Mercado_Conf = 0
        lPrecio_Conf = 0
        lFecha_Conf = ""
        '---------------------------------------
        .FontSize = 9
        .FontBold = True
        .Paragraph = "N�mero Instrucci�n: " & lReg_Ope("id_operacion").Value
        .FontBold = False
        .FontSize = 8
        '---------------------------------------
        '-----------------------------------------------------------
        '--HAF: 07/07/2009
        '--La numero de la cuenta vienen en la vista, no se tiene que ir
        '--a buscar de nuevo.
        lNum_Cuenta = lReg_Ope("num_cuenta").Value
        '-----------------------------------------------------------
        '--HAF: 07/07/2009
        '--La descripcion y el producto vienen en la vista, no se tiene que ir
        '--a buscar de nuevo.
        lDsc_Instrumento = lReg_Ope("dsc_intrumento").Value
        lDsc_Producto = lReg_Ope("dsc_producto").Value
        '-----------------------------------------------------------
        
        Rem Busca los detalles de las operaciones
        Set lDetalle = New Class_Operaciones_Detalle
        With lDetalle
          .Campo("id_operacion").Valor = lReg_Ope("id_operacion").Value
          If .Buscar_Operaciones_Detalle() Then
            Set lCursor_Det = .Cursor
          Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            GoTo ErrProcedure
          End If
        End With
        
        Rem Genera el reporte
        .StartTable
        .TableCell(tcAlignCurrency) = False
        Select Case lReg_Ope("flg_tipo_movimiento").Value
            Case "I"
                lTipoMovimiento = "Ingreso"
            Case "E"
                lTipoMovimiento = "Egreso"
        End Select
            
        sRecord = lReg_Ope("id_operacion").Value & "|" & _
                  lNum_Cuenta & "|" & _
                  lDsc_Producto & "|" & _
                  lDsc_Instrumento & "|" & _
                  lReg_Ope("dsc_estado").Value & "|" & _
                  lReg_Ope("fecha_operacion").Value & "|" & _
                  lTipoMovimiento & "|" & _
                  lReg_Ope("dsc_moneda").Value
        .AddTable sFormat_Ope, sHeader_Ope, sRecord, clrHeader, , bAppend
        .TableCell(tcFontBold, 0) = True
        .EndTable
        
        Rem Detalles
        '---------------------------------
        .FontBold = True
        .Paragraph = "Detalle Instrucci�n"
        .FontBold = False
        '---------------------------------
        .StartTable
        .TableCell(tcAlignCurrency) = False
        
        For Each lReg_Det In lCursor_Det
          Call Sub_StatusBar(pTexto:=lMsg & lcGuion.Avanza)
          Call Sub_Interactivo(pgRelogDB:=gRelogDB)
          
          Rem Busca el mov activo asociado a los detalle
          Set lMov_Activo = New Class_Mov_Activos
          With lMov_Activo
            .Campo("id_operacion_detalle").Valor = lReg_Det("id_operacion_detalle").Value
            If .Buscar() Then
              For Each lReg_MA In .Cursor
                Call Sub_StatusBar(pTexto:=lMsg & lcGuion.Avanza)
                Call Sub_Interactivo(pgRelogDB:=gRelogDB)
                
                lCantidad_Conf = Format(NVL(lReg_MA("cantidad").Value, 0), "###,##0.00")
                lValor_Mercado_Conf = Format(NVL(lReg_MA("monto").Value, 0), "###,##0.00")
                lPrecio_Conf = Format(NVL(lReg_MA("precio").Value, 0), "###,##0.00")
                lFecha_Conf = NVL(lReg_MA("fecha_confirmacion").Value, "")
              Next
            Else
              MsgBox .ErrMsg, vbCritical, Me.Caption
              GoTo ErrProcedure
            End If
          End With
          
          Rem Busca los detalles de las operaciones
          Set lNemotecnico = New Class_Nemotecnicos
          With lNemotecnico
            .Campo("id_nemotecnico").Valor = lReg_Det("id_nemotecnico").Value
            If .Buscar Then
              For Each lReg_Nem In .Cursor
                lNemo = lReg_Nem("nemotecnico").Value
              Next
            Else
              MsgBox .ErrMsg, vbCritical, Me.Caption
              GoTo ErrProcedure
            End If
          End With
          
          sRecord = lNemo & "|" & _
                    Format(lReg_Det("cantidad").Value, "###,##0.00") & "|" & _
                    Format(lReg_Det("precio").Value, "###,##0.00") & "|" & _
                    Format(lReg_Det("monto_pago").Value, "###,##0.00") & "|" & _
                    lCantidad_Conf & "|" & _
                    lPrecio_Conf & "|" & _
                    lValor_Mercado_Conf & "|" & _
                    lFecha_Conf
          .AddTable sFormat_Det, sHeader_Det, sRecord, clrHeader, , bAppend
        Next
        .TableCell(tcFontBold, 0) = True
        .EndTable
        .Paragraph = ""
      Next
    Else
      If lCod_Estado = "" Then
        .Paragraph = "No hay instrucciones para las Fechas indicadas y para todos los Estado."
      Else
        .Paragraph = "No hay instrucciones para las Fechas indicadas y Estado '" & Cmb_Estado.Text & "'."
      End If
    End If
    
    .EndDoc
  End With
  
  Call ColocarEn(lForm, Me, eFP_Abajo)
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  Call Sub_StatusBar(pTexto:="Listo.")
End Sub
