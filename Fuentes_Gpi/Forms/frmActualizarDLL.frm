VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmActualizarDLL 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3720
   ClientLeft      =   255
   ClientTop       =   1410
   ClientWidth     =   5865
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmActualizarDLL.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3720
   ScaleWidth      =   5865
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Height          =   3795
      Left            =   0
      TabIndex        =   0
      Top             =   -60
      Width           =   5880
      Begin VB.PictureBox piclogo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1425
         Left            =   120
         Picture         =   "frmActualizarDLL.frx":000C
         ScaleHeight     =   1425
         ScaleWidth      =   1065
         TabIndex        =   10
         Top             =   720
         Width           =   1065
      End
      Begin MSComctlLib.ProgressBar barActualiza 
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   3360
         Width           =   5655
         _ExtentX        =   9975
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
      End
      Begin VB.Label lblWarning 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Revisando Actualizaciones"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   3120
         Width           =   5415
      End
      Begin VB.Label lblCopyright 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Copyright"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Left            =   3240
         TabIndex        =   3
         Top             =   2700
         Width           =   2415
      End
      Begin VB.Label lblCompany 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Compa��a"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Left            =   3240
         TabIndex        =   2
         Top             =   2910
         Width           =   2415
      End
      Begin VB.Label lblVersion 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Versi�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   285
         Left            =   4770
         TabIndex        =   4
         Top             =   2340
         Width           =   885
      End
      Begin VB.Label lblPlatform 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Plataforma"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   360
         Left            =   4380
         TabIndex        =   5
         Top             =   1980
         Width           =   1275
      End
      Begin VB.Label lblProductName 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   27.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   660
         Left            =   1320
         TabIndex        =   7
         Top             =   1140
         Width           =   2460
      End
      Begin VB.Label lblLicenseTo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Caption         =   "Autorizado a"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   5415
      End
      Begin VB.Label lblCompanyProduct 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Creasys Chile."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   435
         Left            =   1275
         TabIndex        =   6
         Top             =   705
         Width           =   2460
      End
   End
End
Attribute VB_Name = "frmActualizarDLL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const VS_FFI_SIGNATURE = &HFEEF04BD
Const VS_FFI_STRUCVERSION = &H10000
Const VS_FFI_FILEFLAGSMASK = &H3F&
Const VS_FF_DEBUG = &H1
Const VS_FF_PRERELEASE = &H2
Const VS_FF_PATCHED = &H4
Const VS_FF_PRIVATEBUILD = &H8
Const VS_FF_INFOINFERRED = &H10
Const VS_FF_SPECIALBUILD = &H20
Const VOS_UNKNOWN = &H0
Const VOS_DOS = &H10000
Const VOS_OS216 = &H20000
Const VOS_OS232 = &H30000
Const VOS_NT = &H40000
Const VOS__BASE = &H0
Const VOS__WINDOWS16 = &H1
Const VOS__PM16 = &H2
Const VOS__PM32 = &H3
Const VOS__WINDOWS32 = &H4
Const VOS_DOS_WINDOWS16 = &H10001
Const VOS_DOS_WINDOWS32 = &H10004
Const VOS_OS216_PM16 = &H20002
Const VOS_OS232_PM32 = &H30003
Const VOS_NT_WINDOWS32 = &H40004
Const VFT_UNKNOWN = &H0
Const VFT_APP = &H1
Const VFT_DLL = &H2
Const VFT_DRV = &H3
Const VFT_FONT = &H4
Const VFT_VXD = &H5
Const VFT_STATIC_LIB = &H7
Const VFT2_UNKNOWN = &H0
Const VFT2_DRV_PRINTER = &H1
Const VFT2_DRV_KEYBOARD = &H2
Const VFT2_DRV_LANGUAGE = &H3
Const VFT2_DRV_DISPLAY = &H4
Const VFT2_DRV_MOUSE = &H5
Const VFT2_DRV_NETWORK = &H6
Const VFT2_DRV_SYSTEM = &H7
Const VFT2_DRV_INSTALLABLE = &H8
Const VFT2_DRV_SOUND = &H9
Const VFT2_DRV_COMM = &HA

Private Type VS_FIXEDFILEINFO
   dwSignature As Long
   dwStrucVersionl As Integer     '  e.g. = &h0000 = 0
   dwStrucVersionh As Integer     '  e.g. = &h0042 = .42
   dwFileVersionMSl As Integer    '  e.g. = &h0003 = 3
   dwFileVersionMSh As Integer    '  e.g. = &h0075 = .75
   dwFileVersionLSl As Integer    '  e.g. = &h0000 = 0
   dwFileVersionLSh As Integer    '  e.g. = &h0031 = .31
   dwProductVersionMSl As Integer '  e.g. = &h0003 = 3
   dwProductVersionMSh As Integer '  e.g. = &h0010 = .1
   dwProductVersionLSl As Integer '  e.g. = &h0000 = 0
   dwProductVersionLSh As Integer '  e.g. = &h0031 = .31
   dwFileFlagsMask As Long        '  = &h3F for version "0.42"
   dwFileFlags As Long            '  e.g. VFF_DEBUG Or VFF_PRERELEASE
   dwFileOS As Long               '  e.g. VOS_DOS_WINDOWS16
   dwFileType As Long             '  e.g. VFT_DRIVER
   dwFileSubtype As Long          '  e.g. VFT2_DRV_KEYBOARD
   dwFileDateMS As Long           '  e.g. 0
   dwFileDateLS As Long           '  e.g. 0
End Type


'***************************************************************************
'*  C�digo fuente del m�dulo bas
'***************************************************************************
'Declaraciones del Api
'------------------------------------------------------------------------------
'Esta funci�n busca el primer archivo de un Dir
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" ( _
    ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long

'Esta el siguiente archivo o directorio
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" ( _
    ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long

Private Declare Function GetFileAttributes Lib "kernel32" Alias "GetFileAttributesA" ( _
    ByVal lpFileName As String) As Long

'Esta cierra el Handle de b�squeda
Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
'*****************
Private Declare Function GetFileVersionInfo Lib "Version.dll" Alias "GetFileVersionInfoA" (ByVal lptstrFilename As String, ByVal dwhandle As Long, ByVal dwlen As Long, lpData As Any) As Long
Private Declare Function GetFileVersionInfoSize Lib "Version.dll" Alias "GetFileVersionInfoSizeA" (ByVal lptstrFilename As String, lpdwHandle As Long) As Long
Private Declare Function VerQueryValue Lib "Version.dll" Alias "VerQueryValueA" (pBlock As Any, ByVal lpSubBlock As String, lplpBuffer As Any, puLen As Long) As Long
Private Declare Sub MoveMemory Lib "kernel32" Alias "RtlMoveMemory" (dest As Any, ByVal Source As Long, ByVal Length As Long)
Dim Filename As String, Directory As String, FullFileName As String
Dim StrucVer As String, FileVer As String, ProdVer As String
Dim FileFlags As String, FileOS As String, FileType As String, FileSubType As String
'***************



Private Declare Function LoadLibrary Lib "kernel32" Alias "LoadLibraryA" (ByVal lpLibFileName As String) As Long
Private Declare Function GetProcAddress Lib "kernel32" (ByVal hModule As Long, ByVal lpProcName As String) As Long
Private Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" _
                    (ByVal lpPrevWndFunc As Long, _
                    ByVal hwnd As Long, _
                    ByVal Msg As Any, _
                    ByVal wParam As Any, _
                    ByVal lParam As Any) As Long

Private Declare Function FreeLibrary Lib "kernel32" (ByVal hLibModule As Long) As Long

Private Const Err_Reg = &H0

' Constantes
'------------------------------------------------------------------------------

'Constantes de atributos de archivos
Const FILE_ATTRIBUTE_ARCHIVE = &H20
Const FILE_ATTRIBUTE_DIRECTORY = &H10
Const FILE_ATTRIBUTE_HIDDEN = &H2
Const FILE_ATTRIBUTE_NORMAL = &H80
Const FILE_ATTRIBUTE_READONLY = &H1
Const FILE_ATTRIBUTE_SYSTEM = &H4
Const FILE_ATTRIBUTE_TEMPORARY = &H100

'Otras constantes
Const MAX_PATH = 260
Const MAXDWORD = &HFFFF
Const INVALID_HANDLE_VALUE = -1

'UDT
'------------------------------------------------------------------------------
'Estructura para las fechas de los archivos
Private Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type

'Estructura necesaria para la informaci�n de archivos
Private Type WIN32_FIND_DATA
    dwFileAttributes As Long
    ftCreationTime As FILETIME
    ftLastAccessTime As FILETIME
    ftLastWriteTime As FILETIME
    nFileSizeHigh As Long
    nFileSizeLow As Long
    dwReserved0 As Long
    dwReserved1 As Long
    cFileName As String * MAX_PATH
    cAlternate As String * 14
End Type

Dim aDLLNames()
Dim aDeleteDLLNames()

'-----------------------------------------------------------------------
    'Funciones
'-----------------------------------------------------------------------
'Esta funci�n es para formatear los nombres de archivos y directorios. Elimina los CHR(0)
'------------------------------------------------------------------------
Function Eliminar_Nulos(OriginalStr As String) As String
    
    If (InStr(OriginalStr, Chr(0)) > 0) Then
        OriginalStr = Left(OriginalStr, InStr(OriginalStr, Chr(0)) - 1)
    End If
    Eliminar_Nulos = OriginalStr

End Function

'Esta funci�n es la principal que permite buscar _
 los archivos y listarlos en el ListBox


Function FindFilesAPI(Path As String, _
                      SearchStr As String, _
                      FileCount As Long, _
                      DirCount As Long, _
                      Optional bDllOcx As Boolean = True)


    Dim Filename As String
    Dim DirName As String
    Dim dirNames() As String
    Dim nDir As Long
    Dim i As Long
    Dim hSearch As Long
    Dim WFD As WIN32_FIND_DATA
    Dim cont As Long


    If Right(Path, 1) <> "\" Then Path = Path & "\"
        ' Buscamos por mas directorios
        nDir = 0
        ReDim dirNames(nDir)
        cont = True
        hSearch = FindFirstFile(Path & "*", WFD)
            If hSearch <> INVALID_HANDLE_VALUE Then
                Do While cont
                    DirName = Eliminar_Nulos(WFD.cFileName)
                    ' Ignore the current and encompassing directories.
                    If (DirName <> ".") And (DirName <> "..") Then
                        ' Check for directory with bitwise comparison.
                            If GetFileAttributes(Path & DirName) _
                                And FILE_ATTRIBUTE_DIRECTORY Then
                                
                                dirNames(nDir) = DirName
                                DirCount = DirCount + 1
                                nDir = nDir + 1
                                ReDim Preserve dirNames(nDir)
                            
                            End If
                    End If
                    cont = FindNextFile(hSearch, WFD) 'Get next subdirectory.
                Loop
                
                cont = FindClose(hSearch)
            
            End If

        hSearch = FindFirstFile(Path & SearchStr, WFD)
        cont = True
        If hSearch <> INVALID_HANDLE_VALUE Then
            While cont
                Filename = Eliminar_Nulos(WFD.cFileName)
                    If (Filename <> ".") And (Filename <> "..") Then
                        FindFilesAPI = FindFilesAPI + (WFD.nFileSizeHigh * MAXDWORD) _
                                                                  + WFD.nFileSizeLow
                        FileCount = FileCount + 1
                        If bDllOcx Then
                            ReDim Preserve aDLLNames(FileCount)
                            aDLLNames(FileCount - 1) = Path & Filename
                        Else
                            ReDim Preserve aDeleteDLLNames(FileCount)
                            aDeleteDLLNames(FileCount - 1) = Path & Filename
                        End If
                    End If
                cont = FindNextFile(hSearch, WFD) ' Get next file
            Wend
        cont = FindClose(hSearch)
        End If

        ' Si estos son Sub Directorios......
        If nDir > 0 Then

        For i = 0 To nDir - 1
            FindFilesAPI = FindFilesAPI + FindFilesAPI(Path & dirNames(i) & "\", _
                                                SearchStr, FileCount, DirCount)
        Next i
    End If
End Function

Public Sub Mantener_DLLs()
    Dim lRutaApp As String
    Dim lVersion_Local As String
    Dim ln_Archivo As Byte
    Dim lCadena As String
    Dim lEvalua As Boolean
    Dim lListadoDLL As String
    Dim lVersion_Servidor As String
    Dim lVersion_Actual As String
    Dim lArregloDLL() As String
    Dim lIndex As Integer
    Dim lNombreDLL As String
    Dim lVersion_Disco_DLL As String
    Dim lVersion_Registro_DLL As String
    
    'Lectura de Version Local
    lVersion_Local = ""
    lVersion_Local = GetSetting("CsGpi", "Version", "Actual_Entrega")
        
    'Lectura de Version Servidor
    lRutaApp = DameRutaApp()

    lEvalua = False
    lListadoDLL = ""

    ln_Archivo = FreeFile
    Open (lRutaApp & "VERSION_ENTREGA.INI") For Input Access Read As #ln_Archivo
    
        Do While Not EOF(ln_Archivo)
            Line Input #ln_Archivo, lCadena
            If (Trim(lCadena) = "") Then
                lEvalua = False
                Line Input #ln_Archivo, lCadena
            End If
            If (Not lEvalua) Then
                If Left$(lCadena, 1) = "[" Then
                    lVersion_Servidor = Mid(lCadena, InStr(1, lCadena, " ") + 1)
                    lVersion_Servidor = Mid(lVersion_Servidor, 1, Len(lVersion_Servidor) - 1)
                    If (lVersion_Actual = "") Then
                        lVersion_Actual = lVersion_Servidor
                    End If
                End If
                If (lVersion_Local = lVersion_Servidor Or lVersion_Local = "") Then
                    Exit Do
                End If
                Line Input #ln_Archivo, lCadena
                lEvalua = True
            Else
                If (InStr(1, lListadoDLL, lCadena) = 0) Then
                    lListadoDLL = lListadoDLL & lCadena & ","
                End If
            End If
        Loop
    'Line Input #ln_Archivo, LINEA
    Close ln_Archivo
        
    'Al detectarse que no existe registro de la version de entrega, se realiza registro de todas las DLL's
    If (lVersion_Local = "") Then
        Inicial
        'Actualizar Version de Entrega en DLL Local
        Call SaveSetting("CsGpi", "Version", "Actual_Entrega", lVersion_Actual)
    End If

    'Si listado es vacio, es por que no hay DLL que registrar
    If (lListadoDLL <> "") Then
        'Traspasa a Arreglo Listado de DLL's
        lListadoDLL = Mid(lListadoDLL, 1, Len(lListadoDLL) - 1)
        lArregloDLL = Split(lListadoDLL, ",")
            
        'Actualiza Registro de DLL's detectadas
        For lIndex = LBound(lArregloDLL) To UBound(lArregloDLL)
            lNombreDLL = LCase(lArregloDLL(lIndex))
            lNombreDLL = lRutaApp & "DLL\" & lArregloDLL(lIndex)
            
            'Busca Version de la DLL
            lVersion_Disco_DLL = DisplayVerInfo(CStr(lNombreDLL))
            
            lVersion_Registro_DLL = Fnt_ObtieneVersion_DLL(lArregloDLL(lIndex))
            'Borra Registro anterior de la DLL
            If (lVersion_Registro_DLL <> "0.0.0") Then
                Call PubBorraVersion_DLL(lArregloDLL(lIndex))
            End If
            
            'Realiza registro de la DLL
            WinRegSvr lNombreDLL, False
            If WinRegSvr(lNombreDLL, True) Then
                'Graba en registro version de la DLL
                Call PubGrabaVersion_DLL(lNombreDLL, lArregloDLL(lIndex), lVersion_Disco_DLL)
            End If
        Next
        'Actualizar Version de Entrega en DLL Local
        Call SaveSetting("CsGpi", "Version", "Actual_Entrega", lVersion_Actual)
    End If

End Sub

Public Sub Inicial()
    Dim i As Integer
    Dim x As Integer
    Dim sNombre As String
    
    Dim CountFiles As Long
    Dim DirCount As Long
    
    Dim sRutaApp As String
    Dim sVersionDllDisco As String
    Dim sVersionDllRegistro As String
     
    barActualiza.Min = 0
    barActualiza.Max = 100
    
    Me.Show
    
    DoEvents
    
    '(BIN\*.dll BIN\*.ocx CSBPI\DLL\*.dll)
    
    sRutaApp = DameRutaApp()
    Call fnt_borrar_registros_dll
    
    FindFilesAPI sRutaApp & "BIN\", "*.DLL", CountFiles, DirCount
    FindFilesAPI sRutaApp & "BIN\", "*.OCX", CountFiles, DirCount
    FindFilesAPI sRutaApp & "DLL\", "*.DLL", CountFiles, DirCount
    
    DoEvents
    
    barActualiza.Max = UBound(aDLLNames) - 1
    
    For i = LBound(aDLLNames) To UBound(aDLLNames) - 1
        sNombre = LCase(aDLLNames(i))
        
        If InStr(aDLLNames(i), "aspapi.dll") = 0 Then
            sVersionDllDisco = DisplayVerInfo(CStr(aDLLNames(i)))
            sVersionDllRegistro = Fnt_ObtieneVersion_DLL(aDLLNames(i))
            If Trim(sVersionDllDisco) <> Trim(sVersionDllRegistro) Then
                WinRegSvr aDLLNames(i), False
                If WinRegSvr(aDLLNames(i), True) Then
                    sNombre = NombreDeArchivo(aDLLNames(i))
                    Call PubGrabaVersion_DLL(aDLLNames(i), sNombre, sVersionDllDisco)
                End If
                
            End If
            
        End If
        
        barActualiza.Value = i
        DoEvents
        
    Next
    
    Unload Me
    
End Sub

Function NombreDeArchivo(ByVal Path As String) As String
    Dim iPos As Integer
    
    iPos = InStrRev(Path, "\")
    If iPos Then
        NombreDeArchivo = Right(Path, Len(Path) - iPos - 1) 'Sin el menos uno devuelve la \ al final
    Else
        NombreDeArchivo = Path  'Error el path no es valido o algo asi
       
    End If
     Exit Function
End Function


Private Sub Form_Load()
    lblVersion.Caption = "Versi�n " & App.Major & "." & App.Minor & "." & App.Revision
    ' lblProductName.Caption = App.Title
    
    lblProductName.Caption = "CSGPI"
    
    lblPlatform.Caption = ""
    lblLicenseTo.Caption = ""
    lblCopyright.Caption = ""
    lblCompany.Caption = ""
    
    barActualiza.Min = 0
    barActualiza.Max = 100
    
End Sub


Public Function WinRegSvr(ByVal LPath As String, ByVal blnRegistra As Boolean) As Boolean
    On Error Resume Next
    Dim lID, status As Long
    
    lID = LoadLibrary(LPath)
    
    If lID = 0 Then
        MsgBox "Problemas con el registro de las Actualizaciones" & vbCrLf & "(" & LPath & ")"   'Verifica su Dll/OCX  est� carregada
        Exit Function
    End If
    
    If blnRegistra Then
        status = GetProcAddress(lID, "DllRegisterServer") 'Registra a DLL/OCX
    Else
        status = GetProcAddress(lID, "DllUnregisterServer") 'Des-registra a DLL / OCX
    End If
    
    If CallWindowProc(status, Me.hwnd, ByVal 0&, ByVal 0&, ByVal 0&) = Err_Reg Then
        WinRegSvr = True
    End If
    
    FreeLibrary lID

End Function

Public Function DameRutaApp() As String
    DameRutaApp = App.Path
    
    If Right(App.Path, 1) <> "\" Then
       DameRutaApp = DameRutaApp & "\"
    End If
    
End Function

Private Function DisplayVerInfo(ByVal FullFileName As String) As String
   Dim rc As Long, lDummy As Long, sBuffer() As Byte
   Dim lBufferLen As Long, lVerPointer As Long, udtVerBuffer As VS_FIXEDFILEINFO
   Dim lVerbufferLen As Long
   Dim FileVer As String

   '*** Get size ****
   lBufferLen = GetFileVersionInfoSize(FullFileName, lDummy)
   If lBufferLen < 1 Then
      DisplayVerInfo = "0.0.0"
      Exit Function
   End If

   '**** Store info to udtVerBuffer struct ****
   ReDim sBuffer(lBufferLen)
   rc = GetFileVersionInfo(FullFileName, 0&, lBufferLen, sBuffer(0))
   rc = VerQueryValue(sBuffer(0), "\", lVerPointer, lVerbufferLen)
   MoveMemory udtVerBuffer, lVerPointer, Len(udtVerBuffer)

   '**** Determine Structure Version number - NOT USED ****
   StrucVer = Format$(udtVerBuffer.dwStrucVersionh) & "." & Format$(udtVerBuffer.dwStrucVersionl)

   '**** Determine File Version number ****
   FileVer = Format$(udtVerBuffer.dwFileVersionMSh) & "." & Format$(udtVerBuffer.dwFileVersionMSl) & "." & Format$(udtVerBuffer.dwFileVersionLSh) & "." & Format$(udtVerBuffer.dwFileVersionLSl)

   '**** Determine Product Version number ****
   ProdVer = Format$(udtVerBuffer.dwProductVersionMSh) & "." & Format$(udtVerBuffer.dwProductVersionMSl) & "." & Format$(udtVerBuffer.dwProductVersionLSh) & "." & Format$(udtVerBuffer.dwProductVersionLSl)
   DisplayVerInfo = ProdVer

   '**** Determine Boolean attributes of File ****
'   FileFlags = ""
'   If udtVerBuffer.dwFileFlags And VS_FF_DEBUG Then FileFlags = "Debug "
'   If udtVerBuffer.dwFileFlags And VS_FF_PRERELEASE Then FileFlags = FileFlags & "PreRel "
'   If udtVerBuffer.dwFileFlags And VS_FF_PATCHED Then FileFlags = FileFlags & "Patched "
'   If udtVerBuffer.dwFileFlags And VS_FF_PRIVATEBUILD Then FileFlags = FileFlags & "Private "
'   If udtVerBuffer.dwFileFlags And VS_FF_INFOINFERRED Then FileFlags = FileFlags & "Info "
'   If udtVerBuffer.dwFileFlags And VS_FF_SPECIALBUILD Then FileFlags = FileFlags & "Special "
'   If udtVerBuffer.dwFileFlags And VFT2_UNKNOWN Then FileFlags = FileFlags + "Unknown "
'
'   '**** Determine OS for which file was designed ****
'   Select Case udtVerBuffer.dwFileOS
'      Case VOS_DOS_WINDOWS16
'        FileOS = "DOS-Win16"
'      Case VOS_DOS_WINDOWS32
'        FileOS = "DOS-Win32"
'      Case VOS_OS216_PM16
'        FileOS = "OS/2-16 PM-16"
'      Case VOS_OS232_PM32
'        FileOS = "OS/2-16 PM-32"
'      Case VOS_NT_WINDOWS32
'        FileOS = "NT-Win32"
'      Case Else
'        FileOS = "Unknown"
'   End Select
'   Select Case udtVerBuffer.dwFileType
'      Case VFT_APP
'         FileType = "App"
'      Case VFT_DLL
'         FileType = "DLL"
'      Case VFT_DRV
'         FileType = "Driver"
'         Select Case udtVerBuffer.dwFileSubtype
'            Case VFT2_DRV_PRINTER
'               FileSubType = "Printer drv"
'            Case VFT2_DRV_KEYBOARD
'               FileSubType = "Keyboard drv"
'            Case VFT2_DRV_LANGUAGE
'               FileSubType = "Language drv"
'            Case VFT2_DRV_DISPLAY
'               FileSubType = "Display drv"
'            Case VFT2_DRV_MOUSE
'               FileSubType = "Mouse drv"
'            Case VFT2_DRV_NETWORK
'               FileSubType = "Network drv"
'            Case VFT2_DRV_SYSTEM
'               FileSubType = "System drv"
'            Case VFT2_DRV_INSTALLABLE
'               FileSubType = "Installable"
'            Case VFT2_DRV_SOUND
'               FileSubType = "Sound drv"
'            Case VFT2_DRV_COMM
'               FileSubType = "Comm drv"
'            Case VFT2_UNKNOWN
'               FileSubType = "Unknown"
'         End Select
'      Case VFT_VXD
'         FileType = "VxD"
'      Case VFT_STATIC_LIB
'         FileType = "Lib"
'      Case Else
'         FileType = "Unknown"
'   End Select
End Function

Private Function Fnt_ObtieneVersion_DLL(ByVal Name_Dll As String) As String
   Dim strResultado As String
   strResultado = GetSetting("CsGpi", "RegDll", Name_Dll)
   If Trim(strResultado) <> "" Then
    Fnt_ObtieneVersion_DLL = strResultado
   Else
    Fnt_ObtieneVersion_DLL = "0.0.0"
   End If
End Function
Private Sub PubGrabaVersion_DLL(ByVal RutaNombreDll As String, ByVal Name_Dll As String, Version As String)
   Call SaveSetting("CsGpi", "RegDll", Name_Dll, Version)
   'Call SaveSetting("CsGpi", "Rutas", RutaNombreDll, Version)
End Sub
Private Sub PubBorraVersion_DLL(ByVal Name_Dll As String)
   Call DeleteSetting("CsGpi", "RegDll", Name_Dll)
End Sub

Private Function fnt_borrar_registros_dll()
' AQUI SE DEBE DESREGRISTRA LAS DLL Y OCX ELIMINAS DE LA APLICACION
'    ' Busca Los Archivos Excluido para DesRegistra y Borrar la Informaci�n de Versi�n
'    FindFilesAPI sRutaApp & "BIN\", "*.DXX", CountFiles, DirCount, False
'    FindFilesAPI sRutaApp & "BIN\", "*.OXX", CountFiles, DirCount, False
'    FindFilesAPI sRutaApp & "DLL\", "*.DXX", CountFiles, DirCount, False
'
'    barActualiza.Max = UBound(aDeleteDLLNames)
'
'    For i = LBound(aDeleteDLLNames) To UBound(aDeleteDLLNames) - 1
'        sNombre = LCase(aDeleteDLLNames(i))
'        sNombre = Replace(sNombre, ".dxx", ".dll")
'        sNombre = Replace(sNombre, ".oxx", ".ocx")
'        If InStr(sNombre, "aspapi.dll") = 0 Then
'            WinRegSvr sNombre, False
'            Call PubBorraVersion_DLL(sNombre)
'        End If
'        barActualiza.Value = i
'        DoEvents
'    Next
End Function
