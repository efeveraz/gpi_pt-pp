VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Contraparte 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Contraparte"
   ClientHeight    =   5040
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6930
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5040
   ScaleWidth      =   6930
   Begin VB.Frame Frame1 
      Caption         =   "Contraparte"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4545
      Left            =   60
      TabIndex        =   2
      Top             =   450
      Width           =   6795
      Begin C1SizerLibCtl.C1Tab SSTab 
         Height          =   2865
         Left            =   180
         TabIndex        =   6
         Top             =   1470
         Width           =   6435
         _cx             =   11351
         _cy             =   5054
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         Appearance      =   0
         MousePointer    =   0
         Version         =   801
         BackColor       =   6579300
         ForeColor       =   -2147483634
         FrontTabColor   =   -2147483635
         BackTabColor    =   6579300
         TabOutlineColor =   -2147483632
         FrontTabForeColor=   -2147483634
         Caption         =   "&Instrumentos|&Traders"
         Align           =   0
         CurrTab         =   1
         FirstTab        =   0
         Style           =   6
         Position        =   0
         AutoSwitch      =   -1  'True
         AutoScroll      =   -1  'True
         TabPreview      =   -1  'True
         ShowFocusRect   =   0   'False
         TabsPerPage     =   0
         BorderWidth     =   0
         BoldCurrent     =   -1  'True
         DogEars         =   -1  'True
         MultiRow        =   0   'False
         MultiRowOffset  =   200
         CaptionStyle    =   0
         TabHeight       =   0
         TabCaptionPos   =   4
         TabPicturePos   =   0
         CaptionEmpty    =   ""
         Separators      =   0   'False
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   37
         Begin VB.Frame Frame2 
            Caption         =   "Traders"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   2520
            Left            =   15
            TabIndex        =   10
            Top             =   330
            Width           =   6405
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Traders 
               Height          =   2085
               Left            =   120
               TabIndex        =   11
               Top             =   270
               Width           =   5505
               _cx             =   9710
               _cy             =   3678
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   3
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Contraparte.frx":0000
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
            Begin MSComctlLib.Toolbar Toolbar_Trader 
               Height          =   660
               Left            =   5790
               TabIndex        =   12
               Top             =   270
               Width           =   450
               _ExtentX        =   794
               _ExtentY        =   1164
               ButtonWidth     =   1138
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   2
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "ADD"
                     Description     =   "Agregar un nemotecnico a la Operaci�n"
                     Object.ToolTipText     =   "Agregar un instrumento a la Contraparte"
                  EndProperty
                  BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "DEL"
                     Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                     Object.ToolTipText     =   "Elimina el instrumento seleccionado de la Contraparte"
                  EndProperty
               EndProperty
            End
         End
         Begin VB.Frame frm_instrumentos 
            Caption         =   "Instrumentos"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   2520
            Left            =   -7020
            TabIndex        =   7
            Top             =   330
            Width           =   6405
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Instrumento 
               Height          =   2085
               Left            =   120
               TabIndex        =   8
               Top             =   270
               Width           =   5505
               _cx             =   9710
               _cy             =   3678
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   4
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Contraparte.frx":0088
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
            Begin MSComctlLib.Toolbar Toolbar_Instrumento 
               Height          =   660
               Left            =   5790
               TabIndex        =   9
               Top             =   270
               Width           =   450
               _ExtentX        =   794
               _ExtentY        =   1164
               ButtonWidth     =   1138
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   2
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "ADD"
                     Description     =   "Agregar un nemotecnico a la Operaci�n"
                     Object.ToolTipText     =   "Agregar un instrumento a la Contraparte"
                  EndProperty
                  BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "DEL"
                     Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                     Object.ToolTipText     =   "Elimina el instrumento seleccionado de la Contraparte"
                  EndProperty
               EndProperty
            End
         End
      End
      Begin hControl2.hTextLabel Txt_Contraparte 
         Height          =   315
         Left            =   180
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   660
         Width           =   5550
         _ExtentX        =   9790
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Contraparte"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   50
      End
      Begin TrueDBList80.TDBCombo Cmb_TipoContraparte 
         Height          =   345
         Left            =   1500
         TabIndex        =   5
         Tag             =   "OBLI=S;CAPTION=Tipo Contraparte"
         Top             =   1000
         Width           =   4245
         _ExtentX        =   7488
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Contraparte.frx":0155
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin hControl2.hTextLabel Txt_RutContraparte 
         Height          =   315
         Left            =   180
         TabIndex        =   13
         Tag             =   "OBLI"
         Top             =   315
         Width           =   5550
         _ExtentX        =   9790
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Rut"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   50
      End
      Begin VB.Label lbl_tipo_cintraparte 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tipo Contraparte"
         Height          =   345
         Left            =   180
         TabIndex        =   4
         Top             =   1000
         Width           =   1305
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6930
      _ExtentX        =   12224
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Contraparte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'CAAL 08/2013 SE AGREGO RUT CONTRAPARTE
Option Explicit


Public fKey As String
Dim lCod_Producto_new As String
Dim lDsc_Producto_new As String
Dim lCod_Instrumento_new As String
Dim lDsc_Instrumento_new As String

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With
  
  With Toolbar_Instrumento
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Appearance = ccFlat
  End With
  
  With Toolbar_Trader
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Appearance = ccFlat
  End With
  
  Call Sub_CargaForm

  Me.SSTab.CurrTab = 0
  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pId_Contraparte, pCod_Arbol_Sistema)
  fKey = pId_Contraparte
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  Call Form_Resize
  
  Call Sub_CargarDatos
    
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de Contraparte"
  Else
    Me.Caption = "Modificaci�n Contraparte: " & Txt_Contraparte.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Traders_AfterEdit(ByVal Row As Long, ByVal Col As Long)
  Select Case Col
    Case Grilla_Traders.ColIndex("email")
      If Not Fnt_VerificaEMAIL(Grilla_Traders.Text) Then
        With Grilla_Traders
          .Text = .EditText
        End With
      End If
  End Select
End Sub

Private Sub Grilla_Traders_DblClick()
  If Grilla_Traders.Row > 0 Then
    Call Sub_EsperaVentana_Trader(GetCell(Grilla_Traders, Grilla_Traders.Row, "colum_pk"))
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Rem Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lTipo_Contraparte As String
Dim lFila As Long
Dim lRollback As Boolean
Dim lContraparte As Class_Contrapartes
Dim lcRel_Contrapartes_Instr As Class_Rel_Contrapartes_Instrum

  Call Sub_Bloquea_Puntero(Me)
  
  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Call Sub_Desbloquea_Puntero(Me)
    Exit Function
  End If

  lTipo_Contraparte = Fnt_ComboSelected_KEY(Cmb_TipoContraparte)
  
  lRollback = False
  gDB.IniciarTransaccion
  
  Set lContraparte = New Class_Contrapartes
  With lContraparte
    Rem Graba los datos en Contrapartes
    .Campo("id_contraparte").Valor = fKey
    .Campo("dsc_contraparte").Valor = Txt_Contraparte.Text
    .Campo("id_tipo_contraparte").Valor = lTipo_Contraparte
    .Campo("rut_contraparte").Valor = Txt_RutContraparte.Text
    
    
    If Not .Guardar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en guardar Contraparte.", _
                        .ErrMsg, _
                        pConLog:=True)
      lRollback = True
      GoTo ErrProcedure
    End If
    fKey = .Campo("id_contraparte").Valor
  End With
  Set lContraparte = Nothing
  
  Rem Borra todos los registros de Rel_contrapartes_instrumentos seg�n el id
  Set lcRel_Contrapartes_Instr = New Class_Rel_Contrapartes_Instrum
  With lcRel_Contrapartes_Instr
    .Campo("ID_CONTRAPARTE").Valor = fKey
    If Not .Borrar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en guardar Contraparte.", _
                        .ErrMsg, _
                        pConLog:=True)
      lRollback = True
      GoTo ErrProcedure
    End If
  End With
  Set lcRel_Contrapartes_Instr = Nothing
  
  Rem Graba los instrumentos de la Grilla_Instrumento
  If Grilla_Instrumento.Rows > 1 Then
    For lFila = 1 To Grilla_Instrumento.Rows - 1
      Set lcRel_Contrapartes_Instr = New Class_Rel_Contrapartes_Instrum
      With lcRel_Contrapartes_Instr
        .Campo("id_contraparte").Valor = fKey
        .Campo("cod_instrumento").Valor = GetCell(Grilla_Instrumento, lFila, "cod_instrumento")
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                      "Problemas en guardar Contraparte.", _
                      .ErrMsg, _
                      pConLog:=True)
          lRollback = True
          Exit For
        End If
      End With
      Set lcRel_Contrapartes_Instr = Nothing
    Next
  End If
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    Fnt_Grabar = False
  Else
    gDB.CommitTransaccion
    Call Sub_CargarDatos
  End If
  
  Set lContraparte = Nothing
  Set lcRel_Contrapartes_Instr = Nothing

  Call Sub_Desbloquea_Puntero(Me)
End Function

Private Sub Sub_CargaForm()

  Call Sub_FormControl_Color(Me.Controls)

  Rem Carga los Tipos de Contrapartes
  Call Sub_CargaCombo_Tipos_Contrapartes(Cmb_TipoContraparte)
  
  Txt_Contraparte.Text = ""
    
  Grilla_Instrumento.Rows = 1
  Grilla_Traders.Rows = 1
    
End Sub

Private Sub Sub_CargarDatos()
Dim lcContraparte           As Class_Contrapartes
Dim lcRel_Contrapartes_Inst As Class_Rel_Contrapartes_Instrum
Dim lReg                    As hCollection.hFields
'-----------------------------------------------------------------
Dim lLinea  As Long
Dim lID     As String
  
  Load Me
  If Grilla_Instrumento.Row > 0 Then
    lID = GetCell(Grilla_Instrumento, Grilla_Instrumento.Row, "colum_pk")
  Else
    lID = ""
  End If

  Grilla_Instrumento.Rows = 1
  
  Set lcContraparte = New Class_Contrapartes
  With lcContraparte
    .Campo("ID_CONTRAPARTE").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        Txt_RutContraparte.Text = DLL_COMUN.NVL(lReg("RUT_CONTRAPARTE").Value, "")
        Txt_Contraparte.Text = DLL_COMUN.NVL(lReg("DSC_CONTRAPARTE").Value, "")
        
        Call Sub_ComboSelectedItem(Cmb_TipoContraparte, lReg("ID_TIPO_CONTRAPARTE").Value)
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Contraparte.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcContraparte = Nothing
    
  Set lcRel_Contrapartes_Inst = New Class_Rel_Contrapartes_Instrum
  With lcRel_Contrapartes_Inst
    .Campo("ID_CONTRAPARTE").Valor = fKey
    If .BuscarView Then
      For Each lReg In .Cursor
        lLinea = Grilla_Instrumento.Rows
        Grilla_Instrumento.AddItem ""
        
        SetCell Grilla_Instrumento, lLinea, "colum_pk", lReg("COD_PRODUCTO").Value
        SetCell Grilla_Instrumento, lLinea, "dsc_producto", lReg("DSC_PRODUCTO").Value
        SetCell Grilla_Instrumento, lLinea, "cod_instrumento", lReg("COD_INSTRUMENTO").Value
        SetCell Grilla_Instrumento, lLinea, "dsc_instrumento", lReg("DSC_INTRUMENTO").Value
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar la relacion Contraparte/Instrumento.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcRel_Contrapartes_Inst = Nothing
  
  'Carga los traders de la contraparte
  Call Sub_CargarTraders
  
  If Not lID = "" Then
    Grilla_Instrumento.Row = Grilla_Instrumento.FindRow(lID, , Grilla_Instrumento.ColIndex("colum_pk"))
    If Not Grilla_Instrumento.Row = cNewEntidad Then
      Call Grilla_Instrumento.ShowCell(Grilla_Instrumento.Row, Grilla_Instrumento.ColIndex("colum_pk"))
    End If
  End If
    
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function

Private Sub Toolbar_Instrumento_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agregar_Instrumento
    Case "DEL"
      If Grilla_Instrumento.Row > 0 Then
        If MsgBox("�Esta seguro de eliminar el instrumento?", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
          Grilla_Instrumento.RemoveItem (Grilla_Instrumento.Row)
        End If
      End If
  End Select
End Sub

Private Sub Grilla_Instrumento_DblClick()
  Call Sub_EsperaVentana_Instrumento(fKey, "U")
End Sub

Private Sub Sub_Agregar_Instrumento()
  Call Sub_EsperaVentana_Instrumento(fKey, "I")
End Sub

Private Sub Sub_EsperaVentana_Instrumento(ByVal pkey, pTipo As String)
Dim lForm As Frm_Instrumento
Dim lNombre As String
Dim lNom_Form As String
Dim existe_instrumento As Boolean
Dim lFila As Long
  
  Me.Enabled = False
  existe_instrumento = False
  lNom_Form = "Frm_Instrumento"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lNombre = Me.Name
    
    Set lForm = New Frm_Instrumento
    Call lForm.Fnt_Modificar(pId_Contraparte:=pkey _
                           , pCod_Producto:=GetCell(Grilla_Instrumento, Grilla_Instrumento.Row, "colum_pk") _
                           , pCod_Instrumento:=GetCell(Grilla_Instrumento, Grilla_Instrumento.Row, "cod_instrumento") _
                           , pCod_Producto_new:=lCod_Producto_new _
                           , pDsc_Producto_new:=lDsc_Producto_new _
                           , pCod_instrumento_new:=lCod_Instrumento_new _
                           , pDsc_Intrumento_new:=lDsc_Instrumento_new _
                           , pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      If Not lCod_Instrumento_new = "" Then
        If Grilla_Instrumento.Rows > 1 Then
          For lFila = 1 To Grilla_Instrumento.Rows - 1
            If lCod_Instrumento_new = GetCell(Grilla_Instrumento, lFila, "cod_instrumento") Then
              existe_instrumento = True
              Exit For
            End If
          Next
          If Not existe_instrumento Then
            If pTipo = "I" Then
              Grilla_Instrumento.AddItem ""
              Call Sub_Sp_IU_Grilla(Grilla_Instrumento.Rows - 1, lCod_Producto_new, lCod_Instrumento_new, lDsc_Producto_new, lDsc_Instrumento_new)
            ElseIf pTipo = "U" Then
              Call Sub_Sp_IU_Grilla(Grilla_Instrumento.Row, lCod_Producto_new, lCod_Instrumento_new, lDsc_Producto_new, lDsc_Instrumento_new)
            End If
          End If
        Else
          Grilla_Instrumento.AddItem ""
          Call Sub_Sp_IU_Grilla(Grilla_Instrumento.Rows - 1, lCod_Producto_new, lCod_Instrumento_new, lDsc_Producto_new, lDsc_Instrumento_new)
        End If
      End If
    End If
  End If
  Me.Enabled = True
End Sub

Private Sub Sub_Sp_IU_Grilla(lFila As Long, pCod_Producto As String, pCod_Instrumento As String, _
                             pDsc_Producto As String, pDsc_Instrumento As String)

  SetCell Grilla_Instrumento, lFila, "colum_pk", pCod_Producto
  SetCell Grilla_Instrumento, lFila, "cod_instrumento", pCod_Instrumento
  SetCell Grilla_Instrumento, lFila, "dsc_producto", pDsc_Producto
  SetCell Grilla_Instrumento, lFila, "dsc_instrumento", pDsc_Instrumento
End Sub

Private Sub Toolbar_Trader_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_EsperaVentana_Trader(cNewEntidad)
    Case "DEL"
      If Grilla_Traders.Row > 0 Then
        If MsgBox("�Esta seguro de eliminar el Trader?", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
          If Fnt_EliminarTrader(GetCell(Grilla_Traders, Grilla_Traders.Row, "colum_pk")) Then
            Call Sub_CargarTraders
          End If
        End If
      End If
  End Select
End Sub

Private Function Fnt_EliminarTrader(pPK) As Boolean
Dim lcTrader As Class_Traders
  
On Error GoTo ErrProcedure
  
  Fnt_EliminarTrader = False
  
  Set lcTrader = New Class_Traders
  With lcTrader
    .Campo("id_trader").Valor = pPK
    If Not .Borrar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al eliminar el trader.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  
  Fnt_EliminarTrader = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al eliminar el trader.", Err.Description)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Set lcTrader = Nothing
End Function

Private Sub Sub_EsperaVentana_Trader(ByVal pkey)
Dim lForm As Frm_Trader
Dim lNombre As String
Dim lNom_Form As String
Dim existe_instrumento As Boolean
Dim lFila As Long
  
On Error GoTo ErrProcedure
  
  'primero que nada grabo las contrapartes, si el fkey es igual a sin entidad
  If fKey = cNewEntidad Then
    If Not MsgBox("Primero debe grabar antes acceder a esta opci�n." & vbCr & vbCr & _
                  "�Desea grabar?", vbYesNo, Me.Caption) = vbYes Then
      GoTo ExitProcedure
    End If
    
    If Not Fnt_Grabar Then
      GoTo ExitProcedure
    End If
  End If
  
  
  Me.Enabled = False
  existe_instrumento = False
  lNom_Form = "Frm_Trader"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lNombre = Me.Name
    
    Set lForm = New Frm_Trader
    Call lForm.Fnt_Modificar(pId_Trader:=pkey _
                           , pId_Contraparte:=fKey _
                           , pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    Unload lForm
    
    Call Sub_CargarTraders
  End If
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en manejar el Trader.", Err.Description)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Me.Enabled = True
End Sub


Private Sub Sub_CargarTraders()
Dim lcTrader  As Class_Traders
Dim lReg      As hFields
'----------------------------------------------------
Dim lLinea    As Long
Dim lID       As String
  
  If Grilla_Traders.Row > 0 Then
    lID = GetCell(Grilla_Traders, Grilla_Traders.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Grilla_Traders.Rows = 1
  
  Set lcTrader = New Class_Traders
  With lcTrader
    .Campo("ID_CONTRAPARTE").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Traders.Rows
        Grilla_Traders.AddItem ""
        
        SetCell Grilla_Traders, lLinea, "colum_pk", lReg("ID_TRADER").Value
        SetCell Grilla_Traders, lLinea, "dsc_trader", lReg("DSC_TRADER").Value
        SetCell Grilla_Traders, lLinea, "email", lReg("EMAIL_trader").Value
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Traders.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With

  With Grilla_Traders
    If Not lID = "" Then
      .Row = .FindRow(lID, , .ColIndex("colum_pk"))
      If Not .Row = cNewEntidad Then
        Call .ShowCell(.Row, .ColIndex("colum_pk"))
      End If
    End If
  End With
End Sub
