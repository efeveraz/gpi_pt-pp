VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Trader 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Trader"
   ClientHeight    =   1725
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7635
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1725
   ScaleWidth      =   7635
   Begin VB.Frame Frame1 
      Caption         =   "Datos de Trader"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1215
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   7515
      Begin hControl2.hTextLabel Txt_EMail 
         Height          =   315
         Left            =   180
         TabIndex        =   4
         Tag             =   "OBLI=S"
         Top             =   690
         Width           =   5355
         _ExtentX        =   9446
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "E-Mail"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin hControl2.hTextLabel Txt_Nombre 
         Height          =   315
         Left            =   180
         TabIndex        =   3
         Tag             =   "OBLI=S"
         Top             =   330
         Width           =   7155
         _ExtentX        =   12621
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Nombre"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   49
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7635
      _ExtentX        =   13467
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Trader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Dim fId_Contraparte

Dim fFlg_Tipo_Permiso As String

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With
End Sub

Public Function Fnt_Modificar(pId_Trader, pId_Contraparte, pCod_Arbol_Sistema) As Boolean
  fKey = pId_Trader
  fId_Contraparte = pId_Contraparte
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Call Sub_CargarDatos
    
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso Trader"
  Else
    Me.Caption = "Modificaci�n Trader: " & Txt_Nombre.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lReg As hCollection.hFields
Dim lcTrader As Class_Traders
  
  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If

  Set lcTrader = New Class_Traders
  With lcTrader
    .Campo("Id_trader").Valor = fKey
    .Campo("Id_contraparte").Valor = fId_Contraparte
    .Campo("dsc_trader").Valor = Txt_Nombre.Text
    .Campo("email_TRADER").Valor = Txt_EMail.Text
    
    If Not .Guardar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al grabar el Trader.", _
                        .ErrMsg, _
                        pConLog:=True)
      Fnt_Grabar = False
    End If
    
    fKey = NVL(.Campo("id_trader").Valor, -1)
  End With
  Set lcTrader = Nothing
End Function

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lcTrader As Class_Traders

  Load Me
  
  Set lcTrader = New Class_Traders
  With lcTrader
    .Campo("id_trader").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        Txt_Nombre.Text = lReg("DSC_TRADER").Value
        Txt_EMail.Text = lReg("email_TRADER").Value
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al buscar el trader.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcTrader = Nothing
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = False
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ExitProcedure
  End If
  
    If Txt_EMail.Text <> "" Then
        If Not Validar_Email(Txt_EMail.Text) Then
            MsgBox "La direccion de correo no es valida", vbInformation, Me.Caption
            Txt_EMail.SetFocus
            GoTo ExitProcedure
        End If
    End If
  
  Fnt_ValidarDatos = True
ExitProcedure:

End Function

Public Function Validar_Email(ByVal Email As String) As Boolean
    
    Dim i As Integer, iLen As Integer, caracter As String
    Dim pos As Integer, bp As Boolean, ipos As Integer, ipos2 As Integer

    On Local Error GoTo Err_Sub

    Email = Trim$(Email)

    If Email = vbNullString Then
        Exit Function
    End If

    Email = LCase$(Email)
    iLen = Len(Email)

    
    For i = 1 To iLen
        caracter = Mid(Email, i, 1)

        If (Not (caracter Like "[a-z]")) And (Not (caracter Like "[0-9]")) Then
            
            If InStr(1, "_-" & "." & "@", caracter) > 0 Then
                If bp = True Then
                   Exit Function
                Else
                    bp = True
                   
                    If i = 1 Or i = iLen Then
                        Exit Function
                    End If
                    
                    If caracter = "@" Then
                        If ipos = 0 Then
                            ipos = i
                        Else
                            
                            Exit Function
                        End If
                    End If
                    If caracter = "." Then
                        ipos2 = i
                    End If
                    
                End If
            Else
                
                Exit Function
            End If
        Else
            bp = False
        End If
    Next i
    If ipos = 0 Or ipos2 = 0 Then
        Exit Function
    End If
    
    If ipos2 < ipos Then
        Exit Function
    End If

    
    Validar_Email = True

    Exit Function
Err_Sub:
    On Local Error Resume Next
    
    Validar_Email = False
End Function


Private Sub Txt_EMail_LostFocus()

'    If Txt_EMail.Text <> "" Then
'        If Not Validar_Email(Txt_EMail.Text) Then
'            MsgBox "La direccion de correo no es valida", vbInformation, Me.Caption
'            Txt_EMail.SetFocus
'            Exit Sub
'        End If
'    End If
    
End Sub
