VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Cajas_Cuenta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Caja de la Cuenta"
   ClientHeight    =   2550
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6930
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2550
   ScaleWidth      =   6930
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   6930
      _ExtentX        =   12224
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   4
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin C1SizerLibCtl.C1Tab STab 
      Height          =   2115
      Left            =   30
      TabIndex        =   5
      Top             =   390
      Width           =   6855
      _cx             =   12091
      _cy             =   3731
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   6579300
      ForeColor       =   -2147483634
      FrontTabColor   =   -2147483635
      BackTabColor    =   6579300
      TabOutlineColor =   -2147483632
      FrontTabForeColor=   -2147483634
      Caption         =   "&General|&Alias"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   6
      Position        =   0
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   0   'False
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   -1  'True
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   0
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   -1  'True
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.PictureBox TabAlias 
         BorderStyle     =   0  'None
         Height          =   1770
         Left            =   7470
         ScaleHeight     =   1770
         ScaleWidth      =   6825
         TabIndex        =   10
         Top             =   330
         Width           =   6825
      End
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   1770
         Left            =   15
         ScaleHeight     =   1770
         ScaleWidth      =   6825
         TabIndex        =   6
         Top             =   330
         Width           =   6825
         Begin VB.Frame Frame1 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   1785
            Left            =   60
            TabIndex        =   7
            Top             =   -60
            Width           =   6705
            Begin hControl2.hTextLabel Txt_Dsc_Caja_Cuenta 
               Height          =   315
               Left            =   240
               TabIndex        =   0
               Tag             =   "OBLI"
               Top             =   360
               Width           =   6210
               _ExtentX        =   10954
               _ExtentY        =   556
               LabelWidth      =   1300
               TextMinWidth    =   1200
               Caption         =   "Descripci�n"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               MaxLength       =   59
            End
            Begin TrueDBList80.TDBCombo Cmb_Mercado 
               Height          =   345
               Left            =   1560
               TabIndex        =   2
               Tag             =   "OBLI=S;CAPTION=Mercado"
               Top             =   1110
               Width           =   4905
               _ExtentX        =   8652
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   0   'False
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Cajas_Cuenta.frx":0000
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_Moneda 
               Height          =   345
               Left            =   1560
               TabIndex        =   1
               Tag             =   "OBLI=S;CAPTION=Pa�s"
               Top             =   720
               Width           =   4905
               _ExtentX        =   8652
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   0   'False
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Cajas_Cuenta.frx":00AA
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin VB.Label Label3 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Moneda"
               Height          =   345
               Left            =   240
               TabIndex        =   9
               Top             =   720
               Width           =   1305
            End
            Begin VB.Label Label1 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Mercado"
               Height          =   345
               Left            =   240
               TabIndex        =   8
               Top             =   1110
               Width           =   1290
            End
         End
      End
   End
End
Attribute VB_Name = "Frm_Cajas_Cuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Dim fId_Cuenta

Dim fFormAlias As Object

Dim fFlg_Tipo_Permiso As String

Private Sub Cmb_Mercado_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  '-----------------------------------------------------------
  '-- Alias
  '-----------------------------------------------------------
  Set fFormAlias = Proce.CargaFormularioAlias
  fFormAlias.CodigoCSBPI = cTabla_Cajas_Cuenta
  Call Proce.AnexaFormulario(fFormAlias, Me.TabAlias)
  '-----------------------------------------------------------

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pId_Caja_Cuenta _
                            , pId_Cuenta _
                            , pCod_Arbol_Sistema)
  fKey = pId_Caja_Cuenta
  fId_Cuenta = pId_Cuenta
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Call Sub_CargarDatos
  
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso Caja Cuenta"
  Else
    Me.Caption = "Caja Cuenta: " & Txt_Dsc_Caja_Cuenta.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Unload Me
      End If
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
    Case "EXIT"
      Unload Me
      
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
'------------------------------------------
Dim lId_Moneda
Dim lCod_Mercado

  Fnt_Grabar = False

  If Not Fnt_ValidarDatos Then
    Exit Function
  End If

  lId_Moneda = Fnt_ComboSelected_KEY(Cmb_Moneda)
  lCod_Mercado = Fnt_ComboSelected_KEY(Cmb_Mercado)

  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_Caja_Cuenta").Valor = fKey
    .Campo("id_Cuenta").Valor = fId_Cuenta
    .Campo("id_Moneda").Valor = lId_Moneda
    .Campo("cod_Mercado").Valor = lCod_Mercado
    .Campo("dsc_Caja_Cuenta").Valor = Txt_Dsc_Caja_Cuenta.Text
    If Not .Guardar Then
       Call Fnt_MsgError(.SubTipo_LOG, _
                "Problemas en grabar las Cajas de la Cuenta.", _
                .ErrMsg, _
                pConLog:=True)
       GoTo ExitProcedure
    End If
  
    'Actualiza la columna con el ID
    fKey = .Campo("id_Caja_Cuenta").Valor
  End With
  Set lcCaja_Cuenta = Nothing
  
  '-----------------------------------------------------------
  '-- Alias
  '-----------------------------------------------------------
  fFormAlias.CodigoEntidadCSBPI = fKey
  If Not fFormAlias.Grabar Then
    GoTo ExitProcedure
  End If
  '-----------------------------------------------------------
  
  Fnt_Grabar = True
  
ExitProcedure:

End Function

Private Sub Sub_CargaForm()

  Call Sub_FormControl_Color(Me.Controls)
  Call Sub_CargaCombo_Monedas(Cmb_Moneda, pFlg_Es_Moneda_Pago:=gcFlg_SI)
  Call Sub_CargaCombo_Mercados(Cmb_Mercado)

  '-----------------------------------------------------------
  '-- Alias
  '-----------------------------------------------------------
  If Not fFormAlias.CargarSystem Then
    'no se que se hace aqui -.-U
  End If
  '-----------------------------------------------------------
End Sub

Private Sub Sub_CargarDatos()
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lReg As hCollection.hFields
'---------------------------------------------
  
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_caja_cuenta").Valor = fKey
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Bancos.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
    
    For Each lReg In .Cursor
      Txt_Dsc_Caja_Cuenta.Text = "" & lReg("dsc_caja_cuenta").Value
      Call Sub_ComboSelectedItem(Cmb_Moneda, "" & lReg("id_moneda").Value)
      Call Sub_ComboSelectedItem(Cmb_Mercado, "" & lReg("cod_mercado").Value)
    Next
  End With
  
  '-----------------------------------------------------------
  '-- Alias
  '-----------------------------------------------------------
  fFormAlias.CodigoEntidadCSBPI = fKey
  If Not fFormAlias.CargarCSBPI Then
    'no se que se hace aqui -.-U
  End If
  '-----------------------------------------------------------
  
ExitProcedure:
  Set lcCaja_Cuenta = Nothing
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function
