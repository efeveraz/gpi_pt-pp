VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Object = "{0F0877EF-2A93-4AE6-8BA8-4129832C32C3}#230.0#0"; "SmartMenuXP.ocx"
Begin VB.MDIForm MDI_Principal 
   BackColor       =   &H00FFFFFF&
   Caption         =   "CSGPI"
   ClientHeight    =   8505
   ClientLeft      =   7050
   ClientTop       =   5460
   ClientWidth     =   11880
   Icon            =   "MDI_Principal.frx":0000
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   Picture         =   "MDI_Principal.frx":08CA
   WindowState     =   2  'Maximized
   Begin MSComDlg.CommonDialog CommonDialog 
      Left            =   6540
      Top             =   4020
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSMAPI.MAPIMessages MAPIMessages 
      Left            =   4590
      Top             =   1410
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
   Begin MSMAPI.MAPISession MAPISession 
      Left            =   5940
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DownloadMail    =   0   'False
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin MSComctlLib.ImageList ImgClocks 
      Left            =   8490
      Top             =   570
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   20
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":636A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":64CB
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":6626
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":6784
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":68E3
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":6A41
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":6B99
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Frame_Menu 
      Align           =   3  'Align Left
      BackColor       =   &H00C0FFC0&
      Height          =   7815
      Left            =   0
      ScaleHeight     =   7755
      ScaleWidth      =   1935
      TabIndex        =   4
      Top             =   375
      Visible         =   0   'False
      Width           =   1995
      Begin MSComctlLib.TreeView Tree_Menu 
         Height          =   8610
         Left            =   0
         TabIndex        =   0
         Top             =   0
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   15187
         _Version        =   393217
         Indentation     =   265
         LabelEdit       =   1
         Style           =   7
         Appearance      =   1
         MousePointer    =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MouseIcon       =   "MDI_Principal.frx":6CF3
      End
      Begin VB.Label LblMov 
         BackColor       =   &H00000000&
         Height          =   375
         Left            =   1710
         TabIndex        =   5
         Top             =   1260
         Width           =   195
      End
   End
   Begin VB.PictureBox PictureBox 
      Align           =   4  'Align Right
      BorderStyle     =   0  'None
      Height          =   7815
      Left            =   10875
      ScaleHeight     =   7815
      ScaleWidth      =   1005
      TabIndex        =   2
      Top             =   375
      Visible         =   0   'False
      Width           =   1005
      Begin VB.Timer Timer_Pos 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   180
         Top             =   30
      End
      Begin VB.VScrollBar VScroll 
         Height          =   2355
         Left            =   480
         TabIndex        =   3
         Top             =   2460
         Width           =   240
      End
      Begin VB.Image Imagen 
         Height          =   615
         Left            =   120
         Top             =   1410
         Width           =   585
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   8190
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15408
            MinWidth        =   882
            Key             =   "TEXTO"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1032
            MinWidth        =   529
            Text            =   "Fecha:"
            TextSave        =   "Fecha:"
            Key             =   "FECHASERVIDOR"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1323
            MinWidth        =   882
            Text            =   "Empresa:"
            TextSave        =   "Empresa:"
            Key             =   "EMPRESA"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            AutoSize        =   2
            Object.Width           =   1058
            MinWidth        =   1058
            Key             =   "CONNECT"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   767
            MinWidth        =   776
            Key             =   "SESSIONID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   653
            MinWidth        =   653
            Picture         =   "MDI_Principal.frx":728D
            Key             =   "STATUSDB"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageListGlobal16 
      Left            =   3060
      Top             =   1050
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   42
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":73E5
            Key             =   "Equis"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":7CBF
            Key             =   "boton_salir"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":8259
            Key             =   "boton_cancelar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":87F3
            Key             =   "boton_aceptar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":8D8D
            Key             =   "boton_imprimir"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":9327
            Key             =   "boton_grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":98C1
            Key             =   "boton_seguir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":A59B
            Key             =   "globo_victo"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":A8B5
            Key             =   "precaucion"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":ABCF
            Key             =   "globo_equis"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":AEE9
            Key             =   "boton_grilla_buscar"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":B33B
            Key             =   "boton_agregar_grilla"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":B495
            Key             =   "boton_eliminar_grilla"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":BA2F
            Key             =   "open_folder"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":BFC9
            Key             =   "close_folder"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":C563
            Key             =   "document"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":CAFD
            Key             =   "boton_original"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":D097
            Key             =   "boton_modificar"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":D631
            Key             =   "boton_refrescar"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":DBCB
            Key             =   "flecha_up"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":DEE5
            Key             =   "flacha_down"
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":E1FF
            Key             =   "boton_seleccionar_todos"
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":E799
            Key             =   "boton_seleccionar_ninguno"
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":ED33
            Key             =   "form_grilla"
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":F2CD
            Key             =   "form_mantenedor"
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":F867
            Key             =   "boton_valorizar"
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":F9C1
            Key             =   "boton_excel"
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":FA23
            Key             =   "boton_pdf"
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":FAAC
            Key             =   "boton_calcular"
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":FC06
            Key             =   "boton_conectar"
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":10058
            Key             =   "boton_conciliar"
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":105F2
            Key             =   "boton_hacia_izquierda"
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":10A44
            Key             =   "boton_hacia_derecha"
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":10E96
            Key             =   ""
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":112E8
            Key             =   ""
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":1173A
            Key             =   "grilla_warning"
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":11CD4
            Key             =   "grilla_error"
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":1226E
            Key             =   "boton_maxwindow"
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":12808
            Key             =   "boton_restorewindow"
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":12DA2
            Key             =   "boton_buscar"
         EndProperty
         BeginProperty ListImage41 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":131F4
            Key             =   "boton_mail"
         EndProperty
         BeginProperty ListImage42 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":1350E
            Key             =   "boton_paste"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImgDB 
      Left            =   8280
      Top             =   2070
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   20
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":13620
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI_Principal.frx":13A6C
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VBSmartXPMenu.SmartMenuXP menu_persiana 
      Align           =   1  'Align Top
      Height          =   375
      Left            =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   661
      BackColor       =   16761024
      SelBackColor    =   12648384
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "MDI_Principal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Sub AnalizaHijos(ByVal iDPadre As Long, ByRef bVisible As Boolean)
    Dim iHijo       As Long
    Dim lIndexHijo  As Integer
    
    If menu_persiana.MenuItems.ChildCount(iDPadre) = 0 Then
        menu_persiana.MenuItems.Visible(iDPadre) = False
    Else
        For iHijo = 1 To menu_persiana.MenuItems.ChildCount(iDPadre)
            lIndexHijo = menu_persiana.MenuItems.ChildID(iDPadre, iHijo)
            
            If menu_persiana.MenuItems.ChildCount(lIndexHijo) <> 0 Then
                AnalizaHijos lIndexHijo, bVisible
            Else
                If Not BuscarPermisos(menu_persiana.MenuItems.Key(lIndexHijo)) Then
                    menu_persiana.MenuItems.Visible(lIndexHijo) = False
                Else
                    bVisible = True
                End If
            End If
        Next
    End If
End Sub

Private Function BuscarPermisos(pCod_Arbol_Sistema) As Boolean
    Dim lcRel_Roles_Arbol_Sistema As New Class_Rel_Roles_Arbol_Sistema
    '-----------------------------------------------------
    BuscarPermisos = False
    
    ' Set lcRel_Roles_Arbol_Sistema = New Class_Rel_Roles_Arbol_Sistema
    
    With lcRel_Roles_Arbol_Sistema
        If Not .Buscar_Permiso(pCod_Arbol_Sistema:=pCod_Arbol_Sistema, pId_Usuario:=gID_Usuario) Then
            BuscarPermisos = False
        End If
    
        If .Cursor.Count <= 0 Then
            BuscarPermisos = False
        Else
            BuscarPermisos = True
        End If
  End With
  Set lcRel_Roles_Arbol_Sistema = Nothing
End Function

Sub Elije_Menu(ByVal pOpcion_Menu As String)
  
  Select Case pOpcion_Menu
    Case "OPC_REPO_RENT_HIST"
      Call Frm_Reporte_Rentabilidades_Historicas.Mostrar(pOpcion_Menu)
    Case "OPC_COMISIONES_CUENTA_FIJA"
      Call Frm_Man_Comisiones_Cuenta_Fija.Mostrar(pOpcion_Menu)
    Case "OPC_TRASPASO_ASESOR2CUENTA"
      Call Frm_Transpaso_Asesor_Cuenta.Mostrar(pOpcion_Menu)
    Case "OPC_COBRO_COMI_HONO_ASE"
      Call Frm_Cobro_Comi_Hono_Ase.Mostrar(pOpcion_Menu)
    Case "OPC_INGRESO_MENSUAL_CUENTA"
      Call Frm_Man_Ingresos_Mensual_Cuenta.Mostrar(pOpcion_Menu)
    Case "OPC_INGRESO_MENSUAL_CONCEPTO"
      Call Frm_Man_Ingresos_Mensual_Conceptos.Mostrar(pOpcion_Menu)
    Case "OPC_EMPRESA"
      Call Frm_Empresa.Mostrar(pOpcion_Menu)
    Case "OPC_CLIENTE"
      Call Frm_Man_Clientes.Mostrar(pOpcion_Menu)
    Case "OPC_IMPORTADORGPIGPI"
      Call Frm_Importador_GPIGPI.Mostrar(pOpcion_Menu)
    Rem cgarcia 12/01/2010 Nuevo mantenedor de Clientes
    Case "OPC_MAN_CLIENTE"
      Call Frm_Mantenedor_Clientes.Show
    Case "OPC_CAMBIO_EMPRESA"
      '  Call Sub_CierraSesion
      'Call Frm_Login_Sistema.Fnt_Mostrar(True)
      Call Frm_Login_Sistema.Fnt_Mostrar_Cambio_Empresa
      'Fnt_Mostrar_Cambio_Empresa
    Rem --- Menu Operaciones ------------------------------------------------------------
    Case "OPC_OPERACION_DIRECTA"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Directa, pOper_Fecha_Anterior:=False)
    Case "OPC_OPERACION_INSTRUCCION"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Instruccion, pOper_Fecha_Anterior:=False)
    Case "OPC_OPERACION_INSTRUCCION_INTERNACIONAL"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Instruccion, pOper_Fecha_Anterior:=False, pInternacional:=True)
    Case "OPC_OPERACION_DIRECTA_INTERNACIONAL"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Directa, pOper_Fecha_Anterior:=False, pInternacional:=True)
    Case "OPC_OPERACION_CUSTODIA_INTERNACIONAL"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Custodia, pOper_Fecha_Anterior:=False, pInternacional:=True)
      
    Case "OPC_OPERACION_INSTRUCCION_INTERNACIONAL_FECHA_ANT"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Instruccion, pOper_Fecha_Anterior:=True, pInternacional:=True)
    Case "OPC_OPERACION_DIRECTA_INTERNACIONAL_FECHA_ANT"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Directa, pOper_Fecha_Anterior:=True, pInternacional:=True)
    Case "OPC_OPERACION_CUSTODIA_INTERNACIONAL_FECHA_ANT"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Custodia, pOper_Fecha_Anterior:=True, pInternacional:=True)
            
    Case "OPC_OPERACION_CUSTODIA"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Custodia, pOper_Fecha_Anterior:=False)
    Case "OPC_OPERACION_SORTEO_LETRAS"
      Call Frm_Operacion_Sorteo_Letras.Mostrar(pOpcion_Menu)
    Rem ------------------------------------------------------------------------------------
    '15/07/2009 Agregado por MMardones Ingreso Acciones por Monto
    Case "OPC_OPERACION_DIRECTA_XMONTO"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Directa, pOper_Fecha_Anterior:=False, pPorMonto:=True)
    Case "OPC_OPERACION_INSTRUCCION_XMONTO"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Instruccion, pOper_Fecha_Anterior:=False, pPorMonto:=True)
    Case "OPC_OPERACION_DIRECTA_FECHA_ANT_XMONTO"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Directa, pOper_Fecha_Anterior:=True, pPorMonto:=True)
    Case "OPC_OPERACION_INSTRUCCION_FECHA_ANT_XMONTO"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Instruccion, pOper_Fecha_Anterior:=True, pPorMonto:=True)
    Rem ------------------------------------------------------------------------------------
    Rem --- Menu Operaciones Fechas Anteriores -- solo para Menu BackOffice ----------------
    Case "OPC_OPERACION_DIRECTA_FECHA_ANTERIOR"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Directa, pOper_Fecha_Anterior:=True)
    Case "OPC_OPERACION_INSTRUCCION_FECHA_ANTERIOR"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Instruccion, pOper_Fecha_Anterior:=True)
    Case "OPC_OPERACION_CUSTODIA_FECHA_ANTERIOR"
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Custodia, pOper_Fecha_Anterior:=True)
    Rem  --------------------------------------------------------------------------------
    Case "OPC_CONTRAPARTE"
      Call Frm_Man_Contrapartes.Mostrar(pOpcion_Menu)
    Case "OPC_NEMOTECNICO"
      Call Frm_Man_Nemotecnicos.Mostrar(pOpcion_Menu)
    Case "OPC_APORTE_RESCATE"
      Call Frm_Man_AporteRescate.Mostrar(pOpcion_Menu)
    Rem ------ Aporte/Rescate Fechas Anteriores. Solo Menu Backoffice -----------------------
    Case "OPC_APORTE_RESCATE_FECHAS_ANTERIORES"
      Call Frm_Man_AporteRescate.Mostrar(pOpcion_Menu, True)
    Rem -------------------------------------------------------------------------------------
    Case "OPC_CUENTA"
      Call Frm_Man_Cuentas.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_BANCOS"
      Call Frm_Man_Banco.Mostrar(pOpcion_Menu)
    Case "OPC_MANTENCION_IVA"
      Call Frm_Man_Iva.Mostrar(pOpcion_Menu)
    Case "OPC_MANTENCION_DIVIDENDOS"
      Call Frm_Man_Dividendos.Mostrar(pOpcion_Menu)
    Case "OPC_MANTENCION_FERIADOS"
      Call Frm_Man_Feriados.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_Restriccion_Compra"
      Call Frm_Man_Restriccion_Compra.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_Restriccion_Venta"
      Call Frm_Man_Restriccion_Venta.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_COMISIONES_INSTRUMENTO"
      Call Frm_Man_Comisiones_Instrumento_Cuentas.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_EMISORES_GENERALES"
      Call Frm_Man_Emisores_Generales.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_EMISORES_ESPECIFICO"
      Call Frm_Man_Emisores_Especifico.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_ASESORES"
      Call Frm_Man_Asesores.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_GRUPOS_CUENTAS"
      Call Frm_Man_Grupos_Cuentas.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_MERCADOS_TRANSACCION"  'ref
      Call Frm_Man_Mercados_Transaccion.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_CLASIFICACION_RIESGO"
      Call Frm_Man_ClasificadoresRiesgo.Mostrar(pOpcion_Menu)
    Case "OPC_PAISES"
      Call Frm_Man_Paises.Mostrar(pOpcion_Menu)
      
    Case "OPC_PUBLICADORES"
      Call Frm_Man_Publicadores.Mostrar(pOpcion_Menu)
    Case "OPC_ROLES"
      Call Frm_Man_Roles.Mostrar(pOpcion_Menu)
    Case "OPC_USUARIOS"
      Call Frm_Man_Usuarios.Mostrar(pOpcion_Menu)
    Case "OPC_PUBLICADORES_PRECIO"
      Call Frm_Man_Publicadores_Precio.Mostrar(pOpcion_Menu)
    Case "OPC_SECTORES"
      Call Frm_Man_Sectores.Mostrar(pOpcion_Menu)
    Case "OPC_PROC_LIQUIDACIONES"
      Call Frm_Proc_Liquidacion.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_COMISIONES_CUENTAS_PRUEBA"
      Call Frm_Man_Comisiones_Cuentas.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_PERFILES_RIESGO"
      Call Frm_Man_Perfiles_Riesgo.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_MONEDAS"
      Call Frm_Man_Monedas.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_INSTRUMENTOS"
      Call Frm_Man_Instrumentos.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_TIPO_CAMBIOS"
      Call Frm_Man_Tipo_Cambios.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_REMESAS"
      Call Frm_Man_Remesas.Mostrar(pOpcion_Menu)
    Rem ------ Remesa Fechas Anteriores. Solo Menu Backoffice -----------------------
    Case "OPC_MAN_REMESAS_FECHAS_ANTERIORES"
      Call Frm_Man_Remesas.Mostrar(pOpcion_Menu, True)
    '--------------------------------------------------------------------------------
    Case "OPC_MAN_VALOR_TIPO_CAMBIOS"
      Call Frm_Man_Valor_Tipo_Cambios.Mostrar(pOpcion_Menu)
    Case "OPC_ASIGNACION_PERFILES_NEMOTECNICOS"
      Call Frm_AsignaPerfilProductos.Mostrar(pOpcion_Menu)
    Case "OPC_ASIGNACION_NEMOTECNICOS_PERFILES"
      Call Frm_AsignaNemotecnicoPerfil.Mostrar(pOpcion_Menu)
    Case "OPC_MAN_CARGOS_ABONOS"
      Call Frm_Man_Cargos_Abonos.Mostrar(pOpcion_Menu)
    
    Rem ------ ASignaci�n de clases a nemot�cnicos. Agregado por MMA 18/08/2008
    Case "OPC_ASIGNA_ACTIVO_NEMOTECNICO"
      Call Frm_AsignaActivosANemotecnico.Mostrar(pOpcion_Menu)
    Case "OPC_ASIGNA_ACTIVO_NEMOTECNICO_II"
      Call Frm_AsignaActivosANemotecnico_II.Mostrar(pOpcion_Menu)
      
    Rem ------ Cargos/Abonos Fechas Anteriores. Solo Menu Backoffice -----------------------
    Case "OPC_MAN_CARGOS_ABONOS_FECHAS_ANTERIORES"
      Call Frm_Man_Cargos_Abonos.Mostrar(pOpcion_Menu, True)
    Rem ------------------------------------------------------------------------------------
    Case "OPC_CHECKLIST_CLIENTE"
      Call Frm_Man_Checklist_Cliente.Mostrar(pOpcion_Menu)
    Case "OPC_TIPO_LIQUIDACION"
      Call Frm_Man_Tipos_Liquidacion.Mostrar(pOpcion_Menu)
    Case "OPC_PORCENTAJE_PERFILES"
      Call Frm_Man_Porcentajes_Perfiles.Mostrar(pOpcion_Menu)
    Case "OPC_CHECKLIST_ESTADO_CLIENTES"
      Call Frm_Checklist_Cliente_Por_Estados.Show
    Case "OPC_VALORIZA_BONO_REC"
       Frm_Valoracion_BonosRec.Show
    Case "OPC_VALORIZA_INST_INTER"
       Frm_Valoracion_Intermediacion.Show
    Case "OPC_CONFIRMACION_INSTRUCCIONES"
       Frm_Confirmacion_Instrucciones.Show
    Case "OPC_CIERREDIA"
       Frm_Proceso_Cierre.Show
    Case "OPC_CIERRE_PENDIENTE"
       Frm_Proceso_Cierre_Pendientes.Show
    Case "OPC_RE_CIERREDIA"
       Frm_Re_Proceso_Cierre.Show
    Case "OPC_VISOR_SALTO_CUOTA"
       Frm_Visor_Salto_Cuota.Show
    Case "OPC_VISOR_CARTOLA"
 '       Frm_Visor_Cartola_Movimientos_Caja.Show
       Call Frm_Visor_Cartola_Movimientos_Caja.Mostrar(pOpcion_Menu)
    Case "OPC_TRASPASO_NEMOTECNICO"
      Frm_Traspaso_Nemotecnico.Show
    Case "OPC_EVENTOS_CAPITAL"
      Frm_Eventos_Capital.Show
    Case "OPC_REPORTE_ACTIVOS_CUENTAS"
      Frm_Reportes_Activos_Ctas.Show
    Case "OPC_REPORTE_INSTRUCCIONES"
      Frm_Reporte_Instrucciones.Show
    Case "OPC_REPORTE_CLIENTES"
      Frm_Reporte_Clientes.Show
    Case "OPC_REPORTE_CUENTAS"
      Frm_Reporte_Cuentas.Show
    Case "OPC_REPORTE_CUENTAS_C"
      Frm_Reporte_Cuentas_C.Show
    Case "OPC_REPORTE_PRODUCTOS_ACTIVOS"
      Frm_Reporte_Productos_Activos.Show
    Case "OPC_REPORTE_CARGOS_ABONOS"
      Frm_Reporte_Cargos_Abonos.Show
    Case "OPC_REPORTE_DIVIDENDOS"
      Frm_Reporte_Dividendos.Show
    Case "OPC_VALORIZADOR_RF"
      Frm_Valorizador_RF.Show
    Case "OPC_SUCURSALES_EJECUTIVOS"
      Frm_Sucursales_Ejecutivos.Show
    Case "OPC_CUOTA_MULTI"
      Frm_RentabilidadMultiCuota.Show
    Case "OPC_CONCILIADOR_FFMM_LOCAL"
      Call Frm_Conciliador.Fnt_Mostrar("CONCILIADOR_FFMM_LOCAL")
    Case "OPC_REPORTE_INFORME_CONTROL_GESTION"
      Call Frm_Informe_Control_Gestion.Show
    Case "OPC_CONCILIADOR_RV_LOCAL"
      Call Frm_Conciliador.Fnt_Mostrar("CONCILIADOR_RV_LOCAL")
    Case "OPC_CONCILIADOR_RF_LOCAL"
      Call Frm_Conciliador.Fnt_Mostrar("CONCILIADOR_RF_LOCAL")
    Case "OPC_CONCILIADOR_CAJA"                 'jgr 05032010
      Call Frm_Conciliador_Cajas.Fnt_Mostrar("CONCILIADOR_CAJA")
    Case "OPC_CONCILIADOR_SIMUL"
      'Call Frm_Conciliador_Simul.Fnt_Mostrar("CONCILIADOR_SMLT_LOCAL")
    Case "OPC_CONSULTA_OPERACIONES"
'      Call Frm_Consulta_Operaciones.Show     === Modificado por MMA 29/07/08
      Call Frm_Consulta_Operaciones.Mostrar(pOpcion_Menu)
    Case "OPC_CONSULTA_OPERACIONES_CON_DETALLE"
        '=== Agregado por Daniel Ordenes 16/12/2009
      Call Frm_Consulta_Operaciones_Con_Detalle.Mostrar(pOpcion_Menu)
    Case "OPC_CONFIRMACION_INSTR_PEND_FFMM"
      Call Frm_Confirmacion_Instr_Pend_FFMM.Show
    Case "OPC_CARGASAUTOMATICAS"
      Frm_CargasAutomaticas.fProcedimiento = "PKG_CARGA_VALORES_MONEDAS_DESDE_SECURITY"
      Frm_CargasAutomaticas.fOrigen = "ALEF-DATA"
      Call Frm_CargasAutomaticas.Show
    Case "OPC_CARGASAUTOMATICAS_CDELSUR"
      Frm_CargasAutomaticas.fProcedimiento = "PKG_CARGA_PARAMETROS_FINANCIEROS_LEASIVDB"
      Frm_CargasAutomaticas.fOrigen = "BAC - SONDA"
      Call Frm_CargasAutomaticas.Show
       
    Rem ---- Pantallas Confirmacion por Contraparte BBVA -----------------------------------------
    Case "OPC_CONFIRMACION_CONTRAPARTE_FFMM"
      Call Frm_Confirmacion_Contraparte_FFMM.Fnt_Mostrar("CONFIRMACION_X_CONTRAPARTE_FFMM")
    Case "OPC_CONFIRMACION_CONTRAPARTE_RV"
      Call Frm_Confirmacion_Contraparte_RV.Fnt_Mostrar("CONFIRMACION_X_CONTRAPARTE_RV")
    Case "OPC_CONFIRMACION_CONTRAPARTE_RF"
      Call Frm_Confirmacion_Contraparte_RF.Fnt_Mostrar("CONFIRMACION_X_CONTRAPARTE_RF")
    Rem ---- Pantallas Movimientos Custodia BBVA ---------------------------------------------------------------
    Case "OPC_CARGA_MOV_CUST_RV"
      Call Frm_Oper_Mov_Cust_RV.Fnt_Mostrar("CARGA_MOV_CUST_RV")
    Case "OPC_CARGA_MOV_CUST_RF"
      Call Frm_Oper_Mov_Cust_RF.Fnt_Mostrar("CARGA_MOV_CUST_RF")
    Rem ---- Pantallas Saldos Custodia BBVA ---------------------------------------------------------------
    Case "OPC_CARGA_SALDOS_CUST_FM"
      Call Frm_Oper_Saldos_Cust_FM.Fnt_Mostrar("CARGA_SALDOS_CUST_FM")
    Case "OPC_CARGA_SALDOS_CUST_RV"
      Call Frm_Oper_Saldos_Cust_RV.Fnt_Mostrar("CARGA_SALDOS_CUST_RV")
    Case "OPC_CARGA_SALDOS_CUST_RF"
      Call Frm_Oper_Saldos_Cust_RF.Fnt_Mostrar("CARGA_SALDOS_CUST_RF")
    Rem ---- Pantallas Movimientos de Caja BBVA ---------------------------------------------------------------
    Case "OPC_CARGA_MOV_CAJA_BBVA"
      Call Frm_Carga_Mov_Caja_Automatica_BBVA.Fnt_Mostrar("CARGA_MOV_CAJA")
    Rem ---- Pantallas Confimacion por Contraparte Security --------------------------------------
    Case "OPC_CONFIRMACION_CONTRAPARTE_FFMM_SEC"
      Call Frm_Confirmacion_Contraparte_FFMM_Sec.Fnt_Mostrar("CONFIRMACION_X_CONTRAPARTE_FFMM")
    Case "OPC_CONFIRMACION_CONTRAPARTE_RV_SEC"
      Call Frm_Confirmacion_Contraparte_RV_Sec.Fnt_Mostrar("CONFIRMACION_X_CONTRAPARTE_RV")
    Case "OPC_CONFIRMACION_CONTRAPARTE_RF_SEC"
      Call Frm_Confirmacion_Contraparte_RF_Sec.Fnt_Mostrar("CONFIRMACION_X_CONTRAPARTE_RF")
    Rem ------------------------------------------------------------------------------------------
    Case "OPC_REPORTE_CARTOLA_CLIENTE_SECURITY"
'      Frm_Reporte_Cartola_Clientes_Security.Show
       Frm_Reporte_Cartola_Clientes_Security.Mostrar (pOpcion_Menu)
    Rem -------Agregado por MMA, consolidado por cliente y por grupo -----------------------------
    Case "OPC_REPORTE_CARTOLA_SECURITY_POR_CLIENTE"
      Frm_Reporte_Cartola_Security_Consolidado.Mostrar ("CLT")
    Case "OPC_REPORTE_CARTOLA_SECURITY_POR_GRUPO"
      Frm_Reporte_Cartola_Security_Consolidado.Mostrar ("GRP")
    Rem ------------------------------------------------------------------------------------------
    Case "OPC_REPORTE_CARTOLA_CLIENTE_BBVA"
      Frm_Reporte_Cartola_Clientes_BBVA.Show
    Case "OPC_REPORTE_CARTOLA_CLIENTE"
      Frm_Reporte_Cartola_Clientes.Show
    Case "OPC_REPORTE_BOLETA_OPERACION"
      'Call Frm_Reporte_Boleta_Operacion.Show
      Call Fnt_Form_BoletaOperacion
    Case "OPC_MIGRACION"
      Call Frm_Migracion.Show
    Case "OPC_REPORTE_APORTE_RESCATE_CAPITAL"
      Call Frm_Reporte_Aporte_Retiro_Capital.Show
    Case "OPC_MIGRADOR_SECUTITY"
      Call Frm_Migrador_SECUTITY.Show
    Case "OPC_CIERRE_MASIVO"
      Call Frm_Proceso_Cierre_Masivo.Show
    Case "OPC_REPORTE_CAJA_SALDO"
      Call Frm_Reporte_Cajas_Saldos.Show
    Case "OPC_REPORTE_ULTIMOS_CIERRES"
      Call Frm_Consulta_Ultimos_Cierres.Show
    Case "OPC_VALORIZA_RENTA_FIJA"
      Frm_Val_Renta_fija.Show
    Case "OPC_MOVIMIENTOS_SIN_CIERRE"
      Frm_Reporte_Movimientos_Sin_Cierre.Show
    Case "OPC_REPORTE_OPERACIONES"
      Frm_Reporte_Operaciones.Show
    Case "OPC_RECIERRE_CUENTA_MASIVO"
      Call Frm_ReCierre_Cuenta_Masivo.Mostrar(pOpcion_Menu)
    Case "OPC_IMPORTA_OPERACIONES"
      Call Frm_ImportaOperacionesSecurity.Show
    Case "OPC_REPORTE_NEMOTECNICO_C"
      Call Frm_Reporte_Nemotecnico_C.Show
    'Case "OPC_IMPORTA_OPERACIONES_BBVA"
      'Call Frm_ImportaOperacionesBBVA.Fnt_Mostrar("CARGA_FACT_RV", "CARGA_FACT_RF", "CARGA_FACT_FM")
    Case "OPC_BALANCE_INVERSIONES"
 '       Call Frm_Balance.Show      modificado por MMA, 20090403
      Call Frm_Balance.Mostrar(pOpcion_Menu)
    
    Case "OPC_ARBOL_CLASE_NEMOTECNICO"
      Call Frm_Arbol_Clase_Instrumento.Mostrar(pOpcion_Menu)
      
    Case "OPC_ARBOL_CLASE_NEMOTECNICO_II"
      Call Frm_Arbol_Clase_Instrumento_II.Mostrar(pOpcion_Menu)
          
    Case "OPC_RECALCULO_COMISIONES"
      Call Frm_ReCalculo_Comisiones.Mostrar(pOpcion_Menu)
    Case "OPC_REPORTE_SALDOS_CAJAV2"
        Call Frm_Reporte_Cajas_Saldo_V2.Mostrar(pOpcion_Menu)
    Case "OPC_REPORTE_COMISIONES_CUENTAS"
        Call Frm_Reporte_Comisiones_Cuenta.Mostrar(pOpcion_Menu)
    Case "OPC_RECALCULO_VALOR_CUOTA"
      Call Frm_ReCalculo_ValorCuotas.Mostrar(pOpcion_Menu)
    Case "OPC_REPORTE_CLIENTES_C"
      Call Frm_Reporte_Clientes_C.Show
    Case "OPC_GLOSA_DESCRIPCION"
      Call Frm_Man_Glosas.Mostrar(pOpcion_Menu)
    Case "OPC_REPORTE_RENTABILIDAD_PERFIL"
      Call Frm_Reporte_Rentabilidad_Perfil.Mostrar(pOpcion_Menu)
    Case "OPC_REPORTE_FLUJO_RESUMEN"
      Call Frm_Reporte_FlujosResumen.Mostrar(pOpcion_Menu)
    Case "OPC_REPORTE_VCUOTA_PATRIMONIO"
      Call Frm_Reporte_VCuota_Patrimonio.Mostrar(pOpcion_Menu)
    Case "OPC_OPERACION_SUSCRIPCION_OPCIONES"
      Call Frm_Suscripcion_Opciones.Show
    Case "OPC_MAN_REGIONES"                           ' JOVB
        Call Frm_Man_Regiones.Mostrar(pOpcion_Menu)
    Case "OPC_OPERACION_CUSTODIA_SUSCRIPCION"                     'DOG
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Custodia_SusCrip, pOper_Fecha_Anterior:=False)
    Case "OPC_OPERACION_CUSTODIA_SUSCRIPCION_FECHA_ANTERIOR"                     'DOG
      Call Sub_MuestraVentana(pOperacion:=gcOPERACION_Custodia_SusCrip, pOper_Fecha_Anterior:=True)
    Case "OPC_IMPRESION_COMPROBANTES_1862"
        Call Frm_Comprobante_Aporte_Rescate.Mostrar(pOpcion_Menu)   ' Jovb
    Case "OPC_REPORTE_REGISTRO_CLIENTES"                            ' MMA 25/09/2008
        Call Frm_Reporte_Registro_Clientes.Mostrar(pOpcion_Menu)
    Case "OPC_REPORTE_FICHA_ESTAD�STICA_CODIFICADA_UNIFORME"
        Call Frm_Reporte_Ficha_Codificada_Uniforme.Mostrar(pOpcion_Menu)
    Case "OPC_REPORTE_INDIVIDUAL_CARTERA_ADM"                       ' MMA 25/09/2008
        Call Frm_Reporte_Individual_Cartera_Adm.Mostrar(pOpcion_Menu)
    Case "OPC_REPORTE_COMISIONES_COBRADAS"                          'EMH
       Call Frm_Reporte_Comisiones_Cobradas.Mostrar(pOpcion_Menu)
    Case "OPC_CONSULTA_BUSQUEDA_ACTIVOS"                            ' MMA 14/10/2008
        Call Frm_Consulta_Busqueda_Por_Activos.Show
    Case "OPC_REPORTE_FLUJO_CAJA"                                   ' MMA  03/11/2008
        Call Frm_Reporte_Caja.Mostrar(pOpcion_Menu)
    Case "OPC_BLOTTER_OPERACIONES"                                   ' DOG  10/11/2008
        Call Frm_Blotter_Operaciones.Mostrar(pOpcion_Menu)
    Case "OPC_CONSULTA_APORES_CAPITAL"
        Call Frm_Consulta_AporteRetiro_Capital.Mostrar(pOpcion_Menu)
    Case "OPC_CONSULTA_SALDOS"
        Call Frm_Consulta_Saldos.Mostrar(pOpcion_Menu)
    Case "OPC_CONSULTA_REBALANCEO_MANUAL"
        Call Frm_Reporte_Rebalanceo_Manual.Show
'    Case "OPC_CONSULTA_RIESGOS_PERFILES"
'        Call Frm_Consulta_Riesgos_Perfiles.Mostrar(pOpcion_Menu)

    Case "OPC_REPORTE_CONTROL_LIMITE"
        Call Frm_Reporte_Control_Limite.Mostrar(pOpcion_Menu)
'    Case "OPC_FFMMADC_CONSULTA_CAJAS"
'        Call Frm_FFMMADC_Consulta_Cajas_Globales.Show
'    Case "OPC_DISPONIBLE_CAJA"
'        Call Frm_DisponibleCaja.Show
Rem ------------------------------------------------------------------------------------------
Rem MENU APV
Rem ------------------------------------------------------------------------------------------
    Case "OPC_TIPOS_AHORRO_APV"
        Call Frm_Man_TiposAhorroAPV.Mostrar(pOpcion_Menu)
    Case "OPC_INSTITUCION_PREVISIONAL"
        Call Frm_Man_InstitucionPrevisional.Mostrar(pOpcion_Menu)
    Case "OPC_APORTE_RETIRO_APV"
        Call Frm_Man_AportesRetirosAPV.Mostrar(pOpcion_Menu, True)
    Case "OPC_REPORTE_APOR_RET"                                         ' VCA  18/12/2008
        Call Frm_Reporte_AportesRetiros_APV.Mostrar(pOpcion_Menu)
    Case "OPC_REPORTE_CUADRO_SVS"                                         ' VCA  19/12/2008
        Call Frm_Reporte_CuadroSVS_APV.Mostrar(pOpcion_Menu)
Rem ------------------------------------------------------------------------------------------
Rem FIN MENU APV
Rem ------------------------------------------------------------------------------------------
    Case "OPC_EMPRESA_TRASPASO_CUENTAS"                                         ' DOG  06/02/2009
        Call Frm_Empresa_Traspaso_Cuentas.Show
    Case "OPC_OPERACION_ACCIONES"                                   'MMA 06/02/2009
      Call Frm_Operacion_Acciones.Mostrar(pTipo_Operacion:=gcOPERACION_Instruccion, _
                                          pCod_Instrumento:=gcINST_ACCIONES_NAC, _
                                          pOper_Fecha_Anterior:=True)

    Case "OPC_OPERACION_ACCIONES_DIREC"                                   'MMA 14/02/2009
      Call Frm_Operacion_Acciones.Mostrar(pTipo_Operacion:=gcOPERACION_Directa, _
                                          pCod_Instrumento:=gcINST_ACCIONES_NAC, _
                                          pOper_Fecha_Anterior:=True)

    Case "OPC_OPERACION_RENTAFIJA_BONOS_DIREC"                                   'MMA 20/02/2009
      Call Frm_Operacion_Bonos.Mostrar(pTipo_Operacion:=gcOPERACION_Directa, _
                                          pCod_Instrumento:=gcINST_BONOS_NAC, _
                                          pNombreOperacion:="Directa", _
                                          pOper_Fecha_Anterior:=True)

    Case "OPC_OPERACION_RENTAFIJA_BONOS_INST"                                   'MMA 23/02/2009
      Call Frm_Operacion_Bonos.Mostrar(pTipo_Operacion:=gcOPERACION_Instruccion, _
                                          pCod_Instrumento:=gcINST_BONOS_NAC, _
                                          pNombreOperacion:="Instrucci�n", _
                                          pOper_Fecha_Anterior:=True)
    Case "OPC_OPERACION_RENTAFIJA_DEPOS_DIREC"                                   'MMA 20/02/2009
      Call Frm_Operacion_Pactos_Depositos.Mostrar(pTipo_Operacion:=gcOPERACION_Directa, _
                                          pCod_Instrumento:=gcINST_DEPOSITOS_NAC, _
                                          pNombreOperacion:="Directa", _
                                          pOper_Fecha_Anterior:=True)

    Case "OPC_OPERACION_RENTAFIJA_DEPOS_INST"                                   'MMA 23/02/2009
      Call Frm_Operacion_Pactos_Depositos.Mostrar(pTipo_Operacion:=gcOPERACION_Instruccion, _
                                          pCod_Instrumento:=gcINST_DEPOSITOS_NAC, _
                                          pNombreOperacion:="Instrucci�n", _
                                          pOper_Fecha_Anterior:=True)
    
    Case "OPC_OPERACION_RENTAFIJA_PACTOS_INST"                                   'MMA 25/02/2009
      Call Frm_Operacion_Pactos_Depositos.Mostrar(pTipo_Operacion:=gcOPERACION_Instruccion, _
                                          pCod_Instrumento:=gcINST_PACTOS_NAC, _
                                          pNombreOperacion:="Instrucci�n", _
                                          pOper_Fecha_Anterior:=True)
  
    Case "OPC_OPERACION_RENTAFIJA_PACTOS_DIREC"                                   'MMA 25/02/2009
      Call Frm_Operacion_Pactos_Depositos.Mostrar(pTipo_Operacion:=gcOPERACION_Instruccion, _
                                          pCod_Instrumento:=gcINST_PACTOS_NAC, _
                                          pNombreOperacion:="Instrucci�n", _
                                          pOper_Fecha_Anterior:=True)
    Case "OPC_DESBLOQUEO_CUENTAS"
      Frm_DesBloqueo_Cierre_Ctas.Show
    Case "OPC_BLOQUEO_CUENTAS"
      Frm_Bloqueo_Cuentas.Show
    Case "OPC_IMPORTA_OPERACIONES_CDS"
      Call Frm_ImportaOperacionesCDS.Sub_Mostrar
    Case "OPC_IMPORTA_OPERACIONES_BCI"
      Call Frm_ImportaOperacionesBCI.Fnt_Mostrar("CARGA_FACT_RV", "CARGA_FACT_RF", "CARGA_FACT_FM")
    Case "OPC_IMPORTA_OPER_CUST_BCI"
      Call Frm_ImportaOperCustBCI.Fnt_Mostrar("CARGA_MOV_CUST_RV_BCI", "CARGA_MOV_CUST_RF_BCI")
    Case "OPC_MAN_CORTES_RF"
      Frm_Man_CortesRFija.Mostrar (pOpcion_Menu)
    Case "OPC_INGRESO_SOLICITUD_RENTA_FIJA"                 'JGR 04-MAY-09
     Frm_Man_Ingreso_Solicitud_RF.Mostrar (pOpcion_Menu)
     
    Case "OPC_SISTEMA_SALIR"
        Call Sub_CierraSistema
    Case "OPC_SISTEMA_CERRAR_SESION"
      Call Sub_CierraSesion
    
  End Select
  
End Sub

Private Function Fnt_BuscaKey_In_TreeView(pTree_Menu As MSComctlLib.TreeView, ByVal pkey As String) As Boolean
Dim lNodo As Double

  Fnt_BuscaKey_In_TreeView = False
  
  With pTree_Menu
    For lNodo = 1 To .Nodes.Count
      If .Nodes.Item(lNodo).Key = pkey Then
        Fnt_BuscaKey_In_TreeView = True
        Exit Function
      End If
    Next
  End With
  
End Function

Public Function New_Form_BuscaCuentas() As Object
  Set New_Form_BuscaCuentas = New Frm_Busca_Cuentas
End Function

Public Function New_Form_Container() As Object
  Set New_Form_Container = New Frm_DLL
End Function

Public Sub Sub_CargaMenu()
    Dim lCursor_Roles As hRecord
    Dim lReg_Rol      As hFields
    Dim lReg_Arbol    As hFields
    Dim lcRel_Usuarios_Roles As Class_Rel_Usuarios_Roles
    Dim lcArbol_Sistema As Class_Arbol_Sistema
    Dim Fila As Integer
    Dim Columna As Integer
    
    Dim xPadre As String
    Dim xIdMenu As String
    Dim xDesMenu As String
    Dim xTipo As String
    Dim xClase As String
    Dim xTag As String
    '-------------------------------------------
    Dim i, X As Long
    Dim iHijo As Long
    Dim bVisible As Boolean
    
    '---------------------------------------------------------------
    'Se agrego esta linea debido a se estaba callendo si un usuario
    'ingresaba y no tenia ningun men� de componente
    'HAF: 09/01/2008
    ReDim gClases_menu(0 To 0) As Est_Clase_Menu
    '---------------------------------------------------------------

    If gTipo_Menu = "A" Then
        Set lcRel_Usuarios_Roles = New Class_Rel_Usuarios_Roles
        With lcRel_Usuarios_Roles
            .Campo("id_Usuario").Valor = gID_Usuario
            If .Buscar Then
                Set lCursor_Roles = .Cursor
            Else
                MsgBox .ErrMsg, vbCritical, Me.Caption
                GoTo ErrProcedure
            End If
        End With
        
        Set lcRel_Usuarios_Roles = Nothing
  
        Set Tree_Menu.ImageList = ImageListGlobal16
        Fila = 0
        
        With Tree_Menu.Nodes
            .Clear
            If lCursor_Roles.Count > 0 Then
                For Each lReg_Rol In lCursor_Roles
                    Set lcArbol_Sistema = New Class_Arbol_Sistema
                    If lcArbol_Sistema.Buscar_Segun_Usuario(lReg_Rol("id_rol").Value) Then
                        For Each lReg_Arbol In lcArbol_Sistema.Cursor
                            'If BuscarPermisos(lReg_Arbol("Cod_Arbol_Sistema").Value) Then
                                If Not Fnt_BuscaKey_In_TreeView(Tree_Menu, lReg_Arbol("Cod_Arbol_Sistema").Value) Then
                                    With .Add
                                        .Key = lReg_Arbol("Cod_Arbol_Sistema").Value
                                        .Text = lReg_Arbol("Dsc_Arbol_Sistema").Value
                                        If Not NVL(lReg_Arbol("clase").Value, "") = "" Then
                                            ReDim Preserve gClases_menu(0 To Fila)
                                            With gClases_menu(Fila)
                                                .Opc_Menu = lReg_Arbol("Cod_Arbol_Sistema").Value
                                                .Clase = lReg_Arbol("clase").Value
                                            End With
                                            Fila = Fila + 1
                                        End If
                                        Select Case lReg_Arbol("Tipo").Value
                                            Case "C"
                                                .Image = "close_folder"
                                            Case "N"
                                                .Image = "form_grilla"
                                        End Select
                    
                                        If Not IsNull(lReg_Arbol("Cod_Arbol_Sistema_Padre").Value) Then
                                            Set .Parent = Tree_Menu.Nodes(lReg_Arbol("Cod_Arbol_Sistema_Padre").Value)
                                        End If
                                        
                                    End With
                                End If
                            ' End If
                        Next
                            
                    Else
                        MsgBox lcArbol_Sistema.ErrMsg, vbCritical, Me.Caption
                        GoTo ErrProcedure
                    End If
                    Set lcArbol_Sistema = Nothing
                Next
                
                Set Tree_Menu.SelectedItem = Tree_Menu.Nodes(1).Root
                Tree_Menu.SelectedItem.EnsureVisible
            End If
        End With
        
    Else
        ' ------------------ Carga Menu Persiana
        'ReDim gClases_menu(1000, 2)
        Set lcRel_Usuarios_Roles = New Class_Rel_Usuarios_Roles
        With lcRel_Usuarios_Roles
            .Campo("id_Usuario").Valor = gID_Usuario
            If .Buscar Then
                Set lCursor_Roles = .Cursor
            Else
                MsgBox .ErrMsg, vbCritical, Me.Caption
                GoTo ErrProcedure
            End If
        End With
        
        Fila = 0
        Set lcRel_Usuarios_Roles = Nothing
        
        With menu_persiana.MenuItems
            menu_persiana.MenuItems.Clear
            If lCursor_Roles.Count > 0 Then
                For Each lReg_Rol In lCursor_Roles
                
                    Set lcArbol_Sistema = New Class_Arbol_Sistema
                    
                    If lcArbol_Sistema.Buscar_Segun_Usuario(lReg_Rol("id_rol").Value) Then
                    
                        For Each lReg_Arbol In lcArbol_Sistema.Cursor
                        If lReg_Arbol("EXE") Then
                            xPadre = NVL(lReg_Arbol("Cod_Arbol_Sistema_Padre").Value, 0)
                            xIdMenu = lReg_Arbol("Cod_Arbol_Sistema").Value
                            xDesMenu = lReg_Arbol("Dsc_Arbol_Sistema").Value
                            
                            If Not NVL(lReg_Arbol("clase").Value, "") = "" Then
                                ReDim Preserve gClases_menu(0 To Fila)
                                With gClases_menu(Fila)
                                    .Opc_Menu = lReg_Arbol("Cod_Arbol_Sistema").Value
                                    .Clase = lReg_Arbol("clase").Value
                                   ' .ID_Arbol_Sistema = lReg_Arbol("id_Arbol_Sistema").Value
                                End With
                                
                                Fila = Fila + 1
                            End If
                            
                            xTipo = ""
                            xClase = ""
                            
                            If IsNull(lReg_Arbol("Cod_Arbol_Sistema_Padre").Value) Then
                                menu_persiana.MenuItems.Add 0, xIdMenu & xTag, , xDesMenu
                            Else
                                menu_persiana.MenuItems.Add xPadre, xIdMenu & xTag, 0, xDesMenu
                            End If
                            End If
                        Next
                        
                        i = 1
                        Do While i <= menu_persiana.MenuItems.Count
                            bVisible = False
                            
                            If menu_persiana.MenuItems.Parent(i) <> 0 Then
                                Exit Do
                            End If
                            
                            AnalizaHijos i, bVisible
                            menu_persiana.MenuItems.Visible(i) = bVisible
                            
                            i = i + 1
                        Loop
                        
                    Else
                        MsgBox lcArbol_Sistema.ErrMsg, vbCritical, Me.Caption
                        GoTo ErrProcedure
                    End If
                    
                    Set lcArbol_Sistema = Nothing
                Next
            End If
        End With
        'ReDim Preserve gClases_menu(fila, 2)
    End If
  
ErrProcedure:
    Set lcRel_Usuarios_Roles = Nothing
    Set lcArbol_Sistema = Nothing

End Sub

Private Sub Sub_CierraSesion()
Dim lForm As Form
Dim lMensaje As String
  
  lMensaje = "�Est� Seguro de cerrar la sesi�n?" & vbCrLf & " - Perder� todos los cambios no guardados - "
 
  If (MsgBox(lMensaje, vbYesNo + vbExclamation, "Cerrar Sesi�n")) = vbYes Then
    For Each lForm In VB.Forms
      If Not lForm.Name = Me.Name Then
        'Shell (App.Path & "\Gpi+20.exe")
        'End
        Unload lForm
        
      End If
      DoEvents
    Next
    
    With Me
      .Caption = "CSGPI "
      
      .StatusBar.Panels("TEXTO").Text = ""
      .StatusBar.Panels("TEXTO").ToolTipText = ""
      .StatusBar.Panels("FECHASERVIDOR").Text = "Fecha:"
      .StatusBar.Panels("FECHASERVIDOR").ToolTipText = ""
      .StatusBar.Panels("EMPRESA").Text = "Empresa:"
      .StatusBar.Panels("EMPRESA").ToolTipText = ""
      .StatusBar.Panels("CONNECT").Text = ""
      .StatusBar.Panels("CONNECT").ToolTipText = ""
      .StatusBar.Panels("SESSIONID").Text = ""
      .StatusBar.Panels("SESSIONID").ToolTipText = ""
      .StatusBar.Panels("STATUSDB").Text = ""
      .StatusBar.Panels("STATUSDB").ToolTipText = ""
            
      .menu_persiana.MenuItems.Clear
      
    End With
    
    gID_Usuario = 0
    Shell (App.Path & "\Gpi Identificador.exe")
    End
    'Call Proce_Main.Main
  End If

End Sub

Private Sub Sub_CierraSistema()
Dim lMensaje As String

  lMensaje = "�Est� Seguro de abandonar la aplicaci�n?" & vbCrLf & " - Perder� todos los cambios no guardados - "
  
  If (MsgBox(lMensaje, vbYesNo + vbExclamation, "Salir del Sistema")) = vbYes Then
    Unload Me
    End
  End If

End Sub

'15/07/2009 Modificado por MMardones
'Private Sub Sub_MuestraVentana(pOperacion As String, pOper_Fecha_Anterior As Boolean, Optional pInternacional As Boolean = False)
Private Sub Sub_MuestraVentana(pOperacion As String, pOper_Fecha_Anterior As Boolean, Optional pInternacional As Boolean = False, _
                               Optional pPorMonto As Boolean = False)
Dim lForm_OpeGeneral As Form
Dim lNom_Form As String
  
  lNom_Form = "Frm_Operaciones_General"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pOperacion) Then
    
    Select Case pOperacion
      Case gcOPERACION_Directa
         Set lForm_OpeGeneral = New Frm_Operaciones_General
         Call lForm_OpeGeneral.Mostrar(gcOPERACION_Directa, pOper_Fecha_Anterior, pInternacional, pPorMonto)
      
      Case gcOPERACION_Instruccion
         Set lForm_OpeGeneral = New Frm_Operaciones_General
         Call lForm_OpeGeneral.Mostrar(gcOPERACION_Instruccion, pOper_Fecha_Anterior, pInternacional, pPorMonto)
      
      Case gcOPERACION_Custodia
         Set lForm_OpeGeneral = New Frm_Operaciones_General
         Call lForm_OpeGeneral.Mostrar(gcOPERACION_Custodia, pOper_Fecha_Anterior, pInternacional)
         
      Case gcOPERACION_Custodia_SusCrip
         Set lForm_OpeGeneral = New Frm_Operaciones_General
         Call lForm_OpeGeneral.Mostrar(gcOPERACION_Custodia_SusCrip, pOper_Fecha_Anterior)
      End Select
      
  End If

End Sub

Private Sub Sub_VScroll_Ajustar()
Dim lFormVerticalMaximo As Integer
Dim lForm As Form
Dim lAltoStatus As Integer

On Error Resume Next
    For Each lForm In Forms
      If Not lForm Is MDI_Principal Then
        If lForm.Top > lFormVerticalMaximo Then
            lFormVerticalMaximo = lForm.Top + lForm.Height 'Obtengo la posici�n vertical del Form que se encuentre mas abajo
        End If
        
        If TypeName(lForm) = "StatusBar" Then 'Si tengo un Form estatus tambi�n se eval�a su altura
            lAltoStatus = lForm.Height
        End If
      End If
    Next
    
    If VScroll.Height > lFormVerticalMaximo Then
        VScroll.max = VScroll.Height - lFormVerticalMaximo + 50
    Else
        VScroll.max = lFormVerticalMaximo - VScroll.Height + 50
    End If
    
    If lFormVerticalMaximo <= MDI_Principal.ScaleHeight Then
        VScroll.Visible = False
    Else
        VScroll.Visible = True
    End If
    
    VScroll.Top = 0
    VScroll.Height = PictureBox.ScaleHeight - lAltoStatus
    VScroll.Left = PictureBox.ScaleWidth - VScroll.Width
    'aPos = 0
    VScroll.Value = 0
End Sub

'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: MDI_Principal.frm $
'    $Author: Gbuenrostro $
'    $Date: 16-10-15 16:56 $
'    $Revision: 9 $
'-------------------------------------------------------------------------------------------------

Private Sub Frame_Menu_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If X <= Tree_Menu.Width Then
    Frame_Menu.MousePointer = vbDefault
  Else
    Frame_Menu.MousePointer = vbSizeWE
  End If
  
  If Button = vbLeftButton Then
    Select Case X
      Case Is > 1845
        With Frame_Menu
          .MousePointer = vbSizeWE
          .Width = X
        End With
'      Case Else
'        Frame_Menu.Width = 1845
    End Select
  End If
End Sub

Private Sub Frame_Menu_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  With Frame_Menu
    .MousePointer = vbDefault
  End With
End Sub

Private Sub Frame_Menu_Resize()
  With Frame_Menu
    If .Width < 1845 Then
      .Width = 1845
    End If
  End With
  
  With LblMov
    .Top = Frame_Menu.ScaleTop
    .Left = Frame_Menu.ScaleWidth - 40
    .Height = Frame_Menu.ScaleHeight
    .Width = Frame_Menu.ScaleWidth - 40
  End With
  
  With Tree_Menu
    .Height = Frame_Menu.ScaleHeight
    .Width = Frame_Menu.ScaleWidth - 40
  End With
End Sub

Private Sub LblMov_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'  If X <= Tree_Menu.Width Then
'    Frame_Menu.MousePointer = vbDefault
'  Else
'    Frame_Menu.MousePointer = vbSizeWE
'  End If
  Frame_Menu.MousePointer = vbSizeWE
  
  If Button = vbLeftButton Then
    Select Case LblMov.Left + X
      Case Is > 1845
        With Frame_Menu
          .MousePointer = vbSizeWE
          .Width = LblMov.Left + X
        End With
'      Case Else
'        Frame_Menu.Width = 1845
    End Select
  End If
End Sub

Private Sub LblMov_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  With Frame_Menu
    .MousePointer = vbDefault
  End With
End Sub

Private Sub MDIForm_Load()
    Rem RMoya 07/07/2006 Obtener Despues de haber seteado el separador de decimal
  Rem el separador de decimal resultante.
  Call obtenerSepDecimal
  Call ObtenerSepListas
  
  Rem ****************************************************************************
  Call Sub_CargaMenu
  'PictureBox.Width = 0
End Sub

Private Sub MDIForm_Resize()
  PictureBox.Width = 0
  PictureBox.Width = PictureBox.Width + Me.ScaleWidth
End Sub

Private Sub MDIForm_Terminate()
Dim lForm As Form
  
  For Each lForm In VB.Forms
  
    Unload lForm
    DoEvents
  Next
  
  End
End Sub

Private Sub menu_persiana_Click(ByVal ID As Long)
Dim xTag As String
Dim i As Integer
        
  With menu_persiana.MenuItems
    For i = 0 To UBound(gClases_menu)
      If gClases_menu(i).Opc_Menu = .Key(ID) Then
        Call Sub_IniciaFormulario(gClases_menu(i).Clase)
        GoTo ExitProcedure
      End If
    Next i
    Elije_Menu (.Key(ID))
  End With
  
ExitProcedure:
   Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub PictureBox_Resize()
  With Imagen
    .Top = 0
    .Left = 0
    .Width = PictureBox.ScaleWidth
    .Height = PictureBox.ScaleHeight
  End With
  Call Sub_VScroll_Ajustar
End Sub

Private Sub Timer_Pos_Timer()
  Call Sub_VScroll_Ajustar
End Sub

Private Sub Tree_Menu_Collapse(ByVal Node As MSComctlLib.Node)
  Node.Image = "close_folder"
End Sub

Private Sub Tree_Menu_Expand(ByVal Node As MSComctlLib.Node)
  Node.Image = "open_folder"
End Sub

'Private Sub Tree_Menu_GotFocus()
'  If Not Tree_Menu.SelectedItem Is Nothing Then
'    Call Tree_Menu.SelectedItem.EnsureVisible
'  End If
'End Sub

Private Sub Tree_Menu_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Frame_Menu.MousePointer = vbDefault
  DoEvents
End Sub

Private Sub Tree_Menu_NodeClick(ByVal Node As MSComctlLib.Node)
Dim lForm As Form
Dim i As Integer

   'Call Node.EnsureVisible
   
   Call Sub_Bloquea_Puntero(Me)
   
   'Primero revisa si existe una clase asociada
'   If Not ("" & Node.Tag) = "" Then
'      Call Sub_IniciaFormulario(Node.Tag)
'      GoTo ExitProcedure
'   End If
   
   For i = 0 To UBound(gClases_menu)
    If gClases_menu(i).Opc_Menu = Node.Key Then
      Call Sub_IniciaFormulario(gClases_menu(i).Clase)
      GoTo ExitProcedure
    End If
   Next i
   Elije_Menu (Node.Key)
   
ExitProcedure:
   
   Call Sub_Desbloquea_Puntero(Me)
End Sub
