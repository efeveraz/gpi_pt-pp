VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form Frm_Visor_Salto_Cuota 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Visor Salto Cuota"
   ClientHeight    =   6600
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9990
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6600
   ScaleWidth      =   9990
   Begin VB.Frame Frame2 
      Caption         =   "Filtros"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   765
      Left            =   30
      TabIndex        =   3
      Top             =   390
      Width           =   9945
      Begin MSComCtl2.DTPicker DT_fechahasta 
         Height          =   315
         Left            =   3720
         TabIndex        =   5
         Top             =   270
         Width           =   1395
         _ExtentX        =   2461
         _ExtentY        =   556
         _Version        =   393216
         Format          =   65798145
         CurrentDate     =   38852
      End
      Begin MSComCtl2.DTPicker DT_fechadesde 
         Height          =   315
         Left            =   1200
         TabIndex        =   4
         Top             =   270
         Width           =   1395
         _ExtentX        =   2461
         _ExtentY        =   556
         _Version        =   393216
         Format          =   65798145
         CurrentDate     =   38852
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   " Hasta"
         Height          =   315
         Left            =   2670
         TabIndex        =   7
         Top             =   270
         Width           =   1005
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   " Desde"
         Height          =   315
         Left            =   150
         TabIndex        =   6
         Top             =   270
         Width           =   1005
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Detalle"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   5325
      Left            =   30
      TabIndex        =   1
      Top             =   1200
      Width           =   9945
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   4845
         Left            =   150
         TabIndex        =   2
         Top             =   300
         Width           =   9615
         _cx             =   16960
         _cy             =   8546
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   8
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Visor_Salto_Cuota.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9990
      _ExtentX        =   17621
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Buscar"
            Key             =   "FIND"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Excel"
            Key             =   "XLS"
            Description     =   "Genera Archivo Excel"
            Object.ToolTipText     =   "Genera Archivo Excel"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   5160
         TabIndex        =   8
         Top             =   0
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
      End
   End
End
Attribute VB_Name = "Frm_Visor_Salto_Cuota"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: Frm_Visor_Salto_Cuota.frm $
'    $Author: Gbuenrostro $
'    $Date: 28-01-14 18:16 $
'    $Revision: 3 $
'-------------------------------------------------------------------------------------------------

'Public fKey As String

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("XLS").Image = "boton_excel"
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("FIND").Image = cBoton_Buscar
  End With
  Dim lCierre As Class_Verificaciones_Cierre
  
  Set lCierre = New Class_Verificaciones_Cierre
  
  DT_fechadesde = lCierre.Busca_Ultima_FechaCierre - 1
  DT_fechahasta = lCierre.Busca_Ultima_FechaCierre
  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1

End Sub

Public Function Fnt_Modificar(ByVal pId_Cliente As String)
'  fKey = pId_Cliente
'
'  Call Sub_CargarDatos
'
'  Me.Caption = "Modificaci�n Cliente: " & Txt_Rut.Text & " - " & Txt_Nombres.Text
'  Me.Top = 1
'  Me.Left = 1
'  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "FIND"
      Call Sub_CargarDatos
    Case "XLS"
      Call Fnt_Excel
      'If Fnt_Grabar Then
      '  Unload Me
      'End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargaForm
      End If
  End Select
End Sub

Private Sub Sub_CargaForm()
Dim lReg As hFields

  Grilla.Rows = 1
  Call Sub_FormControl_Color(Me.Controls)

'  Call Sub_CargaCombo_Perfil_Riesgo(cbo_perfil, , True)
'  Call Sub_ComboSelectedItem(cbo_perfil, cCmbKALL)
'
'  Call Sub_CargaCombo_Cuentas_Vigentes(cbo_Cuentas, , True)
'  Call Sub_ComboSelectedItem(cbo_Cuentas, cCmbKALL)
End Sub

Private Sub Sub_CargarDatos()
Dim lcPatrimonio_Cuenta As Class_Patrimonio_Cuentas
Dim lReg                As hCollection.hFields
'------------------------------------------------------
Dim lFecDesde As Date
Dim lFecHasta As Date
Dim lId_Cuenta As String
Dim lId_Perfil As String

Dim lLinea As Long

'Dim lValCuotAnterior As Double
'Dim lValCuotActual As Double

Dim lVariacion As Double
Dim lPorc_Salto_Cuota As Double

  If DT_fechadesde.Value >= DT_fechahasta.Value Then
    MsgBox "Fecha desde no debe ser superior o igual a fecha hasta.", vbExclamation
    Exit Sub
  End If
  
  Call Sub_Bloquea_Puntero(Me)
  
  Grilla.Rows = 1
  
'  lId_Perfil = IIf(Fnt_ComboSelected_KEY(cbo_perfil) = cCmbKALL, "", Fnt_ComboSelected_KEY(cbo_perfil))
'  lId_Cuenta = IIf(Fnt_ComboSelected_KEY(cbo_Cuentas) = cCmbKALL, "", Fnt_ComboSelected_KEY(cbo_Cuentas))
  lFecDesde = DT_fechadesde.Value
  lFecHasta = DT_fechahasta.Value

  Load Me
  Set lcPatrimonio_Cuenta = New Class_Patrimonio_Cuentas
  With lcPatrimonio_Cuenta
    .Campo("Id_cuenta").Valor = ""
    If Not .Busca_Saltos_Cuotas(pFecha_Desde:=lFecDesde _
                              , pFecha_Hasta:=lFecHasta _
                              , pId_Perfil:="") Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al buscar los saltos de cuota.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
  
    For Each lReg In .Cursor
      'If lReg("variacion").Value > 0 Then
        lLinea = Grilla.Rows
        Grilla.AddItem ""
        Call SetCell(Grilla, lLinea, "fecha1", lReg("ayer").Value)
        Call SetCell(Grilla, lLinea, "fecha", lReg("today").Value)
        Call SetCell(Grilla, lLinea, "id_cuenta", lReg("num_cuenta").Value)
        Call SetCell(Grilla, lLinea, "perfil", lReg("dsc_perfil_riesgo").Value)
        Call SetCell(Grilla, lLinea, "valor_cuota", lReg("valor_cuota_mon_cuenta").Value)
        Call SetCell(Grilla, lLinea, "renta", lReg("variacion").Value)
        lVariacion = lReg("variacion").Value
        Call SetCell(Grilla, lLinea, "saltocuota", lReg("porc_salto_cuota").Value)
        lPorc_Salto_Cuota = lReg("porc_salto_cuota").Value
        If lVariacion > 0 Then
          Call SetCell(Grilla, lLinea, "diferencia", (lVariacion - lPorc_Salto_Cuota))
        Else
          Call SetCell(Grilla, lLinea, "diferencia", (lVariacion + lPorc_Salto_Cuota))
        End If
     'End If
    Next
  End With


ExitProcedure:

  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = True
End Function

Public Sub Fnt_Excel()
Dim lArchivo As String

  lArchivo = Environ("TMP") & "VisorSaldoCuota.xls"
  
  Grilla.SaveGrid lArchivo, flexFileExcel
  Grilla.SaveGrid lArchivo, flexFileExcel, "Salto Cuota"
  Grilla.SaveGrid lArchivo, flexFileExcel, flexXLSaveFixedCells
  Grilla.SaveGrid lArchivo, flexFileExcel, flexXLSaveFixedRows
  Call Shell("cmd /c """ & lArchivo & """")
End Sub
