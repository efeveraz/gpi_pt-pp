VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form Frm_Proceso_Cierre_Pendientes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Eliminacion de Procesos de Cierres Pendientes"
   ClientHeight    =   4500
   ClientLeft      =   165
   ClientTop       =   330
   ClientWidth     =   9285
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   9285
   Begin VB.Frame Frnm_Grilla_Sucesos 
      Caption         =   "Visor de Sucesos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3255
      Left            =   60
      TabIndex        =   6
      Top             =   1200
      Width           =   9165
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   2865
         Left            =   120
         TabIndex        =   7
         Top             =   270
         Width           =   7965
         _cx             =   14049
         _cy             =   5054
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Proceso_Cierre_Pendientes.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Detalle 
         Height          =   330
         Left            =   8160
         TabIndex        =   8
         Top             =   540
         Width           =   900
         _ExtentX        =   1588
         _ExtentY        =   582
         ButtonWidth     =   1588
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Detalle"
               Key             =   "DETALLE"
               Object.ToolTipText     =   "Muestra el detalle de los sucesos del Cierre"
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frm_Fecha_Cierre 
      Caption         =   "Fecha Cierre"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   765
      Left            =   60
      TabIndex        =   2
      Top             =   390
      Width           =   9165
      Begin MSComCtl2.DTPicker DTP_Fecha_Cierre 
         Height          =   315
         Left            =   1470
         TabIndex        =   3
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   79888385
         CurrentDate     =   37732
      End
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   2910
         TabIndex        =   4
         Top             =   270
         Width           =   1020
         _ExtentX        =   1799
         _ExtentY        =   582
         ButtonWidth     =   1746
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Verificar"
               Key             =   "VERIFICAR"
               Description     =   "Valoriza el nemotécnico a la tasa de inversión."
               Object.ToolTipText     =   "Verificaciones antes del Proceso de Cierre"
            EndProperty
         EndProperty
      End
      Begin VB.Label Lbl_Fecha_Cierre 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Cierre"
         Height          =   345
         Left            =   210
         TabIndex        =   5
         Top             =   270
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9285
      _ExtentX        =   16378
      _ExtentY        =   635
      ButtonWidth     =   3334
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Eliminar Pendientes"
            Key             =   "CIERRE"
            Object.ToolTipText     =   "Elimina los Cierres Pendientes"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Proceso_Cierre_Pendientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("CIERRE").Image = cBoton_Aceptar
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Verificacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("VERIFICAR").Image = cBoton_Agregar_Grilla
  End With
  
  With Toolbar_Detalle
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("DETALLE").Image = cBoton_Agregar_Grilla
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre

  Rem Limpia la grilla
  Grilla.Rows = 1
  
  Set lCierre = New Class_Verificaciones_Cierre
  DTP_Fecha_Cierre.Value = lCierre.Busca_Ultima_FechaCierre + 1
  
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "CIERRE"
      Toolbar.Buttons("EXIT").Enabled = False
      Call Fnt_Proceso_Elimina_Cierre
      Call Sub_Setea_Controles(False)
      Toolbar.Buttons("EXIT").Enabled = True
    Case "EXIT"
      Unload Me
  End Select
End Sub


Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "VERIFICAR"
      Toolbar.Buttons("EXIT").Enabled = False
      If Fnt_Proceso_Verificacion Then
        Call Sub_Setea_Controles(True)
      Else
        Call Sub_Setea_Controles(False)
      End If
      Toolbar.Buttons("EXIT").Enabled = True
  End Select
End Sub

Private Sub DTP_Fecha_Cierre_Change()
  Call Sub_Setea_Controles(False)
End Sub

Private Function Fnt_Proceso_Verificacion() As Boolean
Dim lVerificacion_Cierre As Class_Verificaciones_Cierre
  Me.Enabled = False
  
  Call Sub_Bloquea_Puntero(Me)
  
  Fnt_Proceso_Verificacion = False
  
  Grilla.Rows = 1
  
  Set lVerificacion_Cierre = New Class_Verificaciones_Cierre

  If lVerificacion_Cierre.Verificacion_Cierre_Pendiente(DTP_Fecha_Cierre.Value, Grilla) Then
    Fnt_Proceso_Verificacion = True
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
  Me.Enabled = True
End Function

Private Sub Toolbar_Detalle_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "DETALLE"
      Call Grilla_DblClick
  End Select
End Sub

Private Sub Grilla_DblClick()
Dim lMensaje As String

  With Grilla
    If .Row > 0 Then
      lMensaje = GetCell(Grilla, .Row, "TEXTO")
      If Not lMensaje = "" Then
        MsgBox lMensaje, vbInformation, Me.Caption
      End If
    End If
  End With

End Sub

Private Sub Sub_Setea_Controles(pCierre As Boolean)
  
  If pCierre Then
    Toolbar.Buttons("CIERRE").Enabled = True
    Toolbar_Verificacion.Buttons("VERIFICAR").Enabled = False
  Else
    Toolbar.Buttons("CIERRE").Enabled = False
    Toolbar_Verificacion.Buttons("VERIFICAR").Enabled = True
  End If
  
End Sub

Private Function Fnt_Proceso_Elimina_Cierre() As Boolean
Dim lCierre As Class_Cierre
Dim lVerificacion_Cierre As Class_Verificaciones_Cierre

  Call Sub_Bloquea_Puntero(Me)

  Call Grilla.AddItem("")
  
  Set lCierre = New Class_Cierre
  
  If lCierre.Fnt_Proceso_Cierre(DTP_Fecha_Cierre.Value, Grilla, BarraProceso) Then
    MsgBox "Terminado el Proceso de Cierre para la fecha '" & Format(DTP_Fecha_Cierre.Value, cFormatDate) & "'.", vbInformation, Me.Caption
    Set lVerificacion_Cierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Cierre.Value = lVerificacion_Cierre.Busca_Ultima_FechaCierre + 1
  End If
    
  Call Sub_Desbloquea_Puntero(Me)
  
End Function
