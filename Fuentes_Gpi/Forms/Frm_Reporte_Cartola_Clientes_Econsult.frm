VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Object = "{1BCC7098-34C1-4749-B1A3-6C109878B38F}#1.0#0"; "vspdf8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Cartola_Clientes_Econsult 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cartola de Clientes"
   ClientHeight    =   7215
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   14970
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7215
   ScaleWidth      =   14970
   Begin VB.Frame Frame2 
      Caption         =   "Configuraci�n de Salida"
      ForeColor       =   &H000000FF&
      Height          =   735
      Left            =   15
      TabIndex        =   17
      Top             =   450
      Width           =   14925
      Begin VB.CommandButton Cmb_BuscaFile 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   13110
         TabIndex        =   21
         ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
         Top             =   330
         Width           =   345
      End
      Begin VB.OptionButton optOpciones 
         Caption         =   "Por Pantalla"
         Height          =   210
         Index           =   0
         Left            =   120
         TabIndex        =   20
         Top             =   360
         Value           =   -1  'True
         Width           =   1455
      End
      Begin VB.OptionButton optOpciones 
         Caption         =   "Por Impresora"
         Height          =   210
         Index           =   1
         Left            =   1920
         TabIndex        =   19
         Top             =   360
         Width           =   1455
      End
      Begin VB.OptionButton optOpciones 
         Caption         =   "Genera Archivo"
         Height          =   210
         Index           =   2
         Left            =   3990
         TabIndex        =   18
         Top             =   360
         Width           =   1695
      End
      Begin hControl2.hTextLabel Txt_Directorio 
         Height          =   315
         Left            =   5820
         TabIndex        =   22
         Tag             =   "OBLI"
         Top             =   330
         Width           =   7140
         _ExtentX        =   12594
         _ExtentY        =   556
         LabelWidth      =   900
         TextMinWidth    =   1200
         Caption         =   "Directorio"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   5925
      Left            =   30
      TabIndex        =   0
      Top             =   1230
      Width           =   14895
      Begin VB.CommandButton Btn_Buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   7200
         Picture         =   "Frm_Reporte_Cartola_Clientes_Econsult.frx":0000
         TabIndex        =   15
         Top             =   240
         Width           =   375
      End
      Begin VSPrinter8LibCtl.VSPrinter vp 
         Height          =   615
         Left            =   0
         TabIndex        =   9
         Top             =   5310
         Visible         =   0   'False
         Width           =   1335
         _cx             =   2355
         _cy             =   1085
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         MousePointer    =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoRTF         =   -1  'True
         Preview         =   -1  'True
         DefaultDevice   =   0   'False
         PhysicalPage    =   -1  'True
         AbortWindow     =   -1  'True
         AbortWindowPos  =   0
         AbortCaption    =   "Printing..."
         AbortTextButton =   "Cancel"
         AbortTextDevice =   "on the %s on %s"
         AbortTextPage   =   "Now printing Page %d of"
         FileName        =   ""
         MarginLeft      =   1440
         MarginTop       =   1440
         MarginRight     =   1440
         MarginBottom    =   1440
         MarginHeader    =   0
         MarginFooter    =   0
         IndentLeft      =   0
         IndentRight     =   0
         IndentFirst     =   0
         IndentTab       =   720
         SpaceBefore     =   0
         SpaceAfter      =   0
         LineSpacing     =   100
         Columns         =   1
         ColumnSpacing   =   180
         ShowGuides      =   2
         LargeChangeHorz =   300
         LargeChangeVert =   300
         SmallChangeHorz =   30
         SmallChangeVert =   30
         Track           =   0   'False
         ProportionalBars=   -1  'True
         Zoom            =   -1.15864527629234
         ZoomMode        =   3
         ZoomMax         =   400
         ZoomMin         =   10
         ZoomStep        =   25
         EmptyColor      =   -2147483636
         TextColor       =   0
         HdrColor        =   0
         BrushColor      =   0
         BrushStyle      =   0
         PenColor        =   0
         PenStyle        =   0
         PenWidth        =   0
         PageBorder      =   0
         Header          =   ""
         Footer          =   ""
         TableSep        =   "|;"
         TableBorder     =   7
         TablePen        =   0
         TablePenLR      =   0
         TablePenTB      =   0
         NavBar          =   3
         NavBarColor     =   -2147483633
         ExportFormat    =   0
         URL             =   ""
         Navigation      =   3
         NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
         AutoLinkNavigate=   0   'False
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
         Height          =   345
         Left            =   1605
         TabIndex        =   4
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         Format          =   17170433
         CurrentDate     =   38938
      End
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   5670
         TabIndex        =   10
         Top             =   5370
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2858
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Reporte"
               Key             =   "REPORTE"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               Object.ToolTipText     =   "Genera el Reporte de Instruccones"
            EndProperty
         EndProperty
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
         Height          =   3525
         Left            =   30
         TabIndex        =   8
         Top             =   1740
         Width           =   14265
         _cx             =   25162
         _cy             =   6218
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   11
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Reporte_Cartola_Clientes_Econsult.frx":030A
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComDlg.CommonDialog Cmd_AbreArchivo 
         Left            =   1320
         Top             =   5430
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin MSComctlLib.Toolbar Toolbar_Chequeo 
         Height          =   660
         Left            =   14370
         TabIndex        =   11
         Top             =   2520
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_ALL"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Selecciona todos los items"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_NOTHING"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Deselecciona todos los items"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar3 
            Height          =   255
            Left            =   9420
            TabIndex        =   12
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
      Begin TrueDBList80.TDBCombo Cmb_Moneda 
         Height          =   345
         Left            =   9285
         TabIndex        =   6
         Tag             =   "OBLI=S;CAPTION=Moneda"
         Top             =   240
         Width           =   2565
         _ExtentX        =   4524
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Cartola_Clientes_Econsult.frx":04D9
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   345
         Left            =   4605
         TabIndex        =   5
         Tag             =   "OBLI=S;CAPTION=Moneda"
         Top             =   240
         Width           =   2550
         _ExtentX        =   4498
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Cartola_Clientes_Econsult.frx":0583
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComctlLib.Toolbar Toolbar_Cuentas 
         Height          =   390
         Left            =   3960
         TabIndex        =   7
         Top             =   1200
         Width           =   5010
         _ExtentX        =   8837
         _ExtentY        =   688
         ButtonWidth     =   2752
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   5
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Cartola"
               Key             =   "GENERAR"
               Description     =   "Generar Cartola Cuenta Seleccionada"
               Object.ToolTipText     =   "Generar Cartola Cuenta Seleccionada"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Agregar Cuenta"
               Key             =   "LOAD"
               Description     =   "Agrega Cuenta a Grilla"
               Object.ToolTipText     =   "Agrega Cuenta a Grilla"
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Agregar Todas"
               Key             =   "LOAD_ALL"
               Description     =   "Agrega Todas las Cuentas a la Grilla"
               Object.ToolTipText     =   "Agrega todas las cuentas a la Grilla"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   255
            Left            =   9420
            TabIndex        =   16
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
      Begin hControl2.hTextLabel Txt_Nombres 
         Height          =   315
         Left            =   60
         TabIndex        =   23
         Top             =   720
         Width           =   7545
         _ExtentX        =   13309
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   "Nombre"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Asesor 
         Height          =   315
         Left            =   7740
         TabIndex        =   24
         Top             =   720
         Width           =   6045
         _ExtentX        =   10663
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   "Asesor"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuenta"
         Height          =   345
         Left            =   3060
         TabIndex        =   14
         Top             =   240
         Width           =   1515
      End
      Begin VB.Label lbl_moneda 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Moneda de Cartola"
         Height          =   345
         Left            =   7740
         TabIndex        =   13
         Top             =   240
         Width           =   1515
      End
      Begin VSPDF8LibCtl.VSPDF8 VSPDF81 
         Left            =   2040
         Top             =   5430
         Author          =   ""
         Creator         =   ""
         Title           =   ""
         Subject         =   ""
         Keywords        =   ""
         Compress        =   3
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Cartola"
         Height          =   345
         Index           =   0
         Left            =   30
         TabIndex        =   1
         Top             =   240
         Width           =   1515
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   14970
      _ExtentX        =   26405
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Cartola_Clientes_Econsult"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFecha              As Date
Dim lForm               As Frm_Reporte_Cartola
Dim FilaAux             As Variant
Dim iFilaTermino        As Variant
Dim lId_Cuenta          As String
Dim lFormatoMonedaCta   As String

'Dim lNum_Cuenta         As String
'Dim lDsc_Cuenta         As String
'Dim lNom_Cliente        As String
'Dim lDsc_Moneda         As String
Dim lId_Moneda_Cta      As String
Dim nDecimales          As Integer
Dim Moneda_Cuentas      As String
Dim fDsc_Moneda_Salida  As String
Dim bCargaUnitaria      As Boolean

Dim nPage               As Integer
Dim oCliente            As New Class_Cartola
Dim bImprimioHeader     As Boolean
'----------------------------------------------
Dim iLineasImpresas     As Integer

Dim sTextoIndicadores As String
Dim stextovalores  As String
Dim sTextoValoresAnt As String
Dim sTextoDosPuntos As String
Dim nInterlineado As Integer

Dim nTotalActivos_Actual_Para_Porcentajes As Double
Dim nTotalActivos_Anterior_Para_Porcentajes As Double


'--------------------------------------------------------------------------
' Constantes
'--------------------------------------------------------------------------
Const RColorRelleno = 125 '38     ' 213   ' R
Const GColorRelleno = 18 ' 7      ' 202   ' G
Const BColorRelleno = 2 '115    ' 195   ' B

Const RColorRelleno_TITULOS = 242  ' R
Const GColorRelleno_TITULOS = 242   ' G
Const BColorRelleno_TITULOS = 242   ' B

Const glb_tamletra_titulo = 8
Const glb_tamletra_subtitulo1 = 7.5
Const glb_tamletra_subtitulo2 = 7.5
Const glb_tamletra_registros = 7.3

Const Font_Name = "Times New Roman"
' Const Font_Name = "Gill Sans MT"

Const COLUMNA_INICIO    As String = "10mm"
Const COLUMNA_DOS       As String = "140mm"
Const CONST_ANCHO_GRILLA As String = "255mm"

Const CONS_NUMERO_LINEAS As Integer = 38   ' Numero de Lineas

Const CONS_POR_PANTALLA As Integer = 0
Const CONS_POR_IMPRESORA As Integer = 1
Const CONS_POR_ARCHIVO As Integer = 2

Rem ---------------------------------------------------------------------------
Rem 09/09/2008 MMA, Agrega Moneda a visualizar la cartola
Rem 10/09/2008 MMA, Agrega Combo Cuentas para seleccionar una o m�s a la grilla
Rem 03/10/2008 MMA, Agrega Gr�fico Torta para Renta Variable. S�lo Acciones
Rem 22/10/2008 MMA, Modifica Flujo Patrimonial, debe aparecer Total Neto.
Rem                 Agrega la columna Monto promedio compra a RV
Rem                 Agrega la columna Valor Original a FFMM
Rem 04/11/2008 MMA, Se comenta el Saldo en Flujo Patrimonial. Vuelve a como
Rem                 era antes, indicando total de Aportes y Retiros en forma
Rem                 separada y no como Saldo Neto
Rem 11/11/2008 MMA, Se elimina en forma temporal la entrega de la rentabilidad
Rem                 �ltimos 12 meses hasta completar el a�o, pudiendo
Rem                 presentarla a partir de Enero del 2009.
Rem ---------------------------------------------------------------------------


Private Sub Btn_Buscar_Click()
Dim lId_Moneda_Cuenta As Integer
    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)
    
    If lId_Cuenta <> 0 Then
        Call Sub_ComboSelectedItem(Cmb_Cuentas, lId_Cuenta)
        MuestraDatosCuenta
        
        
    End If

End Sub

'Private Sub Chk_Archivos_Click()
'    If Chk_Archivos.Value Then
'        Txt_Directorio.Enabled = True
'        Cmb_BuscaFile.Enabled = True
'    Else
'        Txt_Directorio.Enabled = False
'        Cmb_BuscaFile.Enabled = False
'    End If
'End Sub

Private Sub Cmb_BuscaFile_Click()
    On Error GoTo Cmd_BuscarArchivo_Err
    
    Dim x As Integer
    'Cheap way to use the common dialog box as a directory-picker
    x = 3

    Cmd_AbreArchivo.CancelError = True    'Do not terminate on error

    On Error Resume Next         'I will hande errors

    Cmd_AbreArchivo.Action = 1            'Present "open" dialog

    'If FileTitle is null, user did not override the default (*.*)
    If Cmd_AbreArchivo.FileTitle <> "" Then x = Len(Cmd_AbreArchivo.FileTitle)

    If Err = 0 Then
        ChDrive Cmd_AbreArchivo.Filename
        Txt_Directorio.Text = Left(Cmd_AbreArchivo.Filename, Len(Cmd_AbreArchivo.Filename) - x)
    Else
        'User pressed "Cancel"
    End If
'
'  With Cmd_AbreArchivo
'    .CancelError = True
'    .InitDir = "C:\"
'    '.Filter = "Texto (*.xls)|*.xls"
'
'   ' .Flags = cdlOFNFileMustExist
'
'    .ShowOpen
'  End With
'
'  Rem Se coloca el path completo, que incluye el nombre de archivo
'  Txt_Directorio.Text = Cmd_AbreArchivo.Filename
'        'ChDrive cdMain.Filename
'        'txtPath.Text = Left(cdMain.Filename, Len(cdMain.Filename) - x)
'
'
'
  Exit Sub

Cmd_BuscarArchivo_Err:
  If Err = 32755 Then 'usuario presiona el boton Cancelar del Command Dialog
    Exit Sub
  End If
End Sub


Private Sub Cmb_Cuentas_ItemChange()
    lId_Cuenta = Fnt_FindValue4Display(Cmb_Cuentas, Cmb_Cuentas.Text)
    If Not lId_Cuenta = "" Then
        MuestraDatosCuenta
    End If
End Sub

Private Sub Cmb_Cuentas_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
        MuestraDatosCuenta
    End If
End Sub
'
'Private Sub Cmb_Cuentas_SelChange(Cancel As Integer)
'    lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
'    MuestraDatosCuenta
'End Sub

Private Sub Form_Load()
    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("EXIT").Image = cBoton_Salir
            .Buttons("REFRESH").Image = cBoton_Refrescar
    End With
  
    With Toolbar_Verificacion
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("REPORTE").Image = cBoton_Modificar
    End With
  
    Call Sub_CargaForm
  
    Me.Top = 1
    Me.Left = 1
  
    With Cmd_AbreArchivo
        .Flags = cdlOFNPathMustExist
        .Flags = .Flags Or cdlOFNHideReadOnly
        .Flags = .Flags Or cdlOFNNoChangeDir
        .Flags = .Flags Or cdlOFNExplorer
        .Flags = .Flags Or cdlOFNNoValidate
        .Filename = "*.*"
    End With
    
    With Toolbar_Chequeo
     Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
        .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
        .Appearance = ccFlat
    End With
    
'MMA 10/09/2008
    With Toolbar_Cuentas
     Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("GENERAR").Image = "document"
        .Buttons("LOAD").Image = "boton_aceptar"
        .Buttons("LOAD_ALL").Image = cBoton_Agregar_Grilla
        .Appearance = ccFlat
    End With
    
    Txt_Directorio.Enabled = False
    Cmb_BuscaFile.Enabled = False
    
  
End Sub

Private Sub Sub_CargaForm()
    Dim lCierre As Class_Verificaciones_Cierre
    
    oCliente.IdCuenta = ""
    
  'MMA 10/09/2008
    'Call Sub_Carga_Grilla    ****ahora se carga a medida q selecciona desde el combo o en Agregar Todas
    Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, pTodos:=False)
    Call Sub_ComboSelectedItem(Cmb_Cuentas, cCmbKALL)

    Set lCierre = New Class_Verificaciones_Cierre
  
    fFecha = lCierre.Busca_Ultima_FechaCierre
    DTP_Fecha_Ter.Value = fFecha

    Call Sub_CargaCombo_Monedas(Cmb_Moneda)
    Call Sub_Limpiar
    

    Call Sub_FormControl_Color(Me.Controls)
    
End Sub

Private Sub Form_Resize()
    Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub
'Private Sub Grilla_Cuentas_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
'  If Col = Grilla_Cuentas.ColIndex("NUM_CUENTA") Then
'    Cancel = True
'  End If
'End Sub
Private Sub Grilla_Cuentas_Click()
    Dim i As Long
    If optOpciones(CONS_POR_PANTALLA).Value Then    ' Si es la opcion por pantalla se desmarcan todos
        With Grilla_Cuentas
            If .Col = Grilla_Cuentas.ColIndex("chk") Then
                For i = 1 To .Rows - 1
                    .Cell(flexcpChecked, i, .ColIndex("chk")) = flexUnchecked
                Next
                .Cell(flexcpChecked, .Row, .ColIndex("chk")) = flexChecked
            End If
        End With
    End If
End Sub

Private Sub optOpciones_Click(Index As Integer)
    Dim i As Long
    
    If Index = CONS_POR_PANTALLA Then ' Si es la opcion por pantalla se desmarcan todos
        With Grilla_Cuentas
            For i = 1 To .Rows - 1
                .Cell(flexcpChecked, i, .ColIndex("chk")) = flexUnchecked
            Next
        End With
    ElseIf Index = CONS_POR_ARCHIVO Then
        If optOpciones(Index).Value Then
            Txt_Directorio.Enabled = True
            Cmb_BuscaFile.Enabled = True
        Else
            Txt_Directorio.Enabled = False
            Cmb_BuscaFile.Enabled = False
        End If
    End If
    
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents

    Select Case Button.Key
        Case "REFRESH"
            Call Sub_Limpiar
        Case "EXIT"
            Unload Me
    End Select
End Sub

Private Sub Toolbar_Cuentas_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "GENERAR"
        If Cmb_Moneda.Text = "" Then
          MsgBox "Debe Seleccionar Moneda de Visualizaci�n Cartola", vbInformation, Me.Caption
          Exit Sub
        End If

        Grilla_Cuentas.Enabled = True
        Grilla_Cuentas.Rows = 1
        bCargaUnitaria = True
        Call Sub_Carga_Grilla
        Toolbar_Cuentas.Buttons(3).Enabled = False
        Toolbar_Cuentas.Buttons(5).Enabled = False
        Call ProcesoCartola
        Grilla_Cuentas.Rows = 1
    Case "LOAD"
        If Cmb_Moneda.Text = "" Then
          MsgBox "Debe Seleccionar Moneda de Visualizaci�n Cartola", vbInformation, Me.Caption
          Exit Sub
        End If
        Toolbar_Cuentas.Buttons(1).Enabled = False
        Toolbar_Verificacion.Buttons(1).Enabled = True
        If Cmb_Cuentas.Text = "" Then
            MsgBox "Debe Seleccionar Cuenta", vbInformation, Me.Caption
           Exit Sub
        End If
        
        Grilla_Cuentas.Enabled = True
        bCargaUnitaria = True
        Call Sub_Carga_Grilla
    Case "LOAD_ALL"
        Toolbar_Cuentas.Buttons(1).Enabled = False
        Toolbar_Cuentas.Buttons(3).Enabled = False
        Toolbar_Verificacion.Buttons(1).Enabled = True
        Cmb_Moneda.Enabled = False
        Grilla_Cuentas.Enabled = True
        Grilla_Cuentas.Rows = 1
        bCargaUnitaria = False
        Call Sub_Carga_Grilla
   End Select

End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents
    Select Case Button.Key
        Case "REPORTE"
            If Cmb_Moneda.Text = "" Then
               MsgBox "Debe Seleccionar Moneda de Visualizaci�n de Cartola", vbExclamation, Me.Caption
               Exit Sub
            End If
            If optOpciones(CONS_POR_ARCHIVO).Value And Txt_Directorio.Text = "" Then
                MsgBox "Debe Ingresar la Carpeta de Destino para los PDF", vbExclamation, Me.Caption
                Exit Sub
            End If
            Call ProcesoCartola
    End Select
End Sub
Private Sub ProcesoCartola()
    Dim i As Integer
    Dim flag As Boolean
    flag = False
    nInterlineado = 110
    Call Sub_Bloquea_Puntero(Me)
    With Grilla_Cuentas
        For i = 1 To .Rows - 1
            Call SetCell(Grilla_Cuentas, i, "mensaje", "")
        Next
        For i = 1 To .Rows - 1
            If .Cell(flexcpChecked, i, .ColIndex("chk")) = flexChecked Then
                If GetCell(Grilla_Cuentas, i, "fecha_cierre") <> "" Then
                    If CDate(GetCell(Grilla_Cuentas, i, "fecha_cierre")) >= DTP_Fecha_Ter.Value Then
                        Call SetCell(Grilla_Cuentas, i, "mensaje", "Generando Cartola")
                        DoEvents
                        fDsc_Moneda_Salida = GetCell(Grilla_Cuentas, i, "dsc_moneda_salida")
                        If GeneraCartola(GetCell(Grilla_Cuentas, i, "colum_pk"), DTP_Fecha_Ter.Value, GetCell(Grilla_Cuentas, i, "moneda"), GetCell(Grilla_Cuentas, i, "id_moneda_salida")) Then
                            Call SetCell(Grilla_Cuentas, i, "mensaje", "Cartola Generada")
                        Else
                            If Not optOpciones(CONS_POR_PANTALLA).Value Then
                                If bCargaUnitaria Then
                                    MsgBox "Error en la Generacion", vbExclamation, Me.Caption
                                End If
                                Call SetCell(Grilla_Cuentas, i, "mensaje", "Error en la Generacion")
                            Else
                                Call SetCell(Grilla_Cuentas, i, "mensaje", "Error en la Generacion")
                            End If
                        End If
                        flag = True
                    Else
                        If optOpciones(CONS_POR_PANTALLA).Value Then
                            If bCargaUnitaria Then
                               MsgBox "Fecha Reporte Mayor a Fecha Cierre Cuenta", vbExclamation, Me.Caption
                            End If
                            Call SetCell(Grilla_Cuentas, i, "mensaje", "Fecha Reporte Mayor a Fecha Cierre Cuenta")
                        Else
                            Call SetCell(Grilla_Cuentas, i, "mensaje", "Fecha Reporte Mayor a Fecha Cierre Cuenta")
                        End If
                    End If
                Else
                    If optOpciones(CONS_POR_PANTALLA).Value Then
                        If bCargaUnitaria Then
                            MsgBox "Cuenta no Tiene Cierre", vbExclamation, Me.Caption
                        End If
                        Call SetCell(Grilla_Cuentas, i, "mensaje", "Cuenta no Tiene Cierre.")
                    Else
                        Call SetCell(Grilla_Cuentas, i, "mensaje", "Cuenta no Tiene Cierre.")
                    End If
                End If
            End If
        Next
    End With
    Screen.MousePointer = vbDefault
    If Not optOpciones(CONS_POR_PANTALLA).Value And flag Then
        MsgBox "Generaci�n Terminada", vbExclamation, Me.Caption
    End If
    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Sub_Limpiar()
    Call Sub_ComboSelectedItem(Cmb_Moneda, cCmbKALL)
    Call Sub_ComboSelectedItem(Cmb_Cuentas, cCmbKALL)
    Cmb_Moneda.Enabled = True
    Toolbar_Cuentas.Buttons(1).Enabled = True
    Toolbar_Cuentas.Buttons(3).Enabled = True
    Toolbar_Cuentas.Buttons(5).Enabled = True
    Grilla_Cuentas.Enabled = True
    Grilla_Cuentas.Rows = 1
    Grilla_Cuentas.Enabled = False
    Toolbar_Verificacion.Buttons(1).Enabled = False
    Txt_Nombres.Text = ""
    Txt_Asesor.Text = ""
    'DTP_Fecha_Ini.Value = fFecha
    'DTP_Fecha_Ter.Value = fFecha
End Sub

Sub FormatoHoja()
    With vp
        .PaperSize = pprLetter
        .MarginLeft = COLUMNA_INICIO   ' "15mm"
        .MarginRight = "15mm"
        .MarginTop = "45mm"
        .MarginBottom = "25mm"
        .MarginFooter = "15mm"
        .MarginHeader = "0mm"
        
        .Font.Name = "Microsoft Sans Serif"
        .Font.Size = glb_tamletra_registros - 1
        
        .LineSpacing = 110
        
        .Orientation = orLandscape
    End With
    
    sTextoIndicadores = ""
    stextovalores = ""
    sTextoValoresAnt = ""
    sTextoDosPuntos = ""
    
End Sub

Public Sub pon_titulo(pTitulo As String, pAlto As Variant, pfont_name As String, pfont_size As Double)
    Dim ant_font_name   As String
    Dim ant_font_size   As Double
    Dim pY              As Variant
    Dim pX              As Variant


    With vp
        'lee valores anteriores
        ant_font_name = .FontName
        ant_font_size = .FontSize

        'pon valores nuevos
        ' .FontName = "Gill Sans MT"   ' pfont_name
        .FontSize = pfont_size

        ' pon titulo
        pY = .CurrentY
        pX = .CurrentX

        Call haz_linea(pY, True)

        .CalcParagraph = pTitulo
        .Paragraph = " " & pTitulo
        
        Call haz_linea(.CurrentY, True)

        'recupera valores anteriores
        .FontName = ant_font_name
        .FontSize = ant_font_size

        '.CurrentX = pX
        '.CurrentY = pY

    End With
End Sub

Function GeneraCartola(lId_Cuenta As Long, Fecha_Cartola As Date, lDsc_Moneda As String, lId_Moneda_Salida As Integer) As Boolean
    Dim i As Integer
    Dim hay_cierre As Boolean
    
    nPage = 0
    Moneda_Cuentas = lDsc_Moneda
    'oCliente.IdCuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Cuentas))
    'oCliente.Fecha_Cartola = DTP_Fecha_Ter.Value
    
    oCliente.IdCuenta = lId_Cuenta
    oCliente.Fecha_Cartola = CDate(Fecha_Cartola)
    oCliente.IdMonedaSalida = lId_Moneda_Salida
    
    GeneraCartola = True
    hay_cierre = GeneraCartola
    
    
'Agregado por MMA. 09/09/2008.
    
    Call DatosDelCliente
    
    FormatoHoja
    vp.StartDoc
    Call PaginaUno(hay_cierre):     DoEvents
    'FormatoHoja
    Call PaginaDos:                 DoEvents
    Call PaginaTres:                DoEvents
    
    'Call PaginaTresyMedia:         DoEvents
    'Call PaginaCuatro:             DoEvents
    'Call PaginaCinco:              DoEvents
    
    'fncDisclaimer
    
    vp.EndDoc
'********************************************
    Dim sFooter As String
    
    Dim vColorAnt As Variant
    sFooter = ""
    ' vp.FontName = "Arial"
    vp.FontName = "Times New Roman"
    vp.FontSize = 5
    vColorAnt = vp.PenColor
    ' vp.PenColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
   
    For i = 1 To nPage
        vp.StartOverlay i
        
        haz_linea ("200mm")
        
        vp.CurrentX = vp.MarginLeft
        vp.CurrentY = rTwips("201mm")
        
        vp.TextAlign = taCenterMiddle
        vp.TextColor = 0 'RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        vp.FontSize = 7
        sFooter = "Nota importante: Si dentro del plazo de 15 d�as no hemos recibido aviso por escrito en contrario, dirigido a Econsult, consideramos como aprobado este estado de cuenta para todos los efectos a que haya lugar."
        vp.Paragraph = sFooter
        vp.FontSize = 5
        sFooter = "Av El Golf 99 Piso 12"
        vp.Paragraph = sFooter & vbCrLf & "Pagina " & i & "/" & nPage
        vp.EndOverlay
    Next
'**************************************************
    'Dim sFileName As String
    'sFileName = "Cartola_" & glb_cuenta & "_" & FormatoFechaYYYYMMDD(glb_fecha_hoy) & ".pdf"
    'VSPDF81.ConvertDocument vp, oApp.PathPdf & sFileName
    '####### Grabar el Documento de esta ventana y recuperarlo en la Otra
    
    If optOpciones(CONS_POR_IMPRESORA).Value Then
        vp.PrintDoc
    ElseIf optOpciones(CONS_POR_ARCHIVO).Value Then
        Dim Carpeta As String
        Dim Directorio As String
        
        If Txt_Directorio.Text <> "" Then
            Carpeta = MonthName(Month(oCliente.Fecha_Cartola)) & Year(oCliente.Fecha_Cartola)
            
            On Error Resume Next
            MkDir Txt_Directorio.Text & Carpeta
            
            Directorio = Txt_Directorio.Text & Carpeta
            ' Dim sFileName As String
            
            oCliente.NombreArchivo = Trim(oCliente.Nombre) & "_" & Trim(oCliente.Num_Cuenta) & "_" & Day(oCliente.Fecha_Cartola) & Carpeta & ".pdf"
            VSPDF81.ConvertDocument vp, Directorio & "\" & oCliente.NombreArchivo
        End If
        Set lForm = Nothing
        
    Else
        Set lForm = New Frm_Reporte_Cartola
        Dim PathAndFile As String
        
        PathAndFile = oCliente.CreateTempFile("car")
        
        'vp.PrintDialog pdPrint
        vp.SaveDoc PathAndFile
        
        'vp.PrintFile (PathAndFile)
        lForm.WindowState = vbMaximized
        lForm.vp.LoadDoc PathAndFile
        lForm.vp.ZoomMode = zmPageWidth
    End If
    '##################################################
    
    'Call ColocarEn(lForm, Me, eFP_Abajo)
   
    ' vp.Clear
    vp.KillDoc
    
    Screen.MousePointer = vbDefault

End Function

Sub PaginaDos()
    ' Call Sub_Generar_Reporte_2
    'Call DetalleInstrumentos
    Call ActivosClaseInstrumento
End Sub

Sub PaginaTres()
    vp.NewPage

    Call Cajas
End Sub

Function Pri_Dia_Mes()

End Function

Sub Cajas()
    'Dim lCursor             As hRecord
    'Dim lReg                As hFields
    Dim lCursor_Caj         As hRecord
    'Dim lCaja_Cuenta        As hRecord
    Dim Cursor_Mov_Caja     As hRecord
    Dim lreg_1              As hFields
    Dim lReg_Caj            As hFields
    'Dim lcPatrimonio_Cuenta As Class_Patrimonio_Cuentas
    Dim ultimosaldo         As Double
    'Dim lPatrimonio         As Double
    Dim anterior            As Double
    
    'Dim iFila               As Integer
    'Dim Xinicio             As Variant
    'Dim YInicio             As Variant
    Dim i                   As Integer
    'Dim j                   As Integer
    '--------------------------------------------------
    Dim lCajas_Ctas         As Class_Cajas_Cuenta
    'Dim lsaldos_cajas       As Class_Saldos_Caja
    '-------------------------------------------------
    Dim nDecimalesCaja      As Integer
    '--------------------------------------------------
    Dim bImprimio         As Boolean
    Dim sCajaActual         As String
    '--------------------------------------------------
    Dim nTotalIngreso   As Double
    Dim nTotalEgreso    As Double
    Dim nSaldo          As Double
        
    'PKG_CAJAS_CUENTA$Buscar
    Set lCajas_Ctas = New Class_Cajas_Cuenta
    With lCajas_Ctas
        .Campo("id_cuenta").Valor = oCliente.IdCuenta
        If .Buscar(True) Then
            Set lCursor_Caj = .Cursor
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            'GoTo ErrProcedure
        End If
    End With
    
    bImprimioHeader = False
    
    With vp
        .CurrentY = .CurrentY + rTwips("2mm")
        
        .StartTable
            .TableBorder = tbBox
            .TableCell(tcCols) = 1
            .TableCell(tcRows) = 1
            .TableCell(tcFontSize, 1, 1, 1, 1) = 12
            .TableCell(tcFontSize, 1, 1) = glb_tamletra_subtitulo2
            .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
            .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
            .TableCell(tcText, 1, 1) = "INFORME DE CAJAS:"
            .TableCell(tcColWidth, 1, 1) = CONST_ANCHO_GRILLA
        .EndTable
        
        iLineasImpresas = 1
        
        For Each lReg_Caj In lCursor_Caj
            nDecimalesCaja = lReg_Caj("Decimales").Value
            
            .CurrentY = .CurrentY + rTwips("2mm")
            
            iLineasImpresas = iLineasImpresas + 1
'--------------------------------------------------------------------------------------------------------------------------
            gDB.Parametros.Clear
            gDB.Procedimiento = "Pkg_Saldos_Caja$Buscar_Saldos_Cuenta"
            gDB.Parametros.Add "PID_CAJA_CUENTA", ePT_Numero, lReg_Caj("id_caja_cuenta").Value, ePD_Entrada
            gDB.Parametros.Add "PID_CUENTA", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
            gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, CDate(UltimoDiaDelMes(oCliente.Fecha_Cartola)) + 1, ePD_Entrada
            gDB.Parametros.Add "PSaldo", ePT_Numero, ultimosaldo, ePD_Salida
            
            '.Parametros.Add "PID_CAJA_CUENTA", ePT_Fecha, request.form("idcaja"), ePD_Entrada
            
            If gDB.EjecutaSP Then
                ultimosaldo = gDB.Parametros("Psaldo").Valor
            End If
'--------------------------------------------------------------------------------------------------------------------------
            gDB.Parametros.Clear
            gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
            gDB.Parametros.Add "Pidcajacuenta", ePT_Numero, lReg_Caj("id_caja_cuenta").Value, ePD_Entrada
            gDB.Parametros.Add "Pfecha_inicio", ePT_Fecha, CDate(UltimoDiaDelMes(oCliente.Fecha_Cartola)) + 1, ePD_Entrada
            gDB.Parametros.Add "Pfecha_termino", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
            gDB.Procedimiento = "PKG_MOVIMIENTOS_CAJA_SECURITY"
            
            If Not gDB.EjecutaSP Then
                Exit Sub
            End If
            
            Set Cursor_Mov_Caja = gDB.Parametros("Pcursor").Valor
'----------------------------------------------------------------------------------------------------------------------------
            sCajaActual = NVL(lReg_Caj("dsc_caja_cuenta").Value, "Caja")
            Call EncabezadoCajas(sCajaActual, False)
            
            iLineasImpresas = iLineasImpresas + 1
            
            i = .TableCell(tcRows)
            
            .TableCell(tcText, i, 1) = ""
            .TableCell(tcText, i, 2) = UltimoDiaDelMes(oCliente.Fecha_Cartola)
            .TableCell(tcText, i, 3) = ""
            .TableCell(tcText, i, 4) = "SALDO ANTERIOR"
            .TableCell(tcText, i, 5) = ""
            .TableCell(tcText, i, 6) = ""
            .TableCell(tcText, i, 7) = FormatNumber(ultimosaldo, nDecimalesCaja)
            
            .TableCell(tcAlign, i, 7) = taRightMiddle
            .TableCell(tcFontSize, i, 1, i, 7) = glb_tamletra_registros
            
            iLineasImpresas = iLineasImpresas + 1
            
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            
            bImprimio = False
            nTotalIngreso = 0
            nTotalEgreso = 0
            nSaldo = 0
            
            For Each lreg_1 In Cursor_Mov_Caja
                i = .TableCell(tcRows)
                bImprimio = True
'------------------------------------------------------------------------------------------------------------
                If (iLineasImpresas Mod CONS_NUMERO_LINEAS) = 0 Then
                ' If (.CurrentLine Mod CONS_NUMERO_LINEAS) = 0 Then
                    .EndTable
                    
                    .NewPage
                    .StartTable
                    
                    Call EncabezadoCajas(sCajaActual, True)
                    
                    iLineasImpresas = iLineasImpresas + 1
                    i = .TableCell(tcRows)
                    
                End If
'------------------------------------------------------------------------------------------------------------
                If lreg_1("id_operacion").Value <> 0 Then
                    .TableCell(tcText, i, 1) = lreg_1("id_operacion").Value
                Else
                    .TableCell(tcText, i, 1) = "-"
                End If
                
                .TableCell(tcText, i, 2) = lreg_1("fecha_liquidacion").Value
                ' .TableCell(tcAlign, i, 2) = taCenterMiddle
                
                .TableCell(tcText, i, 3) = ""
                .TableCell(tcText, i, 4) = Trim(lreg_1("descripcion").Value)
                .TableCell(tcText, i, 5) = FormatNumber(lreg_1("monto_movto_abono").Value, nDecimalesCaja)
                .TableCell(tcAlign, i, 5) = taRightMiddle
                
                .TableCell(tcText, i, 6) = FormatNumber(lreg_1("monto_movto_cargo").Value, nDecimalesCaja)
                .TableCell(tcAlign, i, 6) = taRightMiddle
                
                If anterior <> lreg_1("id_operacion").Value Or anterior = 0 Then
                    ultimosaldo = ultimosaldo + lreg_1("monto_movto_abono").Value - lreg_1("monto_movto_cargo").Value
                End If
                
                .TableCell(tcFontSize, i, 1, i, 7) = glb_tamletra_registros
                .TableCell(tcText, i, 7) = FormatNumber(ultimosaldo, nDecimalesCaja)
                .TableCell(tcAlign, i, 7) = taRightMiddle
                
                nTotalIngreso = nTotalIngreso + lreg_1("monto_movto_abono").Value
                nTotalEgreso = nTotalEgreso + lreg_1("monto_movto_cargo").Value
                nSaldo = nSaldo + ultimosaldo
                 
                
                anterior = lreg_1("id_operacion").Value
                
                iLineasImpresas = iLineasImpresas + 1
                .TableCell(tcRows) = .TableCell(tcRows) + 1
                
            Next ' END OF {For Each lreg_1 In Cursor_Mov_Caja}
            '***********************************************************************
            ' Totales
            '***********************************************************************
            If bImprimio Then
                bImprimio = False
                i = .TableCell(tcRows)
                
                .TableCell(tcBackColor, i) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                .TableCell(tcForeColor, i) = RGB(255, 255, 255)
                .TableCell(tcText, i, 1) = ""
                .TableCell(tcText, i, 2) = ""
                .TableCell(tcText, i, 3) = ""
                .TableCell(tcText, i, 4) = "TOTALES": .TableCell(tcAlign, i, 4) = taRightMiddle
                .TableCell(tcText, i, 5) = FormatNumber(nTotalIngreso, nDecimalesCaja): .TableCell(tcAlign, i, 5) = taRightMiddle
                .TableCell(tcText, i, 6) = FormatNumber(nTotalEgreso, nDecimalesCaja): .TableCell(tcAlign, i, 6) = taRightMiddle
                .TableCell(tcText, i, 7) = FormatNumber(ultimosaldo, nDecimalesCaja): .TableCell(tcAlign, i, 7) = taRightMiddle
                
                .TableCell(tcFontSize, i, 1, i, 7) = glb_tamletra_registros
            End If
            '***********************************************************************
            'Fin totales
            '***********************************************************************
            .EndTable
            
            Set Cursor_Mov_Caja = Nothing
            Set lreg_1 = Nothing
            
        Next  ' END OF {For Each lReg_Caj In lCursor_Caj}
        
        Set lReg_Caj = Nothing
        Set lCursor_Caj = Nothing
        
    End With
    
End Sub

Private Sub EncabezadoCajas(Optional dsc_caja_cuenta As String = "", Optional bContinuacion As Boolean = False)
    Dim iFila As Integer
    
    With vp
        .StartTable
        
        .TableCell(tcCols) = 7
        .TableCell(tcRows) = 1
        
        If dsc_caja_cuenta <> "" Then
            
            .TableBorder = tbBox
            '.TableCell(tcCols) = 7
            '.TableCell(tcRows) = 1
            
            .TableCell(tcColSpan, 1, 1) = 7
            
            .TableCell(tcFontSize, 1, 1, 1, 7) = glb_tamletra_titulo
            .TableCell(tcText, 1, 1) = dsc_caja_cuenta & IIf(bContinuacion, " (Continuacion ...)", "")
            ' .TableCell(tcColWidth, 1, 1) = CONST_ANCHO_GRILLA
            
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iLineasImpresas = iLineasImpresas + 1
            
        End If
        
        .TableBorder = tbBoxRows
        iFila = .TableCell(tcRows)
        
        .TableCell(tcRowHeight, iFila) = 250
        
        .TableCell(tcText, iFila, 1) = "N� OPERACION"
        '.TableCell(tcAlign, iFila, 1) = taLeftMiddle
        .TableCell(tcColWidth, iFila, 1) = 1350
        .TableCell(tcBackColor, iFila, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 2) = "FECHA LIQ."
        '.TableCell(tcAlign, iFila, 2) = taCenterMiddle
        .TableCell(tcColWidth, iFila, 2) = 1305
        .TableCell(tcBackColor, iFila, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 3) = ""
        .TableCell(tcColWidth, iFila, 3) = 0
        .TableCell(tcBackColor, iFila, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 4) = "DETALLE"
        '.TableCell(tcAlign, iFila, 4) = taRightMiddle
        .TableCell(tcColWidth, iFila, 4) = 6300
        .TableCell(tcBackColor, iFila, 4) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 5) = "INGRESO"
        .TableCell(tcAlign, iFila, 5) = taRightMiddle
        .TableCell(tcColWidth, iFila, 5) = 1800
        .TableCell(tcBackColor, iFila, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 6) = "EGRESO"
        .TableCell(tcAlign, iFila, 6) = taRightMiddle
        .TableCell(tcColWidth, iFila, 6) = 1800
        .TableCell(tcBackColor, iFila, 6) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        
        .TableCell(tcText, iFila, 7) = "SALDO"
        .TableCell(tcAlign, iFila, 7) = taRightMiddle
        .TableCell(tcColWidth, iFila, 7) = 1890
        .TableCell(tcBackColor, iFila, 7) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)

        .TableCell(tcFontSize, iFila, 1, iFila, 7) = glb_tamletra_titulo
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)
                
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        
    End With
    
End Sub
        
Sub AsignaTitulos(ByRef oColumnas As Class_Cartola_Campos, sProducto As String)
    oColumnas.CleanDetalle
    
    Select Case sProducto
        Case gcPROD_RV_NAC
            'MENU DE ACCIONES'
            With oColumnas
                .AddColumna "nemotecnico", "NEMOT�CNICO", taLeftMiddle, False, , , 61
                .AddColumna "DSC_MONEDA", "MONEDA", taCenterMiddle, False, , , 40
                .AddColumna "Porcentaje_inversion", "% " & vbCrLf & "DE INVERSI�N", taRightMiddle, True, , 2, 23, True
                .AddColumna "Nominal", "CANTIDAD", taRightMiddle, True, , 0, 26
                .AddColumna "precio_promedio_compra", "PRECIO PROMEDIO " & vbCrLf & "COMPRA", taRightMiddle, True, , 2, 25
                .AddColumna "precio", "PRECIO ACTUAL", taRightMiddle, True, , 2, 25
                '.AddColumna "valoractual", "VALOR ACTUAL", taRightMiddle, True, , 2, 30
                
                .AddColumna "Valorizacion_Mercado", "VALORIZACI�N" & vbCrLf & "MERCADO", taRightMiddle, True, "", oCliente.Decimales, 30, True
                .AddColumna "rentabilidad", "% RENT.", taRightMiddle, True, , 2, 30
                
                '.AddColumna "DSC_EMISOR_ESPECIFICO", "Emisor", taCenterMiddle, 37
                '.AddColumna "tasa_cupon", "Tasa Cupon", taCenterMiddle, 37
                '.AddColumna "Fecha_Vencimiento", "Fecha" & vbCrLf & "Vencimiento", taCenterMiddle, 37
                '.AddColumna "Dias", "Dias", taCenterMiddle, 37
                '.AddColumna "Duration", "Duration", taCenterMiddle, 37
                '.AddColumna "TASA_COMPRA", "Tasa Compra", taCenterMiddle, 37
                '.AddColumna "tasa", "tasa", taCenterMiddle, 37
                '.AddColumna "precio_promedio_compra", "precio_promedio_compra", taCenterMiddle, 37
                '.AddColumna "monto_mon_nemotecnico", "monto_mon_nemotecnico", taCenterMiddle, 37
                '.AddColumna "Fecha_Cierre", "Fecha_Cierre", taCenterMiddle, 37
            End With
'            .Cell(flexcpText, 0, Colum_Nemo) = "Nemot�cnico"
'            .Cell(flexcpText, 0, Colum_Cantidad) = "Cantidad"
'            .Cell(flexcpText, 0, Colum_Precio_Promedio) = "Precio Promedio"
'            .Cell(flexcpText, 0, Colum_Valor_Precio_Promedio) = "Valor a Precio Promedio"
'            .Cell(flexcpText, 0, Colum_Precio_Mercado) = "Precio Mercado"
'            .Cell(flexcpText, 0, Colum_Valor_Mercado) = "Valor Mercado"
        Case gcPROD_RF_NAC
            'MENU DE RENTA FIJA
            With oColumnas
                .AddColumna "nemotecnico", "NEMOT�CNICO", taLeftMiddle, False, , , 25
                .AddColumna "DSC_EMISOR_ESPECIFICO", "EMISOR", taLeftMiddle, False, , , 15
                .AddColumna "Porcentaje_inversion", "% DE" & vbCrLf & "INVERSI�N", taRightMiddle, False, , 2, 20, True
                .AddColumna "DSC_MONEDA", "MONEDA", taCenterMiddle, True, "", 2, 16
                .AddColumna "Nominal", "VALOR" & vbCrLf & "NOMINAL", taRightMiddle, True, "", 0, 18
                .AddColumna "tasa_cupon", "TASA" & vbCrLf & "CUP�N", taRightMiddle, True, "", 2, 13
                .AddColumna "Fecha_Vencimiento", "FECHA" & vbCrLf & "VCTO.", taCenterMiddle, False, , 0, 20
                .AddColumna "Dias", "DIAS", taRightMiddle, True, , 0, 15
                .AddColumna "Duration", "DURATION", taRightMiddle, True, , 2, 20
                .AddColumna "TASA_COMPRA", "TASA" & vbCrLf & "COMPRA", taRightMiddle, True, , 2, 15
                .AddColumna "precio_Fecha_compra", "PRECIO" & vbCrLf & "COMPRA", taRightMiddle, True, , 2, 20
                .AddColumna "tasa", "TASA" & vbCrLf & "MERCADO", taRightMiddle, True, , 2, 17
                .AddColumna "precio", "PRECIO" & vbCrLf & "MERCADO", taRightMiddle, True, , 2, 20
                .AddColumna "Valorizacion_Mercado", "VALORIZACI�N", taRightMiddle, True, , 2, 26, True
                
                '.AddColumna "precio_promedio_compra", "precio_promedio_compra", taCenterMiddle, 37
                '.AddColumna "monto_mon_nemotecnico", "monto_mon_nemotecnico", taCenterMiddle, 37
                '.AddColumna "Fecha_Cierre", "Fecha_Cierre", taCenterMiddle, 37
            End With
        Case gcPROD_FFMM_NAC
            'MENU DE FONDOS MUTUOS
            With oColumnas
                .AddColumna "DSC_nemotecnico", "NEMOT�CNICO", taLeftMiddle, False, , 0, 75
                .AddColumna "DSC_MONEDA", "MONEDA", taCenterMiddle, False, , 0, 44
                .AddColumna "Porcentaje_inversion", "%" & vbCrLf & "DE INVERSI�N", taRightMiddle, True, , 2, 25, True
                .AddColumna "Nominal", "N�MERO" & vbCrLf & "CUOTAS", taRightMiddle, True, "#,###,##0", 2, 26
                .AddColumna "precio_promedio_compra", "VALOR CUOTA" & vbCrLf & "PROMEDIO COMPRA", taRightMiddle, True, , 2, 30
                .AddColumna "precio", "VALOR CUOTA" & vbCrLf & "ACTUAL", taRightMiddle, True, "#,###,##0.00", 2, 30
                .AddColumna "Valorizacion_Mercado", "VALOR MERCADO", taRightMiddle, True, "#,###,##0.00", 2, 30, True
                
                '.AddColumna "precio_Fecha_compra", "Precio" & vbCrLf & "Compra", taRightMiddle, , 38
                '.AddColumna "DSC_EMISOR_ESPECIFICO", "Emisor", taCenterMiddle, 37
                '.AddColumna "tasa_cupon", "Tasa Cupon", taCenterMiddle, 37
                '.AddColumna "Fecha_Vencimiento", "Fecha" & vbCrLf & "Vencimiento", taCenterMiddle, 37
                '.AddColumna "Dias", "Dias", taCenterMiddle, 37
                '.AddColumna "Duration", "Duration", taCenterMiddle, 37
                '.AddColumna "TASA_COMPRA", "Tasa Compra", taCenterMiddle, 37
                '.AddColumna "tasa", "tasa", taCenterMiddle, 37
                
                '.AddColumna "monto_mon_nemotecnico", "monto_mon_nemotecnico", taCenterMiddle, 37
                '.AddColumna "Fecha_Cierre", "Fecha_Cierre", taCenterMiddle, 37
            End With
    End Select

End Sub
        

Sub DatosDelCliente()
    Dim lCursorCuentas
    Dim lReg
    Dim CursorDireccion
    Dim Reg_1
    Dim Reg
    Dim Cursor_1
    Dim Cursor

    Dim IDMoneda As Integer
    'Dim Moneda As String

    'Dim Decimales As Integer

    Dim Fecha_Servidor As Date
    'Dim Fecha_Cierre As Date
    'Dim Fecha_Consulta As Date

    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Procedimiento = "PKG_CUENTAS$Buscar"

    If Not gDB.EjecutaSP Then
        GoTo Fin
    End If

    Set lCursorCuentas = gDB.Parametros("Pcursor").Valor
    gDB.Parametros.Clear

'#################### Direcciones Clientes ####################################
     For Each lReg In lCursorCuentas
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Pid_cliente", ePT_Numero, lReg("id_cliente").Value, ePD_Entrada
        gDB.Procedimiento = "PKG_DIRECCIONES_CLIENTES$Buscar"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set CursorDireccion = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each Reg_1 In CursorDireccion
            oCliente.Direccion = Trim(NVL(Reg_1("direccion").Value, ""))
            oCliente.Comuna = NVL(Reg_1("DSC_COMUNA_CIUDAD").Value, "")
            oCliente.Ciudad = NVL(Reg_1("DSC_REGION").Value, "")
            oCliente.Telefono = NVL(Reg_1("fono").Value, "")
            
            If oCliente.Direccion = "" Then
                oCliente.Direccion = "Sin Direcci�n"
            End If

        Next
'#################### FIN Direcciones Clientes #################################

'#################### Moneda  ####################################
        'IDMoneda = lReg("id_moneda").Value
        IDMoneda = oCliente.IdMonedaSalida

        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Pid_Moneda", ePT_Numero, IDMoneda, ePD_Entrada
        gDB.Procedimiento = "PKG_MONEDAS$Buscar"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set Cursor_1 = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each Reg_1 In Cursor_1
            oCliente.Moneda = Reg_1("dsc_moneda").Value
            oCliente.Decimales = Reg_1("dicimales_mostrar").Value
        Next
        
        
'#################### FIN Moneda #################################

'PKG_CIERRES_CUENTAS$BUSCAR_ultimo
'#################### Moneda  ####################################
        Fecha_Servidor = Fnt_FechaServidor()
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
        gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Servidor), ePD_Entrada
        gDB.Procedimiento = "PKG_CIERRES_CUENTAS$BUSCAR_ultimo"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set Cursor = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each Reg In Cursor
            'Fecha_cierre = Reg("fecha_cierre").value
            oCliente.Fecha_Cierre = CDate(Reg("fecha_cierre").Value)
        Next

'#################### FIN Moneda #################################
        oCliente.Nombre = lReg("nombre_cliente").Value
        
        oCliente.Rut = formato_rut(lReg("rut_cliente").Value)
        
        oCliente.Num_Cuenta = lReg("num_cuenta").Value
        oCliente.Perfil_Riesgo = lReg("dsc_perfil_riesgo").Value
        oCliente.Ejecutivo = NVL(lReg("dsc_asesor").Value, "Sin Asesor")
        oCliente.Fecha_Creacion = lReg("fecha_operativa").Value
        
        oCliente.MailEjecutivo = NVL(lReg("email").Value, "")        ' mail del Asesor.
        oCliente.FonoEjecutivo = NVL(lReg("fono").Value, "")     ' Fono del Asesor.
        
        oCliente.IDEmpresa = lReg("id_empresa").Value
        
        oCliente.NombreArchivo = ""
    Next
    
Fin:

    Set lCursorCuentas = Nothing
    Set lReg = Nothing
    Set CursorDireccion = Nothing
    Set Reg_1 = Nothing
    Set Reg = Nothing
    Set Cursor_1 = Nothing
    Set Cursor = Nothing

End Sub


Private Sub vp_NewPage()
    nPage = nPage + 1
    
    Call pon_header
    
    bImprimioHeader = True
    
    'Call Indicadores
End Sub

Public Sub pon_header()
    Dim xAnt As Variant
    Dim yAnt As Variant
    Dim xtalign As Variant
    Dim xbrushcolor As Variant
    Dim xpencolor As Variant
    Dim xtextcolor As Variant
    Dim xfontsize As Variant
    Dim xmarginleft As Variant
    Dim xAjusteAnt As Variant
    Dim sColorTextoAnt      As Long
    '-------------------------------------------------------------------
    Dim xValTop As Variant
    Dim sRutaImagen As String
    '-------------------------------------------------------------------
    Dim Xinicio As Variant
    Dim YInicio As Variant
    '-------------------------------------------------------------------
    Dim lreg_vtc            As hCollection.hFields
    Dim lValor_Tipo_Cambio  As New Class_Valor_Tipo_Cambio
    '-------------------------------------------------------------------
    
    vp.FontBold = False
    
    vp.MarginLeft = COLUMNA_INICIO
    
    'sRutaImagen = App.Path & "\security.jpg"
    sRutaImagen = App.Path & "\econsult.jpg"
    
    xValTop = ""
    
    'Inserta Logo
    On Error Resume Next
    vp.DrawPicture LoadPicture(sRutaImagen), "252mm", "7mm", "13mm", "13mm"
    
    On Error GoTo 0
    
    'Guarda valores anteriores
    xAnt = vp.CurrentX
    yAnt = vp.CurrentY
    xtalign = vp.TextAlign
    xbrushcolor = vp.BrushColor
    xpencolor = vp.PenColor
    xtextcolor = vp.TextColor
    xfontsize = vp.FontSize
    xmarginleft = vp.MarginLeft
    sColorTextoAnt = vp.TextColor
    
    vp.CurrentX = vp.MarginLeft
    vp.CurrentY = "20mm"
        
'Asigna nuevos valores
    vp.TextAlign = taCenterTop
    vp.BrushColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = RGB(255, 255, 255) ' RGB(100, 100, 100)
    
    vp.FontSize = 14
    'Llena texto
    vp.TextBox "BALANCE DE INVERSI�N", vp.MarginLeft, vp.CurrentY, "122.7mm", "7mm", True, False, True
    'Asigna nuevos valores
    vp.TextAlign = taLeftTop
    vp.BrushColor = RGB(242, 242, 242)
    'vp.PenColor = RGB(232, 226, 222)
    vp.TextColor = 0 'RGB(0, 62, 134)    ' sColorTextoAnt  ' &H0&
    vp.FontSize = 8
    
    'Llena texto
    Xinicio = vp.MarginLeft
    YInicio = vp.CurrentY
    
    Call vp.DrawRectangle(Xinicio, YInicio, Xinicio + rTwips("123mm"), YInicio + rTwips("24mm"))

    ' vp.MarginLeft = "79mm"
    vp.MarginLeft = Xinicio + rTwips("3mm")
    vp.CurrentX = Xinicio + rTwips("3mm")
    vp.CurrentY = YInicio + rTwips("3mm")
    
    Xinicio = vp.CurrentX
    
    'vp.CalcText = UCase("Se�ores" & vbCrLf & _
              oCliente.Nombre & vbCrLf & _
              "RUT N�: " & oCliente.Rut & vbCrLf & _
              oCliente.Direccion & vbCrLf & _
              oCliente.Comuna & " " & oCliente.Ciudad)
              
    vp.Text = UCase("Se�ores" & vbCrLf & _
              oCliente.Nombre & vbCrLf & _
              "RUT N�: " & oCliente.Rut & vbCrLf & _
              oCliente.Direccion & vbCrLf & _
              oCliente.Comuna & " " & oCliente.Ciudad)
              
' ####################################################  Cuadro Dos
    vp.MarginLeft = COLUMNA_DOS ' + rTwips("150mm")
    Xinicio = vp.MarginLeft
    YInicio = "20mm"
                
    'Asigna nuevos valores
    vp.TextAlign = taCenterTop
    vp.BrushColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    'vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = 0 'RGB(0, 62, 134)     ' RGB(100, 100, 100)
    
    vp.FontSize = 14
    'Llena texto
    vp.TextBox " ", Xinicio, YInicio, "124.7mm", "7mm", True, False, True
    
    'Asigna nuevos valores
    vp.TextAlign = taLeftTop
    vp.BrushColor = RGB(242, 242, 242)   ' RGB(232, 226, 222)
    'vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = 0 ' RGB(0, 62, 134)
    vp.FontSize = 8
    
    'Llena texto
     vp.MarginLeft = Xinicio ' + rTwips("3mm")
    Xinicio = vp.MarginLeft
    YInicio = vp.CurrentY
    
    vp.DrawRectangle Xinicio, _
                    YInicio, _
                    Xinicio + rTwips("125mm"), _
                    YInicio + rTwips("24mm")

    vp.MarginLeft = Xinicio + rTwips("3mm")
    vp.CurrentX = Xinicio + rTwips("3mm")
    vp.CurrentY = YInicio + rTwips("3mm")
    
    vp.Text = UCase("Informe al " & vbCrLf & _
                    "N� Cuenta " & vbCrLf & _
                    "Asesor" & vbCrLf & _
                    "Email" & vbCrLf & _
                    "Telefono")
    
    
    vp.MarginLeft = Xinicio + rTwips("22mm")
    vp.CurrentX = Xinicio + rTwips("22mm")
    vp.CurrentY = YInicio + rTwips("3mm")
    
    vp.Text = UCase(": " & oCliente.Fecha_Cartola & vbCrLf & _
              ": " & oCliente.Num_Cuenta & vbCrLf & _
              ": " & oCliente.Ejecutivo & vbCrLf & _
              ": " & oCliente.MailEjecutivo & vbCrLf & _
              ": " & oCliente.FonoEjecutivo)
              
'********************************************************************************
' Indicadores
'********************************************************************************
'-----------------------------------------------------------------------------
    xAjusteAnt = vp.TextAlign
    
    If sTextoIndicadores = "" Then    ' Es la primera vez que pasa por ac�
    
        sTextoIndicadores = "Valores al"
        sTextoDosPuntos = ":"
        stextovalores = oCliente.Fecha_Cartola
        sTextoValoresAnt = UltimoDiaDelMes(oCliente.Fecha_Cartola)
        
        With lValor_Tipo_Cambio
        
            .Campo("FECHA").Valor = oCliente.Fecha_Cartola
            If .BuscarIndicadores Then
                For Each lreg_vtc In .Cursor
                    sTextoIndicadores = sTextoIndicadores & vbCrLf & Trim(lreg_vtc("DSC_MONEDA").Value)
                    sTextoDosPuntos = sTextoDosPuntos & vbCrLf & ":"
                    stextovalores = stextovalores & vbCrLf & FormatNumber(lreg_vtc("VALOR").Value, 2)
                Next
            End If
                    
            .Campo("FECHA").Valor = UltimoDiaDelMes(oCliente.Fecha_Cartola)
            If .BuscarIndicadores Then
                For Each lreg_vtc In .Cursor
                    sTextoValoresAnt = sTextoValoresAnt & vbCrLf & FormatNumber(lreg_vtc("VALOR").Value, 2)
                Next
            End If
            
        End With
        
        Set lValor_Tipo_Cambio = Nothing
    End If
    
    vp.MarginLeft = Xinicio + rTwips("66mm")
    vp.CurrentX = vp.MarginLeft
    vp.CurrentY = YInicio + rTwips("3mm")
    
    vp.StartTable
    
    vp.TableCell(tcRows) = 1
    vp.TableCell(tcCols) = 4
    vp.TableBorder = tbNone
    vp.TableCell(tcFontSize) = 7.5
    
    vp.TableCell(tcText, 1, 1) = UCase(sTextoIndicadores)
    vp.TableCell(tcText, 1, 2) = sTextoDosPuntos
    vp.TableCell(tcText, 1, 3) = UCase(stextovalores)
    vp.TableCell(tcText, 1, 4) = UCase(sTextoValoresAnt)
    
    vp.TableCell(tcColWidth, 1, 1) = rTwips("21mm")
    vp.TableCell(tcColWidth, 1, 2) = rTwips("4mm")
    vp.TableCell(tcColWidth, 1, 3) = rTwips("17mm")
    vp.TableCell(tcColWidth, 1, 4) = rTwips("17mm")
    
    vp.TableCell(tcColAlign, 1, 2) = taLeftMiddle
    vp.TableCell(tcColAlign, 1, 3, 1, 4) = taRightMiddle
    
    vp.EndTable
        
'    vp.Text = UCase(sTextoIndicadores)
'
'    vp.MarginLeft = Xinicio + rTwips("90mm")
'    vp.CurrentX = vp.MarginLeft + rTwips("0mm")
'    vp.CurrentY = YInicio + rTwips("3mm")
'
'    ' vp.TextAlign = taRightMiddle
'    vp.Text = UCase(stextovalores)
''
'    vp.MarginLeft = Xinicio + rTwips("108mm")
'    vp.CurrentX = vp.MarginLeft + rTwips("0mm")
'    vp.CurrentY = YInicio + rTwips("3mm")
'
'    ' vp.TextAlign = taRightMiddle
'    vp.Text = UCase(sTextoValoresAnt)
    
    ' vp.TextAlign = taRightMiddle
    
'----------------------------------------------------------------------------
    vp.MarginLeft = COLUMNA_INICIO
    vp.CurrentY = YInicio + rTwips("25mm")
    vp.CurrentX = vp.MarginLeft
    
    '    vp.CurrentY = YInicio + rTwips("3mm")
    '    'Recupera valores anteriores
    '    vp.CurrentX = xAnt
    '    vp.CurrentY = yAnt
    
    vp.TextAlign = xtalign
    vp.BrushColor = xbrushcolor
    vp.PenColor = xpencolor
    vp.TextAlign = xAjusteAnt
    vp.TextColor = sColorTextoAnt
    
    '    vp.TextColor = xtextcolor
    '    vp.FontSize = xfontsize
    '    vp.MarginLeft = xmarginleft

End Sub

Private Function rTwips(pMM As Variant)
    Dim i As Integer, s As String, d As Double
    If VarType(pMM) <> vbString Then
        rTwips = pMM
    Else
        i = InStr(1, pMM, "mm")
        If i = 0 Then
            MsgBox "Error de conversion de mm a twips"
            End
        End If
        
        s = Mid$(pMM, 1, i - 1)
        
        s = Replace(s, ".", gstrSepDec)
        s = Replace(s, ",", gstrSepDec)
        
        d = CDbl(s)
        
        ' rTwips = ((((d * 254) / 1440) + 0.5) * 567) / 100 + 0.5
        rTwips = 56.52 * d
        
    End If
End Function

Private Function Twips2mm(cTwips As Variant)
    Twips2mm = (cTwips / 56.52)
End Function

Function formato_rut(rut_bruto)
    Dim rut_salida, dv
    'Dim temp(3)
    Dim Rut
    Dim f
    Dim rut_salida3
    Dim rut_salida2
    Dim rut_salida1
    
    rut_salida = Trim(rut_bruto)
    dv = Right(rut_salida, 1)
    Rut = Left(rut_salida, Len(rut_salida) - 2)
    f = Len(Rut)
    
    If f > 6 Then
        rut_salida3 = Mid(Rut, f - 2, 3)
        f = f - 3
        rut_salida2 = Mid(Rut, f - 2, 3)
        f = f - 3
        If f < 3 Then
            rut_salida1 = Left(Rut, f)
        End If
        formato_rut = rut_salida1 & "." & rut_salida2 & "." & rut_salida3 & "-" & dv
    Else
        formato_rut = rut_bruto
    End If
End Function

Private Function DameInicioDeMes(sFecha As String) As String
    Dim sFechaTmp As String
    Dim sMes As String
    
    'Dim sFormatoFecha As String
    
    'sFormatoFecha = Obtener_Simbolo(LOCALE_SSHORTDATE)
    
    If Month(sFecha) < 10 Then
        sMes = "0" & Month(sFecha)
    Else
        sMes = Month(sFecha)
    End If
    
    sFechaTmp = "01/" & sMes & "/" & Year(sFecha)
    
    DameInicioDeMes = sFechaTmp
    
End Function

Private Function Obtener_Simbolo(Valor As Long) As String
   Dim Simbolo  As String
   Dim r1       As Long
   Dim r2       As Long
   Dim p        As Integer
   Dim Locale   As Long
   
   'Locale = GetUserDefaultLCID()
   r1 = GetLocaleInfo(Locale, Valor, vbNullString, 0)
   
   'buffer
   Simbolo = String$(r1, 0)
   
   'En esta llamada devuelve el s�mbolo en el Buffer
   r2 = GetLocaleInfo(Locale, Valor, Simbolo, r1)
   
   'Localiza el espacio nulo de la cadena para eliminarla
   p = InStr(Simbolo, Chr$(0))
   
   If p > 0 Then
      'Elimina los nulos
      Obtener_Simbolo = Left$(Simbolo, p - 1)
   End If
   
End Function
'
' ############################## PAGINA UNO ###########################################
'
Private Sub PaginaUno(ByRef hay_cierre As Boolean)
    'Dim nMarginLeft As Variant
    'Dim nColinicio As Variant

    vp.CurrentY = vp.CurrentY + rTwips("2mm")
    pon_titulo "MONEDA : " + Cmb_Moneda.Text, "5mm", "Times New Roman", glb_tamletra_titulo
    vp.CurrentY = vp.CurrentY + rTwips("2mm")
   
    FilaAux = vp.CurrentY
    
    CuadroActivos
    
    RentabilidadCartera
    
    FlujoPatrimonial
    
    FilaAux = vp.CurrentY
    
    DistribucionActivos
    
'    Call fncCuentaInversion(hay_cierre)
'    If Not Hay_Cierre Then Exit Sub
'    vp.CurrentY = iFilaTermino + rTwips("2mm")
'    CuadroResumen
'    vp.CurrentY = iFilaTermino + rTwips("1mm")
'    FlujoPatrimonial


End Sub
'Function UltimoDiaDelMes(Fecha_proceso As String) As String
'    UltimoDiaDelMes = DateAdd("d", -Day(Fecha_proceso), Fecha_proceso)
'
'End Function

Sub CuadroActivos()
    Dim lCursor As Object
    Dim lCursor_rama As Object
    Dim lReg As Object
    Dim lreg_rama As Object
       
    Dim a As Integer
    Dim suma As Double
    Dim Patrimonio_Actual As Double
    Dim MontoCobrar_Actual  As Double
    Dim MontoPagar_Actual As Double
    Dim MontoTotalCajas_Actual As Double
    
    Dim Patrimonio_Anterior As Double
    Dim MontoCobrar_Anterior  As Double
    Dim MontoPagar_Anterior As Double
    Dim MontoTotalCajas_Anterior As Double
    
    Dim Fecha_Mes_anterior As String
    
    Dim Decimales As Integer
    
    Dim dsc_caja_cuenta  As String
    
    Dim nTotalActivos_Actual As Double
    Dim nTotalActivos_Anterior As Double
    Dim nTotalPasivos_Actual As Double
    Dim nTotalPasivos_Anterior As Double
       
    Dim Mostrado As Integer
    Dim nTotal_Actual  As Double
    Dim nTotal_Anterior  As Double
    
    Dim iFila As Integer
    Dim cadena As String
    
    nTotalActivos_Actual_Para_Porcentajes = 0
    nTotalActivos_Anterior_Para_Porcentajes = 0
    
    Decimales = oCliente.Decimales
    
    Fecha_Mes_anterior = UltimoDiaDelMes(oCliente.Fecha_Cartola)
    
    ' Patrimonio : queda en las variables expuestas
    nTotalActivos_Actual_Para_Porcentajes = oCliente.DamePatrimonio("", Patrimonio_Actual, MontoCobrar_Actual, MontoPagar_Actual)
    nTotalActivos_Anterior_Para_Porcentajes = oCliente.DamePatrimonio(Fecha_Mes_anterior, Patrimonio_Anterior, MontoCobrar_Anterior, MontoPagar_Anterior)
    
    oCliente.TotalCajas "", MontoTotalCajas_Actual
    oCliente.TotalCajas Fecha_Mes_anterior, MontoTotalCajas_Anterior
    
    a = 1
    suma = 0
    
    nTotalPasivos_Actual = 0
    nTotalPasivos_Anterior = 0
    nTotalActivos_Actual = 0
    nTotalActivos_Anterior = 0
    
    nTotalActivos_Actual_Para_Porcentajes = nTotalActivos_Actual_Para_Porcentajes + MontoTotalCajas_Actual + MontoCobrar_Actual
    nTotalActivos_Anterior_Para_Porcentajes = nTotalActivos_Anterior_Para_Porcentajes + MontoTotalCajas_Anterior + MontoCobrar_Anterior
    
    cadena = ""
    
    With vp
        .CurrentY = .CurrentY + rTwips("2mm")
        FilaAux = vp.CurrentY
        .StartTable
        .TableBorder = tbBox
        .TableCell(tcCols) = 5
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcFontSize, 1, 1, 1, 5) = glb_tamletra_registros
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcText, 1, 1) = "ACTIVOS"
        .TableCell(tcText, 1, 2) = "Valor Actual " & oCliente.Fecha_Cartola ' Fecha
        .TableCell(tcColSpan, 1, 2) = 2
        .TableCell(tcText, 1, 4) = "Valor Anterior " & Fecha_Mes_anterior ' Fecha
        .TableCell(tcColSpan, 1, 4) = 2
        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "38mm"
        .TableCell(tcColWidth, 1, 2) = "27mm"
        .TableCell(tcColWidth, 1, 3) = "15mm"
        .TableCell(tcColWidth, 1, 4) = "28mm"
        .TableCell(tcColWidth, 1, 5) = "15mm"
        ' Alineacion de cada columna
        .TableCell(tcAlign, 1, 1) = taLeftMiddle
        .TableCell(tcAlign, 1, 2) = taRightMiddle
        .TableCell(tcAlign, 1, 3) = taRightMiddle
        .TableCell(tcAlign, 1, 4) = taRightMiddle
        .TableCell(tcAlign, 1, 5) = taRightBottom
        .TableCell(tcRowHeight, 1) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        '************************************************************************
        ' Caja
        '************************************************************************
         dsc_caja_cuenta = "Caja"
        .TableCell(tcText, iFila, 1) = dsc_caja_cuenta
        .TableCell(tcText, iFila, 2) = FormatNumber(MontoTotalCajas_Actual, Decimales)
        .TableCell(tcText, iFila, 3) = FormatPorcentaje(MontoTotalCajas_Actual, nTotalActivos_Actual_Para_Porcentajes)
        .TableCell(tcText, iFila, 4) = FormatNumber(MontoTotalCajas_Anterior, Decimales)
        .TableCell(tcText, iFila, 5) = FormatPorcentaje(MontoTotalCajas_Anterior, nTotalActivos_Anterior_Para_Porcentajes)
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        '************************************************************************
        ' Cuentas por Cobrar
        '************************************************************************
        .TableCell(tcText, iFila, 1) = "Cuentas por Cobrar "
        .TableCell(tcText, iFila, 2) = FormatNumber(MontoCobrar_Actual, Decimales)
        .TableCell(tcText, iFila, 3) = FormatPorcentaje(MontoCobrar_Actual, nTotalActivos_Actual_Para_Porcentajes)
        .TableCell(tcText, iFila, 4) = FormatNumber(MontoCobrar_Anterior, Decimales)
        .TableCell(tcText, iFila, 5) = FormatPorcentaje(MontoCobrar_Anterior, nTotalActivos_Anterior_Para_Porcentajes)
        nTotalActivos_Actual = nTotalActivos_Actual + MontoCobrar_Actual
        nTotalActivos_Anterior = nTotalActivos_Anterior + MontoCobrar_Anterior
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        a = 1
        Mostrado = 0
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Activos %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        gDB.Parametros.Clear
        gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
        gDB.Parametros.Add "pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
        gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, DTP_Fecha_Ter.Value, ePD_Entrada
        gDB.Parametros.Add "PId_Moneda_Salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada    'Agregado por MMA 09/09/2008
        gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_ARBOLES"
        If Not gDB.EjecutaSP Then
            Exit Sub
        End If
        Set lCursor = gDB.Parametros("Pcursor").Valor
        gDB.Parametros.Clear
        nTotalActivos_Actual = nTotalActivos_Actual + MontoTotalCajas_Actual
        nTotalActivos_Anterior = nTotalActivos_Anterior + MontoTotalCajas_Anterior
        For Each lReg In lCursor
            gDB.Parametros.Clear
            gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
            gDB.Parametros.Add "pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
            gDB.Parametros.Add "PID_ARBOL_CLASE_INST", ePT_Numero, lReg("id").Value, ePD_Entrada
            gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, DTP_Fecha_Ter.Value, ePD_Entrada
            gDB.Parametros.Add "PId_Moneda_Salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada    'Agregado por MMA 09/09/2008
            gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ABRE_RAMA"
            If Not gDB.EjecutaSP Then
                Exit Sub
            End If
            Set lCursor_rama = gDB.Parametros("Pcursor").Valor
            gDB.Parametros.Clear
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            .TableCell(tcText, .TableCell(tcRows), 1) = UCase(lReg("DSC_ARBOL_CLASE_INST").Value)
            .TableCell(tcText, .TableCell(tcRows), 2) = FormatNumber(lReg("monto_mon_cta").Value, oCliente.Decimales)
            .TableCell(tcText, .TableCell(tcRows), 3) = FormatPorcentaje(lReg("monto_mon_cta").Value, nTotalActivos_Actual_Para_Porcentajes)
            .TableCell(tcBackColor, .TableCell(tcRows)) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
            .TableCell(tcForeColor, .TableCell(tcRows)) = RGB(255, 255, 255)    ' BLANCO
            .TableCell(tcText, .TableCell(tcRows), 4) = FormatNumber(lReg("MONTO_MON_CTA_ANTERIOR").Value, oCliente.Decimales)
            .TableCell(tcText, .TableCell(tcRows), 5) = FormatPorcentaje(lReg("MONTO_MON_CTA_ANTERIOR").Value, nTotalActivos_Anterior_Para_Porcentajes)
            .TableCell(tcFontSize, .TableCell(tcRows), 1, .TableCell(tcRows), 5) = glb_tamletra_registros
            .TableCell(tcAlign, .TableCell(tcRows), 1, .TableCell(tcRows), 5) = taCenterMiddle
            .TableCell(tcRowHeight, .TableCell(tcRows)) = rTwips("4.6mm")
            For Each lreg_rama In lCursor_rama
                .TableCell(tcRows) = .TableCell(tcRows) + 1
                iFila = .TableCell(tcRows)
                nTotal_Actual = 0
                nTotal_Anterior = 0
                .TableCell(tcText, iFila, 1) = lreg_rama("DSC_ARBOL_CLASE_INST").Value
                .TableCell(tcText, iFila, 2) = FormatNumber(lreg_rama("monto_actual").Value, oCliente.Decimales)
                .TableCell(tcText, iFila, 3) = FormatPorcentaje(lreg_rama("monto_actual").Value, nTotalActivos_Actual_Para_Porcentajes)
                .TableCell(tcText, iFila, 4) = FormatNumber(lreg_rama("monto_anterior").Value, oCliente.Decimales)
                .TableCell(tcText, iFila, 5) = FormatPorcentaje(lreg_rama("monto_anterior").Value, nTotalActivos_Anterior_Para_Porcentajes)
                .TableCell(tcFontSize, iFila, 1, iFila, 5) = glb_tamletra_registros
                .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
                iFila = .TableCell(tcRows)
            Next
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
            nTotalActivos_Actual = nTotalActivos_Actual + lReg("monto_mon_cta").Value
            nTotalActivos_Anterior = nTotalActivos_Anterior + lReg("monto_mon_cta_anterior").Value
         Next
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        .TableCell(tcText, iFila, 1) = "TOTAL ACTIVOS"
        .TableCell(tcText, iFila, 2) = FormatNumber(nTotalActivos_Actual, oCliente.Decimales)
        .TableCell(tcText, iFila, 3) = FormatPorcentaje(nTotalActivos_Actual, nTotalActivos_Actual)
        .TableCell(tcText, iFila, 4) = FormatNumber(nTotalActivos_Anterior, oCliente.Decimales)
        .TableCell(tcText, iFila, 5) = FormatPorcentaje(nTotalActivos_Anterior, nTotalActivos_Anterior)
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcFontSize, 1, 1, iFila, 5) = glb_tamletra_subtitulo1
        .TableCell(tcFontSize, iFila, 1, iFila, 5) = glb_tamletra_subtitulo1 + 1
        .TableCell(tcBackColor, iFila) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        .TableCell(tcAlign, 1, 1, .TableCell(tcRows), 1) = taLeftMiddle
        .TableCell(tcAlign, 1, 2, .TableCell(tcRows), 5) = taRightMiddle
        .EndTable
        '######################## PASIVOS #########################################
        .StartTable
        .TableBorder = tbBox
        .TableCell(tcCols) = 5
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcText, 1, 1) = "PASIVOS"
        .TableCell(tcText, 1, 2) = ""
        .TableCell(tcColSpan, 1, 2) = 2
        .TableCell(tcText, 1, 4) = ""
        .TableCell(tcColSpan, 1, 4) = 2
        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "38mm"
        .TableCell(tcColWidth, 1, 2) = "27mm"
        .TableCell(tcColWidth, 1, 3) = "15mm"
        .TableCell(tcColWidth, 1, 4) = "28mm"
        .TableCell(tcColWidth, 1, 5) = "15mm"
        ' Alineacion de cada columna
        .TableCell(tcAlign, 1, 1) = taLeftMiddle
        .TableCell(tcAlign, 1, 2) = taRightMiddle
        .TableCell(tcAlign, 1, 3) = taRightMiddle
        .TableCell(tcAlign, 1, 4) = taRightMiddle
        .TableCell(tcAlign, 1, 5) = taRightMiddle
        .TableCell(tcRowHeight, 1) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        '************************************************************************
        ' Administracion
        '************************************************************************
        .TableCell(tcText, iFila, 1) = "Comisi�n por Adm. "
        .TableCell(tcText, iFila, 2) = "0"
        .TableCell(tcText, iFila, 3) = ""
        .TableCell(tcText, iFila, 4) = "0"
        .TableCell(tcText, iFila, 5) = ""
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        nTotalPasivos_Actual = nTotalPasivos_Actual + 0
        nTotalPasivos_Anterior = nTotalPasivos_Anterior + 0
        '************************************************************************
        ' Cuentas por Pagar
        '************************************************************************
        .TableCell(tcText, iFila, 1) = "Cuentas por Pagar"
        .TableCell(tcText, iFila, 2) = FormatNumber(MontoPagar_Actual, Decimales)
        .TableCell(tcText, iFila, 3) = ""  ' FormatPorcentaje(MontoPagar_Actual, Patrimonio_Actual)
        .TableCell(tcText, iFila, 4) = FormatNumber(MontoPagar_Anterior, Decimales)
        .TableCell(tcText, iFila, 5) = ""  ' FormatPorcentaje(MontoPagar_Anterior, Patrimonio_Anterior)
        .TableCell(tcFontSize, 1, 1, iFila, 5) = glb_tamletra_subtitulo1
        nTotalPasivos_Actual = nTotalPasivos_Actual + MontoPagar_Actual
        nTotalPasivos_Anterior = nTotalPasivos_Anterior + MontoPagar_Anterior
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 2
        iFila = .TableCell(tcRows)
        .TableCell(tcText, iFila, 1) = "TOTAL PASIVOS"
        .TableCell(tcText, iFila, 2) = FormatNumber(nTotalPasivos_Actual, oCliente.Decimales)
        .TableCell(tcText, iFila, 3) = ""
        .TableCell(tcText, iFila, 4) = FormatNumber(nTotalPasivos_Anterior, oCliente.Decimales)
        .TableCell(tcText, iFila, 5) = ""
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcFontSize, iFila, 1, iFila, 5) = glb_tamletra_subtitulo1 + 1
        .TableCell(tcBackColor, iFila) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        .TableCell(tcAlign, 1, 1, .TableCell(tcRows), 1) = taLeftMiddle
        .TableCell(tcAlign, 1, 2, .TableCell(tcRows), 5) = taRightMiddle
        .EndTable
        '************************************************************************
        ' Patrimonio
        '************************************************************************
        .StartTable
        .TableBorder = tbBox
        .TableCell(tcCols) = 5
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcFontSize, 1, 1, 1, 5) = glb_tamletra_subtitulo1 + 1
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "38mm"
        .TableCell(tcColWidth, 1, 2) = "27mm"
        .TableCell(tcColWidth, 1, 3) = "15mm"
        .TableCell(tcColWidth, 1, 4) = "28mm"
        .TableCell(tcColWidth, 1, 5) = "15mm"
        .TableCell(tcText, 1, 1) = "PATRIMONIO"
        Patrimonio_Actual = nTotalActivos_Actual - nTotalPasivos_Actual
        Patrimonio_Anterior = nTotalActivos_Anterior - nTotalPasivos_Anterior
        .TableCell(tcText, 1, 2) = FormatNumber(Patrimonio_Actual, Decimales)
        .TableCell(tcText, 1, 3) = ""
        .TableCell(tcText, 1, 4) = FormatNumber(Patrimonio_Anterior, Decimales)
        .TableCell(tcText, 1, 5) = ""
        .TableCell(tcAlign, 1, 1, .TableCell(tcRows), 1) = taLeftMiddle
        .TableCell(tcAlign, 1, 2, .TableCell(tcRows), 5) = taRightMiddle
        .TableCell(tcRowHeight, iFila) = rTwips("4.6mm")
        .EndTable
    End With
End Sub

Function SumaTotalClase(ByVal lCursor As hRecord, _
                        ByVal sClase As String, _
                        ByRef nTotal_Actual As Double, _
                        ByRef nTotal_Anterior As Double) As Double
    'Dim lReg As hFields
    
    nTotal_Anterior = 0
    nTotal_Actual = 0
    
'    For Each lReg In lCursor
'        'If sClase = lreg("DSC_ARBOL_CLASE_INST").Value Then
'            'nTotal_Actual = nTotal_Actual + CDbl(lreg("MONTO_MON_CTA").Value)
'            'nTotal_Anterior = nTotal_Anterior + CDbl(lreg("Monto_Anterior").Value)
'        'End If
'    Next
   
End Function
Function SumaTotalActivos(ByVal lCursor As hRecord, _
                        ByRef nTotal_Actual As Double, _
                        ByRef nTotal_Anterior As Double) As Double
    'Dim lReg As hFields
    
    nTotal_Anterior = 0
    nTotal_Actual = 0
    
'    For Each lReg In lCursor
        'nTotal_Actual = nTotal_Actual + CDbl(lReg("Monto_Actual").Value)
        'nTotal_Anterior = nTotal_Anterior + CDbl(lReg("Monto_Anterior").Value)
'    Next
   
End Function

Sub RentabilidadCartera()
    Dim lCursor As hRecord
    Dim lReg As hFields
    Dim iFila As Integer
    
    Dim Xinicio As Variant
    Dim YInicio As Variant
    
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
    gDB.Parametros.Add "pId_Moneda_Salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
    gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS$RENTABILIDADES"
    
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor = gDB.Parametros("Pcursor").Valor

    gDB.Parametros.Clear

    With vp
        vp.MarginLeft = COLUMNA_DOS ' + rTwips("150mm")
        Xinicio = vp.MarginLeft
        YInicio = FilaAux
        'YInicio = 5557 + rTwips("5mm")
        .LineSpacing = nInterlineado
        .CurrentY = YInicio
        FilaAux = vp.CurrentY

        .StartTable
        .TableBorder = tbBox

        .TableCell(tcCols) = 4

        .TableCell(tcRows) = .TableCell(tcRows) + 1
        
        .TableCell(tcFontSize, 1, 1, 1, 4) = glb_tamletra_subtitulo2
        .TableCell(tcBackColor, 1, 1, 1, 4) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcRowHeight, 1) = rTwips("4.6mm")
        .TableCell(tcText, 1, 1) = " RENTABILIDADES "
        .TableCell(tcText, 1, 2) = "  $     "
        .TableCell(tcText, 1, 3) = " UF   "
        .TableCell(tcText, 1, 4) = "US$ "
        '.TableCell(tcText, 1, 5) = "Volatilidad"
        
        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "70mm"
        .TableCell(tcColWidth, 1, 2) = "20mm"
        .TableCell(tcColWidth, 1, 3) = "18mm"
        .TableCell(tcColWidth, 1, 4) = "17mm"
        
        .TableCell(tcColAlign, 1, 1, 1, 1) = taLeftTop
        .TableCell(tcColAlign, 1, 2, 1, 4) = taCenterMiddle
        '.TableCell(tcColWidth, 1, 4) = "28mm"
        '.TableCell(tcColWidth, 1, 5) = "15mm"
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        For Each lReg In lCursor
            .TableCell(tcText, iFila, 1) = "Rentabilidad Mensual"
            '.TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_mensual_$$"), 2, True) & "%"
            .TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_mensual_$$").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 4) = FormatNumber(lReg("rentabilidad_mensual_DO").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 3) = FormatNumber(lReg("rentabilidad_mensual_UF").Value, 2, True) & "%"
            '.TableCell(tcText, iFila, 5) = lReg("volatilidad")
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
            .TableCell(tcText, iFila, 1) = "Rentabilidad Acumulada Anual"
            '.TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_anual_$$"), 2, True) & "%"
            .TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_anual_$$"), 2, True) & "%"  'MMA 09/09/2008
            .TableCell(tcText, iFila, 4) = FormatNumber(lReg("rentabilidad_anual_DO"), 2, True) & "%"
            .TableCell(tcText, iFila, 3) = FormatNumber(lReg("rentabilidad_anual_UF"), 2, True) & "%"

            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
    'Modificado por MMA. 11/11/2008
            .TableCell(tcText, iFila, 1) = "" '"Rentabilidad �ltimos 12 meses"
            '.TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_ult_12_meses_$$"), 2, True) & "%"
            .TableCell(tcText, iFila, 2) = "" 'FormatNumber(lReg("rentabilidad_ult_12_meses_$$"), 2, True) & "%"  'MMA 09/09/2008
            .TableCell(tcText, iFila, 4) = "" 'FormatNumber(lReg("rentabilidad_ult_12_meses_DO"), 2, True) & "%"
            .TableCell(tcText, iFila, 3) = "" 'FormatNumber(lReg("rentabilidad_ult_12_meses_UF"), 2, True) & "%"
        Next
        ' Alineacion de cada columna
        .TableCell(tcAlign, , 1) = taLeftMiddle
        .TableCell(tcAlign, , 2) = taRightMiddle
        .TableCell(tcAlign, , 3) = taRightMiddle
        .TableCell(tcAlign, , 4) = taRightMiddle
        '.TableCell(tcAlign, , 4) = taRightMiddle
        '.TableCell(tcAlign, , 5) = taRightMiddle
        .TableCell(tcFontSize, 1, 1, 4, 4) = glb_tamletra_registros
        .EndTable
    End With
    
    Set lCursor = Nothing
    Set lReg = Nothing
End Sub

Sub FlujoPatrimonial()
    Dim lCursor As hRecord
    Dim lReg As hFields
    Dim iFila As Integer
    Dim Xinicio As Variant
    Dim YInicio As Variant
    Dim i As Integer
    Dim j As Integer
    
    Dim lTotal_Aportes As Double
    Dim lTotal_Retiros As Double
    
    Dim lTotal_Aportes_UF As Double
    Dim lTotal_Retiros_UF As Double

    Dim lTotal_Neto As Double
    
    'PKG_CAJAS_CUENTA$Buscar

    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_inicio", ePT_Fecha, CDate(oCliente.Fecha_Creacion), ePD_Entrada
    gDB.Parametros.Add "Pfecha_final", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
    gDB.Parametros.Add "Pid_Moneda_Salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada   'MMA 09/09/2008
    
    gDB.Procedimiento = "PKG_FLUJO_PATRIMONIAL$Buscar"

    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor = gDB.Parametros("Pcursor").Valor
    With vp
        vp.MarginLeft = COLUMNA_DOS ' + rTwips("150mm")
        Xinicio = .MarginLeft
        YInicio = .CurrentY + rTwips("2.7mm")
        .LineSpacing = nInterlineado
        'YInicio = 3120 + rTwips("5mm")
        .CurrentY = YInicio
        FilaAux = .CurrentY

        .StartTable
        .TableBorder = tbBox

        .TableCell(tcCols) = 3

        .TableCell(tcRows) = .TableCell(tcRows) + 1

        .TableCell(tcFontSize, 1, 1, 3, 3) = glb_tamletra_subtitulo1
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
        
        '.TableCell(tcBackColor, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        '.TableCell(tcBackColor, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)

        .TableCell(tcText, 1, 1) = "FLUJO PATRIMONIAL"
        .TableCell(tcText, 1, 2) = "APORTES"
        .TableCell(tcText, 1, 3) = "RETIROS"
        
        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "55mm"
        .TableCell(tcColWidth, 1, 2) = "35mm"
        .TableCell(tcColWidth, 1, 3) = "35mm"
        .TableCell(tcRowHeight, 1) = rTwips("4.6mm")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        
        lTotal_Aportes_UF = 0
        lTotal_Retiros_UF = 0
        
        i = 1

        For Each lReg In lCursor
            If i = 10 Then Exit For
            .TableCell(tcText, iFila, 1) = lReg("FECHA_MOVIMIENTO")
            If Mid(lReg("FLG_TIPO_MOVIMIENTO"), 1, 1) = "A" Then
                  .TableCell(tcText, iFila, 2) = FormatNumber(lReg("monto").Value, oCliente.Decimales)
                  .TableCell(tcText, iFila, 3) = "-"
            Else
                  .TableCell(tcText, iFila, 2) = "-"
                  .TableCell(tcText, iFila, 3) = FormatNumber(lReg("monto").Value, oCliente.Decimales)
            End If
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
            i = i + 1
            lTotal_Aportes = lReg("MONTO_TOTAL_APORTES").Value
            lTotal_Retiros = lReg("MONTO_TOTAL_RESCATES").Value
            lTotal_Aportes_UF = lReg("MONTO_TOTAL_APORTES_UF").Value
            lTotal_Retiros_UF = lReg("MONTO_TOTAL_RESCATES_UF").Value
            .TableCell(tcFontSize, iFila, 1, iFila, 3) = glb_tamletra_registros
        Next
        
        For j = i To 10
            .TableCell(tcText, iFila, 1) = ""
            .TableCell(tcText, iFila, 2) = ""
            .TableCell(tcText, iFila, 3) = ""
            
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
        Next
        
        '.TableCell(tcRows) = .TableCell(tcRows) + 1
        'Modificado por MMA.22/10/2008
        'Modificado por MMA. 04/11/2008. Ahora no necesitan saldar el FP
        'lTotal_Neto = lTotal_Aportes - lTotal_Retiros
        
        .TableCell(tcText, .TableCell(tcRows), 1) = "TOTAL EN " & fDsc_Moneda_Salida    ' Trim(Moneda_Cuentas)  MMA 09/09/2008
'        If lTotal_Neto > 0 Then
'            .TableCell(tcText, .TableCell(tcRows), 2) = FormatNumber(lTotal_Aportes, oCliente.Decimales) 'FormatNumber(lTotal_Neto, oCliente.Decimales)
'            .TableCell(tcText, .TableCell(tcRows), 3) = "" 'FormatNumber(lTotal_Retiros, oCliente.Decimales)
'        Else
'            .TableCell(tcText, .TableCell(tcRows), 2) = ""
'            .TableCell(tcText, .TableCell(tcRows), 3) = FormatNumber(lTotal_Retiros, oCliente.Decimales) 'FormatNumber(lTotal_Neto, oCliente.Decimales)
'        End If
        .TableCell(tcText, .TableCell(tcRows), 2) = FormatNumber(lTotal_Aportes, oCliente.Decimales)
        .TableCell(tcText, .TableCell(tcRows), 3) = FormatNumber(lTotal_Retiros, oCliente.Decimales)
        
        .TableCell(tcBackColor, .TableCell(tcRows)) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, .TableCell(tcRows)) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 3) = glb_tamletra_registros
        ' Alineacion de cada columna
        .TableCell(tcAlign, , 1) = taLeftMiddle
        .TableCell(tcAlign, , 2) = taRightMiddle
        .TableCell(tcAlign, , 3) = taRightMiddle
        .TableCell(tcRowHeight, .TableCell(tcRows)) = rTwips("4.6mm")
        ' *********************************************************
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        'Modificado por MMA. 04/11/2008. Ahora no necesitan saldar el FP
        'lTotal_Neto = lTotal_Aportes_UF - lTotal_Retiros_UF
        
        .TableCell(tcText, .TableCell(tcRows), 1) = "TOTAL EN UF"
'        If lTotal_Neto > 0 Then
'            .TableCell(tcText, .TableCell(tcRows), 2) = FormatNumber(lTotal_Aportes_UF, 2) 'FormatNumber(lTotal_Neto, 2)
'            .TableCell(tcText, .TableCell(tcRows), 3) = ""
'        Else
'            .TableCell(tcText, .TableCell(tcRows), 2) = ""
'            .TableCell(tcText, .TableCell(tcRows), 3) = FormatNumber(lTotal_Retiros_UF, 2) 'FormatNumber(lTotal_Neto, 2)
'        End If
        .TableCell(tcText, .TableCell(tcRows), 2) = FormatNumber(lTotal_Aportes_UF, 2)
        .TableCell(tcText, .TableCell(tcRows), 3) = FormatNumber(lTotal_Retiros_UF, 2)
        
        .TableCell(tcBackColor, .TableCell(tcRows)) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, .TableCell(tcRows)) = RGB(255, 255, 255)    ' BLANCO
        .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 3) = glb_tamletra_registros
        ' Alineacion de cada columna
        .TableCell(tcAlign, , 1) = taLeftMiddle
        .TableCell(tcAlign, , 2) = taRightMiddle
        .TableCell(tcAlign, , 3) = taRightMiddle
        .TableCell(tcRowHeight, .TableCell(tcRows)) = rTwips("4.6mm")
        .EndTable
    End With
    Set lCursor = Nothing
    Set lReg = Nothing
End Sub

Sub DistribucionActivos()
    Dim lCursor As Object
    Dim lReg As Object
    
    Dim Patrimonio_Actual As Double
    Dim MontoCobrar_Actual  As Double
    Dim MontoPagar_Actual As Double
    Dim MontoTotalCajas_Actual As Double
    
    'Dim Patrimonio_Anterior As Double
    'Dim MontoCobrar_Anterior  As Double
    'Dim MontoPagar_Anterior As Double
    'Dim MontoTotalCajas_Anterior As Double
    
    Dim Fecha_Mes_anterior As String
    Dim Mostrado As Integer
    'Dim dsc_producto As String
    Dim prod_anterior As String
    'Dim dsc_intrumento As String
    
    Dim nTotal_Actual  As Double
    Dim nTotal_Anterior  As Double
    
    'Dim nTotalActivos_Actual As Double
    'Dim nTotalActivos_Anterior As Double
    
    'Dim Porc_Monto_mon_cta As Double
    
    'Dim monto_mon_cta As Double
        
    Dim Decimales As Integer
    
    Dim nMargenLeft  As Variant
    'Dim iFilaActual As Variant
    Dim j As Integer
    Dim aux As Double
    Dim aux1 As String
    
    Dim sNombres()
    Dim sValores()
    Dim i As Integer
    Dim mostrado2 As Integer
    
    i = 0
    
    Decimales = oCliente.Decimales
    
    Fecha_Mes_anterior = UltimoDiaDelMes(oCliente.Fecha_Cartola)
    
    ' Patrimonio : queda en las variables expuestas
    oCliente.DamePatrimonio "", Patrimonio_Actual, MontoCobrar_Actual, MontoPagar_Actual
    'oCliente.DamePatrimonio Fecha_Mes_anterior, Patrimonio_Anterior, MontoCobrar_Anterior, MontoPagar_Anterior
    
    oCliente.TotalCajas "", MontoTotalCajas_Actual
    'oCliente.TotalCajas Fecha_Mes_anterior, MontoTotalCajas_Anterior
    
    If MontoTotalCajas_Actual > 0 Then
        ReDim Preserve sNombres(i)
        ReDim Preserve sValores(i)
        sNombres(i) = "CAJA"
        sValores(i) = MontoTotalCajas_Actual
        i = i + 1
    End If
     
    Mostrado = 0
    mostrado2 = 0
              
    '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Activos %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'    gDB.Parametros.Clear
'    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
'    gDB.Parametros.Add "PFecha_Cartola", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
'    gDB.Parametros.Add "PFecha_Anterior", ePT_Fecha, CDate(Fecha_Mes_anterior), ePD_Entrada
'    gDB.Procedimiento = "PKG_CARTOLA$DAME_ACTIVOS_CLASE"
    
        gDB.Parametros.Clear
        gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
        gDB.Parametros.Add "pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
        gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, DTP_Fecha_Ter.Value, ePD_Entrada
        gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_ARBOLES"
    
    
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If
    
    Set lCursor = gDB.Parametros("Pcursor").Valor
    
    gDB.Parametros.Clear
     
    'SumaTotalActivos lCursor, nTotalActivos_Actual, nTotalActivos_Anterior
     
    For Each lReg In lCursor
        If Mostrado = 0 Or prod_anterior <> lReg("DSC_ARBOL_CLASE_INST").Value Then
           nTotal_Actual = 0
           nTotal_Anterior = 0
           Mostrado = 1
           prod_anterior = lReg("DSC_ARBOL_CLASE_INST").Value
           
           'If CDbl(NVL(lreg("MONTO_MON_CTA").Value, 0)) > 0 Then
           If nTotalActivos_Actual_Para_Porcentajes > 0 Then
                If Round(CDbl(NVL(lReg("MONTO_MON_CTA").Value, 0)) / nTotalActivos_Actual_Para_Porcentajes, 2) > 0 Then
                   ReDim Preserve sNombres(i)
                   ReDim Preserve sValores(i)
                   sNombres(i) = lReg("DSC_ARBOL_CLASE_INST").Value
                   'sValores(i) = nTotal_Actual
                   sValores(i) = lReg("MONTO_MON_CTA").Value
                   i = i + 1
                End If
           End If
        End If
    Next
    If i > 0 Then
        For i = LBound(sValores) To UBound(sValores)
            For j = i + 1 To UBound(sValores) - 1
                If CDbl(sValores(i)) > CDbl(sValores(j)) Then
                    aux = sValores(j)
                    sValores(j) = sValores(i)
                    sValores(i) = aux

                    aux1 = sNombres(j)
                    sNombres(j) = sNombres(i)
                    sNombres(i) = aux1
                End If
            Next
        Next
        
        If MontoCobrar_Actual > 0 Then
             'i = i + 1
             ReDim Preserve sNombres(i)
             ReDim Preserve sValores(i)
             sNombres(i) = "Cuentas por Cobrar"
             sValores(i) = MontoCobrar_Actual
             'mostrado2 = 2
             'i = i + 1
        End If
        
        
        Dim sName As String
        
        sName = fncGraficoTorta(sNombres, sValores, "Composici�n Cartera", 11, False)

        With vp
            .CurrentY = FilaAux + rTwips("5mm")
            FilaAux = .CurrentY
            'FilaAux = 7200
            nMargenLeft = .MarginLeft
            .MarginLeft = COLUMNA_DOS
            '.DrawPicture LoadPicture(sName), vp.MarginLeft + rTwips("10mm"), FilaAux, "90mm", "55mm", , False
            .DrawPicture LoadPicture(sName), vp.MarginLeft - rTwips("6mm"), FilaAux - 100, "130mm", "95mm", , False
            .MarginLeft = nMargenLeft
            .CurrentY = FilaAux
        End With
        
    End If
    
ErrProcedure:
  'Call Sub_Desbloquea_Puntero(Me)

End Sub


'############################################ PAGINA UNO ######################################

Public Sub CuadroResumen()
    Dim sName               As String

    'Dim xSql                As String
    'Dim sCodEmpresa         As String
    'Dim sFechaCreacion      As String
    'Dim sFechaTope          As String
    'Dim nMontoPatrimonio    As Double

    Dim sMonedaFondo        As String
    Dim sTituloMoneda       As String
    'Dim Titulo              As String
    'Dim fmt                 As String
    Dim cadena              As String
    'Dim hdr                 As String
    
    Dim lCursor_Cuotas As hRecord
    Dim lCampo As hFields

    ReDim Campos(0 To 8)
    ReDim glosas(0 To 8)
    ReDim anchos(0 To 8)
    ReDim formatos(0 To 8)

    'Dim nro_registros As Long
    
    sMonedaFondo = "$$"
    sTituloMoneda = "CLP"
    
    vp.CurrentY = vp.CurrentY + rTwips("1.5mm")
    pon_titulo "RENTABILIDAD Y FLUJO PATRIMONIAL", "5mm", "Times New Roman", glb_tamletra_titulo
    With vp
        .CurrentY = .CurrentY + rTwips("2mm")
        FilaAux = vp.CurrentY
        .StartTable
        .TableBorder = tbBox
        cadena = ""
        .TableCell(tcRows) = 5
        .TableCell(tcCols) = 5
        .TableCell(tcFontSize, 1, 1, 2, 5) = glb_tamletra_subtitulo2
        .TableCell(tcBackColor, 1, 1, 2, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcColBorderColor, 0, 1, 3, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcText, 1, 1) = "CUADRO RESUMEN"
        .TableCell(tcColWidth, 1, 1, 7, 1) = "39mm"
        .TableCell(tcAlign, 1, 1) = taLeftMiddle

        .TableCell(tcText, 1, 2) = "RENTABILIDAD"
        .TableCell(tcText, 1, 5) = "VOLAT."

        .TableCell(tcColSpan, 1, 2) = 3
        .TableCell(tcRowBorderColor, 1, , 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)

        .TableCell(tcText, 2, 2) = "CLP"
        .TableCell(tcText, 2, 3) = "UF"
        .TableCell(tcText, 2, 4) = "USD"
        .TableCell(tcText, 2, 5) = sTituloMoneda

        .TableCell(tcAlign, 1, 2, 2, 5) = taCenterMiddle
        .TableCell(tcColWidth, 3, 2, 7, 5) = "15mm"
        .TableCell(tcColNoWrap, 1, 1, 2, 7) = False

        .TableCell(tcAlign, 3, 1, 7, 1) = taLeftMiddle
        .TableCell(tcFontSize) = glb_tamletra_registros
        .TableCell(tcAlign, 3, 2, 7, 5) = taRightMiddle

        .TableCell(tcRowBorderAbove, 3) = 2
        
        gDB.Parametros.Clear
        gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "pid_cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
        gDB.Parametros.Add "pfecha_cierre", ePT_Fecha, DTP_Fecha_Ter.Value, ePD_Entrada
        gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS$RENTABILIDADES"
        If gDB.EjecutaSP Then
            Set lCursor_Cuotas = gDB.Parametros("pcursor").Valor
            If lCursor_Cuotas.Count > 0 Then
                For Each lCampo In lCursor_Cuotas
                    .TableCell(tcText, 3, 1) = "�LTIMOS 12 MESES"
                    If lCampo("hay_valor_cuota_ult_12_meses").Value = "SI" Then
                        .TableCell(tcText, 3, 2) = FormatNumber(lCampo("rentabilidad_ult_12_meses_$$").Value, 2, True) & "%"
                        .TableCell(tcText, 3, 3) = FormatNumber(lCampo("rentabilidad_ult_12_meses_UF").Value, 2, True) & "%"
                        .TableCell(tcText, 3, 4) = FormatNumber(lCampo("rentabilidad_ult_12_meses_DO").Value, 2, True) & "%"
                        .TableCell(tcText, 3, 5) = FormatNumber(lCampo("volatilidad").Value, 2, True) & "%"
                    Else
                        .TableCell(tcText, 3, 2) = "?" & "%"
                        .TableCell(tcText, 3, 3) = "?" & "%"
                        .TableCell(tcText, 3, 4) = "?" & "%"
                        .TableCell(tcText, 3, 5) = "?" & "%"
                    End If
                    .TableCell(tcText, 4, 1) = "A�O CALENDARIO"
                    If lCampo("hay_valor_cuota_ano_calendario").Value = "SI" Then
                        .TableCell(tcText, 4, 2) = FormatNumber(lCampo("rentabilidad_anual_$$").Value, 2, True) & "%"
                        .TableCell(tcText, 4, 3) = FormatNumber(lCampo("rentabilidad_anual_UF").Value, 2, True) & "%"
                        .TableCell(tcText, 4, 4) = FormatNumber(lCampo("rentabilidad_anual_DO").Value, 2, True) & "%"
                        .TableCell(tcText, 4, 5) = FormatNumber(lCampo("volatilidad").Value, 2, True) & "%"
                    Else
                        .TableCell(tcText, 4, 2) = "?" & "%"
                        .TableCell(tcText, 4, 3) = "?" & "%"
                        .TableCell(tcText, 4, 4) = "?" & "%"
                        .TableCell(tcText, 4, 5) = "?" & "%"
                    End If
                    .TableCell(tcText, 5, 1) = "MES"
                    If lCampo("hay_valor_cuota_mes").Value = "SI" Then
                        .TableCell(tcText, 5, 2) = FormatNumber(lCampo("rentabilidad_mensual_$$").Value, 2, True) & "%"
                        .TableCell(tcText, 5, 3) = FormatNumber(lCampo("rentabilidad_mensual_UF").Value, 2, True) & "%"
                        .TableCell(tcText, 5, 4) = FormatNumber(lCampo("rentabilidad_mensual_DO").Value, 2, True) & "%"
                        .TableCell(tcText, 5, 5) = FormatNumber(lCampo("volatilidad").Value, 2, True) & "%"
                    Else
                        .TableCell(tcText, 5, 2) = "?" & "%"
                        .TableCell(tcText, 5, 3) = "?" & "%"
                        .TableCell(tcText, 5, 4) = "?" & "%"
                        .TableCell(tcText, 5, 5) = "?" & "%"
                    End If
                Next
            Else
                .TableCell(tcText, 3, 1) = "�LTIMOS 12 MESES"
                .TableCell(tcText, 3, 2) = "?" & "%"
                .TableCell(tcText, 3, 3) = "?" & "%"
                .TableCell(tcText, 3, 4) = "?" & "%"
                .TableCell(tcText, 3, 5) = "?" & "%"
                
                .TableCell(tcText, 4, 1) = "A�O CALENDARIO"
                .TableCell(tcText, 4, 2) = "?" & "%"
                .TableCell(tcText, 4, 3) = "?" & "%"
                .TableCell(tcText, 4, 4) = "?" & "%"
                .TableCell(tcText, 4, 5) = "?" & "%"
                
                .TableCell(tcText, 5, 1) = "MES"
                .TableCell(tcText, 5, 2) = "?" & "%"
                .TableCell(tcText, 5, 3) = "?" & "%"
                .TableCell(tcText, 5, 4) = "?" & "%"
                .TableCell(tcText, 5, 5) = "?" & "%"
            End If
        Else
            .TableCell(tcText, 3, 1) = "�LTIMOS 12 MESES"
            .TableCell(tcText, 3, 2) = "?" & "%"
            .TableCell(tcText, 3, 3) = "?" & "%"
            .TableCell(tcText, 3, 4) = "?" & "%"
            .TableCell(tcText, 3, 5) = "?" & "%"
            
            .TableCell(tcText, 4, 1) = "A�O CALENDARIO"
            .TableCell(tcText, 4, 2) = "?" & "%"
            .TableCell(tcText, 4, 3) = "?" & "%"
            .TableCell(tcText, 4, 4) = "?" & "%"
            .TableCell(tcText, 4, 5) = "?" & "%"
            
            .TableCell(tcText, 5, 1) = "MES"
            .TableCell(tcText, 5, 2) = "?" & "%"
            .TableCell(tcText, 5, 3) = "?" & "%"
            .TableCell(tcText, 5, 4) = "?" & "%"
            .TableCell(tcText, 5, 5) = "?" & "%"
        End If
        gDB.Parametros.Clear
        .TableCell(tcColWidth, 1, 5, 7, 5) = "15mm"
        .TableCell(tcFontSize) = glb_tamletra_registros
        .EndTable
    End With

    vp.CurrentY = vp.CurrentY + rTwips("1mm")

    Dim nMargenLeft  As Variant
    Dim iFilaActual As Variant

    iFilaActual = vp.CurrentY

    sName = fncGraficoValorCuota()

    With vp
        .CurrentY = FilaAux + rTwips("1mm")
        nMargenLeft = .MarginLeft
        .MarginLeft = COLUMNA_INICIO

        .DrawPicture LoadPicture(sName), vp.MarginLeft, vp.CurrentY, "90mm", "57mm", , False ' True

        .MarginLeft = nMargenLeft
        .CurrentY = iFilaActual
    End With
    iFilaTermino = iFilaActual
    
End Sub
Sub fncDisclaimer()
    Dim sTxt As String
    vp.FontSize = 8
    If (vp.CurrentY + rTwips("30mm")) > (vp.PageHeight - vp.MarginBottom - rTwips("10mm")) Then
        vp.NewPage
    Else
        vp.CurrentY = (vp.PageHeight - vp.MarginBottom - rTwips("30mm"))
    End If
    vp.BrushColor = vbWhite
    vp.DrawRectangle vp.MarginLeft - rTwips("0mm"), vp.CurrentY, (vp.PageWidth - vp.MarginRight) + rTwips("0mm"), vp.CurrentY + rTwips("30mm")
    sTxt = "AVISO IMPORTANTE"
    vp.Paragraph = sTxt
    sTxt = "" & vbCrLf
    sTxt = sTxt & "El presente documento ha sido elaborado por Corredora Gesti�n de Inversiones S.A., de conformidad con el Mandato para la Administraci�n de Cartera celebrado entre el Cliente y Moneda Gesti�n de Inversiones S.A. el cual se entiende formar parte del presente instrumento."
    sTxt = sTxt & "Corredora Gesti�n de Inversiones S.A. no asume ninguna responsabilidad de cualquier informaci�n impl�cita o expl�cita que pudiera contener este documento."
    sTxt = sTxt & "La informaci�n contenida en el presente instrumento es confidencial y est� dirigida exclusivamente al destinatario. Cualquier uso, reproducci�n, divulgaci�n o distribuci�n por otras personas se encuentra estrictamente prohibida, acarreando su incumplimiento sanciones penales de conformidad a la ley. "
    sTxt = sTxt & vbCrLf
    vp.Paragraph = sTxt
End Sub
Private Sub haz_linea(pY As Variant, Optional bColor As Boolean = False)
    Dim vColorAnt As Variant
    Dim vColorAnt2 As Variant
    vColorAnt = vp.PenColor
    vColorAnt2 = vp.TextColor
    'If bColor Then
        'vp.PenColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno) 'RGB(0, 62, 134) 'RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        vp.TextColor = 0
    'End If
    vp.DrawLine vp.MarginLeft, pY, vp.PageWidth - vp.MarginRight, pY
    'vp.DrawLine 0, 0, 0, 0
    'vp.DrawLine 0, 0, 0, 0
    vp.PenColor = vColorAnt
    vp.TextColor = vColorAnt2
End Sub
Function Monto_Moneda(ByVal sCodMoneda As String) As String
    Dim s As String
    s = UCase(Trim(sCodMoneda))
    Select Case s
        Case "PESO"
            Monto_Moneda = "$"
        Case "DO"
            Monto_Moneda = "USD"
        Case Else
            Monto_Moneda = s
    End Select
End Function
Function Dame_Decimales(ByVal IDMoneda As Integer)
Dim Cursor_1
Dim iDecimales As Integer
        
        iDecimales = 0
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Pid_Moneda", ePT_Numero, IDMoneda, ePD_Entrada
        gDB.Procedimiento = "PKG_MONEDAS$Buscar"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set Cursor_1 = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear
    iDecimales = Cursor_1(1)("dicimales_mostrar").Value
Fin:
    Dame_Decimales = iDecimales
End Function

Function fncGraficoTorta(catnom, valores, sTitulo, tipo, bLeyenda)
    Dim xColorTitulo As String
    Dim xColor As String
    Dim aColors()
    Dim cd, c
    Dim Path As String
    Dim sFname As String
    Dim xColor1 As String
    Dim xColor2 As String
    Dim xColor3 As String
    Dim xColor4 As String
    Dim pCadena As String
    Dim i As Integer
    Dim j As Integer
    Dim lMatrix
   
    xColor1 = ColorToHex(120, 20, 1) ' CELESTE
    xColor2 = ColorToHex(1, 1, 1) ' AZUL
    xColor3 = ColorToHex(1, 1, 1) ' MAS CELESTE
    xColor4 = ColorToHex(1, 1, 1)      '(204, 22, 22) ' ROJO

    xColorTitulo = ColorToHex(0, 0, 0)
    xColor = ColorToHex(RColorRelleno, GColorRelleno, BColorRelleno)
    
    j = -1
    For i = 0 To UBound(valores)
        j = j + 1
        If j > 3 Then
            j = 1
        End If
        Select Case j
            Case 0
                pCadena = xColor1 & ";"
            Case 1
                pCadena = pCadena & xColor2 & ";"
            Case 2
                pCadena = pCadena & xColor3 & ";"
            Case 3
                pCadena = pCadena & xColor4 & ";"
'            Case 3
'                pCadena = pCadena & xColor4 & ";"
        End Select
    
    Next
    pCadena = Mid(Trim(pCadena), 1, Len(Trim(pCadena)) - 1)
    aColors = Array(ColorToHex(0, 0, 102), _
                    ColorToHex(204, 0, 0), _
                    ColorToHex(101, 76, 254))
    lMatrix = Split(pCadena, ";")
                    
'    aColors = Array(&HFF066, _
'                    &HADBDF1, _
'                    &H654CFE) ', _
'                    &HADBDF1, _
'                    &HF1BDE, _
'                    &H8A8077, _
'                    &HEEF5F1, _
'                    &HBDB8B3, _
'                    &HD6C8BA, _
'                    &HC2B5A7, _
'                    &HC4B7A9, _
'                    &H403D30)
                    
    
    Set cd = CreateObject("ChartDirector.API")
    
    cd.setLicenseCode ("YT5ZJ59NKAF9ZR3942DAE53E")

    Set c = cd.PieChart(680, 370) 'Set c = cd.PieChart(900, 350)

    Call c.setPieSize(340, 95, 90)  'Call c.setpiesize(450, 140, 250)

    Call c.set3D(20) 'Call c.set3D(40, 75)
    
    Call c.setLabelLayout(cd.SideLayout)
    'Call c.setLabelFormat("<*size=10*><*block*>{label}<*br*>{percent}%<*/*>")

    ' Set the label box background color the same as the sector color, with glass
    ' effect, and with 5 pixels rounded corners
    Dim t As Object 'cd.TextBox
    Set t = c.setLabelStyle("arial.ttf", 9, RGB(255, 255, 255))
    Call t.setBackground(cd.SameAsMainColor, cd.Transparent, cd.softlighting) 'cd.glassEffect())
    Call t.setRoundedCorners(5)

    ' Set the border color of the sector the same color as the fill color. Set the
    ' line color of the join line to black (0x0)
    Call c.setLineColor(cd.SameAsMainColor, &H0)


    Call c.setStartAngle(120) '120, False)

    Call c.setColors2(cd.DataColor, lMatrix)
    Call c.SetData(valores, catnom)

    sTitulo = "<*size=12*><*block*><*color=" & xColorTitulo & "*>" & sTitulo & "<*/*>"

    Call c.addTitle(sTitulo, "arial.ttf", 16, xColorTitulo)
    Call c.addTitle(sTitulo, "Times New Roman.ttf", 16, xColorTitulo)
    
    'Call c.setLabelPos(50, cd.LineColor)

    'Call c.setColors(aColors)

    'Path = App.Path & "\" '    App.Path ' Left(Request.ServerVariables("PATH_INFO"), InStrRev(Request.ServerVariables("PATH_INFO"), "/graficos/"))
    
    'MODIFICADO PARA QUE EL SISTEMA DEJE EN UN DIRECTORIO TEMPORAL EL JPG QUE SE GENERA DEBIDO A QUE ESTABA
    'ENSUCIANDO EL DIRECTORIO DE INSTALACION DEL SISTEMA, LO CUAL, ERA INCORRECTO.�
    'HAF: 20070620.
    Path = Environ$("TEMP") & "\"

    sFname = c.makeTmpFile(Path & sFname, cd.JPG)

    fncGraficoTorta = Path & sFname

End Function

Function ColorToHex(r, g, b)
    ColorToHex = "&H" & Hex(r) & Hex(g) & Hex(b)
End Function
Function fncGraficoValorCuota() As String
    'Dim oRs As ADODB.Recordset
    'Dim xSql
    Dim sName As String
    Dim sTitulo
    Dim fechainicrent
    Dim fechabaserent
    Dim lCursor_Cuotas As hRecord
    Dim lCampo As hFields
    Dim catnom(), Valor()
    Dim catnomc(), valorC()
    Dim catnom3(), valor3()

    Dim sFechaMenor
    Dim sFechaMayor

    Dim iValorMenor
    Dim iValorMayor

    Dim nRentabilidad_diaria
    Dim iBase As Integer
    Dim i As Integer
    
    fechainicrent = "21/10/2006" ' & Year(glb_fecha_hoy)
    fechabaserent = "21/11/2006"
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pid_cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_inicio", ePT_Numero, fechainicrent, ePD_Entrada
    gDB.Parametros.Add "Pfecha_final", ePT_Numero, fechabaserent, ePD_Entrada
    gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS.RENTABILIDAD_PERIODOS"
    If gDB.EjecutaSP Then
        Set lCursor_Cuotas = gDB.Parametros("pcursor").Valor
        If lCursor_Cuotas.Count > 0 Then
            iBase = 100
            i = 0
            For Each lCampo In lCursor_Cuotas
            
                If NVL(lCampo("rentabilidad_mon_cuenta").Value, 0) = 0 Then
                    nRentabilidad_diaria = 0
                Else
                    nRentabilidad_diaria = CDbl(lCampo("rentabilidad_mon_cuenta").Value)
                End If
                If nRentabilidad_diaria <> 0 Then
                    ReDim Preserve catnom(i)
                    ReDim Preserve Valor(i)
        
                    ReDim Preserve catnomc(i)
                    ReDim Preserve valorC(i)
                    ReDim Preserve catnom3(i)
                    ReDim Preserve valor3(i)
                    If i = 0 Then
                        sFechaMenor = lCampo("fecha_cierre").Value
                        sFechaMayor = lCampo("fecha_cierre").Value
        
                        iValorMenor = iBase
                        iValorMayor = iBase
        
                        catnom(i) = lCampo("fecha_cierre").Value
                        Valor(i) = iBase
                    Else
                        catnom(i) = lCampo("fecha_cierre").Value
                        Valor(i) = Valor(i - 1) * (1 + nRentabilidad_diaria / 100)
                    End If
        
                    If sFechaMenor > lCampo("fecha_cierre").Value Then
                        sFechaMenor = lCampo("fecha_cierre").Value
                    End If
        
                    If sFechaMayor < lCampo("fecha_cierre").Value Then
                        sFechaMayor = lCampo("fecha_cierre").Value
                    End If
        
                    If iValorMenor > Valor(i) Then
                        iValorMenor = Valor(i)
                    End If
        
                    If iValorMayor < Valor(i) Then
                        iValorMayor = Valor(i)
                    End If
                    catnom(i) = Format(lCampo("fecha_cierre").Value, "dd/mm/yy") 'Mid(DameMes(lCampo("fecha_cierre").Value), 1, 3) & "-" & Year(lCampo("fecha_cierre").Value)
                    catnomc(i) = lCampo("fecha_cierre").Value & "-"
                    valorC(i) = CDbl(lCampo("valor_cuota_mon_cuenta").Value)
                    i = i + 1
                End If
            Next
        End If
    Else
        MsgBox gDB.Parametros.Errnum & vbCr & gDB.Parametros.ErrMsg, vbCritical
    End If
    gDB.Parametros.Clear

    ' cantidades = datediff("d", fechainicrent, fechainicrent)

'    Do While Not oRs.EOF
'        If IsNull(oRs("rentabilidad_diaria")) Then
'            nRentabilidad_diaria = 0
'        Else
'            nRentabilidad_diaria = CDbl(oRs("rentabilidad_diaria"))
'        End If
'
'        If nRentabilidad_diaria <> 0 Then
'            ReDim Preserve catnom(i)
'            ReDim Preserve Valor(i)
'
'            ReDim Preserve catnomc(i)
'            ReDim Preserve valorC(i)
'            ReDim Preserve catnom3(i)
'            ReDim Preserve valor3(i)
'
'            If i = 0 Then
'                sFechaMenor = oRs("fecha")
'                sFechaMayor = oRs("fecha")
'
'                iValorMenor = iBase
'                iValorMayor = iBase
'
'                catnom(i) = oRs("fecha")
'                Valor(i) = iBase
'            Else
'                catnom(i) = oRs("fecha")
'                Valor(i) = Valor(i - 1) * (1 + nRentabilidad_diaria / 100)
'            End If
'
'            If sFechaMenor > oRs("fecha") Then
'                sFechaMenor = oRs("fecha")
'            End If
'
'            If sFechaMayor < oRs("fecha") Then
'                sFechaMayor = oRs("fecha")
'            End If
'
'            If iValorMenor > Valor(i) Then
'                iValorMenor = Valor(i)
'            End If
'
'            If iValorMayor < Valor(i) Then
'                iValorMayor = Valor(i)
'            End If
'
'            catnom(i) = Mid(DameMes(oRs("fecha")), 1, 3) & "-" & Year(oRs("fecha"))
'
'            catnomc(i) = oRs("fecha") & "-"
'            valorC(i) = CDbl(oRs("cuota"))
'
'            i = i + 1
'
'        End If
'
'        oRs.MoveNext
'
'    Loop
    
    Dim nSemestres As Double
    'Dim iDiferencia As Double
    Dim bLineaTendencia As Boolean

'    nSemestres = Round((DateDiff("d", sFechaMenor, sFechaMayor) / 365) * 2)
'    nSemestres = DateDiff("m", sFechaMenor, sFechaMayor)
'
'    iDiferencia = (iValorMayor - iValorMenor) / 10
'
'    iValorMenor = iValorMenor - iDiferencia
'    iValorMayor = iValorMayor + iDiferencia

    ' sTitulo = "Valor Cuota - " & fechainicrent & " hasta " & fechabaserent & " "
    sTitulo = "VALOR CUOTA/BASE 100"

    bLineaTendencia = False
      

'   sName  = GraficoLineas(catnom, valor, sTitulo, false, i-1, nSemestres, bLineaTendencia, Int(iValorMenor/1.1), INT(iValorMayor*1.1) )
    sName = GraficoLineas(catnom, Valor, sTitulo, False, i - 1, nSemestres, bLineaTendencia, iValorMenor, iValorMayor)

    fncGraficoValorCuota = sName

End Function

Function GraficoLineas(Categories1, Vals1, sTitulo, leyenda, entries, nSemestres, bLineaTendencia, iValorMenor, iValorMayor) As String
    Dim cd
    Dim c
    Dim xColor          As String
    Dim xColorTitulo    As String
    Dim noOfPoints      As Integer
    Dim Path            As String
    Dim sFname As String
    'Dim sTitulo
    Dim x As Integer
    Dim y As Integer
    
    
    
    

    xColorTitulo = ColorToHex(0, 0, 0)
    xColor = ColorToHex(RColorRelleno, GColorRelleno, BColorRelleno)
    'x = 950
    'y = 350

    noOfPoints = 1 ' UBound(Vals1) - 1

    Set cd = CreateObject("ChartDirector.API")
 
    cd.setLicenseCode ("YT5ZJ59NKAF9ZR3942DAE53E")
    
    x = 350
    y = 220
    Set c = cd.XYChart(x, y) ' , cd.metalColor(&HDEDEDE), -1, 2)

    'Call c.addTitle(stitulo, "arialbd.ttf", 13, &HFFFFFF).setBackground(&HFF0000, &H0, 0)      ' Add a title to the y axis
    Call c.setPlotArea(50, 60, x - 60, y - 90)

'    Set c = cd.XYChart(x, y, &HFFFFFF, 1, 0)
 '   Call c.setPlotArea(100, 60, Int(x * 0.9), Int(y * 0.7), cd.Transparent, cd.Transparent, cd.Transparent, cd.Transparent, cd.Transparent)

    'Call c.yAxis().setLabelStyle("GILB____.TTF", 10)
    'Call c.yAxis().setLabelStyle("GILB____.TTF", 12)
    Call c.yAxis().setLabelStyle("arialbd.ttf", 7, &H555555)
    Call c.yAxis().setLabelFormat("{value|4.}")
    
    Call c.yAxis().setLinearScale(iValorMenor, iValorMayor)

    Call c.xAxis().setLabels(Categories1)

    'Call c.xAxis().setLabelStyle("GILB____.TTF", 12)  ' , 0, 90)
    Call c.xAxis().setLabelStyle("arialbd.ttf", 7, &H555555, 30).setpos(0, -1)
    
    Call c.xAxis().setLabelStep(3)
    
    Call c.addLineLayer(Vals1, 0)

    sTitulo = "<*size=14,yoffset=-11*><*block*><*color=" & xColorTitulo & "*>" & sTitulo & "<*br*><*/*>"

    Call c.addTitle(sTitulo, "GILB____.TTF", 14, xColorTitulo).setBackground(xColor)
    
    'Path = App.Path & "\" '    App.Path ' "." ' Left(Request.ServerVariables("PATH_INFO"), InStrRev(Request.ServerVariables("PATH_INFO"), "/graficos/"))

    'MODIFICADO PARA QUE EL SISTEMA DEJE EN UN DIRECTORIO TEMPORAL EL JPG QUE SE GENERA DEBIDO A QUE ESTABA
    'ENSUCIANDO EL DIRECTORIO DE INSTALACION DEL SISTEMA, LO CUAL, ERA INCORRECTO.�
    'HAF: 20070620.
    Path = Environ$("TEMP") & "\"

    sFname = c.makeTmpFile(Path & sFname, cd.JPG)
    
    GraficoLineas = Path & sFname

End Function


Private Function FormatPorcentaje(Valor As Double, Valor_Total As Double, Optional ByRef nValorAsignado As Double = 0) As String
    If Valor_Total <> 0 Then
        FormatPorcentaje = FormatNumber((Valor / Valor_Total) * 100, 2) & "%"
        nValorAsignado = (Valor / Valor_Total) * 100
    Else
        FormatPorcentaje = FormatNumber(0, 2) & "%"
        nValorAsignado = 0
    End If
End Function

Function FormatNumber(xValor As Variant, iDecimales As Integer, Optional bPorcentaje As Boolean = False) As String
    Dim xFormato        As String
    Dim sDecimalSep     As String
    Dim sMilesSep       As String
        
    sDecimalSep = Obtener_Simbolo(LOCALE_SDECIMAL)
    sMilesSep = Obtener_Simbolo(LOCALE_SMONTHOUSANDSEP)
    
    On Error GoTo PError
        
    If iDecimales = 0 Then
        xFormato = "###,###,##0"
    ElseIf bPorcentaje Then
        xFormato = "##0." & String(iDecimales - 1, "#") & "0"
    Else
        xFormato = "###,###,##0." & String(iDecimales - 1, "#") & "0"
    End If
    
    GoTo Fin
    
PError:
    xValor = "0"
    
Fin:
    FormatNumber = Format(xValor, xFormato)
    On Error GoTo 0
    
End Function

Function DameMes(sFecha)
    Dim iMes As Integer
    Dim aMeses
    iMes = Month(sFecha)
    aMeses = Array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
    DameMes = aMeses(iMes)
End Function
'******************************************************************************************************************
'Private Sub ActivosClaseInstrumento()
'
'    Const clrHeader = &HD0D0D0
'    '------------------------------------------
'    'Const sHeader_SA = "Nemot�cnico|Cantidad|Precio|Valor Mercado|Valor Mercado Cuenta|"
'    'Const sFormat_SA = "2900|>1600|>1100|>2500|>2500|3600"
'    'Const sHeader_Detalle_Caja = "Fecha Mov.|Fecha Liq.|Origen Mov.|Movimientos|Ingreso|Egreso|Saldo"
'    'Const sFormat_Detalle_Caja = "1200|1100|1950|4550|>1800|>1800|>1800"
'    '------------------------------------------
'    Dim sRecord
'    Dim bAppend
'    Dim lLinea As Integer
'    '------------------------------------------
'    Dim lPatrimonio As Class_Patrimonio_Cuentas
'    Dim lReg_Pat As hFields
'    Dim lPatrim As String
'    Dim lRentabilidad As Double
'    '------------------------------------------
'    Dim lCajas_Ctas As Class_Cajas_Cuenta
'    Dim lCursor_Caj As hRecord
'    Dim lCursor_Caja As hRecord
'    Dim lReg_Caj As hFields
'    Dim lReg_Caja As hFields
'    Dim lMonto_Mon_Caja As String
'    '------------------------------------------
'    Dim lSaldos_Activos As Class_Saldo_Activos
'    Dim lCursor_SA As hRecord
'    Dim lReg_SA As hFields
'    Dim lReg_SN As hFields
'    Dim lCursor_SN As hRecord
'
'    Dim lCuenta_Lineas As Integer
'    Dim lTope As Integer
'
'    '------------------------------------------
'    Dim lParidad As Double
'    Dim lOperacion As String
'    Dim lMonto_Mon_Cta As String
'    '------------------------------------------
'    '    Dim lcTipo_Cambio As Class_Tipo_Cambios
'    Dim lcTipo_Cambio As Object
'    '------------------------------------------
'    Dim lInstrumento As Class_Instrumentos
'    Dim lCursor_Ins As hRecord
'    Dim lReg_Ins As hFields
'    Dim lHay_Activos As Boolean
'    '------------------------------------------
'    Dim lSaldos_Caja As Class_Saldos_Caja
'
'    '--------------------------------------
'    Dim lReg As hCollection.hFields
'    '    Dim lcCuenta As Class_Cuentas
'    Dim lcCuenta As Object
'    Dim lcMov_Caja As Class_Mov_Caja
'    Dim lcSaldos_Caja As Class_Saldos_Caja
'    '--------------------------------------
'    Dim lFecDesde As Date
'    Dim lFecHasta As Date
'    Dim lId_Caja_Cuenta As String
'    Dim lTotal As Double
'    Dim lIngreso As Double
'    Dim lEgreso As Double
'    Dim lUltimaCaja As Long
'    Dim nDecimales As Integer
'    Dim iFila As Integer
'    Dim i As Integer
'    Dim anterior As Integer
'    Dim Mostrado As Integer
'    '------------------------------------------
'    Dim Monto As Double
'    Dim Porcentaje As Double
'
'    Dim cont As Integer
'
'    Dim lCodProducto As String
'
'    Dim yAnt As Variant
'
'    Call Sub_Bloquea_Puntero(Me)
'
'    'If Not Fnt_Form_Validar(Me.Controls) Then
'        'GoTo ErrProcedure
'    'End If
'
'    Rem Busca el patrimonio de la cuenta y su rentabilidad
'    Set lPatrimonio = New Class_Patrimonio_Cuentas
'    With lPatrimonio
'        .Campo("id_cuenta").Valor = lId_Cuenta
'        .Campo("FECHA_CIERRE").Valor = DTP_Fecha_Ter.Value
'        'Valores por defecto
'        .Campo("RENTABILIDAD_MON_CUENTA").Valor = Null
'        .Campo("APORTE_RETIROS_MON_CUENTA").Valor = Null
'
'        If .Buscar Then
'            For Each lReg_Pat In .Cursor
'                lPatrim = FormatNumber(lReg_Pat("PATRIMONIO_MON_CUENTA").Value, nDecimales)
'                lRentabilidad = NVL(lReg_Pat("RENTABILIDAD_MON_CUENTA").Value, 0)
'            Next
'        Else
'            MsgBox .ErrMsg, vbCritical, Me.Caption
'            'GoTo ErrProcedure
'        End If
'    End With
'
'    Rem Busca las cajas de la cuenta
'    Set lCajas_Ctas = New Class_Cajas_Cuenta
'
'    With lCajas_Ctas
'        .Campo("id_cuenta").Valor = lId_Cuenta
'        If .Buscar(True) Then
'            Set lCursor_Caj = .Cursor
'            Set lCursor_Caja = .Cursor
'        Else
'            MsgBox .ErrMsg, vbCritical, Me.Caption
'            'GoTo ErrProcedure
'        End If
'    End With
'
'    Rem Busca los Instrumentos del sistema
'    Set lInstrumento = New Class_Instrumentos
'    With lInstrumento
'        If .Buscar Then
'            Set lCursor_Ins = .Cursor
'        Else
'            MsgBox .ErrMsg, vbCritical, Me.Caption
'            'GoTo ErrProcedure
'        End If
'    End With
'
'    Rem Comienzo de la generaci�n del reporte
'    lLinea = 2
'
'    lHay_Activos = False
'
'    anterior = 0
'    gDB.Parametros.Clear
'    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
'    gDB.Parametros.Add "Pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
'    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
'    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA"
'
'    If Not gDB.EjecutaSP Then
'        Exit Sub
'    End If
'
'    Set lCursor_Ins = gDB.Parametros("Pcursor").Valor
'    Dim iAumentoFila As Double
'    With vp
'        .MarginLeft = COLUMNA_INICIO    ' + rTwips("150mm")
'        FilaAux = .CurrentY
'
'        iAumentoFila = Twips2mm(.LineSpacing)
'
'        cont = 0
'        For Each lReg In lCursor_Ins
'
'            Monto = 0
'            Porcentaje = 0
'
'            If CDbl(NVL(lReg("monto_mon_cta_inst").Value, 0)) <> 0 Then
'                If anterior = lReg("id_arbol_clase_inst").Value Then
'                    '.NewPage
'                    '.Paragraph = ""
'                Else
''                    If lReg("codigo").Value = "FFMM" Then
''                        cont = cont + 1
''                        If cont = 1 Then
''                            '.NewPage
''                            .CurrentY = 2510
''                            .CurrentY = vp.CurrentY + rTwips("2mm")
''                            .Paragraph = ""
''                            'pon_titulo UCase("RESUMEN ACTIVOS"), "5mm", Font_Name, glb_tamletra_titulo
''                            .CurrentY = vp.CurrentY + rTwips("2mm")
''                            'MsgBox CurrentY
''
''                            FilaAux = vp.CurrentY
''                            '.CurrentY = 2750
''                        Else
''                            .Paragraph = ""
''                        End If
''                    Else
'                        'If anterior <> 0 Then
'                            ' If .CurrentY > 4000 Or anterior = 0 Then
'                            .NewPage
'                            '.CurrentY = 2510
'                            .CurrentY = vp.CurrentY + rTwips("2mm")
'                          ' .Paragraph = ""
'                            'pon_titulo UCase("RESUMEN ACTIVOS"), "5mm", Font_Name, glb_tamletra_titulo
'                        'End If
'                        .CurrentY = vp.CurrentY + rTwips("2mm")
'                        FilaAux = vp.CurrentY
'
'                        .TableBorder = tbBox
'                        '.CurrentY = 2750
''                    End If
'                End If
'
'                'If .CurrentY > 8000 Then
'                '    .NewPage
'                'End If
'
'' HAY UN ELSE NUEVO PARA CONTROLAR LA DISTORCI�N DE LA CARTOLA.
'
'                If CDbl(NVL(lReg("monto_mon_cta_inst").Value, 0)) Then
'                    '.CurrentY = 3000
'                    'yAnt = .CurrentY
'                    .FontSize = 10
'                    .FontBold = True
'
'                    If anterior <> lReg("id_arbol_clase_inst").Value Then
'                        .Paragraph = "TIPO DE INSTRUMENTO: " & Trim(lReg("dsc_arbol_clase_inst").Value)
'                    Else
'                        '.Paragraph = ""
'                    End If
'
'                    '.CurrentY = yAnt
'                    .FontBold = False
'
'                    If CDbl(NVL(Trim(lReg("monto_mon_cta_hoja").Value), 0)) <> 0 And _
'                        CInt(lReg("NIVEL").Value) = 0 Then
'
'                        .CurrentY = .CurrentY + rTwips("3mm")
'
'                        ' .FontBold = False
'                        .Paragraph = Trim(lReg("dsc_arbol_clase_inst_hoja").Value)
'                        .FontSize = glb_tamletra_registros
'
'                        .StartTable
'                            .TableBorder = tbBox
'                            .TableBorder = tbBoxRows
'
'                            .TableCell(tcCols) = 11
'                            .TableCell(tcRows) = .TableCell(tcRows) + 1
'                            .TableCell(tcFontSize, 1, 1, 1, 3) = glb_tamletra_registros
'
'                            .TableCell(tcText, 1, 2) = ""
'                        '   .TableCell(tcColSpan, 1, 4) = 2
'                            .TableCell(tcText, 1, 4) = ""
'
'                        '   Ancho de cada columna
'                        '   .TableCell(tcColWidth, 1, 1) = "60mm"
'                        '   .TableCell(tcColWidth, 1, 2) = "25mm"
'                        '   .TableCell(tcColWidth, 1, 3) = "25mm"
'                        '   .TableCell(tcColWidth, 1, 4) = "30mm"
'                        '   .TableCell(tcColWidth, 1, 5) = "30mm"
'                        '   .TableCell(tcColWidth, 1, 6) = "30mm"
'                        '   .TableCell(tcColWidth, 1, 7) = "30mm"
'                        '   .TableCell(tcColWidth, 1, 8) = "25mm"
'
'                        lCodProducto = lReg("codigo").Value
'
'                        Select Case lCodProducto
'                            Case "RV"
'                                .TableCell(tcCols) = 8
'                                .TableCell(tcText, 1, 1) = "NEMOTECNICO"
'                                .TableCell(tcText, 1, 2) = "MONEDA"
'                                .TableCell(tcText, 1, 3) = "% DE INVERSION"
'                                .TableCell(tcText, 1, 4) = "CANTIDAD"
'                                .TableCell(tcText, 1, 5) = "PRECIO PROMEDIO" & vbCrLf & "COMPRA"
'                                .TableCell(tcText, 1, 6) = "PRECIO" & vbCrLf & "ACTUAL"
'                                .TableCell(tcText, 1, 7) = "VALOR" & vbCrLf & "MERCADO"
'                                .TableCell(tcText, 1, 8) = "RENTABILIDAD"
'
'                                .TableCell(tcColWidth, 1, 1) = "70mm"
'                                .TableCell(tcColWidth, 1, 2) = "20mm"
'                                .TableCell(tcColWidth, 1, 3) = "25mm"
'                                .TableCell(tcColWidth, 1, 4) = "25mm"
'                                .TableCell(tcColWidth, 1, 5) = "30mm"
'                                .TableCell(tcColWidth, 1, 6) = "25mm"
'                                .TableCell(tcColWidth, 1, 7) = "35mm"
'                                .TableCell(tcColWidth, 1, 8) = "25mm"
'
'                                '.TableCell(tcColWidth, 1, 9) = "20mm"
'                                '.TableCell(tcColWidth, 1, 9) = "25mm"
'                                '.TableCell(tcColWidth, 1, 10) = "25mm"
'
'                            Case "RF"
'                                .TableCell(tcCols) = 13
'                                .TableCell(tcText, 1, 1) = "NEMOTECNICO"
'                                .TableCell(tcText, 1, 2) = "EMISOR"
'                                .TableCell(tcText, 1, 3) = "MONEDA"
'                                .TableCell(tcText, 1, 4) = "% DE " & vbCrLf & "INVERSION"
'                                .TableCell(tcText, 1, 5) = "" & vbCrLf & "NOMINALES"
'                                .TableCell(tcText, 1, 6) = "TASA" & vbCrLf & "CUP�N"
'                                .TableCell(tcText, 1, 7) = "FECHA" & vbCrLf & "VCTO."
'                                .TableCell(tcText, 1, 8) = " "           ' "DIAS"
'                                .TableCell(tcText, 1, 9) = " "           ' "DURATION"
'                                .TableCell(tcText, 1, 10) = "TASA" & vbCrLf & "COMPRA"
'                                 .TableCell(tcText, 1, 11) = ""   ' "PRECIO" & vbCrLf & "MERCADO"    ' "PRECIO" & vbCrLf & "COMPRA"
'                                .TableCell(tcText, 1, 12) = "TASA" & vbCrLf & "MERCADO"
'                                .TableCell(tcText, 1, 13) = "VALORIZACI�N"
'
'                                .TableCell(tcColWidth, 1, 1) = "35mm"
'                                .TableCell(tcColWidth, 1, 2) = "22mm"
'                                .TableCell(tcColWidth, 1, 3) = "22mm"
'                                .TableCell(tcColWidth, 1, 4) = "15mm"
'                                .TableCell(tcColWidth, 1, 5) = "25mm"
'                                .TableCell(tcColWidth, 1, 6) = "15mm"
'                                .TableCell(tcColWidth, 1, 7) = "25mm"
'                                .TableCell(tcColWidth, 1, 8) = "6mm" ' 10mm
'                                .TableCell(tcColWidth, 1, 9) = "6mm" ' 15mm
'                                .TableCell(tcColWidth, 1, 10) = "25mm"
'                                .TableCell(tcColWidth, 1, 11) = "4mm"   ' "25mm"
'                                .TableCell(tcColWidth, 1, 12) = "25mm"
'                                .TableCell(tcColWidth, 1, 13) = "30mm"
'
'                            Case "FFMM"
'                                .TableCell(tcCols) = 8
'                                .TableCell(tcText, 1, 1) = "DESCRIPCI�N"
'                                .TableCell(tcText, 1, 2) = "MONEDA"
'                                .TableCell(tcText, 1, 3) = "% DE INVERSION"
'                                .TableCell(tcText, 1, 4) = "CANTIDAD DE" & vbCrLf & "CUOTAS"
'                                .TableCell(tcText, 1, 5) = "VALOR CUOTA" & vbCrLf & "PROMEDIO COMPRA"
'                                .TableCell(tcText, 1, 6) = "VALOR CUOTA" & vbCrLf & "ACTUAL"
'                                '.TableCell(tcText, 1, 7) = "" ' "VAL. MERCADO " & vbCrLf & "MON. PAPEL"
'                                .TableCell(tcText, 1, 7) = "VALOR ACTUAL " ' & vbCrLf & "MON. CUENTA"
'                                .TableCell(tcText, 1, 8) = "RENTABILIDAD"
'
'                                .TableCell(tcColWidth, 1, 1) = "70mm"
'                                .TableCell(tcColWidth, 1, 2) = "20mm"
'                                .TableCell(tcColWidth, 1, 3) = "25mm"
'                                .TableCell(tcColWidth, 1, 4) = "25mm"
'                                .TableCell(tcColWidth, 1, 5) = "30mm"
'                                .TableCell(tcColWidth, 1, 6) = "25mm"
'                                .TableCell(tcColWidth, 1, 7) = "35mm"
'                                .TableCell(tcColWidth, 1, 8) = "25mm"
'
'                        End Select
'
'                        .TableCell(tcAlign, 1, 1) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 2) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 3) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 4) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 5) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 6) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 7) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 8) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 9) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 10) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 11) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 12) = taCenterMiddle
'                        .TableCell(tcAlign, 1, 13) = taCenterMiddle
'
'                        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'                        .TableCell(tcBackColor, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'
'                        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)  ' Blanco
'                        .TableCell(tcForeColor, 2) = RGB(255, 255, 255)  ' Blanco
'
'                        gDB.Parametros.Clear
'                        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'                        gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
'                        gDB.Parametros.Add "PID_ARBOL_CLASE", ePT_Numero, lReg("id_arbol_clase_inst_hoja").Value, ePD_Entrada
'                        gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
'                        gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO"
'
'                        If Not gDB.EjecutaSP Then
'                            Exit Sub
'                        End If
'
'                        Set lCursor_SA = gDB.Parametros("Pcursor").Valor
'
'                        vp.MarginLeft = COLUMNA_INICIO    ' + rTwips("150mm")
'                        FilaAux = .CurrentY
'
'                        For Each lReg_SA In lCursor_SA
'                            .TableCell(tcRows) = .TableCell(tcRows) + 1
'                            iFila = .TableCell(tcRows)
'
'                            '  If .CurrentY > 8000 Then
'                            '     .NewPage
'                            '  End If
'
'                            Select Case lCodProducto    ' lReg("codigo").Value
'                                Case "RV"
'                                    .TableCell(tcText, iFila, 1) = Trim(lReg_SA("nemotecnico").Value)
'                                    .TableCell(tcText, iFila, 2) = lReg_SA("simbolo_moneda").Value
'                                    .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
'                                    .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("cantidad").Value, 0)
'                                    .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_compra").Value, 2)
'
'                                    '.TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_promedio_compra").Value, 2)
'                                    '.TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio_promedio_compra").Value * lReg_SA("cantidad").Value, 2)
'
'                                    .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio").Value, 2)
'                                    '.TableCell(tcText, iFila, 8) = FormatNumber(lReg_SA("monto_mon_nemotecnico").Value, 2)
'                                    .TableCell(tcText, iFila, 7) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)    ' Valor de Mercado
'                                    .TableCell(tcText, iFila, 8) = IIf(CDbl(lReg_SA("rentabilidad").Value) > 1000, "-", FormatNumber(lReg_SA("rentabilidad").Value, 2) & "%")
'
'                                    .TableCell(tcAlign, iFila, 2) = taCenterMiddle
'                                    .TableCell(tcAlign, iFila, 3) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 4) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 5) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 6) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 7) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 8) = taRightMiddle
'
'                                    Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
'                                    Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
'
'                                Case "FFMM"
'                                    .TableCell(tcText, iFila, 1) = Trim(lReg_SA("DSC_nemotecnico").Value)
'                                    .TableCell(tcText, iFila, 2) = Trim(lReg_SA("simbolo_moneda").Value)
'                                    .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
'                                    .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("cantidad").Value, 4)
'                                    .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_compra").Value, 4)
'                                    .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio").Value, 4)
'                                    '.TableCell(tcText, iFila, 7) = "" ' FormatNumber(lReg_SA("monto_mon_nemotecnico").Value, oCliente.Decimales)   'Decimales_Moneda(Trim(lReg_SA("simbolo_moneda").Value)))
'                                    .TableCell(tcText, iFila, 7) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)
'                                    .TableCell(tcText, iFila, 8) = IIf(CDbl(lReg_SA("rentabilidad").Value) > 1000, "-", FormatNumber(lReg_SA("rentabilidad").Value, 2) & "%")
'
'                                    .TableCell(tcAlign, iFila, 2) = taCenterMiddle
'                                    .TableCell(tcAlign, iFila, 3) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 4) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 5) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 6) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 7) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 8) = taRightMiddle
'                                    '.TableCell(tcAlign, iFila, 9) = taRightMiddle
'
'                                    Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
'                                    Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
'
'                                Case "RF"
'                                    .TableCell(tcText, iFila, 1) = Trim(lReg_SA("nemotecnico").Value)
'                                    .TableCell(tcText, iFila, 2) = Left(Trim(lReg_SA("cod_emisor").Value), 16)
'                                    .TableCell(tcText, iFila, 3) = Trim(lReg_SA("SIMBOLO_MONEDA").Value)
'                                    .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
'                                    .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("cantidad").Value, 0)
'                                    .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("tasa_emision").Value, 2)
'                                    .TableCell(tcText, iFila, 7) = Trim(lReg_SA("fecha_vencimiento").Value)
'                                    .TableCell(tcText, iFila, 8) = " " ' FormatNumber(lReg_SA("dias").Value, 2)
'                                    .TableCell(tcText, iFila, 9) = " " ' FormatNumber(lReg_SA("duration").Value, 2)
'                                    .TableCell(tcText, iFila, 10) = FormatNumber(lReg_SA("tasa_compra").Value, 2)
'                                    .TableCell(tcText, iFila, 11) = ""    ' FormatNumber(lReg_SA("precio_compra").Value, 2)
'                                    .TableCell(tcText, iFila, 12) = FormatNumber(lReg_SA("tasa").Value, 2)
'                                    .TableCell(tcText, iFila, 13) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)
'
'                                    .TableCell(tcAlign, iFila, 2) = taLeftMiddle
'                                    .TableCell(tcAlign, iFila, 3) = taCenterMiddle
'                                    .TableCell(tcAlign, iFila, 4) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 5) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 6) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 7) = taCenterMiddle   ' taRightMiddle
'                                    .TableCell(tcAlign, iFila, 8) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 9) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 10) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 11) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 12) = taRightMiddle
'                                    .TableCell(tcAlign, iFila, 13) = taRightMiddle
'
'                                    Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
'                                    Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
'
'                            End Select
'
'                        Next  ' Nemotecnicos (For Each lReg_SA In lCursor_SA)
'                        .TableCell(tcRows) = .TableCell(tcRows) + 1
'                        iFila = iFila + 1
'
'                        Select Case lReg("codigo").Value
'                            Case "RV"
'                                .TableCell(tcText, iFila, 3) = FormatNumber(Porcentaje, 2) & "%"
'                                .TableCell(tcAlign, iFila, 3) = taRightMiddle
'                                .TableCell(tcText, iFila, 6) = "TOTAL"
'                                .TableCell(tcAlign, iFila, 6) = taRightMiddle
'                                .TableCell(tcText, iFila, 7) = FormatNumber(Monto, oCliente.Decimales)
'                                .TableCell(tcAlign, iFila, 7) = taRightMiddle
'                            Case "FFMM"
'                                .TableCell(tcText, iFila, 3) = FormatNumber(Porcentaje, 2) & "%"
'                                .TableCell(tcAlign, iFila, 3) = taRightMiddle
'
'                                .TableCell(tcText, iFila, 6) = "TOTAL"
'                                .TableCell(tcAlign, iFila, 6) = taRightMiddle
'
'                                .TableCell(tcText, iFila, 7) = FormatNumber(Monto, oCliente.Decimales)
'                                .TableCell(tcAlign, iFila, 7) = taRightMiddle
'
'                            Case "RF"
'                                .TableCell(tcText, iFila, 4) = FormatNumber(Porcentaje, 2) & "%"
'                                .TableCell(tcAlign, iFila, 4) = taRightMiddle
'
'                                .TableCell(tcText, iFila, 12) = "TOTAL"
'                                .TableCell(tcAlign, iFila, 12) = taRightMiddle
'
'                                .TableCell(tcText, iFila, 13) = FormatNumber(Monto, oCliente.Decimales)
'                                .TableCell(tcAlign, iFila, 13) = taRightMiddle
'                        End Select
'                        .TableCell(tcBackColor, iFila) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'                        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)   ' Blanco
'                        .EndTable
'                    End If
'' ESTO HAY QUE CORREGIR, SE ME FUE EN COLLERA
''                ElseIf CDbl(NVL(lReg("monto_mon_cta_inst").Value, 0)) <> 0 Then
''                    If anterior <> lReg("id_arbol_clase_inst").Value Then
''                        .Paragraph = "TIPO DE INSTRUMENTO: " & Trim(lReg("dsc_arbol_clase_inst").Value)
''                    Else
''                        .Paragraph = ""
''                    End If
'                End If
'            End If
'
'            If lReg("codigo").Value = "RV" And _
'                anterior <> lReg("id_arbol_clase_inst").Value Then
'                '.NewPage
'                '.Paragraph = ""
'                'Call PaginaDos
'            End If
'
'            anterior = lReg("id_arbol_clase_inst").Value
'        Next ' For Each lReg In lCursor_Ins
'
''        .NewPage
''        .CurrentY = 2510
''        .CurrentY = vp.CurrentY + rTwips("2mm")
''        pon_titulo UCase("MOVIMIENTOS DE FONDOS MUTUOS"), "5mm", Font_Name, glb_tamletra_titulo
''        .CurrentY = vp.CurrentY + rTwips("2mm")
''        FilaAux = vp.CurrentY
''                  For Each lReg In lCursor_Ins
''                      If lReg("codigo").Value = "FFMM" Then
''                    gDB.Parametros.Clear
''                    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
''                    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
''                    gDB.Parametros.Add "PID_ARBOL_CLASE", ePT_Numero, lReg("id_arbol_clase_inst_hoja").Value, ePD_Entrada
''                    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
''                    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO"
''
''                    If Not gDB.EjecutaSP Then
''                    Exit Sub
''                    End If
''
''                    Set lCursor_SA = gDB.Parametros("Pcursor").Valor
''
''                      For Each lReg_SA In lCursor_SA
''                          If .CurrentY > 10000 Then
''                              .NewPage
''                              .CurrentY = 2510
''                              .CurrentY = vp.CurrentY + rTwips("2mm")
''                              pon_titulo UCase("MOVIMIENTOS FONDOS MUTUOS"), "5mm", Font_Name, glb_tamletra_titulo
''                              .CurrentY = vp.CurrentY + rTwips("2mm")
''                              FilaAux = vp.CurrentY
''                          End If
''
''                        .CurrentY = vp.CurrentY + rTwips("2mm")
''                        pon_titulo UCase("MOVIMIENTOS ") & lReg_SA("nemotecnico").Value, "3mm", Font_Name, 8
''                        .CurrentY = vp.CurrentY + rTwips("2mm")
''                        FilaAux = vp.CurrentY
''
''                          gDB.Parametros.Clear
''                          gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
''                          gDB.Parametros.Add "id_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
''                          gDB.Parametros.Add "fecha_ini", ePT_Numero, CDate(oCliente.Fecha_Cartola) - 30, ePD_Entrada
''                          gDB.Parametros.Add "fecha_ter", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
''                          gDB.Parametros.Add "id_nemo", ePT_Numero, lReg_SA("id_nemotecnico").Value, ePD_Entrada
''                          gDB.Procedimiento = "PKG_MOV_ACTIVOS$BUSCAR_MOV_NEMO"
''
''                          If Not gDB.EjecutaSP Then
''                          Exit Sub
''                          End If
''
''                          Set lCursor_SN = gDB.Parametros("Pcursor").Valor
''                          .StartTable
''                          .TableCell(tcRows) = 1
''                          .TableCell(tcCols) = 5
''                          '.TableCell(tcRows) = lCursor_SN.Count
''                          iFila = 1
''                          .TableCell(tcText, iFila, 1) = "FECHA MOVIMIENTO"
''                          .TableCell(tcText, iFila, 2) = "DESCRIPCI�N"
''                          .TableCell(tcText, iFila, 3) = "N�MERO DE  CUOTAS"
''                          .TableCell(tcText, iFila, 4) = "VALOR CUOTA"
''                          .TableCell(tcText, iFila, 5) = "MONTO INVERTIDO"
''
''                          .TableCell(tcAlign, iFila, 1) = taCenterMiddle
''                          .TableCell(tcAlign, iFila, 2) = taCenterMiddle
''                          .TableCell(tcAlign, iFila, 3) = taCenterMiddle
''                          .TableCell(tcAlign, iFila, 4) = taCenterMiddle
''                          .TableCell(tcAlign, iFila, 5) = taCenterMiddle
''
''                          .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
''                          iFila = iFila + 1
''                          For Each lReg_SN In lCursor_SN
''                             .TableCell(tcRows) = .TableCell(tcRows) + 1
''                             .TableCell(tcText, iFila, 1) = lReg_SN("fecha_movimiento").Value
''                             .TableCell(tcText, iFila, 2) = lReg_SN("dsc_movimiento").Value
''                             .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SN("cantidad").Value, 4)
''                             .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SN("precio").Value, 4)
''                             .TableCell(tcText, iFila, 5) = IIf(lReg_SN("flg_tipo_movimiento").Value = "I", FormatNumber(lReg_SN("monto_total").Value, oCliente.Decimales), FormatNumber(CDbl(lReg_SN("monto_total").Value) * -1, oCliente.Decimales))
''
''                             .TableCell(tcAlign, iFila, 1) = taCenterMiddle
''                             .TableCell(tcAlign, iFila, 2) = taLeftMiddle
''                             .TableCell(tcAlign, iFila, 3) = taRightMiddle
''                             .TableCell(tcAlign, iFila, 4) = taRightMiddle
''                             .TableCell(tcAlign, iFila, 5) = taRightMiddle
''
''                             'FormatNumber(lReg_SN("monto_total").Value, 2)
''                             'IIf(lReg_SN("flg_tipo_movimiento").Value = 'I', formatnumber(lreg_sn("monto_total").value,2),0)
''                             iFila = iFila + 1
''
''                          Next
''                          .TableCell(tcColWidth, , 1) = "30mm"
''                          .TableCell(tcColWidth, , 2) = "80mm"
''                          .TableCell(tcColWidth, , 3) = "30mm"
''                          .TableCell(tcColWidth, , 4) = "30mm"
''                          .TableCell(tcColWidth, , 5) = "40mm"
''                          .TableCell(tcFontSize, 1, 1, iFila, 5) = 7
''                          .EndTable
''
''
''                      Next
''                      End If
''
''                      Next
'
'
'        Set lCursor_Ins = Nothing
'        Set lReg = Nothing
'
'    End With
'
'End Sub

'***********************************************************************************************************
Private Sub ActivosClaseInstrumento()
    Dim lLinea As Integer
    '------------------------------------------
    Dim lPatrimonio As Class_Patrimonio_Cuentas
    Dim lReg_Pat As hFields
    Dim lPatrim As String
    Dim lRentabilidad As Double
    '------------------------------------------
    Dim lCajas_Ctas As Class_Cajas_Cuenta
    Dim lCursor_Caj As hRecord
    Dim lCursor_Caja As hRecord
    '------------------------------------------
    Dim lCursor_SA As hRecord
    Dim lReg_SA As hFields
    '------------------------------------------
    Dim lInstrumento As Class_Instrumentos
    Dim lCursor_Ins As hRecord
    Dim lHay_Activos As Boolean
    '--------------------------------------
    Dim lReg As hCollection.hFields
    Dim nDecimales As Integer
    Dim iFila As Integer
    Dim anterior As Integer
    '------------------------------------------
    Dim Monto As Double
    Dim Porcentaje As Double
    Dim cont As Integer
    Dim lCodProducto As String
    Dim sColorTextoAnt      As Long

    Dim sDscInstrumento As String    'Agregado por MMA. 03/10/2008
    Dim bPrimero As Boolean
    Dim i As Integer
    Dim dMontoPromedioCompra        As Double   'Agregado por MMA. 22/10/2008
    Dim dTotalMontoPromedioCompra   As Double   'Agregado por MMA. 22/10/2008
    Dim iDecimalesMonedaNemo As Integer
    'Call Sub_Bloquea_Puntero(Me)

    Rem Busca el patrimonio de la cuenta y su rentabilidad
    Set lPatrimonio = New Class_Patrimonio_Cuentas
    With lPatrimonio
        .Campo("id_cuenta").Valor = lId_Cuenta
        .Campo("FECHA_CIERRE").Valor = DTP_Fecha_Ter.Value
        'Valores por defecto
        .Campo("RENTABILIDAD_MON_CUENTA").Valor = Null
        .Campo("APORTE_RETIROS_MON_CUENTA").Valor = Null

        If .Buscar Then
            For Each lReg_Pat In .Cursor
                lPatrim = FormatNumber(lReg_Pat("PATRIMONIO_MON_CUENTA").Value, nDecimales)
                lRentabilidad = NVL(lReg_Pat("RENTABILIDAD_MON_CUENTA").Value, 0)
            Next
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            'GoTo ErrProcedure
        End If
    End With

    Rem Busca las cajas de la cuenta
    Set lCajas_Ctas = New Class_Cajas_Cuenta
    With lCajas_Ctas
        .Campo("id_cuenta").Valor = lId_Cuenta
        If .Buscar(True) Then
            Set lCursor_Caj = .Cursor
            Set lCursor_Caja = .Cursor
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            'GoTo ErrProcedure
        End If
    End With

    Rem Busca los Instrumentos del sistema
    Set lInstrumento = New Class_Instrumentos
    With lInstrumento
        If .Buscar Then
            Set lCursor_Ins = .Cursor
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            'GoTo ErrProcedure
        End If
    End With

    Rem Comienzo de la generaci�n del reporte
    lLinea = 2

    lHay_Activos = False

    anterior = 0
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "Pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA"

    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor_Ins = gDB.Parametros("Pcursor").Valor
    Dim iAumentoLinea As Double
    
    With vp
        ' .NewPage
        iAumentoLinea = Twips2mm(.LineSpacing)
    
        ' .CurrentY = rTwips("55mm")

        cont = 0
        iLineasImpresas = 0
        anterior = 0
        bPrimero = True
        For Each lReg In lCursor_Ins
            Monto = 0
            Porcentaje = 0

            If CDbl(NVL(lReg("monto_mon_cta_inst").Value, 0)) <> 0 And _
                    CDbl(NVL(Trim(lReg("monto_mon_cta_hoja").Value), 0)) <> 0 And _
                        CInt(lReg("NIVEL").Value) = 0 Then
                        
                'If CDbl(NVL(Trim(lReg("monto_mon_cta_hoja").Value), 0)) <> 0 And _
                    CInt(lReg("NIVEL").Value) = 0 Then
                    
                    gDB.Parametros.Clear
                    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
                    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
                    gDB.Parametros.Add "PID_ARBOL_CLASE", ePT_Numero, lReg("id_arbol_clase_inst_hoja").Value, ePD_Entrada
                    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
                    gDB.Parametros.Add "pId_Moneda_Salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada   'MMA 09/09/2008
                    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO"

                    If Not gDB.EjecutaSP Then
                        Exit Sub
                    End If

                    Set lCursor_SA = gDB.Parametros("Pcursor").Valor

                    If anterior <> lReg("id_arbol_clase_inst").Value Then
                        .NewPage

                        .FontSize = 10
                        .FontBold = True
                        .Paragraph = "TIPO DE INSTRUMENTO: " & Trim(lReg("dsc_arbol_clase_inst").Value)
                        .FontBold = False
                        bPrimero = False
                        iLineasImpresas = .LineSpacing
                    End If
                    
                    anterior = lReg("id_arbol_clase_inst").Value
             '      iLineasImpresas = iLineasImpresas + .LineSpacing
                    lCodProducto = lReg("codigo").Value
                    sDscInstrumento = Trim(lReg("dsc_arbol_clase_inst_hoja").Value)  'Agregado por MMA. 03/10/2008
                    If iLineasImpresas + 220 > (.LineSpacing * (CONS_NUMERO_LINEAS - 3)) Then
                        .NewPage
                        .FontSize = 10
                        .Paragraph = Trim(lReg("dsc_arbol_clase_inst_hoja").Value)
                        .FontSize = glb_tamletra_registros
                        'Encabezados de Los Productos
                        Call PoneEncabezados(vp, lCodProducto)
                        iLineasImpresas = .LineSpacing
                    Else
                        .FontBold = False
                        .FontSize = 10
                        .Paragraph = Trim(lReg("dsc_arbol_clase_inst_hoja").Value)
                        .FontBold = False
                        .FontSize = glb_tamletra_registros
                        Call PoneEncabezados(vp, lCodProducto)
                    End If

                    vp.MarginLeft = COLUMNA_INICIO    ' + rTwips("150mm")

                    For Each lReg_SA In lCursor_SA
                        
                        If iLineasImpresas + 220 > (.LineSpacing * (CONS_NUMERO_LINEAS - 3)) Then
                            .TableCell(tcBackColor, .TableCell(tcRows)) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                            .TableCell(tcForeColor, .TableCell(tcRows)) = RGB(255, 255, 255)   ' Blanco
                            .EndTable
                            .NewPage
                            .FontSize = 10
                            .Paragraph = Trim(lReg("dsc_arbol_clase_inst_hoja").Value) & "( Continuaci�n )"
                            .FontSize = glb_tamletra_registros
                            'Encabezados de Los Productos
                            Call PoneEncabezados(vp, lCodProducto)

                            iLineasImpresas = .LineSpacing
                        End If
                        iFila = .TableCell(tcRows)
                        Select Case lCodProducto    ' lReg("codigo").Value
                            Case "RV"
                                'agregado por MMA. 22/10/2008
                                dMontoPromedioCompra = lReg_SA("monto_promedio_compra").Value
                                .TableCell(tcText, iFila, 1) = Trim(lReg_SA("nemotecnico").Value)
                                .TableCell(tcText, iFila, 2) = lReg_SA("simbolo_moneda").Value
                                .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
                                .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("cantidad").Value, 0)
                                .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_compra").Value, 4)
                                .TableCell(tcText, iFila, 6) = FormatNumber(dMontoPromedioCompra, oCliente.Decimales)
                                
                                .TableCell(tcText, iFila, 7) = FormatNumber(lReg_SA("precio").Value, 4)
                                .TableCell(tcText, iFila, 8) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)    ' Valor de Mercado
                                If NVL(lReg_SA("rentabilidad").Value, 99999999) = 99999999 Then
                                    .TableCell(tcText, iFila, 9) = "-"
                                Else
                                    .TableCell(tcText, iFila, 9) = FormatNumber(lReg_SA("rentabilidad").Value, 2) & "%"
                                End If
                                .TableCell(tcAlign, iFila, 2) = taCenterMiddle
                                .TableCell(tcAlign, iFila, 3) = taRightMiddle
                                .TableCell(tcAlign, iFila, 4) = taRightMiddle
                                .TableCell(tcAlign, iFila, 5) = taRightMiddle
                                .TableCell(tcAlign, iFila, 6) = taRightMiddle
                                .TableCell(tcAlign, iFila, 7) = taRightMiddle
                                .TableCell(tcAlign, iFila, 8) = taRightMiddle
                                .TableCell(tcAlign, iFila, 9) = taRightMiddle

                                Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
                                Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
                                dTotalMontoPromedioCompra = dTotalMontoPromedioCompra + dMontoPromedioCompra

                            Case "FFMM"
                                .TableCell(tcText, iFila, 1) = Trim(lReg_SA("DSC_nemotecnico").Value)
                                .TableCell(tcText, iFila, 2) = Trim(lReg_SA("simbolo_moneda").Value)
                                .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
                                .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("cantidad").Value, 4)
                                .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_compra").Value, 4)
                                .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio").Value, 4)
                                .TableCell(tcText, iFila, 7) = FormatNumber(lReg_SA("monto_mon_nemotecnico").Value, Dame_Decimales(lReg_SA("id_moneda_nemotecnico").Value))
                                .TableCell(tcText, iFila, 8) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)
                               ' .TableCell(tcText, iFila, 8) = IIf(CDbl(lReg_SA("rentabilidad").Value) > 1000, "-", FormatNumber(lReg_SA("rentabilidad").Value, 2) & "%")
                                If NVL(lReg_SA("rentabilidad").Value, 99999999) = 99999999 Then
                                    .TableCell(tcText, iFila, 9) = "-"
                                Else
                                    .TableCell(tcText, iFila, 9) = FormatNumber(lReg_SA("rentabilidad").Value, 2) & "%"
                                End If
                                
                                .TableCell(tcAlign, iFila, 2) = taCenterMiddle
                                .TableCell(tcAlign, iFila, 3) = taRightMiddle
                                .TableCell(tcAlign, iFila, 4) = taRightMiddle
                                .TableCell(tcAlign, iFila, 5) = taRightMiddle
                                .TableCell(tcAlign, iFila, 6) = taRightMiddle
                                .TableCell(tcAlign, iFila, 7) = taRightMiddle
                                .TableCell(tcAlign, iFila, 8) = taRightMiddle
                                .TableCell(tcAlign, iFila, 9) = taRightMiddle

                                Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
                                Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)

                            Case "RF"
                                .TableCell(tcText, iFila, 1) = Trim(lReg_SA("nemotecnico").Value)
                                .TableCell(tcText, iFila, 2) = Left(Trim(lReg_SA("cod_emisor").Value), 16)
                                .TableCell(tcText, iFila, 3) = Trim(lReg_SA("SIMBOLO_MONEDA").Value)
                                .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
                                .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("cantidad").Value, 4)
                                .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("tasa_emision").Value, 4)       'modificado por req. 08/10/2008.
                                .TableCell(tcText, iFila, 7) = Trim(NVL(lReg_SA("fecha_vencimiento").Value, ""))
                                .TableCell(tcText, iFila, 8) = FormatNumber(lReg_SA("tasa_compra").Value, 4)  'modificado por req. 08/10/2008.
                                .TableCell(tcText, iFila, 9) = FormatNumber(lReg_SA("tasa").Value, 4)           'modificado por req. 08/10/2008.
                                .TableCell(tcText, iFila, 10) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)

                                .TableCell(tcAlign, iFila, 2) = taLeftMiddle
                                .TableCell(tcAlign, iFila, 3) = taCenterMiddle
                                .TableCell(tcAlign, iFila, 4) = taRightMiddle
                                .TableCell(tcAlign, iFila, 5) = taRightMiddle
                                .TableCell(tcAlign, iFila, 6) = taRightMiddle
                                .TableCell(tcAlign, iFila, 7) = taCenterMiddle
                                .TableCell(tcAlign, iFila, 8) = taRightMiddle
                                .TableCell(tcAlign, iFila, 9) = taRightMiddle
                                .TableCell(tcAlign, iFila, 10) = taRightMiddle

                                Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
                                Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
                                
                        End Select

                        iLineasImpresas = iLineasImpresas + .LineSpacing
                        .TableCell(tcRows) = .TableCell(tcRows) + 1
                        
                    Next  ' Nemotecnicos (For Each lReg_SA In lCursor_SA)
                    
                    iLineasImpresas = iLineasImpresas + .LineSpacing
                    iFila = .TableCell(tcRows)

                    Select Case lReg("codigo").Value
                        Case "RV"
                            .TableCell(tcText, iFila, 3) = FormatNumber(Porcentaje, 2) & "%"
                            .TableCell(tcAlign, iFila, 3) = taRightMiddle
                            .TableCell(tcText, iFila, 5) = "TOTAL"
                            .TableCell(tcAlign, iFila, 5) = taRightMiddle
                            .TableCell(tcText, iFila, 6) = FormatNumber(dTotalMontoPromedioCompra, oCliente.Decimales)
                            .TableCell(tcAlign, iFila, 6) = taRightMiddle
                            .TableCell(tcText, iFila, 8) = FormatNumber(Monto, oCliente.Decimales)
                            .TableCell(tcAlign, iFila, 8) = taRightMiddle

                        Case "FFMM"
                            .TableCell(tcText, iFila, 3) = FormatNumber(Porcentaje, 2) & "%"
                            .TableCell(tcAlign, iFila, 3) = taRightMiddle

                            .TableCell(tcText, iFila, 7) = "TOTAL"
                            .TableCell(tcAlign, iFila, 7) = taRightMiddle

                            .TableCell(tcText, iFila, 8) = FormatNumber(Monto, oCliente.Decimales)
                            .TableCell(tcAlign, iFila, 8) = taRightMiddle

                        Case "RF"
                            .TableCell(tcText, iFila, 4) = FormatNumber(Porcentaje, 2) & "%"
                            .TableCell(tcAlign, iFila, 4) = taRightMiddle

                            .TableCell(tcText, iFila, 9) = "TOTAL"
                            .TableCell(tcAlign, iFila, 9) = taRightMiddle

                            .TableCell(tcText, iFila, 10) = FormatNumber(Monto, oCliente.Decimales)
                            .TableCell(tcAlign, iFila, 10) = taRightMiddle

                    End Select

                    .TableCell(tcBackColor, iFila) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                    .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)   ' Blanco
                    
                    .EndTable
                    
                    .Paragraph = ""
                    
                    'Agregado por MMA. 03/10/2008
'                    If sDscInstrumento = "Acciones" And anterior <> lReg("id_arbol_clase_inst_hoja").Value Then
'                        iLineasImpresas = iLineasImpresas + (12 * .LineSpacing)
'
'                        If iLineasImpresas + 220 > (.LineSpacing * (CONS_NUMERO_LINEAS - 3)) Then
'                                .NewPage
'                                .FontSize = 10
'                                .Paragraph = Trim(lReg("dsc_arbol_clase_inst_hoja").Value) & "( Continuaci�n )"
'                                .FontSize = glb_tamletra_registros
'
'                                iLineasImpresas = iLineasImpresas + (12 * .LineSpacing)
'                        End If
'                        Call DibujaGraficoRV(lReg("id_arbol_clase_inst_hoja").Value)
'                        For i = 1 To 10
'                            .Paragraph = ""
'                             iLineasImpresas = iLineasImpresas + .LineSpacing
'                        Next
'                    End If
                    .Paragraph = ""
                    iLineasImpresas = iLineasImpresas + .LineSpacing

                    
                'End If
                
            Else
                iLineasImpresas = .LineSpacing
            End If

             'anterior = lreg("id_arbol_clase_inst").Value

        Next                ' For Each lReg In lCursor_Ins

        Set lCursor_Ins = Nothing
        Set lReg = Nothing

    End With

End Sub

Private Sub DibujaGraficoRV(ByVal Id_Arbol_Clase As Integer)
Dim nMarginLeft As Variant
Dim nColinicio As Variant
Dim iFila As Integer

Dim lCursor_Cuotas As hRecord
Dim lReg As hFields

Dim sNombres()
Dim sValores()

Dim i As Integer
Dim iFilaComienzo As Variant

Dim Dsc_Instrumento As String
Dim monto_mon_cta As Double
Dim MontoTotal As Double

Dim j As Integer
Dim aux As Double
Dim aux1 As String

Dim iFilaActual As Variant
Dim nMargenLeft As Variant

Dim sName As String
    
    On Local Error Resume Next
    vp.CurrentY = vp.CurrentY + rTwips("2mm")
    pon_titulo "RENTA VARIABLE POR SECTORES ECONOMICOS", "5mm", Font_Name, glb_tamletra_titulo
    vp.CurrentY = vp.CurrentY + rTwips("2mm")
    
    FilaAux = vp.CurrentY
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "pfecha_cierre", ePT_Fecha, oCliente.Fecha_Cartola, ePD_Entrada
    gDB.Parametros.Add "pid_arbol_clase", ePT_Numero, Id_Arbol_Clase, ePD_Entrada
    gDB.Parametros.Add "pId_Moneda_Salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$BUSCAR_RV_SECTOR"
    
    If gDB.EjecutaSP Then
        Set lCursor_Cuotas = gDB.Parametros("pcursor").Valor
    Else
        Exit Sub
    End If
    MontoTotal = 0
    With vp
        iFilaComienzo = .CurrentY
        
        .StartTable
        .TableBorder = tbBox
        .TableCell(tcRows) = 1
        .TableCell(tcCols) = 2
        .TableCell(tcBackColor, 1, 1, 1, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)   ' Blanco

        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)   ' Blanco
        .TableCell(tcText, 1, 1) = "SECTORES ECONOMICOS"
        .TableCell(tcColWidth, , 1) = "78mm"
        .TableCell(tcColAlign, , 1) = taRightMiddle
        .TableCell(tcText, 1, 2) = "MONTO" ' (" & Trim(Monto_Moneda(oCliente.Moneda)) & ")"
        .TableCell(tcColWidth, , 2) = "45mm"
        .TableCell(tcColAlign, , 2) = taRightMiddle
        
        '.TableCell(tcAlign, 1, 2, 1, 2) = taCenterMiddle
        .TableCell(tcFontSize) = glb_tamletra_registros
        
        iFila = .TableCell(tcRows) + 1
        .TableCell(tcRows) = iFila
        
        For Each lReg In lCursor_Cuotas
            
            Dsc_Instrumento = Trim(lReg("DSC_SECTOR").Value)
            monto_mon_cta = Trim(lReg("monto_mon_cta").Value)
            
            .TableCell(tcText, iFila, 1) = Dsc_Instrumento
            .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
            .TableCell(tcText, iFila, 2) = FormatNumber(monto_mon_cta, oCliente.Decimales)
            
            iFila = .TableCell(tcRows) + 1
            .TableCell(tcRows) = iFila
            
            If monto_mon_cta <> 0 Then
                ReDim Preserve sNombres(i)
                ReDim Preserve sValores(i)

                sNombres(i) = Dsc_Instrumento '& " "
                sValores(i) = monto_mon_cta
                i = i + 1
                MontoTotal = MontoTotal + monto_mon_cta
            End If
            
        Next
        iFila = .TableCell(tcRows) + 1
        .TableCell(tcRows) = iFila
            
        .TableCell(tcText, iFila, 1) = "TOTAL"
        .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
        .TableCell(tcText, iFila, 2) = FormatNumber(MontoTotal, oCliente.Decimales)
        .TableCell(tcBackColor, iFila, 1, iFila, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, iFila) = RGB(255, 255, 255)   ' Blanco
    
        .EndTable
    End With
    
    iFilaActual = vp.CurrentY

    If i > 0 Then
        For i = LBound(sValores) To UBound(sValores)
            For j = i + 1 To UBound(sValores) - 1
                If CDbl(sValores(i)) > CDbl(sValores(j)) Then
                    aux = sValores(j)
                    sValores(j) = sValores(i)
                    sValores(i) = aux

                    aux1 = sNombres(j)
                    sNombres(j) = sNombres(i)
                    sNombres(i) = aux1
                End If
            Next
        Next

        sName = fncGraficoTortaRV(sNombres, sValores, "RENTA VARIABLE POR SECTOR", -1, False)

        With vp
            .CurrentY = iFila
            nMargenLeft = .MarginLeft
            .MarginLeft = COLUMNA_DOS 'rTwips(COLUMNA_DOS) - rTwips("30mm")
            '.DrawPicture LoadPicture(sName), vp.MarginLeft + rTwips("2mm"), iFilaComienzo, "95mm", "80mm", , False
            '.DrawPicture LoadPicture(sName), vp.MarginLeft + rTwips("10mm"), FilaAux, "90mm", "55mm", , False
            .DrawPicture LoadPicture(sName), vp.MarginLeft - rTwips("6mm"), FilaAux - 100, "140mm", "95mm", , False
            .MarginLeft = nMargenLeft
            .CurrentY = FilaAux + 1000
        End With
    End If
    
    

End Sub
Function fncGraficoTortaRV(catnom, valores, sTitulo, tipo, bLeyenda)
    Dim xColorTitulo As String
    Dim xColor As String
    Dim aColors()
    Dim cd, c
    Dim Path As String
    Dim sFname As String
    Dim xColor1 As String
    Dim xColor2 As String
    Dim xColor3 As String
    Dim xColor4 As String
    Dim pCadena As String
    Dim i As Integer
    Dim j As Integer
    Dim lMatrix
    
    xColor1 = ColorToHex(101, 76, 254) ' CELESTE
    xColor2 = ColorToHex(19, 30, 128) ' AZUL
    xColor3 = ColorToHex(163, 159, 210) ' MAS CELESTE
    xColor4 = ColorToHex(204, 22, 22) ' ROJO
    
    xColorTitulo = ColorToHex(0, 0, 0)
    xColor = ColorToHex(RColorRelleno, GColorRelleno, BColorRelleno)
    
    j = 0
    pCadena = xColor3 & ";"
    For i = 0 To UBound(valores)
        j = j + 1
        If j > 3 Then
           j = 1
        End If
        Select Case j
            Case 1
                pCadena = pCadena & xColor1 & ";"
            Case 2
                pCadena = pCadena & xColor2 & ";"
            Case 3
                pCadena = pCadena & xColor3 & ";"
        End Select
        
    Next
    pCadena = Mid(Trim(pCadena), 1, Len(Trim(pCadena)) - 1)
    aColors = Array(ColorToHex(0, 0, 102), _
                    ColorToHex(204, 0, 0), _
                    ColorToHex(101, 76, 254))
    lMatrix = Split(pCadena, ";")
                    
'    aColors = Array(&HFF066, _
'                    &HADBDF1, _
'                    &H654CFE) ', _
'                    &HADBDF1, _
'                    &HF1BDE, _
'                    &H8A8077, _
'                    &HEEF5F1, _
'                    &HBDB8B3, _
'                    &HD6C8BA, _
'                    &HC2B5A7, _
'                    &HC4B7A9, _
'                    &H403D30)
                    
    
    Set cd = CreateObject("ChartDirector.API")
    
    cd.setLicenseCode ("YT5ZJ59NKAF9ZR3942DAE53E")

    'Set c = cd.PieChart(700, 450) ', , -1, 1)  '680,370
    Set c = cd.PieChart(680, 370)

    ' Set the center of the pie at (280, 135) and the radius to 110 pixels
    'Call c.setPieSize(350, 135, 110)  '(330,135,100)
    Call c.setPieSize(340, 95, 90)

    ' Draw the pie in 3D with 20 pixels 3D depth
    Call c.set3D(20)  '(20)
    
    ' Use the side label layout method
    Call c.setLabelLayout(cd.SideLayout)

    ' Set the label box background color the same as the sector color, with glass
    ' effect, and with 5 pixels rounded corners
    Dim t As Object 'cd.TextBox
    Set t = c.setLabelStyle("arial.ttf", 9, RGB(255, 255, 255))
    Call t.setBackground(cd.SameAsMainColor, cd.Transparent, cd.softlighting())
    Call t.setRoundedCorners(5)

    ' Set the border color of the sector the same color as the fill color. Set the
    ' line color of the join line to black (0x0)
    Call c.setLineColor(cd.SameAsMainColor, &H0)


    ' Set the start angle to 135 degrees may improve layout when there are many small
    ' sectors at the end of the data array (that is, data sorted in descending
    ' order). It is because this makes the small sectors position near the horizontal
    ' axis, where the text label has the least tendency to overlap. For data sorted
    ' in ascending order, a start angle of 45 degrees can be used instead.
    Call c.setStartAngle(120)

    Call c.setColors2(cd.DataColor, lMatrix)
    Call c.SetData(valores, catnom)

    sTitulo = "<*size=12*><*block*><*color=" & xColorTitulo & "*>" & Space(10) & sTitulo & "<*/*>"

    Call c.addTitle(sTitulo, "arial.ttf", 16, xColorTitulo)
        
    'Call c.setLabelPos(50, cd.LineColor)
    
    'Call c.setColors(aColors)

    'Path = App.Path & "\" '    App.Path ' Left(Request.ServerVariables("PATH_INFO"), InStrRev(Request.ServerVariables("PATH_INFO"), "/graficos/"))
    
    'MODIFICADO PARA QUE EL SISTEMA DEJE EN UN DIRECTORIO TEMPORAL EL JPG QUE SE GENERA DEBIDO A QUE ESTABA
    'ENSUCIANDO EL DIRECTORIO DE INSTALACION DEL SISTEMA, LO CUAL, ERA INCORRECTO.�
    'HAF: 20070620.
    Path = Environ$("TEMP") & "\"

    sFname = c.makeTmpFile(Path & sFname, cd.JPG)

    fncGraficoTortaRV = Path & sFname

End Function

Private Sub PoneEncabezados(vp, lCodProducto)
    With vp
        .StartTable
        .TableBorder = tbBox
        .TableBorder = tbBoxRows
        
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcFontSize) = glb_tamletra_registros
    
        .TableCell(tcText, 1, 2) = ""
        .TableCell(tcText, 1, 4) = ""

        Select Case lCodProducto
            Case "RV"
                .TableCell(tcCols) = 9
                .TableCell(tcText, 1, 1) = "NEMOTECNICO"
                .TableCell(tcText, 1, 2) = "MONEDA"
                .TableCell(tcText, 1, 3) = "  % DE   " & vbCrLf & "INVERSION"
                .TableCell(tcText, 1, 4) = "CANTIDAD"
                .TableCell(tcText, 1, 5) = "PRECIO PROMEDIO" & vbCrLf & "     COMPRA    "
                .TableCell(tcText, 1, 6) = "MONTO PROMEDIO" & vbCrLf & "    COMPRA    "
                .TableCell(tcText, 1, 7) = "PRECIO" & vbCrLf & "ACTUAL"
                .TableCell(tcText, 1, 8) = "VALOR MERCADO"
                .TableCell(tcText, 1, 9) = "RENTABILIDAD"
                
                .TableCell(tcColWidth, 1, 1) = "50mm": .TableCell(tcAlign, 1, 1) = taLeftMiddle  '70mm
                .TableCell(tcColWidth, 1, 2) = "15mm": .TableCell(tcAlign, 1, 2) = taCenterMiddle  '20mm
                .TableCell(tcColWidth, 1, 3) = "20mm": .TableCell(tcAlign, 1, 3) = taRightMiddle  '25mm
                .TableCell(tcColWidth, 1, 4) = "25mm": .TableCell(tcAlign, 1, 4) = taRightMiddle
                .TableCell(tcColWidth, 1, 5) = "30mm": .TableCell(tcAlign, 1, 5) = taRightMiddle
                .TableCell(tcColWidth, 1, 6) = "30mm": .TableCell(tcAlign, 1, 6) = taRightMiddle
                .TableCell(tcColWidth, 1, 7) = "25mm": .TableCell(tcAlign, 1, 7) = taRightMiddle
                .TableCell(tcColWidth, 1, 8) = "35mm": .TableCell(tcAlign, 1, 8) = taRightMiddle
                .TableCell(tcColWidth, 1, 9) = "25mm": .TableCell(tcAlign, 1, 9) = taRightMiddle
                
            Case "RF"
                .TableCell(tcCols) = 10
                .TableCell(tcText, 1, 1) = "NEMOTECNICO"
                .TableCell(tcText, 1, 2) = "EMISOR"
                .TableCell(tcText, 1, 3) = "MONEDA"
                .TableCell(tcText, 1, 4) = "  % DE   " & vbCrLf & "INVERSION"
                .TableCell(tcText, 1, 5) = "" & vbCrLf & "NOMINALES"
                .TableCell(tcText, 1, 6) = "TASA CUP�N"
                .TableCell(tcText, 1, 7) = "FECHA" & vbCrLf & "VCTO."
                .TableCell(tcText, 1, 8) = "TASA COMPRA"
                .TableCell(tcText, 1, 9) = "TASA MERCADO"
                .TableCell(tcText, 1, 10) = "VALORIZACI�N"
                
                .TableCell(tcColWidth, 1, 1) = "40mm": .TableCell(tcAlign, 1, 1) = taLeftMiddle
                .TableCell(tcColWidth, 1, 2) = "25mm": .TableCell(tcAlign, 1, 2) = taLeftMiddle
                .TableCell(tcColWidth, 1, 3) = "22mm": .TableCell(tcAlign, 1, 3) = taCenterMiddle
                .TableCell(tcColWidth, 1, 4) = "20mm": .TableCell(tcAlign, 1, 4) = taRightMiddle
                .TableCell(tcColWidth, 1, 5) = "25mm": .TableCell(tcAlign, 1, 5) = taRightMiddle
                .TableCell(tcColWidth, 1, 6) = "18mm": .TableCell(tcAlign, 1, 6) = taRightMiddle
                .TableCell(tcColWidth, 1, 7) = "25mm": .TableCell(tcAlign, 1, 7) = taCenterMiddle
                .TableCell(tcColWidth, 1, 8) = "25mm": .TableCell(tcAlign, 1, 8) = taRightMiddle
                .TableCell(tcColWidth, 1, 9) = "25mm": .TableCell(tcAlign, 1, 9) = taRightMiddle
                .TableCell(tcColWidth, 1, 10) = "30mm": .TableCell(tcAlign, 1, 10) = taRightMiddle
                
        
            Case "FFMM"
                .TableCell(tcCols) = 9
                .TableCell(tcText, 1, 1) = "DESCRIPCI�N"
                .TableCell(tcText, 1, 2) = "MONEDA"
                .TableCell(tcText, 1, 3) = "  % DE   " & vbCrLf & "INVERSION"
                .TableCell(tcText, 1, 4) = "CANTIDAD DE" & vbCrLf & "   CUOTAS  "
                .TableCell(tcText, 1, 5) = "  VALOR CUOTA  " & vbCrLf & "PROMEDIO COMPRA"
                .TableCell(tcText, 1, 6) = "VALOR CUOTA" & vbCrLf & "   ACTUAL  "
                .TableCell(tcText, 1, 7) = "VALOR ORIGINAL "
                .TableCell(tcText, 1, 8) = "VALOR ACTUAL " ' & vbCrLf & "MON. CUENTA"
                .TableCell(tcText, 1, 9) = "RENTABILIDAD"
                
                .TableCell(tcColWidth, 1, 1) = "50mm": .TableCell(tcAlign, 1, 1) = taLeftMiddle '70mm
                .TableCell(tcColWidth, 1, 2) = "15mm": .TableCell(tcAlign, 1, 2) = taCenterMiddle '20mm
                .TableCell(tcColWidth, 1, 3) = "20mm": .TableCell(tcAlign, 1, 3) = taRightMiddle '25mm
                .TableCell(tcColWidth, 1, 4) = "25mm": .TableCell(tcAlign, 1, 4) = taRightMiddle
                .TableCell(tcColWidth, 1, 5) = "30mm": .TableCell(tcAlign, 1, 5) = taRightMiddle
                .TableCell(tcColWidth, 1, 6) = "25mm": .TableCell(tcAlign, 1, 6) = taRightMiddle
                .TableCell(tcColWidth, 1, 7) = "30mm": .TableCell(tcAlign, 1, 7) = taRightMiddle
                .TableCell(tcColWidth, 1, 8) = "35mm": .TableCell(tcAlign, 1, 8) = taRightMiddle
                .TableCell(tcColWidth, 1, 9) = "25mm": .TableCell(tcAlign, 1, 9) = taRightMiddle

        End Select
        
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcBackColor, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                           
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)  ' Blanco
        .TableCell(tcForeColor, 2) = RGB(255, 255, 255)  ' Blanco
        
        
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        
        iLineasImpresas = iLineasImpresas + (.LineSpacing * .TableCell(tcRows))

    End With
End Sub


Private Sub Sub_Carga_Grilla()
    Dim lCursor         As Object
    Dim lReg            As Object
    Dim lIdMonedaSalida As Integer
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    gDB.Parametros.Add "pId_Usuario", ePT_Numero, gID_Usuario, ePD_Entrada
    
    'gDB.Parametros.Add "Pid_asesor", ePT_Numero, gID_Usuario, ePD_Entrada
    If bCargaUnitaria Then       'MMA 10/09/2008 : CUENTA o TODAS
        lId_Cuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Cuentas))
        gDB.Parametros.Add "pId_Cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    End If
    gDB.Procedimiento = "PKG_CUENTAS$Buscar_Vigentes" ' PKG_CUENTAS$Buscar"
    
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If
    
    Set lCursor = gDB.Parametros("Pcursor").Valor
    'Grilla_Cuentas.Rows = 1
    
    If bCargaUnitaria Then
        lIdMonedaSalida = IIf(Fnt_ComboSelected_KEY(Cmb_Moneda) = cCmbKALL, 0, CInt(Fnt_ComboSelected_KEY(Cmb_Moneda)))
    End If
    
    Dim lLinea
    Dim lAsesor As String
    For Each lReg In lCursor
        If bCargaUnitaria Then
            lAsesor = Txt_Asesor.Text
        Else
            lAsesor = RescataAsesor(lReg("id_cuenta").Value)
        End If
        lLinea = Grilla_Cuentas.Rows
        Call Grilla_Cuentas.AddItem("")
        Call SetCell(Grilla_Cuentas, lLinea, "colum_pk", Trim(lReg("id_cuenta").Value), pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "num_cuenta", lReg("num_cuenta").Value, pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "Rut_cliente", lReg("rut_cliente").Value, pAutoSize:=True)
        Call SetCell(Grilla_Cuentas, lLinea, "razon_social", lReg("nombre_cliente").Value, pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "moneda", lReg("Simbolo_Moneda").Value, pAutoSize:=False)
        If bCargaUnitaria Then
            Call SetCell(Grilla_Cuentas, lLinea, "id_moneda_salida", lIdMonedaSalida, pAutoSize:=False)         'MMA 10/09/2008
            Call SetCell(Grilla_Cuentas, lLinea, "dsc_moneda_salida", Cmb_Moneda.Text, pAutoSize:=False)        'MMA 10/09/2008
        Else
            Call SetCell(Grilla_Cuentas, lLinea, "id_moneda_salida", lReg("id_moneda").Value, pAutoSize:=False)         'MMA 10/09/2008
            Call SetCell(Grilla_Cuentas, lLinea, "dsc_moneda_salida", lReg("dsc_moneda").Value, pAutoSize:=False)        'MMA 10/09/2008
        End If
        Call SetCell(Grilla_Cuentas, lLinea, "asesor", lAsesor, pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "fecha_cierre", NVL(lReg("Fecha_Cierre_Cuenta").Value, ""), pAutoSize:=False)
    Next
    Call Sub_CambiaCheck(True)      'MMA 10/09/2008
End Sub


Public Function PadC(ByVal sSrc As String, ByVal iLen As Integer, Optional ByVal sChr As String = " ") As String
    Dim iLeft As Integer
    Dim iRight As Integer
    
    If iLen > Len(sSrc) Then
        If iLen Mod 2 = 0 Then
            'This is an even length output string...
            iLeft = (iLen - Len(sSrc)) \ 2
        Else
            'This is an odd length output string...
            iLeft = ((iLen + 1) - Len(sSrc)) \ 2
        End If
        iRight = iLen - (iLeft + Len(sSrc))

        PadC = String(iLeft, sChr) & sSrc & String(iRight, sChr)
    Else
        PadC = sSrc
    End If

End Function

Public Function PadR(ByVal sSrc As String, ByVal iLen As Integer, Optional ByVal sChr As String = " ") As String
    Dim x As Integer
    x = iLen - Len(sSrc)
    If x > 0 Then
        PadR = sSrc & String(x, sChr)
    Else
        PadR = sSrc
    End If
End Function

Public Function PadL(ByVal sSrc As String, ByVal iLen As Integer, Optional ByVal sChr As String = " ") As String
    Dim x As Integer
    x = iLen - Len(sSrc)
    If x > 0 Then
        PadL = String(x, sChr) & sSrc
    Else
        PadL = sSrc
    End If
End Function

Private Sub Toolbar_Chequeo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
       Call Sub_CambiaCheck(True)
    Case "SEL_NOTHING"
       Call Sub_CambiaCheck(False)
   End Select
End Sub

Private Sub Sub_CambiaCheck(pValor As Boolean)
    Dim lLinea      As Long
    Dim lCol        As Long
  
  
    lCol = Grilla_Cuentas.ColIndex("CHK")
    If pValor Then
        For lLinea = 1 To Grilla_Cuentas.Rows - 1
            Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexChecked
        Next
    Else
        For lLinea = 1 To Grilla_Cuentas.Rows - 1
            Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexUnchecked
        Next
    End If
  
End Sub

'*******************************************************************************************************************
'Sub Indicadores()
'    Dim xAnt As Variant
'    Dim yAnt As Variant
'    Dim xtalign As Variant
'    Dim xbrushcolor As Variant
'    Dim xpencolor As Variant
'    Dim xtextcolor As Variant
'    Dim xfontsize As Variant
'    Dim xmarginleft As Variant
'
'    Dim xValTop As Variant
'
'    xValTop = ""
'
'    'Inserta Logo Moneda
'    ' .Image('imagenes/logomoneda.jpg',19,12,50)
'    ' vp.DrawPicture p1.Picture, vp.MarginLeft, "20mm"
'    'Guarda valores anteriores
'
'    xAnt = vp.CurrentX
'    yAnt = vp.CurrentY
'    xtalign = vp.TextAlign
'    xbrushcolor = vp.BrushColor
'    xpencolor = vp.PenColor
'    xtextcolor = vp.TextColor
'    xfontsize = vp.FontSize
'    xmarginleft = vp.MarginLeft
'
''Asigna nuevos valores
'    vp.BrushColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'    vp.PenColor = RGB(100, 100, 100)
'    vp.TextColor = RGB(100, 100, 100)
'    vp.FontSize = 10
'
'    'Llena texto
'    vp.TextBox " ", "205mm", "11mm", "60mm", "11mm", True, False, True
'
'    'Asigna nuevos valores
'    vp.TextAlign = taLeftMiddle
'    vp.BrushColor = RGB(232, 226, 222)
'
'    'vp.PenColor = RGB(100, 100, 100)
'    vp.TextColor = &H0&
'    vp.FontSize = 8
'
'    'Llena texto
'    vp.DrawRectangle "205mm", "17mm", "265mm", "40mm"
'
'    vp.MarginLeft = "210mm"
'    vp.CurrentX = "210mm"
'    vp.CurrentY = "18mm"
'
'    vp.Text = "Valor D�lar Observado" & vbCrLf & "Valor UF"
'
'    vp.MarginLeft = "240mm"
'    vp.CurrentX = "240mm"
'    vp.CurrentY = "18mm"
'    vp.TextAlign = taRightMiddle
'
'    'vp.Text = Space(10) & vbCrLf & Space(10) & vbCrLf & Space(10) & vbCrLf & Space(10) & vbCrLf & Space(10)
'    vp.Text = "580,40" & vbCrLf & "18.400,10"
'
'
'    'Recupera valores anteriores
'    vp.CurrentX = xAnt
'    vp.CurrentY = yAnt
'    vp.TextAlign = xtalign
'    vp.BrushColor = xbrushcolor
'    vp.PenColor = xpencolor
'    vp.TextColor = xtextcolor
'    vp.FontSize = xfontsize
'    vp.MarginLeft = xmarginleft
'End Sub

'************************************ FIN MODIFICACIONES *************************************
'Sub TraeDatosCliente()
'    Dim lCuentas As Class_Cuentas
'    Dim lcMoneda As Class_Monedas
'    Dim lCursor_Cta As hRecord
'    Dim lReg_Cta As hFields
'
'    Dim lNum_Cuenta As String
'    '------------------------------------------
'    Dim lId_Cliente As String
'    Dim lCod_Estado As String
'    Dim lId_Asesor As String
'    '------------------------------------------
'    Dim lNom_Clt As String
'    Dim lEstado_Clt As String
'    Dim lFlg_Bloqueado As String
'    Dim lMoneda_Clt As String
'    '------------------------------------------
'    Dim lcClientes  As Object
'    '------------------------------------------
'    Dim lAsesor As String
'    Dim lcDireccion_Cliente As Object
'    Dim lId_Cuenta As String
'
'    lId_Cuenta = oCliente.IdCuenta
'
'
'    Call Sub_Bloquea_Puntero(Me)
'    '---------------------------------------
'    Rem Busca el numero de la cuenta
'    Set lCuentas = New Class_Cuentas
'    With lCuentas
'        .Campo("id_cuenta").Valor = lId_Cuenta
'        If .Buscar(True) Then
'          Set lCursor_Cta = .Cursor
'        Else
'          MsgBox .ErrMsg, vbCritical, Me.Caption
'          'GoTo ErrProcedure
'        End If
'    End With
'
'    Rem Busca el nombre del cliente
'    Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
'
'    With lcClientes
'        Set .gDB = gDB
'        .Campo("id_Cliente").Valor = lCursor_Cta(1)("id_cliente").Value
'        If .Buscar(True) Then
'            oCliente.Nombre = NVL(.Cursor(1)("nombre_cliente").Value, "")
'        Else
'            MsgBox .ErrMsg, vbCritical, Me.Caption
'            Set lcClientes = Nothing
'            'GoTo ErrProcedure
'        End If
'    End With
'
'    Set lcDireccion_Cliente = Fnt_CreateObject(cDLL_Direccion_Cliente)
'    With lcDireccion_Cliente
'      Set .gDB = gDB
'      .Campo("Id_Cliente").Valor = lcClientes.Campo("id_Cliente").Valor
'      If .Buscar Then
'        If .Cursor.Count > 0 Then
'            oCliente.Direccion = NVL(.Cursor(1)("Direccion").Value, "")
'            oCliente.Telefono = NVL(.Cursor(1)("fono").Value, "")
'        Else
'            oCliente.Direccion = "Sin Direcci�n"
'            oCliente.Telefono = "Sin Fono"
'        End If
'      Else
'        MsgBox "Problemas en cargar la Direcci�n del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
'      End If
'    End With
'
'    Set lcClientes = Nothing
'    Set lcDireccion_Cliente = Nothing
'
'    Rem Busca el nombre de la moneda
'    Set lcMoneda = New Class_Monedas
'    With lcMoneda
'        .Campo("id_Moneda").Valor = lCursor_Cta(1)("id_moneda").Value
'        If .Buscar Then
'            lMoneda_Clt = NVL(.Cursor(1)("dsc_moneda").Value, "")
'        Else
'            MsgBox .ErrMsg, vbCritical, Me.Caption
'            Set lcMoneda = Nothing
'            ' GoTo ErrProcedure
'        End If
'    End With
'    Set lcMoneda = Nothing
'
'    ' oCliente.Nombre = Trim(registros("cartera"))       ' registros("razon_social").Value
'
'    If Len(oCliente.Nombre) > 37 Then
'        oCliente.Nombre = Mid(oCliente.Nombre, 1, 37)
'    End If
'
'    oCliente.Rut = NVL(lCursor_Cta(1)("RUT_CLIENTE").Value, "")
'    oCliente.Cuenta = NVL(lCursor_Cta(1)("NUM_CUENTA").Value, "")
'    oCliente.Ejecutivo = NVL(lCursor_Cta(1)("DSC_ASESOR").Value, "")
'    oCliente.Mail = ""
'    oCliente.Fecha_Creacion = lCursor_Cta(1)("FECHA_OPERATIVA").Value
'    oCliente.Moneda_fondo = lMoneda_Clt
'
'    'glb_cliente.Fecha_Proceso = lCursor_Cta(1)("id_cliente").Value
'    Set lCursor_Cta = Nothing
'
'End Sub

'#############################################################################################
'Sub FlujoPatrimonial()
'    Dim nColinicio As String
'    Dim n As Double
'    Dim lReg As hCollection.hFields
'    Dim lLinea As Long
'    Dim lAporte_Rescate_Cuenta As Class_APORTE_RESCATE_CUENTA
''Dim lcCuenta As Class_Cuentas
''
''  Load Me
''
''  'Se tiene que cargar aca debido a que todavia no se sabe ID que le corresponde
''  Call Sub_CargaCombo_Clientes_Ctas_Ctes_Cta(Cmb_Cta_Cte, fId_Cuenta)
''
''  fEstado = ""
'
''
''        Call Sub_ComboSelectedItem(Cmb_Medios_Pago, lReg("cod_medio_pago").Value)
''        Call Sub_ComboSelectedItem(Cmb_Bancos, NVL(lReg("id_banco").Value, ""))
''        Call Sub_ComboSelectedItem(Cmb_Cajas, lReg("Id_Caja_Cuenta").Value)
''        Txt_Observaci�n.Text = lReg("dsc_Apo_Res_Cuenta").Value
''        fTipo_Movimiento = lReg("flg_Tipo_Movimiento").Value
''        Txt_Num_Documento.Text = "" & lReg("num_Documento").Value
''        Txt_Dias_Retencion.Text = lReg("retencion").Value
''        Txt_Monto.Text = lReg("monto").Value
''        Cmb_Cta_Cte.Text = "" & lReg("cta_Cte_Bancaria").Value
''        fEstado = lReg("cod_estado").Value
''        Txt_FechaMovimiento.Text = Format(lReg("fecha_movimiento").Value, cFormatDate)
''      Next
''    Else
''      Call Fnt_MsgError(.SubTipo_LOG _
''                        , "Problemas al buscar el Aporte/Rescate." _
''                        , .ErrMsg _
''                        , pConLog:=True)
''    End If
''  End With
''  Set lAporte_Rescate_Cuenta = Nothing
'
'
'    Dim sFechaInicio, sFechaTermino
'    Dim nDecimales As Integer
'    Dim nMarginLeft As String
'    Dim iFila As Variant
'    Dim xSql As String
'    Dim oRs As New ADODB.Recordset
'    Dim nAporte As Double
'    Dim nRetiro As Double
'    Dim nFlujo As Double
'    Dim bHayDatos As Boolean
'    Dim i As Integer
'
'    'sFechaInicio = FormatoFecha(DateAdd("yyyy", -1, glb_fecha_hoy))
'    'sFechaTermino = glb_fecha_hoy
'
'    With vp
'        vp.StartTable
'        vp.TableBorder = tbBox
'        vp.TableCell(tcRowHeight, 1, 1, 7, 5) = "2mm"
'        vp.TableCell(tcRows) = 12
'        vp.TableCell(tcCols) = 3
'        vp.TableCell(tcBackColor, 1, 1, 1, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'        vp.TableCell(tcText, 1, 1) = "FLUJO PATRIMONIAL ANUAL "
'        vp.TableCell(tcRowBorderBelow, 1) = 2
'        vp.TableCell(tcColSpan, 1, 1) = 3
'
'        vp.TableCell(tcText, 2, 1) = "Fecha"
'        vp.TableCell(tcText, 2, 2) = "Aportes (" & Monto_Moneda(lDsc_Moneda) & ")"
'        vp.TableCell(tcText, 2, 3) = "Retiros (" & Monto_Moneda(lDsc_Moneda) & ")"
'
'        vp.TableCell(tcColWidth, 1, 1, 9, 3) = "33mm"
'        vp.TableCell(tcColAlign, 2, 2, 9, 3) = taRightMiddle
'        vp.TableCell(tcFontSize, 2, 2, 9, 3) = glb_tamletra_registros
'
'        iFila = 2
'        nAporte = 0
'        nRetiro = 0
'
'        nFlujo = 0
'        n = 0
'
'        bHayDatos = False
'        nDecimales = 2
'
'        Set lAporte_Rescate_Cuenta = New Class_APORTE_RESCATE_CUENTA
'        With lAporte_Rescate_Cuenta
'            .Campo("Id_Cuenta").Valor = lId_Cuenta
'            bHayDatos = False
'            If .Buscar_Descendente Then
'                For Each lReg In .Cursor
'                    If .Cursor.Count > 0 Then
'                        bHayDatos = True
'                        If iFila <= 9 Then
'                            'vp.CurrentX = rTwips(nColinicio)
'                            vp.TableCell(tcText, iFila + 1, 1) = Format(lReg("fecha_movimiento").Value, cFormatDate)
'                            If Trim(lReg("flg_tipo_movimiento").Value) = "A" Then
'                                nAporte = Abs(CDbl(lReg("monto").Value))
'                                nRetiro = 0
'                            Else
'                                nAporte = 0
'                                nRetiro = Abs(CDbl(lReg("monto").Value))
'                            End If
'                            If nAporte <> 0 Then
'                                vp.TableCell(tcText, iFila + 1, 2) = FormatNumber(nAporte, nDecimales)
'                            Else
'                                vp.TableCell(tcText, iFila + 1, 2) = " "
'                            End If
'                            If nRetiro <> 0 Then
'                                vp.TableCell(tcText, iFila + 1, 3) = FormatNumber(nRetiro, nDecimales)
'                            Else
'                                vp.TableCell(tcText, iFila + 1, 3) = " "
'                            End If
'                        End If
'                        iFila = iFila + 1
'                    End If
'                Next
'            End If
'        End With
'        Set lAporte_Rescate_Cuenta = Nothing
'        For i = iFila To 10
'            If i = iFila And Not bHayDatos Then
'                vp.TableCell(tcText, i + 2, 1) = "Sin Movimientos"
'            Else
'                vp.TableCell(tcText, i + 2, 1) = " "
'            End If
'
'            vp.TableCell(tcColSpan, i + 2, 1) = 3
'            vp.TableCell(tcColAlign, i + 2, 1) = taCenterTop
'            ' iFila = iFila + 1
'        Next
'
'        ' .TableCell(tcText, i + 1, 1) = "Neto UF Equivalente"
'        '.TableCell(tcText, i + 1, 1) = "Aporte/Retiro UF Equivalente"
'        '.TableCell(tcAlign, i + 1, 1) = taLeftMiddle
'        '.TableCell(tcColSpan, i + 1, 1) = 2
'        '.TableCell(tcText, i + 1, 3) = FormatNumber(nFlujo, 2)
'        '.TableCell(tcAlign, i + 1, 3) = taCenterMiddle
'
'
'        '.TableCell(tcFontSize, 1, 1, i + 3, 3) = glb_tamletra_registros
'        '.TableCell(tcRowBorderBelow, i + 1) = 2
'        '.TableCell(tcRowBorderAbove, i + 1) = 2
'        vp.EndTable
'
'    End With
'End Sub

'Private Sub Sub_Generar_Reporte_2()
'    Const clrHeader = &HD0D0D0
'    '------------------------------------------
'    Const sHeader_Caja = "Caja|Mercado|Moneda|Monto Moneda Caja|Monto Moneda Cuenta|"
'    Const sFormat_Caja = "3000|1000|1000|>2500|>2500|>4200"
'    Const sHeader_SA = "Nemot�cnico|Cantidad|Precio|P.P. Compra|Valor Mercado|Valor Mercado Cuenta|"
'    Const sFormat_SA = "2900|>1100|>1100|>2500|>2500|>2500|1600"
'    Const sHeader_Detalle_Caja = "Fecha Mov.|Fecha Liq.|Origen Mov.|Movimientos|Ingreso|Egreso|Saldo"
'    Const sFormat_Detalle_Caja = "1200|1100|1950|4550|>1800|>1800|>1800"
'
'    '------------------------------------------
'    Dim sRecord
'    Dim bAppend
'    Dim lLinea As Integer
'    '------------------------------------------
'    Dim lPatrimonio As Class_Patrimonio_Cuentas
'    Dim lReg_Pat As hFields
'    Dim lPatrim As String
'    Dim lRentabilidad As Double
'    '------------------------------------------
'    Dim lCajas_Ctas As Class_Cajas_Cuenta
'    Dim lCursor_Caj As hRecord
'    Dim lReg_Caj As hFields
'    Dim lMonto_Mon_Caja As String
'    '------------------------------------------
'    Dim lSaldos_Activos As Class_Saldo_Activos
'    Dim lCursor_SA As hRecord
'    Dim lReg_SA As hFields
'    '------------------------------------------
'    Dim lParidad As Double
'    Dim lOperacion As String
'    Dim lMonto_Mon_Cta As String
'    '------------------------------------------
''    Dim lcTipo_Cambio As Class_Tipo_Cambios
'    Dim lcTipo_Cambio As Object
'    '------------------------------------------
'    Dim lInstrumento As Class_Instrumentos
'    Dim lCursor_Ins As hRecord
'    Dim lReg_Ins As hFields
'    Dim lHay_Activos As Boolean
'    '------------------------------------------
'    Dim lSaldos_Caja As Class_Saldos_Caja
'
'    '--------------------------------------
'    Dim lReg As hCollection.hFields
''    Dim lcCuenta As Class_Cuentas
'    Dim lcCuenta As Object
'    Dim lcMov_Caja As Class_Mov_Caja
'    Dim lcSaldos_Caja As Class_Saldos_Caja
'    '--------------------------------------
'    Dim lFecDesde As Date
'    Dim lFecHasta As Date
'    Dim lId_Caja_Cuenta As String
'    Dim lTotal As Double
'    Dim lIngreso As Double
'    Dim lEgreso As Double
'    Dim lUltimaCaja As Long
'    Dim nDecimales As Integer
'
'    '------------------------------------------
'
'    Call Sub_Bloquea_Puntero(Me)
'
'    If Not Fnt_Form_Validar(Me.Controls) Then
'      GoTo ErrProcedure
'    End If
'
'    Rem Busca el patrimonio de la cuenta y su rentabilidad
'    Set lPatrimonio = New Class_Patrimonio_Cuentas
'    With lPatrimonio
'      .Campo("id_cuenta").Valor = lId_Cuenta
'      .Campo("FECHA_CIERRE").Valor = DTP_Fecha_Ter.Value
'      'Valores por defecto
'      .Campo("RENTABILIDAD_MON_CUENTA").Valor = Null
'      .Campo("APORTE_RETIROS_MON_CUENTA").Valor = Null
'      If .Buscar Then
'        For Each lReg_Pat In .Cursor
'          lPatrim = FormatNumber(lReg_Pat("PATRIMONIO_MON_CUENTA").Value, nDecimales)
'          lRentabilidad = NVL(lReg_Pat("RENTABILIDAD_MON_CUENTA").Value, 0)
'        Next
'      Else
'        MsgBox .ErrMsg, vbCritical, Me.Caption
'        GoTo ErrProcedure
'      End If
'    End With
'
'  Rem Busca las cajas de la cuenta
'  Set lCajas_Ctas = New Class_Cajas_Cuenta
'  With lCajas_Ctas
'    .Campo("id_cuenta").Valor = lId_Cuenta
'    If .Buscar(True) Then
'      Set lCursor_Caj = .Cursor
'    Else
'      MsgBox .ErrMsg, vbCritical, Me.Caption
'      GoTo ErrProcedure
'    End If
'  End With
'
'  Rem Busca los Instrumentos del sistema
'  Set lInstrumento = New Class_Instrumentos
'  With lInstrumento
'    If .Buscar Then
'      Set lCursor_Ins = .Cursor
'    Else
'      MsgBox .ErrMsg, vbCritical, Me.Caption
'      GoTo ErrProcedure
'    End If
'  End With
'
'  Rem Comienzo de la generaci�n del reporte
'
'  With vp
'    Rem Datos de los activos de la cuenta
'    .Paragraph = "" 'salto de linea
'    .FontSize = glb_tamletra_registros
'    .FontBold = True
'    .Paragraph = "Datos de Activos de la Cuenta"
'    .FontBold = False
'    .FontSize = glb_tamletra_registros
'    Rem Busca los Activos en cartera de la cuenta
'    Rem Por cada instrumento del sistema
'    lHay_Activos = False
'    For Each lReg_Ins In lCursor_Ins
'      Rem Se buscan los activos en cartera para cada instrumento
'      Set lSaldos_Activos = New Class_Saldo_Activos
'      With lSaldos_Activos
'        If .Buscar_saldos_para_cartola(DTP_Fecha_Ter.Value, pId_Cuenta:=lId_Cuenta, pCod_Instrumento:=lReg_Ins("cod_instrumento").Value) Then
'          Set lCursor_SA = .Cursor
'        Else
'          MsgBox .ErrMsg, vbCritical, Me.Caption
'          GoTo ErrProcedure
'        End If
'      End With
'
'      Rem Si hay activos para el instrumento consultado procede a la generacion de la tabla
'      If lCursor_SA.Count > 0 Then
'        lHay_Activos = True
'        .FontSize = glb_tamletra_registros
'        .FontBold = True
'        .Paragraph = lReg_Ins("Dsc_Intrumento").Value
'        .FontSize = glb_tamletra_registros
'        .FontBold = False
'        .StartTable
'        .TableCell(tcAlignCurrency) = False
'        Rem Se agrupan los activos por instrumentos
'        For Each lReg_SA In lCursor_SA
'          If Not IsNull(lReg_SA("Fecha_Cierre")) Then
'            sRecord = lReg_SA("nemotecnico").Value & "|" & _
'                        Format(lReg_SA("cantidad").Value, "#,##0.###0") & "|" & _
'                        Format(NVL(lReg_SA("precio").Value, 0), "#,##0.###0") & "|" & _
'                        Format(NVL(lReg_SA("precio_promedio_compra").Value, 0), "#,##0.###0") & "|" & _
'                        Format(lReg_SA("monto_mon_nemotecnico").Value, Fnt_Formato_Moneda(lReg_SA("id_moneda_nemotecnico"))) & "|" & _
'                        Format(lReg_SA("monto_mon_cta").Value, lFormatoMonedaCta) & "|"
'            .AddTable sFormat_SA, sHeader_SA, sRecord, clrHeader, , bAppend
'           End If
'        Next
'        '.TableCell(tcBackColor, 1, 1, 1, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'        .TableCell(tcBackColor, 0, 1, 0, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'        .TableCell(tcFontBold, 0) = True
'        .EndTable
'        .Paragraph = ""
'      End If
'    Next
'
'    If Not lHay_Activos Then
'      .Paragraph = "No hay activos para la cuenta"
'    End If
'
'        Rem Datos de la Caja
'    .Paragraph = "" 'salto de linea
'    .FontSize = glb_tamletra_registros
'    .FontBold = True
'    .Paragraph = "Datos de la(s) Caja(s) de la Cuenta"
'    .FontBold = False
'    .FontSize = glb_tamletra_registros
'    .StartTable
'    .TableCell(tcAlignCurrency) = False
'    For Each lReg_Caj In lCursor_Caj
'      lMonto_Mon_Caja = 0
'
'      Set lSaldos_Caja = New Class_Saldos_Caja
'      With lSaldos_Caja
'        .Campo("fecha_cierre").Valor = DTP_Fecha_Ter.Value
'        .Campo("id_caja_cuenta").Valor = lReg_Caj("id_caja_cuenta").Value
'        If .Buscar Then
'          If .Cursor.Count > 0 Then
'            lMonto_Mon_Caja = (.Cursor(1)("monto_mon_caja").Value + .Cursor(1)("MONTO_X_COBRAR_MON_CTA").Value) - .Cursor(1)("MONTO_X_PAGAR_MON_CTA").Value
'          End If
'        End If
'      End With
'      'Set lSaldos_Caja = Nothing
'
'      'lCajas_Ctas.campo("id_caja_cuenta").Valor = lReg_Caj("id_caja_cuenta").Value
'      'lMonto_Mon_Caja = Format(CStr(lCajas_Ctas.Saldo_Disponible(DTP_Fecha.Value)), "###,##0.0000")
'
''      Set lcTipo_Cambio = New Class_Tipo_Cambios
'      Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
'      If lcTipo_Cambio.Busca_Precio_Paridad(lId_Cuenta _
'                                       , lReg_Caj("Id_Moneda").Value _
'                                       , lId_Moneda_Cta _
'                                       , DTP_Fecha_Ter.Value _
'                                       , lParidad _
'                                       , lOperacion) Then
'        If lOperacion = "M" Then
'          Rem Multiplicacion
'          lMonto_Mon_Cta = lMonto_Mon_Caja * lParidad
'        Else
'          Rem Division
'          lMonto_Mon_Cta = Fnt_Divide(lMonto_Mon_Caja, lParidad)
'        End If
'      Else
'        MsgBox lcTipo_Cambio.ErrMsg, vbCritical, Me.Caption
'      End If
'      sRecord = lReg_Caj("Dsc_Caja_Cuenta").Value & "|" & _
'                lReg_Caj("Desc_Mercado").Value & "|" & _
'                lReg_Caj("Dsc_Moneda").Value & "|" & _
'                Format(lMonto_Mon_Caja, Fnt_Formato_Moneda(lReg_Caj("Id_Moneda").Value)) & "|" & _
'                Format(lMonto_Mon_Cta, lFormatoMonedaCta) & "|"
'      .AddTable sFormat_Caja, sHeader_Caja, sRecord, clrHeader, , bAppend
'    Next
'    .TableCell(tcBackColor, 0, 1, 0, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'    .TableCell(tcFontBold, 0) = True
'    .EndTable
'
'Rem --------------------
'    .Paragraph = "" 'salto de linea
'    .FontSize = glb_tamletra_registros
'    .FontBold = True
'    .Paragraph = "Detalle de Cajas"
'    .FontBold = False
'    .FontSize = glb_tamletra_registros
'    .StartTable
'    .TableCell(tcAlignCurrency) = False
'      Call Sub_Bloquea_Puntero(Me)
'
'      If Not Fnt_Form_Validar(Me) Then
'        'MsgBox "Debe seleccionar una Cuenta para buscar Movimientos de Caja.", vbInformation, Me.Caption
'        GoTo ErrProcedure
'      End If
'
'      lFecDesde = CDate(DameInicioDeMes(DTP_Fecha_Ter.Value))
'
'      lFecHasta = DTP_Fecha_Ter.Value
'
'      If lFecDesde > lFecHasta Then
'        MsgBox "La Fecha Desde debe ser menor que Fecha Hasta.", vbExclamation, Me.Caption
'        GoTo ErrProcedure
'      End If
'
'      lUltimaCaja = 0
'      lIngreso = 0
'      lEgreso = 0
'      lTotal = 0
'      lId_Caja_Cuenta = ""
'      Set lcMov_Caja = New Class_Mov_Caja
'      With lcMov_Caja
'        If Not lId_Caja_Cuenta = "" Then
'          .Campo("Id_Caja_Cuenta").Valor = lId_Caja_Cuenta
'        End If
'        If .Buscar_Movimientos_Cuenta(lId_Cuenta, lFecDesde, lFecHasta) Then
'          For Each lReg In .Cursor
'            If lUltimaCaja = lReg("Id_Caja_Cuenta").Value Then
'              If lReg("FLG_Tipo_Movimiento").Value = gcTipoOperacion_Abono Then
'                lTotal = lTotal + lReg("Monto").Value
'                lIngreso = lIngreso + lReg("Monto").Value
'                sRecord = lReg("Fecha_Movimiento").Value & "|" & _
'                        lReg("Fecha_Liquidacion").Value & "|" & _
'                        lReg("dsc_Origen_Mov_Caja").Value & "|" & _
'                        lReg("Dsc_Mov_Caja").Value & "|" & _
'                        FormatNumber(lReg("Monto").Value, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                        FormatNumber(0, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                        FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
'              Else
'                lTotal = lTotal - lReg("Monto").Value
'                lEgreso = lEgreso + lReg("Monto").Value
'                sRecord = lReg("Fecha_Movimiento").Value & "|" & _
'                        lReg("Fecha_Liquidacion").Value & "|" & _
'                        lReg("dsc_Origen_Mov_Caja").Value & "|" & _
'                        lReg("Dsc_Mov_Caja").Value & "|" & _
'                        FormatNumber(0, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                        FormatNumber(lReg("Monto").Value, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                        FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
'              End If
'              vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
'            Else
'              If Not lUltimaCaja = 0 Then
'                 sRecord = "|" & _
'                         "|" & _
'                         "|" & _
'                         "SALDO CAJA" & "|" & _
'                         FormatNumber(lIngreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                         FormatNumber(lEgreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                         FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
'                vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
'                vp.TableCell(tcFontBold, vp.TableCell(tcRows), 4, vp.TableCell(tcRows), 4) = True
'                lIngreso = 0
'                lEgreso = 0
'                lTotal = 0
'              End If
'              sRecord = "|" & _
'                        "|" & _
'                        "|" & _
'                        UCase(lReg("DSC_CAJA_CUENTA").Value) & "|" & _
'                        "|" & _
'                        "|"
'              vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
'              'vp.TableCell(tcBackColor, 1, 1, 1, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'              vp.TableCell(tcBackColor, vp.TableCell(tcRows), 1, vp.TableCell(tcRows), 7) = RGB(232, 226, 222)
'              vp.TableCell(tcFontBold, vp.TableCell(tcRows), 4, vp.TableCell(tcRows), 4) = True
'              vp.TableCell(tcRowBorderBelow, vp.TableCell(tcRows)) = 2
'              vp.TableCell(tcRowBorderAbove, vp.TableCell(tcRows)) = 2
'
'              lUltimaCaja = lReg("Id_Caja_Cuenta").Value
'              'Buscar Saldo Anterior por Caja
'              Set lcSaldos_Caja = New Class_Saldos_Caja
'              With lcSaldos_Caja
'                .Campo("id_Caja_Cuenta").Valor = lUltimaCaja
'                .Campo("Fecha_cierre").Valor = lFecDesde
'                If .Buscar_Saldos_Cuenta(lTotal, lId_Cuenta) Then
'                    sRecord = lFecDesde - 1 & "|" & _
'                              "|" & _
'                              "SALDO " & "|" & _
'                              "Saldo Anterior" & "|" & _
'                              FormatNumber(lIngreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                              FormatNumber(lEgreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                              FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
'                    vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
'                    vp.TableCell(tcFontBold, vp.TableCell(tcRows), 4, vp.TableCell(tcRows), 4) = True
'                Else
'                  Call Fnt_MsgError(.SubTipo_LOG, _
'                                    "Problemas en cargar Valores Cuota.", _
'                                    .ErrMsg, _
'                                    pConLog:=True)
'                  lTotal = 0
'                End If
'              End With
'              Set lcSaldos_Caja = Nothing
'              If lReg("FLG_Tipo_Movimiento").Value = gcTipoOperacion_Abono Then
'                'Call SetCell(Grilla, lLinea, "Ingresos", lReg("Monto").Value)
'                lTotal = lTotal + lReg("Monto").Value
'                lIngreso = lIngreso + lReg("Monto").Value
'                sRecord = lReg("Fecha_Movimiento").Value & "|" & _
'                        lReg("Fecha_Liquidacion").Value & "|" & _
'                        lReg("dsc_Origen_Mov_Caja").Value & "|" & _
'                        lReg("Dsc_Mov_Caja").Value & "|" & _
'                        FormatNumber(lReg("Monto").Value, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                        FormatNumber(0, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                        FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
'              Else
'                'Call SetCell(Grilla, lLinea, "Egresos", lReg("Monto").Value)
'                lTotal = lTotal - lReg("Monto").Value
'                lEgreso = lEgreso + lReg("Monto").Value
'                sRecord = lReg("Fecha_Movimiento").Value & "|" & _
'                        lReg("Fecha_Liquidacion").Value & "|" & _
'                        lReg("dsc_Origen_Mov_Caja").Value & "|" & _
'                        lReg("Dsc_Mov_Caja").Value & "|" & _
'                        FormatNumber(0, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                        FormatNumber(lReg("Monto").Value, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                        FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
'              End If
'              vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
'            End If
'          Next
'          sRecord = "|" & _
'                  "|" & _
'                  "|" & _
'                  "SALDO CAJA" & "|" & _
'                  FormatNumber(lIngreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                  FormatNumber(lEgreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
'                  FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
'         vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
'         vp.TableCell(tcBackColor, 0, 1, 0, 7) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'         vp.TableCell(tcFontBold, 0) = True
'         vp.EndTable
'        Else
'          Call Fnt_MsgError(.SubTipo_LOG, _
'                            "Problemas en cargar Valores Cuota.", _
'                            .ErrMsg, _
'                            pConLog:=True)
'        End If
'      End With
'      Set lcMov_Caja = Nothing
'Rem --------------------
'    '.EndDoc
'  End With
'ErrProcedure:
'  Call Sub_Desbloquea_Puntero(Me)
'
'End Sub

'Sub DetalleInstrumentos()
'    Dim lCursor             As hRecord
'    Dim lReg                As hFields
'
'    Dim prod_anterior       As String
'    Dim Fecha_Mes_anterior  As String
'    Dim Mostrado            As Integer
'    Dim iFila               As Integer
'
'    Dim nTotal_Actual       As Double
'    Dim nTotal_Anterior     As Double
'
'    Dim oColumnas As New Class_Cartola_Campos
'
'    Fecha_Mes_anterior = UltimoDiaDelMes(oCliente.Fecha_Cartola)
'
'    gDB.Parametros.Clear
'    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
'    gDB.Parametros.Add "PFecha_Cartola", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
'    gDB.Parametros.Add "PFecha_Anterior", ePT_Fecha, CDate(Fecha_Mes_anterior), ePD_Entrada
'    gDB.Procedimiento = "PKG_CARTOLA$DAME_ACTIVOS_CLASE"
'
'    If Not gDB.EjecutaSP Then
'        Exit Sub
'    End If
'
'    Set lCursor = gDB.Parametros("Pcursor").Valor
'
'    gDB.Parametros.Clear
'
'    Mostrado = 0
'    Dim Id_clase        As Double
'    Dim id_SubClase     As Double
'    'Dim Id_Clase_Anterior As Double
'    Dim sDescClase      As String
'    Dim sDescSubClase   As String
'
'    iLineasImpresas = 1
'
'    For Each lReg In lCursor
'
'        Id_clase = lReg("ID_CLASE").Value
'        id_SubClase = NVL(lReg("ID_SUBCLASE").Value, 0)
'        sDescClase = lReg("DSC_CLASE").Value
'        sDescSubClase = NVL(lReg("DSC_SUBCLASE").Value, "")
'
'        If Mostrado = 0 Or _
'            (prod_anterior <> lReg("DSC_CLASE").Value) And _
'            (lReg("monto_actual").Value <> 0) Then
'
'            nTotal_Actual = 0
'            nTotal_Anterior = 0
'
'            SumaTotalClase lCursor, lReg("DSC_CLASE").Value, nTotal_Actual, nTotal_Anterior
'
'            With vp
'                .CurrentY = .CurrentY + rTwips("2mm")
'                FilaAux = vp.CurrentY
'
'                .StartTable
'                    .TableBorder = tbBox
'
'                    .TableCell(tcCols) = 1
'                    .TableCell(tcRows) = .TableCell(tcRows) + 1
'
'                    .TableCell(tcFontSize, 1, 1, 1, 1) = glb_tamletra_titulo
'                    .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'                    .TableCell(tcForeColor, 1) = RGB(255, 255, 255)
'
'                    .TableCell(tcText, 1, 1) = UCase("TIPO DE INSTRUMENTO : " & lReg("DSC_CLASE").Value)
'                    .TableCell(tcColWidth, 1, 1) = CONST_ANCHO_GRILLA
'
'                .EndTable
'
'            End With
'
'            iLineasImpresas = iLineasImpresas + 1
'            Mostrado = 1
'            prod_anterior = lReg("DSC_CLASE").Value
'
'        End If
'
'        Dim lCursorSubClase As hRecord
'        Dim lRegSubClase As hFields
'
'        gDB.Parametros.Clear
'        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'        gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
'        gDB.Parametros.Add "Pfecha_Cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
'        gDB.Parametros.Add "pSubClase", ePT_Numero, id_SubClase, ePD_Entrada
'        gDB.Procedimiento = "PKG_CARTOLA$DAME_ACTIVOS_SUBCLASE"
'
'        If Not gDB.EjecutaSP Then Exit Sub
'
'        Set lCursorSubClase = gDB.Parametros("Pcursor").Valor
'
'        With vp
'            .CurrentY = .CurrentY + rTwips("1mm")
'            FilaAux = vp.CurrentY
'
'            Dim oCol() As TCampos
'            Dim i As Integer
'            Dim bMostrado As Boolean
'            Dim bImprimio As Boolean
'
'            bMostrado = False
'            bImprimio = False
'
'            If lCursorSubClase.Count <> 0 Then
'                .StartTable
'                    .TableBorder = tbBox
'                    .TableCell(tcCols) = 1
'                    .TableCell(tcRows) = .TableCell(tcRows) + 1
'                    iFila = .TableCell(tcRows)
'                    .TableCell(tcFontSize, iFila, 1) = glb_tamletra_registros
'
'                    .TableCell(tcText, iFila, 1) = UCase(NVL(lReg("DSC_SUBCLASE").Value, ""))
'                    '.TableCell(tcText, iFila, 1) = NVL(lReg("Porcentaje_inversion").Value, "")
'                    .TableCell(tcColWidth, iFila, 1) = CONST_ANCHO_GRILLA
'                .EndTable
'                iLineasImpresas = iLineasImpresas + 1
'            End If
'
'            oColumnas.Header_BackColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'            oColumnas.Header_ForeColor = RGB(255, 255, 255)
'            oColumnas.Header_FontSize = glb_tamletra_subtitulo1 'glb_tamletra_titulo
'            oColumnas.Header_Font = Font_Name
'
'            .StartTable
'            .TableBorder = tbBoxRows
'
'            For Each lRegSubClase In lCursorSubClase
'                bImprimio = True
'
'                If Not bMostrado Then
'                    oColumnas.CrearTabla = False
'                    AsignaTitulos oColumnas, lRegSubClase("cod_producto").Value
'
'                    oColumnas.PintaEncabezado vp
'                    oCol = oColumnas.GetLineas
'                    bMostrado = True
'
'                    .TableCell(tcRows) = .TableCell(tcRows) + 1
'                    iFila = .TableCell(tcRows)
'
'                    iLineasImpresas = 2
'
'                End If
'
'                Dim sValor
'                Dim nValor As Double
'
'                For i = 1 To UBound(oCol)
'                    Select Case LCase(oCol(i).Nombre)
'                        Case "porcentaje_inversion"
'                            sValor = FormatPorcentaje(lRegSubClase("Valorizacion_Mercado").Value, nTotal_Actual, nValor)
'                        Case "valoractual"
'                            nValor = lRegSubClase("precio_promedio_compra").Value * lRegSubClase("nominal").Value
'
'                            If oCol(i).TieneFormato Then
'                                sValor = FormatNumber(nValor, oCol(i).Formato_Decimales)
'                            End If
'
'                        Case Else
'                            sValor = NVL(lRegSubClase(oCol(i).Nombre).Value, "")
'
'                            If oCol(i).TieneFormato Then sValor = FormatNumber(sValor, oCol(i).Formato_Decimales)
'
'                            If IsNumeric(sValor) Then nValor = sValor
'                    End Select
'
'                    .TableCell(tcFontSize, iFila, i) = glb_tamletra_registros
'                    .TableCell(tcText, iFila, i) = sValor
'                    .TableCell(tcColAlign, iFila, i) = oCol(i).Alineacion
'
'                    If oCol(i).Totaliza Then
'                        oCol(i).Total = oCol(i).Total + nValor ' CDbl(lRegSubClase(oCol(i).Nombre).Value)
'                    End If
'
'                Next
'
'                .TableCell(tcRows) = .TableCell(tcRows) + 1
'                iFila = .TableCell(tcRows)
'                iLineasImpresas = iLineasImpresas + 1
'
'                If (iLineasImpresas Mod CONS_NUMERO_LINEAS) = 0 Then
'                    bMostrado = False
'                End If
'            Next
'
'            .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 8) = glb_tamletra_registros
'
''            If Not bImprimio Then
''                .TableCell(tcRows) = .TableCell(tcRows) + 1
''                iFila = .TableCell(tcRows)
''                .TableCell(tcCols) = 1
''                .TableCell(tcText, iFila - 1, 1) = "NO REGISTRA POSICIONES"
''                '.TableCell(tcText, iFila, 1) = "NO REGISTRA POSICIONES"
''                vp.TableCell(tcFontSize, iFila, 1) = 12
''                .TableCell(tcColAlign, iFila, 1) = taCenterMiddle
''                .TableCell(tcColWidth, iFila, 1) = CONST_ANCHO_GRILLA
''
''                ' Pone Fila de Totales
''            End If
'
'            oColumnas.PintaTotales vp, oCol
'
'           .EndTable
'
'        End With
'
'        Set lCursorSubClase = Nothing
'        Set lRegSubClase = Nothing
'
'    Next    '  Eof CLASE - SUBCLASE
'
'End Sub


'Private Sub Sub_Generar_Reporte()
'    Dim gDb As New dll_db.Class_Conect
'    Dim a As Integer
'    Dim suma As Double
'    Dim patrimonio As Double
'    Dim monto_x_cobrar As Double
'
'    ' Set gDb = Server.CreateObject("dll_db.class_conect")
'    gDb.Conectar
'
'    gDb.Parametros.Clear
'    gDb.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'    gDb.Parametros.Add "Pid_cuenta", ePT_Numero, Session("idcuenta"), ePD_Entrada
'    gDb.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Session("fecha_consulta")), ePD_Entrada
'    gDb.Procedimiento = "PKG_PATRIMONIO_CUENTAS$Buscar"
'
'    If Not gDb.EjecutaSP Then
'        Exit Sub
'    End If
'
'    Set lCursor = gDb.Parametros("Pcursor").Valor
'
'    gDb.Parametros.Clear
'    a = 1
'    suma = 0
'
'    For Each lReg In lCursor
'        patrimonio = CDbl(lReg("patrimonio_mon_cuenta").Value)
'        monto_x_cobrar = CDbl(lReg("monto_x_cobrar_mon_cta").Value)
'        monto_x_pagar = CDbl(lReg("monto_x_pagar_mon_cta").Value)
'    Next
'
'
'
'Dim glb_cuenta As Long
'Dim glb_fecha_hoy As String
'Dim glb_fecha_inimes As String
'
''------------------------------------------
'Dim lCuenta As Class_Cuentas
'Dim lReg_Cta As hFields
'Dim lcMonedas As Class_Monedas
''------------------------------------------
'
'Rem Comienzo de la generaci�n del reporte
'
'lNum_Cuenta = ""
'lDsc_Cuenta = ""
'lNom_Cliente = ""
'lDsc_Moneda = ""
'lId_Moneda_Cta = ""
'nDecimales = 0
'
'lId_Cuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Cuentas))
'
'Rem Busca los datos de la cuenta
'
'Set lCuenta = New Class_Cuentas
'With lCuenta
'  .Campo("id_cuenta").Valor = lId_Cuenta
'  If .Buscar Then
'    For Each lReg_Cta In .Cursor
'      lNum_Cuenta = lReg_Cta("Num_Cuenta").Value
'      lDsc_Cuenta = lReg_Cta("Dsc_Cuenta").Value
'      lNom_Cliente = lReg_Cta("nombre_cliente").Value
'      lId_Moneda_Cta = lReg_Cta("id_moneda").Value
'    Next
'  Else
'    MsgBox .ErrMsg, vbCritical, Me.Caption
'    GoTo ErrProcedure
'  End If
'
'End With
'
'Rem Busca el nombre de la moneda
'Set lcMonedas = New Class_Monedas
'With lcMonedas
'  .Campo("id_Moneda").Valor = lId_Moneda_Cta
'  lFormatoMonedaCta = Fnt_Formato_Moneda(lId_Moneda_Cta)
'  If .Buscar Then
'    lDsc_Moneda = .Cursor(1)("dsc_moneda").Value
'    nDecimales = .Cursor(1)("dicimales_mostrar").Value
'  Else
'    MsgBox .ErrMsg, vbCritical, Me.Caption
'    Set lcMonedas = Nothing
'    GoTo ErrProcedure
'  End If
'End With
'Set lcMonedas = Nothing
'
'GeneraCartola lId_Cuenta ', lNum_Cuenta, lDsc_Cuenta, lDsc_Moneda, nDecimales
'ErrProcedure:
'  Call Sub_Desbloquea_Puntero(Me)
'End Sub

Private Sub MuestraDatosCuenta()
Dim lcCuentas As Object
Dim lId_Moneda As Integer
    Set lcCuentas = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuentas
        .Campo("id_Cuenta").Valor = lId_Cuenta
        If .Buscar Then
            lId_Moneda = .Cursor(1)("id_moneda").Value
            Call Sub_ComboSelectedItem(Cmb_Moneda, lId_Moneda)
            Txt_Nombres.Text = NVL(.Cursor(1)("razon_social").Value, .Cursor(1)("nombre_cliente").Value)
            Txt_Asesor.Text = NVL(.Cursor(1)("dsc_asesor").Value, "")
        End If
    End With
    
End Sub

Private Function RescataAsesor(ByVal lId_Cuenta As Integer) As String
Dim lcCuentas As Object
Dim lDscAsesor As String

    Set lcCuentas = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuentas
        .Campo("id_Cuenta").Valor = lId_Cuenta
        If .Buscar Then
            lDscAsesor = NVL(.Cursor(1)("dsc_asesor").Value, "")
        End If
    End With
    RescataAsesor = lDscAsesor

End Function
