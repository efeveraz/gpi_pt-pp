VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Frm_Sucursales_Ejecutivos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Carga de Sucursales y Ejecutivos"
   ClientHeight    =   5250
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8700
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5250
   ScaleWidth      =   8700
   Begin VB.Frame Frame1 
      Height          =   1335
      Left            =   60
      TabIndex        =   4
      Top             =   420
      Width           =   8565
      Begin VB.Frame Frame3 
         Caption         =   "Tipo de Archivo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   615
         Left            =   120
         TabIndex        =   7
         Top             =   150
         Width           =   2655
         Begin VB.OptionButton Opt_Ejecutivo 
            Caption         =   "Ejecutivos"
            Height          =   285
            Left            =   1470
            TabIndex        =   9
            Top             =   240
            Width           =   1095
         End
         Begin VB.OptionButton Opt_Sucursales 
            Caption         =   "Sucursales"
            Height          =   225
            Left            =   210
            TabIndex        =   8
            Top             =   270
            Value           =   -1  'True
            Width           =   1095
         End
      End
      Begin hControl2.hTextLabel Txt_Archivo 
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Tag             =   "OBLI"
         Top             =   870
         Width           =   7065
         _ExtentX        =   12462
         _ExtentY        =   556
         LabelWidth      =   1600
         Caption         =   "Archivo"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin MSComctlLib.Toolbar Toolbar_Archivo 
         Height          =   330
         Left            =   7320
         TabIndex        =   6
         Top             =   870
         Width           =   1110
         _ExtentX        =   1958
         _ExtentY        =   582
         ButtonWidth     =   1746
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Buscar"
               Key             =   "BUSCAR"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
            EndProperty
         EndProperty
      End
      Begin MSComDlg.CommonDialog Cmd_AbreArchivo 
         Left            =   3480
         Top             =   270
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Visor de Sucesos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3405
      Left            =   60
      TabIndex        =   2
      Top             =   1770
      Width           =   8565
      Begin MSComctlLib.Toolbar Toolbar_Detalle 
         Height          =   330
         Left            =   7320
         TabIndex        =   3
         Top             =   480
         Width           =   1110
         _ExtentX        =   1958
         _ExtentY        =   582
         ButtonWidth     =   1588
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Detalle"
               Key             =   "DETALLE"
               Object.ToolTipText     =   "Muestra el detalle de los sucesos del Cierre"
            EndProperty
         EndProperty
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3015
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   7065
         _cx             =   12462
         _cy             =   5318
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Sucursales_Ejecutivos.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8700
      _ExtentX        =   15346
      _ExtentY        =   635
      ButtonWidth     =   1588
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Cargar"
            Key             =   "LOAD"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Carga Sucursales o Ejecutivos"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Sucursales_Ejecutivos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Enum eColumnas_Sucursal
  eGD_Codigo_Sucursal = 1
  eGD_Desc_Sucursal
End Enum

Private Enum eColumnas_Ejecutivo
  eGD_Cod_Sucursal = 1
  eGD_Cod_Ejecutivo
  eGD_Dsc_Ejecutivo
End Enum

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("LOAD").Image = "document"
      .Buttons("EXIT").Image = cBoton_Salir
  End With

  With Toolbar_Archivo
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("BUSCAR").Image = "boton_grilla_buscar"
  End With
  
  With Toolbar_Detalle
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("DETALLE").Image = cBoton_Agregar_Grilla
  End With

  Call Sub_CargaForm
  Call Sub_CargarDatos

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
Dim lMensaje As String

  With Grilla
    If .Row > 0 Then
      lMensaje = GetCell(Grilla, .Row, "TEXTO")
      If Not lMensaje = "" Then
        MsgBox lMensaje, vbInformation, Me.Caption
      End If
    End If
  End With
  
End Sub

Private Sub Opt_Ejecutivo_Click()
  Call Sub_Nombre_Texto
End Sub

Private Sub Opt_Sucursales_Click()
  Call Sub_Nombre_Texto
End Sub

Private Sub Sub_Nombre_Texto()
  If Opt_Sucursales.Value Then
    Txt_Archivo.Caption = "Archivo " & Opt_Sucursales.Caption
  ElseIf Opt_Ejecutivo.Value Then
    Txt_Archivo.Caption = "Archivo " & Opt_Ejecutivo.Caption
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "LOAD"
      Call Sub_Carga_Archivo
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Archivo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  If Button.Key = "BUSCAR" Then
    Call Sub_Buscar_Archivo
  End If
End Sub

Private Sub Toolbar_Detalle_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  If Button.Key = "DETALLE" Then
    Call Grilla_DblClick
  End If
End Sub

Private Sub Sub_CargaForm()


  Call Sub_FormControl_Color(Me.Controls)

  Grilla.Rows = 1
  
End Sub

Private Sub Sub_CargarDatos()
  Call Sub_Nombre_Texto
  Txt_Archivo.Text = ""
End Sub

Private Sub Sub_Buscar_Archivo()

On Error GoTo Cmd_BuscarArchivo_Err

  With Cmd_AbreArchivo
    .CancelError = True
    .InitDir = "C:\Mis documentos\"
    .Filter = "Txt (*.txt)|*.txt"
    .Flags = cdlOFNFileMustExist
    .ShowOpen
    Rem se guarda el path completo, que incluye el nombre de archivo
    Txt_Archivo.Text = .FileName
  End With
  
  Exit Sub

Cmd_BuscarArchivo_Err:
  If Err = 32755 Then 'usuario presiona el boton Cancelar del Command Dialog
    Exit Sub
  End If
  
End Sub

Private Sub Sub_Carga_Archivo()
Dim lNombreTipo As String
  
  If Not Txt_Archivo.Text = "" Then
    If Opt_Sucursales.Value Then
      Call Sub_Carga_Sucursales
    ElseIf Opt_Ejecutivo.Value Then
      Call Sub_Carga_Ejecutivos
    End If
  Else
    lNombreTipo = IIf(Opt_Sucursales.Value, Opt_Sucursales.Caption, Opt_Ejecutivo.Caption)
    MsgBox "Debe ingresar el nombre del archivo para realizar la carga de " & lNombreTipo & ".", vbCritical, Me.Caption
  End If
  
End Sub

Private Sub Sub_Carga_Sucursales()
Dim fCls_File As Object
Dim lLinea As Double
Dim lMsg_Error As String
Dim lCod_Sucursal As String
Dim lDsc_Sucursal As String
  
  Call Sub_Bloquea_Puntero(Me)
    
  Grilla.Rows = 1
    
  Set fCls_File = CreateObject("DLLCS_ArchivoPlano.Class_ArchivoPlano")
  
  With fCls_File
    .Path = Mid(Txt_Archivo.Text, 1, InStr(1, Txt_Archivo.Text, "\"))
    .Name = Mid(Txt_Archivo.Text, InStr(1, Txt_Archivo.Text, "\") + 1)
    .fSeparador = ";"
    Call .OpenFile
    
    lLinea = 0
    .MoveFirst
    
    Call Fnt_Escribe_Grilla(Grilla, "N", Fnt_FechaServidor & " -- Inicio Carga de Sucursales.")
    
    Do While Not .EofFile
      lLinea = lLinea + 1
      If .Fields.Count > 0 Then
        If .Fields.Count = eColumnas_Sucursal.eGD_Desc_Sucursal Then
          lCod_Sucursal = Trim(.Fields(eColumnas_Sucursal.eGD_Codigo_Sucursal).Value)
          lDsc_Sucursal = Trim(.Fields(eColumnas_Sucursal.eGD_Desc_Sucursal).Value)
          
          If Fnt_Ingresa_Sucursales(lCod_Sucursal, lDsc_Sucursal, lMsg_Error) Then
            Call Fnt_Escribe_Grilla(Grilla, "N", "Sucursal Ingresada correctamente. L�nea: " & lLinea & " -- C�digo: '" & lCod_Sucursal & "' -- Descripci�n: '" & lDsc_Sucursal & "'.")
          Else
            Call Fnt_Escribe_Grilla(Grilla, "E", "Error en la l�nea: " & lLinea & " -- " & lMsg_Error)
          End If
        Else
          lMsg_Error = "N�mero de campos debe ser igual a " & eColumnas_Sucursal.eGD_Desc_Sucursal & "."
          Call Fnt_Escribe_Grilla(Grilla, "E", "Error en la l�nea: " & lLinea & " -- " & lMsg_Error)
        End If
      Else
        Call Fnt_Escribe_Grilla(Grilla, "E", "Error de formato en la l�nea: " & lLinea & " -- L�nea sin campos.")
      End If
      .MoveNext
    Loop
    .CloseFile
    
    Call Fnt_Escribe_Grilla(Grilla, "N", Fnt_FechaServidor & " -- Fin Carga de Sucursales.")
    
    MsgBox "Carga de Sucursales Finalizada.", vbInformation, Me.Caption
  End With
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Fnt_Ingresa_Sucursales(pCod_Sucursal As String, pDsc_Sucursal As String, ByRef pMsg_Error As String) As Boolean
Dim lcSucursal As Class_Sucursales
  
  Fnt_Ingresa_Sucursales = True
  
  If Not Fnt_Valida_Sucursales(pCod_Sucursal, pDsc_Sucursal, pMsg_Error) Then
    GoTo ErrProcedure
  End If
  
  Set lcSucursal = New Class_Sucursales
  With lcSucursal
    .Campo("Cod_Sucursal").Valor = pCod_Sucursal
    .Campo("Dsc_Sucursal").Valor = pDsc_Sucursal
    If Not .Guardar Then
      pMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcSucursal = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Ingresa_Sucursales = False
  Set lcSucursal = Nothing
  
End Function

Private Function Fnt_Valida_Sucursales(pCod_Sucursal As String, pDsc_Sucursal As String, ByRef pMsg_Error As String) As Boolean
  Fnt_Valida_Sucursales = True
  
  If pCod_Sucursal = "" Then
    pMsg_Error = "C�digo de Sucursal no debe ser vac�o."
    GoTo ErrProcedure
  ElseIf Len(pCod_Sucursal) > 10 Then
    pMsg_Error = "C�digo de Sucursal '" & pCod_Sucursal & "' debe tener menos de 10 caracteres."
    GoTo ErrProcedure
  ElseIf pDsc_Sucursal = "" Then
    pMsg_Error = "Descripci�n de Sucursal no debe ser vac�a."
    GoTo ErrProcedure
  ElseIf Len(pDsc_Sucursal) > 100 Then
    pMsg_Error = "Descripci�n de Sucursal '" & pDsc_Sucursal & "' debe tener menos de 100 caracteres."
    GoTo ErrProcedure
  End If
  
  Exit Function
  
ErrProcedure:
  Fnt_Valida_Sucursales = False
  
End Function

Private Sub Sub_Carga_Ejecutivos()
Dim fCls_File As Object
Dim lLinea As Double
Dim lMsg_Error As String
Dim lCod_Sucursal As String
Dim lCod_Ejecutivo As String
Dim lDsc_Ejecutivo As String
  
  Call Sub_Bloquea_Puntero(Me)
    
  Grilla.Rows = 1
    
  Set fCls_File = CreateObject("DLLCS_ArchivoPlano.Class_ArchivoPlano")
  
  With fCls_File
    .Path = Mid(Txt_Archivo.Text, 1, InStr(1, Txt_Archivo.Text, "\"))
    .Name = Mid(Txt_Archivo.Text, InStr(1, Txt_Archivo.Text, "\") + 1)
    .fSeparador = ";"
    Call .OpenFile
    
    lLinea = 0
    .MoveFirst
    
    Call Fnt_Escribe_Grilla(Grilla, "N", Fnt_FechaServidor & " -- Inicio Carga de Ejecutivos.")
    
    Do While Not .EofFile
      lLinea = lLinea + 1
      If .Fields.Count > 0 Then
        If .Fields.Count = eColumnas_Ejecutivo.eGD_Dsc_Ejecutivo Then
          lCod_Sucursal = Trim(.Fields(eColumnas_Ejecutivo.eGD_Cod_Sucursal).Value)
          lCod_Ejecutivo = Trim(.Fields(eColumnas_Ejecutivo.eGD_Cod_Ejecutivo).Value)
          lDsc_Ejecutivo = Trim(.Fields(eColumnas_Ejecutivo.eGD_Dsc_Ejecutivo).Value)
          
          If Fnt_Ingresa_Ejecutivos(lCod_Sucursal, lCod_Ejecutivo, lDsc_Ejecutivo, lMsg_Error) Then
            Call Fnt_Escribe_Grilla(Grilla, "N", "Ejecutivo Ingresado correctamente. L�nea: " & lLinea & " -- C�digo Sucursal: '" & lCod_Sucursal & "' -- C�digo Ejecutivo: '" & lCod_Ejecutivo & "' -- Descripci�n Ejecutivo: '" & lDsc_Ejecutivo & "'.")
          Else
            Call Fnt_Escribe_Grilla(Grilla, "E", "Error en la l�nea: " & lLinea & " -- " & lMsg_Error)
          End If
        Else
          lMsg_Error = "N�mero de campos debe ser igual a " & eColumnas_Ejecutivo.eGD_Dsc_Ejecutivo & "."
          Call Fnt_Escribe_Grilla(Grilla, "E", "Error en la l�nea: " & lLinea & " -- " & lMsg_Error)
        End If
      Else
        Call Fnt_Escribe_Grilla(Grilla, "E", "Error de formato en la l�nea: " & lLinea & " -- L�nea sin campos.")
      End If
      .MoveNext
    Loop
    .CloseFile
    
    Call Fnt_Escribe_Grilla(Grilla, "N", Fnt_FechaServidor & " -- Fin Carga de Ejecutivos.")
    
    MsgBox "Carga de Ejecutivos Finalizada.", vbInformation, Me.Caption
  End With
  
  Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Function Fnt_Ingresa_Ejecutivos(pCod_Sucursal As String, pCod_Ejecutivo As String, pDsc_Ejecutivo As String, ByRef pMsg_Error As String) As Boolean
Dim lcEjecutivo As Class_Ejecutivos_Sucursal
  
  Fnt_Ingresa_Ejecutivos = True
  
  If Not Fnt_Valida_Ejecutivo(pCod_Sucursal, pCod_Ejecutivo, pDsc_Ejecutivo, pMsg_Error) Then
    GoTo ErrProcedure
  End If
  
  Set lcEjecutivo = New Class_Ejecutivos_Sucursal
  With lcEjecutivo
    .Campo("Cod_Sucursal").Valor = pCod_Sucursal
    .Campo("Cod_Ejecutivo").Valor = pCod_Ejecutivo
    .Campo("Dsc_Ejecutivo").Valor = pDsc_Ejecutivo
    If Not .Guardar Then
      pMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcEjecutivo = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Ingresa_Ejecutivos = False
  Set lcEjecutivo = Nothing
  
End Function

Private Function Fnt_Valida_Ejecutivo(pCod_Sucursal As String, pCod_Ejecutivo As String, pDsc_Ejecutivo As String, ByRef pMsg_Error As String) As Boolean
Dim lcSucursal As Class_Sucursales

  Fnt_Valida_Ejecutivo = True
  
  If pCod_Sucursal = "" Then
    pMsg_Error = "C�digo de Sucursal no debe ser vac�o."
    GoTo ErrProcedure
  ElseIf Len(pCod_Sucursal) > 10 Then
    pMsg_Error = "C�digo de Sucursal '" & pCod_Sucursal & "' debe tener menos de 10 caracteres."
    GoTo ErrProcedure
  ElseIf pCod_Ejecutivo = "" Then
    pMsg_Error = "C�digo de Ejecutivo no debe ser vac�o."
    GoTo ErrProcedure
  ElseIf Len(pCod_Ejecutivo) > 10 Then
    pMsg_Error = "C�digo de Ejecutivo '" & pCod_Ejecutivo & "' debe tener menos de 10 caracteres."
    GoTo ErrProcedure
  ElseIf pDsc_Ejecutivo = "" Then
    pMsg_Error = "Descripci�n de Ejecutivo no debe ser vac�o."
    GoTo ErrProcedure
  ElseIf Len(pDsc_Ejecutivo) > 100 Then
    pMsg_Error = "Descripci�n de Ejecutivo '" & pDsc_Ejecutivo & "' debe tener menos de 100 caracteres."
    GoTo ErrProcedure
  End If
  
  Set lcSucursal = New Class_Sucursales
  With lcSucursal
    .Campo("cod_sucursal").Valor = pCod_Sucursal
    If .Buscar Then
      If .Cursor.Count = 0 Then
        pMsg_Error = "C�digo de Sucursal '" & pCod_Sucursal & "' no fue encontrado en el sistema. El Ejecutivo no pudo ser ingresado."
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  
  Exit Function
  
ErrProcedure:
  Fnt_Valida_Ejecutivo = False
End Function
