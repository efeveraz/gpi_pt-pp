VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form frm_Busca_Nemotecnicos_II 
   Caption         =   "Buscador de nemotecnicos Ver 2.0"
   ClientHeight    =   4875
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10050
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   4875
   ScaleWidth      =   10050
   Begin VB.Frame Frame1 
      Caption         =   "Cuentas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   60
      TabIndex        =   3
      Top             =   1140
      Width           =   9915
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3225
         Left            =   120
         TabIndex        =   4
         Top             =   300
         Width           =   9675
         _cx             =   17066
         _cy             =   5689
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   6
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"frm_Busca_Nemotecnicos_II.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin VB.Frame Frame5 
      Height          =   705
      Left            =   60
      TabIndex        =   0
      Top             =   390
      Width           =   9915
      Begin hControl2.hTextLabel Txt_Nemotecnico 
         Height          =   315
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   3060
         _ExtentX        =   5398
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   1200
         Caption         =   "Nemotecnico"
         Text            =   ""
         MaxLength       =   10
      End
      Begin hControl2.hTextLabel Txt_Emisor_Especifico 
         Height          =   315
         Left            =   3240
         TabIndex        =   2
         Top             =   240
         Width           =   6570
         _ExtentX        =   11589
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Emisor"
         Text            =   ""
         Format          =   ""
         MaxLength       =   35
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   10050
      _ExtentX        =   17727
      _ExtentY        =   635
      ButtonWidth     =   1561
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Buscar"
            Key             =   "FIND"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   6
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "frm_Busca_Nemotecnicos_II"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Rem ----------------------------------------------------------
Rem 10/02/2009. Agrega par�metro Cod_Producto en la b�squeda
Rem ----------------------------------------------------------

Dim fCod_Instrumento
Dim fId_Nemotecnico
Dim fCodOperacion
Dim fId_Cuenta
Dim fFecha_Movimiento
Dim fId_Mov_Activo
Dim fCod_Producto
Dim fCod_SubFamilia
Public fCantidad

Public Function Buscar(Optional pCod_Instrumento = Null _
                     , Optional pNemotecnico = Null _
                     , Optional pDsc_Emisor_Especifico = Null _
                     , Optional pCodOperacion = Null _
                     , Optional pId_Cuenta = Null _
                     , Optional pFecha_Movimiento = Null _
                     , Optional ByRef pId_Mov_Activo _
                     , Optional pSaldoTodasLasCuentas As Boolean = False _
                     , Optional pCod_Producto = Null _
                     , Optional pCod_Subfamilia = Null)
Dim lCargar As Boolean
    
  Call Form_Resize
  Load Me
  
  lCargar = False
  
  fCod_Instrumento = Trim(pCod_Instrumento)
  fCodOperacion = Trim(pCodOperacion)
  fId_Cuenta = pId_Cuenta
  fFecha_Movimiento = pFecha_Movimiento
  fCod_Producto = Trim(pCod_Producto)
  fCod_SubFamilia = Trim(pCod_Subfamilia)
    
  If Not IsNull(pNemotecnico) Then
    If pCodOperacion = gcTipoOperacion_Ingreso Then
        If pNemotecnico <> "" Then
            Txt_Nemotecnico.Text = pNemotecnico
            lCargar = True
        End If
    Else
        Txt_Nemotecnico.Text = pNemotecnico
        lCargar = True
    End If
  End If
   
  If Not IsNull(pDsc_Emisor_Especifico) Then
    Txt_Emisor_Especifico.Text = pDsc_Emisor_Especifico
    lCargar = True
  End If
    
  
  If lCargar Then
    If pSaldoTodasLasCuentas Then
        Call Sub_CargarDatos_Solo_Saldo_Todas_las_cuentas
    Else
        Call Sub_CargarDatos
    End If
    
'    If Grilla.Rows = 2 Then
'      fId_Nemotecnico = GetCell(Grilla, 1, "colum_pk")
'      pId_Mov_Activo = GetCell(Grilla, 1, "id_mov_Activo")
'      Buscar = fId_Nemotecnico
'      GoTo ExitProcedure
'    End If
'
    fId_Nemotecnico = Null
  End If
    
  Me.Show vbModal

  Buscar = fId_Nemotecnico
  pId_Mov_Activo = fId_Mov_Activo
  
ExitProcedure:
  Unload Me
End Function

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("EXIT").Image = cBoton_Salir
        .Buttons("FIND").Image = cBoton_Buscar
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Me.Tag = UnloadMode
End Sub

Private Sub Form_Resize()
    Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub


Private Sub Grilla_DblClick()
  If Grilla.Row > 0 Then
    fId_Nemotecnico = GetCell(Grilla, Grilla.Row, "colum_pk")
    fId_Mov_Activo = GetCell(Grilla, Grilla.Row, "ID_MOV_ACTIVO")
    fCantidad = GetCell(Grilla, Grilla.Row, "cantidad")
    Me.Hide
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "FIND"
      Call Sub_CargarDatos
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_CargarDatos()
Dim lcNemotecnico   As New Class_Nemotecnicos
Dim lReg            As hCollection.hFields
Dim lBonos As Class_Bonos
'--------------------------------------------------------------------
Dim lLinea          As Long
Dim lID             As String
Dim lValRet         As Boolean
Dim lId_Nemotecnico As String
Dim lId_Mov_Activo As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  End If
  
  Grilla.Rows = 1
    
  If Not lcNemotecnico.BuscarAyuda(pCod_Instrumento:=fCod_Instrumento _
                                 , pNemotecnico:=Txt_Nemotecnico.Text _
                                 , pCodOperacion:=fCodOperacion _
                                 , pId_Cuenta:=fId_Cuenta _
                                 , pFecha_Movimiento:=fFecha_Movimiento _
                                 , pCod_Producto:=fCod_Producto _
                                 , pCod_Subfamilia:=fCod_SubFamilia) Then
    MsgBox lcNemotecnico.ErrMsg, vbCritical, Me.Caption
    GoTo ExitProcedure
  End If
   
  Call Prc_ComponentOne.Sub_hRecord2Grilla(pCursor:=lcNemotecnico.Cursor, pGrilla:=Grilla, pColum_PK:="id_nemotecnico", pAutoSize:=True)

  If fCodOperacion = gcTipoOperacion_Egreso Then
    Set lBonos = New Class_Bonos
    For lLinea = 1 To (Grilla.Rows - 1)
      lId_Nemotecnico = GetCell(Grilla, lLinea, "colum_pk")
      lId_Mov_Activo = GetCell(Grilla, lLinea, "id_mov_activo")
      If fCod_Instrumento = "BONOS_NAC" Then
         fCantidad = GetCell(Grilla, lLinea, "cantidad")
      Else
         Call SetCell(Grilla, lLinea, "cantidad", lBonos.Saldo_Activo_Cantidad(fId_Cuenta, lId_Nemotecnico, lId_Mov_Activo))
      End If
    Next
    Set lBonos = Nothing
  End If

  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If


ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargaForm()
  Rem Limpia la grilla
  Grilla.Rows = 1

  Call Sub_FormControl_Color(Me.Controls)
End Sub

Private Sub Txt_Nemotecnico_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub Txt_Emisor_Especifico_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
Private Sub Sub_CargarDatos_Solo_Saldo_Todas_las_cuentas()
Dim lcNemotecnico   As New Class_Nemotecnicos
Dim lReg            As hCollection.hFields
Dim lBonos As Class_Bonos
'--------------------------------------------------------------------
Dim lLinea          As Long
Dim lID             As String
Dim lValRet         As Boolean
Dim lId_Nemotecnico As String
Dim lId_Mov_Activo As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  End If
  Grilla.Rows = 1
  If Not lcNemotecnico.BuscarAyuda(pCod_Instrumento:=fCod_Instrumento _
                                 , pNemotecnico:=Txt_Nemotecnico.Text _
                                 , pCodOperacion:=fCodOperacion _
                                 , pId_Cuenta:=fId_Cuenta _
                                 , pFecha_Movimiento:=fFecha_Movimiento) Then
    MsgBox lcNemotecnico.ErrMsg, vbCritical, Me.Caption
    GoTo ExitProcedure
  End If
   
  Call Prc_ComponentOne.Sub_hRecord2Grilla(pCursor:=lcNemotecnico.Cursor, pGrilla:=Grilla, pColum_PK:="id_nemotecnico", pAutoSize:=True)

  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If

ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub




