VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Grupos_Cuentas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Grupo Cuenta"
   ClientHeight    =   4890
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7635
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4890
   ScaleWidth      =   7635
   Begin VB.Frame Frame1 
      Height          =   4395
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   7485
      Begin hControl2.hTextLabel Txt_Grupo_Cuenta 
         Height          =   315
         Left            =   150
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   270
         Width           =   7170
         _ExtentX        =   12647
         _ExtentY        =   556
         LabelWidth      =   1200
         TextMinWidth    =   1200
         Caption         =   "Grupo Cuenta"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   60
      End
      Begin MSComctlLib.Toolbar Toolbar_Interna 
         Height          =   360
         Left            =   30
         TabIndex        =   4
         Top             =   750
         Width           =   7485
         _ExtentX        =   13203
         _ExtentY        =   635
         ButtonWidth     =   1640
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Wrappable       =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Agrega"
               Key             =   "ADD"
               Description     =   "Agrega informaci�n"
               Object.ToolTipText     =   "Agrega informaci�n"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Elimina"
               Key             =   "DEL"
               Description     =   "Elimina informaci�n"
               Object.ToolTipText     =   "Elimina informaci�n"
            EndProperty
         EndProperty
         BorderStyle     =   1
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   255
            Left            =   9420
            TabIndex        =   5
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   2565
         Left            =   120
         TabIndex        =   6
         Top             =   1710
         Width           =   7275
         _cx             =   12832
         _cy             =   4524
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   2
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Grupos_Cuentas.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin TrueDBList80.TDBCombo cbo_Cuentas 
         Height          =   345
         Left            =   1380
         TabIndex        =   8
         Top             =   1230
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Grupos_Cuentas.frx":006A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuentas"
         Height          =   315
         Left            =   150
         TabIndex        =   7
         Top             =   1230
         Width           =   1200
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7635
      _ExtentX        =   13467
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Grupos_Cuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fFlg_Tipo_Permiso As String

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Interna
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
  End With
  
  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pId_Grupos_Cuentas, pCod_Arbol_Sistema)
  fKey = pId_Grupos_Cuentas
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Call Sub_CargarDatos
    
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso Grupo Cuenta"
  Else
    Me.Caption = "Modificaci�n Grupo Cuenta: " & Txt_Grupo_Cuenta.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Rem Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargaForm
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lFila As Long
Dim lId_Cuenta As Long
Dim lcGrupo_Cuenta As Class_Grupos_Cuentas
Dim lRollback As Boolean
Dim lcRel_Cta_Grupo_Cta As Class_Rel_Cuentas_Grupos_Cuentas

  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If
  
  gDB.IniciarTransaccion
  lRollback = False
  '=========  CREACION O MODIFICACION DEL GRUPO DE CUENTA ========================
  Set lcGrupo_Cuenta = New Class_Grupos_Cuentas
  With lcGrupo_Cuenta
    .Campo("id_Grupo_Cuenta").Valor = fKey
    .Campo("dsc_Grupo_Cuenta").Valor = Txt_Grupo_Cuenta.Text
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If Not .Guardar Then
      MsgBox "Problemas al grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en grabar el Grupo Cuenta.", _
                        .ErrMsg, _
                        pConLog:=True)
      lRollback = True
      GoTo ErrProcedure
    End If
    fKey = .Campo("id_Grupo_Cuenta").Valor
  End With
  Set lcGrupo_Cuenta = Nothing
  
  '=========  ELIMINAR LA RELACI�N ANTES DE GRABAR  ========================
  Set lcRel_Cta_Grupo_Cta = New Class_Rel_Cuentas_Grupos_Cuentas
  With lcRel_Cta_Grupo_Cta
    .Campo("id_Grupo_Cuenta").Valor = fKey
    If Not .Borrar Then
      MsgBox "Problemas al eliminar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en eliminar la relaci�n Cuentas con Grupos Cuentas.", _
                        .ErrMsg, _
                        pConLog:=True)
      lRollback = True
      GoTo ErrProcedure
    End If
  End With
  Set lcRel_Cta_Grupo_Cta = Nothing
  
  '=========  GRABAR LOS REGISTROS  ========================================
  For lFila = 1 To Grilla.Rows - 1
    Set lcRel_Cta_Grupo_Cta = New Class_Rel_Cuentas_Grupos_Cuentas
    With lcRel_Cta_Grupo_Cta
      lId_Cuenta = GetCell(Grilla, lFila, "colum_pk")
      
      .Campo("id_Cuenta").Valor = lId_Cuenta
      .Campo("id_Grupo_Cuenta").Valor = fKey

      If Not .Guardar Then
        Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al grabar la relaci�n Grupos Cuentas.", _
                        .ErrMsg, _
                        pConLog:=True)
        lRollback = True
        GoTo ErrProcedure
      End If
    End With
    Set lcRel_Cta_Grupo_Cta = Nothing
  Next
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    Fnt_Grabar = False
  Else
    gDB.CommitTransaccion
  End If
  
  gDB.Parametros.Clear
  Set lcGrupo_Cuenta = Nothing
  Set lcRel_Cta_Grupo_Cta = Nothing
  
End Function

Private Sub Sub_CargaForm()
Dim lReg As hFields
Dim lLinea As Integer
Dim lcRel_Cta_Grupo_Cta As Class_Rel_Cuentas_Grupos_Cuentas

  Call Sub_FormControl_Color(Me.Controls)

  Txt_Grupo_Cuenta.Text = ""
  
  Grilla.Rows = 1
  
  Call Sub_CargaCombo_Cuentas_Vigentes(cbo_Cuentas)
  
  Set lcRel_Cta_Grupo_Cta = New Class_Rel_Cuentas_Grupos_Cuentas
  With lcRel_Cta_Grupo_Cta
    .Campo("id_grupo_cuenta").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla.Rows
        Grilla.AddItem ("")
        SetCell Grilla, lLinea, "colum_pk", lReg("ID_Cuenta").Value
        SetCell Grilla, lLinea, "dsc_cuentas", lReg("Num_Cuenta").Value & " - " & lReg("abr_cuenta").Value
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en la carga de datos.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcRel_Cta_Grupo_Cta = Nothing
  
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lcGrupo_Cuenta As Class_Grupos_Cuentas
  
  Load Me
  
  Set lcGrupo_Cuenta = New Class_Grupos_Cuentas
  With lcGrupo_Cuenta
    .Campo("ID_GRUPO_CUENTA").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        Txt_Grupo_Cuenta.Text = DLL_COMUN.NVL(lReg("DSC_GRUPO_CUENTA").Value, "")
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en la carga de datos.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcGrupo_Cuenta = Nothing
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function

Private Sub Sub_Agrega()
Dim lLinea As Long
Dim lId_Cuenta As String
  
  lId_Cuenta = Fnt_ComboSelected_KEY(cbo_Cuentas)
  
  If Not lId_Cuenta = "" Then
    Grilla.Row = Grilla.FindRow(lId_Cuenta, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      MsgBox "Cuenta ya se encuentra asociada.", vbExclamation, Me.Caption
    Else
      lLinea = Grilla.Rows
      Grilla.AddItem ("")
      SetCell Grilla, lLinea, "colum_pk", lId_Cuenta
      SetCell Grilla, lLinea, "dsc_cuentas", cbo_Cuentas.Text
    End If
  End If
  
End Sub

Private Sub Sub_Elimina()
Dim lCol As Long

  If Grilla.Row >= 1 Then
    '===  Solo elimina de la grilla, se hace efectivo cuando graba los cambios ======
    Grilla.RemoveItem (Grilla.Row)
  End If
End Sub

Private Sub Toolbar_Interna_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agrega
    Case "DEL"
      Call Sub_Elimina
  End Select
End Sub
