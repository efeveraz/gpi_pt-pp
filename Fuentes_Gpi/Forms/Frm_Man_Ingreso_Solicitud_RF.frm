VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Man_Ingreso_Solicitud_RF 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ingreso de Solicitud de Renta Fija"
   ClientHeight    =   6720
   ClientLeft      =   1290
   ClientTop       =   1785
   ClientWidth     =   11430
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   11430
   Begin VB.Frame Frame1 
      Caption         =   "Rango de Fecha"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   6180
      Left            =   45
      TabIndex        =   0
      Top             =   420
      Width           =   11355
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   5205
         Left            =   120
         TabIndex        =   1
         Top             =   840
         Width           =   11085
         _cx             =   19553
         _cy             =   9181
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   19
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Ingreso_Solicitud_RF.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
         Height          =   315
         Left            =   4030
         TabIndex        =   4
         Tag             =   "OBLI"
         Top             =   315
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   556
         _Version        =   393216
         Format          =   17235969
         CurrentDate     =   38938
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ini 
         Height          =   315
         Left            =   1320
         TabIndex        =   5
         Tag             =   "OBLI"
         Top             =   315
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   556
         _Version        =   393216
         Format          =   17235969
         CurrentDate     =   38938
      End
      Begin MSComctlLib.Toolbar Toolbar_Proceso 
         Height          =   330
         Left            =   5745
         TabIndex        =   6
         Top             =   315
         Width           =   1020
         _ExtentX        =   1799
         _ExtentY        =   582
         ButtonWidth     =   1561
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar"
               Key             =   "BUSC"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
            EndProperty
         EndProperty
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha T�rmino"
         Height          =   315
         Index           =   0
         Left            =   2850
         TabIndex        =   8
         Top             =   315
         Width           =   1170
      End
      Begin VB.Label Lbl_Fecha_Ini 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Inicio"
         Height          =   315
         Index           =   1
         Left            =   135
         TabIndex        =   7
         Top             =   315
         Width           =   1170
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   11430
      _ExtentX        =   20161
      _ExtentY        =   635
      ButtonWidth     =   1984
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Ingresar"
            Key             =   "ADD_SOL"
            Description     =   "Agrega una Solicitud"
            Object.ToolTipText     =   "Agrega una Solicitud"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9000
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Ingreso_Solicitud_RF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Rem Flag para operar fechas anteriores
Dim fFlg_Fechas_Anteriores As Boolean
'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana
Dim fRutCliente As String

Public Sub Mostrar(pCod_Arbol_Sistema, Optional pFlg_Fechas_Anteriores As Boolean = False)
  
  fFlg_Fechas_Anteriores = pFlg_Fechas_Anteriores
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Form_Load()
    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("ADD_SOL").Image = cBoton_Agregar_Grilla
        .Buttons("EXIT").Image = cBoton_Salir
        .Buttons("REFRESH").Image = cBoton_Refrescar
        .Buttons("PRINTER").Image = cBoton_Imprimir
    End With
    With Toolbar_Proceso
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("BUSC").Image = "boton_grilla_buscar"
        .Appearance = ccFlat
    End With
    Call Sub_CargaForm
    Me.Top = 1
    Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD_SOL"
      Call Sub_EsperaVentana(cNewEntidad)
'    Case "REFRESH"
'      Grilla.Rows = 1
    Case "PRINTER"
      Call Sub_Imprimir(GetCell(Grilla, Grilla.RowSel, "id_solicitud"), ePrinter.eP_pantalla)
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_CargaForm()
Dim lReg  As hCollection.hFields
Dim lLinea As Long
  
  Call Sub_FormControl_Color(Me.Controls)

  Call Sub_Bloquea_Puntero(Me)
  
  Grilla.Rows = 1
  DTP_Fecha_Ini.Value = PrimerDiaMes(Fnt_FechaServidor)
  DTP_Fecha_Ter.Value = UltimoDiaDelMesEnCurso(DTP_Fecha_Ini.Value)
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Grilla_DblClick()
'   Dim lKey As String
'
'   With Grilla
'      If .Row > 0 Then
'         lKey = GetCell(Grilla, .Row, "colum_pk")
'
'         Call Sub_EsperaVentana(lKey, "")
'      End If
'   End With
End Sub

Private Sub Sub_EsperaVentana(pkey)
   Dim lForm_Ingreso_Solicitud_RF As Frm_Ingreso_Solicitud_RF
   Dim lNombre As String
   
   Me.Enabled = False
   
      If Not Fnt_ExisteVentanaKey("Frm_Ingreso_Solicitud_RF", pkey) Then
         lNombre = Me.Name
      
         Set lForm_Ingreso_Solicitud_RF = New Frm_Ingreso_Solicitud_RF
         
         Call lForm_Ingreso_Solicitud_RF.Fnt_Modificar(pId_Solicitud:=pkey _
                                                  , pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
         
         Do While Fnt_ExisteVentanaKey("Frm_Ingreso_Solicitud_RF", pkey)
            DoEvents
         Loop
       
         If Fnt_ExisteVentana(lNombre) Then
            Call Sub_BuscarDatosGrilla(True)
          Else
            Exit Sub
         End If
      End If
   
   Me.Enabled = True
   
End Sub

Private Sub Sub_Imprimir(pId_Solicitud, pTipoSalida As ePrinter)
   Dim lForm As Frm_Reporte_Generico
   Dim pArchivoPDF As String
  
   pArchivoPDF = Environ("temp") & "\Solicitud_Compra_Renta_Fija_" & pId_Solicitud & ".pdf"
  
  Set lForm = Frm_Reporte_Generico
  
   Call lForm.Sub_InicarDocumento(pTitulo:="" _
                                , pTipoSalida:=pTipoSalida _
                               , pOrientacion:=orPortrait _
                               , pArchivoSalida:=pArchivoPDF)

   With lForm.VsPrinter
      
      .MarginLeft = "15mm"
      .MarginRight = "10mm"
      
      .StartTable
         .TableBorder = tbNone
         .TableCell(tcRows) = 1
         .TableCell(tcCols) = 1
         .TableCell(tcFontSize, 1, 1, 2, 5) = 14
         .TableCell(tcAlign, 1, 1) = taCenterMiddle
         .TableCell(tcFontBold, 1, 1) = True
         .TableCell(tcText, 1, 1) = "SOLICITUD COMPRA RENTA FIJA"
         .TableCell(tcColWidth, 1, 1, 7, 1) = "200mm"
      .EndTable
      
      .Paragraph = "" 'salto de linea
      .Paragraph = "" 'salto de linea

      .StartTable
         .TableCell(tcRows) = 11
         .TableCell(tcCols) = 3
         .TableCell(tcFontSize, 1, 1, 11, 3) = 10
         
         
         .TableCell(tcColWidth, 1, 1) = "40mm"
         .TableCell(tcColWidth, 1, 2) = "10mm"
         .TableCell(tcColWidth, 1, 3) = "120mm"
         
         .TableCell(tcRowHeight, 1) = 500
         .TableCell(tcText, 1, 1) = "Fecha"
         .TableCell(tcText, 1, 2) = ":"
         .TableCell(tcText, 1, 3) = GetCell(Grilla, Grilla.RowSel, "fecha_solicitud")
         
         .TableCell(tcRowHeight, 2) = 500
         .TableCell(tcText, 2, 1) = "Instrumento"
         .TableCell(tcText, 2, 2) = ":"
         .TableCell(tcText, 2, 3) = GetCell(Grilla, Grilla.RowSel, "instrumento")
         
         .TableCell(tcRowHeight, 3) = 500
         .TableCell(tcText, 3, 1) = "SubFamilia"
         .TableCell(tcText, 3, 2) = ":"
         .TableCell(tcText, 3, 3) = GetCell(Grilla, Grilla.RowSel, "sub_familia")
         
         .TableCell(tcRowHeight, 4) = 500
         .TableCell(tcText, 4, 1) = "Emisor"
         .TableCell(tcText, 4, 2) = ":"
         .TableCell(tcText, 4, 3) = GetCell(Grilla, Grilla.RowSel, "emisor")
         
         .TableCell(tcRowHeight, 5) = 500
         .TableCell(tcText, 5, 1) = "Trader"
         .TableCell(tcText, 5, 2) = ":"
         .TableCell(tcText, 5, 3) = GetCell(Grilla, Grilla.RowSel, "trader")
         
         .TableCell(tcRowHeight, 6) = 500
         .TableCell(tcText, 6, 1) = "Plazo"
         .TableCell(tcText, 6, 2) = ":"
         .TableCell(tcText, 6, 3) = GetCell(Grilla, Grilla.RowSel, "plazo")
         
         .TableCell(tcRowHeight, 7) = 500
         .TableCell(tcText, 7, 1) = "Tipo Plazo"
         .TableCell(tcText, 7, 2) = ":"
         .TableCell(tcText, 7, 3) = GetCell(Grilla, Grilla.RowSel, "tipo_plazo")
         
         If GetCell(Grilla, Grilla.RowSel, "rango") = "" Then
            .TableCell(tcRowHeight, 8) = 1
         Else
            .TableCell(tcRowHeight, 8) = 500
            .TableCell(tcText, 8, 1) = "Rango"
            .TableCell(tcText, 8, 2) = ":"
            .TableCell(tcText, 8, 3) = GetCell(Grilla, Grilla.RowSel, "rango")
         End If
         
         .TableCell(tcRowHeight, 9) = 500
         .TableCell(tcText, 9, 1) = "Duration"
         .TableCell(tcText, 9, 2) = ":"
         .TableCell(tcText, 9, 3) = GetCell(Grilla, Grilla.RowSel, "duration")
         
         .TableCell(tcRowHeight, 10) = 500
         .TableCell(tcText, 10, 1) = "Monto Orden"
         .TableCell(tcText, 10, 2) = ":"
         .TableCell(tcText, 10, 3) = GetCell(Grilla, Grilla.RowSel, "simbolo") & " " & GetCell(Grilla, Grilla.RowSel, "monto_orden")
                  
         .TableCell(tcRowHeight, 11) = 4000
         .TableCell(tcAlign, 11, 3) = taJustTop
         .TableCell(tcText, 11, 1) = "Observaciones"
         .TableCell(tcText, 11, 2) = ":"
         .TableCell(tcText, 11, 3) = GetCell(Grilla, Grilla.RowSel, "Observaciones")
      
      .EndTable
      
   .EndDoc
   
   End With

ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Sub_BuscarDatosGrilla(Optional p_buscar As Boolean = False)
    Dim oSolicitudRentaFija As New Class_Solicitud_RentaFija
    '----------------------------------------------------------------------------
    Dim lReg        As hFields
    Dim ldFechaIni  As String
    Dim ldFechaFin  As String
    '----------------------------------------------------------------------------
    Dim lLinea      As Long
    Dim lFormato    As String
    Dim lSCodEstado As String
    '----------------------------------------------------------------------------
    Dim lhTotales As hRecord
    Dim lfTotal As hFields

    On Error GoTo ErrProcedure

    Call Sub_Bloquea_Puntero(Me)

    If Not Fnt_Form_Validar(Me.Controls) Then
        Exit Sub
    End If
    
    ldFechaIni = DTP_Fecha_Ini.Value
    ldFechaFin = DTP_Fecha_Ter.Value
    Grilla.Rows = 1
    With oSolicitudRentaFija
        If Not .Buscar_EntreFecha(ldFechaIni, ldFechaFin, p_buscar) Then
            GoTo ExitProcedure
        End If
        For Each lReg In .Cursor
            lLinea = Grilla.Rows
            Grilla.AddItem ""
            
            Call SetCell(Grilla, lLinea, "id_solicitud", lReg("ID_SOLICITUD").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "fecha_solicitud", lReg("FECHA_SOLICITUD").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "cod_instrumento", lReg("cod_instrumento").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "instrumento", lReg("dsc_intrumento").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "id_subfamilia", lReg("ID_SUBFAMILIA").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "sub_familia", lReg("dsc_subfamilia").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "id_emisor", lReg("ID_Emisor_Especifico").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "emisor", lReg("dsc_emisor_especifico").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "id_trader", lReg("ID_TRADER").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "trader", lReg("dsc_trader").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "plazo", lReg("PLAZO").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "tipo_plazo", IIf(lReg("TIPO_PLAZO").Value = "M", "Mensual", "Anual"), pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "rango", "" & lReg("RANGO").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "duration", lReg("DURATION").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "id_moneda", lReg("ID_MONEDA").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "dsc_moneda", lReg("DSC_MONEDA").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "simbolo", lReg("SIMBOLO").Value, pAutoSize:=False)
            Call SetCell(Grilla, lLinea, "monto_orden", Format(lReg("MONTO_ORDEN").Value, Fnt_Formato_Moneda(lReg("ID_MONEDA").Value)))
            Call SetCell(Grilla, lLinea, "observaciones", "" & lReg("OBSERVACIONES").Value, pAutoSize:=False)
        Next
    End With
    
    'Grilla.SetFocus

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al traer las Solicitudes de Inversi�n Renta Fija.", Err.Description, pConLog:=False)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
    Set oSolicitudRentaFija = Nothing
    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Sub_Colores(pExcel As Excel.Application, _
                        pLibro As Excel.Workbook, _
                        pRango As String, _
                        pColor As Long)
  
  With pLibro
    .Sheets(1).Select
    .Sheets(1).Select
    .Sheets(1).Activate
    
    .ActiveSheet.Range(pRango).Select
    .ActiveSheet.Range(pRango).Activate
  End With
  
  With pExcel
    .Range(.Selection, .Selection.End(xlToRight)).Select
    pLibro.ActiveSheet.Range(.Selection, .Selection.End(xlToRight)).Font.Bold = True
    
    .Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    .Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    .Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeRight).Weight = xlMedium
        
    .Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    .Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    .Selection.Borders(xlInsideVertical).Weight = xlThin
    
    .Selection.Interior.ColorIndex = pColor
    .Selection.Interior.Pattern = xlSolid
  End With
End Sub
Private Sub Toolbar_Proceso_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "BUSC"
            Call Sub_BuscarDatosGrilla(True)
    End Select
End Sub
