VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_ClasificadorRiesgo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Clasificador de Riesgo"
   ClientHeight    =   4710
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6885
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4710
   ScaleWidth      =   6885
   Begin VB.Frame Frame1 
      Caption         =   "Datos Clasificaci�n de Riesgo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4215
      Left            =   83
      TabIndex        =   2
      Top             =   390
      Width           =   6705
      Begin VB.Frame Frame2 
         Caption         =   "Valor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   3285
         Left            =   150
         TabIndex        =   4
         Top             =   780
         Width           =   6405
         Begin VSFlex8LCtl.VSFlexGrid Grilla 
            Height          =   2625
            Left            =   150
            TabIndex        =   5
            Top             =   270
            Width           =   5595
            _cx             =   9869
            _cy             =   4630
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   5
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ClasificadorRiesgo.frx":0000
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Grilla 
            Height          =   660
            Left            =   5820
            TabIndex        =   6
            Top             =   300
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ADD"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Agregar un registro"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "DEL"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Elimina un registro"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar1 
               Height          =   255
               Left            =   9420
               TabIndex        =   7
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
         Begin MSComctlLib.Toolbar Tool_Grilla_Flechas 
            Height          =   660
            Left            =   5820
            TabIndex        =   8
            Top             =   2220
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "UP"
                  Description     =   "Sube de nivel el registro"
                  Object.ToolTipText     =   "Sube de nivel el registro."
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "DOWN"
                  Description     =   "Baja de nivel el registro"
                  Object.ToolTipText     =   "Baja de nivel el registro."
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar2 
               Height          =   255
               Left            =   9420
               TabIndex        =   9
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
      Begin hControl2.hTextLabel txt_desc 
         Height          =   315
         Left            =   150
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   360
         Width           =   6375
         _ExtentX        =   11245
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Descripci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   49
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6885
      _ExtentX        =   12144
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_ClasificadorRiesgo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fClasificador_Antiguo As String

Dim fBorrar_Clasificador As hRecord

Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana

Private Sub Form_Load()
   With Toolbar
   Set .ImageList = MDI_Principal.ImageListGlobal16
       .Buttons("SAVE").Image = cBoton_Grabar
       .Buttons("EXIT").Image = cBoton_Salir
       .Buttons("REFRESH").Image = cBoton_Original
   End With
   With Toolbar_Grilla
   Set .ImageList = MDI_Principal.ImageListGlobal16
       .Buttons("ADD").Image = cBoton_Agregar_Grilla
       .Buttons("DEL").Image = cBoton_Eliminar_Grilla
       .Appearance = ccFlat
   End With
   With Tool_Grilla_Flechas
   Set .ImageList = MDI_Principal.ImageListGlobal16
       .Buttons("UP").Image = "flecha_up"
       .Buttons("DOWN").Image = "flacha_down"
       .Appearance = ccFlat
   End With

   Call Sub_CargaForm
   
   Set fBorrar_Clasificador = New hRecord
   With fBorrar_Clasificador
     .ClearFields
     .AddField "cod_clasificador_riesgo"
   End With

   Me.Top = 1
   Me.Left = 1
   
End Sub

Public Function Fnt_Modificar(pId_Clasificador_Riesgo, pCod_Arbol_Sistema)
   fKey = pId_Clasificador_Riesgo
  
'-----------------------------------------------------------------------
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
'-----------------------------------------------------------------------
   
   Call Sub_CargarDatos
   
   If fKey = cNewEntidad Then
      Me.Caption = "Ingreso de Clasificador de Riesgo"
   Else
      Me.Caption = "Modificaci�n Clasificador de Riesgo: " & txt_desc.Text
   End If
   
   Me.Top = 1
   Me.Left = 1
   Me.Show
End Function

Private Sub Form_Resize()
   Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  fClasificador_Antiguo = GetCell(Grilla, Row, "COD_VALOR_CLASIFICACION")
End Sub

Private Sub Grilla_DblClick()
Dim pCursor As hRecord
Dim lc_Rel_Nemo_Valor_Clasif As Class_Rel_Nemotec_Valor_Clasific

  If Grilla.Row > 0 Then
    If Not GetCell(Grilla, Grilla.Row, "COD_VALOR_CLASIFICACION") = "" Then
      Set lc_Rel_Nemo_Valor_Clasif = New Class_Rel_Nemotec_Valor_Clasific
      With lc_Rel_Nemo_Valor_Clasif
        .Campo("id_Clasificador_Riesgo").Valor = fKey
        .Campo("Cod_Valor_Clasificacion").Valor = GetCell(Grilla, Grilla.Row, "COD_VALOR_CLASIFICACION")
        If .Buscar Then
           Set pCursor = .Cursor
           If pCursor.Count > 0 Then
             MsgBox "El C�digo de Clasificaci�n de Riesgo '" & GetCell(Grilla, Grilla.Row, "COD_VALOR_CLASIFICACION") & "' no se puede modificar porque est� asociado a otros elementos en el sistema.", vbExclamation, Me.Caption
             SetCell Grilla, Grilla.Row, "COD_VALOR_CLASIFICACION", fClasificador_Antiguo
           Else
             SetCell Grilla, Grilla.Row, "TIPO", "BI"
             fBorrar_Clasificador.Add.Fields("cod_clasificador_riesgo").Value = fClasificador_Antiguo
           End If
        Else
          MsgBox .ErrMsg, vbCritical, Me.Caption
        End If
      End With
      Set lc_Rel_Nemo_Valor_Clasif = Nothing
    End If
  End If

End Sub

Private Sub Tool_Grilla_Flechas_ButtonClick(ByVal Button As MSComctlLib.Button)
   Select Case Button.Key
      Case "UP"
         If Grilla.Row > 0 Then
            Call Sub_Inicio
         End If
      
      Case "DOWN"
         If Grilla.Row > 0 Then
            Call Sub_Final
         End If
   End Select
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

   Select Case Button.Key
      Case "SAVE"
         If Fnt_Grabar Then
            'Si no hubo problemas al grabar, sale
            Unload Me
         End If
      Case "EXIT"
         Unload Me
      Case "REFRESH"
         If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
         , vbYesNo + vbQuestion, Me.Caption) = vbYes Then
            Call Sub_CargarDatos
         End If
   End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lClasificador_Riesgo As Class_Clasificadores_Riesgo
Dim lRollback As Boolean
Dim lBorrar As hFields
Dim lcClasificador_Riesgo_Codigos As Class_Clasificacion_Riesgo_Cod
Dim lInsertar As hFields
  
  Call Sub_Bloquea_Puntero(Me)
  
  Fnt_Grabar = True
  
  gDB.IniciarTransaccion
  lRollback = False
  
  If Not Fnt_ValidarDatos Then
    lRollback = True
    GoTo ErrProcedure
  End If
  
  Set lClasificador_Riesgo = New Class_Clasificadores_Riesgo
  With lClasificador_Riesgo
    .Campo("ID_CLASIFICADOR_RIESGO").Valor = fKey
    .Campo("DSC_CLASIFICADOR_RIESGO").Valor = txt_desc.Text
    If Not .Guardar Then
      MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      lRollback = True
      GoTo ErrProcedure
    Else
      fKey = .Campo("ID_CLASIFICADOR_RIESGO").Valor
    End If
  End With
  
  Rem borrar los elementos modificados
  For Each lBorrar In fBorrar_Clasificador
    Set lcClasificador_Riesgo_Codigos = New Class_Clasificacion_Riesgo_Cod
    With lcClasificador_Riesgo_Codigos
      .Campo("Id_Clasificador_Riesgo").Valor = fKey
      .Campo("cod_Valor_Clasificacion").Valor = lBorrar("cod_clasificador_riesgo").Value
      If Not .Borrar Then
        MsgBox "Problemas al grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        lRollback = True
        GoTo ErrProcedure
      End If
    End With
    Set lcClasificador_Riesgo_Codigos = Nothing
  Next
  
  If Not Fnt_Graba_Grilla_Clas Then
    lRollback = True
    GoTo ErrProcedure
  End If
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    Fnt_Grabar = False
  Else
    gDB.CommitTransaccion
    MsgBox "Valores de Clasificadores de Riesgo guardados correctamente.", vbInformation, Me.Caption
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Function

Private Function Fnt_Graba_Grilla_Clas() As Boolean
Dim lnombre As String
Dim lValor As String
Dim i As Long
Dim lLinea As Long
Dim lClasificador_Riesgo_Codigo As Class_Clasificacion_Riesgo_Cod

  Fnt_Graba_Grilla_Clas = True
  
  If Grilla.Rows > 0 Then
    For lLinea = 1 To Grilla.Rows - 1
      If GetCell(Grilla, lLinea, "TIPO") = "I" Or _
         GetCell(Grilla, lLinea, "TIPO") = "U" Or _
         GetCell(Grilla, lLinea, "TIPO") = "BI" Then
        Set lClasificador_Riesgo_Codigo = New Class_Clasificacion_Riesgo_Cod
        With lClasificador_Riesgo_Codigo
          .Campo("Id_Clasificador_Riesgo").Valor = fKey
          .Campo("Cod_Valor_Clasificacion").Valor = GetCell(Grilla, lLinea, "Cod_Valor_Clasificacion")
          .Campo("Orden_Riesgo").Valor = lLinea - 1
          If Not .Guardar Then
            MsgBox "Problemas al grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            Fnt_Graba_Grilla_Clas = False
            Exit Function
          End If
        End With
      End If
    Next
  End If
  
End Function

Private Sub Sub_CargaForm()

  Call Sub_FormControl_Color(Me.Controls)
  Grilla.Rows = 1
  
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lClasificador_Riesgo As Class_Clasificadores_Riesgo
Dim lClasificacion_Riesgo_Codigo As Class_Clasificacion_Riesgo_Cod

  Set lClasificador_Riesgo = New Class_Clasificadores_Riesgo
  With lClasificador_Riesgo
    .Campo("ID_CLASIFICADOR_RIESGO").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        txt_desc.Text = NVL(lReg("DSC_CLASIFICADOR_RIESGO").Value, "")
      Next
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With

  Grilla.Rows = 1
  Set lReg = Nothing
  
  Set lClasificacion_Riesgo_Codigo = New Class_Clasificacion_Riesgo_Cod
  With lClasificacion_Riesgo_Codigo
    .Campo("Id_Clasificador_Riesgo").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
         lLinea = Grilla.Rows
         Call Grilla.AddItem("")
         Call SetCell(Grilla, lLinea, "Id_Clasificador_Riesgo", NVL(lReg("Id_Clasificador_Riesgo").Value, ""))
         Call SetCell(Grilla, lLinea, "COD_VALOR_CLASIFICACION", NVL(lReg("Cod_Valor_Clasificacion").Value, ""))
         Call SetCell(Grilla, lLinea, "orden_riesgo", NVL(lReg("orden_riesgo").Value, ""))
         Call SetCell(Grilla, lLinea, "tipo", "")
      Next
    Else
      MsgBox "Problemas en cargar" & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
End Sub

Private Function Fnt_ValidarDatos() As Boolean
Dim lLinea As Long
Dim lValor_Clasificador As String

  Fnt_ValidarDatos = False
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    Exit Function
  End If
  
  For lLinea = 1 To (Grilla.Rows - 1)
    lValor_Clasificador = GetCell(Grilla, lLinea, "COD_VALOR_CLASIFICACION")
    If lValor_Clasificador = "" Then
      MsgBox "Ingrese los Valores de Clasificaciones de Riesgo que est�n en blanco.", vbExclamation + vbOKOnly, Me.Caption
      Exit Function
    ElseIf Len(lValor_Clasificador) > 10 Then
      MsgBox "Valores de Clasificaci�n deben tener m�ximo 10 caracteres.", vbExclamation + vbOKOnly, Me.Caption
      Exit Function
    End If
  Next
  
  Fnt_ValidarDatos = True
End Function

Private Sub Toolbar_Grilla_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lLinea As Double
Dim lOrden_Ultimo_clasif As Double
  
  Me.SetFocus
  DoEvents


   Select Case Button.Key
      Case "ADD"
      
         lLinea = Grilla.Rows
         Grilla.AddItem ""
         Call SetCell(Grilla, lLinea, "id_clasificador_riesgo", fKey)
         Call SetCell(Grilla, lLinea, "TIPO", "I")
         
         lOrden_Ultimo_clasif = lLinea - 1 'GetCell(Grilla, lLinea - 1, "orden_riesgo") 'Comentado por HAF, esta wea estaba haciendo algo de mas, lo comento para no meter mas mano.
         
         Call SetCell(Grilla, lLinea, "orden_riesgo", lOrden_Ultimo_clasif + 1)
        
         Call Grilla.ShowCell(Grilla.Rows - 1, Grilla.ColIndex("id_clasificador_riesgo"))
         
      Case "DEL"
         SetCell Grilla, Grilla.Row, "TIPO", "D"
         Call Sub_Eliminar
   End Select
End Sub

Private Sub Sub_Eliminar()
Dim lID As String
Dim lcClasif_Riesgo As Class_Clasificacion_Riesgo_Cod

  Call Sub_Bloquea_Puntero(Me)
  
  If Grilla.Row > 0 Then
    If MsgBox("�Desea eliminar Clasificador " & GetCell(Grilla, Grilla.Row, "COD_VALOR_CLASIFICACION") & "?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      lID = GetCell(Grilla, Grilla.Row, "COD_VALOR_CLASIFICACION")
      If Not lID = "" Then
        Set lcClasif_Riesgo = New Class_Clasificacion_Riesgo_Cod
        With lcClasif_Riesgo
          .Campo("COD_VALOR_CLASIFICACION").Valor = lID
          If .Borrar Then
            MsgBox "Valor Eliminado.", vbInformation + vbOKOnly, Me.Caption
          Else
            If gDB.Errnum = 440 Then   ' si tienes hijos
              MsgBox "El Valor de Clasificador de Riesgo no puede ser eliminado, ya que est� asociado a otros componentes.", vbCritical, Me.Caption
            Else
              MsgBox gDB.ErrMsg, vbCritical, Me.Caption
            End If
            gDB.Parametros.Clear
          End If
        End With
      End If
      Call Sub_CargarDatos
    End If
  End If
  
   Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Inicio()
Dim lOrder_Arriba As Double
Dim lOrden_Actual As Double

  If Grilla.Row >= 2 Then
    lOrder_Arriba = GetCell(Grilla, Grilla.Row - 1, "orden_riesgo")
    lOrden_Actual = GetCell(Grilla, Grilla.Row, "orden_riesgo")
    
    Call SetCell(Grilla, Grilla.Row - 1, "orden_riesgo", lOrden_Actual)
    Call SetCell(Grilla, Grilla.Row, "orden_riesgo", lOrder_Arriba)
   
    Grilla.RowPosition(Grilla.Row) = Grilla.Row - 1
    Grilla.Row = Grilla.Row - 1
  
    If Not GetCell(Grilla, Grilla.Row, "TIPO") = "BI" Then
      SetCell Grilla, Grilla.Row, "TIPO", "U"
    End If
    
    If Not GetCell(Grilla, Grilla.Row + 1, "TIPO") = "BI" Then
      SetCell Grilla, Grilla.Row + 1, "TIPO", "U"
    End If
      
    Call Grilla.ShowCell(Grilla.Row, 0)
  End If
End Sub

Private Sub Sub_Final()
Dim lOrden_Abajo As Double
Dim lOrden_Actual As Double

  If Grilla.Row <= (Grilla.Rows - 2) Then
    lOrden_Abajo = GetCell(Grilla, Grilla.Row + 1, "orden_riesgo")
    lOrden_Actual = GetCell(Grilla, Grilla.Row, "orden_riesgo")
    
    Call SetCell(Grilla, Grilla.Row + 1, "orden_riesgo", lOrden_Actual)
    Call SetCell(Grilla, Grilla.Row, "orden_riesgo", lOrden_Abajo)
  
    Grilla.RowPosition(Grilla.Row) = Grilla.Row + 1
    Grilla.Row = Grilla.Row + 1
  
    If Not GetCell(Grilla, Grilla.Row, "TIPO") = "BI" Then
      SetCell Grilla, Grilla.Row, "TIPO", "U"
    End If
    
    If Not GetCell(Grilla, Grilla.Row - 1, "TIPO") = "BI" Then
      SetCell Grilla, Grilla.Row - 1, "TIPO", "U"
    End If
  
    Call Grilla.ShowCell(Grilla.Row, 0)
  End If
End Sub
