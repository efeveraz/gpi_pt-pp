VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Consulta_Riesgos_Perfiles 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Riesgos y Perfiles"
   ClientHeight    =   7980
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14850
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7980
   ScaleWidth      =   14850
   Begin VB.Frame Frame5 
      Height          =   700
      Left            =   9400
      TabIndex        =   11
      Top             =   720
      Width           =   5100
      Begin TrueDBList80.TDBCombo Cmb_Perfil 
         Height          =   345
         Left            =   1440
         TabIndex        =   12
         Top             =   240
         Width           =   3525
         _ExtentX        =   6218
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Consulta_Riesgos_Perfiles.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Lbl_Perfil 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Perfil de Riesgo"
         Height          =   345
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   1275
      End
   End
   Begin VB.Frame Frame1 
      Height          =   6195
      Left            =   90
      TabIndex        =   9
      Top             =   1650
      Width           =   14655
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Detalle 
         Height          =   5835
         Left            =   90
         TabIndex        =   10
         Top             =   180
         Width           =   14445
         _cx             =   25479
         _cy             =   10292
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   16777215
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   8454143
         ForeColorSel    =   0
         BackColorBkg    =   16777215
         BackColorAlternate=   16777215
         GridColor       =   16777215
         GridColorFixed  =   16777215
         TreeColor       =   -2147483638
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   0
         FixedRows       =   2
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Consulta_Riesgos_Perfiles.frx":00AA
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filtro de B�squeda"
      ForeColor       =   &H000000FF&
      Height          =   1100
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   14600
      Begin VB.Frame Frame4 
         Height          =   700
         Left            =   3315
         TabIndex        =   7
         Top             =   240
         Width           =   5895
         Begin TrueDBList80.TDBCombo Cmb_Asesor 
            Height          =   345
            Left            =   840
            TabIndex        =   0
            Tag             =   "SOLOLECTURA=N"
            Top             =   240
            Width           =   4935
            _ExtentX        =   8705
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   2
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_Riesgos_Perfiles.frx":00C0
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HFFFFFF&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label lbl_Asesor 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Asesor"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   8
            Top             =   240
            Width           =   705
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Fecha"
         ForeColor       =   &H000000FF&
         Height          =   700
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   3135
         Begin MSComCtl2.DTPicker DTP_Fecha 
            Height          =   345
            Left            =   1290
            TabIndex        =   5
            Top             =   240
            Width           =   1695
            _ExtentX        =   2990
            _ExtentY        =   609
            _Version        =   393216
            CustomFormat    =   "MMMM yyyy"
            Format          =   73203713
            CurrentDate     =   39153
         End
         Begin VB.Label Lbl_Fecha 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Consulta"
            Height          =   345
            Left            =   90
            TabIndex        =   6
            Top             =   240
            Width           =   1215
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   14850
      _ExtentX        =   26194
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "SEARCH"
            Description     =   "Carga Aportes y Retiros de Capital"
            Object.ToolTipText     =   "Carga Saldos Activos"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORTE"
            Description     =   "Genera reporte de Aportes y Retiros de Capital"
            Object.ToolTipText     =   "Genera reporte de Saldos Activos"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Re&frescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpiar los Controles"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   6960
         TabIndex        =   2
         Top             =   0
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Consulta_Riesgos_Perfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso   As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema  As String 'Codigo para el permiso sobre la ventana
'----------------------------------------------------------------------------

Public fApp_Excel As Excel.Application      ' Excel application
Public fLibro     As Excel.Workbook      ' Excel workbook
Public fForm      As Frm_Reporte_Generico

Dim fCursor_Datos   As hRecord
Dim fFilaExcel      As Long
Dim fDecimales      As Integer
Dim vMarginLeft     As Variant
Dim fPage           As Long

Const fId_Moneda = 2

Const clrHeader = &HD0D0D0

Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  
  Call Form_Resize
  
  Load Me
End Sub


Private Sub Form_Load()

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEARCH").Image = cBoton_Buscar
    .Buttons("REPORTE").Image = cBoton_Modificar
    .Buttons("REFRESH").Image = cBoton_Original
    .Buttons("EXIT").Image = cBoton_Salir
  End With

  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
   
End Sub

Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre

  Call Sub_Bloquea_Puntero(Me)
  Call Sub_FormControl_Color(Me.Controls)
  
  Call Sub_CargaCombo_Perfil_Riesgo(Cmb_Perfil, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Perfil, cCmbKBLANCO)
  
  Call Sub_CargaCombo_Asesor(Cmb_Asesor, pTodos:=True)
  Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
  
  Call Sub_SeteaGrilla
  
  Set lCierre = New Class_Verificaciones_Cierre
  DTP_Fecha.Value = lCierre.Busca_Ultima_FechaCierre
  Set lCierre = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_SeteaGrilla()
Dim lcACI As Class_Arbol_Clase_Instrumento
Dim lCursor As hRecord
Dim lReg As hFields
Dim lColumna As Integer
Dim lCol As Integer
  
  With Grilla_Detalle
    .FixedCols = 0
    .ExtendLastCol = False
    .Editable = flexEDNone
    .BackColorAlternate = vbWhite
    Rem Contorno
    .OutlineCol = 0
    .OutlineBar = flexOutlineBarSimpleLeaf
    .MergeCells = flexMergeFixedOnly

    Rem Otras
    .AllowUserResizing = flexResizeColumns
    .AllowSelection = False
    .GridLines = flexGridNone
  End With
  
  Set lcACI = New Class_Arbol_Clase_Instrumento
  With lcACI
    .Campo("ID_EMPRESA").Valor = gId_Empresa
    If .Buscar_Consulta Then
      Set lCursor = .Cursor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
    
  With Grilla_Detalle
    .Clear
    .Cols = 17
    .Cell(flexcpText, 0, 0) = " "
    .Cell(flexcpText, 1, 0) = "Cuenta"
    .Cell(flexcpText, 0, 1) = " "
    .Cell(flexcpText, 1, 1) = "Nombre Corto"
    .Cell(flexcpText, 0, 2) = " "
    .Cell(flexcpText, 1, 2) = "Perfil    "
    .Cell(flexcpText, 0, 3) = "Cuociente "
    .Cell(flexcpText, 1, 3) = "Registrado"
    .Cell(flexcpText, 0, 4) = "Cuociente "
    .Cell(flexcpText, 1, 4) = "Real      "
    .Cell(flexcpText, 0, 5) = "Mto Total "
    .Cell(flexcpText, 1, 5) = "RF/RV     "
    .Cell(flexcpText, 0, 6) = "Cuociente "
    .Cell(flexcpText, 1, 6) = "FFMM"
    .Cell(flexcpText, 0, 7) = "Mto Total"
    .Cell(flexcpText, 1, 7) = "FFMM RF /FFMM RV "
    
    .Cell(flexcpText, 0, 8) = "Rentabilidad Mensual"
    .Cell(flexcpText, 1, 8) = "$$"
    .Cell(flexcpText, 0, 9) = "Rentabilidad Mensual"
    .Cell(flexcpText, 1, 9) = "UF"
    .Cell(flexcpText, 0, 10) = "Rentabilidad Mensual"
    .Cell(flexcpText, 1, 10) = "USD"
    
    .Cell(flexcpText, 0, 11) = "Rentabilidad Anual"
    .Cell(flexcpText, 1, 11) = "$$"
    .Cell(flexcpText, 0, 12) = "Rentabilidad Anual"
    .Cell(flexcpText, 1, 12) = "UF"
    .Cell(flexcpText, 0, 13) = "Rentabilidad Anual"
    .Cell(flexcpText, 1, 13) = "USD"
    
    .Cell(flexcpText, 0, 14) = "Rentabilidad Ult. 12 meses"
    .Cell(flexcpText, 1, 14) = "$$"
    .Cell(flexcpText, 0, 15) = "Rentabilidad Ult. 12 meses"
    .Cell(flexcpText, 1, 15) = "UF"
    .Cell(flexcpText, 0, 16) = "Rentabilidad Ult. 12 meses"
    .Cell(flexcpText, 1, 16) = "USD"
    
    
    .MergeRow(0) = True
    .ColAlignment(0) = flexAlignRightCenter
    For lCol = 2 To .Cols - 1
      .ColAlignment(lCol) = flexAlignCenterCenter
    Next
    .AutoSize 0, .Cols - 1
  End With
  
ErrProcedure:
    
End Sub

Private Sub Grilla_Detalle_Click()
On Error GoTo ManejoDeError


With Grilla_Detalle
  
  If .CellForeColor = RGB(255, 0, 0) Then
    .ForeColorSel = RGB(255, 0, 0)
  Else
    .ForeColorSel = RGB(0, 0, 0)
  End If

End With
  
Exit Sub

ManejoDeError:

Select Case Err.Number
  Case 381
    Resume Next
  
  Case Else
    MsgBox "Error: " & Err.Number & " " & Err.Description
End Select
  
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEARCH"
        Call Sub_GeneraConsultaGrilla
    Case "REFRESH"
      Call Sub_Limpiar
    Case "REPORTE"
      Call Sub_GeneraInformePantalla
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_GeneraInformePantalla
    Case "EXCEL"
      Call Sub_GeneraInformeExcel
  End Select
End Sub

Private Sub Sub_GeneraConsultaGrilla()

  Call Sub_Bloquea_Puntero(Me)
  

  If Fnt_CargarDatos("D") Then
    Call Sub_CargaGrilla_Detalle
  Else
    GoTo ExitProcedure
  End If

  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function Fnt_CargarDatos(pTipoConsulta As String) As Boolean
Dim lcRiesgo_Perfiles  As Class_Riesgo_Perfiles
Dim lId_Asesor        As String
Dim lId_Cuenta        As String
Dim lId_Perfil_Riesgo As String
Dim lCargaExitosa     As Boolean

  lCargaExitosa = False

  lId_Asesor = IIf(Cmb_Asesor.Text = "Todos", "", Fnt_ComboSelected_KEY(Cmb_Asesor))
  lId_Perfil_Riesgo = Fnt_ComboSelected_KEY(Cmb_Perfil)
  lId_Perfil_Riesgo = IIf(lId_Perfil_Riesgo = cCmbKBLANCO, "", lId_Perfil_Riesgo)
  
  lId_Cuenta = ""
  
  Set lcRiesgo_Perfiles = New Class_Riesgo_Perfiles
  With lcRiesgo_Perfiles
    .Campo("FECHA_CIERRE").Valor = DTP_Fecha.Value
    .Campo("ID_CUENTA").Valor = lId_Cuenta
    If .Fnt_Consulta_ACI(Fnt_EmpresaActual, lId_Asesor) Then
      Set fCursor_Datos = .Cursor
      If fCursor_Datos.Count > 0 Then
        lCargaExitosa = True
      Else
        MsgBox "No se registran Saldos Activos en esta fecha.", vbInformation, Me.Caption
      End If
    Else
      MsgBox "Error al cargar datos." & vbCr & vbCr & gDB.ErrMsg, vbInformation, Me.Caption
    End If
  End With
  Set lcRiesgo_Perfiles = Nothing

ExitProcedure:
  Fnt_CargarDatos = lCargaExitosa
End Function

Private Function Fnt_DecimalesMoneda(lId_Moneda) As Integer
Dim lDecimales As Integer
Dim lcMoneda   As Object

  lDecimales = 0
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    .Campo("id_Moneda").Valor = lId_Moneda
    If .Buscar Then
      lDecimales = .Cursor(1)("dicimales_mostrar").Value
    End If
  End With
  Set lcMoneda = Nothing
  Fnt_DecimalesMoneda = lDecimales
  
End Function

Private Sub Sub_CargaGrilla_Detalle()
Dim lReg            As hFields
Dim lCol            As Integer
Dim lId_Cuenta_New  As String
Dim lId_Cuenta_Old  As String
Dim lFila           As Long
Dim lFila2           As Long
'-----------------------------
Dim lTotal          As Double
Dim lhTotales       As hRecord
Dim dTotal_Patrimonio As Double
Dim lMonto          As Double

'-----------------------------
Dim lCursor_2 As hRecord
Dim lReg_2 As hFields
'-----------------------------
  
  Set lhTotales = New hRecord
  With lhTotales
    .AddField "TOTAL", 0
  End With
  
  lId_Cuenta_New = ""
  lId_Cuenta_Old = ""
  
  With Grilla_Detalle
    .Rows = 2
    For Each lReg In fCursor_Datos
      lId_Cuenta_New = lReg("ID_CUENTA").Value

      If Not lId_Cuenta_Old = lId_Cuenta_New Then
        If Not lId_Cuenta_Old = "" Then
          .Cell(flexcpText, lFila, .Cols - 3) = FormatNumber(lTotal, fDecimales)
          .Select lFila, .Cols - 3
          .CellAlignment = flexAlignRightCenter

          lTotal = 0
        End If

        lFila = .Rows
        Call .AddItem("")
        lId_Cuenta_Old = lId_Cuenta_New

        '.c= RGB(255, 0, 0)
        If CDbl(lReg("PORCEN_RV").Value) < CDbl((FormatPorcentaje(lReg("mto_total_rv").Value, lReg("mto_total_activos").Value))) Then
          .Cell(flexcpForeColor, lFila, 0, lFila, 16) = RGB(255, 0, 0)
        End If

        .Cell(flexcpText, lFila, 0) = lReg("NUM_CUENTA").Value
        .Cell(flexcpText, lFila, 1) = lReg("DSC_CUENTA").Value
        .Cell(flexcpText, lFila, 2) = lReg("DSC_PERFIL_RIESGO").Value
        .Cell(flexcpText, lFila, 3) = lReg("PORCEN_RF").Value & " / " & lReg("PORCEN_RV").Value
        .Cell(flexcpText, lFila, 4) = FormatPorcentaje(lReg("mto_total_rf").Value, lReg("mto_total_activos").Value) & " / " & FormatPorcentaje(lReg("mto_total_rv").Value, lReg("mto_total_activos").Value)
        .Cell(flexcpText, lFila, 5) = FormatNumber(lReg("mto_total_rf").Value, 0) & " / " & FormatNumber(lReg("mto_total_rv").Value, 0)
        .Cell(flexcpText, lFila, 6) = lReg("cuociente_ff_mm").Value
        .Cell(flexcpText, lFila, 7) = FormatNumber(lReg("mto_total_ffmm_rf").Value, 0) & " / " & FormatNumber(lReg("mto_total_ffmm_rv").Value, 0)
        
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Pid_cuenta", ePT_Numero, lId_Cuenta_New, ePD_Entrada
        gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, DTP_Fecha.Value, ePD_Entrada
        gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS$RENTABILIDADES"
        
        If Not gDB.EjecutaSP Then
            Exit Sub
        End If
    
        Set lCursor_2 = gDB.Parametros("Pcursor").Valor
        gDB.Parametros.Clear
    
        For Each lReg_2 In lCursor_2
                
          .Cell(flexcpText, lFila, 8) = FormatNumber(lReg_2("rentabilidad_mensual_$$").Value, 2)
          .Cell(flexcpText, lFila, 9) = FormatNumber(lReg_2("rentabilidad_mensual_uf").Value, 2)
          .Cell(flexcpText, lFila, 10) = FormatNumber(lReg_2("rentabilidad_mensual_DO").Value, 2)
          
          .Cell(flexcpText, lFila, 11) = FormatNumber(lReg_2("rentabilidad_anual_$$").Value, 2)
          .Cell(flexcpText, lFila, 12) = FormatNumber(lReg_2("rentabilidad_anual_uf").Value, 2)
          .Cell(flexcpText, lFila, 13) = FormatNumber(lReg_2("rentabilidad_ult_12_meses_uf").Value, 2)
          
          .Cell(flexcpText, lFila, 14) = FormatNumber(lReg_2("rentabilidad_ult_12_meses_$$").Value, 2)
          .Cell(flexcpText, lFila, 15) = FormatNumber(lReg_2("rentabilidad_anual_DO").Value, 2)
          .Cell(flexcpText, lFila, 16) = FormatNumber(lReg_2("rentabilidad_ult_12_meses_DO").Value, 2)
                
        Next
        
        Set lCursor_2 = Nothing
        Set lReg_2 = Nothing
       
      End If

    Next

    Rem Autoajusta las columnas y deselecciona filas
    .AutoSize 0, .Cols - 1
    If .Row > 0 Then .IsSelected(.Row) = False
  End With
    

    
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Cmb_Asesor_ItemChange()
Dim lId_Asesor As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Cmb_Asesor <> "Todos" Then
    lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Limpiar()
Dim lCierre As Class_Verificaciones_Cierre

    Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
    
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha.Value = lCierre.Busca_Ultima_FechaCierre
    Set lCierre = Nothing
    
    Grilla_Detalle.Rows = 2

End Sub

Private Sub Sub_GeneraInformeExcel()
    
  Call Sub_Bloquea_Puntero(Me)
  
  If Fnt_CargarDatos("D") Then
    Call Sub_CreaExcel
    Call Sub_CargaExcel_Detalle
  Else
    GoTo ExitProcedure
  End If
  
  fApp_Excel.Visible = True
  fApp_Excel.UserControl = True
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CreaExcel()
Dim lHoja           As Integer
Dim lIndex          As Integer
Dim lTitulo_Detalle As String
Dim lTitulo_Fecha   As String
Dim lItem_Detalle   As String

Const cTama�o_Titulo = 12
Const cTama�o_Fecha = 11

  Call Sub_Bloquea_Puntero(Me)
    
  lHoja = 1
  
  
  Set fApp_Excel = CreateObject("Excel.application")
  fApp_Excel.DisplayAlerts = False
  Set fLibro = fApp_Excel.Workbooks.Add
  
  fApp_Excel.ActiveWindow.DisplayGridlines = False
  fLibro.Worksheets.Add
  
  With fLibro
    For lIndex = .Worksheets.Count To lHoja + 1 Step -1
      .Worksheets(lIndex).Delete
    Next lIndex
      
    For lIndex = 1 To lHoja
      .Sheets(lIndex).Select
      fApp_Excel.ActiveWindow.DisplayGridlines = False
      .ActiveSheet.Range("A5:E6").Font.Bold = True
      .ActiveSheet.Range("A5:E6").HorizontalAlignment = xlLeft
    Next
    lTitulo_Detalle = "Detalle Saldos Activos"
    lTitulo_Fecha = "Fecha Consulta: " & DTP_Fecha.Value
    lItem_Detalle = "Detalle"
            
    .Sheets(1).Select
    .ActiveSheet.Range("A5").Value = lTitulo_Detalle
    .ActiveSheet.Range("A5").Font.Size = cTama�o_Titulo
    .ActiveSheet.Range("A6").Value = lTitulo_Fecha
    .ActiveSheet.Range("A6").Font.Size = cTama�o_Fecha
    .Worksheets.Item(1).Name = lItem_Detalle
            
  End With
    
End Sub

Private Sub Sub_CargaExcel_Detalle()
Dim lxHoja          As Worksheet
Dim lcChart         As Excel.Chart
Dim lId_Cuenta_New  As String
Dim lId_Cuenta_Old  As String
Dim lCol_Grilla     As Integer
Dim lReg            As hFields
'-----------------------------
Dim lTotal          As Double
Dim lCol            As Integer
Dim lFila           As Long
Dim lMonto          As Double
Dim lIndex          As Integer
'--------------------------------
Dim lCursor_2 As hRecord
Dim lReg_2 As hFields
'-----------------------------
  fFilaExcel = 8
  
  Call Sub_ExcelEncabezado(Grilla_Detalle, 1)
  Set lxHoja = fLibro.Sheets(1)
  Call lxHoja.Select
  
  lId_Cuenta_New = ""
  lId_Cuenta_Old = ""
  
  With lxHoja
    For Each lReg In fCursor_Datos
      lId_Cuenta_New = lReg("ID_CUENTA").Value
      
      If Not lId_Cuenta_Old = lId_Cuenta_New Then
        If Not lId_Cuenta_Old = "" Then
          .Cells(fFilaExcel, Grilla_Detalle.Cols - 2) = lTotal
          .Cells(fFilaExcel, Grilla_Detalle.Cols - 2).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
          lTotal = 0
        End If
        
        fFilaExcel = fFilaExcel + 1
        lId_Cuenta_Old = lId_Cuenta_New
        
        If CDbl(lReg("PORCEN_RV").Value) < CDbl((FormatPorcentaje(lReg("mto_total_rv").Value, lReg("mto_total_activos").Value))) Then
          .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 17)).Font.Bold = True
          .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 17)).Interior.Color = RGB(220, 220, 220)

        End If


        
        .Cells(fFilaExcel, 1) = lReg("NUM_CUENTA").Value
        .Cells(fFilaExcel, 2) = lReg("DSC_CUENTA").Value
        .Cells(fFilaExcel, 3) = lReg("DSC_PERFIL_RIESGO").Value
        .Cells(fFilaExcel, 4) = lReg("PORCEN_RF").Value & " / " & lReg("PORCEN_RV").Value
        .Cells(fFilaExcel, 5) = FormatPorcentaje(lReg("mto_total_rf").Value, lReg("mto_total_activos").Value) & " / " & FormatPorcentaje(lReg("mto_total_rv").Value, lReg("mto_total_activos").Value)
        .Cells(fFilaExcel, 6) = FormatNumber(lReg("mto_total_rf").Value, 0) & " / " & FormatNumber(lReg("mto_total_rv").Value, 0)
        .Cells(fFilaExcel, 7) = lReg("cuociente_ff_mm").Value
        .Cells(fFilaExcel, 8) = FormatNumber(lReg("mto_total_ffmm_rf").Value, 0) & " / " & FormatNumber(lReg("mto_total_ffmm_rv").Value, 0)
        
        
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Pid_cuenta", ePT_Numero, lId_Cuenta_New, ePD_Entrada
        gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, DTP_Fecha.Value, ePD_Entrada
        gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS$RENTABILIDADES"
        
        If Not gDB.EjecutaSP Then
            Exit Sub
        End If
    
        Set lCursor_2 = gDB.Parametros("Pcursor").Valor
        gDB.Parametros.Clear
    
        For Each lReg_2 In lCursor_2
                
          .Cells(fFilaExcel, 9) = FormatNumber(lReg_2("rentabilidad_mensual_$$").Value, 2)
          .Cells(fFilaExcel, 10) = FormatNumber(lReg_2("rentabilidad_mensual_uf").Value, 2)
          .Cells(fFilaExcel, 11) = FormatNumber(lReg_2("rentabilidad_mensual_DO").Value, 2)
          
          .Cells(fFilaExcel, 12) = FormatNumber(lReg_2("rentabilidad_anual_$$").Value, 2)
          .Cells(fFilaExcel, 13) = FormatNumber(lReg_2("rentabilidad_anual_uf").Value, 2)
          .Cells(fFilaExcel, 14) = FormatNumber(lReg_2("rentabilidad_anual_DO").Value, 2)
          
          .Cells(fFilaExcel, 15) = FormatNumber(lReg_2("rentabilidad_ult_12_meses_$$").Value, 2)
          .Cells(fFilaExcel, 16) = FormatNumber(lReg_2("rentabilidad_ult_12_meses_uf").Value, 2)
          .Cells(fFilaExcel, 17) = FormatNumber(lReg_2("rentabilidad_ult_12_meses_DO").Value, 2)
                
        Next
        
        Set lCursor_2 = Nothing
        Set lReg_2 = Nothing
        
        
        
        
      End If
      
    Next
    
  End With
  
  fLibro.ActiveSheet.Columns("A:A").Select
  fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
  fApp_Excel.Selection.EntireColumn.AutoFit
      
End Sub

Private Sub Sub_Configuracion_Grafico()
  Rem Tama�o grafico
  fLibro.ActiveSheet.ChartObjects("Gr�fico 1").Activate
  fLibro.ActiveChart.ChartArea.Select
  fLibro.ActiveSheet.Shapes("Gr�fico 1").ScaleWidth 1.1, 0, 2
  fLibro.ActiveSheet.Shapes("Gr�fico 1").ScaleWidth 1.12, 0, 0
  Rem Tama�o,color y lineas del grafico torta
  With fLibro.ActiveChart.PlotArea
    .Left = 30
    .Top = 106
    .Width = 361
    .Height = 146
    .Border.ColorIndex = 2
    .Border.Weight = xlThin
    .Border.LineStyle = xlContinuous
    .Interior.ColorIndex = 2
    .Interior.PatternColorIndex = 1
    .Interior.Pattern = xlSolid
  End With
  
  Rem fuente porcentajes
  fLibro.ActiveChart.SeriesCollection(1).DataLabels.AutoScaleFont = True
  With fLibro.ActiveChart.SeriesCollection(1).DataLabels.Font
    .Name = "Arial"
    .FontStyle = "Normal"
    .Size = 10
    .Strikethrough = False
    .Superscript = False
    .Subscript = False
    .OutlineFont = False
    .Shadow = False
    .Underline = xlUnderlineStyleNone
    .ColorIndex = xlAutomatic
    .Background = xlAutomatic
  End With
  
  fLibro.ActiveSheet.Range("A1").Select
End Sub

Private Sub Sub_ExcelEncabezado(pGrilla As VSFlexGrid, pHoja As Integer)
Dim lxHoja      As Worksheet
Dim lCol        As Integer
Dim lCol_Grilla As Integer
Dim lCol_Desde  As Integer
Dim lTitulo     As String
Dim lhRecord    As hRecord
Dim lReg        As hFields
Dim lReg_C      As hFields

  Set lhRecord = New hRecord
  With lhRecord
    .AddField "COL_DESDE"
    .AddField "COL_HASTA"
  End With

  Set lxHoja = fLibro.Sheets(pHoja)
  Call lxHoja.Select

  If pGrilla.Name = "Grilla_Detalle" Then
    lCol_Desde = 4 '3

  End If

  lCol = 1
  With lxHoja
    Rem Fila Titulos Clase Instrumentos
    For lCol_Grilla = 0 To pGrilla.Cols - 1 ' - 3
      .Cells(fFilaExcel, lCol) = pGrilla.Cell(flexcpText, 0, lCol_Grilla)
      lCol = lCol + 1
    Next
    
    lCol = lCol_Desde
    lTitulo = .Cells(fFilaExcel, lCol)
    Set lReg = lhRecord.Add
    lReg("COL_DESDE").Value = lCol
    
    For lCol = lCol_Desde + 1 To pGrilla.Cols - 1
      If lTitulo = .Cells(fFilaExcel, lCol) Then
        .Cells(fFilaExcel, lCol) = ""
      Else
        lTitulo = .Cells(fFilaExcel, lCol)
        lReg("COL_HASTA").Value = lCol - 1
        Set lReg = lhRecord.Add
        lReg("COL_DESDE").Value = lCol
      End If
    Next
    lReg("COL_HASTA").Value = pGrilla.Cols
    
    For Each lReg_C In lhRecord
      .Range(.Cells(fFilaExcel, lReg_C("COL_DESDE").Value), .Cells(fFilaExcel, lReg_C("COL_HASTA").Value)).MergeCells = True
    Next
    'lCol = 15
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel, lCol)).BorderAround
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel, lCol)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel, lCol)).Interior.Color = RGB(255, 255, 0)   '&HC0FFC0
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel, lCol)).Font.Bold = True
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel, lCol)).HorizontalAlignment = xlCenter
    
    Rem Fila Titulos SubClase Instrumentos
    fFilaExcel = fFilaExcel + 1
    lCol = 1
    For lCol_Grilla = 0 To pGrilla.Cols - 1
      .Cells(fFilaExcel, lCol).Value = pGrilla.Cell(flexcpText, 1, lCol_Grilla)
      lCol = lCol + 1
    Next
    
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, lCol - 1)).BorderAround
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, lCol - 1)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, lCol - 1)).Interior.Color = RGB(255, 255, 0)  '&HC0FFC0
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, lCol - 1)).Font.Bold = True
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, lCol - 1)).HorizontalAlignment = xlCenter
  End With
    
End Sub

Private Sub Sub_GeneraInformePantalla()

  Call Sub_Bloquea_Puntero(Me)
  
  fPage = 1
          
  If Fnt_CargarDatos("D") Then
    Call Sub_Inicia_Documento
    Call Sub_CargaInforme_Detalle
  Else
    GoTo ExitProcedure
  End If
  
  fForm.VsPrinter.EndDoc
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Inicia_Documento()
Dim lTitulo As String
        
  lTitulo = "Consulta de Riesgos y Perfiles"
  
  Set fForm = New Frm_Reporte_Generico
  Call fForm.Sub_InicarDocumento(pTitulo:="" _
                               , pTipoSalida:=ePrinter.eP_pantalla _
                               , pOrientacion:=orLandscape)
  vMarginLeft = fForm.VsPrinter.MarginLeft
End Sub

Private Sub Sub_EncabezadoDetalle()
Dim lTitulo         As String
Dim lMultiplo       As Integer
Dim lFila           As Long
Dim lColRep         As Long
Dim lHasta          As Long
Dim lCol            As Long

    With fForm.VsPrinter
        .FontSize = 14
        .FontBold = True
        .MarginLeft = "5mm"
        If fPage > 1 Then
           .Paragraph = ""
        End If
        .Paragraph = "Consulta de Riesgos y Perfiles"
        .FontSize = 10
        .Paragraph = "Fecha Consulta: " & DTP_Fecha.Value
        
        .StartTable
        .MarginLeft = "5mm"
        .FontSize = 6
        .TableCell(tcAlignCurrency) = False
        
        

        
        lMultiplo = 1

        .TableCell(tcRows) = 1
        .TableCell(tcCols) = 8
        


         .TableCell(tcColWidth, 1, 1) = "57mm"
         .TableCell(tcColWidth, 1, 2) = "20mm"
         .TableCell(tcColWidth, 1, 3) = "28mm"
         .TableCell(tcColWidth, 1, 4) = "13mm"
         .TableCell(tcColWidth, 1, 5) = "28mm"
         .TableCell(tcColWidth, 1, 6) = "42mm"
         .TableCell(tcColWidth, 1, 7) = "42mm"
         .TableCell(tcColWidth, 1, 8) = "42mm"

        .TableCell(tcText, 1, 1) = " "
        .TableCell(tcText, 1, 2) = "Cuociente"
        .TableCell(tcText, 1, 3) = "Mto Total"
        .TableCell(tcText, 1, 4) = "Cuociente"
        .TableCell(tcText, 1, 5) = "Mto Total"
        .TableCell(tcText, 1, 6) = "Rentabilidad Mensual"
        .TableCell(tcText, 1, 7) = "Rentabilidad Anual"
        .TableCell(tcText, 1, 8) = "Rentabilidad Ult. 12 Meses"
        
        .TableCell(tcAlign, 1, 1, 1, 8) = taCenterMiddle
        .TableCell(tcFontBold, 1, 1, 1, 8) = True
        .TableCell(tcBackColor, 1, 1, 1, 8) = RGB(240, 240, 240)
        .EndTable
        
        
        .StartTable
        .MarginLeft = "5mm"
        .TableCell(tcRows) = 1

        .TableCell(tcCols) = 17

        
        .TableCell(tcColWidth, 1, 1) = "11mm"
        .TableCell(tcColWidth, 1, 2) = "30mm"
        .TableCell(tcColWidth, 1, 3) = "16mm"
        .TableCell(tcColWidth, 1, 4) = "10mm"
        .TableCell(tcColWidth, 1, 5) = "10mm"
        .TableCell(tcColWidth, 1, 6) = "28mm"
        .TableCell(tcColWidth, 1, 7) = "13mm"
        .TableCell(tcColWidth, 1, 8) = "28mm"
        .TableCell(tcColWidth, 1, 9) = "14mm"
        .TableCell(tcColWidth, 1, 10) = "14mm"
        .TableCell(tcColWidth, 1, 11) = "14mm"
        .TableCell(tcColWidth, 1, 12) = "14mm"
        .TableCell(tcColWidth, 1, 13) = "14mm"
        .TableCell(tcColWidth, 1, 14) = "14mm"
        .TableCell(tcColWidth, 1, 15) = "14mm"
        .TableCell(tcColWidth, 1, 16) = "14mm"
        .TableCell(tcColWidth, 1, 17) = "14mm"
        
        .TableCell(tcText, 1, 1) = "Cuenta"
        .TableCell(tcText, 1, 2) = "Nombre Corto"
        .TableCell(tcText, 1, 3) = "Perfil"
        .TableCell(tcText, 1, 4) = "Reg."
        .TableCell(tcText, 1, 5) = "Real"
        .TableCell(tcText, 1, 6) = "RF/RV"
        .TableCell(tcText, 1, 7) = "FRMM"
        .TableCell(tcText, 1, 8) = "FRMM (RF/ RV)"
        .TableCell(tcText, 1, 9) = "$$"
        .TableCell(tcText, 1, 10) = "UF"
        .TableCell(tcText, 1, 11) = "USD"
        .TableCell(tcText, 1, 12) = "$$"
        .TableCell(tcText, 1, 13) = "UF"
        .TableCell(tcText, 1, 14) = "USD"
        .TableCell(tcText, 1, 15) = "$$"
        .TableCell(tcText, 1, 16) = "UF"
        .TableCell(tcText, 1, 17) = "USD"
        
        
        
        
        

        
        
        .TableCell(tcAlign, 1, 1, 1, 17) = taCenterMiddle
        .TableCell(tcFontBold, 1, 1, 1, 17) = True
        .TableCell(tcBackColor, 1, 1, 1, 17) = RGB(240, 240, 240)
        .EndTable
        .StartTable
        .MarginLeft = "5mm"
        .TableCell(tcRows) = 0
        .TableCell(tcCols) = Grilla_Detalle.Cols
        
        lFila = .TableCell(tcRows)
    
    End With
End Sub

Private Sub Sub_CargaInforme_Detalle()
Dim sHeader_Det     As String
Dim sFormat_Det     As String
Dim sRecord
Dim sRecord_Total
Dim lReg            As hFields
Dim lTitulo_Fecha   As String
Dim lId_Cuenta_New  As String
Dim lId_Cuenta_Old  As String
Dim lCol            As Integer
Dim lCol_Actual     As Integer
Dim lPromedio       As String
Dim lTotal          As Double
Dim bAppend
Dim lTitulo         As String
Dim lMultiplo       As Integer
Dim lhMontos        As hRecord
Dim lReg_Montos     As hFields
Dim lMonto          As String
Dim lPatrimonio     As String
Dim lFila           As Long
Dim lColRep         As Long
Dim lDesde          As Long
Dim lHasta          As Long
Dim lLineas         As Long
'------------------------------
Dim lCursor_2 As hRecord
Dim lReg_2 As hFields

Const cAncho = 1200
  
  Set lhMontos = New hRecord
  
  With fForm.VsPrinter
    .MarginLeft = "5mm"
    
    .FontBold = False

    lLineas = 1
    Call Sub_EncabezadoDetalle
    lId_Cuenta_New = ""
    lId_Cuenta_Old = ""
    lColRep = 1
    For Each lReg In fCursor_Datos
      
        lId_Cuenta_New = lReg("ID_CUENTA").Value
        
        If Not lId_Cuenta_Old = lId_Cuenta_New Then
          
          lId_Cuenta_Old = lId_Cuenta_New
          lCol_Actual = 2
            
          lLineas = lLineas + 1
          If lLineas > 28 Then
            .EndTable
            fForm.VsPrinter.NewPage
            fPage = fPage + 1
            Call Sub_EncabezadoDetalle
            lLineas = 1
                
          End If
            
          .TableCell(tcRows) = .TableCell(tcRows) + 1
          lFila = .TableCell(tcRows)
            
          .TableCell(tcAlign, lFila, 1, lFila, 1) = taLeftMiddle
          .TableCell(tcAlign, lFila, 2, lFila, 2) = taLeftMiddle
          .TableCell(tcAlign, lFila, 3, lFila, 3) = taLeftMiddle
          .TableCell(tcAlign, lFila, 4, lFila, 17) = taCenterMiddle

           
          .TableCell(tcColWidth, lFila, 1, lFila, 1) = "11mm"
          .TableCell(tcColWidth, lFila, 2, lFila, 2) = "30mm"
          .TableCell(tcColWidth, lFila, 3, lFila, 3) = "16mm"
          .TableCell(tcColWidth, lFila, 4, lFila, 4) = "10mm"
          .TableCell(tcColWidth, lFila, 5, lFila, 5) = "10mm"
          .TableCell(tcColWidth, lFila, 6, lFila, 6) = "28mm"
          .TableCell(tcColWidth, lFila, 7, lFila, 7) = "13mm"
          .TableCell(tcColWidth, lFila, 8, lFila, 8) = "28mm"
          .TableCell(tcColWidth, lFila, 9, lFila, 9) = "14mm"
          .TableCell(tcColWidth, lFila, 10, lFila, 10) = "14mm"
          .TableCell(tcColWidth, lFila, 11, lFila, 11) = "14mm"
          .TableCell(tcColWidth, lFila, 12, lFila, 12) = "14mm"
          .TableCell(tcColWidth, lFila, 13, lFila, 13) = "14mm"
          .TableCell(tcColWidth, lFila, 14, lFila, 14) = "14mm"
          .TableCell(tcColWidth, lFila, 15, lFila, 15) = "14mm"
          .TableCell(tcColWidth, lFila, 16, lFila, 16) = "14mm"
          .TableCell(tcColWidth, lFila, 17, lFila, 17) = "14mm"
          
          
          If CDbl(lReg("PORCEN_RV").Value) < CDbl((FormatPorcentaje(lReg("mto_total_rv").Value, lReg("mto_total_activos").Value))) Then
              .TableCell(tcFontBold, lFila, 1, lFila, 17) = True
              .TableCell(tcBackColor, lFila, 1, lFila, 17) = RGB(240, 240, 240)
              '.TableCell(tcForeColor, lFila, 1, lFila, 17) = RGB(255, 0, 0)
              
          End If
            
          .TableCell(tcText, lFila, 1) = lReg("NUM_CUENTA").Value
          .TableCell(tcText, lFila, 2) = lReg("DSC_CUENTA").Value
          .TableCell(tcText, lFila, 3) = lReg("DSC_PERFIL_RIESGO").Value
          .TableCell(tcText, lFila, 4) = lReg("PORCEN_RF").Value & "/" & lReg("PORCEN_RV").Value
          .TableCell(tcText, lFila, 5) = FormatPorcentaje(lReg("mto_total_rf").Value, lReg("mto_total_activos").Value) & "/" & FormatPorcentaje(lReg("mto_total_rv").Value, lReg("mto_total_activos").Value)
          .TableCell(tcText, lFila, 6) = FormatNumber(lReg("mto_total_rf").Value, 0) & "/" & FormatNumber(lReg("mto_total_rv").Value, 0)
          .TableCell(tcText, lFila, 7) = lReg("cuociente_ff_mm").Value
          .TableCell(tcText, lFila, 8) = FormatNumber(lReg("mto_total_ffmm_rf").Value, 0) & "/" & FormatNumber(lReg("mto_total_ffmm_rv").Value, 0)
            
          gDB.Parametros.Clear
          gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
          gDB.Parametros.Add "Pid_cuenta", ePT_Numero, lId_Cuenta_New, ePD_Entrada
          gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, DTP_Fecha.Value, ePD_Entrada
          gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS$RENTABILIDADES"
        
          If Not gDB.EjecutaSP Then
            Exit Sub
          End If
    
          Set lCursor_2 = gDB.Parametros("Pcursor").Valor
          gDB.Parametros.Clear
    
          For Each lReg_2 In lCursor_2
            
            .TableCell(tcText, lFila, 9) = FormatNumber(lReg_2("rentabilidad_mensual_$$").Value, 2)
            .TableCell(tcText, lFila, 10) = FormatNumber(lReg_2("rentabilidad_mensual_uf").Value, 2)
            .TableCell(tcText, lFila, 11) = FormatNumber(lReg_2("rentabilidad_mensual_do").Value, 2)
            
            .TableCell(tcText, lFila, 12) = FormatNumber(lReg_2("rentabilidad_anual_$$").Value, 2)
            .TableCell(tcText, lFila, 13) = FormatNumber(lReg_2("rentabilidad_anual_uf").Value, 2)
            .TableCell(tcText, lFila, 14) = FormatNumber(lReg_2("rentabilidad_anual_do").Value, 2)
            
            .TableCell(tcText, lFila, 15) = FormatNumber(lReg_2("rentabilidad_ult_12_meses_$$").Value, 2)
            .TableCell(tcText, lFila, 16) = FormatNumber(lReg_2("rentabilidad_ult_12_meses_uf").Value, 2)
            .TableCell(tcText, lFila, 17) = FormatNumber(lReg_2("rentabilidad_ult_12_meses_do").Value, 2)
            
            
          Next
            
          Set lCursor_2 = Nothing
          Set lReg_2 = Nothing
        End If
      Next
   
    
    .TableCell(tcAlign, lFila, 1, lFila, 1) = taLeftMiddle
    .TableCell(tcAlign, lFila, 2, lFila, 2) = taLeftMiddle
    .TableCell(tcAlign, lFila, 3, lFila, 3) = taLeftMiddle
    .TableCell(tcAlign, lFila, 4, lFila, 4) = taCenterMiddle
    .TableCell(tcAlign, lFila, 5, lFila, 5) = taCenterMiddle
    .TableCell(tcAlign, lFila, 6, lFila, 6) = taCenterMiddle
    .TableCell(tcAlign, lFila, 7, lFila, 7) = taCenterMiddle
    .TableCell(tcAlign, lFila, 8, lFila, 8) = taCenterMiddle
    .TableCell(tcAlign, lFila, 9, lFila, 9) = taCenterMiddle
    .TableCell(tcAlign, lFila, 10, lFila, 10) = taCenterMiddle
    .TableCell(tcAlign, lFila, 11, lFila, 11) = taCenterMiddle
    .TableCell(tcAlign, lFila, 12, lFila, 12) = taCenterMiddle
    .TableCell(tcAlign, lFila, 13, lFila, 13) = taCenterMiddle
    .TableCell(tcAlign, lFila, 14, lFila, 14) = taCenterMiddle
    .TableCell(tcAlign, lFila, 15, lFila, 15) = taCenterMiddle
    .TableCell(tcAlign, lFila, 16, lFila, 16) = taCenterMiddle
    .TableCell(tcAlign, lFila, 17, lFila, 17) = taCenterMiddle
    
    .TableCell(tcColWidth, lFila, 1, lFila, 1) = "11mm"
    .TableCell(tcColWidth, lFila, 2, lFila, 2) = "30mm"
    .TableCell(tcColWidth, lFila, 3, lFila, 3) = "16mm"
    .TableCell(tcColWidth, lFila, 4, lFila, 4) = "10mm"
    .TableCell(tcColWidth, lFila, 5, lFila, 5) = "10mm"
    .TableCell(tcColWidth, lFila, 6, lFila, 6) = "28mm"
    .TableCell(tcColWidth, lFila, 7, lFila, 7) = "13mm"
    .TableCell(tcColWidth, lFila, 8, lFila, 8) = "28mm"
    .TableCell(tcColWidth, lFila, 9, lFila, 9) = "14mm"
    .TableCell(tcColWidth, lFila, 10, lFila, 10) = "14mm"
    .TableCell(tcColWidth, lFila, 11, lFila, 11) = "14mm"
    .TableCell(tcColWidth, lFila, 12, lFila, 12) = "14mm"
    .TableCell(tcColWidth, lFila, 13, lFila, 13) = "14mm"
    .TableCell(tcColWidth, lFila, 14, lFila, 14) = "14mm"
    .TableCell(tcColWidth, lFila, 15, lFila, 15) = "14mm"
    .TableCell(tcColWidth, lFila, 16, lFila, 16) = "14mm"
    .TableCell(tcColWidth, lFila, 17, lFila, 17) = "14mm"
        
    .TableCell(tcFontSize, lFila, 1, lFila, 17) = 6
    .EndTable
  End With
  fForm.VsPrinter.MarginLeft = vMarginLeft
End Sub

Private Sub Sub_Carga_Registro(pCol_Actual As Integer _
                             , pGrilla As VSFlexGrid _
                             , pRecord _
                             , pFormat_Det _
                             , pHeader_Det _
                             , pPatrimonio As String _
                             , pPromedio As String _
                             , pTotal _
                             , ByRef pGrid As VsPrinter)
    
Dim lCol As Integer
Dim bAppend

  For lCol = pCol_Actual To pGrilla.Cols - 1
    Select Case lCol
      Case pGrilla.Cols - 3
        pRecord = pRecord & pPatrimonio & "|"
      Case pGrilla.Cols - 2
        pRecord = pRecord & FormatNumber(pTotal, fDecimales) & "|"
      Case pGrilla.Cols - 1
        pRecord = pRecord & pPromedio
      Case Else
        pRecord = pRecord & "|"
    End Select
  Next
  pGrid.AddTable pFormat_Det, pHeader_Det, pRecord, clrHeader, , bAppend
  
End Sub

Private Function FormatPorcentaje(Valor As Double, Valor_Total As Double, Optional ByRef nValorAsignado As Double = 0) As String
    If Valor_Total <> 0 Then
        FormatPorcentaje = FormatNumber((Valor / Valor_Total) * 100, 0)
        nValorAsignado = (Valor / Valor_Total) * 100
    Else
        FormatPorcentaje = FormatNumber(0, 0)
        nValorAsignado = 0
    End If
End Function
