VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Aporte_Retiro_Capital 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Comprobante de Movimiento de Capital"
   ClientHeight    =   1680
   ClientLeft      =   165
   ClientTop       =   330
   ClientWidth     =   7035
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1680
   ScaleWidth      =   7035
   Begin VB.Frame Frame1 
      Caption         =   "Generador de Comprobante de Movimiento de Capital"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1185
      Left            =   30
      TabIndex        =   2
      Top             =   390
      Width           =   6945
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   5160
         TabIndex        =   3
         Top             =   270
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2858
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Reporte"
               Key             =   "REPORTE"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               Object.ToolTipText     =   "Genera el Reporte de Cargos y Abonos"
            EndProperty
         EndProperty
      End
      Begin TrueDBList80.TDBCombo Cmb_Operaciones 
         Height          =   345
         Left            =   1200
         TabIndex        =   4
         Tag             =   "OBLI=S;CAPTION=Operaciones"
         Top             =   720
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Aporte_Retiro_Capital.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   345
         Left            =   1200
         TabIndex        =   5
         Tag             =   "OBLI=S;CAPTION=Cuenta"
         Top             =   330
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Aporte_Retiro_Capital.frx":00AA
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Movimiento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   7
         Top             =   720
         Width           =   1035
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuentas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   6
         Top             =   330
         Width           =   1035
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7035
      _ExtentX        =   12409
      _ExtentY        =   635
      ButtonWidth     =   1429
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Aporte_Retiro_Capital"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Type datos_cliente
    Cuenta As String
    Nombre As String
    Rut As String
    Ejecutivo As String
    Fecha_Operacion As Date
End Type



Dim glb_cliente As datos_cliente
Dim Iid_Cuenta As String
Dim IId_Operacion As String

Private Sub Cmb_Cuentas_ItemChange()
Dim lId_Cuenta As String
'Dim Num_Cuenta As Long  -- Modificado CSM 23/06/2006 --
   
  lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
   
  If Not lId_Cuenta = cCmbKALL Then
    Call Sub_CargaCombo_Operaciones(Cmb_Operaciones, lId_Cuenta) ', Mid(pCombo.Text, 1, InStr(pCombo.Text, "-") - 1))  -- Modificado CSM 23/06/2006 --
  Else
    'Call Sub_CargaCombo_Operaciones(Cmb_Operaciones)
  End If
End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      
  End With
  
  With Toolbar_Verificacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORTE").Image = cBoton_Modificar
  End With
  
  Call Carga_Combo
     
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Carga_Combo()
  Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas)
  'Call Sub_CargaCombo_Operaciones(Cmb_Operaciones) 'Modificado por MMA. POr su demora es mejor cargar cuando ya haya seleccionado cuenta
  'Call Sub_CargaCombo_Operaciones(Cmb_Operaciones)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "REPORTE"
      Call Sub_Generar_Comprobante
  End Select
End Sub

Private Sub Sub_Generar_Comprobante()



Const clrHeader = &HD0D0D0
Dim sRecord
Dim bAppend
Dim lLinea As Integer
Dim lForm As Frm_Reporte_Generico
Dim lReg As hCollection.hFields
Dim a As Integer
Dim iFila As Integer
Dim Filas As Integer
Dim lcOperaciones  As Class_Operaciones

IId_Operacion = IIf(Fnt_ComboSelected_KEY(Cmb_Operaciones) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Operaciones))
Iid_Cuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Cuentas))


Call Trae_Antecedentes_Generales(Iid_Cuenta, IId_Operacion)
 ' Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  End If
  
 
  Set lForm = New Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="" _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orPortrait)



     
  With lForm.VsPrinter
    
    .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 9
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "N� " & IId_Operacion
      .TableCell(tcColWidth, 1, 1, 7, 1) = "160mm"
    .EndTable


    .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 9
      .TableCell(tcAlign, 1, 1) = taCenterMiddle
      .TableCell(tcText, 1, 1) = "COMPROBANTE DE MOVIMIENTO DE CAPITAL"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
    .EndTable

    .Paragraph = "" 'salto de linea

    .StartTable
      .TableBorder = tbBox
      .TableCell(tcRows) = 5
      .TableCell(tcCols) = 3
      .TableCell(tcFontSize, 1, 1, 5, 3) = 8
'      .TableCell(tcAlign, 1, 1) = taCenterMiddle
      .TableCell(tcColWidth, 1, 1) = "20mm"
      .TableCell(tcColWidth, 1, 2) = "5mm"
      .TableCell(tcColWidth, 1, 3) = "135mm"
      
      .TableCell(tcText, 1, 1) = "FECHA"
      .TableCell(tcText, 1, 2) = ":"
      .TableCell(tcText, 1, 3) = glb_cliente.Fecha_Operacion
      
      .TableCell(tcText, 2, 1) = "CLIENTE"
      .TableCell(tcText, 2, 2) = ":"
      .TableCell(tcText, 2, 3) = UCase(glb_cliente.Nombre)
      
      .TableCell(tcText, 3, 1) = "RUT"
      .TableCell(tcText, 3, 2) = ":"
      .TableCell(tcText, 3, 3) = FormatoRut(glb_cliente.Rut)
      
      .TableCell(tcText, 4, 1) = "N� CUENTA"
      .TableCell(tcText, 4, 2) = ":"
      .TableCell(tcText, 4, 3) = Cmb_Cuentas
      
      .TableCell(tcText, 5, 1) = "EJECUTIVO"
      .TableCell(tcText, 5, 2) = ":"
      .TableCell(tcText, 5, 3) = UCase(glb_cliente.Ejecutivo)
      
    .EndTable
      
    .Paragraph = "" 'salto de linea
    
    .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1) = 8
      .TableCell(tcText, 1, 1) = "Sr. Cliente, se informa a Ud., que con esta fecha se han retirado por concepto de Retiro de Capital los instrumentos que se detallan, los cuales en este acto dejan de formar parte integra de vuestro patrimonio administrado por Valores Security S.A. Corredores de Bolsa"
      .TableCell(tcAlign, 1, 1) = taJustMiddle
      .TableCell(tcColWidth, 1, 1) = "160mm"
    .EndTable
    
    .Paragraph = "" 'salto de linea

    .StartTable
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 4
      .TableCell(tcFontSize, 1, 1, 1, 4) = 7
      .TableCell(tcColWidth, 1, 1) = "60mm"
      .TableCell(tcColWidth, 1, 2) = "40mm"
      .TableCell(tcColWidth, 1, 3) = "30mm"
      .TableCell(tcColWidth, 1, 4) = "30mm"
      .TableBorder = tbBoxColumns
      .TableCell(tcAlign, 1, 1, 1, 4) = taCenterMiddle


      .TableCell(tcText, 1, 1) = "INSTRUMENTO"
      .TableCell(tcText, 1, 2) = "CANTIDAD O NOMINAL"
      .TableCell(tcText, 1, 3) = "PRECIO O TASA"
      .TableCell(tcText, 1, 4) = "TOTAL VALORIZADO"


    .EndTable

    .StartTable
      '.TableCell(tcRows) = 20
      .TableCell(tcCols) = 4
      .TableBorder = tbAll
      .TableCell(tcColWidth, 1, 1) = "60mm"
      .TableCell(tcColWidth, 1, 2) = "40mm"
      .TableCell(tcColWidth, 1, 3) = "30mm"
      .TableCell(tcColWidth, 1, 4) = "30mm"
      
         Set lcOperaciones = New Class_Operaciones
        lcOperaciones.Campo("id_operacion").Valor = IId_Operacion
        If lcOperaciones.BuscaConDetalles Then
          Filas = CInt(lcOperaciones.Cursor.Count)
          If Filas < 20 Then
            Filas = 20
          End If
          
          .TableCell(tcRows) = Filas
          
          a = 1
          For Each lReg In lcOperaciones.Cursor
                
               .TableCell(tcText, a, 1) = Entrega_Nemotecnico(lReg("id_nemotecnico").Value)
               .TableCell(tcText, a, 2) = FormatNumber(lReg("cantidad").Value)
               .TableCell(tcText, a, 3) = FormatNumber(lReg("precio").Value)
               .TableCell(tcText, a, 4) = FormatNumber(lReg("monto_pago").Value)

          a = a + 1
          Next

        End If
    .TableCell(tcRowHeight, 1, 2, Filas, 4) = "4mm"
    .TableCell(tcFontSize, 1, 1, Filas, 4) = 6
    .TableCell(tcAlign, 1, 2, Filas, 4) = taRightMiddle
    .EndTable

    .Paragraph = "" 'salto de linea
    
    .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1) = 8
      .TableCell(tcText, 1, 1) = "Atentamente,"
      .TableCell(tcColWidth, 1, 1) = "100mm"
      
    .EndTable
    
    .Paragraph = "" 'salto de linea
    
    
    
    .MarginLeft = "115mm"
    .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 3
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 3, 1) = 8
      .TableCell(tcColWidth, 1, 1) = "80mm"
      .TableCell(tcText, 1, 1) = "_______________________________________"
      .TableCell(tcText, 2, 1) = "P. Valores Security S.A. Corredores de Bolsa"
      '.TableCell(tcText, 3, 1) = "Adm. de Cartera"
      .TableCell(tcAlign, 1, 1, 3, 1) = taCenterMiddle


    .EndTable
    
    .EndDoc
  End With
  
  Call ColocarEn(lForm, Me, eFP_Abajo)
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub


Private Sub Trae_Antecedentes_Generales(lId_Cuenta As String, lId_Operacion As String)
    
    '------------------------------------------
'    Dim lcCuentas As Class_Cuentas
    Dim lcCuenta As Object
    
    Dim lcOperaciones  As Class_Operaciones
'    Dim lcMoneda As Class_Monedas
'    Dim lcMercado As Class_Productos
'    Dim lcContraparte As Class_Contrapartes
'
    '------------------------------------------
    Call Sub_Bloquea_Puntero(Me)
    '---------------------------------------

     
'    Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
       
        .Campo("id_Cuenta").Valor = lId_Cuenta
        
        If .Buscar(True) Then
            glb_cliente.Nombre = "" & .Cursor(1)("nombre_cliente").Value
            glb_cliente.Rut = "" & .Cursor(1)("rut_cliente").Value
            glb_cliente.Ejecutivo = "" & .Cursor(1)("dsc_asesor").Value

        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            Set lcCuenta = Nothing
            'GoTo ErrProcedure
        End If
    End With
    
      Set lcOperaciones = New Class_Operaciones
    With lcOperaciones
      .Campo("id_operacion").Valor = lId_Operacion
      If .Buscar Then
        If .Cursor.Count > 0 Then
          glb_cliente.Fecha_Operacion = .Cursor(1).Fields("fecha_operacion").Value
        End If
      End If
    End With
     Set lcOperaciones = Nothing

'
    
    
End Sub
Function FormatoRut(lrut As String) As String
Dim rut_1 As String
Dim rut_2 As String
Dim rut_c As String
Dim rut_t As String
Dim dv As String
rut_1 = Len(lrut)
rut_c = Left(lrut, rut_1 - 2)
dv = Right(lrut, 1)
rut_2 = Len(rut_c)
While Len(rut_c) > 2
rut_2 = Len(rut_c)
rut_t = "." & Right(rut_c, 3) & rut_t
rut_c = Left(rut_c, rut_2 - 3)
Wend
FormatoRut = rut_c & rut_t & "-" & dv
End Function

Function Entrega_Nemotecnico(lId_Nemotecnico As String) As String
   Dim lcNemotecnicos As Class_Nemotecnicos
   Dim Nemotecnico As String
   
   Set lcNemotecnicos = New Class_Nemotecnicos
    With lcNemotecnicos
      .Campo("id_nemotecnico").Valor = lId_Nemotecnico
      If .Buscar Then
        If .Cursor.Count > 0 Then
          Nemotecnico = .Cursor(1).Fields("nemotecnico").Value
        End If
      End If
    End With
    
    Entrega_Nemotecnico = Nemotecnico
    Set lcNemotecnicos = Nothing
    
End Function
