VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Begin VB.Form Frm_ImportaOperacionesSecurity 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cargas Autom�ticas"
   ClientHeight    =   8865
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   14730
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8865
   ScaleWidth      =   14730
   Begin TabDlg.SSTab Tab_Operaciones 
      Height          =   8145
      Left            =   0
      TabIndex        =   4
      Top             =   1140
      Width           =   14715
      _ExtentX        =   25956
      _ExtentY        =   14367
      _Version        =   393216
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Importa Renta Variable"
      TabPicture(0)   =   "Frm_ImportaOperacionesSecurity.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frnm_Grilla_Sucesos"
      Tab(0).Control(1)=   "Frm_Cuentas"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Importa Renta Fija"
      TabPicture(1)   =   "Frm_ImportaOperacionesSecurity.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame2"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Importar Fondos Mutuos"
      TabPicture(2)   =   "Frm_ImportaOperacionesSecurity.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame4"
      Tab(2).Control(1)=   "Frame3"
      Tab(2).ControlCount=   2
      Begin VB.Frame Frame2 
         Caption         =   "Operaciones Con Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4455
         Left            =   30
         TabIndex        =   15
         Top             =   330
         Width           =   14685
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_RF 
            CausesValidation=   0   'False
            Height          =   3675
            Left            =   60
            TabIndex        =   16
            Top             =   660
            Width           =   14595
            _cx             =   25744
            _cy             =   6482
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   31
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesSecurity.frx":0054
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   7
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Operaciones_RF 
            Height          =   330
            Left            =   60
            TabIndex        =   17
            Top             =   270
            Width           =   10995
            _ExtentX        =   19394
            _ExtentY        =   582
            ButtonWidth     =   2884
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Wrappable       =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   6
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "SAVE"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Importar"
                  Key             =   "IMPORT"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Sel. Todo"
                  Key             =   "SEL_ALL"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Anula Sel."
                  Key             =   "SEL_NOTHING"
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Limpiar"
                  Key             =   "REFRESH"
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Reporte Asesor"
                  Key             =   "REPORT"
               EndProperty
            EndProperty
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Operaciones SIN Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   2925
         Left            =   30
         TabIndex        =   13
         Top             =   4800
         Width           =   14685
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_Sin_Cuenta_RF 
            CausesValidation=   0   'False
            Height          =   2625
            Left            =   60
            TabIndex        =   14
            Top             =   240
            Width           =   14595
            _cx             =   25744
            _cy             =   4630
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   30
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesSecurity.frx":0572
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   7
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Operaciones SIN Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   2925
         Left            =   -74970
         TabIndex        =   9
         Top             =   4800
         Width           =   14685
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_Sin_Cuenta_FM 
            CausesValidation=   0   'False
            Height          =   2625
            Left            =   60
            TabIndex        =   20
            Top             =   240
            Width           =   14595
            _cx             =   25744
            _cy             =   4630
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   25
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesSecurity.frx":0A6A
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   7
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Operaciones Con Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4455
         Left            =   -74970
         TabIndex        =   8
         Top             =   330
         Width           =   14685
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_FM 
            CausesValidation=   0   'False
            Height          =   3675
            Left            =   60
            TabIndex        =   18
            Top             =   660
            Width           =   14595
            _cx             =   25744
            _cy             =   6482
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   26
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesSecurity.frx":0ED2
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   7
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Operaciones_FM 
            Height          =   330
            Left            =   90
            TabIndex        =   19
            Top             =   270
            Width           =   11000
            _ExtentX        =   19394
            _ExtentY        =   582
            ButtonWidth     =   2884
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Wrappable       =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   6
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "SAVE"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Importar"
                  Key             =   "IMPORT"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Sel. Todo"
                  Key             =   "SEL_ALL"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Anula Sel."
                  Key             =   "SEL_NOTHING"
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Limpiar"
                  Key             =   "REFRESH"
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Reporte Asesor"
                  Key             =   "REPORT"
               EndProperty
            EndProperty
         End
      End
      Begin VB.Frame Frnm_Grilla_Sucesos 
         Caption         =   "Operaciones SIN Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   2925
         Left            =   -74970
         TabIndex        =   7
         Top             =   4800
         Width           =   14685
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_Sin_Cuenta_RV 
            CausesValidation=   0   'False
            Height          =   2625
            Left            =   60
            TabIndex        =   12
            Top             =   240
            Width           =   14595
            _cx             =   25744
            _cy             =   4630
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   30
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesSecurity.frx":1362
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   7
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin VB.Frame Frm_Cuentas 
         Caption         =   "Operaciones Con Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4455
         Left            =   -74970
         TabIndex        =   5
         Top             =   330
         Width           =   14685
         Begin MSComctlLib.Toolbar Toolbar_Operaciones_RV 
            Height          =   330
            Left            =   60
            TabIndex        =   6
            Top             =   270
            Width           =   10995
            _ExtentX        =   19394
            _ExtentY        =   582
            ButtonWidth     =   2884
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Wrappable       =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   6
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "SAVE"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Importar"
                  Key             =   "IMPORT"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Sel. Todo"
                  Key             =   "SEL_ALL"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Anula Sel."
                  Key             =   "SEL_NOTHING"
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Limpiar"
                  Key             =   "REFRESH"
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Reporte Asesor"
                  Key             =   "REPORT"
               EndProperty
            EndProperty
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_RV 
            CausesValidation=   0   'False
            Height          =   3675
            Left            =   60
            TabIndex        =   11
            Top             =   660
            Width           =   14595
            _cx             =   25744
            _cy             =   6482
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   2
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   30
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesSecurity.frx":1835
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   7
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
   End
   Begin VB.Frame Frm_Fecha_Proceso 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   765
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   16635
      Begin MSMAPI.MAPISession MAPISession1 
         Left            =   8850
         Top             =   150
         _ExtentX        =   1005
         _ExtentY        =   1005
         _Version        =   393216
         DownloadMail    =   -1  'True
         LogonUI         =   -1  'True
         NewSession      =   0   'False
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Proceso 
         Height          =   315
         Left            =   1470
         TabIndex        =   1
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   60882945
         CurrentDate     =   37732
      End
      Begin MSMAPI.MAPIMessages MAPIMessages1 
         Left            =   9570
         Top             =   180
         _ExtentX        =   1005
         _ExtentY        =   1005
         _Version        =   393216
         AddressEditFieldCount=   1
         AddressModifiable=   0   'False
         AddressResolveUI=   0   'False
         FetchSorted     =   0   'False
         FetchUnreadOnly =   0   'False
      End
      Begin VB.Label Lbl_Fecha_Proceso 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Proceso"
         Height          =   345
         Left            =   210
         TabIndex        =   2
         Top             =   270
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   14730
      _ExtentX        =   25982
      _ExtentY        =   635
      ButtonWidth     =   1429
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   11040
         TabIndex        =   10
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_ImportaOperacionesSecurity"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim lcAlias As Object

Dim fKey_RV As String
Dim fKey_RF As String
Dim fKey_FM As String
Dim lid_Empresa As Double

Const cTipOp_Compra = "C"
Const cTipOp_Venta = "V"

Public Function Fnt_Mostrar(pCod_Proceso_Componente_RV As String, _
                            pCod_Proceso_Componente_RF As String, _
                            pCod_Proceso_Componente_FM As String)
  Fnt_Mostrar = False

  fKey_RV = pCod_Proceso_Componente_RV
  fKey_RF = pCod_Proceso_Componente_RF
  fKey_FM = pCod_Proceso_Componente_FM
  
  If Not Fnt_CargarDatos Then
    Unload Me
    Exit Function
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
  
  Fnt_Mostrar = True
End Function

Private Function Fnt_CargarDatos() As Boolean
Dim lMensaje              As String

On Error GoTo ErrProcedure

  Call Sub_Bloquea_Puntero(Me)
  Fnt_CargarDatos = False
  
  Load Me
  
  Fnt_CargarDatos = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la carga de un Proceso Componente." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Function

Private Sub Form_Load()
Dim lReg  As hCollection.hFields

  Call PubPrdObtienePosicion(Me)
    
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Operaciones_RV
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("IMPORT").Image = cBoton_Aceptar
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
      .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
      .Buttons("REPORT").Image = cBoton_Pdf
      .Buttons("SAVE").Enabled = False
      .Buttons("REFRESH").Enabled = False
      .Buttons("SEL_ALL").Enabled = False
      .Buttons("SEL_NOTHING").Enabled = False
      .Buttons("REPORT").Enabled = False
      .Appearance = ccFlat
  End With
  
  With Toolbar_Operaciones_RF
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("IMPORT").Image = cBoton_Aceptar
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
      .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
      .Buttons("REPORT").Image = cBoton_Pdf
      .Buttons("SAVE").Enabled = False
      .Buttons("REFRESH").Enabled = False
      .Buttons("SEL_ALL").Enabled = False
      .Buttons("SEL_NOTHING").Enabled = False
      .Buttons("REPORT").Enabled = False
      .Appearance = ccFlat
  End With
  
  With Toolbar_Operaciones_FM
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("IMPORT").Image = cBoton_Aceptar
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
      .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
      .Buttons("REPORT").Image = cBoton_Pdf
      .Buttons("SAVE").Enabled = False
      .Buttons("REFRESH").Enabled = False
      .Buttons("SEL_ALL").Enabled = False
      .Buttons("SEL_NOTHING").Enabled = False
      .Buttons("REPORT").Enabled = False
      .Appearance = ccFlat
  End With
  
  'La fecha no esta en el procedimiento limpiar objecto
  DTP_Fecha_Proceso.Value = Fnt_FechaServidor
  
  Call Sub_Limpia_Objetos
  Tab_Operaciones.Tab = 0
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call PubPrdGrabaPosicion(Me)
End Sub

Private Sub Grilla_Operaciones_FM_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_FM.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_FM_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_FM)
End Sub

Private Sub Grilla_Operaciones_FM_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_FM)
End Sub

Private Sub Grilla_Operaciones_FM_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_FM)
End Sub

Private Sub Grilla_Operaciones_FM_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_FM
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub

Private Sub Grilla_Operaciones_RF_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_RF.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_RF_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_RF)
End Sub

Private Sub Grilla_Operaciones_RF_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_RF)
End Sub

Private Sub Grilla_Operaciones_RF_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_RF)
End Sub

Private Sub Grilla_Operaciones_RF_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_RF
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub

Private Sub Grilla_Operaciones_RV_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_RV.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_RV_Click()
    Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_RV)
End Sub

Private Sub Grilla_Operaciones_RV_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_RV)
End Sub

Private Sub Grilla_Operaciones_RV_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_RV
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_Sin_Cuenta_FM)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_Sin_Cuenta_FM
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_Sin_Cuenta_RF.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_Sin_Cuenta_RF)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_Sin_Cuenta_RF)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_Sin_Cuenta_RF)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_Sin_Cuenta_RF
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_Sin_Cuenta_RV.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_Sin_Cuenta_RV)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_Sin_Cuenta_RV)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_Sin_Cuenta_RV)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_Sin_Cuenta_RV
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub
Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Limpia_Objetos()
  'DTP_Fecha_Proceso.Value = Fnt_FechaServidor
  
  Grilla_Operaciones_RV.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_RV.Rows = 1
  Grilla_Operaciones_RF.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_RF.Rows = 1
  Grilla_Operaciones_FM.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_FM.Rows = 1
End Sub

Private Sub Sub_Importa_RV()
Dim lcNemotecnicos  As Class_Nemotecnicos
'Dim lcCuenta        As Class_Cuentas
Dim lcCuenta        As Object
Dim lCursor         As hRecord
Dim lCampo          As hFields
'---------------------------------------
Dim lGrilla As VSFlexGrid
'---------------------------------------
Dim lId_Cuenta As Variant
Dim lTipo_Movimiento As String
Dim lFlg_Tipo_Movimiento As String
Dim lFolioContraparte
Dim lId_Nemotecnico
Dim lId_Operacion_detalle
Dim lNum_Cuenta As String
Dim lCod_Instrumento As String
Dim lNombre_Cliente As String
Dim lMontos_Gastos
Dim lId_Asesor_
Dim lMsg_Error    As String
Dim lLinea        As Long
Dim lFlg_Estado   As String
Dim lDsc_Bloqueo  As String

On Error GoTo ErrProcedure
  
  Me.Enabled = False
  Call Sub_Bloquea_Puntero(Me)
  
  Set lcAlias = CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  Set lcNemotecnicos = New Class_Nemotecnicos
  
  gDB.Parametros.Clear
  gDB.Procedimiento = "CisCB&DBO&AD_QRY_CB_MOVTO_ACCIONES"
  gDB.Parametros.Add "pFecha_movimientos", ePT_Caracter, Format(DTP_Fecha_Proceso.Value, "YYYYMMDD"), ePD_Entrada
  gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
  If Not gDB.EjecutaSP Then
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
    GoTo ExitProcedure
  End If
  
  Set lCursor = gDB.Parametros("pcursor").Valor
    
  If lCursor.Count <= 0 Then
    GoTo ExitProcedure
  End If
  
  BarraProceso.Max = lCursor.Count
  For Each lCampo In lCursor
    BarraProceso.Value = lCampo.Index
    
    lTipo_Movimiento = ""
    Select Case lCampo("TIPO_OPERACION").Value
      Case cTipOp_Compra
        lTipo_Movimiento = "Compra"
        lFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso
      Case cTipOp_Venta
        lTipo_Movimiento = "Venta"
        lFlg_Tipo_Movimiento = gcTipoOperacion_Egreso
      Case Else
        'Si un tipo de movimiento no esta contemplado se salta
        GoTo ProximoMovimiento
    End Select
    
    lFolioContraparte = Trim(lCampo("NUMERO_ORDEN").Value) & "/" & Trim(lCampo("CORRELATIVO").Value)
    
'    lId_Operacion_detalle = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Mov_RV _
'                                                    , pCodigoCSBPI:=cTabla_Operacion_Detalle _
'                                                    , pValor:=lFolioContraparte)
'
'    If Not IsNull(lId_Operacion_detalle) Then
'      'pMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
'      GoTo ProximoMovimiento
'    End If
    
    '------- RESCATA EL ID_nemot�cnico
    With lcNemotecnicos
      .Campo("nemotecnico").Valor = Trim(lCampo("nemotecnico").Value)
      .Campo("cod_instrumento").Valor = gcINST_ACCIONES_NAC
      If Not .Buscar() Then
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en buscar el Nemot�cnico.", _
                          .ErrMsg, _
                          pConLog:=True)
        GoTo ExitProcedure
      End If
      
      If .Cursor.Count > 0 Then
        lId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
        lCod_Instrumento = .Cursor(1)("cod_instrumento").Value
      End If
    End With
    '------- FIN RESCATA EL ID_nemot�cnico
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_SECURITY.IMPORT_AVAIBLE"
    gDB.Parametros.Add "PID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    gDB.Parametros.Add "PVALOR", ePT_Caracter, lFolioContraparte, ePD_Entrada
    gDB.Parametros.Add "PCOD_INSTRUMENTO", ePT_Caracter, lCod_Instrumento, ePD_Entrada
    gDB.Parametros.Add "PRETURN", ePT_Caracter, "", ePD_Salida
    If Not gDB.EjecutaSP Then
      Call Fnt_MsgError(eLS_ErrSystem, _
                        "Problemas para comprobar si esta disponible la importaci�n.", _
                        gDB.ErrMsg, _
                        pConLog:=True)
      GoTo ProximoMovimiento
    End If
    
    If Not gDB.Parametros("PRETURN").Valor = gcFlg_SI Then
      'SI ENTREGA UN VALOR DISTINTO A "S" SIGNIFICA QUE NO TIENE PERMITIDO EL INGRESO
      GoTo ProximoMovimiento
    End If
    

    lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Magic_Valores _
                                          , pCodigoCSBPI:=cTabla_Cuentas _
                                          , pValor:=Trim(lCampo("RUT_CLIENTE").Value))
                                          
    lNum_Cuenta = ""
    lNombre_Cliente = ""
    lid_Empresa = 0
    If IsNull(lId_Cuenta) Then
      Set lGrilla = Grilla_Operaciones_Sin_Cuenta_RV
      lNombre_Cliente = lCampo("nombre_cliente").Value
    Else
      Set lGrilla = Grilla_Operaciones_RV
      
      ' ----------- RESCATA NUMERO CUENTA
      lcCuenta.Campo("id_cuenta").Valor = lId_Cuenta
      If lcCuenta.Buscar(pEnVista:=True) Then
        lNum_Cuenta = lcCuenta.Cursor(1)("num_cuenta").Value
        lNombre_Cliente = lcCuenta.Cursor(1)("nombre_cliente").Value
        lId_Asesor_ = NVL(lcCuenta.Cursor(1)("id_asesor").Value, 0)
        lFlg_Estado = lcCuenta.Cursor(1)("cod_estado").Value
        lDsc_Bloqueo = ""
        lid_Empresa = lcCuenta.Cursor(1)("id_Empresa").Value
        If lFlg_Estado = "H" Then
            If lcCuenta.Cursor(1)("flg_bloqueado").Value = "S" Then
                lFlg_Estado = "B"
                lDsc_Bloqueo = "Bloqueado :" & lcCuenta.Cursor(1)("obs_bloqueo").Value
            End If
        Else
            lDsc_Bloqueo = "Cuenta Deshabilitada"
        End If
        If lid_Empresa <> Fnt_EmpresaActual Then
            GoTo ProximoMovimiento
        End If
      End If
    End If

    lLinea = lGrilla.Rows
    Call lGrilla.AddItem("")
'    Call SetCell(lGrilla, lLinea, "colum_pk", lFolioContraparte, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "n_orden", lCampo("NUMERO_ORDEN").Value, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "id_linea", lCampo("CORRELATIVO").Value, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "rut", Trim(lCampo("RUT_CLIENTE").Value), pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "nombre_cliente", lNombre_Cliente, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "id_cuenta", NVL(lId_Cuenta, 0), pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "num_cuenta", lNum_Cuenta, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "tipo_movimiento", lTipo_Movimiento, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "flg_tipo_movimiento", lFlg_Tipo_Movimiento, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "id_nemotecnico", lId_Nemotecnico, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "nemotecnico", lCampo("nemotecnico").Value, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "Cantidad", lCampo("CANTIDAD").Value, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "precio", lCampo("PRECIO").Value, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "comision", NVL(lCampo("COMISION").Value, 0), pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "Derecho", NVL(lCampo("DERECHO").Value, 0), pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "gastos", NVL(lCampo("GASTOS").Value, 0), pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "iva", NVL(lCampo("IVA").Value, 0), pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "monto", NVL(lCampo("MONTO_NETO").Value, 0), pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "fecha_liquidacion", NVL(lCampo("FECHA_LIQUIDACION").Value, ""), pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "cod_instru", lCod_Instrumento, pAutoSize:=False)
'    Call SetCell(lGrilla, lLinea, "Grabado", gcFlg_NO, pAutoSize:=False)
    
    Call SetCell(lGrilla, lLinea, "colum_pk", Trim(lCampo("NUMERO_ORDEN").Value) & "/" & Trim(lCampo("CORRELATIVO").Value), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "n_orden", lCampo("NUMERO_ORDEN").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_linea", NVL(lCampo("CORRELATIVO").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "rut", Trim(lCampo("RUT_CLIENTE").Value), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "nombre_cliente", lNombre_Cliente, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_cuenta", "" & lId_Cuenta, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "num_cuenta", "" & lNum_Cuenta, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "tipo_movimiento", lTipo_Movimiento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "flg_tipo_movimiento", lFlg_Tipo_Movimiento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_nemotecnico", lId_Nemotecnico, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "nemotecnico", lCampo("nemotecnico").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "Cantidad", lCampo("CANTIDAD").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "dsc_cliente", Left(lNombre_Cliente, 30), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "precio", lCampo("PRECIO").Value, pAutoSize:=False)
    
    lMontos_Gastos = NVL(lCampo("COMISION").Value, 0) + NVL(lCampo("DERECHO").Value, 0) + NVL(lCampo("GASTOS").Value, 0) + NVL(lCampo("IVA").Value, 0)
    
    'lMontos_Gastos = IIf(lFlg_Tipo_Movimiento = "E", lMontos_Gastos * -1, lMontos_Gastos)
    
    
    Call SetCell(lGrilla, lLinea, "comision", NVL(lCampo("COMISION").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "Derecho", NVL(lCampo("DERECHO").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "gastos", NVL(lCampo("GASTOS").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "iva", NVL(lCampo("IVA").Value, 0), pAutoSize:=False)
    
    
    Call SetCell(lGrilla, lLinea, "total_gastos", FormatNumber(NVL(lMontos_Gastos, 0), 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "monto", NVL(lCampo("MONTO_NETO").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "fecha_liquidacion", NVL(lCampo("FECHA_LIQUIDACION").Value, ""), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "cod_instru", lCod_Instrumento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "Grabado", gcFlg_NO, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_asesor", lId_Asesor_, pAutoSize:=False)
    
    Call SetCell(lGrilla, lLinea, "flg_bloqueado", lFlg_Estado, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "dsc_error", lDsc_Bloqueo, pAutoSize:=False)
    
ProximoMovimiento:
    'este label sirve para saltarce el ingreso de un de operacion con un tipo de movimiento que no corresponde,
    'como los ingresos de custodias, la otra forma es hacer un if gigante que no quiero hacer :-P
    'HAF: 20071108
  Next
  
  Call Sub_AjustaColumnas_Grilla(Grilla_Operaciones_Sin_Cuenta_RV)
  Call Sub_AjustaColumnas_Grilla(Grilla_Operaciones_RV)
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la importaci�n de los movimientos." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcCuenta = Nothing
  Set lcNemotecnicos = Nothing
  Set lcAlias = Nothing
  gDB.Parametros.Clear
  
  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Habilita_Reporte_Asesor()
  Select Case Tab_Operaciones.Tab
  
  Case 0
      If Grilla_Operaciones_RV.Rows > 1 Or Grilla_Operaciones_Sin_Cuenta_RV.Rows > 1 Then
        Toolbar_Operaciones_RV.Buttons("REPORT").Enabled = True
      Else
        Toolbar_Operaciones_RV.Buttons("REPORT").Enabled = False
      End If
    
  Case 1
      If Grilla_Operaciones_RF.Rows > 1 Or Grilla_Operaciones_Sin_Cuenta_RF.Rows > 1 Then
        Toolbar_Operaciones_RF.Buttons("REPORT").Enabled = True
      Else
        Toolbar_Operaciones_RF.Buttons("REPORT").Enabled = False
      End If
  Case 2
      If Grilla_Operaciones_FM.Rows > 1 Or Grilla_Operaciones_Sin_Cuenta_FM.Rows > 1 Then
        Toolbar_Operaciones_FM.Buttons("REPORT").Enabled = True
      Else
        Toolbar_Operaciones_FM.Buttons("REPORT").Enabled = False
      End If
  End Select
End Sub
Private Sub Toolbar_Operaciones_FM_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "IMPORT"
      DTP_Fecha_Proceso.Enabled = False
      Tab_Operaciones.TabEnabled(0) = False
      Tab_Operaciones.TabEnabled(1) = False
      With Toolbar_Operaciones_FM
          .Buttons("SAVE").Enabled = True
          .Buttons("IMPORT").Enabled = False
          .Buttons("SEL_ALL").Enabled = True
          .Buttons("SEL_NOTHING").Enabled = True
          .Buttons("REFRESH").Enabled = True
      End With
      Call Sub_Importa_FM
      Call Habilita_Reporte_Asesor
    Case "REFRESH"
        DTP_Fecha_Proceso.Enabled = True
        Tab_Operaciones.TabEnabled(0) = True
        Tab_Operaciones.TabEnabled(1) = True
        With Toolbar_Operaciones_FM
            .Buttons("SAVE").Enabled = False
            .Buttons("IMPORT").Enabled = True
            .Buttons("SEL_ALL").Enabled = False
            .Buttons("SEL_NOTHING").Enabled = False
            .Buttons("REFRESH").Enabled = False
            .Buttons("REPORT").Enabled = False
        End With
        Call Sub_Limpia_Objetos
    Case "SEL_ALL"
        Call Sub_CambiaCheck(Grilla_Operaciones_FM, True)
    Case "SEL_NOTHING"
        Call Sub_CambiaCheck(Grilla_Operaciones_FM, False)
    Case "SAVE"
        Toolbar_Operaciones_FM.Buttons("REPORT").Enabled = False
        Call Sub_Graba_Operaciones_con_Cta_FM
    Case "REPORT"
        Call Sub_Reporte_Asesor(Grilla_Operaciones_FM, Grilla_Operaciones_Sin_Cuenta_FM, "FM")
  End Select

End Sub

Private Sub Toolbar_Operaciones_RF_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
    Select Case Button.Key
        Case "IMPORT"
            DTP_Fecha_Proceso.Enabled = False
            Tab_Operaciones.TabEnabled(0) = False
            Tab_Operaciones.TabEnabled(2) = False
            With Toolbar_Operaciones_RF
                .Buttons("SAVE").Enabled = True
                .Buttons("IMPORT").Enabled = False
                .Buttons("SEL_ALL").Enabled = True
                .Buttons("SEL_NOTHING").Enabled = True
                .Buttons("REFRESH").Enabled = True
            End With
            
            Call Sub_Importa_RF
            Call Habilita_Reporte_Asesor
            
        Case "REFRESH"
            DTP_Fecha_Proceso.Enabled = True
            Tab_Operaciones.TabEnabled(0) = True
            Tab_Operaciones.TabEnabled(2) = True
            With Toolbar_Operaciones_RF
                .Buttons("SAVE").Enabled = False
                .Buttons("IMPORT").Enabled = True
                .Buttons("SEL_ALL").Enabled = False
                .Buttons("SEL_NOTHING").Enabled = False
                .Buttons("REFRESH").Enabled = False
                .Buttons("REPORT").Enabled = False
            End With
            Call Sub_Limpia_Objetos
        Case "SEL_ALL"
            Call Sub_CambiaCheck(Grilla_Operaciones_RF, True)
        Case "SEL_NOTHING"
            Call Sub_CambiaCheck(Grilla_Operaciones_RF, False)
        Case "SAVE"
            Toolbar_Operaciones_RF.Buttons("REPORT").Enabled = False
            Call Sub_Graba_Operaciones_con_Cta_RF
        Case "REPORT"
            Call Sub_Reporte_Asesor(Grilla_Operaciones_RF, Grilla_Operaciones_Sin_Cuenta_RF, "RF")
    End Select
End Sub

Private Sub Toolbar_Operaciones_RV_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "IMPORT"
      DTP_Fecha_Proceso.Enabled = False
      Tab_Operaciones.TabEnabled(1) = False
      Tab_Operaciones.TabEnabled(2) = False
      With Toolbar_Operaciones_RV
        .Buttons("SAVE").Enabled = True
        .Buttons("IMPORT").Enabled = False
        .Buttons("SEL_ALL").Enabled = True
        .Buttons("SEL_NOTHING").Enabled = True
        .Buttons("REFRESH").Enabled = True
      End With
      
      Call Sub_Importa_RV
      Call Habilita_Reporte_Asesor
      
    Case "REFRESH"
      DTP_Fecha_Proceso.Enabled = True
      Tab_Operaciones.TabEnabled(1) = True
      Tab_Operaciones.TabEnabled(2) = True
      With Toolbar_Operaciones_RV
        .Buttons("SAVE").Enabled = False
        .Buttons("IMPORT").Enabled = True
        .Buttons("SEL_ALL").Enabled = False
        .Buttons("SEL_NOTHING").Enabled = False
        .Buttons("REFRESH").Enabled = False
        .Buttons("REPORT").Enabled = False
      End With
      Call Sub_Limpia_Objetos
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_RV, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_RV, False)
    Case "SAVE"
      Toolbar_Operaciones_RV.Buttons("REPORT").Enabled = False
      Call Sub_Graba_Operaciones_con_Cta_RV
    Case "REPORT"
      Call Sub_Reporte_Asesor(Grilla_Operaciones_RV, Grilla_Operaciones_Sin_Cuenta_RV, "RV")
    
  End Select
End Sub

Public Sub Sub_Reporte_Asesor(pGrilla As VSFlexGrid, _
                                pGrilla_SC As VSFlexGrid, _
                                lApellido As String)

Dim Registros As Integer
Dim lFila As Long
Dim Ultimo_Asesor
Dim Asesor_Actual
Dim i As Integer
Dim Asesores()

Ultimo_Asesor = 0
  With pGrilla
    '.Select 1, .ColIndex("id_asesor")
    '.Sort = flexSortGenericAscending
  End With
  
  With pGrilla_SC
    If .Rows > 1 Then
    '.Select 1, .ColIndex("id_asesor")
    '.Sort = flexSortGenericAscending
    End If
  End With
   
 ReDim Preserve Asesores(0)
 
  With pGrilla
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked Then
          If Not Fnt_Buscar_Asesor_Arreglo(Asesores(), GetCell(pGrilla, lFila, "id_asesor")) Then
                i = UBound(Asesores)
                Asesores(i) = GetCell(pGrilla, lFila, "id_asesor")
                i = UBound(Asesores) 'ReDim Asesores(UBound(Asesores) + 1)
                ReDim Preserve Asesores(i)
                
          End If
          
     End If
    Next
  End With
  Ultimo_Asesor = 0
      
  With pGrilla_SC
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked Then
       If Not Fnt_Buscar_Asesor_Arreglo(Asesores(), GetCell(pGrilla_SC, lFila, "id_asesor")) Then
                i = UBound(Asesores)
                Asesores(i) = NVL(GetCell(pGrilla_SC, lFila, "id_asesor"), 0)
                i = UBound(Asesores) + 1 'ReDim Asesores(UBound(Asesores) + 1)
                ReDim Preserve Asesores(i)
                
          End If
      End If
      
    Next
  End With
  
  For i = 0 To UBound(Asesores)
    If Asesores(i) <> "" Then
        If lApellido = "FM" Then
            Call Sub_CreaPDF_Mail_Asesor_FM(Asesores(i), pGrilla, pGrilla_SC, lApellido)
        Else
            Call Sub_CreaPDF_Mail_Asesor(Asesores(i), pGrilla, pGrilla_SC, lApellido)
        End If
    End If
      'MsgBox Asesores(i)
  Next
  
End Sub
Private Function Fnt_Buscar_Asesor_Arreglo(Arreglo(), lId_Asesor) As Boolean
Dim i
Fnt_Buscar_Asesor_Arreglo = False
For i = 0 To UBound(Arreglo)
  If Arreglo(i) = lId_Asesor Then
      Fnt_Buscar_Asesor_Arreglo = True
      Exit For
  End If
Next
End Function


Private Sub Sub_CreaPDF_Mail_Asesor(pId_Asesor, pGrilla As VSFlexGrid, pGrilla_SC As VSFlexGrid, lApellido As String)
Dim lFila As Long
'Dim ArchivosPDF()
'With pGrilla
  
'ReDim Preserve ArchivosPDF(0)
 
Const clrHeader = &HD0D0D0
Dim sRecord
Dim bAppend
Dim lLinea As Integer
Dim lForm As Object 'Frm_Reporte_Generico
Dim lReg As hCollection.hFields
Dim a As Integer
Dim iFila As Integer
Dim Filas As Integer
Dim Fila As Integer
Dim lCursor_Asesor As hRecord
Dim pArchivoPDF As String
Dim lTotalGastos As Double
'-----------------------------------------------------------------------
'Dim lOD_Cursor As hRecord 'CURSOR PARA EL DETALLE DE LAS OPERACIONES.

  'Set Fnt_Generar_Comprobante = Nothing
  'Call Sub_Bloquea_Puntero(Me)
  'Call Sub_CargarDatos_Gen(pId_Operacion)
  
  'Set lForm = fClass.Form_Reporte_Generico
  
  pArchivoPDF = Environ("temp") & "\Confirmacion_Importacion_" & lApellido & "-" & pId_Asesor & ".pdf"
  Set lForm = Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="Confirmaci�n de Ordenes" _
                               , pTipoSalida:=ePrinter.eP_PDF_Automatico _
                               , pOrientacion:=orLandscape _
                               , pArchivoSalida:=pArchivoPDF)
     
Dim lc_Asesores As Class_Asesor
Dim lNombre_Asesor As String
Dim lMail_Asesor As String

Set lc_Asesores = New Class_Asesor

With lc_Asesores
    .Campo("id_asesor").Valor = pId_Asesor
    If .Buscar Then
        Set lCursor_Asesor = .Cursor
        For Each lReg In .Cursor
           lNombre_Asesor = lReg("nombre").Value
           lMail_Asesor = lReg("Email").Value
        Next
    End If
End With
    
  With lForm.VsPrinter

 ' .StartDoc
 .Paragraph = ""
 .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 8
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "ASESOR: " & lNombre_Asesor & " (" & lMail_Asesor & ")"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
  .EndTable
 
 .Paragraph = ""

  .MarginLeft = "30mm"
  .MarginRight = "7mm"
    .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 7
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "Operaciones con Patrimonio Administrado"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
    .EndTable
 
    '.Paragraph = "" 'salto de linea
    
    .StartTable
      
    '.TableCell(tcRows) = pGrilla.Rows
    .TableCell(tcRows) = 1
    .TableCell(tcCols) = 11
    .TableBorder = tbAll
    '
   .TableCell(tcColWidth, 1, 1) = "15mm"
    .TableCell(tcColWidth, 1, 2) = "18mm"
    .TableCell(tcColWidth, 1, 3) = "15mm"
    .TableCell(tcColWidth, 1, 4) = "45mm"
    .TableCell(tcColWidth, 1, 5) = "20mm"
    .TableCell(tcColWidth, 1, 6) = "35mm"
    .TableCell(tcColWidth, 1, 7) = "25mm"
    .TableCell(tcColWidth, 1, 8) = "15mm"
    .TableCell(tcColWidth, 1, 9) = "15mm"
    .TableCell(tcColWidth, 1, 10) = "25mm"
    .TableCell(tcColWidth, 1, 11) = "15mm"
    '.TableCell(tcColWidth, 1, 12) = "25mm"
    '.TableCell(tcColWidth, 1, 13) = "20mm"
    
    .TableCell(tcText, 1, 1) = "N� Orden"
    .TableCell(tcText, 1, 2) = "Rut Cliente"
    .TableCell(tcText, 1, 3) = "Cuenta"
    .TableCell(tcText, 1, 4) = "Cliente"
    .TableCell(tcText, 1, 5) = "Movimiento"
    .TableCell(tcText, 1, 6) = "Nemot�cnico"
    .TableCell(tcText, 1, 7) = "Cantidad"
    .TableCell(tcText, 1, 8) = "Precio / Tasa"
   ' .TableCell(tcText, 1, 8) = "Comisi�n"
   ' .TableCell(tcText, 1, 9) = "Derechos"
   ' .TableCell(tcText, 1, 10) = "Gasto"
   ' .TableCell(tcText, 1, 11) = "IVA"
    .TableCell(tcText, 1, 9) = "Gastos Ope."
    .TableCell(tcText, 1, 10) = "Monto"
    .TableCell(tcText, 1, 11) = "Fecha Liquidaci�n"
    
    
 Fila = 2
 For lFila = 1 To pGrilla.Rows - 1
      If pGrilla.Cell(flexcpChecked, lFila, pGrilla.ColIndex("chk")) = flexChecked And GetCell(pGrilla, lFila, "id_asesor") = pId_Asesor Then
          .TableCell(tcRows) = .TableCell(tcRows) + 1
           'Fila = lFila + 1
          .TableCell(tcText, Fila, 1) = GetCell(pGrilla, lFila, "n_orden")
          .TableCell(tcText, Fila, 2) = GetCell(pGrilla, lFila, "rut")
          .TableCell(tcText, Fila, 3) = GetCell(pGrilla, lFila, "num_cuenta")
          .TableCell(tcText, Fila, 4) = GetCell(pGrilla, lFila, "dsc_cliente")
          .TableCell(tcText, Fila, 5) = GetCell(pGrilla, lFila, "tipo_movimiento")
          .TableCell(tcText, Fila, 6) = GetCell(pGrilla, lFila, "nemotecnico")
          .TableCell(tcText, Fila, 7) = FormatNumber(GetCell(pGrilla, lFila, "cantidad"))
          .TableCell(tcText, Fila, 8) = FormatNumber(GetCell(pGrilla, lFila, "precio"))
          .TableCell(tcText, Fila, 9) = FormatNumber(GetCell(pGrilla, lFila, "total_gastos"))
          If UCase(GetCell(pGrilla, lFila, "tipo_movimiento")) = "COMPRA" Then
            lTotalGastos = FormatNumber(GetCell(pGrilla, lFila, "total_gastos"))
          Else
            lTotalGastos = FormatNumber(GetCell(pGrilla, lFila, "total_gastos")) * -1
          End If
          .TableCell(tcText, Fila, 10) = FormatNumber(FormatNumber(GetCell(pGrilla, lFila, "monto")) + lTotalGastos)
          .TableCell(tcText, Fila, 11) = GetCell(pGrilla, lFila, "fecha_liquidacion")
          
          .TableCell(tcAlign, Fila, 1) = taLeftMiddle
          .TableCell(tcAlign, Fila, 2) = taRightMiddle
          .TableCell(tcAlign, Fila, 3) = taCenterMiddle
          .TableCell(tcAlign, Fila, 6) = taLeftMiddle
          .TableCell(tcAlign, Fila, 7) = taRightMiddle
          .TableCell(tcAlign, Fila, 8) = taRightMiddle
          .TableCell(tcAlign, Fila, 9) = taRightMiddle
          .TableCell(tcAlign, Fila, 10) = taRightMiddle
          .TableCell(tcAlign, Fila, 11) = taRightMiddle
          '.TableCell(tcAlign, Fila, 12) = taRightMiddle
          '.TableCell(tcAlign, Fila, 13) = taRightMiddle
          Fila = Fila + 1
      End If
  Next
  .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 13) = 6
  
  .EndTable
  
.Paragraph = ""
'If pGrilla_SC.Rows > 1 Then
.StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 7
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "Operaciones Fuera del Patrimonio Administrado"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
.EndTable


.StartTable
      
    '.TableCell(tcRows) = pGrilla_SC.Rows
    .TableCell(tcRows) = 1
    .TableCell(tcCols) = 11
    .TableBorder = tbAll
   ' .TableCell(tcFontSize, 1, 1, pGrilla_SC.Rows, 13) = 6
    
   .TableCell(tcColWidth, 1, 1) = "15mm"
    .TableCell(tcColWidth, 1, 2) = "18mm"
    .TableCell(tcColWidth, 1, 3) = "15mm"
    .TableCell(tcColWidth, 1, 4) = "45mm"
    .TableCell(tcColWidth, 1, 5) = "20mm"
    .TableCell(tcColWidth, 1, 6) = "35mm"
    .TableCell(tcColWidth, 1, 7) = "25mm"
    .TableCell(tcColWidth, 1, 8) = "15mm"
    .TableCell(tcColWidth, 1, 9) = "15mm"
    .TableCell(tcColWidth, 1, 10) = "25mm"
    .TableCell(tcColWidth, 1, 11) = "15mm"
    
    
    .TableCell(tcText, 1, 1) = "N� Orden"
    .TableCell(tcText, 1, 2) = "Rut Cliente"
    .TableCell(tcText, 1, 3) = "Cuenta"
    .TableCell(tcText, 1, 4) = "Cliente"
    .TableCell(tcText, 1, 5) = "Movimiento"
    .TableCell(tcText, 1, 6) = "Nemot�cnico"
    .TableCell(tcText, 1, 7) = "Cantidad"
    .TableCell(tcText, 1, 8) = "Precio / Tasa"
    '.TableCell(tcText, 1, 8) = "Comisi�n"
    '.TableCell(tcText, 1, 9) = "Derechos"
    '.TableCell(tcText, 1, 10) = "Gasto"
    '.TableCell(tcText, 1, 11) = "IVA"
    .TableCell(tcText, 1, 9) = "Gastos Ope."
    .TableCell(tcText, 1, 10) = "Monto"
    .TableCell(tcText, 1, 11) = "Fecha Liquidaci�n"


'With pGrilla_SC
Fila = 2
  For lFila = 1 To pGrilla_SC.Rows - 1
      If pGrilla_SC.Cell(flexcpChecked, lFila, pGrilla_SC.ColIndex("chk")) = flexChecked And GetCell(pGrilla_SC, lFila, "id_asesor") = pId_Asesor Then
          .TableCell(tcRows) = .TableCell(tcRows) + 1
           ' Fila = lFila + 1
          .TableCell(tcText, Fila, 1) = GetCell(pGrilla_SC, lFila, "n_orden")
          .TableCell(tcText, Fila, 2) = GetCell(pGrilla_SC, lFila, "rut")
          .TableCell(tcText, Fila, 3) = GetCell(pGrilla_SC, lFila, "num_cuenta")
          .TableCell(tcText, Fila, 4) = GetCell(pGrilla_SC, lFila, "dsc_cliente")
          .TableCell(tcText, Fila, 5) = GetCell(pGrilla_SC, lFila, "tipo_movimiento")
          .TableCell(tcText, Fila, 6) = GetCell(pGrilla_SC, lFila, "nemotecnico")
          .TableCell(tcText, Fila, 7) = FormatNumber(GetCell(pGrilla_SC, lFila, "cantidad"))
          .TableCell(tcText, Fila, 8) = FormatNumber(GetCell(pGrilla_SC, lFila, "precio"))
          '.TableCell(tcText, Fila, 8) = FormatNumber(GetCell(pGrilla_SC, lFila, "comision"))
          '.TableCell(tcText, Fila, 9) = FormatNumber(GetCell(pGrilla_SC, lFila, "derecho"))
          '.TableCell(tcText, Fila, 10) = FormatNumber(GetCell(pGrilla_SC, lFila, "gasto"))
          '.TableCell(tcText, Fila, 11) = FormatNumber(GetCell(pGrilla_SC, lFila, "iva"))
          .TableCell(tcText, Fila, 9) = FormatNumber(GetCell(pGrilla_SC, lFila, "total_gastos"))
          .TableCell(tcText, Fila, 10) = FormatNumber(GetCell(pGrilla_SC, lFila, "monto"))
          .TableCell(tcText, Fila, 11) = GetCell(pGrilla_SC, lFila, "fecha_liquidacion")
     
          .TableCell(tcAlign, Fila, 1) = taLeftMiddle
          .TableCell(tcAlign, Fila, 2) = taRightMiddle
          .TableCell(tcAlign, Fila, 3) = taCenterMiddle
          .TableCell(tcAlign, Fila, 6) = taRightMiddle
          .TableCell(tcAlign, Fila, 7) = taRightMiddle
          .TableCell(tcAlign, Fila, 8) = taRightMiddle
          .TableCell(tcAlign, Fila, 9) = taRightMiddle
          .TableCell(tcAlign, Fila, 10) = taRightMiddle
          .TableCell(tcAlign, Fila, 11) = taRightMiddle
          .TableCell(tcAlign, Fila, 12) = taRightMiddle
          .TableCell(tcAlign, Fila, 13) = taRightMiddle
          Fila = Fila + 1
      End If
  Next
  .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 13) = 6
.EndTable
'End If
.EndDoc
End With
If Envia_Confirmacion(pArchivoPDF, lMail_Asesor, lNombre_Asesor) Then

End If
End Sub

Public Function Envia_Confirmacion(lArchivo As String, lMail As String, lNombre_Asesor As String) As Boolean
    Dim strNotaEnc      As String
    Dim lDestinatarios  As hRecord
    Dim lrArchivosPDFs  As hRecord
    Dim lMailBackOffice As String
    
    Envia_Confirmacion = False
    
    If Trim(lMail) <> "" Then
        Set lrArchivosPDFs = Nothing
        Call Prc_MAPI.Fnt_AgregarArchivoAdjunto(lrArchivosPDFs, lArchivo)
        Set lDestinatarios = Nothing
        Call Fnt_AgregarDestinatario(pRegDestinatarios:=lDestinatarios, pDisplayName:=lNombre_Asesor, pEmail:=lMail) 'lMail)
        lMailBackOffice = Fnt_Lee_Mail_BackOffice(gId_Empresa)
'        If lMailBackOffice <> "" Then
'            Call Fnt_AgregarDestinatario(pRegDestinatarios:=lDestinatarios, pDisplayName:="", pEmail:=lMailBackOffice)
'        End If
        strNotaEnc = " Sr. " & lNombre_Asesor & Chr(10) & "  Se Adjuntan operaciones que ser�n importadas al sistema GPI, favor confirmar "
'        Call Prc_MAPI.Fnt_Enviar("GPI: Confirmacion de Operaciones", strNotaEnc, lDestinatarios, True, lrArchivosPDFs)
        'Modificado por MMA 12/11/2008, se agrega el destinatario de BackOffice como CC y no en Para en el correo
        Call Prc_MAPI.Fnt_Enviar("GPI: Confirmacion de Operaciones", strNotaEnc, lDestinatarios, True, lrArchivosPDFs, lMailBackOffice)
    End If
End Function
Private Sub Grilla_Operaciones_RV_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_RV)
End Sub

Private Sub Sub_Setea_Cta_Nueva(pGrilla As VSFlexGrid)
Dim lId_Cuenta
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
'----------------------------
Dim lId_Cliente As String
Dim lcCliente As Object
'----------------------------
Dim lNum_Cuenta As String
Dim lRut_Cliente As String
Dim lNro_Orden As String
Dim sNombreCliente As String
Dim lId_Asesor
Dim lFila As Long
    
  lId_Cuenta = Frm_Busca_Cuentas.Buscar
  
  If Not IsNull(lId_Cuenta) Then
'    Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
      .Campo("id_cuenta").Valor = lId_Cuenta
      If .Buscar Then
        lId_Cliente = .Cursor(1).Fields("id_cliente").Value
        lNum_Cuenta = .Cursor(1).Fields("num_cuenta").Value
        lId_Asesor = NVL(.Cursor(1).Fields("id_asesor").Value, 0)
      End If
    End With
    Set lcCuenta = Nothing
    
    Set lcCliente = Fnt_CreateObject(cDLL_Clientes)
    With lcCliente
      Set .gDB = gDB
      .Campo("id_cliente").Valor = lId_Cliente
      If .Buscar(True) Then
        lRut_Cliente = .Cursor(1).Fields("rut_cliente").Value
        sNombreCliente = .Cursor(1).Fields("nombre_cliente").Value
      End If
    End With
    Set lcCliente = Nothing
    
    lNro_Orden = GetCell(pGrilla, pGrilla.Row, "n_orden")
    For lFila = 1 To pGrilla.Rows - 1
      If GetCell(pGrilla, lFila, "n_orden") = lNro_Orden Then
        Call SetCell(pGrilla, lFila, "id_cuenta", lId_Cuenta)
        Call SetCell(pGrilla, lFila, "num_cuenta", lNum_Cuenta)
        Call SetCell(pGrilla, lFila, "id_asesor", lId_Asesor)
        Call SetCell(pGrilla, lFila, "rut", lRut_Cliente)
        Call SetCell(pGrilla, lFila, "nombre_cliente", sNombreCliente)
      End If
    Next
  End If
End Sub

Private Sub Sub_Graba_Operaciones_con_Cta_RV()
Dim lFila As Long
    
  With Grilla_Operaciones_RV
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked And _
        Not GetCell(Grilla_Operaciones_RV, lFila, "GRABADO") = gcFlg_SI Then
        If Not Fnt_Ope_Directa_Cont_RV(pGrilla:=Grilla_Operaciones_RV, _
                                       pFila:=lFila, _
                                       pNro_Orden:=GetCell(Grilla_Operaciones_RV, lFila, "N_ORDEN")) Then
          'GoTo ErrProcedure
        End If
      End If
    Next
  End With
  
  With Grilla_Operaciones_Sin_Cuenta_RV
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked And _
          Not GetCell(Grilla_Operaciones_Sin_Cuenta_RV, lFila, "GRABADO") = gcFlg_SI Then
        If Not Fnt_Ope_Directa_Cont_RV(pGrilla:=Grilla_Operaciones_Sin_Cuenta_RV, _
                                       pFila:=lFila, _
                                       pNro_Orden:=GetCell(Grilla_Operaciones_Sin_Cuenta_RV, lFila, "N_ORDEN")) Then
          'GoTo ErrProcedure
        End If
      End If
    Next
  End With
  MsgBox "Grabaci�n de Movimientos Diarios de Renta Variable finalizada.", vbInformation
  
  Tab_Operaciones.TabEnabled(1) = True
  Tab_Operaciones.TabEnabled(2) = True
  
'  With Toolbar_Operaciones_RV
'    .Buttons("SAVE").Enabled = False
'    .Buttons("IMPORT").Enabled = True
'    .Buttons("SEL_ALL").Enabled = False
'    .Buttons("SEL_NOTHING").Enabled = False
'    .Buttons("REFRESH").Enabled = False
'  End With
  
  Call Grilla_Operaciones_RV.Select(0, 0)
  Call Grilla_Operaciones_Sin_Cuenta_RV.Select(0, 0)
  
  'Call Sub_Limpia_Objetos
  
  'Call Toolbar_ButtonClick("REFRESH")
End Sub

Private Function Fnt_Ope_Directa_Cont_RV(pGrilla As VSFlexGrid _
                                       , pFila As Long _
                                       , ByVal pNro_Orden As Long) As Boolean
Dim lFila As Long
Dim lcAcciones As Class_Acciones
Dim lId_Caja_Cuenta As Double
Dim lId_Nemotecnico As String
Dim lFecha_Movimiento As Date
Dim lFecha_Liquidacion As Date
Dim lId_Cuenta As String
Dim lId_Moneda As String
Dim lFecha As Date
Dim lMsg_Error As String
Dim lNum_Cuenta As String
'---------------------------------------
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
'---------------------------------------
Dim lId_Operacion As String
Dim lcOperaciones As Class_Operaciones
Dim lDetalle As Class_Operaciones_Detalle
Dim lRollback As Boolean
Dim lMonto_Operacion As Double
Dim lSuma_Monto_Neto As Double
'---------------------------------------
Dim lcIva As Class_Iva
Dim lcComisiones As Class_Comisiones_Instrumentos
Dim lPorcentaje_Comision As Double
Dim lGastos As Double
Dim lComision As Double
Dim lDerechos As Double
Dim lIva As Double
'---------------------------------------
Dim lCod_Instrumento As String
Dim lId_Operacion_detalle
'---------------------------------------
 
  lRollback = False
  gDB.IniciarTransaccion
  
  Set lcAlias = CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("fecha_operacion").Valor = DTP_Fecha_Proceso.Value
    .Campo("cod_producto").Valor = gcPROD_RV_NAC
    If Not .Anular_Operaciones_Pendientes Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en buscar el Nemot�cnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  Set lcOperaciones = Nothing
    
  lFila = pFila
  lId_Cuenta = GetCell(pGrilla, pFila, "id_cuenta")
  lNum_Cuenta = GetCell(pGrilla, pFila, "num_cuenta")
  lComision = 0
  lDerechos = 0
  lGastos = 0
  lIva = 0
  
  'Crea la Instancia de la clase
  Set lcAcciones = New Class_Acciones
  
  Rem Ingresa el movimiento
  'Esto tiene que recorrer toda la grilla devido a que los registros de una misma operacion no son concecutivas
  For lFila = pFila To (pGrilla.Rows - 1)
    If GetCell(pGrilla, lFila, "n_orden") = pNro_Orden Then
      lId_Nemotecnico = GetCell(pGrilla, lFila, "id_nemotecnico")
      '-----------------------------------------------------------------------------------------
      Rem VALIDACIONES
      lMsg_Error = ""
      
      lFecha_Movimiento = DTP_Fecha_Proceso.Value
      lFecha_Liquidacion = GetCell(pGrilla, pFila, "FECHA_LIQUIDACION")
      lCod_Instrumento = GetCell(pGrilla, lFila, "cod_instru")
      
      gDB.Parametros.Clear
      gDB.Procedimiento = "PKG_SECURITY.IMPORT_AVAIBLE"
      gDB.Parametros.Add "PID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
      gDB.Parametros.Add "PVALOR", ePT_Caracter, GetCell(pGrilla, lFila, "colum_pk"), ePD_Entrada
      gDB.Parametros.Add "PCOD_INSTRUMENTO", ePT_Caracter, lCod_Instrumento, ePD_Entrada
      gDB.Parametros.Add "PRETURN", ePT_Caracter, "", ePD_Salida
      If Not gDB.EjecutaSP Then
        lMsg_Error = gDB.ErrMsg
        GoTo ErrProcedure
      End If
            
      If Not gDB.Parametros("PRETURN").Valor = gcFlg_SI Then
        lMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
        GoTo ErrProcedure
      Rem Si el Id_Cuenta y Num_cuenta es vacio quiere decir que no tiene asociada una cuenta en GPI
      ElseIf lId_Cuenta = "" And lNum_Cuenta = "" Then
        lMsg_Error = "La Orden no tiene asociado una cuenta en el sistema, ya que ""Cuenta"" est� vacio." & vbCr & "No se puede operar la instrucci�n."
        GoTo ErrProcedure
      Rem Si el id_nemot�cnico es "0" quiere decir que no se encontr� el nemot�cnico en GPI
      ElseIf lId_Nemotecnico = "" Then
        lMsg_Error = "El Nemot�cnico '" & GetCell(pGrilla, lFila, "nemotecnico") & "' de la grilla ""Operaciones con Cuenta"" no est� en el sistema." & vbCr & "No se puede operar la instrucci�n."
        GoTo ErrProcedure
      Rem La Fecha de Liquidacion es vacia, no se puede operar
      ElseIf GetCell(pGrilla, lFila, "fecha_liquidacion") = "" Then
        lMsg_Error = "En el N�mero de Orden '" & GetCell(pGrilla, lFila, "colum_pk") & "' la Fecha de Liquidaci�n es vacia en la grilla ""Operaciones con Cuenta""." & vbCr & "No se puede operar la instrucci�n."
        GoTo ErrProcedure
      Rem Busca la moneda del nemot�cnico en CSGPI
      ElseIf Not Fnt_Buscar_Datos_Nemo(lId_Nemotecnico, lId_Moneda, lMsg_Error) Then
        GoTo ErrProcedure
      End If
      '-----------------------------------------------------------------------------
      
      Call lcAcciones.Agregar_Operaciones_Detalle(pId_Nemotecnico:=lId_Nemotecnico, _
                                        pCantidad:=GetCell(pGrilla, lFila, "Cantidad"), _
                                        pPrecio:=GetCell(pGrilla, lFila, "precio"), _
                                        pId_Moneda:=lId_Moneda, _
                                        pMonto:=GetCell(pGrilla, lFila, "monto"), _
                                        pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                        pPrecio_Historico:="")
      lSuma_Monto_Neto = lSuma_Monto_Neto + GetCell(pGrilla, lFila, "monto")
    
      Rem En el Archivo no vienen prorrateados los valores de comisiones => Se toma solo una vez
      lComision = lComision + To_Number(NVL(Trim(GetCell(pGrilla, lFila, "comision")), 0))
      lDerechos = lDerechos + To_Number(GetCell(pGrilla, lFila, "derecho"))
      lGastos = lGastos + To_Number(GetCell(pGrilla, lFila, "gastos"))
      lIva = lIva + To_Number(GetCell(pGrilla, lFila, "iva"))
    End If
  Next
  
  Rem Luego, se suman los montos netos de cada detalle con la comision
  Select Case GetCell(pGrilla, pFila, "flg_tipo_movimiento")
    Case gcTipoOperacion_Ingreso
      lMonto_Operacion = lSuma_Monto_Neto + lComision + lDerechos + lGastos + lIva
    Case gcTipoOperacion_Egreso
      lMonto_Operacion = lSuma_Monto_Neto - (lComision + lDerechos + lGastos + lIva)
  End Select

  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("id_moneda").Valor = lId_Moneda
    .Campo("cod_mercado").Valor = cMercado_Nacional
    If .Buscar_Caja_Para_Invertir() Then
      lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
    Else
      lMsg_Error = "Problemas en cargar Cajas de la Cuenta." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  '-----------------------------------------------------------------------------

  Rem Comisiones
  Set lcComisiones = New Class_Comisiones_Instrumentos
  Set lcIva = New Class_Iva
  With lcComisiones
    .Campo("Id_Cuenta").Valor = lId_Cuenta
    .Campo("COD_INSTRUMENTO").Valor = lCod_Instrumento
    If .Buscar(True) Then
      If .Cursor.Count > 0 Then
        lPorcentaje_Comision = lcIva.Porcentaje_Iva(.Cursor(1)("COMISION").Value)
      End If
    End If
  End With
  Set lcComisiones = Nothing
    
  '-----------------------------------------------------------------------------
  Rem Flag para que no tome en cuenta la cantidad de activos al ingresar una instruccion desde el Importador de movimientos
  Rem y para que ingrese en campos propios de importacion
  lcAcciones.fImportacion = True
  If Not lcAcciones.Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                        pId_cuenta:=lId_Cuenta, _
                                        pDsc_Operacion:="", _
                                        pTipoOperacion:=GetCell(pGrilla, pFila, "flg_tipo_movimiento"), _
                                        pId_Contraparte:="", _
                                        pId_Representante:="", _
                                        pId_Moneda_Operacion:=lId_Moneda, _
                                        pFecha_Operacion:=lFecha_Movimiento, _
                                        pFecha_Vigencia:=lFecha_Movimiento, _
                                        pFecha_Liquidacion:=lFecha_Liquidacion, _
                                        pId_Trader:="", _
                                        pPorc_Comision:=lPorcentaje_Comision, _
                                        pComision:=lComision, _
                                        pDerechos_Bolsa:=lDerechos, _
                                        pGastos:=lGastos, _
                                        pIva:=lIva, _
                                        pMonto_Operacion:=lMonto_Operacion, _
                                        pTipo_Precio:=cTipo_Precio_Mercado) Then
    lMsg_Error = "Problemas al grabar Acciones Nacionales." & vbCr & lcAcciones.ErrMsg
    GoTo ErrProcedure
  End If
  Set lcAcciones = Nothing
  '----------------------------------------------------------------------------

  Rem Con el nuevo id_operacion busca los detalles
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Id_Operacion").Valor = lId_Operacion
  
    Rem Busca el detalle de la operacion
    If Not .BuscaConDetalles Then
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  
    Rem Realiza la Confirmaci�n de la Instrucci�n
    If Not .Confirmar(lId_Caja_Cuenta) Then
      lMsg_Error = "Error en la confirmaci�n de la operaci�n." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  
    Rem Por cada id_operacion_detalle encontrado guarda la relacions
    Rem "nro_orden-id_operacion_detalle" en la tabla rel_conversiones
    lFila = pFila
    For Each lDetalle In lcOperaciones.Detalles
      For lFila = lFila To (pGrilla.Rows - 1)
        If GetCell(pGrilla, lFila, "n_orden") = pNro_Orden Then
          If Not Fnt_Guardar_Rel_Nro_Oper_Detalle_RV(GetCell(pGrilla, lFila, "colum_pk"), _
                                                      lDetalle.Campo("id_operacion_detalle").Valor, _
                                                      lMsg_Error) Then
            GoTo ErrProcedure
          End If
          lFila = lFila + 1
          Exit For
        End If
      Next
    Next
  End With

  lMsg_Error = "Operaci�n Ingresada correctamente."
  GoTo ExitProcedure
  
ErrProcedure:
  lRollback = True
  
ExitProcedure:
  For lFila = pFila To (pGrilla.Rows - 1)
    If To_Number(GetCell(pGrilla, lFila, "n_orden")) = pNro_Orden Then
      Call SetCell(pGrilla, lFila, "dsc_error", lMsg_Error)
      
      If lRollback Then
        pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbRed
        'Grilla_Operaciones_Sin_Cuenta_RV.Cell(flexcpChecked, lFila, "chk") = flexChecked
        Call SetCell(pGrilla, lFila, "chk", flexChecked)
      Else
        pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbBlue
        Call SetCell(pGrilla, lFila, "chk", flexUnchecked)
        Call SetCell(pGrilla, lFila, "grabado", gcFlg_SI, pAutoSize:=False)
      End If
    End If
  Next
  
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  
  Fnt_Ope_Directa_Cont_RV = Not lRollback
  
  Set lcAcciones = Nothing
End Function

Private Function Fnt_Buscar_Datos_Nemo(pId_Nemotecnico As String, _
                                       ByRef pId_Moneda_Transaccion, _
                                       ByRef pMsg_Error As String, _
                                       Optional ByRef pId_Moneda_Valorizacion, _
                                       Optional ByRef pFecha_Vencimiento, _
                                       Optional ByRef pCod_Instrumento) As Boolean
Dim lcAlias As Class_Alias
Dim lcnemot�cnico As Class_Nemotecnicos
  
  Fnt_Buscar_Datos_Nemo = True
  
  Set lcnemot�cnico = New Class_Nemotecnicos
  With lcnemot�cnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pId_Moneda_Transaccion = .Cursor(1)("id_moneda_transaccion").Value
        pId_Moneda_Valorizacion = .Cursor(1)("id_moneda").Value
        pFecha_Vencimiento = .Cursor(1)("fecha_vencimiento").Value
        pCod_Instrumento = .Cursor(1)("cod_instrumento").Value
      Else
        pMsg_Error = "nemot�cnico no tiene moneda asociada." & vbCr & "No se puede operar la instrucci�n."
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = "Problemas en cargar datos de nemot�cnico." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcnemot�cnico = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Buscar_Datos_Nemo = False
End Function

Private Function Fnt_Guardar_Rel_Nro_Oper_Detalle_RV(pNro_Orden As Variant, _
                                                  pId_Operacion_Detalle As String, _
                                                  pMsg_Error As String) As Boolean
Dim lId_Origen As String
Dim lId_Tipo_Conversion As String
Dim lcRel_Conversion As Object

  Fnt_Guardar_Rel_Nro_Oper_Detalle_RV = True
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_Sec_Mov_RV, False)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle, False)
  
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = lId_Origen
    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
    .Campo("valor").Valor = pNro_Orden
    Rem En "id_entidad" se guarda el id_operacion_detalle
    .Campo("id_entidad").Valor = pId_Operacion_Detalle
    If Not .Guardar Then
      pMsg_Error = "Problemas en guardar la relaci�n N�mero de Orden y el Detalle de la Operaci�n." & vbCr & .ErrMsg
      Fnt_Guardar_Rel_Nro_Oper_Detalle_RV = False
    End If
  End With
  Set lcRel_Conversion = Nothing
  
End Function

Private Function Fnt_Guardar_Rel_Nro_Oper_Detalle_RF(pNro_Orden As Variant, _
                                                    pId_Operacion_Detalle As String, _
                                                    pMsg_Error As String) As Boolean
Dim lId_Origen As String
Dim lId_Tipo_Conversion As String
Dim lcRel_Conversion As Object

  Fnt_Guardar_Rel_Nro_Oper_Detalle_RF = True
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_Sec_Mov_RF, False)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle, False)
  
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = lId_Origen
    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
    .Campo("valor").Valor = pNro_Orden
    Rem En "id_entidad" se guarda el id_operacion_detalle
    .Campo("id_entidad").Valor = pId_Operacion_Detalle
    If Not .Guardar Then
      pMsg_Error = "Problemas en guardar la relaci�n N�mero de Orden y el Detalle de la Operaci�n." & vbCr & .ErrMsg
      Fnt_Guardar_Rel_Nro_Oper_Detalle_RF = False
    End If
  End With
  Set lcRel_Conversion = Nothing
  
End Function

Private Function Fnt_Guardar_Rel_Nro_Oper_Detalle_FM(pNro_Orden As Variant, _
                                                    pId_Operacion_Detalle As String, _
                                                    pMsg_Error As String) As Boolean
Dim lId_Origen As String
Dim lId_Tipo_Conversion As String
Dim lcRel_Conversion As Object

  Fnt_Guardar_Rel_Nro_Oper_Detalle_FM = True
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_Sec_Mov_FFMM, False)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle, False)
  
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = lId_Origen
    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
    .Campo("valor").Valor = pNro_Orden
    .Campo("id_entidad").Valor = pId_Operacion_Detalle 'En "id_entidad" se guarda el id_operacion_detalle
    If Not .Guardar Then
      pMsg_Error = "Problemas en guardar la relaci�n N�mero de Orden y el Detalle de la Operaci�n." & vbCr & .ErrMsg
      Fnt_Guardar_Rel_Nro_Oper_Detalle_FM = False
    End If
  End With
  Set lcRel_Conversion = Nothing
  
End Function

Private Function Fnt_Buscar_Rel_Nro_Oper_Detalle_RF(pNro_Orden _
                                                  , ByRef pMsg_Error As String _
                                                  ) As Boolean
Dim lId_Origen As String
Dim lId_Tipo_Conversion As String
Dim lcRel_Conversion As Object
    
  Fnt_Buscar_Rel_Nro_Oper_Detalle_RF = False
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_Sec_Mov_RF)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle)
    
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = lId_Origen
    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
    .Campo("valor").Valor = pNro_Orden
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
        Fnt_Buscar_Rel_Nro_Oper_Detalle_RF = True
      End If
    Else
      pMsg_Error = "Problemas en buscar la relaci�n N�mero Documento y el Detalle de la Operaci�n." & vbCr & .ErrMsg
      Fnt_Buscar_Rel_Nro_Oper_Detalle_RF = True
    End If
  End With
  Set lcRel_Conversion = Nothing
End Function

Private Function Fnt_Buscar_Rel_Nro_Oper_Detalle_FM(pNro_Orden _
                                                , ByRef pMsg_Error As String _
                                                ) As Boolean
Dim lId_Origen As String
Dim lId_Tipo_Conversion As String
Dim lcRel_Conversion As Object
    
  Fnt_Buscar_Rel_Nro_Oper_Detalle_FM = False
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_Sec_Mov_FFMM)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle)
    
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = lId_Origen
    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
    .Campo("valor").Valor = pNro_Orden
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
        Fnt_Buscar_Rel_Nro_Oper_Detalle_FM = True
      End If
    Else
      pMsg_Error = "Problemas en buscar la relaci�n N�mero Documento y el Detalle de la Operaci�n." & vbCr & .ErrMsg
      Fnt_Buscar_Rel_Nro_Oper_Detalle_FM = True
    End If
  End With
  Set lcRel_Conversion = Nothing
End Function


Private Sub Sub_Importa_RF()
Dim lcNemotecnicos As Class_Nemotecnicos
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lCursor As hRecord
Dim lCampo As hFields
'---------------------------------------
Dim lGrilla As VSFlexGrid
'---------------------------------------
Dim lId_Nemotecnico
Dim lId_Cuenta
Dim lId_Asesor_
Dim lNum_Cuenta
Dim lFolioContraparte
Dim lId_Operacion_detalle
Dim lCod_Instrumento      As String
Dim lTipo_Movimiento      As String
Dim lFlg_Tipo_Movimiento  As String
'----------------------------------------------
Dim lLinea      As Long
Dim lMsg_Error  As String
Dim lNombre_Cliente As String
Dim lMontos_Gastos
Dim lcAlias As Object
Dim lFlg_Estado   As String
Dim lDsc_Bloqueo  As String


On Error GoTo ErrProcedure
  
  Me.Enabled = False
  Call Sub_Bloquea_Puntero(Me)
  
  Set lcNemotecnicos = New Class_Nemotecnicos
  'Set lcCuenta = New Class_Cuentas
  
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  Set lcAlias = CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  gDB.Parametros.Clear
  'gDB.Procedimiento = "CisCB&DBO&AD_QRY_CB_MOVTO_RFIJA"
  gDB.Procedimiento = "PKG_PROCESOS_IMPORTADOR$CisCB_CB_MOVTO_RFIJA"
  gDB.Parametros.Add "pFecha_movimientos", ePT_Caracter, Format(DTP_Fecha_Proceso.Value, "YYYYMMDD"), ePD_Entrada
  gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
  If Not gDB.EjecutaSP Then
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
    GoTo ExitProcedure
  End If
  
  Set lCursor = gDB.Parametros("pcursor").Valor
    
  If lCursor.Count <= 0 Then
    GoTo ExitProcedure
  End If
      
  BarraProceso.Max = lCursor.Count
  For Each lCampo In lCursor
    BarraProceso.Value = lCampo.Index
    
    lFolioContraparte = Trim(lCampo("NUMERO_ORDEN").Value) & "/" & Trim(lCampo("CORRELATIVO").Value)
    
'    lId_Operacion_detalle = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Mov_RF _
'                                                    , pCodigoCSBPI:=cTabla_Operacion_Detalle _
'                                                    , pValor:=lFolioContraparte)
'
'    If Not IsNull(lId_Operacion_detalle) Then
'      'pMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
'      GoTo ProximoMovimiento
'    End If

    lId_Nemotecnico = ""
    lCod_Instrumento = ""

    '------- RESCATA EL ID_nemot�cnico
    If Mid(lCampo("NEMOTECNICO").Value, 1, 7) = "PACTO-C" Then
        lId_Nemotecnico = "0"
        lCod_Instrumento = gcINST_PACTOS_NAC
    ElseIf Mid(lCampo("NEMOTECNICO").Value, 1, 2) = "DU" Or _
        Mid(lCampo("NEMOTECNICO").Value, 1, 2) = "DO" Or _
        Mid(lCampo("NEMOTECNICO").Value, 1, 2) = "D$" Or _
        Mid(lCampo("NEMOTECNICO").Value, 1, 4) = "PDBC" Or _
        Mid(lCampo("NEMOTECNICO").Value, 1, 4) = "PRBC" Then
        lId_Nemotecnico = "0"
        lCod_Instrumento = gcINST_DEPOSITOS_NAC
    Else
        Set lcNemotecnicos = New Class_Nemotecnicos
        lcNemotecnicos.Campo("nemotecnico").Valor = lCampo("NEMOTECNICO").Value
        lCod_Instrumento = ""
        Rem Busca el id_nemotecnico del Nemotecnico de la planilla
        If lcNemotecnicos.Buscar_Nemotecnico(pCod_Producto:=Null, pMostrar_Msg:=False) Then
            lId_Nemotecnico = lcNemotecnicos.Campo("id_nemotecnico").Valor
            If Not lId_Nemotecnico = "0" Then
            'Rem Busca atributos del Nemotecnico
                If lcNemotecnicos.Buscar Then
                    lCod_Instrumento = NVL(lcNemotecnicos.Cursor(1)("cod_instrumento").Value, "")
                Else
                End If
            End If
        End If
        Set lcNemotecnicos = Nothing
    End If
    '------- FIN RESCATA EL ID_NEMOTECNICO
'    With lcNemotecnicos
'      .Campo("nemotecnico").Valor = Trim(lCampo("nemotecnico").Value)
'      If Not .Buscar() Then
'        Call Fnt_MsgError(.SubTipo_LOG, _
'                          "Problemas en buscar el Nemot�cnico.", _
'                          .ErrMsg, _
'                          pConLog:=True)
'        GoTo ExitProcedure
'      End If
'
'      If .Cursor.Count > 0 Then
'        lId_nemotecnico = .Cursor(1)("id_nemotecnico").Value
'        lCod_Instrumento = .Cursor(1)("cod_instrumento").Value
'      End If
'    End With
'    '------- FIN RESCATA EL ID_nemot�cnico
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_SECURITY.IMPORT_AVAIBLE"
    gDB.Parametros.Add "PID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    gDB.Parametros.Add "PVALOR", ePT_Caracter, lFolioContraparte, ePD_Entrada
    gDB.Parametros.Add "PCOD_INSTRUMENTO", ePT_Caracter, lCod_Instrumento, ePD_Entrada
    gDB.Parametros.Add "PRETURN", ePT_Caracter, "", ePD_Salida
    If Not gDB.EjecutaSP Then
      Call Fnt_MsgError(eLS_ErrSystem, _
                        "Problemas para comprobar si esta disponible la importaci�n.", _
                        gDB.ErrMsg, _
                        pConLog:=True)
      GoTo ProximoMovimiento
    End If
    
    If Not gDB.Parametros("PRETURN").Valor = gcFlg_SI Then
      'SI ENTREGA UN VALOR DISTINTO A "S" SIGNIFICA QUE NO TIENE PERMITIDO EL INGRESO
      GoTo ProximoMovimiento
    End If
    
      
    lTipo_Movimiento = ""
    Select Case lCampo("TIPO_OPERACION").Value
      Case cTipOp_Compra
        lTipo_Movimiento = "Compra"
        lFlg_Tipo_Movimiento = "I"
      Case cTipOp_Venta
        lTipo_Movimiento = "Venta"
        lFlg_Tipo_Movimiento = "E"
      Case Else
        'Si un tipo de movimiento no esta contemplado se salta
        GoTo ProximoMovimiento
    End Select
      
    lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Magic_Valores _
                                          , pCodigoCSBPI:=cTabla_Cuentas _
                                          , pValor:=NVL(lCampo("RUT_CLIENTE").Value, ""))
                                          
'    lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Magic_Valores _
'                                          , pCodigoCSBPI:=cTabla_Cuentas _
'                                          , pValor:=NVL("12246935-2", ""))

    
    lNum_Cuenta = ""
    lNombre_Cliente = ""
    lid_Empresa = 0
    If IsNull(lId_Cuenta) Then
      Set lGrilla = Grilla_Operaciones_Sin_Cuenta_RF
      lNombre_Cliente = lCampo("nombre_cliente").Value
      lId_Asesor_ = 0
    Else
      Set lGrilla = Grilla_Operaciones_RF
      
      ' ----------- RESCATA NUMERO CUENTA
      lcCuenta.Campo("id_cuenta").Valor = lId_Cuenta
      If lcCuenta.Buscar(pEnVista:=True) Then
        lNum_Cuenta = lcCuenta.Cursor(1)("num_cuenta").Value
        lNombre_Cliente = lcCuenta.Cursor(1)("nombre_cliente").Value
        lId_Asesor_ = NVL(lcCuenta.Cursor(1)("id_asesor").Value, 0)
        lFlg_Estado = lcCuenta.Cursor(1)("cod_estado").Value
        lDsc_Bloqueo = ""
        lid_Empresa = lcCuenta.Cursor(1)("id_empresa").Value
        If lFlg_Estado = "H" Then
            If lcCuenta.Cursor(1)("flg_bloqueado").Value = "S" Then
                lFlg_Estado = "B"
                lDsc_Bloqueo = lcCuenta.Cursor(1)("obs_bloqueo").Value
            End If
        Else
            lDsc_Bloqueo = "Cuenta Deshabilitada"
        End If
        If lid_Empresa <> Fnt_EmpresaActual Then
            GoTo ProximoMovimiento
        End If
      End If
    End If
    lLinea = lGrilla.Rows
    Call lGrilla.AddItem("")
    Call SetCell(lGrilla, lLinea, "colum_pk", Trim(lCampo("NUMERO_ORDEN").Value) & "/" & Trim(lCampo("CORRELATIVO").Value), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "n_orden", lCampo("NUMERO_ORDEN").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_linea", NVL(lCampo("CORRELATIVO").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "rut", Trim(lCampo("RUT_CLIENTE").Value), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "nombre_cliente", lNombre_Cliente, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_cuenta", "" & lId_Cuenta, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "num_cuenta", "" & lNum_Cuenta, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "tipo_movimiento", lTipo_Movimiento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "flg_tipo_movimiento", lFlg_Tipo_Movimiento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_nemotecnico", lId_Nemotecnico, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "nemotecnico", lCampo("nemotecnico").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "Cantidad", lCampo("CANTIDAD").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "dsc_cliente", Left(lNombre_Cliente, 30), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "precio", lCampo("PRECIO").Value, pAutoSize:=False)
    
    lMontos_Gastos = NVL(lCampo("COMISION").Value, 0) + NVL(lCampo("DERECHO").Value, 0) + NVL(lCampo("GASTOS").Value, 0) + NVL(lCampo("IVA").Value, 0)
    
    Call SetCell(lGrilla, lLinea, "comision", NVL(lCampo("COMISION").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "Derecho", NVL(lCampo("DERECHO").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "gastos", NVL(lCampo("GASTOS").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "iva", NVL(lCampo("IVA").Value, 0), pAutoSize:=False)
    
    
    Call SetCell(lGrilla, lLinea, "total_gastos", NVL(lMontos_Gastos, 0), pAutoSize:=False)
        
    Call SetCell(lGrilla, lLinea, "monto", NVL(lCampo("MONTO_OPERACION").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "fecha_liquidacion", NVL(lCampo("FECHA_LIQUIDACION").Value, ""), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "cod_instru", lCod_Instrumento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "Grabado", gcFlg_NO, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_asesor", lId_Asesor_, pAutoSize:=False)
    
    Call SetCell(lGrilla, lLinea, "flg_bloqueado", lFlg_Estado, pAutoSize:=False)

    Call SetCell(lGrilla, lLinea, "id_mov_activo", NVL(lCampo("Id_Mov_Activo").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "dsc_error", lDsc_Bloqueo, pAutoSize:=False)
    
ProximoMovimiento:
        'este label sirve para saltarce el ingreso de un de operacion con un tipo de movimiento que no corresponde,
        'como los ingresos de custodias, la otra forma es hacer un if gigante que no quiero hacer :-P
        'HAF: 20071108
  Next
    
  Call Sub_AjustaColumnas_Grilla(Grilla_Operaciones_Sin_Cuenta_RF)
  Call Sub_AjustaColumnas_Grilla(Grilla_Operaciones_RF)

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la importaci�n de los movimientos." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcCuenta = Nothing
  Set lcNemotecnicos = Nothing
  Set lcAlias = Nothing

  gDB.Parametros.Clear
  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Graba_Operaciones_con_Cta_RF()
Dim lFila As Long
    
  With Grilla_Operaciones_RF
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked And _
        Not GetCell(Grilla_Operaciones_RF, lFila, "GRABADO") = gcFlg_SI Then
        If Not Fnt_Ope_Directa_Cont_RF(pGrilla:=Grilla_Operaciones_RF, _
                                       pFila:=lFila, _
                                       pNro_Orden:=GetCell(Grilla_Operaciones_RF, lFila, "n_orden"), _
                                       pLinea:=GetCell(Grilla_Operaciones_RF, lFila, "id_linea")) Then
          'GoTo ErrProcedure
        End If

      End If
    Next
  End With
  
  With Grilla_Operaciones_Sin_Cuenta_RF
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      'Modificado por MMA. 12/01/09. Error en asociaci�n de par�metro. Se especificaba
      'Grilla_Operaciones_RF para obtener "id_linea" en vez de Grilla_Operaciones_Sin_Cuenta_RF
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked And _
        Not GetCell(Grilla_Operaciones_Sin_Cuenta_RF, lFila, "GRABADO") = gcFlg_SI Then
        If Not Fnt_Ope_Directa_Cont_RF(pGrilla:=Grilla_Operaciones_Sin_Cuenta_RF, _
                                       pFila:=lFila, _
                                       pNro_Orden:=GetCell(Grilla_Operaciones_Sin_Cuenta_RF, lFila, "n_orden"), _
                                       pLinea:=GetCell(Grilla_Operaciones_Sin_Cuenta_RF, lFila, "id_linea")) Then

          'GoTo ErrProcedure
        End If
      End If
    Next
  End With
  MsgBox "Grabaci�n de Movimientos Diarios de Renta Fija finalizada.", vbInformation
  
  Tab_Operaciones.TabEnabled(0) = True
  Tab_Operaciones.TabEnabled(2) = True
  
  Call Grilla_Operaciones_RF.Select(0, 0)
  Call Grilla_Operaciones_Sin_Cuenta_RF.Select(0, 0)
  
  'Call Sub_Limpia_Objetos
  
  'Call Toolbar_ButtonClick("REFRESH")
End Sub

Private Function Fnt_Ope_Directa_Cont_RF(pGrilla As VSFlexGrid _
                                       , pFila As Long _
                                       , ByVal pNro_Orden _
                                       , ByVal pLinea As Long) As Boolean
Dim lcBonos As Class_Bonos
Dim lcDepositos As Class_Depositos
Dim lcPactos As Class_Pactos
'------------------------------------------------
Dim lId_Caja_Cuenta As Double
Dim lId_Nemotecnico As String
Dim lId_Mov_Activo As String
Dim lFecha_Movimiento As Date
Dim lFecha_Liquidacion As Date
Dim lMsg_Error As String
'---------------------------------------
Dim lcCliente As Object
Dim lRut_Cliente As String
Dim lId_Cliente As String
'---------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lId_Cuenta
Dim lNum_Cuenta
Dim lMov_Activo
'---------------------------------------
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
'--------------------------------------- NUEVOS
Dim lRut_Emisor As String
Dim lMoneda_Operacion As String
Dim lMoneda As String
Dim lTipo_Reajuste As String
Dim lFecha_Vencimiento As Date
Dim lId_Moneda_Pago As String
'---------------------------------------
Dim lId_Operacion As String
Dim lcOperaciones As Class_Operaciones
Dim lDetalle As Class_Operaciones_Detalle
Dim lRollback As Boolean
Dim lMonto_Neto As Double
Dim lMonto_Operacion As Double
Dim lId_Moneda_Deposito
'Dim lFecha_Vencimiento
'Dim lFecha As Date
Dim lBase
'---------------------------------------
Dim lcIva As Class_Iva
Dim lcComisiones As Class_Comisiones_Instrumentos
Dim lPorcentaje_Comision As Double
Dim lGastos As Double
Dim lPorcentaje_Derechos As Double
Dim lComision As Double
Dim lDerechos As Double
Dim lIva As Double
'---------------------------------------
Dim lFila As Long
Dim lRecord_Detalle As hRecord
Dim lhArctivos      As hRecord
Dim lReg_Activo     As hFields
Dim lReg As hFields
Dim pFila_Cont As Long
Dim lCod_Instrumento As String
 
    
  Set lRecord_Detalle = New hRecord
  With lRecord_Detalle
      .ClearFields
      .AddField "id_nemotecnico", 0
      .AddField "CANTIDAD", 0
      .AddField "PRECIO", 0
      .AddField "MONTO_DETALLE", 0
      .AddField "ID_MONEDA"
      .AddField "ID_MOV_ACTIVO"
      .AddField "COMISION", 0
      .AddField "DERECHO", 0
      .AddField "GASTOS", 0
      .AddField "IVA", 0
      .AddField "MONTO_OPERACION", 0
      .AddField "fecha_vencimiento"
      .AddField "Base"
      .LimpiarRegistros
  End With

  lRollback = False
  gDB.IniciarTransaccion
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Rem Realiza la operacion directa

  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("fecha_operacion").Valor = DTP_Fecha_Proceso.Value
    .Campo("cod_producto").Valor = gcPROD_RF_NAC
    If Not .Anular_Operaciones_Pendientes Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en buscar el Nemot�cnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  Set lcOperaciones = Nothing
    
  lFila = pFila
  lId_Cuenta = GetCell(pGrilla, pFila, "id_cuenta")
  lNum_Cuenta = GetCell(pGrilla, pFila, "num_cuenta")
  lFecha_Movimiento = DTP_Fecha_Proceso.Value
  lFecha_Liquidacion = GetCell(pGrilla, pFila, "fecha_liquidacion")
  lCod_Instrumento = GetCell(pGrilla, pFila, "cod_instru")
  lId_Mov_Activo = GetCell(pGrilla, pFila, "Id_Mov_Activo")
  
  Rem Ingresa el movimiento
  Do While To_Number(GetCell(pGrilla, lFila, "n_orden")) = pNro_Orden And To_Number(GetCell(pGrilla, lFila, "id_linea")) = pLinea
  
'    lFecha = GetCell(pGrilla, lFila, "fecha")
    lId_Nemotecnico = GetCell(pGrilla, lFila, "id_nemotecnico")

    '-----------------------------------------------------------------------------------------
    Rem VALIDACIONES
    lMsg_Error = ""
    Rem Valida que no se est� ingresando la operacion de nuevo
    If Not Fnt_Buscar_Rel_Nro_Oper_Detalle(GetCell(pGrilla, lFila, "colum_pk"), lMsg_Error) Then
      lMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
      GoTo ErrProcedure
    Rem La Fecha que viene en el Archivo Plano tiene q ser igual a la Fecha Proceso
    Rem Si el Id_Cuenta y Num_cuenta es vacio quiere decir que no tiene asociada una cuenta en GPI
    ElseIf lId_Cuenta = "" And lNum_Cuenta = "" Then
      lMsg_Error = "La Orden no tiene asociado una cuenta en el sistema. El campo ""Cuenta"" est� vacio." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Si el id_nemot�cnico es "0" quiere decir que no se encontr� el nemot�cnico en GPI
    ElseIf lId_Nemotecnico = "0" Then
      Rem Si no encontro el nemotecnico, se crea. Solo para depositos y pactos
      If lCod_Instrumento = gcINST_DEPOSITOS_NAC Or lCod_Instrumento = gcINST_PACTOS_NAC Then
        If Not Fnt_VerificaCreacion_Nemo(pNemotecnico:=GetCell(pGrilla, lFila, "nemotecnico") _
                                      , pFecha_Movimiento:=lFecha_Movimiento _
                                      , pCod_Instrumento:=lCod_Instrumento _
                                      , pTasa:=To_Number(GetCell(pGrilla, pFila, "precio")) _
                                      , pId_Nemotecnico:=lId_Nemotecnico _
                                      , PId_Moneda_Deposito:=lId_Moneda_Deposito _
                                      , pId_Moneda_Pago:=lId_Moneda_Pago _
                                      , pFecha_Vencimiento:=lFecha_Vencimiento _
                                      , pBase:=lBase _
                                      , pMsgError:=lMsg_Error) Then
            lMsg_Error = lMsg_Error & vbCr & "El Nemot�cnico '" & GetCell(pGrilla, lFila, "nemotecnico") & "' de la grilla ""Operaciones con Cuenta"" no est� en el sistema." & vbCr & "No se puede operar la instrucci�n."
            GoTo ErrProcedure
        End If
      Else
        lMsg_Error = "El Nemot�cnico '" & GetCell(pGrilla, lFila, "NEMOTECNICO") & "' no est� en el sistema." & vbCr & "No se puede operar la instrucci�n."
        GoTo ErrProcedure
      End If
    End If
    Rem La Fecha de Liquidacion es vacia, no se puede operar
    If GetCell(pGrilla, lFila, "fecha_liquidacion") = "" Then
      lMsg_Error = "La Fecha de Liquidaci�n no puede estar vacia. No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem La Fecha de Liquidacion es "00/00/0000", no se puede operar
    ElseIf GetCell(pGrilla, lFila, "fecha_liquidacion") = "00/00/0000" Then
      lMsg_Error = "La Fecha de Liquidaci�n no es v�lida. No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Busca la moneda del nemot�cnico en CSGPI
    ElseIf Not Fnt_Buscar_Datos_Nemo_RF(lId_Nemotecnico, lId_Moneda_Pago, lFecha_Vencimiento, lMsg_Error) Then
      GoTo ErrProcedure
    End If
    '-----------------------------------------------------------------------------
'    '-----------------------------------------------------------------------------
  
    Set lReg = lRecord_Detalle.Add
    lReg("id_nemotecnico").Value = lId_Nemotecnico
    lReg("CANTIDAD").Value = To_Number(GetCell(pGrilla, lFila, "Cantidad"))
    lReg("PRECIO").Value = To_Number(GetCell(pGrilla, lFila, "precio"))
    lReg("MONTO_DETALLE").Value = To_Number(GetCell(pGrilla, lFila, "monto"))
    lReg("ID_MONEDA").Value = lId_Moneda_Pago
    lReg("COMISION").Value = To_Number(GetCell(pGrilla, lFila, "comision"))
    lReg("DERECHO").Value = To_Number(GetCell(pGrilla, lFila, "Derecho"))
    lReg("GASTOS").Value = To_Number(GetCell(pGrilla, lFila, "gastos"))
    lReg("IVA").Value = To_Number(GetCell(pGrilla, lFila, "IVA"))
    lReg("MONTO_OPERACION").Value = To_Number(GetCell(pGrilla, lFila, "monto"))
    lReg("fecha_vencimiento").Value = lFecha_Vencimiento
    lReg("Base").Value = lBase
    lReg("ID_MOV_ACTIVO").Value = To_Number(GetCell(pGrilla, lFila, "id_Mov_Activo")) 'lid_Mov_Activo
    lFila = lFila + 1
    If lFila > pGrilla.Rows - 1 Then
        Exit Do
    End If
  Loop
  
  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("id_moneda").Valor = lId_Moneda_Pago
    .Campo("cod_mercado").Valor = cMercado_Nacional
    If .Buscar_Caja_Para_Invertir() Then
      lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
    Else
      lMsg_Error = "Problemas en cargar Cajas de la Cuenta." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  '-----------------------------------------------------------------------------
  
  Rem Comisiones
  Set lcComisiones = New Class_Comisiones_Instrumentos
  Set lcIva = New Class_Iva
  With lcComisiones
      .Campo("Id_Cuenta").Valor = lId_Cuenta
      .Campo("COD_INSTRUMENTO").Valor = lCod_Instrumento
      If .Buscar(True) Then
          If .Cursor.Count > 0 Then
              lPorcentaje_Comision = lcIva.Porcentaje_Iva(.Cursor(1)("COMISION").Value)
          End If
      End If
  End With
  Set lcComisiones = Nothing
  '-----------------------------------------------------------------------------
  
  For Each lReg In lRecord_Detalle
    Select Case lCod_Instrumento
      Case gcINST_BONOS_NAC
      Set lcBonos = New Class_Bonos
        With lcBonos
          If GetCell(pGrilla, pFila, "flg_tipo_movimiento") = gcTipoOperacion_Ingreso Then
            Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                              pCantidad:=lReg("cantidad").Value, _
                                              pTasa:=lReg("precio").Value, _
                                              pId_Moneda:=lReg("id_moneda").Value, _
                                              pMonto:=lReg("monto_detalle").Value, _
                                              pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                              pUtilidad:=0, _
                                              PTasa_Gestion:=lReg("precio").Value, _
                                              pTasa_Historico:=lReg("precio").Value, _
                                              pId_Mov_Activo_Compra:=Null)
            lComision = lComision + lReg("COMISION").Value
            lDerechos = lDerechos + lReg("DERECHO").Value
            lGastos = lGastos + lReg("GASTOS").Value
            lIva = lIva + lReg("IVA").Value
            lMonto_Operacion = lReg("MONTO_OPERACION").Value
          Else
            If Not Fnt_Enlaza_VentasCompras_RF(pCod_Instrumento:=lCod_Instrumento, _
                                               pId_cuenta:=lId_Cuenta, _
                                               pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                               pCantidad:=lReg("cantidad").Value, _
                                               pFecha_Movimiento:=lFecha_Movimiento, _
                                               phActivos:=lhArctivos, _
                                               pId_Mov_Activo:=lReg("id_mov_activo").Value) Then
              Call Fnt_MsgError(.SubTipo_LOG, _
                                "Problemas en buscar el enlaze para las ventas de RF.", _
                                .ErrMsg, _
                                pConLog:=True)
              GoTo ErrProcedure
            End If
            If lhArctivos.Count > 0 Then
                For Each lReg_Activo In lhArctivos
                  Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                    pCantidad:=lReg_Activo("asignado").Value, _
                                                    pTasa:=lReg("precio").Value, _
                                                    pId_Moneda:=lReg("id_moneda").Value, _
                                                    pMonto:=lReg("monto_detalle").Value, _
                                                    pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                    pUtilidad:=0, _
                                                    PTasa_Gestion:=lReg("precio").Value, _
                                                    pTasa_Historico:=lReg("precio").Value, _
                                                    pId_Mov_Activo_Compra:=lReg_Activo("Id_Mov_Activo").Value)
                  lComision = lComision + lReg("COMISION").Value
                  lDerechos = lDerechos + lReg("DERECHO").Value
                  lGastos = lGastos + lReg("GASTOS").Value
                  lIva = lIva + lReg("IVA").Value
                  lMonto_Operacion = lReg("MONTO_OPERACION").Value
                Next
            Else
                lMsg_Error = "No se encuentran activos para vender o Cortes no corresponden. No se puede ingresar la instrucci�n."
                GoTo ErrProcedure
            End If
          End If
          
          If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                            pId_cuenta:=lId_Cuenta, _
                                            pDsc_Operacion:="", _
                                            pTipoOperacion:=GetCell(pGrilla, pFila, "flg_tipo_movimiento"), _
                                            pId_Contraparte:="", _
                                            pId_Representante:="", _
                                            pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                            pFecha_Operacion:=lFecha_Movimiento, _
                                            pFecha_Liquidacion:=lFecha_Liquidacion, _
                                            pId_Trader:="", _
                                            pPorc_Comision:=lPorcentaje_Comision, _
                                            pComision:=lComision, _
                                            pDerechos_Bolsa:=lDerechos, _
                                            pGastos:=lGastos, _
                                            pIva:=lIva, _
                                            pMonto_Operacion:=lMonto_Operacion, _
                                            pTipo_Precio:=cTipo_Precio_Mercado) Then
              lRollback = True
              Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas al grabar.", _
                          .ErrMsg, _
                          pConLog:=True)
              GoTo ErrProcedure
          End If
        End With
        Rem Con el nuevo id_operacion busca los detalles
        Set lcOperaciones = New Class_Operaciones
        With lcOperaciones
          .Campo("Id_Operacion").Valor = lId_Operacion
        
          Rem Busca el detalle de la operacion
          If Not .BuscaConDetalles Then
            lRollback = True
            GoTo ErrProcedure
          End If
          
          Rem Realiza la Confirmaci�n de la Instrucci�n
          If Not .Confirmar(lId_Caja_Cuenta) Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Error en la confirmaci�n de la operaci�n.", _
                              .ErrMsg, _
                              pConLog:=True)
            GoTo ErrProcedure
          End If
        
          lFila = pFila
          For Each lDetalle In lcOperaciones.Detalles
            For lFila = lFila To (pGrilla.Rows - 1)
              If GetCell(pGrilla, lFila, "n_orden") = pNro_Orden And GetCell(pGrilla, lFila, "id_linea") = pLinea Then
                If Not Fnt_Guardar_Rel_Nro_Oper_Detalle_RF(GetCell(pGrilla, lFila, "colum_pk"), _
                                                           lDetalle.Campo("id_operacion_detalle").Valor, _
                                                           lMsg_Error) Then
                  GoTo ErrProcedure
                End If
                lFila = lFila + 1
                Exit For
              End If
            Next
          Next
        End With
        Set lcOperaciones = Nothing
        
      Case gcINST_DEPOSITOS_NAC
        Set lcDepositos = New Class_Depositos
        With lcDepositos
          If GetCell(pGrilla, pFila, "flg_tipo_movimiento") = gcTipoOperacion_Ingreso Then
            Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                              pCantidad:=lReg("CANTIDAD").Value, _
                                              pTasa:=lReg("precio").Value, _
                                              PTasa_Gestion:=lReg("precio").Value, _
                                              pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                              pBase:=lReg("Base").Value, _
                                              pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                              pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                              pMonto_Pago:=lReg("monto_detalle").Value, _
                                              pReferenciado:="F", _
                                              pTipo_Deposito:="", _
                                              pFecha_Valuta:=lFecha_Liquidacion, _
                                              pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                              pTasa_Historico:=lReg("precio").Value, _
                                              pId_Mov_Activo_Compra:=Null)
            lComision = lComision + lReg("COMISION").Value
            lDerechos = lDerechos + lReg("DERECHO").Value
            lGastos = lGastos + lReg("GASTOS").Value
            lIva = lIva + lReg("IVA").Value
            lMonto_Operacion = lReg("MONTO_OPERACION").Value
          Else
            If Not Fnt_Enlaza_VentasCompras_RF(pCod_Instrumento:=lCod_Instrumento, _
                                               pId_cuenta:=lId_Cuenta, _
                                               pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                pCantidad:=lReg("cantidad").Value, _
                                               pFecha_Movimiento:=lFecha_Movimiento, _
                                               phActivos:=lhArctivos, _
                                               pId_Mov_Activo:=lReg("id_mov_activo").Value) Then
              Call Fnt_MsgError(0, _
                                "Problemas en buscar el enlaze para las ventas de RF.", _
                                "", _
                                pConLog:=True)
              GoTo ErrProcedure
            End If
            If lhArctivos.Count > 0 Then
              For Each lReg_Activo In lhArctivos
                Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                  pCantidad:=lReg_Activo("asignado").Value, _
                                                  pTasa:=lReg("precio").Value, _
                                                  PTasa_Gestion:=lReg("precio").Value, _
                                                  pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                                  pBase:=lReg("Base").Value, _
                                                  pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                                  pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                                  pMonto_Pago:=lReg("monto_detalle").Value, _
                                                  pReferenciado:="F", _
                                                  pTipo_Deposito:="", _
                                                  pFecha_Valuta:=lFecha_Liquidacion, _
                                                  pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                  pTasa_Historico:=lReg("precio").Value, _
                                                  pId_Mov_Activo_Compra:=lReg_Activo("Id_Mov_Activo").Value)
    
                lComision = lComision + lReg("COMISION").Value
                lDerechos = lDerechos + lReg("DERECHO").Value
                lGastos = lGastos + lReg("GASTOS").Value
                lIva = lIva + lReg("IVA").Value
                lMonto_Operacion = lReg("MONTO_OPERACION").Value
              Next
            Else
              lMsg_Error = "No se encuentran activos para vender o Cortes no corresponden. No se puede ingresar la instrucci�n."
              GoTo ErrProcedure
            End If
          End If
          
          If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                            pId_cuenta:=lId_Cuenta, _
                                            pDsc_Operacion:="", _
                                            pTipoOperacion:=GetCell(pGrilla, pFila, "flg_tipo_movimiento"), _
                                            pId_Contraparte:="", _
                                            pId_Representante:="", _
                                            pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                            pFecha_Operacion:=lFecha_Movimiento, _
                                            pFecha_Liquidacion:=lFecha_Liquidacion, _
                                            pId_Trader:="", _
                                            pPorc_Comision:=lPorcentaje_Comision, _
                                            pComision:=lComision, _
                                            pDerechos_Bolsa:=lDerechos, _
                                            pGastos:=lGastos, _
                                            pIva:=lIva, _
                                            pMonto_Operacion:=lMonto_Operacion) Then
              lRollback = True
              Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas al grabar.", _
                          .ErrMsg, _
                          pConLog:=True)
              GoTo ErrProcedure
          End If
        End With
        Rem Con el nuevo id_operacion busca los detalles
        Set lcOperaciones = New Class_Operaciones
        With lcOperaciones
          .Campo("Id_Operacion").Valor = lId_Operacion
        
          Rem Busca el detalle de la operacion
          If Not .BuscaConDetalles Then
            lRollback = True
            GoTo ErrProcedure
          End If
          
          Rem Realiza la Confirmaci�n de la Instrucci�n
          If Not .Confirmar(lId_Caja_Cuenta) Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Error en la confirmaci�n de la operaci�n.", _
                              .ErrMsg, _
                              pConLog:=True)
            GoTo ErrProcedure
          End If
        
          lFila = pFila
          For Each lDetalle In lcOperaciones.Detalles
            For lFila = lFila To (pGrilla.Rows - 1)
              If GetCell(pGrilla, lFila, "n_orden") = pNro_Orden Then
                If Not Fnt_Guardar_Rel_Nro_Oper_Detalle_RF(GetCell(pGrilla, lFila, "colum_pk"), _
                                                           lDetalle.Campo("id_operacion_detalle").Valor, _
                                                           lMsg_Error) Then
                  GoTo ErrProcedure
                End If
                'Exit For
              End If
            Next
          Next
        End With
        Set lcOperaciones = Nothing
    
    Case gcINST_PACTOS_NAC
        Set lcPactos = New Class_Pactos
        With lcPactos
          If GetCell(pGrilla, pFila, "flg_tipo_movimiento") = gcTipoOperacion_Ingreso Then
            Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                              pCantidad:=lReg("CANTIDAD").Value, _
                                              pTasa:=lReg("precio").Value, _
                                              PTasa_Gestion:=lReg("precio").Value, _
                                              pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                              pBase:=lReg("Base").Value, _
                                              pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                              pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                              pMonto_Pago:=lReg("monto_detalle").Value, _
                                              pReferenciado:="F", _
                                              pTipo_Deposito:="", _
                                              pFecha_Valuta:=lFecha_Liquidacion, _
                                              pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                              pTasa_Historica:=lReg("precio").Value, _
                                              pId_Mov_Activo_Compra:=Null)
            lComision = lComision + lReg("COMISION").Value
            lDerechos = lDerechos + lReg("DERECHO").Value
            lGastos = lGastos + lReg("GASTOS").Value
            lIva = lIva + lReg("IVA").Value
            lMonto_Operacion = lReg("MONTO_OPERACION").Value
          Else
            If Not Fnt_Enlaza_VentasCompras_RF(pCod_Instrumento:=lCod_Instrumento, _
                                               pId_cuenta:=lId_Cuenta, _
                                               pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                               pCantidad:=lReg("cantidad").Value, _
                                               pFecha_Movimiento:=lFecha_Movimiento, _
                                               phActivos:=lhArctivos, _
                                               pId_Mov_Activo:=lReg("id_mov_activo").Value) Then
              Call Fnt_MsgError(0, _
                                "Problemas en buscar el enlaze para las ventas de RF.", _
                                "", _
                                pConLog:=True)
              GoTo ErrProcedure
            End If
          
            For Each lReg_Activo In lhArctivos
              Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                pCantidad:=lReg_Activo("asignado").Value, _
                                                pTasa:=lReg("precio").Value, _
                                                PTasa_Gestion:=lReg("precio").Value, _
                                                pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                                pBase:=lReg("Base").Value, _
                                                pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                                pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                                pMonto_Pago:=lReg("monto_detalle").Value, _
                                                pReferenciado:="F", _
                                                pTipo_Deposito:="", _
                                                pFecha_Valuta:=lFecha_Liquidacion, _
                                                pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                pTasa_Historica:=lReg("precio").Value, _
                                                pId_Mov_Activo_Compra:=lReg_Activo("Id_Mov_Activo").Value)
  
              lComision = lComision + lReg("COMISION").Value
              lDerechos = lDerechos + lReg("DERECHO").Value
              lGastos = lGastos + lReg("GASTOS").Value
              lIva = lIva + lReg("IVA").Value
              lMonto_Operacion = lReg("MONTO_OPERACION").Value
            Next
          End If
          
          If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                                pId_cuenta:=lId_Cuenta, _
                                                pDsc_Operacion:="", _
                                                pTipoOperacion:=GetCell(pGrilla, pFila, "flg_tipo_movimiento"), _
                                                pId_Contraparte:="", _
                                                pId_Representante:="", _
                                                pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                                pFecha_Operacion:=lFecha_Movimiento, _
                                                pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                pId_Trader:="", _
                                                pPorc_Comision:=lPorcentaje_Comision, _
                                                pComision:=lComision, _
                                                pDerechos:=lDerechos, _
                                                pGastos:=lGastos, _
                                                pIva:=lIva, _
                                                pMonto_Operacion:=lMonto_Operacion) Then
              lRollback = True
              Call Fnt_MsgError(.SubTipo_LOG, _
                                "Problemas al grabar.", _
                                .ErrMsg, _
                                pConLog:=True)
              GoTo ErrProcedure
          End If
        End With
        Rem Con el nuevo id_operacion busca los detalles
        Set lcOperaciones = New Class_Operaciones
        With lcOperaciones
          .Campo("Id_Operacion").Valor = lId_Operacion
        
          Rem Busca el detalle de la operacion
          If Not .BuscaConDetalles Then
            lRollback = True
            GoTo ErrProcedure
          End If
          
          Rem Realiza la Confirmaci�n de la Instrucci�n
          If Not .Confirmar(lId_Caja_Cuenta) Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Error en la confirmaci�n de la operaci�n.", _
                              .ErrMsg, _
                              pConLog:=True)
            GoTo ErrProcedure
          End If
        
          lFila = pFila
          For Each lDetalle In lcOperaciones.Detalles
            For lFila = lFila To (pGrilla.Rows - 1)
              If GetCell(pGrilla, lFila, "n_orden") = pNro_Orden Then
                If Not Fnt_Guardar_Rel_Nro_Oper_Detalle_RF(GetCell(pGrilla, lFila, "colum_pk"), _
                                                           lDetalle.Campo("id_operacion_detalle").Valor, _
                                                           lMsg_Error) Then
                  GoTo ErrProcedure
                End If
                lFila = lFila + 1
                Exit For
              End If
            Next
          Next
        End With
        Set lcOperaciones = Nothing
        
    End Select
  Next
  '----------------------------------------------------------------------------
  
'  Rem Con el nuevo id_operacion busca los detalles
'  Set lcOperaciones = New Class_Operaciones
'  With lcOperaciones
'    .Campo("Id_Operacion").Valor = lId_Operacion
'
'    Rem Busca el detalle de la operacion
'    If Not .BuscaConDetalles Then
'      lRollback = True
'      GoTo ErrProcedure
'    End If
'
'    Rem Realiza la Confirmaci�n de la Instrucci�n
'    If Not .Confirmar(lId_Caja_Cuenta) Then
'      Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Error en la confirmaci�n de la operaci�n.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      GoTo ErrProcedure
'    End If
'
'    lFila = pFila
'    For Each lDetalle In lcOperaciones.Detalles
'      For lFila = lFila To (pGrilla.Rows - 1)
'        If GetCell(pGrilla, lFila, "n_orden") = pNro_Orden Then
'          If Not Fnt_Guardar_Rel_Nro_Oper_Detalle_RF(GetCell(pGrilla, lFila, "colum_pk"), _
'                                                     lDetalle.Campo("id_operacion_detalle").Valor, _
'                                                     lMsg_Error) Then
'            GoTo ErrProcedure
'          End If
'          lFila = lFila + 1
'          Exit For
'        End If
'      Next
'    Next
'  End With
  '----------------------------------------------------------------------------

  lMsg_Error = "Operaci�n Ingresada correctamente."
  GoTo ExitProcedure

ErrProcedure:
  lRollback = True
  
ExitProcedure:
  For lFila = pFila To (pGrilla.Rows - 1)
    If To_Number(GetCell(pGrilla, lFila, "n_orden")) = pNro_Orden And GetCell(pGrilla, lFila, "id_linea") = pLinea Then
      Call SetCell(pGrilla, lFila, "dsc_error", lMsg_Error)
      
      If lRollback Then
        pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbRed
        'Grilla_Operaciones_Sin_Cuenta_RV.Cell(flexcpChecked, lFila, "chk") = flexChecked
        Call SetCell(pGrilla, lFila, "chk", flexChecked)
      Else
        pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbBlue
        Call SetCell(pGrilla, lFila, "chk", flexUnchecked)
        Call SetCell(pGrilla, lFila, "grabado", gcFlg_SI, pAutoSize:=False)
      End If
    End If
  Next
  
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  Set lcBonos = Nothing
  Set lcDepositos = Nothing
  Set lcPactos = Nothing
  
  Fnt_Ope_Directa_Cont_RF = Not lRollback
  
End Function

Private Sub Sub_Importa_FM()
Dim lcNemotecnicos As Class_Nemotecnicos
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lReg As hFields
Dim lCursor As hRecord
'---------------------------------------
Dim lGrilla As VSFlexGrid
Dim lFecha_Movimiento As Date
'---------------------------------------
Dim lLinea As Long
Dim lTipo_Movimiento As String
Dim lCod_Instrumento As String
Dim lId_Nemotecnico
Dim lId_Cuenta
Dim lFolioContraparte
Dim lId_Operacion_detalle
Dim lNum_Cuenta As String
Dim lLinea_Oper_con_cta As Long
Dim lNemot�cnico As String
Dim lFlg_Tipo_Movimiento As String
Dim lRut_Cliente As String
Dim lNombre_Cliente As String
Dim lFlg_Estado   As String
Dim lDsc_Bloqueo  As String


On Error GoTo ErrProcedure
  
  Me.Enabled = False
  Call Sub_Bloquea_Puntero(Me)
  
'  Set lcCuenta = New Class_Cuentas
  
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  Set lcNemotecnicos = New Class_Nemotecnicos
  
  lFecha_Movimiento = DTP_Fecha_Proceso.Value
  
  Set lcAlias = CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB

  gDB.Parametros.Clear
  gDB.Procedimiento = "CisCB&DBO&AD_QRY_FM_MOVCUO"
  gDB.Parametros.Add "pFecha_movimientos", ePT_Caracter, Format(DTP_Fecha_Proceso.Value, "YYYYMMDD"), ePD_Entrada
  gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
  If Not gDB.EjecutaSP Then
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
    GoTo ExitProcedure
  End If
  
  Set lCursor = gDB.Parametros("pcursor").Valor
  
  If lCursor.Count <= 0 Then
    GoTo ExitProcedure
  End If
  
  BarraProceso.Max = lCursor.Count
  For Each lReg In lCursor
    BarraProceso.Value = lReg.Index
    
    lTipo_Movimiento = ""
    Select Case Trim(lReg("TIPO_OPERACION").Value)
      Case "IN"
        lTipo_Movimiento = "Inversion"
        lFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso
      Case "RE"
        lTipo_Movimiento = "Retiro"
        lFlg_Tipo_Movimiento = gcTipoOperacion_Egreso
      Case Else
        'Si un tipo de movimiento no esta contemplado se salta
        GoTo ProximoMovimiento
    End Select
    '-----------------------------------------------
    
    lFolioContraparte = Trim(lReg("NUMERO_OPERACION").Value)
    
'    lId_Operacion_detalle = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Mov_FFMM _
'                                                    , pCodigoCSBPI:=cTabla_Operacion_Detalle _
'                                                    , pValor:=lFolioContraparte)
'
'    If Not IsNull(lId_Operacion_detalle) Then
'      'pMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
'      GoTo ProximoMovimiento
'    End If
    
    'Realiza el nemot�cnico con el producto del nemot�cnico y la serie del msistema de fondos
    lNemot�cnico = UCase(Trim(lReg("nemotecnico").Value)) & " " & Trim(lReg("nombre_serie").Value)
    '------- RESCATA EL ID_nemot�cnico
    Rem Busca el id_nemot�cnico del nemot�cnico de la planilla
    With lcNemotecnicos
      .Campo("nemotecnico").Valor = lNemot�cnico
      If Not .Buscar Then
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en buscar el Nemot�cnico.", _
                          .ErrMsg, _
                          pConLog:=True)
        GoTo ExitProcedure
      End If
    
      If .Cursor.Count > 0 Then
        lId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
        lCod_Instrumento = .Cursor(1)("cod_instrumento").Value
      End If
    End With
    '---------------------------------------------------------------------------------------
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_SECURITY.IMPORT_AVAIBLE"
    gDB.Parametros.Add "PID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    gDB.Parametros.Add "PVALOR", ePT_Caracter, lFolioContraparte, ePD_Entrada
    gDB.Parametros.Add "PCOD_INSTRUMENTO", ePT_Caracter, lCod_Instrumento, ePD_Entrada
    gDB.Parametros.Add "PRETURN", ePT_Caracter, "", ePD_Salida
    If Not gDB.EjecutaSP Then
      Call Fnt_MsgError(eLS_ErrSystem, _
                        "Problemas para comprobar si esta disponible la importaci�n.", _
                        gDB.ErrMsg, _
                        pConLog:=True)
      GoTo ProximoMovimiento
    End If
    
    If Not gDB.Parametros("PRETURN").Valor = gcFlg_SI Then
      'SI ENTREGA UN VALOR DISTINTO A "S" SIGNIFICA QUE NO TIENE PERMITIDO EL INGRESO
      GoTo ProximoMovimiento
    End If
    
    '---------------------------------------------------------------------------------------
    lRut_Cliente = Trim(lReg("RUT_CLIENTE").Value) & "/" & Trim(lReg("cuenta").Value)
    lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Magic_FFMM _
                                         , pCodigoCSBPI:=cTabla_Cuentas _
                                         , pValor:=lRut_Cliente)
    
    lNum_Cuenta = ""
    lNombre_Cliente = ""
    lid_Empresa = 0
    If IsNull(lId_Cuenta) Then
      Set lGrilla = Grilla_Operaciones_Sin_Cuenta_FM
      lNombre_Cliente = lReg("nombre_cliente").Value
    Else
      Set lGrilla = Grilla_Operaciones_FM
      ' ----------- RESCATA NUMERO CUENTA
      lcCuenta.Campo("id_cuenta").Valor = lId_Cuenta
      If lcCuenta.Buscar(pEnVista:=True) Then
        lNum_Cuenta = lcCuenta.Cursor(1)("num_cuenta").Value
        lNombre_Cliente = lcCuenta.Cursor(1)("nombre_cliente").Value
        lFlg_Estado = lcCuenta.Cursor(1)("cod_estado").Value
        lDsc_Bloqueo = ""
        lid_Empresa = lcCuenta.Cursor(1)("id_Empresa").Value
        If lFlg_Estado = "H" Then
            If lcCuenta.Cursor(1)("flg_bloqueado").Value = "S" Then
                lFlg_Estado = "B"
                lDsc_Bloqueo = lcCuenta.Cursor(1)("obs_bloqueo").Value
            End If
        Else
            lDsc_Bloqueo = "Cuenta Deshabilitada"
        End If
        If lid_Empresa <> Fnt_EmpresaActual Then
            GoTo ProximoMovimiento
        End If
      End If
    End If
    
    lLinea = lGrilla.Rows
    Call lGrilla.AddItem("")
    Call SetCell(lGrilla, lLinea, "colum_pk", lReg("NUMERO_OPERACION").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "n_orden", lReg("NUMERO_OPERACION").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "rut", lRut_Cliente, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "nombre_cliente", lNombre_Cliente, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_cuenta", "" & lId_Cuenta, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "num_cuenta", lNum_Cuenta, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "tipo_movimiento", lTipo_Movimiento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "flg_tipo_movimiento", lFlg_Tipo_Movimiento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_nemotecnico", lId_Nemotecnico, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "nemotecnico", lNemot�cnico, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "cantidad", lReg("CANTIDAD_CUOTAS").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "PRECIO", lReg("VALOR_CUOTA").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "monto", NVL(lReg("MONTO_OPERACION").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_moneda_operacion", NVL(lReg("MONEDA").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "moneda_inver", Trim("" & lReg("MONEDA").Value), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "fecha_liquidacion", Trim(lReg("FECHA_LIQUIDACION").Value), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "dsc_error", "", pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "cod_instru", lCod_Instrumento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "Grabado", gcFlg_NO, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "ORI_MOV", lReg("ORIGEN_MOVIMIENTO").Value, pAutoSize:=False)
    
    Call SetCell(lGrilla, lLinea, "flg_bloqueado", lFlg_Estado, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "dsc_error", lDsc_Bloqueo, pAutoSize:=False)
      
ProximoMovimiento:
    'este label sirve para saltarce el ingreso de un de operacion con un tipo de movimiento que no corresponde,
    'como los ingresos de custodias, la otra forma es hacer un if gigante que no quiero hacer :-P
    'HAF: 20071108
  Next
  
  Call Sub_AjustaColumnas_Grilla(Grilla_Operaciones_Sin_Cuenta_FM)
  Call Sub_AjustaColumnas_Grilla(Grilla_Operaciones_FM)
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la importaci�n de los movimientos." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  gDB.Parametros.Clear
  Set lcNemotecnicos = Nothing
  Set lcCuenta = Nothing
  Set lcAlias = Nothing
  
  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Graba_Operaciones_con_Cta_FM()
Dim lFila As Long
  
  With Grilla_Operaciones_FM
    For lFila = 1 To Grilla_Operaciones_FM.Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked And _
          Not GetCell(Grilla_Operaciones_FM, lFila, "GRABADO") = gcFlg_SI Then
        If Not Fnt_Ope_Directa_Cont_FM(pGrilla:=Grilla_Operaciones_FM, _
                                       pFila:=lFila, _
                                       pNro_Orden:=GetCell(Grilla_Operaciones_FM, lFila, "colum_pk")) Then
          'GoTo ErrProcedure
        End If
      End If
    Next
  End With


  With Grilla_Operaciones_Sin_Cuenta_FM
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked And _
          Not GetCell(Grilla_Operaciones_Sin_Cuenta_FM, lFila, "GRABADO") = gcFlg_SI Then
        If Not Fnt_Ope_Directa_Cont_FM(pGrilla:=Grilla_Operaciones_Sin_Cuenta_FM, _
                                       pFila:=lFila, _
                                       pNro_Orden:=GetCell(Grilla_Operaciones_Sin_Cuenta_FM, lFila, "colum_pk")) Then
          'GoTo ErrProcedure
        End If
      End If
    Next
  End With
  
  MsgBox "Grabaci�n de Movimientos Diarios de Fondos Mutos finalizada.", vbInformation
  
  Tab_Operaciones.TabEnabled(1) = True
  Tab_Operaciones.TabEnabled(2) = True
  
  Call Grilla_Operaciones_FM.Select(0, 0)
  Call Grilla_Operaciones_Sin_Cuenta_FM.Select(0, 0)
End Sub

Private Function Fnt_Ope_Directa_Cont_FM(pGrilla As VSFlexGrid _
                                       , ByRef pFila As Long _
                                       , ByVal pNro_Orden As Long) As Boolean
Dim lcFondosMutuos As Class_FondosMutuos
Dim lId_Caja_Cuenta As Double
Dim lId_Nemotecnico As String
Dim lFecha_Movimiento As Date
Dim lFecha_Liquidacion As Date
Dim lMsg_Error As String
'---------------------------------------
Dim lcCliente As Object
Dim lRut_Cliente As String
Dim lId_Cliente As String
'---------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lId_Cuenta
Dim lNum_Cuenta
Dim lCod_Instrumento
'---------------------------------------
Dim lId_Moneda As String
'---------------------------------------
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
'---------------------------------------
Dim lId_Operacion As String
Dim lcOperaciones As Class_Operaciones
Dim lDetalle As Class_Operaciones_Detalle
Dim lRollback As Boolean
Dim lMonto_Neto As Double
Dim lMonto_Operacion As Double
'---------------------------------------
Dim lcIva As Class_Iva
Dim lcComisiones As Class_Comisiones_Instrumentos
Dim lPorcentaje_Comision As Double
Dim lGastos As Double
Dim lPorcentaje_Derechos As Double
Dim lComision As Double
Dim lDerechos As Double
Dim lIva As Double
'---------------------------------------
Dim lFila As Long
Dim lRecord_Detalle As hRecord
Dim lReg As hFields
Dim pFila_Cont As Long
 
  Set lRecord_Detalle = New hRecord
  With lRecord_Detalle
    .ClearFields
    .AddField "id_nemotecnico", 0
    .AddField "CANTIDAD", 0
    .AddField "PRECIO", 0
    .AddField "MONTO_DETALLE", 0
    .AddField "ID_MONEDA"
    .AddField "COMISION", 0
    .AddField "DERECHO", 0
    .AddField "GASTOS", 0
    .AddField "IVA", 0
    .AddField "MONTO_OPERACION", 0
    .AddField "COD_INSTRU", ""
    .LimpiarRegistros
  End With

  lRollback = False
  gDB.IniciarTransaccion
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Rem Realiza la operacion directa
  
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("fecha_operacion").Valor = DTP_Fecha_Proceso.Value
    .Campo("cod_producto").Valor = gcPROD_FFMM_NAC
    If Not .Anular_Operaciones_Pendientes Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en buscar el Nemot�cnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  Set lcOperaciones = Nothing
    
  lFila = pFila
  lId_Cuenta = GetCell(pGrilla, pFila, "id_cuenta")
  lNum_Cuenta = GetCell(pGrilla, pFila, "num_cuenta")
  Rem Ingresa el movimiento
  Do While To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pNro_Orden
    lId_Nemotecnico = GetCell(pGrilla, pFila, "id_nemotecnico")
      
    '-----------------------------------------------------------------------------------------
    Rem VALIDACIONES
    lMsg_Error = ""
    
    lFecha_Movimiento = DTP_Fecha_Proceso.Value
    lFecha_Liquidacion = GetCell(pGrilla, pFila, "FECHA_LIQUIDACION")
    lCod_Instrumento = GetCell(pGrilla, lFila, "cod_instru")
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_SECURITY.IMPORT_AVAIBLE"
    gDB.Parametros.Add "PID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    gDB.Parametros.Add "PVALOR", ePT_Caracter, GetCell(pGrilla, lFila, "colum_pk"), ePD_Entrada
    gDB.Parametros.Add "PCOD_INSTRUMENTO", ePT_Caracter, lCod_Instrumento, ePD_Entrada
    gDB.Parametros.Add "PRETURN", ePT_Caracter, "", ePD_Salida
    If Not gDB.EjecutaSP Then
      lMsg_Error = gDB.ErrMsg
      GoTo ErrProcedure
    End If
          
    If Not gDB.Parametros("PRETURN").Valor = gcFlg_SI Then
      lMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
      GoTo ErrProcedure
    Rem Si el Id_Cuenta y Num_cuenta es vacio quiere decir que no tiene asociada una cuenta en GPI
    ElseIf lId_Cuenta = "" And lNum_Cuenta = "" Then
      lMsg_Error = "La Orden no tiene asociado una cuenta en el sistema, ya que ""Cuenta"" est� vacio." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Si el id_nemot�cnico es "0" quiere decir que no se encontr� el nemot�cnico en GPI
    ElseIf lId_Nemotecnico = "" Then
      lMsg_Error = "El Nemot�cnico '" & GetCell(pGrilla, lFila, "nemotecnico") & "' de la grilla ""Operaciones con Cuenta"" no est� en el sistema." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem La Fecha de Liquidacion es vacia, no se puede operar
    ElseIf GetCell(pGrilla, lFila, "fecha_liquidacion") = "" Then
      lMsg_Error = "En el N�mero de Orden '" & GetCell(pGrilla, lFila, "colum_pk") & "' la Fecha de Liquidaci�n es vacia en la grilla ""Operaciones con Cuenta""." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Busca la moneda del nemot�cnico en CSGPI
    ElseIf Not Fnt_Buscar_Datos_Nemo(pId_Nemotecnico:=lId_Nemotecnico _
                                   , pId_Moneda_Transaccion:=lId_Moneda _
                                   , pMsg_Error:=lMsg_Error _
                                   , pCod_Instrumento:=lCod_Instrumento) Then
      GoTo ErrProcedure
    End If
    '-----------------------------------------------------------------------------
          
    Set lReg = lRecord_Detalle.Add
    lReg("id_nemotecnico").Value = lId_Nemotecnico
    lReg("CANTIDAD").Value = To_Number(GetCell(pGrilla, pFila, "Cantidad"))
    lReg("PRECIO").Value = To_Number(GetCell(pGrilla, pFila, "precio"))
    lReg("MONTO_DETALLE").Value = To_Number(GetCell(pGrilla, pFila, "monto"))
    lReg("ID_MONEDA").Value = lId_Moneda
    lReg("COMISION").Value = 0
    lReg("DERECHO").Value = 0
    lReg("GASTOS").Value = 0
    lReg("IVA").Value = 0
    lReg("MONTO_OPERACION").Value = To_Number(GetCell(pGrilla, pFila, "monto"))
    lReg("COD_INSTRU").Value = GetCell(pGrilla, pFila, "cod_instru")
      
    lFila = lFila + 1
    If lFila > pGrilla.Rows - 1 Then
      Exit Do
    End If
  Loop
      
  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("id_moneda").Valor = lId_Moneda
    .Campo("cod_mercado").Valor = cMercado_Nacional
    If .Buscar_Caja_Para_Invertir() Then
      If .Cursor.Count > 0 Then
        lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
      End If
    Else
      lRollback = True
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Cajas de la Cuenta.", _
                        .ErrMsg, _
                        pConLog:=True)
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  '-----------------------------------------------------------------------------
  
  lFecha_Movimiento = DTP_Fecha_Proceso.Value
  lFecha_Liquidacion = GetCell(pGrilla, pFila, "FECHA_LIQUIDACION")
  
  Set lcFondosMutuos = New Class_FondosMutuos
  With lcFondosMutuos
    For Each lReg In lRecord_Detalle
      Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                        pcuota:=lReg("cantidad").Value, _
                                        pPrecio:=lReg("precio").Value, _
                                        pId_Moneda:=lReg("id_moneda").Value, _
                                        pMonto:=lReg("monto_detalle").Value, _
                                        pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                        pFecha_Liquidacion:=lFecha_Liquidacion, _
                                        pPrecio_Historico:="")
      lComision = lComision + lReg("COMISION").Value
      lDerechos = lDerechos + lReg("DERECHO").Value
      lGastos = lGastos + lReg("GASTOS").Value
      lIva = lIva + lReg("IVA").Value
      lMonto_Operacion = lMonto_Operacion + lReg("MONTO_OPERACION").Value
    Next
  
    Rem Comisiones
    Set lcComisiones = New Class_Comisiones_Instrumentos
    Set lcIva = New Class_Iva
    With lcComisiones
        .Campo("Id_Cuenta").Valor = lId_Cuenta
        .Campo("COD_INSTRUMENTO").Valor = lCod_Instrumento
        If .Buscar(True) Then
            If .Cursor.Count > 0 Then
                lPorcentaje_Comision = lcIva.Porcentaje_Iva(.Cursor(1)("COMISION").Value)
            End If
        End If
    End With
    Set lcComisiones = Nothing
    '-----------------------------------------------------------------------------
    If GetCell(pGrilla, pFila, "ORI_MOV") = "D" Then
        If Not .Realiza_Operacion_Custodia_DividendosFM(pId_Operacion:=lId_Operacion, _
                                              pId_cuenta:=lId_Cuenta, _
                                              pDsc_Operacion:="Ingreso por Importacion", _
                                              pTipoOperacion:=GetCell(pGrilla, pFila, "flg_tipo_movimiento"), _
                                              pId_Contraparte:="", _
                                              pId_Representante:="", _
                                              pId_Moneda_Operacion:=lId_Moneda, _
                                              pFecha_Operacion:=lFecha_Movimiento, _
                                              pFecha_Vigencia:=lFecha_Movimiento, _
                                              pFecha_Liquidacion:=lFecha_Liquidacion, _
                                              pId_Trader:="", _
                                              pPorc_Comision:=lPorcentaje_Comision, _
                                              pComision:=lComision, _
                                              pDerechos_Bolsa:=lDerechos, _
                                              pGastos:=lGastos, _
                                              pIva:=lIva, _
                                              pMonto_Operacion:=lMonto_Operacion, _
                                              pTipo_Precio:=cTipo_Precio_Mercado, _
                                              pCod_Instrumento:=lCod_Instrumento) Then
            lRollback = True
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al grabar Fondos Mutuos.", _
                        .ErrMsg, _
                        pConLog:=True)
            GoTo ErrProcedure
        End If
    Else
        If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                              pId_cuenta:=lId_Cuenta, _
                                              pDsc_Operacion:="Ingreso por Importacion", _
                                              pTipoOperacion:=GetCell(pGrilla, pFila, "flg_tipo_movimiento"), _
                                              pId_Contraparte:="", _
                                              pId_Representante:="", _
                                              pId_Moneda_Operacion:=lId_Moneda, _
                                              pFecha_Operacion:=lFecha_Movimiento, _
                                              pFecha_Vigencia:=lFecha_Movimiento, _
                                              pFecha_Liquidacion:=lFecha_Liquidacion, _
                                              pId_Trader:="", _
                                              pPorc_Comision:=lPorcentaje_Comision, _
                                              pComision:=lComision, _
                                              pDerechos_Bolsa:=lDerechos, _
                                              pGastos:=lGastos, _
                                              pIva:=lIva, _
                                              pMonto_Operacion:=lMonto_Operacion, _
                                              pTipo_Precio:=cTipo_Precio_Mercado, _
                                              pCod_Instrumento:=lCod_Instrumento) Then
            lRollback = True
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al grabar Fondos Mutuos.", _
                        .ErrMsg, _
                        pConLog:=True)
            GoTo ErrProcedure
        End If
    End If
  End With
  '----------------------------------------------------------------------------
    Rem Con el nuevo id_operacion busca los detalles
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Id_Operacion").Valor = lId_Operacion
    
    Rem Busca el detalle de la operacion
    If Not .BuscaConDetalles Then
        lRollback = True
        GoTo ErrProcedure
    End If
      
    Rem Realiza la Confirmaci�n de la Instrucci�n
    If GetCell(pGrilla, pFila, "ORI_MOV") <> "D" Then
        If Not .Confirmar(lId_Caja_Cuenta) Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en la confirmaci�n de la operaci�n.", _
                        .ErrMsg, _
                        pConLog:=True)
            GoTo ErrProcedure
        End If
    End If
    
    Rem Por cada id_operacion_detalle encontrado guarda la relacion
    Rem "nro_orden-id_operacion_detalle" en la tabla rel_conversiones
    For Each lDetalle In lcOperaciones.Detalles
      If Not Fnt_Guardar_Rel_Nro_Oper_Detalle_FM(GetCell(pGrilla, pFila, "colum_pk"), _
                                                lDetalle.Campo("id_operacion_detalle").Valor, _
                                                lMsg_Error) Then
        lRollback = True
        GoTo ErrProcedure
      End If
    Next
  End With
  '----------------------------------------------------------------------------
  
  lMsg_Error = "Operaci�n Ingresada correctamente."
  GoTo ExitProcedure
  
ErrProcedure:
  lRollback = True
  
ExitProcedure:
  lFila = pFila
  Do While To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pNro_Orden
    Call SetCell(pGrilla, lFila, "dsc_error", lMsg_Error)
      
    If lRollback Then
      pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbRed
      'Grilla_Operaciones_Sin_Cuenta_RV.Cell(flexcpChecked, lFila, "chk") = flexChecked
      Call SetCell(pGrilla, lFila, "chk", flexChecked)
    Else
      pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbBlue
      Call SetCell(pGrilla, lFila, "chk", flexUnchecked)
      Call SetCell(pGrilla, lFila, "grabado", gcFlg_SI, pAutoSize:=False)
    End If
    
    lFila = lFila + 1
    
    If lFila > pGrilla.Rows - 1 Then
      Exit Do
    End If
  Loop
  
  Rem Setea la pFila para que siga con la siguiente Orden de la grilla
  pFila = lFila - 1
  
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  Set lcFondosMutuos = Nothing
  
  Fnt_Ope_Directa_Cont_FM = Not lRollback
  
End Function

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_Sin_Cuenta_FM.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_Sin_Cuenta_FM)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_Sin_Cuenta_FM)
End Sub

Private Sub Sub_Check_DesCheck_Mismo_Nro_Orden(pGrilla As VSFlexGrid)
Dim lFil_Grilla As Integer
Dim lCol_Grilla As Integer
Dim lLinea As Integer
Dim lNro_Orden As Long
    
    lFil_Grilla = pGrilla.Row
    lCol_Grilla = pGrilla.Col
    If lCol_Grilla = 0 And lFil_Grilla > 0 Then
        If GetCell(pGrilla, lFil_Grilla, "flg_bloqueado") <> "D" Then
            If Val(GetCell(pGrilla, lFil_Grilla, "chk")) = 0 Then
                lNro_Orden = Val(GetCell(pGrilla, lFil_Grilla, "colum_pk"))
                For lLinea = 1 To pGrilla.Rows - 1
                    If lNro_Orden = Val(GetCell(pGrilla, lLinea, "colum_pk")) Then
                        pGrilla.TextMatrix(lLinea, lCol_Grilla) = flexChecked
                    End If
                Next
            Else
                lNro_Orden = Val(GetCell(pGrilla, lFil_Grilla, "colum_pk"))
                For lLinea = 1 To pGrilla.Rows - 1
                    If lNro_Orden = Val(GetCell(pGrilla, lLinea, "colum_pk")) Then
                        pGrilla.TextMatrix(lLinea, lCol_Grilla) = 0
                    End If
                Next
            End If
        End If
    End If
End Sub

Private Sub Sub_Mensaje_Error(pGrilla As VSFlexGrid)
Dim lMensaje As String

  If pGrilla.Row >= 1 Then
    lMensaje = GetCell(pGrilla, pGrilla.Row, "dsc_error")
    If Not lMensaje = "" Then
      MsgBox lMensaje, vbInformation
    End If
  End If
End Sub

Private Sub Toolbar_Selc_Con_Cta_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_RV, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_RV, False)
  End Select
End Sub

Private Sub Toolbar_Selc_Con_Cta_RF_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_RF, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_RF, False)
  End Select
End Sub

Private Sub Toolbar_Selc_Sin_Cta_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RV, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RV, False)
  End Select
End Sub

Private Sub Toolbar_Selc_Sin_Cta_RF_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RF, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RF, False)
  End Select
End Sub
Private Function Fnt_VerificaCreacion_Nemo(pNemotecnico _
                                         , pFecha_Movimiento _
                                         , pCod_Instrumento _
                                         , pTasa _
                                         , ByRef pId_Nemotecnico _
                                         , ByRef PId_Moneda_Deposito _
                                         , ByRef pId_Moneda_Pago _
                                         , ByRef pFecha_Vencimiento _
                                         , ByRef pBase _
                                         , ByRef pMsgError As String) As Boolean
Dim lcPactos          As Class_Pactos
'--------------------------------------
'Dim lcMoneda          As Class_Monedas
Dim lcMoneda          As Object
'--------------------------------------
Dim lcEmisor_Especifico As Class_Emisores_Especifico
Dim lcNemotecnico As Class_Nemotecnicos
'---------------------------------------------------
Dim lNemotecnico
Dim lId_Emisor_Especifico
Dim lFecha_Emision
  Fnt_VerificaCreacion_Nemo = False
  'Verifica si el nemot�cnico se puede crear o no
  Select Case pCod_Instrumento
    Case gcINST_PACTOS_NAC
        Set lcEmisor_Especifico = New Class_Emisores_Especifico
        With lcEmisor_Especifico
          .Campo("COD_SVS_nemotecnico").Valor = "SEC"  'Trim(Mid(pNemotecnico, 3, 4))
          If Not .Buscar Then
            pMsgError = "Problemas en buscar el codigo SVS del emisor." & .ErrMsg
            GoTo ExitProcedure
          End If
          
          If .Cursor.Count <= 0 Then
            pMsgError = "El codigo del emisor no esta definido en el sistema, cree un emisor especifico con el codigo de SVS correspondiente."
            GoTo ExitProcedure
          End If
          
          lId_Emisor_Especifico = .Cursor(1)("id_emisor_especifico").Value
        End With
        Set lcEmisor_Especifico = Nothing
        pFecha_Vencimiento = DateSerial("20" & Mid(pNemotecnico, 11, 2), Mid(pNemotecnico, 14, 2), Mid(pNemotecnico, 17, 2))
        PId_Moneda_Deposito = Null
'        Set lcMoneda = New Class_Monedas
        Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
        With lcMoneda
            .Campo("cod_moneda").Valor = cMoneda_Cod_Peso
            If Not .Buscar Then
                pMsgError = "Problemas en buscar la moneda del nemot�cnico." & .ErrMsg
                GoTo ExitProcedure
            End If
            
            If .Cursor.Count <= 0 Then
                pMsgError = "Por raro que parezca no esta la moneda definida en el sistema, favor de crear."
                GoTo ExitProcedure
            End If
            
            pId_Moneda_Pago = .Cursor(1)("id_moneda").Value
            PId_Moneda_Deposito = pId_Moneda_Pago
            pBase = 30
        End With
        Set lcMoneda = Nothing
        
        Set lcPactos = New Class_Pactos
        lNemotecnico = lcPactos.Fnt_Genera_Nemo_SVS_Pactos(lId_Emisor_Especifico _
                                                       , pFecha_Vencimiento _
                                                       , PId_Moneda_Deposito _
                                                       , pId_Moneda_Pago)
        Set lcPactos = Nothing
    Case gcINST_DEPOSITOS_NAC
    
        Set lcEmisor_Especifico = New Class_Emisores_Especifico
        With lcEmisor_Especifico
            If Trim(Mid(pNemotecnico, 1, 4)) = "PDBC" Or Trim(Mid(pNemotecnico, 1, 4)) = "PRBC" Then
                .Campo("COD_SVS_nemotecnico").Valor = "CEN"
            Else
                .Campo("COD_SVS_nemotecnico").Valor = Trim(Mid(pNemotecnico, 3, 4))
            End If
          If Not .Buscar Then
            pMsgError = "Problemas en buscar el codigo SVS del emisor." & .ErrMsg
            GoTo ExitProcedure
          End If
          
          If .Cursor.Count <= 0 Then
            pMsgError = "El codigo del emisor no esta definido en el sistema, cree un emisor especifico con el codigo de SVS correspondiente."
            GoTo ExitProcedure
          End If
          
          lId_Emisor_Especifico = .Cursor(1)("id_emisor_especifico").Value
        End With
        Set lcEmisor_Especifico = Nothing
        pFecha_Vencimiento = DateSerial("20" & Mid(pNemotecnico, 11, 2), Mid(pNemotecnico, 9, 2), Mid(pNemotecnico, 7, 2))
        
        PId_Moneda_Deposito = Null
'        Set lcMoneda = New Class_Monedas
        Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
        With lcMoneda
            If Trim(Mid(pNemotecnico, 1, 4)) = "PRBC" Then
                .Campo("cod_moneda").Valor = cMoneda_Cod_UF
            Else
                .Campo("cod_moneda").Valor = cMoneda_Cod_Peso
            End If
            If Not .Buscar Then
                pMsgError = "Problemas en buscar la moneda del nemot�cnico." & .ErrMsg
                GoTo ExitProcedure
            End If
            
            If .Cursor.Count <= 0 Then
                pMsgError = "Por raro que parezca no esta la moneda definida en el sistema, favor de crear."
                GoTo ExitProcedure
            End If
            
            pId_Moneda_Pago = .Cursor(1)("id_moneda").Value
        End With
        Set lcMoneda = Nothing
        
'        Set lcMoneda = New Class_Monedas
        Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
        With lcMoneda
            Select Case Mid(pNemotecnico, 2, 1)
                Case "$"
                    .Campo("cod_moneda").Valor = cMoneda_Cod_Peso
                    pBase = 30
                Case "U"
                    .Campo("cod_moneda").Valor = cMoneda_Cod_UF
                    pBase = 360
                Case "O"
                    .Campo("cod_moneda").Valor = cMoneda_Cod_Dolar
                    pBase = 360
            End Select
            If Not .Buscar Then
                pMsgError = "Problemas en buscar la moneda del nemot�cnico." & .ErrMsg
                GoTo ExitProcedure
            End If
            
            If .Cursor.Count <= 0 Then
                pMsgError = "Por raro que parezca no esta la moneda definida en el sistema, favor de crear."
                GoTo ExitProcedure
            End If
            
            PId_Moneda_Deposito = .Cursor(1)("id_moneda").Value
        End With
        Set lcMoneda = Nothing
        
        Set lcNemotecnico = New Class_Nemotecnicos
        With lcNemotecnico
            .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
            .Campo("Fecha_Vencimiento").Valor = pFecha_Vencimiento
            lNemotecnico = .Fnt_Generacion_Nemo_SVS(PId_Moneda_Deposito, pId_Moneda_Pago)
        End With
        Set lcNemotecnico = Nothing
    Case Else
        pMsgError = "No se puede crear el nemot�cnico """ & pNemotecnico & """."
    GoTo ExitProcedure
  End Select
  
  lFecha_Emision = pFecha_Movimiento
  
 'Busca primero el nemotecnico antes de crearlo
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("Cod_Instrumento").Valor = pCod_Instrumento
    .Campo("nemotecnico").Valor = lNemotecnico
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en buscar el Nemot�cnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
  
    If .Cursor.Count > 0 Then
        pId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
    Else
        .Campo("id_Mercado_Transaccion").Valor = "15" 'fc_Mercado_Transaccion 'Este campo deberia aceptar nulos
        .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
        .Campo("id_Emisor_Especifico_Origen").Valor = lId_Emisor_Especifico
        .Campo("id_Moneda").Valor = PId_Moneda_Deposito
        .Campo("id_Moneda_transaccion").Valor = pId_Moneda_Pago
        .Campo("id_Tipo_Estado").Valor = cTEstado_Nemotecnico
        .Campo("cod_Estado").Valor = cCod_Estado_Vigente
        .Campo("dsc_nemotecnico").Valor = lNemotecnico
        .Campo("tasa_Emision").Valor = pTasa
        .Campo("tipo_Tasa").Valor = ""
        .Campo("periodicidad").Valor = ""
        .Campo("fecha_Vencimiento").Valor = pFecha_Vencimiento
        .Campo("corte_Minimo_Papel").Valor = 1 'Depositos no tiene corte
        .Campo("Monto_Emision").Valor = ""
        .Campo("liquidez").Valor = ""
        .Campo("base").Valor = pBase
        .Campo("cod_Pais").Valor = gcPais_Chile
        .Campo("flg_Fungible").Valor = ""
        .Campo("fecha_emision").Valor = lFecha_Emision
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en grabar el Nemot�cnico.", _
                            .ErrMsg, _
                            pConLog:=True)
          GoTo ExitProcedure
        End If
        pId_Nemotecnico = .Campo("id_nemotecnico").Valor
    End If
  End With
  Set lcNemotecnico = Nothing
  
  Fnt_VerificaCreacion_Nemo = True
  
ExitProcedure:

End Function

Private Function Fnt_Buscar_Rel_Nro_Oper_Detalle(pNro_Orden As Variant, pMsg_Error As String) As Boolean
  gDB.Parametros.Clear
  gDB.Procedimiento = "PKG_SECURITY.IMPORT_AVAIBLE"
  gDB.Parametros.Add "PID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
  gDB.Parametros.Add "PVALOR", ePT_Caracter, pNro_Orden, ePD_Entrada
  gDB.Parametros.Add "PCOD_INSTRUMENTO", ePT_Caracter, "", ePD_Entrada
  gDB.Parametros.Add "PRETURN", ePT_Caracter, "", ePD_Salida
  If Not gDB.EjecutaSP Then
    pMsg_Error = gDB.ErrMsg
    GoTo ErrProcedure
  End If
  Fnt_Buscar_Rel_Nro_Oper_Detalle = (gDB.Parametros("PRETURN").Valor = gcFlg_SI)
  
  Exit Function
ErrProcedure:
  Fnt_Buscar_Rel_Nro_Oper_Detalle = False
End Function

Private Function Fnt_Buscar_Datos_Nemo_RF(pId_Nemotecnico As String, _
                                          ByRef pId_Moneda_Nemo As String, _
                                          ByRef pFecha_Vencimiento As Date, _
                                          ByRef pMsg_Error As String) As Boolean
Dim lcAlias As Class_Alias
Dim lcNemotecnico As Class_Nemotecnicos
  
  Fnt_Buscar_Datos_Nemo_RF = True
  
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pId_Moneda_Nemo = .Cursor(1)("id_moneda_transaccion").Value
        pFecha_Vencimiento = NVL(.Cursor(1)("fecha_vencimiento").Value, Format(Now, cFormatDate))
      Else
        pMsg_Error = "Nemot�cnico no encontrado en el sistema. No se puede operar la instrucci�n."
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = "Problemas en cargar datos de Nemotecnico." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcNemotecnico = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Buscar_Datos_Nemo_RF = False
End Function


Private Sub Sub_CreaPDF_Mail_Asesor_FM(pId_Asesor, pGrilla As VSFlexGrid, pGrilla_SC As VSFlexGrid, lApellido As String)
Dim lFila As Long
'Dim ArchivosPDF()
'With pGrilla
  
'ReDim Preserve ArchivosPDF(0)
 
Const clrHeader = &HD0D0D0
Dim sRecord
Dim bAppend
Dim lLinea As Integer
Dim lForm As Object 'Frm_Reporte_Generico
Dim lReg As hCollection.hFields
Dim a As Integer
Dim iFila As Integer
Dim Filas As Integer
Dim Fila As Integer
Dim lCursor_Asesor As hRecord
Dim pArchivoPDF As String
Dim lTotalGastos As Double
'-----------------------------------------------------------------------
'Dim lOD_Cursor As hRecord 'CURSOR PARA EL DETALLE DE LAS OPERACIONES.

  'Set Fnt_Generar_Comprobante = Nothing
  'Call Sub_Bloquea_Puntero(Me)
  'Call Sub_CargarDatos_Gen(pId_Operacion)
  
  'Set lForm = fClass.Form_Reporte_Generico
  
  pArchivoPDF = Environ("temp") & "\Confirmacion_Importacion_" & lApellido & "-" & pId_Asesor & ".pdf"
  Set lForm = Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="Confirmaci�n de Ordenes al " & DTP_Fecha_Proceso.Value _
                               , pTipoSalida:=ePrinter.eP_PDF_Automatico _
                               , pOrientacion:=orLandscape _
                               , pArchivoSalida:=pArchivoPDF)
     
Dim lc_Asesores As Class_Asesor
Dim lNombre_Asesor As String
Dim lMail_Asesor As String

Set lc_Asesores = New Class_Asesor

With lc_Asesores
    .Campo("id_asesor").Valor = pId_Asesor
    If .Buscar Then
        Set lCursor_Asesor = .Cursor
        For Each lReg In .Cursor
           lNombre_Asesor = lReg("nombre").Value
           lMail_Asesor = lReg("Email").Value
        Next
    End If
End With
    
  With lForm.VsPrinter

 ' .StartDoc
 .Paragraph = ""
 .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 8
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "ASESOR: " & lNombre_Asesor & " (" & lMail_Asesor & ")"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
  .EndTable
 
 .Paragraph = ""

  .MarginLeft = "30mm"
  .MarginRight = "7mm"
    .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 7
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "Operaciones con Patrimonio Administrado"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
    .EndTable
 
    .Paragraph = "" 'salto de linea
    
    .StartTable
      
    '.TableCell(tcRows) = pGrilla.Rows
    .TableCell(tcRows) = 1
    .TableCell(tcCols) = 10
    .TableBorder = tbAll
    '
   .TableCell(tcColWidth, 1, 1) = "15mm"
    .TableCell(tcColWidth, 1, 2) = "18mm"
    .TableCell(tcColWidth, 1, 3) = "15mm"
    .TableCell(tcColWidth, 1, 4) = "45mm"
    .TableCell(tcColWidth, 1, 5) = "20mm"
    .TableCell(tcColWidth, 1, 6) = "35mm"
    .TableCell(tcColWidth, 1, 7) = "25mm"
    .TableCell(tcColWidth, 1, 8) = "20mm"
    .TableCell(tcColWidth, 1, 9) = "25mm"
    .TableCell(tcColWidth, 1, 10) = "22mm"
    
    .TableCell(tcText, 1, 1) = "N� Orden"
    .TableCell(tcText, 1, 2) = "Rut Cliente"
    .TableCell(tcText, 1, 3) = "Cuenta"
    .TableCell(tcText, 1, 4) = "Cliente"
    .TableCell(tcText, 1, 5) = "Movimiento"
    .TableCell(tcText, 1, 6) = "Nemot�cnico"
    .TableCell(tcText, 1, 7) = "Cantidad"
    .TableCell(tcText, 1, 8) = "Valor Cuota"
    .TableCell(tcText, 1, 9) = "Monto"
    .TableCell(tcText, 1, 10) = "Fecha Liquidaci�n"
 Fila = 2
 For lFila = 1 To pGrilla.Rows - 1
      If pGrilla.Cell(flexcpChecked, lFila, pGrilla.ColIndex("chk")) = flexChecked And GetCell(pGrilla, lFila, "id_asesor") = pId_Asesor Then
          .TableCell(tcRows) = .TableCell(tcRows) + 1
           'Fila = lFila + 1
          .TableCell(tcText, Fila, 1) = GetCell(pGrilla, lFila, "n_orden")
          .TableCell(tcText, Fila, 2) = GetCell(pGrilla, lFila, "rut")
          .TableCell(tcText, Fila, 3) = GetCell(pGrilla, lFila, "num_cuenta")
          .TableCell(tcText, Fila, 4) = GetCell(pGrilla, lFila, "dsc_cliente")
          .TableCell(tcText, Fila, 5) = GetCell(pGrilla, lFila, "tipo_movimiento")
          .TableCell(tcText, Fila, 6) = GetCell(pGrilla, lFila, "nemotecnico")
          .TableCell(tcText, Fila, 7) = FormatNumber(GetCell(pGrilla, lFila, "cantidad"))
          .TableCell(tcText, Fila, 8) = FormatNumber(GetCell(pGrilla, lFila, "precio"))
          .TableCell(tcText, Fila, 9) = FormatNumber(GetCell(pGrilla, lFila, "monto"))
          .TableCell(tcText, Fila, 10) = GetCell(pGrilla, lFila, "fecha_liquidacion")
          .TableCell(tcAlign, Fila, 1) = taLeftMiddle
          .TableCell(tcAlign, Fila, 2) = taRightMiddle
          .TableCell(tcAlign, Fila, 3) = taCenterMiddle
          .TableCell(tcAlign, Fila, 6) = taLeftMiddle
          .TableCell(tcAlign, Fila, 7) = taRightMiddle
          .TableCell(tcAlign, Fila, 8) = taRightMiddle
          .TableCell(tcAlign, Fila, 9) = taRightMiddle
          .TableCell(tcAlign, Fila, 10) = taRightMiddle
          Fila = Fila + 1
      End If
  Next
  .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 13) = 6
  
  .EndTable
  
.Paragraph = ""
'If pGrilla_SC.Rows > 1 Then
.StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 7
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "Operaciones Fuera del Patrimonio Administrado"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
.EndTable


.StartTable
      
    '.TableCell(tcRows) = pGrilla_SC.Rows
    .TableCell(tcRows) = 1
    .TableCell(tcCols) = 10
    .TableBorder = tbAll
   ' .TableCell(tcFontSize, 1, 1, pGrilla_SC.Rows, 13) = 6
    
   .TableCell(tcColWidth, 1, 1) = "15mm"
    .TableCell(tcColWidth, 1, 2) = "18mm"
    .TableCell(tcColWidth, 1, 3) = "15mm"
    .TableCell(tcColWidth, 1, 4) = "45mm"
    .TableCell(tcColWidth, 1, 5) = "20mm"
    .TableCell(tcColWidth, 1, 6) = "35mm"
    .TableCell(tcColWidth, 1, 7) = "25mm"
    .TableCell(tcColWidth, 1, 8) = "20mm"
    .TableCell(tcColWidth, 1, 9) = "25mm"
    .TableCell(tcColWidth, 1, 10) = "22mm"
    
    
    .TableCell(tcText, 1, 1) = "N� Orden"
    .TableCell(tcText, 1, 2) = "Rut Cliente"
    .TableCell(tcText, 1, 3) = "Cuenta"
    .TableCell(tcText, 1, 4) = "Cliente"
    .TableCell(tcText, 1, 5) = "Movimiento"
    .TableCell(tcText, 1, 6) = "Nemot�cnico"
    .TableCell(tcText, 1, 7) = "Cantidad"
    .TableCell(tcText, 1, 8) = "Valor Cuota"
    .TableCell(tcText, 1, 9) = "Monto"
    .TableCell(tcText, 1, 10) = "Fecha Liquidaci�n"


'With pGrilla_SC
Fila = 2
  For lFila = 1 To pGrilla_SC.Rows - 1
      If pGrilla_SC.Cell(flexcpChecked, lFila, pGrilla_SC.ColIndex("chk")) = flexChecked And GetCell(pGrilla_SC, lFila, "id_asesor") = pId_Asesor Then
          .TableCell(tcRows) = .TableCell(tcRows) + 1
           ' Fila = lFila + 1
          .TableCell(tcText, Fila, 1) = GetCell(pGrilla_SC, lFila, "n_orden")
          .TableCell(tcText, Fila, 2) = GetCell(pGrilla_SC, lFila, "rut")
          .TableCell(tcText, Fila, 3) = GetCell(pGrilla_SC, lFila, "num_cuenta")
          .TableCell(tcText, Fila, 4) = GetCell(pGrilla_SC, lFila, "nombre_cliente")
          .TableCell(tcText, Fila, 5) = GetCell(pGrilla_SC, lFila, "tipo_movimiento")
          .TableCell(tcText, Fila, 6) = GetCell(pGrilla_SC, lFila, "nemotecnico")
          .TableCell(tcText, Fila, 7) = FormatNumber(GetCell(pGrilla_SC, lFila, "cantidad"))
          .TableCell(tcText, Fila, 8) = FormatNumber(GetCell(pGrilla_SC, lFila, "precio"))
          .TableCell(tcText, Fila, 9) = FormatNumber(GetCell(pGrilla_SC, lFila, "monto"))
          .TableCell(tcText, Fila, 10) = GetCell(pGrilla_SC, lFila, "fecha_liquidacion")
     
          .TableCell(tcAlign, Fila, 1) = taLeftMiddle
          .TableCell(tcAlign, Fila, 2) = taRightMiddle
          .TableCell(tcAlign, Fila, 3) = taCenterMiddle
          .TableCell(tcAlign, Fila, 6) = taRightMiddle
          .TableCell(tcAlign, Fila, 7) = taRightMiddle
          .TableCell(tcAlign, Fila, 8) = taRightMiddle
          .TableCell(tcAlign, Fila, 9) = taRightMiddle
          .TableCell(tcAlign, Fila, 10) = taRightMiddle
          Fila = Fila + 1
      End If
  Next
  .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 13) = 6
.EndTable
.EndDoc
End With

If Envia_Confirmacion(pArchivoPDF, lMail_Asesor, lNombre_Asesor) Then

End If
End Sub


