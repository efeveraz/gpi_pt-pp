VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Busca_Clientes 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Buscador de Clientes"
   ClientHeight    =   5250
   ClientLeft      =   165
   ClientTop       =   315
   ClientWidth     =   10155
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5250
   ScaleWidth      =   10155
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Clientes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   75
      TabIndex        =   3
      Top             =   1470
      Width           =   10035
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3315
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   9825
         _cx             =   17330
         _cy             =   5847
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   7
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Busca_Clientes.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin VB.Frame Frame5 
      Height          =   1065
      Left            =   60
      TabIndex        =   0
      Top             =   360
      Width           =   10035
      Begin hControl2.hTextLabel Txt_Rut_Cliente 
         Height          =   315
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   3060
         _ExtentX        =   5398
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   1200
         Caption         =   "RUT Cliente"
         Text            =   ""
         MaxLength       =   10
      End
      Begin hControl2.hTextLabel TxtRazonSocial 
         Height          =   315
         Left            =   120
         TabIndex        =   2
         Top             =   630
         Width           =   6630
         _ExtentX        =   11695
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Nombre Cliente"
         Text            =   ""
         MaxLength       =   99
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   635
      ButtonWidth     =   1561
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Buscar"
            Key             =   "FIND"
            Description     =   "Graba la información de forma Permanente"
            Object.ToolTipText     =   "Graba la información de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Busca_Clientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fId_Cliente

Public Function BuscarCliente(Optional pFiltro_RutCliente _
                            , Optional pFiltro_NombreCliente)
Dim lCargar As Boolean
                 
  Call Form_Resize
  Load Me
    
  lCargar = False
     
  If Not IsMissing(pFiltro_RutCliente) Then
    If pFiltro_RutCliente <> "" Then
      Txt_Rut_Cliente.Text = pFiltro_RutCliente
      lCargar = True
    End If
  End If
  
  If Not IsMissing(pFiltro_NombreCliente) Then
    If pFiltro_NombreCliente <> "" Then
      TxtRazonSocial.Text = pFiltro_NombreCliente
      lCargar = True
    End If
  End If
      
  If lCargar Then
    Call Sub_CargarDatos
    If Grilla.Rows = 2 Then
      fId_Cliente = GetCell(Grilla, 1, "id_cliente")
      BuscarCliente = fId_Cliente
      GoTo ExitProcedure
    End If
  End If
  
  fId_Cliente = Null
  Me.Show vbModal
  BuscarCliente = fId_Cliente
  
ExitProcedure:
  Unload Me
End Function

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("FIND").Image = cBoton_Buscar
  End With

  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Me.Tag = UnloadMode
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
  If Grilla.Row > 0 Then
    fId_Cliente = GetCell(Grilla, Grilla.Row, "id_cliente")
    Me.Hide
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "FIND"
      Call Sub_CargarDatos
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_CargarDatos(Optional pBusca_Cuenta As Boolean = False)
Dim lcClientes  As Object
Dim lReg        As hCollection.hFields
Dim lLinea      As Long
Dim lID         As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "id_cliente")
  Else
    lID = ""
  End If

  Grilla.Rows = 1
  Set lcClientes = Fnt_CreateObject(cDLL_Clientes)

  With lcClientes
  
    .Campo("rut_cliente").Valor = Fnt_AgregaStrPorcentaje(Txt_Rut_Cliente.Text)
    
    If .Buscar_Formulario(pNombre_Cliente:=Fnt_AgregaStrPorcentaje(TxtRazonSocial.Text)) Then
                        
      If .Cursor.Count > 0 Then
        Call Prc_ComponentOne.Sub_hRecord2Grilla(pCursor:=.Cursor _
                                             , pGrilla:=Grilla _
                                             , pColum_PK:="id_cliente" _
                                             , pAutoSize:=True)
      Else
        MsgBox "No hay información solictada", vbExclamation
      End If
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcClientes = Nothing

  If lID <> "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("id_cliente"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("id_cliente"))
    End If
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargaForm()
  Rem Limpia la grilla
  Grilla.Rows = 1
  
  Call Sub_FormControl_Color(Me.Controls)
End Sub

Private Function Fnt_AgregaStrPorcentaje(pValor As String)
  If Not pValor = "" Then
    If InStr(1, pValor, "%") = 0 Then
      pValor = "%" & UCase(pValor) & "%"
    End If
  End If
  Fnt_AgregaStrPorcentaje = pValor
End Function

Private Sub Txt_Rut_Cliente_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyReturn Then
    Call Sub_CargarDatos
  End If
End Sub

Private Sub TxtRazonSocial_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyReturn Then
    Call Sub_CargarDatos
  End If
End Sub

Private Function Fnt_AgregaStrPorcentajeFinal(pValor As String)
  If Not pValor = "" Then
    If InStr(1, pValor, "%") = 0 Then
      pValor = UCase(pValor) & "%"
    End If
  End If
  Fnt_AgregaStrPorcentajeFinal = pValor
End Function
