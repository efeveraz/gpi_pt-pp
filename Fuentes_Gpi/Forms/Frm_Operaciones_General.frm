VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Operaciones_General 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ingreso de Operaciones Varias"
   ClientHeight    =   8055
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10455
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8055
   ScaleWidth      =   10455
   Begin VB.Frame Frame_Operacion 
      Height          =   975
      Left            =   60
      TabIndex        =   14
      Top             =   7020
      Width           =   10365
      Begin VB.Frame Fra_Tipo_Operacion 
         Caption         =   "Tipo Operaci�n:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   585
         Left            =   6540
         TabIndex        =   15
         Top             =   210
         Width           =   2715
         Begin VB.OptionButton OptVenta 
            Caption         =   "Egreso"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1470
            TabIndex        =   6
            Top             =   232
            Width           =   1095
         End
         Begin VB.OptionButton OptCompra 
            Caption         =   "Ingreso"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   150
            TabIndex        =   5
            Top             =   240
            Value           =   -1  'True
            Width           =   1245
         End
      End
      Begin MSComctlLib.ImageCombo Cmb_Productos 
         Height          =   345
         Left            =   1110
         TabIndex        =   3
         Tag             =   "OBLI=S;CAPTION=Producto"
         Top             =   300
         Width           =   3675
         _ExtentX        =   6482
         _ExtentY        =   609
         _Version        =   393216
         ForeColor       =   -2147483640
         BackColor       =   12648384
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         ImageList       =   "Images"
      End
      Begin MSComctlLib.Toolbar Toolbar_Operacion 
         Height          =   330
         Left            =   4950
         TabIndex        =   4
         Top             =   300
         Width           =   1320
         _ExtentX        =   2328
         _ExtentY        =   582
         ButtonWidth     =   1614
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Operar"
               Key             =   "OPERAR"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Agregar un nemotecnico a la Operaci�n"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   255
            Left            =   9420
            TabIndex        =   16
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   17
         Top             =   300
         Width           =   945
      End
   End
   Begin VB.Frame Frame1 
      Height          =   6645
      Left            =   60
      TabIndex        =   9
      Top             =   390
      Width           =   10365
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2640
         Picture         =   "Frm_Operaciones_General.frx":0000
         TabIndex        =   1
         Top             =   270
         Width           =   375
      End
      Begin VB.Frame Frame2 
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1390
         Left            =   3360
         TabIndex        =   10
         Top             =   120
         Width           =   6945
         Begin hControl2.hTextLabel Txt_Rut 
            Height          =   315
            Left            =   120
            TabIndex        =   8
            Top             =   260
            Width           =   3390
            _ExtentX        =   5980
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   1200
            Caption         =   "RUT"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Perfil 
            Height          =   315
            Left            =   3540
            TabIndex        =   13
            Top             =   255
            Width           =   3300
            _ExtentX        =   5821
            _ExtentY        =   556
            LabelWidth      =   1000
            TextMinWidth    =   1200
            Caption         =   "Perfil Riesgo"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Nombres 
            Height          =   315
            Left            =   120
            TabIndex        =   18
            Top             =   615
            Width           =   6715
            _ExtentX        =   11853
            _ExtentY        =   556
            LabelWidth      =   1300
            Caption         =   "Nombres"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel txtNombreAsesor 
            Height          =   315
            Left            =   120
            TabIndex        =   19
            Top             =   975
            Width           =   6715
            _ExtentX        =   11853
            _ExtentY        =   556
            LabelWidth      =   1300
            Caption         =   "Asesor"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   4935
         Left            =   120
         TabIndex        =   12
         Top             =   1560
         Width           =   10185
         _cx             =   17965
         _cy             =   8705
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   1
         Cols            =   8
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Operaciones_General.frx":030A
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   120
         TabIndex        =   0
         Top             =   270
         Width           =   2475
         _ExtentX        =   4366
         _ExtentY        =   609
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "N� de Cuenta"
         Text            =   ""
         MaxLength       =   100
      End
      Begin TrueDBList80.TDBCombo Cmb_Caja 
         Height          =   345
         Left            =   1380
         TabIndex        =   2
         Top             =   720
         Visible         =   0   'False
         Width           =   1965
         _ExtentX        =   3466
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operaciones_General.frx":04AA
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HFFFFFF&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Lb_Caja 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Caja"
         Height          =   345
         Left            =   120
         TabIndex        =   20
         Top             =   720
         Visible         =   0   'False
         Width           =   1230
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   10455
      _ExtentX        =   18441
      _ExtentY        =   635
      ButtonWidth     =   1640
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   7920
         TabIndex        =   11
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Operaciones_General"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Rem -----------CONTROL CAMBIOS ----------------------------------------
Rem 15/07/2009. M.Mardones. Agrega nuevo ingreso de acciones por Monto
Rem -------------------------------------------------------------------

Dim fTipo_Operacion        As String
Dim fOper_Fecha_Anterior   As Boolean
Dim fInternacional         As Boolean
Dim fCod_Mercado           As String
Dim fPorMonto              As Boolean    '15/07/2009. Agregado por M.Mardones
Dim fEstadoProductoCierre  As Integer
Dim fCuentaSinFungibilidad As Boolean    '26/12/2016. Agregado por V.Cornejo

'Private Declare Function GetTickCount Lib "kernel32" () As Long

Private Sub Cmb_Cuentas_GotFocus()

Call Sub_Limpiar
'  Txt_Rut.Text = ""
'  Txt_Perfil.Text = ""
'  Txt_Nombres.Text = ""
'  txtNombreAsesor.Text = ""
'  Grilla.Rows = 1
End Sub

Private Sub Sub_Limpiar()
  Txt_Rut.Text = ""
  Txt_Perfil.Text = ""
  Txt_Nombres.Text = ""
  txtNombreAsesor.Text = ""
  Toolbar_Operacion.Buttons("OPERAR").Enabled = True
  Grilla.Rows = 1
End Sub

Private Sub Cmb_Cuentas_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Cmb_Cuentas_LostFocus()
  Call Sub_CargarDatos
End Sub

Private Sub Form_Activate()
Txt_Num_Cuenta.SetFocus
End Sub

Private Sub Form_Load()
  Me.Top = 1
  Me.Left = 1
  
  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Operacion
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("OPERAR").Image = "boton_seguir"
  End With
  Call RescataEstadoProductoCierre("FUNGIR_RF")
  Call Sub_CargaForm
  Cmb_Caja.BackColor = &HFFFFFF
End Sub

Private Sub Sub_CargaForm()
Dim lReg              As hCollection.hFields
Dim lReg_Inst         As hCollection.hFields
Dim lProductos        As hRecord
Dim lIndice           As Long
Dim lcTipo_Operacion  As Class_Tipos_Operaciones
'-----------------------------------------------
Dim lcInstrumentos  As Class_Instrumentos
Dim lcProducto      As Class_Productos

  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_FormControl_Color(Me.Controls)
  
  'Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, "id_cliente")
  
  If fInternacional Then
    fCod_Mercado = cMercado_Internacional
  Else
    fCod_Mercado = cMercado_Nacional
  End If
  
  Set Cmb_Productos.ImageList = MDI_Principal.ImageListGlobal16
    
  Set lcProducto = New Class_Productos
  With lcProducto
    If fKey = gcOPERACION_Custodia_SusCrip Then
      .Campo("cod_producto").Valor = gcPROD_RV_NAC
    End If
    If .Buscar Then
      For Each lReg In .Cursor
        If lReg("Cod_Mercado").Value = fCod_Mercado Then
          Call Cmb_Productos.ComboItems.Add(, "K" & lReg("cod_producto").Value, lReg("dsc_producto").Value, "open_folder")
          
          '-------------------
          '-- Instrumentos
          '-------------------
          Set lcInstrumentos = New Class_Instrumentos
          lcInstrumentos.Campo("cod_producto").Valor = lReg("cod_producto").Value
          If lcInstrumentos.Buscar Then
            For Each lReg_Inst In lcInstrumentos.Cursor
              With Cmb_Productos.ComboItems.Add(Key:="K" & lReg_Inst("cod_instrumento").Value, Text:=lReg_Inst("dsc_intrumento").Value, Image:="form_mantenedor", Indentation:=1)
                .Tag = NVL(lReg_Inst("formulario").Value, "")
              End With
            Next
          Else
            MsgBox lcInstrumentos.ErrMsg
          End If
          Set lcInstrumentos = Nothing
          '-------------------
        End If
      Next
    End If
  End With
  Set lcProducto = Nothing
   
  Set lcTipo_Operacion = New Class_Tipos_Operaciones
  With lcTipo_Operacion
    .Campo("COD_TIPO_OPERACION").Valor = fTipo_Operacion
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas al buscar el tipo de operaci�n", .ErrMsg, pConLog:=True)
    End If
    
    OptCompra.Caption = .Cursor(1)("DSC_TIPO_MOV_INGRESO").Value
    OptVenta.Caption = .Cursor(1)("DSC_TIPO_MOV_EGRESO").Value
  End With
    
  Call Sub_Desbloquea_Puntero(Me)
   
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
  With Grilla
    If .Row > 0 Then
      If Not .IsSubtotal(.Row) Then
        'Stop
      End If
    End If
  End With
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lResult As Boolean
  
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "NEXT"
      Call Sub_Mostrar_PantallaOperacion
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Mostrar_PantallaOperacion()
Dim lClass      As Object
Dim lcForm      As Object
Dim lResult     As Boolean
Dim lClass_Form As String
Dim lObject     As Object
'----------------------------------
Dim lId_Cuenta        As String
Dim lOperacion        As String
Dim lCod_Instrumento  As String
Dim lEsActiveX        As Boolean

  If Not Fnt_ValidarDatos Then
    Exit Sub
  End If
  
  Me.Enabled = False
    
  lId_Cuenta = Txt_Num_Cuenta.Tag
  lOperacion = IIf(OptCompra.Value, gcTipoOperacion_Ingreso, gcTipoOperacion_Egreso)
  
  lClass_Form = Cmb_Productos.SelectedItem.Tag
  lCod_Instrumento = Mid(Cmb_Productos.SelectedItem.Key, 2)
 
  lEsActiveX = False ' se inicia como que es normal el formulario
  
  Set lClass = Nothing
  Select Case UCase(NVL(lClass_Form, ""))
    Case "CLASS_ACCIONES"
      Set lClass = New Class_Acciones
    Case "CLASS_BONOS"
      Call RescataCuentaSinFungibilidad(lId_Cuenta)
      If fEstadoProductoCierre = 1 Then
         If fCuentaSinFungibilidad = False Then
            Set lClass = New Class_Bonos_II
            lClass_Form = "Class_Bonos_II"
         Else
            Set lClass = New Class_Bonos
         End If
      Else
         Set lClass = New Class_Bonos
      End If
    Case "CLASS_FONDOSMUTUOS"
      Set lClass = New Class_FondosMutuos
    Case "CLASS_DEPOSITOS"
      Set lClass = New Class_Depositos
    Case "CLASS_PACTOS"
      Set lClass = New Class_Pactos
    Case "CLASS_ACCIONES_INT"
      Set lClass = New Class_Acciones_Int
    Case "CLASS_BONOS_INT"
      Set lClass = New Class_Bonos_Int
    Case "CLASS_FONDOSMUTUOS_INT"
      Set lClass = New Class_FondosMutuos_Int
    Case Is <> ""
      'Si no es de los productos tradicionales
      Set lClass = Fnt_CreateObject(DesCifrado(lClass_Form))
      lEsActiveX = True
  End Select
  
  If Not lClass Is Nothing Then
    
    Set lcForm = Nothing
    Select Case fTipo_Operacion
      Case gcOPERACION_Directa
        '15/07/2009. Agregado por M.Mardones.
        If UCase(NVL(lClass_Form, "")) = "CLASS_ACCIONES" And fPorMonto Then
            Set lcForm = lClass.Form_Operacion_Directa_xMonto
        Else
            Set lcForm = lClass.Form_Operacion_Directa
        End If
      Case gcOPERACION_Instruccion
        '15/07/2009. Agregado por M.Mardones.
        If UCase(NVL(lClass_Form, "")) = "CLASS_ACCIONES" And fPorMonto Then
            Set lcForm = lClass.Form_Operacion_Instruccion_xMonto
        Else
            Set lcForm = lClass.Form_Operacion_Instruccion
        End If
      Case gcOPERACION_Custodia, gcOPERACION_Custodia_SusCrip
        Set lcForm = lClass.Form_Operacion_Custodia
    End Select
    
    If lEsActiveX Then
      'coloca el formulario dentro del mismo
      Call DockForm(lcForm, MDI_Principal)
    End If
    
    If Not lcForm Is Nothing Then
      Call Sub_FormControl_Enabled(Me.Controls, Me, False)
      If Not lEsActiveX Then
        If (lClass_Form = "Class_Acciones_Int") Or _
           (lClass_Form = "Class_Bonos_Int") Or _
           (lClass_Form = "Class_FondosMutuos_Int") Then
                lResult = lcForm.Mostrar(Me, _
                                         lId_Cuenta, _
                                         lOperacion, _
                                         fTipo_Operacion, _
                                         lCod_Instrumento, _
                                         Me.Tag, _
                                         fOper_Fecha_Anterior, _
                                         Cmb_Caja.Text, _
                                         Fnt_ComboSelected_KEY(Cmb_Caja))
        Else
            lResult = lcForm.Mostrar(Me, _
                                     lId_Cuenta, _
                                     lOperacion, _
                                     fTipo_Operacion, _
                                     lCod_Instrumento, _
                                     Me.Tag, _
                                     fOper_Fecha_Anterior)
        End If
      Else
        lResult = lcForm.Mostrar_Operacion(Me, _
                                           lId_Cuenta, _
                                           lOperacion, _
                                           fTipo_Operacion, _
                                           lCod_Instrumento, _
                                           Me.Tag, _
                                           fOper_Fecha_Anterior)
      End If
      Call Sub_FormControl_Enabled(Me.Controls, Me, True)
      
      If lResult Then
        MsgBox "Operaci�n ingresada existosamente.", vbInformation, Me.Caption
      End If
    End If
  End If
  
  Me.Enabled = True
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean

  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
  
  If Fnt_ValidarDatos Then
    If (Cmb_Productos.SelectedItem.Indentation = 0) Then
      Fnt_ValidarDatos = False
      MsgBox "Debe seleccionar un Instrumento para poder operar.", vbInformation, Me.Caption
      GoTo ExitProcedure
    End If
  End If
  
'  If Fnt_Verifica_Feriado(Fnt_FechaServidor) Then
'    Fnt_ValidarDatos = False
'    MsgBox "Solo se pueden ingresar operaciones en d�as habiles.", vbExclamation, Me.Caption
'    GoTo ExitProcedure
'  End If
  
ExitProcedure:
  
End Function

Private Sub Sub_CargarDatos()
Dim lcSaldos_Activos  As Class_Saldo_Activos
Dim lcCuenta          As Object
'-----------------------------------------------
Dim lReg          As hCollection.hFields
Dim lId_Cuenta    As String
Dim lLinea        As Long
Dim lMonto_Total  As Double

Dim sCodInstrumento
Dim IDMovActivo
Dim IdCuenta
Dim IDNemotecnico
Dim nSaldoActivo

  Call Sub_Bloquea_Puntero(Me)
  
  
  
  With Grilla
    '.BindToArray Null
    .Rows = 1
    .ExplorerBar = flexExNone
    .OutlineCol = 0
    .OutlineBar = flexOutlineBarComplete
    .SubtotalPosition = flexSTAbove
    .Subtotal flexSTClear
  End With

  'lId_Cuenta = Fnt_FindValue4Display(Cmb_Cuentas, Cmb_Cuentas.Text)
  
  lId_Cuenta = Txt_Num_Cuenta.Tag

  If Not lId_Cuenta = "" Then
    'Busca el perfil de la cuenta.
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
        .Campo("id_cuenta").Valor = lId_Cuenta
            If .Buscar_Vigentes Then
                If .Cursor.Count > 0 Then
                    Txt_Perfil.Text = .Cursor(1)("dsc_perfil_riesgo").Value
                    Txt_Rut.Text = "" & .Cursor(1)("rut_cliente").Value
                    Txt_Nombres.Text = "" & .Cursor(1)("nombre_cliente").Value
                    txtNombreAsesor.Text = "" & .Cursor(1)("nombre_Asesor").Value
                End If
            Else
                MsgBox "Problemas en cargar la Cuenta." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            End If
        End With

        Set lcSaldos_Activos = New Class_Saldo_Activos
    
            If lcSaldos_Activos.Buscar_Ultimo_CierreCuenta(pId_Cuenta:=lId_Cuenta, pCod_Mercado:=fCod_Mercado, pCod_Instrumento:="BONOS_NAC", pCodProductoCierre:=fEstadoProductoCierre) Then
                For Each lReg In lcSaldos_Activos.Cursor
                    lLinea = Grilla.Rows
                    Grilla.AddItem ""
                    SetCell Grilla, lLinea, "dsc_producto", lReg("dsc_producto").Value
                    SetCell Grilla, lLinea, "dsc_instrumento", lReg("Dsc_Intrumento").Value
                    SetCell Grilla, lLinea, "dsc_nemotecnico", lReg("nemotecnico").Value
                    '---------------
                    SetCell Grilla, lLinea, "valor_mercado", lReg("monto_mon_cta").Value
                    SetCell Grilla, lLinea, "cantidad", lReg("cantidad").Value
                    '---------------
                    SetCell Grilla, lLinea, "id_instrumento", lReg("cod_instrumento").Value
                    SetCell Grilla, lLinea, "costo_historico", lReg("saldo_disponible").Value
               Next
            Else
                MsgBox "Problemas en cargar el Cliente." & vbCr & vbCr & gDB.ErrMsg, vbCritical, Me.Caption
            End If

            With Grilla
                .Subtotal flexSTSum, -1, .ColIndex("valor_mercado"), "#,", &HFFC0C0, , True, "Cartera"
                .Subtotal flexSTSum, .ColIndex("dsc_producto"), .ColIndex("valor_mercado"), "#,", &HFED9D6, , True
                .Subtotal flexSTSum, .ColIndex("dsc_instrumento"), .ColIndex("valor_mercado"), "#,", &HFEE0DE, , True, , , True
                .Subtotal flexSTSum, .ColIndex("dsc_instrumento"), .ColIndex("cantidad"), "#,", &HFEE0DE, , True, , , True
                .AutoSize (.ColIndex("dsc_producto"))
            End With

            If Grilla.Rows > 1 Then
                lMonto_Total = To_Number(GetCell(Grilla, 1, "valor_mercado"))
            Else
                lMonto_Total = 0
            End If

            For lLinea = 1 To Grilla.Rows - 1
                If Not Grilla.IsSubtotal(lLinea) Then
                    If lMonto_Total > 0 Then
                        SetCell Grilla, lLinea, "porc_cartera", Round((To_Number(GetCell(Grilla, lLinea, "valor_mercado")) / lMonto_Total) * 100, 2)
                    Else
                        SetCell Grilla, lLinea, "porc_cartera", 0
                    End If
                End If
            Next

        With Grilla
            .Subtotal flexSTSum, -1, .ColIndex("porc_cartera"), "#,", , , False, , , True
            .Subtotal flexSTSum, .ColIndex("dsc_producto"), .ColIndex("porc_cartera"), "#,", , , True
            .Subtotal flexSTSum, .ColIndex("dsc_instrumento"), .ColIndex("porc_cartera"), "#,", , , True, , , True
        End With
        
        Else
            Call Sub_Limpiar
    End If


  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Toolbar_Operacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "OPERAR"
    
'        Select Case fTipo_Operacion
'               Case gcOPERACION_Instruccion
                If Cmb_Caja.Visible Then
                    If Cmb_Caja.ListCount = 0 Then
                       MsgBox "Cuenta no tiene caja asociada, no puede continuar con la operaci�n.", vbCritical, Me.Caption
                       Exit Sub
                    End If
                End If
'        End Select
      Call Sub_Mostrar_PantallaOperacion
  End Select
End Sub
' 15/07/2009. Modificado por M.Mardones.
'Public Function Mostrar(pTipo_Operacion, pOper_Fecha_Anterior As Boolean, Optional pInternacional As Boolean = False) As Boolean
Public Function Mostrar(pTipo_Operacion, pOper_Fecha_Anterior As Boolean, Optional pInternacional As Boolean = False, _
                        Optional pPorMonto As Boolean = False) As Boolean
Dim lCaption As String

  fTipo_Operacion = pTipo_Operacion
  fKey = fTipo_Operacion
  fInternacional = pInternacional
  fPorMonto = pPorMonto         '15/07/2009. Agregado por M.Mardones.
  
  fOper_Fecha_Anterior = pOper_Fecha_Anterior
  
  If fInternacional Then
    lCaption = " Internacionales"
    Cmb_Caja.Visible = True
    Lb_Caja.Visible = True
  Else
    Cmb_Caja.Visible = False
    Lb_Caja.Visible = False
    lCaption = ""
  End If
  
  With Me
    Select Case fTipo_Operacion
      Case gcOPERACION_Directa
        .Caption = "Ingreso de Operaciones Directas" & lCaption
        .Tag = "Directa"
      Case gcOPERACION_Custodia
        .Caption = "Ingreso de Operaciones de Custodia" & lCaption
        .Tag = "Custodia"
      Case gcOPERACION_Instruccion
        .Caption = "Ingreso de Operaciones de Instrucci�n" & lCaption
        .Tag = "Instrucci�n"
      Case gcOPERACION_Custodia_SusCrip
        .Caption = "Custodia de Opciones de Acciones"
        .Tag = "Suscripcion"
      Case Else
        .Caption = "Ingreso de Operaciones Varias"
        .Tag = ""
    End Select
    .Show
    .SetFocus
  End With
End Function

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call cmb_buscar_Click
    End If
End Sub

Private Sub cmb_buscar_Click()
Dim lId_Cuenta          As String
Dim fId_Cuenta          As String
Dim ftextCuenta         As String
Dim lcCuenta            As Object
Dim lId_MonedaDolar     As Integer
Dim lId_MonedaCaja      As Integer
Dim I                   As Long
Dim lPasoEncontroMonUSD As Boolean

    ftextCuenta = Txt_Num_Cuenta.Text & "-"
    If Len(ftextCuenta) > 1 Then
        ftextCuenta = Trim(Left(ftextCuenta, InStr(1, ftextCuenta, "-") - 1))
    Else
        ftextCuenta = ""
    End If
    fId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(ftextCuenta), 0)
    If fId_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = fId_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                    Txt_Rut.Text = .Cursor(1)("rut_cliente").Value
                    Txt_Nombres.Text = .Cursor(1)("nombre_cliente").Value
                    txtNombreAsesor.Text = .Cursor(1)("nombre_asesor").Value
                    fId_Cuenta = .Cursor(1)("id_cuenta").Value
                    Cmb_Caja.Text = ""
                    Cmb_Caja.ClearFields
                    Cmb_Caja.Clear
                    Cmb_Caja.EmptyRows = True
                    
                    '************************************************
                    Call Sub_CargaCombo_Cajas_Cuenta(Cmb_Caja, Txt_Num_Cuenta.Tag)
                    Call Sub_CargarDatos
                    lPasoEncontroMonUSD = False
                    lId_MonedaDolar = Buscar_Id_Moneda("USD")
                    If Cmb_Caja.ListCount > 0 Then
                       For I = 0 To Cmb_Caja.ListCount - 1
                           lId_MonedaCaja = Sub_RescataId_MonCaja(Cmb_Caja.Columns(0).ValueItems(I).Value, Txt_Num_Cuenta.Tag)
                           If lId_MonedaCaja = lId_MonedaDolar Then
                              Call Sub_ComboSelectedItem(Cmb_Caja, Cmb_Caja.Columns(0).ValueItems(I).Value)
                              lPasoEncontroMonUSD = True
                              Exit For
                           End If
                       Next I
                       If Not lPasoEncontroMonUSD Then
                          Call Sub_ComboSelectedItem(Cmb_Caja, Cmb_Caja.Columns(0).ValueItems(0).Value)
                       End If
                    End If
                    '************************************************
                    
                    Call ValidarAsesor
                Else
                    Txt_Num_Cuenta.Tag = ""
                    Txt_Num_Cuenta.Text = ""
                    fId_Cuenta = ""
                    Txt_Nombres.Text = ""
                    txtNombreAsesor.Text = ""
                    Txt_Perfil.Text = ""
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
    End If
Set lcCuenta = Nothing
End Sub

Function Sub_RescataId_MonCaja(pId_Caja_Cuenta, pId_Cuenta) As Integer
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lReg As hCollection.hFields
'---------------------------------------------
  
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_caja_cuenta").Valor = pId_Caja_Cuenta
    .Campo("id_Cuenta").Valor = pId_Cuenta
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Bancos.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
    
    For Each lReg In .Cursor
      Sub_RescataId_MonCaja = lReg("id_moneda").Value
    Next
  End With
  
ExitProcedure:
  Set lcCaja_Cuenta = Nothing
End Function


Private Sub ValidarAsesor()

 If Trim(txtNombreAsesor.Text) = "" Then
    MsgBox "No puede realizar operaci�n, cuenta no posee Asesor relacionado." & vbNewLine & "Debe realizar mantenci�n de la cuenta.", vbCritical, Me.Caption
    Toolbar_Operacion.Buttons("OPERAR").Enabled = False
 Else
    Toolbar_Operacion.Buttons("OPERAR").Enabled = True
 End If

End Sub

Private Sub RescataEstadoProductoCierre(lProductoCierre As String)
Dim lReg As hFields

  fEstadoProductoCierre = 0
  gDB.Parametros.Clear
  gDB.Procedimiento = "PKG_PROCESO_CIERRE$Verifica_Estado"
  Call gDB.Parametros.Add("pcursor", ePT_Cursor, "", eParam_Direc.ePD_Ambos)
  Call gDB.Parametros.Add("pProductoCierre", ePT_Caracter, lProductoCierre, eParam_Direc.ePD_Entrada)
 
  If gDB.EjecutaSP Then
     For Each lReg In gDB.Parametros("pCursor").Valor
         fEstadoProductoCierre = lReg("Estado").Value
     Next
  End If
  gDB.Parametros.Clear
  
End Sub

Private Sub RescataCuentaSinFungibilidad(lId_Cuenta)
Dim lReg As hFields

  fCuentaSinFungibilidad = False
  gDB.Parametros.Clear
  gDB.Procedimiento = "PKG_BUSCA_CTA_SIN_FUNGIR_RF"
  Call gDB.Parametros.Add("pcursor", ePT_Cursor, "", eParam_Direc.ePD_Ambos)
  Call gDB.Parametros.Add("pId_Cuenta", ePT_Numero, lId_Cuenta, eParam_Direc.ePD_Entrada)
 
  If gDB.EjecutaSP Then
    For Each lReg In gDB.Parametros("pCursor").Valor
      If lReg("Existe").Value = "S" Then
        fCuentaSinFungibilidad = True
      End If
    Next
  End If
  gDB.Parametros.Clear
  
End Sub
