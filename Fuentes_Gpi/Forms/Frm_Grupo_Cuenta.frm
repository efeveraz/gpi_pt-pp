VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "*\A..\..\hControl2\hControl2.vbp"
Begin VB.Form Frm_Grupo_Cuenta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Grupo Cuenta"
   ClientHeight    =   5160
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8595
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5160
   ScaleWidth      =   8595
   Begin TabDlg.SSTab SSTab 
      Height          =   4605
      Left            =   90
      TabIndex        =   0
      Top             =   480
      Width           =   8445
      _ExtentX        =   14896
      _ExtentY        =   8123
      _Version        =   393216
      TabsPerRow      =   6
      TabHeight       =   529
      WordWrap        =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "Frm_Grupo_Cuenta.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Direcciones"
      TabPicture(1)   =   "Frm_Grupo_Cuenta.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Grilla_Direcciones"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Varios"
      TabPicture(2)   =   "Frm_Grupo_Cuenta.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame3"
      Tab(2).ControlCount=   1
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Direcciones 
         Height          =   3915
         Left            =   -74850
         TabIndex        =   3
         Top             =   450
         Width           =   8115
         _cx             =   14314
         _cy             =   6906
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   6
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Grupo_Cuenta.frx":0054
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin VB.Frame Frame1 
         Height          =   4065
         Left            =   180
         TabIndex        =   4
         Top             =   360
         Width           =   8085
         Begin VB.Frame Frame2 
            Caption         =   "Conyuge"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   1905
            Left            =   4080
            TabIndex        =   9
            Top             =   2040
            Width           =   3885
            Begin hControl2.hTextLabel Txt_AP_Paterno_Conyuge 
               Height          =   315
               Left            =   180
               TabIndex        =   10
               Top             =   1050
               Width           =   3555
               _ExtentX        =   6271
               _ExtentY        =   556
               LabelWidth      =   1300
               Caption         =   "Apellido Paterno"
               Text            =   ""
            End
            Begin hControl2.hTextLabel Txt_AP_Materno_Conyuge 
               Height          =   315
               Left            =   180
               TabIndex        =   11
               Top             =   1410
               Width           =   3555
               _ExtentX        =   6271
               _ExtentY        =   556
               LabelWidth      =   1300
               Caption         =   "Apellido Materno"
               Text            =   ""
            End
            Begin hControl2.hTextLabel Txt_Rut_Conyuge 
               Height          =   315
               Left            =   180
               TabIndex        =   12
               Top             =   300
               Width           =   2535
               _ExtentX        =   4471
               _ExtentY        =   556
               LabelWidth      =   1300
               TextMinWidth    =   1200
               Caption         =   "RUT"
               Text            =   ""
            End
            Begin hControl2.hTextLabel Txt_Nombres_Conyuge 
               Height          =   315
               Left            =   180
               TabIndex        =   13
               Top             =   690
               Width           =   3525
               _ExtentX        =   6218
               _ExtentY        =   556
               LabelWidth      =   1300
               Caption         =   "Nombres"
               Text            =   ""
            End
         End
         Begin hControl2.hTextLabel Txt_Rut 
            Height          =   315
            Left            =   150
            TabIndex        =   5
            Top             =   270
            Width           =   3090
            _ExtentX        =   5450
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   1200
            Caption         =   "RUT"
            Text            =   ""
         End
         Begin hControl2.hTextLabel Txt_Nombres 
            Height          =   315
            Left            =   150
            TabIndex        =   6
            Top             =   660
            Width           =   3525
            _ExtentX        =   6218
            _ExtentY        =   556
            LabelWidth      =   1300
            Caption         =   "Nombres"
            Text            =   ""
         End
         Begin MSComctlLib.ImageCombo Cmb_Sexo 
            Height          =   345
            Left            =   5700
            TabIndex        =   7
            Top             =   390
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   609
            _Version        =   393216
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Locked          =   -1  'True
            ImageList       =   "Images"
         End
         Begin VB.Label Label3 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Sexo"
            Height          =   345
            Left            =   4200
            TabIndex        =   8
            Top             =   390
            Width           =   1485
         End
      End
      Begin VB.Frame Frame3 
         Height          =   4065
         Left            =   -74820
         TabIndex        =   14
         Top             =   360
         Width           =   8085
         Begin VB.TextBox Txt_Observación 
            Height          =   1875
            Left            =   1650
            MultiLine       =   -1  'True
            TabIndex        =   15
            Top             =   2010
            Width           =   6315
         End
         Begin hControl2.hTextLabel Txt_Sucursal 
            Height          =   315
            Left            =   120
            TabIndex        =   16
            Top             =   270
            Width           =   3510
            _ExtentX        =   6191
            _ExtentY        =   556
            LabelWidth      =   1500
            TextMinWidth    =   1200
            Caption         =   "Sucursal"
            Text            =   ""
         End
         Begin hControl2.hTextLabel Txt_Nom_Ejecutivo 
            Height          =   315
            Left            =   120
            TabIndex        =   17
            Top             =   660
            Width           =   3525
            _ExtentX        =   6218
            _ExtentY        =   556
            LabelWidth      =   1500
            Caption         =   "Nombre Ejecutivo"
            Text            =   ""
         End
         Begin VB.Label Label2 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Observación"
            Height          =   330
            Left            =   120
            TabIndex        =   18
            Top             =   2040
            Width           =   1485
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8595
      _ExtentX        =   15161
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la información de forma Permanente"
            Object.ToolTipText     =   "Graba la información de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la información perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la información perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   2
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Grupo_Cuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const cTipo_Estado = 1 'Este codigo es unico para los clientes.

Public fKey As String

Private Enum eGrilla_Dir
  eGD_Direccion
  eGD_Comuna
End Enum

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = "boton_grabar"
      .Buttons("EXIT").Image = "boton_salir"
      .Buttons("REFRESH").Image = "boton_original"
  End With

  Call Sub_CargaForm

End Sub

Public Function Fnt_Modificar(ByVal pId_Cliente As String)
  fKey = pId_Cliente
  
  Call Sub_CargarDatos
    
  Me.Caption = "Modificación Cliente: " & Txt_Rut.Text & " - " & Txt_Nombres.Text
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acción perdera las modificaciones." & vbLf & "¿Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub


Private Function Fnt_Grabar() As Boolean
Dim lSexo As String

  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If

  lSexo = Fnt_BuscaKey_In_TextCombo(Cmb_Sexo, Cmb_Sexo.SelectedItem.Text)

  With gDB
    .Procedimiento = "el procedimiento"
    Call .Parametros.Add("Pid_Cliente", DLL_COMUN.ePT_Numero, fKey, DLL_COMUN.ePD_Ambos)
    Call .Parametros.Add("Pid_Tipo_Estado", DLL_COMUN.ePT_Numero, cTipo_Estado, DLL_COMUN.ePD_Entrada)
        
    If Not .EjecutaSP Then
      MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      Fnt_Grabar = False
    End If
    
    .Parametros.Clear
  End With
End Function

Private Sub Sub_CargaForm()
Dim lReg As hFields


  'Carga los Estado del Cliente
  With gDB
    .Procedimiento = "Rea_Estados"
    .Parametros.Add "Pid_Tipo_Estado", DLL_COMUN.ePT_Numero, cTipo_Estado, DLL_COMUN.ePD_Entrada
    .Parametros.Add "pCursor", DLL_COMUN.ePT_Cursor, "", DLL_COMUN.ePD_Ambos
    
    If .EjecutaSP Then
      Cmb_Estado.ComboItems.Clear
      Cmb_Estado.Text = ""
      Set Cmb_Estado.SelectedItem = Nothing
      
      For Each lReg In .Parametros("pcursor").Valor
        Cmb_Estado.ComboItems.Add Key:="K" & lReg("cod_estado").Value, Text:=lReg("dsc_estado").Value
      Next
    Else
      MsgBox "Problemas en cargar los estados." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
    
    .Parametros.Clear
  End With
  
  With Cmb_Sexo.ComboItems
    .Clear
    
    .Add Key:="KM", Text:="Masculino"
    .Add Key:="KF", Text:="Femenino"
    
    Cmb_Sexo.Text = ""
    Set Cmb_Sexo.SelectedItem = Nothing
  End With
  
  With Cmb_Estado_Civil.ComboItems
    .Clear
    
    .Add Key:="KSOL", Text:="Soltero"
    .Add Key:="KCAS", Text:="Casado"
    .Add Key:="KVIU", Text:="Viudo"
    .Add Key:="KSEP", Text:="Separado"
    
    Cmb_Estado_Civil.Text = ""
    Set Cmb_Estado_Civil.SelectedItem = Nothing
  End With
  
  
  Txt_Fecha_Nacimiento.Value = Date
  SSTab.Tab = 0
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
  
  Load Me
  With gDB
    .Procedimiento = "el procedimiento"
    .Parametros.Add "pCursor", DLL_COMUN.ePT_Cursor, "", DLL_COMUN.ePD_Ambos
    .Parametros.Add "pId_Cliente", DLL_COMUN.ePT_Numero, fKey, DLL_COMUN.ePD_Entrada
    If .EjecutaSP Then
      For Each lReg In .Parametros("pcursor").Valor
      
        Txt_Rut.Text = DLL_COMUN.NVL(lReg("rut_cliente").Value, "")
        Txt_Nombres.Text = DLL_COMUN.NVL(lReg("nombres").Value, "")
        Set Cmb_Estado.SelectedItem = Cmb_Estado.ComboItems("K" & DLL_COMUN.NVL(lReg("cod_estado").Value, "V"))
      Next
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
    
    .Parametros.Clear
  End With
  
  
  Grilla_Direcciones.Rows = 1
  With gDB
    .Procedimiento = "el procedimiento"
    .Parametros.Add "pId_Cliente", DLL_COMUN.ePT_Numero, fKey, DLL_COMUN.ePD_Entrada
    .Parametros.Add "pCursor", DLL_COMUN.ePT_Cursor, "", DLL_COMUN.ePD_Ambos
    If .EjecutaSP Then
      For Each lReg In .Parametros("pcursor").Valor
        lLinea = Grilla_Direcciones.Rows
        Grilla_Direcciones.Rows = lLinea + 1
        
        Grilla_Direcciones.TextMatrix(lLinea, eGD_Direccion) = DLL_COMUN.NVL(lReg("direccion").Value, "")
        Grilla_Direcciones.TextMatrix(lLinea, eGD_Comuna) = DLL_COMUN.NVL(lReg("comuna").Value, "")
      Next
    Else
      MsgBox "Problemas en cargar" & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
    
    Grilla_Direcciones.AutoSize eGD_Direccion
    Grilla_Direcciones.AutoSize eGD_Comuna
    
    .Parametros.Clear
  End With
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = True
  
  If Fnt_PreguntaIsNull(Txt_Rut) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If Fnt_PreguntaIsNull(Txt_Nombres) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If Fnt_PreguntaIsNull(Txt_Ap_Paterno) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If

  If Fnt_PreguntaIsNull(Txt_Ap_Materno) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If Fnt_PreguntaIsNothing(Cmb_Estado, "Estado") Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If Fnt_PreguntaIsNothing(Cmb_Sexo, "Sexo") Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If Fnt_PreguntaIsNothing(Cmb_Estado_Civil, "Estado Civil") Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
End Function
