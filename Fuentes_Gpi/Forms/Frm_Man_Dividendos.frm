VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Man_Dividendos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor Variaciones de Capital"
   ClientHeight    =   4620
   ClientLeft      =   165
   ClientTop       =   540
   ClientWidth     =   9660
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4620
   ScaleWidth      =   9660
   Begin VB.Frame Frame1 
      Caption         =   "Dividendos por Nemot�cnico"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4065
      Left            =   15
      TabIndex        =   0
      Top             =   390
      Width           =   9615
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   2805
         Left            =   120
         TabIndex        =   1
         Top             =   1140
         Width           =   9345
         _cx             =   16484
         _cy             =   4948
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   7
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Dividendos.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   0   'False
         AutoSizeMode    =   1
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin TrueDBList80.TDBCombo Cmb_Nemotecnico 
         Height          =   345
         Left            =   1440
         TabIndex        =   5
         Tag             =   "OBLI=S;CAPTION=Nemot�cnico;SOLOLECTURA=N"
         Top             =   330
         Width           =   4155
         _ExtentX        =   7329
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Man_Dividendos.frx":0195
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_TipoVariacion 
         Height          =   345
         Left            =   1440
         TabIndex        =   6
         Tag             =   "OBLI=S;CAPTION=Tipo Variaci�n"
         Top             =   690
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Man_Dividendos.frx":023F
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label lbl_tipo_precio 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tipo Variaci�n"
         Height          =   345
         Left            =   150
         TabIndex        =   7
         Top             =   690
         Width           =   1305
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Nemot�cnico"
         Height          =   345
         Left            =   150
         TabIndex        =   4
         Top             =   330
         Width           =   1305
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   9660
      _ExtentX        =   17039
      _ExtentY        =   635
      ButtonWidth     =   2408
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Agrega un Dividendo"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Eliminar"
            Key             =   "DEL"
            Object.ToolTipText     =   "Elimina un Dividendo"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Object.ToolTipText     =   "Modifica un Dividendo"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   8820
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Dividendos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Rem ---------------------------------------------------------------------
Rem 31/07/2009 MMardones. Titulo del reporte y de mensajes depende del
Rem            Tipo de variaci�n seleccionado y no debe ir en duro
Rem            el valor Dividendos
Rem ---------------------------------------------------------------------
Private Enum eNem_Colum
  eNem_nemotecnico
  eNem_Descripcion
  eNem_Id_Nemotecnico
End Enum

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Cmb_Nemotecnico_ItemChange()
  Call Sub_CargaDatos
End Sub

Private Sub Cmb_TipoVariacion_ItemChange()
  Call Sub_CargaDatos
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With

  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaDatos()
Dim lId_Nemotecnico As Long
Dim lNemotecnico As String
Dim lReg As hFields
Dim lLinea As Long
Dim lID As String
Dim lTipoVariacion As String
Dim lDividendo As Class_VariacionCapital
  
  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Grilla.Rows = 1
  
  Grilla.ColFormat(Grilla.ColIndex("Monto_Dividendo")) = "#,##0.##########"
  Grilla.ColFormat(Grilla.ColIndex("cada_x_acciones")) = "#,##0"
  Grilla.ColFormat(Grilla.ColIndex("entregar_x_acciones")) = "#,##0.############"

  If Not Trim(Cmb_Nemotecnico.Text) = "" Then
    lId_Nemotecnico = Val(Cmb_Nemotecnico.Columns(eNem_Id_Nemotecnico).Text)
    lNemotecnico = Trim(Cmb_Nemotecnico.Columns(eNem_nemotecnico).Text)
    
    Set lDividendo = New Class_VariacionCapital
    With lDividendo
      If Not lId_Nemotecnico = 0 Then
        .Campo("Id_Nemotecnico").Valor = lId_Nemotecnico
        If UCase(Cmb_TipoVariacion.Text) <> "TODAS" Then
            .Campo("Tipo_Variacion").Valor = Cmb_TipoVariacion.Text
        End If
      End If
      If .Buscar() Then
        For Each lReg In .Cursor
          lLinea = Grilla.Rows
          
          Grilla.AddItem ""
          Call SetCell(Grilla, lLinea, "colum_pk", lReg("ID_Dividendo").Value)
          Call SetCell(Grilla, lLinea, "Tipo_Variacion", lReg("Tipo_Variacion").Value)
          Call SetCell(Grilla, lLinea, "Fecha_Corte", lReg("Fecha_Corte").Value)
          Call SetCell(Grilla, lLinea, "Monto_Dividendo", lReg("Monto").Value)
          Call SetCell(Grilla, lLinea, "Fecha_Dividendo", lReg("Fecha").Value) 'Fecha de Cierre
          Call SetCell(Grilla, lLinea, "cada_x_acciones", lReg("CADA_X_CANTIDAD").Value)
          Call SetCell(Grilla, lLinea, "entregar_x_acciones", lReg("ENTREGAR_X_CANTIDAD").Value)
        Next
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en la carga de Dividendos.", _
                        .ErrMsg, _
                        pConLog:=True)
      End If
    End With
  End If

  Cmb_Nemotecnico.Text = lNemotecnico

End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
  If Grilla.Row > 0 Then
    Call Sub_EsperaVentana(GetCell(Grilla, Grilla.Row, "colum_pk"), Cmb_Nemotecnico.Columns(eNem_Id_Nemotecnico).Text, GetCell(Grilla, Grilla.Row, "Tipo_Variacion"))
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agregar
    Case "DEL"
      Call Sub_Eliminar
    Case "UPDATE"
      Call Grilla_DblClick
    Case "REFRESH"
      Call Sub_Limpiar
    Case "EXIT"
      Unload Me
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Sub_Limpiar()
  Cmb_Nemotecnico.Text = ""
  Grilla.Rows = 1
End Sub

Private Sub Sub_Agregar()
Dim sTipoVariacion As String
    sTipoVariacion = IIf(Cmb_TipoVariacion.Text = "Todas", "Deposito/Opci�n", Cmb_TipoVariacion.Text)
  If Cmb_Nemotecnico.Text = "" Then
    MsgBox "Elija un Nemot�cnico para agregar un " & sTipoVariacion & ".", vbExclamation, Me.Caption
  Else
    If Cmb_TipoVariacion.Text = "Todas" Then
        MsgBox "Elija un Tipo de Variaci�n para permitir Agregar"
    Else
        Call Sub_EsperaVentana(cNewEntidad, Cmb_Nemotecnico.Columns(eNem_Id_Nemotecnico).Text, Cmb_TipoVariacion.Text)
    End If
  End If
End Sub

Private Sub Sub_EsperaVentana(ByVal pId_Dividendo As String, ByVal pId_Nemotecnico As String, ByVal pTipoVariacion As String)
Dim lForm As Frm_Dividendo
Dim lNombre As String
  
  Me.Enabled = False
  
  If Not Fnt_ExisteVentanaKey("Frm_Dividendo", pId_Dividendo) Then
    lNombre = Me.Name
    
    Set lForm = New Frm_Dividendo
    Call lForm.Fnt_Modificar(pId_Dividendo _
                           , pId_Nemotecnico _
                           , pCod_Arbol_Sistema:=fCod_Arbol_Sistema _
                           , pTipo_Variacion:=pTipoVariacion)
    
    Do While Fnt_ExisteVentanaKey("Frm_Dividendo", pId_Dividendo)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      Call Sub_CargaDatos
    Else
      Exit Sub
    End If
  End If
  
  Me.Enabled = True
  
End Sub

Private Sub Sub_CargaForm()
Dim lCampo As hFields
Dim lNemotecnico As Class_Nemotecnicos

  Rem Limpia la grilla
  Grilla.Rows = 1
  

  Call Sub_FormControl_Color(Me.Controls)

   '------------------------------------------------
   '-- Carga los nemotecnicos
   '------------------------------------------------
   Call Sub_LimpiarTDBCombo(Cmb_Nemotecnico)
   
   With Cmb_Nemotecnico
      With .Columns.Add(eNem_nemotecnico)
         .Caption = "Nemot�cnico"
         .Visible = True
         
      End With
      With .Columns.Add(eNem_Descripcion)
         .Caption = "Descripci�n"
         .Visible = True
      End With
      With .Columns.Add(eNem_Id_Nemotecnico)
         .Caption = "id_nemotecnico"
         .Visible = False
      End With
       
      Set lNemotecnico = New Class_Nemotecnicos
      lNemotecnico.Campo("cod_instrumento").Valor = gcINST_ACCIONES_NAC
      lNemotecnico.Campo("cod_estado").Valor = cCod_Estado_Vigente
      If lNemotecnico.Buscar Then
         For Each lCampo In lNemotecnico.Cursor
            Call Cmb_Nemotecnico.AddItem(lCampo("nemotecnico").Value & ";" & lCampo("dsc_nemotecnico").Value & ";" & lCampo("id_nemotecnico").Value)
         Next
      End If
      ' AGREGA LOS FFMM
      lNemotecnico.Campo("cod_instrumento").Valor = gcINST_FFMM_RF_NAC
      lNemotecnico.Campo("cod_estado").Valor = cCod_Estado_Vigente
      If lNemotecnico.Buscar Then
         For Each lCampo In lNemotecnico.Cursor
            Call Cmb_Nemotecnico.AddItem(lCampo("nemotecnico").Value & ";" & lCampo("dsc_nemotecnico").Value & ";" & lCampo("id_nemotecnico").Value)
         Next
      End If
      
      lNemotecnico.Campo("cod_instrumento").Valor = gcINST_FFMM_RV_NAC
      lNemotecnico.Campo("cod_estado").Valor = cCod_Estado_Vigente
      If lNemotecnico.Buscar Then
         For Each lCampo In lNemotecnico.Cursor
            Call Cmb_Nemotecnico.AddItem(lCampo("nemotecnico").Value & ";" & lCampo("dsc_nemotecnico").Value & ";" & lCampo("id_nemotecnico").Value)
         Next
      End If
      
'Inicio JGR 190509
      lNemotecnico.Campo("cod_instrumento").Valor = gcINST_FFMM_FIP_NAC
      lNemotecnico.Campo("cod_estado").Valor = cCod_Estado_Vigente
      If lNemotecnico.Buscar Then
         For Each lCampo In lNemotecnico.Cursor
            Call Cmb_Nemotecnico.AddItem(lCampo("nemotecnico").Value & ";" & lCampo("dsc_nemotecnico").Value & ";" & lCampo("id_nemotecnico").Value)
         Next
      End If
      
      lNemotecnico.Campo("cod_instrumento").Valor = gcINST_FFMM_CA_NAC
      lNemotecnico.Campo("cod_estado").Valor = cCod_Estado_Vigente
      If lNemotecnico.Buscar Then
         For Each lCampo In lNemotecnico.Cursor
            Call Cmb_Nemotecnico.AddItem(lCampo("nemotecnico").Value & ";" & lCampo("dsc_nemotecnico").Value & ";" & lCampo("id_nemotecnico").Value)
         Next
      End If
      
      lNemotecnico.Campo("cod_instrumento").Valor = gcINST_FFMM_FI_NAC
      lNemotecnico.Campo("cod_estado").Valor = cCod_Estado_Vigente
      If lNemotecnico.Buscar Then
         For Each lCampo In lNemotecnico.Cursor
            Call Cmb_Nemotecnico.AddItem(lCampo("nemotecnico").Value & ";" & lCampo("dsc_nemotecnico").Value & ";" & lCampo("id_nemotecnico").Value)
         Next
      End If
      
'Fin JGR 190509
      
      
   End With
   
  With Cmb_TipoVariacion
    Call .AddItem("Todas")
    Call .AddItem("Dividendos")
    Call .AddItem("Opcion")
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem("ALL", "Todas")
      .Add Fnt_AgregaValueItem("DIVIDENDOS", "Dividendos")
      .Add Fnt_AgregaValueItem("OPCION", "Opcion")
      .Translate = True
    End With
    
    '.Text = ""
  End With
  Call Sub_ComboSelectedItem(Cmb_TipoVariacion, "ALL")
End Sub

Private Sub Sub_Eliminar()
Dim lId_Dividendo As String
Dim lDividendo As Class_VariacionCapital
Dim sMensaje As String
Dim sTipoVariacion As String

  If Grilla.Row > 0 Then
    sTipoVariacion = IIf(UCase(GetCell(Grilla, Grilla.Row, "Tipo_Variacion")) = "OPCION", "la Opci�n", "el Dividendo")
    
    If MsgBox("Esta seguro(a) que desea eliminar " & sTipoVariacion & "?.", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
      If GetCell(Grilla, Grilla.Row, "colum_pk") = "" Then
        Grilla.RemoveItem (Grilla.Row)
      Else
        Set lDividendo = New Class_VariacionCapital
        lId_Dividendo = GetCell(Grilla, Grilla.Row, "colum_pk")
        With lDividendo
          .Campo("id_dividendo").Valor = lId_Dividendo
          .Campo("mensaje").Valor = "OK"
          If .Borrar Then
            'Agregado por MMA. 29/09/2008
            sMensaje = NVL(.Campo("MENSAJE").Valor, "")
            MsgBox sMensaje, vbInformation + vbOKOnly, Me.Caption
            If Mid(sMensaje, 1, 5) <> "ERROR" Then
                Grilla.RemoveItem (Grilla.Row)
            End If
          Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al eliminar el Dividendo.", _
                        .ErrMsg, _
                        pConLog:=True)
          End If
        End With
      End If
    End If
  End If
End Sub
Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
Dim lNemotecnico As String
Dim sTitulo      As String
Dim sTipoVariacion As String
  
  Set lForm = New Frm_Reporte_Generico
  lNemotecnico = Cmb_Nemotecnico.Text
  Rem Comienzo de la generaci�n del reporte
  sTipoVariacion = IIf(Cmb_TipoVariacion.Text = "Todas", "Dividendos y Opciones", Cmb_TipoVariacion.Text)
  sTitulo = "Listado de " & sTipoVariacion  '31/07/2009. Agregado por MMardones
  With lForm
'    Call .Sub_InicarDocumento(pTitulo:="Listado de Dividendos" _
'                            , pTipoSalida:=pTipoSalida _
'                            , pOrientacion:=orLandscape)
    Call .Sub_InicarDocumento(pTitulo:=sTitulo _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Nemot�cnico: " & lNemotecnico
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub


Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

