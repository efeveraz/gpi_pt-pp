VERSION 5.00
Object = "{C8CF160E-7278-4354-8071-850013B36892}#1.0#0"; "vsrpt8.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reportes_Activos_Ctas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Activos por Cuenta"
   ClientHeight    =   1305
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6090
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1305
   ScaleWidth      =   6090
   Begin VB.Frame Frame1 
      Caption         =   "Cuentas Vigentes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   825
      Left            =   60
      TabIndex        =   5
      Top             =   420
      Width           =   5925
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3000
         Picture         =   "Frm_Reportes.frx":0000
         TabIndex        =   8
         Top             =   270
         Width           =   375
      End
      Begin MSComCtl2.DTPicker DTP_Fecha 
         Height          =   345
         Left            =   4470
         TabIndex        =   6
         Top             =   270
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   55508993
         CurrentDate     =   37732
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   120
         TabIndex        =   9
         Top             =   270
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   609
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "N� de Cuenta"
         Text            =   ""
         MaxLength       =   12
      End
      Begin VB.Label Lbl_Fecha 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         Height          =   345
         Left            =   3630
         TabIndex        =   7
         Top             =   270
         Width           =   795
      End
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Render 2"
      Height          =   525
      Left            =   9000
      TabIndex        =   4
      Top             =   2610
      Width           =   1425
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Archivo XML"
      Height          =   645
      Left            =   9000
      TabIndex        =   3
      Top             =   3180
      Width           =   1425
   End
   Begin VB.CommandButton Command4 
      Caption         =   "HTML"
      Height          =   585
      Left            =   9000
      TabIndex        =   2
      Top             =   3870
      Width           =   1395
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6090
      _ExtentX        =   10742
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   3
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "XLS"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin TrueDBList80.TDBCombo Cmb_Cuentas 
      Height          =   345
      Left            =   810
      TabIndex        =   10
      Tag             =   "OBLI=S;CAPTION=Cuentas"
      Top             =   0
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   609
      _LayoutType     =   0
      _RowHeight      =   -2147483647
      _WasPersistedAsPixels=   0
      _DropdownWidth  =   0
      _EDITHEIGHT     =   609
      _GAPHEIGHT      =   53
      Columns(0)._VlistStyle=   0
      Columns(0)._MaxComboItems=   5
      Columns(0).DataField=   "ub_grid1"
      Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
      Columns(1)._VlistStyle=   0
      Columns(1)._MaxComboItems=   5
      Columns(1).DataField=   "ub_grid2"
      Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
      Columns.Count   =   2
      Splits(0)._UserFlags=   0
      Splits(0).ExtendRightColumn=   -1  'True
      Splits(0).AllowRowSizing=   0   'False
      Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
      Splits(0)._ColumnProps(0)=   "Columns.Count=2"
      Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
      Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
      Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
      Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
      Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
      Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
      Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
      Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
      Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
      Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
      Splits.Count    =   1
      Appearance      =   3
      BorderStyle     =   1
      ComboStyle      =   0
      AutoCompletion  =   -1  'True
      LimitToList     =   0   'False
      ColumnHeaders   =   0   'False
      ColumnFooters   =   0   'False
      DataMode        =   5
      DefColWidth     =   0
      Enabled         =   -1  'True
      HeadLines       =   1
      FootLines       =   1
      RowDividerStyle =   0
      Caption         =   ""
      EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
      LayoutName      =   ""
      LayoutFileName  =   ""
      MultipleLines   =   0
      EmptyRows       =   0   'False
      CellTips        =   0
      AutoSize        =   0   'False
      ListField       =   ""
      BoundColumn     =   ""
      IntegralHeight  =   0   'False
      CellTipsWidth   =   0
      CellTipsDelay   =   1000
      AutoDropdown    =   -1  'True
      RowTracking     =   -1  'True
      RightToLeft     =   0   'False
      MouseIcon       =   0
      MouseIcon.vt    =   3
      MousePointer    =   0
      MatchEntryTimeout=   2000
      OLEDragMode     =   0
      OLEDropMode     =   0
      AnimateWindow   =   3
      AnimateWindowDirection=   5
      AnimateWindowTime=   200
      AnimateWindowClose=   1
      DropdownPosition=   0
      Locked          =   0   'False
      ScrollTrack     =   -1  'True
      ScrollTips      =   -1  'True
      RowDividerColor =   14215660
      RowSubDividerColor=   14215660
      MaxComboItems   =   10
      AddItemSeparator=   ";"
      _PropDict       =   $"Frm_Reportes.frx":030A
      _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
      _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
      _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
      _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
      _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
      _StyleDefs(5)   =   ":id=0,.fontname=Arial"
      _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
      _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
      _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
      _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
      _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
      _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
      _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
      _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
      _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
      _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
      _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
      _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
      _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
      _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
      _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
      _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
      _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
      _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
      _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
      _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
      _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
      _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
      _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
      _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
      _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
      _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
      _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
      _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
      _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
      _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
      _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
      _StyleDefs(38)  =   "Named:id=33:Normal"
      _StyleDefs(39)  =   ":id=33,.parent=0"
      _StyleDefs(40)  =   "Named:id=34:Heading"
      _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(42)  =   ":id=34,.wraptext=-1"
      _StyleDefs(43)  =   "Named:id=35:Footing"
      _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(45)  =   "Named:id=36:Selected"
      _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
      _StyleDefs(47)  =   "Named:id=37:Caption"
      _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
      _StyleDefs(49)  =   "Named:id=38:HighlightRow"
      _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
      _StyleDefs(51)  =   "Named:id=39:EvenRow"
      _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
      _StyleDefs(53)  =   "Named:id=40:OddRow"
      _StyleDefs(54)  =   ":id=40,.parent=33"
      _StyleDefs(55)  =   "Named:id=41:RecordSelector"
      _StyleDefs(56)  =   ":id=41,.parent=34"
      _StyleDefs(57)  =   "Named:id=42:FilterBar"
      _StyleDefs(58)  =   ":id=42,.parent=33"
   End
   Begin VB.Label Label2 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Cuentas"
      Height          =   330
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   795
   End
   Begin VSReport8LibCtl.VSReport VSReport1 
      Left            =   9000
      Top             =   4680
      _rv             =   800
      ReportName      =   "Prueba"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OnOpen          =   ""
      OnClose         =   ""
      OnNoData        =   ""
      OnPage          =   ""
      OnError         =   ""
      MaxPages        =   0
      DoEvents        =   -1  'True
      BeginProperty Layout {D853A4F1-D032-4508-909F-18F074BD547A} 
         Width           =   9024
         MarginLeft      =   1440
         MarginTop       =   1440
         MarginRight     =   1440
         MarginBottom    =   1440
         Columns         =   1
         ColumnLayout    =   0
         Orientation     =   1
         PageHeader      =   0
         PageFooter      =   0
         PictureAlign    =   7
         PictureShow     =   1
         PaperSize       =   0
      EndProperty
      BeginProperty DataSource {D1359088-0913-44EA-AE50-6A7CD77D4C50} 
         ConnectionString=   ""
         RecordSource    =   ""
         Filter          =   ""
         MaxRecords      =   0
      EndProperty
      GroupCount      =   0
      SectionCount    =   5
      BeginProperty Section0 {673CB92F-28D3-421F-86CD-1099425A5037} 
         Name            =   "Detail"
         Visible         =   -1  'True
         Height          =   500
         CanGrow         =   -1  'True
         CanShrink       =   0   'False
         KeepTogether    =   -1  'True
         ForcePageBreak  =   0
         BackColor       =   16777215
         Repeat          =   0   'False
         OnFormat        =   ""
         OnPrint         =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Section1 {673CB92F-28D3-421F-86CD-1099425A5037} 
         Name            =   "Header"
         Visible         =   -1  'True
         Height          =   1000
         CanGrow         =   -1  'True
         CanShrink       =   0   'False
         KeepTogether    =   -1  'True
         ForcePageBreak  =   0
         BackColor       =   16777215
         Repeat          =   0   'False
         OnFormat        =   ""
         OnPrint         =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Section2 {673CB92F-28D3-421F-86CD-1099425A5037} 
         Name            =   "Footer"
         Visible         =   0   'False
         Height          =   0
         CanGrow         =   -1  'True
         CanShrink       =   0   'False
         KeepTogether    =   -1  'True
         ForcePageBreak  =   0
         BackColor       =   16777215
         Repeat          =   0   'False
         OnFormat        =   ""
         OnPrint         =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Section3 {673CB92F-28D3-421F-86CD-1099425A5037} 
         Name            =   "Page Header"
         Visible         =   -1  'True
         Height          =   600
         CanGrow         =   -1  'True
         CanShrink       =   0   'False
         KeepTogether    =   -1  'True
         ForcePageBreak  =   0
         BackColor       =   16777215
         Repeat          =   0   'False
         OnFormat        =   ""
         OnPrint         =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Section4 {673CB92F-28D3-421F-86CD-1099425A5037} 
         Name            =   "Page Footer"
         Visible         =   -1  'True
         Height          =   500
         CanGrow         =   -1  'True
         CanShrink       =   0   'False
         KeepTogether    =   -1  'True
         ForcePageBreak  =   0
         BackColor       =   16777215
         Repeat          =   0   'False
         OnFormat        =   ""
         OnPrint         =   ""
         Object.Tag             =   ""
      EndProperty
      FieldCount      =   4
      BeginProperty Field0 {6AC1BBA5-107E-4F07-BCF0-DF757735D0A8} 
         Name            =   "TitleLbl"
         Text            =   "Prueba"
         Object.Left            =   0
         Object.Top             =   200
         Width           =   9024
         Height          =   600
         BackColor       =   16777215
         ForeColor       =   128
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Align           =   6
         BackStyle       =   0
         BorderColor     =   0
         BorderStyle     =   0
         CanGrow         =   0   'False
         CanShrink       =   0   'False
         Visible         =   -1  'True
         HideDuplicates  =   0   'False
         RunningSum      =   0
         Format          =   ""
         LineSlant       =   0
         LineWidth       =   0
         PictureAlign    =   0
         MarginLeft      =   0
         MarginTop       =   0
         MarginRight     =   0
         MarginBottom    =   0
         Section         =   1
         ForcePageBreak  =   0
         Calculated      =   0   'False
         WordWrap        =   -1  'True
         LineSpacing     =   0
         CheckBox        =   0
         RTF             =   0   'False
         Anchor          =   0
         ZOrder          =   0
         LinkTarget      =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Field1 {6AC1BBA5-107E-4F07-BCF0-DF757735D0A8} 
         Name            =   "FooterLeft"
         Text            =   "Now()"
         Object.Left            =   0
         Object.Top             =   30
         Width           =   4512
         Height          =   300
         BackColor       =   16777215
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Align           =   0
         BackStyle       =   0
         BorderColor     =   0
         BorderStyle     =   0
         CanGrow         =   0   'False
         CanShrink       =   0   'False
         Visible         =   -1  'True
         HideDuplicates  =   0   'False
         RunningSum      =   0
         Format          =   ""
         LineSlant       =   0
         LineWidth       =   0
         PictureAlign    =   0
         MarginLeft      =   0
         MarginTop       =   0
         MarginRight     =   0
         MarginBottom    =   0
         Section         =   4
         ForcePageBreak  =   0
         Calculated      =   -1  'True
         WordWrap        =   -1  'True
         LineSpacing     =   0
         CheckBox        =   0
         RTF             =   0   'False
         Anchor          =   0
         ZOrder          =   0
         LinkTarget      =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Field2 {6AC1BBA5-107E-4F07-BCF0-DF757735D0A8} 
         Name            =   "FooterRight"
         Text            =   """Page "" & [Page] & "" of "" & [Pages]"
         Object.Left            =   4512
         Object.Top             =   30
         Width           =   4512
         Height          =   300
         BackColor       =   16777215
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Align           =   2
         BackStyle       =   0
         BorderColor     =   0
         BorderStyle     =   0
         CanGrow         =   0   'False
         CanShrink       =   0   'False
         Visible         =   -1  'True
         HideDuplicates  =   0   'False
         RunningSum      =   0
         Format          =   ""
         LineSlant       =   0
         LineWidth       =   0
         PictureAlign    =   0
         MarginLeft      =   0
         MarginTop       =   0
         MarginRight     =   0
         MarginBottom    =   0
         Section         =   4
         ForcePageBreak  =   0
         Calculated      =   -1  'True
         WordWrap        =   -1  'True
         LineSpacing     =   0
         CheckBox        =   0
         RTF             =   0   'False
         Anchor          =   0
         ZOrder          =   0
         LinkTarget      =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Field3 {6AC1BBA5-107E-4F07-BCF0-DF757735D0A8} 
         Name            =   "DivLine1"
         Text            =   ""
         Object.Left            =   0
         Object.Top             =   570
         Width           =   9024
         Height          =   30
         BackColor       =   0
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Align           =   -1
         BackStyle       =   1
         BorderColor     =   0
         BorderStyle     =   0
         CanGrow         =   0   'False
         CanShrink       =   0   'False
         Visible         =   -1  'True
         HideDuplicates  =   0   'False
         RunningSum      =   0
         Format          =   ""
         LineSlant       =   0
         LineWidth       =   0
         PictureAlign    =   0
         MarginLeft      =   0
         MarginTop       =   0
         MarginRight     =   0
         MarginBottom    =   0
         Section         =   3
         ForcePageBreak  =   0
         Calculated      =   0   'False
         WordWrap        =   -1  'True
         LineSpacing     =   0
         CheckBox        =   0
         RTF             =   0   'False
         Anchor          =   0
         ZOrder          =   0
         LinkTarget      =   ""
         Object.Tag             =   ""
      EndProperty
   End
End
Attribute VB_Name = "Frm_Reportes_Activos_Ctas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fApp_Excel            As Excel.Application
Dim fLibro                As Excel.Workbook
Dim fFecha                As Date
Dim fFilaExcel            As Integer
Dim fFormatoMonedaCuenta  As String

Const sFormat0Decimal = "###,##0"
Const sFormat2Decimal = "###,##0.00"
Const sFormat4Decimal = "###,##0.0000"

Private Sub cmb_buscar_Click()
Dim lId_Cuenta As String
  lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text), 0)
  If lId_Cuenta <> 0 Then
     Call Sub_MostrarCuenta(lId_Cuenta, True)
  End If
    
End Sub

Private Sub Form_Load()
  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("PRINTER").Image = cBoton_Imprimir
    .Buttons("REFRESH").Image = cBoton_Refrescar
    .Buttons("EXIT").Image = cBoton_Salir
  End With
  Call Sub_CargaForm
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre

  'Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, "id_cliente")
  
  Set lCierre = New Class_Verificaciones_Cierre
  fFecha = lCierre.Busca_Ultima_FechaCierre
  DTP_Fecha.Value = fFecha
    
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
    Case "REFRESH"
      Call Sub_Limpiar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
    Case "XLS"
      Call Sub_Crea_Excel
  End Select

End Sub

Private Sub Sub_Limpiar()
  Cmb_Cuentas.Text = ""
  DTP_Fecha.Value = fFecha
  Txt_Num_Cuenta.Text = ""
  Txt_Num_Cuenta.Tag = ""
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
  
  If Trim(Txt_Num_Cuenta.Tag) = "" Or Trim(Txt_Num_Cuenta.Text) = "" Then
    MsgBox "Debe seleccionar una Cuenta v�lida.", vbInformation, Me.Caption
    GoTo ExitProcedure
  End If
  
  Call Sub_Generar_Reporte(pTipoSalida)
  
ExitProcedure:

End Sub

Private Sub Sub_Generar_Reporte(pTipoSalida As ePrinter)
Const clrHeader = &HD0D0D0
'------------------------------------------
Const sHeader_Cta = "Cuenta|Descripci�n|Rut|Cliente|Asesor|Patrimonio|Moneda|Rentabilidad (%)"
Const sFormat_Cta = "900|1700|1200|3500|2500|>1900|900|>1300"
Const sHeader_Caja = "Caja|Mercado|Moneda|Monto Moneda Caja|Monto Moneda Cuenta"
Const sFormat_Caja = "3000|1000|1000|>2500|>2500"
Const sHeader_SA = "Nemot�cnico|Cantidad|Precio Mercado|Precio Promedio Compra|Moneda|Duraci�n|Valor Mercado|Valor Mercado Cuenta"
Const sFormat_SA = "2800|>1600|>1400|>1400|>1400|>1400|>2000|>2000"
Const sHeader_Total = "Total Activos de la Cuenta|  | |Valor Mercado Cuenta"
Const sFormat_Total = "3500|>2000|6000|>2500"

'------------------------------------------
Dim sRecord
Dim bAppend
Dim lLinea As Integer
'------------------------------------------
Dim lcCuenta As Object
Dim lReg_Cta As hFields
Dim lNum_Cuenta As String
Dim lDsc_Cuenta As String
Dim lNom_Cliente As String
Dim lDsc_Moneda As String
Dim lId_Moneda_Cta As String
'------------------------------------------
Dim lPatrimonio As Class_Patrimonio_Cuentas
Dim lReg_Pat As hFields
Dim lPatrim As String
Dim lRentabilidad As Double
'------------------------------------------
Dim lCajas_Ctas As Class_Cajas_Cuenta
Dim lCursor_Caj As hRecord
Dim lReg_Caj As hFields
Dim lMonto_Mon_Caja As String
'------------------------------------------
Dim lSaldos_Activos As Class_Saldo_Activos
Dim lCursor_SA As hRecord
Dim lReg_SA As hFields
'------------------------------------------
Dim lId_Cuenta As String
Dim lParidad As Double
Dim lOperacion As String
Dim lMonto_Mon_Cta As String
'------------------------------------------
Dim lcTipo_Cambio As Object
'------------------------------------------
Dim lInstrumento As Class_Instrumentos
Dim lCursor_Ins As hRecord
Dim lReg_Ins As hFields
Dim lHay_Activos As Boolean
'------------------------------------------
Dim lcMoneda As Object
'------------------------------------------
Dim lSaldos_Caja As Class_Saldos_Caja
'------------------------------------------
Dim lForm As Frm_Reporte_Generico
Dim TCantidad As Double
Dim TValor As Double
Dim TValorTotal As Double
Dim TCantidadTotal As Double
Dim lDecimales As Integer
Dim FormatoMonedaCuenta As String
Dim Formato As String

'------------------------------------------
Dim strRut As String
Dim strAsesor As String

  Call Sub_Bloquea_Puntero(Me)
  
  'lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
  lId_Cuenta = Txt_Num_Cuenta.Tag
  
  Rem Busca los datos de la cuenta
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    If .Buscar Then
      For Each lReg_Cta In .Cursor
        lNum_Cuenta = lReg_Cta("Num_Cuenta").Value
        lDsc_Cuenta = lReg_Cta("Dsc_Cuenta").Value
        lNom_Cliente = lReg_Cta("nombre_cliente").Value
        'lDsc_Moneda = lReg_Cta("dsc_moneda").Value
        lId_Moneda_Cta = lReg_Cta("id_moneda").Value
        lDecimales = Decimales_Moneda(lReg_Cta("id_moneda").Value)
        
        strRut = "" & lReg_Cta("Rut_cliente").Value
        strAsesor = "" & lReg_Cta("DSC_ASESOR").Value
        
          Select Case lDecimales
            Case 0
                    FormatoMonedaCuenta = sFormat0Decimal
            Case 2
                    FormatoMonedaCuenta = sFormat2Decimal
            Case 4
                    FormatoMonedaCuenta = sFormat4Decimal
          End Select
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  Rem Busca el nombre de la moneda
'  Set lcMonedas = New Class_Monedas
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    .Campo("id_Moneda").Valor = lId_Moneda_Cta
    If .Buscar Then
      lDsc_Moneda = .Cursor(1)("dsc_moneda").Value
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      Set lcMoneda = Nothing
      GoTo ErrProcedure
    End If
  End With
  Set lcMoneda = Nothing
        
  Rem Busca el patrimonio de la cuenta y su rentabilidad
  Set lPatrimonio = New Class_Patrimonio_Cuentas
  With lPatrimonio
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("FECHA_CIERRE").Valor = DTP_Fecha.Value
    'Valores por defecto
    .Campo("RENTABILIDAD_MON_CUENTA").Valor = Null
    .Campo("APORTE_RETIROS_MON_CUENTA").Valor = Null
    If .Buscar Then
      For Each lReg_Pat In .Cursor
        lPatrim = Format(lReg_Pat("PATRIMONIO_MON_CUENTA").Value, FormatoMonedaCuenta)
        lRentabilidad = lReg_Pat("RENTABILIDAD_MON_CUENTA").Value
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  Rem Busca las cajas de la cuenta
  Set lCajas_Ctas = New Class_Cajas_Cuenta
  With lCajas_Ctas
    .Campo("id_cuenta").Valor = lId_Cuenta
    If .Buscar(True) Then
      Set lCursor_Caj = .Cursor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  Rem Busca los Instrumentos del sistema
  Set lInstrumento = New Class_Instrumentos
  With lInstrumento
    If .Buscar Then
      Set lCursor_Ins = .Cursor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  Rem Comienzo de la generaci�n del reporte
  Set lForm = New Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="Reporte Activos de la Cuenta '" & lNum_Cuenta & "'" _
                               , pTipoSalida:=pTipoSalida _
                               , pOrientacion:=orLandscape)
     
  With lForm.VsPrinter
    .FontSize = 9
    .Paragraph = "Fecha Consulta: " & DTP_Fecha.Value
    
    .FontSize = 11
    .FontBold = True
    .Paragraph = "" 'salto de linea
    
    Rem Datos de la cuenta
    '.FontUnderline = True ' subrayado
    .Paragraph = "Datos de la Cuenta"
    '.FontUnderline = False
    
    .FontBold = False
    .FontSize = 8
    
    .StartTable
    .TableCell(tcAlignCurrency) = False
    
    
    
    sRecord = lNum_Cuenta & "|" & lDsc_Cuenta & "|" & strRut & "|" & lNom_Cliente & "|" & strAsesor & "|" & IIf(lPatrim = "", 0, lPatrim) & "|" & lDsc_Moneda & "|" & lRentabilidad
    .AddTable sFormat_Cta, sHeader_Cta, sRecord, clrHeader, , bAppend
    .TableCell(tcFontBold, 0) = True
    .EndTable
    
    Rem Datos de la Caja
    .Paragraph = "" 'salto de linea
    .FontSize = 11
    .FontBold = True
    .Paragraph = "Datos de la(s) Caja(s) de la Cuenta"
    .FontBold = False
    .FontSize = 9
    .StartTable
    .TableCell(tcAlignCurrency) = False
    For Each lReg_Caj In lCursor_Caj
      lMonto_Mon_Caja = 0
      
      Set lSaldos_Caja = New Class_Saldos_Caja
      With lSaldos_Caja
        .Campo("fecha_cierre").Valor = DTP_Fecha.Value
        .Campo("id_caja_cuenta").Valor = lReg_Caj("id_caja_cuenta").Value
        If .Buscar Then
          If .Cursor.Count > 0 Then
            lMonto_Mon_Caja = .Cursor(1)("monto_mon_caja").Value
          End If
        End If
      End With
      Set lSaldos_Caja = Nothing
      
      'lCajas_Ctas.campo("id_caja_cuenta").Valor = lReg_Caj("id_caja_cuenta").Value
      'lMonto_Mon_Caja = Format(CStr(lCajas_Ctas.Saldo_Disponible(DTP_Fecha.Value)), "###,##0.0000")
      
'      Set lcTipo_Cambio = New Class_Tipo_Cambios
      Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
      If lcTipo_Cambio.Busca_Precio_Paridad(lId_Cuenta _
                                       , lReg_Caj("Id_Moneda").Value _
                                       , lId_Moneda_Cta _
                                       , DTP_Fecha.Value _
                                       , lParidad _
                                       , lOperacion) Then
        If lOperacion = "M" Then
          Rem Multiplicacion
          lMonto_Mon_Cta = lMonto_Mon_Caja * lParidad
        Else
          Rem Division
          lMonto_Mon_Cta = Fnt_Divide(lMonto_Mon_Caja, lParidad)
        End If
      Else
        MsgBox lcTipo_Cambio.ErrMsg, vbCritical, Me.Caption
      End If
       lDecimales = Decimales_Moneda(lReg_Caj("Id_Moneda").Value)
          Select Case lDecimales
            Case 0
                    Formato = sFormat0Decimal
            Case 2
                    Formato = sFormat2Decimal
            Case 4
                    Formato = sFormat4Decimal
          End Select
      sRecord = lReg_Caj("Dsc_Caja_Cuenta").Value & "|" & _
                lReg_Caj("Desc_Mercado").Value & "|" & _
                lReg_Caj("Dsc_Moneda").Value & "|" & _
                Format(lMonto_Mon_Caja, Formato) & "|" & _
                Format(lMonto_Mon_Cta, FormatoMonedaCuenta)
      .AddTable sFormat_Caja, sHeader_Caja, sRecord, clrHeader, , bAppend
    Next
    .TableCell(tcFontBold, 0) = True
    .EndTable
    
    Rem Datos de los activos de la cuenta
    .Paragraph = "" 'salto de linea
    .FontSize = 11
    .FontBold = True
    .Paragraph = "Datos de Activos de la Cuenta"
    .FontBold = False
    .FontSize = 9
    
    Rem Busca los Activos en cartera de la cuenta
    Rem Por cada instrumento del sistema
    lHay_Activos = False
    TValorTotal = 0
    TCantidadTotal = 0
    For Each lReg_Ins In lCursor_Ins
      Rem Se buscan los activos en cartera para cada instrumento
      Set lSaldos_Activos = New Class_Saldo_Activos
      With lSaldos_Activos
        If .Buscar_al_cierre(DTP_Fecha.Value, pId_Cuenta:=lId_Cuenta, pCod_Instrumento:=lReg_Ins("cod_instrumento").Value) Then
          Set lCursor_SA = .Cursor
        Else
          MsgBox .ErrMsg, vbCritical, Me.Caption
          GoTo ErrProcedure
        End If
      End With
      
      Rem Si hay activos para el instrumento consultado procede a la generacion de la tabla
      If lCursor_SA.Count > 0 Then
        lHay_Activos = True
        .FontSize = 10
        .FontBold = True
        .Paragraph = lReg_Ins("Dsc_Intrumento").Value
        .FontSize = 9
        .FontBold = False
        .StartTable
        .TableCell(tcAlignCurrency) = False
        Rem Se agrupan los activos por instrumentos
        TCantidad = 0
        TValor = 0
        For Each lReg_SA In lCursor_SA
          TCantidad = TCantidad + lReg_SA("cantidad").Value
          TValor = TValor + lReg_SA("monto_mon_cta").Value
          lDecimales = Decimales_Moneda(lReg_SA("id_moneda_nemotecnico").Value)
          Select Case lDecimales
            Case 0
                    Formato = sFormat0Decimal
            Case 2
                    Formato = sFormat2Decimal
            Case 4
                    Formato = sFormat4Decimal
          End Select
          If lReg_Ins("cod_instrumento").Value = "DEPOSITO_NAC" Or lReg_Ins("cod_instrumento").Value = "BONOS_NAC" Or lReg_Ins("cod_instrumento").Value = "PACTOS_NAC" Then
                sRecord = lReg_SA("nemotecnico").Value & "|" & _
                    Format(lReg_SA("cantidad").Value, "###,##0.00") & "|" & _
                    NVL(lReg_SA("tasa").Value, 0) & "|" & NVL(lReg_SA("tasa_compra").Value, 0) & "|" & _
                    Moneda(lReg_SA("id_moneda_nemotecnico").Value) & "|" & _
                    IIf(lReg_Ins("cod_instrumento").Value <> "PACTOS_NAC", Format(Duracion(DTP_Fecha.Value, lReg_SA("id_nemotecnico").Value), sFormat4Decimal), "") & "|" & _
                    Simbolo_Moneda(lReg_SA("id_moneda_nemotecnico").Value) & " " & Format(lReg_SA("monto_mon_nemotecnico").Value, Formato) & "|" & _
                    Format(lReg_SA("monto_mon_cta").Value, FormatoMonedaCuenta)
          Else
            sRecord = lReg_SA("nemotecnico").Value & "|" & _
                    Format(lReg_SA("cantidad").Value, "###,##0.00") & "|" & _
                    NVL(lReg_SA("precio").Value, 0) & "|" & NVL(lReg_SA("precio_compra").Value, 0) & "|" & _
                    Moneda(lReg_SA("id_moneda_nemotecnico").Value) & "|" & _
                    "" & "|" & _
                    Simbolo_Moneda(lReg_SA("id_moneda_nemotecnico").Value) & " " & Format(lReg_SA("monto_mon_nemotecnico").Value, Formato) & "|" & _
                    Format(lReg_SA("monto_mon_cta").Value, FormatoMonedaCuenta)
          End If
          
          .AddTable sFormat_SA, sHeader_SA, sRecord, clrHeader, , bAppend
          
        Next
        TValorTotal = TValorTotal + TValor
        TCantidadTotal = TCantidadTotal + TCantidad
         sRecord = " " & "|" & _
                   " " & "|" & _
                   " " & "|" & _
                   " " & "|" & _
                   " " & "|" & _
                   " " & "|" & _
                   " " & "|" & _
                   Format(TValor, Formato)
         .AddTable sFormat_SA, sHeader_SA, sRecord, clrHeader, , bAppend
         .TableCell(tcBackColor, .TableCell(tcRows)) = clrHeader
        .TableCell(tcFontBold, 0) = True
        .EndTable
        .Paragraph = ""
      End If
    Next
    If lHay_Activos Then
        .StartTable
        .TableCell(tcAlignCurrency) = False
        
        sRecord = " " & "|" & _
                  " " & "|" & _
                  " " & "|" & _
                  Format(TValorTotal, Formato)
        .AddTable sFormat_Total, sHeader_Total, sRecord, clrHeader, , bAppend
        .TableCell(tcBackColor, .TableCell(tcRows)) = clrHeader
        .TableCell(tcFontBold, 0) = True
        .EndTable
    End If
    
    If Not lHay_Activos Then
      .Paragraph = "No hay activos para la cuenta"
    End If
    
    .EndDoc
  End With
  
  If pTipoSalida = ePrinter.eP_Pantalla Then Call ColocarEn(lForm, Me, eFP_Abajo)
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Duracion(lfecha As Date, lId_Nemo As Integer) As String
Dim Cursor As hRecord
Dim reg As hFields
Dim Fech As Date
Duracion = "0"

Fech = lfecha
'Cambia Dias Sabados y Domingos al Ultimo Viernes
    If DTP_Fecha.DayOfWeek = 7 Then
        Fech = DateAdd("D", -1, lfecha)
    End If
    
    If DTP_Fecha.DayOfWeek = 1 Then
        Fech = DateAdd("D", -2, lfecha)
    End If
'----------------------------------------------------------------------------------------------
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Fecha", ePT_Fecha, Fech, ePD_Entrada
        gDB.Parametros.Add "IdNemo", ePT_Numero, lId_Nemo, ePD_Entrada
        gDB.Procedimiento = "PKG_DURACION$BUSCAR"

        If Not gDB.EjecutaSP Then
            'GoTo Fin
        End If

        Set Cursor = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each reg In Cursor
            'Fecha_cierre = Reg("fecha_cierre").value
            Duracion = NVL(reg("DURACION").Value, 0)
        Next


End Function

Private Function Moneda(lId_Moneda As Integer) As String
Dim Cursor As hRecord
Dim reg As hFields
Moneda = "?"
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "pid_moneda", ePT_Numero, lId_Moneda, ePD_Entrada
        gDB.Procedimiento = "PKG_MONEDAS$Buscar"

        If Not gDB.EjecutaSP Then
            'GoTo Fin
        End If

        Set Cursor = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each reg In Cursor
            'Fecha_cierre = Reg("fecha_cierre").value
            Moneda = reg("dsc_moneda").Value
        Next


End Function

Private Function Simbolo_Moneda(lId_Moneda As Integer) As String
Dim Cursor As hRecord
Dim reg As hFields
Simbolo_Moneda = "?"

        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "pid_moneda", ePT_Numero, lId_Moneda, ePD_Entrada
        gDB.Procedimiento = "PKG_MONEDAS$Buscar"

        If Not gDB.EjecutaSP Then
            'GoTo Fin
        End If

        Set Cursor = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each reg In Cursor
            
            Simbolo_Moneda = NVL(reg("simbolo").Value, reg("cod_moneda").Value)
        Next


End Function

Private Function Decimales_Moneda(lId_Moneda As Integer) As Integer
Dim Cursor As hRecord
Dim reg As hFields
Decimales_Moneda = 2

        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "pid_moneda", ePT_Numero, lId_Moneda, ePD_Entrada
        gDB.Procedimiento = "PKG_MONEDAS$Buscar"

        If Not gDB.EjecutaSP Then
            'GoTo Fin
        End If

        Set Cursor = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each reg In Cursor
            
            Decimales_Moneda = reg("dicimales_mostrar").Value
        Next


End Function

Private Sub Sub_Crea_Excel()
Dim nHojas, i       As Integer
Dim hoja            As Integer
Dim Index           As Integer

  Call Sub_Bloquea_Puntero(Me)
  
  If Trim(Txt_Num_Cuenta.Tag) = "" Or Trim(Txt_Num_Cuenta.Text) = "" Then
  'If Fnt_FindValue4Display(Cmb_Cuentas, Cmb_Cuentas.Text) = "" Then
    MsgBox "Debe seleccionar una Cuenta v�lida.", vbInformation, Me.Caption
    GoTo ErrProcedure
  End If
    
  Set fApp_Excel = CreateObject("Excel.application")
  fApp_Excel.DisplayAlerts = False
  Set fLibro = fApp_Excel.Workbooks.Add
  
  fApp_Excel.ActiveWindow.DisplayGridlines = False

  ' App_Excel.Visible = True
    
  fLibro.Worksheets.Add
   
  With fLibro
    For i = .Worksheets.Count To 2 Step -1
      .Worksheets(i).Delete
    Next i
    .Sheets(1).Select
    .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).Select
        
    .ActiveSheet.Range("E5").Value = "Reporte Activos de la Cuenta " & Cmb_Cuentas.Text
    .ActiveSheet.Range("E5").Font.Bold = True
    .ActiveSheet.Range("E5").Font.Size = 14
    .ActiveSheet.Range("E5").HorizontalAlignment = xlLeft
    .ActiveSheet.Range("E5:F5").MergeCells = True
    
    .ActiveSheet.Range("B7").Value = "Fecha Consulta: " & DTP_Fecha.Value
    
    .ActiveSheet.Range("B6:B7").HorizontalAlignment = xlLeft
    .ActiveSheet.Range("B6:B7").Font.Bold = True
    .ActiveSheet.Range("B6:B7").Font.Size = 11
      
    fFilaExcel = 9
    
    .Worksheets.Item(1).Name = "Activos por Cuenta"
  End With
  
  Call Generar_Listado_Excel
  
  fApp_Excel.ActiveWindow.DisplayGridlines = False
  fLibro.ActiveSheet.Columns("A:A").Select
  fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
  fApp_Excel.Selection.EntireColumn.AutoFit
  
  fLibro.ActiveSheet.Range("A1").Select
  
  fApp_Excel.Visible = True
  fApp_Excel.UserControl = True
    
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Generar_Listado_Excel()
Dim lxHoja              As Worksheet
'------------------------------------
Dim lId_Cuenta As String
Dim lId_Moneda_Cta As String
  
  Set lxHoja = fLibro.ActiveSheet
  
  'lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
  lId_Cuenta = Txt_Num_Cuenta.Tag
  
  If Not Fnt_Datos_Cuenta(lxHoja, lId_Cuenta, lId_Moneda_Cta) Then
    GoTo ErrProcedure
  End If
  
  If Not Fnt_Cajas_Cuenta(lxHoja, lId_Cuenta, lId_Moneda_Cta) Then
    GoTo ErrProcedure
  End If
  
  If Not Fnt_Activos_Cuenta(lxHoja, lId_Cuenta) Then
    GoTo ErrProcedure
  End If

ErrProcedure:
  
End Sub

Private Function Fnt_Datos_Cuenta(pxHoja As Worksheet, pId_Cuenta As String, ByRef pId_Moneda_Cta As String) As Boolean
Dim lcCuenta As Object
Dim lcMoneda As Object
Dim lcPatrimonio As Class_Patrimonio_Cuentas
Dim lReg_Cta As hFields
Dim lReg_Pat As hFields
Dim lNum_Cuenta As String
Dim lDsc_Cuenta As String
Dim lNom_Cliente As String
Dim lDsc_Asesor As String
Dim lDecimales As Integer
Dim lrut As String
Dim lDsc_Moneda As String
Dim lPatrim As Double
Dim lRentabilidad As Double
  
  Fnt_Datos_Cuenta = True
  
  Rem Busca los datos de la cuenta
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cuenta").Valor = pId_Cuenta
    If .Buscar Then
      For Each lReg_Cta In .Cursor
        lNum_Cuenta = lReg_Cta("Num_Cuenta").Value
        lDsc_Cuenta = lReg_Cta("Dsc_Cuenta").Value
        lNom_Cliente = lReg_Cta("nombre_cliente").Value
        lDsc_Asesor = lReg_Cta("DSC_ASESOR").Value
        pId_Moneda_Cta = lReg_Cta("id_moneda").Value
        lDecimales = Decimales_Moneda(lReg_Cta("id_moneda").Value)
        lrut = "" & lReg_Cta("Rut_cliente").Value
        
        Select Case lDecimales
          Case 0
            fFormatoMonedaCuenta = sFormat0Decimal
          Case 2
            fFormatoMonedaCuenta = sFormat2Decimal
          Case 4
            fFormatoMonedaCuenta = sFormat4Decimal
        End Select
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  Rem Busca el nombre de la moneda
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    .Campo("id_Moneda").Valor = pId_Moneda_Cta
    If .Buscar Then
      lDsc_Moneda = .Cursor(1)("dsc_moneda").Value
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      Set lcMoneda = Nothing
      GoTo ErrProcedure
    End If
  End With
  Set lcMoneda = Nothing
        
  Rem Busca el patrimonio de la cuenta y su rentabilidad
  Set lcPatrimonio = New Class_Patrimonio_Cuentas
  With lcPatrimonio
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("FECHA_CIERRE").Valor = DTP_Fecha.Value
    'Valores por defecto
    .Campo("RENTABILIDAD_MON_CUENTA").Valor = Null
    .Campo("APORTE_RETIROS_MON_CUENTA").Valor = Null
    If .Buscar Then
      For Each lReg_Pat In .Cursor
        lPatrim = Format(lReg_Pat("PATRIMONIO_MON_CUENTA").Value, fFormatoMonedaCuenta)
        lRentabilidad = lReg_Pat("RENTABILIDAD_MON_CUENTA").Value
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  With pxHoja
    .Cells(fFilaExcel, 2).Value = "Datos de la Cuenta"
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Font.Bold = True
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Font.Size = 11
    
    fFilaExcel = fFilaExcel + 1
    
    .Cells(fFilaExcel, 2).Value = "Cuenta"
    .Cells(fFilaExcel, 3).Value = "Descripci�n"
    .Cells(fFilaExcel, 4).Value = "Rut"
    .Cells(fFilaExcel, 5).Value = "Cliente"
    .Cells(fFilaExcel, 6).Value = "Asesor"
    .Cells(fFilaExcel, 7).Value = "Patrimonio"
    .Cells(fFilaExcel, 8).Value = "Moneda"
    .Cells(fFilaExcel, 9).Value = "Rentabilidad"
    
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).BorderAround
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Interior.Color = RGB(255, 255, 0)
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Font.Bold = True
    '.Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).WrapText = True
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).HorizontalAlignment = xlLeft
    
    fFilaExcel = fFilaExcel + 1
    
    '.Cells(fFilaExcel, 2).NumberFormat = "#"
    .Cells(fFilaExcel, 2).Value = lNum_Cuenta
    .Cells(fFilaExcel, 3).Value = lDsc_Cuenta
    .Cells(fFilaExcel, 4).Value = lrut
    .Cells(fFilaExcel, 5).Value = lNom_Cliente
    .Cells(fFilaExcel, 6).Value = lDsc_Asesor
    .Cells(fFilaExcel, 7).Value = lPatrim
    .Cells(fFilaExcel, 7).NumberFormat = fFormatoMonedaCuenta
    .Cells(fFilaExcel, 8).Value = lDsc_Moneda
    .Cells(fFilaExcel, 9).Value = lRentabilidad
    
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).BorderAround
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Borders.Color = RGB(0, 0, 0)
    
  End With
  
  Exit Function
  
ErrProcedure:
  Fnt_Datos_Cuenta = False
  
End Function

Private Function Fnt_Cajas_Cuenta(pxHoja As Worksheet, pId_Cuenta As String, pId_Moneda_Cta As String) As Boolean
Dim lcCajas_Ctas As Class_Cajas_Cuenta
Dim lCursor_Caj As hRecord
Dim lReg_Caj As hFields
Dim lMonto_Mon_Caja As Double
Dim lcSaldos_Caja As Class_Saldos_Caja
Dim lcTipo_Cambio As Object
Dim lParidad As Double
Dim lOperacion As String
Dim lMonto_Mon_Cta As Double
Dim lDecimales As Integer
Dim lFormato As String

  Fnt_Cajas_Cuenta = True
  
  With pxHoja
    fFilaExcel = fFilaExcel + 2
    
    .Cells(fFilaExcel, 2).Value = "Datos de la(s) Caja(s) de la Cuenta"
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 6)).Font.Bold = True
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 6)).Font.Size = 11
    
    fFilaExcel = fFilaExcel + 1
    
    .Cells(fFilaExcel, 2).Value = "Caja"
    .Cells(fFilaExcel, 3).Value = "Mercado"
    .Cells(fFilaExcel, 4).Value = "Moneda"
    .Cells(fFilaExcel, 5).Value = "Monto Moneda Caja"
    .Cells(fFilaExcel, 6).Value = "Monto Moneda Cuenta"
    
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 6)).BorderAround
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 6)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 6)).Interior.Color = RGB(255, 255, 0)
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 6)).Font.Bold = True
    '.Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 6)).WrapText = True
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 6)).HorizontalAlignment = xlLeft
  End With
  
  Rem Busca las cajas de la cuenta
  Set lcCajas_Ctas = New Class_Cajas_Cuenta
  With lcCajas_Ctas
    .Campo("id_cuenta").Valor = pId_Cuenta
    If .Buscar(True) Then
      Set lCursor_Caj = .Cursor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  If lCursor_Caj.Count > 0 Then
    For Each lReg_Caj In lCursor_Caj
      lMonto_Mon_Caja = 0
  
      Set lcSaldos_Caja = New Class_Saldos_Caja
      With lcSaldos_Caja
        .Campo("fecha_cierre").Valor = DTP_Fecha.Value
        .Campo("id_caja_cuenta").Valor = lReg_Caj("id_caja_cuenta").Value
        If .Buscar Then
          If .Cursor.Count > 0 Then
            lMonto_Mon_Caja = .Cursor(1)("monto_mon_caja").Value
          End If
        Else
          MsgBox .ErrMsg, vbCritical, Me.Caption
          GoTo ErrProcedure
        End If
      End With
      Set lcSaldos_Caja = Nothing
  
      Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
      If lcTipo_Cambio.Busca_Precio_Paridad(pId_Cuenta _
                                          , lReg_Caj("Id_Moneda").Value _
                                          , pId_Moneda_Cta _
                                          , DTP_Fecha.Value _
                                          , lParidad _
                                          , lOperacion) Then
        lMonto_Mon_Cta = Fnt_Calcula_TipoCambio_Ope(lMonto_Mon_Caja, lParidad, lOperacion)
      Else
        MsgBox lcTipo_Cambio.ErrMsg, vbCritical, Me.Caption
        GoTo ErrProcedure
      End If
        
      lDecimales = Decimales_Moneda(lReg_Caj("Id_Moneda").Value)
      Select Case lDecimales
        Case 0
          lFormato = sFormat0Decimal
        Case 2
          lFormato = sFormat2Decimal
        Case 4
          lFormato = sFormat4Decimal
      End Select
      
      With pxHoja
        fFilaExcel = fFilaExcel + 1
        .Cells(fFilaExcel, 2).Value = lReg_Caj("Dsc_Caja_Cuenta").Value
        .Cells(fFilaExcel, 3).Value = lReg_Caj("Desc_Mercado").Value
        .Cells(fFilaExcel, 4).Value = lReg_Caj("Dsc_Moneda").Value
        .Cells(fFilaExcel, 5).Value = lMonto_Mon_Caja
        .Cells(fFilaExcel, 5).NumberFormat = lFormato
        .Cells(fFilaExcel, 6).Value = lMonto_Mon_Cta
        .Cells(fFilaExcel, 6).NumberFormat = fFormatoMonedaCuenta
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 6)).BorderAround
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 6)).Borders.Color = RGB(0, 0, 0)
      End With
        
    Next
  Else
    With pxHoja
      fFilaExcel = fFilaExcel + 1
      .Cells(fFilaExcel, 2).Value = "Cuenta sin Cajas"
    End With
  End If
  
  Exit Function
  
ErrProcedure:
  Set lcCajas_Ctas = Nothing

  Fnt_Cajas_Cuenta = False
  
End Function

Private Function Fnt_Activos_Cuenta(pxHoja As Worksheet, pId_Cuenta As String) As Boolean
Dim lcInstrumento As Class_Instrumentos
Dim lCursor_Ins As hRecord
Dim lHay_Activos As Boolean
Dim lValorTotal As Double
Dim lCantidadTotal As Double
Dim lReg_Ins As hFields
Dim lcSaldos_Activos As Class_Saldo_Activos
Dim lCursor_SA  As hRecord
Dim lCantidad As Double
Dim lValor  As Double
Dim lReg_SA As hFields
Dim lDecimales As Integer
Dim lFormato As String

  Fnt_Activos_Cuenta = True
  
  With pxHoja
    fFilaExcel = fFilaExcel + 2
    
    .Cells(fFilaExcel, 2).Value = "Datos de Activos de la Cuenta"
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Font.Bold = True
    .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Font.Size = 11
  End With
  
  Rem Busca los Instrumentos del sistema
  Set lcInstrumento = New Class_Instrumentos
  With lcInstrumento
    If .Buscar Then
      Set lCursor_Ins = .Cursor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  Rem Busca los Activos en cartera de la cuenta
  Rem Por cada instrumento del sistema
  lHay_Activos = False
  lValorTotal = 0
  lCantidadTotal = 0
  For Each lReg_Ins In lCursor_Ins
    Rem Se buscan los activos en cartera para cada instrumento
    Set lcSaldos_Activos = New Class_Saldo_Activos
    With lcSaldos_Activos
      If .Buscar_al_cierre(DTP_Fecha.Value, pId_Cuenta:=pId_Cuenta _
                         , pCod_Instrumento:=lReg_Ins("cod_instrumento").Value) Then
        Set lCursor_SA = .Cursor
      Else
        MsgBox .ErrMsg, vbCritical, Me.Caption
        GoTo ErrProcedure
      End If
    End With
    
    Rem Si hay activos para el instrumento consultado procede a la generacion de la tabla
    If lCursor_SA.Count > 0 Then
      lHay_Activos = True
      
      With pxHoja
        fFilaExcel = fFilaExcel + 1
        
        .Cells(fFilaExcel, 2).Value = lReg_Ins("Dsc_Intrumento").Value
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 10)).Font.Bold = True
        
        fFilaExcel = fFilaExcel + 1
    
        .Cells(fFilaExcel, 2).Value = "Nemot�cnico"
        .Cells(fFilaExcel, 3).Value = "Cantidad"
        .Cells(fFilaExcel, 4).Value = "Precio Mercado"
        .Cells(fFilaExcel, 5).Value = "Precio Promedio Compra"
        .Cells(fFilaExcel, 6).Value = "Duraci�n"
        .Cells(fFilaExcel, 7).Value = "Moneda"
        .Cells(fFilaExcel, 8).Value = "Valor Mercado"
        .Cells(fFilaExcel, 9).Value = "Valor Mercado Cuenta"
        
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).BorderAround
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Interior.Color = RGB(255, 255, 0)
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Font.Bold = True
        '.Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).WrapText = True
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).HorizontalAlignment = xlLeft
      End With
      
      Rem Se agrupan los activos por instrumentos
      lCantidad = 0
      lValor = 0
      For Each lReg_SA In lCursor_SA
        lCantidad = lCantidad + lReg_SA("cantidad").Value
        lValor = lValor + lReg_SA("monto_mon_cta").Value
        lDecimales = Decimales_Moneda(lReg_SA("id_moneda_nemotecnico").Value)
        Select Case lDecimales
          Case 0
            lFormato = sFormat0Decimal
          Case 2
            lFormato = sFormat2Decimal
          Case 4
            lFormato = sFormat4Decimal
        End Select
                
        With pxHoja
          fFilaExcel = fFilaExcel + 1
      
            .Cells(fFilaExcel, 2).Value = lReg_SA("nemotecnico").Value
            .Cells(fFilaExcel, 3).Value = lReg_SA("cantidad").Value
            .Cells(fFilaExcel, 3).NumberFormat = sFormat2Decimal
            If lReg_Ins("cod_instrumento").Value = "DEPOSITO_NAC" Or lReg_Ins("cod_instrumento").Value = "BONOS_NAC" Or lReg_Ins("cod_instrumento").Value = "PACTOS_NAC" Then
                .Cells(fFilaExcel, 4).Value = NVL(lReg_SA("tasa").Value, 0)
                .Cells(fFilaExcel, 5).Value = NVL(lReg_SA("tasa_compra").Value, 0) 'Nuevo
            Else
                .Cells(fFilaExcel, 4).Value = NVL(lReg_SA("precio").Value, 0)
                .Cells(fFilaExcel, 5).Value = NVL(lReg_SA("precio_compra").Value, 0) 'Nuevo
            End If
            .Cells(fFilaExcel, 4).NumberFormat = sFormat2Decimal
            .Cells(fFilaExcel, 5).NumberFormat = sFormat2Decimal
            
            If lReg_Ins("cod_instrumento").Value = "DEPOSITO_NAC" Or lReg_Ins("cod_instrumento").Value = "BONOS_NAC" Then
                .Cells(fFilaExcel, 6).Value = Replace(Duracion(DTP_Fecha.Value, lReg_SA("id_nemotecnico").Value), ",", ".")
            Else
                .Cells(fFilaExcel, 6).Value = ""
            End If
            .Cells(fFilaExcel, 6).NumberFormat = sFormat4Decimal
            
            .Cells(fFilaExcel, 7).Value = Moneda(lReg_SA("id_moneda_nemotecnico").Value)
            .Cells(fFilaExcel, 8).Value = Simbolo_Moneda(lReg_SA("id_moneda_nemotecnico").Value) & " " & Format(lReg_SA("monto_mon_nemotecnico").Value, lFormato)
            .Cells(fFilaExcel, 8).NumberFormat = lFormato
            .Cells(fFilaExcel, 9).Value = lReg_SA("monto_mon_cta").Value
            .Cells(fFilaExcel, 9).NumberFormat = fFormatoMonedaCuenta
          
          .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).BorderAround
          .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Borders.Color = RGB(0, 0, 0)
          .Range(.Cells(fFilaExcel, 3), .Cells(fFilaExcel, 9)).HorizontalAlignment = xlRight
        End With
      Next
      lValorTotal = lValorTotal + lValor
      lCantidadTotal = lCantidadTotal + lCantidad
      
      With pxHoja
        fFilaExcel = fFilaExcel + 1
    
        .Cells(fFilaExcel, 9).Value = lValor
        .Cells(fFilaExcel, 9).NumberFormat = lFormato
        
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).BorderAround
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Interior.Color = RGB(255, 255, 0)
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Font.Bold = True
        '.Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).WrapText = True
        .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).HorizontalAlignment = xlRight
        
        fFilaExcel = fFilaExcel + 1
      End With
    End If
  Next
  If lHay_Activos Then
    With pxHoja
      fFilaExcel = fFilaExcel + 1
  
      .Cells(fFilaExcel, 2).Value = "Total Activos de la Cuenta"
      .Cells(fFilaExcel, 9).Value = "Valor Mercado Cuenta"
      
      .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).BorderAround
      .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Borders.Color = RGB(0, 0, 0)
      .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Interior.Color = RGB(255, 255, 0)
      .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Font.Bold = True
      '.Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).WrapText = True
      .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).HorizontalAlignment = xlLeft
      
      fFilaExcel = fFilaExcel + 1
      
      .Cells(fFilaExcel, 9).Value = lValorTotal
      .Cells(fFilaExcel, 9).NumberFormat = lFormato
      
      .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).BorderAround
      .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Borders.Color = RGB(0, 0, 0)
      .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Interior.Color = RGB(255, 255, 0)
      .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).Font.Bold = True
      '.Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).WrapText = True
      .Range(.Cells(fFilaExcel, 2), .Cells(fFilaExcel, 9)).HorizontalAlignment = xlRight
    End With
  Else
    With pxHoja
      fFilaExcel = fFilaExcel + 1
      .Cells(fFilaExcel, 2).Value = "Cuenta sin Activos"
    End With
  End If
  
  Exit Function
  
ErrProcedure:
  
  Fnt_Activos_Cuenta = False
End Function

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call cmb_buscar_Cuenta_Click
    End If
End Sub

Private Sub cmb_buscar_Cuenta_Click()
Dim lId_Cuenta    As String
Dim fId_Cuenta    As String
Dim lcCuenta      As Object
'Dim li_idtipocuenta As Integer
'JRE
   ' fli_idtipocuenta = IIf(Fnt_ComboSelected_KEY(Cmb_TiposCuentas) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_TiposCuentas))
   ' fid_asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesor) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Asesor))
    'Fnt_ComboSelected_KEY(Cmb_Asesor)
    
    fId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text), 0)
    If fId_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = fId_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value
                    fId_Cuenta = .Cursor(1)("id_cuenta").Value
                    If Not fId_Cuenta = "" Then
                        Call Sub_MostrarCuenta(fId_Cuenta, True)
                        'Call Sub_DatosCuenta
                    End If
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
            Set lcCuenta = Nothing
    End If
End Sub

Private Sub Sub_MostrarCuenta(pCuenta, Optional pId As Boolean = True)
Dim lCursor As hRecord
Dim lError  As String
Dim lexiste As Boolean

    Call Sub_Entrega_DatosCuenta_2(pCuenta, pId, lCursor, lError, lexiste)
    If lexiste Then
        Txt_Num_Cuenta.Text = lCursor(1)("num_cuenta").Value
        Txt_Num_Cuenta.Tag = lCursor(1)("id_cuenta").Value
        'Txt_Rut.Text = lCursor(1)("rut_cliente").Value
       ' Txt_Nombres.Text = lCursor(1)("nombre_cliente").Value
       ' txtNombreAsesor.Text = lCursor(1)("nombre_asesor").Value
       ' Call Sub_CargarDatos
    End If
  
End Sub

Public Sub Sub_Entrega_DatosCuenta_2(ByVal pCuenta, pId As Boolean, ByRef pCursor As hRecord, ByRef PError, ByRef pExiste)
Dim lcCuenta As Object
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
        If pId Then
            .Campo("id_cuenta").Valor = pCuenta
        Else
            .Campo("num_cuenta").Valor = pCuenta
        End If
        If .Buscar_Vigentes Then
            If .Cursor.Count = 1 Then
                Set pCursor = .Cursor
                pExiste = True
            Else
                pExiste = False
            End If
        Else
            PError = .ErrMsg
        End If
    End With
    Set lcCuenta = Nothing
End Sub




