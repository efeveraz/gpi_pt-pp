VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Consulta_Saldos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Patrimonio y Activos"
   ClientHeight    =   8745
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12990
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8745
   ScaleWidth      =   12990
   Begin TabDlg.SSTab SST_Saldos 
      Height          =   6375
      Left            =   120
      TabIndex        =   3
      Top             =   2280
      Width           =   12765
      _ExtentX        =   22516
      _ExtentY        =   11245
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Detalle"
      TabPicture(0)   =   "Frm_Consulta_Saldos.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Grilla_Detalle"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Resumen"
      TabPicture(1)   =   "Frm_Consulta_Saldos.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Grilla_Resumen"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Detalle 
         Height          =   5835
         Left            =   -74910
         TabIndex        =   18
         Top             =   420
         Width           =   12525
         _cx             =   22093
         _cy             =   10292
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   16777215
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   8454143
         ForeColorSel    =   0
         BackColorBkg    =   16777215
         BackColorAlternate=   16777215
         GridColor       =   16777215
         GridColorFixed  =   16777215
         TreeColor       =   -2147483638
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   0
         FixedRows       =   2
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Consulta_Saldos.frx":0038
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Resumen 
         Height          =   5835
         Left            =   90
         TabIndex        =   19
         Top             =   420
         Width           =   12525
         _cx             =   22093
         _cy             =   10292
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   16777215
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   8454143
         ForeColorSel    =   0
         BackColorBkg    =   16777215
         BackColorAlternate=   16777215
         GridColor       =   16777215
         GridColorFixed  =   16777215
         TreeColor       =   -2147483638
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   0
         FixedRows       =   2
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Consulta_Saldos.frx":004E
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filtro de B�squeda"
      ForeColor       =   &H000000FF&
      Height          =   1695
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   12780
      Begin VB.Frame Frame5 
         Caption         =   "Moneda de Salida"
         ForeColor       =   &H000000FF&
         Height          =   735
         Left            =   9270
         TabIndex        =   16
         Top             =   240
         Width           =   3375
         Begin TrueDBList80.TDBCombo Cmb_Monedas 
            Height          =   345
            Left            =   120
            TabIndex        =   17
            Tag             =   "OBLI=S;CAPTION=Moneda de Salida"
            Top             =   240
            Width           =   3135
            _ExtentX        =   5530
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_Saldos.frx":0064
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
      End
      Begin VB.Frame Frame4 
         Height          =   1335
         Left            =   3315
         TabIndex        =   11
         Top             =   240
         Width           =   5895
         Begin TrueDBList80.TDBCombo Cmb_Asesor 
            Height          =   345
            Left            =   840
            TabIndex        =   12
            Tag             =   "SOLOLECTURA=N"
            Top             =   240
            Width           =   4935
            _ExtentX        =   8705
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_Saldos.frx":010E
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HFFFFFF&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Cuentas 
            Height          =   315
            Left            =   840
            TabIndex        =   13
            Tag             =   "SOLOLECTURA=N"
            Top             =   840
            Width           =   4935
            _ExtentX        =   8705
            _ExtentY        =   556
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   556
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_Saldos.frx":01B8
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label lbl_Asesor 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Asesor"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   15
            Top             =   240
            Width           =   705
         End
         Begin VB.Label Label1 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Cuentas"
            Height          =   315
            Left            =   120
            TabIndex        =   14
            Top             =   840
            Width           =   705
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Fecha"
         ForeColor       =   &H000000FF&
         Height          =   1335
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   3135
         Begin MSComCtl2.DTPicker DTP_Fecha 
            Height          =   345
            Left            =   1290
            TabIndex        =   9
            Top             =   240
            Width           =   1695
            _ExtentX        =   2990
            _ExtentY        =   609
            _Version        =   393216
            CustomFormat    =   "MMMM yyyy"
            Format          =   66912257
            CurrentDate     =   39153
         End
         Begin VB.Label Lbl_Fecha 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Consulta"
            Height          =   345
            Left            =   90
            TabIndex        =   10
            Top             =   240
            Width           =   1215
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Tipo Reporte"
         ForeColor       =   &H000000FF&
         Height          =   615
         Left            =   9270
         TabIndex        =   4
         Top             =   960
         Width           =   3375
         Begin VB.OptionButton rdb_Ambos 
            Caption         =   "Ambos"
            Height          =   255
            Left            =   120
            TabIndex        =   7
            Top             =   240
            Width           =   855
         End
         Begin VB.OptionButton rdb_Resumen 
            Caption         =   "Resumen"
            Height          =   255
            Left            =   1080
            TabIndex        =   6
            Top             =   240
            Width           =   1035
         End
         Begin VB.OptionButton rdb_Detalle 
            Caption         =   "Detalle"
            Height          =   255
            Left            =   2280
            TabIndex        =   5
            Top             =   240
            Width           =   1035
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   12990
      _ExtentX        =   22913
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "SEARCH"
            Description     =   "Carga Aportes y Retiros de Capital"
            Object.ToolTipText     =   "Carga Saldos Activos"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORTE"
            Description     =   "Genera reporte de Aportes y Retiros de Capital"
            Object.ToolTipText     =   "Genera reporte de Saldos Activos"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Re&frescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpiar los Controles"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   6960
         TabIndex        =   1
         Top             =   0
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Consulta_Saldos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso   As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema  As String 'Codigo para el permiso sobre la ventana
'----------------------------------------------------------------------------

Public fApp_Excel As EXCEL.Application      ' Excel application
Public fLibro     As EXCEL.Workbook      ' Excel workbook
Public fForm      As Frm_Reporte_Generico

Dim fCursor_Datos   As hRecord
Dim fTipoConsulta   As String
Dim fFilaExcel      As Long
Dim fDecimales      As Integer
Dim fId_Moneda      As Integer
Dim vMarginLeft     As Variant
Dim fPage           As Long

Const clrHeader = &HD0D0D0
Rem -----------------------------------------------------------------------
Rem 25/06/09 M.Mardones Agrega columna Patrimonio Total Grilla/PDF/EXCEL
Rem 27/07/09 M.Mardones - Cambia t�tulo de Consulta Saldos Activos a
Rem                       Reporte Patrimonio y Activos
Rem                     - Se traslada la columna Total Patrimonio
Rem                       al final en Grilla/PDF/EXCEL
Rem -----------------------------------------------------------------------

Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Form_Load()

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEARCH").Image = cBoton_Buscar
    .Buttons("REPORTE").Image = cBoton_Modificar
    .Buttons("REFRESH").Image = cBoton_Original
    .Buttons("EXIT").Image = cBoton_Salir
  End With

  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
   
End Sub

Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre

  Call Sub_Bloquea_Puntero(Me)
  Call Sub_FormControl_Color(Me.Controls)
  
  Call Sub_CargaCombo_Asesor(Cmb_Asesor, pTodos:=True)
  Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
  
  Call Sub_CargaCombo_Monedas(Cmb_Monedas)
  Call Sub_ComboSelectedItem(Cmb_Monedas, 1)
  
  Cmb_Cuentas.Text = "Todos"
  
  Call Sub_SeteaGrilla
  
  rdb_Ambos.Value = True
    
  Set lCierre = New Class_Verificaciones_Cierre
  DTP_Fecha.Value = lCierre.Busca_Ultima_FechaCierre
  Set lCierre = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_SeteaGrilla()
Dim lcACI As Class_Arbol_Clase_Instrumento
Dim lcursor As hRecord
Dim lReg As hFields
Dim lColumna As Integer
Dim lCol As Integer
  
  With Grilla_Detalle
    .FixedCols = 0
    .ExtendLastCol = False
    .Editable = flexEDNone
    .BackColorAlternate = vbWhite
    Rem Contorno
    .OutlineCol = 0
    .OutlineBar = flexOutlineBarSimpleLeaf
    .MergeCells = flexMergeFixedOnly

    Rem Otras
    .AllowUserResizing = flexResizeColumns
    .AllowSelection = False
    .GridLines = flexGridNone
  End With
  
  With Grilla_Resumen
    .FixedCols = 0
    .ExtendLastCol = False
    .Editable = flexEDNone
    .BackColorAlternate = vbWhite
    Rem Contorno
    .OutlineCol = 0
    .OutlineBar = flexOutlineBarSimpleLeaf
    .MergeCells = flexMergeFixedOnly

    Rem Otras
    .AllowUserResizing = flexResizeColumns
    .AllowSelection = False
    .GridLines = flexGridNone
  End With
  
  Set lcACI = New Class_Arbol_Clase_Instrumento
  With lcACI
    .Campo("ID_EMPRESA").Valor = gId_Empresa
    If .Buscar_Consulta Then
      Set lcursor = .Cursor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
    
  With Grilla_Detalle
    .Clear
    .Cols = 2
    .Cell(flexcpText, 0, 0) = " "
    .Cell(flexcpText, 1, 0) = "Cuenta"
    .Cell(flexcpText, 0, 1) = " "
    .Cell(flexcpText, 1, 1) = "Nombre Corto"
    
    lColumna = 2
    For Each lReg In lcursor
      lColumna = lColumna + 1
      .Cols = lColumna
      .Cell(flexcpText, 0, lColumna - 1) = lReg("DSC_PADRE_ARBOL_CLASE_INST").Value
      .Cell(flexcpText, 1, lColumna - 1) = lReg("DSC_ARBOL_CLASE_INST").Value
      .ColKey(lColumna - 1) = lReg("ID_ARBOL_CLASE_INST").Value
    Next
    lColumna = lColumna + 1
    Rem Columna Totales
    .Cols = lColumna
    .Cell(flexcpText, 0, lColumna - 1) = " "
    .Cell(flexcpText, 1, lColumna - 1) = "Total Activos"  '"Total Cliente"   'Modificado 27/07/2009 MMardones
    lColumna = lColumna + 1
    Rem Columna "Promedio"
    .Cols = lColumna
    .Cell(flexcpText, 0, lColumna - 1) = " "
    .Cell(flexcpText, 1, lColumna - 1) = "Total Promedio Mensual Activos" '"Promedio Mensual" 'Modificado 27/07/2009 MMardones
    Rem Columna Total Patrimonio   'Agregado 25/06/09.
    lColumna = lColumna + 1
    .Cols = lColumna
    .Cell(flexcpText, 0, lColumna - 1) = " "
    .Cell(flexcpText, 1, lColumna - 1) = "Total Patrimonio"
        
    .MergeRow(0) = True
    .ColAlignment(0) = flexAlignRightCenter
    For lCol = 2 To .Cols - 1
      .ColAlignment(lCol) = flexAlignCenterCenter
    Next
    .AutoSize 0, .Cols - 1
  End With
  
  With Grilla_Resumen
    .Clear
    .Cols = 2
    .Cell(flexcpText, 0, 0) = " "
    .Cell(flexcpText, 1, 0) = "Asesor"
    
    lColumna = 1
    For Each lReg In lcursor
      lColumna = lColumna + 1
      .Cols = lColumna
      .Cell(flexcpText, 0, lColumna - 1) = lReg("DSC_PADRE_ARBOL_CLASE_INST").Value
      .Cell(flexcpText, 1, lColumna - 1) = lReg("DSC_ARBOL_CLASE_INST").Value
      .ColKey(lColumna - 1) = lReg("ID_ARBOL_CLASE_INST").Value
    Next
    
    lColumna = lColumna + 1
    Rem Columna Totales
    .Cols = lColumna
    .Cell(flexcpText, 0, lColumna - 1) = " "
    .Cell(flexcpText, 1, lColumna - 1) = "Total Activos" '"Total Asesor"  'Modificado 27/07/2009 MMardones
    lColumna = lColumna + 1
    Rem Columna "Promedio"
    .Cols = lColumna
    .Cell(flexcpText, 0, lColumna - 1) = " "
    .Cell(flexcpText, 1, lColumna - 1) = "Total Promedio Mensual Activos"  '"Promedio Mensual"   'Modificado 27/07/2009 MMardones
    Rem Columna Totales     'Agregado 25/06/09
    lColumna = lColumna + 1
    .Cols = lColumna
    .Cell(flexcpText, 0, lColumna - 1) = " "
    .Cell(flexcpText, 1, lColumna - 1) = "Total Patrimonio Administrado"
    
    .MergeRow(0) = True
    .ColAlignment(0) = flexAlignRightCenter
    For lCol = 0 To .Cols - 1
      .ColAlignment(lCol) = flexAlignCenterCenter
    Next
    .AutoSize 0, .Cols - 1
  End With
  
ErrProcedure:
    
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEARCH"
        Call Sub_GeneraConsultaGrilla
    Case "REFRESH"
      Call Sub_Limpiar
    Case "REPORTE"
      Call Sub_GeneraInformePantalla
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_GeneraInformePantalla
    Case "EXCEL"
      Call Sub_GeneraInformeExcel
  End Select
End Sub

Private Sub Sub_GeneraConsultaGrilla()

  Call Sub_Bloquea_Puntero(Me)
  
  If Not ValidaEntradaDatos Then
     GoTo ExitProcedure
  End If

  fTipoConsulta = Fnt_ObtieneTipoConsulta
  
  Select Case fTipoConsulta
      Case "A"        'Ambos
          If Fnt_CargarDatos("D") Then
            Call Sub_CargaGrilla_Detalle
          Else
            GoTo ExitProcedure
          End If
          If Fnt_CargarDatos("R") Then
            Call Sub_CargaGrilla_Resumen
          End If
      Case "D"        'Detalle
          If Fnt_CargarDatos("D") Then
            Call Sub_CargaGrilla_Detalle
          End If
      Case "R"        'Resumen
          If Fnt_CargarDatos("R") Then
            Call Sub_CargaGrilla_Resumen
          End If
  End Select
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function ValidaEntradaDatos() As Boolean
Dim bOk As Boolean

  bOk = True
  If Cmb_Monedas.Text = "" Then
      MsgBox "Debe seleccionar Moneda de Salida", vbInformation, Me.Caption
      bOk = False
  End If
  ValidaEntradaDatos = bOk
End Function

Private Function Fnt_ObtieneTipoConsulta() As String
Dim lTipoConsulta As String

  If rdb_Detalle.Value = True Then
    lTipoConsulta = "D"
  End If
  
  If rdb_Resumen.Value = True Then
    lTipoConsulta = "R"
  End If
  
  If rdb_Ambos.Value = True Then
    lTipoConsulta = "A"
  End If
    
  Fnt_ObtieneTipoConsulta = lTipoConsulta
End Function

Private Function Fnt_CargarDatos(pTipoConsulta As String) As Boolean
Dim lcSaldos_Activos  As Class_Saldo_Activos
Dim lId_Asesor        As String
Dim lId_Cuenta        As String
Dim lCargaExitosa     As Boolean

  lCargaExitosa = False

  lId_Asesor = IIf(Cmb_Asesor.Text = "Todos", "", Fnt_ComboSelected_KEY(Cmb_Asesor))
  lId_Cuenta = IIf(Cmb_Cuentas.Text = "Todos", "", Fnt_ComboSelected_KEY(Cmb_Cuentas))
  fId_Moneda = Fnt_ComboSelected_KEY(Cmb_Monedas)
  fDecimales = Fnt_DecimalesMoneda(fId_Moneda)
  
  Set lcSaldos_Activos = New Class_Saldo_Activos
  With lcSaldos_Activos
    .Campo("FECHA_CIERRE").Valor = DTP_Fecha.Value
    .Campo("ID_CUENTA").Valor = lId_Cuenta
    If .Fnt_Consulta_ACI(Fnt_EmpresaActual, fId_Moneda, pTipoConsulta, lId_Asesor) Then
      Set fCursor_Datos = .Cursor
      If fCursor_Datos.Count > 0 Then
        lCargaExitosa = True
      Else
        MsgBox "No se registran Saldos Activos en esta fecha.", vbInformation, Me.Caption
      End If
    Else
      MsgBox "Error al cargar datos." & vbCr & vbCr & gDB.ErrMsg, vbInformation, Me.Caption
    End If
  End With
  Set lcSaldos_Activos = Nothing

ExitProcedure:
  Fnt_CargarDatos = lCargaExitosa
End Function

Private Function Fnt_DecimalesMoneda(lId_Moneda) As Integer
Dim lDecimales As Integer
Dim lcMoneda   As Object

  lDecimales = 0
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    .Campo("id_Moneda").Valor = lId_Moneda
    If .Buscar Then
      lDecimales = .Cursor(1)("dicimales_mostrar").Value
    End If
  End With
  Set lcMoneda = Nothing
  Fnt_DecimalesMoneda = lDecimales
  
End Function

Private Sub Sub_CargaGrilla_Detalle()
Dim lReg            As hFields
Dim lCol            As Integer
Dim lId_Cuenta_New  As String
Dim lId_Cuenta_Old  As String
Dim lFila           As Long
Dim lFila2           As Long
'-----------------------------
Dim lTotal          As Double
Dim lhTotales       As hRecord
Dim dTotal_Patrimonio As Double
Dim lMonto          As Double
  
  Set lhTotales = New hRecord
  With lhTotales
    .AddField "TOTAL", 0
  End With
  
  lId_Cuenta_New = ""
  lId_Cuenta_Old = ""
  
  With Grilla_Detalle
    .Rows = 2
    For Each lReg In fCursor_Datos
      lId_Cuenta_New = lReg("ID_CUENTA").Value
      
      If Not lId_Cuenta_Old = lId_Cuenta_New Then
        If Not lId_Cuenta_Old = "" Then
          .Cell(flexcpText, lFila, .Cols - 3) = FormatNumber(lTotal, fDecimales)  '27/07/09 Cambia posiciones
          .Select lFila, .Cols - 3
          .CellAlignment = flexAlignRightCenter
          
          lTotal = 0
        End If
      
        lFila = .Rows
        Call .AddItem("")
        lId_Cuenta_Old = lId_Cuenta_New
        
        .Cell(flexcpText, lFila, 0) = lReg("NUM_CUENTA").Value
        .Cell(flexcpText, lFila, 1) = lReg("DSC_CUENTA").Value
        
        lMonto = To_Number(NVL(lReg("PROMEDIO").Value, 0))
        .Cell(flexcpText, lFila, .Cols - 2) = FormatNumber(lMonto, fDecimales)  '27/07/09 Cambia posiciones
        .Select lFila, .Cols - 2
        .CellAlignment = flexAlignRightCenter
        
        lMonto = To_Number(NVL(lReg("PATRIMONIO_MON_CUENTA").Value, 0))
        .Cell(flexcpText, lFila, .Cols - 1) = FormatNumber(lMonto, fDecimales)   '27/07/09 Cambia posiciones
        .Select lFila, .Cols - 1
        .CellAlignment = flexAlignRightCenter
        
      End If
      
      For lCol = 2 To .Cols - 1
        If .ColKey(lCol) = lReg("ID_ARBOL_CLASE_INST").Value Then
            lMonto = To_Number(NVL(lReg("MONTO").Value, 0))
            .Cell(flexcpText, lFila, lCol) = FormatNumber(lMonto, fDecimales)
            
          'lTotal = lTotal + FormatNumber(lReg("MONTO").Value, fDecimales)
          lTotal = lTotal + lMonto
          .Select lFila, lCol
          .CellAlignment = flexAlignRightCenter
          Exit For
        End If
      Next
    Next
    
    .Cell(flexcpText, lFila, .Cols - 3) = FormatNumber(lTotal, fDecimales)   '27/07/09 Cambia posiciones
    .Select lFila, .Cols - 3
    .CellAlignment = flexAlignRightCenter
    
    Rem Agrega TOTALES por columna
    For lCol = 2 To .Cols - 1
      lTotal = 0
      For lFila = 2 To .Rows - 1
        If Not .Cell(flexcpText, lFila, lCol) = "" Then
          lTotal = lTotal + To_Number(.Cell(flexcpText, lFila, lCol))
        End If
      Next
      lhTotales.Add.Fields("TOTAL").Value = lTotal
    Next
    
    lFila = .Rows
    Call .AddItem("")
    .Cell(flexcpText, lFila, 1) = "TOTALES"
    .Cell(flexcpFontBold, lFila, 1) = True
    For lCol = 2 To .Cols - 1
'      If Not lhTotales.Item(lCol - 1).Item(1).Value = 0 Then
'        .Cell(flexcpText, lFila, lCol) = FormatNumber(lhTotales.Item(lCol - 1).Item(1).Value, fDecimales)
'        .Cell(flexcpFontBold, lFila, lCol) = True
'        .Select lFila, lCol
'        .CellAlignment = flexAlignRightCenter
'      End If
        lTotal = 0
        For lFila2 = 2 To .Rows - 1
          If Not .Cell(flexcpText, lFila2, lCol) = "" Then
            lTotal = lTotal + To_Number(.Cell(flexcpText, lFila2, lCol))
          End If
        Next
        .Cell(flexcpText, lFila, lCol) = FormatNumber(lTotal, fDecimales)
        .Cell(flexcpFontBold, lFila, lCol) = True
        .Select lFila, lCol
        .CellAlignment = flexAlignRightCenter

    Next
    
    Rem Autoajusta las columnas y deselecciona filas
    .AutoSize 0, .Cols - 1
    If .Row > 0 Then .IsSelected(.Row) = False
  End With
    
End Sub

Private Sub Sub_CargaGrilla_Resumen()
Dim lReg            As hFields
Dim lCol            As Integer
Dim lId_Asesor_New  As String
Dim lId_Asesor_Old  As String
Dim lFila           As Long
Dim lFila2           As Long
'-----------------------------
Dim lTotal          As Double
Dim lhTotales       As hRecord
Dim lMonto          As Double
  
  Set lhTotales = New hRecord
  With lhTotales
    .AddField "TOTAL", 0
  End With

  lId_Asesor_New = ""
  lId_Asesor_Old = ""
  
  With Grilla_Resumen
    .Rows = 2
    For Each lReg In fCursor_Datos
      
      If Not IsNull(lReg("ID_ASESOR").Value) Then
         lId_Asesor_New = lReg("ID_ASESOR").Value
      Else
         lId_Asesor_New = 0
      End If
      
      If Not lId_Asesor_Old = lId_Asesor_New Then
        If Not lId_Asesor_Old = "" Then
          .Cell(flexcpText, lFila, .Cols - 3) = FormatNumber(lTotal, fDecimales)
          .Select lFila, .Cols - 3
          .CellAlignment = flexAlignRightCenter
          
          lTotal = 0
        End If
        
        lFila = .Rows
        Call .AddItem("")
        lId_Asesor_Old = lId_Asesor_New
        
        .Cell(flexcpText, lFila, 0) = IIf(Not IsNull(lReg("ID_ASESOR").Value), Trim("" & lReg("NOMBRE_ASESOR").Value), "Sin Asesor")
        .Select lFila, 0
        .CellAlignment = flexAlignLeftCenter
        .ColHidden(0) = False
        
        lMonto = To_Number(NVL(lReg("PROMEDIO").Value, 0))
        .Cell(flexcpText, lFila, .Cols - 2) = FormatNumber(lMonto, fDecimales)
        .Select lFila, .Cols - 2
        .CellAlignment = flexAlignRightCenter
        
        lMonto = To_Number(NVL(lReg("TOTAL_PATRIMONIO").Value, 0))
        .Cell(flexcpText, lFila, .Cols - 1) = FormatNumber(lMonto, fDecimales)
        .Select lFila, .Cols - 1
        .CellAlignment = flexAlignRightCenter
      
      End If
      
      For lCol = 1 To .Cols - 1
        If .ColKey(lCol) = lReg("ID_ARBOL_CLASE_INST").Value Then
            lMonto = To_Number(NVL(lReg("MONTO").Value, 0))
            .Cell(flexcpText, lFila, lCol) = FormatNumber(lMonto, fDecimales)
            
          'lTotal = lTotal + FormatNumber(lReg("MONTO").Value, fDecimales)
          lTotal = lTotal + lMonto
          .Select lFila, lCol
          .CellAlignment = flexAlignRightCenter
          Exit For
        End If
      Next
    Next
    
    .Cell(flexcpText, lFila, .Cols - 3) = FormatNumber(lTotal, fDecimales)
    .Select lFila, .Cols - 3
    .CellAlignment = flexAlignRightCenter
    
    Rem Agrega TOTALES por columna
    For lCol = 1 To .Cols - 1
      lTotal = 0
      For lFila = 2 To .Rows - 1
        If Not .Cell(flexcpText, lFila, lCol) = "" Then
          lTotal = lTotal + To_Number(.Cell(flexcpText, lFila, lCol))
        End If
      Next
      lhTotales.Add.Fields("TOTAL").Value = lTotal
    Next
    
    lFila = .Rows
    Call .AddItem("")
    .Cell(flexcpText, lFila, 0) = "TOTALES"
    .Cell(flexcpFontBold, lFila, 0) = True
    For lCol = 1 To .Cols - 1
'      If Not lhTotales.Item(lCol).Item(1).Value = 0 Then
'        .Cell(flexcpText, lFila, lCol) = FormatNumber(lhTotales.Item(lCol).Item(1).Value, fDecimales)
'        .Cell(flexcpFontBold, lFila, lCol) = True
'        .Select lFila, lCol
'        .CellAlignment = flexAlignRightCenter
'      End If
        lTotal = 0
        For lFila2 = 2 To .Rows - 1
          If Not .Cell(flexcpText, lFila2, lCol) = "" Then
            lTotal = lTotal + To_Number(.Cell(flexcpText, lFila2, lCol))
          End If
        Next
        .Cell(flexcpText, lFila, lCol) = FormatNumber(lTotal, fDecimales)
        .Cell(flexcpFontBold, lFila, lCol) = True
        .Select lFila, lCol
        .CellAlignment = flexAlignRightCenter
    Next
    
    Rem Autoajusta las columnas y deselecciona filas
    .AutoSize 0, .Cols - 1
    If .Row > 0 Then .IsSelected(.Row) = False
  End With

End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Cmb_Asesor_ItemChange()
Dim lId_Asesor As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Cmb_Asesor <> "Todos" Then
    lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
    Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, pTodos:=True, pId_Asesor:=lId_Asesor)
    Call Sub_ComboSelectedItem(Cmb_Cuentas, cCmbKALL)
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub rdb_Ambos_Click()
    SST_Saldos.TabEnabled(0) = True
    SST_Saldos.TabEnabled(1) = True
    Grilla_Detalle.Rows = 2
    Grilla_Resumen.Rows = 2
    
    SST_Saldos.Tab = 0
End Sub

Private Sub rdb_Resumen_Click()
    SST_Saldos.TabEnabled(0) = True
    SST_Saldos.TabEnabled(1) = True
    Grilla_Detalle.Rows = 2
    Grilla_Resumen.Rows = 2
    
    SST_Saldos.TabEnabled(0) = False
    SST_Saldos.TabEnabled(1) = True
    
    SST_Saldos.Tab = 1
End Sub

Private Sub rdb_Detalle_Click()
    SST_Saldos.TabEnabled(0) = True
    SST_Saldos.TabEnabled(1) = True
    Grilla_Detalle.Rows = 2
    Grilla_Resumen.Rows = 2
    
    SST_Saldos.TabEnabled(0) = True
    SST_Saldos.TabEnabled(1) = False
    
    SST_Saldos.Tab = 0
End Sub

Private Sub Sub_Limpiar()
Dim lCierre As Class_Verificaciones_Cierre

    Cmb_Cuentas.Text = "Todos"
    Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
    Call Sub_ComboSelectedItem(Cmb_Monedas, 1)
    
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha.Value = lCierre.Busca_Ultima_FechaCierre
    Set lCierre = Nothing
    
    Grilla_Detalle.Rows = 2
    Grilla_Resumen.Rows = 2
    
    rdb_Ambos.Value = True
    Grilla_Resumen.ColHidden(0) = True
        
End Sub

Private Sub Sub_GeneraInformeExcel()
    
  Call Sub_Bloquea_Puntero(Me)
  
  If Not ValidaEntradaDatos Then
     GoTo ExitProcedure
  End If

  fTipoConsulta = Fnt_ObtieneTipoConsulta
  
  Select Case fTipoConsulta
      Case "A"        'Ambos
          If Fnt_CargarDatos("D") Then
            Call Sub_CreaExcel
            Call Sub_CargaExcel_Detalle
          Else
            GoTo ExitProcedure
          End If
          If Fnt_CargarDatos("R") Then
            Call Sub_CargaExcel_Resumen
          End If
      Case "D"        'Detalle
          If Fnt_CargarDatos("D") Then
            Call Sub_CreaExcel
            Call Sub_CargaExcel_Detalle
          Else
            GoTo ExitProcedure
          End If
      Case "R"        'Resumen
          If Fnt_CargarDatos("R") Then
            Call Sub_CreaExcel
            Call Sub_CargaExcel_Resumen
          Else
            GoTo ExitProcedure
          End If
  End Select
  
  fApp_Excel.Visible = True
  fApp_Excel.UserControl = True
 ' fApp_Excel.Quit
  
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CreaExcel()
Dim lHoja           As Integer
Dim lIndex          As Integer
Dim lTitulo_Detalle As String
Dim lTitulo_Resumen As String
Dim lTitulo_Fecha   As String
Dim lItem_Detalle   As String
Dim lItem_Resumen   As String

Const cTama�o_Titulo = 12
Const cTama�o_Fecha = 11

  Call Sub_Bloquea_Puntero(Me)
    
  If fTipoConsulta = "A" Then
    lHoja = 2
  Else
    lHoja = 1
  End If
  
  Set fApp_Excel = CreateObject("Excel.application")
  fApp_Excel.DisplayAlerts = False
  Set fLibro = fApp_Excel.Workbooks.Add
  
  fApp_Excel.ActiveWindow.DisplayGridlines = False
  fLibro.Worksheets.Add
  
  With fLibro
    For lIndex = .Worksheets.Count To lHoja + 1 Step -1
      .Worksheets(lIndex).Delete
    Next lIndex
      
    For lIndex = 1 To lHoja
      .Sheets(lIndex).Select
      fApp_Excel.ActiveWindow.DisplayGridlines = False
  '    .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).Select
      .ActiveSheet.Range("A5:E6").Font.Bold = True
      .ActiveSheet.Range("A5:E6").HorizontalAlignment = xlLeft
    Next
    lTitulo_Detalle = "Detalle Saldos Activos"
    lTitulo_Resumen = "Resumen Saldos Activos"
    lTitulo_Fecha = "Fecha Consulta: " & DTP_Fecha.Value
    lItem_Detalle = "Detalle"
    lItem_Resumen = "Resumen"
    
    Select Case fTipoConsulta
        Case "A"
            .Sheets(1).Select
            .ActiveSheet.Range("A5").Value = lTitulo_Detalle
            .ActiveSheet.Range("A5").Font.Size = cTama�o_Titulo
            .ActiveSheet.Range("A6").Value = lTitulo_Fecha
            .ActiveSheet.Range("A6").Font.Size = cTama�o_Fecha
            .Worksheets.Item(1).Name = lItem_Detalle
            
            .Sheets(2).Select
            .ActiveSheet.Range("A5").Value = lTitulo_Resumen
            .ActiveSheet.Range("A5").Font.Size = cTama�o_Titulo
            .ActiveSheet.Range("A6").Value = lTitulo_Fecha
            .ActiveSheet.Range("A6").Font.Size = cTama�o_Fecha
            .Worksheets.Item(2).Name = lItem_Resumen
        Case "D"
            .Sheets(1).Select
            .ActiveSheet.Range("A5").Value = lTitulo_Detalle
            .ActiveSheet.Range("A5").Font.Size = cTama�o_Titulo
            .ActiveSheet.Range("A6").Value = lTitulo_Fecha
            .ActiveSheet.Range("A6").Font.Size = cTama�o_Fecha
            .Worksheets.Item(1).Name = lItem_Detalle
        Case "R"
            .Sheets(1).Select
            .ActiveSheet.Range("A5").Value = lTitulo_Resumen
            .ActiveSheet.Range("A5").Font.Size = cTama�o_Titulo
            .ActiveSheet.Range("A6").Value = lTitulo_Fecha
            .ActiveSheet.Range("A6").Font.Size = cTama�o_Fecha
            .Worksheets.Item(1).Name = lItem_Resumen
    End Select
  End With
    
End Sub

Private Sub Sub_CargaExcel_Detalle()
Dim lxHoja          As Worksheet
Dim lcChart         As EXCEL.Chart
Dim lId_Cuenta_New  As String
Dim lId_Cuenta_Old  As String
Dim lCol_Grilla     As Integer
Dim lReg            As hFields
'-----------------------------
Dim lTotal          As Double
Dim lCol            As Integer
Dim lFila           As Long
Dim lMonto          As Double
Dim lIndex          As Integer
  fFilaExcel = 8
  
  Call Sub_ExcelEncabezado(Grilla_Detalle, 1)
  Set lxHoja = fLibro.Worksheets(1) ' Flibro.sheets(1)
  Call lxHoja.Select
  
  lId_Cuenta_New = ""
  lId_Cuenta_Old = ""
  
  With lxHoja
    For Each lReg In fCursor_Datos
      lId_Cuenta_New = lReg("ID_CUENTA").Value
      
      If Not lId_Cuenta_Old = lId_Cuenta_New Then
        If Not lId_Cuenta_Old = "" Then
          .Cells(fFilaExcel, Grilla_Detalle.Cols - 2) = lTotal
          .Cells(fFilaExcel, Grilla_Detalle.Cols - 2).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
          lTotal = 0
        End If
        
        fFilaExcel = fFilaExcel + 1
        lId_Cuenta_Old = lId_Cuenta_New
        
        .Cells(fFilaExcel, 1) = lReg("NUM_CUENTA").Value
        .Cells(fFilaExcel, 2) = lReg("DSC_CUENTA").Value
        
        lMonto = To_Number(NVL(lReg("PROMEDIO").Value, 0))
        .Cells(fFilaExcel, Grilla_Detalle.Cols - 1) = lMonto
        .Cells(fFilaExcel, Grilla_Detalle.Cols - 1).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
        
        lMonto = To_Number(NVL(lReg("PATRIMONIO_MON_CUENTA").Value, 0))
        .Cells(fFilaExcel, Grilla_Detalle.Cols) = lMonto     'Agregado 25/06/09
        .Cells(fFilaExcel, Grilla_Detalle.Cols).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
      End If
      
      For lCol_Grilla = 2 To Grilla_Detalle.Cols - 1
        If Grilla_Detalle.ColKey(lCol_Grilla) = lReg("ID_ARBOL_CLASE_INST").Value Then
            lMonto = To_Number(NVL(lReg("MONTO").Value, 0))
            .Cells(fFilaExcel, lCol_Grilla + 1) = lMonto
            .Cells(fFilaExcel, lCol_Grilla + 1).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
          
          lTotal = lTotal + lReg("MONTO").Value
          Exit For
        End If
      Next
    Next
    
    .Cells(fFilaExcel, Grilla_Detalle.Cols - 2) = lTotal
    .Cells(fFilaExcel, Grilla_Detalle.Cols - 2).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
    
    Rem Agrega TOTALES por columna
    fFilaExcel = fFilaExcel + 1
    .Cells(fFilaExcel, 2) = "TOTALES"
    .Cells(fFilaExcel, 2).Font.Bold = True
    For lCol = 3 To Grilla_Detalle.Cols
      lTotal = 0
      For lFila = 10 To fFilaExcel
        If Not .Cells(lFila, lCol) = "" Then
          lTotal = lTotal + .Cells(lFila, lCol)
        End If
      Next
      If Not lTotal = 0 Then
        .Cells(fFilaExcel, lCol) = lTotal
        .Cells(fFilaExcel, lCol).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
        .Cells(fFilaExcel, lCol).Font.Bold = True
      End If
    Next
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, Grilla_Detalle.Cols)).BorderAround
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, Grilla_Detalle.Cols)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, Grilla_Detalle.Cols)).Interior.Color = RGB(255, 255, 0)
    
    
   
  End With
  
 ' fLibro.ActiveSheet.Columns("A:A").Select
 ' fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
 ' fApp_Excel.Selection.EntireColumn.AutoFit
  
  Rem Grafico de Torta 'Problema de Objeto Excel
 Set lcChart = fApp_Excel.Charts.Add
  With lcChart
    .ChartType = xl3DPieExploded
    .SetSourceData lxHoja.Range(lxHoja.Cells(fFilaExcel, 3), lxHoja.Cells(fFilaExcel, Grilla_Detalle.Cols - 3)), xlRows
    .SeriesCollection(1).XValues = lxHoja.Range(lxHoja.Cells(9, 3), lxHoja.Cells(9, Grilla_Detalle.Cols - 3))
    .SeriesCollection(1).Name = "Cuenta"
    .SeriesCollection(1).ApplyDataLabels AutoText:=True, LegendKey:= _
        False, HasLeaderLines:=True, ShowSeriesName:=False, ShowCategoryName:=False _
        , ShowValue:=False, ShowPercentage:=True, ShowBubbleSize:=False
    .HasTitle = True
    .ChartTitle.Characters.Text = "Gr�fico de Totales por Cuenta"
    .ChartTitle.Font.Bold = True
  
    .Location xlLocationAutomatic, lxHoja.Name
    
  End With
  
  Call Sub_Configuracion_Grafico
  
  
  lxHoja.ChartObjects.Left = 250
  lxHoja.ChartObjects.Top = 15 * ((fFilaExcel) + 2)
  
  
  lxHoja.Pictures.Insert(gStrPictureEmpresaGrande).Select
    lxHoja.Pictures.Item(1).Top = 0
    lxHoja.Pictures.Item(1).Left = 0
    lxHoja.Range("A5:E6").Font.Bold = True
    lxHoja.Range("A5:E6").HorizontalAlignment = xlLeft
  
  
    
End Sub

Private Sub Sub_CargaExcel_Resumen()
Dim lxHoja          As Worksheet
Dim lcChart         As EXCEL.Chart
Dim lId_Asesor_New  As String
Dim lId_Asesor_Old  As String
Dim lCol_Grilla     As Integer
Dim lReg            As hFields
Dim lHoja           As Integer
'-----------------------------
Dim lTotal          As Double
Dim lCol            As Integer
Dim lFila           As Long
Dim lMonto          As Double

  fFilaExcel = 8
  
  If fTipoConsulta = "A" Then
    lHoja = 2
  Else
    lHoja = 1
  End If
  
  Call Sub_ExcelEncabezado(Grilla_Resumen, lHoja)
  Set lxHoja = fLibro.Worksheets(lHoja)  'fLibro.Sheets(lHoja)
  Call lxHoja.Select
  
  lId_Asesor_New = ""
  lId_Asesor_Old = ""
  
  With lxHoja
    For Each lReg In fCursor_Datos
      
      If Not IsNull(lReg("ID_Asesor").Value) Then
         lId_Asesor_New = lReg("ID_Asesor").Value
      Else
         lId_Asesor_New = 0
      End If
      
      If Not lId_Asesor_Old = lId_Asesor_New Then
        If Not lId_Asesor_Old = "" Then
          .Cells(fFilaExcel, Grilla_Resumen.Cols - 2) = lTotal
          .Cells(fFilaExcel, Grilla_Resumen.Cols - 2).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
          lTotal = 0
        End If
        
        fFilaExcel = fFilaExcel + 1
        lId_Asesor_Old = lId_Asesor_New
        
        .Cells(fFilaExcel, 1) = IIf(Not IsNull(lReg("ID_ASESOR").Value), Trim("" & lReg("NOMBRE_ASESOR").Value), "Sin Asesor")
        lMonto = To_Number(NVL(lReg("PROMEDIO").Value, 0))
        .Cells(fFilaExcel, Grilla_Resumen.Cols - 1) = lMonto
        .Cells(fFilaExcel, Grilla_Resumen.Cols - 1).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
        
        lMonto = To_Number(NVL(lReg("TOTAL_PATRIMONIO").Value, 0))
        .Cells(fFilaExcel, Grilla_Resumen.Cols) = lMonto    'Agregado 25/06/09
        .Cells(fFilaExcel, Grilla_Resumen.Cols).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
      End If
      
      For lCol_Grilla = 1 To Grilla_Resumen.Cols - 1
        If Grilla_Resumen.ColKey(lCol_Grilla) = lReg("ID_ARBOL_CLASE_INST").Value Then
            lMonto = To_Number(NVL(lReg("MONTO").Value, 0))
            .Cells(fFilaExcel, lCol_Grilla + 1) = lMonto
            .Cells(fFilaExcel, lCol_Grilla + 1).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
          
          lTotal = lTotal + lReg("MONTO").Value
          Exit For
        End If
      Next
    Next
    .Cells(fFilaExcel, Grilla_Resumen.Cols - 2) = lTotal
    .Cells(fFilaExcel, Grilla_Resumen.Cols - 2).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
    
    Rem Agrega TOTALES por columna
    fFilaExcel = fFilaExcel + 1
    .Cells(fFilaExcel, 1) = "TOTALES"
    .Cells(fFilaExcel, 1).Font.Bold = True
    For lCol = 2 To Grilla_Resumen.Cols
      lTotal = 0
      For lFila = 10 To fFilaExcel
        If Not .Cells(lFila, lCol) = "" Then
          lTotal = lTotal + .Cells(lFila, lCol)
        End If
      Next
      If Not lTotal = 0 Then
        .Cells(fFilaExcel, lCol) = lTotal
        .Cells(fFilaExcel, lCol).NumberFormat = Fnt_Formato_Moneda(fId_Moneda)
        .Cells(fFilaExcel, lCol).Font.Bold = True
      End If
    Next
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, Grilla_Resumen.Cols)).BorderAround
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, Grilla_Resumen.Cols)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, Grilla_Resumen.Cols)).Interior.Color = RGB(255, 255, 0)
  End With
  
  fLibro.ActiveSheet.Columns("A:A").Select
  fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
  fApp_Excel.Selection.EntireColumn.AutoFit
  
  Rem Grafico de Torta
  Set lcChart = fApp_Excel.Charts.Add
  With lcChart
    .ChartType = xl3DPieExploded
    .SetSourceData lxHoja.Range(lxHoja.Cells(fFilaExcel, 2), lxHoja.Cells(fFilaExcel, Grilla_Resumen.Cols - 3)), xlRows
    .SeriesCollection(1).XValues = lxHoja.Range(lxHoja.Cells(9, 2), lxHoja.Cells(9, Grilla_Resumen.Cols - 3))
    .SeriesCollection(1).Name = "Asesor"
    .SeriesCollection(1).ApplyDataLabels AutoText:=True, LegendKey:= _
        False, HasLeaderLines:=True, ShowSeriesName:=False, ShowCategoryName:=False _
        , ShowValue:=False, ShowPercentage:=True, ShowBubbleSize:=False
    .HasTitle = True
    .ChartTitle.Characters.Text = "Gr�fico de Totales por Asesor"
    .ChartTitle.Font.Bold = True
  
    .Location xlLocationAutomatic, lxHoja.Name
  End With
  
  Call Sub_Configuracion_Grafico
  
  lxHoja.ChartObjects.Left = 250
lxHoja.ChartObjects.Top = 15 * ((fFilaExcel) + 2)
      
    lxHoja.Pictures.Insert(gStrPictureEmpresaGrande).Select
    lxHoja.Pictures.Item(1).Top = 0
    lxHoja.Pictures.Item(1).Left = 0
    lxHoja.Range("A5:E6").Font.Bold = True
    lxHoja.Range("A5:E6").HorizontalAlignment = xlLeft

End Sub

Private Sub Sub_Configuracion_Grafico()
Dim objname As Object
  Rem Tama�o grafico
  'fLibro.ActiveSheet.ChartObjects("Gr�fico 1").Activate
 ' fLibro.ActiveSheet.ChartObjects.Item(1).Activate
 ' fLibro.ActiveChart.ChartArea.Select
 ' ActiveSheet.ChartObjects.Item(1).Activate
 ' ActiveSheet.ChartObjects.Item(1).Top = 130
 
  
 ' fLibro.ActiveSheet.Shapes.Item(1).ScaleWidth 1.1, 0, 2
 ' fLibro.ActiveSheet.Shapes.Item(1).ScaleWidth 1.12, 0, 2

  
  
  'fLibro.ActiveSheet.Shapes("Gr�fico 1").ScaleWidth 1.1, 0, 2
  'fLibro.ActiveSheet.Shapes("Gr�fico 1").ScaleWidth 1.12, 0, 0
  Rem Tama�o,color y lineas del grafico torta
  With fLibro.ActiveChart.PlotArea
    .Left = 30
    .Top = 106
    .Width = 361
    .Height = 146
    .Border.ColorIndex = 2
    .Border.Weight = xlThin
    .Border.LineStyle = xlContinuous
    .Interior.ColorIndex = 2
    .Interior.PatternColorIndex = 1
    .Interior.Pattern = xlSolid
  End With
  
  Rem fuente porcentajes
  fLibro.ActiveChart.SeriesCollection(1).DataLabels.AutoScaleFont = True
  With fLibro.ActiveChart.SeriesCollection(1).DataLabels.Font
    .Name = "Arial"
    .FontStyle = "Normal"
    .Size = 10
    .Strikethrough = False
    .Superscript = False
    .Subscript = False
    .OutlineFont = False
    .Shadow = False
    .Underline = xlUnderlineStyleNone
    .ColorIndex = xlAutomatic
    .Background = xlAutomatic
  End With
  
  fLibro.ActiveSheet.Range("A1").Select
End Sub

Private Sub Sub_ExcelEncabezado(pGrilla As VSFlexGrid, pHoja As Integer)
Dim lxHoja      As Worksheet
Dim lCol        As Integer
Dim lCol_Grilla As Integer
Dim lCol_Desde  As Integer
Dim lTitulo     As String
Dim lhRecord    As hRecord
Dim lReg        As hFields
Dim lReg_C      As hFields

  Set lhRecord = New hRecord
  With lhRecord
    .AddField "COL_DESDE"
    .AddField "COL_HASTA"
  End With

  Set lxHoja = fLibro.Worksheets(pHoja)  'fLibro.Sheets(pHoja)
  Call lxHoja.Select

  If pGrilla.Name = "Grilla_Detalle" Then
    lCol_Desde = 3
  ElseIf pGrilla.Name = "Grilla_Resumen" Then
    lCol_Desde = 2
  End If

  lCol = 1
  With lxHoja
    Rem Fila Titulos Clase Instrumentos
    For lCol_Grilla = 0 To pGrilla.Cols - 3
      .Cells(fFilaExcel, lCol) = pGrilla.Cell(flexcpText, 0, lCol_Grilla)
      lCol = lCol + 1
    Next
    
    lCol = lCol_Desde
    lTitulo = .Cells(fFilaExcel, lCol)
    Set lReg = lhRecord.Add
    lReg("COL_DESDE").Value = lCol
    
    For lCol = lCol_Desde + 1 To pGrilla.Cols - 3
      If lTitulo = .Cells(fFilaExcel, lCol) Then
        .Cells(fFilaExcel, lCol) = ""
      Else
        lTitulo = .Cells(fFilaExcel, lCol)
        lReg("COL_HASTA").Value = lCol - 1 '2
        Set lReg = lhRecord.Add
        lReg("COL_DESDE").Value = lCol
      End If
    Next
    lReg("COL_HASTA").Value = pGrilla.Cols - 3
    
    For Each lReg_C In lhRecord
      .Range(.Cells(fFilaExcel, lReg_C("COL_DESDE").Value), .Cells(fFilaExcel, lReg_C("COL_HASTA").Value)).MergeCells = True
    Next
    
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel, lCol - 1)).BorderAround
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel, lCol - 1)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel, lCol - 1)).Interior.Color = RGB(255, 255, 0)  '&HC0FFC0
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel, lCol - 1)).Font.Bold = True
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel, lCol - 1)).HorizontalAlignment = xlCenter
    
    Rem Fila Titulos SubClase Instrumentos
    fFilaExcel = fFilaExcel + 1
    lCol = 1
    For lCol_Grilla = 0 To pGrilla.Cols - 1
      .Cells(fFilaExcel, lCol).Value = pGrilla.Cell(flexcpText, 1, lCol_Grilla)
      lCol = lCol + 1
    Next
    
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, lCol - 1)).BorderAround
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, lCol - 1)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, lCol - 1)).Interior.Color = RGB(255, 255, 0)  '&HC0FFC0
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, lCol - 1)).Font.Bold = True
    .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, lCol - 1)).HorizontalAlignment = xlCenter
  End With
    
End Sub

Private Sub Sub_GeneraInformePantalla()

  Call Sub_Bloquea_Puntero(Me)
  
  If Not ValidaEntradaDatos Then
    GoTo ExitProcedure
  End If
  
  fTipoConsulta = Fnt_ObtieneTipoConsulta
  fPage = 1
  Select Case fTipoConsulta
      Case "A"        'Ambos
          If Fnt_CargarDatos("D") Then
            Call Sub_Inicia_Documento
            Call Sub_CargaInforme_Detalle
          Else
            GoTo ExitProcedure
          End If
          If Fnt_CargarDatos("R") Then
            fForm.VsPrinter.NewPage
            Call Sub_CargaInforme_Resumen
          End If
      Case "D"        'Detalle
          If Fnt_CargarDatos("D") Then
            Call Sub_Inicia_Documento
            Call Sub_CargaInforme_Detalle
          Else
            GoTo ExitProcedure
          End If
      Case "R"        'Resumen
          If Fnt_CargarDatos("R") Then
            Call Sub_Inicia_Documento
            Call Sub_CargaInforme_Resumen
          Else
            GoTo ExitProcedure
          End If
  End Select
  
  fForm.VsPrinter.EndDoc
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Inicia_Documento()
Dim lTitulo As String
  Select Case fTipoConsulta
    Case "A"
        lTitulo = "Detalle Saldos Activos"
    Case "D"
        lTitulo = "Detalle Saldos Activos"
    Case "R"
        lTitulo = "Resumen Saldos Activos"
  End Select
  
  Set fForm = New Frm_Reporte_Generico
  Call fForm.Sub_InicarDocumento(pTitulo:="" _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orLandscape)
  vMarginLeft = fForm.VsPrinter.MarginLeft
End Sub
Private Sub Sub_EncabezadoDetalle()
Dim lTitulo         As String
Dim lMultiplo       As Integer
Dim lFila           As Long
Dim lColRep         As Long
Dim lDesde          As Long
Dim lHasta          As Long
Dim lCol            As Long

    With fForm.VsPrinter
        .FontSize = 14
        .FontBold = True
        .MarginLeft = vMarginLeft
        If fPage > 1 Then
           .Paragraph = ""
        End If
        .Paragraph = "Detalle Saldos Activos"
        .FontSize = 10
        .Paragraph = "Fecha Consulta: " & DTP_Fecha.Value
        
        .StartTable
        .MarginLeft = "4mm"
        .FontSize = 7
        .TableCell(tcAlignCurrency) = False
        
        
        .TableCell(tcRows) = 1
        .TableCell(tcCols) = 5
        
        lMultiplo = 1
        lTitulo = Grilla_Detalle.Cell(flexcpText, 0, 2)
        
        lDesde = 60
        .TableCell(tcColWidth, 1, 1) = "42mm"
        lColRep = 1
        For lCol = 3 To Grilla_Detalle.Cols - 3
          If lTitulo = Grilla_Detalle.Cell(flexcpText, 0, lCol) Then
            lMultiplo = lMultiplo + 1
          Else
            lHasta = (21 * lMultiplo)
            lColRep = lColRep + 1
            .TableCell(tcText, 1, lColRep) = lTitulo
            .TableCell(tcColWidth, 1, lColRep, 1, lColRep) = CStr(lHasta) & "mm"
            lMultiplo = 1
            lTitulo = Grilla_Detalle.Cell(flexcpText, 0, lCol)
          End If
          
        Next
        
        .TableCell(tcColWidth, 1, 5) = "63mm"
        .TableCell(tcAlign, 1, 1, 1, 5) = taCenterMiddle
        .TableCell(tcFontBold, 1, 1, 1, 5) = True
        .TableCell(tcBackColor, 1, 1, 1, 5) = RGB(240, 240, 240)
        .EndTable
        .StartTable
        .MarginLeft = "4mm"
        .TableCell(tcRows) = 1
        .TableCell(tcCols) = Grilla_Detalle.Cols
        
        lFila = .TableCell(tcRows)
        For lCol = 0 To Grilla_Detalle.Cols - 1
            lTitulo = Grilla_Detalle.Cell(flexcpText, 1, lCol)
            .TableCell(tcText, lFila, lCol + 1) = lTitulo
        Next
        .TableCell(tcAlign, 1, 1, 1, 13) = taCenterMiddle
        .TableCell(tcFontBold, 1, 1, 1, 13) = True
        .TableCell(tcBackColor, 1, 1, 1, 13) = RGB(240, 240, 240)
        
        .TableCell(tcColWidth, 1, 1) = "12mm"
        .TableCell(tcColWidth, 1, 2) = "30mm"
        .TableCell(tcColWidth, 1, 3, 1, 13) = "21mm"
        .TableCell(tcFontBold, 1, 1, 1, 13) = True
        
        .TableCell(tcFontSize, 1, 1, 1, 13) = 7
        
    End With
End Sub
Private Sub Sub_CargaInforme_Detalle()
Dim sHeader_Det     As String
Dim sFormat_Det     As String
Dim sRecord
Dim sRecord_Total
Dim lReg            As hFields
Dim lTitulo_Fecha   As String
Dim lId_Cuenta_New  As String
Dim lId_Cuenta_Old  As String
Dim lCol            As Integer
Dim lCol_Actual     As Integer
Dim lPromedio       As String
Dim lTotal          As Double
Dim bAppend
Dim lTitulo         As String
Dim lMultiplo       As Integer
Dim lhMontos        As hRecord
Dim lReg_Montos     As hFields
Dim lMonto          As String
Dim lPatrimonio     As String
Dim lFila           As Long
Dim lColRep         As Long
Dim lDesde          As Long
Dim lHasta          As Long
Dim lLineas         As Long
Const cAncho = 1200
  
  Set lhMontos = New hRecord
  With lhMontos
    .AddField "ID_ARBOL_CLASE_INST"
    .AddField "MONTO", 0
  End With
      
  For lCol = 2 To Grilla_Detalle.Cols - 4
    Set lReg_Montos = lhMontos.Add
    lReg_Montos("ID_ARBOL_CLASE_INST").Value = Grilla_Detalle.ColKey(lCol)
  Next
  
  Set lReg_Montos = lhMontos.Add
  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL"
  Set lReg_Montos = lhMontos.Add
  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PROMEDIO"
  Set lReg_Montos = lhMontos.Add
  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PATRIMONIO"
  
  lTitulo_Fecha = "Fecha Consulta: " & DTP_Fecha.Value

  
  With fForm.VsPrinter
    .MarginLeft = "4mm"
    
'    .FontSize = 10
'    .FontBold = True
'    .Paragraph = lTitulo_Fecha
    .FontBold = False

    lLineas = 1
    Call Sub_EncabezadoDetalle
    lId_Cuenta_New = ""
    lId_Cuenta_Old = ""
    lColRep = 1
    For Each lReg In fCursor_Datos
      
        lId_Cuenta_New = lReg("ID_CUENTA").Value
        
        If Not lId_Cuenta_Old = lId_Cuenta_New Then
            If Not lId_Cuenta_Old = "" Then
                lColRep = 1
                For Each lReg_Montos In lhMontos
                    If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL" Then
                        lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lTotal
                        Exit For
                    End If
                Next
                
                .TableCell(tcText, lFila, 12) = lPromedio
                .TableCell(tcText, lFila, 13) = lPatrimonio
                .TableCell(tcText, lFila, 11) = FormatNumber(lTotal, fDecimales)
                .TableCell(tcAlign, lFila, 11, lFila, 13) = taRightMiddle
                .TableCell(tcColWidth, lFila, 11, lFila, 13) = "21mm"
                lTotal = 0
            End If
          
            lId_Cuenta_Old = lId_Cuenta_New
            lCol_Actual = 2
            
            lLineas = lLineas + 1
            If lLineas > 28 Then
                .EndTable
                fForm.VsPrinter.NewPage
                fPage = fPage + 1
                Call Sub_EncabezadoDetalle
                lLineas = 1
                
            End If
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            lFila = .TableCell(tcRows)
            
            .TableCell(tcText, lFila, lColRep) = lReg("NUM_CUENTA").Value
            .TableCell(tcAlign, lFila, lColRep) = taLeftMiddle
            .TableCell(tcColWidth, lFila, lColRep) = "12mm"
            lColRep = lColRep + 1
            .TableCell(tcText, lFila, lColRep) = lReg("DSC_CUENTA").Value
            .TableCell(tcAlign, lFila, lColRep) = taLeftMiddle
            .TableCell(tcColWidth, lFila, lColRep) = "30mm"
            
            lPromedio = FormatNumber(lReg("PROMEDIO").Value, fDecimales)
            lPatrimonio = FormatNumber(lReg("PATRIMONIO_MON_CUENTA").Value, fDecimales)
            .TableCell(tcText, lFila, 12) = lPromedio
            .TableCell(tcText, lFila, 13) = lPatrimonio
            For Each lReg_Montos In lhMontos
                If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PATRIMONIO" Then
                    lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + IIf(IsNumeric(lPatrimonio), lPatrimonio, 0)
                   ' Exit For
                End If
                If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PROMEDIO" Then
                    lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + IIf(IsNumeric(lPromedio), lPromedio, 0)
                   ' Exit For
                End If
              
            Next
        End If
        
        For lCol = lCol_Actual To Grilla_Detalle.Cols - 1
          If Grilla_Detalle.ColKey(lCol) = lReg("ID_ARBOL_CLASE_INST").Value Then
            lColRep = lCol + 1
            .TableCell(tcText, lFila, lColRep) = FormatNumber(lReg("MONTO").Value, fDecimales)
            lTotal = lTotal + FormatNumber(lReg("MONTO").Value, fDecimales)
            .TableCell(tcAlign, lFila, lColRep) = taRightMiddle
            .TableCell(tcColWidth, lFila, lColRep) = "21mm"
            
            lCol_Actual = lCol + 1
            Exit For
          Else
            lColRep = lCol + 1
           .TableCell(tcText, lFila, lColRep) = ""
          End If
        Next
        .TableCell(tcText, lFila, 12) = lPromedio
        .TableCell(tcText, lFila, 13) = lPatrimonio
        .TableCell(tcText, lFila, 11) = FormatNumber(lTotal, fDecimales)
        .TableCell(tcAlign, lFila, 11, lFila, 13) = taRightMiddle
        .TableCell(tcColWidth, lFila, 11, lFila, 13) = "21mm"
        
        For Each lReg_Montos In lhMontos
          If lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "TOTAL" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "PROMEDIO" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "PATRIMONIO" Then
            If lReg("ID_ARBOL_CLASE_INST").Value = To_Number(lReg_Montos("ID_ARBOL_CLASE_INST").Value) Then
              lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + FormatNumber(lReg("MONTO").Value, fDecimales)
              Exit For
            End If
        End If
        Next
      
    Next
    
    For Each lReg_Montos In lhMontos
      If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL" Then
        lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lTotal
        Exit For
      End If
    Next
    
    .FontBold = False
    .TableCell(tcRows) = .TableCell(tcRows) + 1
    lFila = .TableCell(tcRows)
    .TableCell(tcText, lFila, 2) = "TOTALES"
    
    lColRep = 2
    For Each lReg_Montos In lhMontos
      If To_Number(lReg_Montos("MONTO").Value) = 0 Then
        lMonto = ""
      Else
        lMonto = FormatNumber(lReg_Montos("MONTO").Value, fDecimales)
      End If
      lColRep = lColRep + 1
      .TableCell(tcText, lFila, lColRep) = lMonto
    Next
    
    .TableCell(tcAlign, lFila, 2, lFila, 13) = taRightMiddle
    .TableCell(tcColWidth, lFila, 1, lFila, 1) = "12mm"
    .TableCell(tcColWidth, lFila, 2, lFila, 2) = "30mm"
    .TableCell(tcColWidth, lFila, 3, lFila, 13) = "21mm"
    
    
    .TableCell(tcFontBold, lFila, 1, lFila, 13) = True
    .TableCell(tcBackColor, lFila, 1, lFila, 13) = RGB(240, 240, 240)
    
    .TableCell(tcFontSize, lFila, 1, lFila, 13) = 7
    .EndTable
  End With
  fForm.VsPrinter.MarginLeft = vMarginLeft
End Sub
Private Sub Sub_EncabezadoResumen()
Dim lTitulo         As String
Dim lMultiplo       As Integer
Dim lFila           As Long
Dim lColRep         As Long
Dim lDesde          As Long
Dim lHasta          As Long
Dim lCol            As Long

    With fForm.VsPrinter
        .FontSize = 14
        .FontBold = True
        .MarginLeft = vMarginLeft
        If fPage > 1 Then
           .Paragraph = ""
        End If
        .Paragraph = "Resumen Saldos Activos"
        .FontSize = 10
        .Paragraph = "Fecha Consulta: " & DTP_Fecha.Value
        
        .StartTable
        .MarginLeft = "4mm"
        .FontSize = 7
        .TableCell(tcAlignCurrency) = False
        
        
        .TableCell(tcRows) = 1
        .TableCell(tcCols) = 5
        
        lMultiplo = 1
        lTitulo = Grilla_Resumen.Cell(flexcpText, 0, 2)
        
        lDesde = 60
        .TableCell(tcColWidth, 1, 1) = "42mm"
        lColRep = 1
        For lCol = 2 To Grilla_Resumen.Cols - 1
          If lTitulo = Grilla_Resumen.Cell(flexcpText, 0, lCol) Then
            lMultiplo = lMultiplo + 1
          Else
            lHasta = (21 * lMultiplo)
            lColRep = lColRep + 1
            .TableCell(tcText, 1, lColRep) = lTitulo
            .TableCell(tcColWidth, 1, lColRep, 1, lColRep) = CStr(lHasta) & "mm"
          
            lTitulo = Grilla_Resumen.Cell(flexcpText, 0, lCol)
            lMultiplo = 1
          End If
        Next
            
        .TableCell(tcColWidth, 1, 5) = "63mm"
        .TableCell(tcAlign, 1, 1, 1, 5) = taCenterMiddle
        .TableCell(tcFontBold, 1, 1, 1, 5) = True
        .TableCell(tcBackColor, 1, 1, 1, 5) = RGB(240, 240, 240)
        .EndTable
        .StartTable
        .MarginLeft = "4mm"
        .TableCell(tcRows) = 1
        .TableCell(tcCols) = Grilla_Resumen.Cols
        
        lFila = .TableCell(tcRows)
        For lCol = 0 To Grilla_Resumen.Cols - 1
            lTitulo = Grilla_Resumen.Cell(flexcpText, 1, lCol)
            .TableCell(tcText, lFila, lCol + 1) = lTitulo
        Next
        .TableCell(tcAlign, 1, 1, 1, 12) = taCenterMiddle
        .TableCell(tcFontBold, 1, 1, 1, 12) = True
        .TableCell(tcBackColor, 1, 1, 1, 12) = RGB(240, 240, 240)
        
        .TableCell(tcColWidth, 1, 1) = "42mm"
        .TableCell(tcColWidth, 1, 2, 1, 12) = "21mm"
        .TableCell(tcFontBold, 1, 1, 1, 12) = True
        
        .TableCell(tcFontSize, 1, 1, 1, 12) = 7
        
    End With
End Sub
Private Sub Sub_CargaInforme_Resumen()
Dim sHeader_Det     As String
Dim sFormat_Det     As String
Dim sFormatRec_Det     As String
Dim sRecord
Dim sRecord_Total
Dim lReg            As hFields
Dim lTitulo_Fecha   As String
Dim lId_Asesor_New  As String
Dim lId_Asesor_Old  As String
Dim lCol            As Integer
Dim lCol_Actual     As Integer
Dim lPromedio       As String
Dim lTotal          As Double
Dim bAppend
Dim lTitulo         As String
Dim lMultiplo       As Integer
Dim lhMontos        As hRecord
Dim lReg_Montos     As hFields
Dim lMonto          As String
Dim lPatrimonio     As String
Dim lFila           As Long
Dim lColRep         As Long
Dim lDesde          As Long
Dim lHasta          As Long
Dim lLineas         As Long

Const cAncho = 1200
  
  Set lhMontos = New hRecord
  With lhMontos
    .AddField "ID_ARBOL_CLASE_INST"
    .AddField "MONTO", 0
  End With
      
  For lCol = 1 To Grilla_Resumen.Cols - 4
    Set lReg_Montos = lhMontos.Add
    lReg_Montos("ID_ARBOL_CLASE_INST").Value = Grilla_Resumen.ColKey(lCol)
  Next
  Set lReg_Montos = lhMontos.Add
  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL"
  Set lReg_Montos = lhMontos.Add
  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PROMEDIO"
  Set lReg_Montos = lhMontos.Add
  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PATRIMONIO"
  
  lTitulo_Fecha = "Fecha Consulta: " & DTP_Fecha.Value
    
  With fForm.VsPrinter
    .MarginLeft = "4mm"
    .FontBold = False
    
    lLineas = 1
    Call Sub_EncabezadoResumen
    
    .FontSize = 7
    .TableCell(tcAlignCurrency) = False
    
    lId_Asesor_New = ""
    lId_Asesor_Old = ""
    lColRep = 1
    For Each lReg In fCursor_Datos
      
          If Not IsNull(lReg("ID_ASESOR").Value) Then
             lId_Asesor_New = lReg("ID_ASESOR").Value
          Else
             lId_Asesor_New = 0
          End If
          
          If lId_Asesor_Old <> lId_Asesor_New Then
                If Not lId_Asesor_Old = "" Then
                    lColRep = 1
                    For Each lReg_Montos In lhMontos
                      If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL" Then
                        lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lTotal
                        Exit For
                      End If
                    Next
                    .TableCell(tcText, lFila, 11) = lPromedio
                    .TableCell(tcText, lFila, 12) = lPatrimonio
                    .TableCell(tcText, lFila, 10) = FormatNumber(lTotal, fDecimales)
                    .TableCell(tcAlign, lFila, 10, lFila, 12) = taRightMiddle
                    .TableCell(tcColWidth, lFila, 10, lFila, 12) = "21mm"
                    lTotal = 0
                End If
                
                lId_Asesor_Old = lId_Asesor_New
                lCol_Actual = 1
                
                lLineas = lLineas + 1
                If lLineas > 28 Then
                    .EndTable
                    fForm.VsPrinter.NewPage
                    fPage = fPage + 1
                    Call Sub_EncabezadoDetalle
                    lLineas = 1
                    
                End If
                .TableCell(tcRows) = .TableCell(tcRows) + 1
                lFila = .TableCell(tcRows)
                
                .TableCell(tcText, lFila, lColRep) = IIf(Not IsNull(lReg("ID_ASESOR").Value), Trim("" & lReg("NOMBRE_ASESOR").Value), "Sin Asesor")
                .TableCell(tcAlign, lFila, lColRep) = taLeftMiddle
                .TableCell(tcColWidth, lFila, lColRep) = "42mm"
                    
                lPromedio = FormatNumber(lReg("PROMEDIO").Value, fDecimales)
                lPatrimonio = FormatNumber(lReg("TOTAL_PATRIMONIO").Value, fDecimales)
                .TableCell(tcText, lFila, 11) = lPromedio
                .TableCell(tcText, lFila, 12) = lPatrimonio
    
                For Each lReg_Montos In lhMontos
                  If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PROMEDIO" Then
                    lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + IIf(IsNumeric(lPromedio), lPromedio, 0)
                  '  Exit For
                  End If
                    If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PATRIMONIO" Then
                      lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + IIf(IsNumeric(lPatrimonio), lPatrimonio, 0)
                   '   Exit For
                    End If
                Next
          End If
          
          For lCol = lCol_Actual To Grilla_Resumen.Cols - 1
            If Grilla_Resumen.ColKey(lCol) = lReg("ID_ARBOL_CLASE_INST").Value Then
                lMonto = FormatNumber(lReg("MONTO").Value, fDecimales)
               lColRep = lCol + 1 'lColRep + 1
               .TableCell(tcText, lFila, lColRep) = lMonto
               .TableCell(tcAlign, lFila, lColRep) = taRightMiddle
               .TableCell(tcColWidth, lFila, lColRep) = "21mm"
               lTotal = lTotal + FormatNumber(lReg("MONTO").Value, fDecimales)
               lCol_Actual = lCol + 1
              Exit For
            Else
                lColRep = lCol + 1 'lColRep + 1
               .TableCell(tcText, lFila, lColRep) = ""
            End If
          Next
        .TableCell(tcText, lFila, 11) = lPromedio
        .TableCell(tcText, lFila, 12) = lPatrimonio
        .TableCell(tcText, lFila, 10) = FormatNumber(lTotal, fDecimales)
        .TableCell(tcAlign, lFila, 10, lFila, 12) = taRightMiddle
        .TableCell(tcColWidth, lFila, 10, lFila, 12) = "21mm"

      
          For Each lReg_Montos In lhMontos
            If lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "TOTAL" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "PROMEDIO" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "PATRIMONIO" Then
                If lReg("ID_ARBOL_CLASE_INST").Value = To_Number(lReg_Montos("ID_ARBOL_CLASE_INST").Value) Then
                  lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + FormatNumber(lReg("MONTO").Value, fDecimales)
                  Exit For
                End If
            End If
          Next
          lColRep = 1
        Next
    
    For Each lReg_Montos In lhMontos
      If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL" Then
        lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lTotal
        Exit For
      End If
    Next
    
    .FontBold = False
    .TableCell(tcRows) = .TableCell(tcRows) + 1
    lFila = .TableCell(tcRows)
    .TableCell(tcText, lFila, 1) = "TOTALES"
    lColRep = 1
    For Each lReg_Montos In lhMontos
      If To_Number(lReg_Montos("MONTO").Value) = 0 Then
        lMonto = ""
      Else
        lMonto = FormatNumber(lReg_Montos("MONTO").Value, fDecimales)
      End If
      lColRep = lColRep + 1
      .TableCell(tcText, lFila, lColRep) = lMonto
      
    Next
    
    .TableCell(tcAlign, lFila, 1, lFila, 12) = taRightMiddle
    .TableCell(tcColWidth, lFila, 1, lFila, 1) = "42mm"
    .TableCell(tcColWidth, lFila, 2, lFila, 12) = "21mm"
    .TableCell(tcFontBold, lFila, 1, lFila, 12) = False
    
    .TableCell(tcFontBold, lFila, 1, lFila, 12) = True
    .TableCell(tcBackColor, lFila, 1, lFila, 12) = RGB(240, 240, 240)
    
    .TableCell(tcFontSize, 1, 1, 1, 12) = 7

    .EndTable

  End With
  fForm.VsPrinter.MarginLeft = vMarginLeft
End Sub

'Private Sub Sub_CargaInforme_Detalle()
'Dim sHeader_Det     As String
'Dim sFormat_Det     As String
'Dim sRecord
'Dim sRecord_Total
'Dim lReg            As hFields
'Dim lTitulo_Fecha   As String
'Dim lId_Cuenta_New  As String
'Dim lId_Cuenta_Old  As String
'Dim lCol            As Integer
'Dim lCol_Actual     As Integer
'Dim lPromedio       As String
'Dim lTotal          As Double
'Dim bAppend
'Dim lTitulo         As String
'Dim lMultiplo       As Integer
'Dim lhMontos        As hRecord
'Dim lReg_Montos     As hFields
'Dim lMonto          As String
'Dim lPatrimonio     As String
'Const cAncho = 1200
'
'  Set lhMontos = New hRecord
'  With lhMontos
'    .AddField "ID_ARBOL_CLASE_INST"
'    .AddField "MONTO", 0
'  End With
'
'  For lCol = 2 To Grilla_Detalle.Cols - 4
'    Set lReg_Montos = lhMontos.Add
'    lReg_Montos("ID_ARBOL_CLASE_INST").Value = Grilla_Detalle.ColKey(lCol)
'  Next
'
'  Set lReg_Montos = lhMontos.Add
'  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL"
'  Set lReg_Montos = lhMontos.Add
'  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PROMEDIO"
'  Set lReg_Montos = lhMontos.Add
'  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PATRIMONIO"
'
'  lTitulo_Fecha = "Fecha Consulta: " & DTP_Fecha.Value
'
'
'  With fForm.VsPrinter
'    '.MarginLeft = "4mm"
'
'    .FontSize = 10
'    .FontBold = True
'    .Paragraph = lTitulo_Fecha
'    .Paragraph = "" 'salto de linea
'    .FontBold = False
'    .StartTable
'    .FontSize = 7
'    .TableCell(tcAlignCurrency) = False
'
'    .MarginLeft = "4mm"
'
'    sHeader_Det = ""
'    sFormat_Det = ""
'
'    sHeader_Det = sHeader_Det & " " & "|"
'    sFormat_Det = sFormat_Det & ">2100|"
'    lMultiplo = 1
'    lTitulo = Grilla_Detalle.Cell(flexcpText, 0, 2)
'
'    For lCol = 3 To Grilla_Detalle.Cols - 1
'      If lTitulo = Grilla_Detalle.Cell(flexcpText, 0, lCol) Then
'        lMultiplo = lMultiplo + 1
'      Else
'        lTitulo = Grilla_Detalle.Cell(flexcpText, 0, lCol)
'        sHeader_Det = sHeader_Det & Grilla_Detalle.Cell(flexcpText, 0, lCol - 1) & "|"
'        sFormat_Det = sFormat_Det & "^" & CStr(cAncho * lMultiplo) & "|"
'        lMultiplo = 1
'      End If
'    Next
'
'    sHeader_Det = sHeader_Det & Grilla_Detalle.Cell(flexcpText, 0, Grilla_Detalle.Cols - 1)
'    sFormat_Det = sFormat_Det & CStr(cAncho * lMultiplo)
'
'    .AddTable sFormat_Det, sHeader_Det, sRecord, clrHeader, , bAppend
'    .EndTable
'    .StartTable
'
'    sHeader_Det = ""
'    sFormat_Det = ""
'
'    sHeader_Det = sHeader_Det & Grilla_Detalle.Cell(flexcpText, 1, 0) & "|"
'    sFormat_Det = sFormat_Det & ">600|"
'    sHeader_Det = sHeader_Det & Grilla_Detalle.Cell(flexcpText, 1, 1) & "|"
'    sFormat_Det = sFormat_Det & "<1500|"
'
'    For lCol = 2 To Grilla_Detalle.Cols - 1
'      sHeader_Det = sHeader_Det & Grilla_Detalle.Cell(flexcpText, 1, lCol)
'      sFormat_Det = sFormat_Det & ">" & CStr(cAncho) '">" & CStr(cAncho)
'      If Not lCol = Grilla_Detalle.Cols - 1 Then
'        sHeader_Det = sHeader_Det & "|"
'        sFormat_Det = sFormat_Det & "|"
'      End If
'    Next
'
''    .AddTable sFormat_Det, sHeader_Det, sRecord, clrHeader, , bAppend
''    .EndTable
''    .StartTable
''
'    lId_Cuenta_New = ""
'    lId_Cuenta_Old = ""
'
'    For Each lReg In fCursor_Datos
'
'        lId_Cuenta_New = lReg("ID_CUENTA").Value
'
'        If Not lId_Cuenta_Old = lId_Cuenta_New Then
'            If Not lId_Cuenta_Old = "" Then
'                Call Sub_Carga_Registro(lCol_Actual, Grilla_Detalle, sRecord, sFormat_Det, sHeader_Det, lPatrimonio, lPromedio, lTotal, fForm.VsPrinter)
'                For Each lReg_Montos In lhMontos
'                    If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL" Then
'                        lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lTotal
'                        Exit For
'                    End If
'                Next
'                lTotal = 0
'            End If
'
'            lId_Cuenta_Old = lId_Cuenta_New
'            lCol_Actual = 2
'
'            sRecord = lReg("NUM_CUENTA").Value & "|" & lReg("DSC_CUENTA").Value & "|"
'            lPromedio = FormatNumber(lReg("PROMEDIO").Value, fDecimales)
'            lPatrimonio = FormatNumber(lReg("PATRIMONIO_MON_CUENTA").Value, fDecimales)
'            For Each lReg_Montos In lhMontos
'                If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PATRIMONIO" Then
'                    lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lPatrimonio
'                   ' Exit For
'                End If
'                If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PROMEDIO" Then
'                    lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lPromedio
'                   ' Exit For
'                End If
'
'            Next
'        End If
'
'        For lCol = lCol_Actual To Grilla_Detalle.Cols - 1
'          If Grilla_Detalle.ColKey(lCol) = lReg("ID_ARBOL_CLASE_INST").Value Then
'            sRecord = sRecord & FormatNumber(lReg("MONTO").Value, fDecimales) & "|"
'            lTotal = lTotal + FormatNumber(lReg("MONTO").Value, fDecimales)
'            lCol_Actual = lCol + 1
'            Exit For
'          Else
'            sRecord = sRecord & "|"
'          End If
'        Next
'
'        For Each lReg_Montos In lhMontos
'          If lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "TOTAL" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "PROMEDIO" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "PATRIMONIO" Then
'            If lReg("ID_ARBOL_CLASE_INST").Value = To_Number(lReg_Montos("ID_ARBOL_CLASE_INST").Value) Then
'              lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + FormatNumber(lReg("MONTO").Value, fDecimales)
'              Exit For
'            End If
'        End If
'        Next
'
'    Next
'
'    Call Sub_Carga_Registro(lCol_Actual, Grilla_Detalle, sRecord, sFormat_Det, sHeader_Det, lPatrimonio, lPromedio, lTotal, fForm.VsPrinter)
'    For Each lReg_Montos In lhMontos
'      If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL" Then
'        lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lTotal
'        Exit For
'      End If
'    Next
'
'    .FontBold = False
'    .EndTable
'
'    Rem Tabla Totales
'    .StartTable
'    sHeader_Det = ""
'    sFormat_Det = ""
'    sHeader_Det = sHeader_Det & "TOTALES" & "|"
'    sFormat_Det = sFormat_Det & ">2100|"
'
'    For Each lReg_Montos In lhMontos
'      If To_Number(lReg_Montos("MONTO").Value) = 0 Then
'        lMonto = ""
'      Else
'        lMonto = FormatNumber(lReg_Montos("MONTO").Value, fDecimales)
'      End If
'      sHeader_Det = sHeader_Det & lMonto
'      sFormat_Det = sFormat_Det & ">" + CStr(cAncho)
'      If Not lReg_Montos.Index = lhMontos.Count Then
'        sHeader_Det = sHeader_Det & "|"
'        sFormat_Det = sFormat_Det & "|"
'      End If
'    Next
'    .AddTable sFormat_Det, sHeader_Det, sRecord_Total, clrHeader, , bAppend
'    .EndTable
'  End With
'  fForm.VsPrinter.MarginLeft = vMarginLeft
'End Sub

'Private Sub Sub_CargaInforme_Resumen()
'Dim sHeader_Det     As String
'Dim sFormat_Det     As String
'Dim sFormatRec_Det     As String
'Dim sRecord
'Dim sRecord_Total
'Dim lReg            As hFields
'Dim lTitulo_Fecha   As String
'Dim lId_Asesor_New  As String
'Dim lId_Asesor_Old  As String
'Dim lCol            As Integer
'Dim lCol_Actual     As Integer
'Dim lPromedio       As String
'Dim lTotal          As Double
'Dim bAppend
'Dim lTitulo         As String
'Dim lMultiplo       As Integer
'Dim lhMontos        As hRecord
'Dim lReg_Montos     As hFields
'Dim lMonto          As String
'Dim lPatrimonio     As String
'
'Const cAncho = 1200
'
'  Set lhMontos = New hRecord
'  With lhMontos
'    .AddField "ID_ARBOL_CLASE_INST"
'    .AddField "MONTO", 0
'  End With
'
'  For lCol = 1 To Grilla_Resumen.Cols - 4
'    Set lReg_Montos = lhMontos.Add
'    lReg_Montos("ID_ARBOL_CLASE_INST").Value = Grilla_Resumen.ColKey(lCol)
'  Next
'  Set lReg_Montos = lhMontos.Add
'  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL"
'  Set lReg_Montos = lhMontos.Add
'  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PROMEDIO"
'  Set lReg_Montos = lhMontos.Add
'  lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PATRIMONIO"
'
'  lTitulo_Fecha = "Fecha Consulta: " & DTP_Fecha.Value
'
'  With fForm.VsPrinter
'    .MarginLeft = vMarginLeft
'    .FontSize = 10
'    .FontBold = True
'    .Paragraph = lTitulo_Fecha
'    .Paragraph = "" 'salto de linea
'    .FontBold = False
'    .StartTable
'    .FontSize = 7
'    .TableCell(tcAlignCurrency) = False
'
'    .MarginLeft = "4mm"
'
'    sHeader_Det = ""
'    sFormat_Det = ""
'
'    sHeader_Det = sHeader_Det & " " & "|"
'    sFormat_Det = sFormat_Det & ">2100|"
'    lMultiplo = 1
'    lTitulo = Grilla_Resumen.Cell(flexcpText, 0, 1)
'
'    For lCol = 2 To Grilla_Resumen.Cols - 1
'      If lTitulo = Grilla_Resumen.Cell(flexcpText, 0, lCol) Then
'        lMultiplo = lMultiplo + 1
'      Else
'        lTitulo = Grilla_Resumen.Cell(flexcpText, 0, lCol)
'        sHeader_Det = sHeader_Det & Grilla_Resumen.Cell(flexcpText, 0, lCol - 1) & "|"
'        sFormat_Det = sFormat_Det & "^" & CStr(cAncho * lMultiplo) & "|"
'        lMultiplo = 1
'      End If
'    Next
'
'    sHeader_Det = sHeader_Det & Grilla_Resumen.Cell(flexcpText, 0, Grilla_Resumen.Cols - 1)
'    sFormat_Det = sFormat_Det & CStr(cAncho * lMultiplo)
'
'    .AddTable sFormat_Det, sHeader_Det, sRecord, clrHeader, , bAppend
'    .EndTable
'    .StartTable
'
'    sHeader_Det = ""
'    sFormat_Det = ""
'
'    sHeader_Det = sHeader_Det & Grilla_Resumen.Cell(flexcpText, 1, 0) & "|"
'    sFormat_Det = sFormat_Det & "<2100|"
'    sFormatRec_Det = sFormatRec_Det & "<2100|"
'
'    For lCol = 1 To Grilla_Resumen.Cols - 1
'      sHeader_Det = sHeader_Det & Grilla_Resumen.Cell(flexcpText, 1, lCol)
'      sFormat_Det = sFormat_Det & "^" + CStr(cAncho)
'      sFormatRec_Det = sFormatRec_Det & ">" + CStr(cAncho)
'      If Not lCol = Grilla_Resumen.Cols - 1 Then
'        sHeader_Det = sHeader_Det & "|"
'        sFormat_Det = sFormat_Det & "|"
'        sFormatRec_Det = sFormatRec_Det & "|"
'      End If
'    Next
'
'
'    lId_Asesor_New = ""
'    lId_Asesor_Old = ""
'
'    For Each lReg In fCursor_Datos
'
'      lId_Asesor_New = lReg("ID_ASESOR").Value
'
'      If Not lId_Asesor_Old = lId_Asesor_New Then
'        If Not lId_Asesor_Old = "" Then
'          Call Sub_Carga_Registro(lCol_Actual, Grilla_Resumen, sRecord, sFormatRec_Det, sHeader_Det, lPatrimonio, lPromedio, lTotal, fForm.VsPrinter)
'          For Each lReg_Montos In lhMontos
'            If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL" Then
'              lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lTotal
'              Exit For
'            End If
'          Next
'          lTotal = 0
'        End If
'
'        lId_Asesor_Old = lId_Asesor_New
'        lCol_Actual = 1
'
'        sRecord = lReg("NOMBRE_ASESOR").Value & "|"
'        lPromedio = FormatNumber(lReg("PROMEDIO").Value, fDecimales)
'        lPatrimonio = FormatNumber(lReg("TOTAL_PATRIMONIO").Value, fDecimales)
'        For Each lReg_Montos In lhMontos
'          If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PROMEDIO" Then
'            lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lPromedio
'          '  Exit For
'          End If
'            If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "PATRIMONIO" Then
'              lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lPatrimonio
'           '   Exit For
'            End If
'        Next
'      End If
'
'      For lCol = lCol_Actual To Grilla_Resumen.Cols - 1
'        If Grilla_Resumen.ColKey(lCol) = lReg("ID_ARBOL_CLASE_INST").Value Then
'          sRecord = sRecord & FormatNumber(lReg("MONTO").Value, fDecimales) & "|"
'          lTotal = lTotal + FormatNumber(lReg("MONTO").Value, fDecimales)
'          lCol_Actual = lCol + 1
'          Exit For
'        Else
'          sRecord = sRecord & "|"
'        End If
'      Next
'
'      For Each lReg_Montos In lhMontos
'        If lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "TOTAL" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "PROMEDIO" And lReg_Montos("ID_ARBOL_CLASE_INST").Value <> "PATRIMONIO" Then
'            If lReg("ID_ARBOL_CLASE_INST").Value = To_Number(lReg_Montos("ID_ARBOL_CLASE_INST").Value) Then
'              lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + FormatNumber(lReg("MONTO").Value, fDecimales)
'              Exit For
'            End If
'        End If
'      Next
'
'    Next
'
'    Call Sub_Carga_Registro(lCol_Actual, Grilla_Resumen, sRecord, sFormatRec_Det, sHeader_Det, lPatrimonio, lPromedio, lTotal, fForm.VsPrinter)
'    For Each lReg_Montos In lhMontos
'      If lReg_Montos("ID_ARBOL_CLASE_INST").Value = "TOTAL" Then
'        lReg_Montos("MONTO").Value = lReg_Montos("MONTO").Value + lTotal
'        Exit For
'      End If
'    Next
'
'    .FontBold = False
'    .EndTable
'
'    Rem Tabla Totales
'    .StartTable
'    sHeader_Det = ""
'    sFormat_Det = ""
'    sHeader_Det = sHeader_Det & "TOTALES" & "|"
'    sFormat_Det = sFormat_Det & ">2100|"
'
'    For Each lReg_Montos In lhMontos
'      If To_Number(lReg_Montos("MONTO").Value) = 0 Then
'        lMonto = ""
'      Else
'        lMonto = FormatNumber(lReg_Montos("MONTO").Value, fDecimales)
'      End If
'
'      sHeader_Det = sHeader_Det & lMonto
'      sFormat_Det = sFormat_Det & ">" + CStr(cAncho)
'      If Not lReg_Montos.Index = lhMontos.Count Then
'        sHeader_Det = sHeader_Det & "|"
'        sFormat_Det = sFormat_Det & "|"
'      End If
'    Next
'    .AddTable sFormat_Det, sHeader_Det, sRecord_Total, clrHeader, , bAppend
'    .EndTable
'  End With
'  fForm.VsPrinter.MarginLeft = vMarginLeft
'End Sub

Private Sub Sub_Carga_Registro(pCol_Actual As Integer _
                             , pGrilla As VSFlexGrid _
                             , pRecord _
                             , pFormat_Det _
                             , pHeader_Det _
                             , pPatrimonio As String _
                             , pPromedio As String _
                             , pTotal _
                             , ByRef pGrid As VsPrinter)
    
Dim lCol As Integer
Dim bAppend

  For lCol = pCol_Actual To pGrilla.Cols - 1
    Select Case lCol
      Case pGrilla.Cols - 3
        pRecord = pRecord & pPatrimonio & "|"
      Case pGrilla.Cols - 2
        pRecord = pRecord & FormatNumber(pTotal, fDecimales) & "|"
      Case pGrilla.Cols - 1
        pRecord = pRecord & pPromedio
      Case Else
        pRecord = pRecord & "|"
    End Select
  Next
  pGrid.AddTable pFormat_Det, pHeader_Det, pRecord, clrHeader, , bAppend
  
End Sub

