VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "*\A..\CSBPI\hControl2\hControl2.vbp"
Begin VB.Form Frm_Paises 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pais"
   ClientHeight    =   5160
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8595
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5160
   ScaleWidth      =   8595
   Begin TabDlg.SSTab SSTab 
      Height          =   4605
      Left            =   90
      TabIndex        =   0
      Top             =   480
      Width           =   8445
      _ExtentX        =   14896
      _ExtentY        =   8123
      _Version        =   393216
      Tabs            =   1
      TabsPerRow      =   6
      TabHeight       =   529
      WordWrap        =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "Frm_Pais.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      Begin VB.Frame Frame1 
         Height          =   4065
         Left            =   180
         TabIndex        =   3
         Top             =   360
         Width           =   8085
         Begin hControl2.hTextLabel txt_paises 
            Height          =   375
            Left            =   120
            TabIndex        =   4
            Top             =   300
            Width           =   3495
            _ExtentX        =   6165
            _ExtentY        =   661
            LabelWidth      =   1275
            Caption         =   "Cod Pais"
            Text            =   ""
         End
         Begin hControl2.hTextLabel txt_dsc_pais 
            Height          =   345
            Left            =   120
            TabIndex        =   5
            Top             =   750
            Width           =   3495
            _ExtentX        =   6165
            _ExtentY        =   609
            LabelWidth      =   1275
            Caption         =   "Descripcion"
            Text            =   ""
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8595
      _ExtentX        =   15161
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   2
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Paises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Const cTipo_Estado = 1 'Este codigo es unico para los clientes.

Public fKey As String
'Dim fBorrar_Direccion As hRecord

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = "boton_grabar"
      .Buttons("EXIT").Image = "boton_salir"
      .Buttons("REFRESH").Image = "boton_original"
  End With
  
  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(ByVal pcod_pais As String)
  fKey = pcod_pais
  
  If Not pcod_pais = "-1" Then
    txt_paises.Locked = True
  End If
  
  Call Sub_CargarDatos
    
  Me.Caption = "Modificaci�n Pais: " & txt_paises.Text & " - " & txt_dsc_pais.Text
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub


Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perdera las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
'Dim lcod_pais As String
'Dim ldsc_pais As String
'Dim lLinea As Long
Dim lResult As Boolean
Dim lReg As hFields

On Error GoTo ErrProcedure
  
  
  Call gDB.IniciarTransaccion
  
  lResult = True

  If Not Fnt_ValidarDatos Then
    lResult = False
    GoTo ErrProcedure
  End If
 
  'Grabando el pais
  With gDB
    .Procedimiento = "Sp_Iu_Pais"
    Call .Parametros.Add("Pcod_pais", ePT_Numero, txt_paises.Text, ePD_Ambos)
    Call .Parametros.Add("Pdsc_pais", ePT_Caracter, txt_dsc_pais.Text, ePD_Entrada)
    
    If Not .EjecutaSP Then
      MsgBox "Problemas en grabar el Pais." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      lResult = False
      GoTo ErrProcedure
    End If
    
    fKey = .Parametros("Pcod_Pais").Valor
    
    .Parametros.Clear
  End With
  
ErrProcedure:
  If Err Then
    lResult = False
  End If
  
  If lResult Then
    Call gDB.CommitTransaccion
  Else
    Call gDB.RollbackTransaccion
  End If
  
  gDB.Parametros.Clear
  Fnt_Grabar = lResult
End Function

Private Sub Sub_CargaForm()
Dim lReg As hFields
Dim lComboList As String

  
  Call Sub_FormControl_Color(Me.Controls)

  SSTab.Tab = 0
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
  
  Load Me
  With gDB
    .Procedimiento = "Rea_View_Paises"
    .Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
    .Parametros.Add "pCod_pais", ePT_Numero, fKey, ePD_Entrada
    If .EjecutaSP Then
      For Each lReg In .Parametros("pcursor").Valor
      
        txt_paises.Text = NVL(lReg("cod_pais").Value, "")
        txt_dsc_pais.Text = NVL(lReg("dsc_pais").Value, "")
        
      Next
    Else
      MsgBox "Problemas en cargar el Pais." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
    
    .Parametros.Clear
  End With
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function


Private Sub Toolbar_Direccion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "ADD"
      Call Grilla_Direcciones.AddItem("I")
'    Case "DEL"
'      If Grilla_Direcciones.Row > 0 Then
'        If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
'          With Grilla_Direcciones
'            If Not GetCell(Grilla_Direcciones, .Row, "id_direccion_cliente") = "" Then
'              fBorrar_Direccion.Add.Fields("id_direccion").Value = GetCell(Grilla_Direcciones, .Row, "id_direccion_cliente")
'            End If
'
'            Call .RemoveItem(.Row)
'          End With
'        End If
'      End If
  End Select
End Sub

