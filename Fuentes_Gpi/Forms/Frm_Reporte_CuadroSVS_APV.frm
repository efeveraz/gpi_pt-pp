VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_CuadroSVS_APV 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cuadros SVS"
   ClientHeight    =   8085
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13605
   ControlBox      =   0   'False
   Icon            =   "Frm_Reporte_CuadroSVS_APV.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7127.386
   ScaleMode       =   0  'User
   ScaleWidth      =   12472.33
   Begin VB.Frame Frm_Periodo_Consulta 
      Caption         =   "Filtro de b�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7440
      Left            =   55
      TabIndex        =   0
      Top             =   480
      Width           =   13515
      Begin VB.Frame Frm_TipoReporte 
         Caption         =   "Tipo Reporte"
         Height          =   1815
         Left            =   5040
         TabIndex        =   10
         Top             =   240
         Width           =   8415
         Begin VB.CheckBox Chk_Cuadro4 
            Caption         =   "Cuadro N� 4 - Traspasos Dep�sitos Convenidos"
            Height          =   255
            Left            =   120
            TabIndex        =   14
            Top             =   1440
            Width           =   5415
         End
         Begin VB.CheckBox Chk_Cuadro3 
            Caption         =   "Cuadro N� 3 - Traspasos Dep�sitos Ahorro Previsional Voluntario"
            Height          =   255
            Left            =   120
            TabIndex        =   13
            Top             =   1080
            Width           =   6975
         End
         Begin VB.CheckBox Chk_Cuadro2 
            Caption         =   "Cuadro N� 2 - Dep�sitos Convenidos"
            Height          =   255
            Left            =   120
            TabIndex        =   12
            Top             =   720
            Width           =   5415
         End
         Begin VB.CheckBox Chk_Cuadro1 
            Caption         =   "Cuadro N� 1 - Dep�sitos Ahorro Previsional Voluntario"
            Height          =   255
            Left            =   120
            TabIndex        =   11
            Top             =   360
            Width           =   5655
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Elija el formato de la fecha"
         ForeColor       =   &H00000000&
         Height          =   1815
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   4815
         Begin VB.OptionButton rdb_RangoFecha 
            Caption         =   "Rango de Fecha"
            Height          =   255
            Left            =   120
            TabIndex        =   5
            Top             =   360
            Value           =   -1  'True
            Width           =   1515
         End
         Begin VB.OptionButton Rdb_Mes 
            Caption         =   "Mes"
            Height          =   255
            Left            =   120
            TabIndex        =   4
            Top             =   720
            Width           =   1275
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
            Height          =   345
            Left            =   3000
            TabIndex        =   6
            Top             =   360
            Width           =   1695
            _ExtentX        =   2990
            _ExtentY        =   609
            _Version        =   393216
            CustomFormat    =   "MMMM yyyy"
            Format          =   68222977
            CurrentDate     =   39153
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
            Height          =   345
            Left            =   3000
            TabIndex        =   7
            Top             =   720
            Width           =   1695
            _ExtentX        =   2990
            _ExtentY        =   609
            _Version        =   393216
            CustomFormat    =   "MMMM yyyy"
            Format          =   68222977
            CurrentDate     =   39153
         End
         Begin VB.Label Lbl_Fecha_Inicial 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Inicial"
            Height          =   345
            Left            =   1800
            TabIndex        =   9
            Top             =   360
            Width           =   1215
         End
         Begin VB.Label Lbl_Fecha_Final 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Final"
            Height          =   345
            Left            =   1800
            TabIndex        =   8
            Top             =   720
            Width           =   1215
         End
      End
      Begin TabDlg.SSTab Tab_Cuadros 
         Height          =   5175
         Left            =   120
         TabIndex        =   15
         Top             =   2160
         Width           =   13335
         _ExtentX        =   23521
         _ExtentY        =   9128
         _Version        =   393216
         Tabs            =   4
         Tab             =   2
         TabsPerRow      =   4
         TabHeight       =   520
         TabCaption(0)   =   "Cuadro N� 1"
         TabPicture(0)   =   "Frm_Reporte_CuadroSVS_APV.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "Grilla_Cuadro1"
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Cuadro N� 2"
         TabPicture(1)   =   "Frm_Reporte_CuadroSVS_APV.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Grilla_Cuadro2"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Cuadro N� 3"
         TabPicture(2)   =   "Frm_Reporte_CuadroSVS_APV.frx":0044
         Tab(2).ControlEnabled=   -1  'True
         Tab(2).Control(0)=   "Grilla_Cuadro3"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Cuadro N' 4"
         TabPicture(3)   =   "Frm_Reporte_CuadroSVS_APV.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "Grilla_Cuadro4"
         Tab(3).ControlCount=   1
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Clientes 
            Height          =   3405
            Left            =   -74880
            TabIndex        =   16
            Top             =   960
            Width           =   12465
            _cx             =   21987
            _cy             =   6006
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   21
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Reporte_CuadroSVS_APV.frx":007C
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin TrueDBList80.TDBCombo Cmb_Clientes 
            Height          =   345
            Left            =   -73335
            TabIndex        =   17
            Tag             =   "OBLI=S;CAPTION=Moneda"
            Top             =   480
            Width           =   4710
            _ExtentX        =   8308
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   -1  'True
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_CuadroSVS_APV.frx":0433
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin MSComctlLib.Toolbar Toolbar_Cliente 
            Height          =   390
            Left            =   -67560
            TabIndex        =   18
            Top             =   480
            Width           =   5010
            _ExtentX        =   8837
            _ExtentY        =   688
            ButtonWidth     =   2752
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Generar Cartola"
                  Key             =   "GENERAR"
                  Description     =   "Generar Cartola Cliente Seleccionado"
                  Object.ToolTipText     =   "Generar Cartola Cliente Seleccionado"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Agregar Cliente"
                  Key             =   "LOAD"
                  Description     =   "Agrega Cliente a Grilla"
                  Object.ToolTipText     =   "Agrega Cliente a Grilla"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Agregar Todos"
                  Key             =   "LOAD_ALL"
                  Description     =   "Agrega Todos los Clientes a la Grilla"
                  Object.ToolTipText     =   "Agrega todos los Clientes a la Grilla"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar4 
               Height          =   255
               Left            =   9420
               TabIndex        =   19
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
            Height          =   3405
            Left            =   -74880
            TabIndex        =   20
            Top             =   960
            Width           =   12465
            _cx             =   21987
            _cy             =   6006
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   22
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Reporte_CuadroSVS_APV.frx":04DD
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin TrueDBList80.TDBCombo Cmb_Cuentas 
            Height          =   345
            Left            =   -73335
            TabIndex        =   21
            Tag             =   "OBLI=S;CAPTION=Moneda"
            Top             =   480
            Width           =   4710
            _ExtentX        =   8308
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   -1  'True
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_CuadroSVS_APV.frx":08C4
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin MSComctlLib.Toolbar Toolbar_Cuentas 
            Height          =   390
            Left            =   -67560
            TabIndex        =   22
            Top             =   480
            Width           =   5010
            _ExtentX        =   8837
            _ExtentY        =   688
            ButtonWidth     =   2752
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Generar Cartola"
                  Key             =   "GENERAR"
                  Description     =   "Generar Cartola Cliente Seleccionado"
                  Object.ToolTipText     =   "Generar Cartola Cliente Seleccionado"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Agregar Cuenta"
                  Key             =   "LOAD"
                  Description     =   "Agrega Cuenta a Grilla"
                  Object.ToolTipText     =   "Agrega Cuenta a Grilla"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Agregar Todos"
                  Key             =   "LOAD_ALL"
                  Description     =   "Agrega Todos los Clientes a la Grilla"
                  Object.ToolTipText     =   "Agrega todos los Clientes a la Grilla"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar1 
               Height          =   255
               Left            =   9420
               TabIndex        =   23
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuadro1 
            Height          =   4545
            Left            =   -74880
            TabIndex        =   24
            Top             =   480
            Width           =   12945
            _cx             =   22834
            _cy             =   8017
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   13
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Reporte_CuadroSVS_APV.frx":096E
            ScrollTrack     =   -1  'True
            ScrollBars      =   0
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuadro2 
            Height          =   4545
            Left            =   -74880
            TabIndex        =   28
            Top             =   480
            Width           =   13065
            _cx             =   23045
            _cy             =   8017
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   11
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Reporte_CuadroSVS_APV.frx":0C9D
            ScrollTrack     =   -1  'True
            ScrollBars      =   0
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuadro3 
            Height          =   4545
            Left            =   120
            TabIndex        =   29
            Top             =   480
            Width           =   13065
            _cx             =   23045
            _cy             =   8017
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   2
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Reporte_CuadroSVS_APV.frx":0F79
            ScrollTrack     =   -1  'True
            ScrollBars      =   0
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuadro4 
            Height          =   4545
            Left            =   -74880
            TabIndex        =   30
            Top             =   480
            Width           =   13065
            _cx             =   23045
            _cy             =   8017
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   2
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Reporte_CuadroSVS_APV.frx":100F
            ScrollTrack     =   -1  'True
            ScrollBars      =   0
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin VB.Label Label1 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Cuenta"
            Height          =   345
            Left            =   -74880
            TabIndex        =   27
            Top             =   495
            Width           =   1515
         End
         Begin VB.Label Label2 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Cliente"
            Height          =   345
            Left            =   -74880
            TabIndex        =   26
            Top             =   495
            Width           =   1515
         End
         Begin VB.Label Label3 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Grupo Cuentas"
            Height          =   345
            Left            =   -74880
            TabIndex        =   25
            Top             =   495
            Width           =   1515
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   13605
      _ExtentX        =   23998
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "SEARCH"
            Object.ToolTipText     =   "Genera reporte cuadros SVS"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORT"
            Object.ToolTipText     =   "Genera reporte cuadros SVS"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
            Object.Width           =   1e-4
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   13800
         TabIndex        =   2
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_CuadroSVS_APV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiqso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana
'----------------------------------------------------------------------------

Public fApp_Excel           As Excel.Application      ' Excel application
Public fLibro               As Excel.Workbook      ' Excel workbook

Dim iFilaExcel          As Integer
Dim iTipoReporte(3)     As Boolean
Dim sFechaDesde         As Date
Dim sFechaHasta         As Date
Dim lCursor_Reporte(3)  As hRecord
Dim sTitulo(3)          As String
Dim mes As Boolean
Dim fFecha_Ini          As Date
Dim fFecha_Fin          As Date
Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  
  Load Me
End Sub

Private Sub Chk_Cuadro1_Click()
    If Chk_Cuadro1.Value = 0 Then
        iTipoReporte(0) = False
    Else
        iTipoReporte(0) = True
    End If
End Sub

Private Sub Chk_Cuadro2_Click()
    If Chk_Cuadro2.Value = 0 Then
        iTipoReporte(1) = False
    Else
        iTipoReporte(1) = True
    End If
End Sub

Private Sub Chk_Cuadro3_Click()
    If Chk_Cuadro3.Value = 0 Then
        iTipoReporte(2) = False
    Else
        iTipoReporte(2) = True
    End If
End Sub

Private Sub Chk_Cuadro4_Click()
    If Chk_Cuadro4.Value = 0 Then
        iTipoReporte(3) = False
    Else
        iTipoReporte(3) = True
    End If
End Sub

Private Sub Form_Load()
   With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SEARCH").Image = cBoton_Buscar
        .Buttons("REPORT").Image = cBoton_Imprimir
        .Buttons("REFRESH").Image = cBoton_Original
        .Buttons("EXIT").Image = cBoton_Salir
   End With
    
   Call Sub_CargaForm
   Me.Top = 1
   Me.Left = 1
   'Call Form_Resize
End Sub

' Sub agregado para formato en mes. Por MEMF.
Private Sub Rdb_Mes_Click()
   mes = True
   DTP_Fecha_Desde.Format = dtpCustom
   DTP_Fecha_Desde.CustomFormat = "MMMM yyyy"
 '  DTP_Fecha_Desde.Value = CDate("01/" & Month(DTP_Fecha_Desde.Value) & "/" & Year(DTP_Fecha_Desde.Value))
   Lbl_Fecha_Inicial.Caption = "Mes Inicial"
   
   DTP_Fecha_Hasta.Format = dtpCustom
   DTP_Fecha_Hasta.CustomFormat = "MMMM yyyy"
'   DTP_Fecha_Hasta.Value = CDate("31/" & Month(DTP_Fecha_Hasta.Value) & "/" & Year(DTP_Fecha_Hasta.Value))
   Lbl_Fecha_Final.Caption = "Mes Final"
   fFecha_Ini = DTP_Fecha_Desde.Value
   fFecha_Fin = DTP_Fecha_Hasta.Value
End Sub
' Sub agregado para formato en rango de d�as. Por MEMF.
Private Sub rdb_RangoFecha_Click()
   mes = False
   DTP_Fecha_Desde.Format = dtpShortDate
   Lbl_Fecha_Inicial.Caption = "Fecha Inicial "
   DTP_Fecha_Hasta.Format = dtpShortDate
   Lbl_Fecha_Final.Caption = "Fecha Final "
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  Select Case Button.Key
    Case "REFRESH"
          Call Sub_Limpiar
    Case "SEARCH"
        If Fnt_CargarDatos Then
           Call Sub_CargarGrilla
        End If
    Case "REPORT"
        If Fnt_CargarDatos Then
           Call Sub_Crea_Informe
        End If
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
Me.SetFocus
  DoEvents
  Select Case ButtonMenu.Key
    Case "SCREEN"
        If Fnt_CargarDatos Then
           Call Sub_Crea_Informe
        End If
    Case "EXCEL"
        If Fnt_CargarDatos Then
          Call Sub_Crea_Excel
       End If
  End Select
End Sub

Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre
Dim i As Integer
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_FormControl_Color(Me.Controls)
    
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Hasta.Value = lCierre.Busca_Ultima_FechaCierre + 1
    DTP_Fecha_Desde.Value = DateAdd("m", -1, lCierre.Busca_Ultima_FechaCierre)
    
    sTitulo(0) = Chk_Cuadro1.Caption
    sTitulo(1) = Chk_Cuadro2.Caption
    sTitulo(2) = Chk_Cuadro3.Caption
    sTitulo(3) = Chk_Cuadro4.Caption
    
    Set lCierre = Nothing

    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Form_Resize()
   Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Function ValidaEntradaDatos() As Boolean
Dim bOk As Boolean
Dim ultimo As Integer

    bOk = True
    If Rdb_Mes.Value = True Then
        DTP_Fecha_Desde.Value = DateSerial(Year(fFecha_Ini), Month(fFecha_Ini) + 0, 1)
        DTP_Fecha_Hasta.Value = DateSerial(Year(fFecha_Fin), Month(fFecha_Fin) + 1, 0)
    End If
    If DTP_Fecha_Desde.Value > DTP_Fecha_Hasta.Value Then
        MsgBox "Fecha Desde debe ser menor a Fecha Hasta", vbInformation, Me.Caption
        bOk = False
    End If
    
    If Chk_Cuadro1.Value = 0 And Chk_Cuadro2.Value = 0 And Chk_Cuadro3.Value = 0 And Chk_Cuadro4.Value = 0 Then
        MsgBox "Debe Seleccionar un Tipo de Reporte", vbInformation, Me.Caption
        bOk = False
    End If
    
    ValidaEntradaDatos = bOk
End Function

Private Sub Sub_Limpiar()
Dim lCierre As Class_Verificaciones_Cierre
Dim i As Integer
    Set lCierre = New Class_Verificaciones_Cierre
    
    DTP_Fecha_Hasta.Value = lCierre.Busca_Ultima_FechaCierre + 1
    DTP_Fecha_Desde.Value = DateAdd("m", -1, lCierre.Busca_Ultima_FechaCierre)
    BarraProceso.Value = 0
    
    Chk_Cuadro1.Value = False
    Chk_Cuadro2.Value = False
    Chk_Cuadro3.Value = False
    Chk_Cuadro4.Value = False
    
    Grilla_Cuadro1.Rows = 1
    Grilla_Cuadro2.Rows = 1
    Grilla_Cuadro3.Rows = 1
    Grilla_Cuadro4.Rows = 1
    
    For i = 0 To 3
        iTipoReporte(i) = False
    Next
End Sub

Private Function Fnt_CargarDatos() As Boolean
Dim CargaExitosa As Boolean
Dim i As Integer

    CargaExitosa = False
    Call Sub_Bloquea_Puntero(Me)
    
    fFecha_Ini = DTP_Fecha_Desde.Value
    fFecha_Fin = DTP_Fecha_Hasta.Value
   
    If Not ValidaEntradaDatos Then
        GoTo ExitProcedure
    End If
    BarraProceso.Value = 0
    
    'Sentencia para cambiar el formato de la fecha si es necesario
    If mes Then
        sFechaDesde = "01-" & CStr(DTP_Fecha_Desde.Month) & "-" & CStr(DTP_Fecha_Desde.Year)
        If DTP_Fecha_Hasta.Month = "2" Then
            If DTP_Fecha_Hasta.Year Mod 4 = 0 Then
                sFechaHasta = "29-" & CStr(DTP_Fecha_Hasta.Month) & "-" & CStr(DTP_Fecha_Hasta.Year)
            Else
                sFechaHasta = "28-" & CStr(DTP_Fecha_Hasta.Month) & "-" & CStr(DTP_Fecha_Hasta.Year)
            End If
        Else
            If DTP_Fecha_Hasta.Month = "1" Or DTP_Fecha_Hasta.Month = "3" Or DTP_Fecha_Hasta.Month = "5" Or DTP_Fecha_Hasta.Month = "7" Or DTP_Fecha_Hasta.Month = "8" Or DTP_Fecha_Hasta.Month = "10" Or DTP_Fecha_Hasta.Month = "12" Then
                sFechaHasta = "31-" & CStr(DTP_Fecha_Hasta.Month) & "-" & CStr(DTP_Fecha_Hasta.Year)
            Else
                sFechaHasta = "30-" & CStr(DTP_Fecha_Hasta.Month) & "-" & CStr(DTP_Fecha_Hasta.Year)
            End If
        End If
    Else
        sFechaDesde = DTP_Fecha_Desde.Value
        sFechaHasta = DTP_Fecha_Hasta.Value
    End If
    ' Fin de sentencia
    
    For i = 0 To 3
        If iTipoReporte(i) Then
            gDB.Parametros.Clear
            gDB.Procedimiento = "PKG_APORTES_RETIROS$ReporteCuadroSVS"
            gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
            gDB.Parametros.Add "pIdTipo_Reporte", ePT_Numero, i, ePD_Entrada
            gDB.Parametros.Add "pFecha_Desde", ePT_Fecha, sFechaDesde, ePD_Entrada
            gDB.Parametros.Add "pFecha_Hasta", ePT_Fecha, sFechaHasta, ePD_Entrada
            gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    
            If gDB.EjecutaSP Then
                Set lCursor_Reporte(i) = gDB.Parametros("Pcursor").Valor
                If lCursor_Reporte(i).Count = 0 Then
                    MsgBox "No se Registra Informaci�n para generar " & sTitulo(i), vbInformation, Me.Caption
                    CargaExitosa = False
                Else
                    BarraProceso.Max = lCursor_Reporte(i).Count
                    CargaExitosa = True
                End If
            Else
                MsgBox "Error al Buscar Informaci�n para Cuadros SVS.", vbInformation, Me.Caption
                CargaExitosa = False
            End If
        End If
    Next
    
ExitProcedure:
   ' If Not CargaExitosa Then
        Call Sub_Desbloquea_Puntero(Me)
   ' End If
    Fnt_CargarDatos = CargaExitosa
End Function
Private Sub Sub_CargarGrilla()
Dim lFila As Integer
Dim i     As Integer
Dim lReg As hFields
Dim lMonto As Double
Dim lTotal_C3 As Double
Dim lTotal_C4 As Double

    Call Sub_Bloquea_Puntero(Me)
    Grilla_Cuadro1.Rows = 1
    Grilla_Cuadro2.Rows = 1
    Grilla_Cuadro3.Rows = 1
    Grilla_Cuadro4.Rows = 1
    lTotal_C3 = 0
    lTotal_C4 = 0
    For i = 0 To 3
        If iTipoReporte(i) Then
            Tab_Cuadros.TabEnabled(i) = True
            For Each lReg In lCursor_Reporte(i)
                Select Case i
                    Case 0
                        lFila = Grilla_Cuadro1.Rows
                        Grilla_Cuadro1.AddItem ""
                        Call SetCell(Grilla_Cuadro1, lFila, "rut_empresa", lReg("rut_empresa").Value)
                        Call SetCell(Grilla_Cuadro1, lFila, "serie", "N/A")
                        Call SetCell(Grilla_Cuadro1, lFila, "total_cuentas_apv", lReg("total_cuentasapv").Value)
                        Call SetCell(Grilla_Cuadro1, lFila, "total_participes", lReg("total_participes").Value)
                        Call SetCell(Grilla_Cuadro1, lFila, "saldo_total", FormatNumber(lReg("monto_saldo").Value, 2))
                        
                        Call SetCell(Grilla_Cuadro1, lFila, "numero_Depositos", lReg("Total_Depositos").Value)
                        lMonto = NVL(lReg("Monto_Depositos").Value, 0) / 1000
                        Call SetCell(Grilla_Cuadro1, lFila, "Monto_Depositos", FormatNumber(lMonto, 2))
                        
                        Call SetCell(Grilla_Cuadro1, lFila, "numero_traspasos_recibidos", lReg("Total_Trasp_Recibidos").Value)
                        lMonto = NVL(lReg("Monto_trasp_recibidos").Value, 0) / 1000
                        Call SetCell(Grilla_Cuadro1, lFila, "Monto_traspasos_recibidos", FormatNumber(lMonto, 2))
                        
                        Call SetCell(Grilla_Cuadro1, lFila, "numero_retiros", lReg("Total_Retiros").Value)
                        lMonto = NVL(lReg("Monto_retiros").Value, 0) / 1000
                        Call SetCell(Grilla_Cuadro1, lFila, "Monto_retiros", FormatNumber(lMonto, 2))
                        
                        Call SetCell(Grilla_Cuadro1, lFila, "numero_traspasos_realizados", lReg("Total_Trasp_Realizados").Value)
                        lMonto = NVL(lReg("Monto_trasp_realizados").Value, 0) / 1000
                        Call SetCell(Grilla_Cuadro1, lFila, "Monto_traspasos_realizados", FormatNumber(lMonto, 2))
                    Case 1
                        lFila = Grilla_Cuadro2.Rows
                        Grilla_Cuadro2.AddItem ""
                        Call SetCell(Grilla_Cuadro2, lFila, "rut_empresa", lReg("rut_empresa").Value)
                        Call SetCell(Grilla_Cuadro2, lFila, "serie", "N/A")
                        Call SetCell(Grilla_Cuadro2, lFila, "total_cuentas_apv", lReg("total_cuentasapv").Value)
                        Call SetCell(Grilla_Cuadro2, lFila, "total_participes", lReg("total_participes").Value)
                        Call SetCell(Grilla_Cuadro2, lFila, "saldo_total", FormatNumber(lReg("monto_saldo").Value, 2))
                        
                        Call SetCell(Grilla_Cuadro2, lFila, "numero_Depositos", NVL(lReg("Total_Depositos").Value, 0))
                        lMonto = NVL(lReg("Monto_Depositos").Value, 0) / 1000
                        Call SetCell(Grilla_Cuadro2, lFila, "Monto_Depositos", FormatNumber(lMonto, 2))
                        
                        Call SetCell(Grilla_Cuadro2, lFila, "numero_traspasos_recibidos", NVL(lReg("Total_Trasp_Recibidos").Value, 0))
                        lMonto = NVL(lReg("Monto_trasp_recibidos").Value, 0) / 1000
                        Call SetCell(Grilla_Cuadro2, lFila, "Monto_traspasos_recibidos", FormatNumber(lMonto, 2))
                        
                        Call SetCell(Grilla_Cuadro2, lFila, "numero_traspasos_realizados", NVL(lReg("Total_Trasp_Realizados").Value, 0))
                        lMonto = NVL(lReg("Monto_trasp_realizados").Value, 0) / 1000
                        Call SetCell(Grilla_Cuadro2, lFila, "Monto_traspasos_realizados", FormatNumber(lMonto, 2))
                    Case 2
                        lFila = Grilla_Cuadro3.Rows
                        Grilla_Cuadro3.AddItem ""
                        Call SetCell(Grilla_Cuadro3, lFila, "institucion", lReg("institucion").Value)
                        lMonto = NVL(lReg("monto_total").Value, 0) / 1000
                        lTotal_C3 = lTotal_C3 + lMonto
                        Call SetCell(Grilla_Cuadro3, lFila, "Monto_Traspasos_Realizados", FormatNumber(lMonto, 2))
                    Case 3
                        lFila = Grilla_Cuadro4.Rows
                        Grilla_Cuadro4.AddItem ""
                        Call SetCell(Grilla_Cuadro4, lFila, "institucion", lReg("institucion").Value)
                        lMonto = NVL(lReg("monto_total").Value, 0) / 1000
                        lTotal_C4 = lTotal_C4 + lMonto
                        Call SetCell(Grilla_Cuadro4, lFila, "Monto_Traspasos_Realizados", FormatNumber(lMonto, 2))
                End Select
            Next
            If i = 2 Then
                lFila = Grilla_Cuadro3.Rows
                Grilla_Cuadro3.AddItem ""
                Call SetCell(Grilla_Cuadro3, lFila, "institucion", "TOTAL")
                Call SetCell(Grilla_Cuadro3, lFila, "Monto_Traspasos_Realizados", FormatNumber(lTotal_C3, 2))
                Grilla_Cuadro3.Cell(flexcpFontBold, lFila, 0, lFila, 1) = True
            End If
            If i = 3 Then
                lFila = Grilla_Cuadro4.Rows
                Grilla_Cuadro4.AddItem ""
                Call SetCell(Grilla_Cuadro4, lFila, "institucion", "TOTAL")
                Call SetCell(Grilla_Cuadro4, lFila, "Monto_Traspasos_Realizados", FormatNumber(lTotal_C4, 2))
                Grilla_Cuadro4.Cell(flexcpFontBold, lFila, 0, lFila, 1) = True
            End If
        End If
    Next
    
    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Sub_Crea_Excel()
Dim i, j, hoja      As Integer
Dim IdTipoReporte   As Integer

    Set fApp_Excel = CreateObject("Excel.application")
    fApp_Excel.DisplayAlerts = False
    Set fLibro = fApp_Excel.Workbooks.Add
    
    fApp_Excel.ActiveWindow.DisplayGridlines = False
    
    For i = 1 To fLibro.Worksheets.Count - 1
           fLibro.Worksheets(i).Delete
    Next
    
    hoja = 0
    
    For i = 0 To 3
        If iTipoReporte(i) Then
            hoja = hoja + 1
            fLibro.Worksheets.Add After:=fLibro.Worksheets(fLibro.Worksheets.Count)
            With fLibro
                .Sheets(hoja).Select
            
                If i = 0 Or i = 1 Then
                    .ActiveSheet.Columns("C:C").ColumnWidth = 8
                    .ActiveSheet.Columns("A:A").ColumnWidth = 10
                    .ActiveSheet.Columns("B:B").ColumnWidth = 4
                    .ActiveSheet.Columns("D:D").ColumnWidth = 6
                    .ActiveSheet.Columns("E:E").ColumnWidth = 10
                Else
                    .ActiveSheet.Columns("A:A").ColumnWidth = 40
                    .ActiveSheet.Columns("B:B").ColumnWidth = 12
                End If
                
                If i = 0 Then
                    .ActiveSheet.Columns("F:F").ColumnWidth = 4
                    .ActiveSheet.Columns("H:H").ColumnWidth = 8
                    .ActiveSheet.Columns("J:J").ColumnWidth = 4
                Else
                    .ActiveSheet.Columns("F:F").ColumnWidth = 8
                    .ActiveSheet.Columns("H:H").ColumnWidth = 8
                    .ActiveSheet.Columns("J:J").ColumnWidth = 8
                End If
        
                .ActiveSheet.Columns("G:G").ColumnWidth = 10
                .ActiveSheet.Columns("I:I").ColumnWidth = 12
                .ActiveSheet.Columns("K:K").ColumnWidth = 12
                .ActiveSheet.Columns("L:L").ColumnWidth = 10
                .ActiveSheet.Columns("M:M").ColumnWidth = 14
                .ActiveSheet.Name = "Cuadro N� " & (i + 1)
            End With
            iFilaExcel = 1
    
            Call Generar_Listado_Excel(i, hoja)
            
        End If
    Next
    fLibro.Worksheets(hoja + 1).Delete
    fApp_Excel.Visible = True
    fApp_Excel.UserControl = True
   
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Generar_Listado_Excel(ByVal i As Integer, ByVal hoja As Integer)
Dim lReg                 As hCollection.hFields
Dim lxHoja               As Worksheet
Dim iColExcel            As Integer
Dim IdTipoReporte        As Integer
Dim Total                As Double
Dim lMonto               As Double
Dim lTotal_C3             As Double
Dim lTotal_C4             As Double
    
    iColExcel = 0
    
    Set lxHoja = fLibro.Worksheets.Item(hoja)
    
   Call ImprimeEncabezadoExcel(i, hoja)
   lTotal_C3 = 0
   lTotal_C4 = 0
   For Each lReg In lCursor_Reporte(i)
        
       BarraProceso.Value = lReg.Index
       iFilaExcel = iFilaExcel + 1
       lxHoja.Rows(iFilaExcel).Font.Size = 7.5
       
       If i = 0 Or i = 1 Then
           iColExcel = iColExcel + 1
           lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("Rut_empresa").Value
           
           iColExcel = iColExcel + 1
           lxHoja.Cells(iFilaExcel, iColExcel).Value = "N/A"
           
           iColExcel = iColExcel + 1
           lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("total_cuentasapv").Value
           lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0" 'Fnt_FormatoNumero(0)
           
           iColExcel = iColExcel + 1
           lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("total_participes").Value
           lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0" 'Fnt_FormatoNumero(0)
           
           iColExcel = iColExcel + 1
           lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("monto_saldo").Value
           lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0.00" 'Fnt_FormatoNumero(2)
           
           iColExcel = iColExcel + 1
           lxHoja.Cells(iFilaExcel, iColExcel).Value = NVL(lReg("Total_Depositos").Value, 0)
           lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0" 'Fnt_FormatoNumero(0)
           
           iColExcel = iColExcel + 1
           lMonto = NVL(lReg("Monto_Depositos").Value, 0) / 1000
           lxHoja.Cells(iFilaExcel, iColExcel).Value = lMonto
           lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0.00" 'Fnt_FormatoNumero(2)

           iColExcel = iColExcel + 1
           lxHoja.Cells(iFilaExcel, iColExcel).Value = NVL(lReg("Total_Trasp_Recibidos").Value, 0)
           lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0" 'Fnt_FormatoNumero(0)

           iColExcel = iColExcel + 1
           lMonto = NVL(lReg("Monto_trasp_recibidos").Value, 0) / 1000
           lxHoja.Cells(iFilaExcel, iColExcel).Value = lMonto
           lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0.00" 'Fnt_FormatoNumero(2)

           If i = 0 Then
               iColExcel = iColExcel + 1
               lxHoja.Cells(iFilaExcel, iColExcel).Value = NVL(lReg("Total_Retiros").Value, 0)
               lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0" 'Fnt_FormatoNumero(0)
    
               iColExcel = iColExcel + 1
               lMonto = NVL(lReg("monto_retiros").Value, 0) / 1000
               lxHoja.Cells(iFilaExcel, iColExcel).Value = lMonto
               lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0.00" 'Fnt_FormatoNumero(2)
           End If
           
           iColExcel = iColExcel + 1
           lxHoja.Cells(iFilaExcel, iColExcel).Value = NVL(lReg("Total_Trasp_Realizados").Value, 0)
           lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0" 'Fnt_FormatoNumero(0)
           
           iColExcel = iColExcel + 1
           lMonto = NVL(lReg("Monto_trasp_realizados").Value, 0) / 1000
           lxHoja.Cells(iFilaExcel, iColExcel).Value = lMonto
           lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0.00" 'Fnt_FormatoNumero(2)
       
       Else
       'CUadros 3 y 4
           iColExcel = iColExcel + 1
           lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("Institucion").Value
           lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin

           iColExcel = iColExcel + 1
           lMonto = NVL(lReg("monto_total").Value, 0) / 1000
           lxHoja.Cells(iFilaExcel, iColExcel).Value = lMonto
           lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0.00" 'Fnt_FormatoNumero(2)
           lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
       
           If i = 2 Then
                lTotal_C3 = lTotal_C3 + NVL(lReg("Monto_Total").Value, 0)
           Else
                lTotal_C4 = lTotal_C4 + NVL(lReg("Monto_Total").Value, 0)
           End If
       End If
       
                  
       iColExcel = 0
   Next
   
   iFilaExcel = iFilaExcel + 1
   
   If i = 2 Or i = 3 Then
       iColExcel = iColExcel + 1
       lxHoja.Rows(iFilaExcel).Font.Size = 7.5
       
       lxHoja.Cells(iFilaExcel, iColExcel).Value = "Total"
       lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
       lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 36
       lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
       
       iColExcel = iColExcel + 1
       If i = 2 Then
            lxHoja.Cells(iFilaExcel, iColExcel).Value = lTotal_C3 / 1000
       Else
            lxHoja.Cells(iFilaExcel, iColExcel).Value = lTotal_C4 / 1000
       End If
       lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0.00" 'Fnt_FormatoNumero(2)
       lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
       lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 36
       lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
   End If
   
End Sub

Private Sub ImprimeEncabezadoExcel(ByVal i As Integer, ByVal hoja As Integer)
Dim lxHoja          As Worksheet
Dim lxfila          As Rows
Dim iColExcel       As Integer
Dim IdTipoReporte   As Integer


    fLibro.Sheets(hoja).Select
    Set lxHoja = fLibro.Worksheets.Item(hoja)
            
    iFilaExcel = iFilaExcel + 1
    lxHoja.Range("A" & CStr(iFilaExcel)).Value = sTitulo(i)
    lxHoja.Range("A" & CStr(iFilaExcel)).Font.ColorIndex = 49
    lxHoja.Range("A" & CStr(iFilaExcel)).Font.Bold = True
    lxHoja.Rows(iFilaExcel).Interior.ColorIndex = 6
    lxHoja.Rows(iFilaExcel).Interior.Pattern = xlSolid
        
       
    iFilaExcel = iFilaExcel + 1
    fLibro.ActiveSheet.Range("A" & CStr(iFilaExcel)).Value = "Nombre de la Instituci�n: VALORES SECURITY S.A. CORREDORES DE BOLSA"
    
    iFilaExcel = iFilaExcel + 2
    fLibro.ActiveSheet.Range("A" & CStr(iFilaExcel)).Value = "Fecha de Consulta: " & sFechaDesde & " al " & sFechaHasta
    
    iFilaExcel = iFilaExcel + 2
    iColExcel = 1
        
    lxHoja.Rows(iFilaExcel).Font.Size = 7.5
    lxHoja.Rows(iFilaExcel).Font.Bold = True
    lxHoja.Rows(iFilaExcel).Font.ColorIndex = 49
    lxHoja.Rows(iFilaExcel).HorizontalAlignment = xlCenter
    lxHoja.Rows(iFilaExcel).VerticalAlignment = xlCenter
    lxHoja.Rows(iFilaExcel).RowHeight = 39
        
    If i = 0 Or i = 1 Then
        lxHoja.Cells(iFilaExcel, iColExcel).Value = "Rut del Fondo"
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
       
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = "Serie"
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
        
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = "N� Cuentas" & Chr(10) & "de APV Vigentes"
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
        
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = "N� de" & Chr(10) & "Participes"
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
        
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = "Saldo Total" & Chr(10) & "Acumulado M$"
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
    Else
        lxHoja.Cells(iFilaExcel, iColExcel).Value = "Instituci�n"
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
        
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = "Miles $"
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
        lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
    
    End If
    Select Case i
           Case 0
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "N� de " & Chr(10) & "Dep�sitos"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
               
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "Monto Total de " & Chr(10) & "Dep�sitos"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
                              
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "N� de " & Chr(10) & "Traspasos" & Chr(10) & "Recibidos"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
               
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "Monto Total de " & Chr(10) & "Traspasos" & Chr(10) & "Recibidos M$"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
                                             
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "N� de " & Chr(10) & "Retiros"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
                
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "Monto Total de " & Chr(10) & "Retiros"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
               
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "N� de " & Chr(10) & "Traspasos" & Chr(10) & "Realizados"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
               
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "Monto Total de " & Chr(10) & "Traspasos" & Chr(10) & "Realizados M$"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
                
           Case 1
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "N� de " & Chr(10) & "Dep�sitos" & Chr(10) & "Convenidos"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
               
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "Monto Total de " & Chr(10) & "Dep�sitos" & Chr(10) & "Convenidos"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
                              
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "N� de " & Chr(10) & "Traspasos" & Chr(10) & "Recibidos"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
               
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "Monto Total de " & Chr(10) & "Traspasos" & Chr(10) & "Recibidos M$"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
                                             
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "N� de " & Chr(10) & "Traspasos" & Chr(10) & "Realizados"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
               
                iColExcel = iColExcel + 1
                lxHoja.Cells(iFilaExcel, iColExcel).Value = "Monto Total de " & Chr(10) & "Traspasos" & Chr(10) & "Realizados M$"
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.ColorIndex = 15
                lxHoja.Cells(iFilaExcel, iColExcel).Interior.Pattern = xlSolid
    End Select
    
End Sub

Private Sub Sub_Crea_Informe()
Const clrHeader = &HD0D0D0
Dim sHeader_Clt             As String
Dim sFormat_Clt             As String
Dim lForm                   As Frm_Reporte_Generico
Dim sRecord
Dim bAppend

Dim lReg                    As hCollection.hFields
Dim lLinea                  As Integer
Dim IdTipoReporte           As Integer
Dim Total                   As Double
Dim lMonto                  As Double
Dim lMonto_dep              As Double
Dim lMonto_trasp_rec        As Double
Dim lMonto_ret              As Double
Dim lMonto_trasp_realiz     As Double
Dim i, j                    As Integer
Dim flag                    As Boolean
Dim lTotal_C3               As Double
Dim lTotal_C4               As Double

'    If Not Fnt_Form_Validar(Me.Controls) Then
'       Exit Sub
'    End If
    lTotal_C3 = 0
    lTotal_C4 = 0
    flag = True
    For i = 0 To 3
        If iTipoReporte(i) Then
            If lCursor_Reporte(i).Count > 0 Then
       
                If flag Then
                    Set lForm = New Frm_Reporte_Generico
            
                    Call lForm.Sub_InicarDocumento(pTitulo:="Cuadro SVS" _
                                                 , pTipoSalida:=ePrinter.eP_Pantalla _
                                                 , pOrientacion:=orLandscape)
                Else
                    lForm.VsPrinter.FontSize = 14
                    lForm.VsPrinter.FontBold = True
                    lForm.VsPrinter.Paragraph = "Cuadro SVS"
                End If
      
                lForm.VsPrinter.FontSize = 8
                lForm.VsPrinter.Paragraph = sTitulo(i)
                lForm.VsPrinter.FontBold = True
                lForm.VsPrinter.Paragraph = "Nombre de la Instituci�n: VALORES SECURITY S.A. CORREDORES DE BOLSA"
                lForm.VsPrinter.Paragraph = ""
                lForm.VsPrinter.Paragraph = "Fecha de Consulta: " & sFechaDesde & " al " & sFechaHasta
                lForm.VsPrinter.Paragraph = "" 'salto de linea
                lForm.VsPrinter.FontBold = False
       
                Select Case i
                    Case 0
               'Se modifica formato de encabezados. Por MMF el 01-06-2010
                        sHeader_Clt = "Rut del Fondo|Serie|N� Cuentas APV|N�Participes|Saldo Total Acumulado M$|N� de Dep�sitos|Monto Total de Dep�sitos|N� de Traspasos Recibidos|Monto Total de Traspasos Recibidos M$|N� de Retiros|Monto Total de Retiros|N� de Traspasos Realizados|Monto Total de Traspasos Realizados M$"
                        sFormat_Clt = ">1000|500|>800|>800|>1300|>800|>1100|>850|>1100|>1100|>1100|>1100|>1100"
                    Case 1
                        sHeader_Clt = "Rut del Fondo|Serie|N�mero Cuentas APV|N�mero Participes|Saldo Total Acumulado M$|N�mero de Dep�sitos Convenidos|Monto Total de Dep�sitos Convenidos|N�mero de Traspasos Recibidos|Monto Total de Traspasos Recibidos M$|N�mero de Traspasos Realizados|Monto Total de Traspasos Realizados M$"
                        sFormat_Clt = ">1000|500|>1000|>1000|>1300|>1300|>1300|>1300|>1300|>1300|>1300"
                    Case Else
                        sHeader_Clt = "Instituci�n|Miles $"
                        sFormat_Clt = "4000|>1300"

                End Select
     
                lForm.VsPrinter.StartTable
                lForm.VsPrinter.FontSize = 7
                lForm.VsPrinter.TableCell(tcAlignCurrency) = False

                For Each lReg In lCursor_Reporte(i)
                    BarraProceso.Value = lReg.Index
           
                    Select Case i
                        Case 0
                            lMonto_dep = NVL(lReg("Monto_Depositos").Value, 0) / 1000
                            lMonto_ret = NVL(lReg("Monto_Retiros").Value, 0) / 1000
                            lMonto_trasp_rec = NVL(lReg("Monto_Trasp_Recibidos").Value, 0) / 1000
                            lMonto_trasp_realiz = NVL(lReg("Monto_Trasp_Realizados").Value, 0) / 1000
                            sRecord = NVL(lReg("Rut_empresa").Value, 0) & "|" & _
                                      "N/A" & "|" & _
                                      FormatNumber(NVL(lReg("total_cuentasapv").Value, 0), 0) & "|" & _
                                      FormatNumber(NVL(lReg("total_participes").Value, 0), 0) & "|" & _
                                      FormatNumber(NVL(lReg("monto_saldo").Value, 0), 2) & "|" & _
                                      FormatNumber(NVL(lReg("total_Depositos").Value, 0), 2) & "|" & _
                                      FormatNumber(lMonto_dep, 2) & "|" & _
                                      FormatNumber(NVL(lReg("total_Trasp_Recibidos").Value, 0), 2) & "|" & _
                                      FormatNumber(lMonto_trasp_rec, 2) & "|" & _
                                      FormatNumber(NVL(lReg("total_Retiros").Value, 0), 2) & "|" & _
                                      FormatNumber(lMonto_ret, 2) & "|" & _
                                      FormatNumber(NVL(lReg("total_Trasp_Realizados").Value, 0), 2) & "|" & _
                                      FormatNumber(lMonto_trasp_realiz, 2)
                        Case 1
                            lMonto_dep = NVL(lReg("Monto_Depositos").Value, 0) / 1000
                            lMonto_trasp_rec = NVL(lReg("Monto_Trasp_Recibidos").Value, 0) / 1000
                            lMonto_trasp_realiz = NVL(lReg("Monto_Trasp_Realizados").Value, 0) / 1000
                            sRecord = NVL(lReg("Rut_empresa").Value, 0) & "|" & _
                                      "N/A" & "|" & _
                                      FormatNumber(NVL(lReg("total_cuentasapv").Value, 0), 0) & "|" & _
                                      FormatNumber(NVL(lReg("total_participes").Value, 0), 0) & "|" & _
                                      FormatNumber(NVL(lReg("monto_saldo").Value, 0), 2) & "|" & _
                                      FormatNumber(NVL(lReg("total_Depositos").Value, 0), 2) & "|" & _
                                      FormatNumber(lMonto_dep, 2) & "|" & _
                                      FormatNumber(NVL(lReg("total_Trasp_Recibidos").Value, 0), 2) & "|" & _
                                      FormatNumber(lMonto_trasp_rec, 2) & "|" & _
                                      FormatNumber(NVL(lReg("total_Trasp_Realizados").Value, 0), 2) & "|" & _
                                      FormatNumber(lMonto_trasp_realiz, 2)
                        Case Else
                            lMonto = NVL(lReg("Monto_Total").Value, 0) / 1000
                            sRecord = NVL(lReg("Institucion").Value, 0) & "|" & _
                                      FormatNumber(lMonto, 2)
                            If i = 2 Then
                                lTotal_C3 = (lTotal_C3 + NVL(lReg("Monto_Total").Value, 0))
                            End If
                            If i = 3 Then
                                lTotal_C4 = (lTotal_C4 + NVL(lReg("Monto_Total").Value, 0))
                            End If
                    End Select
            
                    lForm.VsPrinter.AddTable sFormat_Clt, sHeader_Clt, sRecord, clrHeader, , bAppend

                Next
                If i = 2 Then
                    sRecord = "Total" & "|" & _
                    FormatNumber(lTotal_C3 / 1000, 2)
                    lForm.VsPrinter.AddTable sFormat_Clt, sHeader_Clt, sRecord, clrHeader, , bAppend
                End If
                If i = 3 Then
                    sRecord = "Total" & "|" & _
                    FormatNumber(lTotal_C4 / 1000, 2)
                    lForm.VsPrinter.AddTable sFormat_Clt, sHeader_Clt, sRecord, clrHeader, , bAppend
                End If
                    
                lForm.VsPrinter.TableCell(tcFontBold, 0) = True
                lForm.VsPrinter.EndTable
                
                For j = i To 3
                    If j <> 3 Then
                        If iTipoReporte(j + 1) Then
                            lForm.VsPrinter.NewPage
                            j = 3
                            flag = False
                        End If
                    End If
                Next
            End If
        End If
    Next
    lForm.VsPrinter.EndDoc
    
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Public Function Fnt_FormatoNumero(pDecimales) As String
Dim lFormato  As String
Dim lNum      As Integer
Dim lSepDecimal As String
Dim lSepMiles   As String

    lSepDecimal = Fnt_CualASCIIDecimal
    lSepMiles = Fnt_CualASCIISeparadorMiles
    lFormato = "#" & lSepMiles & "##0"
    If pDecimales > 0 Then
        lFormato = lFormato & lSepDecimal
        For lNum = 1 To pDecimales '- 1
            lFormato = lFormato & "0"
        Next
    End If
    Fnt_FormatoNumero = lFormato
End Function

Public Function Fnt_CualASCIIDecimal()
Dim lNum_String As String

  lNum_String = Format(3.5, "0.0")

  Fnt_CualASCIIDecimal = Mid(lNum_String, 2, 1)
End Function


Public Function Fnt_CualASCIISeparadorMiles()
Dim lNum_String As String

  lNum_String = Format(1000, "0,000")

  Fnt_CualASCIISeparadorMiles = Mid(lNum_String, 2, 1)
End Function


