VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Suscripcion_Opciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Suscripci�n de Acciones"
   ClientHeight    =   5295
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14745
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5295
   ScaleWidth      =   14745
   Begin VB.Frame Frame1 
      Caption         =   "Opciones en Custodia"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4635
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   14520
      Begin MSComCtl2.DTPicker DTP_Fecha_Suscripcion 
         Height          =   315
         Left            =   1920
         TabIndex        =   4
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   17367041
         CurrentDate     =   39609
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3555
         Left            =   120
         TabIndex        =   1
         Top             =   840
         Width           =   14265
         _cx             =   25162
         _cy             =   6271
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   19
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Suscripcion_Opciones.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin TrueDBList80.TDBCombo Cmb_Nemotecnico 
         Height          =   345
         Left            =   6480
         TabIndex        =   7
         Tag             =   "OBLI=S;CAPTION=Pa�s"
         Top             =   360
         Width           =   5985
         _ExtentX        =   10557
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Suscripcion_Opciones.frx":031B
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Lbl_Nemotecnico 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Nemot�cnico"
         Height          =   345
         Left            =   5040
         TabIndex        =   6
         Top             =   360
         Width           =   1425
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Suscripci�n"
         Height          =   315
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   1695
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   14745
      _ExtentX        =   26009
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Buscar"
            Key             =   "BUSCAR"
            Object.ToolTipText     =   "Buscar Opciones"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Suscripcion_Opciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fId_Nemotecnico As String

Private Enum eNem_Colum
  eNem_nemotecnico
  eNem_Descripcion
  eNem_Id_Nemotecnico
End Enum

Private Sub Form_Load()
Dim lCierre As Class_Verificaciones_Cierre

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SAVE").Image = cBoton_Grabar
        .Buttons("EXIT").Image = cBoton_Salir
        .Buttons("BUSCAR").Image = cBoton_Buscar
        .Buttons("REFRESH").Image = cBoton_Original
        .Buttons("SAVE").Enabled = False
  End With
  
  Set lCierre = New Class_Verificaciones_Cierre
  
  DTP_Fecha_Suscripcion = lCierre.Busca_Ultima_FechaCierre
  Grilla.Rows = 1
  'Call Sub_Carga_Combo_Nemotecnico

  Me.Top = 1
  Me.Left = 1
  Call Form_Resize
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Call Guarda_Acciones_Opciones
    Case "BUSCAR"
      Call Sub_Importa_Opciones_Fecha
      Call Activa_Grabar
    Case "REFRESH"
        Call Limpia_controles
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Cmb_Nemotecnico_GotFocus()
  Call Sub_Carga_Combo_Nemotecnico
  'Call Sub_ComboSelectedItem(Cmb_Nemotecnico, "")
End Sub

Private Sub Cmb_Nemotecnico_ItemChange()
  fId_Nemotecnico = Fnt_FindValue4Display(Cmb_Nemotecnico, Cmb_Nemotecnico.Text)
End Sub

Private Sub DTP_Fecha_Suscripcion_Change()
  Grilla.Rows = 1
'    Call Sub_Carga_Combo_Nemotecnico
End Sub

Private Sub Grilla_EnterCell()
  Select Case Grilla.Col
    Case 1, 5
      Grilla.Editable = flexEDKbdMouse
    Case Else
      Grilla.Editable = flexEDNone
  End Select
End Sub

Private Sub Grilla_CellChanged(ByVal Row As Long, ByVal Col As Long)
Dim nNum_Cuenta As Integer
    
  Select Case Col
    Case 5
      If CDbl(NVL(GetCell(Grilla, Row, "cantidad_original"), 0)) >= CDbl(NVL(GetCell(Grilla, Row, "cantidad"), 0)) Then
        Call SetCell(Grilla, Row, "monto", NVL(GetCell(Grilla, Row, "precio"), 0) * NVL(GetCell(Grilla, Row, "cantidad"), 0), pAutoSize:=False)
      Else
        MsgBox "La cantidad a suscribir NO puede superar la cantidad pactada" '& GetCell(Grilla, Row, "cantidad_original")
        Call SetCell(Grilla, Row, "cantidad", NVL(GetCell(Grilla, Row, "cantidad_original"), 0), pAutoSize:=False)
      End If
  End Select
End Sub

Private Sub Sub_Carga_Combo_Nemotecnico()
Dim lcursor As hRecord
Dim lReg As hFields

  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "pFECHA", ePT_Fecha, CDate(DTP_Fecha_Suscripcion.Value), ePD_Entrada
  gDB.Parametros.Add "pID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
  gDB.Procedimiento = "PKG_NEMOTECNICOS$Buscar_OSA"

  If gDB.EjecutaSP Then
    Set lcursor = gDB.Parametros("pcursor").Valor
  Else
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
    Exit Sub
  End If
  
  If lcursor.Count = 0 Then
    MsgBox "No se registran Opciones para esta fecha", vbInformation, Me.Caption
  Else
    Call Sub_LimpiarTDBCombo(Cmb_Nemotecnico)
    With Cmb_Nemotecnico
      With .Columns.Add(eNem_nemotecnico)
        .Caption = "Nemotecnico"
        .Visible = True
      End With
      With .Columns.Add(eNem_Descripcion)
        .Caption = "Descripcion"
        .Visible = True
      End With
      With .Columns.Add(eNem_Id_Nemotecnico)
        .Caption = "id_nemotecnico"
        .Visible = False
      End With
      Call .AddItem("Todos")
      .Columns(0).ValueItems.Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      For Each lReg In lcursor
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
        Call .AddItem(lReg("nemotecnico").Value & ";" & lReg("dsc_nemotecnico").Value & ";" & lReg("id_nemotecnico").Value)
        .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("id_nemotecnico").Value, lReg("nemotecnico").Value)
        Next
      End With
  End If
End Sub

Private Sub Sub_Importa_Opciones_Fecha()
Dim lcursor As hRecord
Dim lReg As hFields
Dim lLinea

  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  If Cmb_Nemotecnico.Text <> "Todos" Then
    gDB.Parametros.Add "Pid_nemotecnico", ePT_Numero, fId_Nemotecnico, ePD_Entrada
  End If
  gDB.Parametros.Add "Pfecha", ePT_Fecha, CDate(DTP_Fecha_Suscripcion.Value), ePD_Entrada
  gDB.Parametros.Add "Pid_empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
  gDB.Procedimiento = "PKG_DIVIDENDOS$BUSCA_OPCIONES_ACCIONES"

  If gDB.EjecutaSP Then
    Set lcursor = gDB.Parametros("pcursor").Valor
  Else
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
    Exit Sub
  End If
    
  'Grilla.Clear
  If lcursor.Count = 0 Then
    MsgBox "No se registran movimientos para esta fecha", vbInformation, Me.Caption
    Exit Sub
  Else
    Grilla.Rows = 1
    For Each lReg In lcursor
      lLinea = Grilla.Rows
      If CDbl(lReg("cantidad").Value) > 0 Then
        Call Grilla.AddItem("")
        Call SetCell(Grilla, lLinea, "id_dividendo", Trim(lReg("id_dividendo").Value), pAutoSize:=False)
        Call SetCell(Grilla, lLinea, "num_cuenta", lReg("num_cuenta").Value, pAutoSize:=False)
        Call SetCell(Grilla, lLinea, "dsc_cuenta", NVL(lReg("dsc_cuenta").Value, 0), pAutoSize:=False)
        Call SetCell(Grilla, lLinea, "precio", NVL(lReg("monto").Value, 0), pAutoSize:=False)
        Call SetCell(Grilla, lLinea, "dsc_nemotecnico", NVL(lReg("nemotecnico").Value, 0), pAutoSize:=False)
        Call SetCell(Grilla, lLinea, "saldo_custodia", NVL(lReg("saldo_custodia").Value, 0), pAutoSize:=False)
        Call SetCell(Grilla, lLinea, "cantidad_original", NVL(lReg("cantidad").Value, 0), pAutoSize:=False)
        Call SetCell(Grilla, lLinea, "cantidad", NVL(lReg("cantidad").Value, 0), pAutoSize:=False)
        
        Call SetCell(Grilla, lLinea, "cada_x_cantidad", NVL(lReg("cada_x_cantidad").Value, 0), pAutoSize:=False)
        Call SetCell(Grilla, lLinea, "entregar_x_cantidad", NVL(lReg("entregar_x_cantidad").Value, 0), pAutoSize:=False)
        
        Call SetCell(Grilla, lLinea, "id_cuenta", NVL(lReg("id_cuenta").Value, 0), pAutoSize:=False)
        Call SetCell(Grilla, lLinea, "id_nemotecnico", NVL(lReg("id_nemotecnico").Value, 0), pAutoSize:=False)
        Call SetCell(Grilla, lLinea, "id_nemotecnico_opcion", NVL(lReg("id_nemotecnico_opcion").Value, 0), pAutoSize:=False)
        
        Call SetCell(Grilla, lLinea, "id_moneda_nemotecnico", NVL(lReg("id_moneda_nemotecnico").Value, 0), pAutoSize:=False)
        Call SetCell(Grilla, lLinea, "ID_OPERACION_CUSTODIA", NVL(lReg("ID_OPERACION_CUSTODIA").Value, 0), pAutoSize:=False)
      End If
    Next
  End If
End Sub

Private Sub Limpia_controles()
  Grilla.Rows = 1
  Toolbar.Buttons("SAVE").Enabled = False
End Sub

Private Sub Activa_Grabar()
  If Grilla.Rows > 1 Then
    Toolbar.Buttons("SAVE").Enabled = True
  End If
End Sub

Private Sub Guarda_Acciones_Opciones()
Dim lFila As Long
Dim lClass_Acciones As Class_Acciones
'----------------------------------------
Dim pId_Cuenta As Long
Dim pId_Contraparte
Dim pId_Representante
Dim pId_Moneda_Operacion
Dim pFecha_Operacion
Dim pFecha_Vigencia
Dim pFecha_Liquidacion
Dim pId_Trader
Dim pPorc_Comision
Dim pComision
Dim pDerechos_Bolsa
Dim pGastos
Dim pIva
Dim pMonto_Operacion
Dim pTipo_Precio
Dim pId_Caja_Cuenta
Dim pChkAporteRetiro
Dim sMensaje As String
'----------------------------------------
  Set lClass_Acciones = New Class_Acciones

  With Grilla
    For lFila = 1 To .Rows - 1
      sMensaje = ""
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked Then
        sMensaje = ""
        If Fnt_Grabar(lFila, sMensaje) Then
          If Fnt_Quitar_Custodia(lFila) Then
            sMensaje = "Operaci�n Realizada Correctamente"
            .Cell(flexcpBackColor, lFila, 0, , .Cols - 1) = vbGreen
            MsgBox "Operaci�n Ingresada Exitosamente", , "Suscripci�n de Acciones"
          Else
            sMensaje = "Operaci�n no realizada"
            .Cell(flexcpBackColor, lFila, 0, , .Cols - 1) = vbRed
          End If
          Call SetCell(Grilla, lFila, "mensaje", sMensaje)
          Call Sub_Importa_Opciones_Fecha
        Else
          Call SetCell(Grilla, lFila, "mensaje", sMensaje)
          .Cell(flexcpBackColor, lFila, 0, , .Cols - 1) = vbRed
        End If
      End If
    Next
  End With
End Sub

Private Function Fnt_Quitar_Custodia(Row As Long) As Boolean
Dim lId_Cuenta As Long
Dim lId_Nemotecnico As Long
Dim lAccion As Class_Acciones
Dim lLinea As Long
Dim lId_Contraparte As String
Dim lTipo_Precio As String
Dim lId_representante As String
Dim lFecha_Operacion  As Date
Dim lFecha_Vigencia As Date
Dim lFecha_Liquidacion As Date
Dim lPrecio_Historico As String
Dim lId_Trader As String
'---------------------------------
Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc
'Dim lId_Nemotecnico As String
'---------------------------------
Dim lId_Caja_Cuenta As Double
Dim lNum_Error      As Double
'---------------------------------
Dim lRollback As Boolean
'------------------------------------------
'Dim lId_Cuenta As Long
'Dim lId_Nemotecnico As Long
Dim lId_Moneda_Nemo As Long
Dim lCod_Instrumento As String
Dim lOperacion As String
Dim lId_Operacion As String
Dim lTipo_Operacion As String
Dim lCod_Mercado As String
Dim lMonto As Double
Dim lFecha_Suscripcion As Date

Dim lPorc_Comision As Double
Dim lComision As Double
Dim lDerechos As Double
Dim lIva As Double
Dim lGastos As Double
Dim lResult As Boolean
'------------------------------------------
Dim sChkAporteRetiro        As String

  sChkAporteRetiro = "NO"
    
  lId_Cuenta = GetCell(Grilla, Row, "id_cuenta")
  lId_Nemotecnico = GetCell(Grilla, Row, "id_nemotecnico_opcion")
  lId_Moneda_Nemo = GetCell(Grilla, Row, "id_moneda_nemotecnico")
  lMonto = 0 ' GetCell(Grilla, Row, "monto")
  lCod_Instrumento = gcINST_ACCIONES_NAC
  lOperacion = gcTipoOperacion_Egreso
  lTipo_Operacion = gcOPERACION_Custodia_SusCrip
  lCod_Mercado = "N"
  lFecha_Suscripcion = CDate(DTP_Fecha_Suscripcion.Value)
  lComision = 0
    
  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
    
  gDB.IniciarTransaccion
    
  lRollback = True

  If lOperacion = gcTipoOperacion_Ingreso Then
    If Not Fnt_Valida_Perfil_Cuenta_Instrm_Nemo(Grilla, lId_Cuenta, lCod_Instrumento) Then
      GoTo ErrProcedure
    End If
    
    Select Case lTipo_Operacion
      Case gcOPERACION_Instruccion, gcOPERACION_Directa
        lId_Caja_Cuenta = Fnt_CheckeaFinanciamiento(pId_Cuenta:=lId_Cuenta _
                                                  , pCod_Mercado:=lCod_Mercado _
                                                  , pMonto:=lMonto _
                                                  , pId_Moneda:=lId_Moneda_Nemo _
                                                  , pFecha_Liquidacion:=lFecha_Suscripcion _
                                                  , pNum_Error:=lNum_Error)
                                                       
        'VERITICA EL RESULTADO DE LA OPERACION
        Select Case lNum_Error
          Case 0, eFinanciamiento_Caja.eFC_InversionDescubierta
              'SI SON ESTOS VALORES SIGNIFICA QUE LA OPERACION SE PUEDE REALIZAR
          Case Else
              'Si el financiamiento tubo problemas
            GoTo ErrProcedure
        End Select
        Case gcOPERACION_Custodia, gcOPERACION_Custodia_SusCrip
          lId_Caja_Cuenta = Fnt_ValidarCaja(lId_Cuenta, lCod_Mercado, lId_Moneda_Nemo)
          If lId_Caja_Cuenta = cNewEntidad Then
            'Si el financiamiento tubo problemas
            GoTo ErrProcedure
          End If
    End Select
  Else
    'Si es una venta
    lId_Caja_Cuenta = Fnt_ValidarCaja(lId_Cuenta, lCod_Mercado, lId_Moneda_Nemo)
    If lId_Caja_Cuenta < 0 Then
      'Significa que hubo problema con la busqueda de la caja
      GoTo ErrProcedure
    End If
  End If
  
  Set lAccion = New Class_Acciones
  If lTipo_Operacion = gcOPERACION_Custodia And lOperacion = gcTipoOperacion_Ingreso Then
    lPrecio_Historico = GetCell(Grilla, lLinea, "precio_historico")
  Else
    lPrecio_Historico = ""
  End If

  Call lAccion.Agregar_Operaciones_Detalle(GetCell(Grilla, Row, "id_nemotecnico_opcion"), _
                                           GetCell(Grilla, Row, "cantidad"), _
                                           0, _
                                           GetCell(Grilla, Row, "id_moneda_nemotecnico"), _
                                           0, _
                                           "N", _
                                           lPrecio_Historico)
                                         
  lId_Contraparte = "" 'Fnt_ComboSelected_KEY(Cmb_Contraparte)
  lId_representante = "" 'Fnt_ComboSelected_KEY(Cmb_Representantes)
  lFecha_Operacion = lFecha_Suscripcion ' DTP_Fecha_Operacion.Value ' Fnt_FechaServidor
  lId_Trader = "" 'Fnt_ComboSelected_KEY(Cmb_Traders)
  lFecha_Vigencia = lFecha_Suscripcion ' '+ To_Number(Txt_DiasVigencia.Text)
  lFecha_Liquidacion = lFecha_Suscripcion '
  
  lTipo_Precio = ""

  Select Case lTipo_Operacion
    Case gcOPERACION_Custodia_SusCrip
      If Not lAccion.Realiza_Operacion_Custodia_Opciones(pId_Operacion:=lId_Operacion, _
                                                         pId_Cuenta:=lId_Cuenta, _
                                                         pDsc_Operacion:="", _
                                                         pTipoOperacion:=lOperacion, _
                                                         pId_Contraparte:=lId_Contraparte, _
                                                         pId_Representante:=lId_representante, _
                                                         pId_Moneda_Operacion:=lId_Moneda_Nemo, _
                                                         pFecha_Operacion:=lFecha_Operacion, _
                                                         pFecha_Vigencia:=lFecha_Vigencia, _
                                                         pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                         pId_Trader:=lId_Trader, _
                                                         pPorc_Comision:=lPorc_Comision, _
                                                         pComision:=lComision, _
                                                         pDerechos_Bolsa:=lDerechos, _
                                                         pGastos:=lGastos, _
                                                         pIva:=lIva, _
                                                         pMonto_Operacion:=lMonto, _
                                                         pTipo_Precio:=lTipo_Precio, _
                                                         pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
                                                
        Call Fnt_MsgError(lAccion.SubTipo_LOG, _
                          "Problemas en grabar Acciones.", _
                          lAccion.ErrMsg, _
                          pConLog:=True)
        GoTo ErrProcedure
      End If
    Case Else
      MsgBox "Operacion no reconocida para operar.", vbCritical, Me.Caption
      GoTo ErrProcedure
  End Select

  lRollback = False
  'Fnt_Grabar = True

ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    lResult = False
  Else
    gDB.CommitTransaccion
    lResult = True
    'Call Fnt_EnvioEMAIL_Trader(fId_Operacion)
    'Call Fnt_EnvioContraparte(fId_Operacion)
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
  Fnt_Quitar_Custodia = lResult
End Function

Private Function Fnt_Grabar(Row As Long, ByRef pMensaje As String) As Boolean
Dim lAccion As Class_Acciones
Dim lLinea As Long
Dim lId_Contraparte As String
Dim lTipo_Precio As String
Dim lId_representante As String
Dim lFecha_Operacion  As Date
Dim lFecha_Vigencia As Date
Dim lFecha_Liquidacion As Date
Dim lPrecio_Historico As String
Dim lId_Trader As String
'---------------------------------
Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc
Dim lId_Nemotecnico As String
'---------------------------------
Dim lId_Caja_Cuenta As Double
Dim lNum_Error      As Double
'---------------------------------
Dim lRollback As Boolean
'------------------------------------------
Dim lId_Cuenta As Long
'Dim lId_Nemotecnico As Long
Dim lId_Moneda_Nemo As Long
Dim lCod_Instrumento As String
Dim lOperacion As String
Dim lTipo_Operacion As String
Dim lCod_Mercado As String
Dim lMonto As Double
Dim lFecha_Suscripcion As Date

Dim lPorc_Comision As Double
Dim lComision As Double
Dim lDerechos As Double
Dim lIva As Double
Dim lGastos As Double
Dim lId_Operacion
'------------------------------------------
Dim sChkAporteRetiro        As String
Dim lOperaciones As Class_Operaciones

  sChkAporteRetiro = "NO"

  lId_Cuenta = GetCell(Grilla, Row, "id_cuenta")
  lId_Nemotecnico = GetCell(Grilla, Row, "id_nemotecnico")
  lId_Moneda_Nemo = GetCell(Grilla, Row, "id_moneda_nemotecnico")
  lMonto = GetCell(Grilla, Row, "monto")
  lCod_Instrumento = gcINST_ACCIONES_NAC
  lOperacion = gcTipoOperacion_Ingreso
  lTipo_Operacion = gcOPERACION_Directa_Opciones
  lCod_Mercado = "N"
  lFecha_Suscripcion = CDate(DTP_Fecha_Suscripcion.Value)
  lComision = 0

  'sChkAporteRetiro = IIf(chkAporteRetiro.Value = 0, "NO", "SI")

  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
  
  gDB.IniciarTransaccion
  
  lRollback = True
  Fnt_Grabar = False

'  If Not Fnt_ValidarDatos Then
'    GoTo ErrProcedure
'  End If

  If lOperacion = gcTipoOperacion_Ingreso Then
    Rem Si es una compra
    Rem Validar si el Instrumento y Nemot�cnicos est�n relacionados al perfil de riesgo que le corresponde a la cuenta
    If Not Fnt_Valida_Perfil_Cuenta_Instrm_Nemo(Grilla, lId_Cuenta, lCod_Instrumento) Then
      GoTo ErrProcedure
    End If
    
    
    Set lOperaciones = New Class_Operaciones
    
    Select Case lTipo_Operacion
      Case gcOPERACION_Directa_Opciones
        lId_Caja_Cuenta = Fnt_CheckeaFinanciamiento(pId_Cuenta:=lId_Cuenta _
                                                  , pCod_Mercado:=lCod_Mercado _
                                                  , pMonto:=lMonto _
                                                  , pId_Moneda:=lId_Moneda_Nemo _
                                                  , pFecha_Liquidacion:=lFecha_Suscripcion _
                                                  , pNum_Error:=lNum_Error)
                                                   
        'VERITICA EL RESULTADO DE LA OPERACION
        Select Case lNum_Error
          Case 0, eFinanciamiento_Caja.eFC_InversionDescubierta
            'SI SON ESTOS VALORES SIGNIFICA QUE LA OPERACION SE PUEDE REALIZAR
            pMensaje = ""
          Case eFinanciamiento_Caja.eFC_NoAlcanza
            pMensaje = "No alcanza para financiar, Decide No Invertir"
            GoTo ErrProcedure
          Case Else
            'Si el financiamiento tubo problemas
            pMensaje = "Problemas en el financiamiento"
            GoTo ErrProcedure
        End Select
      Case gcOPERACION_Custodia, gcOPERACION_Custodia_SusCrip
        lId_Caja_Cuenta = Fnt_ValidarCaja(lId_Cuenta, lCod_Mercado, lId_Moneda_Nemo)
        If lId_Caja_Cuenta = cNewEntidad Then
          'Si el financiamiento tubo problemas
          GoTo ErrProcedure
        End If
    End Select
  Else
    'Si es una venta
    lId_Caja_Cuenta = Fnt_ValidarCaja(lId_Cuenta, lCod_Mercado, lId_Moneda_Nemo)
    If lId_Caja_Cuenta < 0 Then
      'Significa que hubo problema con la busqueda de la caja
      pMensaje = "Problemas en b�squeda de Caja Cuenta"
      GoTo ErrProcedure
    End If
  End If
  
  Set lAccion = New Class_Acciones
  'For lLinea = 1 To (Grilla.Rows - 1)
    If lTipo_Operacion = gcOPERACION_Custodia And lOperacion = gcTipoOperacion_Ingreso Then
      lPrecio_Historico = GetCell(Grilla, lLinea, "precio_historico")
    Else
      lPrecio_Historico = ""
    End If
    
    Call lAccion.Agregar_Operaciones_Opciones_Detalle(GetCell(Grilla, Row, "id_nemotecnico"), _
                                                      GetCell(Grilla, Row, "cantidad"), _
                                                      GetCell(Grilla, Row, "precio"), _
                                                      GetCell(Grilla, Row, "id_moneda_nemotecnico"), _
                                                      GetCell(Grilla, Row, "monto"), _
                                                      "N", _
                                                      lPrecio_Historico)
  'Next

  lId_Contraparte = "" 'Fnt_ComboSelected_KEY(Cmb_Contraparte)
  lId_representante = "" 'Fnt_ComboSelected_KEY(Cmb_Representantes)
  lFecha_Operacion = lFecha_Suscripcion ' DTP_Fecha_Operacion.Value ' Fnt_FechaServidor
  lId_Trader = "" 'Fnt_ComboSelected_KEY(Cmb_Traders)
  lFecha_Vigencia = lFecha_Suscripcion ' '+ To_Number(Txt_DiasVigencia.Text)
  lFecha_Liquidacion = lFecha_Suscripcion '
  
  lTipo_Precio = ""

  Select Case lTipo_Operacion
    Case gcOPERACION_Directa_Opciones
      If Not lAccion.Realiza_Operacion_Opciones_Directa(pId_Operacion:=lId_Operacion, _
                                                        pId_Cuenta:=lId_Cuenta, _
                                                        pDsc_Operacion:="", _
                                                        pTipoOperacion:=lOperacion, _
                                                        pId_Contraparte:=lId_Contraparte, _
                                                        pId_Representante:=lId_representante, _
                                                        pId_Moneda_Operacion:=lId_Moneda_Nemo, _
                                                        pFecha_Operacion:=lFecha_Operacion, _
                                                        pFecha_Vigencia:=lFecha_Vigencia, _
                                                        pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                        pId_Trader:=lId_Trader, _
                                                        pPorc_Comision:=lPorc_Comision, _
                                                        pComision:=lComision, _
                                                        pDerechos_Bolsa:=lDerechos, _
                                                        pGastos:=lGastos, _
                                                        pIva:=lIva, _
                                                        pMonto_Operacion:=lMonto, _
                                                        pTipo_Precio:=lTipo_Precio, _
                                                        pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                                        pChkAporteRetiro:=sChkAporteRetiro) Then
                                               
        Call Fnt_MsgError(lAccion.SubTipo_LOG, _
                          "Problemas en grabar Acciones.", _
                          lAccion.ErrMsg, _
                          pConLog:=True)
        pMensaje = "Problemas en grabar Acciones " & lAccion.ErrMsg
        GoTo ErrProcedure
      End If
    Case Else
      MsgBox "Operacion no reconocida para operar.", vbCritical, Me.Caption
      pMensaje = "Operacion no reconocida para operar."
      GoTo ErrProcedure
  End Select

  lRollback = False
  Fnt_Grabar = True

ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Function
