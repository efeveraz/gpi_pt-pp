VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Control_Limite 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Evaluaci�n de L�mites Contractuales"
   ClientHeight    =   9090
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   15270
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9090
   ScaleWidth      =   15270
   Begin VB.Frame Frm_Propiedades_Cuentas 
      Caption         =   "Criterios de Evaluaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1755
      Left            =   0
      TabIndex        =   2
      Top             =   360
      Width           =   15285
      Begin VB.CommandButton Btn_BuscarNombreCliente 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11040
         Picture         =   "Frm_Reporte_Control_Limite.frx":0000
         TabIndex        =   24
         Top             =   1320
         Width           =   375
      End
      Begin VB.CommandButton Btn_BuscarRut 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11040
         Picture         =   "Frm_Reporte_Control_Limite.frx":030A
         TabIndex        =   23
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox Txt_NombreCliente 
         Height          =   315
         Left            =   7320
         TabIndex        =   21
         Top             =   1320
         Width           =   3615
      End
      Begin VB.TextBox Txt_RutCliente 
         Height          =   315
         Left            =   7320
         TabIndex        =   19
         Top             =   960
         Width           =   3615
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   315
         Left            =   7320
         TabIndex        =   17
         Top             =   600
         Width           =   3615
      End
      Begin VB.CommandButton Btn_BuscarCuenta 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11040
         Picture         =   "Frm_Reporte_Control_Limite.frx":0614
         TabIndex        =   16
         Top             =   600
         Width           =   375
      End
      Begin VB.Frame Frm_Limite 
         Caption         =   "Limite"
         Height          =   1095
         Left            =   11640
         TabIndex        =   3
         Top             =   480
         Width           =   3255
         Begin VB.OptionButton opt_ambas 
            Caption         =   "Ambas"
            Height          =   255
            Left            =   120
            TabIndex        =   8
            Top             =   240
            Value           =   -1  'True
            Width           =   975
         End
         Begin VB.OptionButton opt_noexcedido 
            Caption         =   "No Excedido"
            Height          =   255
            Left            =   120
            TabIndex        =   7
            Top             =   480
            Width           =   1215
         End
         Begin VB.OptionButton opt_excedido 
            Caption         =   "Excedido"
            Height          =   255
            Left            =   120
            TabIndex        =   6
            Top             =   720
            Width           =   975
         End
         Begin VB.CheckBox Chk_Familia 
            Caption         =   "Por Familia"
            Height          =   255
            Left            =   1560
            TabIndex        =   5
            Top             =   600
            Value           =   1  'Checked
            Width           =   1215
         End
         Begin VB.CheckBox Chk_SubFamilia 
            Caption         =   "Por SubFamilia"
            Height          =   255
            Left            =   1560
            TabIndex        =   4
            Top             =   240
            Value           =   1  'Checked
            Width           =   1575
         End
      End
      Begin TrueDBList80.TDBCombo Cmb_Asesor 
         Height          =   315
         Left            =   1560
         TabIndex        =   9
         Tag             =   "OBLI=S"
         Top             =   960
         Width           =   4125
         _ExtentX        =   7276
         _ExtentY        =   556
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   556
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Control_Limite.frx":091E
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Perfil 
         Height          =   315
         Left            =   1560
         TabIndex        =   10
         Tag             =   "OBLI=S"
         Top             =   600
         Width           =   4125
         _ExtentX        =   7276
         _ExtentY        =   556
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   556
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Control_Limite.frx":09C8
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComCtl2.DTPicker DTP_Fecha 
         Height          =   315
         Left            =   1560
         TabIndex        =   11
         Tag             =   "OBLI=S"
         Top             =   240
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   16777215
         Format          =   17039361
         CurrentDate     =   37732
      End
      Begin TrueDBList80.TDBCombo Cmb_Tipo_Administracion 
         Height          =   345
         Left            =   1560
         TabIndex        =   25
         Tag             =   "OBLI=S;CAPTION=Tipo Administraci�n"
         Top             =   1320
         Width           =   4125
         _ExtentX        =   7276
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Control_Limite.frx":0A72
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label3 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tipo Admin."
         Height          =   345
         Left            =   120
         TabIndex        =   26
         Top             =   1320
         Width           =   1395
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Nombre Cliente"
         Height          =   315
         Left            =   5880
         TabIndex        =   22
         Top             =   1320
         Width           =   1395
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Rut Cliente"
         Height          =   315
         Left            =   5880
         TabIndex        =   20
         Top             =   960
         Width           =   1395
      End
      Begin VB.Label Lbl_Cuenta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuenta"
         Height          =   315
         Left            =   5880
         TabIndex        =   18
         Top             =   600
         Width           =   1395
      End
      Begin VB.Label Lbl_Asesor 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Asesor"
         Height          =   315
         Left            =   120
         TabIndex        =   14
         Top             =   960
         Width           =   1395
      End
      Begin VB.Label Lbl_Perfil 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Perfil de Riesgo"
         Height          =   315
         Left            =   120
         TabIndex        =   13
         Top             =   600
         Width           =   1395
      End
      Begin VB.Label Lbl_Fecha 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         Height          =   315
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   1395
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15270
      _ExtentX        =   26935
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "SEARCH"
            Description     =   "Carga Aportes y Retiros de Capital"
            Object.ToolTipText     =   "Carga Saldos Activos"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORTE"
            Description     =   "Genera reporte de Aportes y Retiros de Capital"
            Object.ToolTipText     =   "Genera reporte de Saldos Activos"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   1
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Re&frescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpiar los Controles"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   6960
         TabIndex        =   1
         Top             =   0
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VSFlex8LCtl.VSFlexGrid Grilla_Reporte 
      Height          =   6915
      Left            =   0
      TabIndex        =   15
      Top             =   2160
      Width           =   15255
      _cx             =   26908
      _cy             =   12197
      Appearance      =   2
      BorderStyle     =   1
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MousePointer    =   0
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      BackColorFixed  =   -2147483633
      ForeColorFixed  =   -2147483630
      BackColorSel    =   65535
      ForeColorSel    =   0
      BackColorBkg    =   -2147483643
      BackColorAlternate=   -2147483643
      GridColor       =   -2147483633
      GridColorFixed  =   -2147483632
      TreeColor       =   -2147483632
      FloodColor      =   192
      SheetBorder     =   -2147483642
      FocusRect       =   2
      HighLight       =   1
      AllowSelection  =   0   'False
      AllowBigSelection=   0   'False
      AllowUserResizing=   1
      SelectionMode   =   1
      GridLines       =   10
      GridLinesFixed  =   2
      GridLineWidth   =   1
      Rows            =   2
      Cols            =   0
      FixedRows       =   1
      FixedCols       =   0
      RowHeightMin    =   0
      RowHeightMax    =   0
      ColWidthMin     =   0
      ColWidthMax     =   0
      ExtendLastCol   =   -1  'True
      FormatString    =   $"Frm_Reporte_Control_Limite.frx":0B1C
      ScrollTrack     =   -1  'True
      ScrollBars      =   3
      ScrollTips      =   -1  'True
      MergeCells      =   0
      MergeCompare    =   0
      AutoResize      =   -1  'True
      AutoSizeMode    =   0
      AutoSearch      =   2
      AutoSearchDelay =   2
      MultiTotals     =   -1  'True
      SubtotalPosition=   1
      OutlineBar      =   0
      OutlineCol      =   0
      Ellipsis        =   1
      ExplorerBar     =   3
      PicturesOver    =   0   'False
      FillStyle       =   0
      RightToLeft     =   0   'False
      PictureType     =   0
      TabBehavior     =   0
      OwnerDraw       =   0
      Editable        =   0
      ShowComboButton =   1
      WordWrap        =   0   'False
      TextStyle       =   0
      TextStyleFixed  =   0
      OleDragMode     =   0
      OleDropMode     =   0
      ComboSearch     =   0
      AutoSizeMouse   =   -1  'True
      FrozenRows      =   0
      FrozenCols      =   0
      AllowUserFreezing=   0
      BackColorFrozen =   0
      ForeColorFrozen =   0
      WallPaperAlignment=   9
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   24
   End
End
Attribute VB_Name = "Frm_Reporte_Control_Limite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: Frm_Reporte_Control_Limite.frm $
'    $Author: Gbuenrostro $
'    $Date: 26/11/12 19:52 $
'    $Revision: 17 $
'-------------------------------------------------------------------------------------------------

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String    'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String    'Codigo para el permiso sobre la ventana
'----------------------------------------------------------------------------

Public fApp_Excel   As Excel.Application      ' Excel application
Public fLibro       As Excel.Workbook  ' Excel workbook
Public fForm        As Frm_Reporte_Generico

Dim fCursor_Datos   As hRecord
Dim fFilaExcel      As Long
Dim vMarginLeft     As Variant
Dim fPage           As Long
Dim Id_Cuenta       As Integer

Const clrHeader = &HD0D0D0

Public Sub Mostrar_Cuenta()
Dim pFecha              As Date
Dim lNumCuenta As String
Dim lRutCliente As String
Dim lNombreCliente As String

    pFecha = Fnt_FechaServidor
    Call Sub_Entrega_DatosCuenta(Id_Cuenta, pNum_Cuenta:=lNumCuenta, pNombre_Cliente:=lNombreCliente, pRut_Cliente:=lRutCliente)
    If Id_Cuenta <> 0 Then
        Txt_Cuenta.Text = lNumCuenta
        Txt_NombreCliente.Text = lNombreCliente
        Txt_RutCliente.Text = lRutCliente
    End If
End Sub

Private Sub Btn_BuscarCuenta_Click()
    Id_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(pNum_Cuenta:=Txt_Cuenta.Text), 0)
    Call Mostrar_Cuenta
End Sub

Private Sub Btn_BuscarNombreCliente_Click()
    Id_Cuenta = NVL(Frm_Busca_Cuentas.BuscarCliente(pFiltro_NombreCliente:=Txt_NombreCliente.Text, pBusca_Cuenta:=True), 0)
    Call Mostrar_Cuenta
End Sub

Private Sub Btn_BuscarRut_Click()
    Id_Cuenta = NVL(Frm_Busca_Cuentas.BuscarCliente(pFiltro_RutCliente:=Txt_RutCliente.Text, pBusca_Cuenta:=True), 0)
    Call Mostrar_Cuenta
End Sub

Private Sub opt_ambas_Click()
    Frm_Limite.Height = 1095
    Chk_Familia.Value = 1
    Chk_Familia.Visible = False
    Chk_SubFamilia.Value = 1
    Chk_SubFamilia.Visible = False
End Sub

Private Sub opt_excedido_Click()
    Frm_Limite.Height = 1095
    Chk_Familia.Value = 1
    Chk_Familia.Visible = False
    Chk_SubFamilia.Value = 1
    Chk_SubFamilia.Visible = False
End Sub

Private Sub opt_noexcedido_Click()
    Frm_Limite.Height = 1095
    Chk_Familia.Value = 1
    Chk_Familia.Visible = False
    Chk_SubFamilia.Value = 1
    Chk_SubFamilia.Visible = False
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents
    
    Select Case Button.Key
        Case "SEARCH"
            Call Sub_GeneraConsultaGrilla
        Case "REFRESH"
            Call Sub_Limpiar
        Case "REPORTE"
            Call Sub_GeneraInformeExcel
        Case "EXIT"
            Unload Me
    End Select
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    Me.SetFocus
    DoEvents
    
    Select Case ButtonMenu.Key
        Case "EXCEL"
            Call Sub_GeneraInformeExcel
    End Select
End Sub

Private Sub Sub_Limpiar()
Dim lCierre As Class_Verificaciones_Cierre

Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
Call Sub_ComboSelectedItem(Cmb_Perfil, cCmbKALL)
'Call Sub_ComboSelectedItem(Cmb_Clientes, cCmbKALL)
'Call Sub_ComboSelectedItem(Cmb_Familia, cCmbKALL)
'Call Sub_ComboSelectedItem(Cmb_SubFamilia, cCmbKALL)

Txt_Cuenta.Text = ""
Txt_RutCliente = ""
Txt_NombreCliente = ""

Chk_Familia.Value = 1
Chk_SubFamilia.Value = 1
opt_ambas.Value = True
BarraProceso.Value = 0


Set lCierre = New Class_Verificaciones_Cierre
DTP_Fecha.Value = lCierre.Busca_Ultima_FechaCierre

Set lCierre = Nothing
Grilla_Reporte.Rows = 2

End Sub

Private Sub Sub_GeneraConsultaGrilla()
Call Sub_Bloquea_Puntero(Me)

If Not ValidaEntradaDatos Then GoTo ExitProcedure

If Fnt_CargarDatos Then
    Call Sub_CargaGrilla_Reporte
End If

ExitProcedure:
    Call Sub_Desbloquea_Puntero(Me)
    
End Sub

Private Sub Sub_CargaGrilla_Reporte()
    Dim lReg            As hFields
    Dim lFila           As Long
    
    With Grilla_Reporte
        .Rows = 2
        For Each lReg In fCursor_Datos
            lFila = .Rows
            Call .AddItem("")
            If lReg("FLAG_RV").Value = "S" Then
                .Cell(flexcpForeColor, lFila, 15, lFila, 15) = RGB(255, 0, 0)
                .Cell(flexcpForeColor, lFila, 17, lFila, 17) = RGB(255, 0, 0)
                .Cell(flexcpForeColor, lFila, 21, lFila, 21) = RGB(255, 0, 0)
            End If
            
            If lReg("FLAG_RF").Value = "S" Then
                .Cell(flexcpForeColor, lFila, 16, lFila, 16) = RGB(255, 0, 0)
                .Cell(flexcpForeColor, lFila, 18, lFila, 18) = RGB(255, 0, 0)
                .Cell(flexcpForeColor, lFila, 22, lFila, 22) = RGB(255, 0, 0)
            End If
            
            If lReg("MTO_DIFERENCIA_RV").Value < 0 Then
                .Cell(flexcpForeColor, lFila, 23, lFila, 23) = RGB(255, 0, 0)
                .Cell(flexcpAlignment, lFila, 23, lFila, 23) = 7
            End If
            
            If lReg("MTO_DIFERENCIA_RF").Value < 0 Then
                .Cell(flexcpForeColor, lFila, 24, lFila, 24) = RGB(255, 0, 0)
                .Cell(flexcpAlignment, lFila, 24, lFila, 24) = 7
            End If

            .Cell(flexcpText, lFila, 0) = lReg("NUM_CUENTA").Value
            .Cell(flexcpText, lFila, 1) = lReg("COD_TIPO_ADMINISTRACION").Value
            .Cell(flexcpText, lFila, 2) = lReg("RUT_CLIENTE").Value
            .Cell(flexcpText, lFila, 3) = lReg("NOMBRE_CLIENTE").Value
            .Cell(flexcpText, lFila, 4) = lReg("DSC_PERFIL_RIESGO").Value
            .Cell(flexcpText, lFila, 5) = lReg("NOMBRE_ASESOR").Value
            .Cell(flexcpText, lFila, 6) = FormatNumber(lReg("SALDO_CAJA_MON_CUENTA").Value, 2)
            .Cell(flexcpText, lFila, 7) = FormatNumber(lReg("MONTO_X_COBRAR_MON_CTA").Value, 2)
            .Cell(flexcpText, lFila, 8) = FormatNumber(lReg("MTO_TOTAL_RV").Value, 2)
            .Cell(flexcpText, lFila, 9) = FormatNumber(lReg("MTO_TOTAL_RF").Value, 2)
            .Cell(flexcpText, lFila, 10) = FormatNumber(lReg("MTO_TOTAL_ACTIVOS").Value, 2)
            .Cell(flexcpText, lFila, 11) = IIf(IsNull(lReg("LIMITE_RV").Value), "Sin Limites", FormatNumber(lReg("LIMITE_RV").Value, 4))
            .Cell(flexcpText, lFila, 12) = IIf(IsNull(lReg("LIMITE_RF").Value), "Sin Limites", FormatNumber(lReg("LIMITE_RF").Value, 4))
            .Cell(flexcpText, lFila, 13) = FormatNumber(lReg("LIMITE_REAL_RV").Value, 4)
            .Cell(flexcpText, lFila, 14) = FormatNumber(lReg("LIMITE_REAL_RF").Value, 4)
            .Cell(flexcpText, lFila, 15) = IIf(lReg("FLAG_RV").Value = "S", "EXCEDIDO", "OK")
            .Cell(flexcpText, lFila, 16) = IIf(lReg("FLAG_RF").Value = "S", "EXCEDIDO", "OK")
            .Cell(flexcpText, lFila, 17) = FormatNumber(lReg("LIMITE_EXCESO_RV").Value, 4)
            .Cell(flexcpText, lFila, 18) = FormatNumber(lReg("LIMITE_EXCESO_RF").Value, 4)
            .Cell(flexcpText, lFila, 19) = FormatNumber(lReg("MTO_MAXIMO_RV").Value, 2)
            .Cell(flexcpText, lFila, 20) = FormatNumber(lReg("MTO_MAXIMO_RF").Value, 2)
            .Cell(flexcpText, lFila, 21) = FormatNumber(lReg("MTO_REAL_RV").Value, 2)
            .Cell(flexcpText, lFila, 22) = FormatNumber(lReg("MTO_REAL_RF").Value, 2)
            .Cell(flexcpText, lFila, 23) = IIf(lReg("MTO_DIFERENCIA_RV").Value > 0, FormatNumber(lReg("MTO_DIFERENCIA_RV").Value, 2), "(" & FormatNumber(lReg("MTO_DIFERENCIA_RV").Value, 2) & ")")
            .Cell(flexcpText, lFila, 24) = IIf(lReg("MTO_DIFERENCIA_RF").Value > 0, FormatNumber(lReg("MTO_DIFERENCIA_RF").Value, 2), "(" & FormatNumber(lReg("MTO_DIFERENCIA_RF").Value, 2) & ")")
        Next

        Rem Autoajusta las columnas y deselecciona filas
        .AutoSize 0, .Cols - 1
        If .Row > 0 Then .IsSelected(.Row) = False
    End With

End Sub

Private Function ValidaEntradaDatos() As Boolean
Dim bOk    As Boolean
Dim bNok    As Boolean

bOk = True
bNok = False


If Chk_Familia.Value = False And Chk_SubFamilia.Value = False Then
    ValidaEntradaDatos = bNok
    MsgBox "Elegir a lo menos UNA Opcion de Excedido", vbCritical, "Reporte Control de Limite"
    
Else
    ValidaEntradaDatos = bOk
End If

End Function

Public Sub Mostrar(pCod_Arbol_Sistema)
    fCod_Arbol_Sistema = pCod_Arbol_Sistema
    fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)

    Call Form_Resize

    Load Me
End Sub

Private Sub Form_Load()
With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEARCH").Image = cBoton_Buscar
    .Buttons("REPORTE").Image = cBoton_Modificar
    .Buttons("REFRESH").Image = cBoton_Original
    .Buttons("EXIT").Image = cBoton_Salir
End With

Call Sub_CargaForm

Me.Top = 1
Me.Left = 1
End Sub

Private Sub Form_Resize()
Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Sub_CargaForm()
    Dim lReg   As hFields
    
    Dim lLinea As Long
    Dim lCierre As Class_Verificaciones_Cierre
    
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha.Value = lCierre.Busca_Ultima_FechaCierre
    
    Set lCierre = Nothing
    
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_FormControl_Color(Me.Controls)
    
    Rem Carga los combos con el primer elemento vac�o
    Call Sub_CargaCombo_Perfil_Riesgo(Cmb_Perfil, pTodos:=True)
    Call Sub_ComboSelectedItem(Cmb_Perfil, cCmbKALL)
    
    Call Sub_CargaCombo_Asesor(Cmb_Asesor, pTodos:=True)
    Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
    Call Sub_CargaCombo_Tipos_Administracion(Cmb_Tipo_Administracion, pTodos:=True)
    Call Sub_ComboSelectedItem(Cmb_Tipo_Administracion, cCmbKALL)
    
    Chk_Familia.Value = 1
    Chk_Familia.Visible = False
    Chk_SubFamilia.Value = 1
    Chk_SubFamilia.Visible = False
    
    Call Sub_Encabezado_Grilla
    
    Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Sub Sub_EncabezadoDetalle()
Dim lTitulo         As String
Dim lMultiplo       As Integer
Dim lFila           As Long
Dim lColRep         As Long
Dim lDesde          As Long
Dim lHasta          As Long
Dim lCol            As Long

With fForm.VsPrinter
    .FontSize = 14
    .FontBold = True
    .MarginLeft = vMarginLeft
    If fPage > 1 Then
        .Paragraph = ""
    End If
    .Paragraph = "Reporte Control de Limite"
    .FontSize = 10
    .Paragraph = "Fecha Consulta: " & DTP_Fecha.Value

    .StartTable
    .MarginLeft = "6mm"
    .FontSize = 7
    .TableCell(tcAlignCurrency) = False


    .TableCell(tcRows) = 1
    .TableCell(tcCols) = 14

    lMultiplo = 1
    lTitulo = ""
    
    .TableCell(tcColWidth, 1, 1) = "22mm"
    lColRep = 0
    
    For lCol = 0 To Grilla_Reporte.Cols - 1
    
        If lTitulo = Grilla_Reporte.Cell(flexcpText, 0, lCol) Then
            lMultiplo = lMultiplo + 1
        Else
            lHasta = (22 * lMultiplo)
            
            If lColRep = 1 Or lColRep = 3 Or lColRep = 5 Or lColRep = 11 Or lColRep = 12 Or lColRep = 13 Or lColRep = 14 Then
                lHasta = 16
            End If
            .TableCell(tcText, 1, lColRep) = lTitulo
            .TableCell(tcColWidth, 1, lColRep, 1, lColRep) = CStr(lHasta) & "mm"
            lColRep = lColRep + 1
            lMultiplo = 1
            lTitulo = Grilla_Reporte.Cell(flexcpText, 0, lCol)
        End If

    Next

    .TableCell(tcText, 1, 14) = lTitulo
    .TableCell(tcColWidth, 1, lColRep, 1, lColRep) = "16mm"
    .TableCell(tcAlign, 1, 1, 1, 14) = taCenterMiddle
    .TableCell(tcFontBold, 1, 1, 1, 14) = True
    .TableCell(tcBackColor, 1, 1, 1, 14) = RGB(240, 240, 240)
    .EndTable
    
    .StartTable
    .MarginLeft = "6mm"
    .TableCell(tcCols) = Grilla_Reporte.Cols
    lFila = .TableCell(tcRows)
    .TableCell(tcColWidth, 1, 1, 1, 1) = "16mm"
    .TableCell(tcColWidth, 1, 2, 1, 2) = "22mm"
    .TableCell(tcColWidth, 1, 3, 1, 3) = "16mm"
    .TableCell(tcColWidth, 1, 4, 1, 4) = "22mm"
    .TableCell(tcColWidth, 1, 5, 1, 5) = "16mm"
    .TableCell(tcColWidth, 1, 6, 1, 10) = "22mm"
    .TableCell(tcColWidth, 1, 11, 1, 14) = "16mm"
    .TableCell(tcAlign, 1, 7, 1, 7) = taRightMiddle
    .TableCell(tcAlign, 1, 10, 1, 14) = taRightMiddle
    .TableCell(tcFontSize, 1, 1, 1, 14) = 7

End With
End Sub

Private Function Fnt_CargarDatos() As Boolean
Dim lcLimites           As Class_Limites
Dim lCargaExitosa       As Boolean
Dim strIdAsesor         As String
Dim strIdCliente        As String
Dim strIdCuenta         As String
Dim strSubFamilia       As String
Dim strPerfil           As String
Dim strFamilia          As String
Dim strExceso           As String
Dim intPorFamilia       As Integer
Dim intPorSubFamilia    As Integer
Dim lcod_Tipo_Administracion As String

lCargaExitosa = False

strIdAsesor = IIf(Cmb_Asesor.Text = "Todos" Or Cmb_Asesor.Text = "", "", Fnt_ComboSelected_KEY(Cmb_Asesor))
'strIdCliente = IIf(Cmb_Clientes.Text = "Todos" Or Cmb_Clientes.Text = "", "", Fnt_ComboSelected_KEY(Cmb_Clientes))
strIdCuenta = IIf(Txt_Cuenta.Text = "", "", Id_Cuenta)
'strFamilia = IIf(Cmb_Familia.Text = "Todos" Or Cmb_Familia.Text = "", "", Fnt_ComboSelected_KEY(Cmb_Familia))
'strSubFamilia = IIf(Cmb_SubFamilia.Text = "Todos" Or Cmb_SubFamilia.Text = "", "", Fnt_ComboSelected_KEY(Cmb_SubFamilia))
strPerfil = IIf(Cmb_Perfil.Text = "Todos" Or Cmb_Perfil.Text = "", "", Fnt_ComboSelected_KEY(Cmb_Perfil))
intPorFamilia = Chk_Familia.Value
intPorSubFamilia = Chk_SubFamilia.Value
lcod_Tipo_Administracion = IIf(Cmb_Tipo_Administracion.Text = "Todos", "", Fnt_ComboSelected_KEY(Cmb_Tipo_Administracion))


If opt_noexcedido Then
    strExceso = "N"
End If
If opt_excedido Then
    strExceso = "E"
End If
If opt_ambas Then
    strExceso = "A"
End If


Set lcLimites = New Class_Limites

With lcLimites
    .Campo("Fecha_Cierre").Valor = DTP_Fecha.Value
    .Campo("Id_Asesor").Valor = strIdAsesor
'    .Campo("Id_Cliente").Valor = strIdCliente
    .Campo("Id_Cuenta").Valor = strIdCuenta
    .Campo("Id_Perfil_Riesgo").Valor = strPerfil
    .Campo("Id_Familia").Valor = strFamilia
    .Campo("Id_Sub_Familia").Valor = strSubFamilia
    .Campo("Id_Exceso").Valor = strExceso
    .Campo("Id_Por_Familia").Valor = intPorFamilia
    .Campo("Id_Por_Sub_Familia").Valor = intPorSubFamilia

    If .Trae_Control_Limites(Fnt_EmpresaActual, strIdAsesor, strIdCliente, strIdCuenta, strPerfil, strFamilia, strSubFamilia, strExceso, intPorFamilia, intPorSubFamilia, lcod_Tipo_Administracion) Then
        If .Cursor.Count > 0 Then
            Set fCursor_Datos = .Cursor
            BarraProceso.max = fCursor_Datos.Count
            lCargaExitosa = True
        Else
            MsgBox "No se registran Movimientos en esta fecha.", vbInformation, Me.Caption
        End If
    Else
        MsgBox "Error al cargar datos." & vbCr & vbCr & gDB.ErrMsg, vbInformation, Me.Caption
    End If

End With

Set lcLimites = Nothing

ExitProcedure:
    Fnt_CargarDatos = lCargaExitosa
    
End Function

Private Function Busca_Ultima_FechaCierre()
Dim lCierre As Class_Verificaciones_Cierre
Set lCierre = New Class_Verificaciones_Cierre

Busca_Ultima_FechaCierre = lCierre.Busca_Ultima_FechaCierre

Set lCierre = Nothing
End Function

Private Sub Sub_GeneraInformeExcel()
    Call Sub_Bloquea_Puntero(Me)
    
    If Not ValidaEntradaDatos Then GoTo ExitProcedure
    
    If Me.Grilla_Reporte.Rows < 3 Then
        MsgBox "No se registran Movimientos en esta fecha.", vbInformation, Me.Caption
        GoTo ExitProcedure
    End If
    
    Call Sub_CreaExcel
    Call Sub_CargaExcel_Detalle
    
    fApp_Excel.Visible = True
    fApp_Excel.UserControl = True

ExitProcedure:
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CreaExcel()
Dim lHoja           As Integer
Dim lIndex          As Integer
Dim lTitulo_Detalle As String
Dim lTitulo_Resumen As String
Dim lTitulo_Fecha   As String
Dim lItem_Detalle   As String
Dim lItem_Resumen   As String

Const cTama�o_Titulo = 12
Const cTama�o_Fecha = 11

Call Sub_Bloquea_Puntero(Me)

lHoja = 1

Set fApp_Excel = CreateObject("Excel.application")
fApp_Excel.DisplayAlerts = False
Set fLibro = fApp_Excel.Workbooks.Add

fApp_Excel.ActiveWindow.DisplayGridlines = False
fLibro.Worksheets.Add

With fLibro
    For lIndex = .Worksheets.Count To lHoja + 1 Step -1
        .Worksheets(lIndex).Delete
    Next lIndex

    For lIndex = 1 To lHoja
        .Sheets(lIndex).Select
        fApp_Excel.ActiveWindow.DisplayGridlines = False
        .ActiveSheet.Range("A5:E6").Font.Bold = True
        .ActiveSheet.Range("A5:E6").HorizontalAlignment = xlLeft
    Next
    lTitulo_Detalle = "Control de Limite"
    lTitulo_Fecha = "Fecha Consulta: " & DTP_Fecha.Value
    lItem_Detalle = "Limite"

    .Sheets(1).Select
    .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).Select
    .ActiveSheet.Range("A5").Value = lTitulo_Detalle
    .ActiveSheet.Range("A5").Font.Size = cTama�o_Titulo
    .ActiveSheet.Range("A6").Value = lTitulo_Fecha
    .ActiveSheet.Range("A6").Font.Size = cTama�o_Fecha
    .ActiveSheet.Range("A5:K5").Merge
    .ActiveSheet.Range("A6:K6").Merge
    .Worksheets.Item(1).Name = lItem_Detalle

End With

End Sub

Private Sub Sub_CargaExcel_Detalle()
Dim lxHoja As Worksheet
Dim lcChart As Excel.Chart
Dim lId_Cuenta_New As String
Dim lId_Cuenta_Old As String
Dim lCol_Grilla As Integer
Dim lReg   As hFields
'-----------------------------
Dim lTotal As Double
Dim lCol   As Integer
Dim lFila  As Long
Dim lMonto As Double
Dim lIndex As Integer
fFilaExcel = 8

Call Sub_ExcelEncabezado(Grilla_Reporte, 1)
Set lxHoja = fLibro.Sheets(1)
Call lxHoja.Select

lId_Cuenta_New = ""
lId_Cuenta_Old = ""
fFilaExcel = fFilaExcel + 1
With lxHoja
    For Each lReg In fCursor_Datos
        BarraProceso.Value = lReg.Index
        fFilaExcel = fFilaExcel + 1
                
        If lReg("FLAG_RV").Value = "S" Then
            .Range(.Cells(fFilaExcel, 16), .Cells(fFilaExcel, 16)).Font.Color = RGB(255, 0, 0)
            .Range(.Cells(fFilaExcel, 18), .Cells(fFilaExcel, 18)).Font.Color = RGB(255, 0, 0)
            .Range(.Cells(fFilaExcel, 22), .Cells(fFilaExcel, 22)).Font.Color = RGB(255, 0, 0)
        End If
        
        If lReg("FLAG_RF").Value = "S" Then
            .Range(.Cells(fFilaExcel, 17), .Cells(fFilaExcel, 17)).Font.Color = RGB(255, 0, 0)
            .Range(.Cells(fFilaExcel, 19), .Cells(fFilaExcel, 19)).Font.Color = RGB(255, 0, 0)
            .Range(.Cells(fFilaExcel, 23), .Cells(fFilaExcel, 23)).Font.Color = RGB(255, 0, 0)
        End If
        
        If lReg("MTO_DIFERENCIA_RV").Value < 0 Then
            .Range(.Cells(fFilaExcel, 24), .Cells(fFilaExcel, 24)).Font.Color = RGB(255, 0, 0)
            .Range(.Cells(fFilaExcel, 24), .Cells(fFilaExcel, 24)).Cells.HorizontalAlignment = xlRight
        End If
        
        If lReg("MTO_DIFERENCIA_RF").Value < 0 Then
            .Range(.Cells(fFilaExcel, 25), .Cells(fFilaExcel, 25)).Font.Color = RGB(255, 0, 0)
            .Range(.Cells(fFilaExcel, 25), .Cells(fFilaExcel, 25)).Cells.HorizontalAlignment = xlRight
        End If
        
        .Cells(fFilaExcel, 1) = "'" & lReg("NUM_CUENTA").Value
        .Cells(fFilaExcel, 2) = lReg("COD_TIPO_ADMINISTRACION").Value
        .Cells(fFilaExcel, 3) = lReg("RUT_CLIENTE").Value
        .Cells(fFilaExcel, 4) = lReg("NOMBRE_CLIENTE").Value
        .Cells(fFilaExcel, 5) = lReg("dsc_perfil_riesgo").Value
        .Cells(fFilaExcel, 6) = lReg("nombre_asesor").Value
        .Cells(fFilaExcel, 7).Value = lReg("SALDO_CAJA_MON_CUENTA").Value
        .Cells(fFilaExcel, 7).NumberFormat = Fnt_Formato_Moneda(2)
        .Cells(fFilaExcel, 8).Value = lReg("MONTO_X_COBRAR_MON_CTA").Value
        .Cells(fFilaExcel, 8).NumberFormat = Fnt_Formato_Moneda(2)
        .Cells(fFilaExcel, 9) = lReg("MTO_TOTAL_RV").Value
        .Cells(fFilaExcel, 9).NumberFormat = Fnt_Formato_Moneda(2)
        .Cells(fFilaExcel, 10) = lReg("MTO_TOTAL_RF").Value
        .Cells(fFilaExcel, 10).NumberFormat = Fnt_Formato_Moneda(2)
        .Cells(fFilaExcel, 11) = lReg("MTO_TOTAL_ACTIVOS").Value
        .Cells(fFilaExcel, 11).NumberFormat = Fnt_Formato_Moneda(2) '2 decimales
        If IsNull(lReg("LIMITE_RV").Value) Then
            .Cells(fFilaExcel, 12) = "Sin Limites"
        Else
            .Cells(fFilaExcel, 12).Value = lReg("LIMITE_RV").Value
            .Cells(fFilaExcel, 12).NumberFormat = Fnt_Formato_Moneda(4) '4 decimales
        End If
        If IsNull(lReg("LIMITE_RF").Value) Then
            .Cells(fFilaExcel, 13) = "Sin Limites"
        Else
            .Cells(fFilaExcel, 13).Value = lReg("LIMITE_RF").Value
            .Cells(fFilaExcel, 13).NumberFormat = Fnt_Formato_Moneda(4) '4 decimales
        End If
        .Cells(fFilaExcel, 14) = lReg("LIMITE_REAL_RV").Value
        .Cells(fFilaExcel, 14).NumberFormat = Fnt_Formato_Moneda(4) '4 decimales
        .Cells(fFilaExcel, 15) = lReg("LIMITE_REAL_RF").Value
        .Cells(fFilaExcel, 15).NumberFormat = Fnt_Formato_Moneda(4) '4 decimales
        .Cells(fFilaExcel, 16) = IIf(lReg("FLAG_RV").Value = "S", "EXCEDIDO", "OK")
        .Cells(fFilaExcel, 17) = IIf(lReg("FLAG_RF").Value = "S", "EXCEDIDO", "OK")
        .Cells(fFilaExcel, 18) = lReg("LIMITE_EXCESO_RV").Value
        .Cells(fFilaExcel, 18).NumberFormat = Fnt_Formato_Moneda(4) '4 decimales
        .Cells(fFilaExcel, 19) = lReg("LIMITE_EXCESO_RF").Value
        .Cells(fFilaExcel, 19).NumberFormat = Fnt_Formato_Moneda(4) '4 decimales
        .Cells(fFilaExcel, 20) = lReg("MTO_MAXIMO_RV").Value
        .Cells(fFilaExcel, 20).NumberFormat = Fnt_Formato_Moneda(2) '4 decimales
        .Cells(fFilaExcel, 21) = lReg("MTO_MAXIMO_RF").Value
        .Cells(fFilaExcel, 21).NumberFormat = Fnt_Formato_Moneda(2) '4 decimales
        .Cells(fFilaExcel, 22) = lReg("MTO_REAL_RV").Value
        .Cells(fFilaExcel, 22).NumberFormat = Fnt_Formato_Moneda(2) '4 decimales
        .Cells(fFilaExcel, 23) = lReg("MTO_REAL_RF").Value
        .Cells(fFilaExcel, 23).NumberFormat = Fnt_Formato_Moneda(2) '4 decimales
        If lReg("MTO_DIFERENCIA_RV").Value > 0 Then
            .Cells(fFilaExcel, 24) = FormatNumber(lReg("MTO_DIFERENCIA_RV").Value)
        Else
            .Cells(fFilaExcel, 24).Value = "(" & FormatNumber(lReg("MTO_DIFERENCIA_RV").Value, 2) & ")"
        End If
        If lReg("MTO_DIFERENCIA_RF").Value > 0 Then
            .Cells(fFilaExcel, 25) = FormatNumber(lReg("MTO_DIFERENCIA_RF").Value)
        Else
            .Cells(fFilaExcel, 25).Value = "(" & FormatNumber(lReg("MTO_DIFERENCIA_RF").Value, 2) & ")"
        End If
        
    Next
End With

fLibro.ActiveSheet.Columns("A:A").Select
fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
fApp_Excel.Selection.EntireColumn.AutoFit

End Sub

Private Sub Sub_ExcelEncabezado(ByVal pGrilla As Object, pHoja As Integer)
Dim lxHoja As Worksheet
Dim lCol   As Integer
Dim lCol_Grilla As Integer
Dim lCol_Desde As Integer
Dim lTitulo As String
Dim lhRecord As hRecord
Dim lReg   As hFields
Dim lReg_C As hFields

Set lhRecord = New hRecord
With lhRecord
    .AddField "COL_DESDE"
    .AddField "COL_HASTA"
End With

Set lxHoja = fLibro.Sheets(pHoja)
Call lxHoja.Select

lCol_Desde = 1 '3

lCol = 1
With lxHoja
    For lCol_Grilla = 0 To pGrilla.Cols - 1 '-3
        .Cells(fFilaExcel, lCol) = pGrilla.Cell(flexcpText, 0, lCol_Grilla)
        .Cells(fFilaExcel + 1, lCol) = pGrilla.Cell(flexcpText, 1, lCol_Grilla)
        lCol = lCol + 1
    Next

    lCol = lCol_Desde
    lTitulo = .Cells(fFilaExcel, lCol)
    Set lReg = lhRecord.Add
    lReg("COL_DESDE").Value = lCol

    For lCol = lCol_Desde + 1 To pGrilla.Cols
        If lTitulo = .Cells(fFilaExcel, lCol) Then
            .Cells(fFilaExcel, lCol) = ""
        Else
            lTitulo = .Cells(fFilaExcel, lCol)
            lReg("COL_HASTA").Value = lCol - 1   '-1
            Set lReg = lhRecord.Add
            lReg("COL_DESDE").Value = lCol
        End If
    Next
    lReg("COL_HASTA").Value = pGrilla.Cols

    For Each lReg_C In lhRecord
        .Range(.Cells(fFilaExcel, lReg_C("COL_DESDE").Value), .Cells(fFilaExcel, lReg_C("COL_HASTA").Value)).MergeCells = True
        If lReg_C("COL_DESDE").Value > 0 And lReg_C("COL_HASTA").Value < 12 Then .Range(.Cells(fFilaExcel, lReg_C("COL_DESDE").Value), .Cells(fFilaExcel + 1, lReg_C("COL_HASTA").Value)).MergeCells = True
    Next

    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel + 1, lCol - 1)).BorderAround
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel + 1, lCol - 1)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel + 1, lCol - 1)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel + 1, lCol - 1)).Font.Bold = True
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel + 1, lCol - 1)).HorizontalAlignment = xlCenter
    .Range(.Cells(fFilaExcel, lCol_Desde), .Cells(fFilaExcel + 1, lCol - 1)).VerticalAlignment = xlCenter

End With

End Sub

Sub Sub_Encabezado_Grilla()

With Grilla_Reporte
    .Clear
    .Cols = 25
    .Cell(flexcpText, 0, 0) = "Cuenta"
    .Cell(flexcpText, 1, 0) = "Cuenta"
    .Cell(flexcpText, 0, 1) = "Tipo Admin."
    .Cell(flexcpText, 1, 1) = "Tipo Admin."
    .Cell(flexcpText, 0, 2) = "Rut"
    .Cell(flexcpText, 1, 2) = "Rut"
    .Cell(flexcpText, 0, 3) = "Nombre Cliente"
    .Cell(flexcpText, 1, 3) = "Nombre Cliente"
    .Cell(flexcpText, 0, 4) = "Perfil Riesgo"
    .Cell(flexcpText, 1, 4) = "Perfil Riesgo"
    .Cell(flexcpText, 0, 5) = "Asesor"
    .Cell(flexcpText, 1, 5) = "Asesor"
    .Cell(flexcpText, 0, 6) = "Caja"
    .Cell(flexcpText, 1, 6) = "Caja"
    .Cell(flexcpText, 0, 7) = "Cuentas por Cobrar"
    .Cell(flexcpText, 1, 7) = "Cuentas por Cobrar"
    .Cell(flexcpText, 0, 8) = "Total Activo RV"
    .Cell(flexcpText, 1, 8) = "Total Activo RV"
    .Cell(flexcpText, 0, 9) = "Total Activo RF"
    .Cell(flexcpText, 1, 9) = "Total Activo RF"
    .Cell(flexcpText, 0, 10) = "Total Activos"
    .Cell(flexcpText, 1, 10) = "Total Activos"
    .Cell(flexcpText, 0, 11) = "Lim�tes de Inversi�n"
    .Cell(flexcpText, 1, 11) = "% RV"
    .Cell(flexcpText, 0, 12) = "Lim�tes de Inversi�n"
    .Cell(flexcpText, 1, 12) = "% RF"
    .Cell(flexcpText, 0, 13) = "L�mites Reales"
    .Cell(flexcpText, 1, 13) = "% RV"
    .Cell(flexcpText, 0, 14) = "L�mites Reales"
    .Cell(flexcpText, 1, 14) = "% RF"
    .Cell(flexcpText, 0, 15) = "Exceso de L�mites"
    .Cell(flexcpText, 1, 15) = "$ RV"
    .Cell(flexcpText, 0, 16) = "Exceso de L�mites"
    .Cell(flexcpText, 1, 16) = "$ RF"
    .Cell(flexcpText, 0, 17) = "Exceso de L�mites"
    .Cell(flexcpText, 1, 17) = "% RV"
    .Cell(flexcpText, 0, 18) = "Exceso de L�mites"
    .Cell(flexcpText, 1, 18) = "% RF"
    .Cell(flexcpText, 0, 19) = "Monto M�ximo de L�mites"
    .Cell(flexcpText, 1, 19) = "$ RV"
    .Cell(flexcpText, 0, 20) = "Monto M�ximo de L�mites"
    .Cell(flexcpText, 1, 20) = "$ RF"
    .Cell(flexcpText, 0, 21) = "Monto Limites Reales"
    .Cell(flexcpText, 1, 21) = "$ RV"
    .Cell(flexcpText, 0, 22) = "Monto Limites Reales"
    .Cell(flexcpText, 1, 22) = "$ RF"
    .Cell(flexcpText, 0, 23) = "Diferencia de Limites"
    .Cell(flexcpText, 1, 23) = "$ RV"
    .Cell(flexcpText, 0, 24) = "Diferencia de Limites"
    .Cell(flexcpText, 1, 24) = "$ RF"
    
      .Select 0, 0, 1, .Cols - 1
      .FillStyle = 1 'flexFillRepeat
      .CellAlignment = 4 'flexAlignCenterCenter
      
      .MergeCells = 4 ' flexMergeRestrictAll
      .MergeCol(0) = True
      .MergeCol(1) = True
      .MergeCol(2) = True
      .MergeCol(3) = True
      .MergeCol(4) = True
      .MergeCol(5) = True
      .MergeCol(6) = True
      .MergeCol(7) = True
      .MergeCol(8) = True
      .MergeCol(9) = True
      .MergeCol(10) = True
      
      .MergeRow(0) = True
      .MergeRow(1) = True
      
      .FixedRows = 2
    
End With
End Sub

Private Sub Sub_Inicia_Documento()
Dim lTitulo As String
      
lTitulo = "Control de Limites"

Set fForm = New Frm_Reporte_Generico
Call fForm.Sub_InicarDocumento(pTitulo:="" _
                             , pTipoSalida:=ePrinter.eP_Pantalla _
                             , pOrientacion:=orLandscape)
vMarginLeft = fForm.VsPrinter.MarginLeft
End Sub

Private Sub Txt_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call Btn_BuscarCuenta_Click
    End If
End Sub

Private Sub Txt_NombreCliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call Btn_BuscarCuenta_Click
    End If
End Sub

Private Sub Txt_RutCliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call Btn_BuscarCuenta_Click
    End If
End Sub
