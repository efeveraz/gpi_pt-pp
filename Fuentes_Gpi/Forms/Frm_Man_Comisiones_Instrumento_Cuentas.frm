VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Man_Comisiones_Instrumento_Cuentas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Comisiones Transaccionales"
   ClientHeight    =   4650
   ClientLeft      =   165
   ClientTop       =   540
   ClientWidth     =   8235
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4650
   ScaleWidth      =   8235
   Tag             =   "OBLI"
   Begin VB.Frame Frame1 
      Caption         =   "Comisiones por Cuenta Instrumento"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4155
      Left            =   75
      TabIndex        =   0
      Top             =   420
      Width           =   8070
      Begin VB.CheckBox chk_TodasCtas 
         Caption         =   "Todas"
         Height          =   375
         Left            =   4410
         TabIndex        =   7
         ToolTipText     =   "Todas las Cuentas"
         Top             =   345
         Width           =   855
      End
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3975
         Picture         =   "Frm_Man_Comisiones_Instrumento_Cuentas.frx":0000
         TabIndex        =   5
         Top             =   345
         Width           =   375
      End
      Begin VB.CheckBox Chk_Vigentes 
         Caption         =   "Solo Vigentes"
         Height          =   375
         Left            =   5625
         TabIndex        =   4
         Top             =   345
         Width           =   1455
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3165
         Left            =   120
         TabIndex        =   1
         Top             =   900
         Width           =   7875
         _cx             =   13891
         _cy             =   5583
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   11
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Comisiones_Instrumento_Cuentas.frx":030A
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   135
         TabIndex        =   6
         Top             =   345
         Width           =   3780
         _ExtentX        =   6668
         _ExtentY        =   609
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "N� de Cuenta"
         Text            =   ""
         MaxLength       =   100
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   8235
      _ExtentX        =   14526
      _ExtentY        =   635
      ButtonWidth     =   2037
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Eliminar"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Elimina un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un elemento"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Comisiones_Instrumento_Cuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String
Dim lId_Cuenta As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub

Private Sub chk_TodasCtas_Click()

If chk_TodasCtas.Value = 1 Then
    Txt_Num_Cuenta.Text = ""
    Txt_Num_Cuenta.Tag = ""
    Txt_Num_Cuenta.Enabled = False
    cmb_buscar.Enabled = False
    Grilla.Rows = 1
    Call Sub_CargarDatos
Else
    Txt_Num_Cuenta.Enabled = True
    cmb_buscar.Enabled = True
End If

End Sub

Private Sub Chk_Vigentes_Click()
  Call Sub_CargarDatos
End Sub

Private Sub cmb_buscar_Click()
lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)

If lId_Cuenta <> 0 Then
    Txt_Num_Cuenta.Text = ""
    Txt_Num_Cuenta.Tag = lId_Cuenta
    Call Lpr_Buscar_Cuenta("", CStr(lId_Cuenta))
End If

End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With

  Call Sub_CargaForm
  'Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
Dim lKey As String
Dim lId_Cuenta As String
Dim lNum_Cuenta As String

  With Grilla
    If .Row > 0 Then
      lKey = GetCell(Grilla, .Row, "colum_pk")
      lId_Cuenta = GetCell(Grilla, .Row, "id_cuenta")
      lNum_Cuenta = GetCell(Grilla, .Row, "num_cuenta")
      Call Sub_EsperaVentana(lKey, GetCell(Grilla, .Row, "Instrumento"), lNum_Cuenta, lId_Cuenta)
    End If
  End With
End Sub

Private Sub Sub_EsperaVentana(ByVal pkey, ByVal instrumento, ByVal Cuenta, ByVal IdCuenta)
Dim lForm As Frm_Comisiones_Instrumento_Cuentas
Dim lNombre As String
Dim lNom_Form As String
  
  Me.Enabled = False
  
  lNom_Form = "Frm_Comisiones_Instrumento_Cuentas"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lNombre = Me.Name
    
    Set lForm = New Frm_Comisiones_Instrumento_Cuentas
    Call lForm.Fnt_Modificar(pkey, instrumento, Cuenta, IdCuenta, pCod_Arbol_Sistema:=fCod_Arbol_Sistema, pGrilla:=Grilla)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      Call Sub_CargarDatos
    Else
      Exit Sub
    End If
  End If
  
  Me.Enabled = True
  
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "EXIT"
      Unload Me
    Case "ADD"
      Call Sub_Agregar
    Case "UPDATE"
      Call Grilla_DblClick
    Case "REFRESH"
      Call Sub_CargaForm
    Case "DEL"
      Call Sub_Eliminar
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
      
  End Select
End Sub

Private Sub Sub_CargarDatos()
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lComisiones As Class_Comisiones_Instrumentos
Dim lId_Cuenta As String

  Me.Enabled = False
  Call Sub_Bloquea_Puntero(Me)
  
  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Grilla.Rows = 1
  
  If Trim(Txt_Num_Cuenta.Tag) <> "" And Trim(Txt_Num_Cuenta.Text) <> "" Then
    lId_Cuenta = Txt_Num_Cuenta.Tag
  Else
    lId_Cuenta = ""
  End If
  
  Set lComisiones = New Class_Comisiones_Instrumentos
  With lComisiones
    .Campo("Id_Cuenta").Valor = lId_Cuenta
    If .BuscarView_Todas(Fnt_EmpresaActual) Then
      For Each lReg In .Cursor
        If Fnt_Mostrar_Registro(NVL(lReg("Fecha_Hasta").Value, "")) Then
          lLinea = Grilla.Rows
          Call Grilla.AddItem("")
          'Llenen las correspondiente columnas antes definidas en la grilla
          Call SetCell(Grilla, lLinea, "colum_pk", lReg("id_comision_instrumento_cuenta").Value) 'Aqui solo poner en la columna "colum_pk" el id de la entidad
          Call SetCell(Grilla, lLinea, "Id_Instrumento", lReg("cod_Instrumento").Value)
          Call SetCell(Grilla, lLinea, "Instrumento", lReg("Dsc_Intrumento").Value)
          Call SetCell(Grilla, lLinea, "id_Cuenta", lReg("id_cuenta").Value)
          Call SetCell(Grilla, lLinea, "num_Cuenta", lReg("num_cuenta").Value & " - " & lReg("abr_cuenta").Value)
          Call SetCell(Grilla, lLinea, "Fecha_ini", lReg("Fecha_Desde").Value)
          Call SetCell(Grilla, lLinea, "Fecha_ter", NVL(lReg("Fecha_Hasta").Value, ""))
          Call SetCell(Grilla, lLinea, "Comision", To_Number(NVL(lReg("comision").Value, 0)) * 100)
          Call SetCell(Grilla, lLinea, "Gastos", NVL(lReg("Gastos").Value, 0))
          Call SetCell(Grilla, lLinea, "Derechos", To_Number(NVL(lReg("Derechos_Bolsa").Value, 0)) * 100)
          'Call SetCell(Grilla, lLinea, "Iva", lReg("Flg_Afecta_IVA").Value = "S")
          Grilla.Cell(flexcpChecked, lLinea, Grilla.ColIndex("Iva")) = IIf(lReg("Flg_Afecta_IVA").Value = "S", flexChecked, flexUnchecked)
        End If
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Comisiones Instrumentos.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  
  Set lComisiones = Nothing

  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If
  
  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargaForm()
  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_FormControl_Color(Me.Controls)

  'Limpia la grilla
  Txt_Num_Cuenta.Text = ""
  Txt_Num_Cuenta.Tag = ""
  chk_TodasCtas.Value = 0
  Txt_Num_Cuenta.Enabled = True
  cmb_buscar.Enabled = True
  Grilla.Rows = 1
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Agregar()
  
  If Txt_Num_Cuenta.Tag = "" Or chk_TodasCtas.Value = 1 Then
    MsgBox "Debe seleccionar una cuenta para agregar Comisiones a Instrumentos.", vbExclamation, Me.Caption
  Else
    Call Sub_EsperaVentana(cNewEntidad, "", Txt_Num_Cuenta.Text, Txt_Num_Cuenta.Tag)
  End If
  
End Sub

Private Sub Sub_Eliminar()
Dim lID As String
Dim lcComisiones_Instrumento As Class_Comisiones_Instrumentos

  Call Sub_Bloquea_Puntero(Me)
  
  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      lID = GetCell(Grilla, Grilla.Row, "colum_pk")
      If Not lID = "" Then
        Set lcComisiones_Instrumento = New Class_Comisiones_Instrumentos
        With lcComisiones_Instrumento
          .Campo("Id_Comision_Instrum_Cuenta").Valor = lID
          If .Borrar Then
            MsgBox "Comisi�n eliminada correctamente.", vbInformation + vbOKOnly, Me.Caption
          Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al eliminar la Comisi�n Instrumento.", _
                        .ErrMsg, _
                        pConLog:=True)
          End If
        End With
        Set lcComisiones_Instrumento = Nothing
      End If
      Call Sub_CargarDatos
    End If
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Fnt_Mostrar_Registro(lFecha_Ter As String) As Boolean
  If Chk_Vigentes.Value = 1 Then
     If lFecha_Ter = "" Then
        Fnt_Mostrar_Registro = True
        Exit Function
     Else
        If CDate(lFecha_Ter) >= Date Then
           Fnt_Mostrar_Registro = True
           Exit Function
        Else
           Fnt_Mostrar_Registro = False
           Exit Function
        End If
     End If
  Else
     Fnt_Mostrar_Registro = True
     Exit Function
  End If
End Function

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
Dim lCuenta As String
Dim lVigentes As String

  Set lForm = New Frm_Reporte_Generico
  
  lCuenta = Txt_Num_Cuenta.Text
  lVigentes = IIf(Chk_Vigentes, " (Solo Vigentes)", "")
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Comisiones por Cuenta Instrumento" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Cuenta: " & lCuenta & lVigentes
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call Lpr_Buscar_Cuenta("TXT")
    End If
End Sub

Private Sub Lpr_Buscar_Cuenta(ByVal Origen As String, Optional p_Id_Cuenta As String)
Dim lcCuenta      As Object

    If Origen = "TXT" Then
        If InStr(1, Txt_Num_Cuenta.Text, "-") > 0 Then
           Txt_Num_Cuenta.Text = Trim(Left(Txt_Num_Cuenta.Text, InStr(1, Txt_Num_Cuenta.Text, "-") - 1))
        End If
        p_Id_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text), 0)
    End If
    If p_Id_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = p_Id_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                    lId_Cuenta = .Cursor(1)("id_cuenta").Value
                    If Not lId_Cuenta = "" Then
                       Call Sub_CargarDatos
                    End If
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
            Set lcCuenta = Nothing
    End If
End Sub

