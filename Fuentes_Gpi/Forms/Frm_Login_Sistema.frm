VERSION 5.00
Object = "{E2D000D0-2DA1-11D2-B358-00104B59D73D}#1.0#0"; "titext8.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Login_Sistema 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inicio de sesión"
   ClientHeight    =   5385
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6435
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   204
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Frm_Login_Sistema.frx":0000
   ScaleHeight     =   5385
   ScaleWidth      =   6435
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1425
      Left            =   390
      Picture         =   "Frm_Login_Sistema.frx":0342
      ScaleHeight     =   1425
      ScaleWidth      =   1065
      TabIndex        =   12
      Top             =   1290
      Width           =   1065
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H80000001&
      BorderStyle     =   0  'None
      Height          =   2385
      Left            =   0
      TabIndex        =   5
      Top             =   3120
      Width           =   6435
      Begin VB.CommandButton cmdEntrar 
         BackColor       =   &H00FFFFFF&
         Caption         =   "&Entrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3810
         TabIndex        =   3
         Top             =   360
         Width           =   1125
      End
      Begin VB.CommandButton cmdSair 
         BackColor       =   &H00FFFFFF&
         Caption         =   "&Salir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5040
         TabIndex        =   4
         Top             =   330
         Width           =   1125
      End
      Begin TrueDBList80.TDBCombo Cmb_Empresa 
         Height          =   345
         Left            =   1650
         TabIndex        =   2
         Top             =   1020
         Width           =   3165
         _ExtentX        =   5583
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   2
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   0   'False
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   0
         AnimateWindowDirection=   0
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Login_Sistema.frx":45B0
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HFFFFFF&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TDBText6Ctl.TDBText Txt_Password 
         Height          =   315
         Left            =   1650
         TabIndex        =   1
         Tag             =   "OBLI=S"
         Top             =   660
         Width           =   1785
         _Version        =   65536
         _ExtentX        =   3149
         _ExtentY        =   556
         Caption         =   "Frm_Login_Sistema.frx":465A
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DropDown        =   "Frm_Login_Sistema.frx":46BE
         Key             =   "Frm_Login_Sistema.frx":46DC
         BackColor       =   16777215
         EditMode        =   0
         ForeColor       =   -2147483640
         ReadOnly        =   0
         ShowContextMenu =   1
         MarginLeft      =   1
         MarginRight     =   1
         MarginTop       =   1
         MarginBottom    =   1
         Enabled         =   -1
         MousePointer    =   0
         Appearance      =   2
         BorderStyle     =   1
         AlignHorizontal =   0
         AlignVertical   =   0
         MultiLine       =   0
         ScrollBars      =   0
         PasswordChar    =   "*"
         AllowSpace      =   -1
         Format          =   ""
         FormatMode      =   1
         AutoConvert     =   -1
         ErrorBeep       =   0
         MaxLength       =   0
         LengthAsByte    =   0
         Text            =   ""
         Furigana        =   0
         HighlightText   =   -1
         IMEMode         =   3
         IMEStatus       =   0
         DropWndWidth    =   0
         DropWndHeight   =   0
         ScrollBarMode   =   0
         MoveOnLRKey     =   0
         OLEDragMode     =   0
         OLEDropMode     =   0
      End
      Begin TDBText6Ctl.TDBText txt_usuario 
         Height          =   315
         Left            =   1650
         TabIndex        =   0
         Tag             =   "OBLI=S"
         Top             =   300
         Width           =   1785
         _Version        =   65536
         _ExtentX        =   3149
         _ExtentY        =   556
         Caption         =   "Frm_Login_Sistema.frx":4720
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DropDown        =   "Frm_Login_Sistema.frx":4784
         Key             =   "Frm_Login_Sistema.frx":47A2
         BackColor       =   16777215
         EditMode        =   0
         ForeColor       =   -2147483640
         ReadOnly        =   0
         ShowContextMenu =   1
         MarginLeft      =   1
         MarginRight     =   1
         MarginTop       =   1
         MarginBottom    =   1
         Enabled         =   -1
         MousePointer    =   0
         Appearance      =   2
         BorderStyle     =   1
         AlignHorizontal =   0
         AlignVertical   =   0
         MultiLine       =   0
         ScrollBars      =   0
         PasswordChar    =   ""
         AllowSpace      =   -1
         Format          =   ""
         FormatMode      =   1
         AutoConvert     =   -1
         ErrorBeep       =   0
         MaxLength       =   0
         LengthAsByte    =   0
         Text            =   ""
         Furigana        =   0
         HighlightText   =   -1
         IMEMode         =   0
         IMEStatus       =   0
         DropWndWidth    =   0
         DropWndHeight   =   0
         ScrollBarMode   =   0
         MoveOnLRKey     =   0
         OLEDragMode     =   0
         OLEDropMode     =   0
      End
      Begin VB.Label Label2 
         BackColor       =   &H80000001&
         Caption         =   "Usuario"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   300
         Left            =   330
         TabIndex        =   8
         Top             =   300
         Width           =   930
      End
      Begin VB.Label Label3 
         BackColor       =   &H80000001&
         Caption         =   "Empresa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   300
         Left            =   330
         TabIndex        =   7
         Top             =   990
         Width           =   930
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         BackColor       =   &H80000001&
         Caption         =   "Password"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   330
         TabIndex        =   6
         Top             =   660
         Width           =   1305
      End
   End
   Begin VB.Label lblMsg 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   840
      TabIndex        =   11
      Top             =   2460
      Width           =   4920
   End
   Begin VB.Label lblVersao 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "2.01"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1980
      TabIndex        =   10
      Top             =   1980
      Width           =   2565
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "G P I"
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   615
      Left            =   960
      TabIndex        =   9
      Top             =   1350
      Width           =   4575
   End
   Begin VB.Image Image2 
      Height          =   945
      Left            =   -30
      Picture         =   "Frm_Login_Sistema.frx":47E6
      Stretch         =   -1  'True
      Top             =   0
      Width           =   6510
   End
End
Attribute VB_Name = "Frm_Login_Sistema"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim fResultado As Boolean
Dim LInd_Login  As Boolean

Public Function Fnt_Mostrar(Optional pInd_Login As Boolean = False) As Boolean
    Fnt_Mostrar = False
    
    'Agregado para tratar la implementación de cambio de empresa
    LInd_Login = pInd_Login

    Call Sub_CargarDatos
    Me.Show vbModal
    
    Fnt_Mostrar = fResultado
    Unload Me
  
End Function

Public Function Fnt_Mostrar_Cambio_Empresa() As Boolean
  Fnt_Mostrar_Cambio_Empresa = False
  
  LInd_Login = True
  Call Sub_CargaForm
  Label8.Visible = False
  Txt_Password.Visible = False
  Me.Show vbModal
  
  Fnt_Mostrar_Cambio_Empresa = fResultado

End Function

Public Function Fnt_MostrarLauncher(ByVal Usuario As String, ByVal Negocio As Integer) As Boolean
    Fnt_MostrarLauncher = False
    
    'Agregado para tratar la implementación de cambio de empresa
    'LInd_Login = pInd_Login
    Call Fnt_ValidarUsuarioLauncher(Usuario, Negocio)
    Call Sub_CargarDatos
    'Me.Show vbModal
    fResultado = True
    Fnt_MostrarLauncher = fResultado
    Unload Me
    
End Function

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = True
End Function

Private Function Fnt_ValidarUsuario() As Boolean
Dim lcUsuario As Class_Usuarios
Dim lPassword As String
Dim lcEmpresa As Class_Empresas
  Fnt_ValidarUsuario = False
  If Fnt_PreguntaIsNull(txt_usuario, "Usuario") Then
    Exit Function
  End If
  If Fnt_PreguntaIsNull(Txt_Password, "Password") Then
    Exit Function
  End If
  If Not Fnt_NotNullControl(Cmb_Empresa, "Empresa") Then
    Exit Function
  End If
  Set lcUsuario = New Class_Usuarios
  lcUsuario("id_empresa").Valor = Fnt_ComboSelected_KEY(Cmb_Empresa)
  lcUsuario("dsc_usuario").Valor = txt_usuario.Text
  If lcUsuario.Buscar Then
    If lcUsuario.Cursor.Count = 0 Then
      'el usuario no existe.
      MsgBox "El usuario no existe.", vbCritical, Me.Caption
      Exit Function
    End If
    lPassword = lcUsuario.Cursor(1)("password").Value
    If Not Txt_Password.Text = lPassword Then
      'Si la clave no coincide con la ingresada.
      MsgBox "No coinciden las claves.", vbCritical, Me.Caption
      Exit Function
    End If
    If Not lcUsuario.Cursor(1)("Cod_Estado").Value = "V" Then
      'Si el usuario no se encuentra vigente.
      MsgBox "El usuario no se encuentra vigente.", vbCritical, Me.Caption
      Exit Function
    End If
    gID_Usuario = lcUsuario.Cursor(1)("id_usuario").Value
    gDsc_Usuario = lcUsuario.Cursor(1)("dsc_usuario").Value
    gPassword = lPassword
    gId_Empresa = lcUsuario.Cursor(1)("id_empresa").Value
    gDsc_Empresa = Cmb_Empresa.Text
    MDI_Principal.Caption = "CSGPI  -  " & gDsc_Empresa
    Set lcEmpresa = New Class_Empresas
    With lcEmpresa
        .Campo("id_empresa").Valor = Fnt_ComboSelected_KEY(Cmb_Empresa)
        If Not .Buscar Then
            MsgBox .ErrMsg, vbCritical
            Set lcEmpresa = Nothing
            Exit Function
        End If
        gTipo_Menu = .Cursor(1)("flg_tipo_menu").Value
    End With
    Set lcEmpresa = Nothing
  Else
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
    Exit Function
  End If
  Fnt_ValidarUsuario = True
End Function

Private Function Fnt_ValidarUsuarioCambioEmpresa() As Boolean
Dim lcUsuario As Class_Usuarios
Dim lPassword As String
Dim lcEmpresa As Class_Empresas
  
  Fnt_ValidarUsuarioCambioEmpresa = False
  If Fnt_PreguntaIsNull(txt_usuario, "Usuario") Then
    Exit Function
  End If
'  If Fnt_PreguntaIsNull(Txt_Password, "Password") Then
'    Exit Function
'  End If
  If Not Fnt_NotNullControl(Cmb_Empresa, "Empresa") Then
    Exit Function
  End If
  Set lcUsuario = New Class_Usuarios
  lcUsuario("id_empresa").Valor = Fnt_ComboSelected_KEY(Cmb_Empresa)
  lcUsuario("dsc_usuario").Valor = txt_usuario.Text
  If lcUsuario.Buscar Then
    If lcUsuario.Cursor.Count = 0 Then
      'el usuario no existe.
      MsgBox "El usuario no existe.", vbCritical, Me.Caption
      Exit Function
    End If
'    lPassword = lcUsuario.Cursor(1)("password").Value
'    If Not Txt_Password.Text = lPassword Then
'      'Si la clave no coincide con la ingresada.
'      MsgBox "No coinciden las claves.", vbCritical, Me.Caption
'      Exit Function
'    End If
    If Not lcUsuario.Cursor(1)("Cod_Estado").Value = "V" Then
      'Si el usuario no se encuentra vigente.
      MsgBox "El usuario no se encuentra vigente.", vbCritical, Me.Caption
      Exit Function
    End If
    gID_Usuario = lcUsuario.Cursor(1)("id_usuario").Value
    gDsc_Usuario = lcUsuario.Cursor(1)("dsc_usuario").Value
    'gPassword = lPassword
    gId_Empresa = lcUsuario.Cursor(1)("id_empresa").Value
    gDsc_Empresa = Cmb_Empresa.Text
    MDI_Principal.Caption = "CSGPI  -  " & gDsc_Empresa
    Set lcEmpresa = New Class_Empresas
    With lcEmpresa
        .Campo("id_empresa").Valor = Fnt_ComboSelected_KEY(Cmb_Empresa)
        If Not .Buscar Then
            MsgBox .ErrMsg, vbCritical
            Set lcEmpresa = Nothing
            Exit Function
        End If
        gTipo_Menu = .Cursor(1)("flg_tipo_menu").Value
    End With
    Set lcEmpresa = Nothing
  Else
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
    Exit Function
  End If
  Fnt_ValidarUsuarioCambioEmpresa = True
End Function

Private Function Fnt_ValidarUsuarioLauncher(ByVal Usuario As String, ByVal Negocio As Integer) As Boolean
Dim lcUsuario As Class_Usuarios
Dim lPassword As String
Dim lcEmpresa As Class_Empresas
  Fnt_ValidarUsuarioLauncher = False
'  If Fnt_PreguntaIsNull(txt_usuario, "Usuario") Then
'    Exit Function
'  End If
'  If Fnt_PreguntaIsNull(Txt_Password, "Password") Then
'    Exit Function
'  End If
'  If Not Fnt_NotNullControl(Cmb_Empresa, "Empresa") Then
'    Exit Function
'  End If
  Set lcUsuario = New Class_Usuarios
  lcUsuario("id_empresa").Valor = Negocio 'Fnt_ComboSelected_KEY(Cmb_Empresa)
  lcUsuario("dsc_usuario").Valor = Usuario 'txt_usuario.Text
  If lcUsuario.Buscar Then
    If lcUsuario.Cursor.Count = 0 Then
      'el usuario no existe.
      'MsgBox "El usuario no existe.", vbCritical, Me.Caption
      Exit Function
    End If
'    lPassword = lcUsuario.Cursor(1)("password").Value
'    If Not Txt_Password.Text = lPassword Then
'      'Si la clave no coincide con la ingresada.
'      MsgBox "No coinciden las claves.", vbCritical, Me.Caption
'      Exit Function
'    End If
'    If Not lcUsuario.Cursor(1)("Cod_Estado").Value = "V" Then
'      'Si el usuario no se encuentra vigente.
'      MsgBox "El usuario no se encuentra vigente.", vbCritical, Me.Caption
'      Exit Function
'    End If
    gID_Usuario = lcUsuario.Cursor(1)("id_usuario").Value
    gDsc_Usuario = lcUsuario.Cursor(1)("dsc_usuario").Value
    'gPassword = lPassword
    gId_Empresa = lcUsuario.Cursor(1)("id_empresa").Value
    'gDsc_Empresa = Cmb_Empresa.Text
    
    Set lcEmpresa = New Class_Empresas
    With lcEmpresa
        .Campo("id_empresa").Valor = gId_Empresa 'Fnt_ComboSelected_KEY(Cmb_Empresa)
        If Not .Buscar Then
            MsgBox .ErrMsg, vbCritical
            Set lcEmpresa = Nothing
            Exit Function
        End If
        gTipo_Menu = .Cursor(1)("flg_tipo_menu").Value
        gDsc_Empresa = .Cursor(1)("DSC_EMPRESA").Value
    End With
    MDI_Principal.Caption = "CSGPI  -  " & gDsc_Empresa
    Set lcEmpresa = Nothing
  Else
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
    Exit Function
  End If
  Fnt_ValidarUsuarioLauncher = True
End Function

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "OK"
      If Fnt_ValidarUsuario Then
        fResultado = True
        Me.Hide
      End If
    Case "EXIT"
      Me.Hide
  End Select
End Sub

Private Sub Sub_CargaForm()
  fResultado = False
  
    If (LInd_Login) Then
        'Agregado para tratar la implementación de cambio de empresa
        Call Sub_CargaCombo_Usuario_Empresas(Cmb_Empresa)
        txt_usuario.Text = gDsc_Usuario
        Txt_Password.Text = gPassword
        txt_usuario.Enabled = False
        Txt_Password.Enabled = False
    Else
        Call Sub_CargaCombo_Empresa(Cmb_Empresa)
    End If

End Sub

Private Sub Sub_CargarDatos()
  Load Me
  Me.Caption = Me.Caption & " (" & gDB.Server & ")"
End Sub

Private Sub Cmb_Empresa_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If Fnt_ValidarUsuario Then
            fResultado = True
            Me.Hide
        End If
    End If
End Sub

Private Sub cmdEntrar_Click()
    
    'If Fnt_ValidarUsuario Then
    If Fnt_ValidarUsuarioCambioEmpresa Then
        fResultado = True
        Me.Hide
        'Agregado para tratar la implementación de Cambio de Empresa
        If (LInd_Login) Then
            With MDI_Principal
            .StatusBar.Panels("EMPRESA").Text = "CSGPI  -  " & gDsc_Empresa
            .StatusBar.Panels("EMPRESA").ToolTipText = .StatusBar.Panels("EMPRESA").Text
            End With
        End If
    End If
    
End Sub

Private Sub cmdSair_Click()
    
    'Agregado para tratar la implementación de Cambio de Empresa
    If (LInd_Login) Then
        Unload Me
    Else
        End
    End If
    
End Sub

Private Sub Form_Activate()
    Me.Caption = App.Title & " (" & gDB.Server & ")"
    lblVersao = "Versión: " & App.Major & "." & App.Minor & "." & App.Revision
    DoEvents
    cmdEntrar.Enabled = True
    cmdSair.Enabled = True
    lblMsg.Caption = App.CompanyName
    If (LInd_Login) Then
        Me.cmdEntrar.Caption = "Cambiar"
        Me.cmdSair.Caption = "Cancelar"
        Me.Caption = App.Title & " (Cambio de Empresa)"
        MDI_Principal.Caption = "CSGPI  -  " & gDsc_Empresa
    End If
    
    DoEvents
End Sub

Private Sub Form_Load()
'    Dim lParametro
'    lParametro = Trim(Command)
'    lParametro = "adm"
'    If Len(lParametro) = 0 Then
'        End
'    Else
'        Call Fnt_MostrarLauncher
'    End If
'    Call Sub_CargaForm
End Sub

Private Sub Txt_Password_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        SendKeys "{TAB}"
    End If
End Sub

Private Sub txt_usuario_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        SendKeys "{TAB}"
    End If
End Sub
