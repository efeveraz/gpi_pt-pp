VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Man_Valor_Tipo_Cambios 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Valores de Tipos de Cambios"
   ClientHeight    =   4995
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6450
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4995
   ScaleWidth      =   6450
   Begin VB.Frame frm 
      Height          =   735
      Left            =   60
      TabIndex        =   4
      Top             =   420
      Width           =   6315
      Begin MSComCtl2.DTPicker DTP_Fecha 
         Height          =   315
         Left            =   1170
         TabIndex        =   5
         Tag             =   "SOLOLECTURA=N"
         Top             =   240
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   16777215
         Format          =   76742657
         CurrentDate     =   37732
      End
      Begin VB.Label Label4 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         Height          =   315
         Left            =   180
         TabIndex        =   6
         Top             =   240
         Width           =   915
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Valor Tipos de Cambio"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   60
      TabIndex        =   0
      Top             =   1230
      Width           =   6315
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   90
         TabIndex        =   1
         Top             =   300
         Width           =   6075
         _cx             =   10716
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   5
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Valor_Tipo_Cambios.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   6450
      _ExtentX        =   11377
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Grabar"
            Key             =   "SAVE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Graba la informaci�n de forma permanente"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Valor_Tipo_Cambios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With

  Call Sub_CargaForm
  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_Setear_Cargar_Grilla()
Dim lFila As Long

  If Grilla.Rows > 1 Then
    For lFila = 1 To Grilla.Rows - 1
      SetCell Grilla, lFila, "colum_pk", ""
      SetCell Grilla, lFila, "valor", ""
    Next
    Call Sub_CargarDatos
  End If

End Sub

Private Sub DTP_Fecha_Change()
  Call Sub_Setear_Cargar_Grilla
End Sub

Private Sub Grilla_AfterEdit(ByVal Row As Long, ByVal Col As Long)
  Rem Pregunta si se modifica la columna Valor de la Grilla, coloca un cero en la colum_pk (Id_Valor_Tipo_Cambio)
  If Col = 4 Then
    If GetCell(Grilla, Row, "colum_pk") = "" Then
      SetCell Grilla, Row, "colum_pk", 0
    End If
  End If
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  Rem Cancel si col es Descripci�n Tipo Cambio o C�digo Tipo de Cambio
  If Col = 2 Or Col = 3 Then Cancel = True
End Sub

Private Sub Grilla_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
  If Col = 4 Then
    Call Sub_EsASCIINumero(KeyAscii)
  End If
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_Setear_Cargar_Grilla
      End If
    Case "EXIT"
      Unload Me
    Case "SAVE"
      Call Sub_Grabar
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Sub_CargarDatos()
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lValor_Tipo_Cambio As Class_Valor_Tipo_Cambio
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Set lValor_Tipo_Cambio = New Class_Valor_Tipo_Cambio
  With lValor_Tipo_Cambio
    .Campo("FECHA").Valor = DTP_Fecha.Value
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla.FindRow(lReg("ID_TIPO_CAMBIO").Value, , 1)
        If Not lLinea = -1 Then
          SetCell Grilla, lLinea, "colum_pk", lReg("ID_VALOR_TIPO_CAMBIO").Value
          SetCell Grilla, lLinea, "VALOR", NVL(lReg("VALOR").Value, "")
        End If
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lValor_Tipo_Cambio = Nothing
  
  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If

  Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Sub Sub_CargaForm()
Dim lReg As hFields
Dim lLinea  As Long
'Dim lcTipo_Cambio As Class_Tipo_Cambios
Dim lcTipo_Cambio As Object

  Call Sub_Bloquea_Puntero(Me)

  'Limpia la grilla
  Grilla.Rows = 1
  
  DTP_Fecha.Value = Format(Fnt_FechaServidor, cFormatDate)
  
'  Set lcTipo_Cambio = New Class_Tipo_Cambios
  Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
  With lcTipo_Cambio
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla.Rows
        Grilla.AddItem ""
        SetCell Grilla, lLinea, "ID_TIPO_CAMBIO", lReg("ID_TIPO_CAMBIO").Value
        SetCell Grilla, lLinea, "DSC_TIPO_CAMBIO", lReg("DSC_TIPO_CAMBIO").Value
        SetCell Grilla, lLinea, "ABR_TIPO_CAMBIO", lReg("ABR_TIPO_CAMBIO").Value
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcTipo_Cambio = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Grabar()
Dim lFila As Long
Dim lId_Valor_Tipo_Cambio As String
Dim lResult As Boolean
Dim lValor_Tipo_Cambio As Class_Valor_Tipo_Cambio
  
  gDB.IniciarTransaccion
  lResult = True
  
  If Grilla.Rows > 1 Then
    For lFila = 1 To Grilla.Rows - 1
      lId_Valor_Tipo_Cambio = GetCell(Grilla, lFila, "colum_pk")
      
      Rem Graba la fila de la Grilla donde no sea vac�a la colum_pk ni el valor
      If Not lId_Valor_Tipo_Cambio = "" And Not GetCell(Grilla, lFila, "VALOR") = "" Then
        Set lValor_Tipo_Cambio = New Class_Valor_Tipo_Cambio
        With lValor_Tipo_Cambio
          Rem colom_pk = 0 => Inserta una nuevo registro de la tabla Valor_Tipo_Cambio con los datos de la Grilla
          Rem colum_pk > 0 => Actualiza el registro de la tabla Valor_Tipo_Cambio con los datos de la Grilla
          .Campo("Id_Valor_Tipo_Cambio").Valor = lId_Valor_Tipo_Cambio
          .Campo("Id_Tipo_Cambio").Valor = GetCell(Grilla, lFila, "ID_TIPO_CAMBIO")
          .Campo("Valor").Valor = GetCell(Grilla, lFila, "VALOR")
          .Campo("Fecha").Valor = DTP_Fecha.Value
          .Campo("OrigenConver").Valor = gcORIG_MONVAL
          .Campo("TipoConver").Valor = gcTIPCAMB_MONVAL
          If Not .Guardar Then
            MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            lResult = False
            GoTo ErrProcedure
          End If
          Call SetCell(Grilla, lFila, "colum_pk", .Campo("Id_Valor_Tipo_Cambio").Valor)
        End With
      
      Rem Elimina la fila de la Grilla donde no sea vac�a la colum_pk y el valor es vac�o
      ElseIf Not lId_Valor_Tipo_Cambio = "" And GetCell(Grilla, lFila, "VALOR") = "" Then
        Set lValor_Tipo_Cambio = New Class_Valor_Tipo_Cambio
        With lValor_Tipo_Cambio
          .Campo("id_valor_tipo_cambio").Valor = lId_Valor_Tipo_Cambio
          If Not .Borrar Then
            MsgBox "Problemas en borrar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            lResult = False
            GoTo ErrProcedure
          End If
        End With
      End If
    Next lFila
  End If
  
ErrProcedure:
  If Err Then
    lResult = False
    Resume
  End If
  
  If lResult Then
    gDB.CommitTransaccion
    MsgBox "Datos Grabados con exito", vbInformation, Me.Caption
  Else
    gDB.RollbackTransaccion
  End If
  
  Set lValor_Tipo_Cambio = Nothing
  
  If Grilla.Rows > 1 Then Call Sub_CargarDatos
  
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
Dim lFecha As String

  Set lForm = New Frm_Reporte_Generico
  lFecha = DTP_Fecha.Value
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Listado de Valores de Tipos de Cambios" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha : " & lFecha
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

