VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Frm_Visor_Cartola_Movimientos_Caja 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta Movimientos de Caja"
   ClientHeight    =   7515
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11400
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7515
   ScaleWidth      =   11400
   Begin VB.Frame Frame3 
      Caption         =   "Datos Generales Cuenta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1000
      Left            =   60
      TabIndex        =   12
      Top             =   1560
      Width           =   11295
      Begin hControl2.hTextLabel Txt_Cliente 
         Height          =   315
         Left            =   4320
         TabIndex        =   13
         Top             =   240
         Width           =   4695
         _ExtentX        =   8281
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Cliente"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Descripcion_Cta 
         Height          =   315
         Left            =   120
         TabIndex        =   14
         Top             =   600
         Width           =   4755
         _ExtentX        =   8387
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   "Descripci�n Cuenta"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Rut 
         Height          =   315
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   3390
         _ExtentX        =   5980
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   1200
         Caption         =   "RUT"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel txtNombreAsesor 
         Height          =   315
         Left            =   5160
         TabIndex        =   16
         Top             =   600
         Width           =   5880
         _ExtentX        =   10372
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Asesor"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filtros de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1200
      Left            =   60
      TabIndex        =   7
      Top             =   360
      Width           =   11295
      Begin VB.CommandButton cmb_buscar_Cuenta 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   7680
         Picture         =   "Frm_Visor_Cartola_Movimientos_Caja.frx":0000
         TabIndex        =   17
         Top             =   240
         Width           =   375
      End
      Begin MSComCtl2.DTPicker DT_fechahasta 
         Height          =   345
         Left            =   1260
         TabIndex        =   1
         Top             =   630
         Width           =   1395
         _ExtentX        =   2461
         _ExtentY        =   609
         _Version        =   393216
         Format          =   16842753
         CurrentDate     =   38852
      End
      Begin MSComCtl2.DTPicker DT_fechadesde 
         Height          =   315
         Left            =   1260
         TabIndex        =   0
         Top             =   270
         Width           =   1395
         _ExtentX        =   2461
         _ExtentY        =   556
         _Version        =   393216
         Format          =   16842753
         CurrentDate     =   38852
      End
      Begin TrueDBList80.TDBCombo Cmb_Cajas 
         Height          =   345
         Left            =   4290
         TabIndex        =   2
         Top             =   630
         Width           =   3795
         _ExtentX        =   6694
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Visor_Cartola_Movimientos_Caja.frx":030A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComDlg.CommonDialog cmmExcel 
         Left            =   9420
         Top             =   330
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         DefaultExt      =   "xls"
         DialogTitle     =   "Exportar a Excel"
         Filter          =   "*.xls"
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   3045
         TabIndex        =   18
         Top             =   270
         Width           =   4500
         _ExtentX        =   7938
         _ExtentY        =   609
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "N� de Cuenta"
         Text            =   ""
         MaxLength       =   100
      End
      Begin VB.Label Label4 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuenta"
         Height          =   315
         Left            =   6900
         TabIndex        =   11
         Top             =   630
         Width           =   1185
      End
      Begin VB.Label Label3 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cajas"
         Height          =   315
         Left            =   3060
         TabIndex        =   10
         Top             =   630
         Width           =   1185
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Hasta"
         Height          =   315
         Left            =   150
         TabIndex        =   9
         Top             =   630
         Width           =   1065
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Desde"
         Height          =   315
         Left            =   150
         TabIndex        =   8
         Top             =   270
         Width           =   1065
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Detalle de Movimientos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4815
      Left            =   60
      TabIndex        =   6
      Top             =   2640
      Width           =   11295
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   4395
         Left            =   120
         TabIndex        =   3
         Top             =   270
         Width           =   11025
         _cx             =   19447
         _cy             =   7752
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   7
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Visor_Cartola_Movimientos_Caja.frx":03B4
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   0   'False
         AutoSizeMode    =   1
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   1
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   0   'False
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   11400
      _ExtentX        =   20108
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Buscar"
            Key             =   "RESEARCH"
            Description     =   "Genera Archivo Excel"
            Object.ToolTipText     =   "Busca Movimientos de Caja"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Excel"
            Key             =   "XLS"
            Object.ToolTipText     =   "Genera Archivo Excel"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Visor_Cartola_Movimientos_Caja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'=========================================================================================================
'====  Formulario         : Frm_Visor_Cartola_Movimientos
'====  Descripci�n        : Este formulario permite consultar los movimientos realizados por cuenta,
'====                      entre fechas y por caja.
'====  Desarrollado por   : Hans Muller R.
'====  Fecha Creaci�n     : 16/06/2006
'====  Fecha Modificaci�n : 15/03/2007 CSM
'====  Base de Datos      : Oracle 8i     Usuario : CSBPI
'=========================================================================================================
Rem ---------------------------------------------------------------------------
Rem 03/04/2009. Controla fecha de consulta si es el permiso es sololectura
Rem             se considera la fecha de cierre virtual como fecha de consulta
Rem ---------------------------------------------------------------------------

Option Explicit

Dim fFlg_Tipo_Permiso   As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema  As String 'Codigo para el permiso sobre la ventana
Dim fFechaCierreVirtual

Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
'  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  fFlg_Tipo_Permiso = Fnt_Obtiene_Permiso_Formulario(pCod_Arbol_Sistema)
 
  Call Form_Resize
  
  Load Me
End Sub
Private Sub cmb_buscar_Cuenta_Click()
    Dim lId_Cuenta As String
    Dim ftextCuenta As String
    
    ftextCuenta = Txt_Num_Cuenta.Text & "-"
    If Len(ftextCuenta) > 1 Then
        ftextCuenta = Trim(Left(ftextCuenta, InStr(1, ftextCuenta, "-") - 1))
    Else
        ftextCuenta = ""
    End If
 
    If Txt_Num_Cuenta.Text <> "" Then
        Call Sub_MostrarCuenta(Txt_Num_Cuenta.Text, pId:=False)
    End If
    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text), 0)
    Txt_Num_Cuenta.Tag = lId_Cuenta
    If lId_Cuenta <> 0 Then
       Cmb_Cuentas_ItemChange
    End If
End Sub

Private Sub Cmb_Cuentas_ItemChange()
Dim lId_Cuenta          As String
Dim lcCuenta            As Object

  lId_Cuenta = Txt_Num_Cuenta.Tag
  If Not lId_Cuenta = "" Then
    Call Sub_CargaCombo_Cajas_Cuenta(Cmb_Cajas, lId_Cuenta, pTodos:=True)
    Call Sub_ComboSelectedItem(Cmb_Cajas, cCmbKALL)
    
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
      .Campo("Id_Cuenta").Valor = lId_Cuenta
      If .Buscar Then
        If .Cursor.Count > 0 Then
            Txt_Num_Cuenta.Text = .Cursor(1)("Num_Cuenta").Value
            Txt_Cliente.Text = .Cursor(1)("Nombre_Cliente").Value
            Txt_Descripcion_Cta.Text = .Cursor(1)("DSC_CUENTA").Value
            Txt_Rut.Text = .Cursor(1)("rut_Cliente").Value
            txtNombreAsesor.Text = .Cursor(1)("DSC_ASESOR").Value
            fFechaCierreVirtual = NVL(.Cursor(1)("fecha_cierre_virtual").Value, "")
            If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
              If Not fFechaCierreVirtual = "" Then
                If DT_fechahasta.Value > fFechaCierreVirtual Then
                    DT_fechahasta.Value = fFechaCierreVirtual
                    If DT_fechahasta.Value < DT_fechadesde.Value Then
                        DT_fechadesde.Value = DT_fechahasta.Value - 30
                    End If
                End If
              End If
            End If
            
        End If
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
      End If
    End With
    Set lcCuenta = Nothing
  End If
End Sub

Private Sub Form_Load()
   With Toolbar
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("RESEARCH").Image = "boton_grilla_buscar"
      .Buttons("XLS").Image = "boton_excel"
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
   End With

   Call Sub_CargaForm
   
   Me.Top = 1
   Me.Left = 1
End Sub

Private Sub Form_Resize()
   Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub


Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "RESEARCH"
      Call Sub_CargarDatos
    Case "XLS"
      Call Fnt_Excel
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      Call Sub_Limpiar
  End Select
End Sub

Private Sub Sub_CargaForm()
   'Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas)
   Call Sub_Limpiar
End Sub

Private Sub Sub_Limpiar()
  
  Grilla.Rows = 1
  
  DT_fechahasta.Value = Fnt_FechaServidor
  DT_fechadesde.Value = DT_fechahasta.Value - 30
  
  'Cmb_Cuentas.Text = ""
  Cmb_Cajas.Text = ""
  Txt_Num_Cuenta.Text = ""
  Txt_Num_Cuenta.Tag = ""
  Txt_Cliente.Text = ""
  Txt_Descripcion_Cta.Text = ""
  txtNombreAsesor.Text = ""
  
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
'--------------------------------------
Dim lcMov_Caja As Class_Mov_Caja
Dim lcSaldos_Caja As Class_Saldos_Caja
Dim lCajas_Ctas As Class_Cajas_Cuenta
'--------------------------------------
Dim lLinea As Long
Dim lFecDesde As Date
Dim lFecHasta As Date
Dim lId_Cuenta As String
Dim lId_Caja_Cuenta As String
Dim lTotal As Double
Dim lIngreso As Double
Dim lEgreso As Double
Dim lUltimaCaja As Long
   
  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Form_Validar(Me) Then
    'MsgBox "Debe seleccionar una Cuenta para buscar Movimientos de Caja.", vbInformation, Me.Caption
    GoTo ErrProcedure
  End If
  
  If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
    If Not fFechaCierreVirtual = "" Then
        If DT_fechahasta > fFechaCierreVirtual Then
            DT_fechahasta.Value = fFechaCierreVirtual
        End If
    End If
  End If
  
  lFecDesde = DT_fechadesde.Value
  lFecHasta = DT_fechahasta.Value
   
  If lFecDesde > lFecHasta Then
    MsgBox "La Fecha Desde debe ser menor que Fecha Hasta.", vbExclamation, Me.Caption
    GoTo ErrProcedure
  End If
    
  Rem Buscar datos del Cliente
  'lCuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Cuentas))
  lId_Cuenta = Txt_Num_Cuenta.Tag
   
  Rem Limpia la Grilla
  Grilla.Rows = 1
  
  lUltimaCaja = 0
  lIngreso = 0
  lEgreso = 0
  lTotal = 0
  lLinea = 0
  
  lId_Caja_Cuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cajas) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Cajas))
  
  Set lcMov_Caja = New Class_Mov_Caja
  With lcMov_Caja
    If Not lId_Caja_Cuenta = "" Then
      .Campo("Id_Caja_Cuenta").Valor = lId_Caja_Cuenta
    End If
    If .Buscar_Movimientos_Cuenta(lId_Cuenta, lFecDesde, lFecHasta) Then
      For Each lReg In .Cursor
        If lUltimaCaja = lReg("Id_Caja_Cuenta").Value Then
          lLinea = Grilla.Rows
          Grilla.AddItem ""
          Call SetCell(Grilla, lLinea, "Fecha_Movimiento", lReg("Fecha_Movimiento").Value)
          Call SetCell(Grilla, lLinea, "Fecha_Liquidacion", lReg("Fecha_Liquidacion").Value)
          Call SetCell(Grilla, lLinea, "dsc_Origen_Mov_Caja", lReg("dsc_Origen_Mov_Caja").Value)
          Call SetCell(Grilla, lLinea, "Dsc_Mov_Caja", lReg("Dsc_Mov_Caja").Value)
          If lReg("FLG_Tipo_Movimiento").Value = gcTipoOperacion_Abono Then
            Call SetCell(Grilla, lLinea, "Ingresos", lReg("Monto").Value)
            lTotal = lTotal + lReg("Monto").Value
            lIngreso = lIngreso + lReg("Monto").Value
          Else
            Call SetCell(Grilla, lLinea, "Egresos", lReg("Monto").Value)
            lTotal = lTotal - lReg("Monto").Value
            lEgreso = lEgreso + lReg("Monto").Value
          End If
          Call SetCell(Grilla, lLinea, "Saldo", lTotal)
        Else
          If Not lUltimaCaja = 0 Then
            lLinea = Grilla.Rows
            Grilla.AddItem ""
            Call SetCell(Grilla, lLinea, "Dsc_Mov_Caja", "SALDO CAJA")
            Call SetCell(Grilla, lLinea, "Ingresos", lIngreso)
            Call SetCell(Grilla, lLinea, "Egresos", lEgreso)
            Call SetCell(Grilla, lLinea, "Saldo", lTotal)
            Grilla.Cell(flexcpBackColor, lLinea, 0, lLinea, 6) = &HC0FFC0
            
            lIngreso = 0
            lEgreso = 0
            lTotal = 0
          End If
          
          lLinea = Grilla.Rows
          Grilla.AddItem ""
          
          Call UnionCell(Grilla, Grilla.Rows - 1, 0, 6, UCase(lReg("DSC_CAJA_CUENTA").Value), "&HC0FFFF", True)
          
          lUltimaCaja = lReg("Id_Caja_Cuenta").Value
          
          'Buscar Saldo Anterior por Caja
          Set lcSaldos_Caja = New Class_Saldos_Caja
          With lcSaldos_Caja
            .Campo("id_Caja_Cuenta").Valor = lUltimaCaja
            .Campo("Fecha_cierre").Valor = lFecDesde
            If .Buscar_Saldos_Cuenta(lTotal, lId_Cuenta) Then
              'Total = .Parametros("Saldo").Valor
              lLinea = Grilla.Rows
              Grilla.AddItem ""
              Call SetCell(Grilla, lLinea, "Fecha_Movimiento", lFecDesde - 1)
              Call SetCell(Grilla, lLinea, "dsc_Origen_Mov_Caja", "SALDO")
              Call SetCell(Grilla, lLinea, "Dsc_Mov_Caja", "Saldo Anterior")
              Call SetCell(Grilla, lLinea, "Saldo", lTotal)
            Else
              Call Fnt_MsgError(.SubTipo_LOG, _
                                "Problemas en cargar Valores Cuota.", _
                                .ErrMsg, _
                                pConLog:=True)
              lTotal = 0
            End If
          End With
          Set lcSaldos_Caja = Nothing
          
          lLinea = Grilla.Rows
          Grilla.AddItem ""
          Call SetCell(Grilla, lLinea, "Fecha_Movimiento", lReg("Fecha_Movimiento").Value)
          Call SetCell(Grilla, lLinea, "Fecha_Liquidacion", lReg("Fecha_Liquidacion").Value)
          Call SetCell(Grilla, lLinea, "dsc_Origen_Mov_Caja", lReg("dsc_Origen_Mov_Caja").Value)
          Call SetCell(Grilla, lLinea, "Dsc_Mov_Caja", lReg("Dsc_Mov_Caja").Value)
          If lReg("FLG_Tipo_Movimiento").Value = gcTipoOperacion_Abono Then
            Call SetCell(Grilla, lLinea, "Ingresos", lReg("Monto").Value)
            lTotal = lTotal + lReg("Monto").Value
            lIngreso = lIngreso + lReg("Monto").Value
          Else
            Call SetCell(Grilla, lLinea, "Egresos", lReg("Monto").Value)
            lTotal = lTotal - lReg("Monto").Value
            lEgreso = lEgreso + lReg("Monto").Value
          End If
          Call SetCell(Grilla, lLinea, "Saldo", lTotal)
'          Call SetCell(Grilla, lLinea, "Caja", lReg("DSC_CAJA_CUENTA").Value)
        End If
      Next
      If lLinea = 0 Then
        Set lCajas_Ctas = New Class_Cajas_Cuenta
        With lCajas_Ctas
            .Campo("id_cuenta").Valor = lId_Cuenta
            If .Buscar Then
                For Each lReg In .Cursor
                    Set lcSaldos_Caja = New Class_Saldos_Caja
                    With lcSaldos_Caja
                      .Campo("id_Caja_Cuenta").Valor = lReg("Id_Caja_Cuenta").Value
                      .Campo("Fecha_cierre").Valor = lFecDesde
                      If .Buscar_Saldos_Cuenta(lTotal, lId_Cuenta) Then
                        If lTotal <> 0 Then
                            lLinea = Grilla.Rows
                            Grilla.AddItem ""
                            Call UnionCell(Grilla, Grilla.Rows - 1, 0, 6, UCase(lReg("DSC_CAJA_CUENTA").Value), "&HC0FFFF", True)
                            lLinea = Grilla.Rows
                            Grilla.AddItem ""
                            
                            Call SetCell(Grilla, lLinea, "Fecha_Movimiento", lFecDesde - 1)
                            Call SetCell(Grilla, lLinea, "dsc_Origen_Mov_Caja", "SALDO")
                            Call SetCell(Grilla, lLinea, "Dsc_Mov_Caja", "Saldo Anterior")
                            Call SetCell(Grilla, lLinea, "Saldo", lTotal)
                            lLinea = Grilla.Rows
                            Grilla.AddItem ""
                            Call SetCell(Grilla, lLinea, "Dsc_Mov_Caja", "SALDO CAJA")
                            Call SetCell(Grilla, lLinea, "Ingresos", lIngreso)
                            Call SetCell(Grilla, lLinea, "Egresos", lEgreso)
                            Call SetCell(Grilla, lLinea, "Saldo", lTotal)
                            Grilla.Cell(flexcpBackColor, lLinea, 0, lLinea, 6) = &HC0FFC0
                        End If
                      Else
                        Call Fnt_MsgError(.SubTipo_LOG, _
                                          "Problemas en cargar Caja dia Anterior.", _
                                          .ErrMsg, _
                                          pConLog:=True)
                        lTotal = 0
                      End If
                    End With
                    Set lcSaldos_Caja = Nothing
                Next
            End If
        End With
      Else
        lLinea = Grilla.Rows
        Grilla.AddItem ""
        Call SetCell(Grilla, lLinea, "Dsc_Mov_Caja", "SALDO CAJA")
        Call SetCell(Grilla, lLinea, "Ingresos", lIngreso)
        Call SetCell(Grilla, lLinea, "Egresos", lEgreso)
        Call SetCell(Grilla, lLinea, "Saldo", lTotal)
        Grilla.Cell(flexcpBackColor, lLinea, 0, lLinea, 6) = &HC0FFC0
      End If
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Valores Cuota.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcMov_Caja = Nothing
   
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
   
End Sub

Public Sub Fnt_Excel()
   On Error GoTo Errores
   
   If Grilla.Rows > 0 Then
      cmmExcel.CancelError = False
      
      cmmExcel.ShowSave
      
      If Not cmmExcel.Filename = "" Then
         Grilla.SaveGrid cmmExcel.Filename, flexFileExcel
         Grilla.SaveGrid cmmExcel.Filename, flexFileExcel, "Test_Banco"
         Grilla.SaveGrid cmmExcel.Filename, flexFileExcel, flexXLSaveFixedCells
         Grilla.SaveGrid cmmExcel.Filename, flexFileExcel, flexXLSaveFixedRows
      Else
         MsgBox "Se cancel� la operaci�n.", vbOKOnly + vbCritical, Me.Caption
      End If
   Else
      MsgBox "No existen datos para grabar.", vbOKOnly + vbCritical, Me.Caption
   End If
   
   Exit Sub
   
Errores:
   If Err.Number = 70 Then
      Dim Resp As Long
      Resp = MsgBox("El archivo " & cmmExcel.Filename & ", se encuentra abierto, Desea sobreescribir?.", vbYesNo + vbCritical, Me.Caption)
      If Resp = vbYes Then
         Resume
      End If
   End If
End Sub

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       
      Call cmb_buscar_Cuenta_Click
       
    End If
End Sub

Private Sub Sub_MostrarCuenta(pCuenta, Optional pId As Boolean = True)
Dim lcursor As hRecord
Dim lError  As String
Dim lexiste As Boolean

    Call Sub_Entrega_DatosCuenta_2(pCuenta, pId, lcursor, lError, lexiste)
    If lexiste Then
        Txt_Num_Cuenta.Text = lcursor(1)("num_cuenta").Value
        Txt_Num_Cuenta.Tag = lcursor(1)("id_cuenta").Value
        Txt_Rut.Text = lcursor(1)("rut_cliente").Value
        Txt_Descripcion_Cta.Text = lcursor(1)("Abr_cuenta").Value
        Txt_Cliente.Text = lcursor(1)("nombre_cliente").Value
        txtNombreAsesor.Text = lcursor(1)("nombre_asesor").Value
        Call Sub_CargarDatos
    End If
  
End Sub

Public Sub Sub_Entrega_DatosCuenta_2(ByVal pCuenta, pId As Boolean, ByRef pCursor As hRecord, ByRef PError, ByRef pExiste)
Dim lcCuenta As Object
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
        If pId Then
            .Campo("id_cuenta").Valor = pCuenta
        Else
            .Campo("num_cuenta").Valor = pCuenta
        End If
        If .Buscar_Vigentes Then
            If .Cursor.Count = 1 Then
                Set pCursor = .Cursor
                pExiste = True
            Else
                pExiste = False
            End If
        Else
            PError = .ErrMsg
        End If
    End With
    Set lcCuenta = Nothing
End Sub




