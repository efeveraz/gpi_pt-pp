VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Empresa 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Empresa"
   ClientHeight    =   3435
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7515
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3435
   ScaleWidth      =   7515
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   7515
      _ExtentX        =   13256
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin C1SizerLibCtl.C1Tab STab 
      Height          =   2955
      Left            =   60
      TabIndex        =   6
      Top             =   420
      Width           =   7395
      _cx             =   13044
      _cy             =   5212
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   6579300
      ForeColor       =   -2147483634
      FrontTabColor   =   -2147483635
      BackTabColor    =   6579300
      TabOutlineColor =   -2147483632
      FrontTabForeColor=   -2147483634
      Caption         =   "&General|&Comisiones|&Tipos de Cambio|&Instrumento/Publicador|&Cartola Campa�a"
      Align           =   0
      CurrTab         =   4
      FirstTab        =   0
      Style           =   6
      Position        =   0
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   0   'False
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   -1  'True
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   0
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   -1  'True
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   2610
         Left            =   -8880
         ScaleHeight     =   2610
         ScaleWidth      =   7365
         TabIndex        =   12
         Top             =   330
         Width           =   7365
         Begin VB.Frame Frame1 
            Caption         =   "Cuenta Bancaria"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   2505
            Left            =   90
            TabIndex        =   13
            Top             =   30
            Width           =   7185
            Begin VB.CheckBox Chk_Cortes_RF 
               Caption         =   "Administra Cortes Renta Fija"
               Height          =   375
               Left            =   4095
               TabIndex        =   19
               Top             =   1890
               Width           =   2835
            End
            Begin VB.CheckBox Chk_Bloqueo_Cuentas 
               Caption         =   "Bloqueo de Cuentas en Cierre"
               Height          =   375
               Left            =   180
               TabIndex        =   18
               Top             =   1890
               Width           =   2835
            End
            Begin hControl2.hTextLabel Txt_Cta_Cte 
               Height          =   315
               Left            =   180
               TabIndex        =   1
               Top             =   690
               Width           =   6810
               _ExtentX        =   12012
               _ExtentY        =   556
               LabelWidth      =   1300
               TextMinWidth    =   1200
               Caption         =   "Cuenta Corriente"
               Text            =   ""
               MaxLength       =   59
            End
            Begin TrueDBList80.TDBCombo Cmb_Bancos 
               Height          =   345
               Left            =   1500
               TabIndex        =   0
               Top             =   300
               Width           =   3735
               _ExtentX        =   6588
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   0   'False
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Empresa.frx":0000
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin hControl2.hTextLabel txt_mail_backfofice 
               Height          =   315
               Left            =   180
               TabIndex        =   16
               Tag             =   "OBLI=S;CAPTION=Mail BackOffice"
               Top             =   1080
               Width           =   6810
               _ExtentX        =   12012
               _ExtentY        =   556
               LabelWidth      =   1300
               TextMinWidth    =   1200
               Caption         =   "Mail BackOffice"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               MaxLength       =   59
            End
            Begin hControl2.hTextLabel Txt_Limite_Caja 
               Height          =   315
               Left            =   180
               TabIndex        =   17
               Tag             =   "OBLI=S;CAPTION=LimiteCaja"
               Top             =   1470
               Width           =   3000
               _ExtentX        =   5292
               _ExtentY        =   556
               LabelWidth      =   1300
               TextMinWidth    =   1200
               Caption         =   "Limite Caja"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Tipo_TextBox    =   1
               MaxLength       =   59
            End
            Begin hControl2.hTextLabel Txt_FactorDctoIngPeso 
               Height          =   315
               Left            =   3360
               TabIndex        =   20
               Top             =   1470
               Width           =   3135
               _ExtentX        =   5530
               _ExtentY        =   556
               LabelWidth      =   2600
               TextMinWidth    =   300
               Caption         =   "Factor Descuento Ingreso x Monto"
               Text            =   ",00"
               Text            =   ",00"
               Format          =   "#.00"
               Tipo_TextBox    =   1
               MaxLength       =   59
            End
            Begin VB.Label Label1 
               Caption         =   "%"
               Height          =   255
               Left            =   6600
               TabIndex        =   21
               Top             =   1560
               Width           =   375
            End
            Begin VB.Label Label4 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Banco"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Left            =   180
               TabIndex        =   14
               Top             =   300
               Width           =   1305
            End
         End
      End
      Begin VB.PictureBox Picture2 
         BorderStyle     =   0  'None
         Height          =   2610
         Left            =   -8280
         ScaleHeight     =   2610
         ScaleWidth      =   7365
         TabIndex        =   11
         Top             =   330
         Width           =   7365
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Cambios 
            Height          =   2415
            Left            =   90
            TabIndex        =   3
            Top             =   90
            Width           =   7125
            _cx             =   12568
            _cy             =   4260
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   9
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Empresa.frx":00AA
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin VB.PictureBox Picture3 
         BorderStyle     =   0  'None
         Height          =   2610
         Left            =   15
         ScaleHeight     =   2610
         ScaleWidth      =   7365
         TabIndex        =   10
         Top             =   330
         Width           =   7365
         Begin VB.Frame Frame2 
            Caption         =   "Directorio Cartola Campa�a"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   2505
            Left            =   90
            TabIndex        =   22
            Top             =   30
            Width           =   7185
            Begin hControl2.hTextLabel Txt_Ruta_Cartola_Camp 
               Height          =   330
               Left            =   240
               TabIndex        =   23
               Top             =   360
               Width           =   6570
               _ExtentX        =   11589
               _ExtentY        =   582
               LabelWidth      =   1000
               TextMinWidth    =   1200
               Caption         =   "Ruta"
               Text            =   ""
               MaxLength       =   20
            End
            Begin VB.Label Label2 
               Caption         =   "(Max. 20 caracteres)"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   5280
               TabIndex        =   24
               Top             =   765
               Width           =   1575
            End
         End
      End
      Begin VB.PictureBox Picture4 
         BorderStyle     =   0  'None
         Height          =   2610
         Left            =   -8580
         ScaleHeight     =   2610
         ScaleWidth      =   7365
         TabIndex        =   9
         Top             =   330
         Width           =   7365
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Comisiones 
            Height          =   2415
            Left            =   90
            TabIndex        =   2
            Top             =   90
            Width           =   7125
            _cx             =   12568
            _cy             =   4260
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   5
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Empresa.frx":026A
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin VB.PictureBox Picture5 
         BorderStyle     =   0  'None
         Height          =   2610
         Left            =   -7980
         ScaleHeight     =   2610
         ScaleWidth      =   7365
         TabIndex        =   8
         Top             =   330
         Width           =   7365
         Begin VSFlex8LCtl.VSFlexGrid Grilla_InstrPublic 
            Height          =   2415
            Left            =   90
            TabIndex        =   15
            Top             =   90
            Width           =   7125
            _cx             =   12568
            _cy             =   4260
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   4
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Empresa.frx":0363
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin VB.PictureBox TabAlias 
         BorderStyle     =   0  'None
         Height          =   2610
         Left            =   8010
         ScaleHeight     =   2610
         ScaleWidth      =   7365
         TabIndex        =   7
         Top             =   330
         Width           =   7365
      End
   End
End
Attribute VB_Name = "Frm_Empresa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Rem --------------------------------------------------------
Rem 06/01/2009 Multiempresa
Rem 17/07/2009 M.Mardones. Agrega Factor Ingreso $$ para
Rem            Ingreso de Acciones por Monto.
Rem --------------------------------------------------------

Public fKey As String

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String
Dim cOrigen_Dsc As String
Dim cTipo_Conversion_Dsc As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  Call Fnt_Modificar(pCod_Arbol_Sistema)
End Sub


Private Sub Cmb_Bancos_GotFocus()
  STab.CurrTab = 0
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub


Public Function Fnt_Modificar(pCod_Arbol_Sistema)
  fKey = Fnt_EmpresaActual

  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Call Sub_CargarDatos

  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Cambios_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  With Grilla_Cambios
    If Not .ColIndex("Seleccion") = Col Then
      Cancel = True
    End If
  End With
End Sub

Private Sub Grilla_Cambios_GotFocus()
  STab.CurrTab = 2
End Sub

Private Sub Grilla_Click()

End Sub

Private Sub Grilla_Comisiones_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Grilla_Comisiones.ColIndex("comision") = Col Then
    Cancel = True
  End If
End Sub

Private Sub Grilla_Comisiones_GotFocus()
  STab.CurrTab = 1
End Sub

Private Sub Grilla_InstrPublic_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  Select Case Col
    Case Grilla_InstrPublic.ColIndex("DSC_INSTRUMENTO")
      Cancel = True
  End Select
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "SAVE"
      Call Sub_Grabar
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargaForm
        Call Sub_CargarDatos
      End If
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Grabar()
Dim lcComision_Emp_Inst As Class_Comisiones_Emp_Inst
Dim lcEmpresa           As Class_Empresas
Dim lcRel_Empresa_Tipo_Cambio As Class_Rel_Empresa_Tipo_Cambio
'--------------------------------------------------------
Dim lLinea As Long
Dim lBanco As String
Dim lFila As Long
'--------------------------------------------------------
Dim lResult As Boolean

  lResult = True
  gDB.IniciarTransaccion
  
  If Not Fnt_ValidarDatos Then
    lResult = False
    GoTo ErrProcedure
  End If
    
  lBanco = Fnt_ComboSelected_KEY(Cmb_Bancos)
  
  If Not Fnt_Valida_Ruta_Cartola_Camp Then
    lResult = False
    GoTo ErrProcedure
  End If
  
  Set lcEmpresa = New Class_Empresas
  With lcEmpresa
    .Campo("ID_EMPRESA").Valor = fKey
    .Campo("cta_cte").Valor = Txt_Cta_Cte.Text
    If Not lBanco = "" Then
      .Campo("id_banco").Valor = lBanco
    End If
    .Campo("limite_caja").Valor = Txt_Limite_Caja.Text
    .Campo("flg_bloqueo_cuentas").Valor = IIf(Chk_Bloqueo_Cuentas.Value, gcFlg_SI, gcFlg_NO)
    .Campo("flg_cortes_rf").Valor = IIf(Chk_Cortes_RF.Value, gcFlg_SI, gcFlg_NO)
    '17/07/2009 agregado por MMardones.
    .Campo("factor_dcto_ingreso_peso").Valor = Txt_FactorDctoIngPeso.Text
    .Campo("ruta_cartola_camp").Valor = Txt_Ruta_Cartola_Camp.Text
    If Not .Guardar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en grabar datos de la Empresa.", _
                        .ErrMsg, _
                        pConLog:=True)
      lResult = False
      GoTo ErrProcedure
    End If
  End With
  Set lcEmpresa = Nothing
    
  For lLinea = 1 To Grilla_Comisiones.Rows - 1
    Set lcComision_Emp_Inst = New Class_Comisiones_Emp_Inst
    With lcComision_Emp_Inst
      .Campo("ID_COMISION_EMPRESA").Valor = GetCell(Grilla_Comisiones, lLinea, "id_comision")
      
      If Not GetCell(Grilla_Comisiones, lLinea, "comision") = "" Then
        '.Campo("ID_EMPRESA").Valor = fKey
        .Campo("ID_EMPRESA").Valor = GetCell(Grilla_Comisiones, lLinea, "id_empresa")
        .Campo("Cod_Instrumento").Valor = GetCell(Grilla_Comisiones, lLinea, "cod_instrumento")
        .Campo("COMISION").Valor = GetCell(Grilla_Comisiones, lLinea, "comision")
        
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                      "Problemas en grabar los Porcentajes de las Comisiones.", _
                      .ErrMsg, _
                      pConLog:=True)
          lResult = False
          GoTo ErrProcedure
        End If
      Else
        If Not .Borrar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                      "Problemas al eliminar los Porcentajes de las Comisiones.", _
                      .ErrMsg, _
                      pConLog:=True)
          lResult = False
          GoTo ErrProcedure
        End If
      End If
    End With
  Next
  
  Rem Se eliminan registros de REL_EMPRESA_TIPO_CAMBIO por Empresa
  Set lcRel_Empresa_Tipo_Cambio = New Class_Rel_Empresa_Tipo_Cambio
  With lcRel_Empresa_Tipo_Cambio
    .Campo("id_empresa").Valor = fKey
    If Not .Borrar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al eliminar la relacion Empresa/Tipo Cambio.", _
                        .ErrMsg, _
                        pConLog:=True)
      lResult = False
      GoTo ErrProcedure
    End If
    
    .LimpiaParam
    
    For lFila = 1 To (Grilla_Cambios.Rows - 1)
      If Grilla_Cambios.Cell(flexcpChecked, lFila, 3) = flexChecked Then
        .LimpiaParam
        '.Campo("id_empresa").Valor = fKey     'Modificado MMA 06/01/2009
        .Campo("id_empresa").Valor = GetCell(Grilla_Cambios, lFila, "id_empresa")
        .Campo("id_Tipo_Cambio").Valor = GetCell(Grilla_Cambios, lFila, "id_tipo_cambio")
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en guardar la relaci�n Empresa/Tipo Cambio.", _
                        .ErrMsg, _
                        pConLog:=True)
          lResult = False
          GoTo ErrProcedure
        End If
      End If
    Next lFila
  End With
  Set lcRel_Empresa_Tipo_Cambio = Nothing
    
  If Not Fnt_Graba_InstrucPublic Then
    lResult = False
    GoTo ErrProcedure
  End If
  
  If Not Fnt_Guardar_Rel_Mail_BackOffice(gId_Empresa, txt_mail_backfofice.Text) Then
    lResult = False
    GoTo ErrProcedure
  End If
    
ErrProcedure:
  If lResult Then
    gDB.CommitTransaccion
    MsgBox "Empresa guardada correctamente.", vbInformation, Me.Caption
  Else
    gDB.RollbackTransaccion
  End If
  
End Sub

Private Function Fnt_Graba_InstrucPublic() As Boolean
Dim lReg    As hCollection.hFields
Dim lFila As Long
Dim lRollback As Boolean
Dim lCant As Long
Dim lID As String
Dim lcRel_Empresa_Instrum_Public As Class_Rel_Empresa_Instrum_Public

  Fnt_Graba_InstrucPublic = False
  
  For lFila = 1 To (Grilla_InstrPublic.Rows - 1)
    If Not Trim(GetCell(Grilla_InstrPublic, lFila, "Dsc_Publicador")) = "" Then
      Set lcRel_Empresa_Instrum_Public = New Class_Rel_Empresa_Instrum_Public
      With lcRel_Empresa_Instrum_Public
        '.Campo("Id_Empresa").Valor = Fnt_EmpresaActual     'Modificado MMA 06/01/2009
        .Campo("id_empresa").Valor = GetCell(Grilla_InstrPublic, lFila, "id_empresa")
        .Campo("Id_Publicador").Valor = GetCell(Grilla_InstrPublic, lFila, "Dsc_Publicador")
        .Campo("Cod_Instrumento").Valor = GetCell(Grilla_InstrPublic, lFila, "colum_pk")
        lCant = lCant + 1
        If Not .Guardar Then
          MsgBox .ErrMsg, vbCritical, Me.Caption
          GoTo ExitProcedure
        End If
      End With
      Set lcRel_Empresa_Instrum_Public = Nothing
    Else
      lID = GetCell(Grilla_InstrPublic, lFila, "colum_pk")
      If Not lID = "" Then
        Set lcRel_Empresa_Instrum_Public = New Class_Rel_Empresa_Instrum_Public
        With lcRel_Empresa_Instrum_Public
          '.Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual    'Modificado MMA 06/01/2009
          .Campo("id_empresa").Valor = GetCell(Grilla_InstrPublic, lFila, "id_empresa")
          .Campo("Cod_Instrumento").Valor = lID
          If Not .Borrar Then
            MsgBox .ErrMsg, vbCritical, Me.Caption
            GoTo ExitProcedure
          End If
          
          lCant = lCant + 1
        End With
        Set lcRel_Empresa_Instrum_Public = Nothing
      End If
    End If
  Next
  
  If lCant = 0 Then
    MsgBox "No hay registro a grabar.", vbInformation, Me.Caption
  End If
  
  Fnt_Graba_InstrucPublic = True
  
ExitProcedure:

End Function

Private Sub Sub_CargaForm()
Dim lcInstrumentos As Class_Instrumentos
'Dim lcTipo_Cambio As Class_Tipo_Cambios
Dim lcTipo_Cambio As Object
'---------------------------------------------
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lComboList As String

  Call Sub_Bloquea_Puntero(Me)
  
  STab.CurrTab = 0
  Call Sub_FormControl_Color(Me.Controls)
  Call Sub_CargaCombo_Bancos(Cmb_Bancos, pBlanco:=True)

  Grilla_Comisiones.Rows = 1
  Set lcInstrumentos = New Class_Instrumentos
  With lcInstrumentos
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Comisiones.Rows
        Call Grilla_Comisiones.AddItem("")
        Call SetCell(Grilla_Comisiones, lLinea, "cod_instrumento", lReg("cod_instrumento").Value, False)
        Call SetCell(Grilla_Comisiones, lLinea, "dsc_instrumentos", lReg("dsc_intrumento").Value, False)
        Call SetCell(Grilla_Comisiones, lLinea, "id_comision", cNewEntidad, False)
        Call SetCell(Grilla_Comisiones, lLinea, "id_empresa", fKey, False)
      Next
    End If
  End With
  Set lcInstrumentos = Nothing
  
'  Set lcTipo_Cambio = New Class_Tipo_Cambios
  Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
  With lcTipo_Cambio
    If .Buscar Then
      lComboList = ""
      Grilla_Cambios.Rows = 1
      For Each lReg In .Cursor
        Grilla_Cambios.AddItem ""
        lLinea = Grilla_Cambios.Rows - 1
        
        Call SetCell(Grilla_Cambios, lLinea, "Id_Tipo_Cambio", lReg("ID_TIPO_CAMBIO").Value)
        Grilla_Cambios.Cell(flexcpChecked, lLinea, 2) = flexUnchecked
        Call SetCell(Grilla_Cambios, lLinea, "Id_Moneda_Fuente", lReg("Id_Moneda_Fuente").Value)
        Call SetCell(Grilla_Cambios, lLinea, "dsc_Moneda_Fuente", lReg("dsc_Moneda_Fuente").Value)
        Call SetCell(Grilla_Cambios, lLinea, "Id_Moneda_Destino", lReg("Id_Moneda_Destino").Value)
        Call SetCell(Grilla_Cambios, lLinea, "dsc_Moneda_Destino", lReg("dsc_Moneda_Destino").Value)
        Call SetCell(Grilla_Cambios, lLinea, "dsc_Tipo_Cambio", lReg("dsc_Tipo_Cambio").Value)
        Call SetCell(Grilla_Cambios, lLinea, "dsc_Operacion", lReg("Operacion").Value)
      Next
     End If
  End With
  Grilla_Cambios.ColComboList(Grilla_Cambios.ColIndex("dsc_tipo_cambio")) = lComboList
  Set lcTipo_Cambio = Nothing
  
  Call Sub_CargarForm_IntrPublic
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargarForm_IntrPublic()
Dim lComboList As String
Dim lReg As hFields
Dim lLinea As Long
Dim lPublicador As Class_Publicadores

  Rem Limpia la Grilla_InstrPublic
  Grilla_InstrPublic.Rows = 1
  
  gDB.Parametros.Clear
  Grilla_InstrPublic.ColComboList(Grilla_InstrPublic.ColIndex("dsc_publicador")) = lComboList
  
  Set lPublicador = New Class_Publicadores
  With lPublicador
    If .Buscar Then
      lComboList = ""
      For Each lReg In .Cursor
        lComboList = lComboList & "|#" & lReg("Id_publicador").Value
  
        If lReg.Index = 1 Then
          lComboList = lComboList & "*1"
        End If
  
        lComboList = lComboList & ";" & lReg("Id_publicador").Value & vbTab & lReg("dsc_publicador").Value
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lPublicador = Nothing
  
  Grilla_InstrPublic.ColComboList(Grilla_InstrPublic.ColIndex("dsc_moneda")) = lComboList
End Sub

Private Sub Sub_CargarDatos()
Dim lcComision_Emp_Inst As Class_Comisiones_Emp_Inst
Dim lcEmpresa           As Class_Empresas
Dim lcRel_Empresa_Tipo_Cambio As Class_Rel_Empresa_Tipo_Cambio
'----------------------------------------------
Dim lReg As hCollection.hFields
Dim lLinea As Long
  
cOrigen_Dsc = "BACKOFFICE"
cTipo_Conversion_Dsc = "MAIL"
  
  Call Sub_Bloquea_Puntero(Me)
  
  Set lcEmpresa = New Class_Empresas
  With lcEmpresa
    .Campo("id_empresa").Valor = fKey
    If .Buscar Then
      If .Cursor.Count > 0 Then
        Call Sub_ComboSelectedItem(Cmb_Bancos, NVL(.Cursor(1)("id_banco").Value, cCmbBLANCO))
        Txt_Cta_Cte.Text = "" & .Cursor(1)("CTA_CTE").Value
        Txt_Limite_Caja.Text = "" & Format(.Cursor(1)("LIMITE_CAJA").Value, "#,##0")
        Chk_Bloqueo_Cuentas.Value = IIf(.Cursor(1)("FLG_BLOQUEO_CUENTAS").Value = gcFlg_SI, 1, 0)
        Chk_Cortes_RF.Value = IIf(.Cursor(1)("FLG_CORTES_RF").Value = gcFlg_SI, 1, 0)
        Txt_Ruta_Cartola_Camp.Text = "" & .Cursor(1)("RUTA_CARTOLA_CAMP").Value
        '17/07/2009 Agregado por MMardones
        Txt_FactorDctoIngPeso.Text = FormatNumber(NVL(.Cursor(1)("factor_dcto_ingreso_peso").Value, 0), 2)
      End If
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcEmpresa = Nothing
  
  Set lcComision_Emp_Inst = New Class_Comisiones_Emp_Inst
  With lcComision_Emp_Inst
    '.Campo("id_empresa").Valor = fKey   'modificado MMA 06/01/2009
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Comisiones.FindRow("" & lReg("cod_instrumento").Value, , Grilla_Comisiones.ColIndex("cod_instrumento"))
        
        If lLinea >= 1 Then
          Call SetCell(Grilla_Comisiones, lLinea, "comision", lReg("comision").Value)
          Call SetCell(Grilla_Comisiones, lLinea, "id_comision", lReg("ID_COMISION_EMPRESA").Value)
          Call SetCell(Grilla_Comisiones, lLinea, "id_empresa", lReg("ID_EMPRESA").Value)    'Agregado MMA. 06/01/2008
        End If
      Next
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcComision_Emp_Inst = Nothing
  
  Set lcRel_Empresa_Tipo_Cambio = New Class_Rel_Empresa_Tipo_Cambio
  'lcRel_Empresa_Tipo_Cambio.Campo("id_empresa").Valor = fKey    'Modificado MMA 06/01/2009
  If lcRel_Empresa_Tipo_Cambio.Buscar Then
    With Grilla_Cambios
      For Each lReg In lcRel_Empresa_Tipo_Cambio.Cursor
        lLinea = .FindRow(lReg("ID_TIPO_CAMBIO").Value, , .ColIndex("Id_Tipo_Cambio"))
        If lLinea > -1 Then
          .Cell(flexcpChecked, lLinea, .ColIndex("Seleccion")) = flexChecked
          Call SetCell(Grilla_Cambios, lLinea, "id_empresa", lReg("id_empresa").Value)        'Agregado MMA 06/01/2009
        End If
      Next
    End With
  Else
   Call Fnt_MsgError(lcRel_Empresa_Tipo_Cambio.SubTipo_LOG, _
                     "Problemas en cargar la relaci�n de Empresa/Tipo de Cambio", _
                     lcRel_Empresa_Tipo_Cambio.ErrMsg, _
                     pConLog:=True)
   End If
  Set lcRel_Empresa_Tipo_Cambio = Nothing
  
  Call Sub_CargarDatos_InstruPublic
  txt_mail_backfofice.Text = Fnt_Lee_Mail_BackOffice(gId_Empresa)
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargarDatos_InstruPublic()
Dim lcInstrumento As Class_Instrumentos
Dim lc_Rel_Empresa_Instrum_Public As Class_Rel_Empresa_Instrum_Public
'----------------------------------------------------
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lcod_inst As String

  If Grilla_InstrPublic.Row > 0 Then
    lID = GetCell(Grilla_InstrPublic, Grilla_InstrPublic.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Grilla_InstrPublic.Rows = 1
    
  Set lcInstrumento = New Class_Instrumentos
  With lcInstrumento
      If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_InstrPublic.Rows
        Call Grilla_InstrPublic.AddItem("")
        Call SetCell(Grilla_InstrPublic, lLinea, "colum_pk", NVL(lReg("Cod_Instrumento").Value, ""))
        Call SetCell(Grilla_InstrPublic, lLinea, "DSC_INSTRUMENTO", NVL(lReg("DSC_INTRUMENTO").Value, ""))
      Next
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcInstrumento = Nothing
  
  Set lc_Rel_Empresa_Instrum_Public = New Class_Rel_Empresa_Instrum_Public
  With lc_Rel_Empresa_Instrum_Public
    .Campo("id_empresa").Valor = Fnt_EmpresaActual     'modificado MMA 06/01/2009
    If .Buscar Then
      For Each lReg In .Cursor
        Grilla_InstrPublic.Row = Grilla_InstrPublic.FindRow(lReg("Cod_Instrumento").Value, , Grilla_InstrPublic.ColIndex("colum_pk"))
        If Not Grilla_InstrPublic.Row = cNewEntidad Then
          Call SetCell(Grilla_InstrPublic, Grilla_InstrPublic.Row, "DSC_PUBLICADOR", lReg("Id_publicador").Value)
          Call SetCell(Grilla_InstrPublic, Grilla_InstrPublic.Row, "id_empresa", lReg("id_empresa").Value)
        End If
      Next
    Else
      MsgBox "Problemas en cargar" & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lc_Rel_Empresa_Instrum_Public = Nothing
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function

Private Sub Txt_Cta_Cte_GotFocus()
  STab.CurrTab = 0
End Sub

Private Function Fnt_Guardar_Rel_Mail_BackOffice(pId_Empresa, pMail_BackOffice As String) As Boolean
  Dim lId_Origen            As String
  Dim lId_Tipo_Conversion   As String
  Dim lcRel_Conversiones    As Object
  Dim pMsg_Error            As String

  Fnt_Guardar_Rel_Mail_BackOffice = True
  pMsg_Error = ""
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_Dsc)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTipo_Conversion_Dsc)
    
  Set lcRel_Conversiones = Fnt_CreateObject(cDLL_Rel_Conversiones) 'New Class_Rel_Conversiones
  With lcRel_Conversiones
    Set .gDB = gDB
    .Campo("id_origen").Valor = lId_Origen
    .Campo("id_tipo_conversion").Valor = lId_Tipo_Conversion
    .Campo("id_entidad").Valor = gId_Empresa
    If .Borrar Then
      .Campo("valor").Valor = txt_mail_backfofice.Text
      If Not .Guardar Then
        pMsg_Error = "Problemas al Actualizar Mail de BackOffice."
        Fnt_Guardar_Rel_Mail_BackOffice = False
        Call Fnt_MsgError(.SubTipo_LOG _
                        , pMsg_Error _
                        , .ErrMsg _
                        , pConLog:=True)
      End If
    Else
      Fnt_Guardar_Rel_Mail_BackOffice = False
      pMsg_Error = "Problemas al Eliminar Mail de BackOffice."
      Call Fnt_MsgError(.SubTipo_LOG _
                        , pMsg_Error _
                        , .ErrMsg _
                        , pConLog:=True)
    End If
  End With
  Set lcRel_Conversiones = Nothing
'  With lcRel_Conversiones
'    Set .gDB = gDB
'    .Campo("id_origen").Valor = lId_Origen
'    .Campo("id_tipo_conversion").Valor = lId_Tipo_Conversion
'    .Campo("id_entidad").Valor = gId_Empresa
'    .Campo("valor").Valor = txt_mail_backfofice.Text
'    If Not .Guardar Then
'      Call Fnt_MsgError(.SubTipo_LOG _
'                        , "Problemas al Actualizar Mail de BackOffice." _
'                        , .ErrMsg _
'                        , pConLog:=True)
'      GoTo ErrProcedure
'    End If
'  End With
End Function

Private Function Fnt_Valida_Ruta_Cartola_Camp() As Boolean
Dim Ruta_Cartola As String
Dim lCaracter As String
Dim lPosicion As Long

Ruta_Cartola = Txt_Ruta_Cartola_Camp.Text

  Fnt_Valida_Ruta_Cartola_Camp = True
  If Not Ruta_Cartola = "" Then
    For lPosicion = 1 To Len(Ruta_Cartola)
        lCaracter = Mid(Ruta_Cartola, lPosicion, 1)
        If Not (Asc(lCaracter) >= 48 And Asc(lCaracter) <= 57) Then
            If Not (Asc(lCaracter) >= 65 And Asc(lCaracter) <= 90) Then
                If Not (Asc(lCaracter) >= 97 And Asc(lCaracter) <= 122) Then
                    Fnt_Valida_Ruta_Cartola_Camp = False
                    MsgBox "Ruta Cartola Campa�a contiene caracteres no v�lidos", vbExclamation, Me.Caption
                    Exit Function
                End If
            End If
        End If
    Next lPosicion
  Else
    Fnt_Valida_Ruta_Cartola_Camp = False
    MsgBox "Debe ingresar Ruta Cartola Campa�a.", vbExclamation, Me.Caption
    Exit Function
  End If
End Function

Private Sub Txt_Ruta_Cartola_Camp_KeyPress(KeyAscii As Integer)

'Caracteres v�lidos para directorio cartola campa�a:
'(KeyAscii >= 48 And KeyAscii <= 57) ---> N�meros del 0 al 9
'(KeyAscii >= 65 And KeyAscii <= 90) ---> Letras may�sculas de la 'A' a la 'Z'
'(KeyAscii >= 97 And KeyAscii <= 122) ---> Letras min�sculas de la 'a' la 'z'
'(KeyAscii = 8) ---> Borrar caracter

    If Not (KeyAscii >= 48 And KeyAscii <= 57) Then
        If Not (KeyAscii >= 65 And KeyAscii <= 90) Then
            If Not (KeyAscii >= 97 And KeyAscii <= 122) Then
                If Not KeyAscii = 8 Then
                    KeyAscii = 0
                End If
            End If
        End If
    End If
End Sub
