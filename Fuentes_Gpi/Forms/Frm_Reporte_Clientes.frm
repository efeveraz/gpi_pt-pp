VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Clientes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte de Clientes"
   ClientHeight    =   1695
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   7350
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1695
   ScaleWidth      =   7350
   Begin VB.Frame Frame1 
      Caption         =   "Datos de B�squedas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1185
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   7215
      Begin VB.CheckBox Chk_Clientes_Sin_Cuentas 
         Caption         =   "Clientes sin Cuentas en Empresa"
         Height          =   375
         Left            =   2610
         TabIndex        =   5
         Top             =   300
         Width           =   2685
      End
      Begin TrueDBList80.TDBCombo Cmb_Tipo_Entidad 
         Height          =   345
         Left            =   990
         TabIndex        =   6
         Tag             =   "OBLI=S;CAPTION=Tipo"
         Top             =   300
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Clientes.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Estado 
         Height          =   345
         Left            =   990
         TabIndex        =   7
         Tag             =   "OBLI=S;CAPTION=Estado"
         Top             =   690
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Clientes.frx":00AA
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label lbl_estado 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Estado"
         Height          =   330
         Left            =   180
         TabIndex        =   4
         Top             =   690
         Width           =   795
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tipo"
         Height          =   330
         Left            =   180
         TabIndex        =   3
         Top             =   300
         Width           =   795
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7350
      _ExtentX        =   12965
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Generar"
            Key             =   "REPORTE"
            Object.ToolTipText     =   "Genera nuevamente el Reporte de Clientes"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia Datos de B�squeda"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Clientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim App_Excel       As Excel.Application      ' Excel application
Dim lLibro          As Excel.Workbook      ' Excel workbook
'----------------------------------------------------------------------------
Const COL_NOMBRE_CLIENTE = 2
Const COL_RUT = 3
Const COL_ESTADO = 4
Const COL_NUMERO_CUENTAS = 5
Const COL_SEXO = 6
Const COL_FECHA_NAC = 7
Const COL_EMAIL = 8
Const COL_PAIS = 9
Const COL_TIPO_ENTIDAD = 10

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("REPORTE").Image = cBoton_Modificar
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
  
  Call Sub_CargaCombo_Estados(Cmb_Estado, cTEstado_Cliente, pTodos:=True)
    
'  With Cmb_Tipo_Entidad.ComboItems
'     .Clear
'     .Add Key:=cCmbKALL, Text:="Todos"
'     .Add Key:="KN", Text:="Natural"
'     .Add Key:="KJ", Text:="Jur�dico"
'  End With
  
  With Cmb_Tipo_Entidad
    Call .AddItem("Todos")
    Call .AddItem("Natural")
    Call .AddItem("Jur�dico")
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      .Add Fnt_AgregaValueItem("N", "Natural")
      .Add Fnt_AgregaValueItem("J", "Jur�dico")
      .Translate = True
    End With
  End With
  
  Call Sub_Limpiar
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
        Call Sub_Limpiar
    Case "EXIT"
        Unload Me
    Case "REPORTE"
        Call Sub_Generar_Reporte
  End Select
End Sub
'se agrega opci�n de generar en pantalla o en excel.
Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Generar_Reporte
    Case "EXCEL"
      Call CreaExcel
  End Select
End Sub

Private Sub Sub_Generar_Reporte()
Const clrHeader = &HD0D0D0
'------------------------------------------
Const sHeader_Clt = "Nombre Cliente|Rut|Estado|Nro. de Cuentas|Sexo|Fecha Nacimiento|Correo Electr�nico|Pais|Tipo Entidad"
Const sFormat_Clt = "3000|>1300|900|>1100|1000|>1200|3000|1300|1400"
'------------------------------------------
Dim sRecord
Dim bAppend
Dim lLinea As Integer
'------------------------------------------
Dim lCursor_Clt As hRecord
Dim lReg_Clt As hFields
'------------------------------------------
Dim lPais As Class_Paises
Dim lReg_pPais As hFields
Dim lPais_Clt As String
Dim lestado As String
'------------------------------------------
Dim lSexo As String
Dim lTipo_Entidad As String
'------------------------------------------
Dim lcClientes  As Object
Dim lcClientes2 As Object
Dim lNumero_Cuentas As String
'------------------------------------------
Dim lForm As Frm_Reporte_Generico
Dim sTitulo As String

  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  End If
  
  lestado = Fnt_ComboSelected_KEY(Cmb_Estado)
  lTipo_Entidad = Fnt_ComboSelected_KEY(Cmb_Tipo_Entidad)
  
  Rem Busca los Clientes vigentes en el sistema
  Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
  With lcClientes
    Set .gDB = gDB
    
    If Not lTipo_Entidad = cCmbKALL Then
      .Campo("tipo_entidad").Valor = lTipo_Entidad
    End If
    
    If Not lestado = cCmbKALL Then
      .Campo("cod_estado").Valor = lestado
    End If
      
    If Chk_Clientes_Sin_Cuentas.Value Then
      
      If .Buscar_Clientes_Sin_Cuenta Then
        Set lCursor_Clt = .Cursor
      Else
        MsgBox .ErrMsg, vbCritical, Me.Caption
        Set lcClientes = Nothing
        GoTo ErrProcedure
      End If
      
    Else
      
      If .Buscar(True) Then
        Set lCursor_Clt = .Cursor
      Else
        MsgBox .ErrMsg, vbCritical, Me.Caption
        Set lcClientes = Nothing
        GoTo ErrProcedure
      End If
      
    End If
    
  End With
  Set lcClientes = Nothing
  
  Rem Comienzo de la generaci�n del reporte
    sTitulo = "Reporte de Clientes "
    If Chk_Clientes_Sin_Cuentas.Value Then
      sTitulo = "Reporte de " & Chk_Clientes_Sin_Cuentas.Caption
    End If
    
    Set lForm = New Frm_Reporte_Generico
    Call lForm.Sub_InicarDocumento(pTitulo:=sTitulo _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orLandscape)
     
  With lForm.VsPrinter
    .FontSize = 8
    .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
    .Paragraph = "Tipo: " & Cmb_Tipo_Entidad.Text
    .Paragraph = "Estado: " & Cmb_Estado.Text
    If Chk_Clientes_Sin_Cuentas.Value Then
      .Paragraph = Chk_Clientes_Sin_Cuentas.Caption
    End If
    
    .Paragraph = "" 'salto de linea
    .FontBold = False
    .FontSize = 9
    
    Rem Por cada cliente encontrado
    If lCursor_Clt.Count > 0 Then
      .StartTable
      .TableCell(tcAlignCurrency) = False
      For Each lReg_Clt In lCursor_Clt
        Rem Busca el pais del cliente, si no es nulo
        lPais_Clt = ""
        Set lPais = New Class_Paises
        With lPais
          .Campo("cod_pais").Valor = NVL(lReg_Clt("cod_pais").Value, 0)
          If .Buscar Then
            For Each lReg_pPais In .Cursor
              lPais_Clt = lReg_pPais("dsc_pais").Value
            Next
          Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            GoTo ErrProcedure
          End If
        End With
        Set lPais = Nothing
        
        lNumero_Cuentas = ""
        Set lcClientes2 = Fnt_CreateObject(cDLL_Clientes)
        With lcClientes2
          Set .gDB = gDB
          .Campo("id_cliente").Valor = lReg_Clt("id_cliente").Value
          If .Buscar_Nro_Ctas_de_Cltes(Fnt_EmpresaActual) Then
            If .Cursor.Count > 0 Then
              lNumero_Cuentas = .Cursor(1)("numero_cuentas").Value
            End If
          Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            GoTo ErrProcedure
          End If
        End With
        Set lcClientes2 = Nothing
        
        lSexo = NVL(lReg_Clt("sexo").Value, "")
        If lSexo = "M" Then
          lSexo = "Masculino"
        ElseIf lSexo = "F" Then
          lSexo = "Femenino"
        End If
          
        lTipo_Entidad = NVL(lReg_Clt("Tipo_Entidad").Value, "")
        If lTipo_Entidad = "N" Then
          lTipo_Entidad = "Natural"
        ElseIf lTipo_Entidad = "J" Then
          lTipo_Entidad = "Jur�dico"
        End If
          
        Rem Genera el reporte
        sRecord = NVL(lReg_Clt("nombre_cliente").Value, "") & "|" & _
                  NVL(lReg_Clt("rut_cliente").Value, "") & "|" & _
                  NVL(lReg_Clt("dsc_estado").Value, "") & "|" & _
                  lNumero_Cuentas & "|" & _
                  lSexo & "|" & _
                  NVL(lReg_Clt("fecha_nacimiento").Value, "") & "|" & _
                  NVL(lReg_Clt("email").Value, "") & "|" & _
                  lPais_Clt & "|" & _
                  lTipo_Entidad
        .AddTable sFormat_Clt, sHeader_Clt, sRecord, clrHeader, , bAppend
      Next
      .TableCell(tcFontBold, 0) = True
      .EndTable
    Else
      .Paragraph = "No se encontraron Clientes seg�n los filtros se�alados."
    End If
    
    .EndDoc
  End With
  
  Call ColocarEn(lForm, Me, eFP_Abajo)
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Limpiar()
  
  Call Sub_ComboSelectedItem(Cmb_Estado, cCmbKALL)
  Call Sub_ComboSelectedItem(Cmb_Tipo_Entidad, cCmbKALL)
  Chk_Clientes_Sin_Cuentas.Value = False
  
End Sub
Private Sub CreaExcel()
    Dim i           As Integer
    Dim hoja        As Integer
    Dim nFilaExcel  As Integer
        
    Set App_Excel = CreateObject("Excel.application")
    App_Excel.DisplayAlerts = False
    Set lLibro = App_Excel.Workbooks.Add
    
    App_Excel.Visible = False
    
    For i = 1 To lLibro.Worksheets.Count - 1
        lLibro.Worksheets(i).Delete
    Next
    
    lLibro.Worksheets.Add After:=lLibro.Worksheets(lLibro.Worksheets.Count)
    
    'NOMBRE HOJA
    lLibro.ActiveSheet.Name = "Reporte de Clientes " & Cmb_Estado.Text & "s"
    lLibro.ActiveSheet.Cells(1, 1).Select
    lLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresa).ShapeRange.IncrementLeft 29.75
    
    nFilaExcel = HeaderXls(i)
        
    Genera_Informe_Excel i, nFilaExcel
    
    lLibro.ActiveSheet.Columns.AutoFit
    
    lLibro.ActiveSheet.Cells(1, 1).Select
       
    lLibro.Worksheets(1).Delete
    
    App_Excel.Visible = True
    App_Excel.UserControl = True
    
    Set App_Excel = Nothing
    Set lLibro = Nothing
    
End Sub

Function HeaderXls(ByVal hoja As Integer) As Integer
Dim intFilaInicioDatos As Integer
Dim sTitulo As String
    
    'lLibro.Worksheets(hoja).Select
    App_Excel.ActiveWindow.DisplayGridlines = False
    
    With lLibro.ActiveSheet
        'TITULO
        sTitulo = "Listado de Clientes "
        If Chk_Clientes_Sin_Cuentas.Value Then
          sTitulo = "Reporte de " & Chk_Clientes_Sin_Cuentas.Caption
        End If
        
        .Range("B5").Value = sTitulo
        
        .Range("B5:E5").Font.Bold = True
        .Range("B5:E5").HorizontalAlignment = xlCenter
        .Range("B5:E5").Merge
        
        .Range("B7:C10").Font.Bold = True
        .Range("B7:C10").HorizontalAlignment = xlLeft
        
        .Range("B7").Value = "Tipo: " & Cmb_Tipo_Entidad.Text
        .Range("B7:C7").Merge
        .Range("B8").Value = "Estado: " & Cmb_Estado.Text
        .Range("B8:C8").Merge
                
        
    
        .Range("B9").Value = "Fecha Consulta : " & Format(Fnt_FechaServidor, cFormatDate)
        .Range("B9:D9").Merge
        
        .Cells(11, COL_NOMBRE_CLIENTE).Value = "Nombre Cliente"
        .Cells(11, COL_RUT).Value = "Rut"
        .Cells(11, COL_ESTADO).Value = "Estado"
        .Cells(11, COL_NUMERO_CUENTAS).Value = "N�mero de Cuentas"
        .Cells(11, COL_SEXO).Value = "Sexo"
        .Cells(11, COL_FECHA_NAC).Value = "Fecha Nacimiento"
        .Cells(11, COL_EMAIL).Value = "Correo Electr�nico"
        .Cells(11, COL_PAIS).Value = "Pa�s"
        .Cells(11, COL_TIPO_ENTIDAD).Value = "Tipo Entidad"
        
        
        .Range(.Cells(11, COL_NOMBRE_CLIENTE), .Cells(11, COL_TIPO_ENTIDAD)).Interior.Color = RGB(255, 255, 0)
        .Range(.Cells(11, COL_NOMBRE_CLIENTE), .Cells(11, COL_TIPO_ENTIDAD)).Font.Bold = True
        .Range(.Cells(11, COL_NOMBRE_CLIENTE), .Cells(11, COL_TIPO_ENTIDAD)).BorderAround
        .Range(.Cells(11, COL_NOMBRE_CLIENTE), .Cells(11, COL_TIPO_ENTIDAD)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(11, COL_NOMBRE_CLIENTE), .Cells(11, COL_TIPO_ENTIDAD)).HorizontalAlignment = xlCenter
        
        .Range("B:B").HorizontalAlignment = xlLeft
        .Range("C:C").HorizontalAlignment = xlRight
        .Range("D:G").HorizontalAlignment = xlCenter
        .Range("G:G").HorizontalAlignment = xlLeft
        .Range("I:J").HorizontalAlignment = xlCenter
                
        .Range("B7:M11").Font.Bold = True
        
        .Columns("A:A").ColumnWidth = 2
        .Columns("B:B").ColumnWidth = 25
        .Columns("C:C").ColumnWidth = 13
        .Columns("D:D").ColumnWidth = 10
        .Columns("E:E").ColumnWidth = 10
        .Columns("F:F").ColumnWidth = 5
        .Columns("G:G").ColumnWidth = 15
        .Columns("H:H").ColumnWidth = 41
        .Columns("I:I").ColumnWidth = 20
        
         intFilaInicioDatos = 12
    End With
    
    
    HeaderXls = intFilaInicioDatos ' Columna donde comienzan los datos
    
End Function

Private Sub Genera_Informe_Excel(ByVal hoja As Integer, ByRef iFilaExcel As Integer)
Dim lxHoja          As Worksheet
'----------------------------------------------------
Dim intFila           As Integer
    
'------------------------------------------
Dim lCursor_Clt As hRecord
Dim lReg_Clt As hFields
'------------------------------------------
Dim lPais As Class_Paises
Dim lReg_pPais As hFields
Dim lPais_Clt As String
Dim lestado As String
'------------------------------------------
Dim lSexo As String
Dim lTipo_Entidad As String
'------------------------------------------
Dim lcClientes  As Object
Dim lcClientes2 As Object
Dim lNumero_Cuentas As String
'------------------------------------------

  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  End If
  
lestado = Fnt_ComboSelected_KEY(Cmb_Estado)
  lTipo_Entidad = Fnt_ComboSelected_KEY(Cmb_Tipo_Entidad)
  
  Rem Busca los Clientes vigentes en el sistema
  Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
  With lcClientes
    Set .gDB = gDB
    
    If Not lTipo_Entidad = cCmbKALL Then
      .Campo("tipo_entidad").Valor = lTipo_Entidad
    End If
    
    If Not lestado = cCmbKALL Then
      .Campo("cod_estado").Valor = lestado
    End If
      
    If Chk_Clientes_Sin_Cuentas.Value Then
      
      If .Buscar_Clientes_Sin_Cuenta Then
        Set lCursor_Clt = .Cursor
      Else
        MsgBox .ErrMsg, vbCritical, Me.Caption
        Set lcClientes = Nothing
        GoTo ErrProcedure
      End If
      
    Else
      
      If .Buscar(True) Then
        Set lCursor_Clt = .Cursor
      Else
        MsgBox .ErrMsg, vbCritical, Me.Caption
        Set lcClientes = Nothing
        GoTo ErrProcedure
      End If
      
    End If
    
  End With
  Set lcClientes = Nothing
  
  Rem Comienzo de la generaci�n del reporte
      
    With lLibro.ActiveSheet
        Rem Por cada cuenta encontrada
        If lCursor_Clt.Count > 0 Then
            For Each lReg_Clt In lCursor_Clt
                lPais_Clt = ""
                Set lPais = New Class_Paises
                With lPais
                  .Campo("cod_pais").Valor = NVL(lReg_Clt("cod_pais").Value, 0)
                  If .Buscar Then
                    For Each lReg_pPais In .Cursor
                      lPais_Clt = lReg_pPais("dsc_pais").Value
                    Next
                  Else
                    MsgBox .ErrMsg, vbCritical, Me.Caption
                    GoTo ErrProcedure
                  End If
                End With
                Set lPais = Nothing
                
                lNumero_Cuentas = ""
                Set lcClientes2 = Fnt_CreateObject(cDLL_Clientes)
                With lcClientes2
                  Set .gDB = gDB
                  .Campo("id_cliente").Valor = lReg_Clt("id_cliente").Value
                  If .Buscar_Nro_Ctas_de_Cltes(Fnt_EmpresaActual) Then
                    If .Cursor.Count > 0 Then
                      lNumero_Cuentas = .Cursor(1)("numero_cuentas").Value
                    End If
                  Else
                    MsgBox .ErrMsg, vbCritical, Me.Caption
                    GoTo ErrProcedure
                  End If
                End With
                Set lcClientes2 = Nothing
                
                lSexo = NVL(lReg_Clt("sexo").Value, "")
                If lSexo = "M" Then
                  lSexo = "Masculino"
                ElseIf lSexo = "F" Then
                  lSexo = "Femenino"
                End If
                  
                lTipo_Entidad = NVL(lReg_Clt("Tipo_Entidad").Value, "")
                If lTipo_Entidad = "N" Then
                  lTipo_Entidad = "Natural"
                ElseIf lTipo_Entidad = "J" Then
                  lTipo_Entidad = "Jur�dico"
                End If
        
                Rem Imprime l�nea
                .Cells(iFilaExcel, COL_NOMBRE_CLIENTE).Value = NVL(lReg_Clt("nombre_cliente").Value, "")
                .Cells(iFilaExcel, COL_RUT).Value = NVL(lReg_Clt("rut_cliente").Value, "")
                .Cells(iFilaExcel, COL_ESTADO).Value = NVL(lReg_Clt("dsc_estado").Value, "")
                .Cells(iFilaExcel, COL_NUMERO_CUENTAS).Value = lNumero_Cuentas
                .Cells(iFilaExcel, COL_SEXO).Value = lSexo
                .Cells(iFilaExcel, COL_FECHA_NAC).Value = NVL(lReg_Clt("fecha_nacimiento").Value, "")
                .Cells(iFilaExcel, COL_EMAIL).Value = NVL(lReg_Clt("email").Value, "")
                .Cells(iFilaExcel, COL_PAIS).Value = lPais_Clt
                .Cells(iFilaExcel, COL_TIPO_ENTIDAD).Value = lTipo_Entidad

                iFilaExcel = iFilaExcel + 1
            Next
        Else
          .Cells(iFilaExcel, COL_NOMBRE_CLIENTE).Value = "No hay Clientes " & Cmb_Estado.Text & "s en el sistema."
        End If
    
    End With
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub




