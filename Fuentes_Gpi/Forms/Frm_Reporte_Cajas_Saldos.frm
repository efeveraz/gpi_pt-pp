VERSION 5.00
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Reporte_Cajas_Saldos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Cajas Saldo"
   ClientHeight    =   1710
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   7095
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1710
   ScaleWidth      =   7095
   Begin VB.Frame Frame1 
      Caption         =   "Cajas Saldo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1185
      Left            =   60
      TabIndex        =   2
      Top             =   450
      Width           =   6945
      Begin VB.OptionButton Opt_Detalle 
         Caption         =   "Detalle"
         Height          =   285
         Left            =   1740
         TabIndex        =   7
         Top             =   810
         Width           =   1305
      End
      Begin VB.OptionButton Opt_Resumen 
         Caption         =   "Resumen"
         Height          =   285
         Left            =   330
         TabIndex        =   6
         Top             =   810
         Width           =   1305
      End
      Begin MSComCtl2.DTPicker DTP_Fecha 
         Height          =   345
         Left            =   1320
         TabIndex        =   3
         Top             =   360
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   57278465
         CurrentDate     =   37732
      End
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   3120
         TabIndex        =   4
         Top             =   510
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2858
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Reporte"
               Key             =   "REPORTE"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               Object.ToolTipText     =   "Genera el Reporte de Cargos y Abonos"
            EndProperty
         EndProperty
      End
      Begin VSPrinter8LibCtl.VSPrinter vp 
         Height          =   855
         Left            =   5490
         TabIndex        =   8
         Top             =   240
         Visible         =   0   'False
         Width           =   1335
         _cx             =   2355
         _cy             =   1508
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         MousePointer    =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoRTF         =   -1  'True
         Preview         =   -1  'True
         DefaultDevice   =   0   'False
         PhysicalPage    =   -1  'True
         AbortWindow     =   -1  'True
         AbortWindowPos  =   0
         AbortCaption    =   "Printing..."
         AbortTextButton =   "Cancel"
         AbortTextDevice =   "on the %s on %s"
         AbortTextPage   =   "Now printing Page %d of"
         FileName        =   ""
         MarginLeft      =   1440
         MarginTop       =   1440
         MarginRight     =   1440
         MarginBottom    =   1440
         MarginHeader    =   0
         MarginFooter    =   0
         IndentLeft      =   0
         IndentRight     =   0
         IndentFirst     =   0
         IndentTab       =   720
         SpaceBefore     =   0
         SpaceAfter      =   0
         LineSpacing     =   100
         Columns         =   1
         ColumnSpacing   =   180
         ShowGuides      =   2
         LargeChangeHorz =   300
         LargeChangeVert =   300
         SmallChangeHorz =   30
         SmallChangeVert =   30
         Track           =   0   'False
         ProportionalBars=   -1  'True
         Zoom            =   0.267379679144385
         ZoomMode        =   3
         ZoomMax         =   400
         ZoomMin         =   10
         ZoomStep        =   25
         EmptyColor      =   -2147483636
         TextColor       =   0
         HdrColor        =   0
         BrushColor      =   0
         BrushStyle      =   0
         PenColor        =   0
         PenStyle        =   0
         PenWidth        =   0
         PageBorder      =   0
         Header          =   ""
         Footer          =   ""
         TableSep        =   "|;"
         TableBorder     =   7
         TablePen        =   0
         TablePenLR      =   0
         TablePenTB      =   0
         NavBar          =   3
         NavBarColor     =   -2147483633
         ExportFormat    =   0
         URL             =   ""
         Navigation      =   3
         NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
         AutoLinkNavigate=   0   'False
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
      End
      Begin VB.Label Lbl_Fecha 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         Height          =   345
         Left            =   270
         TabIndex        =   5
         Top             =   330
         Width           =   795
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7095
      _ExtentX        =   12515
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Cajas_Saldos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim fFecha As Date
Dim lForm               As Frm_Reporte_Cartola
Const RColorRelleno = 213   ' R
Const GColorRelleno = 202   ' G
Const BColorRelleno = 195   ' B

Const glb_tamletra_titulo = 10
Const glb_tamletra_subtitulo1 = 8
Const glb_tamletra_subtitulo2 = 8
Const glb_tamletra_registros = 8


Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
  End With
  
  With Toolbar_Verificacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORTE").Image = cBoton_Modificar
  End With
  
  Call Sub_CargaForm
  Call Sub_Limpiar
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()


  fFecha = Format(Fnt_FechaServidor, cFormatDate)
  Opt_Resumen.Value = True
  
   
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Call Sub_Limpiar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REPORTE"
      Call Sub_Generar_Reportes
  End Select
End Sub

Private Sub Sub_Limpiar()
  DTP_Fecha.Value = fFecha

End Sub

Private Sub Sub_Generar_Reportes()
   If Opt_Detalle.Value = True Then
    Sub_Generar_Reporte_Completo
     
   Else
      If Opt_Resumen.Value = True Then
        Sub_Generar_Reporte
      End If
   End If
End Sub

Private Sub Sub_Generar_Reporte()
Const clrHeader = &HD0D0D0
'------------------------------------------
Const sHeader_CA = "Cuenta|Fecha Movimiento|Tipo Cargo|Descripci�n Cargo/Abono|Monto|Moneda|Estado|Origen Movimiento|Caja Cuenta|D�as Retenci�n|Tipo de Origen"
Const sFormat_CA = "800|>1100|800|2500|>1500|800|1000|1700|1600|>1000|1200"
'------------------------------------------
Dim sRecord
Dim bAppend
Dim lLinea As Integer
'------------------------------------------
Dim lCajas As Class_Cajas_Cuenta
Dim lCursor_CA As hRecord
Dim lReg_Cta As hFields
Dim lReg_Caja As hFields
Dim lReg_Saldo_Caja As hFields
Dim Filas, a As Integer
'------------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object

Dim lSaldo_Caja As Class_Saldos_Caja
Dim lNum_Cta As String
Dim Caja As String


'------------------------------------------
Dim lForm As Frm_Reporte_Generico

  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  End If
  
  Rem Busca todos productos
  Set lForm = New Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="" _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orPortrait)
     
 
  With lForm.VsPrinter
  '.MarginLeft = "15mm"
  '.MarginRight = "15mm"
  .PaperSize = pprLetter
  
    
    .FontSize = 10
    .DrawLine .MarginLeft, .CurrentY - 10, .PageWidth - .MarginRight, .CurrentY - 10
    .CalcParagraph = UCase("REPORTE CAJAS - SALDOS AL " & DTP_Fecha.Value & " (Versi�n Resumida)")
    .Paragraph = " " & UCase("REPORTE CAJAS - SALDOS AL " & DTP_Fecha.Value & " (Versi�n Resumida)")
    .DrawLine .MarginLeft, .CurrentY + 10, .PageWidth - .MarginRight, .CurrentY + 10
      .Paragraph = "" 'salto de linea
'        Set lcCuenta = New Class_Cuentas
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
          lcCuenta.Campo("id_empresa").Valor = Fnt_EmpresaActual
          lcCuenta.Campo("cod_estado").Valor = "H"
          If lcCuenta.Buscar Then
              Set lCajas = New Class_Cajas_Cuenta
              Set lSaldo_Caja = New Class_Saldos_Caja
              For Each lReg_Cta In lcCuenta.Cursor
                If .CurrentY > 12500 Then
                  .NewPage
                  .FontSize = 10
                  .DrawLine .MarginLeft, .CurrentY - 10, .PageWidth - .MarginRight, .CurrentY - 10
                  .CalcParagraph = UCase("REPORTE CAJAS - SALDOS AL " & DTP_Fecha.Value & " (Versi�n Resumida)")
                  .Paragraph = " " & UCase("REPORTE CAJAS - SALDOS AL " & DTP_Fecha.Value & " (Versi�n Resumida)")
                  .DrawLine .MarginLeft, .CurrentY + 10, .PageWidth - .MarginRight, .CurrentY + 10
                  .Paragraph = ""
                End If
                .StartTable
                  .TableCell(tcRows) = 1
                  .TableCell(tcCols) = 1
                  .TableCell(tcFontSize, 1, 1, 1, 1) = 7
                  .TableCell(tcColWidth, 1, 1) = "179.9mm"
                  .TableBorder = tbBoxColumns
                  .TableCell(tcText, 1, 1) = "Cuenta: " & lReg_Cta("num_cuenta").Value & " - " & lReg_Cta("rut_cliente").Value & " - " & lReg_Cta("nombre_cliente").Value & " | Moneda: " & Devuelve_Moneda(lReg_Cta("id_moneda").Value)
                  .TableCell(tcFontBold, 1, 1) = True
                  .TableCell(tcBackColor, 1, 1) = RGB(200, 200, 200)
                  .TableCell(tcRowHeight, 1, 1) = "5mm"
                  
                .EndTable
                
              
              
                .StartTable
                  .TableCell(tcRows) = 1
                  .TableCell(tcCols) = 4
                  .TableCell(tcFontSize, 1, 1, 1, 4) = 7
                  .TableCell(tcColWidth, 1, 1) = "60mm"
                  .TableCell(tcColWidth, 1, 2) = "20mm"
                  .TableCell(tcColWidth, 1, 3) = "50mm"
                  .TableCell(tcColWidth, 1, 4) = "50mm"
                  .TableBorder = tbBoxColumns
                  .TableCell(tcAlign, 1, 1, 1, 4) = taCenterMiddle
                  .TableCell(tcText, 1, 1) = "Caja"
                  .TableCell(tcText, 1, 2) = "Moneda Caja"
                  .TableCell(tcText, 1, 3) = "Saldo Moneda Caja"
                  .TableCell(tcText, 1, 4) = "Saldo Moneda Cuenta"
                  .TableCell(tcFontBold, 1, 1, 1, 4) = False
                  .TableCell(tcBackColor, 1, 1, 1, 4) = RGB(240, 240, 240)
                  
                .EndTable
'--

                
              
                lCajas.Campo("id_Cuenta").Valor = lReg_Cta("id_Cuenta").Value
                If lCajas.Buscar Then
                     'Filas = CInt(lCajas.Cursor.Count)
                     
                  
                    .StartTable
                    '.TableCell(tcRows) = 1
                    .TableCell(tcCols) = 4
                  
                    
                     

                    For Each lReg_Caja In lCajas.Cursor
                        
                        lSaldo_Caja.Campo("ID_CAJA_CUENTA").Valor = lReg_Caja("ID_CAJA_CUENTA").Value
                        lSaldo_Caja.Campo("fecha_cierre").Valor = DTP_Fecha.Value
                        
                        If lSaldo_Caja.Buscar Then
                          'a = 2
                          For Each lReg_Saldo_Caja In lSaldo_Caja.Cursor
          
                          
                          
                              .TableCell(tcRows) = .TableCell(tcRows) + 1
                              a = .TableCell(tcRows)
                              '.TableCell(tcRowHeight, a, 1) = 20
                              .TableCell(tcText, a, 1) = lReg_Caja("dsc_caja_cuenta").Value
                              .TableCell(tcFontSize, a) = 7
                              .TableCell(tcFontBold, a) = False
                              .TableCell(tcAlign, a) = taLeftMiddle
                              .TableCell(tcText, a, 2) = Devuelve_Moneda(lReg_Caja("id_MONEDA").Value)
                              .TableCell(tcAlign, a, 2) = taCenterMiddle
                              .TableCell(tcAlign, a, 3) = taRightMiddle
                              .TableCell(tcAlign, a, 4) = taRightMiddle
                             ' .TableCell(tcText, a, 3) = FormatNumber(lReg_Saldo_Caja("MONTO_X_COBRAR_MON_CAJA").Value)
                             ' .TableCell(tcText, a, 4) = FormatNumber(lReg_Saldo_Caja("MONTO_X_PAGAR_MON_CAJA").Value)
                              .TableCell(tcText, a, 3) = FormatNumber(lReg_Saldo_Caja("monto_mon_cta").Value)
                              .TableCell(tcText, a, 4) = FormatNumber(lReg_Saldo_Caja("monto_mon_caja").Value)
                              
                            'a = a + 1
                          Next
                          
                          
                        End If
                       
                    
                    Next
      
                    '.TableCell(tcAlign, 1, 1, 2, 4) = taRightMiddle
                    '.TableCell(tcAlign, 1, 1, 1, 2) = taLeftMiddle
                    .TableCell(tcColWidth, 1, 1) = "60mm"
                    .TableCell(tcColWidth, 1, 2) = "20mm"
                    .TableCell(tcColWidth, 1, 3) = "50mm"
                    .TableCell(tcColWidth, 1, 4) = "50mm"
                    '.TableCell(tcFontBold, 1, 1, 1, 4) = False
                 .EndTable
         
                End If
                
            Next
          Else
             MsgBox .ErrMsg, vbCritical, Me.Caption
             GoTo ErrProcedure
          End If
       

    '.MarginLeft = "122.95mm"
    

    
    .EndDoc
  End With
  
  Call ColocarEn(lForm, Me, eFP_Abajo)
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)

 

  
End Sub

Function Devuelve_Moneda(Iid_moneda)
'  Dim lMoneda As Class_Monedas
  Dim lcMoneda As Object
  Dim lReg As hFields
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)

  lcMoneda.Campo("id_moneda").Valor = Iid_moneda
  If lcMoneda.Buscar Then
    For Each lReg In lcMoneda.Cursor
      Devuelve_Moneda = lReg("cod_moneda").Value
    Next
    
  End If
 
End Function

Private Sub Sub_Generar_Reporte_Completo()
Const clrHeader = &HD0D0D0
'------------------------------------------

Dim sRecord
Dim bAppend
Dim lLinea As Integer
'------------------------------------------
Dim lCajas As Class_Cajas_Cuenta
Dim lCursor_CA As hRecord
Dim lReg_Cta As hFields
Dim lReg_Caja As hFields
Dim lReg_Saldo_Caja As hFields
Dim Filas, a As Integer
'------------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lSaldo_Caja As Class_Saldos_Caja
Dim lNum_Cta As String
Dim Caja As String


'------------------------------------------
'Dim lForm As Frm_Reporte_Generico

  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  End If
  
  Rem Busca todos productos
  
  
  


      
  
  'Set lForm = New Frm_Reporte_Generico
  'Call lForm.Sub_InicarDocumento(pTitulo:="" _
  '                             , pTipoSalida:=ePrinter.eP_Pantalla _
  '                             , pOrientacion:=orLandscape)
     
 Call ConfiguraHoja
  With vp
  .StartDoc
  '.NewPage
  Call pon_header

  
    

    


      
    
    
'
'    .StartTable
'      .TableCell(tcRows) = 1
'      .TableCell(tcCols) = 1
'      .TableCell(tcFontSize, 1, 1) = 7
'      .TableCell(tcColWidth, 1, 1) = "179mm"
'      .TableCell(tcText, 1, 1) = "CAJAS"
'      .TableBorder = tbNone
'
'    .EndTable
    
    

    

'        Set lcCuenta = New Class_Cuentas
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        
          lcCuenta.Campo("id_empresa").Valor = Fnt_EmpresaActual
          lcCuenta.Campo("cod_estado").Valor = "H"
          If lcCuenta.Buscar Then
              Set lCajas = New Class_Cajas_Cuenta
              Set lSaldo_Caja = New Class_Saldos_Caja
              For Each lReg_Cta In lcCuenta.Cursor
                If .CurrentY > 10000 Then
                  .NewPage
                  Call pon_header
                End If
                .StartTable
                  .TableCell(tcRows) = 1
                  .TableCell(tcCols) = 1
                  .TableCell(tcFontSize, 1, 1, 1, 1) = 7
                  .TableCell(tcColWidth, 1, 1) = "260mm"
                  .TableBorder = tbBoxColumns
                  .TableCell(tcText, 1, 1) = "Cuenta: " & lReg_Cta("dsc_cuenta").Value & " - " & lReg_Cta("nombre_cliente").Value & " | Moneda: " & Devuelve_Moneda(lReg_Cta("id_moneda").Value)
                  .TableCell(tcFontBold, 1, 1) = True
                  .TableCell(tcBackColor, 1, 1) = RGB(200, 200, 200)
                  .TableCell(tcRowHeight, 1, 1) = "5mm"
                  
                .EndTable
                
              
                .StartTable
                  .TableCell(tcRows) = 1
                  .TableCell(tcCols) = 10
                  .TableCell(tcFontSize, 1, 1, 1, 19) = 7
                   .TableCell(tcColWidth, 1, 1) = "35mm"
                    .TableCell(tcColWidth, 1, 2) = "25mm"
                    .TableCell(tcColWidth, 1, 3) = "25mm"
                    .TableCell(tcColWidth, 1, 4) = "25mm"
                    .TableCell(tcColWidth, 1, 5) = "25mm"
                    .TableCell(tcColWidth, 1, 6) = "25mm"
                    .TableCell(tcColWidth, 1, 7) = "25mm"
                    .TableCell(tcColWidth, 1, 8) = "25mm"
                    .TableCell(tcColWidth, 1, 9) = "25mm"
                    .TableCell(tcColWidth, 1, 10) = "25mm"
                  .TableBorder = tbBoxColumns
                  .TableCell(tcAlign, 1, 1, 1, 10) = taCenterMiddle
                   .TableCell(tcText, 1, 1) = "Caja"
                  .TableCell(tcText, 1, 2) = "Moneda Caja"
                  .TableCell(tcText, 1, 3) = "Monto por Cobrar Moneda Caja"
                  .TableCell(tcText, 1, 4) = "Monto por Pagar Moneda Caja"
                  .TableCell(tcText, 1, 5) = "Saldo Moneda Caja"
                  .TableCell(tcText, 1, 6) = "Saldo Neto Moneda Caja"
                  .TableCell(tcText, 1, 7) = "Monto por Cobrar Moneda Cuenta"
                  .TableCell(tcText, 1, 8) = "Monto por Pagar Moneda Cuenta"
                  .TableCell(tcText, 1, 9) = "Saldo Moneda Cuenta"
                  .TableCell(tcText, 1, 10) = "Saldo Neto Moneda Cuenta"
                  .TableCell(tcFontBold, 1, 1, 1, 10) = False
                  .TableCell(tcBackColor, 1, 1, 1, 10) = RGB(240, 240, 240)
                  
                .EndTable
                
              

                

                
              
                lCajas.Campo("id_Cuenta").Valor = lReg_Cta("id_Cuenta").Value
                If lCajas.Buscar Then
                    ' Filas = CInt(lCajas.Cursor.Count)
                     
                  
                    .StartTable
                    .TableCell(tcRows) = 1
                    .TableCell(tcCols) = 10
                    .TableCell(tcFontSize, 1, 1, 1, 10) = 7
                    .TableCell(tcAlign, 1, 1, 2, 10) = taRightMiddle
                    .TableCell(tcAlign, 1, 1, 1, 2) = taLeftMiddle
                   .TableCell(tcColWidth, 1, 1) = "35mm"
                    .TableCell(tcColWidth, 1, 2) = "25mm"
                    .TableCell(tcColWidth, 1, 3) = "25mm"
                    .TableCell(tcColWidth, 1, 4) = "25mm"
                    .TableCell(tcColWidth, 1, 5) = "25mm"
                    .TableCell(tcColWidth, 1, 6) = "25mm"
                    .TableCell(tcColWidth, 1, 7) = "25mm"
                    .TableCell(tcColWidth, 1, 8) = "25mm"
                    .TableCell(tcColWidth, 1, 9) = "25mm"
                    .TableCell(tcColWidth, 1, 10) = "25mm"
                    .TableCell(tcFontBold, 1, 1, 1, 10) = False
                    
                     Dim Mostrado As Integer
                     Mostrado = 0
                    For Each lReg_Caja In lCajas.Cursor
                        
                        lSaldo_Caja.Campo("ID_CAJA_CUENTA").Valor = lReg_Caja("ID_CAJA_CUENTA").Value
                        lSaldo_Caja.Campo("fecha_cierre").Valor = DTP_Fecha.Value
                        
                        If lSaldo_Caja.Buscar Then
                          
                          For Each lReg_Saldo_Caja In lSaldo_Caja.Cursor
                              If Mostrado <> 0 Then
                                  .TableCell(tcRows) = .TableCell(tcRows) + 1
                              Else
                                  Mostrado = 1
                              End If
                              
                              a = .TableCell(tcRows)
                              
'
                              .TableCell(tcText, a, 1) = lReg_Caja("dsc_caja_cuenta").Value
                              .TableCell(tcText, a, 2) = Devuelve_Moneda(lReg_Caja("id_MONEDA").Value)
                              
                              .TableCell(tcText, a, 3) = FormatNumber(lReg_Saldo_Caja("MONTO_X_COBRAR_MON_CAJA").Value)
                              
                              
                              
                              .TableCell(tcText, a, 4) = FormatNumber(lReg_Saldo_Caja("MONTO_X_PAGAR_MON_CAJA").Value)
                              .TableCell(tcText, a, 5) = FormatNumber(lReg_Saldo_Caja("MONTO_MON_CAJA").Value)
                              .TableCell(tcText, a, 6) = FormatNumber(lReg_Saldo_Caja("MONTO_MON_CAJA").Value + lReg_Saldo_Caja("MONTO_X_COBRAR_MON_CTA").Value - lReg_Saldo_Caja("MONTO_X_PAGAR_MON_CAJA").Value)
                              .TableCell(tcFontBold, a, 6) = True
                              .TableCell(tcText, a, 7) = FormatNumber(lReg_Saldo_Caja("MONTO_X_COBRAR_MON_CTA").Value)
                              .TableCell(tcText, a, 8) = FormatNumber(lReg_Saldo_Caja("MONTO_X_PAGAR_MON_CTA").Value)
                              .TableCell(tcText, a, 9) = FormatNumber(lReg_Saldo_Caja("MONTO_MON_CTA").Value)
                              .TableCell(tcText, a, 10) = FormatNumber(lReg_Saldo_Caja("MONTO_MON_CTA").Value + lReg_Saldo_Caja("MONTO_X_COBRAR_MON_CTA").Value - lReg_Saldo_Caja("MONTO_X_PAGAR_MON_CTA").Value)
                              .TableCell(tcFontBold, a, 10) = True
                              
                              .TableCell(tcFontSize, a) = 7
                              .TableCell(tcFontBold, a) = False
                              '.TableCell(tcAlign, a) = taLeftMiddle
                              .TableCell(tcAlign, a) = taRightMiddle
                              .TableCell(tcAlign, a, 1) = taLeftMiddle
                              .TableCell(tcAlign, a, 2) = taCenterMiddle
                              
                            'a = a + 1
                          Next
                          
                           
                        End If
                       
                    
                    Next
                    
                 .EndTable
                 .FontSize = 1
                 .Paragraph = ""
                End If
                
            Next
          Else
             MsgBox .ErrMsg, vbCritical, Me.Caption
             GoTo ErrProcedure
          End If
       
      
            .StartTable
      '.TableCell(tcRows) = 4
      .TableCell(tcCols) = 4
      .TableBorder = tbBoxColumns
      
      .TableCell(tcColWidth, 1, 1) = "30mm"
      .TableCell(tcColWidth, 1, 2) = "30mm"
      .TableCell(tcColWidth, 1, 3) = "30mm"
      .TableCell(tcColWidth, 1, 4) = "30mm"
      
      
    .TableCell(tcFontSize, 1, 1, Filas, 4) = 6
    .TableCell(tcAlign, 1, 2, Filas, 4) = taRightMiddle
    '.TableCell(tcAlign, 2, 2, Filas, 4) = taRightMiddle
    .EndTable
    
    '.MarginLeft = "122.95mm"
    

    
    .EndDoc
  End With
  
        Set lForm = New Frm_Reporte_Cartola
        Dim PathAndFile As String
        PathAndFile = "asa"
        
        vp.SaveDoc PathAndFile
        lForm.vp.LoadDoc PathAndFile
  Call ColocarEn(lForm, Me, eFP_Abajo)
 ' Call ColocarEn(vp, Me, eFP_Abajo)
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)

 

  
End Sub


Sub ConfiguraHoja()

    With vp
    
        .PaperSize = pprLetter
        .MarginLeft = "12mm"
        .MarginRight = "12mm"
        .MarginTop = "40mm"
        .MarginBottom = "25mm"
        .MarginFooter = "15mm"
        .MarginHeader = "0mm"
        'Call lForm.trae_datos_cliente(lId_Cuenta, DTP_Fecha_Ter.Val,ue)
        .Font.Name = "arial"
        .Font.Size = 7
        .Orientation = orLandscape
        
    End With
End Sub


Public Sub pon_header()
    Dim xValTop As Variant
    Dim sRutaImagen As String
    'dim lcursor_valores as hCollection
    Dim Xinicio As Variant
    Dim YInicio As Variant
    Dim lCursor_Valores As hRecord
    Dim lReg_Valores As hFields
    Dim iFil As Integer
    vp.MarginLeft = "10mm"
    
    sRutaImagen = App.Path & "\logo_empresa.jpg"
    
    xValTop = ""
    
    'Inserta Logo Moneda
    On Error Resume Next
    vp.DrawPicture LoadPicture(sRutaImagen), vp.MarginLeft, "10mm", "40mm", "30mm"
    
    On Error GoTo 0
    vp.CurrentY = vp.CurrentY - 200
    vp.FontSize = 10
    vp.DrawLine vp.MarginLeft, vp.CurrentY - 10, vp.PageWidth - vp.MarginRight, vp.CurrentY - 10
    vp.CalcParagraph = UCase("REPORTE CAJAS - SALDOS AL " & DTP_Fecha.Value & " (Versi�n Detallada)")
    vp.Paragraph = " " & UCase("REPORTE CAJAS - SALDOS AL " & DTP_Fecha.Value & " (Versi�n Detallada)")
    vp.DrawLine vp.MarginLeft, vp.CurrentY + 10, vp.PageWidth - vp.MarginRight, vp.CurrentY + 10
    vp.Paragraph = ""
    

    
End Sub

