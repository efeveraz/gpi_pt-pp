VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Balance 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Balance Inversiones"
   ClientHeight    =   9360
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13230
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9360
   ScaleWidth      =   13230
   Begin VB.Frame Frame1 
      Height          =   7245
      Left            =   0
      TabIndex        =   3
      Top             =   390
      Width           =   13185
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3360
         Picture         =   "Frm_Balance.frx":0000
         TabIndex        =   18
         Top             =   230
         Width           =   375
      End
      Begin VB.Frame Frame2 
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1305
         Left            =   5130
         TabIndex        =   4
         Top             =   120
         Width           =   8055
         Begin hControl2.hTextLabel Txt_Rut 
            Height          =   315
            Left            =   30
            TabIndex        =   1
            Top             =   240
            Width           =   3150
            _ExtentX        =   5556
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   1200
            Caption         =   "RUT"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Nombres 
            Height          =   315
            Left            =   30
            TabIndex        =   2
            Top             =   570
            Width           =   7965
            _ExtentX        =   14049
            _ExtentY        =   556
            LabelWidth      =   1300
            Caption         =   "Nombres"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Perfil 
            Height          =   315
            Left            =   3225
            TabIndex        =   7
            Top             =   240
            Width           =   2520
            _ExtentX        =   4445
            _ExtentY        =   556
            LabelWidth      =   1000
            TextMinWidth    =   1200
            Caption         =   "Perfil Riesgo"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel txt_mon_cta 
            Height          =   315
            Left            =   5760
            TabIndex        =   24
            Top             =   240
            Width           =   2235
            _ExtentX        =   3942
            _ExtentY        =   556
            LabelWidth      =   1200
            TextMinWidth    =   500
            Caption         =   "Moneda Cuenta"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel txtNombreAsesor 
            Height          =   315
            Left            =   30
            TabIndex        =   48
            Top             =   900
            Width           =   7980
            _ExtentX        =   14076
            _ExtentY        =   556
            LabelWidth      =   1300
            Caption         =   "Asesor"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ini 
         Height          =   345
         Left            =   1185
         TabIndex        =   8
         Top             =   1020
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         CalendarBackColor=   16777215
         Format          =   16711681
         CurrentDate     =   38938
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   5670
         Left            =   60
         TabIndex        =   10
         Top             =   1470
         Width           =   13080
         _cx             =   23072
         _cy             =   10001
         Appearance      =   0
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   8421504
         BackColorAlternate=   -2147483643
         GridColor       =   12632256
         GridColorFixed  =   0
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   0
         FocusRect       =   0
         HighLight       =   0
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   0
         SelectionMode   =   0
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   29
         Cols            =   20
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   $"Frm_Balance.frx":030A
         ScrollTrack     =   0   'False
         ScrollBars      =   2
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   1
         OutlineCol      =   3
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaper       =   "Frm_Balance.frx":044A
         WallPaperAlignment=   0
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
         Begin VB.Frame frmCustodia 
            BeginProperty Font 
               Name            =   "Comic Sans MS"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   5265
            Left            =   30
            TabIndex        =   38
            Top             =   -60
            Visible         =   0   'False
            Width           =   12855
            Begin VB.CommandButton cmd_salir 
               Height          =   210
               Left            =   12510
               Picture         =   "Frm_Balance.frx":0FA6
               Style           =   1  'Graphical
               TabIndex        =   39
               Top             =   210
               Width           =   255
            End
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Detalle 
               Height          =   4665
               Left            =   60
               TabIndex        =   43
               Top             =   480
               Width           =   12735
               _cx             =   22463
               _cy             =   8229
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   0   'False
               AllowBigSelection=   0   'False
               AllowUserResizing=   1
               SelectionMode   =   1
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   200
               Cols            =   10
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Balance.frx":112C
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   0
               AutoSearchDelay =   2
               MultiTotals     =   0   'False
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   0
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   1
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   0
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
            Begin VB.Label lblFrameCustodia 
               BackColor       =   &H00FF0000&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "CUSTODIA "
               ForeColor       =   &H00FFFFFF&
               Height          =   270
               Left            =   60
               TabIndex        =   41
               Top             =   180
               Width           =   12735
            End
            Begin VB.Label lblNemotecnico 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   615
               TabIndex        =   40
               Top             =   3510
               Width           =   1530
            End
         End
         Begin VB.Frame Frm_Sombra 
            BackColor       =   &H00808080&
            BorderStyle     =   0  'None
            Height          =   5145
            Left            =   60
            TabIndex        =   42
            Top             =   135
            Visible         =   0   'False
            Width           =   12900
         End
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
         Height          =   345
         Left            =   3750
         TabIndex        =   45
         Top             =   1020
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         CalendarBackColor=   16777215
         Format          =   16711681
         CurrentDate     =   38938
      End
      Begin TrueDBList80.TDBCombo Cmb_Moneda 
         Height          =   345
         Left            =   1185
         TabIndex        =   49
         Tag             =   "OBLI=S;CAPTION=Moneda"
         Top             =   610
         Width           =   3525
         _ExtentX        =   6218
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Balance.frx":1304
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   60
         TabIndex        =   51
         Top             =   240
         Width           =   3285
         _ExtentX        =   5794
         _ExtentY        =   609
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "N� de Cuenta"
         Text            =   ""
         MaxLength       =   100
      End
      Begin VB.Label lbl_moneda 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Moneda"
         Height          =   345
         Left            =   60
         TabIndex        =   50
         Top             =   610
         Width           =   1065
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha T�rmino"
         Height          =   345
         Index           =   1
         Left            =   2610
         TabIndex        =   44
         Top             =   1020
         Width           =   1125
      End
      Begin VB.Label Lbl_Fecha_Ter2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Inicio"
         Height          =   345
         Index           =   0
         Left            =   60
         TabIndex        =   9
         Top             =   1020
         Width           =   1065
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   13230
      _ExtentX        =   23336
      _ExtentY        =   635
      ButtonWidth     =   1746
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Buscar"
            Key             =   "BUSCAR"
            Description     =   "Busca Balance de Inversi�n"
            Object.ToolTipText     =   "Busca Balance de Inversi�n"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   7920
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame_Operacion 
      Height          =   1785
      Left            =   0
      TabIndex        =   6
      Top             =   7560
      Width           =   13185
      Begin VB.Frame Frame4 
         Caption         =   "VALORES CUOTAS"
         ForeColor       =   &H000000FF&
         Height          =   1545
         Left            =   8100
         TabIndex        =   19
         Top             =   150
         Width           =   4995
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Rentabilidad Per�odo"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   180
            TabIndex        =   47
            Top             =   1200
            Width           =   2355
         End
         Begin VB.Label lbl_Rentabilidad_Periodo 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   2610
            TabIndex        =   46
            Top             =   1170
            Width           =   1875
         End
         Begin VB.Label lbl_ValorCuota_Actual 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   2610
            TabIndex        =   23
            Top             =   810
            Width           =   1875
         End
         Begin VB.Label lbl_ValorCuota_Anterior 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   2610
            TabIndex        =   22
            Top             =   450
            Width           =   1875
         End
         Begin VB.Label lbl_Fecha_Valor_Cuota_Actual 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   180
            TabIndex        =   21
            Top             =   840
            Width           =   2355
         End
         Begin VB.Label lbl_Fecha_Valor_Cuota_Anterior 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   180
            TabIndex        =   20
            Top             =   480
            Width           =   2355
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   " RENTABILIDADES"
         ForeColor       =   &H000000FF&
         Height          =   1545
         Left            =   30
         TabIndex        =   11
         Top             =   150
         Width           =   7755
         Begin VB.Label Label14 
            Alignment       =   2  'Center
            Caption         =   "USD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   240
            Left            =   4980
            TabIndex        =   37
            Top             =   150
            Width           =   720
         End
         Begin VB.Label Label13 
            Alignment       =   2  'Center
            Caption         =   "PESOS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   240
            Left            =   6420
            TabIndex        =   36
            Top             =   150
            Width           =   720
         End
         Begin VB.Label lbl_moneda_cta 
            Alignment       =   2  'Center
            Caption         =   "Cuenta"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   240
            Left            =   2070
            TabIndex        =   35
            Top             =   150
            Width           =   720
         End
         Begin VB.Label Label11 
            Alignment       =   2  'Center
            Caption         =   "UF"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   240
            Left            =   3540
            TabIndex        =   34
            Top             =   150
            Width           =   720
         End
         Begin VB.Label lbl_Ret_Ult_PESOS 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   6210
            TabIndex        =   33
            Top             =   1170
            Width           =   1305
         End
         Begin VB.Label lbl_Ret_Anu_PESOS 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   6210
            TabIndex        =   32
            Top             =   810
            Width           =   1305
         End
         Begin VB.Label lbl_Ret_Men_PESOS 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   6210
            TabIndex        =   31
            Top             =   450
            Width           =   1305
         End
         Begin VB.Label lbl_Ret_Ult_USD 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   4740
            TabIndex        =   30
            Top             =   1170
            Width           =   1305
         End
         Begin VB.Label lbl_Ret_Anu_USD 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   4740
            TabIndex        =   29
            Top             =   810
            Width           =   1305
         End
         Begin VB.Label lbl_Ret_Men_USD 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   4740
            TabIndex        =   28
            Top             =   450
            Width           =   1305
         End
         Begin VB.Label lbl_Ret_Ult_UF 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   3270
            TabIndex        =   27
            Top             =   1170
            Width           =   1305
         End
         Begin VB.Label lbl_Ret_Anu_UF 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   3270
            TabIndex        =   26
            Top             =   810
            Width           =   1305
         End
         Begin VB.Label lbl_Ret_Men_UF 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   3270
            TabIndex        =   25
            Top             =   450
            Width           =   1305
         End
         Begin VB.Label Label7 
            Alignment       =   1  'Right Justify
            Caption         =   "Anual"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   75
            TabIndex        =   17
            Top             =   840
            Width           =   1590
         End
         Begin VB.Label Label6 
            Alignment       =   1  'Right Justify
            Caption         =   "Ultimos 12 Meses"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   75
            TabIndex        =   16
            Top             =   1170
            Width           =   1590
         End
         Begin VB.Label Label5 
            Alignment       =   1  'Right Justify
            Caption         =   "Mensual"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   75
            TabIndex        =   15
            Top             =   480
            Width           =   1590
         End
         Begin VB.Label lbl_Ret_Ult_cta 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   1800
            TabIndex        =   14
            Top             =   1170
            Width           =   1300
         End
         Begin VB.Label lbl_Ret_Anu_cta 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   1800
            TabIndex        =   13
            Top             =   810
            Width           =   1300
         End
         Begin VB.Label lbl_Ret_Men_cta 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   300
            Left            =   1800
            TabIndex        =   12
            Top             =   450
            Width           =   1300
         End
      End
   End
   Begin VB.Image Image4 
      Height          =   165
      Left            =   1680
      Picture         =   "Frm_Balance.frx":13AE
      Top             =   8490
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.Image Image1 
      Height          =   255
      Left            =   600
      Picture         =   "Frm_Balance.frx":1550
      Top             =   8130
      Visible         =   0   'False
      Width           =   210
   End
   Begin VB.Image Image3 
      Height          =   225
      Left            =   330
      Picture         =   "Frm_Balance.frx":187E
      Top             =   8130
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.Image imgClear 
      Height          =   375
      Left            =   450
      Top             =   8580
      Visible         =   0   'False
      Width           =   495
   End
End
Attribute VB_Name = "Frm_Balance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const Colum_Cero = 0
Const Colum_Uno = 1
Const Colum_Dos = 2
Const Colum_Tres = 3
Const Colum_Cuatro = 4
Const Colum_Cinco = 5
Const Colum_Seis = 6
Const Colum_Siete = 7
Const Colum_Ocho = 8
Const Colum_Nueve = 9

Dim fFlg_Tipo_Permiso As String 'Flags para el permiqso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana  -> no se ocupa mucho

Dim fTipo_Operacion As String
Dim fOper_Fecha_Anterior As Boolean
Dim mcol As Integer
Dim t As Integer
Dim sp As String

Public fKey As String
Dim oCliente As New Class_Cartola
Dim lcCierres_Cuenta As New Class_Cierres_Cuentas

Dim Detalle_Cajas_Actual() As Est_Detalle_Cajas
Dim Detalle_Cajas_Anterior() As Est_Detalle_Cajas
'Dim lcCuenta As New Class_Cuentas
Dim lcCuenta As Object
Dim lId_Cuenta As String
Dim nTotalActivos_Actual As Double
Dim xFecha_Cierre As Date
Dim xFecha_Operativa As Date

Rem ----------------------------------------------------------------------------------------
Rem 10/10/2008. Se agrega Moneda Salida del Balance, decimales de acuerdo
Rem             a la moneda de salida y los de tasa presentarlos con 4
Rem 03/04/2009. Se agrega control de permisos.
Rem             Si el permiso es de solo lectura, se muestra la fecha de cierre virtual
Rem 19/08/2009. Se agrega el env�o del Flag de Permiso que tiene el usuario para traer la
Rem             informaci�n
Rem ----------------------------------------------------------------------------------------


Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_Obtiene_Permiso_Formulario(pCod_Arbol_Sistema)
  Call Form_Resize
  
  Load Me
End Sub

Private Sub cmb_buscar_Click()
Dim lId_Cuenta As String
Dim ftextCuenta  As String

    ftextCuenta = Txt_Num_Cuenta.Text & "-"
    If Len(ftextCuenta) > 1 Then
        ftextCuenta = Trim(Left(ftextCuenta, InStr(1, ftextCuenta, "-") - 1))
    Else
        ftextCuenta = ""
    End If

    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(ftextCuenta), 0)
    Txt_Num_Cuenta.Tag = lId_Cuenta
    
    If Txt_Num_Cuenta.Tag <> "" Then
        Call Sub_MostrarCuenta(Txt_Num_Cuenta.Tag, True)
    End If
    
    If lId_Cuenta <> 0 Then
        Cmb_Cuentas_ItemChange
    End If
End Sub

Private Sub Cmb_Cuentas_ItemChange()
Dim pFecha              As Date
Dim lFechaCierreVirtual As Date

  pFecha = Fnt_FechaServidor
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  lId_Cuenta = Txt_Num_Cuenta.Tag
  If Not lId_Cuenta = "" Then
    lcCierres_Cuenta.Campo("id_cuenta").Valor = Txt_Num_Cuenta.Tag
    lcCierres_Cuenta.Campo("fecha_cierre").Valor = pFecha
    If lcCierres_Cuenta.BuscarUltimo Then
        If lcCierres_Cuenta.Cursor.Count > 0 Then
            If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
                If Fnt_Obtiene_FechaCierre_Virtual(lFechaCierreVirtual) Then
                    DTP_Fecha_Ter.Value = lFechaCierreVirtual
                    xFecha_Cierre = lFechaCierreVirtual
                Else
                    DTP_Fecha_Ter.Value = NVL(lcCierres_Cuenta.Cursor(1)("fecha_cierre").Value, "")
                    xFecha_Cierre = NVL(lcCierres_Cuenta.Cursor(1)("fecha_cierre").Value, "")
                End If
            Else
                DTP_Fecha_Ter.Value = NVL(lcCierres_Cuenta.Cursor(1)("fecha_cierre").Value, "")
                xFecha_Cierre = NVL(lcCierres_Cuenta.Cursor(1)("fecha_cierre").Value, "")
            End If
            DTP_Fecha_Ini.Value = UltimoDiaDelMes(DTP_Fecha_Ter.Value)
            lbl_Fecha_Valor_Cuota_Actual.Caption = "" & Format$(DTP_Fecha_Ter.Value, "dd   mmmm, yyyy")
            lbl_Fecha_Valor_Cuota_Anterior.Caption = Format$(DTP_Fecha_Ini.Value, "dd   mmmm, yyyy")
            Toolbar.Buttons("BUSCAR").Enabled = True
            With lcCuenta
              .Campo("id_cuenta").Valor = lId_Cuenta
              If .Buscar_Vigentes Then
                If .Cursor.Count > 0 Then
                  Txt_Perfil.Text = .Cursor(1)("dsc_perfil_riesgo").Value
                  Txt_Rut.Text = "" & .Cursor(1)("rut_cliente").Value
                  Txt_Nombres.Text = "" & .Cursor(1)("nombre_cliente").Value
                  txt_mon_cta.Text = "" & .Cursor(1)("dsc_moneda").Value
                  
                  txtNombreAsesor.Text = "" & .Cursor(1)("Nombre_asesor").Value
                  Call Sub_ComboSelectedItem(Cmb_Moneda, .Cursor(1)("id_moneda").Value)     'Agregado por MMA 10/10/2008
                                    
                  lbl_moneda_cta.Caption = txt_mon_cta.Text
                  xFecha_Operativa = .Cursor(1)("Fecha_operativa").Value
                  Grilla.Clear
                End If
              Else
                MsgBox "Problemas en cargar la Cuenta." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
              End If
            End With
        Else
            Toolbar.Buttons("BUSCAR").Enabled = False
            MsgBox "No hay cierres para esta cuentas", vbExclamation
        End If
    Else
        MsgBox "No hay cierres para esta cuentas", vbExclamation
        Toolbar.Buttons("BUSCAR").Enabled = False
    End If
  End If
  Set lcCuenta = Nothing
End Sub

Private Sub Cmb_Cuentas_KeyPress(KeyAscii As Integer)
Dim pFecha As Date
Dim lFechaCierreVirtual As Date

If KeyAscii = vbKeyReturn Then
  pFecha = Fnt_FechaServidor
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  lId_Cuenta = Txt_Num_Cuenta.Tag
  If Not lId_Cuenta = "" Then
    lcCierres_Cuenta.Campo("id_cuenta").Valor = Txt_Num_Cuenta.Tag
    lcCierres_Cuenta.Campo("fecha_cierre").Valor = pFecha
    If lcCierres_Cuenta.BuscarUltimo Then
        If lcCierres_Cuenta.Cursor.Count > 0 Then
            If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
                If Fnt_Obtiene_FechaCierre_Virtual(lFechaCierreVirtual) Then
                    DTP_Fecha_Ter.Value = lFechaCierreVirtual
                    xFecha_Cierre = lFechaCierreVirtual
                Else
                    DTP_Fecha_Ter.Value = NVL(lcCierres_Cuenta.Cursor(1)("fecha_cierre").Value, "")
                    xFecha_Cierre = NVL(lcCierres_Cuenta.Cursor(1)("fecha_cierre").Value, "")
                End If
            Else
                DTP_Fecha_Ter.Value = NVL(lcCierres_Cuenta.Cursor(1)("fecha_cierre").Value, "")
                xFecha_Cierre = NVL(lcCierres_Cuenta.Cursor(1)("fecha_cierre").Value, "")
            End If
            DTP_Fecha_Ini.Value = UltimoDiaDelMes(DTP_Fecha_Ter.Value)
            lbl_Fecha_Valor_Cuota_Actual.Caption = "" & Format$(DTP_Fecha_Ter.Value, "dd   mmmm, yyyy")
            lbl_Fecha_Valor_Cuota_Anterior.Caption = Format$(DTP_Fecha_Ini.Value, "dd   mmmm, yyyy")
            Toolbar.Buttons("BUSCAR").Enabled = True
            With lcCuenta
              .Campo("id_cuenta").Valor = lId_Cuenta
              If .Buscar_Vigentes Then
                If .Cursor.Count > 0 Then
                  Txt_Perfil.Text = .Cursor(1)("dsc_perfil_riesgo").Value
                  Txt_Rut.Text = "" & .Cursor(1)("rut_cliente").Value
                  Txt_Nombres.Text = "" & .Cursor(1)("nombre_cliente").Value
                  txt_mon_cta.Text = "" & .Cursor(1)("dsc_moneda").Value
                  
                  Call Sub_ComboSelectedItem(Cmb_Moneda, .Cursor(1)("id_moneda").Value)     'Agregado por MMA 10/10/2008
                  
                  txtNombreAsesor.Text = "" & .Cursor(1)("Nombre_asesor").Value
                  
                  lbl_moneda_cta.Caption = txt_mon_cta.Text
                  Grilla.Clear
                End If
              Else
                MsgBox "Problemas en cargar la Cuenta." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
              End If
            End With
        Else
            Toolbar.Buttons("BUSCAR").Enabled = False
            MsgBox "No hay cierres para esta cuentas", vbExclamation
        End If
    Else
        MsgBox "No hay cierres para esta cuentas", vbExclamation
        Toolbar.Buttons("BUSCAR").Enabled = False
    End If
  End If
  Set lcCuenta = Nothing
End If

End Sub
Private Sub cmd_salir_Click()
    Grilla_Detalle.Clear
    Frm_Sombra.Visible = False
    frmCustodia.Visible = False
    Toolbar.Buttons("BUSCAR").Enabled = True
    Toolbar.Buttons("EXIT").Enabled = True
    DTP_Fecha_Ter.Enabled = True
    Cmb_Buscar.Enabled = True
End Sub

Private Sub DTP_Fecha_Ter_Change()
    lbl_Fecha_Valor_Cuota_Actual.Caption = Format$(DTP_Fecha_Ter.Value, "dd   mmmm, yyyy")
End Sub

Private Sub DTP_Fecha_Ini_Change()
    lbl_Fecha_Valor_Cuota_Anterior.Caption = Format$(DTP_Fecha_Ini.Value, "dd   mmmm, yyyy")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If Chr(KeyAscii) = vbTab Then
        Call Cmb_Cuentas_KeyPress(vbKeyReturn)
    End If
End Sub

Private Sub Form_Load()
   
   With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("BUSCAR").Image = "boton_grilla_buscar"
        .Buttons("EXIT").Image = cBoton_Salir
   End With

   Call Sub_CargaForm
   
   Me.Top = 1
   Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
    Dim lProductos As hRecord
    Dim lIndice As Long
    '-------
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_FormControl_Color(Me.Controls)
    'Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, "id_cliente")
    Call Sub_CargaCombo_Monedas(Cmb_Moneda)             'Agregado por MMA. 10/10/2008
    Call Sub_Desbloquea_Puntero(Me)
    Grilla.GridLines = flexGridNone
    Grilla.Clear
    lbl_Fecha_Valor_Cuota_Actual.Caption = "" & Format$(DTP_Fecha_Ter.Value, "dd   mmmm, yyyy")
    lbl_Fecha_Valor_Cuota_Anterior.Caption = Format$(UltimoDiaDelMes(DTP_Fecha_Ter.Value), "dd   mmmm, yyyy")
End Sub

Private Sub Form_Resize()
   Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    Cancel = True
End Sub

Private Sub Grilla_Detalle_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    Cancel = True
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "BUSCAR"
        If fnt_valida_datos() Then
            Call Sub_CargarDatos
        End If
    Case "EXIT"
        Unload Me
  End Select
End Sub

Private Sub Sub_CargarDatos()
'-----------------------------------------------
Dim lReg As hFields
Dim lCursor As Object

Dim lCursor_Cuenta As Object
Dim lCursor_Cajas As Object
Dim Reg_Cajas As Object
Dim Reg_Cuenta  As Object
Dim a As Integer
Dim suma As Double
Dim Patrimonio_Actual As Double
Dim MontoCobrar_Actual  As Double
Dim MontoPagar_Actual As Double
Dim MontoTotalCajas_Actual As Double
Dim Patrimonio_Anterior As Double
Dim MontoCobrar_Anterior  As Double
Dim MontoPagar_Anterior As Double
Dim MontoTotalCajas_Anterior As Double
Dim Fecha_Mes_anterior As String
Dim Decimales As Integer
Dim dsc_caja_cuenta  As String
Dim Mostrado As Integer
Dim nTotal_Actual  As Double
Dim nTotal_Anterior  As Double

Dim nTotalActivos_Anterior As Double
Dim Porc_Monto_mon_cta As Double
Dim monto_mon_cta As Double
Dim Porc_monto_x_pagar As String
Dim prod_anterior As String
Dim nFila As Integer
Dim nRegistros As Integer
Dim I As Integer
Dim ValorCuota_Actual As Double
Dim ValorCuota_Anterior As Double

Dim rentabilidad_mensual_PESOS As String
Dim rentabilidad_mensual_UF As String
Dim rentabilidad_mensual_USD As String
Dim rentabilidad_mensual_CTA As String
Dim rentabilidad_anual_PESOS As String
Dim rentabilidad_anual_UF As String
Dim rentabilidad_anual_USD As String
Dim rentabilidad_anual_CTA As String
Dim rentabilidad_ult_12_meses_PESOS As String
Dim rentabilidad_ult_12_meses_UF As String
Dim rentabilidad_ult_12_meses_USD As String
Dim rentabilidad_ult_12_meses_CTA As String

Dim volatilidad_mensual As String
Dim volatilidad_anual As String
Dim volatilidad_ult_12_meses As String

nTotalActivos_Actual = 0
                        
Dim Forward_Actual As Double
Dim Forward_Anterior As Double
    'Call Sub_CargaCombo_Cajas_Cuenta(Cmb_Cajas, Txt_Num_Cuenta.Tag, pTodos:=True)
    If Cmb_Moneda.Text = "" Then
        Call Sub_ComboSelectedItem(Cmb_Moneda, 1)
    End If
    
    oCliente.IdMonedaSalida = Fnt_ComboSelected_KEY(Cmb_Moneda)
    oCliente.Decimales = Fnt_Obtiene_Decimales_Salida
'--------------------------
    mcol = 3                ' Tree Column
    t = 8                   ' Amount Column
    sp = Space$(5)          ' OutLine Indentention
    oCliente.IdCuenta = Txt_Num_Cuenta.Tag
    oCliente.Fecha_Cartola = DTP_Fecha_Ter.Value
    Call Sub_Bloquea_Puntero(Me)
    lId_Cuenta = oCliente.IdCuenta
  '--------------------------&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
    Decimales = oCliente.Decimales
    Fecha_Mes_anterior = DTP_Fecha_Ini.Value
  
    oCliente.DamePatrimonio "" _
                          , Patrimonio_Actual _
                          , MontoCobrar_Actual _
                          , MontoPagar_Actual _
                          , ValorCuota_Actual _
                          , Forward_Actual
                          
    oCliente.DamePatrimonio Fecha_Mes_anterior _
                          , Patrimonio_Anterior _
                          , MontoCobrar_Anterior _
                          , MontoPagar_Anterior _
                          , ValorCuota_Anterior _
                          , Forward_Anterior
                          
    oCliente.TotalCajas "" _
                      , MontoTotalCajas_Actual
                      
    oCliente.TotalCajas Fecha_Mes_anterior _
                      , MontoTotalCajas_Anterior
                      
    oCliente.Saldos_de_Cajas Detalle_Cajas_Actual(), ""
    oCliente.Saldos_de_Cajas Detalle_Cajas_Anterior(), Fecha_Mes_anterior
    lbl_ValorCuota_Actual.Caption = FormatNumber(ValorCuota_Actual, 4)
    lbl_ValorCuota_Anterior.Caption = FormatNumber(ValorCuota_Anterior, 4)
    If ValorCuota_Anterior > 0 And ValorCuota_Actual > 0 Then
        lbl_Rentabilidad_Periodo = FormatNumber(Round((ValorCuota_Actual / ValorCuota_Anterior - 1) * 100, 2), 2) & "%"
    Else
        lbl_Rentabilidad_Periodo = "N/A"
    End If
    Mostrado = 0
     '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Activos %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

'--------------------------------------------------------------
' Cambia el proceso de carga de activos. MMardones 13/08/2008
'--------------------------------------------------------------
'    gDB.Parametros.Clear
'    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
'    gDB.Parametros.Add "PFecha_Cartola", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
'    gDB.Parametros.Add "PFecha_Anterior", ePT_Fecha, CDate(Fecha_Mes_anterior), ePD_Entrada
'    gDB.Procedimiento = "PKG_CARTOLA$DAME_ACTIVOS_CLASE"
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "PFecha_Cartola", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
    gDB.Parametros.Add "PFecha_Anterior", ePT_Fecha, CDate(Fecha_Mes_anterior), ePD_Entrada
    gDB.Parametros.Add "Pid_moneda_salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
    gDB.Parametros.Add "Pid_Empresa", ePT_Numero, gId_Empresa, ePD_Entrada
    gDB.Parametros.Add "PControl", ePT_Caracter, fFlg_Tipo_Permiso, ePD_Entrada     '19/08/2009
    
    gDB.Procedimiento = "PKG_BALANCE_INVERSIONES_V2$BUSCAR_POR_CUENTA"
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If
    Set lCursor = gDB.Parametros("Pcursor").Valor
    nRegistros = lCursor.Count
    gDB.Parametros.Clear
    SumaTotalActivos lCursor, nTotalActivos_Actual, nTotalActivos_Anterior
    nTotalActivos_Actual = nTotalActivos_Actual + MontoTotalCajas_Actual + MontoCobrar_Actual
    nTotalActivos_Anterior = nTotalActivos_Anterior + MontoTotalCajas_Anterior + MontoCobrar_Anterior
    With Grilla
        .Rows = 3
        nFila = 2
        For I = 0 To 5
            .ColWidth(I) = 200
        Next I
        .ColWidth(mcol) = 800   ' more space for the tree column
        .GridLines = flexGridNone
        .ScrollTrack = False
        .ScrollTips = True
        .Editable = True
        .BackColorBkg = vbWhite
        .SheetBorder = vbWhite
        .Cell(flexcpText, nFila - 2, mcol) = "Fecha Operativa Cuenta"
        .Cell(flexcpText, nFila - 2, t) = Format$(CDate(xFecha_Operativa), "dd   mmmm, yyyy")
        .Cell(flexcpFontSize, nFila - 2, mcol) = 11
        .Cell(flexcpForeColor, nFila - 2, mcol) = vbBlue
        .Cell(flexcpForeColor, nFila - 2, t) = vbBlue
        .RowHeight(nFila - 2) = 300
        .Cell(flexcpText, nFila, mcol) = "Patrimonio Administrador"
        .Cell(flexcpFontSize, nFila, mcol) = 12
        .Cell(flexcpForeColor, nFila, mcol) = vbBlue
        .RowHeight(nFila) = 300
        .Cell(flexcpText, nFila, t) = "AL   " & Format$(DTP_Fecha_Ter.Value, "dd   mmmm, yyyy") & "   "
        .Cell(flexcpText, nFila, t + 4) = "AL   " & Format$(Fecha_Mes_anterior, "dd   mmmm, yyyy") & "   "
        .Cell(flexcpForeColor, nFila, t) = vbBlue
        .Cell(flexcpForeColor, nFila, t + 4) = vbBlue
        .Cell(flexcpPicture, nFila, t - 1) = Image4.Picture
        .Cell(flexcpPicture, nFila, t + 1) = Image4.Picture
        .Cell(flexcpPicture, nFila, (t + 4) - 1) = Image4.Picture
        .Cell(flexcpPicture, nFila, (t + 4) + 1) = Image4.Picture
        If Decimales = 0 Then
            .ColFormat(t) = "#,##0"
            .ColFormat(t + 4) = "#,##0"
            .ColFormat(t + 2) = "#,##0%"
        Else
            .ColFormat(t) = "#,##0.#0"
            .ColFormat(t + 4) = "#,##0.#0"
        End If
        .ColFormat(t + 1) = "##0.#0%"
        .ColFormat(t + 5) = "##0.#0%"
        '------------------------------------------------------------
        ' create outline nodes and data
        '------------------------------------------------------------
        Grilla.AddItem ""
        nFila = nFila + 1
        Agrupa nFila, 0
        .Cell(flexcpText, nFila, mcol) = "PATRIMONIO"
        .Cell(flexcpText, nFila, t) = Patrimonio_Actual
        .Cell(flexcpBackColor, nFila, t) = &HC0FFC0
        .Cell(flexcpText, nFila, t + 4) = Patrimonio_Anterior
        .Cell(flexcpBackColor, nFila, t + 4) = &HC0FFC0
        Grilla.AddItem ""
        nFila = nFila + 1
        Agrupa nFila, 1
        .Cell(flexcpText, nFila, mcol) = "ACTIVOS"
        .Cell(flexcpText, nFila, t) = nTotalActivos_Actual
        If nTotalActivos_Actual > 0 Then
            .Cell(flexcpText, nFila, t + 1) = IIf(nTotalActivos_Actual <> 0, 1, 0)
            .Cell(flexcpText, nFila, t + 5) = IIf(nTotalActivos_Actual <> 0, 1, 0)
        Else
            .Cell(flexcpText, nFila, t + 1) = 0
            .Cell(flexcpText, nFila, t + 5) = 0
        End If
        .Cell(flexcpText, nFila, t + 4) = nTotalActivos_Anterior
        Grilla.AddItem ""
        nFila = nFila + 1
        Agrupa nFila, 2
        .Cell(flexcpText, nFila, mcol) = "CAJA"
        
        '&H00C0FFFF&
        .Cell(flexcpText, nFila, t) = FormatNumber(MontoTotalCajas_Actual, oCliente.Decimales)
        .Cell(flexcpBackColor, nFila, t) = &HC0FFFF
        .Cell(flexcpText, nFila, t + 1) = FormatPorcentaje(MontoTotalCajas_Actual, IIf(nTotalActivos_Actual <> 0, nTotalActivos_Actual, 0))
        .Cell(flexcpText, nFila, t + 4) = FormatNumber(MontoTotalCajas_Anterior, oCliente.Decimales)
        .Cell(flexcpBackColor, nFila, t + 4) = &HC0FFFF
        .Cell(flexcpText, nFila, t + 5) = FormatPorcentaje(MontoTotalCajas_Anterior, IIf(nTotalActivos_Anterior <> 0, nTotalActivos_Anterior, 0))
        .IsCollapsed(nFila) = 2
        Grilla.AddItem ""
        nFila = nFila + 1
        For I = 0 To UBound(Detalle_Cajas_Actual)
            .Cell(flexcpText, nFila, mcol) = sp & Detalle_Cajas_Actual(I).Descripcion
            .Cell(flexcpText, nFila, t) = FormatNumber(CDbl(Detalle_Cajas_Actual(I).saldo), oCliente.Decimales)
            .Cell(flexcpText, nFila, t + 1) = FormatPorcentaje(CDbl(Detalle_Cajas_Actual(I).saldo), IIf(nTotalActivos_Actual <> 0, nTotalActivos_Actual, 0))
            .Cell(flexcpText, nFila, t + 4) = FormatNumber(CDbl(Detalle_Cajas_Anterior(I).saldo), oCliente.Decimales)
            .Cell(flexcpText, nFila, t + 5) = FormatPorcentaje(CDbl(Detalle_Cajas_Anterior(I).saldo), IIf(nTotalActivos_Anterior <> 0, nTotalActivos_Anterior, 0))
            Grilla.AddItem ""
            nFila = nFila + 1
        Next I
        .IsCollapsed(nFila) = 2
        Grilla.AddItem ""
        nFila = nFila + 1
        Agrupa nFila, 2
        .Cell(flexcpText, nFila, mcol) = UCase("Cuentas Por Cobrar")
        .Cell(flexcpText, nFila, t) = FormatNumber(MontoCobrar_Actual, oCliente.Decimales)
        .Cell(flexcpBackColor, nFila, t) = &HC0FFFF
        .Cell(flexcpText, nFila, t + 1) = FormatPorcentaje(MontoCobrar_Actual, IIf(nTotalActivos_Actual <> 0, nTotalActivos_Actual, 0))
        .Cell(flexcpText, nFila, t + 4) = FormatNumber(MontoCobrar_Anterior, oCliente.Decimales)
        .Cell(flexcpBackColor, nFila, t + 4) = &HC0FFFF
        .Cell(flexcpText, nFila, t + 5) = FormatPorcentaje(MontoCobrar_Anterior, IIf(nTotalActivos_Anterior <> 0, nTotalActivos_Anterior, 0))
        .IsCollapsed(nFila) = 2
        For Each lReg In lCursor
'             If Mostrado = 0 Or prod_anterior <> lReg("DSC_CLASE").Value Then   **** MMA 13/08/2008
              If Mostrado = 0 Or prod_anterior <> lReg("DSC_ARBOL").Value Then
                If Mostrado = 1 Then
                    Grilla.AddItem ""
                    Grilla.AddItem ""
                    nFila = nFila + 1
                    .Cell(flexcpText, nFila, mcol) = "Total " & Trim(prod_anterior)
                    .Cell(flexcpText, nFila, t) = FormatNumber(nTotal_Actual, oCliente.Decimales)
                    .Cell(flexcpText, nFila, t + 4) = FormatNumber(nTotal_Anterior, oCliente.Decimales)
                    .Col = t + 4
                    .Row = nFila
                    .CellBorder vbBlack, 0, 1, 0, 0, 0, 0
                    .Col = t
                    .Row = nFila
                    .CellBorder vbBlack, 0, 1, 0, 0, 0, 0
                    nFila = nFila + 1
                    .IsCollapsed(nFila) = 2
                End If
                Grilla.AddItem ""
                nFila = nFila + 1
                Agrupa nFila, 2
                nTotal_Actual = 0
                nTotal_Anterior = 0
'               SumaTotalClase lCursor, lReg("DSC_CLASE").Value, nTotal_Actual, nTotal_Anterior     **** MMA 13/08/2008
                SumaTotalClase lCursor, lReg("DSC_ARBOL").Value, nTotal_Actual, nTotal_Anterior
                Mostrado = 1
'               prod_anterior = lReg("DSC_CLASE").Value                                             **** MMA 13/08/2008
                prod_anterior = lReg("DSC_ARBOL").Value
                .Cell(flexcpText, nFila, mcol) = UCase(prod_anterior)
                .Cell(flexcpText, nFila, t) = FormatNumber(nTotal_Actual, oCliente.Decimales)
                .Cell(flexcpBackColor, nFila, t) = &HC0FFFF
                .Cell(flexcpText, nFila, t + 1) = FormatPorcentaje(nTotal_Actual, IIf(nTotalActivos_Actual <> 0, nTotalActivos_Actual, 0))
                .Cell(flexcpText, nFila, t + 4) = FormatNumber(nTotal_Anterior, oCliente.Decimales)
                .Cell(flexcpBackColor, nFila, t + 4) = &HC0FFFF
                .Cell(flexcpText, nFila, t + 5) = FormatPorcentaje(nTotal_Anterior, IIf(nTotalActivos_Anterior <> 0, nTotalActivos_Anterior, 0))
            End If
'           If NVL(lReg("DSC_SUBCLASE").Value, "") <> "" Then                                       **** MMA 13/08/2008
            If NVL(lReg("DSC_ARBOL_CLASE_INST").Value, "") <> "" Then
                Grilla.AddItem ""
                nFila = nFila + 1
'               .Cell(flexcpText, nFila, mcol) = sp & NVL(lReg("DSC_SUBCLASE").Value, "")           **** MMA 13/08/2008
                .Cell(flexcpText, nFila, mcol) = sp & NVL(lReg("DSC_ARBOL_CLASE_INST").Value, "")
                .Cell(flexcpText, nFila, t) = FormatNumber(CDbl(lReg("Monto_Actual").Value), oCliente.Decimales)
                .Cell(flexcpText, nFila, t + 1) = FormatPorcentaje(CDbl(lReg("Monto_Actual").Value), IIf(nTotalActivos_Actual <> 0, nTotalActivos_Actual, 0))
                .Cell(flexcpText, nFila, t + 4) = FormatNumber(CDbl(lReg("Monto_Anterior").Value), oCliente.Decimales)
                .Cell(flexcpText, nFila, t + 5) = FormatPorcentaje(CDbl(lReg("Monto_Anterior").Value), IIf(nTotalActivos_Anterior <> 0, nTotalActivos_Anterior, 0))
                .Cell(flexcpText, nFila, t + 10) = lReg("CANT_HOJA").Value
                .Cell(flexcpText, nFila, t + 11) = lReg("CANT_ANTERIOR_HOJA").Value
                .Cell(flexcpText, nFila, 0) = NVL(lReg("id_arbol_clase_inst").Value, 0)
            End If
        Next
        .IsCollapsed(nFila) = 2
        Grilla.AddItem ""
        nFila = nFila + 1
        Agrupa nFila, 1
        .Cell(flexcpText, nFila, mcol) = "PASIVOS"
        .Cell(flexcpText, nFila, t) = FormatNumber(MontoPagar_Actual, oCliente.Decimales)
        Grilla.AddItem ""
        nFila = nFila + 1
        .Cell(flexcpText, nFila, mcol) = sp & "Administraci�n"
        .Cell(flexcpText, nFila, t) = "0"
        Grilla.AddItem ""
        nFila = nFila + 1
        .Cell(flexcpText, nFila, mcol) = sp & "Cuentas Por Pagar"
        .Cell(flexcpText, nFila, t) = FormatNumber(MontoPagar_Actual, oCliente.Decimales)
        Grilla.AddItem ""
        nFila = nFila + 1
        .Cell(flexcpText, nFila, mcol) = sp & "Forwards"
        If Forward_Actual > 0 Then
            Forward_Actual = 0
        Else
            Forward_Actual = Forward_Actual * -1
        End If
        If Forward_Anterior > 0 Then
            Forward_Anterior = 0
        Else
            Forward_Anterior = Forward_Anterior * -1
        End If
        
        .Cell(flexcpText, nFila, t) = FormatNumber(Forward_Actual, oCliente.Decimales)
        
        .IsCollapsed(nFila) = 2
        '.Cell(flexcpFontBold, 4, (t + 4), .Rows - 1, (t + 4)) = False ' set font for amounts
        
        ' make it look good
        .AutoSizeMode = flexAutoSizeColWidth
        .AutoSize mcol
        .AutoSize t
        .AutoSize (t + 4)
        .MergeCells = flexMergeSpill
        .ColWidth(t - 1) = 300
        .ColWidth(t + 1) = 300
        .ColWidth((t + 4) - 1) = 300
        .ColWidth((t + 4) + 1) = 300
    End With
  '--------------------------&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&666
    RentabilidadCartera DTP_Fecha_Ter.Value, _
                        oCliente.IdCuenta, _
                        rentabilidad_mensual_PESOS, _
                        rentabilidad_mensual_UF, _
                        rentabilidad_mensual_USD, _
                        rentabilidad_mensual_CTA, _
                        rentabilidad_anual_PESOS, _
                        rentabilidad_anual_UF, _
                        rentabilidad_anual_USD, _
                        rentabilidad_anual_CTA, _
                        rentabilidad_ult_12_meses_PESOS, _
                        rentabilidad_ult_12_meses_UF, _
                        rentabilidad_ult_12_meses_USD, _
                        rentabilidad_ult_12_meses_CTA, _
                        volatilidad_mensual, _
                        volatilidad_anual, _
                        volatilidad_ult_12_meses
                        
    lbl_Ret_Men_PESOS.Caption = rentabilidad_mensual_PESOS
    lbl_Ret_Men_UF.Caption = rentabilidad_mensual_UF
    lbl_Ret_Men_USD.Caption = rentabilidad_mensual_USD
    lbl_Ret_Men_cta.Caption = rentabilidad_mensual_CTA
    
    lbl_Ret_Anu_PESOS.Caption = rentabilidad_anual_PESOS
    lbl_Ret_Anu_UF.Caption = rentabilidad_anual_UF
    lbl_Ret_Anu_USD.Caption = rentabilidad_anual_USD
    lbl_Ret_Anu_cta.Caption = rentabilidad_anual_CTA
    
    lbl_Ret_Ult_PESOS.Caption = rentabilidad_ult_12_meses_PESOS
    lbl_Ret_Ult_UF.Caption = rentabilidad_ult_12_meses_UF
    lbl_Ret_Ult_USD.Caption = rentabilidad_ult_12_meses_USD
    lbl_Ret_Ult_cta.Caption = rentabilidad_ult_12_meses_CTA
  
    Call Sub_Desbloquea_Puntero(Me)
End Sub

'Public Function Mostrar(pTipo_Operacion, pOper_Fecha_Anterior As Boolean) As Boolean
'  Call DockForm(Me, MDI_Principal.PictureBox, True)
'  fTipo_Operacion = pTipo_Operacion
'  fKey = fTipo_Operacion
'
'  fOper_Fecha_Anterior = pOper_Fecha_Anterior
'
'  With Me
'    Select Case fTipo_Operacion
'      Case gcOPERACION_Directa
'        .Caption = "Ingreso de Operaciones Directas"
'        .Tag = "Directa"
'      Case gcOPERACION_Custodia
'        .Caption = "Ingreso de Operaciones de Custodia"
'        .Tag = "Custodia"
'      Case gcOPERACION_Instruccion
'        .Caption = "Ingreso de Operaciones de Instrucci�n"
'        .Tag = "Instrucci�n"
'      Case Else
'        .Caption = "Ingreso de Operaciones Varias"
'        .Tag = ""
'    End Select
'    .Show
'    .SetFocus
'  End With
'End Function

Function SumaTotalClase(ByVal lCursor As hRecord, _
                        ByVal sClase As String, _
                        ByRef nTotal_Actual As Double, _
                        ByRef nTotal_Anterior As Double) As Double
    Dim lReg As hFields
    
    nTotal_Anterior = 0
    nTotal_Actual = 0
    
    For Each lReg In lCursor
        'If sClase = lReg("DSC_CLASE").Value Then
        If sClase = lReg("DSC_ARBOL").Value Then
            nTotal_Actual = nTotal_Actual + CDbl(lReg("Monto_Actual").Value)
            nTotal_Anterior = nTotal_Anterior + CDbl(lReg("Monto_Anterior").Value)
        End If
    Next
   
End Function

Private Sub Agrupa(Row, lvl)
    ' set the row as a group
    Grilla.IsSubtotal(Row) = True
    ' set the indentation level of the group
    Grilla.RowOutlineLevel(Row) = lvl
    If lvl = 0 Then Exit Sub
    If lvl = 1 Then
        Grilla.Cell(flexcpForeColor, Row, t) = vbWhite
        Grilla.Cell(flexcpBackColor, Row, t) = vbBlue
        Grilla.Cell(flexcpForeColor, Row, t + 4) = vbWhite
        Grilla.Cell(flexcpBackColor, Row, t + 4) = vbBlue
    End If
    Grilla.Cell(flexcpFontBold, Row, t) = True
    Grilla.Cell(flexcpFontBold, Row, t + 4) = True
End Sub

Private Sub grilla_BeforeScrollTip(ByVal Row As Long)
    If Row < 3 Or Grilla.Cell(flexcpTextDisplay, Row, 3) = "" Then Exit Sub
    Grilla.ScrollTipText = " " & Trim$(Grilla.Cell(flexcpTextDisplay, Row, 3)) & " "
End Sub

Private Sub Grilla_DblClick()
'------------------------------------------------------------------------------------
' Se agrega las columnas de montos actuales(col=8) y anteriores (col=12) para la llamada al Detalle
' MMardones 13/08/2008
'-----------------------------------------------------------------------------------
Dim lEsForward As String
Dim lRama As String

    lRama = Grilla.Cell(flexcpText, Grilla.Row, 3)
    If UCase(Trim(lRama)) = "FORWARDS" Then
        lEsForward = "S"
    Else
        lEsForward = "N"
    End If
    
    If Grilla.Col = 8 Or Grilla.Col = 3 Then
        
        'If Val(Grilla.Cell(flexcpText, Grilla.Row, 0)) <> 0 And Val(Grilla.Cell(flexcpText, Grilla.Row, t)) <> 0 Then/*SE CAMBIA POR (CANTIDAD_HOJA Y CANTIDAD_ANTERIOR_HOJA) 09-05-2017 RACV*/
        If Grilla.Cell(flexcpText, Grilla.Row, t + 10) > 0 And Grilla.Cell(flexcpText, Grilla.Row, t + 11) > 0 Then
            If InStr(Grilla.Cell(flexcpText, Grilla.Row, t + 1), "%") > 0 Then
                Call Entrega_Custodia(fecha:=DTP_Fecha_Ter.Value _
                                    , idSubclase:=Val(Grilla.Cell(flexcpText, Grilla.Row, 0)) _
                                    , Porc_Subclase:=Val(Mid(Grilla.Cell(flexcpText, Grilla.Row, t + 1), 1, InStr(Grilla.Cell(flexcpText, Grilla.Row, t + 1), "%") - 1)) _
                                    , pEsForward:=lEsForward)
            Else
                Call Entrega_Custodia(fecha:=DTP_Fecha_Ter.Value _
                                    , idSubclase:=Val(Grilla.Cell(flexcpText, Grilla.Row, 0)) _
                                    , Porc_Subclase:=Val(Mid(Grilla.Cell(flexcpText, Grilla.Row, t + 1), 1, InStr(Grilla.Cell(flexcpText, Grilla.Row, t + 1), "%"))) _
                                    , pEsForward:=lEsForward)
            End If
        End If
        Exit Sub
    End If
     If Grilla.Col = 12 Then
        'If Val(Grilla.Cell(flexcpText, Grilla.Row, 0)) <> 0 And Val(Grilla.Cell(flexcpText, Grilla.Row, t)) <> 0 Then /*SE CAMBIA POR (CANTIDAD_HOJA Y CANTIDAD_ANTERIOR_HOJA) 09-05-2017 RACV*/
        If Grilla.Cell(flexcpText, Grilla.Row, t + 10, 0) <> 0 And Grilla.Cell(flexcpText, Grilla.Row, t + 11) <> 0 Then
            If InStr(Grilla.Cell(flexcpText, Grilla.Row, t + 1), "%") > 0 Then
                Entrega_Custodia DTP_Fecha_Ini.Value, Val(Grilla.Cell(flexcpText, Grilla.Row, 0)), Val(Mid(Grilla.Cell(flexcpText, Grilla.Row, t + 1), 1, InStr(Grilla.Cell(flexcpText, Grilla.Row, t + 1), "%") - 1)), lEsForward
            Else
                Entrega_Custodia DTP_Fecha_Ini.Value, Val(Grilla.Cell(flexcpText, Grilla.Row, 0)), Val(Mid(Grilla.Cell(flexcpText, Grilla.Row, t + 1), 1, InStr(Grilla.Cell(flexcpText, Grilla.Row, t + 1), "%"))), lEsForward
            End If
        End If
        Exit Sub
    End If
    If Grilla.IsCollapsed(Grilla.Row) = flexOutlineCollapsed Then
        Grilla.IsCollapsed(Grilla.Row) = flexOutlineExpanded
    Else
        Grilla.IsCollapsed(Grilla.Row) = flexOutlineCollapsed
    End If
End Sub

Private Sub grilla_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim I#
    Static r&, c&

    ' avoid extra work
    If Grilla.MouseRow = r Then Exit Sub
    ' MouseRow returns -1 when not over sheet
    If Grilla.MouseRow = -1 Then Exit Sub
    r = Grilla.MouseRow

    ' move selection (even with no click)
    Grilla.Select r, 8
    
    ' remove cursor image
    Grilla.Cell(flexcpPicture, 3, 7, Grilla.Rows - 1) = imgClear
    Grilla.Cell(flexcpPicture, 3, 9, Grilla.Rows - 1) = imgClear
    
    Grilla.Cell(flexcpPicture, 3, 11, Grilla.Rows - 1) = imgClear
    Grilla.Cell(flexcpPicture, 3, 13, Grilla.Rows - 1) = imgClear
    

    ' show cursor image if there is some text in column 5
    If r < 3 Or Grilla.Cell(flexcpText, r, 8) = "" Then Exit Sub
    Grilla.Cell(flexcpPicture, r, 7) = Image3.Picture
    Grilla.Cell(flexcpPicture, r, 9) = Image1.Picture
    Grilla.Cell(flexcpPicture, r, 11) = Image3.Picture
    Grilla.Cell(flexcpPicture, r, 13) = Image1.Picture
    
End Sub

Private Function FormatPorcentaje(Valor As Double, Valor_Total As Double) As String
    If Valor_Total <> 0 Then
        FormatPorcentaje = FormatNumber((Valor / Valor_Total) * 100, 2) & "%"
    Else
        FormatPorcentaje = FormatNumber(0, 2) & "%"
    End If
End Function

Function SumaTotalActivos(ByVal lCursor As hRecord, _
                        ByRef nTotal_Actual As Double, _
                        ByRef nTotal_Anterior As Double) As Double
    Dim lReg As hFields
    nTotal_Anterior = 0
    nTotal_Actual = 0
    For Each lReg In lCursor
        nTotal_Actual = nTotal_Actual + CDbl(lReg("Monto_Actual").Value)
        nTotal_Anterior = nTotal_Anterior + CDbl(lReg("Monto_Anterior").Value)
    Next
End Function

Function FormatNumber(xValor As Variant, iDecimales As Integer, Optional bPorcentaje As Boolean = False) As String
    Dim xFormato        As String
    Dim sDecimalSep     As String
    Dim sMilesSep       As String
    sDecimalSep = Obtener_Simbolo(LOCALE_SDECIMAL)
    sMilesSep = Obtener_Simbolo(LOCALE_SMONTHOUSANDSEP)
    On Error GoTo PError
    If iDecimales = 0 Then
        xFormato = "###,###,##0"
    ElseIf bPorcentaje Then
        xFormato = "##0." & String(iDecimales - 1, "#") & "0"
    Else
        xFormato = "###,###,##0." & String(iDecimales - 1, "#") & "0"
    End If
    GoTo Fin
PError:
    xValor = "0"
Fin:
    FormatNumber = Format(xValor, xFormato)
    On Error GoTo 0
End Function

Private Function Obtener_Simbolo(Valor As Long) As String
   Dim Simbolo  As String
   Dim r1       As Long
   Dim r2       As Long
   Dim p        As Integer
   Dim Locale   As Long
   'Locale = GetUserDefaultLCID()
   r1 = GetLocaleInfo(Locale, Valor, vbNullString, 0)
   'buffer
   Simbolo = String$(r1, 0)
   'En esta llamada devuelve el s�mbolo en el Buffer
   r2 = GetLocaleInfo(Locale, Valor, Simbolo, r1)
   'Localiza el espacio nulo de la cadena para eliminarla
   p = InStr(Simbolo, Chr$(0))
   If p > 0 Then
      'Elimina los nulos
      Obtener_Simbolo = Left$(Simbolo, p - 1)
   End If
End Function

Private Sub Entrega_Custodia(fecha As String, idSubclase As Integer, Porc_Subclase As Double, pEsForward As String)
Dim lReg As hFields
Dim lCursor As hRecord
Dim nFlag As Integer
Dim nFila As Integer
Dim nPorc_Total As Double
Dim nTotReg As Integer


    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_Cierre", ePT_Fecha, CDate(fecha), ePD_Entrada
    gDB.Parametros.Add "pId_Arbol_Clase", ePT_Numero, idSubclase, ePD_Entrada
    gDB.Parametros.Add "pId_Moneda_Salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada   'MMA 09/09/2008
    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO"
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If
    frmCustodia.Visible = True
    Frm_Sombra.Visible = True
    Toolbar.Buttons("BUSCAR").Enabled = False
    Toolbar.Buttons("EXIT").Enabled = False
    DTP_Fecha_Ter.Enabled = False
    Cmb_Buscar.Enabled = False
    Set lCursor = gDB.Parametros("Pcursor").Valor
    gDB.Parametros.Clear
    nFlag = 0
    nFila = 1
    nTotReg = lCursor.Count
    lblFrameCustodia.Caption = "CUSTODIA AL " & CDate(fecha) & "  " & UCase(Grilla.Cell(flexcpText, Grilla.Row, mcol))
    If pEsForward = "N" Then
        With Grilla_Detalle
            For Each lReg In lCursor
                Select Case lReg("codigo").Value
                    Case "RV"
                        'MENU DE ACCIONES'
                        If nFlag = 0 Then
                            .ColHidden(Colum_Ocho) = True
                            .ColHidden(Colum_Nueve) = True
                            
                            .Cell(flexcpText, 0, Colum_Cero) = "Nemot�cnico"
                            .Cell(flexcpText, 0, Colum_Uno) = "Cantidad"
                            .Cell(flexcpText, 0, Colum_Dos) = "Precio"
                            .Cell(flexcpText, 0, Colum_Tres) = "Precio Promedio"
                            .Cell(flexcpText, 0, Colum_Cuatro) = "Valor a Precio Promedio"
                            .Cell(flexcpText, 0, Colum_Cinco) = "Precio Mercado"
                            .Cell(flexcpText, 0, Colum_Seis) = "Valor Mercado"
                            .Cell(flexcpText, 0, Colum_Siete) = "% Cartera"
                            nFlag = 1
                        End If
                        .Cell(flexcpText, nFila, Colum_Cero) = lReg("nemotecnico").Value
                        .Cell(flexcpText, nFila, Colum_Uno) = CDbl(NVL(lReg("cantidad").Value, 0))
                        .Cell(flexcpText, nFila, Colum_Dos) = FormatNumber(CDbl(NVL(lReg("precio").Value, 0)), 4)   'cambia a 4 dec. 10/10/2008
                        .Cell(flexcpText, nFila, Colum_Tres) = FormatNumber(CDbl(NVL(lReg("precio_promedio_compra").Value, 0)), 4)
                        .Cell(flexcpText, nFila, Colum_Cuatro) = FormatNumber(CDbl(NVL(lReg("cantidad").Value, 0)) * CDbl(NVL(lReg("precio_promedio_compra").Value, 0)), oCliente.Decimales) ' cambia a 4 decimales. 10/10/2008 oCliente.Decimales)
                        .Cell(flexcpText, nFila, Colum_Cinco) = FormatNumber(CDbl(NVL(lReg("precio").Value, 0)), 4)
                        .Cell(flexcpText, nFila, Colum_Seis) = FormatNumber(CDbl(NVL(lReg("monto_mon_cta").Value, 0)), oCliente.Decimales)
                        If nFila = nTotReg Then
                            .Cell(flexcpText, nFila, Colum_Siete) = FormatNumber(Porc_Subclase - nPorc_Total, 2) & "%"
                        Else
                            .Cell(flexcpText, nFila, Colum_Siete) = FormatPorcentaje(CDbl(NVL(lReg("monto_mon_cta").Value, 0)), nTotalActivos_Actual)
                        End If
                        nPorc_Total = nPorc_Total + Round((CDbl(NVL(lReg("monto_mon_cta").Value, 0)) / nTotalActivos_Actual) * 100, 2)
                        
                    Case "FFMM", "FM_OA"
                        If nFlag = 0 Then
                            'MENU DE FONDOS MUTUOS
                            .ColHidden(Colum_Ocho) = True
                            .ColHidden(Colum_Nueve) = True
                            
                            .Cell(flexcpText, 0, Colum_Cero) = "Nemot�cnico"
                            .Cell(flexcpText, 0, Colum_Uno) = "Nro. Cuotas"
                            .Cell(flexcpText, 0, Colum_Dos) = "Precio"
                            .Cell(flexcpText, 0, Colum_Tres) = "VC. Promedio"
                            .Cell(flexcpText, 0, Colum_Cuatro) = "Valor a VC. Promedio"
                            .Cell(flexcpText, 0, Colum_Cinco) = "VC. Cierre"
                            .Cell(flexcpText, 0, Colum_Seis) = "Valor VC. Cierre"
                            .Cell(flexcpText, 0, Colum_Siete) = "% Cartera"
                            nFlag = 1
                        End If
                        
                        .Cell(flexcpText, nFila, Colum_Cero) = lReg("dsc_nemotecnico").Value
                        .Cell(flexcpText, nFila, Colum_Uno) = CDbl(NVL(lReg("cantidad").Value, 0))
                        .Cell(flexcpText, nFila, Colum_Dos) = FormatNumber(CDbl(NVL(lReg("precio").Value, 0)), 4)   'cambia a 4 dec. 10/10/2008
                        .Cell(flexcpText, nFila, Colum_Tres) = FormatNumber(CDbl(NVL(lReg("precio_promedio_compra").Value, 0)), 4)
                        .Cell(flexcpText, nFila, Colum_Cuatro) = FormatNumber(CDbl(NVL(lReg("cantidad").Value, 0)) * CDbl(NVL(lReg("precio_promedio_compra").Value, 0)), oCliente.Decimales) ' cambia a 4 decimales. 10/10/2008 oCliente.Decimales)
                        .Cell(flexcpText, nFila, Colum_Cinco) = FormatNumber(CDbl(NVL(lReg("precio").Value, 0)), 4)
                        .Cell(flexcpText, nFila, Colum_Seis) = FormatNumber(CDbl(NVL(lReg("monto_mon_cta").Value, 0)), oCliente.Decimales)
                        If nFila = nTotReg Then
                            .Cell(flexcpText, nFila, Colum_Siete) = FormatNumber(Porc_Subclase - nPorc_Total, 2) & "%"
                        Else
                            .Cell(flexcpText, nFila, Colum_Siete) = FormatPorcentaje(CDbl(NVL(lReg("monto_mon_cta").Value, 0)), nTotalActivos_Actual)
                        End If
                        If nTotalActivos_Actual > 0 Then
                           nPorc_Total = nPorc_Total + Round((CDbl(NVL(lReg("monto_mon_cta").Value, 0)) / nTotalActivos_Actual) * 100, 2)
                        Else
                           nPorc_Total = 0
                        End If
                            
                    Case "RF"
                            'MENU DE RENTA FIJA
                        If nFlag = 0 Then
                            .ColHidden(Colum_Ocho) = True
                            .ColHidden(Colum_Nueve) = True
                            
                            .Cell(flexcpText, 0, Colum_Cero) = "Nemot�cnico"
                            .Cell(flexcpText, 0, Colum_Uno) = "Nominales"
                            .Cell(flexcpText, 0, Colum_Dos) = "Tasa Emisi�n"
                            .Cell(flexcpText, 0, Colum_Tres) = "Tasa Compra"
                            .Cell(flexcpText, 0, Colum_Cuatro) = "Valor Compra"
                            .Cell(flexcpText, 0, Colum_Cinco) = "Tasa Mercado"
                            .Cell(flexcpText, 0, Colum_Seis) = "Valor Mercado"
                            .Cell(flexcpText, 0, Colum_Siete) = "% Cartera"
                            nFlag = 1
                        End If
                        
                        .Cell(flexcpText, nFila, Colum_Cero) = lReg("nemotecnico").Value
                        .Cell(flexcpText, nFila, Colum_Uno) = CDbl(NVL(lReg("cantidad").Value, 0))
                        .Cell(flexcpText, nFila, Colum_Dos) = FormatNumber(CDbl(NVL(lReg("tasa_emision").Value, 0)), 4)   'cambia a 4 dec. 10/10/2008
                        .Cell(flexcpText, nFila, Colum_Tres) = FormatNumber(CDbl(NVL(lReg("TASA_COMPRA").Value, 0)), 4)   'cambia a 4 dec. 10/10/2008
                        .Cell(flexcpText, nFila, Colum_Cuatro) = FormatNumber(CDbl(NVL(lReg("monto_valor_compra").Value, 0)), oCliente.Decimales) 'cambia a 4 decimales 10/10/2008 oCliente.Decimales)
                        .Cell(flexcpText, nFila, Colum_Cinco) = FormatNumber(CDbl(NVL(lReg("tasa").Value, 0)), 4)
                        .Cell(flexcpText, nFila, Colum_Seis) = FormatNumber(CDbl(NVL(lReg("monto_mon_cta").Value, 0)), oCliente.Decimales)
                        If nFila = nTotReg Then
                            .Cell(flexcpText, nFila, Colum_Siete) = FormatNumber(Porc_Subclase - nPorc_Total, 2) & "%"
                        Else
                            .Cell(flexcpText, nFila, Colum_Siete) = FormatPorcentaje(CDbl(NVL(lReg("monto_mon_cta").Value, 0)), nTotalActivos_Actual)
                        End If
                        nPorc_Total = nPorc_Total + Round((CDbl(NVL(lReg("monto_mon_cta").Value, 0)) / nTotalActivos_Actual) * 100, 2)
                            
                    Case "RFINT", "RVINT", "FM_INT"
                            'MENU INTERNACIONAL
                        If nFlag = 0 Then
                            .ColHidden(Colum_Ocho) = False
                            .ColHidden(Colum_Nueve) = False
                            
                            .Cell(flexcpText, 0, Colum_Cero) = "Plataforma"
                            .Cell(flexcpText, 0, Colum_Uno) = "Instrumento"
                            .Cell(flexcpText, 0, Colum_Dos) = "Moneda"
                            .Cell(flexcpText, 0, Colum_Tres) = "Cantidad"
                            .Cell(flexcpText, 0, Colum_Cuatro) = "Precio Compra"
                            .Cell(flexcpText, 0, Colum_Cinco) = "Monto Invertido"
                            .Cell(flexcpText, 0, Colum_Seis) = "Precio Actual"
                            .Cell(flexcpText, 0, Colum_Siete) = "Valor Mercado"
                            .Cell(flexcpText, 0, Colum_Ocho) = "Valorizaci�n en USD"
                            .Cell(flexcpText, 0, Colum_Nueve) = "Valorizaci�n en $"
                            nFlag = 1
                        End If
                        
                        If lReg("origen").Value = "PERSHING" Then
                            .Cell(flexcpText, nFila, Colum_Cero) = "Pershing"
                        Else
                            .Cell(flexcpText, nFila, Colum_Cero) = "Valores Internacionales"
                        End If
                        .Cell(flexcpText, nFila, Colum_Uno) = lReg("dsc_nemotecnico").Value
                        .Cell(flexcpText, nFila, Colum_Dos) = lReg("simbolo_moneda").Value
                        .Cell(flexcpText, nFila, Colum_Tres) = FormatNumber(lReg("cantidad").Value, 0)
                        .Cell(flexcpText, nFila, Colum_Cuatro) = FormatNumber(lReg("precio_compra").Value, 4)
                        .Cell(flexcpText, nFila, Colum_Cinco) = FormatNumber(lReg("monto_invertido").Value, 2)
                        .Cell(flexcpText, nFila, Colum_Seis) = FormatNumber(lReg("precio_actual").Value, 4)
                        .Cell(flexcpText, nFila, Colum_Siete) = FormatNumber(lReg("valor_mercado").Value, 2)
                        .Cell(flexcpText, nFila, Colum_Ocho) = FormatNumber(lReg("monto_mon_usd").Value, 2)
                        .Cell(flexcpText, nFila, Colum_Nueve) = FormatNumber(lReg("monto_mon_cta").Value, 0)
                        
                    Case "NOTAE"
                        'NOTAS ESTRUCTURADAS
                        If nFlag = 0 Then
                            .ColHidden(Colum_Ocho) = False
                            .ColHidden(Colum_Nueve) = False
                            
                            .Cell(flexcpText, 0, Colum_Cero) = "Instrumento"
                            .Cell(flexcpText, 0, Colum_Uno) = "Moneda"
                            .Cell(flexcpText, 0, Colum_Dos) = "Cantidad"
                            .Cell(flexcpText, 0, Colum_Tres) = "Precio Compra"
                            .Cell(flexcpText, 0, Colum_Cuatro) = "Monto Invertido"
                            .Cell(flexcpText, 0, Colum_Cinco) = "Precio Actual"
                            .Cell(flexcpText, 0, Colum_Seis) = "Valor de Mercado"
                            .Cell(flexcpText, 0, Colum_Siete) = "Utilidades/P�rdidas"
                            .Cell(flexcpText, 0, Colum_Ocho) = "Valorizaci�n en USD"
                            .Cell(flexcpText, 0, Colum_Nueve) = "Valorizaci�n en $"
                            nFlag = 1
                        End If
                        
                        .Cell(flexcpText, nFila, Colum_Cero) = lReg("nemotecnico").Value
                        .Cell(flexcpText, nFila, Colum_Uno) = lReg("simbolo_moneda").Value
                        .Cell(flexcpText, nFila, Colum_Dos) = FormatNumber(lReg("cantidad").Value, 0)
                        .Cell(flexcpText, nFila, Colum_Tres) = FormatNumber(lReg("precio_compra").Value, 4)
                        .Cell(flexcpText, nFila, Colum_Cuatro) = FormatNumber(lReg("monto_invertido").Value, 2)
                        .Cell(flexcpText, nFila, Colum_Cinco) = FormatNumber(lReg("precio_actual").Value, 4)
                        .Cell(flexcpText, nFila, Colum_Seis) = FormatNumber(lReg("valor_mercado").Value, 2)
                        .Cell(flexcpText, nFila, Colum_Siete) = FormatNumber(lReg("utilidad_perdida").Value, 2)
                        .Cell(flexcpText, nFila, Colum_Ocho) = FormatNumber(lReg("monto_mon_usd").Value, 2)
                        .Cell(flexcpText, nFila, Colum_Nueve) = FormatNumber(lReg("monto_mon_cta").Value, 0)
                    End Select
                nFila = nFila + 1
            Next
            nFila = nFila - 1
            .Row = 1
        End With
    Else
        With Grilla_Detalle
            .ColHidden(Colum_Ocho) = False
            .ColHidden(Colum_Nueve) = False
            For Each lReg In lCursor
                If nFlag = 0 Then
                    .Cell(flexcpText, 0, Colum_Cero) = "N�mero Contrato"
                    .Cell(flexcpText, 0, Colum_Uno) = "Tipo Operaci�n"
                    .Cell(flexcpText, 0, Colum_Dos) = "Moneda Emisi�n Contrato"
                    .Cell(flexcpText, 0, Colum_Tres) = "Modalidad"
                    .Cell(flexcpText, 0, Colum_Cuatro) = "Unidad Activo Subyacente"
                    .Cell(flexcpText, 0, Colum_Cinco) = "Cantidad Nominal Activo Subyacente"
                    .Cell(flexcpText, 0, Colum_Seis) = "Fecha Inicial"
                    .Cell(flexcpText, 0, Colum_Siete) = "Fecha Final"
                    .Cell(flexcpText, 0, Colum_Ocho) = "Precio Pactado"
                    .Cell(flexcpText, 0, Colum_Nueve) = "Resultado"
                    nFlag = 1
                End If
                .Cell(flexcpText, nFila, Colum_Cero) = lReg("numero_contrato").Value
                .Cell(flexcpText, nFila, Colum_Uno) = lReg("tipo_fwd").Value
                .Cell(flexcpText, nFila, Colum_Dos) = lReg("moneda_emision").Value
                .Cell(flexcpText, nFila, Colum_Tres) = lReg("modalidad").Value
                .Cell(flexcpText, nFila, Colum_Cuatro) = lReg("unidad_activo_subyacente").Value
                .Cell(flexcpText, nFila, Colum_Cinco) = FormatNumber(CDbl(NVL(lReg("valor_nominal").Value, 0)), lReg("decimales_activo_subyacente").Value)
                .Cell(flexcpText, nFila, Colum_Seis) = lReg("fecha_inicio").Value
                .Cell(flexcpText, nFila, Colum_Siete) = lReg("fecha_vencimiento").Value
                .Cell(flexcpText, nFila, Colum_Ocho) = lReg("precio_pactado").Value
                .Cell(flexcpText, nFila, Colum_Nueve) = FormatNumber(CDbl(lReg("valor_mercado").Value), oCliente.Decimales)
                
                nFila = nFila + 1
            Next
            nFila = nFila - 1
            .Row = 1
        End With
    
    End If
End Sub

Private Function fnt_valida_datos() As Boolean
    fnt_valida_datos = False
    If DTP_Fecha_Ini.Value >= DTP_Fecha_Ter.Value Then
        MsgBox "Fecha Inicio debe ser menor a Fecha T�rmino", vbExclamation
        Exit Function
    ElseIf DTP_Fecha_Ter.Value <= DTP_Fecha_Ini.Value Then
        MsgBox "Fecha T�rmino debe ser mayor a Fecha Inicio", vbExclamation
        Exit Function
    ElseIf DTP_Fecha_Ter.Value > xFecha_Cierre Then
        MsgBox "Fecha T�rmino debe ser menor a Fecha Cierre", vbExclamation
        Exit Function
    End If
    fnt_valida_datos = True
End Function

Private Function Fnt_Obtiene_Decimales_Salida() As Double
Dim lcMoneda As Object
Dim nDecimales As Integer
    Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
    With lcMoneda
      .Campo("id_Moneda").Valor = oCliente.IdMonedaSalida
      If .Buscar Then
        nDecimales = .Cursor(1)("dicimales_mostrar").Value
      Else
        MsgBox .ErrMsg, vbCritical, Me.Caption
        Set lcMoneda = Nothing
        
      End If
    End With
    Set lcMoneda = Nothing

    Fnt_Obtiene_Decimales_Salida = nDecimales
End Function

Private Function Fnt_Obtiene_FechaCierre_Virtual(ByRef pFecha As Date) As Boolean
Dim lcCuentas As Object
Dim lResult As Boolean

    lResult = True
    Set lcCuentas = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuentas
        .Campo("id_cuenta").Valor = lId_Cuenta
        If .Buscar Then
            If NVL(.Cursor(1)("fecha_cierre_virtual").Value, "") = "" Then
               lResult = False
            Else
                pFecha = .Cursor(1)("fecha_cierre_virtual").Value
            End If
        End If
    End With
    Fnt_Obtiene_FechaCierre_Virtual = lResult
End Function

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       
      Call cmb_buscar_Click
       
    End If
End Sub

''Private Sub cmb_buscar_Cuenta_Click()
'Dim lId_Cuenta    As String
'Dim fId_Cuenta    As String
'Dim lcCuenta      As Object
''Dim li_idtipocuenta As Integer
''JRE
'   ' fli_idtipocuenta = IIf(Fnt_ComboSelected_KEY(Cmb_TiposCuentas) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_TiposCuentas))
'   ' fid_asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesor) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Asesor))
'    'Fnt_ComboSelected_KEY(Cmb_Asesor)
'
'    fId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text), 0)
'    If fId_Cuenta <> "" Then
'        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
'        With lcCuenta
'            .Campo("Id_Cuenta").Valor = fId_Cuenta
'            If .Buscar Then
'                If .Cursor.Count > 0 Then
'                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
'                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value
'                    Txt_Descripcion_Cta.Text = .Cursor(1)("Abr_cuenta").Value
'                    Txt_Cliente.Text = .Cursor(1)("nombre_cliente").Value
'                    fId_Cuenta = .Cursor(1)("id_cuenta").Value
'                    If Not fId_Cuenta = "" Then
'                        Call Sub_MostrarCuenta(fId_Cuenta, True)
'                        'Call Sub_DatosCuenta
'                    End If
'                End If
'            Else
'                Call Fnt_MsgError(.SubTipo_LOG, _
'                          "Problemas en cargar datos de la Cuenta.", _
'                          .ErrMsg, _
'                          pConLog:=True)
'            End If
'        End With
'        Else
'            MsgBox "No existe informacion", vbInformation
'            Set lcCuenta = Nothing
'    End If
'End Sub

Private Sub Sub_MostrarCuenta(pCuenta, Optional pId As Boolean = True)
Dim lCursor As hRecord
Dim lError  As String
Dim lexiste As Boolean

    Call Sub_Entrega_DatosCuenta_2(pCuenta, pId, lCursor, lError, lexiste)
    If lexiste Then
        Txt_Num_Cuenta.Text = lCursor(1)("num_cuenta").Value
        Txt_Num_Cuenta.Tag = lCursor(1)("id_cuenta").Value
        Txt_Rut.Text = lCursor(1)("rut_cliente").Value
        txtNombreAsesor.Text = lCursor(1)("nombre_asesor").Value
        Call Sub_CargarDatos
    End If
  
End Sub

Public Sub Sub_Entrega_DatosCuenta_2(ByVal pCuenta, pId As Boolean, ByRef pCursor As hRecord, ByRef PError, ByRef pExiste)
Dim lcCuenta As Object
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
        If pId Then
            .Campo("id_cuenta").Valor = pCuenta
        Else
            .Campo("num_cuenta").Valor = pCuenta
        End If
        If .Buscar_Vigentes Then
            If .Cursor.Count = 1 Then
                Set pCursor = .Cursor
                pExiste = True
            Else
                pExiste = False
            End If
        Else
            PError = .ErrMsg
        End If
    End With
    Set lcCuenta = Nothing
End Sub

