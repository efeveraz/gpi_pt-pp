VERSION 5.00
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Traspaso_Nemotecnico 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Traspaso de Nemotécnicos"
   ClientHeight    =   4380
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6705
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4380
   ScaleWidth      =   6705
   Begin VB.Frame Frm_Origen 
      Caption         =   "Origen"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1485
      Left            =   60
      TabIndex        =   12
      Top             =   1260
      Width           =   6555
      Begin hControl2.hTextLabel Txt_Cantidad_Cartera 
         Height          =   315
         Left            =   120
         TabIndex        =   1
         Top             =   660
         Width           =   3105
         _ExtentX        =   5477
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   "Cantidad Cartera"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,###.##"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Monto_VM 
         Height          =   315
         Left            =   3330
         TabIndex        =   3
         Top             =   660
         Width           =   3105
         _ExtentX        =   5477
         _ExtentY        =   556
         LabelWidth      =   1400
         Caption         =   "Monto VM"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,###.##"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Precio_Cierre 
         Height          =   315
         Left            =   3330
         TabIndex        =   14
         Tag             =   "OBLI"
         Top             =   1020
         Width           =   3105
         _ExtentX        =   5477
         _ExtentY        =   556
         LabelWidth      =   1400
         Caption         =   "Precio Cierre"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,###.##"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Cantidad_Destino 
         Height          =   315
         Left            =   120
         TabIndex        =   2
         Tag             =   "OBLI"
         Top             =   1020
         Width           =   3105
         _ExtentX        =   5477
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   "Cantidad Destino"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin TrueDBList80.TDBCombo Cmb_Nemotecnico 
         Height          =   345
         Left            =   1650
         TabIndex        =   16
         Tag             =   "OBLI=S;CAPTION=Nemotécnico"
         Top             =   270
         Width           =   4785
         _ExtentX        =   8440
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Traspaso_Nemotecnico.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Lbl_Nemotecnico 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Nemotécnico"
         Height          =   345
         Left            =   120
         TabIndex        =   13
         Top             =   270
         Width           =   1500
      End
   End
   Begin VB.Frame Frm_Cuenta 
      Caption         =   "Cuenta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   795
      Left            =   60
      TabIndex        =   10
      Top             =   420
      Width           =   6555
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   345
         Left            =   990
         TabIndex        =   18
         Tag             =   "OBLI=S;CAPTION=Cuentas"
         Top             =   270
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Traspaso_Nemotecnico.frx":00AA
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Lbl_Cuentas 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuentas"
         Height          =   330
         Left            =   180
         TabIndex        =   11
         Top             =   270
         Width           =   795
      End
   End
   Begin VB.Frame Frm_Destino 
      Caption         =   "Destino"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1485
      Left            =   60
      TabIndex        =   0
      Top             =   2790
      Width           =   6555
      Begin hControl2.hTextLabel Txt_Factor_Conversion 
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Tag             =   "OBLI"
         Top             =   660
         Width           =   3105
         _ExtentX        =   5477
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   "Factor Conversión"
         Text            =   "1.00"
         Text            =   "1.00"
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Cantidad_Traspaso 
         Height          =   315
         Left            =   120
         TabIndex        =   6
         Tag             =   "OBLI"
         Top             =   1020
         Width           =   3105
         _ExtentX        =   5477
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   "Cantidad Traspaso"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Precio_Traspaso 
         Height          =   315
         Left            =   3300
         TabIndex        =   7
         Tag             =   "OBLI"
         Top             =   660
         Width           =   3105
         _ExtentX        =   5477
         _ExtentY        =   556
         LabelWidth      =   1400
         Caption         =   "Precio Traspaso"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Monto_Traspaso 
         Height          =   315
         Left            =   3300
         TabIndex        =   8
         Tag             =   "OBLI"
         Top             =   1020
         Width           =   3105
         _ExtentX        =   5477
         _ExtentY        =   556
         LabelWidth      =   1400
         Caption         =   "Monto Traspaso"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin TrueDBList80.TDBCombo Cmb_Nemo_Vigentes 
         Height          =   345
         Left            =   1650
         TabIndex        =   17
         Tag             =   "OBLI=S;CAPTION=Nemotécnico"
         Top             =   270
         Width           =   4785
         _ExtentX        =   8440
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Traspaso_Nemotecnico.frx":0154
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Nemotécnico"
         Height          =   345
         Left            =   120
         TabIndex        =   15
         Top             =   270
         Width           =   1500
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   6705
      _ExtentX        =   11827
      _ExtentY        =   635
      ButtonWidth     =   2170
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Traspasar"
            Key             =   "TRASPASAR"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Traspasa de cuenta nemotécnicos"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   9
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Traspaso_Nemotecnico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Enum eNem
  eNem_Nemotecnicos
  eNem_Instrumentos
  eNem_Id_Saldo_Activo
  eNem_Id_Nemotecnicos
  eNem_Cod_Productos
  eNem_Id_Monedas
  eNem_Cod_Instrumentos
End Enum

Private Enum eNem_Vgt
  eNem_nemotecnico
  eNem_Instrumento
  eNem_Id_Nemotecnico
  eNem_Id_Moneda
  eNem_Cod_Producto
  eNem_Cod_Instrumento
End Enum

Private Sub Cmb_Cuentas_ItemChange()
Dim lcSaldos_Activos As Class_Saldo_Activos
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lReg As hFields
Dim lId_Cuenta As String
Dim lid_perfil_riesgo As String
Dim lcRel_Perfiles_Nemotec As Class_Rel_Perfiles_Nemotec
  
  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_Limpiar
  
  lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
  
  If Not lId_Cuenta = "" Then
    Rem Carga los Nemotecnicos en cartera para la cuenta
    With Cmb_Nemotecnico
      .Clear
      Set lcSaldos_Activos = New Class_Saldo_Activos
      If lcSaldos_Activos.Buscar_Ultimo_Cierre(lId_Cuenta) Then
        For Each lReg In lcSaldos_Activos.Cursor
          Call .AddItem(lReg("nemotecnico").Value & ";" & _
                        lReg("dsc_intrumento").Value & ";" & _
                        lReg("id_saldo_activo").Value & ";" & _
                        lReg("id_nemotecnico").Value & ";" & _
                        lReg("Cod_Producto").Value & ";" & _
                        lReg("id_moneda_nemotecnico").Value & ";" & _
                        lReg("cod_instrumento").Value)
        Next
      End If
    End With
    
    Set lReg = Nothing
    
    Rem Busca el id_perfil_riesgo segun la cuenta
'    Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
      .Campo("Id_Cuenta").Valor = lId_Cuenta
      If .Buscar Then
        For Each lReg In .Cursor
          lid_perfil_riesgo = NVL(lReg("id_perfil_riesgo").Value, "")
        Next
      End If
      .LimpiaParam
    End With
    
    Set lReg = Nothing
    
    Rem Con el id_perfil_riesgo se busca los nemotecnicos vigentes y que esten asociados al perfil de riesgo
    If Not lid_perfil_riesgo = "" Then
      Cmb_Nemo_Vigentes.Clear
      Set lcRel_Perfiles_Nemotec = New Class_Rel_Perfiles_Nemotec
      With lcRel_Perfiles_Nemotec
        .Campo("id_Perfil_Riesgo").Valor = lid_perfil_riesgo
        If .Buscar_Nemo_Vigentes Then
          For Each lReg In .Cursor
            Call Cmb_Nemo_Vigentes.AddItem(lReg("nemotecnico").Value & ";" & _
                                           lReg("Dsc_Intrumento").Value & ";" & _
                                           lReg("id_nemotecnico").Value & ";" & _
                                           lReg("id_moneda").Value & ";" & _
                                           lReg("Cod_Producto").Value & ";" & _
                                           lReg("cod_instrumento").Value)
          Next
        Else
          MsgBox .ErrMsg, vbCritical, Me.Caption
        End If
      End With
    End If
  End If
    
  Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Sub Cmb_Nemotecnico_ItemChange()
Dim lId_Saldo_Activo As String
Dim lCod_Producto As String
Dim lId_Nemotecnico As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  Txt_Cantidad_Cartera.Text = ""
  Txt_Monto_VM.Text = ""
  Txt_Precio_Cierre.Text = ""
  
  If Not Cmb_Nemotecnico.Text = "" Then
    lId_Saldo_Activo = Cmb_Nemotecnico.Columns(eNem.eNem_Id_Saldo_Activo).Text
    lCod_Producto = Cmb_Nemotecnico.Columns(eNem.eNem_Cod_Productos).Text
    lId_Nemotecnico = Cmb_Nemotecnico.Columns(eNem.eNem_Id_Nemotecnicos).Text
    Call Sub_CargaDatos(lCod_Producto, lId_Nemotecnico, lId_Saldo_Activo)
  End If

  Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Sub Sub_CargaDatos(pCod_Producto As String, pId_Nemotecnico As String, pId_Saldo_Activo As String)
  
  Select Case pCod_Producto
    Case gcPROD_FFMM_NAC, gcPROD_RV_NAC
      Call Sub_Productos_No_RF(pId_Nemotecnico, pId_Saldo_Activo)
    Case gcPROD_RF_NAC
      Call Sub_Producto_RF(pId_Nemotecnico, pId_Saldo_Activo)
  End Select

End Sub

Private Sub Sub_Productos_No_RF(pId_Nemotecnico As String, pId_Saldo_Activo As String)
Dim lSaldos_Activos As New Class_Saldo_Activos
Dim lMov_Activos As New Class_Mov_Activos
Dim lUC_Cantidad_Saldo As Double
Dim lUC_Precio_Saldo As Double
Dim lCantidad_Pendiente As Double
Dim lReg As hFields
Dim lId_Cuenta As String
Dim lSaldo_Activo_Cantidad As Double

  '-------------------------------------------------------------------------------------------
  '----- Calculo de Cantidades y Montos Productos: Fondos Mutuos y Renta Variable ------------
  '-------------------------------------------------------------------------------------------
  lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
  lUC_Cantidad_Saldo = 0
  lUC_Precio_Saldo = 0
  With lSaldos_Activos
    If .Buscar_Ultimo_Cierre(pId_Saldo_Activo:=pId_Saldo_Activo) Then
      For Each lReg In .Cursor
        lUC_Cantidad_Saldo = lReg("cantidad").Value
        lUC_Precio_Saldo = lReg("precio").Value
        'Txt_Monto_VM.Text = lReg("monto_mon_nemotecnico").Value
      Next
    End If
  End With

  lCantidad_Pendiente = 0
  Rem Solo busca los movimientos pendientes de Ventas
  If lMov_Activos.Buscar_Pendientes_Cierre(pId_Cuenta:=lId_Cuenta _
                                         , pId_Nemotecnico:=pId_Nemotecnico _
                                         , pFecha:=Format(Fnt_FechaServidor, cFormatDate) _
                                         , pTipo_Movimiento:=gcTipoOperacion_Egreso) Then
    For Each lReg In lMov_Activos.Cursor
      lCantidad_Pendiente = lCantidad_Pendiente + lReg("cantidad").Value
    Next
  End If
  
  lSaldo_Activo_Cantidad = lUC_Cantidad_Saldo - lCantidad_Pendiente
  
  Rem Rellena los campos
  Txt_Cantidad_Cartera.Text = lSaldo_Activo_Cantidad
  Txt_Precio_Cierre.Text = lUC_Precio_Saldo
  Txt_Monto_VM.Text = lSaldo_Activo_Cantidad * lUC_Precio_Saldo
  
End Sub

Private Sub Sub_Producto_RF(pId_Nemotecnico As String, pId_Saldo_Activo As String)
Dim lcSaldos_Activos As Class_Saldo_Activos
Dim lcMov_Activos As Class_Mov_Activos
Dim lUC_Cantidad_Saldo As Double
Dim lUC_Tasa_Saldo As Double
Dim lCantidad_Pendiente As Double
Dim lReg As hFields
Dim lId_Cuenta As String
Dim lSaldo_Activo_Cantidad As Double
Dim lId_Mov_Activo As String
Dim lValorizacion As Double
Dim lcBonos As Class_Bonos

  '-------------------------------------------------------------------------------------------
  '-------------------- Calculo de Cantidades y Montos Productos Renta Fija ------------------
  '-------------------------------------------------------------------------------------------
  lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
  lUC_Cantidad_Saldo = 0
  lUC_Tasa_Saldo = 0
  
  Set lcSaldos_Activos = New Class_Saldo_Activos
  With lcSaldos_Activos
    If .Buscar_Ultimo_Cierre(pId_Nemotecnico:=pId_Nemotecnico _
                           , pId_Saldo_Activo:=pId_Saldo_Activo) Then
      For Each lReg In .Cursor
        lUC_Cantidad_Saldo = lReg("cantidad").Value
        lUC_Tasa_Saldo = lReg("tasa").Value
        lId_Mov_Activo = lReg("Id_Mov_Activo").Value
      Next
    End If
  End With

  lCantidad_Pendiente = 0
  Rem Solo busca los movimientos pendientes de Ventas
  Set lcMov_Activos = New Class_Mov_Activos
  If lcMov_Activos.Buscar_Pendientes_Cierre(pId_Cuenta:=lId_Cuenta _
                                          , pFecha:=Format(Fnt_FechaServidor, cFormatDate) _
                                          , pId_Nemotecnico:=pId_Nemotecnico _
                                          , pTipo_Movimiento:=gcTipoOperacion_Egreso _
                                          , pId_Mov_Activo_Compra:=lId_Mov_Activo) Then
    For Each lReg In lcMov_Activos.Cursor
      lCantidad_Pendiente = lCantidad_Pendiente + lReg("cantidad").Value
    Next
  End If

  lSaldo_Activo_Cantidad = lUC_Cantidad_Saldo - lCantidad_Pendiente
  
  Rem Rellena los campos
  Txt_Cantidad_Cartera.Text = lSaldo_Activo_Cantidad
  Txt_Precio_Cierre.Text = lUC_Tasa_Saldo
  
  '------------------------------------------------------------------------------------------
  '------------------------------- Valorización Bonos ---------------------------------------
  '------------------------------------------------------------------------------------------
  
  Set lcBonos = New Class_Bonos
  lValorizacion = lcBonos.ValorizaPapel(pId_Nemotecnico, _
                                        Format(Fnt_FechaServidor, cFormatDate), _
                                        lUC_Tasa_Saldo, _
                                        lSaldo_Activo_Cantidad)

  Txt_Monto_VM.Text = lValorizacion
  
End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("TRASPASAR").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
  End With

  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()

  Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas)
  
  '------------------------------------------------
  '-- Carga los nemotecnicos
  '------------------------------------------------
  Call Sub_LimpiarTDBCombo(Cmb_Nemotecnico)
  With Cmb_Nemotecnico
    With .Columns.Add(eNem.eNem_Nemotecnicos)
      .Caption = "Nemotécnico"
      .Visible = True
      
    End With
    With .Columns.Add(eNem.eNem_Instrumentos)
      .Caption = "Instrumento"
      .Visible = True
    End With
    With .Columns.Add(eNem.eNem_Id_Saldo_Activo)
      .Caption = "id_saldo_activo"
      .Visible = False
    End With
    With .Columns.Add(eNem.eNem_Id_Nemotecnicos)
      .Caption = "id_nemotecnico"
      .Visible = False
    End With
    With .Columns.Add(eNem.eNem_Cod_Productos)
      .Caption = "cod_producto"
      .Visible = False
    End With
    With .Columns.Add(eNem.eNem_Id_Monedas)
      .Caption = "id_moneda"
      .Visible = False
    End With
    With .Columns.Add(eNem.eNem_Cod_Instrumentos)
      .Caption = "cod_instrumento"
      .Visible = False
    End With
  End With
  
  '------------------------------------------------
  '-- Carga los nemotecnicos vigentes
  '------------------------------------------------
  Call Sub_LimpiarTDBCombo(Cmb_Nemo_Vigentes)
  With Cmb_Nemo_Vigentes
    With .Columns.Add(eNem_Vgt.eNem_nemotecnico)
      .Caption = "Nemotécnico"
      .Visible = True
    End With
    With .Columns.Add(eNem_Vgt.eNem_Instrumento)
      .Caption = "Instrumento"
      .Visible = True
    End With
    With .Columns.Add(eNem_Vgt.eNem_Id_Nemotecnico)
      .Caption = "id_nemotecnico"
      .Visible = False
    End With
    With .Columns.Add(eNem_Vgt.eNem_Id_Moneda)
      .Caption = "id_moneda"
      .Visible = False
    End With
    With .Columns.Add(eNem_Vgt.eNem_Cod_Producto)
      .Caption = "cod_producto"
      .Visible = False
    End With
    With .Columns.Add(eNem_Vgt.eNem_Cod_Instrumento)
      .Caption = "cod_instrumento"
      .Visible = False
    End With
  End With
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "TRASPASAR"
      If Fnt_Validaciones Then
        Toolbar.Buttons("EXIT").Enabled = False
        Call Sub_Traspasar_Nemotecnico
        Toolbar.Buttons("EXIT").Enabled = True
      End If
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Limpiar()

  Rem Limpia los controles del frame Origen
  Cmb_Nemotecnico.Text = ""
  Txt_Cantidad_Cartera.Text = ""
  Txt_Monto_VM.Text = ""
  Txt_Cantidad_Destino.Text = ""
  Txt_Precio_Cierre.Text = ""
  
  Rem Limpia los controles del frame Destino
  Cmb_Nemo_Vigentes.Text = ""
  Txt_Factor_Conversion.Text = 1
  Txt_Precio_Traspaso.Text = ""
  Txt_Cantidad_Traspaso.Text = ""
  Txt_Monto_Traspaso.Text = ""

End Sub

Private Sub Txt_Cantidad_Destino_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Cantidad_Destino_LostFocus()
  Txt_Cantidad_Traspaso.Text = Txt_Cantidad_Destino.Text * Txt_Factor_Conversion.Text
End Sub

Private Sub Txt_Factor_Conversion_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Factor_Conversion_LostFocus()
  Txt_Cantidad_Traspaso.Text = Txt_Cantidad_Destino.Text * Txt_Factor_Conversion.Text
  Txt_Monto_Traspaso.Text = Txt_Cantidad_Traspaso.Text * Txt_Precio_Traspaso.Text
End Sub

Private Sub Txt_Cantidad_Traspaso_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Cantidad_Traspaso_LostFocus()
  Txt_Factor_Conversion.Text = Fnt_Divide(Txt_Cantidad_Traspaso.Text, Txt_Cantidad_Destino.Text)
  Txt_Monto_Traspaso.Text = Txt_Cantidad_Traspaso.Text * Txt_Precio_Traspaso.Text
End Sub


Private Sub Txt_Precio_Traspaso_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Precio_Traspaso_LostFocus()
  Txt_Monto_Traspaso.Text = Txt_Cantidad_Traspaso.Text * Txt_Precio_Traspaso.Text
End Sub

Private Sub Txt_Monto_Traspaso_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Monto_Traspaso_LostFocus()
  Txt_Cantidad_Traspaso.Text = Fnt_Divide(Txt_Monto_Traspaso.Text, Txt_Precio_Traspaso.Text)
  Txt_Factor_Conversion.Text = Fnt_Divide(Txt_Cantidad_Traspaso.Text, Txt_Cantidad_Destino.Text)
End Sub

Private Function Fnt_Validaciones() As Boolean
Dim lcSaldos_Activos As Class_Saldo_Activos
Dim lId_Saldo_Activo As String
Dim lUC_Cantidad_Saldo As Double
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lId_Cuenta As String
Dim lReg As hFields
Dim lCliente_esta As Boolean
Dim lCod_Instrumento_Origen As String
Dim lCod_Instrumento_Destino As String
Dim lId_Nemotecnico_Origen As String
Dim lId_Nemotecnico_Destino As String

  Call Sub_Bloquea_Puntero(Me)
  
  Fnt_Validaciones = True
  
  Rem Valida los Controles del Formulario
'  If Not Fnt_Form_Validar(Me.Controls) Then
'    Fnt_Validaciones = False
'    GoTo Salir
'  End If
  
  Rem Valida que el Nemotecnico de Origen y de Destino sea del mismo instrumento
  lCod_Instrumento_Origen = Cmb_Nemotecnico.Columns(eNem.eNem_Cod_Instrumentos).Text
  lCod_Instrumento_Destino = Cmb_Nemo_Vigentes.Columns(eNem_Vgt.eNem_Cod_Instrumento).Text
  If Not lCod_Instrumento_Origen = lCod_Instrumento_Destino Then
    MsgBox "Los Nemotécnicos de Origen y Destino deben ser del mismo Instrumento.", vbCritical, Me.Caption
    Fnt_Validaciones = False
    GoTo Salir
  Else
    Rem Valida que el Traspaso sea entre Nemotécnicos distintos
    lId_Nemotecnico_Origen = Cmb_Nemotecnico.Columns(eNem.eNem_Id_Nemotecnicos).Text
    lId_Nemotecnico_Destino = Cmb_Nemo_Vigentes.Columns(eNem_Vgt.eNem_Id_Nemotecnico).Text
    If lId_Nemotecnico_Origen = lId_Nemotecnico_Destino Then
      MsgBox "Traspaso no permitido al mismo Nemotécnico.", vbCritical, Me.Caption
      Fnt_Validaciones = False
      GoTo Salir
    End If
  End If
  
  Rem Valida que el Traspaso sea entre Nemotécnicos distintos
  If Cmb_Nemotecnico.Text = Cmb_Nemo_Vigentes.Text Then
    MsgBox "No se puede Traspasar al mismo .", vbCritical, Me.Caption
    Fnt_Validaciones = False
    GoTo Salir
  End If
  
  Rem Valida que la cantidad de ingreso debe ser menor o igual a la cantidad en cartera
  If To_Number(Txt_Cantidad_Destino.Text) > To_Number(Txt_Cantidad_Cartera.Text) Then
    MsgBox "La Cantidad Destino no puede ser mayor que la Cantidad Cartera.", vbCritical, Me.Caption
    Fnt_Validaciones = False
    GoTo Salir
  End If
  
  Set lcSaldos_Activos = New Class_Saldo_Activos
  lId_Saldo_Activo = Cmb_Nemotecnico.Columns(eNem.eNem_Id_Saldo_Activo).Text
  
  With lcSaldos_Activos
    If .Buscar_Ultimo_Cierre(pId_Saldo_Activo:=lId_Saldo_Activo) Then
      For Each lReg In .Cursor
        lUC_Cantidad_Saldo = lReg("cantidad").Value
      Next
    End If
  End With
  
  Rem Valida en la BD que la cantidad ingresada sea menor o igual.
  If To_Number(Txt_Cantidad_Destino.Text) > lUC_Cantidad_Saldo Then
    Fnt_Validaciones = False
    GoTo Salir
  End If
  
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  lCliente_esta = False
  With lcCuenta
    lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
    .LimpiaParam
    .Campo("id_cuenta").Valor = lId_Cuenta
    If .Buscar(True) Then
      For Each lReg In .Cursor
        lCliente_esta = True
      Next
    End If
    .LimpiaParam
  End With
  
  Rem Valida que la cuenta esté desbloqueada y dehabilitada
  If Not lCliente_esta Then
    MsgBox "El Cliente está bloqueado o deshabilitado para realizar el traspaso.", vbCritical, Me.Caption
    Fnt_Validaciones = False
    GoTo Salir
  End If

Salir:
  Call Sub_Desbloquea_Puntero(Me)
  
End Function

Private Sub Sub_Traspasar_Nemotecnico()
Dim lCod_Instrumento As String
Dim lId_Cuenta As String
Dim lId_Nemotecnico As String
Dim lCantidad As Double
Dim lPrecio As Double
Dim lId_Moneda As String
Dim lMonto As Double
Dim lRollback As Boolean
Dim lCod_Producto As String

  Call Sub_Bloquea_Puntero(Me)
  
  lRollback = False
  
  lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
  
  Rem Operacion de Egreso, con los datos del Frame Origen
  lCod_Instrumento = Cmb_Nemotecnico.Columns(eNem.eNem_Cod_Instrumentos).Text
  lId_Nemotecnico = Cmb_Nemotecnico.Columns(eNem.eNem_Id_Nemotecnicos).Text
  lCantidad = Txt_Cantidad_Destino.Text
  lPrecio = Txt_Precio_Cierre.Text
  lId_Moneda = Cmb_Nemotecnico.Columns(eNem.eNem_Id_Monedas).Text
  lMonto = lCantidad * lPrecio
  lCod_Producto = Cmb_Nemotecnico.Columns(eNem.eNem_Cod_Productos).Text
  
  gDB.IniciarTransaccion
  If Fnt_Traspasar(lId_Cuenta, _
                   lCod_Instrumento, _
                   lId_Nemotecnico, _
                   lCantidad, _
                   lPrecio, _
                   lId_Moneda, _
                   lMonto, _
                   gcTipoOperacion_Egreso, _
                   lCod_Producto) Then
  
    Rem Operacion de Ingreso, con los datos del Frame Destino
    lId_Nemotecnico = Cmb_Nemo_Vigentes.Columns(eNem_Vgt.eNem_Id_Nemotecnico).Text
    lCantidad = Txt_Cantidad_Traspaso.Text
    lPrecio = Txt_Precio_Traspaso.Text
    lId_Moneda = Cmb_Nemo_Vigentes.Columns(eNem_Vgt.eNem_Id_Moneda).Text
    lMonto = Txt_Monto_Traspaso.Text
    lCod_Producto = Cmb_Nemo_Vigentes.Columns(eNem_Vgt.eNem_Cod_Producto).Text
  
    If Not Fnt_Traspasar(lId_Cuenta, _
                         lCod_Instrumento, _
                         lId_Nemotecnico, _
                         lCantidad, _
                         lPrecio, _
                         lId_Moneda, _
                         lMonto, _
                         gcTipoOperacion_Ingreso, _
                         lCod_Producto) Then
      lRollback = True
    End If
  Else
    lRollback = True
  End If
  
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
    MsgBox "El Traspaso de Nemotécnicos ha sido realizado correctamente.", vbInformation, Me.Caption
    Call Cmb_Nemotecnico_ItemChange
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function Fnt_Traspasar(pId_Cuenta As String, _
                          pCod_Instrumento As String, _
                          pId_Nemotecnico As String, _
                          pCantidad As Double, _
                          pPrecio As Double, _
                          pId_Moneda As String, _
                          pMonto As Double, _
                          pFlg_Tipo_Operacion As String, _
                          pCod_Producto As String) As Boolean
Dim lClass As Object
Dim lFechaServidor As Date
  
  Fnt_Traspasar = True
  
  lFechaServidor = Fnt_FechaServidor
  
  Select Case pCod_Instrumento
    Case gcINST_ACCIONES_NAC
      Set lClass = New Class_Acciones
      Call lClass.Agregar_Operaciones_Detalle("", _
                                              pId_Nemotecnico, _
                                              pCantidad, _
                                              pPrecio, _
                                              pId_Moneda, _
                                              pMonto, _
                                              cFlg_No_Vende_Todo)
      
    Case gcINST_FFMM_RF_NAC, gcINST_FFMM_RV_NAC
      Set lClass = New Class_FondosMutuos
      Call lClass.Agregar_Operaciones_Detalle("", _
                                              pId_Nemotecnico, _
                                              pCantidad, _
                                              pPrecio, _
                                              pId_Moneda, _
                                              pMonto, _
                                              cFlg_No_Vende_Todo)
      
    Case gcINST_BONOS_NAC
      Set lClass = New Class_Bonos
      Call lClass.Agregar_Operaciones_Detalle("", _
                                              pId_Nemotecnico, _
                                              pCantidad, _
                                              pPrecio, _
                                              "", _
                                              pId_Moneda, _
                                              pMonto, _
                                              "", _
                                              cFlg_No_Vende_Todo)
      
    Case gcINST_DEPOSITOS_NAC
      Set lClass = New Class_Depositos
      Call lClass.Agregar_Operaciones_Detalle(pId_Nemotecnico, _
                                              pCantidad, _
                                              pPrecio, _
                                              "", _
                                              "", _
                                              "", _
                                              lFechaServidor, _
                                              pId_Moneda, _
                                              pMonto, _
                                              "", _
                                              "", _
                                              lFechaServidor, _
                                              cFlg_No_Vende_Todo)
      
    Case gcINST_PACTOS_NAC
      Set lClass = New Class_Pactos
      Call lClass.Agregar_Operaciones_Detalle(pId_Nemotecnico, _
                                              pCantidad, _
                                              pPrecio, _
                                              "", _
                                              "", _
                                              "", _
                                              lFechaServidor, _
                                              pId_Moneda, _
                                              pMonto, _
                                              "", _
                                              "", _
                                              lFechaServidor, _
                                              cFlg_No_Vende_Todo)
      
  End Select
  
  Rem Luego se llama a la operacion Mov_Cuenta, donde se realiza la transacción.
  If Not lClass.Realiza_Operacion_Mov_Cuenta(pId_Cuenta, _
                                             pId_Moneda, _
                                             pCod_Producto, _
                                             pCod_Instrumento, _
                                             pFlg_Tipo_Operacion, _
                                             lFechaServidor, _
                                             "", _
                                             0, _
                                             pMonto, _
                                             "") Then
    MsgBox lClass.ErrMsg, vbCritical, Me.Caption
    GoTo ErrProcedure
  End If
  
  Exit Function
  
ErrProcedure:
  Fnt_Traspasar = False
  
End Function
