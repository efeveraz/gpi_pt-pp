VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Frm_Man_Glosa_Descripcion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Glosas "
   ClientHeight    =   5055
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8130
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   8130
   Begin VB.Frame Frame3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1215
      Left            =   60
      TabIndex        =   6
      Top             =   390
      Width           =   7995
      Begin hControl2.hTextLabel Txt_Cod_Glosa_Descripcion_Tipo 
         Height          =   315
         Left            =   180
         TabIndex        =   7
         Top             =   300
         Width           =   7125
         _ExtentX        =   12568
         _ExtentY        =   556
         LabelWidth      =   1275
         Enabled         =   0   'False
         Caption         =   "C�digo"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin hControl2.hTextLabel Txt_Dsc_Glosa_Descripcion_Tipo 
         Height          =   315
         Left            =   180
         TabIndex        =   8
         Top             =   720
         Width           =   7125
         _ExtentX        =   12568
         _ExtentY        =   556
         LabelWidth      =   1275
         Enabled         =   0   'False
         Caption         =   "Descripci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Glosa Descripci�n de Operaciones"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3285
      Left            =   60
      TabIndex        =   0
      Top             =   1650
      Width           =   7995
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   2835
         Left            =   120
         TabIndex        =   1
         Top             =   300
         Width           =   7155
         _cx             =   12621
         _cy             =   5001
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   2
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Glosa_Descripcion.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Grilla 
         Height          =   990
         Left            =   7410
         TabIndex        =   4
         Top             =   570
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1746
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "ADD"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Agregar una Cuenta al Sorteo de Letra"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "UPD"
               Object.ToolTipText     =   "Modifica una Cuenta al Sorteo de Letra"
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "DEL"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Elimina una Cuenta al Sorteo de Letra"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   255
            Left            =   9420
            TabIndex        =   5
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   8130
      _ExtentX        =   14340
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Grabar"
            Key             =   "SAVE"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Graba Glosa Descripci�n Tipo"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Glosa_Descripcion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Dim lhBorrar As hRecord
Dim fFlg_Tipo_Permiso As String

Public Function Fnt_Modificar(pId_Glosa_Descripcion_Tipo, pCod_Arbol_Sistema)
  fKey = pId_Glosa_Descripcion_Tipo
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso Tipo Glosa"
  Else
    Me.Caption = "Modificaci�n Tipo Glosa"
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
Dim lKey As String

  With Grilla
    If .Row > 0 Then
      lKey = GetCell(Grilla, .Row, "colum_pk")
      Call Sub_EsperaVentana(lKey)
    End If
  End With
  
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        MsgBox "Tipo de Glosa guardada correctamente.", vbInformation, Me.Caption
        Unload Me
      End If
    Case "REFRESH"
      Call Sub_CargarDatos
      Set lhBorrar = Nothing
      
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Grilla
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("UPD").Image = cBoton_Modificar
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Appearance = ccFlat
  End With
  
  Call Sub_CargaForm
  Call Sub_CargarDatos
  Grilla.ColHidden(0) = True
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
  Rem Limpia la grilla
  Grilla.Rows = 1
  
  Set lhBorrar = New hRecord
  With lhBorrar
    .ClearFields
    .AddField "id_glosa_despcripcion"
  End With
  
End Sub

Private Sub Sub_CargarDatos()
Dim lcGlosa_Tipo As Class_Glosa_Descripcion_Tipo
Dim lReg    As hCollection.hFields
Dim lLinea  As Long

  Call Sub_Bloquea_Puntero(Me)

  Set lcGlosa_Tipo = New Class_Glosa_Descripcion_Tipo
  With lcGlosa_Tipo
    .Campo("ID_GLOSA_DESCRIPCION_TIPO").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        Txt_Cod_Glosa_Descripcion_Tipo.Text = lReg("Cod_Glosa_Descripcion_Tipo").Value
        Txt_Dsc_Glosa_Descripcion_Tipo.Text = lReg("Dsc_Glosa_Descripcion_Tipo").Value
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcGlosa_Tipo = Nothing
  
  Call Sub_CargaDatos_Glosa
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargaDatos_Glosa()
Dim lcGlosa As Class_Glosa_Descripcion
Dim lReg    As hCollection.hFields
Dim lLinea  As Long

  Grilla.Rows = 1
  
  Set lcGlosa = New Class_Glosa_Descripcion
  With lcGlosa
    .Campo("ID_GLOSA_DESCRIPCION_TIPO").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla.Rows
        Grilla.Rows = lLinea + 1
        SetCell Grilla, lLinea, "colum_pk", lReg("id_Glosa_Descripcion").Value
        SetCell Grilla, lLinea, "Dsc_Glosa_Descripcion", lReg("Dsc_Glosa_Descripcion").Value
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcGlosa = Nothing
  
End Sub

Private Sub Toolbar_Grilla_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lMensaje As String
Dim lReg As hFields

  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "ADD"
      If fKey = cNewEntidad Then
        lMensaje = "Para Agregar una nueva Glosa primero hay que grabar el Tipo de Glosa." & vbCr & "�Desea grabar el Tipo de Glosa?"
        If MsgBox(lMensaje, vbYesNo + vbInformation + vbDefaultButton2) = vbYes Then
          If Fnt_Grabar Then
            Call Sub_EsperaVentana(cNewEntidad)
          End If
        End If
      Else
        Call Sub_EsperaVentana(cNewEntidad)
      End If
      
    Case "UPD"
      Call Grilla_DblClick
      
    Case "DEL"
      If Grilla.Row > 0 Then
        lMensaje = "�Est� seguro que desea eliminar la descripcion de la Glosa?"
        If MsgBox(lMensaje, vbYesNo + vbInformation + vbDefaultButton2) = vbYes Then
          Set lReg = lhBorrar.Add
          lReg("id_glosa_descripcion").Value = GetCell(Grilla, Grilla.Row, "colum_pk")
          
          Grilla.RemoveItem (Grilla.Row)
        End If
      End If
      
  End Select
  
End Sub

Private Sub Sub_EsperaVentana(ByVal pKey)
Dim lForm As Frm_Glosa_Descripcion_Tipo
Dim lNombre As String
Dim lNom_Form As String
Dim lDsc_Glosa_Descripcion As String
  
  Me.Enabled = False
  
  lNom_Form = "Frm_Glosa_Descripcion_Tipo"

  If Not Fnt_ExisteVentanaKey(lNom_Form, pKey) Then
    lNombre = Me.Name

    Set lForm = New Frm_Glosa_Descripcion_Tipo
    Call lForm.Fnt_Modificar(pKey, fKey, lDsc_Glosa_Descripcion)

    Do While Fnt_ExisteVentanaKey(lNom_Form, pKey)
      DoEvents
    Loop

    If Fnt_ExisteVentana(lNombre) Then
      Call Sub_CargaDatos_Glosa
    End If
  End If
  
  Me.Enabled = True
  
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcGlosa_Tipo As Class_Glosa_Descripcion_Tipo
Dim lcGlosa As Class_Glosa_Descripcion
Dim lReg As hFields

  Call Sub_Bloquea_Puntero(Me)
    
  Fnt_Grabar = True
  
  If Txt_Cod_Glosa_Descripcion_Tipo.Text = "" Then
    MsgBox "El C�digo del Tipo de Glosa no puede ser vac�a.", vbExclamation, Me.Caption
    GoTo ErrProcedure
  ElseIf Txt_Dsc_Glosa_Descripcion_Tipo.Text = "" Then
    MsgBox "La Descripci�n del Tipo de Glosa no puede ser vac�a.", vbExclamation, Me.Caption
    GoTo ErrProcedure
  End If
  
  Set lcGlosa = New Class_Glosa_Descripcion
  With lcGlosa
    For Each lReg In lhBorrar
      .Campo("id_glosa_descripcion").Valor = lReg("id_glosa_descripcion").Value
      If Not .Borrar Then
        MsgBox .ErrMsg, vbCritical, Me.Caption
        GoTo ErrProcedure
      End If
    Next
  End With
  
  Set lcGlosa_Tipo = New Class_Glosa_Descripcion_Tipo
  With lcGlosa_Tipo
    .Campo("id_glosa_descripcion_tipo").Valor = fKey
    .Campo("cod_glosa_descripcion_tipo").Valor = Txt_Cod_Glosa_Descripcion_Tipo.Text
    .Campo("dsc_glosa_descripcion_tipo").Valor = Txt_Dsc_Glosa_Descripcion_Tipo.Text
    If Not .Guardar Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
    fKey = .Campo("ID_GLOSA_DESCRIPCION_TIPO").Valor
  End With
    
ExitProcedure:
  Set lcGlosa_Tipo = Nothing
  Set lcGlosa = Nothing
  Call Sub_Desbloquea_Puntero(Me)
  Exit Function
  
ErrProcedure:
  Fnt_Grabar = False
  GoTo ExitProcedure

End Function

Private Sub Txt_Cod_Glosa_Descripcion_Tipo_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub
