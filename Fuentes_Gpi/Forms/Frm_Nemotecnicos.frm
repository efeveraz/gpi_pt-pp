VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Nemotecnicos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Nemot�cnico"
   ClientHeight    =   9360
   ClientLeft      =   960
   ClientTop       =   1050
   ClientWidth     =   9105
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9360
   ScaleWidth      =   9105
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9105
      _ExtentX        =   16060
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   31
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin C1SizerLibCtl.C1Tab SSTab 
      Height          =   8955
      Left            =   60
      TabIndex        =   32
      Top             =   370
      Width           =   8955
      _cx             =   15796
      _cy             =   15796
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   6579300
      ForeColor       =   -2147483634
      FrontTabColor   =   -2147483635
      BackTabColor    =   6579300
      TabOutlineColor =   -2147483632
      FrontTabForeColor=   -2147483634
      Caption         =   "&Instrumentos|&Clasificaciones de Riesgo|Clase|Alias|Serie y Tabla Desarrollo"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   6
      Position        =   0
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   0   'False
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   -1  'True
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   0
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   -1  'True
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame FrameSerie 
         Height          =   8610
         Left            =   10470
         TabIndex        =   69
         Top             =   330
         Width           =   8925
         Begin VB.Frame Fra_Cupones 
            Height          =   5595
            Left            =   90
            TabIndex        =   81
            Top             =   1980
            Width           =   8475
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Cupones 
               Height          =   5190
               Left            =   120
               TabIndex        =   82
               Top             =   270
               Width           =   7800
               _cx             =   13758
               _cy             =   9155
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   7
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Nemotecnicos.frx":0000
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   1
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   2
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   10
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
            Begin MSComctlLib.Toolbar tlbGrillaCupones 
               Height          =   990
               Left            =   7965
               TabIndex        =   85
               Top             =   270
               Width           =   450
               _ExtentX        =   794
               _ExtentY        =   1746
               ButtonWidth     =   1138
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   3
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "ADD"
                     Description     =   "Agregar Cup�n"
                     Object.ToolTipText     =   "Agregar Cup�n"
                  EndProperty
                  BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "DEL"
                     Description     =   "Quitar Cupon"
                     Object.ToolTipText     =   "Quitar Cup�n"
                  EndProperty
                  BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PASTE"
                     Description     =   "Pegar Tabla"
                     Object.ToolTipText     =   "Pegar Tabla"
                  EndProperty
               EndProperty
               Begin MSComctlLib.ProgressBar ProgressBar2 
                  Height          =   255
                  Left            =   9420
                  TabIndex        =   86
                  Top             =   30
                  Width           =   780
                  _ExtentX        =   1376
                  _ExtentY        =   450
                  _Version        =   393216
                  Appearance      =   1
                  Scrolling       =   1
               End
            End
         End
         Begin hControl2.hTextLabel TxtTasaEfectiva 
            Height          =   315
            Left            =   360
            TabIndex        =   70
            Tag             =   "OBLI=S;CAPTION=Tasa Efectiva"
            Top             =   150
            Width           =   3360
            _ExtentX        =   5927
            _ExtentY        =   556
            LabelWidth      =   1600
            TextMinWidth    =   1200
            Caption         =   "Tasa Efectiva"
            Text            =   "0,000000"
            Text            =   "0,000000"
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.000000"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin MSComCtl2.DTPicker dtpPrimerVmto 
            Height          =   345
            Left            =   1950
            TabIndex        =   71
            Tag             =   "OBLI=S;CAPTION=Primer Vencimiento"
            Top             =   495
            Width           =   1800
            _ExtentX        =   3175
            _ExtentY        =   609
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   67305473
            CurrentDate     =   37732
         End
         Begin TrueDBList80.TDBCombo CmbDecimalesTabla 
            Height          =   345
            Left            =   1950
            TabIndex        =   73
            Tag             =   "OBLI=S;CAPTION=Decimales Tabla"
            Top             =   1230
            Width           =   1770
            _ExtentX        =   3122
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   2
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Nemotecnicos.frx":014A
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin hControl2.hTextLabel txtDiaCorteCupon 
            Height          =   315
            Left            =   360
            TabIndex        =   72
            Tag             =   "OBLI=S;CAPTION=D�a Corte Cup�n"
            Top             =   885
            Width           =   3360
            _ExtentX        =   5927
            _ExtentY        =   556
            LabelWidth      =   1600
            TextMinWidth    =   1200
            Caption         =   "Dia corte Cup�n"
            Text            =   "0"
            Text            =   "0"
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0"
            Tipo_TextBox    =   1
         End
         Begin hControl2.hTextLabel txtSerieBanco 
            Height          =   315
            Left            =   4725
            TabIndex        =   78
            Tag             =   "OBLI=S;CAPTION=Serie Banco"
            Top             =   1260
            Width           =   3360
            _ExtentX        =   5927
            _ExtentY        =   556
            LabelWidth      =   1600
            TextMinWidth    =   1200
            Caption         =   "Serie Banco"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
         End
         Begin hControl2.hTextLabel txtPlazoAnos 
            Height          =   315
            Left            =   4725
            TabIndex        =   77
            Tag             =   "OBLI=S;CAPTION=Plazo A�os"
            Top             =   900
            Width           =   3375
            _ExtentX        =   5953
            _ExtentY        =   556
            LabelWidth      =   1600
            TextMinWidth    =   1200
            Caption         =   "Plazo A�os"
            Text            =   "0"
            Text            =   "0"
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0"
            Tipo_TextBox    =   1
         End
         Begin hControl2.hTextLabel txtNroAmortizaciones 
            Height          =   315
            Left            =   4725
            TabIndex        =   76
            Tag             =   "OBLI=S;CAPTION=Nro Amortizaciones"
            Top             =   540
            Width           =   3375
            _ExtentX        =   5953
            _ExtentY        =   556
            LabelWidth      =   1600
            TextMinWidth    =   1200
            Caption         =   "Nro. Amortizaciones"
            Text            =   "0"
            Text            =   "0"
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0"
            Tipo_TextBox    =   1
         End
         Begin hControl2.hTextLabel txtNroCupones 
            Height          =   315
            Left            =   4725
            TabIndex        =   75
            Tag             =   "OBLI=S;CAPTION=Nro Cupones"
            Top             =   180
            Width           =   3375
            _ExtentX        =   5953
            _ExtentY        =   556
            LabelWidth      =   1600
            TextMinWidth    =   1200
            Caption         =   "Nro. Cupones"
            Text            =   "0"
            Text            =   "0"
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0"
            Tipo_TextBox    =   1
         End
         Begin TrueDBList80.TDBCombo cmbFamilia 
            Height          =   345
            Left            =   6315
            TabIndex        =   83
            Tag             =   "OBLI=S;CAPTION=Familia Serie"
            Top             =   1620
            Width           =   1770
            _ExtentX        =   3122
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   2
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Nemotecnicos.frx":01F4
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo cmbMonedaSerie 
            Height          =   345
            Left            =   1950
            TabIndex        =   74
            Tag             =   "OBLI=S;CAPTION=Moneda Serie"
            Top             =   1620
            Width           =   1770
            _ExtentX        =   3122
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   2
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Nemotecnicos.frx":029E
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Label18 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Moneda"
            Height          =   345
            Left            =   360
            TabIndex        =   87
            Top             =   1620
            Width           =   1575
         End
         Begin VB.Label Label17 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Familia"
            Height          =   345
            Left            =   4725
            TabIndex        =   84
            Top             =   1620
            Width           =   1575
         End
         Begin VB.Label Label21 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Decimales Tabla"
            Height          =   345
            Left            =   360
            TabIndex        =   80
            Top             =   1230
            Width           =   1575
         End
         Begin VB.Label Label2 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Primer Vencimiento"
            Height          =   345
            Left            =   360
            TabIndex        =   79
            Top             =   495
            Width           =   1635
         End
      End
      Begin VB.PictureBox Picture3 
         BorderStyle     =   0  'None
         Height          =   8610
         Left            =   10170
         ScaleHeight     =   8610
         ScaleWidth      =   8925
         TabIndex        =   67
         Top             =   330
         Width           =   8925
         Begin VB.Frame Fra_Alias 
            Height          =   4005
            Left            =   90
            TabIndex        =   68
            Top             =   -30
            Width           =   8535
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Alias 
               Height          =   3675
               Left            =   90
               TabIndex        =   30
               Top             =   180
               Width           =   8325
               _cx             =   14684
               _cy             =   6482
               Appearance      =   3
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   3
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Nemotecnicos.frx":0348
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   2
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
      End
      Begin VB.PictureBox Picture4 
         BorderStyle     =   0  'None
         Height          =   8610
         Left            =   9870
         ScaleHeight     =   8610
         ScaleWidth      =   8925
         TabIndex        =   65
         Top             =   330
         Width           =   8925
         Begin VB.Frame fra_Arbol 
            Height          =   6645
            Left            =   90
            TabIndex        =   66
            Top             =   480
            Width           =   8535
            Begin MSComctlLib.TreeView Arbol 
               Height          =   6315
               Left            =   90
               TabIndex        =   29
               Top             =   210
               Width           =   8325
               _ExtentX        =   14684
               _ExtentY        =   11139
               _Version        =   393217
               HideSelection   =   0   'False
               Indentation     =   18
               Style           =   7
               Checkboxes      =   -1  'True
               BorderStyle     =   1
               Appearance      =   1
            End
         End
         Begin TrueDBList80.TDBCombo Cmb_TipoArbol 
            Height          =   375
            Left            =   2520
            TabIndex        =   96
            Tag             =   "OBLI=S;CAPTION=Cuenta"
            Top             =   120
            Width           =   3255
            _ExtentX        =   5741
            _ExtentY        =   661
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   661
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Nemotecnicos.frx":03CF
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Lbl_TipoArbol 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Tipo de Clase de Instrumento"
            Height          =   375
            Left            =   120
            TabIndex        =   97
            Top             =   120
            Width           =   2295
         End
      End
      Begin VB.PictureBox Picture2 
         BorderStyle     =   0  'None
         Height          =   8610
         Left            =   9570
         ScaleHeight     =   8610
         ScaleWidth      =   8925
         TabIndex        =   61
         Top             =   330
         Width           =   8925
         Begin VB.Frame Fra_Riesgo 
            Height          =   4005
            Left            =   90
            TabIndex        =   62
            Top             =   -30
            Width           =   8535
            Begin VSFlex8LCtl.VSFlexGrid Grilla 
               Height          =   3675
               Left            =   90
               TabIndex        =   28
               Top             =   180
               Width           =   7785
               _cx             =   13732
               _cy             =   6482
               Appearance      =   3
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   3
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Nemotecnicos.frx":0479
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
            Begin MSComctlLib.Toolbar Toolbar_Grilla 
               Height          =   660
               Left            =   7980
               TabIndex        =   63
               Top             =   450
               Width           =   450
               _ExtentX        =   794
               _ExtentY        =   1164
               ButtonWidth     =   1138
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   2
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "ADD"
                     Description     =   "Agregar un nemotecnico a la Operaci�n"
                     Object.ToolTipText     =   "Agregar un Clasificador de Riesgo al Nemot�cnico"
                  EndProperty
                  BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "DEL"
                     Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                     Object.ToolTipText     =   "Elimina un Clasificador de Riesgo al Nemot�cnico"
                  EndProperty
               EndProperty
               Begin MSComctlLib.ProgressBar ProgressBar1 
                  Height          =   255
                  Left            =   9420
                  TabIndex        =   64
                  Top             =   30
                  Width           =   780
                  _ExtentX        =   1376
                  _ExtentY        =   450
                  _Version        =   393216
                  Appearance      =   1
                  Scrolling       =   1
               End
            End
         End
      End
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   8610
         Left            =   15
         ScaleHeight     =   8610
         ScaleWidth      =   8925
         TabIndex        =   33
         Top             =   330
         Width           =   8925
         Begin VB.Frame Frame_Principal 
            Height          =   5385
            Left            =   90
            TabIndex        =   44
            Top             =   -30
            Width           =   9255
            Begin VB.CheckBox Chk_CuentaPaso 
               Caption         =   "S�"
               Height          =   195
               Left            =   6435
               TabIndex        =   89
               Top             =   3465
               Visible         =   0   'False
               Width           =   1095
            End
            Begin VB.Frame Frame1 
               Caption         =   "Atributos Emisor"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   1155
               Left            =   150
               TabIndex        =   48
               Top             =   4125
               Width           =   4995
               Begin hControl2.hTextLabel Txt_Sector_Original 
                  Height          =   315
                  Left            =   180
                  TabIndex        =   49
                  Top             =   660
                  Width           =   4695
                  _ExtentX        =   8281
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Sector"
                  Text            =   ""
                  Locked          =   -1  'True
                  BackColorTxt    =   12648447
                  BackColorTxt    =   12648447
               End
               Begin hControl2.hTextLabel Txt_EmisorGeneral_Original 
                  Height          =   315
                  Left            =   180
                  TabIndex        =   50
                  Top             =   300
                  Width           =   4695
                  _ExtentX        =   8281
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Emisor General"
                  Text            =   ""
                  Locked          =   -1  'True
                  BackColorTxt    =   12648447
                  BackColorTxt    =   12648447
               End
            End
            Begin VB.Frame Frame2 
               Caption         =   "Atributos Emisor"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   1155
               Left            =   150
               TabIndex        =   45
               Top             =   2475
               Width           =   4995
               Begin hControl2.hTextLabel Txt_Sector_Deudor 
                  Height          =   315
                  Left            =   180
                  TabIndex        =   46
                  Top             =   660
                  Width           =   4695
                  _ExtentX        =   8281
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Sector"
                  Text            =   ""
                  Locked          =   -1  'True
                  BackColorTxt    =   12648447
                  BackColorTxt    =   12648447
               End
               Begin hControl2.hTextLabel Txt_EmisorGeneral_Deudor 
                  Height          =   315
                  Left            =   180
                  TabIndex        =   47
                  Top             =   300
                  Width           =   4695
                  _ExtentX        =   8281
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Emisor General"
                  Text            =   ""
                  Locked          =   -1  'True
                  BackColorTxt    =   12648447
                  BackColorTxt    =   12648447
               End
            End
            Begin hControl2.hTextLabel Txt_Nemotecnico 
               Height          =   315
               Left            =   150
               TabIndex        =   4
               Tag             =   "OBLI"
               Top             =   1365
               Width           =   3120
               _ExtentX        =   5503
               _ExtentY        =   556
               LabelWidth      =   1425
               TextMinWidth    =   1200
               Caption         =   "Nemot�cnico"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
            End
            Begin hControl2.hTextLabel Txt_Descripcion 
               Height          =   315
               Left            =   3450
               TabIndex        =   5
               Tag             =   "OBLI"
               Top             =   1365
               Width           =   5235
               _ExtentX        =   9234
               _ExtentY        =   556
               LabelWidth      =   1425
               Caption         =   "Descripci�n"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               MaxLength       =   119
            End
            Begin TrueDBList80.TDBCombo Cmb_Estado 
               Height          =   330
               Left            =   6720
               TabIndex        =   11
               Tag             =   "OBLI=S;CAPTION=Estado"
               Top             =   2415
               Width           =   1965
               _ExtentX        =   3466
               _ExtentY        =   582
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   582
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":0545
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_Productos 
               Height          =   345
               Left            =   1590
               TabIndex        =   1
               Tag             =   "OBLI=S;CAPTION=Producto"
               Top             =   210
               Width           =   4185
               _ExtentX        =   7382
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":05EF
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_Instrumento 
               Height          =   345
               Left            =   1590
               TabIndex        =   2
               Tag             =   "OBLI=S;CAPTION=Instrumento"
               Top             =   600
               Width           =   3195
               _ExtentX        =   5636
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":0699
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_PaisOrigen 
               Height          =   345
               Left            =   1590
               TabIndex        =   6
               Tag             =   "OBLI=S;CAPTION=Pa�s Origen"
               Top             =   1725
               Width           =   2445
               _ExtentX        =   4313
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":0743
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_EmisorDeudor 
               Height          =   345
               Left            =   1590
               TabIndex        =   7
               Tag             =   "OBLI=S;CAPTION=Emisor Deudor"
               Top             =   2115
               Width           =   3465
               _ExtentX        =   6112
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":07ED
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_EmisorOriginal 
               Height          =   345
               Left            =   1590
               TabIndex        =   8
               Tag             =   "OBLI=S;CAPTION=Emisor Original"
               Top             =   3750
               Width           =   3465
               _ExtentX        =   6112
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":0897
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_Subfamilia 
               Height          =   345
               Left            =   1590
               TabIndex        =   3
               Tag             =   "OBLI=S;CAPTION=SubFamilia"
               Top             =   990
               Width           =   7095
               _ExtentX        =   12515
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":0941
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_MonedaOrigen 
               Height          =   330
               Left            =   6720
               TabIndex        =   9
               Tag             =   "OBLI=S;CAPTION=Moneda Origen"
               Top             =   1725
               Width           =   1965
               _ExtentX        =   3466
               _ExtentY        =   582
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   582
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":09EB
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_MonedaTransaccion 
               Height          =   330
               Left            =   6720
               TabIndex        =   10
               Tag             =   "OBLI=S;CAPTION=Moneda Transacci�n"
               Top             =   2070
               Width           =   1965
               _ExtentX        =   3466
               _ExtentY        =   582
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   582
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":0A95
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_MercadoTransaccion 
               Height          =   330
               Left            =   6720
               TabIndex        =   12
               Tag             =   "OBLI=S;CAPTION=Merc. Transacci�n"
               Top             =   3060
               Width           =   1965
               _ExtentX        =   3466
               _ExtentY        =   582
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   582
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":0B3F
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin hControl2.hTextLabel Txt_NumAccionesEmisor 
               Height          =   285
               Left            =   5280
               TabIndex        =   88
               Top             =   2760
               Width           =   3420
               _ExtentX        =   6033
               _ExtentY        =   503
               LabelWidth      =   1420
               TextMinWidth    =   1200
               Caption         =   "N� Acc. Emisor"
               Text            =   ""
               Tipo_TextBox    =   1
               Alignment       =   1
               MaxLength       =   20
            End
            Begin hControl2.hTextLabel Txt_Isim 
               Height          =   285
               Left            =   5280
               TabIndex        =   90
               Top             =   3735
               Width           =   3435
               _ExtentX        =   6059
               _ExtentY        =   503
               LabelWidth      =   1100
               TextMinWidth    =   1200
               Caption         =   "ISIN"
               Text            =   ""
               MaxLength       =   20
            End
            Begin hControl2.hTextLabel Txt_Cusip 
               Height          =   285
               Left            =   5280
               TabIndex        =   91
               Top             =   4050
               Width           =   3435
               _ExtentX        =   6059
               _ExtentY        =   503
               LabelWidth      =   1100
               TextMinWidth    =   1200
               Caption         =   "CUSIP"
               Text            =   ""
               MaxLength       =   20
            End
            Begin hControl2.hTextLabel Txt_Sedol 
               Height          =   285
               Left            =   5280
               TabIndex        =   92
               Top             =   4365
               Width           =   3435
               _ExtentX        =   6059
               _ExtentY        =   503
               LabelWidth      =   1100
               TextMinWidth    =   1200
               Caption         =   "SEDOL/BB N�"
               Text            =   ""
               MaxLength       =   20
            End
            Begin hControl2.hTextLabel Txt_Ticker 
               Height          =   285
               Left            =   5280
               TabIndex        =   93
               Top             =   4680
               Width           =   3435
               _ExtentX        =   6059
               _ExtentY        =   503
               LabelWidth      =   1100
               TextMinWidth    =   1200
               Caption         =   "TICKER"
               Text            =   ""
               MaxLength       =   50
            End
            Begin hControl2.hTextLabel Txt_CodAchs 
               Height          =   285
               Left            =   5280
               TabIndex        =   94
               Top             =   4995
               Width           =   2955
               _ExtentX        =   5212
               _ExtentY        =   503
               LabelWidth      =   1100
               TextMinWidth    =   1200
               Caption         =   "C�digo ACHS"
               Text            =   ""
               MaxLength       =   12
            End
            Begin MSComctlLib.Toolbar Toolbar_CAchs 
               Height          =   330
               Left            =   8265
               TabIndex        =   98
               Top             =   4980
               Width           =   450
               _ExtentX        =   794
               _ExtentY        =   582
               ButtonWidth     =   1138
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   1
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "ADD"
                     Description     =   "Agregar un nemotecnico a la Operaci�n"
                     Object.ToolTipText     =   "Generar C�digo ACHS"
                  EndProperty
               EndProperty
               Begin MSComctlLib.ProgressBar ProgressBar3 
                  Height          =   255
                  Left            =   9420
                  TabIndex        =   99
                  Top             =   30
                  Width           =   780
                  _ExtentX        =   1376
                  _ExtentY        =   450
                  _Version        =   393216
                  Appearance      =   1
                  Scrolling       =   1
               End
            End
            Begin TrueDBList80.TDBCombo Cmb_InstrumentoSVS 
               Height          =   345
               Left            =   6570
               TabIndex        =   101
               Top             =   600
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":0BE9
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin VB.Label Label20 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Instrumento SVS"
               Height          =   345
               Left            =   5010
               TabIndex        =   100
               Top             =   600
               Width           =   1545
            End
            Begin VB.Label Label19 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Cuenta Paso"
               Height          =   345
               Left            =   5280
               TabIndex        =   95
               Top             =   3405
               Visible         =   0   'False
               Width           =   1095
            End
            Begin VB.Label Label14 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Moneda Transacci�n"
               Height          =   330
               Left            =   5100
               TabIndex        =   60
               Top             =   2055
               Width           =   1605
            End
            Begin VB.Label Label13 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Emisor Original"
               Height          =   345
               Left            =   150
               TabIndex        =   59
               Top             =   3750
               Width           =   1425
            End
            Begin VB.Label Label3 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Estado"
               Height          =   315
               Left            =   5280
               TabIndex        =   58
               Top             =   2400
               Width           =   1425
            End
            Begin VB.Label Label1 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Pa�s Origen"
               Height          =   345
               Left            =   150
               TabIndex        =   57
               Top             =   1725
               Width           =   1425
            End
            Begin VB.Label Label4 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Moneda Origen"
               Height          =   315
               Left            =   5100
               TabIndex        =   56
               Top             =   1725
               Width           =   1605
            End
            Begin VB.Label Label5 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Emisor Deudor"
               Height          =   345
               Left            =   150
               TabIndex        =   55
               Top             =   2115
               Width           =   1425
            End
            Begin VB.Label Label6 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Merc. Transacci�n"
               Height          =   315
               Left            =   5280
               TabIndex        =   54
               Top             =   3060
               Width           =   1425
            End
            Begin VB.Label Label7 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Producto"
               Height          =   345
               Left            =   150
               TabIndex        =   53
               Top             =   210
               Width           =   1425
            End
            Begin VB.Label Label8 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Instrumento"
               Height          =   345
               Left            =   150
               TabIndex        =   52
               Top             =   600
               Width           =   1425
            End
            Begin VB.Label lblSubfamilias 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Subfamilia"
               Height          =   345
               Left            =   150
               TabIndex        =   51
               Top             =   990
               Width           =   1425
            End
         End
         Begin VB.Frame Frame_RF 
            Caption         =   "Atributos Renta Fija"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   2325
            Left            =   90
            TabIndex        =   38
            Top             =   5340
            Width           =   8775
            Begin VB.CheckBox Chk_Fungible 
               Caption         =   "Fungible"
               Height          =   225
               Left            =   7260
               TabIndex        =   22
               Top             =   1545
               Width           =   1095
            End
            Begin hControl2.hTextLabel Txt_MontoEmision 
               Height          =   315
               Left            =   150
               TabIndex        =   13
               Top             =   330
               Width           =   3630
               _ExtentX        =   6403
               _ExtentY        =   556
               LabelWidth      =   1485
               TextMinWidth    =   1200
               Caption         =   "Monto Emisi�n"
               Text            =   ""
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin MSComCtl2.DTPicker Txt_FechaVencimiento 
               Height          =   315
               Left            =   1650
               TabIndex        =   14
               Top             =   690
               Width           =   1365
               _ExtentX        =   2408
               _ExtentY        =   556
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Format          =   67305473
               CurrentDate     =   37732
            End
            Begin hControl2.hTextLabel Txt_CorteMinimo 
               Height          =   315
               Left            =   4020
               TabIndex        =   17
               Top             =   330
               Width           =   2940
               _ExtentX        =   5186
               _ExtentY        =   556
               LabelWidth      =   1455
               TextMinWidth    =   1200
               Caption         =   "Corte Minimo"
               Text            =   ""
               Format          =   "#,###"
               Alignment       =   1
            End
            Begin MSComCtl2.DTPicker Txt_FechaPrepago 
               Height          =   345
               Left            =   5490
               TabIndex        =   20
               Top             =   1875
               Visible         =   0   'False
               Width           =   1485
               _ExtentX        =   2619
               _ExtentY        =   609
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Format          =   67305473
               CurrentDate     =   26123
            End
            Begin hControl2.hTextLabel Txt_TasaEmision 
               Height          =   315
               Left            =   4020
               TabIndex        =   18
               Top             =   690
               Width           =   2940
               _ExtentX        =   5186
               _ExtentY        =   556
               LabelWidth      =   1455
               TextMinWidth    =   1200
               Caption         =   "Tasa Emisi�n"
               Text            =   ""
               Format          =   "#,##0.00"
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Base 
               Height          =   345
               Left            =   4020
               TabIndex        =   21
               Top             =   1500
               Width           =   2715
               _ExtentX        =   4789
               _ExtentY        =   609
               LabelWidth      =   1455
               TextMinWidth    =   1200
               Caption         =   "Base"
               Text            =   ""
               Format          =   "#,###"
               Alignment       =   1
            End
            Begin MSComCtl2.DTPicker DTP_Fecha_Emision 
               Height          =   345
               Left            =   5490
               TabIndex        =   19
               Top             =   1080
               Width           =   1485
               _ExtentX        =   2619
               _ExtentY        =   609
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               CheckBox        =   -1  'True
               Format          =   67305473
               CurrentDate     =   37732
            End
            Begin TrueDBList80.TDBCombo Cmb_TipoTasa 
               Height          =   345
               Left            =   1650
               TabIndex        =   15
               Top             =   1080
               Width           =   1965
               _ExtentX        =   3466
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":0C93
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin TrueDBList80.TDBCombo Cmb_Periodicidad 
               Height          =   345
               Left            =   1650
               TabIndex        =   16
               Top             =   1470
               Width           =   1965
               _ExtentX        =   3466
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":0D3D
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin VB.Label Label9 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Tipo Tasa"
               Height          =   345
               Left            =   150
               TabIndex        =   43
               Top             =   1080
               Width           =   1485
            End
            Begin VB.Label Label10 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Periodicidad"
               Height          =   345
               Left            =   150
               TabIndex        =   42
               Top             =   1470
               Width           =   1485
            End
            Begin VB.Label Label11 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha Vencimiento"
               Height          =   345
               Left            =   150
               TabIndex        =   41
               Top             =   690
               Width           =   1485
            End
            Begin VB.Label Label12 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha Prepago"
               Height          =   345
               Left            =   4020
               TabIndex        =   40
               Top             =   1875
               Visible         =   0   'False
               Width           =   1455
            End
            Begin VB.Label Label16 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha Emisi�n"
               Height          =   345
               Left            =   4020
               TabIndex        =   39
               Top             =   1080
               Width           =   1455
            End
         End
         Begin VB.Frame Frame_FFMM 
            Caption         =   "Atributos Fondo Mutuos"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   825
            Left            =   90
            TabIndex        =   34
            Top             =   7680
            Width           =   8775
            Begin VB.Frame Frm_inversion 
               Caption         =   "Inversi�n"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   555
               Left            =   3210
               TabIndex        =   36
               Top             =   150
               Width           =   2655
               Begin VB.OptionButton Opt_Inversion_Conocida 
                  Caption         =   "Conocida"
                  Height          =   225
                  Left            =   120
                  TabIndex        =   24
                  Top             =   240
                  Value           =   -1  'True
                  Width           =   1005
               End
               Begin VB.OptionButton Opt_Inversion_Desconocida 
                  Caption         =   "Desconocida"
                  Height          =   285
                  Left            =   1230
                  TabIndex        =   25
                  Top             =   210
                  Width           =   1275
               End
            End
            Begin VB.Frame Frm_Rescate 
               Caption         =   "Rescate"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   555
               Left            =   6000
               TabIndex        =   35
               Top             =   150
               Width           =   2655
               Begin VB.OptionButton Opt_Rescate_Conocido 
                  Caption         =   "Conocida"
                  Height          =   225
                  Left            =   120
                  TabIndex        =   26
                  Top             =   240
                  Value           =   -1  'True
                  Width           =   1005
               End
               Begin VB.OptionButton Opt_Rescate_Desconocido 
                  Caption         =   "Desconocida"
                  Height          =   285
                  Left            =   1230
                  TabIndex        =   27
                  Top             =   210
                  Width           =   1275
               End
            End
            Begin TrueDBList80.TDBCombo Dias_Liquidez 
               Height          =   345
               Left            =   1890
               TabIndex        =   23
               Tag             =   "OBLI=S;CAPTION=Di�s Valuta"
               Top             =   300
               Width           =   1185
               _ExtentX        =   2090
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   -1  'True
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Nemotecnicos.frx":0DE7
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin VB.Label Label15 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "D�as Valuta"
               Height          =   345
               Left            =   120
               TabIndex        =   37
               Top             =   300
               Width           =   1725
            End
         End
      End
   End
End
Attribute VB_Name = "Frm_Nemotecnicos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFormInicial As Single
Dim fFormRF_NAC As Single
Dim fFormFFMM_NAC As Single

Public fKey As String
Public fIdSerie As String
Public fIdCupones As String

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String
Dim fCod_Producto As String

Dim bRecalcula As Boolean
Dim bConTblDesa As Boolean

Dim Nemotecnico_Nuevo As Boolean

'Tipo de Arbol
'---------------------------
Dim sTipoDeArbol  As Long

Const cPrefijo = "K"
Const cRoot = "ROOT"

Private Sub Sub_ParamEmpresa()
   Dim lcEmpresa       As Class_Empresas

   Set lcEmpresa = New Class_Empresas
   With lcEmpresa
      .Campo("id_empresa").Valor = gId_Empresa
      If .Buscar Then
         If .Cursor.Count > 0 Then
           bConTblDesa = .Cursor(1)("flg_tabla_desarrollo").Value = "S"
         Else
            bConTblDesa = False
         End If
      Else
         bConTblDesa = False
      End If
    End With
    Set lcEmpresa = Nothing

End Sub

Private Sub Sub_ReCalculaTabla()
   Dim lLinea As Integer
   Dim lFlujo As Double
   Dim lSaldo As Double
   Dim lfecha As Date
   
   
   For lLinea = 1 To Grilla_Cupones.Rows - 1
      If Not IsDate(GetCell(Grilla_Cupones, lLinea, "FECHACUPON")) Then
         MsgBox "La Fecha de cup�n es incorrecta", vbInformation, "Nro Cup�n " & GetCell(Grilla_Cupones, lLinea, "NroCupon")
         Exit Sub
      ElseIf GetCell(Grilla_Cupones, lLinea, "INTERES") = "" Then
         MsgBox "Falta Ingresar el Inter�s al cup�n", vbInformation, "Nro Cup�n " & GetCell(Grilla_Cupones, lLinea, "NroCupon")
         Exit Sub
      ElseIf GetCell(Grilla_Cupones, lLinea, "CAPITAL") = "" Then
         MsgBox "Falta Ingresar el Capital al cup�n", vbInformation, "Nro Cup�n " & GetCell(Grilla_Cupones, lLinea, "NroCupon")
         Exit Sub
      End If
   Next
   
   lSaldo = 100
   For lLinea = 1 To Grilla_Cupones.Rows - 1
      lFlujo = Round(CDbl(NVL(GetCell(Grilla_Cupones, lLinea, "INTERES"), 0)), CmbDecimalesTabla.Text) + Round(CDbl(NVL(GetCell(Grilla_Cupones, lLinea, "CAPITAL"), 0)), CmbDecimalesTabla.Text)
      lSaldo = Round(lSaldo - Round(CDbl(NVL(GetCell(Grilla_Cupones, lLinea, "CAPITAL"), 0)), CmbDecimalesTabla.Text), CmbDecimalesTabla.Text)
      lfecha = Round(CDate(GetCell(Grilla_Cupones, lLinea, "FECHACUPON")), CmbDecimalesTabla.Text)
      
      SetCell Grilla_Cupones, lLinea, "NroCupon", lLinea, True
      SetCell Grilla_Cupones, lLinea, "FECHACUPON", lfecha, True
      SetCell Grilla_Cupones, lLinea, "FLUJO", lFlujo, True
      SetCell Grilla_Cupones, lLinea, "SALDO", lSaldo, True
      
   Next
End Sub

Private Sub Arbol_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim lcNodo As Node
  
  For Each lcNodo In Arbol.Nodes
    If (lcNodo.Checked) And (lcNodo.Children > 0) Then
      lcNodo.Checked = False
    End If
  Next
  
End Sub

Private Sub Arbol_NodeCheck(ByVal Node As MSComctlLib.Node)
  Call Sub_LimpiarCheckArbol(Arbol)
End Sub

Private Sub Arbol_NodeClick(ByVal Node As MSComctlLib.Node)
  Call Sub_LimpiarCheckArbol(Arbol)
End Sub

Private Sub Chk_Fungible_Click()
  If Chk_Fungible.Value = vbChecked Then
    Rem Corte Minimo No obligatorio
    Txt_CorteMinimo.BackColorTxt = fColorOpcional
    Txt_CorteMinimo.Tag = ""
  Else
    Rem Corte Minimo Obligatorio
    Txt_CorteMinimo.BackColorTxt = fColorOBligatorio
    Txt_CorteMinimo.Tag = "OBLI"
  End If
End Sub

Private Sub Cmb_EmisorDeudor_ItemChange()
Dim lKey As String
Dim lReg As hFields
Dim lEmisor_Especifico As Class_Emisores_Especifico

  Call Sub_Bloquea_Puntero(Me)

  lKey = Fnt_FindValue4Display(Cmb_EmisorDeudor, Cmb_EmisorDeudor.Text)
  If Not lKey = "" Then
    Call Sub_ComboSelectedItem(Cmb_EmisorOriginal, lKey)
    Call Cmb_EmisorOriginal_ItemChange
    
    Set lEmisor_Especifico = New Class_Emisores_Especifico
    With lEmisor_Especifico
      .Campo("id_Emisor_Especifico").Valor = lKey
      If .Buscar(True) Then
        For Each lReg In .Cursor
          Txt_EmisorGeneral_Deudor.Text = lReg("Dsc_emisor_general").Value
          Txt_Sector_Deudor.Text = lReg("Dsc_sector").Value
        Next
      Else
        Call Fnt_MsgError(.SubTipo_LOG, "Problemas al buscar los datos del 'Emisor Especifico'" _
                          , .ErrMsg _
                          , pConLog:=True)
      End If
    End With
    Set lEmisor_Especifico = Nothing
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Cmb_EmisorDeudor_LostFocus()
Dim lKey As String
  
  lKey = Fnt_FindValue4Display(Cmb_EmisorDeudor, Cmb_EmisorDeudor.Text)
  If Not lKey = "" Then
    If Fnt_FindValue4Display(Cmb_EmisorOriginal, Cmb_EmisorOriginal.Text) = "" Then
      Call Sub_ComboSelectedItem(Cmb_EmisorOriginal, lKey)
      Call Cmb_EmisorOriginal_ItemChange
    End If
  End If
End Sub

Private Sub Cmb_EmisorOriginal_ItemChange()
Dim lKey As String
Dim lReg As hFields
Dim lEmisor_Especifico As Class_Emisores_Especifico

  Call Sub_Bloquea_Puntero(Me)

  If Not Fnt_ComboSelected_KEY(Cmb_EmisorOriginal) = "" Then
    lKey = Fnt_FindValue4Display(Cmb_EmisorOriginal, Cmb_EmisorOriginal.Text)
    
    Set lEmisor_Especifico = New Class_Emisores_Especifico
    With lEmisor_Especifico
      .Campo("id_Emisor_Especifico").Valor = lKey
      If .Buscar(True) Then
        For Each lReg In .Cursor
          Txt_EmisorGeneral_Original.Text = lReg("Dsc_emisor_general").Value
          Txt_Sector_Original.Text = lReg("Dsc_sector").Value
        Next
      Else
        Call Fnt_MsgError(.SubTipo_LOG, "Problemas al buscar los datos del 'Emisor Especifico'" _
                        , .ErrMsg _
                        , pConLog:=True)
      End If
    End With
    Set lEmisor_Especifico = Nothing
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Cmb_Productos_LostFocus
' Author    : Jorge Vidal
' Date      : 21/04/2008
' Purpose   : Se agrega el metodo lostFocus al combo productos ya que el control no
'             cuando se digita el nombre de un producto y se presiona la tecla TAB
'             no ejecuta el metodo "Cmb_Instrumento_ItemChange" por tanto no mueve a la
'             posicion correcta el puntero.
'---------------------------------------------------------------------------------------
Private Sub Cmb_Productos_LostFocus()
    Dim lcod_producto As String
    lcod_producto = Fnt_FindValue4Display(Cmb_Productos, Cmb_Productos.Text)
    
    ' corte minimo solo obligatorio con resta variable nacional
    If lcod_producto = "RF_NAC" Then
        Txt_CorteMinimo.BackColorTxt = fColorOBligatorio
        Txt_CorteMinimo.Tag = "OBLI"
    End If
    Call Sub_MostrarFrame(lcod_producto)
    Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento, lcod_producto)
End Sub

Private Sub Cmb_Instrumento_ItemChange()
Dim lCod_Instrumento As String
  lCod_Instrumento = Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text)
  
  If bConTblDesa Then
'   SSTab.TabVisible(4) = lCod_Instrumento = gcINST_BONOS_NAC Or lCod_Instrumento = gcINST_BONOS_INT
  
      If lCod_Instrumento = gcINST_BONOS_NAC Or lCod_Instrumento = gcINST_BONOS_INT Then
         SSTab.TabVisible(4) = True
         Cmb_Periodicidad.Tag = "OBLI=S;CAPTION=Periodicidad"
         Cmb_Periodicidad.BackColor = &HC0FFC0
         Txt_TasaEmision.Tag = "OBLI"
         Txt_TasaEmision.BackColorTxt = &HC0FFC0
      Else
         SSTab.TabVisible(4) = False
         Cmb_Periodicidad.Tag = ""
         Cmb_Periodicidad.BackColor = vbWhite
         Txt_TasaEmision.Tag = ""
         Txt_TasaEmision.BackColorTxt = vbWhite
      End If
  End If
  
  Call Sub_CargaCombo_Subfamilias(Cmb_Subfamilia, lCod_Instrumento)
  If Cmb_Subfamilia.ListCount > 0 Then
    Rem Subfamilia Obligatorio
    Cmb_Subfamilia.Tag = "OBLI=S;CAPTION=Subfamilia"
  Else
    Rem Subfamilia No obligatorio
    Cmb_Subfamilia.Tag = ""
  End If
  
  If lCod_Instrumento = gcINST_BONOS_NAC Or lCod_Instrumento = gcINST_DEPOSITOS_NAC Then
     Txt_CodAchs.Visible = True: Toolbar_CAchs.Visible = True
  Else
     Txt_CodAchs.Visible = False: Toolbar_CAchs.Visible = False
  End If
  
  Call Sub_FormControl_Color(Me.Controls)
End Sub
'---------------------------------------------------------------------------------------
' Procedure : Cmb_Instrumento_LostFocus
' Author    : Jorge Vidal
' Date      : 17/04/2008
' Purpose   : Se agrega el metodo lostFocus al combo instrumento ya que el control no
'             cuando se digita el nombre de un instrumento y se presiona la tecla TAB
'             no ejecuta el metodo "Cmb_Instrumento_ItemChange" por tanto no mueve a la
'             posicion correcta el puntero.
'---------------------------------------------------------------------------------------
Private Sub Cmb_Instrumento_LostFocus()
    Dim lCod_Instrumento As String
    lCod_Instrumento = Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text)
    
   If bConTblDesa Then
'      SSTab.TabVisible(4) = lCod_Instrumento = gcINST_BONOS_NAC Or lCod_Instrumento = gcINST_BONOS_INT
      If lCod_Instrumento = gcINST_BONOS_NAC Or lCod_Instrumento = gcINST_BONOS_INT Then
         SSTab.TabVisible(4) = True
         Cmb_Periodicidad.Tag = "OBLI=S;CAPTION=Periodicidad"
         Cmb_Periodicidad.BackColor = &HC0FFC0
         Txt_TasaEmision.Tag = "OBLI"
         Txt_TasaEmision.BackColorTxt = &HC0FFC0
      Else
         SSTab.TabVisible(4) = False
         Cmb_Periodicidad.Tag = ""
         Cmb_Periodicidad.BackColor = vbWhite
         Txt_TasaEmision.Tag = ""
         Txt_TasaEmision.BackColorTxt = vbWhite
      End If
   End If
   
    Call Sub_CargaCombo_Subfamilias(Cmb_Subfamilia, lCod_Instrumento)
    
    If Cmb_Subfamilia.ListCount > 0 Then
        Rem Subfamilia Obligatorio
        Cmb_Subfamilia.Tag = "OBLI=S;CAPTION=Subfamilia"
    Else
        Rem Subfamilia No obligatorio
        Cmb_Subfamilia.Tag = ""
    End If
    
    If lCod_Instrumento = gcINST_BONOS_NAC Or lCod_Instrumento = gcINST_DEPOSITOS_NAC Then
        Txt_CodAchs.Visible = True: Toolbar_CAchs.Visible = True
    Else
        Txt_CodAchs.Visible = False: Toolbar_CAchs.Visible = False
    End If
    
    Call Sub_FormControl_Color(Me.Controls)
End Sub



Private Sub Cmb_TipoArbol_ItemChange()
  If Not Cmb_TipoArbol.Text = "" Then
    sTipoDeArbol = Fnt_ComboSelected_KEY(Cmb_TipoArbol)
    Call Sub_Carga_Clases_Instrumento
    Call Fnt_Carga_Rel_ACI_Emp_Nemo
  End If
End Sub

'Private Sub Cmb_Productos_ItemChange()
'Dim lCod_Producto As String
'  lCod_Producto = Fnt_FindValue4Display(Cmb_Productos, Cmb_Productos.Text)
'  Call Sub_MostrarFrame(lCod_Producto)
'  Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento, lCod_Producto)
'End Sub

Private Sub Dias_Liquidez_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", "-" y "backspace"
  If Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  With Toolbar_Grilla
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("ADD").Image = cBoton_Agregar_Grilla
    .Buttons("DEL").Image = cBoton_Eliminar_Grilla
    .Appearance = ccFlat
  End With
  
  With tlbGrillaCupones
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("ADD").Image = cBoton_Agregar_Grilla
    .Buttons("DEL").Image = cBoton_Eliminar_Grilla
    .Buttons("PASTE").Image = cBoton_Paste
    .Appearance = ccFlat
  End With
  
  With Toolbar_CAchs
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("ADD").Image = cBoton_Buscar
    .Appearance = ccFlat
  End With
  
  Call Sub_CargaCombo_Arbol_Clase_Inst_Tipo(Cmb_TipoArbol)
  Call Sub_ComboSelectedItem(Cmb_TipoArbol, 1)
  sTipoDeArbol = Fnt_ComboSelected_KEY(Cmb_TipoArbol)
  
  Call Sub_ParamEmpresa
  Call Sub_CargaForm
  
  

  Me.Top = 1
  Me.Left = 1
End Sub
Public Function Fnt_Modificar(pColum_PK, pCod_Arbol_Sistema, Optional pCod_Producto)
  fKey = pColum_PK
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  
  If IsMissing(pCod_Producto) Then
    fCod_Producto = ""
  Else
    fCod_Producto = pCod_Producto
  End If
  
  Call Form_Resize
  
  Call Sub_CargarDatos
    
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de Nemot�cnico"
  Else
    Me.Caption = "Modificaci�n de Nemot�cnico: " & Txt_Nemotecnico.Text
  End If
  
  Call Sub_ComboSelectedItem(Cmb_Productos, pCod_Producto)
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
    
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub


Private Sub Grilla_Alias_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  Select Case Grilla_Alias.ColKey(Col)
    Case "Valor"
      'Esto eso tiene
    Case Else
      Cancel = True
  End Select
End Sub

Private Sub Grilla_Cupones_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
   Dim lNombreCol As String

   lNombreCol = UCase(Grilla_Cupones.ColKey(Col))

   If lNombreCol = "NROCUPON" Or lNombreCol = "FLUJO" Or lNombreCol = "SALDO" Then
      Cancel = True
   End If
   
End Sub

Private Sub Grilla_Cupones_CellChanged(ByVal Row As Long, ByVal Col As Long)
      If bRecalcula Then Call Sub_ReCalculaTabla
End Sub

Private Sub Grilla_Cupones_KeyPress(KeyAscii As Integer)
   If KeyAscii = 22 Then Sub_Clipboard_Paste
End Sub

Private Sub Sub_Clipboard_Paste()
   Dim lClip As String
   Dim lmMatriX
   Dim lLinea
   Dim lLinGrilla As Long
   Dim lValor
   Dim lRespuesta As String
   Dim lContinuar As Boolean
   Dim lTodo      As String
   Dim lRow As Long

  Call Sub_Bloquea_Puntero(Me)
  Call Sub_FormControl_Enabled(Me.Controls, Me, False)
  
  lTodo = ""
  Call Sub_StatusBar(pTexto:="Preparando Importaci�n...")

  If Not Clipboard.GetFormat(vbCFText) Then
    GoTo ExitProcedure
  End If
  
  lClip = Clipboard.GetText(vbCFText)
  
  lmMatriX = Fnt_Matrix_Text(pText:=lClip)
  If IsArray(lmMatriX) Then
    Grilla_Cupones.Rows = 1
    lRow = 0
    
    For lLinea = LBound(lmMatriX) To UBound(lmMatriX)
      Call Sub_Interactivo(gRelogDB)
      Call Sub_StatusBar(pTexto:="Importando " & Format(lLinea + 1, "#,##0") & " de " & Format(UBound(lmMatriX) + 1, "#,##0") & " - " & Format(((lLinea + 1) / (UBound(lmMatriX) + 1)) * 100, "#,##0.00") & "% compleado.")
        
      If IsArray(lmMatriX(lLinea)) Then
        If UBound(lmMatriX(lLinea)) > 0 Then
          With Grilla_Cupones
            If lmMatriX(lLinea)(1) <> "" And IsNumeric(lmMatriX(lLinea)(5)) Then
               If Trim(UCase(lmMatriX(lLinea)(1))) = "VARIABLE" Then lmMatriX(lLinea)(1) = "01/01/1900"
               lRow = lRow + 1
               .AddItem ""
               SetCell Grilla_Cupones, lRow, "IDCUPON", "-1"
               SetCell Grilla_Cupones, lRow, "NROCUPON", lmMatriX(lLinea)(0)
               SetCell Grilla_Cupones, lRow, "FECHACUPON", lmMatriX(lLinea)(1)
               SetCell Grilla_Cupones, lRow, "INTERES", lmMatriX(lLinea)(2)
               SetCell Grilla_Cupones, lRow, "CAPITAL", lmMatriX(lLinea)(3)
               SetCell Grilla_Cupones, lRow, "FLUJO", lmMatriX(lLinea)(4)
               SetCell Grilla_Cupones, lRow, "SALDO", lmMatriX(lLinea)(5)
                  
            End If
          End With
        End If
      End If
    
    Next
  End If
  
ExitProcedure:
  Call Sub_StatusBar(pTexto:="Listo.")
  
  Call Sub_Desbloquea_Puntero(Me)
  Call Sub_FormControl_Enabled(Me.Controls, Me, True)
  
End Sub


Private Sub Grilla_Cupones_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
    If KeyAscii <> vbKeyReturn And KeyAscii <> vbKeyBack Then
        Call Sub_ValidarColumnas(KeyAscii, Col, 1)
    End If
End Sub

Private Sub SSTab_Click()
    
    
    
    Select Case SSTab.CurrTab
        Case 0 ' INSTRUMENTOS
            Exit Sub
        Case 1 ' CLASIFICACION DE RIESGO
        Case 2 ' CLASE
        Case 3 ' ALIAS
        Case 4 ' SERIE Y TABLA DESARROLLO
            If Txt_Nemotecnico.Text = "" Then
                MsgBox "No ha ingresado Nemotecnico"
                SSTab.CurrTab = 0
                Exit Sub
            Else
               If fIdSerie = -1 Or Me.TxtTasaEfectiva.Text = 0 Then
                  Call Sub_Carga_Serie_Por_Nemotecnico
               End If
               If fIdCupones = -1 Or Grilla_Cupones.Rows = 1 Then
                  bRecalcula = False
                  Call Sub_Carga_Cupones_Por_Nemotecnico
                  bRecalcula = True
               End If
            End If
    End Select
    
End Sub

Private Sub tlbGrillaCupones_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
   Select Case Button.Key
      Case "ADD"
         Grilla_Cupones.AddItem ""
         SetCell Grilla_Cupones, Grilla_Cupones.Rows - 1, "IdCupon", -1
         SetCell Grilla_Cupones, Grilla_Cupones.Rows - 1, "FECHACUPON", "01/01/1900"
         SetCell Grilla_Cupones, Grilla_Cupones.Rows - 1, "INTERES", "0"
         SetCell Grilla_Cupones, Grilla_Cupones.Rows - 1, "CAPITAL", "0"
      Case "DEL"
         If Grilla_Cupones.Row > 0 Then
            If MsgBox("�Est� seguro de eliminar el Cup�n?", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
               Grilla_Cupones.RemoveItem (Grilla_Cupones.Row)
               Call Sub_ReCalculaTabla
            End If
         End If
      Case "PASTE"
         bRecalcula = False
         Sub_Clipboard_Paste
         bRecalcula = True
   End Select
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "SAVE"
      bRecalcula = False
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
      bRecalcula = True
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Sub Toolbar_CAchs_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lReg As hFields
Dim lLinea As Long
Dim lcNemotecnicos As Class_Nemotecnicos
    
If Fnt_ComboSelected_KEY(Cmb_Subfamilia) = "" Then
 MsgBox "Falta SubFamilia", vbExclamation, "C�digo ACHS"
 Exit Sub
End If
If Trim(Txt_Nemotecnico.Text) = "" Then
 MsgBox "Falta Nemot�cnico", vbExclamation, "C�digo ACHS"
 Exit Sub
End If
If Fnt_ComboSelected_KEY(Cmb_EmisorDeudor) = "" Then
 MsgBox "Falta Emisor Deudor", vbExclamation, "C�digo ACHS"
 Exit Sub
End If
If Not IsDate(DTP_Fecha_Emision.Value) Then
 MsgBox "Falta Fecha Emisi�n", vbExclamation, "C�digo ACHS"
 Exit Sub
End If
If Not IsDate(Txt_FechaVencimiento.Value) Then
 MsgBox "Falta Fecha Vencimiento", vbExclamation, "C�digo ACHS"
 Exit Sub
End If
    
Call Sub_Bloquea_Puntero(Me)

Set lcNemotecnicos = New Class_Nemotecnicos
With lcNemotecnicos
  .Campo("ID_EMISOR_ESPECIFICO").Valor = Fnt_ComboSelected_KEY(Cmb_EmisorDeudor)
  .Campo("ID_SUBFAMILIA").Valor = Fnt_ComboSelected_KEY(Cmb_Subfamilia)
  .Campo("NEMOTECNICO").Valor = Trim(Txt_Nemotecnico.Text)
  .Campo("FECHA_EMISION").Valor = DTP_Fecha_Emision.Value
  .Campo("FECHA_VENCIMIENTO").Valor = Txt_FechaVencimiento.Value
  .Campo("TASA_EMISION").Valor = IIf(IsNumeric(Txt_TasaEmision.Text), Txt_TasaEmision.Text, 0)
  If .GenerarCodAchs Then
    For Each lReg In .Cursor
      Txt_CodAchs.Text = NVL(lReg("COD_ACHS").Value, "")
    Next
  Else
    MsgBox "Problemas al Generar C�digo Achs." & vbCr & vbCr & .ErrMsg, vbCritical, "C�digo ACHS"
  End If
End With
Set lcNemotecnicos = Nothing

Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Toolbar_Grilla_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "ADD"
      Call Sub_Agregar
    Case "DEL"
      If Grilla.Row > 0 Then
        If MsgBox("�Est� seguro de eliminar el Clasificador de Riesgo?", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
          Grilla.RemoveItem (Grilla.Row)
        End If
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcNemotecnicos As Class_Nemotecnicos
'------------------------------------------------
Dim lCod_Instrumento As String
Dim lId_Subfamilia As String
Dim lid_Mercado_Transaccion ' As String
Dim lId_Emisor_Especifico As String
Dim lId_Emisor_Especifico_Original As String
Dim lId_Moneda_Transaccion As String
Dim lId_Moneda As String
Dim lCod_Estado As String
Dim lTipoTasa As String
Dim lPeriodicidad As String
Dim lLiquidez
Dim lCod_Pais ' As String
Dim lFungible As String
Dim lTipo_Cuota_Ingreso
Dim lTipo_Cuota_Egreso
Dim lFecha_Vencimiento

  Fnt_Grabar = False

  If Not Fnt_ValidarDatos Then
    Exit Function
  End If
  
  
  gDB.IniciarTransaccion
  
  lCod_Instrumento = Fnt_ComboSelected_KEY(Cmb_Instrumento)
  lId_Subfamilia = Fnt_ComboSelected_KEY(Cmb_Subfamilia)
  
  lid_Mercado_Transaccion = Fnt_ComboSelected_KEY(Cmb_MercadoTransaccion)
  
  lId_Emisor_Especifico = Fnt_ComboSelected_KEY(Cmb_EmisorDeudor)
  lId_Emisor_Especifico_Original = Fnt_ComboSelected_KEY(Cmb_EmisorOriginal)
  lId_Moneda = Fnt_ComboSelected_KEY(Cmb_MonedaOrigen)
  lCod_Estado = Fnt_ComboSelected_KEY(Cmb_Estado)
  lTipoTasa = Fnt_ComboSelected_KEY(Cmb_TipoTasa)
  lPeriodicidad = Fnt_ComboSelected_KEY(Cmb_Periodicidad)
  lLiquidez = IIf(Dias_Liquidez.Text = "", Null, Dias_Liquidez.Text)
  
  lCod_Pais = Fnt_ComboSelected_KEY(Cmb_PaisOrigen)
  
  lId_Moneda_Transaccion = Fnt_ComboSelected_KEY(Cmb_MonedaTransaccion)
  
  If Fnt_ComboSelected_KEY(Cmb_Productos) = gcPROD_RF_NAC Or Fnt_ComboSelected_KEY(Cmb_Productos) = gcPROD_RF_INT Then
    lFungible = IIf(Chk_Fungible.Value = vbUnchecked, "N", "S")
    lFecha_Vencimiento = Txt_FechaVencimiento.Value
    If lid_Mercado_Transaccion = "" Then
        lid_Mercado_Transaccion = Null
    End If
    
    If lCod_Pais = "" Then
        lCod_Pais = Null
    End If
    
  Else
    lFungible = ""
    lFecha_Vencimiento = ""
  End If
  
  If Fnt_ComboSelected_KEY(Cmb_Productos) = gcPROD_FFMM_NAC Then
    If Opt_Inversion_Conocida.Value Then
      lTipo_Cuota_Ingreso = cTCuota_Conocida
    ElseIf Opt_Inversion_Desconocida.Value Then
      lTipo_Cuota_Ingreso = cTCuota_Desconocida
    End If
    
    If Opt_Rescate_Conocido.Value Then
      lTipo_Cuota_Egreso = cTCuota_Conocida
    ElseIf Opt_Rescate_Desconocido.Value Then
      lTipo_Cuota_Egreso = cTCuota_Desconocida
    End If
  Else
    lTipo_Cuota_Ingreso = Null
    lTipo_Cuota_Egreso = Null
  End If
  
  Set lcNemotecnicos = New Class_Nemotecnicos
  With lcNemotecnicos
    .Campo("id_Nemotecnico").Valor = fKey
    .Campo("cod_Instrumento").Valor = lCod_Instrumento
    .Campo("id_subfamilia").Valor = lId_Subfamilia
    .Campo("nemotecnico").Valor = Txt_Nemotecnico.Text
    .Campo("id_Mercado_Transaccion").Valor = lid_Mercado_Transaccion
    .Campo("id_Emisor_Especifico").Valor = lId_Emisor_Especifico
    .Campo("id_Emisor_Especifico_Origen").Valor = lId_Emisor_Especifico_Original
    .Campo("id_Moneda").Valor = lId_Moneda
    .Campo("id_Tipo_Estado").Valor = cTEstado_Nemotecnico
    .Campo("cod_Estado").Valor = lCod_Estado
    .Campo("dsc_Nemotecnico").Valor = Txt_Descripcion.Text
    .Campo("tasa_Emision").Valor = Txt_TasaEmision.Text
    .Campo("tipo_Tasa").Valor = lTipoTasa
    .Campo("periodicidad").Valor = lPeriodicidad
    .Campo("fecha_Vencimiento").Valor = lFecha_Vencimiento
    .Campo("corte_Minimo_Papel").Valor = IIf(Txt_CorteMinimo.Text = "", 0, Txt_CorteMinimo.Text)
    .Campo("monto_Emision").Valor = Txt_MontoEmision.Text
    .Campo("base").Valor = Txt_Base.Text
    .Campo("cod_Pais").Valor = lCod_Pais
    .Campo("id_Moneda_transaccion").Valor = lId_Moneda_Transaccion
    .Campo("flg_Fungible").Valor = lFungible
    .Campo("fecha_emision").Valor = DTP_Fecha_Emision.Value
    Rem FFMM
    .Campo("liquidez").Valor = lLiquidez
    .Campo("flg_tipo_cuota_ingreso").Valor = lTipo_Cuota_Ingreso
    .Campo("flg_tipo_cuota_egreso").Valor = lTipo_Cuota_Egreso
    If Not .Guardar Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas en grabar.", .ErrMsg, pConLog:=True)
      Fnt_Grabar = False
      GoTo ErrProcedure
    End If
    fKey = .Campo("id_Nemotecnico").Valor
  End With
  Set lcNemotecnicos = Nothing
  
  If Not Fnt_Grabar_Clasificadores_Riesgo() Then
    GoTo ErrProcedure
  End If
  
  If Not Fnt_Grabar_Alias Then
    GoTo ErrProcedure
  End If
  
  If Not Fnt_Grabar_Rel_Atributos Then
    GoTo ErrProcedure
  End If
  
  If Not Fnt_Guarda_Rel_ACI_Emp_Nemo Then
    GoTo ErrProcedure
  End If
  
  If SSTab.TabVisible(4) Then
      If Not Fnt_Guarda_Serie_Tabla_Desarrollo Then
         GoTo ErrProcedure
      End If
  End If
  
  
  Fnt_Grabar = True
  
ErrProcedure:
  If Fnt_Grabar Then
    gDB.CommitTransaccion
  Else
    gDB.RollbackTransaccion
  End If
  gDB.Parametros.Clear
  
End Function

Private Sub Sub_CargaForm()
Dim lReg As hFields
  
  SSTab.CurrTab = 0
  SSTab.Height = 5775
  fFormInicial = 6705
  fFormRF_NAC = fFormInicial + Frame_RF.Height
  fFormFFMM_NAC = fFormInicial + Frame_FFMM.Height
  
  Rem --------- Fungible ------------
  Chk_Fungible.Value = vbUnchecked
  Rem -------------------------------
  
  Call Sub_MostrarFrame("")
  Call Sub_FormControl_Color(Me.Controls)
  
  Call Sub_CargaCombo_Estados(Cmb_Estado, cTEstado_Nemotecnico)
  Call Sub_Elije_EstadoDefault(Cmb_Estado) 'HAF 20070703

  Call Sub_CargaCombo_Productos(Cmb_Productos)
  Call Sub_ComboSelectedItem(Cmb_Productos, fCod_Producto)
  
  Call Sub_CargaCombo_Monedas(Cmb_MonedaOrigen)
  Call Sub_CargaCombo_Monedas(Cmb_MonedaTransaccion): Sub_ComboSelectedItem Cmb_MonedaTransaccion, 1

  Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento, 0)
  Call Sub_CargaCombo_Mercados_Transaccion(Cmb_MercadoTransaccion)
  Call Sub_CargaCombo_Paises(Cmb_PaisOrigen)
  Call Sub_CargaCombo_Emisores_Especifico(Cmb_EmisorDeudor)
  Call Sub_CargaCombo_Emisores_Especifico(Cmb_EmisorOriginal)
  
  Call Sub_Cargar_Origenes
  Call Sub_Carga_Clases_Instrumento
  Call CargaPeriodicidad
  
  Call CargaDias
  
   Call CargaDecimales
   Call Sub_CargaCombo_Familia_Serie(cmbFamilia)
   Call Sub_CargaCombo_Moneda_Serie(cmbMonedaSerie)
   
  fIdSerie = -1
  fIdCupones = -1
  Grilla_Cupones.Rows = 1
   
  SSTab.TabVisible(4) = False
   
End Sub
Private Sub Sub_Cargar_Origenes()
Dim lcOrigen As Object
Dim lReg     As hFields
'-----------------------------

  Set lcOrigen = Fnt_CreateObject(cDLL_Origenes)
  With lcOrigen
    Set .gDB = gDB
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas al cargar las contrapartes del alias.", .ErrMsg, pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  
  Grilla_Alias.Rows = 1
  Call Sub_hRecord2Grilla(lcOrigen.Cursor, Grilla_Alias, "id_origen")

ExitProcedure:

End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lRecord As hRecord
Dim lFlg_Fungible As String
Dim lNemotecnico As Class_Nemotecnicos
  
  Call Sub_Bloquea_Puntero(Me)
  
  Load Me
  Set lNemotecnico = New Class_Nemotecnicos
  With lNemotecnico
    .Campo("Id_Nemotecnico").Valor = fKey
    If .BuscarView Then
      For Each lReg In .Cursor
        
        Call Sub_ComboSelectedItem(Cmb_Productos, NVL(lReg("cod_producto").Value, ""))
        Call Cmb_Productos_LostFocus
        DoEvents
        Call Sub_ComboSelectedItem(Cmb_Instrumento, NVL(lReg("Cod_Instrumento").Value, ""))
        Call Cmb_Instrumento_ItemChange
        DoEvents
        Call Sub_CargaCombo_Instrumentos_SVS(Cmb_InstrumentoSVS, NVL(lReg("Cod_Instrumento").Value, ""), pBlanco:=True)
        
        Call Sub_ComboSelectedItem(Cmb_Subfamilia, NVL(lReg("id_subfamilia").Value, ""))
        
        Txt_Descripcion.Text = NVL(lReg("dsc_nemotecnico").Value, "")
        Txt_Nemotecnico.Text = NVL(lReg("nemotecnico").Value, "")

        Call Sub_ComboSelectedItem(Cmb_PaisOrigen, NVL(lReg("cod_pais").Value, ""))
        Call Sub_ComboSelectedItem(Cmb_MonedaOrigen, NVL(lReg("id_moneda").Value, ""))
        Call Sub_ComboSelectedItem(Cmb_EmisorDeudor, NVL(lReg("id_emisor_especifico").Value, ""))
        Call Sub_ComboSelectedItem(Cmb_EmisorOriginal, NVL(lReg("Id_Emisor_Especifico_Origen").Value, ""))
        Call Sub_ComboSelectedItem(Cmb_Estado, NVL(lReg("Cod_Estado").Value, ""))
        Call Sub_ComboSelectedItem(Cmb_MercadoTransaccion, NVL(lReg("id_mercado_transaccion").Value, ""))
        Call Sub_ComboSelectedItem(Cmb_MonedaTransaccion, NVL(lReg("Id_Moneda_Transaccion").Value, ""))
        Call Sub_ComboSelectedItem(Cmb_MercadoTransaccion, NVL(lReg("id_mercado_transaccion").Value, ""))
        
        Rem Atributos Renta Fija
        Txt_CorteMinimo.Text = NVL(lReg("Corte_Minimo_Papel").Value, "0")
        Txt_TasaEmision.Text = NVL(lReg("tasa_emision").Value, "")
        Txt_MontoEmision.Text = NVL(lReg("monto_emision").Value, "")
        DTP_Fecha_Emision.Value = NVL(lReg("fecha_emision").Value, "")
        Txt_FechaVencimiento.Value = NVL(lReg("fecha_vencimiento").Value, Date)
        
        Call Sub_ComboSelectedItem(Cmb_Periodicidad, NVL(lReg("Periodicidad").Value, ""))
        
        Txt_Base.Text = NVL(lReg("base").Value, "")
        lFlg_Fungible = NVL(lReg("Flg_Fungible").Value, "")
        If Not lFlg_Fungible = "" Then
          Chk_Fungible.Value = IIf(lReg("Flg_Fungible").Value = "N", vbUnchecked, vbChecked)
        End If
        '-------------------------------------------------------------------------------------
        
        Rem Atributos Fondos Mutuos
        Dias_Liquidez.Text = NVL(lReg("Dias_Liquidez").Value, "0")
        
        Select Case lReg("flg_tipo_cuota_ingreso").Value
          Case cTCuota_Conocida, Null
            Opt_Inversion_Conocida.Value = True
            Opt_Inversion_Desconocida.Value = False
          Case cTCuota_Desconocida
            Opt_Inversion_Conocida.Value = False
            Opt_Inversion_Desconocida.Value = True
        End Select
        
        Select Case lReg("flg_tipo_cuota_egreso").Value
          Case cTCuota_Conocida, Null
            Opt_Rescate_Conocido.Value = True
            Opt_Rescate_Desconocido.Value = False
          Case cTCuota_Desconocida
            Opt_Rescate_Conocido.Value = False
            Opt_Rescate_Desconocido.Value = True
        End Select
        '-------------------------------------------------------------------------------------
        If (fIdSerie = -1 Or Me.TxtTasaEfectiva.Text = 0) And bConTblDesa Then
            Call Sub_Carga_Serie_Por_Nemotecnico
        End If
        If (fIdCupones = -1 Or Grilla_Cupones.Rows = 1) And bConTblDesa Then
            bRecalcula = False
            Call Sub_Carga_Cupones_Por_Nemotecnico
            bRecalcula = True
        End If
        '-------------------------------------------------------------------------------------
      Next
      If Not .Cursor.Count > 0 Then
        Txt_CorteMinimo.Text = "0"
      End If

    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
    
  End With

  Call Cmb_EmisorDeudor_ItemChange
  Call Cmb_EmisorOriginal_ItemChange
  Call Sub_Carga_Clasificadores_Riesgo
  Call Sub_Carga_Rel_Atributos
  Call Sub_CargarAlias
  Call Fnt_Carga_Rel_ACI_Emp_Nemo
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Carga_Clasificadores_Riesgo()
Dim lReg As hFields
Dim lLinea As Long
Dim lClasificador_Riesgo As Class_Clasificadores_Riesgo
  
  Grilla.Rows = 1
  
  Set lClasificador_Riesgo = New Class_Clasificadores_Riesgo
  With lClasificador_Riesgo
    If .Buscar_Clasif_Riesgo_Nemo(fKey) Then
      For Each lReg In .Cursor
        lLinea = Grilla.Rows
        Grilla.AddItem ""
        SetCell Grilla, lLinea, "colum_pk", lReg("ID_CLASIFICADOR_RIESGO").Value
        SetCell Grilla, lLinea, "DSC_CLASIFICADOR_RIESGO", lReg("DSC_CLASIFICADOR_RIESGO").Value, True
        SetCell Grilla, lLinea, "COD_VALOR_CLASIFICACION", lReg("COD_VALOR_CLASIFICACION").Value, True
      Next
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lClasificador_Riesgo = Nothing

End Sub

Private Sub Sub_Carga_Rel_Atributos()
Dim lReg As hFields
Dim lLinea As Long
Dim lRel_Nemotecnico_Atributo As Class_Rel_Nemotecnico_Atributo
    
  Set lRel_Nemotecnico_Atributo = New Class_Rel_Nemotecnico_Atributo
  With lRel_Nemotecnico_Atributo
    .Campo("id_Nemotecnico").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        Txt_Isim.Text = NVL(lReg("ISIM").Value, "")
        Txt_Cusip.Text = NVL(lReg("CUSIP").Value, "")
        Txt_Sedol.Text = NVL(lReg("SEDOL").Value, "")
        Txt_NumAccionesEmisor.Text = NVL(lReg("N_ACCIONESEMISOR").Value, "")
        If Fnt_FindValue4Display(Cmb_Productos, Cmb_Productos.Text) = gcPROD_RF_NAC Then
          Txt_FechaPrepago.Value = NVL(lReg("FECHA_PREPAGO").Value, Txt_FechaPrepago.Value)
        End If
        If Fnt_FindValue4Display(Cmb_Productos, Cmb_Productos.Text) = gcPROD_FFMM_NAC Then
          Chk_CuentaPaso.Value = IIf(NVL(lReg("NEMO_CTAPASO").Value, "") = "S", vbChecked, vbUnchecked)
        End If
        Txt_Ticker.Text = NVL(lReg("TICKER").Value, "")
        Txt_CodAchs.Text = NVL(lReg("COD_ACHS").Value, "")
        Call Sub_ComboSelectedItem(Cmb_InstrumentoSVS, NVL(lReg("ID_INSTRUMENTO_SVS").Value, "0"))
      Next
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lRel_Nemotecnico_Atributo = Nothing

End Sub

Private Sub Sub_Carga_Cupones_Por_Nemotecnico()
Dim lReg As hFields
Dim lLinea As Long
Dim lTablaDesa As Class_Tabla_Desarrollo
  
  Grilla_Cupones.Rows = 1
  
  Set lTablaDesa = New Class_Tabla_Desarrollo
  With lTablaDesa
    If .Cupones_por_Nemotecnicos(Txt_Nemotecnico.Text) Then
      For Each lReg In .Cursor
        lLinea = Grilla_Cupones.Rows
        Grilla_Cupones.AddItem ""
        SetCell Grilla_Cupones, lLinea, "IdCupon", lReg("idcupones").Value
        SetCell Grilla_Cupones, lLinea, "NroCupon", lReg("nrocupon").Value
        SetCell Grilla_Cupones, lLinea, "FECHACUPON", lReg("FECHACUPON").Value
        SetCell Grilla_Cupones, lLinea, "INTERES", lReg("INTERES").Value, True
        SetCell Grilla_Cupones, lLinea, "CAPITAL", lReg("CAPITAL").Value, True
        SetCell Grilla_Cupones, lLinea, "FLUJO", lReg("FLUJO").Value, True
        SetCell Grilla_Cupones, lLinea, "SALDO", lReg("SALDO").Value, True
        fIdCupones = lReg("idcupones").Value
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas al leer los cupones." _
                        , .ErrMsg _
                        , pConLog:=True)
      Grilla_Cupones.AddItem ""
      fIdCupones = -1
    End If
  End With
  Set lTablaDesa = Nothing
   
End Sub

Private Sub Sub_Carga_Serie_Por_Nemotecnico()
Dim lReg As hFields
Dim lTablaSeries As Class_Tabla_Series
 
  Set lTablaSeries = New Class_Tabla_Series
  With lTablaSeries
    If .Serie_por_Nemotecnicos(Txt_Nemotecnico.Text) Then
      For Each lReg In .Cursor
        
        fIdSerie = lReg("idSerie").Value
        TxtTasaEfectiva.Text = lReg("tasaefectiva").Value
        dtpPrimerVmto.Value = lReg("fechaprimercorte").Value
        txtDiaCorteCupon.Text = lReg("diavctocupon").Value
        CmbDecimalesTabla.Text = lReg("decimalestd").Value
        Call Sub_ComboSelectedItem(cmbMonedaSerie, NVL(lReg("idMoneda").Value, ""))
        txtNroCupones.Text = lReg("nrocupones").Value
        txtNroAmortizaciones.Text = lReg("nroamortizaciones").Value
        txtPlazoAnos.Text = lReg("anos").Value
        txtSerieBanco.Text = lReg("seriebanco").Value
        Call Sub_ComboSelectedItem(cmbFamilia, NVL(lReg("idfamilia").Value, ""))
        
        Exit For
      Next
    Else
      fIdSerie = -1
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas al leer la serie." _
                        , .ErrMsg _
                        , pConLog:=True)
    End If
  End With
   Set lTablaSeries = Nothing
End Sub

Private Function Fnt_ValidarDatos() As Boolean

  Fnt_ValidarDatos = True
  
  If Not Fnt_Form_Validar(Me.Controls, Frame_Principal) Then
    GoTo ErrProcedure
  End If
  
  If Fnt_ComboSelected_KEY(Cmb_Productos) <> gcPROD_RF_INT Then ' COG 08/09
    If Not Fnt_Form_Validar(Me.Controls, Me.Frame_RF) Then
      GoTo ErrProcedure
    End If
  End If
  
  If Fnt_ComboSelected_KEY(Cmb_Productos) = gcPROD_RF_NAC And Chk_Fungible.Value = vbUnchecked And Txt_CorteMinimo.Text = "" Then
    MsgBox "Debe ingresar ""Corte M�nimo"".", vbInformation, Me.Caption
    GoTo ErrProcedure
  End If
  
  If Fnt_ComboSelected_KEY(Cmb_Productos) = gcPROD_FFMM_NAC And Dias_Liquidez.Text = "" Then
    MsgBox "Debe ingresar ""Fecha Valuta"".", vbInformation, Me.Caption
    GoTo ErrProcedure
  End If
  
  If Fnt_ComboSelected_KEY(Cmb_Productos) = gcPROD_RF_NAC Or Fnt_ComboSelected_KEY(Cmb_Productos) = gcPROD_RF_INT Then
    If IsNull(DTP_Fecha_Emision.Value) Then
      MsgBox "Debe ingresar ""Fecha Emision"".", vbInformation, Me.Caption
      GoTo ErrProcedure
    End If
  End If
  
  If Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text) = gcINST_BONOS_NAC Or Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text) = gcINST_DEPOSITOS_NAC Then
    If Trim(Txt_CodAchs.Text) <> "" Then
       If (Len(Txt_CodAchs.Text) <> 10) And (Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text) = gcINST_BONOS_NAC) Then
           MsgBox "Advertencia: C�digo ACHS No contiene la cantidad de caracteres exigidos (10).", vbExclamation, Me.Caption
       End If
       If (Len(Txt_CodAchs.Text) <> 12) And (Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text) = gcINST_DEPOSITOS_NAC) Then
           MsgBox "Advertencia: C�digo ACHS No contiene la cantidad de caracteres exigidos (12).", vbExclamation, Me.Caption
       End If
    End If
  Else
     Txt_CodAchs.Text = ""
  End If
  
  Exit Function
  
ErrProcedure:
  Fnt_ValidarDatos = False
  
'  If Cmb_Productos.Text = "" Then
'    MsgBox "Debe ingresar Producto.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Cmb_Instrumento.Text = "" Then
'    MsgBox "Debe ingresar Instrumento.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Cmb_Subfamilia.ListCount > 0 And IsNull(Cmb_Subfamilia.SelectedItem) Then
'    MsgBox "Debe ingresar Subfamilia.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Txt_Nemotecnico.Text = "" Then
'    MsgBox "Debe ingresar Nemot�cnico.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Cmb_PaisOrigen.Text = "" Then
'    MsgBox "Debe ingresar Pais Origen.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Cmb_EmisorDeudor.Text = "" Then
'    MsgBox "Debe ingresar Emisor Deudor.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Txt_Descripcion.Text = "" Then
'    MsgBox "Debe ingresar Descripci�n.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Cmb_MonedaOrigen.Text = "" Then
'    MsgBox "Debe ingresar Moneda Origen.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Cmb_MonedaTransaccion.Text = "" Then
'    MsgBox "Debe ingresar Moneda Transacci�n.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Cmb_Estado.Text = "" Then
'    MsgBox "Debe ingresar Estado.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Cmb_EmisorOriginal.Text = "" Then
'    MsgBox "Debe ingresar Emisor Original.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Fnt_ComboSelected_KEY(Cmb_Productos) = gcPROD_RF_NAC And Chk_Fungible.Value = vbUnchecked And Txt_CorteMinimo.Text = "" Then
'    MsgBox "Debe ingresar Corte M�nimo.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  ElseIf Fnt_ComboSelected_KEY(Cmb_Productos) = gcPROD_FFMM_NAC And Dias_Liquidez.Text = "" Then
'    MsgBox "Debe ingresar Fecha Valuta.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'  End If
  
End Function

Private Function Fnt_Valida_Serie_Tabla() As Boolean
   Dim lRow As Integer
   
  Fnt_Valida_Serie_Tabla = True
  
If Fnt_ComboSelected_KEY(Cmb_Productos) <> gcPROD_RF_INT Then '08/09 COG
  If Not Fnt_Form_Validar(Me.Controls, FrameSerie) Then
    GoTo ErrProcedure
  End If
  
'  If Txt_TasaEmision.Text = "" Then
'    MsgBox "Debe ingresar la Tasa Emisi�n desde Instrumentos.", vbInformation, Me.Caption
'    GoTo ErrProcedure
'  End If
'
'  If Cmb_Periodicidad.Text = "" Then
'    MsgBox "Debe seleccionar la Periodicidad desde Instrumentos.", vbInformation, Me.Caption
'    GoTo ErrProcedure
'  End If


  If Grilla_Cupones.Rows - 1 <> Val(txtNroCupones.Text) Then
    MsgBox "La Cantidad de Cupones debe coincidir con el Nro Cupones.", vbInformation, Me.Caption
    GoTo ErrProcedure
  End If
  
  For lRow = 1 To Grilla_Cupones.Rows - 1
      If GetCell(Grilla_Cupones, lRow, "NroCupon") <> lRow Then
         MsgBox "Los Nro Cup�n deben ser correlativos.", vbInformation, Me.Caption
         GoTo ErrProcedure
      End If
  Next
  
  If GetCell(Grilla_Cupones, Grilla_Cupones.Rows - 1, "SALDO") <> 0 Then
    MsgBox "El Saldo del ultimo Cup�n debe ser 0.", vbInformation, Me.Caption
    GoTo ErrProcedure
  End If
End If
  
  Exit Function
  
ErrProcedure:
  Fnt_Valida_Serie_Tabla = False
End Function

Private Sub Sub_MostrarFrame(ByVal pCod_Producto As String)
  
  Select Case pCod_Producto
    
    Case gcPROD_FFMM_NAC
      Frame_RF.Visible = False
      With Frame_FFMM
        .Visible = True
        .Top = Frame_Principal.Height + 20
      End With
      SSTab.Height = Frame_FFMM.Top + Frame_FFMM.Height + 400
      
      Call Sub_Tama�oForm(Me, pHeight:=fFormFFMM_NAC)
      'TabClase
      Call Sub_Tama�oCtrl(fra_Arbol, pHeight:=Me.Height, pPorc:=25): Call Sub_Tama�oCtrl(Arbol, pHeight:=Me.Height, pPorc:=30)
      'TabRiesgo
      Call Sub_Tama�oCtrl(Fra_Riesgo, pHeight:=Me.Height, pPorc:=20): Call Sub_Tama�oCtrl(Grilla, pHeight:=Me.Height, pPorc:=25)
      'TabAlias
      Call Sub_Tama�oCtrl(Fra_Alias, pHeight:=Me.Height, pPorc:=20): Call Sub_Tama�oCtrl(Grilla_Alias, pHeight:=Me.Height, pPorc:=25)
            
      Label19.Visible = True
      Chk_CuentaPaso.Visible = True
      Txt_CodAchs.Visible = False: Toolbar_CAchs.Visible = False
    
    Case gcPROD_RF_NAC, gcPROD_RF_INT
      Frame_FFMM.Visible = False
      Frame_RF.Visible = True
      
      Txt_MontoEmision.Visible = pCod_Producto = gcPROD_RF_NAC
      Txt_CorteMinimo.Visible = pCod_Producto = gcPROD_RF_NAC
      Label9.Visible = pCod_Producto = gcPROD_RF_NAC
      Cmb_TipoTasa.Visible = pCod_Producto = gcPROD_RF_NAC
      'Label16.Visible = pCod_Producto = gcPROD_RF_NAC
      'DTP_Fecha_Emision.Visible = pCod_Producto = gcPROD_RF_NAC
      Txt_Base.Visible = pCod_Producto = gcPROD_RF_NAC
      Label12.Visible = pCod_Producto = gcPROD_RF_NAC
      Txt_FechaPrepago.Visible = pCod_Producto = gcPROD_RF_NAC
      Chk_Fungible.Visible = pCod_Producto = gcPROD_RF_NAC
      
      Frame_RF.Top = Frame_Principal.Height + 20
      SSTab.Height = Frame_RF.Top + Frame_RF.Height + 400
      
      Call Sub_Tama�oForm(Me, pHeight:=SSTab.Height + 950)
      'TabClase
      Call Sub_Tama�oCtrl(fra_Arbol, pHeight:=Me.Height, pPorc:=20): Call Sub_Tama�oCtrl(Arbol, pHeight:=Me.Height, pPorc:=25)
      'TabRiesgo
      Call Sub_Tama�oCtrl(Fra_Riesgo, pHeight:=Me.Height, pPorc:=15): Call Sub_Tama�oCtrl(Grilla, pHeight:=Me.Height, pPorc:=20)
      'TabAlias
      Call Sub_Tama�oCtrl(Fra_Alias, pHeight:=Me.Height, pPorc:=15): Call Sub_Tama�oCtrl(Grilla_Alias, pHeight:=Me.Height, pPorc:=20)
      
      Label19.Visible = False
      Chk_CuentaPaso.Visible = False
      Txt_CodAchs.Visible = False: Toolbar_CAchs.Visible = False
      If pCod_Producto = gcPROD_RF_NAC Then
         Txt_CodAchs.Visible = True: Toolbar_CAchs.Visible = True
      End If
      
    Case gcPROD_RF_INT
      Frame_FFMM.Visible = False
      Frame_RF.Visible = True
      Frame_RF.Top = Frame_Principal.Height + 20
      Txt_MontoEmision.Visible = False
      Txt_CorteMinimo.Visible = False
      
      SSTab.Height = Frame_RF.Top + Frame_RF.Height + 400
      
      Call Sub_Tama�oForm(Me, pHeight:=SSTab.Height + 950)
      
      'TabClase
      Call Sub_Tama�oCtrl(fra_Arbol, pHeight:=Me.Height, pPorc:=25): Call Sub_Tama�oCtrl(Arbol, pHeight:=Me.Height, pPorc:=30)
      'TabRiesgo
      Call Sub_Tama�oCtrl(Fra_Riesgo, pHeight:=Me.Height, pPorc:=25): Call Sub_Tama�oCtrl(Grilla, pHeight:=Me.Height, pPorc:=30)
      'TabAlias
      Call Sub_Tama�oCtrl(Fra_Alias, pHeight:=Me.Height, pPorc:=25): Call Sub_Tama�oCtrl(Grilla_Alias, pHeight:=Me.Height, pPorc:=30)
      
      Txt_CodAchs.Visible = False: Toolbar_CAchs.Visible = False
      
    Case Else
      
      Call Sub_Tama�oForm(Me, pHeight:=fFormInicial)
      Frame_RF.Visible = False
      Frame_FFMM.Visible = False
      Label19.Visible = False
      Chk_CuentaPaso.Visible = False
      Txt_CodAchs.Visible = False: Toolbar_CAchs.Visible = False
      'SSTab.Height = 5835
      
      If (pCod_Producto = gcPROD_RV_NAC) Or (pCod_Producto = gcPROD_FFMM_INT) Or (pCod_Producto = gcPROD_RV_INT) Then
        'TabClase
        Call Sub_Tama�oCtrl(fra_Arbol, pHeight:=Me.Height, pPorc:=30): Call Sub_Tama�oCtrl(Arbol, pHeight:=Me.Height, pPorc:=35)
        'TabRiesgo
        Call Sub_Tama�oCtrl(Fra_Riesgo, pHeight:=Me.Height, pPorc:=20): Call Sub_Tama�oCtrl(Grilla, pHeight:=Me.Height, pPorc:=25)
        'TabAlias
        Call Sub_Tama�oCtrl(Fra_Alias, pHeight:=Me.Height, pPorc:=20): Call Sub_Tama�oCtrl(Grilla_Alias, pHeight:=Me.Height, pPorc:=25)
      End If
      
  End Select
End Sub

Private Sub Grilla_DblClick()
  If Grilla.Row > 0 Then
    Call Sub_EsperaVentana(GetCell(Grilla, Grilla.Row, "colum_pk"), _
                           GetCell(Grilla, Grilla.Row, "DSC_CLASIFICADOR_RIESGO"), _
                           GetCell(Grilla, Grilla.Row, "COD_VALOR_CLASIFICACION"), _
                           "U", _
                           Grilla.Row)
  End If
End Sub

Private Sub Sub_Agregar()
  
  If Not Fnt_ComboSelected_KEY(Cmb_Instrumento) = "" Then
    Call Sub_EsperaVentana(0, "", "", "I")
  Else
    MsgBox "Debe seleccionar un Instrumento antes de elegir Clasificadores de Riesgo.", vbExclamation, Me.Caption
  End If
  
End Sub

Private Sub Sub_EsperaVentana(pId_Clasificador_Riesgo As Double, _
                              pDsc_Clasificador_Riesgo As String, _
                              pValor_Clasificador As String, _
                              pTipo As String, _
                              Optional pLineaGrilla As Long)

Dim lForm As Frm_ClasifRiesgoNemotecnico
Dim lNombre As String
Dim lNom_Form As String
Dim lNewpId_Clasificador_Riesgo As Double
Dim lNewpDsc_Clasificador_Riesgo As String
Dim lNewValor_Clasificador As String
Dim lLinea As Long
Dim lExiste_Clasificador As Boolean
Dim lFila As Long
Dim lCod_Instrumento As String
  
  Me.Enabled = False
  lExiste_Clasificador = False
  lNom_Form = "Frm_ClasifRiesgoNemotecnico"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pId_Clasificador_Riesgo) Then
    lNombre = Me.Name
    lCod_Instrumento = Fnt_ComboSelected_KEY(Cmb_Instrumento)
    
    Set lForm = New Frm_ClasifRiesgoNemotecnico
    Call lForm.Fnt_Modificar(pId_Clasificador_Riesgo _
                           , pDsc_Clasificador_Riesgo _
                           , pValor_Clasificador _
                           , lCod_Instrumento _
                           , lNewpId_Clasificador_Riesgo _
                           , lNewpDsc_Clasificador_Riesgo _
                           , lNewValor_Clasificador _
                           , pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pId_Clasificador_Riesgo)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      If Not lNewpId_Clasificador_Riesgo = 0 Then
        
        If pTipo = "I" Then
          If Grilla.Rows > 1 Then
            For lFila = 1 To Grilla.Rows - 1
              If lNewpId_Clasificador_Riesgo = GetCell(Grilla, lFila, "colum_pk") Then
                lExiste_Clasificador = True
                Exit For
              End If
            Next
          End If
          If lExiste_Clasificador Then
            MsgBox "El Nemot�cnico no puede tener dos Valores de Clasificaci�n de Riesgo del tipo '" & _
                   GetCell(Grilla, lFila, "DSC_CLASIFICADOR_RIESGO") & "'.", vbExclamation, Me.Caption
          Else
            lLinea = Grilla.Rows
            Grilla.AddItem ""
            SetCell Grilla, lLinea, "colum_pk", lNewpId_Clasificador_Riesgo
            SetCell Grilla, lLinea, "DSC_CLASIFICADOR_RIESGO", lNewpDsc_Clasificador_Riesgo
            SetCell Grilla, lLinea, "COD_VALOR_CLASIFICACION", lNewValor_Clasificador
          End If
        ElseIf pTipo = "U" Then
          SetCell Grilla, pLineaGrilla, "colum_pk", lNewpId_Clasificador_Riesgo
          SetCell Grilla, pLineaGrilla, "DSC_CLASIFICADOR_RIESGO", lNewpDsc_Clasificador_Riesgo
          SetCell Grilla, pLineaGrilla, "COD_VALOR_CLASIFICACION", lNewValor_Clasificador
        End If
      End If
    End If
  End If
  Me.Enabled = True
End Sub

Private Function Fnt_Grabar_Clasificadores_Riesgo() As Boolean
Dim lFila As Long
Dim lcRel_Nemotec_Valor_Clasif As Class_Rel_Nemotec_Valor_Clasific

  Fnt_Grabar_Clasificadores_Riesgo = True
  
  Rem Valida que el instrumento tenga asociados los Clasificadores de Riesgo
  If Not Fnt_Valida_Clasif_Instrum Then
    GoTo ErrProcedure
  End If
  
  Rem Elimina todos los clasificadores de riesgo para el nemotecnico
  Set lcRel_Nemotec_Valor_Clasif = New Class_Rel_Nemotec_Valor_Clasific
  With lcRel_Nemotec_Valor_Clasif
    .Campo("Id_Nemotecnico").Valor = fKey
    If Not .Borrar Then
      MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  Set lcRel_Nemotec_Valor_Clasif = Nothing
  
  Rem Guarda todos los clasificador del nemotecnico
  If Grilla.Rows > 0 Then
    For lFila = 1 To Grilla.Rows - 1
      Set lcRel_Nemotec_Valor_Clasif = New Class_Rel_Nemotec_Valor_Clasific
      With lcRel_Nemotec_Valor_Clasif
        .Campo("Id_Nemotecnico").Valor = fKey
        'CAAL 09-2013 .Campo("Id_Clasificador_Riesgo").Valor = GetCell(Grilla, lFila, "colum_pk")
        .Campo("Cod_Valor_Clasificacion").Valor = GetCell(Grilla, lFila, "COD_VALOR_CLASIFICACION")
        If Not .Guardar Then
          MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
          GoTo ErrProcedure
        End If
      End With
      Set lcRel_Nemotec_Valor_Clasif = Nothing
    Next
  End If
  
  Exit Function
  
ErrProcedure:
  Fnt_Grabar_Clasificadores_Riesgo = False
  Set lcRel_Nemotec_Valor_Clasif = Nothing
  
End Function

Private Function Fnt_Grabar_Rel_Atributos() As Boolean
Dim lFila As Long
Dim lcRel_Nemotecnico_Atributo As Class_Rel_Nemotecnico_Atributo

  Fnt_Grabar_Rel_Atributos = True
 
  Rem Guarda todos los atributos del nemotecnico
  
  Set lcRel_Nemotecnico_Atributo = New Class_Rel_Nemotecnico_Atributo
  With lcRel_Nemotecnico_Atributo
    .Campo("Id_Nemotecnico").Valor = fKey
    .Campo("ISIM").Valor = Txt_Isim.Text
    .Campo("CUSIP").Valor = Txt_Cusip.Text
    .Campo("SEDOL").Valor = Txt_Sedol.Text
    .Campo("N_ACCIONESEMISOR").Valor = Txt_NumAccionesEmisor.Text
    If Fnt_FindValue4Display(Cmb_Productos, Cmb_Productos.Text) = gcPROD_RF_NAC Then
      .Campo("FECHA_PREPAGO").Valor = Txt_FechaPrepago.Value
    End If
    If Fnt_FindValue4Display(Cmb_Productos, Cmb_Productos.Text) = gcPROD_FFMM_NAC Then
      If Chk_CuentaPaso.Value = vbChecked Then
        .Campo("NEMO_CTAPASO").Valor = "S"
      Else
        .Campo("NEMO_CTAPASO").Valor = "N"
      End If
    Else
      .Campo("NEMO_CTAPASO").Valor = "N"
    End If
    .Campo("TICKER").Valor = Txt_Ticker.Text
    .Campo("COD_ACHS").Valor = Txt_CodAchs.Text
    .Campo("ID_INSTRUMENTO_EXT").Valor = IIf(Fnt_ComboSelected_KEY(Cmb_InstrumentoSVS) <> "", Fnt_ComboSelected_KEY(Cmb_InstrumentoSVS), "")
    
    If Not .Guardar Then
      MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  Set lcRel_Nemotecnico_Atributo = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Grabar_Rel_Atributos = False
  Set lcRel_Nemotecnico_Atributo = Nothing
  
End Function

Private Function Fnt_Valida_Clasif_Instrum() As Boolean
Dim lFila As Long
Dim lReg As hFields
Dim lCod_Instrumento As String
Dim lExiste_Rel_Instrum_Clasif As Boolean
Dim lcClasificadores_Riesgo As Class_Clasificadores_Riesgo
  
  Fnt_Valida_Clasif_Instrum = True
  lExiste_Rel_Instrum_Clasif = False
  With Grilla
    If .Rows > 1 Then
      lCod_Instrumento = Fnt_ComboSelected_KEY(Cmb_Instrumento)
      For lFila = 1 To .Rows - 1
        Set lcClasificadores_Riesgo = New Class_Clasificadores_Riesgo
        With lcClasificadores_Riesgo
          .Campo("Id_Clasificador_Riesgo").Valor = GetCell(Grilla, lFila, "colum_pk")
          If .Buscar_Instru_Clasif_Riesgo Then
            For Each lReg In .Cursor
              If lCod_Instrumento = lReg("COD_INSTRUMENTO").Value Then
                lExiste_Rel_Instrum_Clasif = True
                Exit For
              End If
            Next
            If Not lExiste_Rel_Instrum_Clasif Then
              MsgBox "El Clasificador de Riesgo '" & GetCell(Grilla, lFila, "DSC_CLASIFICADOR_RIESGO") & _
                     "' no est� asociado al Instrumento '" & Cmb_Instrumento.Text & "'.", vbExclamation, Me.Caption
              Fnt_Valida_Clasif_Instrum = False
              Exit For
            End If
          Else
            MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            Fnt_Valida_Clasif_Instrum = False
            Exit For
          End If
        End With
        Set lcClasificadores_Riesgo = Nothing
      Next
    End If
  End With

End Function

Private Sub CargaDias()
  With Dias_Liquidez
    Call .AddItem("0")
    Call .AddItem("1")
    Call .AddItem("2")
    Call .AddItem("3")
    Call .AddItem("4")
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem("0", "0")
      .Add Fnt_AgregaValueItem("1", "1")
      .Add Fnt_AgregaValueItem("2", "2")
      .Add Fnt_AgregaValueItem("3", "3")
      .Add Fnt_AgregaValueItem("4", "4")
      .Translate = True
    End With
    
    .Text = ""
    .BackColor = fColorOBligatorio
  End With
End Sub

Private Sub CargaDecimales()
  With CmbDecimalesTabla
    Call .AddItem("1")
    Call .AddItem("2")
    Call .AddItem("3")
    Call .AddItem("4")
    Call .AddItem("5")
    Call .AddItem("6")
    Call .AddItem("7")
    Call .AddItem("8")
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem("1", "1")
      .Add Fnt_AgregaValueItem("2", "2")
      .Add Fnt_AgregaValueItem("3", "3")
      .Add Fnt_AgregaValueItem("4", "4")
      .Add Fnt_AgregaValueItem("5", "5")
      .Add Fnt_AgregaValueItem("6", "6")
      .Add Fnt_AgregaValueItem("7", "7")
      .Add Fnt_AgregaValueItem("8", "8")
      .Translate = True
    End With
    
    .Text = ""
    .BackColor = fColorOBligatorio
  End With
End Sub

Private Sub CargaPeriodicidad()
  With Cmb_Periodicidad
    Call .AddItem("1")
    Call .AddItem("2")
    Call .AddItem("3")
    Call .AddItem("4")
    Call .AddItem("5")
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem("1", "3")
      .Add Fnt_AgregaValueItem("2", "6")
      .Add Fnt_AgregaValueItem("3", "12")
      .Add Fnt_AgregaValueItem("4", "24")
      .Add Fnt_AgregaValueItem("5", "36")
      .Translate = True
    End With
    
    .Text = ""
    .BackColor = fColorOBligatorio
  End With
End Sub

Private Sub Txt_Base_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", "-" y "backspace"
  If Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Txt_CorteMinimo_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", "-" y "backspace"
  If Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Sub_CargarAlias()
Dim lcTipo_Conversion As Object
Dim lcRel_Conversiones As Object
Dim lReg As hFields
'----------------------------------------------
Dim lLinea As Long
Dim lId_Tipo_Conversion As Double

  Set lcTipo_Conversion = Fnt_CreateObject(cDLL_Tipos_Conversion)
  With lcTipo_Conversion
    Set .gDB = gDB
    .Campo("tabla").Valor = cTabla_Nemotecnicos
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas en la carga de los alias." _
                        , .ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      MsgBox "No esta definido el tipo de conversion para " & cTabla_Nemotecnicos & ".", vbInformation, Me.Caption
      GoTo ExitProcedure
    End If
    
    lId_Tipo_Conversion = .Cursor(1)("id_tipo_conversion").Value
  End With

  Set lcRel_Conversiones = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversiones
    Set .gDB = gDB
    .Campo("id_tipo_conversion").Valor = lId_Tipo_Conversion
    .Campo("id_entidad").Valor = fKey
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas en la carga de los alias." _
                        , .ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  
  For Each lReg In lcRel_Conversiones.Cursor
    lLinea = Grilla_Alias.FindRow(Item:=lReg("id_origen").Value _
                                , Col:=Grilla_Alias.ColIndex("colum_pk"))
    If Not lLinea = cNewEntidad Then
      Call SetCell(Grilla_Alias, lLinea, "valor", lReg("valor").Value)
    End If
  Next

ExitProcedure:
  Set lcTipo_Conversion = Nothing
  Set lcRel_Conversiones = Nothing
End Sub

Private Function Fnt_Grabar_Alias() As Boolean
Dim lcTipo_Conversion As Object
Dim lcRel_Conversiones As Object
'--------------------------------------------
Dim lId_Tipo_Conversion As Double
Dim lLinea              As Long

Const cMsgError = "Problemas en la guardar de los alias."

  Fnt_Grabar_Alias = False

  Set lcTipo_Conversion = Fnt_CreateObject(cDLL_Tipos_Conversion)
  With lcTipo_Conversion
    Set .gDB = gDB
    .Campo("tabla").Valor = cTabla_Nemotecnicos
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG _
                        , cMsgError _
                        , .ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      MsgBox "No esta definido el tipo de conversion para " & cTabla_Nemotecnicos & ".", vbInformation, Me.Caption
      GoTo ExitProcedure
    End If
    
    lId_Tipo_Conversion = .Cursor(1)("id_tipo_conversion").Value
  End With

  
  Set lcRel_Conversiones = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversiones
    Set .gDB = gDB
    .Campo("id_tipo_conversion").Valor = lId_Tipo_Conversion
    .Campo("id_entidad").Valor = fKey
    If Not .Borrar Then
      Call Fnt_MsgError(.SubTipo_LOG _
                        , cMsgError _
                        , .ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  
  For lLinea = 1 To Grilla_Alias.Rows - 1
    If Not GetCell(Grilla_Alias, lLinea, "valor") = "" Then
      Set lcRel_Conversiones = Fnt_CreateObject(cDLL_Rel_Conversiones)
      With lcRel_Conversiones
        Set .gDB = gDB
        .Campo("Id_Origen").Valor = GetCell(Grilla_Alias, lLinea, "colum_pk")
        .Campo("id_tipo_conversion").Valor = lId_Tipo_Conversion
        .Campo("Id_Entidad").Valor = fKey
        .Campo("Valor").Valor = GetCell(Grilla_Alias, lLinea, "valor")
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG _
                            , cMsgError _
                            , .ErrMsg _
                            , pConLog:=True)
          GoTo ExitProcedure
        End If
      End With
    End If
    Set lcRel_Conversiones = Nothing
  Next

  Fnt_Grabar_Alias = True

ExitProcedure:
  Set lcTipo_Conversion = Nothing
  Set lcRel_Conversiones = Nothing
End Function
Private Function Fnt_Guarda_Serie_Tabla_Desarrollo() As Boolean
'   Dim lcEntidad As Class_Entidad
   Dim lcTablaSerie As New Class_Tabla_Series
   Dim lcTablaDesa As New Class_Tabla_Desarrollo
   Dim lReg As hFields
   Dim lId_Emisor_Especifico As String
   Dim lId_Moneda As String
   Dim lCodSerie As String
   Dim Row As Long

  Fnt_Guarda_Serie_Tabla_Desarrollo = False

    If Not Fnt_Valida_Serie_Tabla Then
        Exit Function
    End If
 
'    If Fnt_ComboSelected_KEY(Cmb_Productos) = gcPROD_RF_INT Then  '08/09 COG
'        Fnt_Guarda_Serie_Tabla_Desarrollo = True
'        Exit Function
'    End If
  
    Set lcTablaSerie = New Class_Tabla_Series
    With lcTablaSerie
        .Campo("idserie").Valor = fIdSerie
        .Campo("codserie").Valor = Txt_Nemotecnico.Text
        .Campo("tipoamortizacion").Valor = "D"
        .Campo("tasaemision").Valor = Txt_TasaEmision.Text '(instrumentos)
        .Campo("tasaefectiva").Valor = TxtTasaEfectiva.Text
        .Campo("fechaemision").Valor = DTP_Fecha_Emision.Value '(instrumentos)
        .Campo("fechavcto").Valor = Txt_FechaVencimiento.Value '(instrumentos)
        .Campo("nrocupones").Valor = txtNroCupones.Text
        .Campo("nroamortizaciones").Valor = txtNroAmortizaciones.Text
        .Campo("diavctocupon").Valor = txtDiaCorteCupon.Text
        .Campo("periodocupon").Valor = Cmb_Periodicidad.Text '(instrumentos)
        .Campo("anos").Valor = txtPlazoAnos.Text
        .Campo("decimalestd").Valor = CmbDecimalesTabla.Text
        .Campo("fechaprimercorte").Valor = dtpPrimerVmto.Value
        .Campo("corteminimo").Valor = Txt_CorteMinimo.Text '(instrumentos)
        .Campo("idemisor").Valor = 1 'lId_Emisor_Especifico
        .Campo("idmoneda").Valor = Fnt_ComboSelected_KEY(cmbMonedaSerie)
        .Campo("idfamilia").Valor = Fnt_ComboSelected_KEY(cmbFamilia)
        .Campo("seriebanco").Valor = txtSerieBanco.Text
  
        If Not .Guardar Then
            Call Fnt_MsgError(.SubTipo_LOG, "Problemas en grabar Serie.", .ErrMsg, pConLog:=True)
            Fnt_Guarda_Serie_Tabla_Desarrollo = False
            GoTo ErrProcedure
        End If
        fIdSerie = .Campo("idserie").Valor
    
        .LimpiaParam
        .Campo("idserie").Valor = fIdSerie
    
        If Not .Buscar Then
          Call Fnt_MsgError(.SubTipo_LOG, "Problemas en grabar Serie.", .ErrMsg, pConLog:=True)
          Fnt_Guarda_Serie_Tabla_Desarrollo = False
          GoTo ErrProcedure
        End If
    
        If .Cursor.Count > 0 Then
          lCodSerie = .Cursor(1)("codserie").Value
        End If
    End With
    Set lcTablaSerie = Nothing
    
    Set lcTablaDesa = New Class_Tabla_Desarrollo
    With lcTablaDesa
        .Campo("idserie").Valor = fIdSerie
        If Not .Borrar Then
            Call Fnt_MsgError(.SubTipo_LOG, "Problemas en Limpiar Cupones.", .ErrMsg, pConLog:=True)
            Fnt_Guarda_Serie_Tabla_Desarrollo = False
            GoTo ErrProcedure
        End If
     
   
        For Row = 1 To Grilla_Cupones.Rows - 1
        '    .LimpiaParam
            .Campo("idcupones").Valor = GetCell(Grilla_Cupones, Row, "idcupon")
            .Campo("idserie").Valor = fIdSerie
            .Campo("codserie").Valor = lCodSerie
            .Campo("nrocupon").Valor = GetCell(Grilla_Cupones, Row, "NroCupon")
            .Campo("fechacupon").Valor = GetCell(Grilla_Cupones, Row, "fechacupon")
            .Campo("interes").Valor = GetCell(Grilla_Cupones, Row, "Interes")
            .Campo("capital").Valor = GetCell(Grilla_Cupones, Row, "capital")
            .Campo("flujo").Valor = GetCell(Grilla_Cupones, Row, "flujo")
            .Campo("saldo").Valor = GetCell(Grilla_Cupones, Row, "saldo")
           
            If Not .Guardar Then
                Call Fnt_MsgError(.SubTipo_LOG, "Problemas en grabar Tabla Desarrollo. ", .ErrMsg, pConLog:=True)
                Fnt_Guarda_Serie_Tabla_Desarrollo = False
                GoTo ErrProcedure
            End If
       
            fIdCupones = .Campo("idcupones").Valor
            SetCell Grilla_Cupones, Row, "IdCupon", fIdCupones
        Next
    End With
  
    'gDB.Parametros.Clear
    'Set lcEntidad = Nothing
  
  
    Fnt_Guarda_Serie_Tabla_Desarrollo = True
  
ErrProcedure:
   Set lcTablaSerie = Nothing
   Set lcTablaDesa = Nothing
End Function


Private Sub Sub_Carga_Clases_Instrumento()
Dim lcArbol_Clase_Instrumento As Class_Arbol_Clase_Instrumento
Dim lReg                      As hFields
Dim lcNode                     As Node
'---------------------------------------------------------------
Dim lPadre

  Arbol.Nodes.Clear
'  Set lcNode = Arbol.Nodes.Add(Key:=cRoot _
'                             , Text:="Raiz")
'  lcNode.Expanded = True
  
  Rem Carga el Arbol de clasificaciones de instrumentos
  Set lcArbol_Clase_Instrumento = New Class_Arbol_Clase_Instrumento
  lcArbol_Clase_Instrumento.Campo("id_empresa").Valor = Fnt_EmpresaActual
  lcArbol_Clase_Instrumento.Campo("ID_ACI_TIPO").Valor = sTipoDeArbol
  If Not lcArbol_Clase_Instrumento.Buscar Then
    Call Fnt_MsgError(lcArbol_Clase_Instrumento.SubTipo_LOG _
                    , "Problemas al buscar." _
                    , lcArbol_Clase_Instrumento.ErrMsg _
                    , pConLog:=True)
    GoTo ExitProcedure
  End If
  
  For Each lReg In lcArbol_Clase_Instrumento.Cursor
    If IsNull(lReg("ID_PADRE_ARBOL_CLASE_INST").Value) Then
      Set lcNode = Arbol.Nodes.Add(Key:=cPrefijo & lReg("ID_ARBOL_CLASE_INST").Value _
                               , Text:=lReg("DSC_ARBOL_CLASE_INST").Value)
    Else
      lPadre = cPrefijo & lReg("ID_PADRE_ARBOL_CLASE_INST").Value
    
      Set lcNode = Arbol.Nodes.Add(relative:=lPadre _
                               , relationship:=tvwChild _
                               , Key:=cPrefijo & lReg("ID_ARBOL_CLASE_INST").Value _
                               , Text:=lReg("DSC_ARBOL_CLASE_INST").Value)
    End If
    
    'lNode.Bold = True
    'lNode.Image = "close_folder"
    lcNode.Expanded = True
  Next
  Set lcArbol_Clase_Instrumento = Nothing

ExitProcedure:
  Set lcArbol_Clase_Instrumento = Nothing
End Sub


Private Function Fnt_Carga_Rel_ACI_Emp_Nemo() As Boolean
Dim lcRel_ACI_Emp_Nemotecnico As Class_Rel_ACI_Emp_Nemotecnico

  Call Sub_LimpiarCheckArbol(Arbol)

  Fnt_Carga_Rel_ACI_Emp_Nemo = False

  Set lcRel_ACI_Emp_Nemotecnico = New Class_Rel_ACI_Emp_Nemotecnico
  With lcRel_ACI_Emp_Nemotecnico
    .Campo("id_nemotecnico").Valor = fKey
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    .Campo("ID_ACI_TIPO").Valor = sTipoDeArbol
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG _
                      , "Problemas al buscar la relaci�n de la clase con el nemot�cnico." _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
      
    If .Cursor.Count > 0 Then
      On Error Resume Next
      Arbol.Nodes(cPrefijo & .Cursor(1)("ID_ARBOL_CLASE_INST").Value).Checked = True
    End If
  End With
  
  Fnt_Carga_Rel_ACI_Emp_Nemo = True
  
ExitProcedure:
  Set lcRel_ACI_Emp_Nemotecnico = Nothing
End Function


Private Function Fnt_Guarda_Rel_ACI_Emp_Nemo() As Boolean
Dim lcRel_ACI_Emp_Nemotecnico As Class_Rel_ACI_Emp_Nemotecnico
Dim lcNodo As Node
'------------------------------------------------------------------
Dim lId_Arbol_Clase_Inst

  Fnt_Guarda_Rel_ACI_Emp_Nemo = False

  Set lcRel_ACI_Emp_Nemotecnico = New Class_Rel_ACI_Emp_Nemotecnico
  With lcRel_ACI_Emp_Nemotecnico
    .Campo("id_nemotecnico").Valor = fKey
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    .Campo("ID_ACI_TIPO").Valor = sTipoDeArbol
    If Not .Borrar Then
      Call Fnt_MsgError(.SubTipo_LOG _
                      , "Problemas al eliminar la relaci�n de la clase con el nemot�cnico." _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  Set lcRel_ACI_Emp_Nemotecnico = Nothing
  
  lId_Arbol_Clase_Inst = Null
  For Each lcNodo In Arbol.Nodes
    If lcNodo.Checked Then
      lId_Arbol_Clase_Inst = Mid(lcNodo.Key, 2)
      Exit For
    End If
  Next

  If Not IsNull(lId_Arbol_Clase_Inst) Then
    Set lcRel_ACI_Emp_Nemotecnico = New Class_Rel_ACI_Emp_Nemotecnico
    With lcRel_ACI_Emp_Nemotecnico
      .Campo("id_nemotecnico").Valor = fKey
      .Campo("id_empresa").Valor = Fnt_EmpresaActual
      .Campo("ID_ARBOL_CLASE_INST").Valor = lId_Arbol_Clase_Inst
      .Campo("ID_ACI_TIPO").Valor = sTipoDeArbol
      If Not .Guardar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                        , "Problemas al guardar la relaci�n de la clase con el nemot�cnico." _
                        , .ErrMsg _
                        , pConLog:=True)
        GoTo ExitProcedure
      End If
    End With
  End If
  Set lcRel_ACI_Emp_Nemotecnico = Nothing

  Fnt_Guarda_Rel_ACI_Emp_Nemo = True

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en el proceso de guardar la relaci�n de la clase con el nemot�cnico.", Err.Description)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcRel_ACI_Emp_Nemotecnico = Nothing
End Function





Private Sub Txt_Nemotecnico_KeyPress(KeyAscii As Integer)
    KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Nemotecnico_Validate(Cancel As Boolean)
Dim lcNemotecnicos As Class_Nemotecnicos
  
  If fKey = cNewEntidad And Not Trim(Txt_Nemotecnico.Text) = "" Then
    Set lcNemotecnicos = New Class_Nemotecnicos
    With lcNemotecnicos
      .Campo("nemotecnico").Valor = Txt_Nemotecnico.Text
       
      If Not .Buscar Then
        Call Fnt_MsgError(.SubTipo_LOG, "Problemas al buscar el Nemot�cnico.", .ErrMsg, pConLog:=True)
        GoTo ErrProcedure
      End If
      
      If .Cursor.Count > 0 Then
        MsgBox "El Nemot�cnico " & Txt_Nemotecnico.Text & " ya existe en el sistema.", vbInformation + vbOKOnly, Me.Caption
        Cancel = True
      End If
    End With
  End If
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la busqueda de duplicidad del nemot�cnico (" & Txt_Nemotecnico.Text & ")." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcNemotecnicos = Nothing
End Sub

Private Sub Sub_Tama�oCtrl(Ctrl As Object, Optional pWidth, Optional pHeight, Optional pPorc As Double)

  If IsMissing(pPorc) Then
      pPorc = 0
  End If
  With Ctrl
    If Not IsMissing(pWidth) Then
      .Width = pWidth - (pWidth * (pPorc / 100))
    End If
    If Not IsMissing(pHeight) Then
      .Height = pHeight - (pHeight * (pPorc / 100))
    End If
  End With
  
End Sub
