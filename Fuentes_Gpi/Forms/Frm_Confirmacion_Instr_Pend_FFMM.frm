VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Confirmacion_Instr_Pend_FFMM 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Confirmaci�n Instrucciones Pendientes FFMM"
   ClientHeight    =   6450
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   10575
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6450
   ScaleWidth      =   10575
   Begin VB.Frame Frame1 
      Caption         =   "Confirmaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4515
      Left            =   60
      TabIndex        =   6
      Top             =   1860
      Width           =   10425
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   4125
         Left            =   120
         TabIndex        =   7
         Top             =   270
         Width           =   9495
         _cx             =   16748
         _cy             =   7276
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   12
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Confirmacion_Instr_Pend_FFMM.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Grilla 
         Height          =   660
         Left            =   9780
         TabIndex        =   14
         Top             =   480
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_ALL"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Selecciona todos los items"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_NOTHING"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Deselecciona todos los items"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   255
            Left            =   9420
            TabIndex        =   15
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
   End
   Begin VB.Frame Frame_Filtros 
      Caption         =   "Filtros de B�squedas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1425
      Left            =   60
      TabIndex        =   0
      Top             =   390
      Width           =   10425
      Begin VB.Frame Frame_Actuliza_Cuota 
         Caption         =   "Actualizar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1155
         Left            =   3720
         TabIndex        =   9
         Top             =   150
         Width           =   6585
         Begin TrueDBList80.TDBCombo Cmb_Nemotecnico 
            Height          =   345
            Left            =   1170
            TabIndex        =   10
            Top             =   270
            Width           =   2565
            _ExtentX        =   4524
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Confirmacion_Instr_Pend_FFMM.frx":022A
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin hControl2.hTextLabel Txt_Valor_Cuota 
            Height          =   345
            Left            =   120
            TabIndex        =   12
            Top             =   660
            Width           =   2550
            _ExtentX        =   4498
            _ExtentY        =   609
            LabelWidth      =   1035
            Caption         =   "Valor Cuota"
            Text            =   ""
            Tipo_TextBox    =   1
            Alignment       =   1
            MaxLength       =   19
         End
         Begin MSComctlLib.Toolbar Toolbar_Actualiza_Cuota 
            Height          =   330
            Left            =   4350
            TabIndex        =   13
            Top             =   690
            Width           =   2040
            _ExtentX        =   3598
            _ExtentY        =   582
            ButtonWidth     =   3413
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Actualiza Valor Cuota"
                  Key             =   "UPDATE"
                  Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
                  Object.ToolTipText     =   "Actualiza el Valor Cuota de Nemot�cnicos en la grilla"
               EndProperty
            EndProperty
         End
         Begin VB.Label lbl_nemotecnicos 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Nemot�cnico"
            Height          =   330
            Left            =   120
            TabIndex        =   11
            Top             =   270
            Width           =   1035
         End
      End
      Begin TrueDBList80.TDBCombo Cmb_Tipo_Movimiento 
         Height          =   345
         Left            =   1410
         TabIndex        =   1
         Tag             =   "OBLI=S;CAPTION=Tipo Operaci�n"
         Top             =   330
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Confirmacion_Instr_Pend_FFMM.frx":02D4
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Fecha 
         Height          =   345
         Left            =   1410
         TabIndex        =   8
         Tag             =   "OBLI=S;CAPTION=Fecha"
         Top             =   750
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Confirmacion_Instr_Pend_FFMM.frx":037E
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Lbl_Tipo_Movimiento 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tipo Movimiento"
         Height          =   330
         Left            =   150
         TabIndex        =   3
         Top             =   330
         Width           =   1245
      End
      Begin VB.Label lbl_Fecha 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         Height          =   345
         Left            =   150
         TabIndex        =   2
         Top             =   750
         Width           =   1245
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   10575
      _ExtentX        =   18653
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Buscar"
            Key             =   "RESEARCH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Confirmar"
            Key             =   "CONF"
            Object.ToolTipText     =   "Confirma las instrucciones pendientes"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   8010
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Confirmacion_Instr_Pend_FFMM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fDsc_Ingreso As String
Dim fDsc_Egreso As String

Dim fNemotecnicos As hRecord

Const fc_Mercado = "N"

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("RESEARCH").Image = "boton_grilla_buscar"
    .Buttons("CONF").Image = cBoton_Grabar
    .Buttons("REFRESH").Image = cBoton_Refrescar
    .Buttons("PRINTER").Image = cBoton_Imprimir
    .Buttons("EXIT").Image = cBoton_Salir
  End With

  With Toolbar_Actualiza_Cuota
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("UPDATE").Image = cBoton_Modificar
  End With
  
  With Toolbar_Grilla
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With

  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
Dim lcTipo_Operacion As Class_Tipos_Operaciones

  Call Sub_FormControl_Color(Me.Controls)

  '------------------------------
  ' Carga Combo Tipo_Movimiento
  '------------------------------
  Set lcTipo_Operacion = New Class_Tipos_Operaciones
  With lcTipo_Operacion
    .Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Instruccion
    If .Buscar Then
      If .Cursor.Count > 0 Then
        fDsc_Ingreso = .Cursor(1)("DSC_TIPO_MOV_INGRESO").Value
        fDsc_Egreso = .Cursor(1)("DSC_TIPO_MOV_EGRESO").Value
      End If
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Tipos de Movimiento", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcTipo_Operacion = Nothing
  
  With Cmb_Tipo_Movimiento
    Call .AddItem("Todos")
    Call .AddItem(fDsc_Ingreso)
    Call .AddItem(fDsc_Egreso)

    Call .Columns.Remove(1)

    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem(cCmbKALL, "Todos")
      .Add Fnt_AgregaValueItem(gcTipoOperacion_Ingreso, fDsc_Ingreso)
      .Add Fnt_AgregaValueItem(gcTipoOperacion_Egreso, fDsc_Egreso)
      .Translate = True
    End With
  End With
  
  '------------------------------
  ' Setea Combo Nemotecnico
  '------------------------------
  With Cmb_Nemotecnico
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
    End With
  End With
  
  '------------------------------
  ' Carga Combo Fechas
  '------------------------------
  Call Sub_CargaCombo_Fechas
  
  '------------------------------
  ' Setea la coleccion de Nemotecnicos que van a ir en el combo nemotecnicos
  '------------------------------
  Set fNemotecnicos = New hRecord
  With fNemotecnicos
    .AddField "id_nemotecnico"
    .AddField "nemotecnico"
  End With
  
  Call Sub_Limpiar
  
End Sub

Private Sub Sub_CargaCombo_Fechas()
Dim lcOperaciones As Class_Operaciones
Dim lReg As hFields
  
  With Cmb_Fecha
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
    End With
    
    Set lcOperaciones = New Class_Operaciones
    lcOperaciones.Campo("cod_estado").Valor = cCod_Estado_Pendiente
    lcOperaciones.Campo("cod_producto").Valor = gcPROD_FFMM_NAC
    lcOperaciones.Campo("cod_tipo_operacion").Valor = gcOPERACION_Instruccion
    If lcOperaciones.Fnt_Buscar_Fechas_Operaciones Then
      For Each lReg In lcOperaciones.Cursor
        
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
        
        .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("fecha_operacion").Value, lReg("fecha_operacion").Value)
        
        Call .AddItem(lReg("fecha_operacion").Value)
        
      Next
    Else
      Call Fnt_MsgError(lcOperaciones.SubTipo_LOG, _
                        "Problemas en cargar combo Fecha.", _
                        lcOperaciones.ErrMsg, _
                        pConLog:=True)
    End If
  End With
  
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_AfterEdit(ByVal Row As Long, ByVal Col As Long)
Dim lNumero_Cuota As Double
Dim lValor_Cuota As Double
Dim lMonto As Double

  With Grilla
    If Col = .ColIndex("chk") Then
      Call Sub_Cambia_Chk(GetCell(Grilla, Row, "colum_pk"), Row)
    ElseIf Col = .ColIndex("valor_cuota") Then
      If Not GetCell(Grilla, Row, "Valor_Cuota") = "" Then
        lValor_Cuota = GetCell(Grilla, Row, "Valor_Cuota")
      Else
        MsgBox "No se puede ingresar un Valor Cuota vacio.", vbCritical, Me.Caption
        Call SetCell(Grilla, Row, "Cuotas", 0)
        Exit Sub
      End If
      
      lMonto = GetCell(Grilla, Row, "Monto_Detalle")
      
      Call Sub_Cambia_Cuotas(GetCell(Grilla, Row, "id_moneda_operacion"), _
                             lNumero_Cuota, _
                             lValor_Cuota, _
                             lMonto)
      
      Call SetCell(Grilla, Row, "Cuotas", lNumero_Cuota)
      Call SetCell(Grilla, Row, "valor_cuota", lValor_Cuota)
    End If
  End With
End Sub

Private Sub Sub_Cambia_Chk(pId_Operacion As String, lFila_Actual As Long)
Dim lFila As Long

  With Grilla
    For lFila = 1 To .Rows - 1
      If Not lFila_Actual = lFila Then
        If pId_Operacion = GetCell(Grilla, lFila, "colum_pk") Then
          If .Cell(flexcpChecked, lFila, "chk") = flexChecked Then
            .Cell(flexcpChecked, lFila, "chk") = flexUnchecked
          ElseIf .Cell(flexcpChecked, lFila, "chk") = flexUnchecked Then
            .Cell(flexcpChecked, lFila, "chk") = flexChecked
          End If
        End If
      End If
    Next
  End With
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  Select Case Col
    Case Grilla.ColIndex("chk"), Grilla.ColIndex("valor_cuota")
      Cancel = False
    Case Else
      Cancel = True
  End Select
End Sub

Private Sub Toolbar_Actualiza_Cuota_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  If Button.Key = "UPDATE" Then
    Call Sub_Actualiza_Valor_Cuota
  End If
End Sub

Private Sub Sub_Actualiza_Valor_Cuota()
Dim lValor_Cuota As Double
Dim lNumero_Cuota As Double
Dim lMonto As Double
Dim lId_Nemotecnico As String
Dim lFila As Long
  
  Call Sub_Bloquea_Puntero(Me)
  
  lId_Nemotecnico = Fnt_ComboSelected_KEY(Cmb_Nemotecnico)
  lValor_Cuota = To_Number(Txt_Valor_Cuota.Text)
  lNumero_Cuota = 0
  lMonto = 0
  
  If lId_Nemotecnico = "" Then
    MsgBox "Debe seleccionar un Nemot�cnico.", vbInformation, Me.Caption
    Cmb_Nemotecnico.SetFocus
    GoTo ErrProcedure
  End If
  
  If lValor_Cuota = 0 Then
    MsgBox "El Valor Cuota debe ser mayor a cero.", vbInformation, Me.Caption
    Txt_Valor_Cuota.SetFocus
    GoTo ErrProcedure
  End If
  
  For lFila = 1 To Grilla.Rows - 1
    If lId_Nemotecnico = GetCell(Grilla, lFila, "id_nemotecnico") Then
      lMonto = GetCell(Grilla, lFila, "monto_detalle")
      
      Call Sub_Cambia_Cuotas(GetCell(Grilla, lFila, "id_moneda_operacion"), _
                             lNumero_Cuota, _
                             lValor_Cuota, _
                             lMonto)
      
      Call SetCell(Grilla, lFila, "valor_cuota", lValor_Cuota)
      Call SetCell(Grilla, lFila, "cuotas", lNumero_Cuota)
    End If
  Next
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "RESEARCH"
      Call Sub_CargarDatos
    Case "CONF"
      Call Sub_Confirmacion
    Case "REFRESH"
      Call Sub_CargaCombo_Fechas
      Call Sub_Limpiar
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Limpiar()

  Grilla.Rows = 1
  
  Call Sub_ComboSelectedItem(Cmb_Tipo_Movimiento, cCmbKALL)
  
  Cmb_Nemotecnico.Clear
  Call Sub_ComboSelectedItem(Cmb_Nemotecnico, "")
  
  Txt_Valor_Cuota.Text = ""
  
End Sub

Private Sub Sub_CargarDatos()
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lcOperaciones As Class_Operaciones
Dim lFecha As String
Dim lFlg_Tipo_Movimiento As String

  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    Exit Sub
  End If
  
  Call Sub_Bloquea_Puntero(Me)
  
  Grilla.Rows = 1
  
  lFlg_Tipo_Movimiento = IIf(Fnt_ComboSelected_KEY(Cmb_Tipo_Movimiento) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Tipo_Movimiento))
  lFecha = IIf(Fnt_ComboSelected_KEY(Cmb_Fecha) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Fecha))
  
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Cod_Instrumento").Valor = ""
    .Campo("cod_producto").Valor = gcPROD_FFMM_NAC
    .Campo("cod_tipo_operacion").Valor = gcOPERACION_Instruccion
    .Campo("cod_estado").Valor = cCod_Estado_Pendiente
    .Campo("Id_Operacion").Valor = ""
    .Campo("Flg_Tipo_Movimiento").Valor = lFlg_Tipo_Movimiento
    If .Fnt_Buscar_Operaciones(lFecha, lFecha, "", "") Then
      For Each lReg In .Cursor
        If Not Fnt_Inserta_Grilla(lReg("id_operacion").Value, _
                                  lReg("ID_CUENTA").Value, _
                                  lReg("NUM_CUENTA").Value, _
                                  lReg("flg_tipo_movimiento").Value, _
                                  lReg("ID_MONEDA_OPERACION").Value) Then
          Exit For
        End If
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Operaciones.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  
  Call Sub_Carga_Combo_Nemotecnico
  
  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Fnt_Inserta_Grilla(pId_Operacion As String, _
                                    pId_Cuenta As String, _
                                    pNum_Cuenta, _
                                    pFlg_Tipo_Movimiento As String, _
                                    pId_Moneda As String) As Boolean
Dim lcOperaciones As Class_Operaciones
Dim lcDetalle As Class_Operaciones_Detalle
Dim lcNemotecnico As Class_Nemotecnicos
Dim lcPublicadores_Precio As Class_Publicadores_Precio
'------------------------------------------------------
Dim lReg As hCollection.hFields
Dim lLinea As Long
'----------------------------------------------------
Dim lNemotecnico As String
Dim lTipo_Cuota As String
Dim lFecha As Date
Dim lValor_Cuota As Double
Dim lNumero_Cuotas As Double
Dim lMonto_Detalle As Double
Dim lFormto_Fecha As String
Dim lTipo_Movimiento As String
  
  Fnt_Inserta_Grilla = True
  
  Select Case pFlg_Tipo_Movimiento
    Case gcTipoOperacion_Ingreso
      lTipo_Movimiento = fDsc_Ingreso
    Case gcTipoOperacion_Egreso
      lTipo_Movimiento = fDsc_Egreso
    Case Else
      lTipo_Movimiento = ""
  End Select
  
  Set lcOperaciones = New Class_Operaciones
  lcOperaciones.Campo("Id_Operacion").Valor = pId_Operacion
  
  If lcOperaciones.BuscaConDetalles Then
  
    For Each lcDetalle In lcOperaciones.Detalles
    
      Set lcNemotecnico = New Class_Nemotecnicos
      With lcNemotecnico
        .Campo("id_nemotecnico").Valor = lcDetalle.Campo("Id_Nemotecnico").Valor
        If .Buscar Then
          For Each lReg In .Cursor
            lNemotecnico = lReg("NEMOTECNICO").Value
            If pFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
              lTipo_Cuota = NVL(lReg("FLG_TIPO_CUOTA_INGRESO").Value, "")
            ElseIf pFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
              lTipo_Cuota = NVL(lReg("FLG_TIPO_CUOTA_EGRESO").Value, "")
            Else
              lTipo_Cuota = ""
            End If
          Next
        End If
      End With
      Set lcNemotecnico = Nothing
      
      If lTipo_Cuota = cTCuota_Conocida Then
        lFecha = Fnt_String2Date(Cmb_Fecha.Text) - 1
      ElseIf lTipo_Cuota = cTCuota_Desconocida Then
        lFecha = Fnt_String2Date(Cmb_Fecha.Text)
      End If
      
      lValor_Cuota = 0
      Set lcPublicadores_Precio = New Class_Publicadores_Precio
      With lcPublicadores_Precio
        .Campo("Id_Nemotecnico").Valor = lcDetalle.Campo("Id_Nemotecnico").Valor
        .Campo("fecha").Valor = lFecha
        If .Buscar_Ultimo_Cta_Nemotecnico(pId_Cuenta) Then
          lValor_Cuota = NVL(.Campo("precio").Valor, 0)
        Else
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en carga de Precio de Publicadores.", _
                            .ErrMsg, _
                            pConLog:=True)
        End If
      End With
      Set lcPublicadores_Precio = Nothing
      
      lMonto_Detalle = lcDetalle.Campo("Monto_Pago").Valor
      
      Call Sub_Cambia_Cuotas(lcDetalle.Campo("Id_MONEDA_PAGO").Valor, _
                             lNumero_Cuotas, _
                             lValor_Cuota, _
                             lMonto_Detalle)
      
      lLinea = Grilla.Rows
      Call Grilla.AddItem("")
      Grilla.Cell(flexcpChecked, lLinea, "chk") = flexChecked
      Call SetCell(Grilla, lLinea, "colum_pk", pId_Operacion)
      Call SetCell(Grilla, lLinea, "id_cuenta", pId_Cuenta)
      Call SetCell(Grilla, lLinea, "num_cuenta", pNum_Cuenta)
      Call SetCell(Grilla, lLinea, "tipo_movimiento", lTipo_Movimiento)
      Call SetCell(Grilla, lLinea, "id_moneda_operacion", pId_Moneda)
      Call SetCell(Grilla, lLinea, "id_operacion_detalle", lcDetalle.Campo("id_operacion_detalle").Valor)
      Call SetCell(Grilla, lLinea, "id_nemotecnico", lcDetalle.Campo("Id_Nemotecnico").Valor)
      Call SetCell(Grilla, lLinea, "nemotecnico", lNemotecnico)
      Call SetCell(Grilla, lLinea, "cuotas", lNumero_Cuotas)
      Call SetCell(Grilla, lLinea, "valor_cuota", lValor_Cuota)
      Call SetCell(Grilla, lLinea, "monto_detalle", lMonto_Detalle)
      
      
      Call Sub_Colecciona_Nemotecnico(lcDetalle.Campo("Id_Nemotecnico").Valor, lNemotecnico)
      
    Next
  Else
    Call Fnt_MsgError(lcOperaciones.SubTipo_LOG, _
                      "Problemas en cargar la Operaci�n.", _
                      lcOperaciones.ErrMsg, _
                      pConLog:=True)
    Fnt_Inserta_Grilla = False
  End If
  
End Function

Private Sub Sub_Colecciona_Nemotecnico(pId_Nemotecnico As String, pNemotecnico As String)
Dim lReg As hFields
Dim lReg_Nemo As hFields
Dim lNemo_Encontrado As Boolean
  
  lNemo_Encontrado = False
  For Each lReg In fNemotecnicos
    If pId_Nemotecnico = lReg("id_nemotecnico").Value Then
      lNemo_Encontrado = True
      Exit For
    End If
  Next
  
  If Not lNemo_Encontrado Then
    Set lReg_Nemo = fNemotecnicos.Add
    lReg_Nemo("id_nemotecnico").Value = pId_Nemotecnico
    lReg_Nemo("nemotecnico").Value = pNemotecnico
  End If
  
End Sub

Private Sub Sub_Carga_Combo_Nemotecnico()
Dim lReg As hFields
  
  Txt_Valor_Cuota.Text = ""
  
  With Cmb_Nemotecnico
    .Clear
    .Text = ""
    For Each lReg In fNemotecnicos
    
      If Not gRelogDB Is Nothing Then
        gRelogDB.AvanzaRelog
      End If
      
      .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("Id_Nemotecnico").Value, lReg("Nemotecnico").Value)
      
      Call .AddItem(lReg("Nemotecnico").Value)
        
    Next
  End With
  
End Sub

Private Sub Sub_Confirmacion()
Dim lFila As Long
Dim lFila_S As Long
Dim lId_Operacion As String
Dim lhOperaciones As hRecord
Dim lReg As hFields
  
  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
  
  Set lhOperaciones = New hRecord
  With lhOperaciones
    .AddField "Id_Operacion"
    .AddField "Id_Operacion_detalle"
    .AddField "Numero_Cuotas"
    .AddField "Valor_Cuota"
  End With
  
  For lFila = 1 To Grilla.Rows - 1
    If Grilla.Cell(flexcpChecked, lFila, Grilla.ColIndex("chk")) = flexChecked Then
      lId_Operacion = GetCell(Grilla, lFila, "colum_pk")
    
      For lFila_S = 1 To Grilla.Rows - 1
        If lId_Operacion = GetCell(Grilla, lFila_S, "colum_pk") Then
          Set lReg = lhOperaciones.Add
          lReg("id_operacion").Value = GetCell(Grilla, lFila_S, "colum_pk")
          lReg("Id_Operacion_detalle").Value = GetCell(Grilla, lFila_S, "Id_Operacion_detalle")
          lReg("Numero_Cuotas").Value = GetCell(Grilla, lFila_S, "Cuotas")
          lReg("Valor_Cuota").Value = GetCell(Grilla, lFila_S, "Valor_Cuota")
          
          Grilla.Cell(flexcpChecked, lFila_S, Grilla.ColIndex("chk")) = flexUnchecked
        End If
      Next
      
      If Not Fnt_Grabar_Confirmacion(lhOperaciones) Then
        Exit For
      End If
    End If
  Next
  
  Call Sub_CargaCombo_Fechas
  Call Sub_CargarDatos
  
  Set lhOperaciones = Nothing
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
  
End Sub

Private Function Fnt_Grabar_Confirmacion(phOperaciones As hRecord) As Boolean
Dim lId_Caja_Cuenta As Double
Dim lRollback As Boolean
Dim lcOperaciones As Class_Operaciones
Dim lDetalle As Class_Operaciones_Detalle
Dim lReg As hFields

  gDB.IniciarTransaccion
  
  lRollback = True
  Fnt_Grabar_Confirmacion = False

  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Id_Operacion").Valor = phOperaciones.Record(1)("id_operacion").Value
    
    If Not .BuscaConDetalles Then
      GoTo ErrProcedure
    End If
    
    If .Campo("flg_tipo_movimiento").Valor = gcTipoOperacion_Ingreso Then
      lId_Caja_Cuenta = Fnt_ValirdarFinanciamiento(.Campo("Id_cuenta").Valor, _
                                                   fc_Mercado, _
                                                   .Campo("Monto_operacion").Valor, _
                                                   .Campo("Id_Moneda_Operacion").Valor, _
                                                   .Campo("Fecha_Liquidacion").Valor)
      If lId_Caja_Cuenta = -1 Then
        'Si el financiamiento tubo problemas
        GoTo ErrProcedure
      End If
    Else
      'Si es una venta
      lId_Caja_Cuenta = Fnt_ValidarCaja(.Campo("Id_cuenta").Valor, _
                                        fc_Mercado, _
                                        .Campo("Id_Moneda_Operacion").Valor)
      If lId_Caja_Cuenta < 0 Then
        'Significa que hubo problema con la busqueda de la caja
        GoTo ErrProcedure
      End If
    End If
  
'    .Campo("Id_Operacion").valor = fId_Operacion
'    .Campo("Id_Cuenta").valor = fId_Cuenta
'    .Campo("Cod_Tipo_Operacion").valor = gcOPERACION_Directa
'    .Campo("Cod_Estado").valor = cCod_Estado_Pendiente
'    .Campo("Id_Contraparte").Valor = lId_Contraparte
'    .Campo("Id_Representante").Valor = lId_representante
'    .Campo("Id_Tipo_Liquidacion").Valor = lFormaPago
'    .Campo("Id_Moneda_Operacion").valor = Txt_Moneda.Tag
'    .Campo("Cod_Producto").valor = gcPROD_FFMM_NAC 'Esto va en duro
'    .Campo("Cod_Instrumento").valor = fCod_Instrumento
'    .Campo("Flg_Tipo_Movimiento").valor = fOperacion
'    .Campo("Fecha_Operacion").valor = lFecha_Operacion
'    .Campo("Fecha_Vigencia").valor = lFecha_Vigencia
'    .Campo("Fecha_Liquidacion").Valor = Dtp_FechaLiquidacion.Value
'    .Campo("Dsc_Operacion").valor = pDsc_Operacion
'    .Campo("Trader").Valor = Txt_Trader.Text
'    .Campo("Porc_Comision").Valor = 0
'    .Campo("Comision").Valor = 0
'    .Campo("Derechos").Valor = 0
'    .Campo("Gastos").Valor = 0
'    .Campo("Iva").Valor = 0
'    .Campo("Monto_Operacion").Valor = To_Number(Txt_MontoTotal.Text)
'    .Campo("Flg_Limite_Precio").valor = pTipo_Precio
    
    For Each lDetalle In lcOperaciones.Detalles
      For Each lReg In phOperaciones
        If To_Number(lDetalle.Campo("id_operacion_detalle").Valor) = To_Number(lReg("id_operacion_detalle").Value) Then
          lDetalle.Campo("Cantidad").Valor = lReg("numero_cuotas").Value
          lDetalle.Campo("Precio").Valor = lReg("valor_cuota").Value
        End If
      Next
    Next
    
    If Not .Confirmar(lId_Caja_Cuenta) Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en la Confirmaci�n de la Instrucci�n FFMM.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  
  lRollback = False
  Fnt_Grabar_Confirmacion = True

ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  
End Function

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:=Me.Caption _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_Grilla_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(False)
   End Select
End Sub

Private Sub Sub_CambiaCheck(pValor As Boolean)
Dim lLinea As Long
  
  For lLinea = 1 To Grilla.Rows - 1
     SetCell Grilla, lLinea, "CHK", pValor
  Next
End Sub

Private Sub Txt_Valor_Cuota_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", "-" y "backspace"
  If Not KeyAscii = 8 And Not KeyAscii = 46 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Sub_Cambia_Cuotas(pId_Moneda As String, _
                              ByRef pNumero_Cuota As Double, _
                              ByRef pValor_Cuota As Double, _
                              ByRef pMonto As Double)
Dim lFormato_Fecha As String

  lFormato_Fecha = Fnt_Formato_Moneda(pId_Moneda)
  
  pNumero_Cuota = Fnt_Divide(pMonto, pValor_Cuota)
  
  pNumero_Cuota = Format(pNumero_Cuota, lFormato_Fecha)
  pValor_Cuota = Format(pValor_Cuota, lFormato_Fecha)
  pMonto = Format(pMonto, lFormato_Fecha)
  
End Sub
