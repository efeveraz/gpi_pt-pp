VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Man_Publicadores_Precio 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Precios de Publicadores"
   ClientHeight    =   5955
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6510
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   204
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5955
   ScaleWidth      =   6510
   Begin VB.Frame Frame2 
      Caption         =   "Filtros de B�squeda de Precios"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1545
      Left            =   60
      TabIndex        =   4
      Top             =   450
      Width           =   6375
      Begin VB.CheckBox Chk_Cartera 
         Caption         =   "En Cartera"
         Height          =   285
         Left            =   5190
         TabIndex        =   9
         Tag             =   "SOLOLECTURA=N"
         Top             =   1080
         Value           =   1  'Checked
         Width           =   1095
      End
      Begin MSComCtl2.DTPicker DTP_Fecha 
         Height          =   315
         Left            =   1050
         TabIndex        =   6
         Tag             =   "SOLOLECTURA=N"
         Top             =   1080
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   16777215
         Format          =   69140481
         CurrentDate     =   37732
      End
      Begin TrueDBList80.TDBCombo Cmb_Publicador 
         Height          =   345
         Left            =   1050
         TabIndex        =   10
         Tag             =   "OBLI=S;CAPTION=Publicador;SOLOLECTURA=N"
         Top             =   300
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Man_Publicadores_Precio.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Producto 
         Height          =   345
         Left            =   1050
         TabIndex        =   11
         Tag             =   "OBLI=S;CAPTION=Producto;SOLOLECTURA=N"
         Top             =   690
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Man_Publicadores_Precio.frx":00AA
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Publicador"
         Height          =   345
         Left            =   120
         TabIndex        =   8
         Top             =   300
         Width           =   915
      End
      Begin VB.Label Label4 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         Height          =   315
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   915
      End
      Begin VB.Label Label3 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Producto"
         Height          =   345
         Left            =   120
         TabIndex        =   5
         Top             =   690
         Width           =   915
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Nemot�cnicos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3825
      Left            =   60
      TabIndex        =   0
      Top             =   2040
      Width           =   6375
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3435
         Left            =   120
         TabIndex        =   1
         Top             =   300
         Width           =   6105
         _cx             =   10769
         _cy             =   6059
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   8
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   $"Frm_Man_Publicadores_Precio.frx":0154
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   0   'False
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   6510
      _ExtentX        =   11483
      _ExtentY        =   635
      ButtonWidth     =   2011
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Importar"
            Key             =   "IMPORT"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Importa de una archivo plano"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Grabar"
            Key             =   "SAVE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Graba los cambios de precios"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Publicadores_Precio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const fId_Superintendencia_AFP = "2"

Dim fFlg_Tipo_Permiso           As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema          As String 'Codigo para el permiso sobre la ventana
Dim blnModifico As Boolean

Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  
  Call Form_Resize
  
  Load Me
  
End Sub

Private Sub Cmb_Producto_ItemChange()
    Dim lProducto As String
    Dim lPublicador As String

    Rem limpia la Grilla
    Grilla.Rows = 1

    lProducto = Fnt_ComboSelected_KEY(Cmb_Producto)
    lPublicador = UCase(Trim(Cmb_Publicador.Text))
    
    Rem Agregado CSM 03/03/2009 producto rf internacional
    If lProducto = gcPROD_RF_NAC Then 'Or lProducto = gcPROD_RF_INT Then
      Rem Muestra las columnas Tasa y Duraci�n de la Grilla
      Grilla.ColHidden(6) = False
      Grilla.ColHidden(7) = False
    Else
      Rem Oculta las columnas Tasa y Duraci�n de la Grilla
      Grilla.ColHidden(6) = True
      Grilla.ColHidden(7) = True
    End If
    
    Rem Muestra las columnas ISIN/TICKER
    Grilla.ColHidden(4) = True
    If (lPublicador = "RISK AMERICA") And (lProducto = gcPROD_FFMM_INT Or lProducto = gcPROD_RV_INT Or lProducto = gcPROD_RF_INT) Then
      Grilla.ColHidden(4) = False
      If lProducto = gcPROD_FFMM_INT Or lProducto = gcPROD_RV_INT Then
         Grilla.TextMatrix(0, 4) = "TICKER"
      Else
         Grilla.TextMatrix(0, 4) = "ISIN"
      End If
    End If
    
    Call Sub_Cargar_Precios
End Sub

Private Sub Cmb_Publicador_ItemChange()
  Rem limpia la Grilla
  Grilla.Rows = 1
  Call Sub_Cargar_Precios
End Sub

Private Sub Form_Load()
 
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("IMPORT").Image = "document"
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With

  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub DTP_Fecha_Change()
  Rem limpia la Grilla
  Grilla.Rows = 1
  Call Sub_Cargar_Precios
End Sub

Private Sub Chk_Cartera_Click()
  Rem limpia la Grilla
  Grilla.Rows = 1
  Call Sub_Cargar_Precios
End Sub

Private Sub Grilla_AfterEdit(ByVal Row As Long, ByVal Col As Long)
  Rem Pregunta si se modifica la columna Precio, Tasa o Duraci�n de la Grilla, coloca un "-1" en la colum_pk (id_publicadores_precio)
  If Col = (5) Or Col = (6) Or Col = (7) Then
    If GetCell(Grilla, Row, "colum_pk") = "" Then
      SetCell Grilla, Row, "colum_pk", cNewEntidad
    End If
    blnModifico = True
  End If
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  Rem Cancel si col es Nemotecnico-ISIN
  If Col = 2 Then Cancel = True
  If Col = 4 Then Cancel = True
End Sub

Private Sub Grilla_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
  Select Case Col
    Case 3, 4
      KeyAscii = 0
    Case 5, 7
      Call Sub_EsASCIINumero(KeyAscii)
    Case 6
      Call Sub_EsASCIINumero(KeyAscii)
  End Select
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Call Sub_Bloquea_Puntero(Me)
      If blnModifico Then
        If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
                , vbYesNo + vbQuestion _
                , Me.Caption) = vbYes Then
          Call Sub_Cargar_Precios
        End If
      Else
          Call Sub_Cargar_Precios
      End If
      blnModifico = False
      Call Sub_Desbloquea_Puntero(Me)
    Case "EXIT"
      Unload Me
    Case "IMPORT"
      Me.Enabled = False
      Call Sub_Importar
      Me.Enabled = True
      blnModifico = False
    Case "SAVE"
      Call Sub_Bloquea_Puntero(Me)
      Call Sub_Grabar
      Call Sub_Desbloquea_Puntero(Me)
      blnModifico = False
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Sub_CargarDatos()
    Dim lReg        As hCollection.hFields
    Dim lLinea      As Long
    Dim lProducto   As String
    Dim lPublicador As String
    Dim lFila       As String
    Dim lFecha      As String
    Dim lID         As String
    Dim bEnCartera  As Boolean

    ' Dim lNemotecnico As Class_Nemotecnicos
    Dim lPublicador_Precio As Class_Publicadores_Precio
    Dim lSaldos_Activos As Class_Saldo_Activos
    Dim mt As Long
  
    If Grilla.Row > 0 Then
        lID = GetCell(Grilla, Grilla.Row, "colum_pk")
    Else
        lID = ""
    End If
  
    Grilla.Rows = 1
  
    lProducto = Fnt_ComboSelected_KEY(Cmb_Producto)
    lPublicador = Fnt_ComboSelected_KEY(Cmb_Publicador)
    lFecha = DTP_Fecha.Value
  
    bEnCartera = (Chk_Cartera.Value = 1)
    mt = GetTickCount
    
    Set lPublicador_Precio = New Class_Publicadores_Precio
    
    With lPublicador_Precio
        If .Buscar_Precio(bEnCartera, lProducto, lPublicador, lFecha) Then
            For Each lReg In .Cursor
                lLinea = Grilla.Rows
                Grilla.AddItem ""
                SetCell Grilla, lLinea, "colum_pk", NVL(lReg("ID_PUBLICADOR_PRECIO").Value, ""), False ' True
                SetCell Grilla, lLinea, "id_nemotecnico", lReg("ID_NEMOTECNICO").Value, False  ' True
                SetCell Grilla, lLinea, "dsc_nemotecnico", lReg("NEMOTECNICO").Value, False  ' True
                SetCell Grilla, lLinea, "dsc_moneda", lReg("ID_MONEDA").Value, False  ' True
                SetCell Grilla, lLinea, "dsc_descnemo2", lReg("DSC_NEMOTECNICO_INT").Value, False  ' True
                SetCell Grilla, lLinea, "dsc_precio", NVL(lReg("PRECIO").Value, ""), False  ' True
                SetCell Grilla, lLinea, "tasa", NVL(lReg("TASA").Value, ""), False ' True
                SetCell Grilla, lLinea, "duracion", NVL(lReg("DURACION").Value, ""), False   ' True
            Next
            
        Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar Publicadores Precios.", _
                          .ErrMsg, _
                          pConLog:=True)
        End If
      
    End With
    
    Set lPublicador_Precio = Nothing
 
    Rem Ordena la grilla por Nemot�cnico en forma ascendente
    Rem (cuando se insertan nemot�cnicos que no estaban en la grilla)
    If Not lID = "" Then
        Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
        If Not Grilla.Row = cNewEntidad Then
            Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
        End If
    End If
    
    ' MsgBox (GetTickCount - mt) / 1000
End Sub

Private Sub Sub_CargaForm()
    Dim lComboList As String
    Dim lReg As hFields
    Dim lcMoneda As Object
  
    Rem Limpia la grilla
    Grilla.Rows = 1

    Call Sub_FormControl_Color(Me.Controls)

    Chk_Cartera.Value = 0
    Rem Carga el Combo Publicadores
    Call Sub_CargaCombo_Publicadores(Cmb_Publicador)
    Rem Carga el Combo Productos
    Call Sub_CargaCombo_Productos(Cmb_Producto)
  
    Rem Carga el Combo Monedas de la Grilla
  
'    Set lcMoneda = New Class_Monedas
    Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
    If lcMoneda.Buscar Then
        For Each lReg In lcMoneda.Cursor
            lComboList = lComboList & "|#" & lReg("id_moneda").Value
            If lReg.Index = 1 Then
                lComboList = lComboList & "*1"
            End If
            lComboList = lComboList & ";" & lReg("cod_moneda").Value & vbTab & lReg("dsc_moneda").Value
        Next
    End If
    Set lcMoneda = Nothing
    Grilla.ColComboList(Grilla.ColIndex("dsc_moneda")) = lComboList
  
    DTP_Fecha.Value = Format(Fnt_FechaServidor, cFormatDate)
    
    blnModifico = False

End Sub

Private Sub Sub_Grabar()
Dim lFila As Long
Dim lId_Publicador_Precio As String
Dim lPublicador As String
Dim lProducto As String
Dim lresult As Boolean
Dim lPublicadores_Precio As Class_Publicadores_Precio
  
On Error GoTo ErrProcedure
  
  gDB.IniciarTransaccion
  lresult = True
  
  If Grilla.Rows > 1 Then
    lPublicador = Fnt_ComboSelected_KEY(Cmb_Publicador)
    lProducto = Fnt_ComboSelected_KEY(Cmb_Producto)
    
    For lFila = 1 To Grilla.Rows - 1
      lId_Publicador_Precio = GetCell(Grilla, lFila, "colum_pk")
      '***************************************************************************************************************
      Rem Agregado CSM 03/03/2009 producto rf internacional
      Select Case lProducto
        Case gcPROD_RF_NAC 'Case gcPROD_RF_NAC, gcPROD_RF_INT
              Rem Graba la fila de la Grilla donde no sea vac�a la colum_pk ni la tasa
              'Modificado por MMA. 23/01/2009
              'If Not lId_Publicador_Precio = "" And Not GetCell(Grilla, lFila, "tasa") = "" Then
              If Not GetCell(Grilla, lFila, "tasa") = "" Then
                Set lPublicadores_Precio = New Class_Publicadores_Precio
                With lPublicadores_Precio
                  Rem colom_pk = -1 => Inserta una nuevo registro de la tabla Publicadores_Precio con los datos de la Grilla
                  Rem colum_pk > 0 => Actualiza el registro de la tabla Publicadores_Precio con los datos de la Grilla
                  If lId_Publicador_Precio = "" Then
                      .Campo("Id_Publicador_Precio").Valor = cNewEntidad
                  Else
                      .Campo("Id_Publicador_Precio").Valor = lId_Publicador_Precio
                  End If
                  .Campo("Id_Publicador").Valor = lPublicador
                  .Campo("Id_Nemotecnico").Valor = GetCell(Grilla, lFila, "id_nemotecnico")
                  .Campo("Id_Moneda").Valor = GetCell(Grilla, lFila, "dsc_moneda")
                  .Campo("Fecha").Valor = DTP_Fecha.Value
                  .Campo("Tasa").Valor = GetCell(Grilla, lFila, "tasa")
                  If Not GetCell(Grilla, lFila, "dsc_precio") = "" Then
                    .Campo("Precio").Valor = GetCell(Grilla, lFila, "dsc_precio")
                  End If
                  If Not GetCell(Grilla, lFila, "duracion") = "" Then
                    .Campo("Duracion").Valor = GetCell(Grilla, lFila, "duracion")
                  End If
                  
                  If Not .Guardar Then
                    Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en grabar Publicadores Precios.", _
                              .ErrMsg, _
                              pConLog:=True)
                    lresult = False
                    GoTo ErrProcedure
                  End If
                  Call SetCell(Grilla, lFila, "colum_pk", .Campo("Id_Publicador_Precio").Valor)
                End With
              
              Rem Elimina la fila de la Grilla donde no sea vac�a la colum_pk y la tasa es vac�a
              ElseIf Not lId_Publicador_Precio = "" And GetCell(Grilla, lFila, "tasa") = "" Then
                Set lPublicadores_Precio = New Class_Publicadores_Precio
                With lPublicadores_Precio
                  .Campo("ID_PUBLICADOR_PRECIO").Valor = lId_Publicador_Precio
                  If Not .Borrar Then
                    Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en grabar Publicadores Precios.", _
                              .ErrMsg, _
                              pConLog:=True)
                    lresult = False
                    GoTo ErrProcedure
                  End If
                End With
              End If
              
      '***************************************************************************************************************
        Rem Agregado CSM 03/03/2009 producto rf internacional
        Case gcPROD_RV_NAC, gcPROD_FFMM_NAC, gcPROD_RV_INT, gcPROD_FFMM_INT, gcPROD_RF_INT
              'Modificado por MMA. 23/01/2009
              'If Not lId_Publicador_Precio = "" And Not GetCell(Grilla, lFila, "dsc_precio") = "" Then
              If Not GetCell(Grilla, lFila, "dsc_precio") = "" Then
                Set lPublicadores_Precio = New Class_Publicadores_Precio
                With lPublicadores_Precio
                  Rem colom_pk = -1 => Inserta una nuevo registro de la tabla Publicadores_Precio con los datos de la Grilla
                  Rem colum_pk > 0 => Actualiza el registro de la tabla Publicadores_Precio con los datos de la Grilla
                  If lId_Publicador_Precio = "" Then
                      .Campo("Id_Publicador_Precio").Valor = cNewEntidad
                  Else
                      .Campo("Id_Publicador_Precio").Valor = lId_Publicador_Precio
                  End If
                  .Campo("Id_Publicador").Valor = lPublicador
                  .Campo("Id_Nemotecnico").Valor = GetCell(Grilla, lFila, "id_nemotecnico")
                  .Campo("Id_Moneda").Valor = GetCell(Grilla, lFila, "dsc_moneda")
                  .Campo("Fecha").Valor = DTP_Fecha.Value
                  .Campo("Precio").Valor = GetCell(Grilla, lFila, "dsc_precio")
                  If Not .Guardar Then
                    Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en grabar Publicadores Precios.", _
                              .ErrMsg, _
                              pConLog:=True)
                    lresult = False
                    GoTo ErrProcedure
                  End If
                  Call SetCell(Grilla, lFila, "colum_pk", .Campo("Id_Publicador_Precio").Valor)
                End With
              
              Rem Elimina la fila de la Grilla donde no sea vac�a la colum_pk y el precio es vac�o
              ElseIf Not lId_Publicador_Precio = "" And GetCell(Grilla, lFila, "dsc_precio") = "" Then
                Set lPublicadores_Precio = New Class_Publicadores_Precio
                With lPublicadores_Precio
                  .Campo("ID_PUBLICADOR_PRECIO").Valor = lId_Publicador_Precio
                  If Not .Borrar Then
                    Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en grabar Publicadores Precios.", _
                              .ErrMsg, _
                              pConLog:=True)
                    lresult = False
                    GoTo ErrProcedure
                  End If
                End With
              End If
      End Select
    Next lFila
  End If
  
ErrProcedure:
  If Err Then
    lresult = False
    Resume
  End If
  
  If lresult Then
    gDB.CommitTransaccion
  Else
    gDB.RollbackTransaccion
  End If
  
  gDB.Parametros.Clear
  Set lPublicadores_Precio = Nothing
  
  If Grilla.Rows > 1 Then Call Sub_CargarDatos
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function

Private Sub Sub_Importar()
Dim lcRel_Publica_Instru_Compon As Class_Rel_Publica_Instru_Compon
Dim lcClase As Object
'-------------------------------------------------------------
Dim lId_Publicador As Double
Dim lDsc_Publicador As String
Dim lcod_producto As String
Dim lDsc_Producto As String
Dim lCod_Proceso_Componente As String
  
  If IsNull(Cmb_Publicador.SelectedItem) Then
    MsgBox "Debe seleccionar un Publicador para importar precios de un archivo plano.", vbExclamation, Me.Caption
    Exit Sub
  End If
  
  lId_Publicador = Fnt_ComboSelected_KEY(Cmb_Publicador)
  lDsc_Publicador = Cmb_Publicador.Text
  lcod_producto = Fnt_ComboSelected_KEY(Cmb_Producto)
  lDsc_Producto = Cmb_Producto.Text
  
'  Select Case lId_Publicador
'    Case fId_Superintendencia_AFP
'      Call Sub_EsperaVentana(lId_Publicador)
''    Case Else
''      Rem MENSAJE PROVISORIO !!
''      MsgBox "El Publicador no corresponde a Superintendencia de AFP.", vbExclamation, Me.Caption
'  End Select
    
  Set lcRel_Publica_Instru_Compon = New Class_Rel_Publica_Instru_Compon
  With lcRel_Publica_Instru_Compon
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    .Campo("id_publicador").Valor = lId_Publicador
    .Campo("cod_producto").Valor = lcod_producto
    
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas para leer el componente para la importaci�n de precios.", .ErrMsg, pConLog:=True)
      GoTo ExitProcedure
    End If
    
    If Not .Cursor.Count > 0 Then
      MsgBox "No existe importador para el Publicador/Producto seleccionado.", vbInformation, Me.Caption
      GoTo ExitProcedure
    End If
    
    lCod_Proceso_Componente = .Cursor(1)("COD_PROCESO_COMPONENTE").Value
  End With
  
  Set lcClase = Fnt_IniciaFormulario(pCod_Proceso_Componente:=lCod_Proceso_Componente)
  If lcClase Is Nothing Then
    GoTo ExitProcedure
  End If
  
  With lcClase
    Set .gDB = gDB
    'Set .gRelogDB = gRelogDB
    .Id_Publicador = lId_Publicador
    .fecha = DTP_Fecha.Value
    .Cod_Producto = lcod_producto
    .Dsc_Publicador = lDsc_Publicador
    .dsc_producto = lDsc_Producto
  End With
  
ExitProcedure:
  Set lcRel_Publica_Instru_Compon = Nothing
End Sub

Private Sub Sub_Cargar_Precios()
  If Not IsNull(Cmb_Publicador.SelectedItem) And Not IsNull(Cmb_Producto.SelectedItem) Then
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_CargarDatos
    Call Sub_Desbloquea_Puntero(Me)
  End If
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
Dim lPublicador As String
Dim lProducto As String
Dim lFecha As String

  Set lForm = New Frm_Reporte_Generico
  lPublicador = Cmb_Publicador.Text
  lProducto = Cmb_Producto.Text
  lFecha = DTP_Fecha.Value
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Listado de Precios de Publicadores" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Publicador: " & lPublicador
      .Paragraph = "Producto: " & lProducto
      .Paragraph = "Fecha: " & lFecha
      .Paragraph = "En Cartera: " & IIf(Chk_Cartera, "Si", "No")
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

