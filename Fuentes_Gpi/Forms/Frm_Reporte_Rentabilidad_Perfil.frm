VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Rentabilidad_Perfil 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Rentabilidad por Perfil"
   ClientHeight    =   8940
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6900
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8940
   ScaleWidth      =   6900
   Begin VB.Frame Frm_Fecha_Re_Proceso 
      Caption         =   "Fechas Proceso"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   795
      Left            =   60
      TabIndex        =   16
      Top             =   390
      Width           =   6765
      Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
         Height          =   315
         Left            =   1470
         TabIndex        =   0
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   67371009
         CurrentDate     =   37732
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
         Height          =   315
         Left            =   4500
         TabIndex        =   1
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   67371009
         CurrentDate     =   37732
      End
      Begin VB.Label Lbl_Fecha_Desde 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha_Desde"
         Height          =   345
         Left            =   210
         TabIndex        =   18
         Top             =   270
         Width           =   1215
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Hasta"
         Height          =   345
         Left            =   3240
         TabIndex        =   17
         Top             =   270
         Width           =   1215
      End
   End
   Begin VB.Frame Frm_Filtros 
      Caption         =   "B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7545
      Left            =   60
      TabIndex        =   6
      Top             =   1230
      Width           =   6765
      Begin VB.Frame Frm_Cuentas 
         Caption         =   "Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4815
         Left            =   150
         TabIndex        =   12
         Top             =   2640
         Width           =   6525
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
            Height          =   4425
            Left            =   90
            TabIndex        =   13
            Top             =   270
            Width           =   5745
            _cx             =   10134
            _cy             =   7805
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   5
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Reporte_Rentabilidad_Perfil.frx":0000
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Chequeo 
            Height          =   660
            Left            =   5940
            TabIndex        =   14
            Top             =   540
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar3 
               Height          =   255
               Left            =   9420
               TabIndex        =   15
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
      Begin VB.Frame Frm_Propiedades_Cuentas 
         Caption         =   "Propiedades de la Cuenta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   2355
         Left            =   150
         TabIndex        =   7
         Top             =   270
         Width           =   6525
         Begin MSComctlLib.Toolbar Toolbar_Chequear_Propietario 
            Height          =   330
            Left            =   5070
            TabIndex        =   8
            Top             =   1890
            Width           =   1320
            _ExtentX        =   2328
            _ExtentY        =   582
            ButtonWidth     =   1958
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Chequear"
                  Key             =   "CHK"
                  Object.ToolTipText     =   "Busca las cuentas seg�n los filtros"
               EndProperty
            EndProperty
         End
         Begin TrueDBList80.TDBCombo Cmb_Asesor 
            Height          =   345
            Left            =   1380
            TabIndex        =   3
            Top             =   690
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_Rentabilidad_Perfil.frx":00D1
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Clientes 
            Height          =   345
            Left            =   1380
            TabIndex        =   4
            Top             =   1080
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_Rentabilidad_Perfil.frx":017B
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Grupos_Cuentas 
            Height          =   345
            Left            =   1380
            TabIndex        =   5
            Top             =   1470
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_Rentabilidad_Perfil.frx":0225
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Perfil 
            Height          =   345
            Left            =   1380
            TabIndex        =   2
            Top             =   300
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_Rentabilidad_Perfil.frx":02CF
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Lbl_Perfil 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Perfil de Riesgo"
            Height          =   345
            Left            =   90
            TabIndex        =   21
            Top             =   300
            Width           =   1275
         End
         Begin VB.Label lbl_GruposCuentas 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Grupos Cuentas"
            Height          =   345
            Left            =   90
            TabIndex        =   11
            Top             =   1470
            Width           =   1275
         End
         Begin VB.Label Lbl_Clientes 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Clientes"
            Height          =   345
            Left            =   90
            TabIndex        =   10
            Top             =   1080
            Width           =   1275
         End
         Begin VB.Label Lbl_Asesor 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Asesor"
            Height          =   345
            Left            =   90
            TabIndex        =   9
            Top             =   690
            Width           =   1275
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   6900
      _ExtentX        =   12171
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORT"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Genera reporte de Rentabilidad por Perfil"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   20
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Rentabilidad_Perfil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Const cNA = "N/A"

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Form_Load()
  
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORT").Image = cBoton_Modificar
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Chequear_Propietario
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("CHK").Image = cBoton_Agregar_Grilla
  End With
  
  With Toolbar_Chequeo
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
      .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
      .Appearance = ccFlat
  End With
   
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Cuentas_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla_Cuentas.ColIndex("CHK") Then
    Cancel = True
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "REPORT"
      Call Sub_Generar_Reporte
    Case "REFRESH"
      Call Sub_CargaForm
    Case "EXIT"
      Unload Me
  End Select
  
End Sub

Private Sub Sub_CargaForm()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lReg As hFields
Dim lLinea As Long
  
  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_FormControl_Color(Me.Controls)

  Grilla_Cuentas.Rows = 1
  
  DTP_Fecha_Hasta.Value = Fnt_FechaServidor
  DTP_Fecha_Desde.Value = Fnt_FechaServidor
  
  Rem Carga los combos con el primer elemento vac�o
  Call Sub_CargaCombo_Perfil_Riesgo(Cmb_Perfil, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Perfil, cCmbKBLANCO)
  
  Call Sub_CargaCombo_Asesor(Cmb_Asesor, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKBLANCO)
  
  Call Sub_CargaCombo_Clientes(Cmb_Clientes, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Clientes, cCmbKBLANCO)
  
  Call Sub_CargaCombo_Grupos_Cuentas(Cmb_Grupos_Cuentas, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Grupos_Cuentas, cCmbKBLANCO)

  Rem Carga las cuentas habilitadas y de la empresa en la grilla
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("Cod_Estado").Valor = cCod_Estado_Habilitada
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    If .Buscar_Vigentes Then
      Call Sub_hRecord2Grilla(lcCuenta.Cursor, Grilla_Cuentas, "id_cuenta")
    Else
      Call Fnt_MsgError(.SubTipo_LOG _
                      , "Problema con buscar las cuentas vigentes." _
                      , .ErrMsg _
                      , pConLog:=True)
      Err.Clear
    End If
  End With
  Set lcCuenta = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Toolbar_Chequear_Propietario_ButtonClick(ByVal Button As MSComctlLib.Button)
Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "CHK"
      Call Sub_Busca_Cuentas_Propiedades
  End Select
  
End Sub

Private Sub Toolbar_Chequeo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
       Call Sub_CambiaCheck(True)
    Case "SEL_NOTHING"
       Call Sub_CambiaCheck(False)
  End Select
   
End Sub

Private Sub Sub_CambiaCheck(pValor As Boolean)
Dim lLinea As Long
Dim lCol As Long
  
  lCol = Grilla_Cuentas.ColIndex("CHK")
  If pValor Then
    For lLinea = 1 To Grilla_Cuentas.Rows - 1
      Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexChecked
    Next
  Else
    For lLinea = 1 To Grilla_Cuentas.Rows - 1
      Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexUnchecked
    Next
  End If
  
End Sub

Private Sub Sub_Busca_Cuentas_Propiedades()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lCursor As hCollection.hRecord
Dim lReg As hCollection.hFields
Dim lId_Perfil_Riesgo As String
Dim lId_Asesor As String
Dim lId_Cliente As String
Dim lId_Grupos_Cuentas As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  lId_Perfil_Riesgo = Fnt_ComboSelected_KEY(Cmb_Perfil)
  lId_Perfil_Riesgo = IIf(lId_Perfil_Riesgo = cCmbKBLANCO, "", lId_Perfil_Riesgo)
  
  lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
  lId_Asesor = IIf(lId_Asesor = cCmbKBLANCO, "", lId_Asesor)
  
  lId_Cliente = Fnt_ComboSelected_KEY(Cmb_Clientes)
  lId_Cliente = IIf(lId_Cliente = cCmbKBLANCO, "", lId_Cliente)
  
  lId_Grupos_Cuentas = Fnt_ComboSelected_KEY(Cmb_Grupos_Cuentas)
  lId_Grupos_Cuentas = IIf(lId_Grupos_Cuentas = cCmbKBLANCO, "", lId_Grupos_Cuentas)
  
'  Set lcCuenta = New Class_Cuentas
  
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  With lcCuenta
    .Campo("id_perfil_riesgo").Valor = lId_Perfil_Riesgo
    .Campo("id_asesor").Valor = lId_Asesor
    .Campo("Id_Cliente").Valor = lId_Cliente
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar(pEnVista:=False, pId_Grupo_Cuenta:=lId_Grupos_Cuentas) Then
      For Each lReg In .Cursor
        Call Sub_Llena_Grilla_Cuentas(lReg("id_cuenta").Value)
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With

  Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Sub Sub_Llena_Grilla_Cuentas(pId_Cuenta As String)
Dim lFila As Long
Dim lCol As Long
  
  With Grilla_Cuentas
    If .Rows > 0 Then
      lCol = Grilla_Cuentas.ColIndex("CHK")
      For lFila = 1 To .Rows - 1
        If GetCell(Grilla_Cuentas, lFila, "colum_pk") = pId_Cuenta Then
          .Cell(flexcpChecked, lFila, lCol) = flexChecked
          Exit For
        End If
      Next
    End If
  End With
  
End Sub

Private Sub Sub_Generar_Reporte()
Const clrHeader = &HD0D0D0
'------------------------------------------
Const sHeader = "|Total Cartera por periodo (t)|Clase de activo por periodo (t)"
Const sFormat = "7500|3200|3000"
Const sHeader_Cta = "Tipo Perfil|Asesor|N� Cuenta|Abr Cuenta|RF/RV(%)|Rent. Nominal(%)|Rent. Real(%)|Acciones(%)|Renta Fija(%)"
Const sFormat_Cta = "1600|2400|1000|>1300|>1200|>1700|>1500|>1500|>1500"
'------------------------------------------
Dim sRecord
Dim bAppend
'------------------------------------------
Dim lForm As Frm_Reporte_Generico
Dim lCol As Long
'------------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lId_Cuenta As String
Dim lFila_Cta As Long
Dim lCol_Cta As Long
Dim lId_Perfil_Riesgo As String
Dim lCursor_Cta As hRecord
Dim lDsc_Perfil_Riesgo As String
Dim lcPerfil As Class_Perfiles_Riesgo
Dim lCursor_Perfil As hRecord
Dim lReg_Perfil As hFields
Dim lReg_Cta As hFields
Dim lCursor_Ordenado As hRecord
Dim lReg_Ordenado As hFields
Dim lId_Perfil_Riesgo_Ant As String
Dim lId_Perfil_Riesgo_Act As String
Dim lRegistros As Long
Dim lRegistros_Total As Long
'-------------------------------------------
Dim lPorc_RV As Double
Dim lPorc_RF As Double
Dim lRent_Nominal As Variant
Dim lRent_Real As Variant
Dim lSum_Rent_Nominal As Double
Dim lSum_Rent_Real As Double
Dim lSum_Acciones As Double
Dim lSum_Renta_Fija As Double
Dim lPromedio_Rent_Nominal As Double
Dim lPromedio_Rent_Real As Double
Dim lPromedio_Acciones As Double
Dim lPromedio_Renta_Fija As Double
Dim lAcciones As Variant
Dim lRenta_Fija As Variant

  Call Sub_Bloquea_Puntero(Me)
  
  If DTP_Fecha_Desde.Value > DTP_Fecha_Hasta.Value Then
    MsgBox "La Fecha Desde no puede ser mayor que la Fecha Hasta.", vbInformation, Me.Caption
    GoTo ErrProcedure
  End If
  
  Set lCursor_Ordenado = New hRecord
  With lCursor_Ordenado
    .ClearFields
    .AddField "ID_PERFIL_RIESGO"
    .AddField "ID_CUENTA"
    .AddField "DSC_PERFIL_RIESGO"
    .AddField "DSC_ASESOR"
    .AddField "NUM_CUENTA"
    .AddField "ABR_CUENTA"
    .AddField "ID_MONEDA"
    .AddField "PORCEN_RF"
    .AddField "PORCEN_RV"
    .LimpiarRegistros
  End With
  
  lId_Perfil_Riesgo = Fnt_ComboSelected_KEY(Cmb_Perfil)
  lId_Perfil_Riesgo = IIf(lId_Perfil_Riesgo = cCmbKBLANCO, "", lId_Perfil_Riesgo)
  
  Rem Busca los perfiles de riesgo
  Set lcPerfil = New Class_Perfiles_Riesgo
  With lcPerfil
    .Campo("id_perfil_riesgo").Valor = lId_Perfil_Riesgo
    If .Buscar Then
      Set lCursor_Perfil = .Cursor
    Else
      Call Fnt_MsgError(.SubTipo_LOG _
                      , "Problema en buscar Perfiles de Riesgo." _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  Set lcPerfil = Nothing
  
  lCol = Grilla_Cuentas.ColIndex("CHK")
  
  Rem Por cada perfil de riesgo busca todas las cuentas que esten asociadas
  For Each lReg_Perfil In lCursor_Perfil
'    Set lcCuenta = New Class_Cuentas
    
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    
    With lcCuenta
      .Campo("id_perfil_riesgo").Valor = lReg_Perfil("ID_PERFIL_RIESGO").Value
      If .Buscar(pEnVista:=True) Then
        Set lCursor_Cta = .Cursor
      Else
        Call Fnt_MsgError(.SubTipo_LOG _
                        , "Problema en buscar cuentas." _
                        , .ErrMsg _
                        , pConLog:=True)
        GoTo ErrProcedure
      End If
    End With
    Set lcCuenta = Nothing
    
    For Each lReg_Cta In lCursor_Cta
      lId_Cuenta = lReg_Cta("id_cuenta").Value
      lCol_Cta = Grilla_Cuentas.ColIndex("colum_pk")
      lFila_Cta = Grilla_Cuentas.FindRow(Item:=lId_Cuenta, Col:=lCol_Cta)
      
      If Not lFila_Cta = -1 Then
        If Grilla_Cuentas.Cell(flexcpChecked, lFila_Cta, lCol) = flexChecked Then
          Set lReg_Ordenado = lCursor_Ordenado.Add
          lReg_Ordenado("ID_PERFIL_RIESGO").Value = lReg_Perfil("ID_PERFIL_RIESGO").Value
          lReg_Ordenado("ID_CUENTA").Value = lId_Cuenta
          lReg_Ordenado("DSC_PERFIL_RIESGO").Value = lReg_Perfil("DSC_PERFIL_RIESGO").Value
          lReg_Ordenado("DSC_ASESOR").Value = lReg_Cta("DSC_ASESOR").Value
          lReg_Ordenado("NUM_CUENTA").Value = lReg_Cta("NUM_CUENTA").Value
          lReg_Ordenado("ABR_CUENTA").Value = lReg_Cta("ABR_CUENTA").Value
          lReg_Ordenado("ID_MONEDA").Value = lReg_Cta("ID_MONEDA").Value
          lReg_Ordenado("PORCEN_RF").Value = lReg_Cta("PORCEN_RF").Value
          lReg_Ordenado("PORCEN_RV").Value = lReg_Cta("PORCEN_RV").Value
        End If
      End If
    Next
  Next
  
  Rem Comienzo de la generaci�n del reporte
  Set lForm = New Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="Reporte Rentabilidad por Perfil" _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orLandscape)
     
  With lForm.VSPrinter
    .FontSize = 9
    .Paragraph = "Periodo Consulta: " & DTP_Fecha_Desde.Value & " a " & DTP_Fecha_Hasta.Value
    
    .FontSize = 8
    .FontBold = False
    .Paragraph = "" 'salto de linea
    
    Rem Se recorre el cursor ordenado
    lRegistros = 0
    lSum_Rent_Nominal = 0
    lSum_Rent_Real = 0
    lSum_Acciones = 0
    lSum_Renta_Fija = 0
    If lCursor_Ordenado.Count > 0 Then
      .StartTable
      .TableCell(tcAlignCurrency) = False
      .AddTable sFormat, sHeader, "", clrHeader, , bAppend
      .TableCell(tcFontBold, 0) = True
      .EndTable
      .StartTable
      .TableCell(tcAlignCurrency) = False
      
      lId_Perfil_Riesgo_Ant = lCursor_Ordenado(1)("Id_Perfil_Riesgo").Value
      BarraProceso.Max = lCursor_Ordenado.Count
      For Each lReg_Ordenado In lCursor_Ordenado
        
        lRegistros_Total = lRegistros_Total + 1
        lRegistros = lRegistros + 1
        
        BarraProceso.Value = lRegistros_Total
        
        Call Sub_Interactivo(gRelogDB)
        
        lId_Perfil_Riesgo_Act = lReg_Ordenado("ID_PERFIL_RIESGO").Value
        If lId_Perfil_Riesgo_Ant = lId_Perfil_Riesgo_Act Then
            lPorc_RV = Round(NVL(lReg_Ordenado("PORCEN_RV").Value, 0), 2)
            lPorc_RF = Round(NVL(lReg_Ordenado("PORCEN_RF").Value, 0), 2)
            
'          If Not Fnt_Porcentaje_RV_RF(lReg_Ordenado("id_cuenta").Value, _
'                                      DTP_Fecha_Desde.Value, _
'                                      DTP_Fecha_Hasta.Value, _
'                                      lPorc_RV, _
'                                      lPorc_RF) Then
'            GoTo ErrProcedure
'          End If
          
          If Not Fnt_Rentabilidad_Nominal(lReg_Ordenado("id_cuenta").Value, _
                                          lReg_Ordenado("id_moneda").Value, _
                                          DTP_Fecha_Desde.Value, _
                                          DTP_Fecha_Hasta.Value, _
                                          lRent_Nominal, _
                                          lRent_Real) Then
            GoTo ErrProcedure
          End If
          
          If Not Fnt_Acciones(lReg_Ordenado("id_cuenta").Value, _
                              DTP_Fecha_Desde.Value, _
                              DTP_Fecha_Hasta.Value, _
                              lAcciones) Then
            GoTo ErrProcedure
          End If
          
          If Not Fnt_Renta_Fija(lReg_Ordenado("id_cuenta").Value, _
                                DTP_Fecha_Desde.Value, _
                                DTP_Fecha_Hasta.Value, _
                                lRenta_Fija) Then
            GoTo ErrProcedure
          End If
          
          sRecord = lReg_Ordenado("DSC_PERFIL_RIESGO").Value & "|" & _
                    lReg_Ordenado("dsc_asesor").Value & "|" & _
                    lReg_Ordenado("num_cuenta").Value & "|" & _
                    lReg_Ordenado("abr_cuenta").Value & "|" & _
                    lPorc_RF & "/" & lPorc_RV & "|" & _
                    lRent_Nominal & "|" & _
                    lRent_Real & "|" & _
                    lAcciones & "|" & _
                    lRenta_Fija
          Rem Suma las rentabilidades, para el promedio
          lSum_Rent_Nominal = lSum_Rent_Nominal + IIf(lRent_Nominal = cNA, 0, lRent_Nominal)
          lSum_Rent_Real = lSum_Rent_Real + IIf(lRent_Real = cNA, 0, lRent_Real)
          lSum_Acciones = lSum_Acciones + IIf(lAcciones = cNA, 0, lAcciones)
          lSum_Renta_Fija = lSum_Renta_Fija + IIf(lRenta_Fija = cNA, 0, lRenta_Fija)
          .AddTable sFormat_Cta, sHeader_Cta, sRecord, clrHeader, , bAppend
        Else
          lId_Perfil_Riesgo_Ant = lReg_Ordenado("ID_PERFIL_RIESGO").Value
          Rem Calcula los promedios
          lPromedio_Rent_Nominal = 0
          lPromedio_Rent_Real = 0
          lPromedio_Acciones = 0
          lPromedio_Renta_Fija = 0
          lPromedio_Rent_Nominal = Round(Fnt_Divide(lSum_Rent_Nominal, lRegistros - 1), 2)
          lPromedio_Rent_Real = Round(Fnt_Divide(lSum_Rent_Real, lRegistros - 1), 2)
          lPromedio_Acciones = Round(Fnt_Divide(lSum_Acciones, lRegistros - 1), 2)
          lPromedio_Renta_Fija = Round(Fnt_Divide(lSum_Renta_Fija, lRegistros - 1), 2)
          
          sRecord = "" & "|" & _
                    "" & "|" & _
                    "" & "|" & _
                    "" & "|" & _
                    "Promedios" & "|" & _
                    lPromedio_Rent_Nominal & "|" & _
                    lPromedio_Rent_Real & "|" & _
                    lPromedio_Acciones & "|" & _
                    lPromedio_Renta_Fija
          .AddTable sFormat_Cta, sHeader_Cta, sRecord, clrHeader, , bAppend
          .TableCell(tcFontBold, lRegistros_Total) = True
          '----------------------------------------------------------------
          Rem Se dejan en cero las sumas acumuladas para empezar de nuevo el conteo
          lRegistros = 0
          lSum_Rent_Nominal = 0
          lSum_Rent_Real = 0
          lSum_Acciones = 0
          lSum_Renta_Fija = 0
        End If
        
      Next
      
      '--------------------------------------------------------------------
      Rem Para el ultimo perfil
      Rem Calcula los promedios
      lPromedio_Rent_Nominal = 0
      lPromedio_Rent_Real = 0
      lPromedio_Acciones = 0
      lPromedio_Renta_Fija = 0
      lPromedio_Rent_Nominal = Round(Fnt_Divide(lSum_Rent_Nominal, lRegistros), 2)
      lPromedio_Rent_Real = Round(Fnt_Divide(lSum_Rent_Real, lRegistros), 2)
      lPromedio_Acciones = Round(Fnt_Divide(lSum_Acciones, lRegistros), 2)
      lPromedio_Renta_Fija = Round(Fnt_Divide(lSum_Renta_Fija, lRegistros), 2)
      
      sRecord = "" & "|" & _
                "" & "|" & _
                "" & "|" & _
                "" & "|" & _
                "Promedios" & "|" & _
                lPromedio_Rent_Nominal & "|" & _
                lPromedio_Rent_Real & "|" & _
                lPromedio_Acciones & "|" & _
                lPromedio_Renta_Fija
      .AddTable sFormat_Cta, sHeader_Cta, sRecord, clrHeader, , bAppend
      '--------------------------------------------------------------------
      .TableCell(tcFontBold, 0) = True
      .TableCell(tcFontBold, 1 + lRegistros_Total) = True
      .EndTable
    Else
      .FontBold = False
      .Paragraph = "No hay Cuentas seleccionadas."
    End If
    .EndDoc
  End With
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Fnt_Porcentaje_RV_RF(pId_Cuenta As String, _
                                      pFecha_Desde As Date, _
                                      pFecha_Hasta As Date, _
                                      ByRef pPorc_RV As Double, _
                                      ByRef pPorc_RF As Double) As Boolean
'**************************************************************************************
  With gDB
    .Parametros.Clear
    .Parametros.Add "PID_CUENTA", ePT_Numero, pId_Cuenta, ePD_Entrada
    .Parametros.Add "PFECHA_DESDE", ePT_Fecha, pFecha_Desde, ePD_Entrada
    .Parametros.Add "PFECHA_HASTA", ePT_Fecha, pFecha_Hasta, ePD_Entrada
    .Parametros.Add "PPORC_RV", ePT_Numero, "", ePD_Salida
    .Parametros.Add "PPORC_RF", ePT_Numero, "", ePD_Salida
    .Procedimiento = "PKG_BBVA_REPORTE_RENTAB_PERFIL.PORCENT_RV_RF"
    If .EjecutaSP Then
      pPorc_RV = .Parametros("PPORC_RV").Valor
      pPorc_RF = .Parametros("PPORC_RF").Valor
      Fnt_Porcentaje_RV_RF = True
    Else
      Fnt_Porcentaje_RV_RF = False
      MsgBox .Errnum & .ErrMsg, vbCritical
    End If
    .Parametros.Clear
  End With
'********************************************************************
End Function

Private Function Fnt_Rentabilidad_Nominal(pId_Cuenta As String, _
                                          pId_Moneda As String, _
                                          pFecha_Desde As Date, _
                                          pFecha_Hasta As Date, _
                                          ByRef pRent_Nominal As Variant, _
                                          ByRef pRent_Real As Variant) As Boolean
Dim lcPatrimonio As Class_Patrimonio_Cuentas
Dim lValor_Cuota_Ini As Double
Dim lValor_Cuota_Fin As Double
Dim lDias As Double

  Fnt_Rentabilidad_Nominal = True
  
  lValor_Cuota_Ini = 0
  lValor_Cuota_Fin = 0
  
  Rem Busca los valores cuota a la fecha desde y a la fecha desde
  Set lcPatrimonio = New Class_Patrimonio_Cuentas
  With lcPatrimonio
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("fecha_cierre").Valor = pFecha_Desde
    If .Buscar Then
      If .Cursor.Count > 0 Then
        lValor_Cuota_Ini = NVL(.Cursor(1)("valor_cuota_mon_cuenta").Value, 0)
      End If
    Else
      Fnt_Rentabilidad_Nominal = False
      MsgBox .ErrMsg, vbCritical
      GoTo ErrProcedure
    End If
    
    .Campo("fecha_cierre").Valor = pFecha_Hasta
    If .Buscar Then
      If .Cursor.Count > 0 Then
        lValor_Cuota_Fin = NVL(.Cursor(1)("valor_cuota_mon_cuenta").Value, 0)
      End If
    Else
      Fnt_Rentabilidad_Nominal = False
      MsgBox .ErrMsg, vbCritical
      GoTo ErrProcedure
    End If
    
  End With
  Set lcPatrimonio = Nothing
  
  '-----------------------------------------------------------------------
  Rem Calcula la rentabilidad nominal anualizada
  lDias = (pFecha_Hasta - pFecha_Desde) + 1
  pRent_Nominal = Fnt_Calculo_Rent(lValor_Cuota_Ini, lValor_Cuota_Fin, lDias)
    
  '-----------------------------------------------------------------------
  Rem Calcula la rentabilidad real anualizada
  If Not Fnt_Rentabilidad_Real(pId_Cuenta, _
                               pId_Moneda, _
                               DTP_Fecha_Desde.Value, _
                               DTP_Fecha_Hasta.Value, _
                               lValor_Cuota_Ini, _
                               lValor_Cuota_Fin, _
                               pRent_Real) Then
    GoTo ErrProcedure
  End If
      
  Exit Function
    
ErrProcedure:
  Set lcPatrimonio = Nothing
  
End Function

Private Function Fnt_Rentabilidad_Real(pId_Cuenta As String, _
                                       pId_Moneda_Cta As String, _
                                       pFecha_Desde As Date, _
                                       pFecha_Hasta As Date, _
                                       pValor_Cuota_Ini As Double, _
                                       pValor_Cuota_Fin As Double, _
                                       ByRef pRent_Real As Variant) As Boolean
Dim lDias As Double
'Dim lcTipo_Cambio As Class_Tipo_Cambios
Dim lcTipo_Cambio As Object
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
'------------------------------------
'Dim lcMonedas As Class_Monedas
Dim lcMoneda As Object
'------------------------------------
Dim lId_Moneda_UF As String
Dim lParidad As Double
Dim lOperacion As String
Dim lValor_Cuota_Ini_UF As Double
Dim lValor_Cuota_Fin_UF As Double
Dim lUF_Fecha_Hasta As Double
Dim lUF_Fecha_Desde As Double

  Fnt_Rentabilidad_Real = True
  
  pRent_Real = 0
  lValor_Cuota_Ini_UF = 0
  lValor_Cuota_Fin_UF = 0
  
  Rem Se busca el id_moneda de la UF, la cual es la de destino
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    .Campo("cod_moneda").Valor = cMoneda_Cod_UF
    If .Buscar Then
      lId_Moneda_UF = .Cursor(1)("id_moneda").Value
    Else
      MsgBox .ErrMsg, vbCritical
      GoTo ErrProcedure
    End If
  End With
  Set lcMoneda = Nothing
  
'  Set lcTipo_Cambio = New Class_Tipo_Cambios
  Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
  With lcTipo_Cambio
    lParidad = 0
    Rem Busca el valor de UF a la fecha desde
    If .Busca_Precio_Paridad(pId_Cuenta _
                           , pId_Moneda_Cta _
                           , lId_Moneda_UF _
                           , pFecha_Desde _
                           , lParidad _
                           , lOperacion) Then
      lUF_Fecha_Desde = lParidad
    Else
      MsgBox .ErrMsg, vbCritical
      GoTo ErrProcedure
    End If
    lParidad = 0
    Rem Busca el valor de UF a la fecha hasta
    If .Busca_Precio_Paridad(pId_Cuenta _
                           , pId_Moneda_Cta _
                           , lId_Moneda_UF _
                           , pFecha_Hasta _
                           , lParidad _
                           , lOperacion) Then
      lUF_Fecha_Hasta = lParidad
    Else
      MsgBox .ErrMsg, vbCritical
      GoTo ErrProcedure
    End If
  End With
  Set lcTipo_Cambio = Nothing
    
  Rem Valor cuota por UF
  lValor_Cuota_Ini_UF = pValor_Cuota_Ini / lUF_Fecha_Desde
  lValor_Cuota_Fin_UF = pValor_Cuota_Fin / lUF_Fecha_Hasta
  
  '-----------------------------------------------------------------------
  Rem Calcula la rentabilidad real anualizada
  lDias = (pFecha_Hasta - pFecha_Desde) + 1
  pRent_Real = Fnt_Calculo_Rent(lValor_Cuota_Ini_UF, lValor_Cuota_Fin_UF, lDias)
  
  Exit Function
    
ErrProcedure:
  Fnt_Rentabilidad_Real = False
      
End Function

Private Function Fnt_Calculo_Rent(pValor_Cuota_Ini As Double, pValor_Cuota_Fin As Double, pDias As Double) As Variant
Dim lRentabilidad As Double

  Fnt_Calculo_Rent = 0
  
  If pValor_Cuota_Fin = 0 Or pValor_Cuota_Ini = 0 Then
    Fnt_Calculo_Rent = cNA
  Else
    lRentabilidad = Fnt_Divide(pValor_Cuota_Fin, pValor_Cuota_Ini) - 1
  
    Fnt_Calculo_Rent = Round(Fnt_Divide(lRentabilidad, pDias) * 365 * 100, 2)
  End If
  
End Function

Private Function Fnt_Acciones(pId_Cuenta As String, _
                              pFecha_Desde As Date, _
                              pFecha_Hasta As Date, _
                              ByRef pAcciones As Variant) As Boolean
Dim lValor_Cuota_Ini As Double
Dim lValor_Cuota_Fin As Double
Dim lDias As Double
  
  Fnt_Acciones = True
  
  If Not Fnt_Valor_Cuota_Ponderado_RV(pId_Cuenta, pFecha_Desde, lValor_Cuota_Ini) Then
    GoTo ErrProcedure
  End If
  
  If Not Fnt_Valor_Cuota_Ponderado_RV(pId_Cuenta, pFecha_Hasta, lValor_Cuota_Fin) Then
    GoTo ErrProcedure
  End If
  
  If lValor_Cuota_Fin = 0 Or lValor_Cuota_Ini = 0 Then
    pAcciones = cNA
  Else
    pAcciones = Fnt_Calculo_Rent(lValor_Cuota_Ini, lValor_Cuota_Fin, (pFecha_Hasta - pFecha_Desde) + 1)
    'pAcciones = Round((Fnt_Divide(lValor_Cuota_Fin, lValor_Cuota_Ini) - 1) * 100, 2)
  End If
  
  Exit Function
  
ErrProcedure:
  Fnt_Acciones = False
  
End Function

Private Function Fnt_Valor_Cuota_Ponderado_RV(pId_Cuenta As String, _
                                              pFecha As Date, _
                                              ByRef pValor_Cuota_Pond As Double) As Boolean

  With gDB
    .Parametros.Clear
    .Parametros.Add "PID_CUENTA", ePT_Numero, pId_Cuenta, ePD_Entrada
    .Parametros.Add "PFECHA", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "PVALOR_CUOTA_POND", ePT_Numero, "", ePD_Salida
    .Procedimiento = "PKG_BBVA_REPORTE_RENTAB_PERFIL.VALOR_CUOTA_PONDERADO_RV"
    If .EjecutaSP Then
      pValor_Cuota_Pond = NVL(.Parametros("PVALOR_CUOTA_POND").Valor, 0)
      Fnt_Valor_Cuota_Ponderado_RV = True
    Else
      Fnt_Valor_Cuota_Ponderado_RV = False
      MsgBox .Errnum & .ErrMsg, vbCritical
    End If
    .Parametros.Clear
  End With

End Function

Private Function Fnt_Renta_Fija(pId_Cuenta As String, _
                                pFecha_Desde As Date, _
                                pFecha_Hasta As Date, _
                                ByRef pRenta_Fija As Variant) As Boolean
Dim lValor_Cuota_Ini As Double
Dim lValor_Cuota_Fin As Double
Dim lDias As Double
  
  Fnt_Renta_Fija = True
  
  If Not Fnt_Valor_Cuota_Ponderado_RF(pId_Cuenta, pFecha_Desde, lValor_Cuota_Ini) Then
    GoTo ErrProcedure
  End If
  
  If Not Fnt_Valor_Cuota_Ponderado_RF(pId_Cuenta, pFecha_Hasta, lValor_Cuota_Fin) Then
    GoTo ErrProcedure
  End If
  
  If lValor_Cuota_Fin = 0 Or lValor_Cuota_Ini = 0 Then
    pRenta_Fija = cNA
  Else
    'pRenta_Fija = Round((Fnt_Divide(lValor_Cuota_Fin, lValor_Cuota_Ini) - 1) * 100, 2)
    pRenta_Fija = Fnt_Calculo_Rent(lValor_Cuota_Ini, lValor_Cuota_Fin, (pFecha_Hasta - pFecha_Desde) + 1)
  End If
  
  Exit Function
  
ErrProcedure:
  Fnt_Renta_Fija = False
  
End Function

Private Function Fnt_Valor_Cuota_Ponderado_RF(pId_Cuenta As String, _
                                              pFecha As Date, _
                                              ByRef pValor_Cuota_Pond As Double) As Boolean

  With gDB
    .Parametros.Clear
    .Parametros.Add "PID_CUENTA", ePT_Numero, pId_Cuenta, ePD_Entrada
    .Parametros.Add "PFECHA", ePT_Fecha, pFecha, ePD_Entrada
    .Parametros.Add "PVALOR_CUOTA_POND", ePT_Numero, "", ePD_Salida
    .Procedimiento = "PKG_BBVA_REPORTE_RENTAB_PERFIL.VALOR_CUOTA_PONDERADO_RF"
    If .EjecutaSP Then
      pValor_Cuota_Pond = NVL(.Parametros("PVALOR_CUOTA_POND").Valor, 0)
      Fnt_Valor_Cuota_Ponderado_RF = True
    Else
      Fnt_Valor_Cuota_Ponderado_RF = False
      MsgBox .Errnum & .ErrMsg, vbCritical
    End If
    .Parametros.Clear
  End With

End Function
