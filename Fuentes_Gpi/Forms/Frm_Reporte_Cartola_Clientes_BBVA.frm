VERSION 5.00
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Cartola_Clientes_BBVA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cartola de Clientes"
   ClientHeight    =   1755
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   8190
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1755
   ScaleWidth      =   8190
   Begin VB.Frame Frame1 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1275
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   8055
      Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
         Height          =   345
         Left            =   1365
         TabIndex        =   5
         Top             =   270
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         Format          =   71565313
         CurrentDate     =   38938
      End
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   3960
         TabIndex        =   1
         Top             =   480
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2858
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Reporte"
               Key             =   "REPORTE"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               Object.ToolTipText     =   "Genera el Reporte de Instruccones"
            EndProperty
         EndProperty
      End
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   345
         Left            =   1365
         TabIndex        =   7
         Tag             =   "OBLI=S;CAPTION=Cuenta"
         Top             =   720
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Cartola_Clientes_BBVA.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VSPrinter8LibCtl.VSPrinter vp 
         Height          =   855
         Left            =   6570
         TabIndex        =   8
         Top             =   240
         Visible         =   0   'False
         Width           =   1335
         _cx             =   2355
         _cy             =   1508
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         MousePointer    =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoRTF         =   -1  'True
         Preview         =   -1  'True
         DefaultDevice   =   0   'False
         PhysicalPage    =   -1  'True
         AbortWindow     =   -1  'True
         AbortWindowPos  =   0
         AbortCaption    =   "Printing..."
         AbortTextButton =   "Cancel"
         AbortTextDevice =   "on the %s on %s"
         AbortTextPage   =   "Now printing Page %d of"
         FileName        =   ""
         MarginLeft      =   1440
         MarginTop       =   1440
         MarginRight     =   1440
         MarginBottom    =   1440
         MarginHeader    =   0
         MarginFooter    =   0
         IndentLeft      =   0
         IndentRight     =   0
         IndentFirst     =   0
         IndentTab       =   720
         SpaceBefore     =   0
         SpaceAfter      =   0
         LineSpacing     =   100
         Columns         =   1
         ColumnSpacing   =   180
         ShowGuides      =   2
         LargeChangeHorz =   300
         LargeChangeVert =   300
         SmallChangeHorz =   30
         SmallChangeVert =   30
         Track           =   0   'False
         ProportionalBars=   -1  'True
         Zoom            =   0.267141585040071
         ZoomMode        =   3
         ZoomMax         =   400
         ZoomMin         =   10
         ZoomStep        =   25
         EmptyColor      =   -2147483636
         TextColor       =   0
         HdrColor        =   0
         BrushColor      =   0
         BrushStyle      =   0
         PenColor        =   0
         PenStyle        =   0
         PenWidth        =   0
         PageBorder      =   0
         Header          =   ""
         Footer          =   ""
         TableSep        =   "|;"
         TableBorder     =   7
         TablePen        =   0
         TablePenLR      =   0
         TablePenTB      =   0
         NavBar          =   3
         NavBarColor     =   -2147483633
         ExportFormat    =   0
         URL             =   ""
         Navigation      =   3
         NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
         AutoLinkNavigate=   0   'False
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
      End
      Begin VB.Label Lbl_Cuenta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   " Cuenta"
         Height          =   330
         Left            =   150
         TabIndex        =   6
         Top             =   720
         Width           =   1170
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Cartola"
         Height          =   345
         Index           =   0
         Left            =   150
         TabIndex        =   2
         Top             =   270
         Width           =   1185
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   8190
      _ExtentX        =   14446
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   4
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Cartola_Clientes_BBVA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFecha              As Date
Dim lForm               As Frm_Reporte_Cartola
Dim FilaAux             As Variant
Dim iFilaTermino        As Variant
Dim lId_Cuenta          As String
Dim lFormatoMonedaCta   As String
Dim lId_Moneda_Cta      As String
Dim nDecimales          As Integer

Const RColorRelleno = 213   ' R
Const GColorRelleno = 202   ' G
Const BColorRelleno = 195   ' B

Const glb_tamletra_titulo = 10
Const glb_tamletra_subtitulo1 = 8
Const glb_tamletra_subtitulo2 = 8
Const glb_tamletra_registros = 8

Const Font_Name = "Tahoma"

Const COLUMNA_INICIO        As String = "10mm"
Const COLUMNA_DOS           As String = "175mm"
Const CONST_ANCHO_GRILLA    As String = "255mm"

Dim nPage                   As Integer
Dim oCliente                As Class_Cartola

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
  End With
  
  With Toolbar_Verificacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORTE").Image = "boton_modificar"
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
  
End Sub

Private Sub Sub_CargaForm()
    Dim lCierre As Class_Verificaciones_Cierre
    
    Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, pTodos:=False)
    Call Sub_ComboSelectedItem(Cmb_Cuentas, cCmbKALL)
      
    Set lCierre = New Class_Verificaciones_Cierre
    fFecha = lCierre.Busca_Ultima_FechaCierre
    DTP_Fecha_Ter.Value = fFecha
    Call Sub_Limpiar
End Sub

Private Sub Form_Resize()
    Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
      Case "REFRESH"
        Call Sub_Limpiar
      Case "EXIT"
        Unload Me
    End Select
End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "REPORTE"
      Call Sub_Generar_Reporte
  End Select
End Sub
Private Sub Sub_Limpiar()
      'Call Sub_ComboSelectedItem(Cmb_Estado, cCmbKALL)
      'DTP_Fecha_Ini.Value = fFecha
      DTP_Fecha_Ter.Value = fFecha
End Sub

Private Sub vp_NewPage()
    nPage = nPage + 1
    Call pon_header
    
End Sub

Public Sub pon_header()
    Dim xValTop As Variant
    Dim sRutaImagen As String
    Dim Xinicio As Variant
    Dim YInicio As Variant
    Dim lCursor_Valores As hRecord
    Dim lReg_Valores As hFields
    Dim iFil As Integer
    vp.MarginLeft = COLUMNA_INICIO
    
    sRutaImagen = App.Path & "\bbva.jpg"
    
    xValTop = ""
    
    'Inserta Logo Moneda
    On Error Resume Next
    vp.DrawPicture LoadPicture(sRutaImagen), vp.MarginLeft, "10mm", "40mm", "30mm"
    
    On Error GoTo 0
    
    vp.MarginLeft = rTwips(COLUMNA_INICIO) + rTwips("45mm")
    
    vp.CurrentX = vp.MarginLeft + rTwips("35mm")
    vp.CurrentY = "10mm"
        
'Asigna nuevos valores
    vp.TextAlign = taCenterTop
    vp.BrushColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = RGB(100, 100, 100)
    
    vp.FontSize = 12
    
'Llena texto
    vp.TextBox "BANCA PATRIMONIAL DE INVERSIONES", vp.MarginLeft, vp.CurrentY, "114.7mm", "7mm", True, False, True
    'Asigna nuevos valores
    vp.TextAlign = taLeftTop
    vp.BrushColor = RGB(232, 226, 222)
    'vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = &H0&
    vp.FontSize = 8
    
    'Llena texto
    Xinicio = vp.MarginLeft
    YInicio = vp.CurrentY
    
    vp.DrawRectangle Xinicio, _
                        YInicio, _
                        Xinicio + rTwips("115mm"), _
                        YInicio + rTwips("24mm")

    ' vp.MarginLeft = "79mm"
    vp.MarginLeft = Xinicio + rTwips("3mm")
    vp.CurrentX = Xinicio + rTwips("3mm")
    vp.CurrentY = YInicio + rTwips("3mm")
    
    Xinicio = vp.CurrentX
    With vp
      .StartTable
        .TableBorder = tbNone
       
        .TableCell(tcRows) = 5
        .TableCell(tcCols) = 1
        .TableCell(tcText, 1, 1) = "Se�ores"
        .TableCell(tcColWidth, 1, 1) = "120mm"
        .TableCell(tcText, 2, 1) = Trim(oCliente.Nombre)
        .TableCell(tcText, 3, 1) = "RUT N�:" & Trim(oCliente.Rut)
        .TableCell(tcText, 4, 1) = Trim(oCliente.Direccion)
        .TableCell(tcText, 5, 1) = Trim(oCliente.Comuna)
        .TableCell(tcFontSize, 2, 1) = 9
        .TableCell(tcFontBold, 2, 1) = True
      .EndTable
    
    End With
    

              
' ####################################################  Cuadro Dos
    vp.MarginLeft = COLUMNA_DOS ' + rTwips("40mm")
    
    Xinicio = vp.MarginLeft
    YInicio = "10mm"
                
    'Asigna nuevos valores
    vp.TextAlign = taCenterTop
    vp.BrushColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = RGB(100, 100, 100)
    
    vp.FontSize = 12
    'Llena texto
    vp.TextBox "", Xinicio, YInicio, "89.7mm", "7mm", True, False, True
    
    'Asigna nuevos valores
    vp.TextAlign = taLeftTop
    vp.BrushColor = RGB(232, 226, 222)
    'vp.PenColor = RGB(100, 100, 100)
    
    vp.TextColor = &H0&
    vp.FontSize = 8
    
    'Llena texto
     vp.MarginLeft = Xinicio ' + rTwips("3mm")
    Xinicio = vp.MarginLeft
    YInicio = vp.CurrentY
    
    vp.DrawRectangle Xinicio, _
                    YInicio, _
                    Xinicio + rTwips("90mm"), _
                    YInicio + rTwips("24mm")

    vp.MarginLeft = Xinicio + rTwips("3mm")
    vp.CurrentX = Xinicio + rTwips("3mm")
    vp.CurrentY = YInicio + rTwips("2mm")
    
    vp.Text = "Informe al" & vbCrLf & _
              "N� Cuenta " & vbCrLf & _
              "Asesor" & vbCrLf & _
              "Email" & vbCrLf & _
              "Telefono" & vbCrLf & _
              "Perfil"
    
    
    vp.MarginLeft = Xinicio + rTwips("20mm")
    vp.CurrentX = Xinicio + rTwips("20mm")
    vp.CurrentY = YInicio + rTwips("2mm")
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
    gDB.Parametros.Add "pfecha", ePT_Fecha, oCliente.Fecha_Cartola, ePD_Entrada
    gDB.Procedimiento = "PKG_TIPO_CAMBIO_VALOR$Buscar_Valores"
    If gDB.EjecutaSP Then
        Set lCursor_Valores = gDB.Parametros("pcursor").Valor
    Else
        Exit Sub
    End If
    
    
      With vp
      .StartTable
        .TableBorder = tbNone
       
        .TableCell(tcRows) = 6
        .TableCell(tcCols) = 3
        .TableCell(tcText, 1, 1) = oCliente.Fecha_Cartola
        iFil = 1
        For Each lReg_Valores In lCursor_Valores
          .TableCell(tcText, iFil, 2) = Trim(NVL(lReg_Valores("dsc_moneda_destino").Value, ""))
          .TableCell(tcText, iFil, 3) = Trim(FormatNumber(NVL(lReg_Valores("valor").Value, 0), 2))
          .TableCell(tcAlign, iFil, 2) = taLeftMiddle
          .TableCell(tcAlign, iFil, 3) = taRightMiddle
          iFil = 1 + iFil
        Next
        .TableCell(tcColWidth, 1, 1) = "35mm"
        .TableCell(tcColWidth, 1, 2) = "15mm"
        .TableCell(tcColWidth, 1, 3) = "18mm"
        .TableCell(tcText, 2, 1) = Trim(oCliente.Num_Cuenta)
        .TableCell(tcText, 3, 1) = Trim(oCliente.Ejecutivo)
        .TableCell(tcText, 4, 1) = Trim(oCliente.Mail)
        .TableCell(tcText, 5, 1) = Trim(oCliente.Telefono)
        .TableCell(tcText, 6, 1) = Trim(oCliente.Perfil_Riesgo)


      .EndTable
    
    End With
    

              
    vp.MarginLeft = COLUMNA_INICIO
    vp.CurrentY = YInicio + rTwips("30mm")
    vp.CurrentX = vp.MarginLeft
    
End Sub

Private Sub Sub_Generar_Reporte()
    Dim glb_cuenta As Long
    Dim glb_fecha_hoy As String
    Dim glb_fecha_inimes As String
    
    '------------------------------------------
'    Dim lCuenta As Class_Cuentas
    Dim lcCuenta As Object
    Dim lReg_Cta As hFields
'    Dim lcMoneda As Object
    '------------------------------------------

    
    lId_Moneda_Cta = ""
    nDecimales = 0
    
    lId_Cuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Cuentas))
    Set oCliente = New Class_Cartola
    oCliente.IdCuenta = lId_Cuenta
    oCliente.Fecha_Cartola = DTP_Fecha_Ter.Value
    
    Call DatosDelCliente
    
    GeneraCartola ' lId_Cuenta ', lNum_Cuenta, lDsc_Cuenta, lDsc_Moneda, nDecimales
    
ErrProcedure:
      Call Sub_Desbloquea_Puntero(Me)
      
End Sub

Private Function DameInicioDeMes(sFecha As String) As String
    Dim sFechaTmp As String
    Dim sMes As String
    
    Dim sFormatoFecha As String
    
    If Month(sFecha) < 10 Then
        sMes = "0" & Month(sFecha)
    Else
        sMes = Month(sFecha)
    End If
    
    sFechaTmp = "01/" & sMes & "/" & Year(sFecha)
    
    DameInicioDeMes = sFechaTmp
    
End Function

Private Function Obtener_Simbolo(Valor As Long) As String
   Dim Simbolo  As String
   Dim r1       As Long
   Dim r2       As Long
   Dim p        As Integer
   Dim Locale   As Long
   
   'Locale = GetUserDefaultLCID()
   r1 = GetLocaleInfo(Locale, Valor, vbNullString, 0)
   
   'buffer
   Simbolo = String$(r1, 0)
   
   'En esta llamada devuelve el s�mbolo en el Buffer
   r2 = GetLocaleInfo(Locale, Valor, Simbolo, r1)
   
   'Localiza el espacio nulo de la cadena para eliminarla
   p = InStr(Simbolo, Chr$(0))
   
   
   If p > 0 Then
      'Elimina los nulos
      Obtener_Simbolo = Left$(Simbolo, p - 1)
   End If
   
End Function

Sub ConfiguraHoja()

    With vp
    
        .PaperSize = pprLetter
        .MarginLeft = "15mm"
        .MarginRight = "15mm"
        .MarginTop = "45mm"
        .MarginBottom = "25mm"
        .MarginFooter = "15mm"
        .MarginHeader = "0mm"
        'Call lForm.trae_datos_cliente(lId_Cuenta, DTP_Fecha_Ter.Val,ue)
        .Font.Name = Font_Name
        .Font.Size = glb_tamletra_registros
        .Orientation = orLandscape
        
    End With
End Sub

Function GeneraCartola()
    Dim i           As Integer
    Dim hay_cierre  As Boolean
    
    GeneraCartola = True
    hay_cierre = GeneraCartola
    nPage = 0
    
    ConfiguraHoja
    
    vp.StartDoc
    
    Call PaginaUno(hay_cierre)
    
    If Not hay_cierre Then
        vp.EndDoc
        vp.Clear
        'Unload lForm
        Set lForm = Nothing
        Exit Function
        
    End If
    
    DoEvents
    
    Call Sub_Generar_Reporte_2
    
    vp.EndDoc
'********************************************
    Dim sFooter As String
    
    sFooter = ""
    vp.FontName = "Arial"
    vp.FontSize = 5
   
    For i = 1 To nPage
        vp.StartOverlay i
        
        haz_linea ("200mm")
        
        vp.CurrentX = vp.MarginLeft
        vp.CurrentY = rTwips("201mm")
        'DIRECCION BANCO BBVA
        sFooter = ""
        
        vp.TextAlign = taCenterMiddle
        vp.Paragraph = sFooter & vbCrLf & "Pagina " & i & "/" & nPage
        
        vp.EndOverlay
    Next
    
    '####### Grabar el Documento de esta ventana y recuperarlo en la Otra
    
        Set lForm = New Frm_Reporte_Cartola
        Dim PathAndFile As String
        PathAndFile = oCliente.CreateTempFile("car")
        
        vp.SaveDoc PathAndFile
        lForm.vp.LoadDoc PathAndFile
        
                
    '##################################################
    
    Call ColocarEn(lForm, Me, eFP_Abajo)
    'lForm.vp.Preview
End Function
Private Sub PaginaUno(ByRef GeneraCartola As Boolean)
    Dim nMarginLeft As Variant
    Dim nColinicio As Variant
    
    vp.CurrentY = vp.CurrentY + rTwips("2mm")
    'vp.CurrentY = 2000 + rTwips("2mm")
    pon_titulo "PATRIMONIO Y RENTABILIDAD", "5mm", Font_Name, glb_tamletra_titulo
    vp.CurrentY = vp.CurrentY + rTwips("2mm")
    FilaAux = vp.CurrentY
    
    Call fncCuentaInversion(GeneraCartola)
    
    If Not GeneraCartola Then Exit Sub
    
    vp.CurrentY = iFilaTermino + rTwips("2mm")
    vp.CurrentY = iFilaTermino + rTwips("1mm")
    
     FlujoPatrimonial
     
'    ComposicionCarteraNacional
End Sub

Private Sub PaginaDos()
    Dim nMarginLeft As Variant
    Dim nColinicio As Variant
    Dim iFila As Integer
    
    Dim lCursor_Cuotas As hRecord
    Dim lReg As hFields
    
    Dim sNombres()
    Dim sValores()
    
    Dim i As Integer
    Dim iFilaComienzo As Variant
    
    Dim Dsc_Instrumento As String
    Dim monto_mon_cta As Double
    
    Dim j As Integer
    Dim aux As Double
    Dim aux1 As String
    
    Dim iFilaActual As Variant
    Dim nMargenLeft As Variant
    
    Dim sName As String
    'vp.CurrentY = vp.CurrentY + rTwips("10mm")
    'vp.NewPage
    On Local Error Resume Next
    vp.CurrentY = vp.CurrentY + rTwips("2mm")
    pon_titulo "RENTA VARIABLE POR SECTORES ECONOMICOS", "5mm", Font_Name, glb_tamletra_titulo
    vp.CurrentY = vp.CurrentY + rTwips("2mm")
    
    FilaAux = vp.CurrentY
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "pfecha_cierre", ePT_Fecha, oCliente.Fecha_Cartola, ePD_Entrada
    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$BUSCAR_RV_SECTOR"
    
    If gDB.EjecutaSP Then
        Set lCursor_Cuotas = gDB.Parametros("pcursor").Valor
    Else
        Exit Sub
    End If
    
    With vp
        iFilaComienzo = .CurrentY
        
        .StartTable
        .TableBorder = tbBox
        .TableCell(tcRows) = 1
        .TableCell(tcCols) = 2
        .TableCell(tcBackColor, 1, 1, 1, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)

        .TableCell(tcText, 1, 1) = "SECTORES ECONOMICOS"
        .TableCell(tcColWidth, , 1) = "54mm"
        .TableCell(tcText, 1, 2) = "MONTO (" & Trim(Monto_Moneda(oCliente.Moneda)) & ")"
        .TableCell(tcColWidth, , 2) = "35mm"
        .TableCell(tcColAlign, , 2) = taRightMiddle

        .TableCell(tcAlign, 1, 2, 1, 2) = taCenterMiddle
        .TableCell(tcFontSize) = glb_tamletra_registros
        
        iFila = .TableCell(tcRows) + 1
        .TableCell(tcRows) = iFila
        
        For Each lReg In lCursor_Cuotas
            
            Dsc_Instrumento = Trim(lReg("DSC_SECTOR").Value)
            monto_mon_cta = Trim(lReg("monto_mon_cta").Value)
            
            .TableCell(tcText, iFila, 1) = Dsc_Instrumento
            .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
            .TableCell(tcText, iFila, 2) = FormatNumber(monto_mon_cta, 0)
            
            iFila = .TableCell(tcRows) + 1
            .TableCell(tcRows) = iFila
            
            If monto_mon_cta <> 0 Then
                ReDim Preserve sNombres(i)
                ReDim Preserve sValores(i)

                sNombres(i) = Dsc_Instrumento
                sValores(i) = monto_mon_cta
                i = i + 1
            End If
            
        Next
        
        .EndTable
    End With
    
    iFilaActual = vp.CurrentY

    If i > 0 Then
        For i = LBound(sValores) To UBound(sValores)
            For j = i + 1 To UBound(sValores) - 1
                If CDbl(sValores(i)) > CDbl(sValores(j)) Then
                    aux = sValores(j)
                    sValores(j) = sValores(i)
                    sValores(i) = aux

                    aux1 = sNombres(j)
                    sNombres(j) = sNombres(i)
                    sNombres(i) = aux1
                End If
            Next
        Next

        sName = fncGraficoTorta(sNombres, sValores, "RENTA VARIABLE POR SECTOR", -1, False)

        With vp
            .CurrentY = iFila
            nMargenLeft = .MarginLeft
            .MarginLeft = rTwips(COLUMNA_DOS) - rTwips("25mm")
            .DrawPicture LoadPicture(sName), vp.MarginLeft + rTwips("2mm"), iFilaComienzo, "95mm", "80mm", , False

             .MarginLeft = nMargenLeft

             .CurrentY = FilaAux
        End With
    End If
    
    
End Sub

Public Sub pon_titulo(pTitulo As String, pAlto As Variant, pfont_name As String, pfont_size As Double)
    Dim ant_font_name   As String
    Dim ant_font_size   As Double
    Dim pY              As Variant

    With vp
        'lee valores anteriores
        ant_font_name = .FontName
        ant_font_size = .FontSize
        
        'pon valores nuevos
        ' .FontName = "Gill Sans MT"   ' pfont_name
        .FontSize = pfont_size
        
        'pon titulo
        pY = .CurrentY
        haz_linea (pY)
        'vp.CurrentY = pY + rTwips("0.5mm")
        .CalcParagraph = pTitulo
        .Paragraph = " " & pTitulo
        ' vp.CurrentY = pY + rTwips(pAlto) + rTwips("2mm")
        'haz_linea (pY + rTwips(pAlto))
        haz_linea (.CurrentY)
        
        'recupera valores anteriores
        .FontName = ant_font_name
        .FontSize = ant_font_size
    End With
    
End Sub
Private Function rTwips(pMM As Variant)
    Dim i As Integer, s As String, d As Double
    If VarType(pMM) <> vbString Then
        rTwips = pMM
    Else
        i = InStr(1, pMM, "mm")
        If i = 0 Then
            MsgBox "Error de conversion de mm a twips"
            End
        End If
        
        s = Mid$(pMM, 1, i - 1)
        d = CDbl(s)
        
        ' rTwips = ((((d * 254) / 1440) + 0.5) * 567) / 100 + 0.5
        rTwips = 56.52 * d
        
    End If
End Function
Sub fncDisclaimer()
    Dim sTxt As String
    vp.FontSize = 8
    If (vp.CurrentY + rTwips("30mm")) > (vp.PageHeight - vp.MarginBottom - rTwips("10mm")) Then
        vp.NewPage
    Else
        vp.CurrentY = (vp.PageHeight - vp.MarginBottom - rTwips("30mm"))
    End If
    vp.BrushColor = vbWhite
    vp.DrawRectangle vp.MarginLeft - rTwips("0mm"), vp.CurrentY, (vp.PageWidth - vp.MarginRight) + rTwips("0mm"), vp.CurrentY + rTwips("30mm")
    sTxt = "AVISO IMPORTANTE"
    vp.Paragraph = sTxt
    sTxt = "" & vbCrLf
    sTxt = sTxt & "El presente documento ha sido elaborado por Corredora Gesti�n de Inversiones S.A., de conformidad con el Mandato para la Administraci�n de Cartera celebrado entre el Cliente y Moneda Gesti�n de Inversiones S.A. el cual se entiende formar parte del presente instrumento."
    sTxt = sTxt & "Corredora Gesti�n de Inversiones S.A. no asume ninguna responsabilidad de cualquier informaci�n impl�cita o expl�cita que pudiera contener este documento."
    sTxt = sTxt & "La informaci�n contenida en el presente instrumento es confidencial y est� dirigida exclusivamente al destinatario. Cualquier uso, reproducci�n, divulgaci�n o distribuci�n por otras personas se encuentra estrictamente prohibida, acarreando su incumplimiento sanciones penales de conformidad a la ley. "
    sTxt = sTxt & vbCrLf
    vp.Paragraph = sTxt
End Sub
Private Sub haz_linea(pY As Variant)
    vp.DrawLine vp.MarginLeft, pY, vp.PageWidth - vp.MarginRight, pY
End Sub


Sub RentabilidadCartera()
    Dim lCursor As hRecord
    Dim lReg As hFields
    Dim iFila As Integer
    Dim Xinicio As Variant
    Dim YInicio As Variant
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
    gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS$RENTABILIDADES"

    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor = gDB.Parametros("Pcursor").Valor

    gDB.Parametros.Clear

    With vp
        '.CurrentY = 4300
        .CurrentY = CurrentY + 4300
        
        FilaAux = .CurrentY

        YInicio = FilaAux
        'YInicio = 5557 + rTwips("5mm")
        .CurrentY = YInicio
        FilaAux = vp.CurrentY

        .StartTable
        .TableBorder = tbBox

        .TableCell(tcCols) = 5

        .TableCell(tcRows) = .TableCell(tcRows) + 1

        .TableCell(tcFontSize, 1, 1, 1, 3) = glb_tamletra_subtitulo2
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)

        .TableCell(tcText, 1, 1) = "RENTABILIDAD"
        .TableCell(tcText, 1, 2) = "$"
        .TableCell(tcText, 1, 3) = "UF"
        .TableCell(tcText, 1, 4) = "US$"
        .TableCell(tcText, 1, 5) = "Volatilidad"
        
        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "45mm"
        .TableCell(tcColWidth, 1, 2) = "20mm"
        .TableCell(tcColWidth, 1, 3) = "20mm"
        .TableCell(tcColWidth, 1, 4) = "20mm"
        .TableCell(tcColWidth, 1, 5) = "20mm"

        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)

        For Each lReg In lCursor
            .TableCell(tcText, iFila, 1) = "Rentabilidad Mensual"
            .TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_mensual_$$").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 3) = FormatNumber(lReg("rentabilidad_mensual_UF").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 4) = FormatNumber(lReg("rentabilidad_mensual_DO").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 5) = FormatNumber(lReg("volatilidad_mensual").Value, 2, True)
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)

            .TableCell(tcText, iFila, 1) = "Rentabilidad Acumulada Anual"
            .TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_anual_$$").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 3) = FormatNumber(lReg("rentabilidad_anual_UF").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 4) = FormatNumber(lReg("rentabilidad_anual_DO").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 5) = FormatNumber(lReg("volatilidad_anual").Value, 2, True)

            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
            
            .TableCell(tcText, iFila, 1) = "Rentabilidad �ltimos 12 meses"
            .TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_ult_12_meses_$$").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 3) = FormatNumber(lReg("rentabilidad_ult_12_meses_UF").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 4) = FormatNumber(lReg("rentabilidad_ult_12_meses_DO").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 5) = FormatNumber(lReg("volatilidad_ult_12_meses").Value, 2, True)
        Next
        
        ' Alineacion de cada columna
        .TableCell(tcAlign, , 1) = taLeftMiddle
        .TableCell(tcAlign, , 2) = taRightMiddle
        .TableCell(tcAlign, , 3) = taRightMiddle
        .TableCell(tcAlign, , 4) = taRightMiddle
        .TableCell(tcAlign, , 5) = taRightMiddle
        .TableCell(tcFontSize, 1, 1, 4, 4) = glb_tamletra_registros
        .EndTable
        
    End With
    
    Set lCursor = Nothing
    Set lReg = Nothing
End Sub
Sub fncCuentaInversion(ByRef GeneraCartola As Boolean)
    Dim sProductoAnterior As String
    Dim modo As String
    Dim Filtro As String
    Dim xSql As String
    Dim nDecimales As Integer
    Dim Mostrado As Integer
    Dim anterior As String
    Dim Espacios As String
    Dim suma As Double
    Dim mTitulo As Integer
    Dim iFila As Integer
            
    Dim lcPatrimonio_Cuenta As Class_Patrimonio_Cuentas
    Dim lcSaldos_Activos As Class_Saldo_Activos
    Dim lCaja_Cuenta_Neta As Double
    Dim lCaja_Cuenta As Double
    Dim lCaja_Cuentas_por_Pagar As Double
    Dim lCaja_Cuentas_por_Cobrar As Double
    Dim lPatrimonio_mes_anterior As Double
    Dim lTotal_Activos As Double
    Dim lPatrimonio As Double
    Dim lPatrimonio_UF As Double
    Dim lCursor_Activos As hRecord
    Dim lReg As hFields
    Dim Dsc_Instrumento As String
    Dim monto_mon_cta As Double
    Dim Porcentaje As Double
    Dim sNombres()
    Dim sValores()
    Dim i As Integer
    Dim sName As String
    Dim iFilaComienzo As Variant
    'Dim monto_mon_cta As Double
    Dim lSaldo_neto_aporte_retiro As Double
    Dim lSaldo_neto_aporte_retiro_uf As Double
    
    
    Dim iFilaActual As Variant
    Dim nMargenLeft As Variant
    
  
    Dim volatilidad_mensual As String
    Dim volatilidad_anual As String
    Dim volatilidad_ult_12_meses As String

    Set lcPatrimonio_Cuenta = New Class_Patrimonio_Cuentas
    
    With lcPatrimonio_Cuenta
        .Campo("id_cuenta").Valor = oCliente.IdCuenta
        .Campo("FECHA_CIERRE").Valor = oCliente.Fecha_Cartola
        
        If .Buscar Then
            If .Cursor.Count > 0 Then
                lCaja_Cuenta_Neta = (.Cursor(1)("SALDO_CAJA_MON_CUENTA").Value + .Cursor(1)("MONTO_X_COBRAR_MON_CTA").Value) - .Cursor(1)("MONTO_X_PAGAR_MON_CTA").Value
                lCaja_Cuenta = .Cursor(1)("SALDO_CAJA_MON_CUENTA").Value
                lCaja_Cuentas_por_Pagar = .Cursor(1)("MONTO_X_PAGAR_MON_CTA").Value * -1
                lCaja_Cuentas_por_Cobrar = .Cursor(1)("MONTO_X_COBRAR_MON_CTA").Value
                lPatrimonio = .Cursor(1)("PATRIMONIO_MON_CUENTA").Value
                lPatrimonio_UF = NVL(.Cursor(1)("patrimonio_uf").Value, 0)
                lPatrimonio_mes_anterior = NVL(.Cursor(1)("patrimonio_mes_anterior").Value, 0)
                lSaldo_neto_aporte_retiro = NVL(.Cursor(1)("SALDO_NETO_APORTE_RETIRO").Value, 0)
                lSaldo_neto_aporte_retiro_uf = NVL(.Cursor(1)("SALDO_NETO_APORTE_RETIRO_uf").Value, 0)
                
            Else
                MsgBox "No hay Cierres Vigentes", vbInformation, Me.Caption
                GeneraCartola = False
                GoTo ErrProcedure
            End If
        End If
    End With
    Set lcPatrimonio_Cuenta = Nothing
    
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
    gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, DTP_Fecha_Ter.Value, ePD_Entrada
    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_ARBOLES"
    
    If Not gDB.EjecutaSP Then
              'GoTo Fin
          End If
    
          Set lCursor_Activos = gDB.Parametros("Pcursor").Valor
    
          gDB.Parametros.Clear
    

    Set lcSaldos_Activos = Nothing
    
    iFilaActual = vp.CurrentY
    
    vp.CurrentY = 3088
    
    With vp
        iFilaComienzo = .CurrentY
        .StartTable
        .TableBorder = tbBox
        .TableCell(tcRows) = 1
        .TableCell(tcCols) = 3
        .TableCell(tcBackColor, 1, 1, 1, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)

        .TableCell(tcText, 1, 1) = "PATRIMONIO ACTUAL"
        .TableCell(tcColWidth, , 1) = "50mm"
        
        .TableCell(tcText, 1, 2) = ""
        .TableCell(tcColWidth, , 2) = "40mm"
        .TableCell(tcColAlign, , 2) = taRightMiddle
        
        .TableCell(tcText, 1, 3) = Monto_Moneda(Trim(oCliente.Moneda)) & " " & FormatNumber(lPatrimonio, 0)
        
        
        .TableCell(tcColWidth, , 3) = "35mm"
        .TableCell(tcColAlign, , 3) = taRightMiddle

      
        .TableCell(tcFontSize, 1, 1, 1, 3) = glb_tamletra_registros
        .TableCell(tcFontSize, 1, 3) = 9
        .TableCell(tcFontSize, 1, 1) = 9
        .TableCell(tcFontBold, 1, 3) = True
        .TableCell(tcFontBold, 1, 1) = True
        .TableCell(tcRowBorderBelow, 1) = 2

        sProductoAnterior = ""
        nDecimales = 0

        Mostrado = 0
        Espacios = "  "
        suma = 0
        mTitulo = 1
        iFila = 2
        i = 0
        
        .TableCell(tcText, iFila, 1) = " "
        .TableCell(tcColSpan, iFila, 1) = 3
        .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
        .TableCell(tcRowHeight, iFila, 1, iFila, 3) = "1mm"
        
        iFila = .TableCell(tcRows) + 1
        
        iFila = .TableCell(tcRows) + 1
        .TableCell(tcRows) = iFila
        
        

        
        .TableCell(tcText, iFila, 1) = "Patrimonio Mes Anterior"
        .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
        .TableCell(tcText, iFila, 3) = FormatNumber(lPatrimonio_mes_anterior, 0)
        iFila = .TableCell(tcRows) + 1
        .TableCell(tcRows) = iFila
        

        
        
        
        .TableCell(tcText, iFila, 1) = "Patrimonio (UF)"
        .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
        .TableCell(tcText, iFila, 3) = FormatNumber(lPatrimonio_UF, 4)
        .TableCell(tcRows) = iFila
        .EndTable
        .Paragraph = ""
        .Paragraph = ""
    End With
    
'##########################  VALOR CUOTA ###########################################################
    
    vp.CurrentY = vp.CurrentY + rTwips("1mm")
    iFila = vp.CurrentY

    sName = fncGraficoValorCuota()
    
    'sName = fncGraficoTorta(sNombres, sValores, "Composici�n Cartera", 11, False)

    With vp
        .CurrentY = iFilaActual + rTwips("1mm")
        
        nMargenLeft = .MarginLeft
        .MarginLeft = COLUMNA_DOS
        On Error Resume Next
        .DrawPicture LoadPicture(sName), vp.MarginLeft, vp.CurrentY, "90mm", "57mm", , False ' True

        .MarginLeft = nMargenLeft
        .CurrentY = iFilaActual
    End With

    
    Call RentabilidadCartera
    
    
   
' ########################################### ACTIVOS POR PRODUCTO ##############################
    vp.CurrentY = iFila
    vp.CurrentY = vp.CurrentY + rTwips("40mm")
    
    pon_titulo "CUENTA INVERSI�N", "5mm", Font_Name, glb_tamletra_titulo
    vp.CurrentY = vp.CurrentY + rTwips("5mm")


   With vp
        iFilaComienzo = .CurrentY
        
        .StartTable
        .TableBorder = tbBox
        ' .TableCell(tcRowHeight, 1, 1, 7, 5) = "1mm"
        .TableCell(tcRows) = 1
        .TableCell(tcCols) = 3
        .TableCell(tcBackColor, 1, 1, 1, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)

        .TableCell(tcText, 1, 1) = "ACTIVOS POR PRODUCTO"
        .TableCell(tcColWidth, , 1) = "70mm"
        .TableCell(tcText, 1, 2) = "PORCENTAJE"
        .TableCell(tcColWidth, , 2) = "20mm"
        .TableCell(tcColAlign, , 2) = taRightMiddle
        .TableCell(tcText, 1, 3) = "MONTO (" & Monto_Moneda(oCliente.Moneda) & ")"
        .TableCell(tcColWidth, , 3) = "55mm"
        .TableCell(tcColAlign, , 3) = taRightMiddle

        .TableCell(tcAlign, 1, 2, 1, 3) = taCenterMiddle
        .TableCell(tcFontSize) = glb_tamletra_registros
        ' .TableCell(tcRowBorderBelow, 1) = 2

        sProductoAnterior = ""
        nDecimales = 0

        Mostrado = 0
        Espacios = "  "
        suma = 0
        mTitulo = 1
        iFila = 1
        i = 0
        
        
        For Each lReg In lCursor_Activos
                iFila = iFila + 1
                .TableCell(tcRows) = iFila
                .TableCell(tcText, iFila, 1) = Trim(lReg("dsc_arbol_clase_inst").Value)
                Dsc_Instrumento = Trim(lReg("dsc_arbol_clase_inst").Value)
                monto_mon_cta = Trim(IIf(lReg("monto_mon_cta").Value = 0, 1, lReg("monto_mon_cta").Value))
                Porcentaje = Trim(CDbl(monto_mon_cta) / lPatrimonio * 100)
                .TableCell(tcText, iFila, 2) = FormatNumber(Trim(Porcentaje), 2, True) & "%"
                .TableCell(tcText, iFila, 3) = FormatNumber(Trim(lReg("monto_mon_cta").Value), 2)
            
            If monto_mon_cta <> 0 Then
                ReDim Preserve sNombres(i)
                ReDim Preserve sValores(i)

                sNombres(i) = Dsc_Instrumento
                sValores(i) = monto_mon_cta
                i = i + 1
            End If
        Next
        iFila = .TableCell(tcRows) + 1
        Dsc_Instrumento = "Caja Neta"
        monto_mon_cta = lCaja_Cuenta_Neta
        
        If monto_mon_cta <> 0 And lPatrimonio <> 0 Then
            Porcentaje = Round((monto_mon_cta / lPatrimonio) * 100, 2)
        Else
            Porcentaje = 0
        End If
        
        .TableCell(tcText, iFila, 1) = Dsc_Instrumento
        .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
        .TableCell(tcText, iFila, 2) = FormatNumber(Porcentaje, 2, True) & "%"
        .TableCell(tcText, iFila, 3) = FormatNumber(monto_mon_cta, 0)


        iFila = .TableCell(tcRows) + 1
        .TableCell(tcRows) = iFila
        
        If monto_mon_cta > 0 Then
            ReDim Preserve sNombres(i)
            ReDim Preserve sValores(i)

            sNombres(i) = Dsc_Instrumento
            sValores(i) = monto_mon_cta
            i = i + 1
        End If
        
        .TableCell(tcRowBorderAbove, iFila) = 2
        Dsc_Instrumento = "Caja"
        monto_mon_cta = lCaja_Cuenta
        
        .TableCell(tcText, iFila, 1) = Dsc_Instrumento
        .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
        .TableCell(tcText, iFila, 3) = FormatNumber(monto_mon_cta, 0)
        iFila = .TableCell(tcRows) + 1
        .TableCell(tcRows) = iFila
                
        Dsc_Instrumento = "Cuentas por cobrar"
        monto_mon_cta = lCaja_Cuentas_por_Cobrar
        
        .TableCell(tcText, iFila, 1) = Dsc_Instrumento
        .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
        .TableCell(tcText, iFila, 3) = FormatNumber(monto_mon_cta, 0)
        
        iFila = .TableCell(tcRows) + 1
        .TableCell(tcRows) = iFila
        
        Dsc_Instrumento = "Cuentas por pagar"
        monto_mon_cta = lCaja_Cuentas_por_Pagar
        
        .TableCell(tcText, iFila, 1) = Dsc_Instrumento
        .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
        .TableCell(tcText, iFila, 3) = FormatNumber(monto_mon_cta, 0)
        
        
        
        .EndTable
    End With
    
    Dim j As Integer
    Dim aux As Double
    Dim aux1 As String

    iFilaActual = vp.CurrentY

    If i > 0 Then
        For i = LBound(sValores) To UBound(sValores)
            For j = i + 1 To UBound(sValores) - 1
                If CDbl(sValores(i)) > CDbl(sValores(j)) Then
                    aux = sValores(j)
                    sValores(j) = sValores(i)
                    sValores(i) = aux

                    aux1 = sNombres(j)
                    sNombres(j) = sNombres(i)
                    sNombres(i) = aux1
                End If
            Next
        Next

        sName = fncGraficoTorta(sNombres, sValores, "Composici�n Cartera", 11, False)

        With vp
            .CurrentY = iFila
            nMargenLeft = .MarginLeft
            .MarginLeft = COLUMNA_DOS
            .DrawPicture LoadPicture(sName), vp.MarginLeft + rTwips("2mm"), iFilaComienzo, "90mm", "55mm", , False

             .MarginLeft = nMargenLeft

             .CurrentY = FilaAux
        End With
    End If
    
    '############################## ACTIVOS POR MONEDA ######################################
    iFilaTermino = iFilaActual
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub
Function Monto_Moneda(ByVal sCodMoneda As String) As String
    Dim s As String
    s = UCase(Trim(sCodMoneda))
    Select Case s
        Case "PESO"
            Monto_Moneda = "$"
        Case "DO"
            Monto_Moneda = "USD"
        Case Else
            Monto_Moneda = s
    End Select
End Function
Function Dame_Decimales(ByVal s As String)
    Select Case s
        Case "PESO"
            Dame_Decimales = 0
        Case Else
            Dame_Decimales = 2
    End Select
End Function

Function fncGraficoTorta(catnom, valores, stitulo, tipo, bLeyenda)
    Dim xColorTitulo As String
    Dim xColor As String
    Dim aColors()
    Dim cd, c
    Dim Path As String
    Dim sFname As String
    
    On Local Error Resume Next
    xColorTitulo = ColorToHex(0, 0, 0)
    xColor = ColorToHex(RColorRelleno, GColorRelleno, BColorRelleno)

    aColors = Array(ColorToHex(44, 41, 38), _
                    ColorToHex(18, 17, 15), _
                    ColorToHex(196, 183, 169), _
                    ColorToHex(81, 41, 38), _
                    ColorToHex(248, 245, 241), _
                    ColorToHex(138, 128, 119), _
                    ColorToHex(238, 245, 241), _
                    ColorToHex(189, 184, 179), _
                    ColorToHex(214, 200, 186), _
                    ColorToHex(194, 181, 167), _
                    ColorToHex(196, 183, 169), _
                    ColorToHex(64, 61, 48) _
                    )

    
    Set cd = CreateObject("ChartDirector.API")
    
    Set c = cd.PieChart(1000, 350)

    Call c.setPieSize(500, 140, 250)

    Call c.set3D(40, 75)
    
    'Call c.addLegend(330, 40)
    
    If tipo <> -1 Then
        Call c.setLabelFormat("<*size=11*><*block*>{label}<*br*>{percent}%<*/*>")
    Else
        Call c.setLabelFormat("<*size=11*><*block*>{label} {percent}%<*/*>")
    End If

    Call c.setStartAngle(120, False)

    Call c.SetData(valores, catnom)

    'sTitulo= "<*size=12*><*block*><*color=" & xColorTitulo & "*>" & sTitulo & "<*/*>"

    Call c.addTitle(stitulo, "arial.ttf", 16, xColorTitulo)
    
    Call c.setLabelPos(50, cd.LineColor)

    Call c.setColors2(cd.DataColor, aColors)
    ' Call c.setColors(aColors)

    'Path = App.Path & "\" '    App.Path ' Left(Request.ServerVariables("PATH_INFO"), InStrRev(Request.ServerVariables("PATH_INFO"), "/graficos/"))
    
    'MODIFICADO PARA QUE EL SISTEMA DEJE EN UN DIRECTORIO TEMPORAL EL JPG QUE SE GENERA DEBIDO A QUE ESTABA
    'ENSUCIANDO EL DIRECTORIO DE INSTALACION DEL SISTEMA, LO CUAL, ERA INCORRECTO.�
    'HAF: 20070620.
    Path = Environ$("TEMP") & "\"

    sFname = c.makeTmpFile(Path & sFname, cd.JPG)

    fncGraficoTorta = Path & sFname

End Function
Function ColorToHex(r, g, b)
    ColorToHex = "&H" & Hex(r) & Hex(g) & Hex(b)
End Function
Function fncGraficoValorCuota() As String
    Dim oRs             As ADODB.Recordset
    Dim xSql
    Dim sName           As String
    Dim stitulo
    Dim fechainicrent
    Dim fechabaserent
    Dim lCursor_Cuotas  As hRecord
    Dim lCampo          As hFields
    
    Dim catnom(), Valor()
    Dim catnomc(), valorC()
    Dim catnom3(), valor3()

    Dim sFechaMenor
    Dim sFechaMayor

    Dim iValorMenor
    Dim iValorMayor

    Dim nRentabilidad_diaria
    Dim iBase           As Integer
    Dim i               As Integer
    
    'fechainicrent = DameInicioDeMes(oCliente.Fecha_Cartola)  ' "01/01/2006" ' & Year(glb_fecha_hoy)
    fechainicrent = CDate(oCliente.Fecha_Cartola) - 365
    fechabaserent = oCliente.Fecha_Cartola
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pid_cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_inicio", ePT_Numero, fechainicrent, ePD_Entrada
    gDB.Parametros.Add "Pfecha_final", ePT_Numero, fechabaserent, ePD_Entrada
    gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS.RENTABILIDAD_PERIODOS"
    
    If gDB.EjecutaSP Then
        Set lCursor_Cuotas = gDB.Parametros("pcursor").Valor
        If lCursor_Cuotas.Count > 0 Then
            iBase = 100
            i = 0
            For Each lCampo In lCursor_Cuotas
            
                If NVL(lCampo("rentabilidad_mon_cuenta").Value, 0) = 0 Then
                    nRentabilidad_diaria = 0
                Else
                    nRentabilidad_diaria = CDbl(lCampo("rentabilidad_mon_cuenta").Value)
                End If
                
                If nRentabilidad_diaria <> 0 Then
                    ReDim Preserve catnom(i)
                    ReDim Preserve Valor(i)
        
                    ReDim Preserve catnomc(i)
                    ReDim Preserve valorC(i)
                    ReDim Preserve catnom3(i)
                    ReDim Preserve valor3(i)
                    
                    If i = 0 Then
                        sFechaMenor = lCampo("fecha_cierre").Value
                        sFechaMayor = lCampo("fecha_cierre").Value
        
                        iValorMenor = iBase
                        iValorMayor = iBase
        
                        catnom(i) = lCampo("fecha_cierre").Value
                        Valor(i) = iBase
                    Else
                        catnom(i) = lCampo("fecha_cierre").Value
                        Valor(i) = Valor(i - 1) * (1 + nRentabilidad_diaria / 100)
                    End If
        
                    If sFechaMenor > lCampo("fecha_cierre").Value Then
                        sFechaMenor = lCampo("fecha_cierre").Value
                    End If
        
                    If sFechaMayor < lCampo("fecha_cierre").Value Then
                        sFechaMayor = lCampo("fecha_cierre").Value
                    End If
        
                    If iValorMenor > Valor(i) Then
                        iValorMenor = Valor(i)
                    End If
        
                    If iValorMayor < Valor(i) Then
                        iValorMayor = Valor(i)
                    End If
                    
                    catnom(i) = Format(lCampo("fecha_cierre").Value, "dd/mm/yy") 'Mid(DameMes(lCampo("fecha_cierre").Value), 1, 3) & "-" & Year(lCampo("fecha_cierre").Value)
                    catnomc(i) = lCampo("fecha_cierre").Value & "-"
                    valorC(i) = CDbl(lCampo("valor_cuota_mon_cuenta").Value)
                    
                    i = i + 1
                    
                End If
            Next
        End If
    Else
        MsgBox gDB.Parametros.Errnum & vbCr & gDB.Parametros.ErrMsg, vbCritical
    End If
    
    gDB.Parametros.Clear

    
    Dim nSemestres As Double
    Dim iDiferencia As Double
    Dim bLineaTendencia As Boolean

    stitulo = "VALOR CUOTA/BASE 100"

    bLineaTendencia = False

    
    sName = GraficoLineas(catnom, Valor, stitulo, False, i - 1, nSemestres, bLineaTendencia, iValorMenor, iValorMayor)

    fncGraficoValorCuota = sName

End Function

Function GraficoLineas(Categories1, Vals1, stitulo, leyenda, entries, nSemestres, bLineaTendencia, iValorMenor, iValorMayor) As String
    Dim cd
    Dim c
    Dim xColor          As String
    Dim xColorTitulo    As String
    Dim noOfPoints      As Integer
    Dim Path            As String
    Dim sFname As String
    'Dim sTitulo
    Dim x As Integer
    Dim y As Integer
    On Local Error Resume Next
    xColorTitulo = ColorToHex(0, 0, 0)
    
    noOfPoints = 1 ' UBound(Vals1) - 1
    
    Set cd = CreateObject("ChartDirector.API")
    
    cd.setLicenseCode ("YT5ZJ59NKAF9ZR3942DAE53E")
    
    x = 450
    y = 320
    
    Set c = cd.XYChart(x, y) ' , cd.metalColor(&HDEDEDE), -1, 2)


    Call c.setPlotArea(50, 60, x - 60, y - 90)
    Call c.yAxis().setLabelStyle("arialbd.ttf", 7, &H555555)
    Call c.yAxis().setLabelFormat("{value|4.}")
    Call c.yAxis().setLinearScale(iValorMenor, iValorMayor)
    Call c.xAxis().setLabels(Categories1)
    Call c.xAxis().setLabelStyle("arialbd.ttf", 7, &H555555, 30).setpos(0, -1)
    Call c.xAxis().setLabelStep(30)
    Call c.addLineLayer(Vals1, 0)
    stitulo = "<*size=14,yoffset=-11*><*block*><*color=" & xColorTitulo & "*>" & stitulo & "<*br*><*/*>"
    Call c.addTitle(stitulo, "GILB____.TTF", 14, xColorTitulo)  ' .setBackground(xColor)

    'MODIFICADO PARA QUE EL SISTEMA DEJE EN UN DIRECTORIO TEMPORAL EL JPG QUE SE GENERA DEBIDO A QUE ESTABA
    'ENSUCIANDO EL DIRECTORIO DE INSTALACION DEL SISTEMA, LO CUAL, ERA INCORRECTO.�
    'HAF: 20070620.
    Path = Environ$("TEMP") & "\"

    sFname = c.makeTmpFile(Path & sFname, cd.JPG)
    
    GraficoLineas = Path & sFname

End Function
Sub FlujoPatrimonial()
    Dim lCursor As hRecord
    Dim lReg As hFields
    Dim iFila As Integer
    Dim Xinicio As Variant
    Dim YInicio As Variant
    Dim i As Integer
    Dim j As Integer
    Dim lSaldo_neto_aporte_retiro As Double
    Dim lSaldo_neto_aporte_retiro_uf As Double
    'PKG_CAJAS_CUENTA$Buscar
    Dim lcPatrimonio_Cuenta As Class_Patrimonio_Cuentas
    
    Set lcPatrimonio_Cuenta = New Class_Patrimonio_Cuentas
    
    With lcPatrimonio_Cuenta
        .Campo("id_cuenta").Valor = oCliente.IdCuenta
        .Campo("FECHA_CIERRE").Valor = oCliente.Fecha_Cartola
        
        If .Buscar Then
            If .Cursor.Count > 0 Then
                lSaldo_neto_aporte_retiro = NVL(.Cursor(1)("SALDO_NETO_APORTE_RETIRO").Value, 0)
                lSaldo_neto_aporte_retiro_uf = NVL(.Cursor(1)("SALDO_NETO_APORTE_RETIRO_uf").Value, 0)
                
            Else
                MsgBox "No hay Cierres Vigentes", vbInformation, Me.Caption
                GeneraCartola = False
                'GoTo ErrProcedure
            End If
        End If
    End With
    Set lcPatrimonio_Cuenta = Nothing
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_inicio", ePT_Fecha, CDate(oCliente.Fecha_Creacion), ePD_Entrada
    gDB.Parametros.Add "Pfecha_final", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
    gDB.Procedimiento = "PKG_APORTE_RESCATE_CTA$Buscar_Entre"

    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor = gDB.Parametros("Pcursor").Valor
    With vp
        vp.MarginLeft = COLUMNA_INICIO    ' + rTwips("150mm")
        Xinicio = .MarginLeft
        'YInicio = .CurrentY + rTwips("2mm")
        YInicio = .CurrentY - rTwips("0.9mm")

        .CurrentY = YInicio
        FilaAux = .CurrentY

        .StartTable
        .TableBorder = tbBox

        .TableCell(tcCols) = 5

        .TableCell(tcRows) = 9

        .TableCell(tcFontSize, 1, 1, 1, 3) = glb_tamletra_registros
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcBackColor, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcBackColor, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)

        .TableCell(tcText, 1, 1) = "APORTES Y RETIROS"
        '.TableCell(tcColWidth, , 1) = "60mm"
        .TableCell(tcColSpan, 1, 1) = 2
        '.TableCell(tcAlign, 1, 1) = taLeftMiddle
        .TableCell(tcText, 2, 1) = "FECHA"
        .TableCell(tcColSpan, 2, 2) = 2
        .TableCell(tcText, 2, 2) = "APORTES"
        .TableCell(tcColSpan, 2, 4) = 2
        .TableCell(tcText, 2, 4) = "RETIROS"

        ' Ancho de cada columna
        .TableCell(tcColWidth, 1, 1) = "35mm"
        .TableCell(tcColWidth, 1, 2) = "30mm"
        .TableCell(tcColWidth, 1, 3) = "25mm"
        .TableCell(tcColWidth, 1, 4) = "30mm"
        .TableCell(tcColWidth, 1, 5) = "25mm"
        
        .TableCell(tcText, 3, 1) = "OPERACI�N"
        
        .TableCell(tcText, 3, 2) = "$"
        .TableCell(tcText, 3, 3) = "UF"
        
        .TableCell(tcText, 3, 4) = "$"
        .TableCell(tcText, 3, 5) = "UF"
        
        

        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        
        i = 1
        .TableCell(tcAlign, 2) = taCenterMiddle
        .TableCell(tcAlign, 3) = taCenterMiddle
        For Each lReg In lCursor
        
            If i = 5 Then
                Exit For
            End If
            iFila = i + 3
            If lReg("dsc_mov").Value <> "" Then
              .TableCell(tcText, iFila, 1) = lReg("FECHA_MOVIMIENTO") & " (" & lReg("dsc_mov").Value & ")"
            Else
              .TableCell(tcText, iFila, 1) = lReg("FECHA_MOVIMIENTO")
            End If
            

            If lReg("FLG_TIPO_MOVIMIENTO") = "A" Then
                  .TableCell(tcText, iFila, 2) = FormatNumber(lReg("monto").Value, 2)
                  .TableCell(tcText, iFila, 3) = FormatNumber(lReg("monto_uf").Value, 2)
                  .TableCell(tcText, iFila, 4) = "-"
                  .TableCell(tcText, iFila, 5) = "-"
            Else
                  .TableCell(tcText, iFila, 2) = "-"
                  .TableCell(tcText, iFila, 3) = "-"
                  .TableCell(tcText, iFila, 4) = FormatNumber(lReg("monto").Value, 2)
                  .TableCell(tcText, iFila, 5) = FormatNumber(lReg("monto_uf").Value, 2)
            End If
  
            '.TableCell(tcRows) = .TableCell(tcRows) + 1
            
            i = i + 1
        Next
        
        
        '.TableCell(tcRows) = .TableCell(tcRows) + 1
        'iFila = .TableCell(tcRows)
        

        iFila = 8
        
        .TableCell(tcRowBorderBelow, iFila - 1) = 2
       '.TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcText, iFila, 1) = "SALDO NETO APORTES RETIROS"
        .TableCell(tcBackColor, iFila) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcColSpan, iFila, 1) = 2
        If lSaldo_neto_aporte_retiro > 0 Then
        .TableCell(tcText, iFila + 1, 2) = "$"
        .TableCell(tcText, iFila + 1, 3) = "UF"
        .TableCell(tcText, iFila + 2, 2) = FormatNumber(lSaldo_neto_aporte_retiro, 2)
        .TableCell(tcText, iFila + 2, 3) = FormatNumber(lSaldo_neto_aporte_retiro_uf, 2)
        .TableCell(tcBackColor, iFila + 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        Else
        .TableCell(tcText, iFila + 1, 4) = "$"
        .TableCell(tcText, iFila + 1, 5) = "UF"
        .TableCell(tcText, iFila + 2, 4) = FormatNumber(lSaldo_neto_aporte_retiro, 2)
        .TableCell(tcText, iFila + 2, 5) = FormatNumber(lSaldo_neto_aporte_retiro_uf * -1, 2)
        .TableCell(tcBackColor, iFila + 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        End If
        
        
        .TableCell(tcAlign, , 1) = taLeftMiddle
        .TableCell(tcAlign, , 2) = taRightMiddle
        .TableCell(tcAlign, , 3) = taRightMiddle
        .TableCell(tcAlign, , 4) = taRightMiddle
        .TableCell(tcAlign, , 5) = taRightMiddle
        
        .TableCell(tcAlign, 2, 1) = taCenterMiddle
        .TableCell(tcAlign, 2, 2) = taCenterMiddle
        .TableCell(tcAlign, 2, 4) = taCenterMiddle
        
        .TableCell(tcAlign, 2, 1) = taCenterMiddle
        .TableCell(tcAlign, 2, 2) = taCenterMiddle
        .TableCell(tcAlign, 2, 3) = taCenterMiddle
        .TableCell(tcAlign, 2, 4) = taCenterMiddle
        .TableCell(tcAlign, 2, 5) = taCenterMiddle
        
              
                

        .EndTable

    End With

    Set lCursor = Nothing
    Set lReg = Nothing
End Sub


Sub AsignaTitulos(ByRef oColumnas As Class_Cartola_Campos, sProducto As String)
    oColumnas.CleanDetalle


    Select Case sProducto
        Case gcPROD_RV_NAC
            'MENU DE ACCIONES'
            With oColumnas
                .AddColumna "nemotecnico", "Nemot�cnico", taLeftMiddle, False, , , 40
                .AddColumna "Porcentaje_inversion", "%" & vbCrLf & " de Inversi�n", taRightMiddle, True, , 2, 30, True
                .AddColumna "dummy", "", taRightMiddle, , , 0, 10
                .AddColumna "Nominal", "Cantidad", taRightMiddle, True, , 0, 20
                .AddColumna "precio_compra", "Precio Compra", taRightMiddle, True, , 2, 32
                .AddColumna "precio", "Precio Actual", taRightMiddle, True, , 2, 33
               ' .AddColumna "precio_promedio_compra", "Costo Hist�rico", taRightMiddle, True, , 2, 35
                .AddColumna "MONTO_MON_CTA", "Valorizaci�n", taRightMiddle, True, "", 2, 30, True
                .AddColumna "rentabilidad", "Rentabilidad", taRightMiddle, True, , 2, 25
            End With
            
        Case gcPROD_RF_NAC
            'MENU DE RENTA FIJA
            With oColumnas
                .AddColumna "nemotecnico", "Nemot�cnico", taLeftMiddle, False, , , 25
                .AddColumna "DSC_EMISOR_ESPECIFICO", "Emisor", taLeftMiddle, False, , , 42
                .AddColumna "Porcentaje_inversion", "% de" & vbCrLf & "Inversi�n", taRightMiddle, False, , 2, 18, True
                .AddColumna "DSC_MONEDA_SIMBOLO", "Moneda", taCenterMiddle, True, "", 2, 16
                .AddColumna "Nominal", "Valor" & vbCrLf & "Nominal", taRightMiddle, True, "", 0, 23
                .AddColumna "tasa_cupon", "Tasa" & vbCrLf & "Cupon", taRightMiddle, True, "", 2, 18
                .AddColumna "Fecha_Vencimiento", "Fecha" & vbCrLf & "Vcto.", taCenterMiddle, False, , 0, 18
                .AddColumna "TASA_COMPRA", "Tasa" & vbCrLf & "Compra", taRightMiddle, True, , 2, 20
                .AddColumna "precio_Fecha_compra", "Precio" & vbCrLf & "Compra", taRightMiddle, True, , 2, 23
                .AddColumna "tasa", "Tasa" & vbCrLf & "Mercado", taRightMiddle, True, , 2, 22
                .AddColumna "MONTO_MON_CTA", "Valorizaci�n", taRightMiddle, True, , 2, 30, True
            End With
            
        Case gcPROD_FFMM_NAC
            'MENU DE FONDOS MUTUOS
            With oColumnas
                .AddColumna "nemotecnico", "Nemot�cnico", taLeftMiddle, False, , 0, 55
                .AddColumna "DSC_MONEDA_SIMBOLO", "Moneda", taCenterMiddle, False, , 0, 24
                ' .AddColumna "dummy", "", taRightMiddle, , , 0, 5
                .AddColumna "Porcentaje_inversion", "%" & vbCrLf & "de Inversi�n", taRightMiddle, True, , 2, 25, True
                .AddColumna "dummy", "", taRightMiddle, , , 0, 8
                .AddColumna "CANTIDAD", "Numero" & vbCrLf & "Cuotas", taRightMiddle, True, "#,###,##0", 2, 26
                .AddColumna "dummy", "", taRightMiddle, , , 0, 9
                .AddColumna "tasa_compra", "Valor" & vbCrLf & "Cuota Compra", taRightMiddle, True, , 2, 30
                .AddColumna "dummy", "", taRightMiddle, , , 0, 8
                .AddColumna "precio", "Valor" & vbCrLf & "Cuota Actual", taRightMiddle, True, "#,###,##0.00", 2, 30
                .AddColumna "dummy", "", taRightMiddle, , , 0, 10
                .AddColumna "MONTO_MON_CTA", "Valorizaci�n", taRightMiddle, True, "#,###,##0.00", 2, 30, True
                
            End With
            
    End Select

End Sub

Private Function FormatPorcentaje(Valor As Double, Valor_Total As Double, Optional ByRef nValorAsignado As Double = 0) As String
    If Valor_Total <> 0 Then
        FormatPorcentaje = FormatNumber((Valor / Valor_Total) * 100, 2) & "%"
        nValorAsignado = (Valor / Valor_Total) * 100
    Else
        FormatPorcentaje = FormatNumber(0, 2) & "%"
        nValorAsignado = 0
    End If
End Function

Private Sub Sub_Generar_Reporte_2()
    Const clrHeader = &HD0D0D0
    '------------------------------------------
    Const sHeader_SA = "Nemot�cnico|Cantidad|Precio|Valor Mercado|Valor Mercado Cuenta|"
    Const sFormat_SA = "2900|>1600|>1100|>2500|>2500|3600"
    Const sHeader_Detalle_Caja = "Fecha Mov.|Fecha Liq.|Origen Mov.|Movimientos|Ingreso|Egreso|Saldo"
    Const sFormat_Detalle_Caja = "1200|1100|1950|4550|>1800|>1800|>1800"
    '------------------------------------------
    Dim sRecord
    Dim bAppend
    Dim lLinea As Integer
    '------------------------------------------
    Dim lPatrimonio As Class_Patrimonio_Cuentas
    Dim lReg_Pat As hFields
    Dim lPatrim As String
    Dim lRentabilidad As Double
    '------------------------------------------
    Dim lCajas_Ctas As Class_Cajas_Cuenta
    Dim lCursor_Caj As hRecord
    Dim lCursor_Caja As hRecord
    Dim lReg_Caj As hFields
    Dim lReg_Caja As hFields
    Dim lMonto_Mon_Caja As String
    '------------------------------------------
    Dim lSaldos_Activos As Class_Saldo_Activos
    Dim lCursor_SA As hRecord
    Dim lReg_SA As hFields
    Dim lReg_SN As hFields
    Dim lCursor_SN As hRecord
    
    Dim lCuenta_Lineas As Integer
    Dim lTope As Integer
    
    
    '------------------------------------------
    Dim lParidad As Double
    Dim lOperacion As String
    Dim lMonto_Mon_Cta As String
    '------------------------------------------
'    Dim lcTipo_Cambio As Class_Tipo_Cambios
    Dim lcTipo_Cambio As Object
    '------------------------------------------
    Dim lInstrumento As Class_Instrumentos
    Dim lCursor_Ins As hRecord
    Dim lReg_Ins As hFields
    Dim lHay_Activos As Boolean
    '------------------------------------------
    Dim lSaldos_Caja As Class_Saldos_Caja
    
    '--------------------------------------
    Dim lReg As hCollection.hFields
'    Dim lcCuenta As Class_Cuentas
    Dim lcCuenta As Object
    Dim lcMov_Caja As Class_Mov_Caja
    Dim lcSaldos_Caja As Class_Saldos_Caja
    '--------------------------------------
    Dim lFecDesde As Date
    Dim lFecHasta As Date
    Dim lId_Caja_Cuenta As String
    Dim lTotal As Double
    Dim lIngreso As Double
    Dim lEgreso As Double
    Dim lUltimaCaja As Long
    Dim nDecimales As Integer
    Dim iFila As Integer
    Dim i As Integer
    Dim anterior As Integer
    Dim Mostrado As Integer
    '------------------------------------------
    Dim Monto As Double
    Dim Porcentaje As Double
    Call Sub_Bloquea_Puntero(Me)
    Dim cont As Integer
    
    If Not Fnt_Form_Validar(Me.Controls) Then
        GoTo ErrProcedure
    End If
           
    Rem Busca el patrimonio de la cuenta y su rentabilidad
    Set lPatrimonio = New Class_Patrimonio_Cuentas
    With lPatrimonio
        .Campo("id_cuenta").Valor = lId_Cuenta
        .Campo("FECHA_CIERRE").Valor = DTP_Fecha_Ter.Value
        'Valores por defecto
        .Campo("RENTABILIDAD_MON_CUENTA").Valor = Null
        .Campo("APORTE_RETIROS_MON_CUENTA").Valor = Null
        
        If .Buscar Then
            For Each lReg_Pat In .Cursor
                lPatrim = FormatNumber(lReg_Pat("PATRIMONIO_MON_CUENTA").Value, nDecimales)
                lRentabilidad = NVL(lReg_Pat("RENTABILIDAD_MON_CUENTA").Value, 0)
            Next
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            GoTo ErrProcedure
        End If
    End With
  
    Rem Busca las cajas de la cuenta
    Set lCajas_Ctas = New Class_Cajas_Cuenta
    
    With lCajas_Ctas
        .Campo("id_cuenta").Valor = lId_Cuenta
        If .Buscar(True) Then
            Set lCursor_Caj = .Cursor
            Set lCursor_Caja = .Cursor
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            GoTo ErrProcedure
        End If
    End With
  
    Rem Busca los Instrumentos del sistema
    Set lInstrumento = New Class_Instrumentos
    With lInstrumento
        If .Buscar Then
            Set lCursor_Ins = .Cursor
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            GoTo ErrProcedure
        End If
    End With
  
    Rem Comienzo de la generaci�n del reporte
    lLinea = 2
  
    With vp
  
      
    
    lHay_Activos = False
    
    anterior = 0
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "Pid_empresa", ePT_Numero, oCliente.IDEmpresa, ePD_Entrada
    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA"

    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor_Ins = gDB.Parametros("Pcursor").Valor
    With vp
        vp.MarginLeft = COLUMNA_INICIO    ' + rTwips("150mm")
         FilaAux = .CurrentY
        
        cont = 0
        For Each lReg In lCursor_Ins
            
            Monto = 0
            Porcentaje = 0
            If CDbl(NVL(lReg("monto_mon_cta_inst").Value, 0)) <> 0 Then
                      If anterior = lReg("id_arbol_clase_inst").Value Then
                      '.NewPage
                      '.Paragraph = ""
                      Else
                          If lReg("codigo").Value = "FFMM" Then
                              cont = cont + 1
                              If cont = 1 Then
                                .NewPage
                                .CurrentY = 2510
                                .CurrentY = vp.CurrentY + rTwips("2mm")
                                pon_titulo UCase("RESUMEN ACTIVOS"), "5mm", Font_Name, glb_tamletra_titulo
                                .CurrentY = vp.CurrentY + rTwips("2mm")
                                'MsgBox CurrentY
                                
                                FilaAux = vp.CurrentY
                                '.CurrentY = 2750
                                Else
                                .Paragraph = ""
                              End If
                                
                                
                          Else
                           'If anterior <> 0 Then
                              .NewPage
                              .CurrentY = 2510
                              .CurrentY = vp.CurrentY + rTwips("2mm")
                              pon_titulo UCase("RESUMEN ACTIVOS"), "5mm", Font_Name, glb_tamletra_titulo
                              .CurrentY = vp.CurrentY + rTwips("2mm")
                              FilaAux = vp.CurrentY
                   
                              .TableBorder = tbBox
                              '.CurrentY = 2750
                             
                          End If
                      End If
                      
                     If CDbl(NVL(Trim(lReg("monto_mon_cta_hoja").Value), 0)) <> 0 And CInt(lReg("NIVEL").Value) = 0 Then
                    '.CurrentY = 3000
                    .FontSize = 10
                    .FontBold = True
                    .Paragraph = Trim(lReg("dsc_arbol_clase_inst").Value) & " - " & Trim(lReg("dsc_arbol_clase_inst_hoja").Value)
                    .FontSize = glb_tamletra_registros
                    .FontBold = False
                    .StartTable
                    .TableBorder = tbBox
                    .TableCell(tcCols) = 11
                    .TableCell(tcRows) = .TableCell(tcRows) + 1
                    .TableCell(tcFontSize, 1, 1, 1, 3) = glb_tamletra_registros
                    
                    .TableCell(tcText, 1, 2) = ""
                    '.TableCell(tcColSpan, 1, 4) = 2
                    .TableCell(tcText, 1, 4) = ""
                    ' Ancho de cada columna
                    
                    .TableCell(tcColWidth, 1, 1) = "60mm"
                    .TableCell(tcColWidth, 1, 2) = "25mm"
                    .TableCell(tcColWidth, 1, 3) = "25mm"
                    .TableCell(tcColWidth, 1, 4) = "30mm"
                    .TableCell(tcColWidth, 1, 5) = "30mm"
                    .TableCell(tcColWidth, 1, 6) = "30mm"
                    .TableCell(tcColWidth, 1, 7) = "30mm"
                    .TableCell(tcColWidth, 1, 8) = "25mm"
                    
                    If lReg("codigo").Value = "RV" Then
                    .TableCell(tcCols) = 8
                    .TableCell(tcText, 1, 1) = "NEMOTECNICO"
                    .TableCell(tcText, 1, 2) = "% DE INVERSION"
                    .TableCell(tcText, 1, 3) = "CANTIDAD"
                    .TableCell(tcText, 1, 4) = "PRECIO" & vbCrLf & "COMPRA"
                    '.TableCell(tcText, 1, 5) = "COSTO HISTORICO"
                    .TableCell(tcText, 1, 5) = "MONTO HIST�RICO"
                    .TableCell(tcText, 1, 6) = "PRECIO" & vbCrLf & "ACTUAL"
                    '.TableCell(tcText, 1, 8) = "VAL. MERCADO" & vbCrLf & "MON. PAPEL"
                    '.TableCell(tcText, 1, 9) = "VAL. MERCADO" & vbCrLf & "MON. CUENTA"
                    .TableCell(tcText, 1, 7) = "VALOR MERCADO"
                    .TableCell(tcText, 1, 8) = "RENTABILIDAD"
                    .TableCell(tcColWidth, 1, 1) = "70mm"
                    .TableCell(tcColWidth, 1, 2) = "20mm"
                    .TableCell(tcColWidth, 1, 3) = "25mm"
                    .TableCell(tcColWidth, 1, 4) = "25mm"
                    .TableCell(tcColWidth, 1, 5) = "25mm"
                    .TableCell(tcColWidth, 1, 6) = "25mm"
                    .TableCell(tcColWidth, 1, 7) = "40mm"
                    .TableCell(tcColWidth, 1, 8) = "25mm"
                    '.TableCell(tcColWidth, 1, 9) = "20mm"
                    '.TableCell(tcColWidth, 1, 9) = "25mm"
                    '.TableCell(tcColWidth, 1, 10) = "25mm"
                    
                    
                    ElseIf lReg("codigo").Value = "RF" Then
                    .TableCell(tcCols) = 11
                    .TableCell(tcText, 1, 1) = "NEMOTECNICO"
                    .TableCell(tcText, 1, 2) = "EMISOR"
                    .TableCell(tcText, 1, 3) = "% DE " & vbCrLf & "INVERSION"
                    .TableCell(tcText, 1, 4) = "MONEDA"
                    .TableCell(tcText, 1, 5) = "VALOR" & vbCrLf & " NOMINAL"
                    .TableCell(tcText, 1, 6) = "TASA" & vbCrLf & "CUP�N"
                    .TableCell(tcText, 1, 7) = "FECHA" & vbCrLf & " VCTO."
                    .TableCell(tcText, 1, 8) = "TASA" & vbCrLf & " COMPRA"
                    '.TableCell(tcText, 1, 9) = "PRECIO " & vbCrLf & "COMPRA"
                    .TableCell(tcText, 1, 9) = "TASA" & vbCrLf & "MERCADO"
                    .TableCell(tcText, 1, 10) = "VAL. MERCADO" & vbCrLf & "MONEDA PAPEL"
                    .TableCell(tcText, 1, 11) = "VAL. MERCADO" & vbCrLf & "MONEDA CUENTA"
                    '.TableCell(tcText, 1, 12) = "VALOR MERCADO"
               
                    .TableCell(tcColWidth, 1, 1) = "30mm"
                    .TableCell(tcColWidth, 1, 2) = "32mm"
                    .TableCell(tcColWidth, 1, 3) = "18mm"
                    .TableCell(tcColWidth, 1, 4) = "15mm"
                    .TableCell(tcColWidth, 1, 5) = "25mm"
                    .TableCell(tcColWidth, 1, 6) = "15mm"
                    .TableCell(tcColWidth, 1, 7) = "20mm"
                    .TableCell(tcColWidth, 1, 8) = "20mm"
                    '.TableCell(tcColWidth, 1, 9) = "20mm"
                    .TableCell(tcColWidth, 1, 9) = "20mm"
                    .TableCell(tcColWidth, 1, 10) = "30mm"
                    .TableCell(tcColWidth, 1, 11) = "30mm"
                    
                    ElseIf lReg("codigo").Value = "FFMM" Then
                    .TableCell(tcCols) = 9
                    .TableCell(tcText, 1, 1) = "NEMOTECNICO"
                    .TableCell(tcText, 1, 2) = "MONEDA"
                    .TableCell(tcText, 1, 3) = "% DE INVERSION"
                    .TableCell(tcText, 1, 4) = "N�MERO DE CUOTAS"
                    .TableCell(tcText, 1, 5) = "VALOR CUOTA COMPRA"
                    .TableCell(tcText, 1, 6) = "VALOR CUOTA ACTUAL"
                    .TableCell(tcText, 1, 7) = "VAL. MERCADO " & vbCrLf & "MON. PAPEL"
                    .TableCell(tcText, 1, 8) = "VAL. MERCADO " & vbCrLf & "MON. CUENTA"
                    .TableCell(tcText, 1, 9) = "RENTABILIDAD"
                    .TableCell(tcColWidth, 1, 1) = "50mm"
                    .TableCell(tcColWidth, 1, 2) = "20mm"
                    .TableCell(tcColWidth, 1, 3) = "20mm"
                    .TableCell(tcColWidth, 1, 4) = "25mm"
                    .TableCell(tcColWidth, 1, 5) = "25mm"
                    .TableCell(tcColWidth, 1, 6) = "30mm"
                    .TableCell(tcColWidth, 1, 7) = "30mm"
                    .TableCell(tcColWidth, 1, 8) = "30mm"
                    .TableCell(tcColWidth, 1, 9) = "25mm"
                    
                    End If
                      .TableCell(tcAlign, 1, 1) = taCenterMiddle
                      .TableCell(tcAlign, 1, 2) = taCenterMiddle
                      .TableCell(tcAlign, 1, 3) = taCenterMiddle
                      .TableCell(tcAlign, 1, 4) = taCenterMiddle
                      .TableCell(tcAlign, 1, 5) = taCenterMiddle
                      .TableCell(tcAlign, 1, 6) = taCenterMiddle
                      .TableCell(tcAlign, 1, 7) = taCenterMiddle
                      .TableCell(tcAlign, 1, 8) = taCenterMiddle
                      .TableCell(tcAlign, 1, 9) = taCenterMiddle
                      .TableCell(tcAlign, 1, 10) = taCenterMiddle
                      .TableCell(tcAlign, 1, 11) = taCenterMiddle
                    

                    
                    .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                    .TableCell(tcBackColor, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                                       
                                       
                    gDB.Parametros.Clear
                    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
                    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
                    gDB.Parametros.Add "PID_ARBOL_CLASE", ePT_Numero, lReg("id_arbol_clase_inst_hoja").Value, ePD_Entrada
                    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
                    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO"
              
                    If Not gDB.EjecutaSP Then
                    Exit Sub
                    End If
              
                    Set lCursor_SA = gDB.Parametros("Pcursor").Valor
             
                      vp.MarginLeft = COLUMNA_INICIO    ' + rTwips("150mm")
                      FilaAux = .CurrentY
                      
              
                      For Each lReg_SA In lCursor_SA
                      .TableCell(tcRows) = .TableCell(tcRows) + 1
                      iFila = .TableCell(tcRows)
                      
                       If lReg("codigo").Value = "RV" Then
                       
                        .TableCell(tcText, iFila, 1) = Trim(lReg_SA("nemotecnico").Value)
                        .TableCell(tcText, iFila, 2) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
                        .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SA("cantidad").Value, 0)
                        .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("precio_compra").Value, 2)
                        .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_promedio_compra").Value, 2)
                        '.TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio_promedio_compra").Value * lReg_SA("cantidad").Value, 2)
                        .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio").Value, 2)
                        '.TableCell(tcText, iFila, 8) = FormatNumber(lReg_SA("monto_mon_nemotecnico").Value, 2)
                        .TableCell(tcText, iFila, 7) = FormatNumber(lReg_SA("monto_mon_cta").Value, 2)
                        
                        .TableCell(tcText, iFila, 8) = IIf(CDbl(lReg_SA("rentabilidad").Value) > 1000, "-", FormatNumber(lReg_SA("rentabilidad").Value, 2) & "%")
                        .TableCell(tcAlign, iFila, 2) = taRightMiddle
                        .TableCell(tcAlign, iFila, 3) = taRightMiddle
                        .TableCell(tcAlign, iFila, 4) = taRightMiddle
                        .TableCell(tcAlign, iFila, 5) = taRightMiddle
                        .TableCell(tcAlign, iFila, 6) = taRightMiddle
                        .TableCell(tcAlign, iFila, 7) = taRightMiddle
                        .TableCell(tcAlign, iFila, 8) = taRightMiddle
                        '.TableCell(tcAlign, iFila, 9) = taRightMiddle
                        '.TableCell(tcAlign, iFila, 10) = taRightMiddle
                        Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
                        Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
                        
                      ElseIf lReg("codigo").Value = "FFMM" Then
                        .TableCell(tcText, iFila, 1) = Trim(lReg_SA("nemotecnico").Value)
                        .TableCell(tcText, iFila, 2) = Trim(lReg_SA("simbolo_moneda").Value)
                        .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
                        .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SA("cantidad").Value, 4)
                        .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("precio_compra").Value, 4)
                        .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio").Value, 4)
  '                      .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("precio_promedio_compra").Value, 2)
                        .TableCell(tcText, iFila, 7) = FormatNumber(lReg_SA("monto_mon_nemotecnico").Value, Decimales_Moneda(Trim(lReg_SA("simbolo_moneda").Value)))
                        .TableCell(tcText, iFila, 8) = FormatNumber(lReg_SA("monto_mon_cta").Value, oCliente.Decimales)
                        .TableCell(tcText, iFila, 9) = IIf(CDbl(lReg_SA("rentabilidad").Value) > 1000, "-", FormatNumber(lReg_SA("rentabilidad").Value, 2) & "%")
                        .TableCell(tcAlign, iFila, 2) = taCenterMiddle
                        .TableCell(tcAlign, iFila, 3) = taRightMiddle
                        .TableCell(tcAlign, iFila, 4) = taRightMiddle
                        .TableCell(tcAlign, iFila, 5) = taRightMiddle
                        .TableCell(tcAlign, iFila, 6) = taRightMiddle
                        .TableCell(tcAlign, iFila, 7) = taRightMiddle
                        .TableCell(tcAlign, iFila, 8) = taRightMiddle
                        .TableCell(tcAlign, iFila, 9) = taRightMiddle
                        Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
                        Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
                                          
                      ElseIf lReg("codigo").Value = "RF" Then
                        .TableCell(tcText, iFila, 1) = Trim(lReg_SA("nemotecnico").Value)
                        .TableCell(tcText, iFila, 2) = Left(Trim(lReg_SA("emisor").Value), 16)
                        .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SA("porcentaje_rama").Value, 2) & "%"
                        .TableCell(tcText, iFila, 4) = Trim(lReg_SA("SIMBOLO_MONEDA").Value)
                        .TableCell(tcText, iFila, 5) = FormatNumber(lReg_SA("cantidad").Value, 0)
                        .TableCell(tcText, iFila, 6) = FormatNumber(lReg_SA("tasa_emision").Value, 2)
                        .TableCell(tcText, iFila, 7) = Trim(lReg_SA("fecha_vencimiento").Value)
                        .TableCell(tcText, iFila, 8) = FormatNumber(lReg_SA("tasa_compra").Value, 2)
                        .TableCell(tcText, iFila, 9) = FormatNumber(lReg_SA("tasa").Value, 2)
                        .TableCell(tcText, iFila, 10) = FormatNumber(lReg_SA("monto_mon_nemotecnico").Value, 2)
                        .TableCell(tcText, iFila, 11) = FormatNumber(lReg_SA("monto_mon_cta").Value, 2)
                        .TableCell(tcAlign, iFila, 2) = taLeftMiddle
                        .TableCell(tcAlign, iFila, 3) = taRightMiddle
                        .TableCell(tcAlign, iFila, 4) = taCenterMiddle
                        .TableCell(tcAlign, iFila, 5) = taRightMiddle
                        .TableCell(tcAlign, iFila, 6) = taRightMiddle
                        .TableCell(tcAlign, iFila, 7) = taRightMiddle
                        .TableCell(tcAlign, iFila, 8) = taRightMiddle
                        .TableCell(tcAlign, iFila, 9) = taRightMiddle
                        .TableCell(tcAlign, iFila, 10) = taRightMiddle
                        .TableCell(tcAlign, iFila, 11) = taRightMiddle
                        Porcentaje = Porcentaje + CDbl(lReg_SA("porcentaje_rama").Value)
                        Monto = Monto + CDbl(lReg_SA("monto_mon_cta").Value)
                      End If

                      Next  ' Nemotecnicos
                      .TableCell(tcRows) = .TableCell(tcRows) + 1
                      iFila = iFila + 1
                      If lReg("codigo").Value = "RV" Then
                        .TableCell(tcText, iFila, 2) = FormatNumber(Porcentaje, 2) & "%"
                        .TableCell(tcAlign, iFila, 2) = taRightMiddle
                        .TableCell(tcText, iFila, 7) = FormatNumber(Monto, oCliente.Decimales)
                        .TableCell(tcAlign, iFila, 7) = taRightMiddle
                      ElseIf lReg("codigo").Value = "FFMM" Then
                        .TableCell(tcText, iFila, 3) = FormatNumber(Porcentaje, 2) & "%"
                        .TableCell(tcAlign, iFila, 3) = taRightMiddle
                        .TableCell(tcText, iFila, 8) = FormatNumber(Monto, oCliente.Decimales)
                        .TableCell(tcAlign, iFila, 8) = taRightMiddle
                        
                      ElseIf lReg("codigo").Value = "RF" Then
                        .TableCell(tcText, iFila, 3) = FormatNumber(Porcentaje, 2) & "%"
                        .TableCell(tcAlign, iFila, 3) = taRightMiddle
                        .TableCell(tcText, iFila, 11) = FormatNumber(Monto, oCliente.Decimales)
                        .TableCell(tcAlign, iFila, 11) = taRightMiddle
                      End If
                      .TableCell(tcBackColor, iFila) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                      .EndTable
                      
                      End If
                
                End If
                                  
                If lReg("codigo").Value = "RV" And anterior <> lReg("id_arbol_clase_inst").Value Then
                    '.NewPage
                    '.Paragraph = ""
                    Call PaginaDos
                End If
                anterior = lReg("id_arbol_clase_inst").Value
        
        Next
        
        .NewPage
        .CurrentY = 2510
        .CurrentY = vp.CurrentY + rTwips("2mm")
        pon_titulo UCase("MOVIMIENTOS DE FONDOS MUTUOS"), "5mm", Font_Name, glb_tamletra_titulo
        .CurrentY = vp.CurrentY + rTwips("2mm")
        FilaAux = vp.CurrentY
        
                          
                  
                  For Each lReg In lCursor_Ins
                      If lReg("codigo").Value = "FFMM" Then
                    gDB.Parametros.Clear
                    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
                    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
                    gDB.Parametros.Add "PID_ARBOL_CLASE", ePT_Numero, lReg("id_arbol_clase_inst_hoja").Value, ePD_Entrada
                    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
                    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO"
              
                    If Not gDB.EjecutaSP Then
                    Exit Sub
                    End If
              
                    Set lCursor_SA = gDB.Parametros("Pcursor").Valor
                  
                      For Each lReg_SA In lCursor_SA
                          If .CurrentY > 10000 Then
                              .NewPage
                              .CurrentY = 2510
                              .CurrentY = vp.CurrentY + rTwips("2mm")
                              pon_titulo UCase("MOVIMIENTOS FONDOS MUTUOS"), "5mm", Font_Name, glb_tamletra_titulo
                              .CurrentY = vp.CurrentY + rTwips("2mm")
                              FilaAux = vp.CurrentY
                          End If

                        .CurrentY = vp.CurrentY + rTwips("2mm")
                        pon_titulo UCase("MOVIMIENTOS ") & lReg_SA("nemotecnico").Value, "3mm", Font_Name, 8
                        .CurrentY = vp.CurrentY + rTwips("2mm")
                        FilaAux = vp.CurrentY

                          gDB.Parametros.Clear
                          gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
                          gDB.Parametros.Add "id_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
                          gDB.Parametros.Add "fecha_ini", ePT_Numero, CDate(oCliente.Fecha_Cartola) - 30, ePD_Entrada
                          gDB.Parametros.Add "fecha_ter", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
                          gDB.Parametros.Add "id_nemo", ePT_Numero, lReg_SA("id_nemotecnico").Value, ePD_Entrada
                          gDB.Procedimiento = "PKG_MOV_ACTIVOS$BUSCAR_MOV_NEMO"

                          If Not gDB.EjecutaSP Then
                          Exit Sub
                          End If

                          Set lCursor_SN = gDB.Parametros("Pcursor").Valor
                          .StartTable
                          .TableCell(tcRows) = 1
                          .TableCell(tcCols) = 5
                          '.TableCell(tcRows) = lCursor_SN.Count
                          iFila = 1
                          .TableCell(tcText, iFila, 1) = "FECHA MOVIMIENTO"
                          .TableCell(tcText, iFila, 2) = "DESCRIPCI�N"
                          .TableCell(tcText, iFila, 3) = "N�MERO DE  CUOTAS"
                          .TableCell(tcText, iFila, 4) = "VALOR CUOTA"
                          .TableCell(tcText, iFila, 5) = "MONTO INVERTIDO"

                          .TableCell(tcAlign, iFila, 1) = taCenterMiddle
                          .TableCell(tcAlign, iFila, 2) = taCenterMiddle
                          .TableCell(tcAlign, iFila, 3) = taCenterMiddle
                          .TableCell(tcAlign, iFila, 4) = taCenterMiddle
                          .TableCell(tcAlign, iFila, 5) = taCenterMiddle

                          .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                          iFila = iFila + 1
                          For Each lReg_SN In lCursor_SN
                             .TableCell(tcRows) = .TableCell(tcRows) + 1
                             .TableCell(tcText, iFila, 1) = lReg_SN("fecha_movimiento").Value
                             .TableCell(tcText, iFila, 2) = lReg_SN("dsc_movimiento").Value
                             .TableCell(tcText, iFila, 3) = FormatNumber(lReg_SN("cantidad").Value, 4)
                             .TableCell(tcText, iFila, 4) = FormatNumber(lReg_SN("precio").Value, 4)
                             .TableCell(tcText, iFila, 5) = IIf(lReg_SN("flg_tipo_movimiento").Value = "I", FormatNumber(lReg_SN("monto_total").Value, oCliente.Decimales), FormatNumber(CDbl(lReg_SN("monto_total").Value) * -1, oCliente.Decimales))

                             .TableCell(tcAlign, iFila, 1) = taCenterMiddle
                             .TableCell(tcAlign, iFila, 2) = taLeftMiddle
                             .TableCell(tcAlign, iFila, 3) = taRightMiddle
                             .TableCell(tcAlign, iFila, 4) = taRightMiddle
                             .TableCell(tcAlign, iFila, 5) = taRightMiddle

                             'FormatNumber(lReg_SN("monto_total").Value, 2)
                             'IIf(lReg_SN("flg_tipo_movimiento").Value = 'I', formatnumber(lreg_sn("monto_total").value,2),0)
                             iFila = iFila + 1

                          Next
                          .TableCell(tcColWidth, , 1) = "30mm"
                          .TableCell(tcColWidth, , 2) = "80mm"
                          .TableCell(tcColWidth, , 3) = "30mm"
                          .TableCell(tcColWidth, , 4) = "30mm"
                          .TableCell(tcColWidth, , 5) = "40mm"
                          .TableCell(tcFontSize, 1, 1, iFila, 5) = 7
                          .EndTable


                      Next
                      End If
                      
                      Next
End With

    Set lCursor_Ins = Nothing
    Set lReg = Nothing
        
  
        
        
    ' ##################################### DATOS CAJA
    
    Const sHeader_Caja = "Caja|Mercado|Moneda|Monto Moneda Caja|Monto Moneda Cuenta|"
    Const sFormat_Caja = "3000|1000|1000|>2500|>2500|>4200"
   Dim a As Integer
   Dim c As Integer
    .NewPage
    .CurrentY = vp.CurrentY + rTwips("2mm")
    pon_titulo UCase("RESUMEN ACTIVOS"), "5mm", Font_Name, glb_tamletra_titulo
    .CurrentY = vp.CurrentY + rTwips("2mm")
    FilaAux = vp.CurrentY
        Rem Datos de la Caja
    .Paragraph = "" 'salto de linea
    .FontSize = glb_tamletra_registros
    .FontBold = True
    .Paragraph = "Datos de la(s) Caja(s) de la Cuenta"
    .FontBold = False
    .FontSize = glb_tamletra_registros
    
    .StartTable
    .TableCell(tcCols) = 5
    .TableCell(tcRows) = 1
    .TableBorder = tbBox
    
    .TableCell(tcText, 1, 1) = "Caja"
    .TableCell(tcText, 1, 2) = "Mercado"
    .TableCell(tcText, 1, 3) = "Moneda"
    .TableCell(tcText, 1, 4) = "Monto Moneda Caja"
    .TableCell(tcText, 1, 5) = "Monto Moneda Cuenta"
    
    .TableCell(tcColWidth, 1, 1) = "70mm"
    .TableCell(tcColWidth, 1, 2) = "40mm"
    .TableCell(tcColWidth, 1, 3) = "40mm"
    .TableCell(tcColWidth, 1, 4) = "50mm"
    .TableCell(tcColWidth, 1, 5) = "50mm"
    
    
    For Each lReg_Caj In lCursor_Caj
    
'      If .CurrentY > 10000 Then
'      .NewPage
'      .CurrentY = 2510
'      .CurrentY = vp.CurrentY + rTwips("2mm")
'      pon_titulo UCase("RESUMEN ACTIVOS"), "5mm", Font_Name, glb_tamletra_titulo
'      .CurrentY = vp.CurrentY + rTwips("2mm")
'      FilaAux = vp.CurrentY
'      End If

    
    
    
      lMonto_Mon_Caja = 0
      Set lSaldos_Caja = New Class_Saldos_Caja
      
      With lSaldos_Caja
        .Campo("fecha_cierre").Valor = DTP_Fecha_Ter.Value
        .Campo("id_caja_cuenta").Valor = lReg_Caj("id_caja_cuenta").Value
        
        If .Buscar Then
          If .Cursor.Count > 0 Then
            lMonto_Mon_Caja = (.Cursor(1)("monto_mon_caja").Value + .Cursor(1)("MONTO_X_COBRAR_MON_CTA").Value) - .Cursor(1)("MONTO_X_PAGAR_MON_CTA").Value
          End If
        End If
      End With
      
'      Set lcTipo_Cambio = New Class_Tipo_Cambios
      Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
      If lcTipo_Cambio.Busca_Precio_Paridad(lId_Cuenta _
                                       , lReg_Caj("Id_Moneda").Value _
                                       , lId_Moneda_Cta _
                                       , DTP_Fecha_Ter.Value _
                                       , lParidad _
                                       , lOperacion) Then
        If lOperacion = "M" Then
          Rem Multiplicacion
          lMonto_Mon_Cta = lMonto_Mon_Caja * lParidad
        Else
          Rem Division
          lMonto_Mon_Cta = Fnt_Divide(lMonto_Mon_Caja, lParidad)
        End If
      Else
        MsgBox lcTipo_Cambio.ErrMsg, vbCritical, Me.Caption
      End If
      
      .TableCell(tcRows) = .TableCell(tcRows) + 1
      a = .TableCell(tcRows)
      .TableCell(tcText, a, 1) = Trim(lReg_Caj("Dsc_Caja_Cuenta").Value)
      .TableCell(tcText, a, 2) = Trim(lReg_Caj("Desc_Mercado").Value)
      .TableCell(tcText, a, 3) = Trim(lReg_Caj("Dsc_Moneda").Value)
      .TableCell(tcText, a, 4) = Format(lMonto_Mon_Caja, Fnt_Formato_Moneda(lReg_Caj("Id_Moneda").Value))
      .TableCell(tcText, a, 5) = Format(lMonto_Mon_Cta, lFormatoMonedaCta)
      
      

    Next
    
    
    .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    .TableCell(tcFontBold, 1) = True
    .EndTable
    

    .Paragraph = "" 'salto de linea
    .FontSize = glb_tamletra_registros
    .FontBold = True
    .Paragraph = "Detalle de Movimiento de Cajas Mes"
    .FontBold = False
    .FontSize = glb_tamletra_registros
    '.StartTable
    '.TableCell(tcAlignCurrency) = False
      
      Call Sub_Bloquea_Puntero(Me)
      
      If Not Fnt_Form_Validar(Me) Then
        'MsgBox "Debe seleccionar una Cuenta para buscar Movimientos de Caja.", vbInformation, Me.Caption
        GoTo ErrProcedure
      End If
      
      lFecDesde = CDate(DameInicioDeMes(DTP_Fecha_Ter.Value))
      'lFecDesde = CDate("20-01-2006")
      lFecHasta = DTP_Fecha_Ter.Value
       
      If lFecDesde > lFecHasta Then
        MsgBox "La Fecha Desde debe ser menor que Fecha Hasta.", vbExclamation, Me.Caption
        GoTo ErrProcedure
      End If
      
     ' For Each lReg_Caj In lCursor_Caj
      
      
      
      lUltimaCaja = 0
      lIngreso = 0
      lEgreso = 0
      lTotal = 0
      'lId_Caja_Cuenta = lReg_Caj("ID_CAJA_CUENTA").Value
      
      
      
      
      lCuenta_Lineas = 0
      lTope = 25
     'MsgBox lCursor_Caj.Count
      
      
      
      
      
      For Each lReg_Caja In lCursor_Caja
          
          Set lcMov_Caja = New Class_Mov_Caja
          lcMov_Caja.Campo("Id_Caja_Cuenta").Valor = lReg_Caja("ID_CAJA_CUENTA").Value
          c = 0
          lTotal = 0
          If lcMov_Caja.Buscar_Movimientos_Cuenta(lId_Cuenta, lFecDesde, lFecHasta) Then
            For Each lReg In lcMov_Caja.Cursor
              
              If c = 0 Then
              .Paragraph = ""
               If .CurrentY > 9000 Then
                .NewPage
                .CurrentY = vp.CurrentY + rTwips("2mm")
                pon_titulo UCase("RESUMEN ACTIVOS"), "5mm", Font_Name, glb_tamletra_titulo
                .CurrentY = vp.CurrentY + rTwips("2mm")
                FilaAux = vp.CurrentY
                Rem Datos de la Caja
                .Paragraph = "" 'salto de linea
              End If

              .StartTable
                  .TableBorder = tbBox
                  .TableCell(tcRows) = 2
                  .TableCell(tcCols) = 7
                  .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                  .TableCell(tcBackColor, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                  .TableCell(tcFontBold, 1) = True
                  .TableCell(tcColSpan, 1, 1) = 3
                  .TableCell(tcText, 1, 1) = UCase(lReg_Caja("dsc_CAJA_CUENTA").Value)
                  .TableCell(tcText, 2, 1) = "Fecha Mov."
                  .TableCell(tcText, 2, 2) = "Fecha Liq."
                  .TableCell(tcText, 2, 3) = "Origen Mov."
                  .TableCell(tcText, 2, 4) = "Movimientos"
                  .TableCell(tcText, 2, 5) = "Ingreso"
                  .TableCell(tcText, 2, 6) = "Egreso"
                  .TableCell(tcText, 2, 7) = "Saldo"
                  .TableCell(tcAlign, 2) = taCenterMiddle
                  .TableCell(tcColWidth, 1, 1) = "18mm"
                  .TableCell(tcColWidth, 1, 2) = "18mm"
                  .TableCell(tcColWidth, 1, 3) = "35mm"
                  .TableCell(tcColWidth, 1, 4) = "105mm"
                  .TableCell(tcColWidth, 1, 5) = "25mm"
                  .TableCell(tcColWidth, 1, 6) = "25mm"
                  .TableCell(tcColWidth, 1, 7) = "25mm"
                  .TableCell(tcAlign, , 5) = taRightMiddle
                  .TableCell(tcAlign, , 6) = taRightMiddle
                  .TableCell(tcAlign, , 7) = taRightMiddle
                  
                  
                  Set lcSaldos_Caja = New Class_Saldos_Caja
                 ' With lcSaldos_Caja
                    lcSaldos_Caja.Campo("id_Caja_Cuenta").Valor = lReg_Caja("ID_CAJA_CUENTA").Value
                    lcSaldos_Caja.Campo("Fecha_cierre").Valor = lFecDesde
                    If lcSaldos_Caja.Buscar_Saldos_Cuenta(lTotal, lId_Cuenta) Then
                      
                      .TableCell(tcRows) = .TableCell(tcRows) + 1
                      .TableCell(tcText, .TableCell(tcRows), 1) = lFecDesde - 1
                      .TableCell(tcText, .TableCell(tcRows), 2) = ""
                      .TableCell(tcText, .TableCell(tcRows), 3) = "SALDO"
                      .TableCell(tcText, .TableCell(tcRows), 4) = "SALDO ANTERIOR"
                      .TableCell(tcText, .TableCell(tcRows), 5) = FormatNumber(0, Dame_Decimales(oCliente.Moneda))
                      .TableCell(tcText, .TableCell(tcRows), 6) = FormatNumber(0, Dame_Decimales(oCliente.Moneda))
                      .TableCell(tcText, .TableCell(tcRows), 7) = FormatNumber(lTotal, Dame_Decimales(oCliente.Moneda))
                  End If
                  
                  '.TableCell(tcRows) = .TableCell(tcRows) + 1
               c = 1
               End If
              'Else
              
              
                  .TableCell(tcRows) = .TableCell(tcRows) + 1
                  
                If Trim(lReg("FLG_Tipo_Movimiento").Value) = "A" Then
                  lTotal = lTotal + lReg("Monto").Value
                  lIngreso = lIngreso + lReg("Monto").Value
                  .TableCell(tcText, .TableCell(tcRows), 1) = Trim(lReg("Fecha_Movimiento").Value)
                  .TableCell(tcText, .TableCell(tcRows), 2) = Trim(lReg("Fecha_Liquidacion").Value)
                  .TableCell(tcText, .TableCell(tcRows), 3) = Trim(lReg("dsc_Origen_Mov_Caja").Value)
                  .TableCell(tcText, .TableCell(tcRows), 4) = Trim(lReg("Dsc_Mov_Caja").Value)
                  .TableCell(tcText, .TableCell(tcRows), 5) = FormatNumber(lReg("Monto").Value, Dame_Decimales(oCliente.Moneda))
                  .TableCell(tcText, .TableCell(tcRows), 6) = FormatNumber(0, Dame_Decimales(oCliente.Moneda))
                  .TableCell(tcText, .TableCell(tcRows), 7) = FormatNumber(lTotal, Dame_Decimales(oCliente.Moneda))
                Else
                  lTotal = lTotal - lReg("Monto").Value
                  lEgreso = lEgreso + lReg("Monto").Value
                  .TableCell(tcText, .TableCell(tcRows), 1) = Trim(lReg("Fecha_Movimiento").Value)
                  .TableCell(tcText, .TableCell(tcRows), 2) = Trim(lReg("Fecha_Liquidacion").Value)
                  .TableCell(tcText, .TableCell(tcRows), 3) = Trim(lReg("dsc_Origen_Mov_Caja").Value)
                  .TableCell(tcText, .TableCell(tcRows), 4) = Trim(lReg("Dsc_Mov_Caja").Value)
                  .TableCell(tcText, .TableCell(tcRows), 5) = FormatNumber(0, Dame_Decimales(oCliente.Moneda))
                  .TableCell(tcText, .TableCell(tcRows), 6) = FormatNumber(lReg("Monto").Value, Dame_Decimales(oCliente.Moneda))
                  .TableCell(tcText, .TableCell(tcRows), 7) = FormatNumber(lTotal, Dame_Decimales(oCliente.Moneda))
                 
                End If
                 
             ' End If
              
              
              
            Next
            If lcMov_Caja.Cursor.Count > 0 Then
               .EndTable
            End If
          Else
              .StartTable
                  .TableCell(tcRows) = 2
                  .TableCell(tcCols) = 2
                  .TableCell(tcText, 1, 1) = lReg_Caja("ID_CAJA_CUENTA").Value
              .EndTable
                
          End If
          
      Next
      
      'MsgBox lTotal
      
'            Set lcMov_Caja = New Class_Mov_Caja
'            lcMov_Caja.Campo("Id_Caja_Cuenta").Valor = lReg_Caj("ID_CAJA_CUENTA").Value
'
'          If lcMov_Caja.Buscar_Movimientos_Cuenta(lId_Cuenta, lFecDesde, lFecHasta) Then
'
'             c = 0
                  
'            For Each lReg In lcMov_Caja.Cursor
          
'                If lCuenta_Lineas > lTope Then
''
''                'If .CurrentY > 3000 Then
'                lTope = 28
'                .EndTable
''
'                .NewPage
'                .StartTable
'                .TableBorder = tbBox
'                lCuenta_Lineas = 0
'
'                 c = 0
''                '.TableCell(tcRows) = 2
''                '.TableCell(tcCols) = 7
'                .CurrentY = 2510
'                .CurrentY = vp.CurrentY + rTwips("2mm")
'                pon_titulo UCase("RESUMEN ACTIVOS"), "5mm", Font_Name, glb_tamletra_titulo
'                .CurrentY = vp.CurrentY + rTwips("2mm")
''                'FilaAux = vp.CurrentY
'               End If
'
'
'            'If c = 0 Then
'            '.EndTable
'            '.NewPage
'            .StartTable
'            .TableCell(tcRows) = 2
'            .TableCell(tcCols) = 7
'            .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'            .TableCell(tcBackColor, 2) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'            .TableCell(tcFontBold, 1) = True
'            .TableCell(tcColSpan, 1, 1) = 3
'            .TableCell(tcText, 1, 1) = UCase(lReg_Caj("dsc_CAJA_CUENTA").Value)
'
'            .TableCell(tcText, 2, 1) = "Fecha Mov."
'            .TableCell(tcText, 2, 2) = "Fecha Liq."
'            .TableCell(tcText, 2, 3) = "Origen Mov."
'            .TableCell(tcText, 2, 4) = "Movimientos"
'            .TableCell(tcText, 2, 5) = "Ingreso"
'            .TableCell(tcText, 2, 6) = "Egreso"
'            .TableCell(tcText, 2, 7) = "Saldo"
'
'            .TableCell(tcAlign, 2) = taCenterMiddle
'            .TableCell(tcColWidth, 1, 1) = "18mm"
'            .TableCell(tcColWidth, 1, 2) = "18mm"
'            .TableCell(tcColWidth, 1, 3) = "35mm"
'            .TableCell(tcColWidth, 1, 4) = "105mm"
'            .TableCell(tcColWidth, 1, 5) = "25mm"
'            .TableCell(tcColWidth, 1, 6) = "25mm"
'            .TableCell(tcColWidth, 1, 7) = "25mm"
'            .TableCell(tcAlign, , 5) = taRightMiddle
'            .TableCell(tcAlign, , 6) = taRightMiddle
'            .TableCell(tcAlign, , 7) = taRightMiddle
'
'            c = 1
'           ' End If
'
'
'            'If lUltimaCaja = lReg("Id_Caja_Cuenta").Value Then
'              .TableCell(tcRows) = .TableCell(tcRows) + 1
'              a = .TableCell(tcRows)
'              If lReg("FLG_Tipo_Movimiento").Value = "A" Then
'                lTotal = lTotal + lReg("Monto").Value
'                lIngreso = lIngreso + lReg("Monto").Value
'                .TableCell(tcText, a, 1) = Trim(lReg("Fecha_Movimiento").Value)
'                .TableCell(tcText, a, 2) = Trim(lReg("Fecha_Liquidacion").Value)
'                .TableCell(tcText, a, 3) = Trim(lReg("dsc_Origen_Mov_Caja").Value)
'                .TableCell(tcText, a, 4) = Trim(lReg("Dsc_Mov_Caja").Value)
'                .TableCell(tcText, a, 5) = FormatNumber(lReg("Monto").Value, Dame_Decimales(oCliente.Moneda))
'                .TableCell(tcText, a, 6) = FormatNumber(0, Dame_Decimales(oCliente.Moneda))
'                .TableCell(tcText, a, 7) = FormatNumber(lTotal, Dame_Decimales(oCliente.Moneda))
'
'            Else
'                lTotal = lTotal - lReg("Monto").Value
'                lEgreso = lEgreso + lReg("Monto").Value
'                .TableCell(tcText, a, 1) = Trim(lReg("Fecha_Movimiento").Value)
'                .TableCell(tcText, a, 2) = Trim(lReg("Fecha_Liquidacion").Value)
'                .TableCell(tcText, a, 3) = Trim(lReg("dsc_Origen_Mov_Caja").Value)
'                .TableCell(tcText, a, 4) = Trim(lReg("Dsc_Mov_Caja").Value)
'                .TableCell(tcText, a, 5) = FormatNumber(0, Dame_Decimales(oCliente.Moneda))
'                .TableCell(tcText, a, 6) = FormatNumber(lReg("Monto").Value, Dame_Decimales(oCliente.Moneda))
'                .TableCell(tcText, a, 7) = FormatNumber(lTotal, Dame_Decimales(oCliente.Moneda))
'              End If
'           'Else
'
'              If Not lUltimaCaja = 0 Then
'                 .TableCell(tcRows) = .TableCell(tcRows) + 1
'                 a = .TableCell(tcRows)
'
'                .TableCell(tcText, a, 1) = ""
'                .TableCell(tcText, a, 2) = ""
'                .TableCell(tcText, a, 3) = ""
'                .TableCell(tcText, a, 4) = "SALDO CAJA"
'                .TableCell(tcText, a, 5) = FormatNumber(lIngreso, Dame_Decimales(oCliente.Moneda))
'                .TableCell(tcText, a, 6) = FormatNumber(lEgreso, Dame_Decimales(oCliente.Moneda))
'                .TableCell(tcText, a, 7) = FormatNumber(lTotal, Dame_Decimales(oCliente.Moneda))
'
'                lIngreso = 0
'                lEgreso = 0
'                lTotal = 0
'              End If
'
'
'              lUltimaCaja = lReg("Id_Caja_Cuenta").Value
'              'Buscar Saldo Anterior por Caja
'
'              Set lcSaldos_Caja = New Class_Saldos_Caja
'             ' With lcSaldos_Caja
'                lcSaldos_Caja.Campo("id_Caja_Cuenta").Valor = lUltimaCaja
'                lcSaldos_Caja.Campo("Fecha_cierre").Valor = lFecDesde
'                If lcSaldos_Caja.Buscar_Saldos_Cuenta(lTotal, lId_Cuenta) Then
'                        .TableCell(tcRows) = .TableCell(tcRows) + 1
'                        a = .TableCell(tcRows)
'                        .TableCell(tcText, a, 1) = lFecDesde - 1
'                        .TableCell(tcText, a, 2) = ""
'                        .TableCell(tcText, a, 3) = "SALDO"
'                        .TableCell(tcText, a, 4) = "SALDO ANTERIOR"
'                        .TableCell(tcText, a, 5) = FormatNumber(lIngreso, Dame_Decimales(oCliente.Moneda))
'                        .TableCell(tcText, a, 6) = FormatNumber(lEgreso, Dame_Decimales(oCliente.Moneda))
'                        .TableCell(tcText, a, 7) = FormatNumber(lTotal, Dame_Decimales(oCliente.Moneda))
'
'
'                Else
'
'
'                  Call Fnt_MsgError(.SubTipo_LOG, _
'                                    "Problemas en cargar Valores Cuota.", _
'                                    .ErrMsg, _
'                                    pConLog:=True)
'                  lTotal = 0
'                End If
'
'              Set lcSaldos_Caja = Nothing
'
'              If lReg("FLG_Tipo_Movimiento").Value = "A" Then
'                'Call SetCell(Grilla, lLinea, "Ingresos", lReg("Monto").Value)
'                lTotal = lTotal + lReg("Monto").Value
'                lIngreso = lIngreso + lReg("Monto").Value
'                .TableCell(tcRows) = .TableCell(tcRows) + 1
'                a = .TableCell(tcRows)
'
'                .TableCell(tcText, a, 1) = Trim(lReg("Fecha_Movimiento").Value)
'                .TableCell(tcText, a, 2) = Trim(lReg("Fecha_Liquidacion").Value)
'                .TableCell(tcText, a, 3) = Trim(lReg("dsc_Origen_Mov_Caja").Value)
'                .TableCell(tcText, a, 4) = Trim(lReg("Dsc_Mov_Caja").Value)
'                .TableCell(tcText, a, 5) = FormatNumber(lReg("Monto").Value, Dame_Decimales(oCliente.Moneda))
'                .TableCell(tcText, a, 6) = FormatNumber(0, Dame_Decimales(oCliente.Moneda))
'                .TableCell(tcText, a, 7) = FormatNumber(lTotal, Dame_Decimales(oCliente.Moneda))
'
'               Else
'                'Call SetCell(Grilla, lLinea, "Egresos", lReg("Monto").Value)
'                lTotal = lTotal - lReg("Monto").Value
'                lEgreso = lEgreso + lReg("Monto").Value
'                'MsgBox "EGRESO"
'                .TableCell(tcRows) = .TableCell(tcRows) + 1
'                a = .TableCell(tcRows)
'                .TableCell(tcText, a, 1) = Trim(lReg("Fecha_Movimiento").Value)
'                .TableCell(tcText, a, 2) = Trim(lReg("Fecha_Liquidacion").Value)
'                .TableCell(tcText, a, 3) = Trim(lReg("dsc_Origen_Mov_Caja").Value)
'                .TableCell(tcText, a, 4) = Trim(lReg("Dsc_Mov_Caja").Value)
'                .TableCell(tcText, a, 5) = FormatNumber(0, Dame_Decimales(oCliente.Moneda))
'                .TableCell(tcText, a, 6) = FormatNumber(lReg("Monto").Value, Dame_Decimales(oCliente.Moneda))
'                .TableCell(tcText, a, 7) = FormatNumber(lTotal, Dame_Decimales(oCliente.Moneda))
'                End If
'              lUltimaCaja = lReg("Id_Caja_Cuenta").Value
'
'
'
'
'
'
'              .EndTable
'
'         ' End If
'
'lCuenta_Lineas = lCuenta_Lineas + 1
'
'MsgBox lCursor_Caj.Count
'
'Next
'
'
'
'End If
'
'Next

End With
      

'
     
    
Rem --------------------
    '.EndDoc
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub
Function SacaDecimalesMoneda(pId_Moneda As Double) As Integer
    Dim lReg        As hFields
    Dim lResult     As String
    Dim lDecimales  As Integer
    
    Set lReg = gmMatrixMonedas.Buscar("ID_MONEDA", pId_Moneda)
    If Not lReg Is Nothing Then
      lDecimales = lReg("dicimales_mostrar").Value
    End If
    
    SacaDecimalesMoneda = lDecimales

End Function



Function FormatNumber(xValor As Variant, iDecimales As Integer, Optional bPorcentaje As Boolean = False) As String
    Dim xFormato        As String
    Dim sDecimalSep     As String
    Dim sMilesSep       As String
        
    sDecimalSep = Obtener_Simbolo(LOCALE_SDECIMAL)
    sMilesSep = Obtener_Simbolo(LOCALE_SMONTHOUSANDSEP)
    
    On Error GoTo PError
        
    If iDecimales = 0 Then
        xFormato = "###,###,##0"
    ElseIf bPorcentaje Then
        xFormato = "##0." & String(iDecimales - 1, "#") & "0"
    Else
        xFormato = "###,###,##0." & String(iDecimales - 1, "#") & "0"
    End If
    
    GoTo Fin
    
PError:
    xValor = "0"
    
Fin:
    FormatNumber = Format(xValor, xFormato)
    On Error GoTo 0
    
End Function
Function DameMes(sFecha)
    Dim iMes
    Dim aMeses
    iMes = Month(sFecha)
    aMeses = Array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
    DameMes = aMeses(iMes)
End Function


Sub DatosDelCliente()
    Dim lCursorCuentas
    Dim lReg
    Dim CursorDireccion
    Dim Reg_1
    Dim Reg
    Dim Cursor_1
    Dim Cursor

    Dim IDMoneda As Integer
    Dim Moneda As String

    Dim Decimales As Integer

    Dim Fecha_Servidor As Date
    Dim Fecha_Cierre As Date
    Dim Fecha_Consulta As Date

    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Procedimiento = "PKG_CUENTAS$Buscar"

    If Not gDB.EjecutaSP Then
        GoTo Fin
    End If

    Set lCursorCuentas = gDB.Parametros("Pcursor").Valor
    gDB.Parametros.Clear

'#################### Direcciones Clientes ####################################
     For Each lReg In lCursorCuentas
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Pid_cliente", ePT_Numero, lReg("id_cliente").Value, ePD_Entrada
        gDB.Procedimiento = "PKG_DIRECCIONES_CLIENTES$Buscar"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set CursorDireccion = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each Reg_1 In CursorDireccion
            oCliente.Direccion = Trim(NVL(Reg_1("direccion").Value, ""))
            oCliente.Comuna = NVL(Reg_1("dsc_comuna_ciudad").Value, "")
            'oCliente.Ciudad = NVL(Reg_1("Ciudad").Value, "")
            oCliente.Telefono = NVL(Reg_1("fono").Value, "")
            
            If oCliente.Direccion = "" Then
                oCliente.Direccion = "Sin Direcci�n"
            End If

        Next
'#################### FIN Direcciones Clientes #################################

'#################### Moneda  ####################################
        IDMoneda = lReg("id_moneda").Value

        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Pid_Moneda", ePT_Numero, IDMoneda, ePD_Entrada
        gDB.Procedimiento = "PKG_MONEDAS$Buscar"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set Cursor_1 = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each Reg_1 In Cursor_1
            oCliente.Moneda = Reg_1("dsc_moneda").Value
            oCliente.Decimales = Reg_1("dicimales_mostrar").Value
        Next
'#################### FIN Moneda #################################

'PKG_CIERRES_CUENTAS$BUSCAR_ultimo
'#################### Moneda  ####################################
        Fecha_Servidor = Fnt_FechaServidor()
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
        gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Servidor), ePD_Entrada
        gDB.Procedimiento = "PKG_CIERRES_CUENTAS$BUSCAR_ultimo"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set Cursor = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each Reg In Cursor
            'Fecha_cierre = Reg("fecha_cierre").value
            oCliente.Fecha_Cierre = CDate(Reg("fecha_cierre").Value)
        Next

'#################### FIN Moneda #################################

        oCliente.Nombre = lReg("nombre_cliente").Value
        
        oCliente.Rut = formato_rut(lReg("rut_cliente").Value)
        
        oCliente.Num_Cuenta = lReg("num_cuenta").Value
        oCliente.Perfil_Riesgo = lReg("dsc_perfil_riesgo").Value
        oCliente.Ejecutivo = NVL(lReg("dsc_asesor").Value, "Sin Asesor")
        oCliente.Fecha_Creacion = lReg("fecha_operativa").Value

        oCliente.IDEmpresa = lReg("id_empresa").Value
    Next
Fin:

    Set lCursorCuentas = Nothing
    Set lReg = Nothing
    Set CursorDireccion = Nothing
    Set Reg_1 = Nothing
    Set Reg = Nothing
    Set Cursor_1 = Nothing
    Set Cursor = Nothing
    
End Sub

Function formato_rut(rut_bruto)
    Dim rut_salida, dv
    Dim temp(3)
    Dim Rut
    Dim f
    Dim rut_salida3
    Dim rut_salida2
    Dim rut_salida1
    
    rut_salida = Trim(rut_bruto)
    dv = Right(rut_salida, 1)
    Rut = Left(rut_salida, Len(rut_salida) - 2)
    f = Len(Rut)
    
    If f > 6 Then
        rut_salida3 = Mid(Rut, f - 2, 3)
        f = f - 3
        rut_salida2 = Mid(Rut, f - 2, 3)
        f = f - 3
        If f < 3 Then
            rut_salida1 = Left(Rut, f)
        End If
        formato_rut = rut_salida1 & "." & rut_salida2 & "." & rut_salida3 & "-" & dv
    Else
        formato_rut = rut_bruto
    End If
    
End Function


Function Decimales_Moneda(CodMoneda)
Dim Cursor As hRecord
Dim Reg As hFields
Decimales_Moneda = 0
'Dim Monedas As Class_Monedas
Dim clMoneda As Object

Set clMoneda = Fnt_CreateObject(cDLL_Monedas)
  With clMoneda
  '.Campo("SIMBOLO") = CodMoneda
  If .Buscar Then
  
      Set Cursor = .Cursor
      
      For Each Reg In Cursor
        If Reg("simbolo") = Trim(CodMoneda) Then
        Decimales_Moneda = Reg("DICIMALES_MOSTRAR").Value
        End If
      Next
      
      
  Else
    Decimales_Moneda = 0
  
  End If
  End With
End Function
