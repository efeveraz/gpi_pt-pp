VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{E2D000D0-2DA1-11D2-B358-00104B59D73D}#1.0#0"; "titext8.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Conciliador_Cajas 
   Caption         =   "Conciliador de Cajas"
   ClientHeight    =   7095
   ClientLeft      =   2100
   ClientTop       =   3015
   ClientWidth     =   9300
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7095
   ScaleWidth      =   9300
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   6720
      Width           =   9300
      _ExtentX        =   16404
      _ExtentY        =   661
      Style           =   1
      ShowTips        =   0   'False
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Tag             =   "OBLI"
      Top             =   0
      Width           =   9300
      _ExtentX        =   16404
      _ExtentY        =   635
      ButtonWidth     =   1773
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Conciliar"
            Key             =   "CONCILIAR"
            Description     =   "Graba la información de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   3
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Max/Min"
            Key             =   "MaxMin"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   2
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin C1SizerLibCtl.C1Elastic C1Elastic 
      Height          =   6360
      Left            =   0
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   360
      Width           =   9300
      _cx             =   16404
      _cy             =   11218
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   1
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FloodColor      =   6553600
      ForeColorDisabled=   -2147483631
      Caption         =   ""
      Align           =   5
      AutoSizeChildren=   8
      BorderWidth     =   6
      ChildSpacing    =   10
      Splitter        =   0   'False
      FloodDirection  =   1
      FloodPercent    =   0
      CaptionPos      =   1
      WordWrap        =   -1  'True
      MaxChildSize    =   0
      MinChildSize    =   0
      TagWidth        =   0
      TagPosition     =   3
      Style           =   0
      TagSplit        =   2
      PicturePos      =   4
      CaptionStyle    =   0
      ResizeFonts     =   0   'False
      GridRows        =   2
      GridCols        =   2
      Frame           =   3
      FrameStyle      =   0
      FrameWidth      =   1
      FrameColor      =   -2147483628
      FrameShadow     =   -2147483632
      FloodStyle      =   1
      _GridInfo       =   $"Frm_Conciliador_Cajas.frx":0000
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   9
      Begin C1SizerLibCtl.C1Elastic C1Elastic2 
         Height          =   1800
         Left            =   4755
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   90
         Width           =   4455
         _cx             =   7858
         _cy             =   3175
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         Appearance      =   4
         MousePointer    =   0
         Version         =   801
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         FloodColor      =   6553600
         ForeColorDisabled=   -2147483631
         Caption         =   ""
         Align           =   0
         AutoSizeChildren=   8
         BorderWidth     =   6
         ChildSpacing    =   4
         Splitter        =   0   'False
         FloodDirection  =   0
         FloodPercent    =   0
         CaptionPos      =   1
         WordWrap        =   -1  'True
         MaxChildSize    =   0
         MinChildSize    =   0
         TagWidth        =   0
         TagPosition     =   0
         Style           =   0
         TagSplit        =   2
         PicturePos      =   4
         CaptionStyle    =   0
         ResizeFonts     =   0   'False
         GridRows        =   2
         GridCols        =   2
         Frame           =   3
         FrameStyle      =   0
         FrameWidth      =   1
         FrameColor      =   -2147483628
         FrameShadow     =   -2147483632
         FloodStyle      =   1
         _GridInfo       =   $"Frm_Conciliador_Cajas.frx":0050
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
         Begin MSComctlLib.Toolbar Toolbar_Buscar_Cont 
            Height          =   330
            Left            =   3435
            TabIndex        =   5
            Top             =   90
            Width           =   930
            _ExtentX        =   1640
            _ExtentY        =   582
            ButtonWidth     =   1561
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Buscar"
                  Key             =   "BUSCAR"
                  Description     =   "Agregar un nemotecnico a la Operación"
                  Object.ToolTipText     =   "Selecciona todos los Mov. Contraparte"
               EndProperty
            EndProperty
         End
         Begin TDBText6Ctl.TDBText Txt_OrigenComparacion 
            Height          =   1200
            Left            =   90
            TabIndex        =   6
            Top             =   510
            Width           =   4275
            _Version        =   65536
            _ExtentX        =   7541
            _ExtentY        =   2117
            Caption         =   "Frm_Conciliador_Cajas.frx":009C
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DropDown        =   "Frm_Conciliador_Cajas.frx":0100
            Key             =   "Frm_Conciliador_Cajas.frx":011E
            BackColor       =   12648447
            EditMode        =   0
            ForeColor       =   -2147483640
            ReadOnly        =   -1
            ShowContextMenu =   -1
            MarginLeft      =   1
            MarginRight     =   1
            MarginTop       =   1
            MarginBottom    =   1
            Enabled         =   -1
            MousePointer    =   0
            Appearance      =   2
            BorderStyle     =   1
            AlignHorizontal =   0
            AlignVertical   =   0
            MultiLine       =   -1
            ScrollBars      =   3
            PasswordChar    =   ""
            AllowSpace      =   -1
            Format          =   ""
            FormatMode      =   1
            AutoConvert     =   -1
            ErrorBeep       =   0
            MaxLength       =   0
            LengthAsByte    =   0
            Text            =   ""
            Furigana        =   0
            HighlightText   =   -1
            IMEMode         =   0
            IMEStatus       =   0
            DropWndWidth    =   0
            DropWndHeight   =   0
            ScrollBarMode   =   1
            MoveOnLRKey     =   0
            OLEDragMode     =   0
            OLEDropMode     =   0
         End
         Begin VB.Label Lbl_Origen_Contraparte 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Origen Contraparte"
            Height          =   360
            Left            =   90
            TabIndex        =   7
            Top             =   90
            Width           =   3285
         End
      End
      Begin C1SizerLibCtl.C1Elastic C1Elastic3 
         Height          =   1800
         Left            =   90
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   90
         Width           =   4515
         _cx             =   7964
         _cy             =   3175
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         Appearance      =   4
         MousePointer    =   0
         Version         =   801
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         FloodColor      =   6553600
         ForeColorDisabled=   -2147483631
         Caption         =   ""
         Align           =   0
         AutoSizeChildren=   8
         BorderWidth     =   6
         ChildSpacing    =   4
         Splitter        =   0   'False
         FloodDirection  =   0
         FloodPercent    =   0
         CaptionPos      =   1
         WordWrap        =   -1  'True
         MaxChildSize    =   0
         MinChildSize    =   0
         TagWidth        =   0
         TagPosition     =   0
         Style           =   0
         TagSplit        =   2
         PicturePos      =   4
         CaptionStyle    =   0
         ResizeFonts     =   0   'False
         GridRows        =   2
         GridCols        =   2
         Frame           =   3
         FrameStyle      =   0
         FrameWidth      =   1
         FrameColor      =   -2147483628
         FrameShadow     =   -2147483632
         FloodStyle      =   1
         _GridInfo       =   $"Frm_Conciliador_Cajas.frx":0162
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
         Begin MSComctlLib.Toolbar Toolbar_Buscar_CSBPI 
            Height          =   330
            Left            =   3495
            TabIndex        =   9
            Top             =   90
            Width           =   930
            _ExtentX        =   1640
            _ExtentY        =   582
            ButtonWidth     =   1561
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Buscar"
                  Key             =   "BUSCAR"
                  Description     =   "Agregar un nemotecnico a la Operación"
                  Object.ToolTipText     =   "Selecciona todos los Mov. Locales"
               EndProperty
            EndProperty
         End
         Begin TDBText6Ctl.TDBText Txt_OrigenCSBPI 
            Height          =   1200
            Left            =   90
            TabIndex        =   10
            Top             =   510
            Width           =   4335
            _Version        =   65536
            _ExtentX        =   7646
            _ExtentY        =   2117
            Caption         =   "Frm_Conciliador_Cajas.frx":01AE
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DropDown        =   "Frm_Conciliador_Cajas.frx":0212
            Key             =   "Frm_Conciliador_Cajas.frx":0230
            BackColor       =   12648447
            EditMode        =   0
            ForeColor       =   -2147483640
            ReadOnly        =   -1
            ShowContextMenu =   -1
            MarginLeft      =   1
            MarginRight     =   1
            MarginTop       =   1
            MarginBottom    =   1
            Enabled         =   -1
            MousePointer    =   0
            Appearance      =   2
            BorderStyle     =   1
            AlignHorizontal =   0
            AlignVertical   =   0
            MultiLine       =   -1
            ScrollBars      =   3
            PasswordChar    =   ""
            AllowSpace      =   -1
            Format          =   ""
            FormatMode      =   1
            AutoConvert     =   -1
            ErrorBeep       =   0
            MaxLength       =   0
            LengthAsByte    =   0
            Text            =   ""
            Furigana        =   0
            HighlightText   =   -1
            IMEMode         =   0
            IMEStatus       =   0
            DropWndWidth    =   0
            DropWndHeight   =   0
            ScrollBarMode   =   1
            MoveOnLRKey     =   0
            OLEDragMode     =   0
            OLEDropMode     =   0
         End
         Begin VB.Label Lbl_Origen_Local 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Origen Local"
            Height          =   360
            Left            =   90
            TabIndex        =   11
            Top             =   90
            Width           =   3345
         End
      End
      Begin C1SizerLibCtl.C1Tab STab 
         Height          =   4230
         Left            =   90
         TabIndex        =   12
         Top             =   2040
         Width           =   9120
         _cx             =   16087
         _cy             =   7461
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         Appearance      =   0
         MousePointer    =   0
         Version         =   801
         BackColor       =   6579300
         ForeColor       =   -2147483634
         FrontTabColor   =   -2147483635
         BackTabColor    =   6579300
         TabOutlineColor =   -2147483632
         FrontTabForeColor=   -2147483634
         Caption         =   "&Diferencias|&Solo GPI|Solo &Contraparte"
         Align           =   0
         CurrTab         =   0
         FirstTab        =   0
         Style           =   6
         Position        =   0
         AutoSwitch      =   -1  'True
         AutoScroll      =   -1  'True
         TabPreview      =   -1  'True
         ShowFocusRect   =   0   'False
         TabsPerPage     =   0
         BorderWidth     =   0
         BoldCurrent     =   -1  'True
         DogEars         =   -1  'True
         MultiRow        =   0   'False
         MultiRowOffset  =   200
         CaptionStyle    =   0
         TabHeight       =   0
         TabCaptionPos   =   4
         TabPicturePos   =   0
         CaptionEmpty    =   ""
         Separators      =   -1  'True
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   37
         Begin C1SizerLibCtl.C1Elastic C1Elastic4 
            Height          =   3885
            Left            =   15
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   330
            Width           =   9090
            _cx             =   16034
            _cy             =   6853
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   -1  'True
            Appearance      =   4
            MousePointer    =   0
            Version         =   801
            BackColor       =   -2147483633
            ForeColor       =   -2147483630
            FloodColor      =   6553600
            ForeColorDisabled=   -2147483631
            Caption         =   ""
            Align           =   0
            AutoSizeChildren=   8
            BorderWidth     =   6
            ChildSpacing    =   4
            Splitter        =   0   'False
            FloodDirection  =   0
            FloodPercent    =   0
            CaptionPos      =   1
            WordWrap        =   -1  'True
            MaxChildSize    =   0
            MinChildSize    =   0
            TagWidth        =   0
            TagPosition     =   0
            Style           =   0
            TagSplit        =   2
            PicturePos      =   4
            CaptionStyle    =   0
            ResizeFonts     =   0   'False
            GridRows        =   1
            GridCols        =   1
            Frame           =   3
            FrameStyle      =   0
            FrameWidth      =   1
            FrameColor      =   -2147483628
            FrameShadow     =   -2147483632
            FloodStyle      =   1
            _GridInfo       =   $"Frm_Conciliador_Cajas.frx":0274
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   9
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Diferencias 
               Height          =   3705
               Left            =   90
               TabIndex        =   14
               Top             =   90
               Width           =   8910
               _cx             =   15716
               _cy             =   6535
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   0
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Conciliador_Cajas.frx":02AB
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
         Begin C1SizerLibCtl.C1Elastic C1Elastic5 
            Height          =   3885
            Left            =   9735
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   330
            Width           =   9090
            _cx             =   16034
            _cy             =   6853
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   -1  'True
            Appearance      =   4
            MousePointer    =   0
            Version         =   801
            BackColor       =   -2147483633
            ForeColor       =   -2147483630
            FloodColor      =   6553600
            ForeColorDisabled=   -2147483631
            Caption         =   ""
            Align           =   0
            AutoSizeChildren=   8
            BorderWidth     =   6
            ChildSpacing    =   4
            Splitter        =   0   'False
            FloodDirection  =   0
            FloodPercent    =   0
            CaptionPos      =   1
            WordWrap        =   -1  'True
            MaxChildSize    =   0
            MinChildSize    =   0
            TagWidth        =   0
            TagPosition     =   0
            Style           =   0
            TagSplit        =   2
            PicturePos      =   4
            CaptionStyle    =   0
            ResizeFonts     =   0   'False
            GridRows        =   1
            GridCols        =   1
            Frame           =   3
            FrameStyle      =   0
            FrameWidth      =   1
            FrameColor      =   -2147483628
            FrameShadow     =   -2147483632
            FloodStyle      =   1
            _GridInfo       =   $"Frm_Conciliador_Cajas.frx":02C1
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   9
            Begin VSFlex8LCtl.VSFlexGrid Grilla_BPI 
               Height          =   3705
               Left            =   90
               TabIndex        =   16
               Top             =   90
               Width           =   8910
               _cx             =   15716
               _cy             =   6535
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   0
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Conciliador_Cajas.frx":02F8
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
         Begin C1SizerLibCtl.C1Elastic C1Elastic6 
            Height          =   3885
            Left            =   10035
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   330
            Width           =   9090
            _cx             =   16034
            _cy             =   6853
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   -1  'True
            Appearance      =   4
            MousePointer    =   0
            Version         =   801
            BackColor       =   -2147483633
            ForeColor       =   -2147483630
            FloodColor      =   6553600
            ForeColorDisabled=   -2147483631
            Caption         =   ""
            Align           =   0
            AutoSizeChildren=   8
            BorderWidth     =   6
            ChildSpacing    =   4
            Splitter        =   0   'False
            FloodDirection  =   0
            FloodPercent    =   0
            CaptionPos      =   1
            WordWrap        =   -1  'True
            MaxChildSize    =   0
            MinChildSize    =   0
            TagWidth        =   0
            TagPosition     =   0
            Style           =   0
            TagSplit        =   2
            PicturePos      =   4
            CaptionStyle    =   0
            ResizeFonts     =   0   'False
            GridRows        =   1
            GridCols        =   1
            Frame           =   3
            FrameStyle      =   0
            FrameWidth      =   1
            FrameColor      =   -2147483628
            FrameShadow     =   -2147483632
            FloodStyle      =   1
            _GridInfo       =   $"Frm_Conciliador_Cajas.frx":030E
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   9
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Contraparte 
               Height          =   3705
               Left            =   90
               TabIndex        =   18
               Top             =   90
               Width           =   8910
               _cx             =   15716
               _cy             =   6535
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   0
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Conciliador_Cajas.frx":0345
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
      End
   End
End
Attribute VB_Name = "Frm_Conciliador_Cajas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Dim fConciliador As Object
Dim fOrigenComparación As Variant
Dim fTag_Impresion As String

Public Function Fnt_Mostrar(pCod_Proceso_Componente As String)
  Fnt_Mostrar = False

  fKey = pCod_Proceso_Componente
  
  If Not Fnt_CargarDatos Then
    Unload Me
    Exit Function
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
  
  Fnt_Mostrar = True
End Function

Private Function Fnt_CargarDatos() As Boolean
Dim lcProceso_Componente  As Class_Proceso_Componente
Dim lMensaje              As String

On Error GoTo ErrProcedure

  Call Sub_Bloquea_Puntero(Me)
  
  Load Me
  Fnt_CargarDatos = False
  
  'Busca el componente que corresponde al conciliador
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = fKey
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set fConciliador = .IniciaClass(lMensaje)
    
    If fConciliador Is Nothing Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                      , lMensaje _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  Set lcProceso_Componente = Nothing
  
  
  'Busca el componente que corresponde a la carga de los datos a comparar
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = fConciliador.CodigoDLLContraparte
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set fConciliador.Clase_Contraparte = .IniciaClass(lMensaje)
    
    If fConciliador.Clase_Contraparte Is Nothing Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                      , lMensaje _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  Set lcProceso_Componente = Nothing
  
  'Inicia la configuracion de la grilla
  Call fConciliador.ConfiguraFormulario(Me)
  Set fConciliador.gRelogDB = gRelogDB
  fConciliador.ID_EMPRESA = Fnt_EmpresaActual
  
  Me.Caption = fConciliador.CaptionConciliador
  
  Fnt_CargarDatos = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la carga del conciliador (" & fKey & ")." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Function

'Private Sub C1Elastic1_ResizeChildren()
'Dim lBand As Band
'
''  For Each lBand In CoolBar.Bands
''    lBand.MinHeight = C1Elastic1.Grid(gsColWidth, 0) + C1Elastic1.Grid(gsColWidth, 1) - 230
''  Next
'End Sub

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("CONCILIAR").Image = "boton_conciliar"
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With
  
  With Toolbar_Buscar_Cont
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("BUSCAR").Image = "boton_conectar"
      '.Buttons("PRINT").Image = cBoton_Imprimir
  End With
  
  With Toolbar_Buscar_CSBPI
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("BUSCAR").Image = "boton_conectar"
      '.Buttons("PRINT").Image = cBoton_Imprimir
  End With
  
  Set fConciliador = Nothing
  
  Me.Width = 11385
  
  Call Sub_FormControl_Color(Me.Controls)
  'Call Sub_AjustaBands(CoolBar)
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
  With Toolbar
    .Buttons("MaxMin").Image = IIf(Me.WindowState = vbMaximized, cBoton_RestoreWindow, cBoton_MaxWindow)
  End With
  C1Elastic.Refresh
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Set fConciliador = Nothing
End Sub

Private Sub Toolbar_Buscar_Cont_ButtonClick(ByVal Button As MSComctlLib.Button)
On Error GoTo ErrProcedure

  Me.SetFocus
  DoEvents

  If fConciliador Is Nothing Then
    Exit Sub
  End If

  Err.Clear

  Select Case Button.Key
    Case "BUSCAR"
      Set fConciliador.gDB = gDB
      If Not fConciliador.BuscarComparación() Then
        Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                        , "No se puede conciliar por problemas en la busqueda del origen de compración (" & fKey & ")." _
                        , Err.Description _
                        , pConLog:=True)
        GoTo ExitProcedure
      End If
      
      Txt_OrigenComparacion.Text = fConciliador.MsgInformacion
  End Select

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la carga del la busqueda del origen del conciliador (" & fKey & ")." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If


ExitProcedure:

End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "CONCILIAR"
      Call Fnt_Conciliar
    Case "EXIT"
      Unload Me
    Case "MaxMin"
      Me.WindowState = IIf(Me.WindowState = vbMaximized, vbNormal, vbMaximized)
    Case "AJUSTARBARS"
      'Call Sub_AjustaBands(CoolBar)
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Toolbar_Buscar_CSBPI_ButtonClick(ByVal Button As MSComctlLib.Button)
On Error GoTo ErrProcedure

  Me.SetFocus
  DoEvents

  If fConciliador Is Nothing Then
    Exit Sub
  End If

  Err.Clear

  Select Case Button.Key
    Case "BUSCAR"
      If Not fConciliador.BuscarComparación_Local() Then
        Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                        , "No se puede conciliar por problemas en la busqueda del origen de compración (" & fKey & ")." _
                        , Err.Description _
                        , pConLog:=True)
        GoTo ExitProcedure
      End If
      
      Txt_OrigenCSBPI.Text = fConciliador.MsgInformacion_Local
  End Select

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la carga del la busqueda del origen del conciliador (" & fKey & ")." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If


ExitProcedure:

End Sub

Private Function Fnt_Conciliar() As Boolean
Dim lMensaje As String

On Error GoTo ErrProcedure
  
  
  Call Sub_Bloquea_Puntero(Me)
  
  'Desabilita todo lo que pueda entorpecer el proceso
  Toolbar_Buscar_Cont.Enabled = False
  Toolbar_Buscar_CSBPI.Enabled = False
  Toolbar.Buttons("CONCILIAR").Enabled = False
  
  Fnt_Conciliar = False
  
  fConciliador.AppName = App.ProductName
  Set fConciliador.gDB = gDB
  If Not fConciliador.Conciliar() Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                    , "Error al Conciliar" _
                    , fConciliador.ErrMsg _
                    , pConLog:=True)
    
    GoTo ExitProcedure
  End If
  
  If Grilla_Diferencias.Rows > Grilla_Diferencias.FixedRows Then
    Call Sub_AjustaColumnas_Grilla(Grilla_Diferencias)
  End If
  
  If Grilla_Contraparte.Rows > Grilla_Contraparte.FixedRows Then
    Call Sub_AjustaColumnas_Grilla(Grilla_Contraparte)
  End If
  
  If Grilla_BPI.Rows > Grilla_BPI.FixedRows Then
    Call Sub_AjustaColumnas_Grilla(Grilla_BPI)
  End If
  
  Grilla_Contraparte.Select 0, 0
  Grilla_Contraparte.Sort = flexSortGenericAscending
  
'  Grilla_Diferencias.SetFocus
  
  Fnt_Conciliar = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la carga del conciliador (" & fKey & ")." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If


ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  Toolbar_Buscar_Cont.Enabled = True
  Toolbar_Buscar_CSBPI.Enabled = True
  Toolbar.Buttons("CONCILIAR").Enabled = True
  
  MsgBox "Conciliación Terminada", vbInformation, Me.Caption
End Function

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
Dim lCuenta As String
Dim lLargo As Double
'--------------------------------------------------------
Dim lRep_Diferencias As Boolean
Dim lRep_SoloBPI As Boolean
Dim lRep_SoloContraparte As Boolean

  If fConciliador Is Nothing Then
    MsgBox "Oparación no valida.", vbInformation, Me.Caption
    Exit Sub
  End If

  If Not Frm_Conciliador_Imprimir.Mostrar(pRep_Diferencias:=lRep_Diferencias _
                                        , pRep_SoloBPI:=lRep_SoloBPI _
                                        , pRep_SoloContraparte:=lRep_SoloContraparte) Then
    Exit Sub
  End If

  fTag_Impresion = ""

  Set lForm = New Frm_Reporte_Generico
  Rem Comienzo de la generación del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:=fConciliador.CaptionConciliador _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
    
    Set .Object_Padre = Me
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      .PaperSize = pprLetter
      
      .PhysicalPage = True
      lLargo = IIf(.PaperHeight = 0, 15680, .PaperHeight) - (.MarginRight + .MarginLeft)
      
      .StartTable
      .FontSize = 9
      Call .AddTable("^=|^=|^=", Lbl_Origen_Local.Caption & "|  |" & Lbl_Origen_Contraparte.Caption, Txt_OrigenCSBPI.Text & "| |" & Txt_OrigenComparacion.Text, Grilla_BPI.BackColorFixed, , False)
      .TableCell(tcColWidth, col1:=1) = (lLargo - 100) / 2
      .TableCell(tcColWidth, col1:=2) = 100
      .TableCell(tcColWidth, col1:=3) = (lLargo - 100) / 2
      
      .TableCell(tcRowHeight, row1:=1) = 776
      
      .TableCell(tcBackColor, row1:=0, col1:=2) = vbWhite
      
      .EndTable
      
      If lRep_Diferencias Then
        fTag_Impresion = "DIF"
        .FontSize = 10
        .FontBold = True
        .Paragraph = ""
        .Paragraph = "Diferencias" 'CoolBar.Bands("DIF").Caption
        .FontBold = False
        .FontSize = 8
        Call Sub_Grilla2VsPrinter(lForm.VsPrinter _
                                , Me.Grilla_Diferencias _
                                , pTitulo4Pagina:=False _
                                , pNoEndTable:=True _
                                , pHeaders:="" _
                                , pConcideraCabecera:=True _
                                , pLineasCabezeras:=2 _
                                , pLineas1raHoja:=31 _
                                , pLineasOtrasHojas:=41)
        Call fConciliador.Conf_Print_Diferencias(lForm.VsPrinter)
        .EndTable
      End If
      
      If lRep_SoloBPI Then
        fTag_Impresion = "BPI"
        .Paragraph = ""
        .FontSize = 10
        .FontBold = True
        .Paragraph = ""
        .Paragraph = "Solo CSGPI" 'CoolBar.Bands("BPI").Caption
        .FontBold = False
        .FontSize = 8
        Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla_BPI, pNoEndTable:=True)
        Call fConciliador.Conf_Print_BPI(lForm.VsPrinter)
        .EndTable
      End If
      
      If lRep_SoloContraparte Then
        fTag_Impresion = "CONT"
        .Paragraph = ""
        .FontSize = 10
        .FontBold = True
        .Paragraph = "Solo Contraparte" 'CoolBar.Bands("CONT").Caption
        .FontBold = False
        .FontSize = 8
        Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla_Contraparte, pNoEndTable:=True)
        Call fConciliador.Conf_Print_Cont(lForm.VsPrinter)
        .EndTable
      End If
      
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub


Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "EXCEL"
      Call Sub_Excel
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

Public Sub VsPrint_NewPage(pVsPrinter As VsPrinter)
  On Error Resume Next
  Call fConciliador.VsPrint_NewPage(pVsPrinter, fTag_Impresion)
  On Error GoTo 0
End Sub

Private Sub Sub_Excel()
Dim lcExcel As Excel.Application
Dim lcLibro As Excel.Workbook
Dim lcHoja  As Excel.Worksheet
'------------------------------------------
Dim lArchivo
Dim pGrilla As VSFlexGrid
Dim lFilaGrilla As Integer
Dim lColGrilla As Integer

Dim i As Integer
Dim lFilaExcel As Integer
Dim lColExcel As Integer
Dim lFilasTotal As Integer
Dim lPosIni     As Integer
Dim lPosFin     As Integer
Dim lTextoFecha As String

On Error GoTo ErrProcedure

  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False

  lPosIni = InStr(1, Txt_OrigenComparacion.Text, "Fecha a Consultar: ") + 18
  lPosFin = Len(Txt_OrigenComparacion.Text) - InStr(1, Txt_OrigenComparacion.Text, "Fecha a Consultar: ")
  lTextoFecha = Mid(Txt_OrigenComparacion.Text, lPosIni + 1, lPosFin)
  StatusBar.SimpleText = "Seleccione Archivo..."
  StatusBar.Refresh

  lArchivo = Fnt_GuardarComo(pFileName:="Conciliación Caja " + Format(CDate(lTextoFecha), "yyyymmdd"))
  
  If IsNull(lArchivo) Then
    GoTo ExitProcedure
  End If

  StatusBar.SimpleText = "Abriendo Excel..."
  StatusBar.Refresh
  
  Set lcExcel = Fnt_CreateObject(cExcel)
  lcExcel.DisplayAlerts = False
  lcExcel.Visible = False
  Set lcLibro = lcExcel.Workbooks.Add
  
  'DIFERENCIAS
  StatusBar.SimpleText = "Exportando a Excel Diferencias..."
  StatusBar.Refresh
  
  Set lcHoja = lcLibro.Worksheets(1)
  lcHoja.Name = "Diferencias"
  lcHoja.Activate
  
  With lcLibro
    .ActiveSheet.Cells(1, 1).Select
    .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).ShapeRange.IncrementLeft 39.75
    .Worksheets.Item(1).Range("B5").Value = "Conciliación de Cajas al " & lTextoFecha
    .Worksheets.Item(1).Range("B5").Font.Size = 14
    .Worksheets.Item(1).Range("B5").Font.Bold = True
    
    .Worksheets.Item(1).Range("A7").Value = "Empresa: " & gDsc_Empresa
    .Worksheets.Item(1).Range("A7:D7").Merge
  End With
  
  With lcHoja
    Set pGrilla = Grilla_Diferencias
    
    lFilaExcel = 10
    lColExcel = 1
  
    For lFilaGrilla = 0 To (pGrilla.Rows - 1)
      pGrilla.Row = lFilaGrilla
      For lColGrilla = 0 To (pGrilla.Cols - 1)
        pGrilla.Col = lColGrilla
        If lColExcel >= 5 And lFilaExcel >= 12 Then
          .Cells(lFilaExcel, lColExcel).Value = To_Number(pGrilla.Text)
          If .Cells(lFilaExcel, 4).Value = "$" Then
            .Cells(lFilaExcel, lColExcel).NumberFormat = "#,##0" 'Fnt_FormatoNumero(0)
          ElseIf .Cells(lFilaExcel, 4).Value = "US$" Then
            If .Cells(lFilaExcel, lColExcel).Value = 0 Then
              .Cells(lFilaExcel, lColExcel).NumberFormat = "#,##0"
            Else
              .Cells(lFilaExcel, lColExcel).NumberFormat = "#,##0.00" 'Fnt_FormatoNumero(4)
            End If
          End If
        Else
          .Cells(lFilaExcel, lColExcel).Value = pGrilla.Text
        End If
        lColExcel = lColExcel + 1
      Next
      lFilaExcel = lFilaExcel + 1
      lColExcel = 1
    Next
    lFilasTotal = lFilaExcel
        
    lFilaExcel = 10
    
    For lColExcel = 1 To 4
      .Range(.Cells(lFilaExcel, lColExcel), .Cells(lFilaExcel + 1, lColExcel)).Merge
      .Range(.Cells(lFilaExcel, lColExcel), .Cells(lFilaExcel + 1, lColExcel)).HorizontalAlignment = xlLeft
      .Range(.Cells(lFilaExcel, lColExcel), .Cells(lFilaExcel + 1, lColExcel)).VerticalAlignment = xlCenter
    Next
    For lColExcel = 5 To 7
      .Range(.Cells(lFilaExcel, lColExcel), .Cells(lFilaExcel + 1, lColExcel)).HorizontalAlignment = xlLeft
      .Range(.Cells(lFilaExcel, lColExcel), .Cells(lFilaExcel + 1, lColExcel)).VerticalAlignment = xlCenter
    Next

    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel + 1, 7)).BorderAround
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel + 1, 7)).Borders.Weight = 4
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel + 1, 7)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel + 1, 7)).Interior.Color = RGB(255, 255, 0)
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel + 1, 7)).Font.Bold = True
    
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilasTotal, 7)).EntireColumn.AutoFit
  End With

  'CSGPI
  StatusBar.SimpleText = "Exportando a Excel Solo CSGPI..."
  StatusBar.Refresh

  Set lcHoja = lcLibro.Worksheets(2)
  lcHoja.Name = "Solo CSGPI"
  lcHoja.Activate

  With lcLibro
    .ActiveSheet.Cells(1, 1).Select
    .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).ShapeRange.IncrementLeft 39.75
    .Worksheets.Item(2).Range("B5").Value = "Conciliación de Cajas al " & lTextoFecha
    .Worksheets.Item(2).Range("B5").Font.Size = 14
    .Worksheets.Item(2).Range("B5").Font.Bold = True
    
    .Worksheets.Item(2).Range("A7").Value = "Empresa: " & gDsc_Empresa
    .Worksheets.Item(2).Range("A7:D7").Merge

  End With

  With lcHoja
    Set pGrilla = Grilla_BPI
    
    lFilaExcel = 10
    lColExcel = 1
  
    For lFilaGrilla = 0 To (pGrilla.Rows - 1)
      pGrilla.Row = lFilaGrilla
      For lColGrilla = 0 To (pGrilla.Cols - 1)
        pGrilla.Col = lColGrilla
        If lColExcel = 5 And lFilaExcel >= 11 Then
          .Cells(lFilaExcel, lColExcel).Value = To_Number(pGrilla.Text)
          If .Cells(lFilaExcel, 4).Value = "$" Then
            .Cells(lFilaExcel, lColExcel).NumberFormat = "#,##0" 'Fnt_FormatoNumero(0)
          ElseIf .Cells(lFilaExcel, 4).Value = "US$" Then
            If .Cells(lFilaExcel, lColExcel).Value = 0 Then
              .Cells(lFilaExcel, lColExcel).NumberFormat = "#,##0"
            Else
              .Cells(lFilaExcel, lColExcel).NumberFormat = "#,##0.00" 'Fnt_FormatoNumero(4)
            End If
          End If
        Else
          .Cells(lFilaExcel, lColExcel).Value = pGrilla.Text
        End If
        lColExcel = lColExcel + 1
      Next
      lFilaExcel = lFilaExcel + 1
      lColExcel = 1
    Next
    lFilasTotal = lFilaExcel
        
    lFilaExcel = 10
    
    For lColExcel = 1 To 6
      .Range(.Cells(lFilaExcel, lColExcel), .Cells(lFilaExcel, lColExcel)).HorizontalAlignment = xlLeft
      .Range(.Cells(lFilaExcel, lColExcel), .Cells(lFilaExcel, lColExcel)).VerticalAlignment = xlCenter
    Next

    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 6)).BorderAround
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 6)).Borders.Weight = 4
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 6)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 6)).Interior.Color = RGB(255, 255, 0)
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 6)).Font.Bold = True
    
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilasTotal, 6)).EntireColumn.AutoFit
  End With

  'CONTRAPARTE
  StatusBar.SimpleText = "Exportando a Excel Solo Contrapartes..."
  StatusBar.Refresh

  Set lcHoja = lcLibro.Worksheets(3)
  lcHoja.Name = "Solo Contraparte"
  lcHoja.Activate

  With lcLibro
    .ActiveSheet.Cells(1, 1).Select
    .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).ShapeRange.IncrementLeft 39.75
    .Worksheets.Item(3).Range("B5").Value = "Conciliación de Cajas al " & lTextoFecha
    .Worksheets.Item(3).Range("B5").Font.Size = 14
    .Worksheets.Item(3).Range("B5").Font.Bold = True
    .Worksheets.Item(3).Range("A7").Value = "Empresa: " & gDsc_Empresa
    .Worksheets.Item(3).Range("A7:D7").Merge
  
  End With

  With lcHoja
    Set pGrilla = Grilla_Contraparte
    
    lFilaExcel = 10
    lColExcel = 1
  
    For lFilaGrilla = 0 To (pGrilla.Rows - 1)
      pGrilla.Row = lFilaGrilla
      For lColGrilla = 0 To (pGrilla.Cols - 1)
        pGrilla.Col = lColGrilla
        If lColExcel = 3 And lFilaExcel >= 11 Then
          .Cells(lFilaExcel, lColExcel).Value = To_Number(pGrilla.Text)
          If .Cells(lFilaExcel, 2).Value = "$" Then
            .Cells(lFilaExcel, lColExcel).NumberFormat = "#,##0" 'Fnt_FormatoNumero(0)
          ElseIf .Cells(lFilaExcel, 2).Value = "US$" Then
            If .Cells(lFilaExcel, lColExcel).Value = 0 Then
              .Cells(lFilaExcel, lColExcel).NumberFormat = "#,##0"
            Else
              .Cells(lFilaExcel, lColExcel).NumberFormat = "#,##0.00" 'Fnt_FormatoNumero(4)
            End If
          End If
        Else
          .Cells(lFilaExcel, lColExcel).Value = pGrilla.Text
        End If
        lColExcel = lColExcel + 1
      Next
      lFilaExcel = lFilaExcel + 1
      lColExcel = 1
    Next
    lFilasTotal = lFilaExcel
        
    lFilaExcel = 10
    
    For lColExcel = 1 To 3
      .Range(.Cells(lFilaExcel, lColExcel), .Cells(lFilaExcel, lColExcel)).HorizontalAlignment = xlLeft
      .Range(.Cells(lFilaExcel, lColExcel), .Cells(lFilaExcel, lColExcel)).VerticalAlignment = xlCenter
    Next

    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 3)).BorderAround
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 3)).Borders.Weight = 4
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 3)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 3)).Interior.Color = RGB(255, 255, 0)
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 3)).Font.Bold = True
    
    .Range(.Cells(lFilaExcel, 1), .Cells(lFilasTotal, 3)).EntireColumn.AutoFit
  End With
  
  lcLibro.Worksheets(1).Activate
  Call lcLibro.SaveAs(lArchivo)

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al exportar a Excel.", Err.Description)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  lcExcel.Visible = True
  lcExcel.DisplayAlerts = True

  Set lcExcel = Nothing
  Set lcLibro = Nothing
  Set lcHoja = Nothing
  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)

  StatusBar.SimpleText = "Listo."
  StatusBar.Refresh
End Sub

Public Function Fnt_GuardarComo(Optional pFileName = "" _
                               , Optional pFilter = "Planilla Excel (*.xls)|*.xls|Todos los Archivos (*.*)|*.*" _
                               , Optional pFlags = cdlOFNExplorer + cdlOFNLongNames + cdlOFNPathMustExist + cdlOFNHideReadOnly + cdlOFNNoReadOnlyReturn + cdlOFNOverwritePrompt)
On Error GoTo ErrProcedure
  
  Fnt_GuardarComo = Null
  
  With MDI_Principal.CommonDialog
    .CancelError = True
    .Filter = pFilter
    .Flags = pFlags
    .Filename = pFileName
    .ShowSave
    
    Rem Se coloca el path completo, que incluye el nombre de archivo
    Fnt_GuardarComo = .Filename
  End With
  
ErrProcedure:
  Rem Usuario presiona el boton Cancelar del Command Dialog
  Rem o presiona No o Cancelar en el dialogo "ya existe archivo...desea reemplazarlo"
  If Err.Number = 32755 Or Err.Number = 1004 Then
    GoTo ExitProcedure
  End If
  
  If Not Err.Number = 0 Then
    MsgBox "Error en la exportación de datos a la planilla Excel.", vbExclamation, "Error en Guarda Como..."
    GoTo ExitProcedure
  End If
  
ExitProcedure:

End Function

Public Function Fnt_FormatoNumero(pDecimales) As String
Dim lFormato  As String
Dim lNum      As Integer
Dim lSepDecimal As String
Dim lSepMiles   As String

    lSepDecimal = Fnt_CualASCIIDecimal
    lSepMiles = Fnt_CualASCIISeparadorMiles
    lFormato = "#" & lSepMiles & "##0"
    If pDecimales > 0 Then
        lFormato = lFormato & lSepDecimal
        For lNum = 1 To pDecimales '- 1
            lFormato = lFormato & "0"
        Next
    End If
    Fnt_FormatoNumero = lFormato
End Function

Public Function Fnt_CualASCIIDecimal()
Dim lNum_String As String

  lNum_String = Format(3.5, "0.0")

  Fnt_CualASCIIDecimal = Mid(lNum_String, 2, 1)
End Function

Public Function Fnt_CualASCIISeparadorMiles()
Dim lNum_String As String

  lNum_String = Format(1000, "0,000")

  Fnt_CualASCIISeparadorMiles = Mid(lNum_String, 2, 1)
End Function
