VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Valoracion_BonosRec 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Valoraci�n Bonos Reconocimiento"
   ClientHeight    =   7065
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11865
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7065
   ScaleWidth      =   11865
   Begin TrueDBList80.TDBCombo TDBCombo1 
      Height          =   345
      Left            =   13290
      TabIndex        =   40
      Tag             =   "OBLI"
      Top             =   660
      Width           =   4065
      _ExtentX        =   7170
      _ExtentY        =   609
      _LayoutType     =   4
      _RowHeight      =   -2147483647
      _WasPersistedAsPixels=   0
      _DropdownWidth  =   0
      _EDITHEIGHT     =   609
      _GAPHEIGHT      =   53
      Columns(0)._VlistStyle=   0
      Columns(0)._MaxComboItems=   5
      Columns(0).Caption=   "Nemot�cnico"
      Columns(0).DataField=   ""
      Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
      Columns(1)._VlistStyle=   0
      Columns(1)._MaxComboItems=   5
      Columns(1).Caption=   "Descripci�n"
      Columns(1).DataField=   ""
      Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
      Columns.Count   =   2
      Splits(0)._UserFlags=   0
      Splits(0).ExtendRightColumn=   -1  'True
      Splits(0).AllowRowSizing=   0   'False
      Splits(0).AllowColSelect=   0   'False
      Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
      Splits(0)._ColumnProps(0)=   "Columns.Count=2"
      Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
      Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
      Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
      Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
      Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
      Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
      Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
      Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
      Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
      Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
      Splits.Count    =   1
      Appearance      =   1
      BorderStyle     =   1
      ComboStyle      =   0
      AutoCompletion  =   -1  'True
      LimitToList     =   0   'False
      ColumnHeaders   =   -1  'True
      ColumnFooters   =   0   'False
      DataMode        =   5
      DefColWidth     =   0
      Enabled         =   -1  'True
      HeadLines       =   1
      FootLines       =   1
      RowDividerStyle =   6
      Caption         =   ""
      EditFont        =   "Size=8.25,Charset=0,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
      LayoutName      =   ""
      LayoutFileName  =   ""
      MultipleLines   =   0
      EmptyRows       =   0   'False
      CellTips        =   2
      AutoSize        =   -1  'True
      ListField       =   ""
      BoundColumn     =   ""
      IntegralHeight  =   0   'False
      CellTipsWidth   =   0
      CellTipsDelay   =   1000
      AutoDropdown    =   0   'False
      RowTracking     =   -1  'True
      RightToLeft     =   0   'False
      MouseIcon       =   0
      MouseIcon.vt    =   3
      MousePointer    =   0
      MatchEntryTimeout=   2000
      OLEDragMode     =   0
      OLEDropMode     =   0
      AnimateWindow   =   3
      AnimateWindowDirection=   0
      AnimateWindowTime=   200
      AnimateWindowClose=   2
      DropdownPosition=   0
      Locked          =   0   'False
      ScrollTrack     =   0   'False
      ScrollTips      =   -1  'True
      RowDividerColor =   14933984
      RowSubDividerColor=   14933984
      AddItemSeparator=   ";"
      _PropDict       =   $"Frm_Valoracion_BonosRec.frx":0000
      _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
      _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
      _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
      _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
      _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=0"
      _StyleDefs(5)   =   ":id=0,.fontname=MS Sans Serif"
      _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&,.bold=0,.fontsize=825"
      _StyleDefs(7)   =   ":id=1,.italic=0,.underline=0,.strikethrough=0,.charset=0"
      _StyleDefs(8)   =   ":id=1,.fontname=Arial"
      _StyleDefs(9)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
      _StyleDefs(10)  =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
      _StyleDefs(11)  =   "FooterStyle:id=3,.parent=1,.namedParent=35"
      _StyleDefs(12)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(13)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
      _StyleDefs(14)  =   "EditorStyle:id=7,.parent=1"
      _StyleDefs(15)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
      _StyleDefs(16)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
      _StyleDefs(17)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
      _StyleDefs(18)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
      _StyleDefs(19)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
      _StyleDefs(20)  =   "Splits(0).Style:id=13,.parent=1"
      _StyleDefs(21)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
      _StyleDefs(22)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
      _StyleDefs(23)  =   "Splits(0).FooterStyle:id=15,.parent=3"
      _StyleDefs(24)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
      _StyleDefs(25)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
      _StyleDefs(26)  =   "Splits(0).EditorStyle:id=17,.parent=7"
      _StyleDefs(27)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
      _StyleDefs(28)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
      _StyleDefs(29)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
      _StyleDefs(30)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
      _StyleDefs(31)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
      _StyleDefs(32)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
      _StyleDefs(33)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
      _StyleDefs(34)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
      _StyleDefs(35)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
      _StyleDefs(36)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
      _StyleDefs(37)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
      _StyleDefs(38)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
      _StyleDefs(39)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
      _StyleDefs(40)  =   "Named:id=33:Normal"
      _StyleDefs(41)  =   ":id=33,.parent=0"
      _StyleDefs(42)  =   "Named:id=34:Heading"
      _StyleDefs(43)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(44)  =   ":id=34,.wraptext=-1"
      _StyleDefs(45)  =   "Named:id=35:Footing"
      _StyleDefs(46)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(47)  =   "Named:id=36:Selected"
      _StyleDefs(48)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
      _StyleDefs(49)  =   "Named:id=37:Caption"
      _StyleDefs(50)  =   ":id=37,.parent=34,.alignment=2"
      _StyleDefs(51)  =   "Named:id=38:HighlightRow"
      _StyleDefs(52)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
      _StyleDefs(53)  =   "Named:id=39:EvenRow"
      _StyleDefs(54)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
      _StyleDefs(55)  =   "Named:id=40:OddRow"
      _StyleDefs(56)  =   ":id=40,.parent=33"
      _StyleDefs(57)  =   "Named:id=41:RecordSelector"
      _StyleDefs(58)  =   ":id=41,.parent=34"
      _StyleDefs(59)  =   "Named:id=42:FilterBar"
      _StyleDefs(60)  =   ":id=42,.parent=33"
      _StyleDefs(61)  =   "Named:id=43:StyleCreasys"
      _StyleDefs(62)  =   ":id=43,.parent=33"
   End
   Begin VB.Frame Frame1 
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6615
      Left            =   60
      TabIndex        =   1
      Top             =   360
      Width           =   11715
      Begin MSComCtl2.DTPicker dtp_Fecha_Actual 
         Height          =   315
         Left            =   1680
         TabIndex        =   0
         Top             =   210
         Width           =   1845
         _ExtentX        =   3254
         _ExtentY        =   556
         _Version        =   393216
         Format          =   64815105
         CurrentDate     =   38876
      End
      Begin VB.TextBox txt_fecha_emision 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8220
         TabIndex        =   42
         Top             =   240
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1485
         Left            =   90
         TabIndex        =   30
         Top             =   540
         Width           =   7635
         Begin MSComCtl2.DTPicker dtp_Mes_Emision 
            Height          =   345
            Left            =   5820
            TabIndex        =   3
            Top             =   210
            Width           =   1695
            _ExtentX        =   2990
            _ExtentY        =   609
            _Version        =   393216
            CalendarBackColor=   12648384
            CustomFormat    =   "MMMM"
            Format          =   64815107
            UpDown          =   -1  'True
            CurrentDate     =   38876
         End
         Begin MSComCtl2.DTPicker dtp_Ano_Emision 
            Height          =   315
            Left            =   5820
            TabIndex        =   4
            Top             =   630
            Width           =   1695
            _ExtentX        =   2990
            _ExtentY        =   556
            _Version        =   393216
            CustomFormat    =   "yyyy"
            Format          =   64815107
            UpDown          =   -1  'True
            CurrentDate     =   38876
         End
         Begin MSComCtl2.DTPicker dtp_fecha_vcto 
            Height          =   315
            Left            =   5820
            TabIndex        =   5
            Tag             =   "OBLI"
            Top             =   1020
            Width           =   1695
            _ExtentX        =   2990
            _ExtentY        =   556
            _Version        =   393216
            CalendarBackColor=   16777215
            CalendarTitleBackColor=   14737632
            Format          =   64815105
            CurrentDate     =   38876
         End
         Begin VB.TextBox cmb_Nemotecnico 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1560
            TabIndex        =   2
            Top             =   210
            Width           =   2445
         End
         Begin VB.TextBox txt_emisor 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   4050
            TabIndex        =   46
            Top             =   1740
            Visible         =   0   'False
            Width           =   1695
         End
         Begin TrueDBList80.TDBCombo Cmb_Nemotecnico_old 
            Height          =   345
            Left            =   1575
            TabIndex        =   12
            Top             =   210
            Width           =   2115
            _ExtentX        =   3731
            _ExtentY        =   609
            _LayoutType     =   4
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).Caption=   "Nemot�cnico"
            Columns(0).DataField=   ""
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).Caption=   "Descripci�n"
            Columns(1).DataField=   ""
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0).AllowColSelect=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   1
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   -1  'True
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   6
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=0,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   2
            AutoSize        =   -1  'True
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   0   'False
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   0
            AnimateWindowTime=   200
            AnimateWindowClose=   2
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   0   'False
            ScrollTips      =   -1  'True
            RowDividerColor =   14933984
            RowSubDividerColor=   14933984
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Valoracion_BonosRec.frx":00AA
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(5)   =   ":id=0,.fontname=MS Sans Serif"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&,.bold=0,.fontsize=825"
            _StyleDefs(7)   =   ":id=1,.italic=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(8)   =   ":id=1,.fontname=Arial"
            _StyleDefs(9)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(10)  =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(11)  =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(12)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(13)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(14)  =   "EditorStyle:id=7,.parent=1,.bgcolor=&H80000005&"
            _StyleDefs(15)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(16)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(17)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(18)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(19)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(20)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(21)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(22)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(23)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(24)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(25)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(26)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(27)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(28)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(29)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(30)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(31)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(32)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(33)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(34)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(35)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(36)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(37)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(38)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(39)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(40)  =   "Named:id=33:Normal"
            _StyleDefs(41)  =   ":id=33,.parent=0"
            _StyleDefs(42)  =   "Named:id=34:Heading"
            _StyleDefs(43)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(44)  =   ":id=34,.wraptext=-1"
            _StyleDefs(45)  =   "Named:id=35:Footing"
            _StyleDefs(46)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(47)  =   "Named:id=36:Selected"
            _StyleDefs(48)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(49)  =   "Named:id=37:Caption"
            _StyleDefs(50)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(51)  =   "Named:id=38:HighlightRow"
            _StyleDefs(52)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(53)  =   "Named:id=39:EvenRow"
            _StyleDefs(54)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(55)  =   "Named:id=40:OddRow"
            _StyleDefs(56)  =   ":id=40,.parent=33"
            _StyleDefs(57)  =   "Named:id=41:RecordSelector"
            _StyleDefs(58)  =   ":id=41,.parent=34"
            _StyleDefs(59)  =   "Named:id=42:FilterBar"
            _StyleDefs(60)  =   ":id=42,.parent=33"
            _StyleDefs(61)  =   "Named:id=43:StyleCreasys"
            _StyleDefs(62)  =   ":id=43,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Instrumento 
            Height          =   345
            Left            =   1590
            TabIndex        =   49
            Tag             =   "OBLI=S;CAPTION=Instrumento"
            Top             =   630
            Width           =   2445
            _ExtentX        =   4313
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Valoracion_BonosRec.frx":0154
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_moneda 
            Height          =   345
            Left            =   1590
            TabIndex        =   50
            Tag             =   "OBLI=S;CAPTION=Instrumento"
            Top             =   1020
            Width           =   2445
            _ExtentX        =   4313
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Valoracion_BonosRec.frx":01FE
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Label2 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Nemot�nico"
            Height          =   345
            Left            =   165
            TabIndex        =   36
            Top             =   210
            Width           =   1395
         End
         Begin VB.Label Label5 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Instrumento"
            Height          =   315
            Left            =   165
            TabIndex        =   35
            Top             =   630
            Width           =   1395
         End
         Begin VB.Label Label16 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Mes Emisi�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   4110
            TabIndex        =   34
            Top             =   210
            Width           =   1695
         End
         Begin VB.Label Label17 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "A�o Emisi�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4110
            TabIndex        =   33
            Top             =   630
            Width           =   1695
         End
         Begin VB.Label Label18 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Vcto."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4110
            TabIndex        =   32
            Top             =   1020
            Width           =   1695
         End
         Begin VB.Label Label19 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Moneda UM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   165
            TabIndex        =   31
            Top             =   1020
            Width           =   1395
         End
      End
      Begin VB.Frame Frame3 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1485
         Left            =   7770
         TabIndex        =   26
         Top             =   540
         Width           =   3855
         Begin VB.TextBox txt_tasa_valoracion 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1800
            TabIndex        =   6
            Tag             =   "OBLI"
            Top             =   210
            Width           =   1965
         End
         Begin VB.TextBox txt_valor_nominal 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1800
            TabIndex        =   7
            Tag             =   "OBLI"
            Top             =   630
            Width           =   1965
         End
         Begin VB.TextBox txt_presente_pesos 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1800
            TabIndex        =   8
            Tag             =   "OBLI"
            Top             =   1020
            Width           =   1965
         End
         Begin VB.Label Label20 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Tasa Valorizaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   90
            TabIndex        =   29
            Top             =   210
            Width           =   1695
         End
         Begin VB.Label Label21 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Valor Nominal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   90
            TabIndex        =   28
            Top             =   630
            Width           =   1695
         End
         Begin VB.Label Label22 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Valor Presente Pesos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   90
            TabIndex        =   27
            Top             =   1020
            Width           =   1695
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Resultados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1725
         Left            =   90
         TabIndex        =   13
         Top             =   2040
         Width           =   9495
         Begin VB.TextBox txt_tasa_emision 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1800
            TabIndex        =   45
            Top             =   1830
            Visible         =   0   'False
            Width           =   2085
         End
         Begin VB.TextBox txt_dias_ref 
            BackColor       =   &H00C0FFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   6090
            TabIndex        =   43
            Top             =   1860
            Visible         =   0   'False
            Width           =   2085
         End
         Begin VB.TextBox txt_valor_par_UM 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1530
            Locked          =   -1  'True
            TabIndex        =   19
            Tag             =   "OBLI"
            Top             =   360
            Width           =   2415
         End
         Begin VB.TextBox txt_valor_par_pesos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1530
            Locked          =   -1  'True
            TabIndex        =   18
            Tag             =   "OBLI"
            Top             =   780
            Width           =   2415
         End
         Begin VB.TextBox txt_porcentaje_valor_par 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1530
            Locked          =   -1  'True
            TabIndex        =   17
            Tag             =   "OBLI"
            Top             =   1200
            Width           =   2415
         End
         Begin VB.TextBox txt_valor_presente_UM 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   5820
            Locked          =   -1  'True
            TabIndex        =   16
            Tag             =   "OBLI"
            Top             =   360
            Width           =   2085
         End
         Begin VB.TextBox txt_valor_presente_pesos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   5820
            Locked          =   -1  'True
            TabIndex        =   15
            Tag             =   "OBLI"
            Top             =   780
            Width           =   2085
         End
         Begin VB.TextBox txt_TIR 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   5820
            Locked          =   -1  'True
            TabIndex        =   14
            Tag             =   "OBLI"
            Top             =   1200
            Width           =   2085
         End
         Begin VB.Label Label3 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Dias de Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   4350
            TabIndex        =   44
            Top             =   1860
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.Label Label7 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Valor Par UM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   150
            TabIndex        =   25
            Top             =   360
            Width           =   1335
         End
         Begin VB.Label Label8 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Valor Par Pesos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   150
            TabIndex        =   24
            Top             =   780
            Width           =   1335
         End
         Begin VB.Label Label9 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "% Valor Par"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   150
            TabIndex        =   23
            Top             =   1200
            Width           =   1335
         End
         Begin VB.Label Label11 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Valor Presente UM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   4080
            TabIndex        =   22
            Top             =   360
            Width           =   1695
         End
         Begin VB.Label Label12 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Valor Presente Pesos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   4080
            TabIndex        =   21
            Top             =   780
            Width           =   1695
         End
         Begin VB.Label Label13 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "TIR"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   4080
            TabIndex        =   20
            Top             =   1200
            Width           =   1695
         End
      End
      Begin MSComDlg.CommonDialog Dialogo 
         Left            =   2250
         Top             =   6180
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         DefaultExt      =   "XLS"
         DialogTitle     =   "Grabar Grilla"
         Filter          =   ".XLS"
      End
      Begin MSComctlLib.Toolbar Toolbar3 
         Height          =   600
         Left            =   9870
         TabIndex        =   10
         Top             =   3000
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   1058
         ButtonWidth     =   1217
         ButtonHeight    =   953
         Appearance      =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Agregar"
               Key             =   "ADD"
               Description     =   "Agrega a la grilla"
               Object.ToolTipText     =   "Agrega a la grilla"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Quitar"
               Key             =   "QUITA"
               Description     =   "Quita Fila de la grila"
               Object.ToolTipText     =   "Quita Fila de la grila"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar Toolbar2 
         Height          =   600
         Left            =   9870
         TabIndex        =   9
         Top             =   2220
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   1058
         ButtonWidth     =   1191
         ButtonHeight    =   953
         Appearance      =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Calcular"
               Key             =   "CALC"
               Description     =   "Calcula Valor del Instrumento"
               Object.ToolTipText     =   "Calcula Valor del Instrumento"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Limpiar"
               Key             =   "LIMP"
               Description     =   "Limpia el Formulario (No la Grilla)"
               Object.ToolTipText     =   "Limpia el Formulario (No la Grilla)"
            EndProperty
         EndProperty
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   2295
         Left            =   60
         TabIndex        =   11
         Top             =   3840
         Width           =   11535
         _cx             =   20346
         _cy             =   4048
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   8
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Valoracion_BonosRec.frx":02A8
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin VB.Label Label6 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha de C�lculo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   270
         TabIndex        =   39
         Top             =   210
         Width           =   1395
      End
      Begin VB.Label Label23 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8490
         TabIndex        =   38
         Top             =   6180
         Width           =   675
      End
      Begin VB.Label lbl_total 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9180
         TabIndex        =   37
         Top             =   6180
         Width           =   2415
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   47
      Top             =   0
      Width           =   11865
      _ExtentX        =   20929
      _ExtentY        =   635
      ButtonWidth     =   1561
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Excel"
            Key             =   "EXCEL"
            Description     =   "Traspasa la grilla a Excel"
            Object.ToolTipText     =   "Traspasa la grilla a Excel"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Description     =   "Salir"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   5220
         TabIndex        =   48
         Top             =   360
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Nemot�nico"
      Height          =   345
      Left            =   11910
      TabIndex        =   41
      Top             =   660
      Width           =   1365
   End
End
Attribute VB_Name = "Frm_Valoracion_BonosRec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fSalir      As Boolean

Private Function ValidaAgregar() As Boolean
   If Not Trim(Cmb_Instrumento.Text) = "" And _
      Not Trim(Cmb_Moneda.Text) = "" And _
      Not Trim(dtp_fecha_vcto.Value) = "" And _
      Not Trim(txt_dias_ref.Text) = "" And _
      Not Trim(txt_valor_nominal.Text) = "" Then
      
      ValidaAgregar = True
   Else
      ValidaAgregar = False
   End If
End Function

Private Function Buscar_Dias_Referencia()

  With gDB
    .Parametros.Clear
    .Procedimiento = "valrf.pkg_k.dias_ref_br"
          
    If .EjecutaFunction(eParam_Tipos.ePT_Numero) Then
      Buscar_Dias_Referencia = .Parametros("presultado").Valor
    End If
    .Parametros.Clear
  End With
  
End Function

Private Sub Calcular()
   Dim p_VparMonEmi As Double
   Dim cod_Ret As Integer
   Dim p_VparMonLoc As Double
   Dim p_VPteMonEmi As Double
   Dim p_VPteMonLoc As Double
   Dim p_durat As Double
   Dim p_madur As Double
   Dim p_PorcValPar As Double
   Dim p_tas_cal As Double
   Dim p_nemotecnico As String
   Dim p_VExtMonEmi As Double
 
   gDB.Parametros.Clear
   gDB.Procedimiento = "valrf.pkg_func_nemo.sp_Nemo_BonosRec"
   
   gDB.Parametros.Add "p_cod_ins", ePT_Caracter, Cmb_Instrumento.Tag, ePD_Entrada
   gDB.Parametros.Add "p_fec_emi", ePT_Fecha, CDate(Format(Txt_Fecha_Emision.Text, cFormatDate)), ePD_Entrada
   gDB.Parametros.Add "p_fec_ven", ePT_Fecha, CDate(Format(dtp_fecha_vcto.Value, cFormatDate)), ePD_Entrada
   
   gDB.Parametros.Add "p_nemotec", ePT_Caracter, "", ePD_Salida
   gDB.Parametros.Add "p_cod_ret", ePT_Numero, "", ePD_Salida
   gDB.Parametros.Add "p_men_err", ePT_Caracter, "", ePD_Salida
   
   If gDB.EjecutaSP Then
      If (gDB.Parametros("p_cod_ret").Valor = 0) Then
         p_nemotecnico = gDB.Parametros("p_nemotec").Valor
                            
         Cmb_Nemotecnico.Text = p_nemotecnico
         
         Call Sp_Desc_Nemo_Bonosrec
      Else
         MsgBox "Package " & gDB.Procedimiento & " : " & gDB.Parametros("p_men_err").Valor & vbCrLf & gDB.ErrMsg, vbCritical + vbExclamation + vbOKOnly, Me.Caption
      End If
   Else
      MsgBox "No se Ejecuto Package " & gDB.Procedimiento & " : " & vbCrLf & " Pkg_Func_Nemo.Sp_Desc_Nemo " & gDB.ErrMsg, vbCritical + vbOKOnly, Me.Caption
   End If
            
   If Not txt_tasa_valoracion.Text = "" Then 'Ejecutar Package con tasa de valoraci�n
      'Ejecutar Package con valor presente pesos
      gDB.Parametros.Clear
      gDB.Procedimiento = "valrf.pkg_valora_rf.sp_valbonosrec"
      gDB.Parametros.Add "p_fecha_emision", ePT_Fecha, Txt_Fecha_Emision.Text, ePD_Entrada
      gDB.Parametros.Add "p_fecha_calculo", ePT_Fecha, dtp_Fecha_Actual.Value, ePD_Entrada
      gDB.Parametros.Add "p_fecha_extincion", ePT_Fecha, dtp_fecha_vcto.Value, ePD_Entrada
      gDB.Parametros.Add "p_tasa_emision", ePT_Numero, txt_tasa_emision.Text, ePD_Entrada
      gDB.Parametros.Add "p_tasa_calculo", ePT_Numero, txt_tasa_valoracion.Text, ePD_Entrada
      gDB.Parametros.Add "p_val_nom", ePT_Numero, txt_valor_nominal.Text, ePD_Entrada
      
      gDB.Parametros.Add "p_VPteMonEmi", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_VParMonEmi", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_VExtMonEmi", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_durat", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_madur", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_PorcValPar", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_cod_ret", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_men_err", ePT_Caracter, "", ePD_Salida
      
      If gDB.EjecutaSP Then
         If (gDB.Parametros("p_cod_ret").Valor = 0) Then
            p_VparMonEmi = gDB.Parametros("p_VParMonEmi").Valor
            p_VparMonLoc = gDB.Parametros("p_VParMonEmi").Valor
            p_VPteMonEmi = gDB.Parametros("p_VPteMonEmi").Valor
            p_VPteMonLoc = gDB.Parametros("p_VPteMonEmi").Valor
            p_VExtMonEmi = gDB.Parametros("p_VExtMonEmi").Valor
            p_durat = gDB.Parametros("p_durat").Valor
            p_madur = gDB.Parametros("p_madur").Valor
            p_PorcValPar = gDB.Parametros("p_PorcValPar").Valor
            
            txt_valor_par_UM.Text = Format(p_VparMonEmi, "###,###,###.##")
            txt_valor_par_pesos.Text = Format(p_VparMonLoc, "###,###,###.##")
            txt_valor_presente_UM.Text = Format(p_VPteMonEmi, "###,###,###.##")
            txt_valor_presente_pesos.Text = Format(p_VPteMonLoc, "###,###,###.##")
            txt_porcentaje_valor_par.Text = Format(p_PorcValPar, "###,###,###.##")
            txt_TIR.Text = Format(txt_tasa_valoracion.Text, "###,###,###.##")
         Else
            MsgBox "Package " & gDB.Procedimiento & " : " & gDB.Parametros("p_men_err").Valor, vbCritical + vbExclamation + vbOKOnly, Me.Caption
         End If
      Else
         MsgBox "No se Ejecuto Package " & gDB.Procedimiento & " : " & vbCrLf & " Pkg_Func_Nemo.Sp_Desc_Nemo " & gDB.ErrMsg, vbCritical + vbOKOnly, Me.Caption
      End If
   Else
      If Not txt_valor_presente_pesos.Text = "" Then
         gDB.Parametros.Clear
         gDB.Procedimiento = "valrf.pkg_tir_rf.sp_TirBonosRec"
         
         gDB.Parametros.Add "p_fec_cal", ePT_Fecha, dtp_Fecha_Actual.Value, ePD_Entrada
         gDB.Parametros.Add "p_fec_emi", ePT_Fecha, Txt_Fecha_Emision.Text, ePD_Entrada
         gDB.Parametros.Add "p_fec_ven", ePT_Fecha, dtp_fecha_vcto.Value, ePD_Entrada
         
         gDB.Parametros.Add "p_tas_emi", ePT_Numero, txt_tasa_emision.Text, ePD_Entrada
         gDB.Parametros.Add "p_val_pte", ePT_Numero, txt_valor_presente_pesos.Text, ePD_Entrada
         gDB.Parametros.Add "p_val_nom", ePT_Numero, txt_valor_nominal.Text, ePD_Entrada
         
         gDB.Parametros.Add "p_tas_cal", ePT_Numero, "", ePD_Salida
         gDB.Parametros.Add "p_cod_ret", ePT_Numero, "", ePD_Salida
         gDB.Parametros.Add "p_men_err", ePT_Caracter, "", ePD_Salida
         
         If gDB.EjecutaSP Then
            If (gDB.Parametros("p_cod_ret").Valor = 0) Then
               p_tas_cal = gDB.Parametros("p_tas_cal").Valor
               txt_TIR.Text = Format(p_tas_cal, "###,###,###.##")
            Else
               MsgBox "Package " & gDB.Procedimiento & " : " & gDB.Parametros("p_men_err").Valor, vbCritical + vbExclamation + vbOKOnly, Me.Caption
            End If
         Else
            MsgBox "No se Ejecuto Package " & gDB.Procedimiento & " : " & vbCrLf & " Pkg_Func_Nemo.Sp_Desc_Nemo " & gDB.ErrMsg, vbCritical + vbOKOnly, Me.Caption
         End If
      End If
   End If
End Sub

Private Sub CargaDatos()
   dtp_Fecha_Actual.Value = Format(Fnt_FechaServidor, cFormatDate)
        
   Load Me
End Sub

Function DateCmp(fecha_calculo As Date, fecha_emision As Date) As Boolean
   ' retorna se la segunda fecha es anterior a la primera TRUE
   ' y FALSE en caso de la primera ser anterior o igual a la segunda
   If (fecha_calculo <= fecha_emision) Then
      DateCmp = True
   Else
      DateCmp = False
   End If
End Function

Private Sub Limpiar_Form()
   Dim obj As Control
      
   For Each obj In Me.Controls
      If TypeOf obj Is TextBox Then
         obj.Text = ""
         obj.Tag = ""
      End If
   Next
   If (dtp_Fecha_Actual.Value = "") Then
      dtp_Fecha_Actual.Value = Format(Fnt_FechaServidor, cFormatDate)
   End If
   Cmb_Nemotecnico.SetFocus
End Sub

Private Sub Sp_Desc_Nemo_Bonosrec()
   Dim lId_Nemotecnico As String
   Dim Campo As hFields
   Dim ID_Emisor_Especifico As Variant
   Dim Cod_Emisor_Especifico As Variant
   Dim Cod_Instrumento As String
   Dim Dsc_Instrumento As String
   Dim Dsc_Emisor As String
      
   Dim cod_Serie As String
   Dim cod_Moneda As String
   Dim Tasa_Emision As Double
   Dim fecha_emision As Date
   Dim Cupon_Vigente As Integer
   Dim Fecha_Prox_Venc As Variant
   
   lId_Nemotecnico = Cmb_Nemotecnico.Text
            
   gDB.Parametros.Clear
   gDB.Procedimiento = "VALRF.Pkg_Func_Nemo.sp_Desc_Nemo_BonosRec"
   gDB.Parametros.Add "p_nemotec", ePT_Caracter, lId_Nemotecnico, ePD_Entrada
   
   gDB.Parametros.Add "p_cod_emi", ePT_Caracter, "", ePD_Salida
   gDB.Parametros.Add "p_cod_ins", ePT_Caracter, "", ePD_Salida
   gDB.Parametros.Add "p_cod_mon", ePT_Caracter, "", ePD_Salida
   gDB.Parametros.Add "p_fec_emi", ePT_Fecha, "", ePD_Salida
   gDB.Parametros.Add "p_fec_ven", ePT_Fecha, "", ePD_Salida
   gDB.Parametros.Add "p_tip_val", ePT_Numero, "", ePD_Salida
   gDB.Parametros.Add "p_tas_emi", ePT_Numero, "", ePD_Salida
   gDB.Parametros.Add "p_dia_ref", ePT_Numero, "", ePD_Salida
   gDB.Parametros.Add "p_tip_int", ePT_Numero, "", ePD_Salida
   gDB.Parametros.Add "p_tip_nom", ePT_Numero, "", ePD_Salida
   gDB.Parametros.Add "p_cod_ret", ePT_Numero, "", ePD_Salida
   gDB.Parametros.Add "p_men_error", ePT_Caracter, "", ePD_Salida
   
   If gDB.EjecutaSP Then
      If gDB.Parametros("p_cod_ret").Valor = 0 Then
         fecha_emision = gDB.Parametros("p_fec_emi").Valor
         Txt_Fecha_Emision.Text = fecha_emision
         dtp_Mes_Emision.Value = Format(fecha_emision, cFormatDate)
         dtp_Ano_Emision.Value = fecha_emision
         txt_tasa_emision.Text = gDB.Parametros("p_tas_emi").Valor
         Tasa_Emision = gDB.Parametros("p_tas_emi").Valor
         Fecha_Prox_Venc = Format(gDB.Parametros("p_fec_ven").Valor, cFormatDate)
         dtp_fecha_vcto.Value = Format(Fecha_Prox_Venc, cFormatDate)
         cod_Moneda = gDB.Parametros("p_cod_mon").Valor
         Cmb_Moneda.Tag = cod_Moneda
         txt_dias_ref.Text = gDB.Parametros("p_dia_ref").Valor
         Cmb_Instrumento.Tag = gDB.Parametros("p_cod_ins").Valor
         Txt_Emisor.Tag = gDB.Parametros("p_cod_emi").Valor
                        
         gDB.Parametros.Clear
         gDB.Tabla = "VALRF.EMISORRF"
         gDB.Parametros.Add "COD_EMISOR", ePT_Caracter, Txt_Emisor.Tag, ePD_Entrada
         If gDB.EjecutaSelect("") Then
            For Each Campo In gDB.Parametros("Cursor").Valor
               Txt_Emisor.Text = Campo("dsc_emisor")
            Next
         End If
                        
         gDB.Parametros.Clear
         gDB.Tabla = "VALRF.INSTRUMENTORF"
         gDB.Parametros.Add "COD_INSTRUMENTO", ePT_Caracter, Cmb_Instrumento.Tag, ePD_Entrada
         If gDB.EjecutaSelect("") Then
            For Each Campo In gDB.Parametros("Cursor").Valor
               Cmb_Instrumento.Text = Campo("dsc_instrumento")
            Next
         End If
                        
         gDB.Parametros.Clear
         gDB.Tabla = "VALRF.MONEDAS"
         gDB.Parametros.Add "COD_MONEDA", ePT_Caracter, Cmb_Moneda.Tag, ePD_Entrada
         If gDB.EjecutaSelect("") Then
            For Each Campo In gDB.Parametros("Cursor").Valor
               Cmb_Moneda.Text = Campo("dsc_moneda")
            Next
         End If
                        
         txt_tasa_valoracion.SetFocus
      Else
         MsgBox "Error en el procedimiento" & gDB.Procedimiento & vbCrLf & gDB.Parametros("p_cod_ret").Valor & " : " & gDB.Parametros("p_men_error").Valor, vbInformation + vbOKOnly, Me.Caption
      End If
   Else
      MsgBox "Error al Ejecutar Proc. " & gDB.Procedimiento, vbCritical + vbOKOnly, Me.Caption
   End If
End Sub

Private Sub sp_formaFechaEmiBR()
   Dim fecha_emision As Date
            
   gDB.Parametros.Clear
   gDB.Procedimiento = "VALRF.Pkg_Func_Nemo.sp_FormaFechaEmiBr"
   gDB.Parametros.Add "p_mes_emi", ePT_Numero, Format(dtp_Mes_Emision.Value, "mm"), ePD_Entrada
   gDB.Parametros.Add "p_ano_emi", ePT_Numero, Format(dtp_Ano_Emision.Value, "yyyy"), ePD_Entrada
   
   gDB.Parametros.Add "p_fec_emi", ePT_Fecha, "", ePD_Salida
   gDB.Parametros.Add "p_cod_ret", ePT_Numero, "", ePD_Salida
   gDB.Parametros.Add "p_men_err", ePT_Caracter, "", ePD_Salida
   
   If gDB.EjecutaSP Then
      If gDB.Parametros("p_cod_ret").Valor = 0 Or IsNull(gDB.Parametros("p_cod_ret").Valor) Then
         fecha_emision = gDB.Parametros("p_fec_emi").Valor
         Txt_Fecha_Emision.Text = fecha_emision
                        
         If DateCmp(dtp_Fecha_Actual.Value, Txt_Fecha_Emision.Text) Then
            MsgBox "La fecha de emisi�n debe ser anterior a la fecha de calculo", vbCritical + vbOKOnly, Me.Caption
            dtp_Ano_Emision.SetFocus
         End If
      Else
         MsgBox "Error en el procedimiento" & gDB.Procedimiento & vbCrLf & gDB.Parametros("p_cod_ret").Valor & " : " & gDB.Parametros("p_men_err").Valor, vbInformation + vbOKOnly, Me.Caption
      End If
   Else
      MsgBox "Error al Ejecutar Proc. " & gDB.Procedimiento, vbCritical + vbOKOnly, Me.Caption
   End If
End Sub

Private Sub Cmb_Instrumento_GotFocus()
      Dim Campo As hFields
      
      'llena Combo instrumento
            Cmb_Instrumento.Clear
            gDB.Parametros.Clear
            gDB.Tabla = "valrf.Instrumentorf"
            gDB.OrderBy = "dsc_instrumento"
            gDB.Parametros.Add "Tipo_valorizacion", ePT_Numero, 3, ePD_Entrada
            If gDB.EjecutaSelect("") Then
                  For Each Campo In gDB.Parametros("Cursor").Valor
                        Cmb_Instrumento.AddItem Campo("dsc_instrumento") & ";" & Campo("cod_instrumento")
                  Next
            End If
End Sub

Private Sub Cmb_Instrumento_LostFocus()
      If Cmb_Nemotecnico = "" Then
            'Cmb_Instrumento.Text = Cmb_Instrumento.Text
            Cmb_Instrumento.Tag = Cmb_Instrumento.Columns(1).Text
      End If
End Sub

Private Sub Cmb_moneda_GotFocus()
      Dim Campo As hFields
      
      'llena Combo instrumento
            Cmb_Moneda.Clear
            gDB.Parametros.Clear
            gDB.Tabla = "valrf.monedas"
            gDB.OrderBy = "dsc_moneda"
            If gDB.EjecutaSelect("") Then
                  For Each Campo In gDB.Parametros("Cursor").Valor
                        Cmb_Moneda.AddItem Campo("dsc_moneda") & ";" & Campo("cod_moneda")
                        Cmb_Moneda.AutoCompletion = True
                        If Campo("id_moneda") = 1 Then 'cuando el id es 1, es pesos chilenos
                               Cmb_Moneda.SelectedItem = Cmb_Moneda.ListCount
                        
                        End If
                  Next
            End If
End Sub

Private Sub Cmb_moneda_LostFocus()
      'Cmb_moneda.Text = Cmb_moneda.Text
      Cmb_Moneda.Tag = Cmb_Moneda.Columns(1).Text
End Sub

Private Sub Cmb_Nemotecnico_LostFocus()
      Dim lId_Nemotecnico As String
      Dim Campo As hFields
      Dim ID_Emisor_Especifico As Variant
      Dim Cod_Emisor_Especifico As Variant
      Dim Cod_Instrumento As String
      Dim Dsc_Instrumento As String
      Dim Dsc_Emisor As String
      
      Dim cod_Serie As String
      Dim cod_Moneda As String
      Dim Tasa_Emision As Double
      Dim fecha_emision As Date
      Dim Cupon_Vigente As Integer
      Dim Fecha_Prox_Venc As Variant
      
      On Error GoTo errhand
      
      If Not Cmb_Nemotecnico.Text = "" Then
      
            Call Sp_Desc_Nemo_Bonosrec
      
      Else
            'Si nemotecnico es blanco
            'Cmb_Instrumento.Visible = False
            Cmb_Instrumento.SetFocus
            Cmb_Moneda.Visible = True
            Cmb_Moneda.ZOrder 0
            
      End If
Exit Sub

errhand:
      MsgBox "ERROR en la Aplicaci�n " & Me.Caption & gDB.ErrMsg
End Sub

Private Sub dtp_Ano_Emision_LostFocus()
   sp_formaFechaEmiBR
End Sub

Private Sub dtp_Mes_Emision_LostFocus()
   sp_formaFechaEmiBR
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
      Dim Resp As Integer
      
      If (KeyCode = vbKeyEscape) Then
            Resp = MsgBox("Esta seguro(a) que desea salir?", vbYesNo + vbQuestion, Me.Caption)
            If Resp = vbYes Then
                  Unload Me
            End If
      End If
       
      If (KeyCode = vbKeyReturn) Then
            SendKeys "{TAB}"
      End If
End Sub

Private Sub Form_Load()
      With Toolbar1
            Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("EXCEL").Image = cBoton_Grabar
            .Buttons("EXIT").Image = cBoton_Salir
            '.Buttons("REFRESH").Image = cBoton_Original
      End With
      
      With Toolbar2
            Set .ImageList = MDI_Principal.ImageListGlobal16
                .Buttons("CALC").Image = cBoton_Agregar_Grilla
                .Buttons("LIMP").Image = cBoton_Eliminar_Grilla
                .Appearance = ccFlat
      End With
      
      With Toolbar3
            Set .ImageList = MDI_Principal.ImageListGlobal16
                .Buttons("ADD").Image = cBoton_Aceptar
                .Buttons("QUITA").Image = cBoton_Cancelar
                .Appearance = ccFlat
      End With
      
      Call Sub_CargaForm
        
      CargaDatos
      
      Me.Top = 1
      Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
  Grilla.Rows = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar1, BarraProceso, Me)
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
      Dim FileName As String
      
      Select Case Button.Key
            Case "EXCEL"
                  Dialogo.ShowSave
                  FileName = Dialogo.FileName
                  If Not FileName = "" Then
                        If (Not FileExists(FileName)) Then
                              Grilla.SaveGrid FileName, flexFileExcel, 3
                        Else
                              If MsgBox("Archivo ya Existe. �Desea Reemplazarlo? ", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
                                    Grilla.SaveGrid FileName, flexFileExcel, 3
                              End If
                        End If
                  End If
            Case "EXIT"
                  fSalir = True
                  Unload Me
      End Select
End Sub

Private Function FileExists(FileName As String) As Boolean
      On Error GoTo ErrorHandler
      ' get the attributes and ensure that it isn't a directory
      
      FileExists = (GetAttr(FileName) And vbDirectory) = 0
      
      Exit Function

ErrorHandler:
    ' if an error occurs, this function returns False
    FileExists = False
End Function

Private Sub Toolbar2_ButtonClick(ByVal Button As MSComctlLib.Button)
      Dim Validado As Boolean
      Dim obj As Control
      
      sp_formaFechaEmiBR
      
      Validado = True
      Select Case Button
      Case "Calcular"
            If (txt_dias_ref.Text = "") Then
                  'MsgBox "Debe ingresar los dias de referncia", vbInformation + vbOKOnly, Me.Caption
                  'Validado = False
                  txt_dias_ref.Text = Buscar_Dias_Referencia
                  'txt_dias_ref.SetFocus
            End If
            
            If (IsDate(dtp_Fecha_Actual.Value) And IsDate(Txt_Fecha_Emision.Text)) Then
                  If DateCmp(CDate(dtp_Fecha_Actual.Value), CDate(Txt_Fecha_Emision.Text)) Then
                        MsgBox "Fecha Actual debe ser Posterior a fecha de Emisi�n", vbInformation + vbOKOnly, Me.Caption
                        dtp_Fecha_Actual.SetFocus
                        Validado = False
                  Else
                        txt_tasa_valoracion.SetFocus
                  End If
            Else
                  If Txt_Fecha_Emision.Text = "" Then
                        MsgBox "No se encuentra la Fecha de Emisi�n", vbInformation + vbOKOnly, Me.Caption
                        Validado = False
                  End If
                  
            End If
      
            If (Cmb_Instrumento.Text = "") Then
                  MsgBox "Debe ingresar el Instrumento ", vbInformation + vbOKOnly, Me.Caption
                  Validado = False
                  Cmb_Instrumento.SetFocus
            End If
            
            If (Cmb_Moneda.Text = "") Then
                  MsgBox "Debe ingresar la Moneda ", vbInformation + vbOKOnly, Me.Caption
                  Validado = False
                  Cmb_Moneda.SetFocus
            End If
            
            If (Not IsDate(dtp_fecha_vcto.Value)) Then
                  MsgBox "Debe ingresar la Fecha de Vencimiento", vbInformation + vbOKOnly, Me.Caption
                  Validado = False
                  dtp_fecha_vcto.SetFocus
            End If
            
            If (txt_presente_pesos.Text = "" And txt_tasa_valoracion.Text = "") Then
                  MsgBox "Debe Ingresar la tasa de valoraci�n � Valor Presente en Pesos", vbInformation + vbOKOnly, Me.Caption
                  txt_tasa_valoracion.SetFocus
                  Validado = False
            End If
            
            If (txt_valor_nominal = "") Then
                  MsgBox "Debe ingresar el valor nominal", vbInformation + vbOKOnly, Me.Caption
                  txt_valor_nominal.SetFocus
                  Validado = False
            End If
            
            If (Validado) Then
                  Calcular
            End If
            
      Case "Limpiar"
           Limpiar_Form
      End Select
End Sub

Private Sub Toolbar3_ButtonClick(ByVal Button As MSComctlLib.Button)
       Select Case Button
            Case "Agregar"
               If ValidaAgregar Then
                  Grilla.AddItem Grilla.Row
                  Randomize
                  Grilla.TextMatrix(Grilla.Rows - 1, 1) = Cmb_Nemotecnico.Text
                  Grilla.TextMatrix(Grilla.Rows - 1, 2) = Txt_Emisor.Text
                  Grilla.TextMatrix(Grilla.Rows - 1, 3) = Cmb_Instrumento.Text
                  Grilla.TextMatrix(Grilla.Rows - 1, 4) = Format(txt_valor_nominal.Text, "###,###,###.##")
                  Grilla.TextMatrix(Grilla.Rows - 1, 6) = Format(txt_tasa_valoracion.Text, "###,###,###.##")
                  Grilla.TextMatrix(Grilla.Rows - 1, 7) = Format(txt_valor_presente_pesos.Text, "###,###,###.##")
                  Calcula_Total
               Else
                  MsgBox "Falta ingresar datos.", vbCritical + vbOKOnly, Me.Caption
               End If
            
            Case "Quitar"
               Grilla.Col = 1
               If Grilla.Rows > 1 And Grilla.Row > 0 Then
                  If MsgBox("�Desea eliminar este Item? " & Grilla.Text, vbYesNo + vbQuestion, Me.Caption) = vbYes Then
                     Grilla.RemoveItem Grilla.Row
                     Calcula_Total
                  End If
               Else
                  If Grilla.Rows > 1 Then
                     If Grilla.Row = 0 Then
                        MsgBox "Debe seleccionar Item a Borrar.", vbOKOnly + vbCritical, Me.Caption
                     End If
                  Else
                     MsgBox "Debe tener Items para esta operaci�n.", vbOKOnly + vbCritical, Me.Caption
                  End If
               End If
      End Select
End Sub

Private Sub Calcula_Total()
      Dim lLinea As Long
      Dim lTotal As Double
      Dim lCol As Variant
  
      lTotal = 0
      For lLinea = 1 To (Grilla.Rows - 1)
            lCol = Grilla.ColIndex("pesos")
            If Not Grilla.TextMatrix(lLinea, lCol) = "" Then
                  lTotal = lTotal + GetCell(Grilla, lLinea, "pesos")
            End If
      Next
  
      lbl_total.Caption = Format(lTotal, "###,###,###.##")
End Sub

Private Sub txt_presente_pesos_LostFocus()
  If txt_tasa_valoracion.Text = "" Then
    MsgBox "Debe Ingresar el valor presente en pesos o la tasa de valoraci�n", vbInformation + vbOKOnly, Me.Caption
  End If
End Sub

Private Sub txt_valor_nominal_LostFocus()
      If txt_valor_nominal.Text = "" Then
            MsgBox "Debe Ingresar el valor nominal", vbInformation + vbOKOnly, Me.Caption
      End If
End Sub

Private Sub txt_valor_presente_pesos_Change()
      txt_presente_pesos.Text = Format(txt_valor_presente_pesos.Text, "###,###,###.##")
End Sub
