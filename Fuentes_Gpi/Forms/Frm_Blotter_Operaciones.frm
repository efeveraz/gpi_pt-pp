VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Blotter_Operaciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Blother de Operaciones"
   ClientHeight    =   9045
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14880
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9045
   ScaleWidth      =   14880
   Begin VB.Frame Frm_Fecha_Re_Proceso 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   795
      Left            =   -15
      TabIndex        =   2
      Top             =   390
      Width           =   14895
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   13590
         Picture         =   "Frm_Blotter_Operaciones.frx":0000
         TabIndex        =   12
         Top             =   330
         Width           =   375
      End
      Begin VB.CheckBox chk_TodasCtas 
         Caption         =   "Todas"
         Height          =   195
         Left            =   14040
         TabIndex        =   11
         ToolTipText     =   "Todas las Cuentas"
         Top             =   405
         Width           =   765
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Consulta 
         Height          =   315
         Left            =   1350
         TabIndex        =   3
         Top             =   320
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   66977793
         CurrentDate     =   37732
      End
      Begin TrueDBList80.TDBCombo Cmb_Asesor 
         Height          =   345
         Left            =   7560
         TabIndex        =   4
         Tag             =   "SOLOLECTURA=N"
         Top             =   315
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Blotter_Operaciones.frx":030A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Instrumento 
         Height          =   345
         Left            =   4080
         TabIndex        =   9
         Tag             =   "SOLOLECTURA=N"
         Top             =   320
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Blotter_Operaciones.frx":03B4
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   10440
         TabIndex        =   13
         Top             =   330
         Width           =   3120
         _ExtentX        =   5503
         _ExtentY        =   609
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "N� de Cuenta"
         Text            =   ""
         MaxLength       =   100
      End
      Begin VB.Label lblInstrumento 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Instrumento"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3000
         TabIndex        =   10
         Top             =   320
         Width           =   1065
      End
      Begin VB.Label Lbl_Fecha_Consulta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Consulta"
         Height          =   345
         Left            =   90
         TabIndex        =   6
         Top             =   320
         Width           =   1215
      End
      Begin VB.Label lblAsesor 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Asesor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   6480
         TabIndex        =   5
         Top             =   315
         Width           =   1065
      End
   End
   Begin VB.Frame Frm_Cuentas 
      Caption         =   "Flujos de Caja"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7725
      Left            =   -15
      TabIndex        =   0
      Top             =   1200
      Width           =   14895
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
         Height          =   7275
         Left            =   30
         TabIndex        =   1
         Top             =   240
         Width           =   14745
         _cx             =   26009
         _cy             =   12832
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         SelectionMode   =   1
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   13
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Blotter_Operaciones.frx":045E
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   0
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   14880
      _ExtentX        =   26247
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "SEARCH"
            Description     =   "Busca Cuentas"
            Object.ToolTipText     =   "Carga en Grilla las Cuentas del Nemot�cnico"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORT"
            Object.ToolTipText     =   "Genera reporte de Rentabilidad por Perfil"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   8
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Blotter_Operaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Rem ---------------------------------------------------------------------------
Rem 03/04/2009. Controla fecha de consulta si es el permiso es sololectura
Rem             se considera la fecha de cierre virtual como fecha de consulta
Rem 29/07/2009. MMardones. Agrega Tipo Inversi�n P(Pesos) o N(Nominales)
Rem             MMardones. Corrige salida PDF y Excel. No consideraba filtro
Rem             por Instrumento y el PDF sale cortado
Rem 30/07/2009. MMardones. Se agrega el llenado del combo de cuentas con filtro
Rem             asesor.
Rem ---------------------------------------------------------------------------

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiqso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana
'----------------------------------------------------------------------------
Public fApp_Excel           As Excel.Application      ' Excel application
Public fLibro               As Excel.Workbook      ' Excel workbook
Dim iFilaExcel    As Integer
Dim sFormato As String
Dim iTotalColExcel As Integer
Dim lFechaCierreVirtual As Date

Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
'  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  fFlg_Tipo_Permiso = Fnt_Obtiene_Permiso_Formulario(pCod_Arbol_Sistema)
 
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Form_Load()
   Me.Top = 1
   Me.Left = 1
   With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SEARCH").Image = cBoton_Buscar
        .Buttons("REPORT").Image = cBoton_Modificar
        .Buttons("REFRESH").Image = cBoton_Original
        .Buttons("EXIT").Image = cBoton_Salir
   End With

   Call Sub_CargaForm
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEARCH"
        If ValidaEntradaDatos Then
            Call Sub_CargarDatos
        End If
    Case "REFRESH"
      Call Sub_Limpiar
    Case "REPORT"
      Call Sub_Crea_Informe
    Case "EXIT"
      Unload Me
  End Select

End Sub
Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Crea_Informe
    Case "EXCEL"
      Call Sub_Crea_Excel
  End Select
End Sub
Private Function ValidaEntradaDatos() As Boolean
Dim bOk As Boolean

    bOk = True
    
    ValidaEntradaDatos = bOk
End Function
Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_FormControl_Color(Me.Controls)
    
    Call Sub_CargaCombo_Asesor(Cmb_Asesor, pTodos:=True)
    Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
    
    Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento, pTodos:=True)
    Call Sub_ComboSelectedItem(Cmb_Instrumento, cCmbKALL)
    
    Grilla_Cuentas.GridLines = flexGridNone
    Grilla_Cuentas.Rows = 1
    
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Consulta.Value = lCierre.Busca_Ultima_FechaCierre
    Set lCierre = Nothing
    
    sFormato = "#,##0.0000"
    
    chk_TodasCtas.Value = 0
    Txt_Num_Cuenta.Text = ""
    Txt_Num_Cuenta.Tag = ""
    Txt_Num_Cuenta.Enabled = True
    cmb_buscar.Enabled = True

    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Form_Resize()
   Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Sub_Limpiar()
Dim lCierre As Class_Verificaciones_Cierre
Dim i As Integer

    Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
    Call Sub_ComboSelectedItem(Cmb_Instrumento, cCmbKALL)
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Consulta.Value = lCierre.Busca_Ultima_FechaCierre
    Grilla_Cuentas.Rows = 1
    
    chk_TodasCtas.Value = 0
    Txt_Num_Cuenta.Text = ""
    Txt_Num_Cuenta.Tag = ""
    Txt_Num_Cuenta.Enabled = True
    cmb_buscar.Enabled = True
    
    
End Sub
Private Sub Sub_CargarDatos()
Dim lCursor                 As Object
Dim lReg                    As Object
Dim lLinea                  As Integer
Dim dTotalValor             As Double
Dim dTotalCantidad          As Double
Dim iDecimales              As Integer
Dim sFechaDesde             As String
Dim sFechaHasta             As String
Dim lId_Cuenta              As Integer
Dim lId_Asesor              As Integer
Dim lMoneda_Cuenta          As Integer
Dim sTituloT0               As String
Dim sTituloT1               As String
Dim sTituloT2               As String
Dim lDecimalesCuenta        As Integer
Dim dTotalT0                As Double
Dim dTotalT1                As Double
Dim dTotalT2                As Double
'---------------------------------------------
Dim lCursor_Blotter       As hRecord
Dim lCajas_Ctas             As Class_Cajas_Cuenta
Dim lReg_Caj                As hFields
Dim lCursor_Caj             As hRecord
Dim dSaldoCaja              As Double
Dim lCaja                   As Integer
Dim lCol                    As Integer
Dim sNombre_Col             As String
Dim sSimbolo                As String
Dim nDecimalesCaja          As Integer
Dim nombre_col              As String
Dim lOtrasCajasActivas      As Integer
Dim i As Integer
Dim sCod_Producto           As String
'---------------------------------------------
    Call Sub_Bloquea_Puntero(Me)
    Grilla_Cuentas.Rows = 1
    
    lId_Cuenta = IIf(Trim(Txt_Num_Cuenta.Tag) = "" Or chk_TodasCtas.Value = 1, 0, Txt_Num_Cuenta.Tag)
    lId_Asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesor) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Asesor))
    sCod_Producto = IIf(Fnt_ComboSelected_KEY(Cmb_Instrumento) = cCmbKALL, "", Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text))
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_Operaciones$ConsultaBlotter"
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pFecha_Desde", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
    gDB.Parametros.Add "pFecha_Hasta", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
    gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    If lId_Cuenta <> 0 Then
        gDB.Parametros.Add "pId_Cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    End If
    If lId_Asesor <> 0 Then
        gDB.Parametros.Add "pId_asesor", ePT_Numero, lId_Asesor, ePD_Entrada
    End If
    If sCod_Producto <> Cmb_Instrumento.Text Then
        gDB.Parametros.Add "pCod_Instrumento", ePT_Caracter, sCod_Producto, ePD_Entrada
    End If
    If Not gDB.EjecutaSP Then
        MsgBox "Error " & gDB.ErrMsg
        Exit Sub
    End If
    Set lCursor_Blotter = gDB.Parametros("Pcursor").Valor
    If lCursor_Blotter.Count > 0 Then
        For Each lReg In lCursor_Blotter
            lDecimalesCuenta = TraeDecimalesCuenta(lReg("id_moneda").Value)
            lLinea = Grilla_Cuentas.Rows
            Call Grilla_Cuentas.AddItem("")
            Call SetCell(Grilla_Cuentas, lLinea, "colum_pk", Trim(lReg("id_cuenta").Value), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "dsc_cuenta", lReg("num_cuenta").Value, pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "id_cliente", lReg("id_cliente").Value, pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "Rut_cliente", lReg("rut_cliente").Value, pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "nombre_cliente", lReg("nombre_cliente").Value, pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "nombre_asesor", NVL(lReg("nombre_asesor").Value, ""), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "Dsc_Movimiento", NVL(lReg("dsc_movimiento_operacion").Value, ""), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "Nemotecnico", NVL(lReg("Nemotecnico").Value, ""), pAutoSize:=False)
            'Agregado 28/07/2009 MMardones.
            Call SetCell(Grilla_Cuentas, lLinea, "tipo_inversion", NVL(lReg("tipo_inversion").Value, "N"), pAutoSize:=False)
            '------------------------------
            Call SetCell(Grilla_Cuentas, lLinea, "Cantidad", FormatNumber(NVL(lReg("Cantidad").Value, 0), 4), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "Precio_Tasa", FormatNumber(NVL(lReg("Precio_Tasa").Value, 0), 4), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "Monto", FormatNumber(NVL(lReg("Monto").Value, 0), lDecimalesCuenta), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "orden", NVL(lReg("Orden_Magic").Value, 0), pAutoSize:=False)
        Next
    Else
        MsgBox "No se registran movimientos en esta fecha.", vbInformation, Me.Caption
    End If
ExitProcedure:
    Set lCursor_Caj = Nothing
    Set lCursor_Blotter = Nothing
    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Sub_Crea_Excel()
Dim nHojas, i       As Integer
Dim hoja            As Integer
Dim Index           As Integer
    
    Set fApp_Excel = CreateObject("Excel.application")
    fApp_Excel.DisplayAlerts = False
    Set fLibro = fApp_Excel.Workbooks.Add
    
    fApp_Excel.ActiveWindow.DisplayGridlines = False
  
    ' App_Excel.Visible = True
    Call Sub_Bloquea_Puntero(Me)
    
    fLibro.Worksheets.Add
   
    With fLibro
        For i = .Worksheets.Count To 2 Step -1
            .Worksheets(i).Delete
        Next i
        .Sheets(1).Select
        .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).Select
            
        .ActiveSheet.Range("A5").Value = "Blotter de Operaciones"
        
        .ActiveSheet.Range("A5:E5").Font.Bold = True
        .ActiveSheet.Range("A5:E5").HorizontalAlignment = xlLeft
        '.ActiveSheet.Range("A5:E5").Merge
        
        .ActiveSheet.Range("A8").Value = "Fecha Consulta   " & DTP_Fecha_Consulta.Value
        
        .ActiveSheet.Range("A8:B8").HorizontalAlignment = xlLeft
        .ActiveSheet.Range("A8:B8").Font.Bold = True

        .Worksheets.Item(1).Columns("A:A").ColumnWidth = 15
        .Worksheets.Item(1).Columns("B:B").ColumnWidth = 15
        .Worksheets.Item(1).Columns("C:C").ColumnWidth = 50
        .Worksheets.Item(1).Columns("D:D").ColumnWidth = 40
        .Worksheets.Item(1).Columns("E:E").ColumnWidth = 20
        .Worksheets.Item(1).Columns("F:F").ColumnWidth = 30
        .Worksheets.Item(1).Columns("G:G").ColumnWidth = 20
        .Worksheets.Item(1).Columns("H:H").ColumnWidth = 20
        .Worksheets.Item(1).Columns("I:I").ColumnWidth = 20
        .Worksheets.Item(1).Columns("J:J").ColumnWidth = 10
        .Worksheets.Item(1).Columns("K:K").ColumnWidth = 20
        .Worksheets.Item(1).Columns("L:L").ColumnWidth = 20
        .Worksheets.Item(1).Columns("M:M").ColumnWidth = 20
        .Worksheets.Item(1).Columns("N:N").ColumnWidth = 20
        .Worksheets.Item(1).Name = "Blother"
    End With
    
    iFilaExcel = 10
    
    Generar_Listado_Excel
    
    fApp_Excel.Visible = True
    fApp_Excel.UserControl = True
    
    
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub


Private Sub Generar_Listado_Excel()
Dim lReg                 As hCollection.hFields

Dim lxHoja               As Worksheet
'----------------------------------------------------
Dim bImprimioTitulo      As Boolean
Dim lFilaGrilla          As Integer
Dim iFilaInicio          As Integer
Dim iColGrilla           As Integer
Dim iColExcel            As Integer
Dim lIdCuenta            As String
Dim dTotalCantidad       As Double
Dim dTotalValor          As Double
Dim sNombre_Col          As String
Dim iTotalColGrilla      As Integer
Dim sValor               As String
Dim nDecimales           As Integer
Dim sFormatoValorMercado As String
Dim dblTotal             As Double
Dim sFechaDesde          As String
Dim sFechaHasta          As String
Dim lCursor_Blotter    As hRecord
Dim lId_Cuenta           As Integer
Dim lId_Asesor           As Integer
Dim lMoneda_Cuenta       As Integer
Dim iColFinal            As Integer
Dim sFormatoCaja         As String
Dim i                    As Integer
Dim sTituloT0            As String
Dim sTituloT1            As String
Dim sTituloT2            As String
Dim lDecimalesCuenta     As Integer
Dim dTotalT0             As Double
Dim dTotalT1             As Double
Dim dTotalT2             As Double
'---------------------------------------------
Dim lCajas_Ctas          As Class_Cajas_Cuenta
Dim lReg_Caj             As hFields
Dim lCursor_Caj          As hRecord
Dim dSaldoCaja           As Double
Dim lCaja                As Integer
Dim lCol                 As Integer
Dim sSimbolo             As String
Dim nDecimalesCaja       As Integer
Dim nombre_col           As String
Dim lOtrasCajasActivas   As Integer
Dim sCod_Producto           As String
'---------------------------------------------
    'On Error GoTo ErrProcedure
    bImprimioTitulo = False
    iColExcel = 1
    iTotalColGrilla = Grilla_Cuentas.Cols
    Set lxHoja = fLibro.ActiveSheet
    If Not bImprimioTitulo Then
        Call ImprimeEncabezado
        bImprimioTitulo = True
        iFilaInicio = iFilaExcel + 1
    End If
    lId_Cuenta = IIf(Trim(Txt_Num_Cuenta.Tag) = "" Or chk_TodasCtas.Value = 1, 0, Txt_Num_Cuenta.Tag)
    lId_Asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesor) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Asesor))
    '29/07/2009. Agregado por MMardones
    sCod_Producto = IIf(Fnt_ComboSelected_KEY(Cmb_Instrumento) = cCmbKALL, "", Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text))
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_Operaciones$ConsultaBlotter"
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pFecha_Desde", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
    gDB.Parametros.Add "pFecha_Hasta", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
    gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    If lId_Cuenta <> 0 Then
        gDB.Parametros.Add "pId_Cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    End If
    If lId_Asesor <> 0 Then
        gDB.Parametros.Add "pId_asesor", ePT_Numero, lId_Asesor, ePD_Entrada
    End If
    '29/07/2009. Agregado por MMardones
    If sCod_Producto <> Cmb_Instrumento.Text Then
        gDB.Parametros.Add "pCod_Instrumento", ePT_Caracter, sCod_Producto, ePD_Entrada
    End If
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If
    
    lOtrasCajasActivas = 0
    
    iFilaInicio = iFilaExcel
    Set lCursor_Blotter = gDB.Parametros("Pcursor").Valor
    If lCursor_Blotter.Count = 0 Then
        MsgBox "No se registran movimientos en esta fecha.", vbInformation, Me.Caption
    Else
        With lxHoja

            For Each lReg In lCursor_Blotter
                lDecimalesCuenta = TraeDecimalesCuenta(lReg("id_moneda").Value)
                iColExcel = 1
                Set lCajas_Ctas = New Class_Cajas_Cuenta
                With lCajas_Ctas
                    .Campo("id_cuenta").Valor = lReg("id_cuenta").Value
                    If .Buscar(False) Then
                        Set lCursor_Caj = .Cursor
                    Else
                        MsgBox .ErrMsg, vbCritical, Me.Caption
                        Exit Sub
                    End If
                End With
                lMoneda_Cuenta = lReg("id_moneda").Value
        
                iFilaExcel = iFilaExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("num_cuenta").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("rut_cliente").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("nombre_cliente").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("nombre_asesor").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("dsc_movimiento_operacion").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("nemotecnico").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = NVL(lReg("tipo_inversion").Value, "N") '29/07/2009 Agregado por MMardones
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = NVL(lReg("cantidad").Value, 0)
                .Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0.0000" '"#,##0.###0"
        
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = NVL(lReg("precio_tasa").Value, 0)
                .Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0.0000"
        
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = NVL(lReg("monto").Value, 0)
                .Cells(iFilaExcel, iColExcel).NumberFormat = Fnt_Formato_Moneda(lReg("id_moneda").Value)
              
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = NVL(lReg("orden_magic").Value, 0)
            Next
            
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaExcel, 2)).HorizontalAlignment = xlRight
            .Range(.Cells(iFilaInicio, 3), .Cells(iFilaExcel, 6)).HorizontalAlignment = xlLeft
            .Range(.Cells(iFilaInicio, 7), .Cells(iFilaExcel, 7)).HorizontalAlignment = xlCenter
            .Range(.Cells(iFilaInicio, 8), .Cells(iFilaExcel, 11)).HorizontalAlignment = xlRight
            
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).BorderAround
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).Font.Bold = True
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).WrapText = True
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).HorizontalAlignment = xlCenter

        End With
    End If
ExitProcedure:

End Sub
Private Sub ImprimeEncabezado()
Dim lxHoja          As Worksheet
Dim iColExcel       As Integer
Dim i               As Integer

    Set lxHoja = fLibro.ActiveSheet
    iFilaExcel = iFilaExcel + 1
    iColExcel = 1
    With lxHoja
        .Cells(iFilaExcel, iColExcel).Value = "Cuenta"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Rut"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Cliente"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Asesor"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Movimiento"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Nemotecnico"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "P/N"             '29/07/2009 Agregado por MMardones
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Cantidad"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Precio/Tasa"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Monto"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Orden Magic"
    End With
End Sub

Private Sub Sub_Crea_Informe()
'------------------------------------------
Const clrHeader = &HD0D0D0
'Const sHeader_Clt = "Cuenta|Rut|Nombre Cliente|Disponible T+0|Retenci�n T+1|Retenci�n T+2"
'Const sFormat_Clt = "1000|>1300|3000|>1100|>1100|>1100"
Dim sHeader_Clt             As String
Dim sFormat_Clt             As String
'------------------------------------------
Dim sRecord
Dim sRecordCaja
Dim bAppend
Dim lLinea                  As Integer
'------------------------------------------
Dim lForm                   As Frm_Reporte_Generico
Dim sTitulo                 As String
Dim sFechaDesde             As String
Dim sFechaHasta             As String
Dim lId_Cuenta              As Integer
Dim lId_Asesor              As Integer
Dim lMoneda_Cuenta          As Integer
Dim sNombre_Col             As String
Dim sTituloT0               As String
Dim sTituloT1               As String
Dim sTituloT2               As String
Dim lDecimalesCuenta        As Integer
Dim dTotalT0                As Double
Dim dTotalT1                As Double
Dim dTotalT2                As Double
'---------------------------------------------
Dim lReg                    As hCollection.hFields
Dim lCursor_Blotter       As hRecord
Dim lCajas_Ctas             As Class_Cajas_Cuenta
Dim lReg_Caj                As hFields
Dim lCursor_Caj             As hRecord
Dim dSaldoCaja              As Double
Dim lCaja                   As Integer
Dim lCol                    As Integer
Dim sSimbolo                As String
Dim nDecimalesCaja          As Integer
Dim nombre_col              As String
Dim lOtrasCajasActivas      As Integer
Dim i                       As Integer
Dim sCod_Producto           As String
'---------------------------------------------

    Call Sub_Bloquea_Puntero(Me)
        
    sTitulo = "Blotter Operaciones al " & DTP_Fecha_Consulta.Value
    If Not Fnt_Form_Validar(Me.Controls) Then
        Exit Sub
    End If
    
    lId_Cuenta = IIf(Trim(Txt_Num_Cuenta.Tag) = "" Or chk_TodasCtas.Value = 1, 0, Txt_Num_Cuenta.Tag)
    lId_Asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesor) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Asesor))
    '29/07/2009 Agregado por MMardones
    sCod_Producto = IIf(Fnt_ComboSelected_KEY(Cmb_Instrumento) = cCmbKALL, "", Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text))
       
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_Operaciones$ConsultaBlotter"
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pFecha_Desde", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
    gDB.Parametros.Add "pFecha_Hasta", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
    gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    If lId_Cuenta <> 0 Then
        gDB.Parametros.Add "pId_Cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    End If
    If lId_Asesor <> 0 Then
        gDB.Parametros.Add "pId_asesor", ePT_Numero, lId_Asesor, ePD_Entrada
    End If
    '29/07/2009 Agregado por MMardones
    If sCod_Producto <> Cmb_Instrumento.Text Then
        gDB.Parametros.Add "pCod_Instrumento", ePT_Caracter, sCod_Producto, ePD_Entrada
    End If
    
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor_Blotter = gDB.Parametros("Pcursor").Valor
    If lCursor_Blotter.Count = 0 Then
        MsgBox "No se registran movimientos en esta fecha.", vbInformation, Me.Caption
    Else
        sHeader_Clt = "Cuenta|Rut|Nombre Cliente|Asesor|Movimiento|Nemotecnico|P/N|Cantidad|Precio/Tasa|Monto|Ord.Magic"
        sFormat_Clt = "<600|>900|<3200|<1900|<1000|<1700|<400|>1300|>1300|>1300|>1000"
        Set lForm = New Frm_Reporte_Generico
        lForm.VsPrinter.MarginLeft = 1200
        Call lForm.Sub_InicarDocumento(pTitulo:=sTitulo _
                                       , pTipoSalida:=ePrinter.eP_Pantalla _
                                       , pOrientacion:=orLandscape)
        With lForm.VsPrinter
           ' .MarginLeft = 1200
            .FontSize = 8
            .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
            .Paragraph = "" 'salto de linea
            .FontBold = False
            .StartTable
            .FontSize = 7
            .TableCell(tcAlignCurrency) = False
            For Each lReg In lCursor_Blotter
                lDecimalesCuenta = TraeDecimalesCuenta(lReg("id_moneda").Value)
                Set lCajas_Ctas = New Class_Cajas_Cuenta
                With lCajas_Ctas
                    .Campo("id_cuenta").Valor = lReg("id_cuenta").Value
                    If .Buscar(False) Then
                        Set lCursor_Caj = .Cursor
                    Else
                        MsgBox .ErrMsg, vbCritical, Me.Caption
                        Exit Sub
                    End If
                End With
                lMoneda_Cuenta = lReg("id_moneda").Value
                Rem Genera el reporte
                sRecord = NVL(lReg("num_cuenta").Value, "") & "|" & _
                          NVL(lReg("rut_cliente").Value, "") & "|" & _
                          NVL(lReg("nombre_cliente").Value, "") & "|" & _
                          NVL(lReg("nombre_asesor").Value, "") & "|" & _
                          NVL(lReg("dsc_movimiento_operacion").Value, "") & "|" & _
                          NVL(lReg("Nemotecnico").Value, "") & "|" & _
                          NVL(lReg("tipo_inversion").Value, "N") & "|" & _
                          FormatNumber(NVL(lReg("Cantidad").Value, 0), 4) & "|" & _
                          FormatNumber(NVL(lReg("Precio_Tasa").Value, 0), 4) & "|" & _
                          FormatNumber(NVL(lReg("monto").Value, 0), lDecimalesCuenta) & "|" & _
                          NVL(lReg("Orden_Magic").Value, 0)
                .AddTable sFormat_Clt, sHeader_Clt, sRecord, clrHeader, , bAppend
            Next
            .Paragraph = ""
            .TableCell(tcFontBold, 0) = True
            .EndTable
            .EndDoc
        End With
        
    End If
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function TraeDecimalesCuenta(lId_Moneda) As Integer
Dim lDecimalesCuenta    As Integer
Dim lcMoneda            As Object
    lDecimalesCuenta = 0
    Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
    With lcMoneda
      .Campo("id_Moneda").Valor = lId_Moneda
      If .Buscar Then
        lDecimalesCuenta = .Cursor(1)("dicimales_mostrar").Value
      End If
    End With
    Set lcMoneda = Nothing
    TraeDecimalesCuenta = lDecimalesCuenta
End Function

Private Function Fnt_Obtiene_FechaCierre_Virtual(ByVal pId_Cuenta As String, ByRef pFecha As Date) As Boolean
Dim lcCuentas As Object
Dim lresult As Boolean

    lresult = True
    Set lcCuentas = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuentas
        .Campo("id_cuenta").Valor = pId_Cuenta
        If .Buscar Then
            If NVL(.Cursor(1)("fecha_cierre_virtual").Value, "") = "" Then
               lresult = False
            Else
                pFecha = .Cursor(1)("fecha_cierre_virtual").Value
            End If
        End If
    End With
    Fnt_Obtiene_FechaCierre_Virtual = lresult
End Function

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call Lpr_Buscar_Cuenta("TXT")
    End If
End Sub

Private Sub cmb_buscar_Click()
Dim lId_Cuenta As Integer

    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)
    If lId_Cuenta <> 0 Then
        Txt_Num_Cuenta.Text = ""
        Txt_Num_Cuenta.Tag = lId_Cuenta
        Call Lpr_Buscar_Cuenta("", CStr(lId_Cuenta))
    End If
End Sub

Private Sub Lpr_Buscar_Cuenta(ByVal Origen As String, Optional p_Id_Cuenta As String)
Dim lcCuenta      As Object
Dim lFechaCierreVirtual As Date
Dim lFechaCierre        As Date

    If Origen = "TXT" Then
        If InStr(1, Txt_Num_Cuenta.Text, "-") > 0 Then
           Txt_Num_Cuenta.Text = Trim(Left(Txt_Num_Cuenta.Text, InStr(1, Txt_Num_Cuenta.Text, "-") - 1))
        End If
        p_Id_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text), 0)
    End If
    If p_Id_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = p_Id_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                    If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
                        lFechaCierre = DTP_Fecha_Consulta.Value
                        If Fnt_Obtiene_FechaCierre_Virtual(p_Id_Cuenta, lFechaCierreVirtual) Then
                            If DTP_Fecha_Consulta.Value > lFechaCierreVirtual Then
                                DTP_Fecha_Consulta.Value = lFechaCierreVirtual
                            End If
                        Else
                            DTP_Fecha_Consulta.Value = lFechaCierre
                        End If
                    End If
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
            Set lcCuenta = Nothing
    End If
End Sub

Private Sub chk_TodasCtas_Click()

If chk_TodasCtas.Value = 1 Then
    Txt_Num_Cuenta.Text = ""
    Txt_Num_Cuenta.Tag = ""
    Txt_Num_Cuenta.Enabled = False
    cmb_buscar.Enabled = False
Else
    Txt_Num_Cuenta.Enabled = True
    cmb_buscar.Enabled = True
End If

End Sub





