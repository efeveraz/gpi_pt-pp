VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Man_Banco 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Bancos"
   ClientHeight    =   4200
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   8670
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   8670
   Begin VB.Frame Frame1 
      Caption         =   "Listado de Bancos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   75
      TabIndex        =   0
      Top             =   420
      Width           =   8535
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   120
         TabIndex        =   1
         Top             =   300
         Width           =   8355
         _cx             =   14737
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   6
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Banco.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   8670
      _ExtentX        =   15293
      _ExtentY        =   635
      ButtonWidth     =   2037
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Eliminar"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Elimina un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un elemento"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   7800
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Banco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With

  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
Dim lkey As String

  With Grilla
    If .Row > 0 Then
      lkey = GetCell(Grilla, .Row, "id_banco")
      Call Sub_EsperaVentana(lkey)
    End If
  End With
  
End Sub

Private Sub Sub_EsperaVentana(ByVal pkey)
Dim lForm As Frm_Banco
Dim lnombre As String
Dim lNom_Form As String
  
  Me.Enabled = False
  
  lNom_Form = "Frm_Banco"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lnombre = Me.Name
    
    Set lForm = New Frm_Banco
    Call lForm.Fnt_Modificar(pkey, pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lnombre) Then
      Call Sub_CargarDatos
    Else
      Exit Sub
    End If
  End If
  
  Me.Enabled = True
  
End Sub


Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agregar
    Case "UPDATE"
      Call Grilla_DblClick
    Case "REFRESH"
      Grilla.Rows = 1
      Call Sub_CargarDatos
    Case "DEL"
      Call Sub_Eliminar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_CargarDatos()
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lcBanco As Class_Bancos
  

  Call Sub_FormControl_Color(Me.Controls)

  Call Sub_Bloquea_Puntero(Me)
  
  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "id_banco")
  Else
    lID = ""
  End If
  
  Grilla.Rows = 1
  
  Set lcBanco = New Class_Bancos
  With lcBanco
    If .Buscar(True) Then
      For Each lReg In .Cursor
        lLinea = Grilla.Rows
        Call Grilla.AddItem("")
        Call SetCell(Grilla, lLinea, "id_banco", NVL(lReg("id_banco").Value, ""))
        Call SetCell(Grilla, lLinea, "dsc_pais", NVL(lReg("dsc_pais").Value, ""))
        Call SetCell(Grilla, lLinea, "desc_mercado", NVL(lReg("desc_mercado").Value, ""))
        Call SetCell(Grilla, lLinea, "dsc_banco", NVL(lReg("dsc_banco").Value, ""))
      Next
    End If
  End With
  Set lcBanco = Nothing
  
  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("id_banco"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("id_banco"))
    End If
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Agregar()
  Call Sub_EsperaVentana(cNewEntidad)
End Sub

Private Sub Sub_Eliminar()
Dim lId_banco As String
Dim lcBanco As Class_Bancos

  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      lId_banco = GetCell(Grilla, Grilla.Row, "id_banco")
      If Not lId_banco = "" Then
        Set lcBanco = New Class_Bancos
        With lcBanco
          .Campo("id_banco").Valor = lId_banco
          If .Borrar Then
            MsgBox "Banco eliminado correctamente.", vbInformation + vbOKOnly, Me.Caption
          Else
            If .Errnum = 440 Then ' Si el Banco tiene hijos asociados
              MsgBox "El Banco no puede ser eliminado, ya que est� asociado a otros componentes.", vbCritical, Me.Caption
            Else
              MsgBox .ErrMsg, vbCritical, Me.Caption
            End If
          End If
        End With
        Set lcBanco = Nothing
      End If
      Call Sub_CargarDatos
    End If
  End If
  
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Listado de Bancos" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VSPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VSPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub



Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

