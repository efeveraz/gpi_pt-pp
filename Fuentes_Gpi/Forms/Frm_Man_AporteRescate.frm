VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Man_AporteRescate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Aportes y Rescates"
   ClientHeight    =   7785
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   11385
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7785
   ScaleWidth      =   11385
   Begin VB.Frame Frame1 
      Caption         =   "Aportes/Rescates"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7365
      Left            =   30
      TabIndex        =   0
      Top             =   390
      Width           =   11295
      Begin VB.Frame Frame2 
         Caption         =   " Filtro "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   795
         Left            =   120
         TabIndex        =   6
         Top             =   630
         Width           =   7515
         Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
            Height          =   345
            Left            =   4230
            TabIndex        =   7
            Top             =   270
            Width           =   1275
            _ExtentX        =   2249
            _ExtentY        =   609
            _Version        =   393216
            Format          =   71565313
            CurrentDate     =   38938
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Ini 
            Height          =   345
            Left            =   1500
            TabIndex        =   8
            Top             =   270
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   609
            _Version        =   393216
            Format          =   71565313
            CurrentDate     =   38938
         End
         Begin MSComctlLib.Toolbar Toolbar_Proceso 
            Height          =   330
            Left            =   5640
            TabIndex        =   11
            Top             =   270
            Width           =   1020
            _ExtentX        =   1799
            _ExtentY        =   582
            ButtonWidth     =   1561
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Buscar"
                  Key             =   "BUSC"
                  Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               EndProperty
            EndProperty
         End
         Begin VB.Label Lbl_Fecha_Ter 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha T�rmino"
            Height          =   345
            Index           =   0
            Left            =   3030
            TabIndex        =   10
            Top             =   270
            Width           =   1155
         End
         Begin VB.Label Lbl_Fecha_Ini 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Inicio"
            Height          =   345
            Index           =   1
            Left            =   90
            TabIndex        =   9
            Top             =   270
            Width           =   1395
         End
      End
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3450
         Picture         =   "Frm_Man_AporteRescate.frx":0000
         TabIndex        =   4
         Top             =   300
         Width           =   375
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   5805
         Left            =   120
         TabIndex        =   1
         Top             =   1470
         Width           =   11055
         _cx             =   19500
         _cy             =   10239
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   12
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_AporteRescate.frx":030A
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin hControl2.hTextLabel Txt_Nombres 
         Height          =   315
         Left            =   3840
         TabIndex        =   5
         Top             =   300
         Width           =   7245
         _ExtentX        =   12779
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Nombre"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   240
         TabIndex        =   12
         Top             =   300
         Width           =   3180
         _ExtentX        =   5609
         _ExtentY        =   609
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "N� de Cuenta"
         Text            =   ""
         MaxLength       =   12
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   11385
      _ExtentX        =   20082
      _ExtentY        =   635
      ButtonWidth     =   2037
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   12
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Aporte"
            Key             =   "ADD_APO"
            Description     =   "Agrega un aporte"
            Object.ToolTipText     =   "Agrega un Aporte"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Rescate"
            Key             =   "ADD_RES"
            Description     =   "Agrega un rescate"
            Object.ToolTipText     =   "Agrega un Rescate"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Anular"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Anula un Aporte/Rescate"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un Aporte/Rescate"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Excel"
            Key             =   "XLS"
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Object.ToolTipText     =   "Imprimir"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9240
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_AporteRescate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Rem Flag para Fechas anteriores
Dim fFlg_Fechas_Anteriores As Boolean

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String

Dim fRutCliente As String

Public Sub Mostrar(pCod_Arbol_Sistema, Optional pFlg_Fechas_Anteriores As Boolean = False)
  
  fFlg_Fechas_Anteriores = pFlg_Fechas_Anteriores
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
  
End Sub

Private Sub Sub_CargarDatos()
Dim lReg    As hFields
Dim lLinea  As Long
Dim lCuenta As String
Dim lID     As String

'-----------------------
'Dim lAporte_Rescate_Cuenta As Class_APORTE_RESCATE_CUENTA
   
  Call Sub_Bloquea_Puntero(Me)
   
  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Grilla.Rows = 1
  lCuenta = Txt_Num_Cuenta.Tag
  
  If Not lCuenta = "" Then
    BuscarNombreCuenta lCuenta
    Call Sub_BuscarDatosGrilla
  End If
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub BuscarNombreCuenta(lIdCuenta As String)
'    Dim lcCuenta As Class_Cuentas
    Dim lcCuenta As Object
    
'    Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    
    With lcCuenta
        .Campo("id_cuenta").Valor = lIdCuenta
        .Campo("id_Empresa").Valor = Fnt_EmpresaActual
        
        If .Buscar_Vigentes Then
          If .Cursor.Count > 0 Then
              Txt_Nombres.Text = .Cursor(1)("nombre_cliente").Value
              fRutCliente = Txt_Nombres.Text = .Cursor(1)("rut_cliente").Value
          Else
            Txt_Nombres.Text = ""
          End If
        Else
            Txt_Nombres.Text = ""
            
            Call Fnt_MsgError(.SubTipo_LOG _
                              , "Problemas con la busqueda de los datos de la cuenta (" & lIdCuenta & ")." _
                              , .ErrMsg _
                              , pConLog:=True)
        End If
        
    End With

    Set lcCuenta = Nothing
End Sub


Private Sub Cmb_Cuentas_ItemChange()
  Call Sub_CargarDatos
End Sub

Private Sub Cmb_Cuentas_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    Call Sub_CargarDatos
  End If
End Sub

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD_APO").Image = cBoton_Agregar_Grilla
      .Buttons("ADD_RES").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
      .Buttons("XLS").Image = cBoton_Excel
  End With
  With Toolbar_Proceso
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("BUSC").Image = "boton_grilla_buscar"
        .Appearance = ccFlat
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD_APO"
      Call Sub_EsperaVentana(cNewEntidad, gcTipoOperacion_Aporte)
    Case "ADD_RES"
      Call Sub_EsperaVentana(cNewEntidad, gcTipoOperacion_Rescate)
    Case "EXIT"
      Unload Me
    Case "DEL"
      ' Call Sub_Eliminar
      Call Sub_Anular
    Case "REFRESH"
      Txt_Num_Cuenta.Text = ""
      Txt_Nombres.Text = ""
      DTP_Fecha_Ini.Value = Date - 1
      DTP_Fecha_Ter.Value = Date + 1
      Grilla.Rows = 1
    Case "UPDATE"
      Call Grilla_DblClick
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
    Case "XLS"
      Call Sub_Excel
  End Select
End Sub

Private Sub Sub_CargaForm()
   Dim lReg  As hCollection.hFields
   Dim lLinea As Long
  
  Call Sub_FormControl_Color(Me.Controls)

  Call Sub_Bloquea_Puntero(Me)
  
  'Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas)
  
  Grilla.Rows = 1
  DTP_Fecha_Ini.Value = PrimerDiaMes(Fnt_FechaServidor)
  DTP_Fecha_Ter.Value = UltimoDiaDelMesEnCurso(DTP_Fecha_Ini.Value)
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Grilla_DblClick()
Dim lKey As String

   With Grilla
      If .Row > 0 Then
         lKey = GetCell(Grilla, .Row, "colum_pk")
         Call Sub_EsperaVentana(lKey, GetCell(Grilla, .Row, "flg_tipo_movimiento"))
      End If
   End With
End Sub

Private Sub Sub_EsperaVentana(pkey, pTipo_Movimiento As String)
    Dim lForm_Fechas_Anteriores As Frm_AporteRescate_Fechas_Anteriores
    Dim lNombre As String
    Dim lCuenta As String
    Me.Enabled = False
    lCuenta = Txt_Num_Cuenta.Tag
    If Not lCuenta = "" Then
    'If fFlg_Fechas_Anteriores Then
        If Not Fnt_ExisteVentanaKey("Frm_AporteRescate_Fechas_Anteriores", pkey) Then
           lNombre = Me.Name
           Set lForm_Fechas_Anteriores = New Frm_AporteRescate_Fechas_Anteriores
           Call lForm_Fechas_Anteriores.Fnt_Modificar(pkey:=pkey _
                                                    , pTipo_Movimiento:=pTipo_Movimiento _
                                                    , pId_Cuenta:=lCuenta _
                                                    , pCod_Arbol_Sistema:=fCod_Arbol_Sistema _
                                                    , pFlg_Fechas_Anteriores:=fFlg_Fechas_Anteriores)
        
           Do While Fnt_ExisteVentanaKey("Frm_AporteRescate_Fechas_Anteriores", pkey)
              DoEvents
           Loop
           If Fnt_ExisteVentana(lNombre) Then
              Call Sub_CargarDatos
            Else
              Exit Sub
           End If
        End If
    Else
       MsgBox "Primero elija una cuenta.", vbInformation + vbOKOnly, Me.Caption
    End If
    Me.Enabled = True
End Sub
Private Sub Sub_Eliminar()
    Dim lid_aporte_rescate_cuenta    As String
    Dim lAporte_Rescate_Cuenta       As Class_APORTE_RESCATE_CUENTA
    If Grilla.Row > 0 Then
        If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
            lid_aporte_rescate_cuenta = GetCell(Grilla, Grilla.Row, "colum_pk")
            If Not lid_aporte_rescate_cuenta = "" Then
                gDB.IniciarTransaccion
                Set lAporte_Rescate_Cuenta = New Class_APORTE_RESCATE_CUENTA
                With lAporte_Rescate_Cuenta
                    .Campo("Id_Apo_Res_Cuenta").Valor = lid_aporte_rescate_cuenta
                    If .Eliminar Then
                        gDB.CommitTransaccion
                        MsgBox GetCell(Grilla, Grilla.Row, "Dsc_Apo_Res_Cuenta") & " Eliminado correctamente.", vbInformation + vbOKOnly, Me.Caption
                    Else
                        gDB.RollbackTransaccion
                        Call Fnt_MsgError(.SubTipo_LOG, _
                                            "Problemas al eliminar el Aporte Rescate.", _
                                            .ErrMsg, _
                                            pConLog:=True)
                    End If
                End With
                Set lAporte_Rescate_Cuenta = Nothing
            End If
            Call Sub_CargarDatos
        End If
    End If
End Sub
Private Sub Sub_Anular()
    Dim lid_aporte_rescate_cuenta    As String
    Dim lId_Mov_Caja                 As String
    Dim lAporte_Rescate_Cuenta       As Class_APORTE_RESCATE_CUENTA
    Dim oMov_caja                   As Class_Mov_Caja
    If Grilla.Row > 0 Then
        If MsgBox("�Seguro que desea Anular ?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
            lid_aporte_rescate_cuenta = GetCell(Grilla, Grilla.Row, "colum_pk")
            lId_Mov_Caja = GetCell(Grilla, Grilla.Row, "id_mov_caja")
            If (Not lid_aporte_rescate_cuenta = "") And (Not lId_Mov_Caja = "") Then
                Set oMov_caja = New Class_Mov_Caja
                With oMov_caja
                    gDB.IniciarTransaccion
                    If .Anular(lId_Mov_Caja) Then
                        gDB.CommitTransaccion
                        MsgBox GetCell(Grilla, Grilla.Row, "Dsc_Apo_Res_Cuenta") & " Anulado correctamente.", vbInformation + vbOKOnly, Me.Caption
                    Else
                        gDB.RollbackTransaccion
                        Call Fnt_MsgError(.SubTipo_LOG, _
                                            "Problemas al Anular el Aporte Rescate.", _
                                            .ErrMsg, _
                                            pConLog:=True)
                                            
                    End If
                End With
                
                Set oMov_caja = Nothing
                
            End If
            
            Call Sub_CargarDatos
        End If
    End If
    
End Sub
Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  Dim lCuenta As String

  Set lForm = New Frm_Reporte_Generico
  lCuenta = Txt_Num_Cuenta.Tag
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Aportes/Rescates" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Cuenta: " & lCuenta
      .Paragraph = "Rut Cliente: " & fRutCliente
      .Paragraph = "Nombre  Cliente: " & Txt_Nombres.Text
      .Paragraph = "Periodo Consulta:  desde el " & DTP_Fecha_Ini.Value & " al " & DTP_Fecha_Ter.Value
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
  
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub
Private Sub Toolbar_Proceso_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim lCuenta As String
    lCuenta = Txt_Num_Cuenta.Tag
    If Not lCuenta = "" Then
    Else
       MsgBox "Primero debe elegir una cuenta.", vbInformation + vbOKOnly, Me.Caption
       Exit Sub
    End If
    Me.SetFocus
    DoEvents
    Select Case Button.Key
        Case "BUSC"
            Call Sub_BuscarDatosGrilla(True)
    End Select
End Sub
Private Sub Sub_BuscarDatosGrilla(Optional p_buscar As Boolean = False)

    Dim oAporteRescate As New Class_APORTE_RESCATE_CUENTA
    '----------------------------------------------------------------------------
    Dim lReg        As hFields
    '----------------------------------------------------------------------------
    Dim lId_Cuenta  As String
    Dim ldFechaIni  As String
    Dim ldFechaFin  As String
    '----------------------------------------------------------------------------
    Dim lLinea      As Long
    Dim lFormato    As String
    Dim lSCodEstado As String
    '----------------------------------------------------------------------------
    Dim lhTotales As hRecord
    Dim lfTotal As hFields

    On Error GoTo ErrProcedure

    Call Sub_Bloquea_Puntero(Me)

    If Txt_Num_Cuenta.Tag = "" Then
        Exit Sub
    End If
    
    lId_Cuenta = Txt_Num_Cuenta.Tag
    ldFechaIni = DTP_Fecha_Ini.Value
    ldFechaFin = DTP_Fecha_Ter.Value
    Grilla.Rows = 1
    With oAporteRescate
        If Not .Buscar_AporteRescatePorFecha(lId_Cuenta, ldFechaIni, ldFechaFin, p_buscar) Then
            GoTo ExitProcedure
        End If
        For Each lReg In .Cursor
            lLinea = Grilla.Rows
            Grilla.AddItem ""
            Call SetCell(Grilla, lLinea, "colum_pk", lReg("Id_Apo_Res_Cuenta").Value)
            Call SetCell(Grilla, lLinea, "fecha", lReg("fecha_movimiento").Value)
            Call SetCell(Grilla, lLinea, "monto", Format(lReg("monto").Value, Fnt_Formato_Moneda(lReg("id_moneda").Value)))
            Call SetCell(Grilla, lLinea, "descripcion", lReg("dsc_apo_res_cuenta").Value)
            Call SetCell(Grilla, lLinea, "banco", NVL(lReg("dsc_banco").Value, ""))
            Call SetCell(Grilla, lLinea, "dsc_estado", NVL(lReg("dsc_estado").Value, ""))
            Call SetCell(Grilla, lLinea, "id_moneda", NVL(lReg("id_moneda").Value, ""))     '=== Agregado por MMA 25/07/08
            Call SetCell(Grilla, lLinea, "dsc_moneda", NVL(lReg("dsc_moneda").Value, ""))
            Call SetCell(Grilla, lLinea, "dsc_caja_cuenta", NVL(lReg("dsc_caja_cuenta").Value, ""))
            Call SetCell(Grilla, lLinea, "Dsc_Apo_Res_Cuenta", IIf(lReg("Flg_Tipo_Movimiento").Value = gcTipoOperacion_Aporte, "Aporte", "Rescate"))
            Call SetCell(Grilla, lLinea, "Flg_Tipo_Movimiento", lReg("Flg_Tipo_Movimiento").Value)
            Call SetCell(Grilla, lLinea, "id_mov_caja", lReg("id_mov_caja").Value)
        Next
    End With

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al traer los Aportes y Rescates.", Err.Description, pConLog:=False)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
    Set oAporteRescate = Nothing
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Excel()
Dim lcExcel As Excel.Application
Dim lcLibro As Excel.Workbook
Dim lcHoja  As Excel.Worksheet
Dim lNro_Libro As Long
Dim lLinea As Long
Dim lFila As Long
Dim lColumna As Long

Const cLineaTitulo = 8
Const cColumna = 2

  Call Sub_Bloquea_Puntero(Me)
  
  If Not Grilla.Rows > 1 Then
    MsgBox "No hay datos en la grilla para exportar a Excel.", vbInformation, Me.Caption
    GoTo ExitProcedure
  End If
  
  Set lcExcel = New Excel.Application
  Set lcLibro = lcExcel.Workbooks.Add
  
  lcExcel.ActiveWindow.DisplayGridlines = False
  
  With lcLibro
    For lNro_Libro = .Worksheets.Count To 2 Step -1
      .Worksheets(lNro_Libro).Delete
    Next lNro_Libro
  
    .Sheets(1).Select
    .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).ShapeRange.IncrementLeft 39.75
'    .ActiveSheet.Pictures.Insert(gStrPictureEmpresa).Select
'    Selection.ShapeRange.ScaleWidth 3.19, 0, 0
'    Selection.ShapeRange.ScaleHeight 3.15, 0, 0
'    Selection.ShapeRange.IncrementLeft 28.5
'    .Sheets(1).Select
    .Worksheets.Item(1).Range("B6").Value = "Gesti�n de Portafolio de Inversi�n"
    .Worksheets.Item(1).Range("B6").Font.Bold = True
    .Worksheets.Item(1).Columns("A:A").ColumnWidth = 5
    .Worksheets.Item(1).Columns("B:B").ColumnWidth = 20
    
    .Worksheets.Item(1).Name = "AportesRescates"
  End With
  
  Set lcHoja = lcLibro.Sheets(1)
  
  BarraProceso.max = Grilla.Rows - 1
  
  With lcHoja
  '=== Agregado por MMA 25/07/08
    .Cells(cLineaTitulo, cColumna).Value = "Cuenta "
    .Cells(cLineaTitulo, cColumna + 1).Value = Txt_Num_Cuenta.Text
    .Cells(cLineaTitulo + 1, cColumna).Value = "Rut Cliente "
    .Cells(cLineaTitulo + 1, cColumna + 1).Value = fRutCliente
    .Cells(cLineaTitulo + 2, cColumna).Value = "Nombre Cliente "
    .Cells(cLineaTitulo + 2, cColumna + 1).Value = Txt_Nombres.Text
    
    .Cells(cLineaTitulo + 4, cColumna + 6).Value = "Fecha Generaci�n Reporte : " & Fnt_FechaServidor
 '===
    .Cells(cLineaTitulo + 4, cColumna).Value = "Aportes/Rescates desde el " & DTP_Fecha_Ini.Value & " al " & DTP_Fecha_Ter.Value
    .Cells(cLineaTitulo + 4, cColumna).Font.Bold = True
    
    If Grilla.Rows = 1 Then
      .Cells(15, cColumna).Value = "Sin Datos"
      .Cells(15, cColumna).Font.Bold = True
    End If
    
    lLinea = cLineaTitulo + 6
    lColumna = cColumna
    
    For lFila = 0 To Grilla.Rows - 1
      BarraProceso.Value = lFila
      Call Sub_Interactivo(gRelogDB)
      If lFila = 0 Then
        .Cells(lLinea, lColumna).Value = Trim(GetCell(Grilla, lFila, "fecha"))
      Else
        .Cells(lLinea, lColumna).Value = Fnt_String2Date(GetCell(Grilla, lFila, "fecha"))
      End If
      
      .Cells(lLinea, lColumna + 1).Value = Trim(GetCell(Grilla, lFila, "Dsc_Apo_Res_Cuenta"))
      .Cells(lLinea, lColumna + 2).Value = Grilla.Cell(flexcpValue, lFila, 5) 'Trim(GetCell(Grilla, lFila, "monto")) '=== Modificado por MMA 25/07/08
      .Cells(lLinea, lColumna + 2).NumberFormat = Fnt_Formato_Moneda(Grilla.Cell(flexcpValue, lFila, 9))             '=== Agregado por MMA 25/07/08
      .Cells(lLinea, lColumna + 3).Value = GetCell(Grilla, lFila, "dsc_estado")
      .Cells(lLinea, lColumna + 4).Value = GetCell(Grilla, lFila, "dsc_caja_cuenta")
      .Cells(lLinea, lColumna + 5).Value = GetCell(Grilla, lFila, "descripcion")
      .Cells(lLinea, lColumna + 6).Value = GetCell(Grilla, lFila, "dsc_moneda")
      .Cells(lLinea, lColumna + 7).Value = GetCell(Grilla, lFila, "banco")
      If lFila = 0 Then
        Call Sub_Colores(lcExcel, lcLibro, "B" & CStr(lLinea), 36)
      End If
      lLinea = lLinea + 1
    Next
  End With
  
  lcLibro.ActiveSheet.Columns("C:C").Select
  lcExcel.Range(lcExcel.Selection, lcExcel.Selection.End(xlToRight)).Select
  lcExcel.Selection.EntireColumn.AutoFit
  lcExcel.Range("B10:B" + CStr(lLinea)).Select
  lcExcel.Range("B10:B" + CStr(lLinea)).HorizontalAlignment = xlLeft

  lcLibro.ActiveSheet.Range("A1").Select
  
  lcExcel.Visible = True
  lcExcel.UserControl = True
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Colores(pExcel As Excel.Application, _
                        pLibro As Excel.Workbook, _
                        pRango As String, _
                        pColor As Long)
  
  With pLibro
    .Sheets(1).Select
    .Sheets(1).Select
    .Sheets(1).Activate
    
    .ActiveSheet.Range(pRango).Select
    .ActiveSheet.Range(pRango).Activate
  End With
  
  With pExcel
    .Range(.Selection, .Selection.End(xlToRight)).Select
    pLibro.ActiveSheet.Range(.Selection, .Selection.End(xlToRight)).Font.Bold = True
    
    .Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    .Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    .Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeRight).Weight = xlMedium
        
    .Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    .Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    .Selection.Borders(xlInsideVertical).Weight = xlThin
    
    .Selection.Interior.ColorIndex = pColor
    .Selection.Interior.Pattern = xlSolid
  End With
  
End Sub

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call cmb_buscar_Click
    End If
End Sub

Private Sub cmb_buscar_Click()
Dim lId_Cuenta    As String
Dim fId_Cuenta    As String
Dim lcCuenta      As Object
Dim ftextCuenta  As String

    ftextCuenta = Txt_Num_Cuenta.Text & "-"
    If Len(ftextCuenta) > 1 Then
        ftextCuenta = Trim(Left(ftextCuenta, InStr(1, ftextCuenta, "-") - 1))
    Else
        ftextCuenta = ""
    End If
    
    
    fId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(ftextCuenta), 0)
    If fId_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = fId_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value
                    Txt_Nombres.Text = .Cursor(1)("nombre_cliente").Value
                    fId_Cuenta = .Cursor(1)("id_cuenta").Value
                    Call Sub_CargarDatos
                Else
                    Txt_Num_Cuenta.Tag = ""
                    Txt_Num_Cuenta.Text = ""
                    Txt_Nombres.Text = ""
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
            Set lcCuenta = Nothing
    End If
End Sub



