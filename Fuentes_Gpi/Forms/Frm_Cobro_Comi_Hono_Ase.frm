VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Cobro_Comi_Hono_Ase 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Calculo Comision Adm. de Cartera"
   ClientHeight    =   5055
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   13605
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   13605
   Begin VB.Frame Frame3 
      Caption         =   "Imputaci�n y Liquidaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1920
      Left            =   60
      TabIndex        =   11
      Top             =   3030
      Width           =   2775
      Begin MSComCtl2.DTPicker DTP_Fecha_Liq 
         Height          =   345
         Left            =   1470
         TabIndex        =   12
         Top             =   1140
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   609
         _Version        =   393216
         Format          =   68026369
         CurrentDate     =   38938
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Mov 
         Height          =   345
         Left            =   1470
         TabIndex        =   13
         Top             =   570
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   609
         _Version        =   393216
         Format          =   68026369
         CurrentDate     =   38938
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Liquidaci�n"
         Height          =   345
         Index           =   1
         Left            =   60
         TabIndex        =   15
         Top             =   1140
         Width           =   1395
      End
      Begin VB.Label Lbl_Fecha_Ini 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Movimiento"
         Height          =   345
         Index           =   0
         Left            =   60
         TabIndex        =   14
         Top             =   570
         Width           =   1395
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filtro - Rango"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2625
      Left            =   60
      TabIndex        =   4
      Top             =   390
      Width           =   2775
      Begin VB.CheckBox chk_TodasCtas 
         Caption         =   "Todas"
         Height          =   210
         Left            =   705
         TabIndex        =   18
         ToolTipText     =   "Todas las Cuentas"
         Top             =   780
         Width           =   855
      End
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2205
         Picture         =   "Frm_Cobro_Comi_Hono_Ase.frx":0000
         TabIndex        =   16
         Top             =   375
         Width           =   375
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
         Height          =   345
         Left            =   1290
         TabIndex        =   5
         Top             =   1620
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   609
         _Version        =   393216
         Format          =   68026369
         CurrentDate     =   38938
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ini 
         Height          =   345
         Left            =   1290
         TabIndex        =   6
         Top             =   1230
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   609
         _Version        =   393216
         Format          =   68026369
         CurrentDate     =   38938
      End
      Begin MSComctlLib.Toolbar Toolbar_Calcular 
         Height          =   330
         Left            =   690
         TabIndex        =   9
         Top             =   2130
         Width           =   1740
         _ExtentX        =   3069
         _ExtentY        =   582
         ButtonWidth     =   3043
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar Comisiones"
               Key             =   "CALC"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
            EndProperty
         EndProperty
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   105
         TabIndex        =   17
         Tag             =   "OBLI=S;CAPTION=Cuenta"
         Top             =   375
         Width           =   2070
         _ExtentX        =   3651
         _ExtentY        =   609
         LabelWidth      =   600
         TextMinWidth    =   1200
         Caption         =   "Cuenta"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   100
      End
      Begin VB.Label Lbl_Fecha_Ini 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Inicio"
         Height          =   345
         Index           =   1
         Left            =   90
         TabIndex        =   8
         Top             =   1230
         Width           =   1185
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha T�rmino"
         Height          =   345
         Index           =   0
         Left            =   90
         TabIndex        =   7
         Top             =   1590
         Width           =   1185
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Comisiones"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4575
      Left            =   2880
      TabIndex        =   0
      Top             =   390
      Width           =   10725
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3765
         Left            =   90
         TabIndex        =   1
         Top             =   630
         Width           =   10575
         _cx             =   18653
         _cy             =   6641
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   15
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Cobro_Comi_Hono_Ase.frx":030A
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Chequeo 
         Height          =   330
         Left            =   90
         TabIndex        =   10
         Top             =   240
         Width           =   1290
         _ExtentX        =   2275
         _ExtentY        =   582
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Wrappable       =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_ALL"
               Description     =   "Selecciona todos los items"
               Object.ToolTipText     =   "Selecciona todos los items"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_NOTHING"
               Description     =   "Deselecciona todos los items"
               Object.ToolTipText     =   "Deselecciona todos los items"
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   13605
      _ExtentX        =   23998
      _ExtentY        =   635
      ButtonWidth     =   1773
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Ejecutar"
            Key             =   "CARGOS"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Excel"
            Key             =   "XLS"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Cobro_Comi_Hono_Ase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fTipo_Permiso As String

Dim fFecha_Ini As Date
Dim fFECHA_TER As Date
Dim fFECHA_MOV As Date
Dim fFECHA_LIQ As Date

Public Sub Mostrar(pCod_Arbol_Sistema)
  Me.Show
End Sub

Private Sub cmb_buscar_Click()
Dim lId_Cuenta As String
lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)
If lId_Cuenta <> 0 Then
    Txt_Num_Cuenta.Text = ""
    Txt_Num_Cuenta.Tag = lId_Cuenta
    Call Lpr_Buscar_Cuenta("", lId_Cuenta)
End If
End Sub

Private Sub DTP_Fecha_Mov_Change()
    DTP_Fecha_Liq = DTP_Fecha_Mov + 30
End Sub

Private Sub Form_Load()
  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("CARGOS").Image = cBoton_Grabar
    .Buttons("XLS").Image = cBoton_Excel
    .Buttons("PRINTER").Image = cBoton_Imprimir
    .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Chequeo
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
      .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
      .Appearance = ccFlat
  End With
  
  With Toolbar_Calcular
   Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("CALC").Image = cBoton_Modificar
    .Appearance = ccFlat
  End With
  
  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla.ColIndex("CHK") Then
    Cancel = True
  ElseIf GetCell(Grilla, Row, "flg_total") = gcFlg_SI Then
    Cancel = True
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "CARGOS"
      Call Sub_RealizarCargos
    Case "XLS"
      Call Sub_Excel
    Case "PRINTER"
      Call Sub_Crea_Informe(ePrinter.eP_Impresora)
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Excel()
Dim lcExcel As Excel.Application
Dim lcLibro As Excel.Workbook
Dim lcHoja  As Excel.Worksheet
Dim lNro_Libro As Long
Dim lLinea As Long
Dim lFila As Long
Dim lColumna As Long
Dim lId_Moneda
Dim lFormato As String

Const cLineaTitulo = 8
Const cColumna = 2

  Call Sub_Bloquea_Puntero(Me)
  
  If Not Grilla.Rows > 1 Then
    MsgBox "No hay datos en la grilla para exportar a Excel.", vbInformation, Me.Caption
    GoTo ExitProcedure
  End If
  
  Set lcExcel = New Excel.Application
  Set lcLibro = lcExcel.Workbooks.Add
  
  lcExcel.ActiveWindow.DisplayGridlines = False
  
  With lcLibro
    For lNro_Libro = .Worksheets.Count To 2 Step -1
      .Worksheets(lNro_Libro).Delete
    Next lNro_Libro
  
    .Sheets(1).Select
    
    .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).Select
'    Selection.ShapeRange.ScaleWidth 3.19, 0, 0
'    Selection.ShapeRange.ScaleHeight 3.15, 0, 0
'    Selection.ShapeRange.IncrementLeft 28.5
'    .Sheets(1).Select
    .Worksheets.Item(1).Range("B6").Value = "Gesti�n de Portafolio de Inversi�n"
    .Worksheets.Item(1).Range("B6").Font.Bold = True
    .Worksheets.Item(1).Columns("A:A").ColumnWidth = 5
    .Worksheets.Item(1).Columns("B:B").ColumnWidth = 20
    
    .Worksheets.Item(1).Name = "Com. Honorarios y Asesorias"
  End With
  
  Set lcHoja = lcLibro.Sheets(1)
  
  BarraProceso.max = Grilla.Rows - 1
  
  With lcHoja
    .Cells(cLineaTitulo, cColumna + 7).Value = "Fecha Generaci�n Reporte : " & Fnt_FechaServidor
    .Cells(cLineaTitulo, cColumna).Value = "Comisiones de Honorarios y Asesorias desde " & DTP_Fecha_Ini & " al " & DTP_Fecha_Ter
    .Cells(cLineaTitulo, cColumna).Font.Bold = True
    
    If Grilla.Rows = 1 Then
      .Cells(12, cColumna).Value = "Sin Datos"
      .Cells(12, cColumna).Font.Bold = True
    End If
    
    lLinea = cLineaTitulo + 2
    lColumna = cColumna
    
    For lFila = 0 To Grilla.Rows - 1
      BarraProceso.Value = lFila
      Call Sub_Interactivo(gRelogDB)
      .Cells(lLinea, lColumna).NumberFormat = "@"
      .Cells(lLinea, lColumna).Value = Trim(GetCell(Grilla, lFila, "dsc_cuenta"))
      .Cells(lLinea, lColumna + 1).Value = Trim(GetCell(Grilla, lFila, "alias"))
      .Range(.Cells(lLinea, lColumna), .Cells(lLinea, lColumna)).HorizontalAlignment = xlCenter
      .Range(.Cells(lLinea, lColumna + 1), .Cells(lLinea, lColumna + 1)).HorizontalAlignment = xlLeft
      .Cells(lLinea, lColumna + 2).Value = Trim(GetCell(Grilla, lFila, "nombre_cliente"))
      .Cells(lLinea, lColumna + 3).Value = GetCell(Grilla, lFila, "dsc_moneda")
      If lFila = 0 Then
        .Cells(lLinea, lColumna + 4).Value = GetCell(Grilla, lFila, "comi_honorarios")
        .Cells(lLinea, lColumna + 5).Value = GetCell(Grilla, lFila, "comi_asesoria")
        .Cells(lLinea, lColumna + 6).Value = GetCell(Grilla, lFila, "total")
      Else
        If lFila = 1 Then
          lId_Moneda = GetCell(Grilla, lFila, "id_moneda")
          lFormato = Fnt_Formato_Moneda(lId_Moneda)
        End If
        .Cells(lLinea, lColumna + 4).Value = Grilla.Cell(flexcpValue, lFila, lColumna + 7) 'GetCell(Grilla, lFila, "comi_honorarios")
        .Cells(lLinea, lColumna + 5).Value = Grilla.Cell(flexcpValue, lFila, lColumna + 8) 'GetCell(Grilla, lFila, "comi_asesoria")
        .Cells(lLinea, lColumna + 6).Value = Grilla.Cell(flexcpValue, lFila, lColumna + 9) 'GetCell(Grilla, lFila, "total")
        .Cells(lLinea, lColumna + 4).NumberFormat = lFormato
        .Cells(lLinea, lColumna + 5).NumberFormat = lFormato
        .Cells(lLinea, lColumna + 6).NumberFormat = lFormato
      End If
      .Cells(lLinea, lColumna + 7).Value = GetCell(Grilla, lFila, "asesor")
      
      If lFila = 0 Then
        Call Sub_Colores(lcExcel, lcLibro, "B" & CStr(lLinea), 36)
      End If

      If GetCell(Grilla, lFila, "flg_total") = gcFlg_SI Then
        .Cells(lLinea, lColumna).Value = ""
        .Cells(lLinea, lColumna + 2).Value = ""
        .Cells(lLinea, lColumna + 3).Value = ""
        Call Sub_Colores(lcExcel, lcLibro, "C" & CStr(lLinea), 35)
        Call Sub_Colores(lcExcel, lcLibro, "D" & CStr(lLinea), 35)
        Call Sub_Colores(lcExcel, lcLibro, "E" & CStr(lLinea) & ":" & "H" & CStr(lLinea), 35)
      End If
          
      lLinea = lLinea + 1
    Next
  End With
  lcLibro.ActiveSheet.Columns("C:C").Select
  lcExcel.Range(lcExcel.Selection, lcExcel.Selection.End(xlToRight)).Select
  lcExcel.Selection.EntireColumn.AutoFit
  lcLibro.ActiveSheet.Range("A1").Select
  
  lcExcel.Visible = True
  lcExcel.UserControl = True
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Colores(pExcel As Excel.Application, _
                        pLibro As Excel.Workbook, _
                        pRango As String, _
                        pColor As Long)
  
  With pLibro
    .Sheets(1).Select
    .Sheets(1).Select
    .Sheets(1).Activate
    
    .ActiveSheet.Range(pRango).Select
    .ActiveSheet.Range(pRango).Activate
  End With
  
  With pExcel
    .Range(.Selection, .Selection.End(xlToRight)).Select
    pLibro.ActiveSheet.Range(.Selection, .Selection.End(xlToRight)).Font.Bold = True
    
    .Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    .Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    .Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeRight).Weight = xlMedium
        
    .Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    .Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    .Selection.Borders(xlInsideVertical).Weight = xlThin
    
    .Selection.Interior.ColorIndex = pColor
    .Selection.Interior.Pattern = xlSolid
  End With
  
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Listado de Comisiones a Cargar" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla_cobro_VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Crea_Informe(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Crea_Informe(ePrinter.eP_PDF)
  End Select
End Sub

Private Sub Sub_CargaForm()
  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_FormControl_Color(Me.Controls)
  
  Grilla.Rows = 1
  
  
  DTP_Fecha_Ter.Value = Fnt_FechaServidor
  DTP_Fecha_Ini.Value = DTP_Fecha_Ter.Value - 30
  
  DTP_Fecha_Mov = DTP_Fecha_Ter
  DTP_Fecha_Liq = DTP_Fecha_Ter + 30
  
  Txt_Num_Cuenta.Text = ""
  Txt_Num_Cuenta.Tag = ""
  Txt_Num_Cuenta.Enabled = True
  cmb_buscar.Enabled = True
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub


Private Sub Toolbar_Calcular_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "CALC"
      Call Sub_BuscarCalculo
  End Select
End Sub


Private Sub Sub_BuscarCalculo()
Dim lcComision_Hono_Ase_Cuenta As Class_Comisiones_Hono_Ase_Cuenta
Dim lcComi_Fija_Hono_Ase_Cuenta As Class_Comi_Fija_Hono_Ase_Cuenta
Dim lReg As hFields
'----------------------------------------------------------------------------
Dim lId_Cuenta  As String
Dim lLinea      As Long
Dim lFormato    As String
'----------------------------------------------------------------------------
Dim lhTotales         As hRecord
Dim lfTotal           As hFields
Dim IdCuenta_Aux      As String
Dim Descripcion_Error As String

On Error GoTo ErrProcedure

  Call Sub_Bloquea_Puntero(Me)

  If Not Fnt_Form_Validar(Me.Controls) Then
    Exit Sub
  End If

  Set lhTotales = New hRecord
  With lhTotales
    .AddField "id_moneda"
    .AddField "dsc_moneda"
    .AddField "honorarios"
    .AddField "asesorias"
    .AddField "total"
  End With
    
  'lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
  lId_Cuenta = Txt_Num_Cuenta.Tag
  If chk_TodasCtas.Value = 1 Then
    lId_Cuenta = ""
  End If
  
  Grilla.Rows = 1

  fFecha_Ini = DTP_Fecha_Ini.Value
  fFECHA_TER = DTP_Fecha_Ter.Value
  
  Set lcComision_Hono_Ase_Cuenta = New Class_Comisiones_Hono_Ase_Cuenta
  With lcComision_Hono_Ase_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    If Not .Calcular_Periodo(fFecha_Ini, fFECHA_TER) Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas al calcular las comisiones.", .ErrMsg, pConLog:=True)
      GoTo ExitProcedure
    End If
      
    For Each lReg In .Cursor
      IdCuenta_Aux = ""
      Descripcion_Error = ""
      
      lLinea = Grilla.Rows
      Grilla.AddItem ""
      
      Grilla.Cell(flexcpChecked, lLinea, Grilla.ColIndex("chk")) = flexChecked
      Call SetCell(Grilla, lLinea, "id_cuenta", lReg("id_cuenta").Value, False)
      Call SetCell(Grilla, lLinea, "flg_total", gcFlg_NO, False)
      Call SetCell(Grilla, lLinea, "comi_honorarios_orig", lReg("comision_honorarios").Value, False)
      Call SetCell(Grilla, lLinea, "comi_asesoria_orig", lReg("comision_asesorias").Value, False)
      Call SetCell(Grilla, lLinea, "id_moneda", lReg("id_moneda").Value, False)
      '----
      Call SetCell(Grilla, lLinea, "dsc_cuenta", lReg("num_cuenta").Value)
      IdCuenta_Aux = lReg("num_cuenta").Value
      
      Call SetCell(Grilla, lLinea, "rut_cliente", lReg("rut_cliente").Value)
      Call SetCell(Grilla, lLinea, "nombre_cliente", Trim(lReg("nombre_cliente").Value))
      Call SetCell(Grilla, lLinea, "dsc_moneda", lReg("dsc_moneda").Value)
      Call SetCell(Grilla, lLinea, "comi_honorarios", lReg("comision_honorarios").Value)
      Call SetCell(Grilla, lLinea, "comi_asesoria", lReg("comision_asesorias").Value)
      Call SetCell(Grilla, lLinea, "Total", lReg("comision_asesorias").Value + lReg("comision_honorarios").Value)
      Call SetCell(Grilla, lLinea, "asesor", lReg("Asesor").Value)
      Call SetCell(Grilla, lLinea, "alias", lReg("Alias_Cta").Value)
      Set lfTotal = lhTotales.Buscar("id_moneda", lReg("id_moneda").Value)
      If lfTotal Is Nothing Then
        Set lfTotal = lhTotales.Add
        lfTotal("id_moneda").Value = lReg("id_moneda").Value
        lfTotal("dsc_moneda").Value = Trim(lReg("dsc_moneda").Value)
        lfTotal("honorarios").Value = lReg("comision_honorarios").Value
        lfTotal("asesorias").Value = lReg("comision_asesorias").Value
        lfTotal("total").Value = lReg("comision_asesorias").Value + lReg("comision_honorarios").Value
      Else
        lfTotal("honorarios").Value = lfTotal("honorarios").Value + lReg("comision_honorarios").Value
        lfTotal("asesorias").Value = lfTotal("asesorias").Value + lReg("comision_asesorias").Value
        lfTotal("total").Value = lfTotal("total").Value + lReg("comision_asesorias").Value + lReg("comision_honorarios").Value
      End If
    Next
    
  End With

  Set lcComi_Fija_Hono_Ase_Cuenta = New Class_Comi_Fija_Hono_Ase_Cuenta
  With lcComi_Fija_Hono_Ase_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    If Not .Calcular_Periodo(fFecha_Ini, fFECHA_TER) Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas al calcular las comisiones fijas.", .ErrMsg, pConLog:=True)
      GoTo ExitProcedure
    End If
    
    For Each lReg In .Cursor
      IdCuenta_Aux = ""
      Descripcion_Error = ""
      
      lLinea = Grilla.FindRow(Item:=lReg("id_cuenta").Value, Col:=Grilla.ColIndex("id_cuenta"))
      If lLinea = cNewEntidad Then
        lLinea = Grilla.Rows
        Grilla.AddItem ""
        
        Grilla.Cell(flexcpChecked, lLinea, Grilla.ColIndex("chk")) = flexChecked
        Call SetCell(Grilla, lLinea, "id_cuenta", lReg("id_cuenta").Value, False)
        Call SetCell(Grilla, lLinea, "flg_total", gcFlg_NO, False)
        Call SetCell(Grilla, lLinea, "id_moneda", lReg("id_moneda").Value, False)
        Call SetCell(Grilla, lLinea, "dsc_cuenta", lReg("num_cuenta").Value & "-" & lReg("abr_cuenta").Value)
        IdCuenta_Aux = lReg("num_cuenta").Value
        Call SetCell(Grilla, lLinea, "dsc_moneda", lReg("dsc_moneda").Value)
      End If
      
      Call SetCell(Grilla, lLinea, "comi_honorarios_orig", To_Number(GetCell(Grilla, lLinea, "comi_honorarios_orig")) + lReg("monto_comision_honorario").Value, False)
      Call SetCell(Grilla, lLinea, "comi_asesoria_orig", To_Number(GetCell(Grilla, lLinea, "comi_asesoria_orig")) + lReg("monto_comision_asesoria").Value, False)
      Call SetCell(Grilla, lLinea, "rut_cliente", lReg("rut_cliente").Value)
      Call SetCell(Grilla, lLinea, "nombre_cliente", Trim(lReg("nombre_cliente").Value))
      '----
      Call SetCell(Grilla, lLinea, "comi_honorarios", To_Number(GetCell(Grilla, lLinea, "comi_honorarios")) + lReg("monto_comision_honorario").Value)
      Call SetCell(Grilla, lLinea, "comi_asesoria", To_Number(GetCell(Grilla, lLinea, "comi_asesoria")) + lReg("monto_comision_asesoria").Value)
      Call SetCell(Grilla, lLinea, "Total", To_Number(GetCell(Grilla, lLinea, "comi_honorarios")) + To_Number(GetCell(Grilla, lLinea, "comi_asesoria")))
      Call SetCell(Grilla, lLinea, "asesor", lReg("Asesor").Value)
      Call SetCell(Grilla, lLinea, "alias", lReg("Alias_Cta").Value)
      Set lfTotal = lhTotales.Buscar("id_moneda", lReg("id_moneda").Value)
      If lfTotal Is Nothing Then
        Set lfTotal = lhTotales.Add
        lfTotal("id_moneda").Value = lReg("id_moneda").Value
        lfTotal("dsc_moneda").Value = Trim(lReg("dsc_moneda").Value)
        lfTotal("honorarios").Value = lReg("monto_comision_honorario").Value
        lfTotal("asesorias").Value = lReg("monto_comision_asesoria").Value
        lfTotal("total").Value = lReg("monto_comision_asesoria").Value + lReg("monto_comision_honorario").Value
      Else
        lfTotal("honorarios").Value = lfTotal("honorarios").Value + lReg("monto_comision_honorario").Value
        lfTotal("asesorias").Value = lfTotal("asesorias").Value + lReg("monto_comision_asesoria").Value
        lfTotal("total").Value = lfTotal("total").Value + lReg("monto_comision_asesoria").Value + lReg("monto_comision_honorario").Value
      End If
    Next
  End With
  
  'Le coloca formato a los valores numericos dependiendo de la moneda
  For lLinea = 1 To Grilla.Rows - 1
    lFormato = Fnt_Formato_Moneda(GetCell(Grilla, lLinea, "id_moneda"))
  
    Call SetCell(Grilla, lLinea, "comi_honorarios", Format(To_Number(GetCell(Grilla, lLinea, "comi_honorarios")), lFormato))
    Call SetCell(Grilla, lLinea, "comi_asesoria", Format(To_Number(GetCell(Grilla, lLinea, "comi_asesoria")), lFormato))
    Call SetCell(Grilla, lLinea, "Total", Format(GetCell(Grilla, lLinea, "total"), lFormato))
  Next
  
  Rem Coloca el total de comisiones y asesorias
  For Each lfTotal In lhTotales
    lLinea = Grilla.Rows
    Grilla.AddItem ""
    
    lFormato = Fnt_Formato_Moneda(lfTotal("id_moneda").Value)
    
    Call SetCell(Grilla, lLinea, "flg_total", gcFlg_SI, False)
    Call SetCell(Grilla, lLinea, "id_moneda", lfTotal("id_moneda").Value)
    Call SetCell(Grilla, lLinea, "comi_honorarios", Format(lfTotal("honorarios").Value, lFormato))
    Grilla.ColFormat(9) = lFormato
    Call SetCell(Grilla, lLinea, "comi_asesoria", Format(lfTotal("asesorias").Value, lFormato))
    Call SetCell(Grilla, lLinea, "Total", Format(lfTotal("total").Value, lFormato))
    Grilla.ColFormat(10) = lFormato
    Grilla.Cell(flexcpBackColor, lLinea, 0, lLinea, 13) = &HC0FFC0
    Grilla.Cell(flexcpFontBold, lLinea, 0, lLinea, 13) = True
    Call UnionCell(Grilla, Grilla.Rows - 1, 1, 8, "Total Moneda " & lfTotal("dsc_moneda").Value, &HC0FFC0, True)
  Next
  
  Grilla.SetFocus

ErrProcedure:
  If Not Err.Number = 0 Then
    If Err.Number = 94 And IdCuenta_Aux <> "" Then
        Descripcion_Error = Err.Description & " -    Cuenta N�: " & IdCuenta_Aux
    Else
        Descripcion_Error = Err.Description
    End If
    
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al calcular las comisiones.", Descripcion_Error, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcComision_Hono_Ase_Cuenta = Nothing
  Set lcComi_Fija_Hono_Ase_Cuenta = Nothing
  Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Sub_RealizarCargos()
Dim lcComi_Hono_Ase_Cta_Cargo As Class_Comi_Hono_Ase_Cta_Cargo
'---------------------------------------------------------------------
Dim lHoy As Date
'----
Dim lLinea As Long
Dim lColCHK As Long

fFECHA_MOV = DTP_Fecha_Mov.Value
fFECHA_LIQ = DTP_Fecha_Liq.Value

  Call Sub_Bloquea_Puntero(Me)

  lColCHK = Grilla.ColIndex("chk")

  BarraProceso.Value = 0
  If Grilla.Rows > 1 Then
    BarraProceso.max = (Grilla.Rows - 1)
  End If

  lHoy = Fnt_FechaServidor
  For lLinea = 1 To (Grilla.Rows - 1)
    BarraProceso.Value = BarraProceso.Value + 1
    If Grilla.Cell(flexcpChecked, lLinea, lColCHK) = flexChecked And GetCell(Grilla, lLinea, "flg_total") <> gcFlg_SI Then
      Set lcComi_Hono_Ase_Cta_Cargo = New Class_Comi_Hono_Ase_Cta_Cargo
      With lcComi_Hono_Ase_Cta_Cargo
        .Campo("ID_CUENTA").Valor = GetCell(Grilla, lLinea, "id_cuenta")
        .Campo("ID_MONEDA").Valor = GetCell(Grilla, lLinea, "ID_MONEDA")
        .Campo("COMISION_HONORARIOS").Valor = GetCell(Grilla, lLinea, "comi_honorarios")
        .Campo("COMISION_ASESORIAS").Valor = GetCell(Grilla, lLinea, "comi_asesoria")
        .Campo("COMISION_ASESORIAS_ORIG").Valor = GetCell(Grilla, lLinea, "comi_asesoria_orig")
        .Campo("COMISION_HONORARIOS_ORIG").Valor = GetCell(Grilla, lLinea, "comi_honorarios_orig")
        .Campo("FCH_COMI_HA_CTA_CARGO").Valor = fFECHA_MOV
        .Campo("FECHA_INI").Valor = fFecha_Ini
        .Campo("FECHA_TER").Valor = fFECHA_TER
        If Not .Guardar(fFECHA_LIQ - fFECHA_MOV) Then
          Call Fnt_MsgError(.SubTipo_LOG, "Problemas al guardar las comisiones.", .ErrMsg, pConLog:=True)
          GoTo ExitProcedure
        End If
      End With
      Set lcComi_Hono_Ase_Cta_Cargo = Nothing
    End If
  Next
  
  MsgBox "Cargos Realizados.", vbInformation, Me.Caption
  Call Sub_CargaForm
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al guardar las comisiones.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcComi_Hono_Ase_Cta_Cargo = Nothing
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Toolbar_Chequeo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Call Sub_CambiaCheck(Grilla, (Button.Key = "SEL_ALL"))
End Sub

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call Lpr_Buscar_Cuenta("TXT")
    End If
End Sub

Private Sub Lpr_Buscar_Cuenta(ByVal Origen As String, Optional p_Id_Cuenta As String)
Dim lcCuenta      As Object

    If Origen = "TXT" Then
        If InStr(1, Txt_Num_Cuenta.Text, "-") > 0 Then
           Txt_Num_Cuenta.Text = Trim(Left(Txt_Num_Cuenta.Text, InStr(1, Txt_Num_Cuenta.Text, "-") - 1))
        End If
        p_Id_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text), 0)
    End If
    If p_Id_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = p_Id_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
            Set lcCuenta = Nothing
    End If
End Sub

Private Sub chk_TodasCtas_Click()

If chk_TodasCtas.Value = 1 Then
    Txt_Num_Cuenta.Text = ""
    Txt_Num_Cuenta.Tag = ""
    Txt_Num_Cuenta.Enabled = False
    cmb_buscar.Enabled = False
Else
    Txt_Num_Cuenta.Enabled = True
    cmb_buscar.Enabled = True
End If

End Sub

Public Sub Sub_Grilla_cobro_VsPrinter(ByRef pVsPrinter As VsPrinter _
                              , ByRef pGrilla As VSFlexGrid _
                              , Optional pTitulo4Pagina As Boolean = True _
                              , Optional pNoEndTable As Boolean = False _
                              , Optional pLineaNoLinea As Boolean = True _
                              , Optional pHeaders _
                              , Optional pConcideraCabecera As Boolean = False _
                              , Optional pLineasCabezeras _
                              , Optional pLineas1raHoja _
                              , Optional pLineasOtrasHojas)
Dim lVSFormat As String
Dim lVSHeader As String
Dim lVSRecord As String
'---------
Dim lCantColum  As Long
Dim lLinea      As Long
Dim lColum      As Long
Dim lText       As String
Dim lAncho      As Double
'----------------------------
Dim lHojas      As Long
Dim lhCabeceras As hRecord
Dim lfCabecera  As hFields
Dim lLinea_Table As Long

  lCantColum = 0
  lVSFormat = ""
  lVSHeader = ""
  
  Set lhCabeceras = New hRecord
  With lhCabeceras
    .AddField "Texto"
  End With
  
  With pGrilla
    lLinea = 0
    For lColum = 0 To .Cols - 1
      If Not .ColHidden(lColum) Then
       If Not .ColKey(lColum) = "chk" Then
        If Not lVSHeader = "" Then
          lVSHeader = lVSHeader & "|"
          lVSFormat = lVSFormat & "|"
        End If
        
        lVSHeader = lVSHeader & .TextMatrix(lLinea, lColum)
        lVSFormat = lVSFormat & Fnt_Formats_Grilla2VsPrinter(pGrilla, lColum)
        
        lCantColum = lCantColum + 1
       End If
      End If
    Next
    
    If Not IsMissing(pHeaders) Then
      lVSHeader = pHeaders
    End If
    
    pVsPrinter.StartTable
    pVsPrinter.TableCell(tcAlignCurrency) = False
    
    lHojas = 1
    
    lCantColum = 0
    lLinea_Table = 0
    For lLinea = IIf(pConcideraCabecera, 0, 1) To (.Rows - 1) '.FixedRows To (.Rows - 1)
      lLinea_Table = lLinea_Table + 1
      lVSRecord = ""
      lCantColum = 0
      For lColum = 0 To .Cols - 1
        If Not .ColHidden(lColum) Then
         If Not .ColKey(lColum) = "chk" Then
          lCantColum = lCantColum + 1
          If Not lVSRecord = "" Then
            lVSRecord = lVSRecord & "|"
          End If
        
          lText = String(.RowOutlineLevel(lLinea), vbTab)
        
          If .ColDataType(lColum) = flexDTBoolean Then
'            If .Cell(flexcpChecked, lLinea, lColum) = flexChecked Then
'              lText = lText & "X"
            'End If
          Else
            lText = lText & .Cell(flexcpTextDisplay, lLinea, lColum)
          End If
        
          lVSRecord = lVSRecord & lText
         End If
        End If
      Next
      
      If Not IsMissing(pLineasCabezeras) Then
        If (lLinea + 1) <= pLineasCabezeras Then
          lhCabeceras.Add.Fields("texto").Value = lVSRecord
        End If
      End If
      
      If Not IsMissing(pLineas1raHoja) Then
        If (lHojas = 1) And (((lLinea + 1) Mod pLineas1raHoja) = 0) Then
          For Each lfCabecera In lhCabeceras
            Call pVsPrinter.AddTable(lVSFormat, lVSHeader, lfCabecera("texto").Value, pGrilla.BackColorFixed, vbRed, pTitulo4Pagina)
          Next
          pVsPrinter.TableCell(tcBackColor, lLinea_Table, 1, lLinea_Table + pLineas1raHoja, lCantColum) = pGrilla.BackColorFixed
          lLinea_Table = lLinea_Table + pLineasCabezeras
          lHojas = lHojas + 1
        End If
      End If
        
      If Not IsMissing(pLineasOtrasHojas) Then
        If (Not lHojas = 1) And (lLinea > pLineas1raHoja) And (((lLinea - pLineas1raHoja + 1) Mod pLineasOtrasHojas) = 0) Then
          For Each lfCabecera In lhCabeceras
            Call pVsPrinter.AddTable(lVSFormat, lVSHeader, lfCabecera("texto").Value, pGrilla.BackColorFixed, vbRed, pTitulo4Pagina)
          Next
          pVsPrinter.TableCell(tcBackColor, lLinea_Table, 1, lLinea_Table + pLineasOtrasHojas, lCantColum) = pGrilla.BackColorFixed
          lLinea_Table = lLinea_Table + pLineasCabezeras
          lHojas = lHojas + 1
        End If
      End If
        
        
        
      Call pVsPrinter.AddTable(lVSFormat, lVSHeader, lVSRecord, pGrilla.BackColorFixed, , pTitulo4Pagina)
    Next
  
    If lLinea = 0 Then
      If pLineaNoLinea Then
        For lColum = 0 To .Cols - 1
          If Not lVSRecord = "" Then
            lVSRecord = lVSRecord & "|"
          End If
          lVSRecord = lVSRecord & " "
        Next
        Call pVsPrinter.AddTable(lVSFormat, lVSHeader, lVSRecord, pGrilla.BackColorFixed, , pTitulo4Pagina)
      End If
    End If
    
    For lLinea = 0 To (.FixedRows - 1)
      'Configura los caption de la grilla
      pVsPrinter.TableCell(tcFontBold, lLinea) = True
      pVsPrinter.TableCell(tcFontSize, lLinea) = 7
      pVsPrinter.TableCell(tcBackColor, lLinea) = pGrilla.BackColorFixed
    Next
        
    'Ajusta los arnchos a los textos
    For lLinea = IIf(pConcideraCabecera, 1, 0) To pVsPrinter.TableCell(tcRows)
      For lColum = 1 To pVsPrinter.TableCell(tcCols)
        lAncho = pVsPrinter.TextWidth(pVsPrinter.TableCell(tcText, lLinea, lColum))
        If lAncho > pVsPrinter.TableCell(tcColWidth, col1:=lColum) Then
          pVsPrinter.TableCell(tcColWidth, col1:=lColum) = lAncho
        End If
      Next
    Next
    
    pVsPrinter.FontSize = 6
    pVsPrinter.Align = vbAlignRight
    pVsPrinter.Paragraph = "Cantidad: " & Format(.Rows - 1, "#,##0")
    
    If Not pNoEndTable Then
      pVsPrinter.EndTable
    End If
  End With
End Sub

Public Function Fnt_Formats_Grilla2VsPrinter(pGrilla As VSFlexGrid, pCol As Long) As String
Dim lFormat As String
Dim lAling As String

  lFormat = ""
  lAling = ""
  With pGrilla
    Select Case .FixedAlignment(pCol)
      Case flexAlignRightCenter, flexAlignRightBottom, flexAlignRightTop
        lAling = ">"
    End Select
    
    'lFormat = lFormat & lAling & "=+~" & .ColWidth(pCol)
    lFormat = lFormat & lAling & "+~" & .ColWidth(pCol)
  End With
  
  Fnt_Formats_Grilla2VsPrinter = lFormat
End Function

Private Sub Sub_Crea_Informe(pTipoSalida As ePrinter)
'------------------------------------------
Const clrHeader = &HD0D0D0
'Const sHeader_Clt = "Cuenta|Rut|Nombre Cliente|Disponible T+0|Retenci�n T+1|Retenci�n T+2"
'Const sFormat_Clt = "1000|>1300|3000|>1100|>1100|>1100"
Dim sHeader_Clt             As String
Dim sFormat_Clt             As String
'------------------------------------------
Dim sRecord
Dim sRecordCaja
Dim bAppend
Dim lLinea                  As Integer
'------------------------------------------
Dim lForm                   As Frm_Reporte_Generico
Dim sTitulo                 As String
Dim sFechaDesde             As String
Dim sFechaHasta             As String
Dim lId_Cuenta              As Integer
Dim lId_Asesor              As Integer
Dim lMoneda_Cuenta          As Integer
Dim sNombre_Col             As String
Dim sTituloT0               As String
Dim sTituloT1               As String
Dim sTituloT2               As String
Dim lDecimalesCuenta        As Integer
Dim dTotalT0                As Double
Dim dTotalT1                As Double
Dim dTotalT2                As Double
'---------------------------------------------
Dim lReg                    As hCollection.hFields
Dim lCursor_Blotter       As hRecord
Dim lCajas_Ctas             As Class_Cajas_Cuenta
Dim lReg_Caj                As hFields
Dim lCursor_Caj             As hRecord
Dim dSaldoCaja              As Double
Dim lCaja                   As Integer
Dim lCol                    As Integer
Dim sSimbolo                As String
Dim nDecimalesCaja          As Integer
Dim nombre_col              As String
Dim lOtrasCajasActivas      As Integer
Dim i                       As Integer
Dim sCod_Producto           As String
Dim lCantColum  As Long
Dim lColum      As Long
'---------------------------------------------

    Call Sub_Bloquea_Puntero(Me)
        
'    sTitulo = "Blotter Operaciones al " & DTP_Fecha_Consulta.Value
'    If Not Fnt_Form_Validar(Me.Controls) Then
'        Exit Sub
'    End If
'
'    lId_Cuenta = IIf(Trim(Txt_Num_Cuenta.Tag) = "" Or chk_TodasCtas.Value = 1, 0, Txt_Num_Cuenta.Tag)
'    lId_Asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesor) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Asesor))
'    '29/07/2009 Agregado por MMardones
'    sCod_Producto = IIf(Fnt_ComboSelected_KEY(Cmb_Instrumento) = cCmbKALL, "", Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text))
'
'    gDB.Parametros.Clear
'    gDB.Procedimiento = "PKG_Operaciones$ConsultaBlotter"
'    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
'    gDB.Parametros.Add "pFecha_Desde", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
'    gDB.Parametros.Add "pFecha_Hasta", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
'    gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
'    If lId_Cuenta <> 0 Then
'        gDB.Parametros.Add "pId_Cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
'    End If
'    If lId_Asesor <> 0 Then
'        gDB.Parametros.Add "pId_asesor", ePT_Numero, lId_Asesor, ePD_Entrada
'    End If
'    '29/07/2009 Agregado por MMardones
'    If sCod_Producto <> Cmb_Instrumento.Text Then
'        gDB.Parametros.Add "pCod_Instrumento", ePT_Caracter, sCod_Producto, ePD_Entrada
'    End If
'
'    If Not gDB.EjecutaSP Then
'        Exit Sub
'    End If

'    Set lCursor_Blotter = gDB.Parametros("Pcursor").Valor
'    If lCursor_Blotter.Count = 0 Then
'        MsgBox "No se registran movimientos en esta fecha.", vbInformation, Me.Caption
'    Else
        sHeader_Clt = "Cuenta|Rut Cliente/Alias|Nombre Cliente|Moneda|Honorarios|Asesoria|Total"
        sFormat_Clt = "<1500|<1500|<4500|<1000|1700>|1000>|2000>"
        Set lForm = New Frm_Reporte_Generico
        lForm.VsPrinter.MarginLeft = 1200
        Call lForm.Sub_InicarDocumento(pTitulo:="Listado de Comisiones a Cargar" _
                                       , pTipoSalida:=pTipoSalida _
                                       , pOrientacion:=orLandscape)
       'With lForm.VsPrinter
           With lForm.VsPrinter
                .FontSize = 10
                .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
                .Paragraph = "" 'salto de linea
                .FontBold = False
                .FontSize = 9
                .AddTable sFormat_Clt, sHeader_Clt, "", clrHeader, bAppend
            For lColum = 1 To Grilla.Rows - 2
                sRecord = GetCell(Grilla, lColum, "dsc_cuenta") & "|" & _
                          GetCell(Grilla, lColum, "alias") & "|" & _
                          GetCell(Grilla, lColum, "nombre_cliente") & "|" & _
                          GetCell(Grilla, lColum, "dsc_moneda") & "|" & _
                          GetCell(Grilla, lColum, "comi_honorarios") & "|" & _
                          GetCell(Grilla, lColum, "comi_asesoria") & "|" & _
                          GetCell(Grilla, lColum, "Total")
                         
                          
                          
                 .AddTable sFormat_Clt, "", sRecord, clrHeader, , bAppend
                '.AddTable sFormat_Clt, sHeader_Clt, sRecord, clrHeader, , bAppend
            Next
            
            For lColum = (Grilla.Rows - 1) To Grilla.Rows - 1
                sRecord = "Total" & "|" & _
                           "|" & _
                           "|" & _
                           "|" & _
                          GetCell(Grilla, lColum, "comi_honorarios") & "|" & _
                           "|" & _
                          GetCell(Grilla, lColum, "Total")
             .AddTable sFormat_Clt, sRecord, "", clrHeader, , bAppend
           Next
            '.EndTable
            .EndDoc
        End With
        
    'End If
    Call Sub_Desbloquea_Puntero(Me)
End Sub

