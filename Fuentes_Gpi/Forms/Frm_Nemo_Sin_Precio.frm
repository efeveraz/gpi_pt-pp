VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Nemo_Sin_Precio 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Nemotécnicos sin Precio de Cierre"
   ClientHeight    =   4080
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6690
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4080
   ScaleWidth      =   6690
   Begin VB.Frame Frnm_Grilla_Detalle 
      Caption         =   "Nemotécnicos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3615
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   6645
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   6375
         _cx             =   11245
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   1
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Nemo_Sin_Precio.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   6690
      _ExtentX        =   11800
      _ExtentY        =   635
      ButtonWidth     =   1429
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Excel"
            Key             =   "EXCEL"
            Description     =   "Exporta a Excel Nemotécnicos"
            Object.ToolTipText     =   "Exporta a Excel Nemotécnicos"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Nemo_Sin_Precio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fKey As String
Dim fFecha As Date
Dim fDsc_Producto As String
Dim fId_Empresa As Integer

Public Function Fnt_Modificar(pCod_Producto, pFecha, pDsc_Producto, pId_Empresa)
  fKey = pCod_Producto
  fFecha = pFecha
  fDsc_Producto = pDsc_Producto
  fId_Empresa = pId_Empresa

  
  Load Me
  
  Call Form_Resize
   
  Call Sub_CargaForm
  Me.Caption = "Nemotécnicos de " & fDsc_Producto & " sin precio de cierre"
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXCEL").Image = cBoton_Excel
      .Buttons("EXIT").Image = cBoton_Salir
  End With
End Sub
Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
    
  Select Case Button.Key
    Case "EXCEL"
        Call Sub_Excel
    Case "EXIT"
      Unload Me
  End Select
End Sub
Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  On Error Resume Next
  Unload Me
End Sub

Private Sub Sub_CargaForm()

    Call Sub_Bloquea_Puntero(Me)
    Call Sub_CargarDatos
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargarDatos()
Dim lReg        As hFields
Dim lMsg As String
Dim lCursor As hRecord
Dim lLinea     As Long

    Grilla.Rows = 1
    With gDB
      .Parametros.Clear
      .Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
      .Parametros.Add "Pfecha_cierre", ePT_Fecha, fFecha, ePD_Entrada
      .Parametros.Add "Pcod_producto", ePT_Caracter, fKey, ePD_Entrada
      .Parametros.Add "pid_empresa", ePT_Caracter, fId_Empresa, ePD_Entrada
      .Procedimiento = "PKG_PROCESO_PRECIERRE$Trae_Nemos_Sin_Precio_Cierre"
      If .EjecutaSP Then
        Set lCursor = gDB.Parametros("Pcursor").Valor
        For Each lReg In lCursor
            lLinea = Grilla.Rows
            Grilla.AddItem ""
            Call SetCell(Grilla, lLinea, "nemotecnico", lReg("nemotecnico").Value)
        Next
      Else
        MsgBox "Error al consultar Nemotécnicos sin precios" & .ErrMsg, vbInformation, Me.Caption
      End If
      .Parametros.Clear
    End With
End Sub

Private Sub Sub_Excel()
Dim lcExcel As Excel.Application
Dim lcLibro As Excel.Workbook
Dim lcHoja  As Excel.Worksheet
Dim lFilaExcel   As Long
Dim lFilaGrilla  As Long

  Set lcExcel = Fnt_CreateObject(cExcel)
  lcExcel.DisplayAlerts = False
  lcExcel.Visible = False
  Set lcLibro = lcExcel.Workbooks.Add

  Set lcHoja = lcLibro.Worksheets(1)
  lcHoja.Name = "Nemotécnicos sin precio"
  lcHoja.Activate
  
  With lcLibro
    .ActiveSheet.Cells(1, 1).Select
    .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).ShapeRange.IncrementLeft 39.75
    .Worksheets.Item(1).Range("B5").Value = "Nemotécnicos " & fDsc_Producto & " sin Precio de cierre"
    .Worksheets.Item(1).Range("B5").Font.Size = 14
    .Worksheets.Item(1).Range("B5").Font.Bold = True
    .Worksheets.Item(1).Range("B7").Value = "Empresa: " & gDsc_Empresa
    .Worksheets.Item(1).Range("B8").Value = "Fecha Cierre: " & Fnt_FechaServidor

  End With

  With lcHoja
    
    lFilaExcel = 10
    
    .Cells(lFilaExcel, 2).Value = "Nemotécnico"
    .Range(.Cells(11, 2), .Cells(lFilaExcel, 2)).HorizontalAlignment = xlCenter
    .Range(.Cells(lFilaExcel, 2), .Cells(lFilaExcel, 2)).BorderAround
    .Range(.Cells(lFilaExcel, 2), .Cells(lFilaExcel, 2)).Borders.Color = RGB(0, 0, 0)
    .Range(.Cells(lFilaExcel, 2), .Cells(lFilaExcel, 2)).Interior.Color = RGB(255, 255, 0)
    .Range(.Cells(lFilaExcel, 2), .Cells(lFilaExcel, 2)).Font.Bold = True

    lFilaExcel = lFilaExcel + 1
    For lFilaGrilla = 1 To (Grilla.Rows - 1)
      .Cells(lFilaExcel, 2).Value = GetCell(Grilla, lFilaGrilla, "nemotecnico")
      lFilaExcel = lFilaExcel + 1
    Next
    .Range(.Cells(11, 2), .Cells(lFilaExcel - 1, 2)).HorizontalAlignment = xlLeft
    .Range(.Cells(11, 2), .Cells(lFilaExcel - 1, 2)).EntireColumn.AutoFit
    .Range(.Cells(11, 2), .Cells(lFilaExcel - 1, 2)).BorderAround
    .Range(.Cells(11, 2), .Cells(lFilaExcel - 1, 2)).Borders.Color = RGB(0, 0, 0)
      
    
  End With
ExitProcedure:
  lcExcel.Visible = True
  lcExcel.DisplayAlerts = True

  Set lcExcel = Nothing
  Set lcLibro = Nothing
  Set lcHoja = Nothing
  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)
End Sub
