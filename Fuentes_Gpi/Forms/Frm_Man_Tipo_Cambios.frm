VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Man_Tipo_Cambios 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Tipos de Cambios"
   ClientHeight    =   4185
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   8820
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4185
   ScaleWidth      =   8820
   Begin VB.Frame Frame1 
      Caption         =   "Tipos de Cambio"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   75
      TabIndex        =   0
      Top             =   420
      Width           =   8685
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   90
         TabIndex        =   1
         Top             =   300
         Width           =   8415
         _cx             =   14843
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   6
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Tipo_Cambios.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   8820
      _ExtentX        =   15558
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Eliminar"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Elimina un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Grabar"
            Key             =   "SAVE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Graba la informaci�n de forma permanente"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Tipo_Cambios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With

  Call Sub_CargaForm
  Call Sub_CargarDatos
   
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
  If Col = 3 Or Col = 4 Or Col = 5 Then
    KeyAscii = 0
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
    Case "EXIT"
      Unload Me
    Case "ADD"
      Call Sub_Agregar
    Case "SAVE"
      Call Sub_Grabar
    Case "DEL"
      Call Sub_Eliminar
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Sub_Grabar()
Dim lTipo_Contraparte As String
Dim lFila As Long
Dim lId_Contraparte As String
Dim lRollback As Boolean
'Dim lcTipo_Cambio As Class_Tipo_Cambios
Dim lcTipo_Cambio As Object

  If Not Validar_Datos Then
    Exit Sub
  End If

  lRollback = False
  gDB.IniciarTransaccion
  
'  Set lcTipo_Cambio = New Class_Tipo_Cambios
  Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
  With lcTipo_Cambio
    For lFila = 1 To Grilla.Rows - 1
      Rem Graba los datos en Tipo_Cambios
      .Campo("id_Tipo_Cambio").Valor = GetCell(Grilla, lFila, "colum_pk")
      .Campo("dsc_Tipo_Cambio").Valor = GetCell(Grilla, lFila, "DSC_TIPO_CAMBIO")
      .Campo("abr_tipo_cambio").Valor = GetCell(Grilla, lFila, "ABR_TIPO_CAMBIO")
      .Campo("id_moneda_fuente").Valor = GetCell(Grilla, lFila, "ID_MONEDA_FUENTE")
      .Campo("id_moneda_destino").Valor = GetCell(Grilla, lFila, "ID_MONEDA_DESTINO")
      .Campo("flg_tipo_cambio").Valor = GetCell(Grilla, lFila, "FLG_TIPO_CAMBIO")
      If Not .Guardar Then
        MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        lRollback = True
      End If
    Next
  End With
  Set lcTipo_Cambio = Nothing
  
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
    Call Sub_CargarDatos
  End If
  
End Sub

Private Function Validar_Datos() As Boolean
Dim lFila As Long

  Validar_Datos = True
  With Grilla
    If .Rows > 1 Then
      For lFila = 1 To .Rows - 1
        If GetCell(Grilla, lFila, "DSC_TIPO_CAMBIO") = "" Then
          MsgBox "La Descripci�n del Tipo de Cambio no puede ser vac�o.", vbExclamation, Me.Caption
          Validar_Datos = False
        ElseIf GetCell(Grilla, lFila, "ABR_TIPO_CAMBIO") = "" Then
          MsgBox "El C�digo del Tipo de Cambio no puede ser vac�o.", vbExclamation, Me.Caption
          Validar_Datos = False
        ElseIf GetCell(Grilla, lFila, "ID_MONEDA_FUENTE") = "" Then
          MsgBox "La Moneda Fuente del Tipo de Cambio no puede ser vac�a.", vbExclamation, Me.Caption
          Validar_Datos = False
        ElseIf GetCell(Grilla, lFila, "ID_MONEDA_DESTINO") = "" Then
          MsgBox "La Moneda Destino del Tipo de Cambio no puede ser vac�a.", vbExclamation, Me.Caption
          Validar_Datos = False
        ElseIf GetCell(Grilla, lFila, "FLG_TIPO_CAMBIO") = "" Then
          MsgBox "La Operaci�n del Tipo de Cambio no puede ser vac�a.", vbExclamation, Me.Caption
          Validar_Datos = False
        ElseIf GetCell(Grilla, lFila, "ID_MONEDA_FUENTE") = GetCell(Grilla, lFila, "ID_MONEDA_DESTINO") Then
          MsgBox "Las Monedas de Fuente y Destino no pueden ser iguales en '" & GetCell(Grilla, lFila, "DSC_TIPO_CAMBIO") & "'.", vbExclamation, Me.Caption
          Validar_Datos = False
        End If
      Next
    Else
      Validar_Datos = False
    End If
  End With

End Function

Private Sub Sub_CargarDatos()
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
'Dim lcTipo_Cambio As Class_Tipo_Cambios
Dim lcTipo_Cambio As Object

  Call Sub_Bloquea_Puntero(Me)

  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Grilla.Rows = 1
  
'  Set lcTipo_Cambio = New Class_Tipo_Cambios
  Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
  With lcTipo_Cambio
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla.Rows
        Call Grilla.AddItem("")
        Call SetCell(Grilla, lLinea, "colum_pk", lReg("ID_TIPO_CAMBIO").Value)
        Call SetCell(Grilla, lLinea, "DSC_TIPO_CAMBIO", lReg("DSC_TIPO_CAMBIO").Value)
        Call SetCell(Grilla, lLinea, "ABR_TIPO_CAMBIO", lReg("ABR_TIPO_CAMBIO").Value)
        Call SetCell(Grilla, lLinea, "ID_MONEDA_FUENTE", NVL(lReg("ID_MONEDA_FUENTE").Value, ""))
        Call SetCell(Grilla, lLinea, "ID_MONEDA_DESTINO", NVL(lReg("ID_MONEDA_DESTINO").Value, ""))
        Call SetCell(Grilla, lLinea, "FLG_TIPO_CAMBIO", NVL(lReg("FLG_TIPO_CAMBIO").Value, ""))
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcTipo_Cambio = Nothing
  
  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_CargaForm()
Dim lComboList As String
Dim lOperacion As String
Dim lReg As hFields
'Dim lcMoneda As Class_Monedas
Dim lcMoneda As Object

  Call Sub_Bloquea_Puntero(Me)


  Call Sub_FormControl_Color(Me.Controls)

  'Limpia la grilla
  Grilla.Rows = 1
  
'  Set lcMoneda = New Class_Monedas
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    If .Buscar Then
      lComboList = ""
      For Each lReg In .Cursor
        lComboList = lComboList & "|#" & lReg("id_moneda").Value
        
        If lReg.Index = 1 Then
          lComboList = lComboList & "*1"
        End If
        
        lComboList = lComboList & ";" & lReg("cod_moneda").Value & vbTab & lReg("dsc_moneda").Value
      Next
    End If
  End With
  Set lcMoneda = Nothing
  
  Grilla.ColComboList(Grilla.ColIndex("ID_MONEDA_FUENTE")) = lComboList
  Grilla.ColComboList(Grilla.ColIndex("ID_MONEDA_DESTINO")) = lComboList
  
  lOperacion = "|#M;Multiplicaci�n|#D;Divisi�n"
  Grilla.ColComboList(Grilla.ColIndex("FLG_TIPO_CAMBIO")) = lOperacion
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Agregar()
Dim lLinea As Long

  lLinea = Grilla.Rows
  Grilla.AddItem ""
  SetCell Grilla, lLinea, "colum_pk", 0
  SetCell Grilla, lLinea, "DSC_TIPO_CAMBIO", ""
  SetCell Grilla, lLinea, "ABR_TIPO_CAMBIO", ""
  
  Call Grilla.ShowCell(lLinea, Grilla.ColIndex("colum_pk"))
  
End Sub

Private Sub Sub_Eliminar()
Dim lID As String
'Dim lcTipo_Cambio As Class_Tipo_Cambios
Dim lcTipo_Cambio As Object

  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      lID = GetCell(Grilla, Grilla.Row, "colum_pk")
      If Not lID = "" Then
'        Set lcTipo_Cambio = New Class_Tipo_Cambios
        Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
        With lcTipo_Cambio
          .Campo("ID_TIPO_CAMBIO").Valor = lID
          If .Borrar Then
            MsgBox "Tipo Cambio Eliminado correctamente.", vbInformation + vbOKOnly, Me.Caption
          Else
            If .Errnum = 440 Then
              MsgBox "El Tipo de Cambio no puede ser eliminado, ya que est� asociado a otros componentes.", vbCritical, Me.Caption
            Else
              MsgBox .ErrMsg, vbCritical, Me.Caption
            End If
          End If
        End With
        Set lcTipo_Cambio = Nothing
      End If
      Call Sub_CargarDatos
    End If
  End If
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Listado de Tipos de Cambios" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

