VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Rebalanceo_Manual 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte de Rebalanceo Manual"
   ClientHeight    =   2100
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9465
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2100
   ScaleWidth      =   9465
   Begin VB.Frame frm_busqueda 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1695
      Left            =   60
      TabIndex        =   0
      Top             =   360
      Width           =   9345
      Begin MSComCtl2.DTPicker DTP_Fecha 
         Height          =   345
         Left            =   1250
         TabIndex        =   1
         Top             =   360
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   58392577
         CurrentDate     =   37732
      End
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   6120
         TabIndex        =   2
         Top             =   1170
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2858
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Reporte"
               Key             =   "REPORTE"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               Object.ToolTipText     =   "Genera el Reporte de Productos y Activos"
            EndProperty
         EndProperty
      End
      Begin TrueDBList80.TDBCombo Cmb_Asesor 
         Height          =   345
         Left            =   1245
         TabIndex        =   3
         Tag             =   "OBLI=S;CAPTION=Asesor"
         Top             =   770
         Width           =   4245
         _ExtentX        =   7488
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Rebalanceo_Manual.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Instrumento 
         Height          =   345
         Left            =   1250
         TabIndex        =   8
         Tag             =   "OBLI=S;CAPTION=Cuentas"
         Top             =   1170
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Rebalanceo_Manual.frx":00AA
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label lblInstrumento 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Instrumento"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   9
         Top             =   1170
         Width           =   1065
      End
      Begin VB.Label Lbl_Fecha 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         Height          =   330
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   1065
      End
      Begin VB.Label lbl_asesor 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Asesor"
         Height          =   330
         Left            =   120
         TabIndex        =   4
         Top             =   770
         Width           =   1065
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   9465
      _ExtentX        =   16695
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   7
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Rebalanceo_Manual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public fApp_Excel As Excel.Application      ' Excel application
Public fLibro     As Excel.Workbook      ' Excel workbook
Dim fCursor_Datos   As hRecord
Dim fFilaExcel      As Long
Dim fDecimales      As Integer
Dim fCod_Producto   As String
Dim fFecha As Date

Const cFormato2Dec = "#,##0.00"
Const cFormato4Dec = "#,##0.0000"
Const cFormato0Dec = "#,##0"

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("EXIT").Image = cBoton_Salir
    .Buttons("REFRESH").Image = cBoton_Refrescar
  End With
  
  With Toolbar_Verificacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORTE").Image = cBoton_Modificar
  End With
    
  Call Sub_CargaForm
  Call Sub_Limpiar
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre

    Call Sub_CargaCombo_Asesor(Cmb_Asesor, , True)
    Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento, pTag:="Cod_Producto", pTodos:=True)
    
    Set lCierre = New Class_Verificaciones_Cierre
    fFecha = lCierre.Busca_Ultima_FechaCierre
    fCod_Producto = "Todos"
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Call Sub_Limpiar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REPORTE"
      Call Sub_GeneraInformeExcel
      ' Sub_Generar_Reporte
  End Select
End Sub

Private Sub Sub_Limpiar()
  
    Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
    Call Sub_ComboSelectedItem(Cmb_Instrumento, cCmbKALL)
    
    DTP_Fecha.Value = fFecha
    fCod_Producto = "Todos"
End Sub

Private Sub Sub_GeneraInformeExcel()
    
  Call Sub_Bloquea_Puntero(Me)
  
  fCod_Producto = Fnt_ObtieneCodProducto
  If Fnt_CargarDatos() Then
    Call Sub_CreaExcel
    Call Sub_CargaExcel_Detalle
  Else
    GoTo ExitProcedure
  End If
  
  fApp_Excel.Visible = True
  fApp_Excel.UserControl = True
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CreaExcel()
Dim lHoja           As Integer
Dim lIndex          As Integer
Dim lTitulo_Detalle As String
Dim lTitulo_Fecha   As String
Dim lItem_Detalle   As String

Const cTama�o_Titulo = 12
Const cTama�o_Fecha = 11

  Call Sub_Bloquea_Puntero(Me)
    
  lHoja = IIf(fCod_Producto = "Todos", 3, 1)
  
  
  Set fApp_Excel = CreateObject("Excel.application")
  fApp_Excel.DisplayAlerts = False
  Set fLibro = fApp_Excel.Workbooks.Add
  
  fApp_Excel.ActiveWindow.DisplayGridlines = False
  fLibro.Worksheets.Add
  
    With fLibro
  
        For lIndex = .Worksheets.Count To lHoja + 1 Step -1
          .Worksheets(lIndex).Delete
        Next lIndex
      
        For lIndex = 1 To lHoja
            .Sheets(lIndex).Select
            fApp_Excel.ActiveWindow.DisplayGridlines = False
            .ActiveSheet.Range("A5:E6").Font.Bold = True
            .ActiveSheet.Range("A5:E6").HorizontalAlignment = xlLeft
            lTitulo_Detalle = "Informe de datos para Rebalanceo Manual"
            lTitulo_Fecha = "Fecha Consulta: " & DTP_Fecha.Value
            
            .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).Select
            .ActiveSheet.Range("A5").Value = lTitulo_Detalle
            .ActiveSheet.Range("A5").Font.Size = cTama�o_Titulo
            .ActiveSheet.Range("A6").Value = lTitulo_Fecha
            .ActiveSheet.Range("A6").Font.Size = cTama�o_Fecha
            .ActiveSheet.Range("A5:J5").Merge
            .ActiveSheet.Range("A6:J6").Merge
            Select Case lIndex
                Case 1
                    Select Case fCod_Producto
                        Case gcPROD_RV_NAC, "Todos"
                            lItem_Detalle = "Renta Variable"
                            .Worksheets.Item(lIndex).Columns("K:K").ColumnWidth = 15
                            .Worksheets.Item(lIndex).Columns("K:K").HorizontalAlignment = xlCenter
                        Case gcPROD_RF_NAC
                            lItem_Detalle = "Renta Fija"
                            .Worksheets.Item(lIndex).Columns("K:K").ColumnWidth = 24
                            .Worksheets.Item(lIndex).Columns("K:K").HorizontalAlignment = xlRight
                        Case gcPROD_FFMM_NAC
                            lItem_Detalle = "Fondos Mutuos"
                            .Worksheets.Item(lIndex).Columns("K:K").ColumnWidth = 15
                            .Worksheets.Item(lIndex).Columns("K:K").HorizontalAlignment = xlCenter
                    End Select
                    .Worksheets.Item(lIndex).Name = lItem_Detalle
                Case 2
                    lItem_Detalle = "Renta Fija"
                    .Worksheets.Item(lIndex).Name = lItem_Detalle
                    .Worksheets.Item(lIndex).Columns("K:K").ColumnWidth = 24
                    .Worksheets.Item(lIndex).Columns("K:K").HorizontalAlignment = xlRight
                Case 3
                    lItem_Detalle = "Fondos Mutuos"
                    .Worksheets.Item(lIndex).Name = lItem_Detalle
                    .Worksheets.Item(lIndex).Columns("K:K").ColumnWidth = 15
                    .Worksheets.Item(lIndex).Columns("K:K").HorizontalAlignment = xlCenter
            End Select
            .Worksheets.Item(lIndex).Columns("A:A").ColumnWidth = 10
            .Worksheets.Item(lIndex).Columns("B:B").ColumnWidth = 25
            .Worksheets.Item(lIndex).Columns("C:C").ColumnWidth = 25
            .Worksheets.Item(lIndex).Columns("D:D").ColumnWidth = 27
            .Worksheets.Item(lIndex).Columns("E:E").ColumnWidth = 15
            .Worksheets.Item(lIndex).Columns("F:F").ColumnWidth = 24
            .Worksheets.Item(lIndex).Columns("G:G").ColumnWidth = 24
            .Worksheets.Item(lIndex).Columns("H:H").ColumnWidth = 24
            .Worksheets.Item(lIndex).Columns("I:I").ColumnWidth = 24
            .Worksheets.Item(lIndex).Columns("A:A").HorizontalAlignment = xlRight
            .Worksheets.Item(lIndex).Columns("B:B").HorizontalAlignment = xlLeft
            .Worksheets.Item(lIndex).Columns("C:C").HorizontalAlignment = xlLeft
            .Worksheets.Item(lIndex).Columns("D:D").HorizontalAlignment = xlLeft
            .Worksheets.Item(lIndex).Columns("E:E").HorizontalAlignment = xlCenter
            .Worksheets.Item(lIndex).Columns("F:F").HorizontalAlignment = xlRight
            .Worksheets.Item(lIndex).Columns("G:G").HorizontalAlignment = xlRight
            .Worksheets.Item(lIndex).Columns("H:H").HorizontalAlignment = xlRight
            .Worksheets.Item(lIndex).Columns("I:I").HorizontalAlignment = xlRight
            .Worksheets.Item(lIndex).Columns("J:J").HorizontalAlignment = xlRight
            
        Next
            
  End With
    
End Sub

Private Sub Sub_CargaExcel_Detalle()
Dim lxHoja          As Worksheet
Dim lcChart         As Excel.Chart
Dim lCol_Grilla     As Integer
Dim lReg            As hFields
'---------------------------------
Dim lCol            As Integer
Dim lFila           As Long
Dim lMonto          As Double
Dim lIndex          As Integer
'---------------------------------
Dim lCursor_2       As hRecord
Dim lReg_2          As hFields
'---------------------------------
Dim sproducto       As String
Dim lFilaRV         As Long
Dim lFilaRF         As Long
Dim lFilaFM         As Long


  lFilaRV = 8
  lFilaRF = 8
  lFilaFM = 8

    Select Case fCod_Producto
      Case "Todos"
          Call Sub_ExcelEncabezado(1, gcPROD_RV_NAC)
          Call Sub_ExcelEncabezado(2, gcPROD_RF_NAC)
          Call Sub_ExcelEncabezado(3, gcPROD_FFMM_NAC)
      Case "RV_NAC"
          Call Sub_ExcelEncabezado(1, gcPROD_RV_NAC)
      Case "RF_NAC"
          Call Sub_ExcelEncabezado(1, gcPROD_RF_NAC)
      Case "FFMM_NAC"
          Call Sub_ExcelEncabezado(1, gcPROD_FFMM_NAC)
    End Select
  
    For Each lReg In fCursor_Datos
        fFilaExcel = fFilaExcel + 1
        sproducto = lReg("cod_producto").Value
        Select Case sproducto
            Case gcPROD_RV_NAC
                Set lxHoja = fLibro.Sheets(1)
                lFilaRV = lFilaRV + 1
                fFilaExcel = lFilaRV
            Case gcPROD_RF_NAC
                If fCod_Producto = gcPROD_RF_NAC Then
                    Set lxHoja = fLibro.Sheets(1)
                Else
                    Set lxHoja = fLibro.Sheets(2)
                End If
                lFilaRF = lFilaRF + 1
                fFilaExcel = lFilaRF
            Case gcPROD_FFMM_NAC
                If fCod_Producto = gcPROD_FFMM_NAC Then
                    Set lxHoja = fLibro.Sheets(1)
                Else
                    Set lxHoja = fLibro.Sheets(3)
                End If
                lFilaFM = lFilaFM + 1
                fFilaExcel = lFilaFM
        End Select
        With lxHoja

        
            .Cells(fFilaExcel, 1) = lReg("NUM_CUENTA").Value
            .Cells(fFilaExcel, 2) = lReg("DSC_CUENTA").Value
            .Cells(fFilaExcel, 3) = lReg("NOMBRE_ASESOR").Value
            .Cells(fFilaExcel, 4) = lReg("NEMOTECNICO").Value
            
            .Cells(fFilaExcel, 5).Value = lReg("SIMBOLO_MONEDA_CUENTA").Value
            
            Select Case sproducto
                Case gcPROD_RV_NAC
                    .Cells(fFilaExcel, 6).Value = lReg("CANTIDAD").Value
                    .Cells(fFilaExcel, 6).NumberFormat = cFormato0Dec
                    .Cells(fFilaExcel, 7).Value = lReg("PRECIO_COMPRA").Value
                    .Cells(fFilaExcel, 7).NumberFormat = cFormato4Dec
                    
                    'lMonto = NVL(lReg("CANTIDAD").Value, 0) * NVL(lReg("PRECIO_COMPRA").Value, 0)
                    lMonto = NVL(lReg("monto_promedio_compra").Value, 0)
                    .Cells(fFilaExcel, 8).Value = lMonto
                    .Cells(fFilaExcel, 8).NumberFormat = Fnt_Formato_Moneda(lReg("MONEDA_CUENTA").Value)
                    
                    .Cells(fFilaExcel, 9).Value = lReg("PRECIO").Value
                    .Cells(fFilaExcel, 9).NumberFormat = cFormato4Dec
                    .Cells(fFilaExcel, 10).Value = lReg("MONTO_MON_CTA").Value
                    .Cells(fFilaExcel, 10).NumberFormat = Fnt_Formato_Moneda(lReg("MONEDA_CUENTA").Value)
                    .Cells(fFilaExcel, 11).Value = lReg("RENTABILIDAD").Value 'FormatNumber(lReg("RENTABILIDAD").Value, 2)
                    .Cells(fFilaExcel, 11).NumberFormat = cFormato2Dec
                Case gcPROD_RF_NAC
                    .Cells(fFilaExcel, 6).Value = lReg("CANTIDAD").Value
                    .Cells(fFilaExcel, 6).NumberFormat = cFormato4Dec
                    .Cells(fFilaExcel, 7).Value = lReg("TASA_EMISION").Value
                    .Cells(fFilaExcel, 7).NumberFormat = cFormato4Dec
                    .Cells(fFilaExcel, 8).Value = lReg("TASA_COMPRA").Value
                    .Cells(fFilaExcel, 8).NumberFormat = cFormato4Dec
                    .Cells(fFilaExcel, 9).Value = lReg("TASA").Value
                    .Cells(fFilaExcel, 9).NumberFormat = cFormato4Dec
                    
                    .Cells(fFilaExcel, 10).Value = lReg("MONTO_VALOR_COMPRA").Value
                    .Cells(fFilaExcel, 10).NumberFormat = Fnt_Formato_Moneda(lReg("MONEDA_CUENTA").Value)
                    
                    .Cells(fFilaExcel, 11).Value = lReg("MONTO_MON_CTA").Value
                    .Cells(fFilaExcel, 11).NumberFormat = Fnt_Formato_Moneda(lReg("MONEDA_CUENTA").Value)
                Case gcPROD_FFMM_NAC
                    .Cells(fFilaExcel, 6).Value = lReg("CANTIDAD").Value
                    .Cells(fFilaExcel, 6).NumberFormat = cFormato4Dec
                    .Cells(fFilaExcel, 7).Value = lReg("PRECIO_COMPRA").Value
                    .Cells(fFilaExcel, 7).NumberFormat = cFormato4Dec
                    
                    'lMonto = NVL(lReg("CANTIDAD").Value, 0) * NVL(lReg("PRECIO_COMPRA").Value, 0)
                    lMonto = NVL(lReg("monto_promedio_compra").Value, 0)
                    .Cells(fFilaExcel, 8).Value = lMonto
                    .Cells(fFilaExcel, 8).NumberFormat = Fnt_Formato_Moneda(lReg("MONEDA_CUENTA").Value)
                    
                    .Cells(fFilaExcel, 9).Value = lReg("PRECIO").Value
                    .Cells(fFilaExcel, 9).NumberFormat = cFormato4Dec
                    .Cells(fFilaExcel, 10).Value = lReg("MONTO_MON_CTA").Value
                    .Cells(fFilaExcel, 10).NumberFormat = Fnt_Formato_Moneda(lReg("MONEDA_CUENTA").Value)
                    .Cells(fFilaExcel, 11).Value = lReg("RENTABILIDAD").Value 'FormatNumber(lReg("RENTABILIDAD").Value, 2)
                    .Cells(fFilaExcel, 11).NumberFormat = cFormato2Dec
            End Select
        End With
        
        
'
'        If IsNull(lReg("MONTO_VALOR_COMPRA").Value) Then
'            .Cells(fFilaExcel, 8).Value = (lReg("CANTIDAD").Value * lReg("PRECIO_COMPRA").Value)
'        Else
'            .Cells(fFilaExcel, 8).Value = lReg("MONTO_VALOR_COMPRA").Value
'        End If
'            .Cells(fFilaExcel, 8).NumberFormat = Fnt_Formato_Moneda(1)
'
'
'        If IsNull(lReg("PRECIO").Value) Then
'            .Cells(fFilaExcel, 9).Value = lReg("TASA").Value
'        Else
'            .Cells(fFilaExcel, 9).Value = lReg("PRECIO").Value
'        End If
'        .Cells(fFilaExcel, 9).NumberFormat = Fnt_Formato_Moneda(4)
'
'        .Cells(fFilaExcel, 10).Value = lReg("MONTO_MON_CTA").Value
'        .Cells(fFilaExcel, 10).NumberFormat = Fnt_Formato_Moneda(1)
'
'        If (lReg("COD_INSTRUMENTO").Value) = "BONOS_NAC" Or (lReg("COD_INSTRUMENTO").Value) = "PACTOS_NAC" Then
'            .Cells(fFilaExcel, 11) = "N/A"
'        Else
'            .Cells(fFilaExcel, 11).Value = lReg("RENTABILIDAD").Value
'            .Cells(fFilaExcel, 11).NumberFormat = Fnt_Formato_Moneda(2)
'        End If
    Next
    
    
  fLibro.ActiveSheet.Columns("A:A").Select
  fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
  fApp_Excel.Selection.EntireColumn.AutoFit
      
End Sub

Private Sub Sub_ExcelEncabezado(pHoja As Integer, sproducto As String)
Dim lxHoja      As Worksheet


    Set lxHoja = fLibro.Sheets(pHoja)
    Call lxHoja.Select
    fFilaExcel = 8
    With lxHoja
        Select Case sproducto
            Case gcPROD_RV_NAC
                .Cells(fFilaExcel, 1) = "Cuenta"
                .Cells(fFilaExcel, 2) = "Nombre Corto"
                .Cells(fFilaExcel, 3) = "Ejecutivo"
                .Cells(fFilaExcel, 4) = "Nemot�cnico"
                .Cells(fFilaExcel, 5) = "Moneda Cuenta"
                .Cells(fFilaExcel, 6) = "Cantidad"
                .Cells(fFilaExcel, 7) = "Precio Promedio Compra"
                .Cells(fFilaExcel, 8) = "Monto Promedio Compra"
                .Cells(fFilaExcel, 9) = "Precio Actual"
                .Cells(fFilaExcel, 10) = "Valor Mercado"
                .Cells(fFilaExcel, 11) = "% Rentabilidad"
                
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).BorderAround
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).Borders.Color = RGB(0, 0, 0)
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).Interior.Color = RGB(255, 255, 0)   '&HC0FFC0
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).Font.Bold = True
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).HorizontalAlignment = xlCenter
            Case gcPROD_RF_NAC
                .Cells(fFilaExcel, 1) = "Cuenta"
                .Cells(fFilaExcel, 2) = "Nombre Corto"
                .Cells(fFilaExcel, 3) = "Ejecutivo"
                .Cells(fFilaExcel, 4) = "Nemot�cnico"
                .Cells(fFilaExcel, 5) = "Moneda Cuenta"
                .Cells(fFilaExcel, 6) = "Cantidad"
                .Cells(fFilaExcel, 7) = "Tasa Cup�n"
                .Cells(fFilaExcel, 8) = "Tasa Compra"
                .Cells(fFilaExcel, 9) = "Tasa Mercado"
                .Cells(fFilaExcel, 10) = "Monto Promedio Compra"
                .Cells(fFilaExcel, 11) = "Valorizaci�n"
                '.Cells(fFilaExcel, 12) = "% Rentabilidad"
                
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).BorderAround
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).Borders.Color = RGB(0, 0, 0)
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).Interior.Color = RGB(255, 255, 0)   '&HC0FFC0
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).Font.Bold = True
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).HorizontalAlignment = xlCenter
            Case gcPROD_FFMM_NAC
                .Cells(fFilaExcel, 1) = "Cuenta"
                .Cells(fFilaExcel, 2) = "Nombre Corto"
                .Cells(fFilaExcel, 3) = "Ejecutivo"
                .Cells(fFilaExcel, 4) = "Nemot�cnico"
                .Cells(fFilaExcel, 5) = "Moneda Cuenta"
                .Cells(fFilaExcel, 6) = "Cantidad de Cuotas"
                .Cells(fFilaExcel, 7) = "Valor Cuota Promedio Compra"
                .Cells(fFilaExcel, 8) = "Monto Promedio Compra"
                .Cells(fFilaExcel, 9) = "Valor Cuota Actual"
                .Cells(fFilaExcel, 10) = "Valor Actual"
                .Cells(fFilaExcel, 11) = "% Rentabilidad"
                
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).BorderAround
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).Borders.Color = RGB(0, 0, 0)
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).Interior.Color = RGB(255, 255, 0)   '&HC0FFC0
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).Font.Bold = True
                .Range(.Cells(fFilaExcel, 1), .Cells(fFilaExcel, 11)).HorizontalAlignment = xlCenter
        End Select
    End With

End Sub


Private Function Fnt_CargarDatos() As Boolean
Dim lId_Asesor        As String
Dim lId_Instrumento   As String
Dim lCargaExitosa     As Boolean


  lCargaExitosa = False

  lId_Asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesor) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Asesor))
  lId_Instrumento = IIf(Fnt_ComboSelected_KEY(Cmb_Instrumento) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Instrumento))
  
  
    gDB.Parametros.Clear
    gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pid_empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    gDB.Parametros.Add "pfecha_cierre", ePT_Fecha, DTP_Fecha.Value, ePD_Entrada
    If lId_Asesor <> "" Then
        gDB.Parametros.Add "pid_asesor", ePT_Numero, lId_Asesor, ePD_Entrada
    End If
    If lId_Instrumento <> "" Then
        gDB.Parametros.Add "pId_instrumento", ePT_Caracter, lId_Instrumento, ePD_Entrada
    End If
    gDB.Procedimiento = "PKG_SALDOS_ACTIVOS$Busca_x_Asesor_Instrumento"

    If gDB.EjecutaSP Then
        Set fCursor_Datos = gDB.Parametros("pcursor").Valor
    Else
        MsgBox "Error al cargar datos." & vbCr & vbCr & gDB.ErrMsg, vbInformation, Me.Caption
        GoTo ExitProcedure
    End If
  
    If fCursor_Datos.Count > 0 Then
        lCargaExitosa = True
    Else
        MsgBox "No se registran Saldos Activos en esta fecha.", vbInformation, Me.Caption
    End If
    
  
  
ExitProcedure:
  Fnt_CargarDatos = lCargaExitosa
End Function

Private Function Fnt_ObtieneCodProducto() As String
Dim lcod_producto     As String
Dim lId_Instrumento   As String
Dim lInstrumentos As Class_Instrumentos
Dim lReg As hCollection.hFields

    
    If Cmb_Instrumento.Text = "Todos" Then
        lcod_producto = "Todos"
    Else
        lId_Instrumento = IIf(Fnt_ComboSelected_KEY(Cmb_Instrumento) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Instrumento))
        Set lInstrumentos = New Class_Instrumentos
        With lInstrumentos
            .Campo("cod_instrumento").Valor = lId_Instrumento
            If .Buscar Then
                For Each lReg In .Cursor
                    lcod_producto = DLL_COMUN.NVL(lReg("COD_PRODUCTO").Value, "")
                Next
            End If
        End With
    End If
    Set lInstrumentos = Nothing
    Set lReg = Nothing
  
    Fnt_ObtieneCodProducto = lcod_producto
End Function

