VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_ImportaOperacionesCDS 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cargas Autom�ticas"
   ClientHeight    =   10065
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   14730
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10065
   ScaleWidth      =   14730
   Begin TabDlg.SSTab Tab_Operaciones 
      Height          =   8895
      Left            =   0
      TabIndex        =   4
      Top             =   1140
      Width           =   14715
      _ExtentX        =   25956
      _ExtentY        =   15690
      _Version        =   393216
      Tab             =   2
      TabHeight       =   520
      TabCaption(0)   =   "Importa Renta Variable"
      TabPicture(0)   =   "Frm_ImportaOperacionesCDS.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame6"
      Tab(0).Control(1)=   "Frame_3"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Importa Renta Fija"
      TabPicture(1)   =   "Frm_ImportaOperacionesCDS.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame2"
      Tab(1).Control(1)=   "Frame1"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Importar Fondos Mutuos"
      TabPicture(2)   =   "Frm_ImportaOperacionesCDS.frx":0038
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Frame3"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "Frame4"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).ControlCount=   2
      Begin VB.Frame Frame6 
         Caption         =   "Operaciones SIN Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   3255
         Left            =   -74880
         TabIndex        =   24
         Top             =   5550
         Width           =   14475
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_Sin_Cuenta_RV 
            CausesValidation=   0   'False
            Height          =   2925
            Left            =   90
            TabIndex        =   25
            Top             =   240
            Width           =   13755
            _cx             =   24262
            _cy             =   5159
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   25
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesCDS.frx":0054
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Selc_Sin_Cta 
            Height          =   660
            Left            =   13920
            TabIndex        =   26
            Top             =   510
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar2 
               Height          =   255
               Left            =   9420
               TabIndex        =   27
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
      Begin VB.Frame Frame_3 
         Caption         =   "Operaciones Con Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   5175
         Left            =   -74880
         TabIndex        =   16
         Top             =   360
         Width           =   14475
         Begin VB.Frame Frame5 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   705
            Left            =   90
            TabIndex        =   17
            Top             =   600
            Width           =   14295
            Begin VB.CommandButton Cmb_BuscaFile_RV 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   8730
               TabIndex        =   18
               ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
               Top             =   240
               Width           =   345
            End
            Begin hControl2.hTextLabel Txt_ArchivoPlano_RV 
               Height          =   315
               Left            =   120
               TabIndex        =   19
               Tag             =   "OBLI"
               Top             =   240
               Width           =   8550
               _ExtentX        =   15081
               _ExtentY        =   556
               LabelWidth      =   1200
               TextMinWidth    =   1200
               Caption         =   "Directorio"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
            End
         End
         Begin MSComctlLib.Toolbar Toolbar_Operaciones_RV 
            Height          =   330
            Left            =   90
            TabIndex        =   20
            Top             =   270
            Width           =   4935
            _ExtentX        =   8705
            _ExtentY        =   582
            ButtonWidth     =   2011
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Wrappable       =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "SAVE"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Importar"
                  Key             =   "IMPORT"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Excel"
                  Key             =   "EXCEL"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Limpiar"
                  Key             =   "REFRESH"
               EndProperty
            EndProperty
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_RV 
            CausesValidation=   0   'False
            Height          =   3675
            Left            =   90
            TabIndex        =   21
            Top             =   1380
            Width           =   13755
            _cx             =   24262
            _cy             =   6482
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   0
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   25
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesCDS.frx":0480
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Selc_Con_Cta 
            Height          =   660
            Left            =   13920
            TabIndex        =   22
            Top             =   1650
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar1 
               Height          =   255
               Left            =   9420
               TabIndex        =   23
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Operaciones Con Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4455
         Left            =   -74970
         TabIndex        =   10
         Top             =   330
         Width           =   14685
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_RF 
            CausesValidation=   0   'False
            Height          =   3675
            Left            =   60
            TabIndex        =   11
            Top             =   660
            Width           =   14595
            _cx             =   25744
            _cy             =   6482
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   31
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesCDS.frx":08A3
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   7
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Operaciones_RF 
            Height          =   330
            Left            =   60
            TabIndex        =   12
            Top             =   270
            Width           =   10995
            _ExtentX        =   19394
            _ExtentY        =   582
            ButtonWidth     =   2884
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Wrappable       =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   6
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "SAVE"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Importar"
                  Key             =   "IMPORT"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Sel. Todo"
                  Key             =   "SEL_ALL"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Anula Sel."
                  Key             =   "SEL_NOTHING"
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Limpiar"
                  Key             =   "REFRESH"
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Reporte Asesor"
                  Key             =   "REPORT"
               EndProperty
            EndProperty
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Operaciones SIN Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4035
         Left            =   -74970
         TabIndex        =   8
         Top             =   4800
         Width           =   14685
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_Sin_Cuenta_RF 
            CausesValidation=   0   'False
            Height          =   3735
            Left            =   60
            TabIndex        =   9
            Top             =   240
            Width           =   14595
            _cx             =   25744
            _cy             =   6588
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   30
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesCDS.frx":0DC1
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   7
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Operaciones SIN Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4035
         Left            =   30
         TabIndex        =   6
         Top             =   4800
         Width           =   14685
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_Sin_Cuenta_FM 
            CausesValidation=   0   'False
            Height          =   3735
            Left            =   60
            TabIndex        =   15
            Top             =   240
            Width           =   14595
            _cx             =   25744
            _cy             =   6588
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   25
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesCDS.frx":12B9
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   7
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Operaciones Con Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4455
         Left            =   30
         TabIndex        =   5
         Top             =   330
         Width           =   14685
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Operaciones_FM 
            CausesValidation=   0   'False
            Height          =   3675
            Left            =   60
            TabIndex        =   13
            Top             =   660
            Width           =   14595
            _cx             =   25744
            _cy             =   6482
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   26
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_ImportaOperacionesCDS.frx":1713
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   0   'False
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   7
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   1
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   0
            AutoSizeMouse   =   0   'False
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Operaciones_FM 
            Height          =   330
            Left            =   90
            TabIndex        =   14
            Top             =   270
            Width           =   11000
            _ExtentX        =   19394
            _ExtentY        =   582
            ButtonWidth     =   2884
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Wrappable       =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   6
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "SAVE"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Importar"
                  Key             =   "IMPORT"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Sel. Todo"
                  Key             =   "SEL_ALL"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Anula Sel."
                  Key             =   "SEL_NOTHING"
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Limpiar"
                  Key             =   "REFRESH"
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Reporte Asesor"
                  Key             =   "REPORT"
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frm_Fecha_Proceso 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   765
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   16635
      Begin MSMAPI.MAPISession MAPISession1 
         Left            =   8850
         Top             =   150
         _ExtentX        =   1005
         _ExtentY        =   1005
         _Version        =   393216
         DownloadMail    =   -1  'True
         LogonUI         =   -1  'True
         NewSession      =   0   'False
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Proceso 
         Height          =   315
         Left            =   1470
         TabIndex        =   1
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   21299201
         CurrentDate     =   37732
      End
      Begin MSMAPI.MAPIMessages MAPIMessages1 
         Left            =   9570
         Top             =   180
         _ExtentX        =   1005
         _ExtentY        =   1005
         _Version        =   393216
         AddressEditFieldCount=   1
         AddressModifiable=   0   'False
         AddressResolveUI=   0   'False
         FetchSorted     =   0   'False
         FetchUnreadOnly =   0   'False
      End
      Begin VB.Label Lbl_Fecha_Proceso 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Proceso"
         Height          =   345
         Left            =   210
         TabIndex        =   2
         Top             =   270
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   14730
      _ExtentX        =   25982
      _ExtentY        =   635
      ButtonWidth     =   1429
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   11040
         TabIndex        =   7
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_ImportaOperacionesCDS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim lcAlias As Object

Dim fMov_Fact_RV As Object

Dim lid_Empresa As Double

Const cTipOp_Compra = "C"
Const cTipOp_Venta = "V"

Const cProc_Componente_RV = "CARGA_FACT_RV"

Public Sub Sub_Mostrar()

  If Not Fnt_CargarDatos() Then
    Unload Me
    Exit Sub
  End If

  Me.Top = 1
  Me.Left = 1
  Me.Show

End Sub

Private Function Fnt_CargarDatos() As Boolean
Dim lcProceso_Componente  As Class_Proceso_Componente
Dim lMensaje              As String

On Error GoTo ErrProcedure

  Fnt_CargarDatos = False
  
  Set fMov_Fact_RV = Nothing
  
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = cProc_Componente_RV
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set fMov_Fact_RV = .IniciaClass(lMensaje)
    
    If fMov_Fact_RV Is Nothing Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                      , lMensaje _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  Set lcProceso_Componente = Nothing
  
  Fnt_CargarDatos = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                    , "Problemas en la carga de un Proceso Componente." _
                    , Err.Description)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  
End Function

Private Sub Cmb_BuscaFile_RV_Click()
  If fMov_Fact_RV.Fnt_Busca_Archivo_Plano Then
    Txt_ArchivoPlano_RV.Text = fMov_Fact_RV.fPath
  End If
End Sub

Private Sub Form_Load()

  Call PubPrdObtienePosicion(Me)
    
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Operaciones_RV
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("IMPORT").Image = cBoton_Aceptar
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("EXCEL").Image = cBoton_Excel
      .Buttons("SAVE").Enabled = False
      .Buttons("REFRESH").Enabled = False
      .Buttons("EXCEL").Enabled = False
      .Appearance = ccFlat
  End With
  
  With Toolbar_Selc_Con_Cta
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
  End With
  
  With Toolbar_Selc_Sin_Cta
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
  End With
  
  With Toolbar_Operaciones_RF
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("IMPORT").Image = cBoton_Aceptar
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
      .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
      .Buttons("REPORT").Image = cBoton_Pdf
      .Buttons("SAVE").Enabled = False
      .Buttons("REFRESH").Enabled = False
      .Buttons("SEL_ALL").Enabled = False
      .Buttons("SEL_NOTHING").Enabled = False
      .Buttons("REPORT").Enabled = False
      .Appearance = ccFlat
  End With
  
  With Toolbar_Operaciones_FM
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("IMPORT").Image = cBoton_Aceptar
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
      .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
      .Buttons("REPORT").Image = cBoton_Pdf
      .Buttons("SAVE").Enabled = False
      .Buttons("REFRESH").Enabled = False
      .Buttons("SEL_ALL").Enabled = False
      .Buttons("SEL_NOTHING").Enabled = False
      .Buttons("REPORT").Enabled = False
      .Appearance = ccFlat
  End With
  
  'La fecha no esta en el procedimiento limpiar objecto
  DTP_Fecha_Proceso.Value = Fnt_FechaServidor
  
  Call Sub_Limpia_Objetos
  Tab_Operaciones.Tab = 0
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call PubPrdGrabaPosicion(Me)
End Sub

Private Sub Grilla_Operaciones_FM_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_FM.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_FM_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_FM)
End Sub

Private Sub Grilla_Operaciones_FM_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_FM)
End Sub

Private Sub Grilla_Operaciones_FM_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_FM)
End Sub

Private Sub Grilla_Operaciones_FM_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_FM
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub

Private Sub Grilla_Operaciones_RF_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_RF.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_RF_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_RF)
End Sub

Private Sub Grilla_Operaciones_RF_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_RF)
End Sub

Private Sub Grilla_Operaciones_RF_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_RF)
End Sub

Private Sub Grilla_Operaciones_RF_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_RF
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub

Private Sub Grilla_Operaciones_RV_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_RV.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_RV_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden_RV(Grilla_Operaciones_RV)
End Sub

Private Sub Grilla_Operaciones_RV_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_RV)
End Sub

Private Sub Grilla_Operaciones_RV_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_RV
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_Sin_Cuenta_FM)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_Sin_Cuenta_FM
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_Sin_Cuenta_RF.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_Sin_Cuenta_RF)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_Sin_Cuenta_RF)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_Sin_Cuenta_RF)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RF_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_Sin_Cuenta_RF
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Grilla_Operaciones_Sin_Cuenta_RV.ColIndex("num_cuenta") = Col And _
     Not Grilla_Operaciones_Sin_Cuenta_RV.ColIndex("chk") = Col Then
    Cancel = True
  End If
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_Sin_Cuenta_RV, "colum_pk", False, True)
End Sub

'Private Sub Grilla_Operaciones_Sin_Cuenta_RV_Click()
'  Call Sub_Check_DesCheck_Mismo_Nro_Orden_RV(Grilla_Operaciones_Sin_Cuenta_RV)
'End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_DblClick()
  Call Sub_Mensaje_Error(Grilla_Operaciones_Sin_Cuenta_RV)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_RV_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  With Grilla_Operaciones_Sin_Cuenta_RV
    DoEvents
    If .MouseRow > 0 Then
      .ToolTipText = .TextMatrix(.MouseRow, .ColIndex("nombre_cliente"))
    Else
      .ToolTipText = ""
    End If
  End With
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Limpia_Objetos()
  'DTP_Fecha_Proceso.Value = Fnt_FechaServidor
  
  Grilla_Operaciones_RV.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_RV.Rows = 1
  Grilla_Operaciones_RF.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_RF.Rows = 1
  Grilla_Operaciones_FM.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_FM.Rows = 1
  
  BarraProceso.Value = 0
  
End Sub

Private Sub Habilita_Reporte_Asesor()
  Select Case Tab_Operaciones.Tab
  Case 0
      If Grilla_Operaciones_RV.Rows > 1 Or Grilla_Operaciones_Sin_Cuenta_RV.Rows > 1 Then
        Toolbar_Operaciones_RV.Buttons("REPORT").Enabled = True
      Else
        Toolbar_Operaciones_RV.Buttons("REPORT").Enabled = False
      End If
    
  Case 1
      If Grilla_Operaciones_RF.Rows > 1 Or Grilla_Operaciones_Sin_Cuenta_RF.Rows > 1 Then
        Toolbar_Operaciones_RF.Buttons("REPORT").Enabled = True
      Else
        Toolbar_Operaciones_RF.Buttons("REPORT").Enabled = False
      End If
  Case 2
      If Grilla_Operaciones_FM.Rows > 1 Or Grilla_Operaciones_Sin_Cuenta_FM.Rows > 1 Then
        Toolbar_Operaciones_FM.Buttons("REPORT").Enabled = True
      Else
        Toolbar_Operaciones_FM.Buttons("REPORT").Enabled = False
      End If
  End Select
End Sub

Private Sub Toolbar_Operaciones_RF_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
    Select Case Button.Key
        Case "IMPORT"
            DTP_Fecha_Proceso.Enabled = False
            Tab_Operaciones.TabEnabled(0) = False
            Tab_Operaciones.TabEnabled(2) = False
            With Toolbar_Operaciones_RF
                .Buttons("SAVE").Enabled = True
                .Buttons("IMPORT").Enabled = False
                .Buttons("SEL_ALL").Enabled = True
                .Buttons("SEL_NOTHING").Enabled = True
                .Buttons("REFRESH").Enabled = True
            End With
            
            Call Sub_Importa_RF
            Call Habilita_Reporte_Asesor
            
        Case "REFRESH"
            DTP_Fecha_Proceso.Enabled = True
            Tab_Operaciones.TabEnabled(0) = True
            Tab_Operaciones.TabEnabled(2) = True
            With Toolbar_Operaciones_RF
                .Buttons("SAVE").Enabled = False
                .Buttons("IMPORT").Enabled = True
                .Buttons("SEL_ALL").Enabled = False
                .Buttons("SEL_NOTHING").Enabled = False
                .Buttons("REFRESH").Enabled = False
                .Buttons("REPORT").Enabled = False
            End With
            Call Sub_Limpia_Objetos
        Case "SEL_ALL"
            Call Sub_CambiaCheck(Grilla_Operaciones_RF, True)
        Case "SEL_NOTHING"
            Call Sub_CambiaCheck(Grilla_Operaciones_RF, False)
        Case "SAVE"
            Toolbar_Operaciones_RF.Buttons("REPORT").Enabled = False
            Call Sub_Graba_Operaciones_con_Cta_RF
        Case "REPORT"
            Call Sub_Reporte_Asesor(Grilla_Operaciones_RF, Grilla_Operaciones_Sin_Cuenta_RF, "RF")
    End Select
End Sub

Private Sub Toolbar_Operaciones_RV_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lNombre_Hoja  As String
Dim lTitulo       As String
Dim lMsg_Error    As String

  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Call Sub_Graba_Operaciones_con_Cta_RV
        
    Case "IMPORT"
      If Fnt_Importa_RV Then
        Tab_Operaciones.TabEnabled(1) = False
        Tab_Operaciones.TabEnabled(2) = False
        With Toolbar_Operaciones_RV
          .Buttons("SAVE").Enabled = True
          .Buttons("IMPORT").Enabled = False
          .Buttons("EXCEL").Enabled = True
          .Buttons("REFRESH").Enabled = True
        End With
      End If
        
    Case "EXCEL"
      lNombre_Hoja = "Importador de Movimientos RV"
      lTitulo = "Importador de Movimientos Renta Variable"
      If Not Fnt_Excel(lNombre_Hoja, _
                       lTitulo, _
                       Txt_ArchivoPlano_RV, _
                       Grilla_Operaciones_RV, _
                       Grilla_Operaciones_Sin_Cuenta_RV, _
                       lMsg_Error) Then
        MsgBox lMsg_Error, vbCritical, Me.Caption
      End If
        
    Case "REFRESH"
      Tab_Operaciones.TabEnabled(1) = True
      Tab_Operaciones.TabEnabled(2) = True
      With Toolbar_Operaciones_RV
        .Buttons("SAVE").Enabled = False
        .Buttons("IMPORT").Enabled = True
        .Buttons("EXCEL").Enabled = False
        .Buttons("REFRESH").Enabled = False
      End With
      Call Sub_Limpia_Objetos
          
  End Select
End Sub

Private Function Fnt_Importa_RV() As Boolean
Dim lcCuenta            As Object
Dim lCampo              As hFields
Dim lTipo_Movimiento    As String
Dim lCod_Instrumento    As String
Dim lId_Nemotecnico     As String
Dim lId_Cuenta
Dim lAlias_Cta          As String
Dim lLinea_Oper_sin_cta As Long
Dim lNum_Cuenta         As String
Dim lLinea_Oper_con_cta As Long
Dim lNombre_Cliente     As String
Dim lNemotecnico        As String
Dim lDsc_Larga          As String
  
  Fnt_Importa_RV = True
  Call Sub_Bloquea_Puntero(Me)
  
  Grilla_Operaciones_RV.Rows = 1
  Grilla_Operaciones_Sin_Cuenta_RV.Rows = 1
  
  With fMov_Fact_RV
    .fPath = Txt_ArchivoPlano_RV.Text
    If Not .Carga_Facturacion_RV Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
    
    If .Cursor.Count = 0 Then
      MsgBox "No hay datos en Archivos XML.", vbInformation, Me.Caption
      GoTo ErrProcedure
    End If
    
    BarraProceso.Max = .Cursor.Count
    
    For Each lCampo In .Cursor
      BarraProceso.Value = lCampo.Index
        
      lTipo_Movimiento = ""
      Select Case lCampo("TIPO_MOVIMIENTO").Value
        Case gcTipoOperacion_Ingreso
          lTipo_Movimiento = "Ingreso"
        Case gcTipoOperacion_Egreso
          lTipo_Movimiento = "Egreso"
      End Select
      
      Rem Analiza Nemotecnico
      If Not Fnt_Valida_Nemotecnico(lCampo("NEMOTECNICO").Value, lId_Nemotecnico, lCod_Instrumento, lNemotecnico) Then
        GoTo ErrProcedure
      End If
      
      Rem Busca el Alias de la Cuenta por el "Rut Cliente"
      lAlias_Cta = NVL(lCampo("RUT_CLIENTE").Value, "")
      Set lcAlias = CreateObject(cDLL_Alias)
      Set lcAlias.gDB = gDB
      lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cTabla_PARTICIPE_FFMM _
                                           , pCodigoCSBPI:=cTabla_Cuentas _
                                           , pValor:=lAlias_Cta)
      If IsNull(lId_Cuenta) Then
        lLinea_Oper_sin_cta = Grilla_Operaciones_Sin_Cuenta_RV.Rows
        Call Grilla_Operaciones_Sin_Cuenta_RV.AddItem("")
        Grilla_Operaciones_Sin_Cuenta_RV.Cell(flexcpChecked, lLinea_Oper_sin_cta, "chk") = flexChecked
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "colum_pk", NVL(lCampo("FOLIO").Value, 0))
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "fecha", NVL(lCampo("FECHA").Value, 0))
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "rut", Trim(lCampo("RUT_CLIENTE").Value))
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "id_cuenta", "")
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "num_cuenta", "")
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "nombre_cliente", "")
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "tipo_movimiento", lTipo_Movimiento)
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "flg_tipo_movimiento", lCampo("TIPO_MOVIMIENTO").Value)
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "id_nemotecnico", lId_Nemotecnico)
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "nemotecnico", lNemotecnico)
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "Cantidad", lCampo("CANTIDAD").Value)
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "precio", lCampo("PRECIO").Value)
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "comi", NVL(lCampo("COMISION").Value, 0))
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "Dere", NVL(lCampo("DERECHO").Value, 0))
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "gast", NVL(lCampo("GASTOS").Value, 0))
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "iva", NVL(lCampo("IVA").Value, 0))
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "monto_neto", NVL(lCampo("MONTO_NETO").Value, 0))
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "monto_total", NVL(lCampo("MONTO_TOTAL").Value, 0))
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "fecha_liquidacion", NVL(lCampo("FECHA_LIQUIDACION").Value, ""))
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "cod_instru", lCod_Instrumento)
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "dsc_error", "")
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "archivo", NVL(lCampo("ARCHIVO").Value, ""))
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "dsc_corta", "")
        Call SetCell(Grilla_Operaciones_Sin_Cuenta_RV, lLinea_Oper_sin_cta, "dsc_larga", "")
      Else
        ' ----------- RESCATA NUMERO CUENTA
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        lcCuenta.Campo("id_cuenta").Valor = lId_Cuenta
        If lcCuenta.Buscar(pEnVista:=True) Then
          lNum_Cuenta = lcCuenta.Cursor(1)("num_cuenta").Value
          lNombre_Cliente = lcCuenta.Cursor(1)("nombre_cliente").Value
          lDsc_Larga = lcCuenta.Cursor(1)("descripcion_larga").Value
        End If
        Set lcCuenta = Nothing
        ' ----------- FIN RESCATA NUMERO CUENTA
        lLinea_Oper_con_cta = Grilla_Operaciones_RV.Rows
        Call Grilla_Operaciones_RV.AddItem("")
        Grilla_Operaciones_RV.Cell(flexcpChecked, lLinea_Oper_con_cta, "chk") = flexChecked
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "colum_pk", NVL(lCampo("FOLIO").Value, 0))
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "fecha", NVL(lCampo("FECHA").Value, 0))
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "rut", Trim(lCampo("RUT_CLIENTE").Value))
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "id_cuenta", lId_Cuenta)
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "num_cuenta", lNum_Cuenta)
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "nombre_cliente", lNombre_Cliente)
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "tipo_movimiento", lTipo_Movimiento)
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "flg_tipo_movimiento", lCampo("TIPO_MOVIMIENTO").Value)
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "id_nemotecnico", lId_Nemotecnico)
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "nemotecnico", lNemotecnico)
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "Cantidad", lCampo("CANTIDAD").Value)
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "precio", lCampo("PRECIO").Value)
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "comi", NVL(lCampo("COMISION").Value, 0))
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "Dere", NVL(lCampo("DERECHO").Value, 0))
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "gast", NVL(lCampo("GASTOS").Value, 0))
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "iva", NVL(lCampo("IVA").Value, 0))
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "monto_neto", NVL(lCampo("MONTO_NETO").Value, 0))
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "monto_total", NVL(lCampo("MONTO_TOTAL").Value, 0))
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "fecha_liquidacion", lCampo("FECHA_LIQUIDACION").Value)
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "cod_instru", lCod_Instrumento)
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "dsc_error", "")
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "archivo", NVL(lCampo("ARCHIVO").Value, ""))
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "dsc_corta", "")
        Call SetCell(Grilla_Operaciones_RV, lLinea_Oper_con_cta, "dsc_larga", lDsc_Larga)
      End If
    Next
  End With
  
  Call Sub_Desbloquea_Puntero(Me)
  Exit Function
  
ErrProcedure:
  Fnt_Importa_RV = False
  Call Sub_Desbloquea_Puntero(Me)
    
End Function

Private Function Fnt_Valida_Nemotecnico(pNemotecnico As String _
                                      , ByRef pId_Nemotecnico As String _
                                      , ByRef pCod_Instrumento As String _
                                      , ByRef pDsc_Nemotecnico As String) As Boolean
Dim lcNemotecnicos  As Class_Nemotecnicos
Dim lMatriz_Nemotec
Dim lIndex          As Integer
  
  Fnt_Valida_Nemotecnico = True
  
  pId_Nemotecnico = "0"
  pCod_Instrumento = ""
  pDsc_Nemotecnico = ""
  
  If pNemotecnico = "" Then
    GoTo ExitProcedure
  End If
  
  Set lcNemotecnicos = New Class_Nemotecnicos
  
  lMatriz_Nemotec = Fnt_SeparaString(pNemotecnico, " ")
  
  Rem Si hay solo elemento en pNemotecnico
  If UBound(lMatriz_Nemotec) = 0 Then
    lcNemotecnicos.Campo("nemotecnico").Valor = pNemotecnico
    If lcNemotecnicos.Buscar_Nemotecnico(pCod_Producto:=Null, pMostrar_Msg:=False) Then
      pId_Nemotecnico = lcNemotecnicos.Campo("ID_NEMOTECNICO").Valor
      If Not pId_Nemotecnico = "0" Then
        pDsc_Nemotecnico = pNemotecnico
        If lcNemotecnicos.Buscar Then
          pCod_Instrumento = NVL(lcNemotecnicos.Cursor(1)("COD_INSTRUMENTO").Value, "")
          GoTo ExitProcedure
        Else
          MsgBox lcNemotecnicos.ErrMsg, vbCritical, Me.Caption
          GoTo ErrProcedure
        End If
      End If
    Else
      MsgBox lcNemotecnicos.ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End If
  
  Rem Si hay mas elementos en pNemotecnico
  For lIndex = 0 To UBound(lMatriz_Nemotec) - 1
    lcNemotecnicos.Campo("nemotecnico").Valor = lMatriz_Nemotec(lIndex)
    If lcNemotecnicos.Buscar_Nemotecnico(pCod_Producto:=Null, pMostrar_Msg:=False) Then
      pId_Nemotecnico = lcNemotecnicos.Campo("ID_NEMOTECNICO").Valor
      If Not pId_Nemotecnico = "0" Then
        pDsc_Nemotecnico = lMatriz_Nemotec(lIndex)
        If lcNemotecnicos.Buscar Then
          pCod_Instrumento = NVL(lcNemotecnicos.Cursor(1)("COD_INSTRUMENTO").Value, "")
          GoTo ExitProcedure
        Else
          MsgBox lcNemotecnicos.ErrMsg, vbCritical, Me.Caption
          GoTo ErrProcedure
        End If
      End If
    Else
      MsgBox lcNemotecnicos.ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  Next

ErrProcedure:
  Fnt_Valida_Nemotecnico = False
  
ExitProcedure:
  Set lcNemotecnicos = Nothing

End Function

Private Sub Sub_Graba_Operaciones_con_Cta_RV()
Dim lFila       As Long
Dim lMsg_Error  As String
    
  For lFila = 1 To Grilla_Operaciones_RV.Rows - 1
    Rem Busca las operaciones chequeadas
    If Grilla_Operaciones_RV.Cell(flexcpChecked, lFila, Grilla_Operaciones_Sin_Cuenta_RV.ColIndex("chk")) = flexChecked Then
      If Not Fnt_Ope_Directa_Cont_RV(pGrilla:=Grilla_Operaciones_RV, _
                                     pFila:=lFila, _
                                     pFolio:=To_Number(To_Number(GetCell(Grilla_Operaciones_RV, lFila, "colum_pk")))) Then
        'GoTo ErrProcedure
      End If
    End If
  Next lFila
  
  For lFila = 1 To Grilla_Operaciones_Sin_Cuenta_RV.Rows - 1
    Rem Busca las operaciones chequeadas
    If Grilla_Operaciones_Sin_Cuenta_RV.Cell(flexcpChecked, lFila, Grilla_Operaciones_Sin_Cuenta_RV.ColIndex("chk")) = flexChecked Then
      If Not Fnt_Ope_Directa_Cont_RV(pGrilla:=Grilla_Operaciones_Sin_Cuenta_RV, _
                                     pFila:=lFila, _
                                     pFolio:=To_Number(To_Number(GetCell(Grilla_Operaciones_Sin_Cuenta_RV, lFila, "colum_pk")))) Then
        'GoTo ErrProcedure
      End If
    End If
  Next lFila
  
  MsgBox "Grabaci�n de Movimientos Diarios de Renta Variable finalizada.", vbInformation
  
  Tab_Operaciones.TabEnabled(1) = True
  Tab_Operaciones.TabEnabled(2) = True
  
  Call Grilla_Operaciones_RV.Select(0, 0)
  Call Grilla_Operaciones_Sin_Cuenta_RV.Select(0, 0)
  
End Sub

Private Function Fnt_Ope_Directa_Cont_RV(pGrilla As VSFlexGrid, _
                                         pFila As Long, _
                                         ByVal pFolio As Long) As Boolean
Dim lFila                 As Long
Dim lcAcciones            As Class_Acciones
Dim lId_Caja_Cuenta       As Double
Dim lId_Nemotecnico       As String
Dim lFecha_Movimiento     As Date
Dim lFecha_Liquidacion    As Date
Dim lId_Cuenta            As String
Dim lId_Moneda            As String
Dim lFecha                As Date
Dim lMsg_Error            As String
Dim lNum_Cuenta           As String
Dim lFlg_Tipo_Movimiento  As String
Dim lDsc_Corta            As String
Dim lCta_Es_FFMM          As Boolean
'---------------------------------------
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
'---------------------------------------
Dim lId_Operacion As String
Dim lcOperaciones As Class_Operaciones
Dim lDetalle As Class_Operaciones_Detalle
Dim lRollback As Boolean
Dim lMonto_Operacion As Double
'---------------------------------------
Dim lcComisiones As Class_Comisiones_Instrumentos
Dim lPorcentaje_Comision As Double
Dim lGastos As Double
Dim lComision As Double
Dim lDerecho As Double
Dim lIva As Double
'---------------------------------------
Dim lRecord_Detalle As hRecord
Dim lReg As hFields
Dim lCod_Instrumento As String
'---------------------------------------
 
  Set lRecord_Detalle = New hRecord
  With lRecord_Detalle
    .ClearFields
    .AddField "ID_NEMOTECNICO", 0
    .AddField "CANTIDAD", 0
    .AddField "PRECIO", 0
    .AddField "MONTO_DETALLE", 0
    .AddField "ID_MONEDA"
    .AddField "COMISION", 0
    .AddField "DERECHO", 0
    .AddField "GASTOS", 0
    .AddField "IVA", 0
    .AddField "MONTO_OPERACION", 0
    .LimpiarRegistros
  End With

  lRollback = False
  gDB.IniciarTransaccion
  
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("fecha_operacion").Valor = DTP_Fecha_Proceso.Value
    .Campo("cod_producto").Valor = gcPROD_RV_NAC
    If Not .Anular_Operaciones_Pendientes Then
      lRollback = True
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcOperaciones = Nothing
  
  lCta_Es_FFMM = False
  
  lFila = pFila
  lId_Cuenta = GetCell(pGrilla, pFila, "id_cuenta")
  lNum_Cuenta = GetCell(pGrilla, pFila, "num_cuenta")
  lDsc_Corta = GetCell(pGrilla, pFila, "dsc_corta")
  
  Rem Si la descripcion corta de tipo cuenta es "FFMM" entonces se realiza la operacion en una sola linea
  If lDsc_Corta = cTIPOCUENTA_FFMM Then
    lCta_Es_FFMM = True
    If Fnt_Graba_Oper_FFMM(pGrilla, lFila, pFolio, lMsg_Error) Then
      GoTo ExitProcedure
    Else
      GoTo ErrProcedure
    End If
  End If
  
  Rem Ingresa el movimiento
  Do While To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pFolio
    Rem Agrega detalles solo para los movimientos del mismo folio, la misma cuenta, q no sean FFMM
    Rem y que esten chequeados
    If pGrilla.Cell(flexcpChecked, lFila, pGrilla.ColIndex("chk")) = flexChecked And _
       GetCell(pGrilla, lFila, "id_cuenta") = lId_Cuenta And _
       Not GetCell(pGrilla, lFila, "dsc_corta") = cTIPOCUENTA_FFMM Then
       
      lId_Nemotecnico = GetCell(pGrilla, lFila, "id_nemotecnico")
      lFecha = GetCell(pGrilla, lFila, "fecha")
      '-----------------------------------------------------------------------------------------
      Rem VALIDACIONES
      lMsg_Error = ""
      Rem Valida que no se est� ingresando la operacion de nuevo
      If Not Fnt_Buscar_Rel_Nro_Oper_Detalle(cOrigen_CDS_Fact_RV, GetCell(pGrilla, lFila, "colum_pk"), lMsg_Error) Then
        lMsg_Error = "El Folio '" & GetCell(pGrilla, lFila, "colum_pk") & "' ya est� ingresado en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
        GoTo ErrProcedure
      Rem La Fecha que viene en el Archivo Plano tiene q ser igual a la Fecha Proceso
      ElseIf Not lFecha = DTP_Fecha_Proceso.Value Then
        lMsg_Error = "La Fecha '" & lFecha & "' no corresponde a la Fecha de Proceso seleccionada." & vbCr & "No se puede operar la instrucci�n."
        GoTo ErrProcedure
      Rem Si el Id_Cuenta y Num_cuenta es vacio quiere decir que no tiene asociada una cuenta en GPI
      ElseIf lId_Cuenta = "" And lNum_Cuenta = "" Then
        lMsg_Error = "La Operaci�n no tiene asociado una cuenta en el sistema. El campo ""Cuenta"" est� vacio." & vbCr & "No se puede operar la instrucci�n."
        GoTo ErrProcedure
      Rem Si el id_nemotecnico es "0" quiere decir que no se encontr� el nemotecnico en GPI
      ElseIf lId_Nemotecnico = "0" Then
        lMsg_Error = "No se reconoci� Nemot�cnico en el sistema: '" & GetCell(pGrilla, lFila, "NEMOTECNICO") & "'." & vbCr & "No se puede operar la instrucci�n."
        GoTo ErrProcedure
      Rem La Fecha de Liquidacion es vacia, no se puede operar
      ElseIf GetCell(pGrilla, lFila, "fecha_liquidacion") = "" Then
        lMsg_Error = "La Fecha de Liquidaci�n no puede estar vacia. No se puede operar la instrucci�n."
        GoTo ErrProcedure
      Rem Busca la moneda del nemotecnico en CSGPI
      ElseIf Not Fnt_Buscar_Datos_Nemo_RV(lId_Nemotecnico, lId_Moneda, lMsg_Error) Then
        GoTo ErrProcedure
      End If
      '-----------------------------------------------------------------------------
      Set lReg = lRecord_Detalle.Add
      lReg("ID_NEMOTECNICO").Value = lId_Nemotecnico
      lReg("CANTIDAD").Value = To_Number(GetCell(pGrilla, lFila, "Cantidad"))
      lReg("PRECIO").Value = To_Number(GetCell(pGrilla, lFila, "precio"))
      lReg("MONTO_DETALLE").Value = To_Number(GetCell(pGrilla, lFila, "monto_neto"))
      lReg("ID_MONEDA").Value = lId_Moneda
      lReg("COMISION").Value = To_Number(GetCell(pGrilla, lFila, "comi"))
      lReg("DERECHO").Value = To_Number(GetCell(pGrilla, lFila, "dere"))
      lReg("GASTOS").Value = To_Number(GetCell(pGrilla, lFila, "gast"))
      lReg("IVA").Value = To_Number(GetCell(pGrilla, lFila, "iva"))
      lReg("MONTO_OPERACION").Value = To_Number(GetCell(pGrilla, lFila, "monto_total"))
    End If
    
    lFila = lFila + 1
    
    If lFila > pGrilla.Rows - 1 Then
      Exit Do
    End If
    
  Loop

  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("id_moneda").Valor = lId_Moneda
    .Campo("cod_mercado").Valor = cMercado_Nacional
    If .Buscar_Caja_Para_Invertir() Then
      lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
    Else
      lMsg_Error = "Problemas en cargar Cajas de la Cuenta." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  '-----------------------------------------------------------------------------

  lFecha_Movimiento = DTP_Fecha_Proceso.Value
  lFecha_Liquidacion = GetCell(pGrilla, pFila, "FECHA_LIQUIDACION")
  lCod_Instrumento = GetCell(pGrilla, pFila, "cod_instru")
  lFlg_Tipo_Movimiento = GetCell(pGrilla, pFila, "flg_tipo_movimiento")
  
  Set lcAcciones = New Class_Acciones
  With lcAcciones
    For Each lReg In lRecord_Detalle
      Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("ID_NEMOTECNICO").Value, _
                                        pCantidad:=lReg("CANTIDAD").Value, _
                                        pPrecio:=lReg("PRECIO").Value, _
                                        pId_Moneda:=lReg("ID_MONEDA").Value, _
                                        pMonto:=lReg("MONTO_DETALLE").Value, _
                                        pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                        pPrecio_Historico:="")
      Rem No vienen prorrateados
      lComision = lReg("COMISION").Value
      lDerecho = lReg("DERECHO").Value
      lGastos = lReg("GASTOS").Value
      lIva = lReg("IVA").Value
      lMonto_Operacion = lReg("MONTO_OPERACION")
    Next
    
    Rem Comisiones
    Set lcComisiones = New Class_Comisiones_Instrumentos
    With lcComisiones
      .Campo("Id_Cuenta").Valor = lId_Cuenta
      .Campo("COD_INSTRUMENTO").Valor = lCod_Instrumento
      If .Buscar(True) Then
        If .Cursor.Count > 0 Then
          lPorcentaje_Comision = NVL(.Cursor(1)("COMISION").Value, 0)
        End If
      End If
    End With
    Set lcComisiones = Nothing
    '-----------------------------------------------------------------------------
    Rem Flag para que no tome en cuenta la cantidad de activos al ingresar una instruccion desde el Importador de movimientos
    Rem y para que ingrese en campos propios de importacion
    .fImportacion = True
    If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                          pId_Cuenta:=lId_Cuenta, _
                                          pDsc_Operacion:="", _
                                          pTipoOperacion:=lFlg_Tipo_Movimiento, _
                                          pId_Contraparte:="", _
                                          pId_Representante:="", _
                                          pId_Moneda_Operacion:=lId_Moneda, _
                                          pFecha_Operacion:=lFecha_Movimiento, _
                                          pFecha_Vigencia:=lFecha_Movimiento, _
                                          pFecha_Liquidacion:=lFecha_Liquidacion, _
                                          pId_Trader:="", _
                                          pPorc_Comision:=lPorcentaje_Comision, _
                                          pComision:=lComision, _
                                          pDerechos_Bolsa:=lDerecho, _
                                          pGastos:=lGastos, _
                                          pIva:=lIva, _
                                          pMonto_Operacion:=lMonto_Operacion, _
                                          pTipo_Precio:=cTipo_Precio_Mercado) Then
      lMsg_Error = "Problemas al grabar Acciones Nacionales." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcAcciones = Nothing
  '----------------------------------------------------------------------------

  Rem Con el nuevo id_operacion busca los detalles
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Id_Operacion").Valor = lId_Operacion
  
    Rem Busca el detalle de la operacion
    If Not .BuscaConDetalles Then
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  
    Rem Realiza la Confirmaci�n de la Instrucci�n
    If Not .Confirmar(lId_Caja_Cuenta) Then
      lMsg_Error = "Error en la confirmaci�n de la operaci�n." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  
    Rem Por cada id_operacion_detalle encontrado guarda la relacion
    Rem "nro_orden-id_operacion_detalle" en la tabla rel_conversiones
    For Each lDetalle In lcOperaciones.Detalles
      If Not Fnt_Guardar_Rel_Nro_Oper_Detalle_RV(GetCell(pGrilla, pFila, "colum_pk"), _
                                                 lDetalle.Campo("id_operacion_detalle").Valor, _
                                                 lMsg_Error) Then
        GoTo ErrProcedure
      End If
    Next
  End With
    
  lMsg_Error = "Operaci�n Ingresada correctamente." & vbCr & vbCr & "Folio: " & pFolio & vbCr & "N�mero Operaci�n CSGPI: " & lId_Operacion
  GoTo ExitProcedure
  
ErrProcedure:
  lRollback = True
  
ExitProcedure:
  lFila = pFila
  lMsg_Error = "Archivo: " & GetCell(pGrilla, lFila, "archivo") & vbCr & vbCr & lMsg_Error
    
  If lCta_Es_FFMM Then
    Call Sub_Setea_Grilla_After(pGrilla, lFila, lMsg_Error, lRollback)
  Else
    Do While To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pFolio
      If pGrilla.Cell(flexcpChecked, lFila, pGrilla.ColIndex("chk")) = flexChecked And _
         GetCell(pGrilla, lFila, "id_cuenta") = lId_Cuenta And _
         Not GetCell(pGrilla, lFila, "dsc_corta") = cTIPOCUENTA_FFMM Then
       
        Call Sub_Setea_Grilla_After(pGrilla, lFila, lMsg_Error, lRollback)
        
      End If
      
      lFila = lFila + 1
      
      If lFila > pGrilla.Rows - 1 Then
        Exit Do
      End If
    Loop
  
  End If
  
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  Set lcAcciones = Nothing
  
  Call Fnt_AgregarLog(pId_Log_SubTipo:=eLog_Subtipo.eLS_Importador_Movimientos _
                    , pCod_Estado:=cEstado_Log_Mensaje _
                    , pGls_Log_Registro:=lMsg_Error _
                    , pId_Log_Proceso:=Fnt_Agregar_Log_Proceso(eLog_Subtipo.eLS_Importador_Movimientos))
                        
  Fnt_Ope_Directa_Cont_RV = Not lRollback
  
End Function

Private Sub Sub_Setea_Grilla_After(pGrilla As VSFlexGrid, pFila As Long, pMsg_Error As String, pRollback As Boolean)
  Call SetCell(pGrilla, pFila, "dsc_error", "")
  Call SetCell(pGrilla, pFila, "dsc_error", pMsg_Error)
  pGrilla.Cell(flexcpChecked, pFila, pGrilla.ColIndex("CHK")) = flexUnchecked
  If pRollback Then
    pGrilla.Cell(flexcpForeColor, pFila, 1, pFila, pGrilla.Cols - 1) = vbRed
  Else
    pGrilla.Cell(flexcpForeColor, pFila, 1, pFila, pGrilla.Cols - 1) = vbBlue
  End If
End Sub

Private Function Fnt_Buscar_Datos_Nemo_RV(pId_Nemotecnico As String, _
                                          ByRef pId_Moneda_Nemo As String, _
                                          ByRef pMsg_Error As String) As Boolean
Dim lcAlias As Class_Alias
Dim lcNemotecnico As Class_Nemotecnicos
  
  Fnt_Buscar_Datos_Nemo_RV = True
  
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pId_Moneda_Nemo = .Cursor(1)("id_moneda_transaccion").Value
      Else
        pMsg_Error = "Nemot�cnico no encontrado en el sistema. No se puede operar la instrucci�n."
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = "Problemas en cargar datos de Nemotecnico." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcNemotecnico = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Buscar_Datos_Nemo_RV = False
End Function

Private Function Fnt_Graba_Oper_FFMM(pGrilla As VSFlexGrid, _
                                     ByRef pFila As Long, _
                                     ByVal pFolio As Long, _
                                     pMsg_Error As String) As Boolean
Dim lTipo_Mov    As String
Dim lRut_Cliente As String
  
  lTipo_Mov = GetCell(pGrilla, pFila, "flg_tipo_movimiento")
  
  lRut_Cliente = GetCell(pGrilla, pFila, "rut")
  lRut_Cliente = Left(lRut_Cliente, InStr(1, lRut_Cliente, "-") - 1)
  
  If lTipo_Mov = gcTipoOperacion_Ingreso Then
    
    With gDB
      .Parametros.Clear
      .Procedimiento = "LEASIVDB&dbo&SP_GENERA_COMPRA_RV_NACIONAL"
      
      .Parametros.Add "Fecha_Operacion", ePT_Fecha, GetCell(pGrilla, pFila, "fecha"), ePD_Entrada
      .Parametros.Add "Codigo_Fondo", ePT_Caracter, "F02", ePD_Entrada
      .Parametros.Add "Nemotecnico", ePT_Caracter, GetCell(pGrilla, pFila, "nemotecnico"), ePD_Entrada
      .Parametros.Add "Rut_Cliente", ePT_Numero, lRut_Cliente, ePD_Entrada
      .Parametros.Add "Nominal", ePT_Numero, GetCell(pGrilla, pFila, "cantidad"), ePD_Entrada
      .Parametros.Add "Sub_Tipo_Operacion", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Moneda_Reajuste", ePT_Caracter, " ", ePD_Entrada
      .Parametros.Add "Tipo_Cartera", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Fecha_Vcto_Pacto", ePT_Fecha, "", ePD_Entrada
      .Parametros.Add "Mto_Comision", ePT_Numero, 0, ePD_Entrada
      .Parametros.Add "Mto_Derechos", ePT_Numero, 0, ePD_Entrada
      .Parametros.Add "Mto_Gastos", ePT_Numero, 0, ePD_Entrada
      .Parametros.Add "Mto_Iva", ePT_Numero, 0, ePD_Entrada
      .Parametros.Add "Mto_Operacion", ePT_Numero, GetCell(pGrilla, pFila, "monto_neto"), ePD_Entrada
      .Parametros.Add "Mto_Inicio_Pacto", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Tasa_Pacto", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Mto_Vcto_Pacto", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Forma_Pago", ePT_Caracter, "CHE", ePD_Entrada
      .Parametros.Add "Forma_Pago_Vcto", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Tipo_Liquidacion", ePT_Caracter, "CN", ePD_Entrada
      .Parametros.Add "Fecha_Liquidacion", ePT_Fecha, GetCell(pGrilla, pFila, "fecha_liquidacion"), ePD_Entrada
      .Parametros.Add "Glosa_Observacion", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Usuario_Operacion", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Tipo_Retiro", ePT_Caracter, "VAM", ePD_Entrada
      .Parametros.Add "Empresa_Relacionada", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Tipo_Oper_Relacionada", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Operacion_Relacionada", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Numero_Cuenta", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Cor_Codigo", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Porcentaje_Margen", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Moneda_Arbitraje", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Tipo_Filtro", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Tir", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Pvc", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Tasa_Estimada", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Valor_Presente", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Valor_Presente_Mon_Orig", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Valor_Par_$$", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Valor_Par_UM", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Duration_Par", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Valor_Mercado", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Valor_Mercado_UM", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Duration_Mercado", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Tipo_Custodia", ePT_Caracter, "1", ePD_Entrada
      .Parametros.Add "Dias_Compromiso", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Tasa_Emision", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Precio_Origen", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Modo_Actualizacion", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Monto_Origen", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Monto_operacion_liq", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Moneda_liquidaci�n", ePT_Caracter, "", ePD_Entrada
      
      If .EjecutaSP Then
        Fnt_Graba_Oper_FFMM = True
        pMsg_Error = "Grabado correctamente"
      Else
        Fnt_Graba_Oper_FFMM = False
        pMsg_Error = .ErrMsg
      End If
    End With
      
  ElseIf lTipo_Mov = gcTipoOperacion_Egreso Then
    
    With gDB
      .Parametros.Clear
      .Procedimiento = "LEASIVDB&dbo&SP_CREA_VENTA_RV_NACIONAL"
      
      .Parametros.Add "Codigo_Fondo", ePT_Caracter, "F05", ePD_Entrada
      .Parametros.Add "Sub_Tipo_Operacion", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Rut_Cliente", ePT_Numero, lRut_Cliente, ePD_Entrada
      .Parametros.Add "Tipo_Cartera", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Fecha_Operacion", ePT_Fecha, GetCell(pGrilla, pFila, "fecha"), ePD_Entrada
      .Parametros.Add "Comision", ePT_Numero, 0, ePD_Entrada
      .Parametros.Add "Derechos", ePT_Numero, 0, ePD_Entrada
      .Parametros.Add "Gastos", ePT_Numero, 0, ePD_Entrada
      .Parametros.Add "Iva", ePT_Numero, 0, ePD_Entrada
      .Parametros.Add "Monto_Operacion", ePT_Numero, GetCell(pGrilla, pFila, "monto_neto"), ePD_Entrada
      .Parametros.Add "Forma_Pago", ePT_Caracter, "CHE", ePD_Entrada
      .Parametros.Add "Tipo_Liquidacion", ePT_Caracter, "CN", ePD_Entrada
      .Parametros.Add "Fecha_Liquidacion", ePT_Fecha, GetCell(pGrilla, pFila, "fecha_liquidacion"), ePD_Entrada
      .Parametros.Add "Observacion", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Terminal_PC", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Usuario_Operacion", ePT_Caracter, "", ePD_Entrada
      .Parametros.Add "Tipo_Retiro", ePT_Caracter, "VAM", ePD_Entrada
      .Parametros.Add "Monto_Operacion_Liq", ePT_Numero, 0, ePD_Entrada
      .Parametros.Add "Nemotecnico", ePT_Caracter, GetCell(pGrilla, pFila, "nemotecnico"), ePD_Entrada
      .Parametros.Add "Nominales", ePT_Numero, GetCell(pGrilla, pFila, "cantidad"), ePD_Entrada
      .Parametros.Add "Precio_Unitario", ePT_Numero, GetCell(pGrilla, pFila, "precio"), ePD_Entrada
      .Parametros.Add "Valor_Presente", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Valor_Presente_UM", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Valor_Mercado", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Valor_Mercado_UM", ePT_Numero, "", ePD_Entrada
      .Parametros.Add "Tipo_Custodia", ePT_Caracter, "1", ePD_Entrada
      
      If .EjecutaSP Then
        Fnt_Graba_Oper_FFMM = True
        pMsg_Error = "Grabado correctamente"
      Else
        Fnt_Graba_Oper_FFMM = False
        pMsg_Error = .ErrMsg
      End If
    End With
      
  End If
      
End Function

Private Function Fnt_Guardar_Rel_Nro_Oper_Detalle_RV(pNro_Orden As Variant, _
                                                     pId_Operacion_Detalle As String, _
                                                     pMsg_Error As String) As Boolean
Dim lId_Origen          As String
Dim lId_Tipo_Conversion As String
Dim lcRel_Conversion    As Object

  Fnt_Guardar_Rel_Nro_Oper_Detalle_RV = True
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_CDS_Fact_RV, False)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle, False)
  
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = lId_Origen
    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
    .Campo("valor").Valor = pNro_Orden
    Rem En "id_entidad" se guarda el id_operacion_detalle
    .Campo("id_entidad").Valor = pId_Operacion_Detalle
    If Not .Guardar Then
      pMsg_Error = "Problemas en guardar la relaci�n N�mero de Orden y el Detalle de la Operaci�n." & vbCr & .ErrMsg
      Fnt_Guardar_Rel_Nro_Oper_Detalle_RV = False
    End If
  End With
  Set lcRel_Conversion = Nothing
  
End Function

Public Function Fnt_Excel(pNombre_Hoja As String, _
                          pTitulo As String, _
                          pArchivo As hTextLabel, _
                          pGrilla_con_Cta As VSFlexGrid, _
                          pGrilla_sin_Cta As VSFlexGrid, _
                          ByRef pMsg_Error As String) As Boolean
Dim lApp_Excel      As Excel.Application
Dim lLibro          As Excel.Workbook
Dim lxHoja          As Worksheet
Dim lHojas          As Integer
Dim lNro_Libro      As Integer
Dim lHoja           As Integer
Dim lFila           As Long
Dim lColumna        As Long
Dim lFila_Grilla    As Long
Dim lColumna_Grilla As Long
    
  On Error GoTo ErrProcedure
  
  Call Sub_Bloquea_Puntero(Me)
  
  Fnt_Excel = True
  
  If pGrilla_con_Cta.Rows = 1 And pGrilla_sin_Cta.Rows = 1 Then
    Call Sub_Desbloquea_Puntero(Me)
    Exit Function
  End If
  
  Set lApp_Excel = CreateObject("Excel.application")
  Set lLibro = lApp_Excel.Workbooks.Add
  
  For lNro_Libro = lLibro.Worksheets.Count To lLibro.Worksheets.Count - 1 Step -1
      lLibro.Worksheets(lNro_Libro).Delete
  Next lNro_Libro
  
  For lNro_Libro = lLibro.Worksheets.Count To lHojas - 1
      lLibro.Worksheets.Add
  Next lNro_Libro
  
  For lHoja = 1 To lLibro.Worksheets.Count
    lLibro.Sheets(lHoja).Select
    lLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresa).ShapeRange.IncrementLeft 39.75
    lLibro.Worksheets.Item(lHoja).Range("B6").Value = "Banca Patrimonial de Inversiones"
    lLibro.Worksheets.Item(lHoja).Range("B6").Font.Bold = True
    lLibro.Worksheets.Item(lHoja).Columns("A:A").ColumnWidth = 2
  Next lHoja
  
  lLibro.Sheets(1).Select
  lApp_Excel.ActiveWindow.DisplayGridlines = False

  'NOMBRE HOJA
  lLibro.Worksheets.Item(1).Name = pNombre_Hoja

  'TITULO
  lLibro.ActiveSheet.Range("D7").Value = pTitulo
  lLibro.ActiveSheet.Range("D7").Font.Bold = True
  lLibro.ActiveSheet.Range("D7:H7").HorizontalAlignment = xlCenter
  lLibro.ActiveSheet.Range("D7:H7").Merge
  
  'ENCABEZADO
  lFila = 10
  lColumna = 2
  lLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Operaciones con Cuentas"
  lLibro.ActiveSheet.Cells(lFila, lColumna).Font.Bold = True
  
  Set lxHoja = lLibro.Sheets(1)
  Call lxHoja.Select
  
  With pGrilla_con_Cta
    If .Rows > 1 Then
      For lFila_Grilla = 0 To .Rows - 1
        lFila = lFila + 1
        For lColumna_Grilla = 1 To .Cols - 1
          If Not .ColHidden(lColumna_Grilla) Then
            lxHoja.Cells(lFila, lColumna).NumberFormat = .ColFormat(lColumna_Grilla)
            lxHoja.Cells(lFila, lColumna).Value = .TextMatrix(lFila_Grilla, lColumna_Grilla)
            lColumna = lColumna + 1
          End If
        Next
        lColumna = 2
      Next
      Call Sub_Formato_Hoja(lApp_Excel, lLibro, "B11")
    Else
      lLibro.ActiveSheet.Cells(lFila + 1, lColumna).Value = "No hay datos"
    End If
  End With
  
  lFila = lFila + 3
  lColumna = 2
  lLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Operaciones sin Cuentas"
  lLibro.ActiveSheet.Cells(lFila, lColumna).Font.Bold = True
  
  With pGrilla_sin_Cta
    If .Rows > 1 Then
      For lFila_Grilla = 0 To .Rows - 1
        lFila = lFila + 1
        For lColumna_Grilla = 1 To .Cols - 1
          If Not .ColHidden(lColumna_Grilla) Then
            lxHoja.Cells(lFila, lColumna).NumberFormat = .ColFormat(lColumna_Grilla)
            lxHoja.Cells(lFila, lColumna).Value = .TextMatrix(lFila_Grilla, lColumna_Grilla)
            lColumna = lColumna + 1
          End If
        Next
        lColumna = 2
      Next
      Call Sub_Formato_Hoja(lApp_Excel, lLibro, "B" & (lFila + 1 - pGrilla_sin_Cta.Rows))
    Else
      lLibro.ActiveSheet.Cells(lFila + 1, lColumna).Value = "No hay datos"
    End If
  End With
  
  lLibro.Sheets(1).Select
  lApp_Excel.Visible = True
  lApp_Excel.UserControl = True
    
  Call Sub_Desbloquea_Puntero(Me)
  
  Exit Function
   
ErrProcedure:
  Fnt_Excel = False
  pMsg_Error = Err.Description
  Err.Clear
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Function

Private Sub Sub_Formato_Hoja(pApp_Excel As Excel.Application, pLibro As Excel.Workbook, ByVal pRango As String)
  pLibro.Sheets(1).Select
  pLibro.Sheets(1).Activate

  pLibro.ActiveSheet.Range(pRango).Select
  pLibro.ActiveSheet.Range(pRango).Activate
  pApp_Excel.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlDown)).Select
  pApp_Excel.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlToRight)).Select
  
  pApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
  
  pApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
  
  pApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
  
  pApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
  
  pApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
  
  pApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
  pApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
  
  pLibro.ActiveSheet.Range(pRango).Select
  pLibro.ActiveSheet.Range(pRango).Activate
  pApp_Excel.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlToRight)).Select
  pLibro.ActiveSheet.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlToRight)).Activate
  pLibro.ActiveSheet.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlToRight)).Font.Bold = True

  pApp_Excel.Selection.Interior.ColorIndex = 36
  pApp_Excel.Selection.Interior.Pattern = xlSolid
  
  pLibro.ActiveSheet.Columns("B:B").Select
  pApp_Excel.Range(pApp_Excel.Selection, pApp_Excel.Selection.End(xlToRight)).Select
  pApp_Excel.Selection.EntireColumn.AutoFit
  pLibro.ActiveSheet.Range("A1").Select
    
End Sub

Private Sub Sub_Check_DesCheck_Mismo_Nro_Orden(pGrilla As VSFlexGrid)
Dim lFil_grilla As Integer
Dim lCol_grilla As Integer
Dim lLinea      As Integer
Dim lNro_Orden  As Long
    
    lFil_grilla = pGrilla.Row
    lCol_grilla = pGrilla.Col
    If lCol_grilla = 0 And lFil_grilla > 0 Then
        If GetCell(pGrilla, lFil_grilla, "flg_bloqueado") <> "D" Then
            If Val(GetCell(pGrilla, lFil_grilla, "chk")) = 0 Then
                lNro_Orden = Val(GetCell(pGrilla, lFil_grilla, "colum_pk"))
                For lLinea = 1 To pGrilla.Rows - 1
                    If lNro_Orden = Val(GetCell(pGrilla, lLinea, "colum_pk")) Then
                        pGrilla.TextMatrix(lLinea, lCol_grilla) = flexChecked
                    End If
                Next
            Else
                lNro_Orden = Val(GetCell(pGrilla, lFil_grilla, "colum_pk"))
                For lLinea = 1 To pGrilla.Rows - 1
                    If lNro_Orden = Val(GetCell(pGrilla, lLinea, "colum_pk")) Then
                        pGrilla.TextMatrix(lLinea, lCol_grilla) = 0
                    End If
                Next
            End If
        End If
    End If
End Sub

Public Sub Sub_Reporte_Asesor(pGrilla As VSFlexGrid, _
                                pGrilla_SC As VSFlexGrid, _
                                lApellido As String)
Dim Registros     As Integer
Dim lFila         As Long
Dim Ultimo_Asesor
Dim Asesor_Actual
Dim i             As Integer
Dim Asesores()

Ultimo_Asesor = 0
  With pGrilla
    '.Select 1, .ColIndex("id_asesor")
    '.Sort = flexSortGenericAscending
  End With
  
  With pGrilla_SC
    If .Rows > 1 Then
    '.Select 1, .ColIndex("id_asesor")
    '.Sort = flexSortGenericAscending
    End If
  End With
   
 ReDim Preserve Asesores(0)
 
  With pGrilla
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked Then
          If Not Fnt_Buscar_Asesor_Arreglo(Asesores(), GetCell(pGrilla, lFila, "id_asesor")) Then
                i = UBound(Asesores)
                Asesores(i) = GetCell(pGrilla, lFila, "id_asesor")
                i = UBound(Asesores) 'ReDim Asesores(UBound(Asesores) + 1)
                ReDim Preserve Asesores(i)
                
          End If
          
     End If
    Next
  End With
  Ultimo_Asesor = 0
      
  With pGrilla_SC
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked Then
       If Not Fnt_Buscar_Asesor_Arreglo(Asesores(), GetCell(pGrilla_SC, lFila, "id_asesor")) Then
                i = UBound(Asesores)
                Asesores(i) = NVL(GetCell(pGrilla_SC, lFila, "id_asesor"), 0)
                i = UBound(Asesores) + 1 'ReDim Asesores(UBound(Asesores) + 1)
                ReDim Preserve Asesores(i)
                
          End If
      End If
      
    Next
  End With
  
  For i = 0 To UBound(Asesores)
    If Asesores(i) <> "" Then
        If lApellido = "FM" Then
            Call Sub_CreaPDF_Mail_Asesor_FM(Asesores(i), pGrilla, pGrilla_SC, lApellido)
        Else
            Call Sub_CreaPDF_Mail_Asesor(Asesores(i), pGrilla, pGrilla_SC, lApellido)
        End If
    End If
      'MsgBox Asesores(i)
  Next
  
End Sub

Private Function Fnt_Buscar_Asesor_Arreglo(Arreglo(), lId_Asesor) As Boolean
Dim i
  Fnt_Buscar_Asesor_Arreglo = False
  For i = 0 To UBound(Arreglo)
    If Arreglo(i) = lId_Asesor Then
        Fnt_Buscar_Asesor_Arreglo = True
        Exit For
    End If
  Next
End Function

Private Sub Sub_CreaPDF_Mail_Asesor(pId_Asesor, pGrilla As VSFlexGrid, pGrilla_SC As VSFlexGrid, lApellido As String)
Dim lFila As Long
'Dim ArchivosPDF()
'With pGrilla
  
'ReDim Preserve ArchivosPDF(0)
 
Const clrHeader = &HD0D0D0
Dim sRecord
Dim bAppend
Dim lLinea As Integer
Dim lForm As Object 'Frm_Reporte_Generico
Dim lReg As hCollection.hFields
Dim a As Integer
Dim iFila As Integer
Dim Filas As Integer
Dim Fila As Integer
Dim lCursor_Asesor As hRecord
Dim pArchivoPDF As String
Dim lTotalGastos As Double
'-----------------------------------------------------------------------
'Dim lOD_Cursor As hRecord 'CURSOR PARA EL DETALLE DE LAS OPERACIONES.

  'Set Fnt_Generar_Comprobante = Nothing
  'Call Sub_Bloquea_Puntero(Me)
  'Call Sub_CargarDatos_Gen(pId_Operacion)
  
  'Set lForm = fClass.Form_Reporte_Generico
  
  pArchivoPDF = Environ("temp") & "\Confirmacion_Importacion_" & lApellido & "-" & pId_Asesor & ".pdf"
  Set lForm = Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="Confirmaci�n de Ordenes" _
                               , pTipoSalida:=ePrinter.eP_PDF_Automatico _
                               , pOrientacion:=orLandscape _
                               , pArchivoSalida:=pArchivoPDF)
     
Dim lc_Asesores As Class_Asesor
Dim lNombre_Asesor As String
Dim lMail_Asesor As String

Set lc_Asesores = New Class_Asesor

With lc_Asesores
    .Campo("id_asesor").Valor = pId_Asesor
    If .Buscar Then
        Set lCursor_Asesor = .Cursor
        For Each lReg In .Cursor
           lNombre_Asesor = lReg("nombre").Value
           lMail_Asesor = lReg("Email").Value
        Next
    End If
End With
    
  With lForm.VsPrinter

 ' .StartDoc
 .Paragraph = ""
 .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 8
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "ASESOR: " & lNombre_Asesor & " (" & lMail_Asesor & ")"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
  .EndTable
 
 .Paragraph = ""

  .MarginLeft = "30mm"
  .MarginRight = "7mm"
    .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 7
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "Operaciones con Patrimonio Administrado"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
    .EndTable
 
    '.Paragraph = "" 'salto de linea
    
    .StartTable
      
    '.TableCell(tcRows) = pGrilla.Rows
    .TableCell(tcRows) = 1
    .TableCell(tcCols) = 11
    .TableBorder = tbAll
    '
   .TableCell(tcColWidth, 1, 1) = "15mm"
    .TableCell(tcColWidth, 1, 2) = "18mm"
    .TableCell(tcColWidth, 1, 3) = "15mm"
    .TableCell(tcColWidth, 1, 4) = "45mm"
    .TableCell(tcColWidth, 1, 5) = "20mm"
    .TableCell(tcColWidth, 1, 6) = "35mm"
    .TableCell(tcColWidth, 1, 7) = "25mm"
    .TableCell(tcColWidth, 1, 8) = "15mm"
    .TableCell(tcColWidth, 1, 9) = "15mm"
    .TableCell(tcColWidth, 1, 10) = "25mm"
    .TableCell(tcColWidth, 1, 11) = "15mm"
    '.TableCell(tcColWidth, 1, 12) = "25mm"
    '.TableCell(tcColWidth, 1, 13) = "20mm"
    
    .TableCell(tcText, 1, 1) = "N� Orden"
    .TableCell(tcText, 1, 2) = "Rut Cliente"
    .TableCell(tcText, 1, 3) = "Cuenta"
    .TableCell(tcText, 1, 4) = "Cliente"
    .TableCell(tcText, 1, 5) = "Movimiento"
    .TableCell(tcText, 1, 6) = "Nemot�cnico"
    .TableCell(tcText, 1, 7) = "Cantidad"
    .TableCell(tcText, 1, 8) = "Precio / Tasa"
   ' .TableCell(tcText, 1, 8) = "Comisi�n"
   ' .TableCell(tcText, 1, 9) = "Derechos"
   ' .TableCell(tcText, 1, 10) = "Gasto"
   ' .TableCell(tcText, 1, 11) = "IVA"
    .TableCell(tcText, 1, 9) = "Gastos Ope."
    .TableCell(tcText, 1, 10) = "Monto"
    .TableCell(tcText, 1, 11) = "Fecha Liquidaci�n"
    
    
 Fila = 2
 For lFila = 1 To pGrilla.Rows - 1
      If pGrilla.Cell(flexcpChecked, lFila, pGrilla.ColIndex("chk")) = flexChecked And GetCell(pGrilla, lFila, "id_asesor") = pId_Asesor Then
          .TableCell(tcRows) = .TableCell(tcRows) + 1
           'Fila = lFila + 1
          .TableCell(tcText, Fila, 1) = GetCell(pGrilla, lFila, "n_orden")
          .TableCell(tcText, Fila, 2) = GetCell(pGrilla, lFila, "rut")
          .TableCell(tcText, Fila, 3) = GetCell(pGrilla, lFila, "num_cuenta")
          .TableCell(tcText, Fila, 4) = GetCell(pGrilla, lFila, "dsc_cliente")
          .TableCell(tcText, Fila, 5) = GetCell(pGrilla, lFila, "tipo_movimiento")
          .TableCell(tcText, Fila, 6) = GetCell(pGrilla, lFila, "nemotecnico")
          .TableCell(tcText, Fila, 7) = FormatNumber(GetCell(pGrilla, lFila, "cantidad"))
          .TableCell(tcText, Fila, 8) = FormatNumber(GetCell(pGrilla, lFila, "precio"))
          .TableCell(tcText, Fila, 9) = FormatNumber(GetCell(pGrilla, lFila, "total_gastos"))
          If UCase(GetCell(pGrilla, lFila, "tipo_movimiento")) = "COMPRA" Then
            lTotalGastos = FormatNumber(GetCell(pGrilla, lFila, "total_gastos"))
          Else
            lTotalGastos = FormatNumber(GetCell(pGrilla, lFila, "total_gastos")) * -1
          End If
          .TableCell(tcText, Fila, 10) = FormatNumber(FormatNumber(GetCell(pGrilla, lFila, "monto")) + lTotalGastos)
          .TableCell(tcText, Fila, 11) = GetCell(pGrilla, lFila, "fecha_liquidacion")
          
          .TableCell(tcAlign, Fila, 1) = taLeftMiddle
          .TableCell(tcAlign, Fila, 2) = taRightMiddle
          .TableCell(tcAlign, Fila, 3) = taCenterMiddle
          .TableCell(tcAlign, Fila, 6) = taLeftMiddle
          .TableCell(tcAlign, Fila, 7) = taRightMiddle
          .TableCell(tcAlign, Fila, 8) = taRightMiddle
          .TableCell(tcAlign, Fila, 9) = taRightMiddle
          .TableCell(tcAlign, Fila, 10) = taRightMiddle
          .TableCell(tcAlign, Fila, 11) = taRightMiddle
          '.TableCell(tcAlign, Fila, 12) = taRightMiddle
          '.TableCell(tcAlign, Fila, 13) = taRightMiddle
          Fila = Fila + 1
      End If
  Next
  .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 13) = 6
  
  .EndTable
  
.Paragraph = ""
'If pGrilla_SC.Rows > 1 Then
.StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 7
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "Operaciones Fuera del Patrimonio Administrado"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
.EndTable


.StartTable
      
    '.TableCell(tcRows) = pGrilla_SC.Rows
    .TableCell(tcRows) = 1
    .TableCell(tcCols) = 11
    .TableBorder = tbAll
   ' .TableCell(tcFontSize, 1, 1, pGrilla_SC.Rows, 13) = 6
    
   .TableCell(tcColWidth, 1, 1) = "15mm"
    .TableCell(tcColWidth, 1, 2) = "18mm"
    .TableCell(tcColWidth, 1, 3) = "15mm"
    .TableCell(tcColWidth, 1, 4) = "45mm"
    .TableCell(tcColWidth, 1, 5) = "20mm"
    .TableCell(tcColWidth, 1, 6) = "35mm"
    .TableCell(tcColWidth, 1, 7) = "25mm"
    .TableCell(tcColWidth, 1, 8) = "15mm"
    .TableCell(tcColWidth, 1, 9) = "15mm"
    .TableCell(tcColWidth, 1, 10) = "25mm"
    .TableCell(tcColWidth, 1, 11) = "15mm"
    
    
    .TableCell(tcText, 1, 1) = "N� Orden"
    .TableCell(tcText, 1, 2) = "Rut Cliente"
    .TableCell(tcText, 1, 3) = "Cuenta"
    .TableCell(tcText, 1, 4) = "Cliente"
    .TableCell(tcText, 1, 5) = "Movimiento"
    .TableCell(tcText, 1, 6) = "Nemot�cnico"
    .TableCell(tcText, 1, 7) = "Cantidad"
    .TableCell(tcText, 1, 8) = "Precio / Tasa"
    '.TableCell(tcText, 1, 8) = "Comisi�n"
    '.TableCell(tcText, 1, 9) = "Derechos"
    '.TableCell(tcText, 1, 10) = "Gasto"
    '.TableCell(tcText, 1, 11) = "IVA"
    .TableCell(tcText, 1, 9) = "Gastos Ope."
    .TableCell(tcText, 1, 10) = "Monto"
    .TableCell(tcText, 1, 11) = "Fecha Liquidaci�n"


'With pGrilla_SC
Fila = 2
  For lFila = 1 To pGrilla_SC.Rows - 1
      If pGrilla_SC.Cell(flexcpChecked, lFila, pGrilla_SC.ColIndex("chk")) = flexChecked And GetCell(pGrilla_SC, lFila, "id_asesor") = pId_Asesor Then
          .TableCell(tcRows) = .TableCell(tcRows) + 1
           ' Fila = lFila + 1
          .TableCell(tcText, Fila, 1) = GetCell(pGrilla_SC, lFila, "n_orden")
          .TableCell(tcText, Fila, 2) = GetCell(pGrilla_SC, lFila, "rut")
          .TableCell(tcText, Fila, 3) = GetCell(pGrilla_SC, lFila, "num_cuenta")
          .TableCell(tcText, Fila, 4) = GetCell(pGrilla_SC, lFila, "dsc_cliente")
          .TableCell(tcText, Fila, 5) = GetCell(pGrilla_SC, lFila, "tipo_movimiento")
          .TableCell(tcText, Fila, 6) = GetCell(pGrilla_SC, lFila, "nemotecnico")
          .TableCell(tcText, Fila, 7) = FormatNumber(GetCell(pGrilla_SC, lFila, "cantidad"))
          .TableCell(tcText, Fila, 8) = FormatNumber(GetCell(pGrilla_SC, lFila, "precio"))
          '.TableCell(tcText, Fila, 8) = FormatNumber(GetCell(pGrilla_SC, lFila, "comision"))
          '.TableCell(tcText, Fila, 9) = FormatNumber(GetCell(pGrilla_SC, lFila, "derecho"))
          '.TableCell(tcText, Fila, 10) = FormatNumber(GetCell(pGrilla_SC, lFila, "gasto"))
          '.TableCell(tcText, Fila, 11) = FormatNumber(GetCell(pGrilla_SC, lFila, "iva"))
          .TableCell(tcText, Fila, 9) = FormatNumber(GetCell(pGrilla_SC, lFila, "total_gastos"))
          .TableCell(tcText, Fila, 10) = FormatNumber(GetCell(pGrilla_SC, lFila, "monto"))
          .TableCell(tcText, Fila, 11) = GetCell(pGrilla_SC, lFila, "fecha_liquidacion")
     
          .TableCell(tcAlign, Fila, 1) = taLeftMiddle
          .TableCell(tcAlign, Fila, 2) = taRightMiddle
          .TableCell(tcAlign, Fila, 3) = taCenterMiddle
          .TableCell(tcAlign, Fila, 6) = taRightMiddle
          .TableCell(tcAlign, Fila, 7) = taRightMiddle
          .TableCell(tcAlign, Fila, 8) = taRightMiddle
          .TableCell(tcAlign, Fila, 9) = taRightMiddle
          .TableCell(tcAlign, Fila, 10) = taRightMiddle
          .TableCell(tcAlign, Fila, 11) = taRightMiddle
          .TableCell(tcAlign, Fila, 12) = taRightMiddle
          .TableCell(tcAlign, Fila, 13) = taRightMiddle
          Fila = Fila + 1
      End If
  Next
  .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 13) = 6
.EndTable
'End If
.EndDoc
End With
If Envia_Confirmacion(pArchivoPDF, lMail_Asesor, lNombre_Asesor) Then

End If
End Sub

Public Function Envia_Confirmacion(lArchivo As String, lMail As String, lNombre_Asesor As String) As Boolean
Dim strNotaEnc      As String
Dim lDestinatarios  As hRecord
Dim lrArchivosPDFs  As hRecord
Dim lMailBackOffice As String
    
    Envia_Confirmacion = False
    
    If Trim(lMail) <> "" Then
        Set lrArchivosPDFs = Nothing
        Call Prc_MAPI.Fnt_AgregarArchivoAdjunto(lrArchivosPDFs, lArchivo)
        Set lDestinatarios = Nothing
        Call Fnt_AgregarDestinatario(pRegDestinatarios:=lDestinatarios, pDisplayName:=lNombre_Asesor, pEmail:=lMail) 'lMail)
        lMailBackOffice = Fnt_Lee_Mail_BackOffice(gId_Empresa)
'        If lMailBackOffice <> "" Then
'            Call Fnt_AgregarDestinatario(pRegDestinatarios:=lDestinatarios, pDisplayName:="", pEmail:=lMailBackOffice)
'        End If
        strNotaEnc = " Sr. " & lNombre_Asesor & Chr(10) & "  Se Adjuntan operaciones que ser�n importadas al sistema GPI, favor confirmar "
'        Call Prc_MAPI.Fnt_Enviar("GPI: Confirmacion de Operaciones", strNotaEnc, lDestinatarios, True, lrArchivosPDFs)
        'Modificado por MMA 12/11/2008, se agrega el destinatario de BackOffice como CC y no en Para en el correo
        Call Prc_MAPI.Fnt_Enviar("GPI: Confirmacion de Operaciones", strNotaEnc, lDestinatarios, True, lrArchivosPDFs, lMailBackOffice)
    End If
End Function

Private Sub Grilla_Operaciones_RV_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_RV, "colum_pk", False)
End Sub

Private Sub Sub_Setea_Cta_Nueva(pGrilla As VSFlexGrid _
                              , Optional pColumn As String = "n_orden" _
                              , Optional pExiste_Asesor As Boolean = True _
                              , Optional pExiste_Dsc_Corta As Boolean = False)
Dim lId_Cuenta
Dim lcCuenta            As Object
Dim lNum_Cuenta         As String
Dim lRut_Cliente        As String
Dim lNro_Orden          As String
Dim sNombreCliente      As String
Dim lId_Asesor
Dim lFila               As Long
Dim lTipo_Cta_Dsc_Corta As String
Dim lTipo_Cta_Dsc_Larga As String
    
  lId_Cuenta = Frm_Busca_Cuentas.Buscar
  
  If Not IsNull(lId_Cuenta) Then
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
      .Campo("id_cuenta").Valor = lId_Cuenta
      If .Buscar(pEnVista:=True) Then
        lNum_Cuenta = .Cursor(1).Fields("num_cuenta").Value
        lRut_Cliente = .Cursor(1).Fields("rut_cliente").Value
        sNombreCliente = .Cursor(1).Fields("nombre_cliente").Value
        lId_Asesor = NVL(.Cursor(1).Fields("id_asesor").Value, 0)
        lTipo_Cta_Dsc_Corta = .Cursor(1).Fields("DESCRIPCION_CORTA").Value
        lTipo_Cta_Dsc_Larga = .Cursor(1).Fields("DESCRIPCION_LARGA").Value
      End If
    End With
    Set lcCuenta = Nothing
    
    If pExiste_Dsc_Corta Then
      Call SetCell(pGrilla, pGrilla.Row, "id_cuenta", lId_Cuenta)
      Call SetCell(pGrilla, pGrilla.Row, "num_cuenta", lNum_Cuenta)
      If pExiste_Asesor Then Call SetCell(pGrilla, pGrilla.Row, "id_asesor", lId_Asesor)
      Call SetCell(pGrilla, pGrilla.Row, "rut", lRut_Cliente)
      Call SetCell(pGrilla, pGrilla.Row, "nombre_cliente", sNombreCliente)
      Call SetCell(pGrilla, pGrilla.Row, "dsc_corta", lTipo_Cta_Dsc_Corta)
      Call SetCell(pGrilla, pGrilla.Row, "dsc_larga", lTipo_Cta_Dsc_Larga)
    Else
      lNro_Orden = GetCell(pGrilla, pGrilla.Row, pColumn)
      For lFila = 1 To pGrilla.Rows - 1
        If GetCell(pGrilla, lFila, pColumn) = lNro_Orden Then
          Call SetCell(pGrilla, lFila, "id_cuenta", lId_Cuenta)
          Call SetCell(pGrilla, lFila, "num_cuenta", lNum_Cuenta)
          If pExiste_Asesor Then Call SetCell(pGrilla, lFila, "id_asesor", lId_Asesor)
          Call SetCell(pGrilla, lFila, "rut", lRut_Cliente)
          Call SetCell(pGrilla, lFila, "nombre_cliente", sNombreCliente)
          If pExiste_Dsc_Corta Then
            Call SetCell(pGrilla, lFila, "dsc_corta", lTipo_Cta_Dsc_Corta)
            Call SetCell(pGrilla, lFila, "dsc_larga", lTipo_Cta_Dsc_Larga)
          End If
        End If
      Next
    End If
  End If
End Sub

Private Function Fnt_Buscar_Datos_Nemo(pId_Nemotecnico As String, _
                                       ByRef pId_Moneda_Transaccion, _
                                       ByRef pMsg_Error As String, _
                                       Optional ByRef pId_Moneda_Valorizacion, _
                                       Optional ByRef pFecha_Vencimiento, _
                                       Optional ByRef pCod_Instrumento) As Boolean
Dim lcAlias       As Class_Alias
Dim lcnemot�cnico As Class_Nemotecnicos
  
  Fnt_Buscar_Datos_Nemo = True
  
  Set lcnemot�cnico = New Class_Nemotecnicos
  With lcnemot�cnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pId_Moneda_Transaccion = .Cursor(1)("id_moneda_transaccion").Value
        pId_Moneda_Valorizacion = .Cursor(1)("id_moneda").Value
        pFecha_Vencimiento = .Cursor(1)("fecha_vencimiento").Value
        pCod_Instrumento = .Cursor(1)("cod_instrumento").Value
      Else
        pMsg_Error = "Nemot�cnico no tiene moneda asociada." & vbCr & "No se puede operar la instrucci�n."
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = "Problemas en cargar datos de nemot�cnico." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcnemot�cnico = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Buscar_Datos_Nemo = False
End Function

Private Function Fnt_Guardar_Rel_Nro_Oper_Detalle_RF(pNro_Orden As Variant, _
                                                     pId_Operacion_Detalle As String, _
                                                     pMsg_Error As String) As Boolean
Dim lId_Origen          As String
Dim lId_Tipo_Conversion As String
Dim lcRel_Conversion    As Object

  Fnt_Guardar_Rel_Nro_Oper_Detalle_RF = True
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_Sec_Mov_RF, False)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle, False)
  
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = lId_Origen
    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
    .Campo("valor").Valor = pNro_Orden
    Rem En "id_entidad" se guarda el id_operacion_detalle
    .Campo("id_entidad").Valor = pId_Operacion_Detalle
    If Not .Guardar Then
      pMsg_Error = "Problemas en guardar la relaci�n N�mero de Orden y el Detalle de la Operaci�n." & vbCr & .ErrMsg
      Fnt_Guardar_Rel_Nro_Oper_Detalle_RF = False
    End If
  End With
  Set lcRel_Conversion = Nothing
  
End Function

Private Function Fnt_Buscar_Rel_Nro_Oper_Detalle_RF(pNro_Orden _
                                                  , ByRef pMsg_Error As String) As Boolean
Dim lId_Origen          As String
Dim lId_Tipo_Conversion As String
Dim lcRel_Conversion    As Object
    
  Fnt_Buscar_Rel_Nro_Oper_Detalle_RF = False
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_Sec_Mov_RF)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle)
    
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = lId_Origen
    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
    .Campo("valor").Valor = pNro_Orden
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
        Fnt_Buscar_Rel_Nro_Oper_Detalle_RF = True
      End If
    Else
      pMsg_Error = "Problemas en buscar la relaci�n N�mero Documento y el Detalle de la Operaci�n." & vbCr & .ErrMsg
      Fnt_Buscar_Rel_Nro_Oper_Detalle_RF = True
    End If
  End With
  Set lcRel_Conversion = Nothing
End Function

Private Function Fnt_Buscar_Rel_Nro_Oper_Detalle_FM(pNro_Orden _
                                                  , ByRef pMsg_Error As String) As Boolean
Dim lId_Origen          As String
Dim lId_Tipo_Conversion As String
Dim lcRel_Conversion    As Object
    
  Fnt_Buscar_Rel_Nro_Oper_Detalle_FM = False
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_Sec_Mov_FFMM)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle)
    
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = lId_Origen
    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
    .Campo("valor").Valor = pNro_Orden
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
        Fnt_Buscar_Rel_Nro_Oper_Detalle_FM = True
      End If
    Else
      pMsg_Error = "Problemas en buscar la relaci�n N�mero Documento y el Detalle de la Operaci�n." & vbCr & .ErrMsg
      Fnt_Buscar_Rel_Nro_Oper_Detalle_FM = True
    End If
  End With
  Set lcRel_Conversion = Nothing
End Function

Private Sub Sub_Importa_RF()
Dim lcNemotecnicos  As Class_Nemotecnicos
Dim lcCuenta        As Object
Dim lCursor         As hRecord
Dim lCampo          As hFields
'---------------------------------------
Dim lGrilla As VSFlexGrid
'---------------------------------------
Dim lId_Nemotecnico
Dim lId_Cuenta
Dim lId_Asesor
Dim lNum_Cuenta
Dim lFolioContraparte
Dim lCod_Instrumento      As String
Dim lTipo_Movimiento      As String
Dim lFlg_Tipo_Movimiento  As String
'----------------------------------------------
Dim lLinea          As Long
Dim lNombre_Cliente As String
Dim lMontos_Gastos
Dim lFlg_Estado     As String
Dim lDsc_Bloqueo    As String

On Error GoTo ErrProcedure
  
  Me.Enabled = False
  Call Sub_Bloquea_Puntero(Me)
  
  Set lcNemotecnicos = New Class_Nemotecnicos
  
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  Set lcAlias = CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  gDB.Parametros.Clear
  'gDB.Procedimiento = "CisCB&DBO&AD_QRY_CB_MOVTO_RFIJA"
  gDB.Procedimiento = "PKG_PROCESOS_IMPORTADOR$CisCB_CB_MOVTO_RFIJA"
  gDB.Parametros.Add "pFecha_movimientos", ePT_Caracter, Format(DTP_Fecha_Proceso.Value, "YYYYMMDD"), ePD_Entrada
  gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
  If Not gDB.EjecutaSP Then
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
    GoTo ExitProcedure
  End If
  
  Set lCursor = gDB.Parametros("pcursor").Valor
    
  If lCursor.Count <= 0 Then
    GoTo ExitProcedure
  End If
      
  BarraProceso.Max = lCursor.Count
  For Each lCampo In lCursor
    BarraProceso.Value = lCampo.Index
    
    lFolioContraparte = Trim(lCampo("NUMERO_ORDEN").Value) & "/" & Trim(lCampo("CORRELATIVO").Value)
    
'    lId_Operacion_detalle = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Mov_RF _
'                                                    , pCodigoCSBPI:=cTabla_Operacion_Detalle _
'                                                    , pValor:=lFolioContraparte)
'
'    If Not IsNull(lId_Operacion_detalle) Then
'      'pMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
'      GoTo ProximoMovimiento
'    End If

    lId_Nemotecnico = ""
    lCod_Instrumento = ""

    '------- RESCATA EL ID_nemot�cnico
    If Mid(lCampo("NEMOTECNICO").Value, 1, 7) = "PACTO-C" Then
        lId_Nemotecnico = "0"
        lCod_Instrumento = gcINST_PACTOS_NAC
    ElseIf Mid(lCampo("NEMOTECNICO").Value, 1, 2) = "DU" Or _
        Mid(lCampo("NEMOTECNICO").Value, 1, 2) = "DO" Or _
        Mid(lCampo("NEMOTECNICO").Value, 1, 2) = "D$" Or _
        Mid(lCampo("NEMOTECNICO").Value, 1, 4) = "PDBC" Or _
        Mid(lCampo("NEMOTECNICO").Value, 1, 4) = "PRBC" Then
        lId_Nemotecnico = "0"
        lCod_Instrumento = gcINST_DEPOSITOS_NAC
    Else
        Set lcNemotecnicos = New Class_Nemotecnicos
        lcNemotecnicos.Campo("nemotecnico").Valor = lCampo("NEMOTECNICO").Value
        lCod_Instrumento = ""
        Rem Busca el id_nemotecnico del Nemotecnico de la planilla
        If lcNemotecnicos.Buscar_Nemotecnico(pCod_Producto:=Null, pMostrar_Msg:=False) Then
            lId_Nemotecnico = lcNemotecnicos.Campo("id_nemotecnico").Valor
            If Not lId_Nemotecnico = "0" Then
            'Rem Busca atributos del Nemotecnico
                If lcNemotecnicos.Buscar Then
                    lCod_Instrumento = NVL(lcNemotecnicos.Cursor(1)("cod_instrumento").Value, "")
                Else
                End If
            End If
        End If
        Set lcNemotecnicos = Nothing
    End If
    '------- FIN RESCATA EL ID_NEMOTECNICO
'    With lcNemotecnicos
'      .Campo("nemotecnico").Valor = Trim(lCampo("nemotecnico").Value)
'      If Not .Buscar() Then
'        Call Fnt_MsgError(.SubTipo_LOG, _
'                          "Problemas en buscar el Nemot�cnico.", _
'                          .ErrMsg, _
'                          pConLog:=True)
'        GoTo ExitProcedure
'      End If
'
'      If .Cursor.Count > 0 Then
'        lId_nemotecnico = .Cursor(1)("id_nemotecnico").Value
'        lCod_Instrumento = .Cursor(1)("cod_instrumento").Value
'      End If
'    End With
'    '------- FIN RESCATA EL ID_nemot�cnico
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_SECURITY.IMPORT_AVAIBLE"
    gDB.Parametros.Add "PID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    gDB.Parametros.Add "PVALOR", ePT_Caracter, lFolioContraparte, ePD_Entrada
    gDB.Parametros.Add "PCOD_INSTRUMENTO", ePT_Caracter, lCod_Instrumento, ePD_Entrada
    gDB.Parametros.Add "PRETURN", ePT_Caracter, "", ePD_Salida
    If Not gDB.EjecutaSP Then
      Call Fnt_MsgError(eLS_ErrSystem, _
                        "Problemas para comprobar si esta disponible la importaci�n.", _
                        gDB.ErrMsg, _
                        pConLog:=True)
      GoTo ProximoMovimiento
    End If
    
    If Not gDB.Parametros("PRETURN").Valor = gcFlg_SI Then
      'SI ENTREGA UN VALOR DISTINTO A "S" SIGNIFICA QUE NO TIENE PERMITIDO EL INGRESO
      GoTo ProximoMovimiento
    End If
    
      
    lTipo_Movimiento = ""
    Select Case lCampo("TIPO_OPERACION").Value
      Case cTipOp_Compra
        lTipo_Movimiento = "Compra"
        lFlg_Tipo_Movimiento = "I"
      Case cTipOp_Venta
        lTipo_Movimiento = "Venta"
        lFlg_Tipo_Movimiento = "E"
      Case Else
        'Si un tipo de movimiento no esta contemplado se salta
        GoTo ProximoMovimiento
    End Select
      
    lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Magic_Valores _
                                          , pCodigoCSBPI:=cTabla_Cuentas _
                                          , pValor:=NVL(lCampo("RUT_CLIENTE").Value, ""))
    lNum_Cuenta = ""
    lNombre_Cliente = ""
    lid_Empresa = 0
    If IsNull(lId_Cuenta) Then
      Set lGrilla = Grilla_Operaciones_Sin_Cuenta_RF
      lNombre_Cliente = lCampo("nombre_cliente").Value
      lId_Asesor = 0
    Else
      Set lGrilla = Grilla_Operaciones_RF
      
      ' ----------- RESCATA NUMERO CUENTA
      lcCuenta.Campo("id_cuenta").Valor = lId_Cuenta
      If lcCuenta.Buscar(pEnVista:=True) Then
        lNum_Cuenta = lcCuenta.Cursor(1)("num_cuenta").Value
        lNombre_Cliente = lcCuenta.Cursor(1)("nombre_cliente").Value
        lId_Asesor = NVL(lcCuenta.Cursor(1)("id_asesor").Value, 0)
        lFlg_Estado = lcCuenta.Cursor(1)("cod_estado").Value
        lDsc_Bloqueo = ""
        lid_Empresa = lcCuenta.Cursor(1)("id_empresa").Value
        If lFlg_Estado = "H" Then
            If lcCuenta.Cursor(1)("flg_bloqueado").Value = "S" Then
                lFlg_Estado = "B"
                lDsc_Bloqueo = lcCuenta.Cursor(1)("obs_bloqueo").Value
            End If
        Else
            lDsc_Bloqueo = "Cuenta Deshabilitada"
        End If
        If lid_Empresa <> Fnt_EmpresaActual Then
            GoTo ProximoMovimiento
        End If
      End If
    End If
    lLinea = lGrilla.Rows
    Call lGrilla.AddItem("")
    Call SetCell(lGrilla, lLinea, "colum_pk", Trim(lCampo("NUMERO_ORDEN").Value) & "/" & Trim(lCampo("CORRELATIVO").Value), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "n_orden", lCampo("NUMERO_ORDEN").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_linea", NVL(lCampo("CORRELATIVO").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "rut", Trim(lCampo("RUT_CLIENTE").Value), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "nombre_cliente", lNombre_Cliente, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_cuenta", "" & lId_Cuenta, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "num_cuenta", "" & lNum_Cuenta, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "tipo_movimiento", lTipo_Movimiento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "flg_tipo_movimiento", lFlg_Tipo_Movimiento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_nemotecnico", lId_Nemotecnico, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "nemotecnico", lCampo("nemotecnico").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "Cantidad", lCampo("CANTIDAD").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "dsc_cliente", Left(lNombre_Cliente, 30), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "precio", lCampo("PRECIO").Value, pAutoSize:=False)
    
    lMontos_Gastos = NVL(lCampo("COMISION").Value, 0) + NVL(lCampo("DERECHO").Value, 0) + NVL(lCampo("GASTOS").Value, 0) + NVL(lCampo("IVA").Value, 0)
    
    Call SetCell(lGrilla, lLinea, "comision", NVL(lCampo("COMISION").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "Derecho", NVL(lCampo("DERECHO").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "gastos", NVL(lCampo("GASTOS").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "iva", NVL(lCampo("IVA").Value, 0), pAutoSize:=False)
    
    
    Call SetCell(lGrilla, lLinea, "total_gastos", NVL(lMontos_Gastos, 0), pAutoSize:=False)
        
    Call SetCell(lGrilla, lLinea, "monto", NVL(lCampo("MONTO_OPERACION").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "fecha_liquidacion", NVL(lCampo("FECHA_LIQUIDACION").Value, ""), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "cod_instru", lCod_Instrumento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "Grabado", gcFlg_NO, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_asesor", lId_Asesor, pAutoSize:=False)
    
    Call SetCell(lGrilla, lLinea, "flg_bloqueado", lFlg_Estado, pAutoSize:=False)

    Call SetCell(lGrilla, lLinea, "id_mov_activo", NVL(lCampo("Id_Mov_Activo").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "dsc_error", lDsc_Bloqueo, pAutoSize:=False)
    
ProximoMovimiento:
        'este label sirve para saltarce el ingreso de un de operacion con un tipo de movimiento que no corresponde,
        'como los ingresos de custodias, la otra forma es hacer un if gigante que no quiero hacer :-P
        'HAF: 20071108
  Next
    
  Call Sub_AjustaColumnas_Grilla(Grilla_Operaciones_Sin_Cuenta_RF)
  Call Sub_AjustaColumnas_Grilla(Grilla_Operaciones_RF)

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la importaci�n de los movimientos." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  Set lcCuenta = Nothing
  Set lcNemotecnicos = Nothing
  Set lcAlias = Nothing

  gDB.Parametros.Clear
  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Graba_Operaciones_con_Cta_RF()
Dim lFila As Long
    
  With Grilla_Operaciones_RF
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked And _
        Not GetCell(Grilla_Operaciones_RF, lFila, "GRABADO") = gcFlg_SI Then
        If Not Fnt_Ope_Directa_Cont_RF(pGrilla:=Grilla_Operaciones_RF, _
                                       pFila:=lFila, _
                                       pNro_Orden:=GetCell(Grilla_Operaciones_RF, lFila, "n_orden"), _
                                       pLinea:=GetCell(Grilla_Operaciones_RF, lFila, "id_linea")) Then
          'GoTo ErrProcedure
        End If

      End If
    Next
  End With
  
  With Grilla_Operaciones_Sin_Cuenta_RF
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      'Modificado por MMA. 12/01/09. Error en asociaci�n de par�metro. Se especificaba
      'Grilla_Operaciones_RF para obtener "id_linea" en vez de Grilla_Operaciones_Sin_Cuenta_RF
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked And _
        Not GetCell(Grilla_Operaciones_Sin_Cuenta_RF, lFila, "GRABADO") = gcFlg_SI Then
        If Not Fnt_Ope_Directa_Cont_RF(pGrilla:=Grilla_Operaciones_Sin_Cuenta_RF, _
                                       pFila:=lFila, _
                                       pNro_Orden:=GetCell(Grilla_Operaciones_Sin_Cuenta_RF, lFila, "n_orden"), _
                                       pLinea:=GetCell(Grilla_Operaciones_Sin_Cuenta_RF, lFila, "id_linea")) Then

          'GoTo ErrProcedure
        End If
      End If
    Next
  End With
  MsgBox "Grabaci�n de Movimientos Diarios de Renta Fija finalizada.", vbInformation
  
  Tab_Operaciones.TabEnabled(0) = True
  Tab_Operaciones.TabEnabled(2) = True
  
  Call Grilla_Operaciones_RF.Select(0, 0)
  Call Grilla_Operaciones_Sin_Cuenta_RF.Select(0, 0)
  
  'Call Sub_Limpia_Objetos
  
  'Call Toolbar_ButtonClick("REFRESH")
End Sub

Private Function Fnt_Ope_Directa_Cont_RF(pGrilla As VSFlexGrid _
                                       , pFila As Long _
                                       , ByVal pNro_Orden _
                                       , ByVal pLinea As Long) As Boolean
Dim lcBonos     As Class_Bonos
Dim lcDepositos As Class_Depositos
Dim lcPactos    As Class_Pactos
'------------------------------------------------
Dim lcCaja_Cuenta       As Class_Cajas_Cuenta
Dim lId_Caja_Cuenta     As Double
Dim lId_Nemotecnico     As String
Dim lId_Mov_Activo      As String
Dim lFecha_Movimiento   As Date
Dim lFecha_Liquidacion  As Date
Dim lMsg_Error          As String
'---------------------------------------
Dim lId_Cuenta
Dim lNum_Cuenta
Dim lFecha_Vencimiento  As Date
Dim lId_Moneda_Pago     As String
'---------------------------------------
Dim lId_Operacion       As String
Dim lcOperaciones       As Class_Operaciones
Dim lDetalle            As Class_Operaciones_Detalle
Dim lRollback           As Boolean
Dim lMonto_Operacion    As Double
Dim lId_Moneda_Deposito
Dim lBase
'---------------------------------------
Dim lcIva                 As Class_Iva
Dim lcComisiones          As Class_Comisiones_Instrumentos
Dim lPorcentaje_Comision  As Double
Dim lGastos               As Double
Dim lComision             As Double
Dim lDerechos             As Double
Dim lIva                  As Double
'---------------------------------------
Dim lFila             As Long
Dim lRecord_Detalle   As hRecord
Dim lhActivos         As hRecord
Dim lReg_Activo       As hFields
Dim lReg              As hFields
Dim lCod_Instrumento  As String
    
  Set lRecord_Detalle = New hRecord
  With lRecord_Detalle
      .ClearFields
      .AddField "id_nemotecnico", 0
      .AddField "CANTIDAD", 0
      .AddField "PRECIO", 0
      .AddField "MONTO_DETALLE", 0
      .AddField "ID_MONEDA"
      .AddField "ID_MOV_ACTIVO"
      .AddField "COMISION", 0
      .AddField "DERECHO", 0
      .AddField "GASTOS", 0
      .AddField "IVA", 0
      .AddField "MONTO_OPERACION", 0
      .AddField "fecha_vencimiento"
      .AddField "Base"
      .LimpiarRegistros
  End With

  lRollback = False
  gDB.IniciarTransaccion
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Rem Realiza la operacion directa

  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("fecha_operacion").Valor = DTP_Fecha_Proceso.Value
    .Campo("cod_producto").Valor = gcPROD_RF_NAC
    If Not .Anular_Operaciones_Pendientes Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en buscar el Nemot�cnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  Set lcOperaciones = Nothing
    
  lFila = pFila
  lId_Cuenta = GetCell(pGrilla, pFila, "id_cuenta")
  lNum_Cuenta = GetCell(pGrilla, pFila, "num_cuenta")
  lFecha_Movimiento = DTP_Fecha_Proceso.Value
  lFecha_Liquidacion = GetCell(pGrilla, pFila, "fecha_liquidacion")
  lCod_Instrumento = GetCell(pGrilla, pFila, "cod_instru")
  lId_Mov_Activo = GetCell(pGrilla, pFila, "Id_Mov_Activo")
  
  Rem Ingresa el movimiento
  Do While To_Number(GetCell(pGrilla, lFila, "n_orden")) = pNro_Orden And To_Number(GetCell(pGrilla, lFila, "id_linea")) = pLinea
'    lFecha = GetCell(pGrilla, lFila, "fecha")
    lId_Nemotecnico = GetCell(pGrilla, lFila, "id_nemotecnico")

    '-----------------------------------------------------------------------------------------
    Rem VALIDACIONES
    lMsg_Error = ""
    Rem Valida que no se est� ingresando la operacion de nuevo
    If Not Fnt_Buscar_Rel_Nro_Oper_Detalle(cOrigen_BAC_Fondos_BBVA, GetCell(pGrilla, lFila, "colum_pk"), lMsg_Error) Then
      lMsg_Error = "El N�mero de Orden '" & pNro_Orden & "' ya est� ingresada en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
      GoTo ErrProcedure
    Rem La Fecha que viene en el Archivo Plano tiene q ser igual a la Fecha Proceso
    Rem Si el Id_Cuenta y Num_cuenta es vacio quiere decir que no tiene asociada una cuenta en GPI
    ElseIf lId_Cuenta = "" And lNum_Cuenta = "" Then
      lMsg_Error = "La Orden no tiene asociado una cuenta en el sistema. El campo ""Cuenta"" est� vacio." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Si el id_nemot�cnico es "0" quiere decir que no se encontr� el nemot�cnico en GPI
    ElseIf lId_Nemotecnico = "0" Then
      Rem Si no encontro el nemotecnico, se crea. Solo para depositos y pactos
      If lCod_Instrumento = gcINST_DEPOSITOS_NAC Or lCod_Instrumento = gcINST_PACTOS_NAC Then
        If Not Fnt_VerificaCreacion_Nemo(pNemotecnico:=GetCell(pGrilla, lFila, "nemotecnico") _
                                      , pFecha_Movimiento:=lFecha_Movimiento _
                                      , pCod_Instrumento:=lCod_Instrumento _
                                      , pTasa:=To_Number(GetCell(pGrilla, pFila, "precio")) _
                                      , pId_Nemotecnico:=lId_Nemotecnico _
                                      , PId_Moneda_Deposito:=lId_Moneda_Deposito _
                                      , pId_Moneda_Pago:=lId_Moneda_Pago _
                                      , pFecha_Vencimiento:=lFecha_Vencimiento _
                                      , pBase:=lBase _
                                      , pMsgError:=lMsg_Error) Then
            lMsg_Error = "El Nemot�cnico '" & GetCell(pGrilla, lFila, "nemotecnico") & "' de la grilla ""Operaciones con Cuenta"" no est� en el sistema." & vbCr & "No se puede operar la instrucci�n."
            GoTo ErrProcedure
        End If
      Else
        lMsg_Error = "El Nemot�cnico '" & GetCell(pGrilla, lFila, "NEMOTECNICO") & "' no est� en el sistema." & vbCr & "No se puede operar la instrucci�n."
        GoTo ErrProcedure
      End If
    End If
    Rem La Fecha de Liquidacion es vacia, no se puede operar
    If GetCell(pGrilla, lFila, "fecha_liquidacion") = "" Then
      lMsg_Error = "La Fecha de Liquidaci�n no puede estar vacia. No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem La Fecha de Liquidacion es "00/00/0000", no se puede operar
    ElseIf GetCell(pGrilla, lFila, "fecha_liquidacion") = "00/00/0000" Then
      lMsg_Error = "La Fecha de Liquidaci�n no es v�lida. No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Busca la moneda del nemot�cnico en CSGPI
    ElseIf Not Fnt_Buscar_Datos_Nemo_RF(lId_Nemotecnico, lId_Moneda_Pago, lFecha_Vencimiento, lMsg_Error) Then
      GoTo ErrProcedure
    End If
    '-----------------------------------------------------------------------------
'    '-----------------------------------------------------------------------------
  
    Set lReg = lRecord_Detalle.Add
    lReg("id_nemotecnico").Value = lId_Nemotecnico
    lReg("CANTIDAD").Value = To_Number(GetCell(pGrilla, lFila, "Cantidad"))
    lReg("PRECIO").Value = To_Number(GetCell(pGrilla, lFila, "precio"))
    lReg("MONTO_DETALLE").Value = To_Number(GetCell(pGrilla, lFila, "monto"))
    lReg("ID_MONEDA").Value = lId_Moneda_Pago
    lReg("COMISION").Value = To_Number(GetCell(pGrilla, lFila, "comision"))
    lReg("DERECHO").Value = To_Number(GetCell(pGrilla, lFila, "Derecho"))
    lReg("GASTOS").Value = To_Number(GetCell(pGrilla, lFila, "gastos"))
    lReg("IVA").Value = To_Number(GetCell(pGrilla, lFila, "IVA"))
    lReg("MONTO_OPERACION").Value = To_Number(GetCell(pGrilla, lFila, "monto"))
    lReg("fecha_vencimiento").Value = lFecha_Vencimiento
    lReg("Base").Value = lBase
    lReg("ID_MOV_ACTIVO").Value = To_Number(GetCell(pGrilla, lFila, "id_Mov_Activo")) 'lid_Mov_Activo
    lFila = lFila + 1
    If lFila > pGrilla.Rows - 1 Then
        Exit Do
    End If
  Loop
  
  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("id_moneda").Valor = lId_Moneda_Pago
    .Campo("cod_mercado").Valor = cMercado_Nacional
    If .Buscar_Caja_Para_Invertir() Then
      lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
    Else
      lMsg_Error = "Problemas en cargar Cajas de la Cuenta." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  '-----------------------------------------------------------------------------
  
  Rem Comisiones
  Set lcComisiones = New Class_Comisiones_Instrumentos
  Set lcIva = New Class_Iva
  With lcComisiones
      .Campo("Id_Cuenta").Valor = lId_Cuenta
      .Campo("COD_INSTRUMENTO").Valor = lCod_Instrumento
      If .Buscar(True) Then
          If .Cursor.Count > 0 Then
              lPorcentaje_Comision = lcIva.Porcentaje_Iva(.Cursor(1)("COMISION").Value)
          End If
      End If
  End With
  Set lcComisiones = Nothing
  '-----------------------------------------------------------------------------
  
  For Each lReg In lRecord_Detalle
    Select Case lCod_Instrumento
      Case gcINST_BONOS_NAC
      Set lcBonos = New Class_Bonos
        With lcBonos
          If GetCell(pGrilla, pFila, "flg_tipo_movimiento") = gcTipoOperacion_Ingreso Then
            Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                              pCantidad:=lReg("cantidad").Value, _
                                              pTasa:=lReg("precio").Value, _
                                              pId_Moneda:=lReg("id_moneda").Value, _
                                              pMonto:=lReg("monto_detalle").Value, _
                                              pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                              pUtilidad:=0, _
                                              PTasa_Gestion:=lReg("precio").Value, _
                                              pTasa_Historico:=lReg("precio").Value, _
                                              pId_Mov_Activo_Compra:=Null)
            lComision = lComision + lReg("COMISION").Value
            lDerechos = lDerechos + lReg("DERECHO").Value
            lGastos = lGastos + lReg("GASTOS").Value
            lIva = lIva + lReg("IVA").Value
            lMonto_Operacion = lReg("MONTO_OPERACION").Value
          Else
            If Not Fnt_Enlaza_VentasCompras_RF(pCod_Instrumento:=lCod_Instrumento, _
                                               pId_Cuenta:=lId_Cuenta, _
                                               pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                               pCantidad:=lReg("cantidad").Value, _
                                               pFecha_Movimiento:=lFecha_Movimiento, _
                                               phActivos:=lhActivos, _
                                               pId_Mov_Activo:=lReg("id_mov_activo").Value) Then
              Call Fnt_MsgError(.SubTipo_LOG, _
                                "Problemas en buscar el enlaze para las ventas de RF.", _
                                .ErrMsg, _
                                pConLog:=True)
              GoTo ErrProcedure
            End If
          
            For Each lReg_Activo In lhActivos
              Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                pCantidad:=lReg_Activo("asignado").Value, _
                                                pTasa:=lReg("precio").Value, _
                                                pId_Moneda:=lReg("id_moneda").Value, _
                                                pMonto:=lReg("monto_detalle").Value, _
                                                pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                pUtilidad:=0, _
                                                PTasa_Gestion:=lReg("precio").Value, _
                                                pTasa_Historico:=lReg("precio").Value, _
                                                pId_Mov_Activo_Compra:=lReg_Activo("Id_Mov_Activo").Value)
              lComision = lComision + lReg("COMISION").Value
              lDerechos = lDerechos + lReg("DERECHO").Value
              lGastos = lGastos + lReg("GASTOS").Value
              lIva = lIva + lReg("IVA").Value
              lMonto_Operacion = lReg("MONTO_OPERACION").Value
            Next
          End If
          
          If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                            pId_Cuenta:=lId_Cuenta, _
                                            pDsc_Operacion:="", _
                                            pTipoOperacion:=GetCell(pGrilla, pFila, "flg_tipo_movimiento"), _
                                            pId_Contraparte:="", _
                                            pId_Representante:="", _
                                            pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                            pFecha_Operacion:=lFecha_Movimiento, _
                                            pFecha_Liquidacion:=lFecha_Liquidacion, _
                                            pId_Trader:="", _
                                            pPorc_Comision:=lPorcentaje_Comision, _
                                            pComision:=lComision, _
                                            pDerechos_Bolsa:=lDerechos, _
                                            pGastos:=lGastos, _
                                            pIva:=lIva, _
                                            pMonto_Operacion:=lMonto_Operacion, _
                                            pTipo_Precio:=cTipo_Precio_Mercado) Then
              lRollback = True
              Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas al grabar.", _
                          .ErrMsg, _
                          pConLog:=True)
              GoTo ErrProcedure
          End If
        End With
        Rem Con el nuevo id_operacion busca los detalles
        Set lcOperaciones = New Class_Operaciones
        With lcOperaciones
          .Campo("Id_Operacion").Valor = lId_Operacion
        
          Rem Busca el detalle de la operacion
          If Not .BuscaConDetalles Then
            lRollback = True
            GoTo ErrProcedure
          End If
          
          Rem Realiza la Confirmaci�n de la Instrucci�n
          If Not .Confirmar(lId_Caja_Cuenta) Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Error en la confirmaci�n de la operaci�n.", _
                              .ErrMsg, _
                              pConLog:=True)
            GoTo ErrProcedure
          End If
        
          lFila = pFila
          For Each lDetalle In lcOperaciones.Detalles
            For lFila = lFila To (pGrilla.Rows - 1)
              If GetCell(pGrilla, lFila, "n_orden") = pNro_Orden And GetCell(pGrilla, lFila, "id_linea") = pLinea Then
                If Not Fnt_Guardar_Rel_Nro_Oper_Detalle_RF(GetCell(pGrilla, lFila, "colum_pk"), _
                                                           lDetalle.Campo("id_operacion_detalle").Valor, _
                                                           lMsg_Error) Then
                  GoTo ErrProcedure
                End If
                lFila = lFila + 1
                Exit For
              End If
            Next
          Next
        End With
        Set lcOperaciones = Nothing
        
      Case gcINST_DEPOSITOS_NAC
        Set lcDepositos = New Class_Depositos
        With lcDepositos
          If GetCell(pGrilla, pFila, "flg_tipo_movimiento") = gcTipoOperacion_Ingreso Then
            Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                              pCantidad:=lReg("CANTIDAD").Value, _
                                              pTasa:=lReg("precio").Value, _
                                              PTasa_Gestion:=lReg("precio").Value, _
                                              pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                              pBase:=lReg("Base").Value, _
                                              pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                              pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                              pMonto_Pago:=lReg("monto_detalle").Value, _
                                              pReferenciado:="F", _
                                              pTipo_Deposito:="", _
                                              pFecha_Valuta:=lFecha_Liquidacion, _
                                              pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                              pTasa_Historico:=lReg("precio").Value, _
                                              pId_Mov_Activo_Compra:=Null)
            lComision = lComision + lReg("COMISION").Value
            lDerechos = lDerechos + lReg("DERECHO").Value
            lGastos = lGastos + lReg("GASTOS").Value
            lIva = lIva + lReg("IVA").Value
            lMonto_Operacion = lReg("MONTO_OPERACION").Value
          Else
            If Not Fnt_Enlaza_VentasCompras_RF(pCod_Instrumento:=lCod_Instrumento, _
                                               pId_Cuenta:=lId_Cuenta, _
                                               pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                pCantidad:=lReg("cantidad").Value, _
                                               pFecha_Movimiento:=lFecha_Movimiento, _
                                               phActivos:=lhActivos, _
                                               pId_Mov_Activo:=lReg("id_mov_activo").Value) Then
              Call Fnt_MsgError(0, _
                                "Problemas en buscar el enlaze para las ventas de RF.", _
                                "", _
                                pConLog:=True)
              GoTo ErrProcedure
            End If
          
            For Each lReg_Activo In lhActivos
              Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                pCantidad:=lReg_Activo("asignado").Value, _
                                                pTasa:=lReg("precio").Value, _
                                                PTasa_Gestion:=lReg("precio").Value, _
                                                pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                                pBase:=lReg("Base").Value, _
                                                pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                                pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                                pMonto_Pago:=lReg("monto_detalle").Value, _
                                                pReferenciado:="F", _
                                                pTipo_Deposito:="", _
                                                pFecha_Valuta:=lFecha_Liquidacion, _
                                                pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                pTasa_Historico:=lReg("precio").Value, _
                                                pId_Mov_Activo_Compra:=lReg_Activo("Id_Mov_Activo").Value)
  
              lComision = lComision + lReg("COMISION").Value
              lDerechos = lDerechos + lReg("DERECHO").Value
              lGastos = lGastos + lReg("GASTOS").Value
              lIva = lIva + lReg("IVA").Value
              lMonto_Operacion = lReg("MONTO_OPERACION").Value
            Next
          End If
          
          If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                            pId_Cuenta:=lId_Cuenta, _
                                            pDsc_Operacion:="", _
                                            pTipoOperacion:=GetCell(pGrilla, pFila, "flg_tipo_movimiento"), _
                                            pId_Contraparte:="", _
                                            pId_Representante:="", _
                                            pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                            pFecha_Operacion:=lFecha_Movimiento, _
                                            pFecha_Liquidacion:=lFecha_Liquidacion, _
                                            pId_Trader:="", _
                                            pPorc_Comision:=lPorcentaje_Comision, _
                                            pComision:=lComision, _
                                            pDerechos_Bolsa:=lDerechos, _
                                            pGastos:=lGastos, _
                                            pIva:=lIva, _
                                            pMonto_Operacion:=lMonto_Operacion) Then
              lRollback = True
              Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas al grabar.", _
                          .ErrMsg, _
                          pConLog:=True)
              GoTo ErrProcedure
          End If
        End With
        Rem Con el nuevo id_operacion busca los detalles
        Set lcOperaciones = New Class_Operaciones
        With lcOperaciones
          .Campo("Id_Operacion").Valor = lId_Operacion
        
          Rem Busca el detalle de la operacion
          If Not .BuscaConDetalles Then
            lRollback = True
            GoTo ErrProcedure
          End If
          
          Rem Realiza la Confirmaci�n de la Instrucci�n
          If Not .Confirmar(lId_Caja_Cuenta) Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Error en la confirmaci�n de la operaci�n.", _
                              .ErrMsg, _
                              pConLog:=True)
            GoTo ErrProcedure
          End If
        
          lFila = pFila
          For Each lDetalle In lcOperaciones.Detalles
            For lFila = lFila To (pGrilla.Rows - 1)
              If GetCell(pGrilla, lFila, "n_orden") = pNro_Orden Then
                If Not Fnt_Guardar_Rel_Nro_Oper_Detalle_RF(GetCell(pGrilla, lFila, "colum_pk"), _
                                                           lDetalle.Campo("id_operacion_detalle").Valor, _
                                                           lMsg_Error) Then
                  GoTo ErrProcedure
                End If
                'Exit For
              End If
            Next
          Next
        End With
        Set lcOperaciones = Nothing
    
    Case gcINST_PACTOS_NAC
        Set lcPactos = New Class_Pactos
        With lcPactos
          If GetCell(pGrilla, pFila, "flg_tipo_movimiento") = gcTipoOperacion_Ingreso Then
            Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                              pCantidad:=lReg("CANTIDAD").Value, _
                                              pTasa:=lReg("precio").Value, _
                                              PTasa_Gestion:=lReg("precio").Value, _
                                              pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                              pBase:=lReg("Base").Value, _
                                              pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                              pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                              pMonto_Pago:=lReg("monto_detalle").Value, _
                                              pReferenciado:="F", _
                                              pTipo_Deposito:="", _
                                              pFecha_Valuta:=lFecha_Liquidacion, _
                                              pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                              pTasa_Historica:=lReg("precio").Value, _
                                              pId_Mov_Activo_Compra:=Null)
            lComision = lComision + lReg("COMISION").Value
            lDerechos = lDerechos + lReg("DERECHO").Value
            lGastos = lGastos + lReg("GASTOS").Value
            lIva = lIva + lReg("IVA").Value
            lMonto_Operacion = lReg("MONTO_OPERACION").Value
          Else
            If Not Fnt_Enlaza_VentasCompras_RF(pCod_Instrumento:=lCod_Instrumento, _
                                               pId_Cuenta:=lId_Cuenta, _
                                               pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                               pCantidad:=lReg("cantidad").Value, _
                                               pFecha_Movimiento:=lFecha_Movimiento, _
                                               phActivos:=lhActivos, _
                                               pId_Mov_Activo:=lReg("id_mov_activo").Value) Then
              Call Fnt_MsgError(0, _
                                "Problemas en buscar el enlaze para las ventas de RF.", _
                                "", _
                                pConLog:=True)
              GoTo ErrProcedure
            End If
          
            For Each lReg_Activo In lhActivos
              Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                pCantidad:=lReg_Activo("asignado").Value, _
                                                pTasa:=lReg("precio").Value, _
                                                PTasa_Gestion:=lReg("precio").Value, _
                                                pPlazo:=(lReg("fecha_vencimiento").Value - lFecha_Movimiento), _
                                                pBase:=lReg("Base").Value, _
                                                pFecha_Vencimiento:=lReg("fecha_vencimiento").Value, _
                                                pId_Moneda_Pago:=lReg("id_moneda").Value, _
                                                pMonto_Pago:=lReg("monto_detalle").Value, _
                                                pReferenciado:="F", _
                                                pTipo_Deposito:="", _
                                                pFecha_Valuta:=lFecha_Liquidacion, _
                                                pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                pTasa_Historica:=lReg("precio").Value, _
                                                pId_Mov_Activo_Compra:=lReg_Activo("Id_Mov_Activo").Value)
  
              lComision = lComision + lReg("COMISION").Value
              lDerechos = lDerechos + lReg("DERECHO").Value
              lGastos = lGastos + lReg("GASTOS").Value
              lIva = lIva + lReg("IVA").Value
              lMonto_Operacion = lReg("MONTO_OPERACION").Value
            Next
          End If
          
          If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                                pId_Cuenta:=lId_Cuenta, _
                                                pDsc_Operacion:="", _
                                                pTipoOperacion:=GetCell(pGrilla, pFila, "flg_tipo_movimiento"), _
                                                pId_Contraparte:="", _
                                                pId_Representante:="", _
                                                pId_Moneda_Operacion:=lId_Moneda_Pago, _
                                                pFecha_Operacion:=lFecha_Movimiento, _
                                                pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                pId_Trader:="", _
                                                pPorc_Comision:=lPorcentaje_Comision, _
                                                pComision:=lComision, _
                                                pDerechos:=lDerechos, _
                                                pGastos:=lGastos, _
                                                pIva:=lIva, _
                                                pMonto_Operacion:=lMonto_Operacion) Then
              lRollback = True
              Call Fnt_MsgError(.SubTipo_LOG, _
                                "Problemas al grabar.", _
                                .ErrMsg, _
                                pConLog:=True)
              GoTo ErrProcedure
          End If
        End With
        Rem Con el nuevo id_operacion busca los detalles
        Set lcOperaciones = New Class_Operaciones
        With lcOperaciones
          .Campo("Id_Operacion").Valor = lId_Operacion
        
          Rem Busca el detalle de la operacion
          If Not .BuscaConDetalles Then
            lRollback = True
            GoTo ErrProcedure
          End If
          
          Rem Realiza la Confirmaci�n de la Instrucci�n
          If Not .Confirmar(lId_Caja_Cuenta) Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Error en la confirmaci�n de la operaci�n.", _
                              .ErrMsg, _
                              pConLog:=True)
            GoTo ErrProcedure
          End If
        
          lFila = pFila
          For Each lDetalle In lcOperaciones.Detalles
            For lFila = lFila To (pGrilla.Rows - 1)
              If GetCell(pGrilla, lFila, "n_orden") = pNro_Orden Then
                If Not Fnt_Guardar_Rel_Nro_Oper_Detalle_RF(GetCell(pGrilla, lFila, "colum_pk"), _
                                                           lDetalle.Campo("id_operacion_detalle").Valor, _
                                                           lMsg_Error) Then
                  GoTo ErrProcedure
                End If
                lFila = lFila + 1
                Exit For
              End If
            Next
          Next
        End With
        Set lcOperaciones = Nothing
        
    End Select
  Next
  
  lMsg_Error = "Operaci�n Ingresada correctamente."
  GoTo ExitProcedure

ErrProcedure:
  lRollback = True
  
ExitProcedure:
  For lFila = pFila To (pGrilla.Rows - 1)
    If To_Number(GetCell(pGrilla, lFila, "n_orden")) = pNro_Orden And GetCell(pGrilla, lFila, "id_linea") = pLinea Then
      Call SetCell(pGrilla, lFila, "dsc_error", lMsg_Error)
      
      If lRollback Then
        pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbRed
        Call SetCell(pGrilla, lFila, "chk", flexChecked)
      Else
        pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbBlue
        Call SetCell(pGrilla, lFila, "chk", flexUnchecked)
        Call SetCell(pGrilla, lFila, "grabado", gcFlg_SI, pAutoSize:=False)
      End If
    End If
  Next
  
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  Set lcBonos = Nothing
  Set lcDepositos = Nothing
  Set lcPactos = Nothing
  
  Fnt_Ope_Directa_Cont_RF = Not lRollback
  
End Function

Private Sub Toolbar_Operaciones_FM_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "IMPORT"
      DTP_Fecha_Proceso.Enabled = False
      Tab_Operaciones.TabEnabled(0) = False
      Tab_Operaciones.TabEnabled(1) = False
      With Toolbar_Operaciones_FM
          .Buttons("SAVE").Enabled = True
          .Buttons("IMPORT").Enabled = False
          .Buttons("SEL_ALL").Enabled = True
          .Buttons("SEL_NOTHING").Enabled = True
          .Buttons("REFRESH").Enabled = True
      End With
      Call Sub_Importa_FM
      Call Habilita_Reporte_Asesor
    Case "REFRESH"
        DTP_Fecha_Proceso.Enabled = True
        Tab_Operaciones.TabEnabled(0) = True
        Tab_Operaciones.TabEnabled(1) = True
        With Toolbar_Operaciones_FM
            .Buttons("SAVE").Enabled = False
            .Buttons("IMPORT").Enabled = True
            .Buttons("SEL_ALL").Enabled = False
            .Buttons("SEL_NOTHING").Enabled = False
            .Buttons("REFRESH").Enabled = False
            .Buttons("REPORT").Enabled = False
        End With
        Call Sub_Limpia_Objetos
    Case "SEL_ALL"
        Call Sub_CambiaCheck(Grilla_Operaciones_FM, True)
    Case "SEL_NOTHING"
        Call Sub_CambiaCheck(Grilla_Operaciones_FM, False)
    Case "SAVE"
        Toolbar_Operaciones_FM.Buttons("REPORT").Enabled = False
        Call Sub_Graba_Operaciones_con_Cta_FM
    Case "REPORT"
        Call Sub_Reporte_Asesor(Grilla_Operaciones_FM, Grilla_Operaciones_Sin_Cuenta_FM, "FM")
  End Select

End Sub

Private Sub Sub_Importa_FM()
Dim lcNemotecnicos        As Class_Nemotecnicos
Dim lcCuenta              As Object
Dim lReg                  As hFields
Dim lCursor               As hRecord
Dim lGrilla               As VSFlexGrid
Dim lFecha_Movimiento     As Date
Dim lLinea                As Long
Dim lTipo_Movimiento      As String
Dim lCod_Instrumento      As String
Dim lId_Nemotecnico
Dim lId_Cuenta
Dim lFolioContraparte
Dim lNum_Cuenta           As String
Dim lNemotecnico          As String
Dim lFlg_Tipo_Movimiento  As String
Dim lRut_Cliente          As String
Dim lNombre_Cliente       As String
Dim lFlg_Estado           As String
Dim lDsc_Bloqueo          As String

On Error GoTo ErrProcedure
  
  Me.Enabled = False
  Call Sub_Bloquea_Puntero(Me)
  
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  Set lcNemotecnicos = New Class_Nemotecnicos
  
  lFecha_Movimiento = DTP_Fecha_Proceso.Value
  
  Set lcAlias = CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB

  gDB.Parametros.Clear
  gDB.Procedimiento = "leaspadb&DBO&SP_OBTIENE_MOV_DIARIOS_CLIENTES_ADC"
  gDB.Parametros.Add "Fechamovimiento", ePT_Caracter, Format(DTP_Fecha_Proceso.Value, "YYYYMMDD"), ePD_Entrada
  gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
  If Not gDB.EjecutaSP Then
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
    GoTo ExitProcedure
  End If
  
  Set lCursor = gDB.Parametros("pcursor").Valor
  
  If lCursor.Count <= 0 Then
    MsgBox "No hay movimientos para la fecha seleccionada.", vbInformation, Me.Caption
    GoTo ExitProcedure
  End If
  
  BarraProceso.Max = lCursor.Count
  For Each lReg In lCursor
    BarraProceso.Value = lReg.Index
    
    lTipo_Movimiento = ""
    Select Case Trim(lReg("TIPO_MOVIMIENTO").Value)
      Case "IN"
        lTipo_Movimiento = "Inversion"
        lFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso
      Case "RE"
        lTipo_Movimiento = "Retiro"
        lFlg_Tipo_Movimiento = gcTipoOperacion_Egreso
      Case Else
        'Si un tipo de movimiento no esta contemplado se salta
        GoTo ProximoMovimiento
    End Select
    
    lFolioContraparte = Trim(lReg("FOLIO_OPERACION").Value)
    lNemotecnico = UCase(Trim(lReg("FONDO_MUTUO").Value))
    '------- RESCATA EL ID_nemot�cnico
    Rem Busca el id_nemot�cnico del nemot�cnico de la planilla
    With lcNemotecnicos
      .Campo("nemotecnico").Valor = lNemotecnico
      If Not .Buscar Then
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en buscar el Nemot�cnico.", _
                          .ErrMsg, _
                          pConLog:=True)
        GoTo ExitProcedure
      End If
    
      If .Cursor.Count > 0 Then
        lId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
        lCod_Instrumento = .Cursor(1)("cod_instrumento").Value
      End If
    End With
    '---------------------------------------------------------------------------------------
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_CDS.IMPORT_AVAIBLE"
    gDB.Parametros.Add "PID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    gDB.Parametros.Add "PVALOR", ePT_Caracter, lFolioContraparte, ePD_Entrada
    gDB.Parametros.Add "PCOD_INSTRUMENTO", ePT_Caracter, lCod_Instrumento, ePD_Entrada
    gDB.Parametros.Add "PRETURN", ePT_Caracter, "", ePD_Salida
    If Not gDB.EjecutaSP Then
      Call Fnt_MsgError(eLS_ErrSystem, _
                        "Problemas para comprobar si esta disponible la importaci�n.", _
                        gDB.ErrMsg, _
                        pConLog:=True)
      GoTo ProximoMovimiento
    End If
    
    If Not gDB.Parametros("PRETURN").Valor = gcFlg_SI Then
      'SI ENTREGA UN VALOR DISTINTO A "S" SIGNIFICA QUE NO TIENE PERMITIDO EL INGRESO
      GoTo ProximoMovimiento
    End If
    
    '---------------------------------------------------------------------------------------
    lRut_Cliente = Trim(lReg("RUT").Value) & "-" & Trim(lReg("DV").Value) & "/" & Trim(lReg("cuenta").Value)
    lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Participe_FFMM _
                                         , pCodigoCSBPI:=cTabla_Cuentas _
                                         , pValor:=lRut_Cliente)
    
    lNum_Cuenta = ""
    lNombre_Cliente = ""
    lid_Empresa = 0
    If IsNull(lId_Cuenta) Then
      Set lGrilla = Grilla_Operaciones_Sin_Cuenta_FM
      lNombre_Cliente = Trim(lReg("nombre_cliente").Value)
    Else
      Set lGrilla = Grilla_Operaciones_FM
      ' ----------- RESCATA NUMERO CUENTA
      lcCuenta.Campo("id_cuenta").Valor = lId_Cuenta
      If lcCuenta.Buscar(pEnVista:=True) Then
        lNum_Cuenta = lcCuenta.Cursor(1)("num_cuenta").Value
        lNombre_Cliente = lcCuenta.Cursor(1)("nombre_cliente").Value
        lFlg_Estado = lcCuenta.Cursor(1)("cod_estado").Value
        lDsc_Bloqueo = ""
        lid_Empresa = lcCuenta.Cursor(1)("id_Empresa").Value
        If lFlg_Estado = "H" Then
            If lcCuenta.Cursor(1)("flg_bloqueado").Value = "S" Then
                lFlg_Estado = "B"
                lDsc_Bloqueo = lcCuenta.Cursor(1)("obs_bloqueo").Value
            End If
        Else
            lDsc_Bloqueo = "Cuenta Deshabilitada"
        End If
        If lid_Empresa <> Fnt_EmpresaActual Then
            GoTo ProximoMovimiento
        End If
      End If
    End If
    
    lLinea = lGrilla.Rows
    Call lGrilla.AddItem("")
    Call SetCell(lGrilla, lLinea, "colum_pk", lReg("FOLIO_OPERACION").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "n_orden", lReg("FOLIO_OPERACION").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "rut", lRut_Cliente, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "nombre_cliente", lNombre_Cliente, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_cuenta", "" & lId_Cuenta, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "num_cuenta", lNum_Cuenta, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "tipo_movimiento", lTipo_Movimiento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "flg_tipo_movimiento", lFlg_Tipo_Movimiento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_nemotecnico", lId_Nemotecnico, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "nemotecnico", lNemotecnico, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "cantidad", lReg("MONTO_CUOTAS").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "PRECIO", lReg("VALOR_CUOTA").Value, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "monto", NVL(lReg("MONTO").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "id_moneda_operacion", NVL(lReg("MONEDA").Value, 0), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "moneda_inver", Trim("" & lReg("MONEDA").Value), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "fecha_liquidacion", Trim(lReg("FECHA_LIQUIDACION").Value), pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "dsc_error", "", pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "cod_instru", lCod_Instrumento, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "Grabado", gcFlg_NO, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "ORI_MOV", lReg("ORIGEN_MOVIMIENTO").Value, pAutoSize:=False)
    
    Call SetCell(lGrilla, lLinea, "flg_bloqueado", lFlg_Estado, pAutoSize:=False)
    Call SetCell(lGrilla, lLinea, "dsc_error", lDsc_Bloqueo, pAutoSize:=False)
      
ProximoMovimiento:
    'este label sirve para saltarce el ingreso de un de operacion con un tipo de movimiento que no corresponde,
    'como los ingresos de custodias, la otra forma es hacer un if gigante que no quiero hacer :-P
    'HAF: 20071108
  Next
  
  Call Sub_AjustaColumnas_Grilla(Grilla_Operaciones_Sin_Cuenta_FM)
  Call Sub_AjustaColumnas_Grilla(Grilla_Operaciones_FM)
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la importaci�n de los movimientos." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  gDB.Parametros.Clear
  Set lcNemotecnicos = Nothing
  Set lcCuenta = Nothing
  Set lcAlias = Nothing
  
  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Graba_Operaciones_con_Cta_FM()
Dim lFila As Long
  
  With Grilla_Operaciones_FM
    For lFila = 1 To Grilla_Operaciones_FM.Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked And _
          Not GetCell(Grilla_Operaciones_FM, lFila, "GRABADO") = gcFlg_SI Then
        If Not Fnt_Ope_Directa_Cont_FM(pGrilla:=Grilla_Operaciones_FM, _
                                       pFila:=lFila, _
                                       pNro_Orden:=GetCell(Grilla_Operaciones_FM, lFila, "colum_pk")) Then
          'GoTo ErrProcedure
        End If
      End If
    Next
  End With


  With Grilla_Operaciones_Sin_Cuenta_FM
    For lFila = 1 To .Rows - 1
      Rem Busca las operaciones chequeadas
      If .Cell(flexcpChecked, lFila, .ColIndex("chk")) = flexChecked And _
          Not GetCell(Grilla_Operaciones_Sin_Cuenta_FM, lFila, "GRABADO") = gcFlg_SI Then
        If Not Fnt_Ope_Directa_Cont_FM(pGrilla:=Grilla_Operaciones_Sin_Cuenta_FM, _
                                       pFila:=lFila, _
                                       pNro_Orden:=GetCell(Grilla_Operaciones_Sin_Cuenta_FM, lFila, "colum_pk")) Then
          'GoTo ErrProcedure
        End If
      End If
    Next
  End With
  
  MsgBox "Grabaci�n de Movimientos Diarios de Fondos Mutuos finalizada.", vbInformation
  
  Tab_Operaciones.TabEnabled(1) = True
  Tab_Operaciones.TabEnabled(2) = True
  
  Call Grilla_Operaciones_FM.Select(0, 0)
  Call Grilla_Operaciones_Sin_Cuenta_FM.Select(0, 0)
End Sub

Private Function Fnt_Ope_Directa_Cont_FM(pGrilla As VSFlexGrid _
                                       , ByRef pFila As Long _
                                       , ByVal pNro_Orden As Long) As Boolean
Dim lcFondosMutuos      As Class_FondosMutuos
Dim lcCaja_Cuenta       As Class_Cajas_Cuenta
Dim lId_Caja_Cuenta     As Double
Dim lId_Nemotecnico     As String
Dim lFecha_Movimiento   As Date
Dim lFecha_Liquidacion  As Date
Dim lMsg_Error          As String
'---------------------------------------
Dim lId_Cuenta
Dim lNum_Cuenta
Dim lCod_Instrumento
'---------------------------------------
Dim lId_Moneda        As String
Dim lId_Operacion     As String
Dim lcOperaciones     As Class_Operaciones
Dim lDetalle          As Class_Operaciones_Detalle
Dim lRollback         As Boolean
Dim lMonto_Operacion  As Double
'---------------------------------------
Dim lcIva                 As Class_Iva
Dim lcComisiones          As Class_Comisiones_Instrumentos
Dim lPorcentaje_Comision  As Double
'---------------------------------------
Dim lFila           As Long
Dim lRecord_Detalle As hRecord
Dim lReg            As hFields
 
  Set lRecord_Detalle = New hRecord
  With lRecord_Detalle
    .ClearFields
    .AddField "id_nemotecnico", 0
    .AddField "CANTIDAD", 0
    .AddField "PRECIO", 0
    .AddField "MONTO_DETALLE", 0
    .AddField "ID_MONEDA"
    .AddField "MONTO_OPERACION", 0
    .AddField "COD_INSTRU", ""
    .LimpiarRegistros
  End With

  lRollback = False
  gDB.IniciarTransaccion
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Rem Realiza la operacion directa
  
  Rem BORRA TODAS LAS INSTRUCCIONES PENDIENTES DESDE EL DIA DE PROCESO HACIA ATRAS
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("fecha_operacion").Valor = DTP_Fecha_Proceso.Value
    .Campo("cod_producto").Valor = gcPROD_FFMM_NAC
    If Not .Anular_Operaciones_Pendientes Then
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcOperaciones = Nothing
    
  lFila = pFila
  lId_Cuenta = GetCell(pGrilla, pFila, "id_cuenta")
  lNum_Cuenta = GetCell(pGrilla, pFila, "num_cuenta")
  Rem Ingresa el movimiento
  Do While To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pNro_Orden
    lId_Nemotecnico = GetCell(pGrilla, pFila, "id_nemotecnico")
      
    '-----------------------------------------------------------------------------------------
    Rem VALIDACIONES
    lMsg_Error = ""
    lFecha_Movimiento = DTP_Fecha_Proceso.Value
    lFecha_Liquidacion = GetCell(pGrilla, pFila, "FECHA_LIQUIDACION")
    lCod_Instrumento = GetCell(pGrilla, lFila, "cod_instru")
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_CDS.IMPORT_AVAIBLE"
    gDB.Parametros.Add "PID_EMPRESA", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    gDB.Parametros.Add "PVALOR", ePT_Caracter, pNro_Orden, ePD_Entrada
    gDB.Parametros.Add "PCOD_INSTRUMENTO", ePT_Caracter, lCod_Instrumento, ePD_Entrada
    gDB.Parametros.Add "PRETURN", ePT_Caracter, "", ePD_Salida
    If Not gDB.EjecutaSP Then
      lMsg_Error = gDB.ErrMsg
      GoTo ErrProcedure
    End If
          
    If Not gDB.Parametros("PRETURN").Valor = gcFlg_SI Then
      lMsg_Error = "El Folio '" & pNro_Orden & "' ya est� ingresado en el sistema." & vbCr & "No se puede operar la instrucci�n nuevamente."
      GoTo ErrProcedure
    Rem Si el Id_Cuenta y Num_cuenta es vacio quiere decir que no tiene asociada una cuenta en GPI
    ElseIf lId_Cuenta = "" And lNum_Cuenta = "" Then
      lMsg_Error = "El Folio no tiene asociado una cuenta en el sistema, ya que ""Cuenta"" est� vacio." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Si el id_nemot�cnico es "0" quiere decir que no se encontr� el nemot�cnico en GPI
    ElseIf lId_Nemotecnico = "" Then
      lMsg_Error = "El Nemot�cnico '" & GetCell(pGrilla, lFila, "nemotecnico") & "' no est� en el sistema." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem La Fecha de Liquidacion es vacia, no se puede operar
    ElseIf GetCell(pGrilla, lFila, "fecha_liquidacion") = "" Then
      lMsg_Error = "En el Folio '" & pNro_Orden & "' la Fecha de Liquidaci�n es vacia en la grilla ""Operaciones con Cuenta""." & vbCr & "No se puede operar la instrucci�n."
      GoTo ErrProcedure
    Rem Busca la moneda del nemot�cnico en CSGPI
    ElseIf Not Fnt_Buscar_Datos_Nemo(pId_Nemotecnico:=lId_Nemotecnico _
                                   , pId_Moneda_Transaccion:=lId_Moneda _
                                   , pMsg_Error:=lMsg_Error _
                                   , pCod_Instrumento:=lCod_Instrumento) Then
      GoTo ErrProcedure
    End If
    '-----------------------------------------------------------------------------
          
    Set lReg = lRecord_Detalle.Add
    lReg("id_nemotecnico").Value = lId_Nemotecnico
    lReg("CANTIDAD").Value = To_Number(GetCell(pGrilla, pFila, "Cantidad"))
    lReg("PRECIO").Value = To_Number(GetCell(pGrilla, pFila, "precio"))
    lReg("MONTO_DETALLE").Value = To_Number(GetCell(pGrilla, pFila, "monto"))
    lReg("ID_MONEDA").Value = lId_Moneda
    lReg("MONTO_OPERACION").Value = To_Number(GetCell(pGrilla, pFila, "monto"))
    lReg("COD_INSTRU").Value = GetCell(pGrilla, pFila, "cod_instru")
      
    lFila = lFila + 1
    If lFila > pGrilla.Rows - 1 Then
      Exit Do
    End If
  Loop
      
  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("id_moneda").Valor = lId_Moneda
    .Campo("cod_mercado").Valor = cMercado_Nacional
    If .Buscar_Caja_Para_Invertir() Then
      If .Cursor.Count > 0 Then
        lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
      End If
    Else
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  '-----------------------------------------------------------------------------
  
  lFecha_Movimiento = DTP_Fecha_Proceso.Value
  lFecha_Liquidacion = GetCell(pGrilla, pFila, "FECHA_LIQUIDACION")
  
  Set lcFondosMutuos = New Class_FondosMutuos
  With lcFondosMutuos
    For Each lReg In lRecord_Detalle
      Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                        pcuota:=lReg("cantidad").Value, _
                                        pPrecio:=lReg("precio").Value, _
                                        pId_Moneda:=lReg("id_moneda").Value, _
                                        pMonto:=lReg("monto_detalle").Value, _
                                        pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                        pFecha_Liquidacion:=lFecha_Liquidacion, _
                                        pPrecio_Historico:="")
      lMonto_Operacion = lMonto_Operacion + lReg("MONTO_OPERACION").Value
    Next
  
    Rem Comisiones
    Set lcComisiones = New Class_Comisiones_Instrumentos
    Set lcIva = New Class_Iva
    With lcComisiones
      .Campo("Id_Cuenta").Valor = lId_Cuenta
      .Campo("COD_INSTRUMENTO").Valor = lCod_Instrumento
      If .Buscar(True) Then
        If .Cursor.Count > 0 Then
          lPorcentaje_Comision = lcIva.Porcentaje_Iva(.Cursor(1)("COMISION").Value)
        End If
      End If
    End With
    Set lcComisiones = Nothing
    '-----------------------------------------------------------------------------
    If GetCell(pGrilla, pFila, "ORI_MOV") = "DIVIDENDO" Then
        If Not .Realiza_Operacion_Custodia_DividendosFM(pId_Operacion:=lId_Operacion, _
                                                        pId_Cuenta:=lId_Cuenta, _
                                                        pDsc_Operacion:="Ingreso por Importacion", _
                                                        pTipoOperacion:=GetCell(pGrilla, pFila, "flg_tipo_movimiento"), _
                                                        pId_Contraparte:="", _
                                                        pId_Representante:="", _
                                                        pId_Moneda_Operacion:=lId_Moneda, _
                                                        pFecha_Operacion:=lFecha_Movimiento, _
                                                        pFecha_Vigencia:=lFecha_Movimiento, _
                                                        pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                        pId_Trader:="", _
                                                        pPorc_Comision:=lPorcentaje_Comision, _
                                                        pComision:=0, _
                                                        pDerechos_Bolsa:=0, _
                                                        pGastos:=0, _
                                                        pIva:=0, _
                                                        pMonto_Operacion:=lMonto_Operacion, _
                                                        pTipo_Precio:=cTipo_Precio_Mercado, _
                                                        pCod_Instrumento:=lCod_Instrumento) Then
            lMsg_Error = .ErrMsg
            GoTo ErrProcedure
        End If
    ElseIf GetCell(pGrilla, pFila, "ORI_MOV") = "NORMAL" Then
        If Not .Realiza_Operacion_Instruccion(pId_Operacion:=lId_Operacion, _
                                              pId_Cuenta:=lId_Cuenta, _
                                              pDsc_Operacion:="Ingreso por Importacion", _
                                              pTipoOperacion:=GetCell(pGrilla, pFila, "flg_tipo_movimiento"), _
                                              pId_Contraparte:="", _
                                              pId_Representante:="", _
                                              pId_Moneda_Operacion:=lId_Moneda, _
                                              pFecha_Operacion:=lFecha_Movimiento, _
                                              pFecha_Vigencia:=lFecha_Movimiento, _
                                              pFecha_Liquidacion:=lFecha_Liquidacion, _
                                              pId_Trader:="", _
                                              pPorc_Comision:=lPorcentaje_Comision, _
                                              pComision:=0, _
                                              pDerechos_Bolsa:=0, _
                                              pGastos:=0, _
                                              pIva:=0, _
                                              pMonto_Operacion:=lMonto_Operacion, _
                                              pTipo_Precio:=cTipo_Precio_Mercado, _
                                              pCod_Instrumento:=lCod_Instrumento) Then
            lMsg_Error = .ErrMsg
            GoTo ErrProcedure
        End If
    End If
  End With
  '----------------------------------------------------------------------------
    Rem Con el nuevo id_operacion busca los detalles
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Id_Operacion").Valor = lId_Operacion
    
    Rem Busca el detalle de la operacion
    If Not .BuscaConDetalles Then
      lMsg_Error = .ErrMsg
      GoTo ErrProcedure
    End If
      
    Rem Realiza la Confirmaci�n de la Instrucci�n
    If Not GetCell(pGrilla, pFila, "ORI_MOV") = "DIVIDENDO" Then
      If Not .Confirmar(lId_Caja_Cuenta) Then
        lMsg_Error = .ErrMsg
        GoTo ErrProcedure
      End If
    End If
    
    Rem Por cada id_operacion_detalle encontrado guarda la relacion
    Rem "nro_orden-id_operacion_detalle" en la tabla rel_conversiones
    For Each lDetalle In lcOperaciones.Detalles
      If Not Fnt_Guardar_Rel_Nro_Oper_Detalle_FM(GetCell(pGrilla, pFila, "colum_pk"), _
                                                 lDetalle.Campo("id_operacion_detalle").Valor, _
                                                 lMsg_Error) Then
        GoTo ErrProcedure
      End If
    Next
  End With
  '----------------------------------------------------------------------------
  
  lMsg_Error = "Operaci�n Ingresada correctamente."
  GoTo ExitProcedure
  
ErrProcedure:
  lRollback = True
  
ExitProcedure:
  lFila = pFila
  Do While To_Number(GetCell(pGrilla, lFila, "colum_pk")) = pNro_Orden
    Call SetCell(pGrilla, lFila, "dsc_error", lMsg_Error)
      
    If lRollback Then
      pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbRed
      pGrilla.Cell(flexcpChecked, lFila, pGrilla.ColIndex("chk")) = flexChecked
    Else
      pGrilla.Cell(flexcpForeColor, lFila, 1, lFila, pGrilla.Cols - 1) = vbBlue
      pGrilla.Cell(flexcpChecked, lFila, pGrilla.ColIndex("chk")) = flexUnchecked
      Call SetCell(pGrilla, lFila, "grabado", gcFlg_SI, pAutoSize:=False)
    End If
    
    lFila = lFila + 1
    
    If lFila > pGrilla.Rows - 1 Then
      Exit Do
    End If
  Loop
  
  Rem Setea la pFila para que siga con la siguiente Orden de la grilla
  pFila = lFila - 1
  
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  Set lcFondosMutuos = Nothing
  
  Fnt_Ope_Directa_Cont_FM = Not lRollback
  
End Function

Private Function Fnt_Guardar_Rel_Nro_Oper_Detalle_FM(pNro_Orden As Variant, _
                                                    pId_Operacion_Detalle As String, _
                                                    pMsg_Error As String) As Boolean
Dim lId_Origen          As String
Dim lId_Tipo_Conversion As String
Dim lcRel_Conversion    As Object

  Fnt_Guardar_Rel_Nro_Oper_Detalle_FM = True
  
  lId_Origen = Fnt_Busca_Id_Origen(cOrigen_CDS_Mov_FFMM, False)
  lId_Tipo_Conversion = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle, False)
  
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = lId_Origen
    .Campo("Id_Tipo_Conversion").Valor = lId_Tipo_Conversion
    .Campo("valor").Valor = pNro_Orden
    .Campo("id_entidad").Valor = pId_Operacion_Detalle 'En "id_entidad" se guarda el id_operacion_detalle
    If Not .Guardar Then
      pMsg_Error = "Problemas en guardar la relaci�n Folio y el Detalle de la Operaci�n." & vbCr & .ErrMsg
      Fnt_Guardar_Rel_Nro_Oper_Detalle_FM = False
    End If
  End With
  Set lcRel_Conversion = Nothing
  
End Function

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Grilla_Operaciones_Sin_Cuenta_FM.ColIndex("num_cuenta") <> Col Then Cancel = True
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
  Call Sub_Setea_Cta_Nueva(Grilla_Operaciones_Sin_Cuenta_FM)
End Sub

Private Sub Grilla_Operaciones_Sin_Cuenta_FM_Click()
  Call Sub_Check_DesCheck_Mismo_Nro_Orden(Grilla_Operaciones_Sin_Cuenta_FM)
End Sub

Private Sub Sub_Check_DesCheck_Mismo_Nro_Orden_RV(pGrilla As VSFlexGrid)
Dim lFil_grilla As Integer
Dim lCol_grilla As Integer
Dim lLinea As Integer
Dim lNro_Orden As Long
    
    lFil_grilla = pGrilla.Row
    lCol_grilla = pGrilla.Col
    If lCol_grilla = 0 And lFil_grilla > 0 Then
      If Val(GetCell(pGrilla, lFil_grilla, "chk")) = 0 Then
        lNro_Orden = Val(GetCell(pGrilla, lFil_grilla, "colum_pk"))
        For lLinea = 1 To pGrilla.Rows - 1
          If lNro_Orden = Val(GetCell(pGrilla, lLinea, "colum_pk")) Then
            pGrilla.TextMatrix(lLinea, lCol_grilla) = flexChecked
          End If
        Next
      Else
        lNro_Orden = Val(GetCell(pGrilla, lFil_grilla, "colum_pk"))
        For lLinea = 1 To pGrilla.Rows - 1
          If lNro_Orden = Val(GetCell(pGrilla, lLinea, "colum_pk")) Then
            pGrilla.TextMatrix(lLinea, lCol_grilla) = 0
          End If
        Next
      End If
    End If
    
End Sub

Private Sub Sub_Mensaje_Error(pGrilla As VSFlexGrid)
Dim lMensaje As String

  If pGrilla.Row >= 1 Then
    lMensaje = GetCell(pGrilla, pGrilla.Row, "dsc_error")
    If Not lMensaje = "" Then
      MsgBox lMensaje, vbInformation
    End If
  End If
End Sub

Private Sub Toolbar_Selc_Con_Cta_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_RV, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_RV, False)
  End Select
End Sub

Private Sub Toolbar_Selc_Con_Cta_RF_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_RF, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_RF, False)
  End Select
End Sub

Private Sub Toolbar_Selc_Sin_Cta_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RV, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RV, False)
  End Select
End Sub

Private Sub Toolbar_Selc_Sin_Cta_RF_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RF, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Operaciones_Sin_Cuenta_RF, False)
  End Select
End Sub

Private Function Fnt_VerificaCreacion_Nemo(pNemotecnico _
                                         , pFecha_Movimiento _
                                         , pCod_Instrumento _
                                         , pTasa _
                                         , ByRef pId_Nemotecnico _
                                         , ByRef PId_Moneda_Deposito _
                                         , ByRef pId_Moneda_Pago _
                                         , ByRef pFecha_Vencimiento _
                                         , ByRef pBase _
                                         , ByRef pMsgError As String) As Boolean
Dim lcPactos            As Class_Pactos
Dim lcMoneda            As Object
Dim lcEmisor_Especifico As Class_Emisores_Especifico
Dim lcNemotecnico       As Class_Nemotecnicos
'---------------------------------------------------
Dim lNemotecnico
Dim lId_Emisor_Especifico
Dim lFecha_Emision

  Fnt_VerificaCreacion_Nemo = False
  'Verifica si el nemot�cnico se puede crear o no
  Select Case pCod_Instrumento
    Case gcINST_PACTOS_NAC
        Set lcEmisor_Especifico = New Class_Emisores_Especifico
        With lcEmisor_Especifico
          .Campo("COD_SVS_nemotecnico").Valor = "SEC"  'Trim(Mid(pNemotecnico, 3, 4))
          If Not .Buscar Then
            pMsgError = "Problemas en buscar el codigo SVS del emisor." & .ErrMsg
            GoTo ExitProcedure
          End If
          
          If .Cursor.Count <= 0 Then
            pMsgError = "El codigo del emisor no esta definido en el sistema, cree un emisor especifico con el codigo de SVS correspondiente."
            GoTo ExitProcedure
          End If
          
          lId_Emisor_Especifico = .Cursor(1)("id_emisor_especifico").Value
        End With
        Set lcEmisor_Especifico = Nothing
        pFecha_Vencimiento = DateSerial("20" & Mid(pNemotecnico, 11, 2), Mid(pNemotecnico, 14, 2), Mid(pNemotecnico, 17, 2))
        PId_Moneda_Deposito = Null
        Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
        With lcMoneda
            .Campo("cod_moneda").Valor = cMoneda_Cod_Peso
            If Not .Buscar Then
                pMsgError = "Problemas en buscar la moneda del nemot�cnico." & .ErrMsg
                GoTo ExitProcedure
            End If
            
            If .Cursor.Count <= 0 Then
                pMsgError = "Por raro que parezca no esta la moneda definida en el sistema, favor de crear."
                GoTo ExitProcedure
            End If
            
            pId_Moneda_Pago = .Cursor(1)("id_moneda").Value
            PId_Moneda_Deposito = pId_Moneda_Pago
            pBase = 30
        End With
        Set lcMoneda = Nothing
        
        Set lcPactos = New Class_Pactos
        lNemotecnico = lcPactos.Fnt_Genera_Nemo_SVS_Pactos(lId_Emisor_Especifico _
                                                       , pFecha_Vencimiento _
                                                       , PId_Moneda_Deposito _
                                                       , pId_Moneda_Pago)
        Set lcPactos = Nothing
    Case gcINST_DEPOSITOS_NAC
    
        Set lcEmisor_Especifico = New Class_Emisores_Especifico
        With lcEmisor_Especifico
          .Campo("COD_SVS_nemotecnico").Valor = Trim(Mid(pNemotecnico, 3, 4))
          If Not .Buscar Then
            pMsgError = "Problemas en buscar el codigo SVS del emisor." & .ErrMsg
            GoTo ExitProcedure
          End If
          
          If .Cursor.Count <= 0 Then
            pMsgError = "El codigo del emisor no esta definido en el sistema, cree un emisor especifico con el codigo de SVS correspondiente."
            GoTo ExitProcedure
          End If
          
          lId_Emisor_Especifico = .Cursor(1)("id_emisor_especifico").Value
        End With
        Set lcEmisor_Especifico = Nothing
        pFecha_Vencimiento = DateSerial("20" & Mid(pNemotecnico, 11, 2), Mid(pNemotecnico, 9, 2), Mid(pNemotecnico, 7, 2))
        
        PId_Moneda_Deposito = Null
        Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
        With lcMoneda
            .Campo("cod_moneda").Valor = cMoneda_Cod_Peso
            If Not .Buscar Then
                pMsgError = "Problemas en buscar la moneda del nemot�cnico." & .ErrMsg
                GoTo ExitProcedure
            End If
            
            If .Cursor.Count <= 0 Then
                pMsgError = "Por raro que parezca no esta la moneda definida en el sistema, favor de crear."
                GoTo ExitProcedure
            End If
            
            pId_Moneda_Pago = .Cursor(1)("id_moneda").Value
        End With
        Set lcMoneda = Nothing
        
        Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
        With lcMoneda
            Select Case Mid(pNemotecnico, 2, 1)
                Case "$"
                    .Campo("cod_moneda").Valor = cMoneda_Cod_Peso
                    pBase = 30
                Case "U"
                    .Campo("cod_moneda").Valor = cMoneda_Cod_UF
                    pBase = 360
                Case "O"
                    .Campo("cod_moneda").Valor = cMoneda_Cod_Dolar
                    pBase = 360
            End Select
            If Not .Buscar Then
                pMsgError = "Problemas en buscar la moneda del nemot�cnico." & .ErrMsg
                GoTo ExitProcedure
            End If
            
            If .Cursor.Count <= 0 Then
                pMsgError = "Por raro que parezca no esta la moneda definida en el sistema, favor de crear."
                GoTo ExitProcedure
            End If
            
            PId_Moneda_Deposito = .Cursor(1)("id_moneda").Value
        End With
        Set lcMoneda = Nothing
        
        Set lcNemotecnico = New Class_Nemotecnicos
        With lcNemotecnico
            .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
            .Campo("Fecha_Vencimiento").Valor = pFecha_Vencimiento
            lNemotecnico = .Fnt_Generacion_Nemo_SVS(PId_Moneda_Deposito, pId_Moneda_Pago)
        End With
        Set lcNemotecnico = Nothing
    Case Else
        pMsgError = "No se puede crear el nemot�cnico """ & pNemotecnico & """."
    GoTo ExitProcedure
  End Select
  
  lFecha_Emision = pFecha_Movimiento
  
 'Busca primero el nemotecnico antes de crearlo
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("Cod_Instrumento").Valor = pCod_Instrumento
    .Campo("nemotecnico").Valor = lNemotecnico
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en buscar el Nemot�cnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ExitProcedure
    End If
  
    If .Cursor.Count > 0 Then
        pId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
    Else
        .Campo("id_Mercado_Transaccion").Valor = "15" 'fc_Mercado_Transaccion 'Este campo deberia aceptar nulos
        .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
        .Campo("id_Emisor_Especifico_Origen").Valor = lId_Emisor_Especifico
        .Campo("id_Moneda").Valor = PId_Moneda_Deposito
        .Campo("id_Moneda_transaccion").Valor = pId_Moneda_Pago
        .Campo("id_Tipo_Estado").Valor = cTEstado_Nemotecnico
        .Campo("cod_Estado").Valor = cCod_Estado_Vigente
        .Campo("dsc_nemotecnico").Valor = lNemotecnico
        .Campo("tasa_Emision").Valor = pTasa
        .Campo("tipo_Tasa").Valor = ""
        .Campo("periodicidad").Valor = ""
        .Campo("fecha_Vencimiento").Valor = pFecha_Vencimiento
        .Campo("corte_Minimo_Papel").Valor = 1 'Depositos no tiene corte
        .Campo("Monto_Emision").Valor = ""
        .Campo("liquidez").Valor = ""
        .Campo("base").Valor = pBase
        .Campo("cod_Pais").Valor = gcPais_Chile
        .Campo("flg_Fungible").Valor = ""
        .Campo("fecha_emision").Valor = lFecha_Emision
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en grabar el Nemot�cnico.", _
                            .ErrMsg, _
                            pConLog:=True)
          GoTo ExitProcedure
        End If
        pId_Nemotecnico = .Campo("id_nemotecnico").Valor
    End If
  End With
  Set lcNemotecnico = Nothing
  
  Fnt_VerificaCreacion_Nemo = True
  
ExitProcedure:

End Function

Private Function Fnt_Buscar_Rel_Nro_Oper_Detalle(pOrigen As String _
                                               , pNro_Orden As Variant _
                                               , pMsg_Error As String) As Boolean
Dim lId_Operacion_detalle
  
  Set lcAlias = Fnt_CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  lId_Operacion_detalle = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=pOrigen _
                                                  , pCodigoCSBPI:=cTabla_Operacion_Detalle _
                                                  , pValor:=pNro_Orden)
  
  If IsNull(lId_Operacion_detalle) Then
    Fnt_Buscar_Rel_Nro_Oper_Detalle = True
  Else
    pMsg_Error = "N�mero de Orden: '" & pNro_Orden & "'. El N�mero de Orden ya est� ingresado en el sistema. No se puede operar la instrucci�n nuevamente."
    Fnt_Buscar_Rel_Nro_Oper_Detalle = False
  End If
  
End Function

Private Function Fnt_Buscar_Datos_Nemo_RF(pId_Nemotecnico As String, _
                                          ByRef pId_Moneda_Nemo As String, _
                                          ByRef pFecha_Vencimiento As Date, _
                                          ByRef pMsg_Error As String) As Boolean
Dim lcNemotecnico As Class_Nemotecnicos
  
  Fnt_Buscar_Datos_Nemo_RF = True
  
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pId_Moneda_Nemo = .Cursor(1)("id_moneda_transaccion").Value
        pFecha_Vencimiento = NVL(.Cursor(1)("fecha_vencimiento").Value, Format(Now, cFormatDate))
      Else
        pMsg_Error = "Nemot�cnico no encontrado en el sistema. No se puede operar la instrucci�n."
        GoTo ErrProcedure
      End If
    Else
      pMsg_Error = "Problemas en cargar datos de Nemotecnico." & vbCr & .ErrMsg
      GoTo ErrProcedure
    End If
  End With
  Set lcNemotecnico = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Buscar_Datos_Nemo_RF = False
End Function

Private Sub Sub_CreaPDF_Mail_Asesor_FM(pId_Asesor, pGrilla As VSFlexGrid, pGrilla_SC As VSFlexGrid, lApellido As String)
Dim lFila As Long
'Dim ArchivosPDF()
'With pGrilla
  
'ReDim Preserve ArchivosPDF(0)
 
Const clrHeader = &HD0D0D0
Dim sRecord
Dim bAppend
Dim lLinea As Integer
Dim lForm As Object 'Frm_Reporte_Generico
Dim lReg As hCollection.hFields
Dim a As Integer
Dim iFila As Integer
Dim Filas As Integer
Dim Fila As Integer
Dim lCursor_Asesor As hRecord
Dim pArchivoPDF As String
Dim lTotalGastos As Double
'-----------------------------------------------------------------------
'Dim lOD_Cursor As hRecord 'CURSOR PARA EL DETALLE DE LAS OPERACIONES.

  'Set Fnt_Generar_Comprobante = Nothing
  'Call Sub_Bloquea_Puntero(Me)
  'Call Sub_CargarDatos_Gen(pId_Operacion)
  
  'Set lForm = fClass.Form_Reporte_Generico
  
  pArchivoPDF = Environ("temp") & "\Confirmacion_Importacion_" & lApellido & "-" & pId_Asesor & ".pdf"
  Set lForm = Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="Confirmaci�n de Ordenes al " & DTP_Fecha_Proceso.Value _
                               , pTipoSalida:=ePrinter.eP_PDF_Automatico _
                               , pOrientacion:=orLandscape _
                               , pArchivoSalida:=pArchivoPDF)
     
Dim lc_Asesores As Class_Asesor
Dim lNombre_Asesor As String
Dim lMail_Asesor As String

Set lc_Asesores = New Class_Asesor

With lc_Asesores
    .Campo("id_asesor").Valor = pId_Asesor
    If .Buscar Then
        Set lCursor_Asesor = .Cursor
        For Each lReg In .Cursor
           lNombre_Asesor = lReg("nombre").Value
           lMail_Asesor = lReg("Email").Value
        Next
    End If
End With
    
  With lForm.VsPrinter

 ' .StartDoc
 .Paragraph = ""
 .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 8
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "ASESOR: " & lNombre_Asesor & " (" & lMail_Asesor & ")"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
  .EndTable
 
 .Paragraph = ""

  .MarginLeft = "30mm"
  .MarginRight = "7mm"
    .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 7
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "Operaciones con Patrimonio Administrado"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
    .EndTable
 
    .Paragraph = "" 'salto de linea
    
    .StartTable
      
    '.TableCell(tcRows) = pGrilla.Rows
    .TableCell(tcRows) = 1
    .TableCell(tcCols) = 10
    .TableBorder = tbAll
    '
   .TableCell(tcColWidth, 1, 1) = "15mm"
    .TableCell(tcColWidth, 1, 2) = "18mm"
    .TableCell(tcColWidth, 1, 3) = "15mm"
    .TableCell(tcColWidth, 1, 4) = "45mm"
    .TableCell(tcColWidth, 1, 5) = "20mm"
    .TableCell(tcColWidth, 1, 6) = "35mm"
    .TableCell(tcColWidth, 1, 7) = "25mm"
    .TableCell(tcColWidth, 1, 8) = "20mm"
    .TableCell(tcColWidth, 1, 9) = "25mm"
    .TableCell(tcColWidth, 1, 10) = "22mm"
    
    .TableCell(tcText, 1, 1) = "N� Orden"
    .TableCell(tcText, 1, 2) = "Rut Cliente"
    .TableCell(tcText, 1, 3) = "Cuenta"
    .TableCell(tcText, 1, 4) = "Cliente"
    .TableCell(tcText, 1, 5) = "Movimiento"
    .TableCell(tcText, 1, 6) = "Nemot�cnico"
    .TableCell(tcText, 1, 7) = "Cantidad"
    .TableCell(tcText, 1, 8) = "Valor Cuota"
    .TableCell(tcText, 1, 9) = "Monto"
    .TableCell(tcText, 1, 10) = "Fecha Liquidaci�n"
 Fila = 2
 For lFila = 1 To pGrilla.Rows - 1
      If pGrilla.Cell(flexcpChecked, lFila, pGrilla.ColIndex("chk")) = flexChecked And GetCell(pGrilla, lFila, "id_asesor") = pId_Asesor Then
          .TableCell(tcRows) = .TableCell(tcRows) + 1
           'Fila = lFila + 1
          .TableCell(tcText, Fila, 1) = GetCell(pGrilla, lFila, "n_orden")
          .TableCell(tcText, Fila, 2) = GetCell(pGrilla, lFila, "rut")
          .TableCell(tcText, Fila, 3) = GetCell(pGrilla, lFila, "num_cuenta")
          .TableCell(tcText, Fila, 4) = GetCell(pGrilla, lFila, "dsc_cliente")
          .TableCell(tcText, Fila, 5) = GetCell(pGrilla, lFila, "tipo_movimiento")
          .TableCell(tcText, Fila, 6) = GetCell(pGrilla, lFila, "nemotecnico")
          .TableCell(tcText, Fila, 7) = FormatNumber(GetCell(pGrilla, lFila, "cantidad"))
          .TableCell(tcText, Fila, 8) = FormatNumber(GetCell(pGrilla, lFila, "precio"))
          .TableCell(tcText, Fila, 9) = FormatNumber(GetCell(pGrilla, lFila, "monto"))
          .TableCell(tcText, Fila, 10) = GetCell(pGrilla, lFila, "fecha_liquidacion")
          .TableCell(tcAlign, Fila, 1) = taLeftMiddle
          .TableCell(tcAlign, Fila, 2) = taRightMiddle
          .TableCell(tcAlign, Fila, 3) = taCenterMiddle
          .TableCell(tcAlign, Fila, 6) = taLeftMiddle
          .TableCell(tcAlign, Fila, 7) = taRightMiddle
          .TableCell(tcAlign, Fila, 8) = taRightMiddle
          .TableCell(tcAlign, Fila, 9) = taRightMiddle
          .TableCell(tcAlign, Fila, 10) = taRightMiddle
          Fila = Fila + 1
      End If
  Next
  .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 13) = 6
  
  .EndTable
  
.Paragraph = ""
'If pGrilla_SC.Rows > 1 Then
.StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 7
      .TableCell(tcAlign, 1, 1) = taLeftMiddle
      .TableCell(tcText, 1, 1) = "Operaciones Fuera del Patrimonio Administrado"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
.EndTable


.StartTable
      
    '.TableCell(tcRows) = pGrilla_SC.Rows
    .TableCell(tcRows) = 1
    .TableCell(tcCols) = 10
    .TableBorder = tbAll
   ' .TableCell(tcFontSize, 1, 1, pGrilla_SC.Rows, 13) = 6
    
   .TableCell(tcColWidth, 1, 1) = "15mm"
    .TableCell(tcColWidth, 1, 2) = "18mm"
    .TableCell(tcColWidth, 1, 3) = "15mm"
    .TableCell(tcColWidth, 1, 4) = "45mm"
    .TableCell(tcColWidth, 1, 5) = "20mm"
    .TableCell(tcColWidth, 1, 6) = "35mm"
    .TableCell(tcColWidth, 1, 7) = "25mm"
    .TableCell(tcColWidth, 1, 8) = "20mm"
    .TableCell(tcColWidth, 1, 9) = "25mm"
    .TableCell(tcColWidth, 1, 10) = "22mm"
    
    
    .TableCell(tcText, 1, 1) = "N� Orden"
    .TableCell(tcText, 1, 2) = "Rut Cliente"
    .TableCell(tcText, 1, 3) = "Cuenta"
    .TableCell(tcText, 1, 4) = "Cliente"
    .TableCell(tcText, 1, 5) = "Movimiento"
    .TableCell(tcText, 1, 6) = "Nemot�cnico"
    .TableCell(tcText, 1, 7) = "Cantidad"
    .TableCell(tcText, 1, 8) = "Valor Cuota"
    .TableCell(tcText, 1, 9) = "Monto"
    .TableCell(tcText, 1, 10) = "Fecha Liquidaci�n"


'With pGrilla_SC
Fila = 2
  For lFila = 1 To pGrilla_SC.Rows - 1
      If pGrilla_SC.Cell(flexcpChecked, lFila, pGrilla_SC.ColIndex("chk")) = flexChecked And GetCell(pGrilla_SC, lFila, "id_asesor") = pId_Asesor Then
          .TableCell(tcRows) = .TableCell(tcRows) + 1
           ' Fila = lFila + 1
          .TableCell(tcText, Fila, 1) = GetCell(pGrilla_SC, lFila, "n_orden")
          .TableCell(tcText, Fila, 2) = GetCell(pGrilla_SC, lFila, "rut")
          .TableCell(tcText, Fila, 3) = GetCell(pGrilla_SC, lFila, "num_cuenta")
          .TableCell(tcText, Fila, 4) = GetCell(pGrilla_SC, lFila, "nombre_cliente")
          .TableCell(tcText, Fila, 5) = GetCell(pGrilla_SC, lFila, "tipo_movimiento")
          .TableCell(tcText, Fila, 6) = GetCell(pGrilla_SC, lFila, "nemotecnico")
          .TableCell(tcText, Fila, 7) = FormatNumber(GetCell(pGrilla_SC, lFila, "cantidad"))
          .TableCell(tcText, Fila, 8) = FormatNumber(GetCell(pGrilla_SC, lFila, "precio"))
          .TableCell(tcText, Fila, 9) = FormatNumber(GetCell(pGrilla_SC, lFila, "monto"))
          .TableCell(tcText, Fila, 10) = GetCell(pGrilla_SC, lFila, "fecha_liquidacion")
     
          .TableCell(tcAlign, Fila, 1) = taLeftMiddle
          .TableCell(tcAlign, Fila, 2) = taRightMiddle
          .TableCell(tcAlign, Fila, 3) = taCenterMiddle
          .TableCell(tcAlign, Fila, 6) = taRightMiddle
          .TableCell(tcAlign, Fila, 7) = taRightMiddle
          .TableCell(tcAlign, Fila, 8) = taRightMiddle
          .TableCell(tcAlign, Fila, 9) = taRightMiddle
          .TableCell(tcAlign, Fila, 10) = taRightMiddle
          Fila = Fila + 1
      End If
  Next
  .TableCell(tcFontSize, 1, 1, .TableCell(tcRows), 13) = 6
.EndTable
.EndDoc
End With

If Envia_Confirmacion(pArchivoPDF, lMail_Asesor, lNombre_Asesor) Then

End If
End Sub
