VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Operacion_Pactos_Depositos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ingreso Pactos"
   ClientHeight    =   8655
   ClientLeft      =   150
   ClientTop       =   810
   ClientWidth     =   11880
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8655
   ScaleWidth      =   11880
   Begin VB.Frame Pnl_Cuenta 
      Caption         =   "Cuenta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1335
      Left            =   120
      TabIndex        =   46
      Top             =   360
      Width           =   4935
      Begin VB.CommandButton Btn_Venta 
         Caption         =   "Venta"
         Height          =   375
         Left            =   2640
         TabIndex        =   49
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton Btn_Compra 
         Caption         =   "Compra"
         Height          =   375
         Left            =   720
         TabIndex        =   48
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4440
         Picture         =   "Frm_Operacion_Pactos_Depositos.frx":0000
         TabIndex        =   47
         Top             =   360
         Width           =   375
      End
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   345
         Left            =   120
         TabIndex        =   50
         Tag             =   "OBLI=S;CAPTION=Cuentas"
         Top             =   360
         Width           =   4245
         _ExtentX        =   7488
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Pactos_Depositos.frx":030A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
   End
   Begin VB.Frame Pnl_Cliente 
      Caption         =   "Cliente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1335
      Left            =   5160
      TabIndex        =   41
      Top             =   360
      Width           =   6615
      Begin hControl2.hTextLabel Txt_Rut 
         Height          =   315
         Left            =   240
         TabIndex        =   42
         Top             =   240
         Width           =   2940
         _ExtentX        =   5186
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "RUT"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Nombres 
         Height          =   315
         Left            =   240
         TabIndex        =   43
         Top             =   600
         Width           =   6105
         _ExtentX        =   10769
         _ExtentY        =   556
         LabelWidth      =   1000
         Caption         =   "Nombres"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Perfil 
         Height          =   315
         Left            =   3360
         TabIndex        =   44
         Top             =   240
         Width           =   2940
         _ExtentX        =   5186
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "Perfil Riesgo"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel txtNombreAsesor 
         Height          =   315
         Left            =   240
         TabIndex        =   45
         Top             =   960
         Width           =   6120
         _ExtentX        =   10795
         _ExtentY        =   556
         LabelWidth      =   1000
         Caption         =   "Asesor"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
   End
   Begin VB.Frame Pnl_Operacion 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   6945
      Left            =   120
      TabIndex        =   3
      Top             =   1680
      Width           =   11650
      Begin VB.Frame Pnl_DatosOperacion 
         Caption         =   "Datos Operaci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   3375
         Left            =   120
         TabIndex        =   11
         Top             =   3480
         Width           =   11415
         Begin VB.TextBox Txt_TasaTransferencia 
            Height          =   315
            Left            =   10320
            MaxLength       =   10
            TabIndex        =   37
            Top             =   2880
            Visible         =   0   'False
            Width           =   765
         End
         Begin VB.CheckBox chkAporteRetiro 
            Caption         =   "� Aporte/Retiro de Capital ?"
            Height          =   375
            Left            =   4170
            TabIndex        =   12
            Top             =   600
            Value           =   1  'Checked
            Width           =   2415
         End
         Begin hControl2.hTextLabel Txt_Porcentaje_Comision 
            Height          =   315
            Left            =   7530
            TabIndex        =   13
            Tag             =   "OBLI"
            Top             =   360
            Width           =   1485
            _ExtentX        =   2619
            _ExtentY        =   556
            LabelWidth      =   750
            TextMinWidth    =   500
            Caption         =   "Comisi�n"
            Text            =   "0,0000%"
            Text            =   "0,0000%"
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "##0.0000%"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Derechos 
            Height          =   315
            Left            =   9000
            TabIndex        =   14
            Tag             =   "OBLI"
            Top             =   720
            Width           =   1995
            _ExtentX        =   3519
            _ExtentY        =   556
            LabelWidth      =   15
            TextMinWidth    =   500
            Caption         =   ""
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Iva 
            Height          =   315
            Left            =   7530
            TabIndex        =   15
            Tag             =   "OBLI"
            Top             =   1440
            Width           =   3465
            _ExtentX        =   6112
            _ExtentY        =   556
            LabelWidth      =   1500
            TextMinWidth    =   500
            Caption         =   " Iva (%)"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Comision 
            Height          =   315
            Left            =   9000
            TabIndex        =   16
            Tag             =   "OBLI"
            Top             =   360
            Width           =   1995
            _ExtentX        =   3519
            _ExtentY        =   556
            LabelWidth      =   15
            TextMinWidth    =   500
            Caption         =   ""
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Gastos 
            Height          =   315
            Left            =   7530
            TabIndex        =   17
            Tag             =   "OBLI"
            Top             =   1080
            Width           =   3465
            _ExtentX        =   6112
            _ExtentY        =   556
            LabelWidth      =   1500
            TextMinWidth    =   500
            Caption         =   "Gastos"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Porcentaje_Derechos 
            Height          =   315
            Left            =   7530
            TabIndex        =   18
            Tag             =   "OBLI"
            Top             =   720
            Width           =   1485
            _ExtentX        =   2619
            _ExtentY        =   556
            LabelWidth      =   750
            TextMinWidth    =   500
            Caption         =   "Derechos "
            Text            =   "0,0000%"
            Text            =   "0,0000%"
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "##0.0000%"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_MontoTotal 
            Height          =   345
            Left            =   7530
            TabIndex        =   19
            Top             =   2250
            Width           =   3465
            _ExtentX        =   6112
            _ExtentY        =   609
            LabelWidth      =   1500
            Caption         =   "Monto Total"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   16761024
            BackColorTxt    =   16761024
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_MontoNeto 
            Height          =   315
            Left            =   7530
            TabIndex        =   20
            Top             =   1800
            Width           =   3465
            _ExtentX        =   6112
            _ExtentY        =   556
            LabelWidth      =   1500
            Caption         =   "Neto"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Num_Operacion 
            Height          =   315
            Left            =   120
            TabIndex        =   21
            Top             =   2700
            Width           =   3285
            _ExtentX        =   5794
            _ExtentY        =   556
            LabelWidth      =   1400
            Caption         =   "N� Operaci�n"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin TrueDBList80.TDBCombo Cmb_Representantes 
            Height          =   345
            Left            =   1530
            TabIndex        =   22
            Top             =   2250
            Width           =   4365
            _ExtentX        =   7699
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Depositos.frx":03B4
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Contraparte 
            Height          =   345
            Left            =   1530
            TabIndex        =   23
            Top             =   1470
            Width           =   4365
            _ExtentX        =   7699
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Depositos.frx":045E
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Traders 
            Height          =   345
            Left            =   1530
            TabIndex        =   24
            Top             =   1860
            Width           =   4365
            _ExtentX        =   7699
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Depositos.frx":0508
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin hControl2.hTextLabel Txt_FechaIngreso_Real 
            Height          =   315
            Left            =   120
            TabIndex        =   25
            Top             =   720
            Width           =   3330
            _ExtentX        =   5874
            _ExtentY        =   556
            LabelWidth      =   1400
            Caption         =   "Fecha Operaci�n"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin MSComCtl2.DTPicker Dtp_FechaLiquidacion 
            Height          =   345
            Left            =   2280
            TabIndex        =   26
            Tag             =   "OBLI"
            Top             =   1080
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   609
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   57475073
            CurrentDate     =   39883
         End
         Begin TrueDBList80.TDBCombo Cmb_FechaLiquidacion 
            Height          =   345
            Left            =   1530
            TabIndex        =   27
            Top             =   1080
            Width           =   705
            _ExtentX        =   1244
            _ExtentY        =   609
            _LayoutType     =   4
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   1
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=1"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Depositos.frx":05B2
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Named:id=33:Normal"
            _StyleDefs(35)  =   ":id=33,.parent=0"
            _StyleDefs(36)  =   "Named:id=34:Heading"
            _StyleDefs(37)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(38)  =   ":id=34,.wraptext=-1"
            _StyleDefs(39)  =   "Named:id=35:Footing"
            _StyleDefs(40)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(41)  =   "Named:id=36:Selected"
            _StyleDefs(42)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(43)  =   "Named:id=37:Caption"
            _StyleDefs(44)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(45)  =   "Named:id=38:HighlightRow"
            _StyleDefs(46)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=39:EvenRow"
            _StyleDefs(48)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(49)  =   "Named:id=40:OddRow"
            _StyleDefs(50)  =   ":id=40,.parent=33"
            _StyleDefs(51)  =   "Named:id=41:RecordSelector"
            _StyleDefs(52)  =   ":id=41,.parent=34"
            _StyleDefs(53)  =   "Named:id=42:FilterBar"
            _StyleDefs(54)  =   ":id=42,.parent=33"
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Operacion 
            Height          =   345
            Left            =   1530
            TabIndex        =   28
            Tag             =   "OBLI"
            Top             =   720
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   609
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   57475073
            CurrentDate     =   38768
         End
         Begin hControl2.hTextLabel Txt_FechaVigencia 
            Height          =   315
            Left            =   4200
            TabIndex        =   29
            Top             =   1080
            Width           =   3300
            _ExtentX        =   5821
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   1000
            Caption         =   "Fecha Vigencia"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin TrueDBList80.TDBCombo Cmb_Moneda_Pago 
            Height          =   345
            Left            =   1560
            TabIndex        =   30
            Tag             =   "OBLI=S;CAPTION=Moneda de Pago"
            Top             =   360
            Width           =   2475
            _ExtentX        =   4366
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Operacion_Pactos_Depositos.frx":065C
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&,.fgcolor=&H0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Label3 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Transferencia"
            Height          =   315
            Left            =   9000
            TabIndex        =   38
            Top             =   2880
            Visible         =   0   'False
            Width           =   1110
         End
         Begin VB.Label lbl_fecha_ingreso 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Operaci�n"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   36
            Top             =   720
            Width           =   1395
         End
         Begin VB.Label Label6 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Contraparte"
            Height          =   345
            Left            =   120
            TabIndex        =   35
            Top             =   1470
            Width           =   1395
         End
         Begin VB.Label Label5 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Trader"
            Height          =   345
            Left            =   120
            TabIndex        =   34
            Top             =   1860
            Width           =   1395
         End
         Begin VB.Label Label4 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Representantes"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   33
            Top             =   2250
            Width           =   1395
         End
         Begin VB.Label lbl_fecha_liquidacion 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Liquidaci�n"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   32
            Top             =   1080
            Width           =   1395
         End
         Begin VB.Label Label2 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Moneda de Pago"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   31
            Top             =   360
            Width           =   1395
         End
      End
      Begin VB.Frame Pnl_DetalleOperacion 
         Caption         =   "Detalle Operaci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   2775
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   11415
         Begin VSFlex8LCtl.VSFlexGrid Grilla 
            Height          =   1935
            Left            =   120
            TabIndex        =   0
            Top             =   240
            Width           =   11055
            _cx             =   19500
            _cy             =   3413
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   0
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   50
            Cols            =   24
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   $"Frm_Operacion_Pactos_Depositos.frx":0706
            ScrollTrack     =   0   'False
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
            Begin VB.Frame Pnl_Nemo 
               ForeColor       =   &H000000FF&
               Height          =   2055
               Left            =   1080
               TabIndex        =   7
               Top             =   -120
               Width           =   2415
               Begin VB.CommandButton Btn_GrillaNemo 
                  Caption         =   "X"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   9
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Left            =   1920
                  TabIndex        =   8
                  Top             =   120
                  Width           =   375
               End
               Begin VSFlex8LCtl.VSFlexGrid Grilla_Nemo 
                  Height          =   1575
                  Left            =   120
                  TabIndex        =   9
                  Top             =   360
                  Width           =   2175
                  _cx             =   3836
                  _cy             =   2778
                  Appearance      =   1
                  BorderStyle     =   1
                  Enabled         =   -1  'True
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  MousePointer    =   0
                  BackColor       =   -2147483643
                  ForeColor       =   -2147483640
                  BackColorFixed  =   -2147483633
                  ForeColorFixed  =   -2147483630
                  BackColorSel    =   -2147483635
                  ForeColorSel    =   -2147483634
                  BackColorBkg    =   -2147483636
                  BackColorAlternate=   -2147483643
                  GridColor       =   -2147483633
                  GridColorFixed  =   -2147483632
                  TreeColor       =   -2147483632
                  FloodColor      =   192
                  SheetBorder     =   -2147483642
                  FocusRect       =   1
                  HighLight       =   1
                  AllowSelection  =   0   'False
                  AllowBigSelection=   0   'False
                  AllowUserResizing=   1
                  SelectionMode   =   0
                  GridLines       =   1
                  GridLinesFixed  =   2
                  GridLineWidth   =   1
                  Rows            =   50
                  Cols            =   2
                  FixedRows       =   1
                  FixedCols       =   0
                  RowHeightMin    =   0
                  RowHeightMax    =   0
                  ColWidthMin     =   0
                  ColWidthMax     =   0
                  ExtendLastCol   =   0   'False
                  FormatString    =   $"Frm_Operacion_Pactos_Depositos.frx":0B4C
                  ScrollTrack     =   0   'False
                  ScrollBars      =   3
                  ScrollTips      =   0   'False
                  MergeCells      =   0
                  MergeCompare    =   0
                  AutoResize      =   -1  'True
                  AutoSizeMode    =   0
                  AutoSearch      =   0
                  AutoSearchDelay =   2
                  MultiTotals     =   -1  'True
                  SubtotalPosition=   1
                  OutlineBar      =   0
                  OutlineCol      =   0
                  Ellipsis        =   0
                  ExplorerBar     =   0
                  PicturesOver    =   0   'False
                  FillStyle       =   0
                  RightToLeft     =   0   'False
                  PictureType     =   0
                  TabBehavior     =   0
                  OwnerDraw       =   0
                  Editable        =   0
                  ShowComboButton =   1
                  WordWrap        =   0   'False
                  TextStyle       =   0
                  TextStyleFixed  =   0
                  OleDragMode     =   0
                  OleDropMode     =   0
                  ComboSearch     =   3
                  AutoSizeMouse   =   -1  'True
                  FrozenRows      =   0
                  FrozenCols      =   0
                  AllowUserFreezing=   0
                  BackColorFrozen =   0
                  ForeColorFrozen =   0
                  WallPaperAlignment=   9
                  AccessibleName  =   ""
                  AccessibleDescription=   ""
                  AccessibleValue =   ""
                  AccessibleRole  =   24
               End
            End
            Begin VB.Frame Pnl_Emisores 
               ForeColor       =   &H000000FF&
               Height          =   2055
               Left            =   3840
               TabIndex        =   5
               Top             =   -120
               Visible         =   0   'False
               Width           =   2535
               Begin VB.CommandButton Btn_GrillaEmisor 
                  Caption         =   "X"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   9
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Left            =   2040
                  TabIndex        =   10
                  Top             =   120
                  Width           =   375
               End
               Begin VSFlex8LCtl.VSFlexGrid Grilla_Emisor 
                  Height          =   1545
                  Left            =   120
                  TabIndex        =   6
                  Top             =   360
                  Width           =   2280
                  _cx             =   4022
                  _cy             =   2725
                  Appearance      =   2
                  BorderStyle     =   1
                  Enabled         =   -1  'True
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  MousePointer    =   0
                  BackColor       =   -2147483643
                  ForeColor       =   -2147483640
                  BackColorFixed  =   -2147483633
                  ForeColorFixed  =   -2147483630
                  BackColorSel    =   65535
                  ForeColorSel    =   0
                  BackColorBkg    =   -2147483643
                  BackColorAlternate=   -2147483643
                  GridColor       =   -2147483633
                  GridColorFixed  =   -2147483632
                  TreeColor       =   -2147483632
                  FloodColor      =   192
                  SheetBorder     =   -2147483642
                  FocusRect       =   2
                  HighLight       =   1
                  AllowSelection  =   -1  'True
                  AllowBigSelection=   -1  'True
                  AllowUserResizing=   1
                  SelectionMode   =   3
                  GridLines       =   10
                  GridLinesFixed  =   2
                  GridLineWidth   =   1
                  Rows            =   2
                  Cols            =   3
                  FixedRows       =   1
                  FixedCols       =   0
                  RowHeightMin    =   0
                  RowHeightMax    =   0
                  ColWidthMin     =   0
                  ColWidthMax     =   0
                  ExtendLastCol   =   -1  'True
                  FormatString    =   $"Frm_Operacion_Pactos_Depositos.frx":0BC3
                  ScrollTrack     =   -1  'True
                  ScrollBars      =   3
                  ScrollTips      =   -1  'True
                  MergeCells      =   0
                  MergeCompare    =   0
                  AutoResize      =   -1  'True
                  AutoSizeMode    =   0
                  AutoSearch      =   2
                  AutoSearchDelay =   2
                  MultiTotals     =   -1  'True
                  SubtotalPosition=   1
                  OutlineBar      =   0
                  OutlineCol      =   0
                  Ellipsis        =   1
                  ExplorerBar     =   3
                  PicturesOver    =   0   'False
                  FillStyle       =   0
                  RightToLeft     =   0   'False
                  PictureType     =   0
                  TabBehavior     =   0
                  OwnerDraw       =   0
                  Editable        =   0
                  ShowComboButton =   1
                  WordWrap        =   0   'False
                  TextStyle       =   0
                  TextStyleFixed  =   0
                  OleDragMode     =   0
                  OleDropMode     =   0
                  ComboSearch     =   3
                  AutoSizeMouse   =   -1  'True
                  FrozenRows      =   0
                  FrozenCols      =   0
                  AllowUserFreezing=   0
                  BackColorFrozen =   0
                  ForeColorFrozen =   0
                  WallPaperAlignment=   9
                  AccessibleName  =   ""
                  AccessibleDescription=   ""
                  AccessibleValue =   ""
                  AccessibleRole  =   24
               End
            End
         End
         Begin hControl2.hTextLabel Txt_TotalDetalle 
            Height          =   315
            Left            =   6960
            TabIndex        =   39
            Top             =   2280
            Width           =   4095
            _ExtentX        =   7223
            _ExtentY        =   556
            LabelWidth      =   2000
            Caption         =   "Monto Total Detalle"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
      End
      Begin MSComctlLib.Toolbar Toolbar_Operacion 
         Height          =   330
         Left            =   2880
         TabIndex        =   40
         Top             =   3120
         Width           =   4980
         _ExtentX        =   8784
         _ExtentY        =   582
         ButtonWidth     =   4128
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Ingresar Datos Operaci�n"
               Key             =   "OPERACION"
               Description     =   "Ingresa Datos Operaci�n"
               Object.ToolTipText     =   "Ingresa Datos Operaci�n"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Style           =   3
               Object.Width           =   1e-4
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Ingresar Detalle Operaci�n"
               Key             =   "DETALLEOPERACION"
               Description     =   "Ingresar detalle de operaci�n"
               Object.ToolTipText     =   "Ingresar detalle de operaci�n"
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Description     =   "Inicializa campos para nuevo ingreso"
            Object.ToolTipText     =   "Inicializa campos para nuevo ingreso"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   2
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Operacion_Pactos_Depositos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
'------------------------------------
Rem PARA CONSULTA DE OPERACION
Dim fConsulta_Operacion As Boolean
'------------------------------------
Dim fSalir      As Boolean
Dim fEstadoOK   As Boolean
Dim fOperacion  As String
Dim fCod_Producto       As String
Dim fId_Cuenta  As String
Dim fId_Cliente As Double
Dim fTipo_Operacion As String
Dim fId_Nemotecnico As String
Dim fValor_Iva As Double
Dim fFecha_Operacion As Date
Dim fDerechosBolsa As Double
Dim lcSubfamilia As Class_SubFamilias
Dim lId_Subfamilia As String
Dim fMonto_Operacion As String
Dim lId_Moneda_Peso
Dim fDsc_Instrumento    As String
Dim fNombre_Operacion    As String
Dim fDsc_Operacion      As String
Dim bOper_Fecha_Anterior As Boolean
Dim fId_Mov_Activo As String
Dim fId_Emisor As String
Dim fModif_Grilla As Boolean
Dim bEliminaLinea       As Boolean
Dim fId_Moneda_Transaccion As Integer
Dim fDsc_Moneda_Transaccion As String
Dim fTasa_Emision As Double
Dim fId_Operacion As String
Dim fRowGrilla          As Long
Dim fColGrilla          As Long
Dim fDsc_Nemotecnico    As String
Dim fDsc_Emisor         As String
Dim bModificaCelda      As Boolean
Dim fCmbMoneda          As String
'dim fCaracterFecha      as Integer

Const fc_Mercado_Transaccion = "15" ' Corresponde a Santiago
Const fc_Mercado = "N"
Const fc_Tipo_Interes = 1 ' esto va en duro => como el inter�s es de tipo simple es 1 (si es compuesto es 2)
Const fc_Id_Sector = 3 'en duro => financiero
Const fc_Corte_Minimo_Pac = 1 'pactos no tiene corte minimo, para no tener problemas va a ser 1.
Const fc_Corte_Minimo_Dep = 1 'deposito no tiene corte minimo, para no tener problemas va a ser 1.

Const MAX_COLUMNAS = 23

Const c_operacion = 6
Const c_instrumento = 7
Const c_nemo = 16
Const c_emisor = 17
Const c_fechavcto = 18
Const c_moneda = 19
'Const c_monedapago = 13
Const c_cantidad_nemo = 20
Const c_cantidad = 21
Const c_precio = 22
Const c_monto = 23

'Esta variable contiene el monto valorizado a la tasa nominal
Dim fMonto_Nominal As Double
'Este es la variable que contiene el codigo del instrumento que fue
'iniciado para operar
Dim fCod_Instrumento As String

'Dim frowgrilla%, edcol%, edKey%
Const vb_vs_numerico As Integer = 1
Const vb_vs_alfanumerico As Integer = 2
Const vb_vs_decimal As Integer = 3
Const vb_vs_fecha As Integer = 4

Enum TiposPactos
   Numerico = vb_vs_numerico
   AlfaNumerico = vb_vs_alfanumerico
   Decimales = vb_vs_decimal
   Fecha = vb_vs_fecha
End Enum


Public Function Mostrar(pTipo_Operacion As String, _
                        pCod_Instrumento As String, _
                        pNombreOperacion As String, _
                        pOper_Fecha_Anterior As Boolean, _
                        Optional pId_Operacion As String = "") As Boolean
  
    If Fnt_Verifica_Feriado(Fnt_FechaServidor) And Not pOper_Fecha_Anterior Then
      Mostrar = False
      MsgBox "Solo se pueden ingresar operaciones en d�as habiles.", vbExclamation, Me.Caption
      Unload Me
      Exit Function
    End If

'    fId_Operacion = pId_Operacion
    fCod_Producto = gcPROD_RF_NAC
    fCod_Instrumento = pCod_Instrumento
    fDsc_Instrumento = IIf(fCod_Instrumento = gcINST_PACTOS_NAC, "Pactos", "Depositos")
    fNombre_Operacion = pNombreOperacion
    fTipo_Operacion = pTipo_Operacion
    bOper_Fecha_Anterior = pOper_Fecha_Anterior
    
    Me.Caption = fDsc_Instrumento & " - " & fNombre_Operacion
    
    If Not pTipo_Operacion = gcOPERACION_Custodia Then
      lbl_fecha_liquidacion.Visible = True
      Cmb_FechaLiquidacion.Visible = True
      Dtp_FechaLiquidacion.Visible = True
    Else
      lbl_fecha_liquidacion.Visible = False
      Cmb_FechaLiquidacion.Visible = False
      Dtp_FechaLiquidacion.Visible = False
    End If
    
    Load Me

End Function

Private Sub Form_Load()

    With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SAVE").Image = cBoton_Grabar
        .Buttons("REFRESH").Image = cBoton_Original
        .Buttons("EXIT").Image = cBoton_Salir
    End With
    
    With Toolbar_Operacion
    Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("OPERACION").Image = cBoton_Modificar
        .Buttons("DETALLEOPERACION").Image = cBoton_Agregar_Grilla
    End With
    Toolbar_Operacion.Buttons(1).Enabled = True
    Toolbar_Operacion.Buttons(3).Enabled = False
 
  
    Call Sub_CargaForm
    
    Me.Top = 1
    Me.Left = 1
    
    Grilla.Editable = flexEDKbdMouse
    fRowGrilla = 0
    Pnl_Nemo.Visible = False
    Pnl_Emisores.Visible = False
    bEliminaLinea = False
 
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Rem ------------------------------------------------------------
Rem Eventos de Campos
Rem ------------------------------------------------------------
Private Sub Toolbar_Operacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "OPERACION"
        Pnl_DetalleOperacion.Enabled = False
        Pnl_DatosOperacion.Enabled = True
        Toolbar_Operacion.Buttons(1).Enabled = False
        Toolbar_Operacion.Buttons(3).Enabled = True
    Case "DETALLEOPERACION"
        Pnl_DatosOperacion.Enabled = False
        Pnl_DetalleOperacion.Enabled = True
        Toolbar_Operacion.Buttons(1).Enabled = True
        Toolbar_Operacion.Buttons(3).Enabled = False
  End Select
End Sub

Private Sub Btn_Compra_click()
    fOperacion = gcTipoOperacion_Ingreso
    Me.Caption = "Compra de " & fDsc_Instrumento & " - " & fNombre_Operacion
    fDsc_Operacion = "Compra"
    Btn_Venta.Enabled = False
    Pnl_Cuenta.Enabled = False
    Pnl_Cliente.Enabled = False
    Pnl_Operacion.Enabled = True
    Pnl_DatosOperacion.Enabled = False
    Grilla.ColHidden(c_cantidad_nemo) = True
    Grilla.Col = c_emisor
    Grilla.Row = 1
    Call grilla_StartEdit(1, c_emisor, False)
End Sub
Private Sub Btn_Venta_click()
    fOperacion = gcTipoOperacion_Egreso
    Me.Caption = "Venta de " & fDsc_Instrumento & " - " & fNombre_Operacion
    fDsc_Operacion = "Venta"
    Btn_Compra.Enabled = False
    Pnl_Cuenta.Enabled = False
    Pnl_Cliente.Enabled = False
    Pnl_Operacion.Enabled = True
    Pnl_DatosOperacion.Enabled = False
    Grilla.ColHidden(c_cantidad_nemo) = True
    Grilla.Col = c_nemo
    Grilla.Row = 1
    Call grilla_StartEdit(1, c_nemo, False)
End Sub

Private Sub Cmb_Cuentas_GotFocus()
    Txt_Rut.Text = ""
    Txt_Perfil.Text = ""
    Txt_Nombres.Text = ""
    txtNombreAsesor.Text = ""
    
End Sub

Private Sub Cmb_Cuentas_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
      SendKeys "{TAB}"
    End If
End Sub

Private Sub Cmb_Cuentas_LostFocus()
    Call Sub_CargaDatosCliente
    Call Sub_CargaDatos
   
    If bOper_Fecha_Anterior Then
      lbl_fecha_ingreso.Visible = True
      DTP_Fecha_Operacion.Visible = True
      Txt_FechaIngreso_Real.Visible = False
      DTP_Fecha_Operacion.MaxDate = fFecha_Operacion
    Else
      lbl_fecha_ingreso.Visible = False
      DTP_Fecha_Operacion.Visible = False
      Txt_FechaIngreso_Real.Visible = True
    End If
'    Call Sub_GeneraNemotecnicoSVS
'    Txt_Cantidad.Format = Fnt_Formato_Moneda(Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos))
'    Txt_MontoTotal.Format = Fnt_Formato_Moneda(Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos))

End Sub

Private Sub Cmb_Contraparte_ItemChange()
Dim lId_Contraparte As String
  
  lId_Contraparte = Fnt_FindValue4Display(Cmb_Contraparte, Cmb_Contraparte.Text)
  
  If lId_Contraparte = "" Then
    'como nunca va a existir la contraparte -1 se pasa el parametro
    lId_Contraparte = "-1"
  End If
  
  Call Sub_CargaCombo_Traders(Cmb_Traders, lId_Contraparte)
End Sub

'Private Sub Chk_Vende_Todo_Click()
'  If Chk_Vende_Todo.Value Then
'    Rem ---------------------------------------------
'    Txt_Cantidad.Locked = True
'    Txt_Cantidad.Text = Txt_Cantidad.Tag
'    Rem ---------------------------------------------
'    Txt_MontoTotal.Locked = True
'    Rem ---------------------------------------------
'    Call Sub_ValorizaPapel
'  Else
'    Rem ---------------------------------------------
'    Txt_Cantidad.Locked = False
'    Txt_Cantidad.Text = 0
'    Rem ---------------------------------------------
'    Txt_MontoTotal.Locked = False
'    Txt_MontoTotal.Text = 0
'  End If
'End Sub



Private Sub Cmb_FechaLiquidacion_Change()
  If Not Cmb_FechaLiquidacion.Text = "" Then
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Fnt_FindValue4Display(Cmb_FechaLiquidacion, Cmb_FechaLiquidacion.Text))
  End If
End Sub

Private Sub Cmb_FechaLiquidacion_ItemChange()
  If Not Cmb_FechaLiquidacion.Text = "" Then
    Dtp_FechaLiquidacion.MinDate = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Fnt_FindValue4Display(Cmb_FechaLiquidacion, Cmb_FechaLiquidacion.Text))
    Dtp_FechaLiquidacion.Value = Dtp_FechaLiquidacion.MinDate
  End If

End Sub

Private Sub Cmb_FechaLiquidacion_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", "-" y "backspace"
  If Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

'Private Sub Cmb_Plazo_ItemChange()
'  If Not Cmb_Plazo.Text = "" Then
'    Dtp_Fecha_Vencimiento.Value = Format(DateAdd("d", To_Number(Cmb_Plazo.Text), Format(fFecha_Operacion, cFormatDate)), cFormatDate)
'    Dtp_Fecha_Valuta.MinDate = Dtp_Fecha_Vencimiento.Value
'    Call Sub_Genera_Nemotecnico_SVS
'  End If
'End Sub

Private Sub DTP_Fecha_Operacion_Change()
    fFecha_Operacion = DTP_Fecha_Operacion.Value
    'Dtp_Fecha_Vencimiento.MinDate = DTP_Fecha_Operacion.Value
    'Dtp_Fecha_Vencimiento.Value = Format(fFecha_Operacion + 30, cFormatDate)
'    Dtp_Fecha_Valuta.MinDate = fFecha_Operacion
'    Dtp_Fecha_Valuta.Value = fFecha_Operacion
    Dtp_FechaLiquidacion.MinDate = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Fnt_FindValue4Display(Cmb_FechaLiquidacion, Cmb_FechaLiquidacion.Text))
    'Call Sub_Genera_Nemotecnico_SVS
End Sub

Private Sub Dtp_FechaLiquidacion_Change()
  Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Fnt_FindValue4Display(Cmb_FechaLiquidacion, Cmb_FechaLiquidacion.Text))
End Sub

Private Sub Form_Initialize()
    fFecha_Operacion = Fnt_FechaServidor
End Sub


Private Sub Txt_Derechos_LostFocus()
  If Not fMonto_Operacion = "" Then
    Txt_Derechos.Text = Int((fMonto_Operacion * Txt_Porcentaje_Derechos.Text) / 100)
    Call Txt_Iva_LostFocus
  
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub

Private Sub Txt_Gastos_LostFocus()
  If Not fMonto_Operacion = "" Then
    Call Txt_Iva_LostFocus
    
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub

Private Sub Txt_MontoTotal_LostFocus()
  fMonto_Operacion = Txt_MontoTotal.Text
  Txt_Comision.Text = Int((fMonto_Operacion * Txt_Porcentaje_Comision.Text) / 100)
  Txt_Derechos.Text = Int((fMonto_Operacion * Txt_Porcentaje_Derechos.Text) / 100)
  Txt_Iva.Text = Int((Int(Txt_Comision.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)) * fValor_Iva)
End Sub

Private Sub Txt_Porcentaje_Comision_LostFocus()
  If Not fMonto_Operacion = "" Then
    Txt_Comision.Text = Int((fMonto_Operacion * Txt_Porcentaje_Comision.Text) / 100)
    Call Txt_Iva_LostFocus
    
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub

Private Sub Txt_Comision_LostFocus()
  If Not fMonto_Operacion = "" Then
    Txt_Comision.Text = Int((fMonto_Operacion * Txt_Porcentaje_Comision.Text) / 100)
    Call Txt_Iva_LostFocus
  
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub

Private Sub Txt_Iva_LostFocus()
  If Not fMonto_Operacion = "" Then
    Txt_Iva.Text = Int((Int(Txt_Comision.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)) * fValor_Iva)
  
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub
'Private Sub Cmb_Plazo_Change()
'  If Not Cmb_Plazo.Text = "" Then
'    Dtp_Fecha_Vencimiento.Value = Format(DateAdd("d", To_Number(Cmb_Plazo.Text), Format(fFecha_Operacion, cFormatDate)), cFormatDate)
'    Dtp_Fecha_Valuta.MinDate = Dtp_Fecha_Vencimiento.Value
'    Call Sub_Genera_Nemotecnico_SVS
'  End If
'End Sub

Private Sub Cmb_Plazo_KeyPress(KeyAscii As Integer)
  If Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 57) Then
    KeyAscii = 0
  End If
End Sub

'Private Sub Dtp_Fecha_Vencimiento_Change()
'  Cmb_Plazo.Text = To_Number(Dtp_Fecha_Vencimiento.Value) - To_Number(Format(fFecha_Operacion, cFormatDate))
'  Dtp_Fecha_Valuta.MinDate = Dtp_Fecha_Vencimiento.Value
'  Call Sub_Genera_Nemotecnico_SVS
'End Sub

Rem ------------------------------------------------------------
Rem Manejo en Grilla Nemo
Rem ------------------------------------------------------------

Private Sub Btn_GrillaNemo_Click()
    Pnl_Nemo.Visible = False
    Grilla.Col = c_nemo
End Sub

Private Sub Grilla_Nemo_DblClick()
Dim sValor As String

    fDsc_Nemotecnico = GetCell(Grilla_Nemo, Grilla_Nemo.Row, "dsc_nemotecnico")
    fId_Nemotecnico = GetCell(Grilla_Nemo, Grilla_Nemo.Row, "id_nemotecnico")
    Pnl_Nemo.Visible = False
    If Fnt_BuscaDatosNemotecnico Then
        Grilla.Col = c_cantidad
    Else
        Grilla.Col = c_nemo
    End If
    
End Sub

Rem ------------------------------------------------------------
Rem Manejo en Grilla Emisor
Rem ------------------------------------------------------------

Private Sub Btn_GrillaEmisor_Click()
    Pnl_Emisores.Visible = False
    Grilla.Col = c_emisor
End Sub

Private Sub Grilla_Emisor_DblClick()
Dim sValor As String
    fId_Emisor = GetCell(Grilla_Emisor, Grilla_Emisor.Row, "id_emisor")
    sValor = GetCell(Grilla_Emisor, Grilla_Emisor.Row, "dsc_emisor_especifico")
    Call SetCell(Grilla, fRowGrilla, "id_emisor", fId_Emisor, pAutoSize:=False)
    Call SetCell(Grilla, fRowGrilla, "dsc_emisor", sValor, pAutoSize:=False)
    Pnl_Emisores.Visible = False
    Grilla.Col = c_fechavcto
    
End Sub

Rem ------------------------------------------------------------
Rem Manejo en Grilla Operaciones
Rem ------------------------------------------------------------
Private Sub Grilla_AfterEdit(ByVal Row As Long, ByVal Col As Long)

    Grilla.TabBehavior = flexTabControls
    fRowGrilla = Row
    fColGrilla = Col
    Select Case Col
        Case c_nemo
            fDsc_Nemotecnico = UCase(Grilla.TextMatrix(Row, Col))
            If Fnt_BuscaDatosNemotecnico Then
                Grilla.Col = c_cantidad
            Else
                Grilla.Col = c_nemo
            End If
        Case c_emisor
            fDsc_Emisor = UCase(Grilla.TextMatrix(Row, Col))
            If Fnt_BuscaDatosEmisor Then
                Grilla.Col = c_fechavcto
            Else
                Grilla.Col = c_emisor
            End If
        Case c_fechavcto
            If Fnt_ValidaFechaVcto(Grilla.TextMatrix(Row, Col)) Then
                Grilla.Col = c_moneda
            Else
                Grilla.Col = c_fechavcto
            End If
        Case c_moneda
            If Fnt_BuscaDatosMoneda(UCase(Grilla.TextMatrix(Row, Col))) Then
                Grilla.Col = c_moneda
                If fCod_Instrumento = gcINST_DEPOSITOS_NAC Or fCod_Instrumento = gcINST_DEPOSITOS_DAP Then 'JGR 080509
                    Sub_GeneraNemotecnicoSVSDepositos
                Else
                    Sub_GeneraNemotecnicoSVSPactos
                End If
                Grilla.Col = c_cantidad
            Else
                Grilla.Col = c_moneda
            End If
        Case c_cantidad
            Grilla.Col = c_precio
        Case c_precio
            Call Sub_ValorizaPapel
            Grilla.Col = c_nemo
        Case c_monto
            Call Sub_ValorizaPapel
            If fTipo_Operacion = gcTipoOperacion_Ingreso Then
                Grilla.Col = c_emisor
            Else
                Grilla.Col = c_nemo
            End If
    End Select
    bModificaCelda = False
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    
    Select Case Col
        Case c_nemo
            If fOperacion = gcTipoOperacion_Egreso Then
                Grilla.EditMask = ""
                Grilla.EditMaxLength = 12
            Else
                Cancel = True
            End If
        Case c_emisor
            If fOperacion = gcTipoOperacion_Ingreso Then
                Grilla.EditMask = ""
                Grilla.EditMaxLength = 12
            Else
                Cancel = True
            End If
        Case c_fechavcto
            If fOperacion = gcTipoOperacion_Ingreso Then
                Grilla.EditMask = "99/99/9999"
                Grilla.EditMaxLength = 10
            Else
                Cancel = True
            End If
        Case c_moneda
            If fOperacion = gcTipoOperacion_Ingreso Then
                Grilla.EditMask = ""
                Grilla.ColComboList(c_moneda) = fCmbMoneda
'                If Grilla.TextMatrix(Row, Col) = "" Then
'                    Grilla.TextMatrix(Row, Col) = "Pesos"
'                End If
            Else
                Cancel = True
            End If
        Case c_cantidad, c_precio, c_monto
            Grilla.EditMask = ""
            Grilla.EditMaxLength = 13
    End Select
End Sub

Private Sub Grilla_BeforeRowColChange(ByVal OldRow As Long, ByVal OldCol As Long, ByVal NewRow As Long, ByVal NewCol As Long, Cancel As Boolean)
    If NewRow = OldRow Then Exit Sub
    'If NewCol < OldCol Then Exit Sub
    'If OldRow > NewRow Then Exit Sub
    If Not bModificaCelda Then
        If Grilla.Cell(flexcpText, OldRow, c_nemo) = "" _
            Or Grilla.Cell(flexcpText, OldRow, c_emisor) = "" _
            Or Grilla.Cell(flexcpText, OldRow, c_moneda) = "" _
            Or Grilla.Cell(flexcpText, OldRow, c_cantidad) = "" _
            Or Grilla.Cell(flexcpText, OldRow, c_precio) = "" _
            Or Grilla.Cell(flexcpText, OldRow, c_monto) = "" Then
            Cancel = True
            bModificaCelda = False
            Exit Sub
        End If
    End If
End Sub

Private Sub Grilla_DblClick()
    fRowGrilla = Grilla.Row
    fColGrilla = Grilla.Col
    bModificaCelda = True
End Sub
Private Sub Grilla_EnterCell()
    If fOperacion = gcTipoOperacion_Ingreso Then
        Select Case Grilla.Col
            Case c_emisor, c_fechavcto, c_moneda, c_cantidad, c_precio, c_monto
                Grilla.Editable = flexEDKbdMouse
            Case Else
                Grilla.Editable = flexEDNone
        End Select
    Else
        Select Case Grilla.Col
            Case c_nemo, c_cantidad, c_precio, c_monto
                Grilla.Editable = flexEDKbdMouse
            Case Else
                Grilla.Editable = flexEDNone
        End Select
    End If
End Sub
Private Sub Grilla_KeyDownEdit(ByVal Row As Long, ByVal Col As Long, KeyCode As Integer, ByVal Shift As Integer)
'    If KeyCode = vbKeyTab Then
'        ' tabbing out of the last cell moves on to next control
'        If Row = Grilla.Rows - 1 And Col = Grilla.Cols - 1 Then
'            If (Shift And vbShiftMask) = 0 Then
'                Grilla.TabBehavior = flexTabControls
'                SendKeys "{tab}"
'            End If
'
'        ' tabbing out of the first cell moves on to previous control
'        ElseIf Row = Grilla.FixfRowGrillas And Col = Grilla.Fixfcolgrillas Then
'            If (Shift And vbShiftMask) <> 0 Then
'                Grilla.TabBehavior = flexTabControls
'                SendKeys "+{tab}"
'            End If
'        End If
'    End If
    If Col = c_nemo And KeyCode = vbKeyF5 Then
        Pnl_Nemo.Visible = True
    Else
        If Col = c_emisor And KeyCode = vbKeyF5 Then
            Pnl_Emisores.Visible = True
        End If
    End If
End Sub

Private Sub Grilla_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
    If KeyAscii <> vbKeyReturn And KeyAscii <> vbKeyBack Then
        Select Case Col
            Case c_nemo
                Call prdValidarColumnas(Me, Grilla, KeyAscii, c_nemo, AlfaNumerico)
            Case c_emisor
                Call prdValidarColumnas(Me, Grilla, KeyAscii, c_emisor, AlfaNumerico)
            Case c_cantidad
                Call prdValidarColumnas(Me, Grilla, KeyAscii, c_cantidad, Numerico)
            Case c_precio
                Call prdValidarColumnas(Me, Grilla, KeyAscii, c_precio, Decimales)
            Case c_fechavcto
'                Call prdValidarColumnas(Me, Grilla, KeyAscii, c_fechavcto, Fecha)
        End Select
    End If
End Sub

Private Sub Grilla_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        Call Sub_EliminarLinea
    End If

End Sub


Private Sub grilla_StartEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    Grilla.TabBehavior = flexTabCells
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
        If fCod_Instrumento = gcINST_PACTOS_NAC Then
            If Fnt_GrabarPactos Then
                MsgBox "Operaci�n fue grabada Exitosamente.", vbInformation, Me.Caption
                Unload Me
            End If
        Else
            If Fnt_GrabarDepositos Then
                MsgBox "Operaci�n fue grabada Exitosamente.", vbInformation, Me.Caption
                Unload Me
            End If
        End If
    Case "REFRESH"
      If MsgBox("Con esta acci�n inicializar� los valores." & vbLf & "�Realiza un nuevo ingreso?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_Limpiar
      End If
        
    Case "EXIT"
        Unload Me
  End Select
End Sub

Private Sub Sub_CargaForm()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lcIva As Class_Iva
Dim lcComisiones As Class_Comisiones_Instrumentos
'---------------------------------------------------------------
Dim lTexto As String
Dim lcMoneda As Object

  Call Sub_Bloquea_Puntero(Me)
  
    
    '------------------------------------------------
    '-- Carga Cuentas
    '------------------------------------------------
    Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, "id_cliente")
  
  
    Call Sub_Setea_Valores
    Call Sub_Setea_Comisiones
    
    '------------------------------------------------
    '-- Carga combo de Emisores Especificos, que solo sean del sector Financiero
    '---------------------------------------------------------------
'    Call Sub_CargaEmisoresSectorFinan
'
'    Call Sub_CargaCombo_Emisores_Especifico(Cmb_Emisor_Especifico)
'    Call Sub_ComboSelectedItem(Cmb_Emisor_Especifico, 246)
'
    '------------------------------------------------
    '-- Carga Combo Monedas instrumento en Grilla
    '------------------------------------------------
    Call Sub_CargaComboMoneda
    
    '------------------------------------------------
    '-- Carga Combo Monedas Pago en Grilla
    '------------------------------------------------
    Call Sub_CargaCombo_Monedas(Cmb_Moneda_Pago, , "S")
    Call Sub_ComboSelectedItem(Cmb_Moneda_Pago, 1)
    fId_Moneda_Transaccion = 1
    fDsc_Moneda_Transaccion = Cmb_Moneda_Pago.Text
  
  
    '------------------------------------------------
    '-- Carga Contrapartes segun instrumento asociado
    '------------------------------------------------
    Call Sub_CargaContrapartes
  
    '------------------------------------------------
    '-- Carga el dias liquidacion
    '------------------------------------------------
    Call Sub_CargaFechaLiquidacion
    
    If Not fTipo_Operacion = gcOPERACION_Custodia Then
    
      Rem Valor Iva del Sistema
      Set lcIva = New Class_Iva
      With lcIva
        If .Buscar(True) Then
          fValor_Iva = .Cursor(1)("valor").Value
          Txt_Iva.Caption = " Iva (a " & .Porcentaje_Iva(fValor_Iva) & "%)"
        End If
      End With
      
      '------------------------------------------------
      Rem Comisiones
      Set lcComisiones = New Class_Comisiones_Instrumentos
      With lcComisiones
        .Campo("Id_Cuenta").Valor = fId_Cuenta
        .Campo("COD_INSTRUMENTO").Valor = fCod_Instrumento
        If .Buscar(True) Then
          If .Cursor.Count > 0 Then
            Txt_Porcentaje_Comision.Text = lcIva.Porcentaje_Iva(.Cursor(1)("COMISION").Value)
            Txt_Gastos.Text = Int(.Cursor(1)("GASTOS").Value)
            fDerechosBolsa = NVL(.Cursor(1)("DERECHOS_BOLSA").Value, 0)
          End If
        End If
      End With
      Set lcComisiones = Nothing
      Set lcIva = Nothing
    
    End If
  
    ' Si la operacion es custodia, muestra el Check de Aporte o Retiro
    If fTipo_Operacion = gcOPERACION_Custodia Then
      chkAporteRetiro.Value = 1
      chkAporteRetiro.Visible = True
    Else
      chkAporteRetiro.Value = 0
      chkAporteRetiro.Visible = False
    End If


    '------------------------------------------------
    '-- Carga Grilla de Nemos
    '------------------------------------------------
    'Call Sub_CargaPanelNemotecnicos
    
    '------------------------------------------------
    '-- Carga grilla de emisores
    '------------------------------------------------
    Call Sub_CargaPanelEmisores
  
    Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_CargaDatosCliente()
Dim lcCuenta As Object
'-----------------------------------------------
Dim lReg            As hCollection.hFields
Dim lId_Cuenta      As String

    Call Sub_Bloquea_Puntero(Me)
    
    fId_Cuenta = Fnt_FindValue4Display(Cmb_Cuentas, Cmb_Cuentas.Text)
  
    If Not fId_Cuenta = "" Then
        'Busca el perfil de la cuenta.
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("id_cuenta").Valor = fId_Cuenta
            If .Buscar_Vigentes Then
                If .Cursor.Count > 0 Then
                    Txt_Perfil.Text = .Cursor(1)("dsc_perfil_riesgo").Value
                    fId_Cliente = .Cursor(1)("id_cliente").Value
                    Txt_Rut.Text = "" & .Cursor(1)("rut_cliente").Value
                    Txt_Nombres.Text = "" & .Cursor(1)("nombre_cliente").Value
                    txtNombreAsesor.Text = "" & .Cursor(1)("nombre_Asesor").Value
                End If
            Else
                MsgBox "Problemas en cargar la Cuenta." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            End If
        End With
        Set lcCuenta = Nothing
        Call Sub_CargaPanelNemotecnicos
    
    End If
  
End Sub

Private Sub Sub_CargaDatos()
Dim lReg As hFields
Dim lcSaldos_Activos As Class_Saldo_Activos
Dim lcTipo_Liq As Class_Tipos_Liquidacion

  Call Sub_Bloquea_Puntero(Me)
  
  '------------------------------------------------
  '-- Setea Fechas
  '------------------------------------------------
  fFecha_Operacion = Fnt_FechaServidor
  Txt_FechaIngreso_Real.Text = fFecha_Operacion
  DTP_Fecha_Operacion.Value = fFecha_Operacion
  
  If fTipo_Operacion = gcOPERACION_Custodia Then
    Dtp_FechaLiquidacion.Value = fFecha_Operacion
  Else
  
    'Dtp_FechaLiquidacion.MinDate = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
    
    Set lcTipo_Liq = New Class_Tipos_Liquidacion
    With lcTipo_Liq
      .Campo("cod_instrumento").Valor = fCod_Instrumento
      .Campo("id_empresa").Valor = Fnt_EmpresaActual
      .Campo("tipo_movimiento").Valor = fOperacion
      If .Buscar Then
        If .Cursor.Count > 0 Then
          Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, NVL(.Cursor(1).Fields("retencion").Value, 0))
          Cmb_FechaLiquidacion.Text = NVL(.Cursor(1).Fields("retencion").Value, 0)
        Else
          Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
        End If
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en carga de Tipo Liquidacion.", _
                          .ErrMsg, _
                          pConLog:=True)
        Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
      End If
    End With
    Set lcTipo_Liq = Nothing
    
  End If
  
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Setea_Valores()
  
  Txt_MontoTotal.Text = 0
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Dim pMsgError
  'If Not Fnt_Form_Validar(Me.Controls, Frame_Principal) Then
  Dim lcCuenta As Object

  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)

  lcCuenta.Campo("id_cuenta").Valor = fId_Cuenta
  If lcCuenta.Cuenta_Bloqueada(pMsgError) Then
    Select Case fTipo_Operacion
        Case gcOPERACION_Instruccion
            MsgBox "Cuenta Bloqueada. Motivo : " & pMsgError, vbExclamation, Me.Caption
            Fnt_ValidarDatos = False
            Exit Function
        Case gcOPERACION_Directa
            pMsgError = pMsgError & vbCr & vbCr & "�Desea continuar con la operaci�n?"
            If MsgBox("Cuenta Bloqueada. Motivo : " & pMsgError, vbQuestion + vbYesNo, Me.Caption) = vbNo Then
                Fnt_ValidarDatos = False
                Exit Function
            End If
    End Select
  End If

'  If Not Fnt_Form_Validar(Me.Controls) Then
'    Fnt_ValidarDatos = False
'    Exit Function
'  End If
'
  If Fnt_Verifica_Feriado(fFecha_Operacion) Then
    Fnt_ValidarDatos = False
    MsgBox "Solo se pueden ingresar operaciones en d�as h�biles.", vbExclamation, Me.Caption
    Exit Function
  End If
'
'  If Cmb_Plazo.Text = "" Then
'    MsgBox "Debe ingresar D�as al Vencimiento.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'    Exit Function
'  End If
'
'  If Cmb_Base.Text = "" Then
'    MsgBox "Debe ingresar Base en D�as.", vbExclamation, Me.Caption
'    Fnt_ValidarDatos = False
'    Exit Function
'  End If
'
  If Txt_Porcentaje_Comision.Text < 0 Then
    MsgBox "Porcentaje Comisi�n no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Txt_Comision.Text < 0 Then
    MsgBox "Comisi�n a Cobrar no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Txt_Iva.Text < 0 Then
    MsgBox "El Valor Iva no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Txt_Gastos.Text < 0 Then
    MsgBox "Gastos no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Txt_Derechos.Text < 0 Then
    MsgBox "Derechos Gastos no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  End If

  Fnt_ValidarDatos = True
End Function


Private Sub Sub_Limpiar()
Dim lRow As Integer
Dim lCol As Integer

    Call Sub_Bloquea_Puntero(Me)
    
    For lRow = 1 To Grilla.Rows - 1
        For lCol = 0 To MAX_COLUMNAS
            Grilla.TextMatrix(lRow, lCol) = ""
        Next
    Next
        
    fDsc_Instrumento = IIf(fCod_Instrumento = gcINST_PACTOS_NAC, "Pactos", "Depositos")
    
    Me.Caption = fDsc_Instrumento & " - " & fNombre_Operacion
    
    Toolbar_Operacion.Buttons(1).Enabled = True
    Toolbar_Operacion.Buttons(3).Enabled = False

    Grilla.Editable = flexEDKbdMouse
    fRowGrilla = 0
    Pnl_Nemo.Visible = False
    Pnl_Emisores.Visible = False
    bEliminaLinea = False
 
    Call Sub_ComboSelectedItem(Cmb_Moneda_Pago, 1)
    fId_Moneda_Transaccion = 1
    fDsc_Moneda_Transaccion = Cmb_Moneda_Pago.Text
  
    Call Sub_Setea_Valores
    Call Sub_Setea_Comisiones
    Call Sub_Desbloquea_Puntero(Me)
    
    Cmb_Cuentas.Text = ""
    Txt_Rut.Text = ""
    Txt_Nombres.Text = ""
    Txt_Perfil.Text = ""
    txtNombreAsesor.Text = ""
    Pnl_Cliente.Enabled = True
    Pnl_Cuenta.Enabled = True
    Btn_Compra.Enabled = True
    Btn_Venta.Enabled = True
End Sub

Private Sub Sub_Setea_Comisiones()
  Txt_Porcentaje_Comision.Text = 0
  Txt_Comision.Text = 0
  Txt_Iva.Text = 0
  Txt_Gastos.Text = 0
  Txt_Derechos.Text = 0
  Txt_TotalDetalle.Text = 0
End Sub

Private Function Fnt_GrabarPactos() As Boolean
Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc
Dim lNemotecnico        As Class_Nemotecnicos
'-------------------------------------------------------
Dim lId_Moneda_Pago As String
Dim lPactos As Class_Pactos
Dim lId_Nemotecnico As String
Dim lId_Emisor_Especifico As String
Dim lPlazo As String
Dim lBase As String
Dim lReferenciado As String
Dim lTipo_Deposito As String
Dim lId_Contraparte As String
Dim lFecha_Operacion  As Date
Dim lFecha_Vigencia As Date
Dim lFecha_Liquidacion As Date
Dim lId_Moneda_Deposito As String
Dim lFlg_Vende_Todo As String
Dim lTasa_Historico As String
Dim lId_Trader As String
Dim lId_Mov_Activo As String
'---------------------------------
Dim lId_Caja_Cuenta As Double
Dim lNum_Error      As Double
'---------------------------------
Dim lRollback As Boolean
'------------------------------------------
Dim sCC As String   'agregado por MMA 12/11/2008
Dim sChkAporteRetiro    As String
Dim lDias_Base As Integer
Dim lLinea As Integer
    
    sChkAporteRetiro = IIf(chkAporteRetiro.Value = 1, "SI", "NO")
    

    Call Sub_Bloquea_Puntero(Me)
    Me.Enabled = False
    
    gDB.IniciarTransaccion
    lRollback = True
  
    If Not Fnt_ValidarDatos Then
        GoTo ErrProcedure
    End If

    If fDsc_Moneda_Transaccion = "PESOS" Then
        lDias_Base = 30
    Else
        lDias_Base = 360
    End If

'    lId_Mov_Activo = ""
'    lId_Moneda_Pago = Fnt_ComboSelected_KEY(Cmb_Moneda_Pago)
'    lId_Emisor_Especifico = Fnt_ComboSelected_KEY(Cmb_Emisor_Especifico)
'    lId_Moneda_Deposito = Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos)
  
    If fOperacion = gcTipoOperacion_Ingreso Then
    
        Select Case fTipo_Operacion
          Case gcOPERACION_Instruccion, gcOPERACION_Directa
            lId_Caja_Cuenta = Fnt_CheckeaFinanciamiento(pId_Cuenta:=fId_Cuenta _
                                                       , pCod_Mercado:=fc_Mercado _
                                                       , pMonto:=To_Number(Txt_MontoTotal.Text) _
                                                       , pId_Moneda:=lId_Moneda_Pago _
                                                       , pFecha_Liquidacion:=Dtp_FechaLiquidacion.Value _
                                                       , pNum_Error:=lNum_Error)
                                                       
            'VERITICA EL RESULTADO DE LA OPERACION
            Select Case lNum_Error
              Case 0, eFinanciamiento_Caja.eFC_InversionDescubierta
                'SI SON ESTOS VALORES SIGNIFICA QUE LA OPERACION SE PUEDE REALIZAR
              Case Else
                'Si el financiamiento tubo problemas
                GoTo ErrProcedure
            End Select
          Case gcOPERACION_Custodia, gcOPERACION_Custodia_NoCapital
            lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, lId_Moneda_Pago)
            If lId_Caja_Cuenta = cNewEntidad Then
              'Si el financiamiento tubo problemas
              GoTo ErrProcedure
            End If
        End Select
      Else
        Rem Si es una venta
        lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, lId_Moneda_Pago)
        If lId_Caja_Cuenta = -1 Then
          Rem Significa que hubo problema con la busqueda de la caja
          GoTo ErrProcedure
        End If
'        lId_Nemotecnico = Cmb_Nemotecnico.Columns(eNem_Id_Nemotecnico).Text
'        lId_Mov_Activo = Cmb_Nemotecnico.Columns(eNem_Id_Mov_Activo).Text
    End If
    For lLinea = 1 To (Grilla.Rows - 1)
        If GetCell(Grilla, lLinea, "dsc_emisor") = "" Then
            Exit For
        Else
            If fOperacion = gcTipoOperacion_Ingreso Then
                Rem Busca que el nemotecnicos exista en el sistema
                Rem Busca que el nemotecnicos exista en el sistema
                Set lNemotecnico = New Class_Nemotecnicos
                With lNemotecnico
                    .Campo("cod_instrumento").Valor = IIf(fCod_Instrumento = gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_NAC) 'JGR 020509 'gcINST_DEPOSITOS_NAC
                    .Campo("nemotecnico").Valor = GetCell(Grilla, lLinea, "dsc_nemotecnico")
                    If .Buscar_Nemotecnico(gcPROD_RF_NAC) Then
                        lId_Nemotecnico = .Campo("id_nemotecnico").Valor
        
                        Rem Si lId_Nemotecnico = "0" se crea el nemotecnico en el sistema (no existe ;))
                        If lId_Nemotecnico = "0" Then
                            .LimpiaParam
                            .Campo("id_Nemotecnico").Valor = cNewEntidad
                            .Campo("cod_Instrumento").Valor = IIf(fCod_Instrumento = gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_NAC) 'JGR 020509 'gcINST_DEPOSITOS_NAC
                            .Campo("id_subfamilia").Valor = lId_Subfamilia
                            .Campo("nemotecnico").Valor = GetCell(Grilla, lLinea, "dsc_nemotecnico")
                            .Campo("id_Mercado_Transaccion").Valor = fc_Mercado_Transaccion
                            .Campo("Id_Emisor_Especifico").Valor = GetCell(Grilla, lLinea, "Id_Emisor")
                            .Campo("id_Emisor_Especifico_Origen").Valor = GetCell(Grilla, lLinea, "Id_Emisor")
                            .Campo("id_Moneda").Valor = GetCell(Grilla, lLinea, "Id_Moneda")
                            .Campo("id_Moneda_transaccion").Valor = fId_Moneda_Transaccion
                            .Campo("id_Tipo_Estado").Valor = cTEstado_Nemotecnico
                            .Campo("cod_Estado").Valor = cCod_Estado_Vigente
                            .Campo("dsc_Nemotecnico").Valor = GetCell(Grilla, lLinea, "dsc_nemotecnico")
                            .Campo("tasa_Emision").Valor = GetCell(Grilla, lLinea, "precio")
                            .Campo("tipo_Tasa").Valor = ""
                            .Campo("periodicidad").Valor = ""
                            .Campo("fecha_Vencimiento").Valor = GetCell(Grilla, lLinea, "fecha_vencimiento")
                            .Campo("corte_Minimo_Papel").Valor = fc_Corte_Minimo_Pac
                            .Campo("monto_Emision").Valor = ""
                            .Campo("liquidez").Valor = ""
                            .Campo("base").Valor = lDias_Base
                            .Campo("cod_Pais").Valor = gcPais_Chile
                            .Campo("flg_Fungible").Valor = ""
                            .Campo("fecha_emision").Valor = fFecha_Operacion
        
                            If Not .Guardar Then
                                Call Fnt_MsgError(.SubTipo_LOG, _
                                                  "Problemas en grabar el Nemot�cnico.", _
                                                  .ErrMsg, _
                                                  pConLog:=True)
                                GoTo ErrProcedure
                            End If
                            lId_Nemotecnico = .Campo("id_Nemotecnico").Valor
                            
                        End If
                        Call SetCell(Grilla, lLinea, "id_nemotecnico", lId_Nemotecnico, pAutoSize:=False)
                    Else
                        GoTo ErrProcedure
                    End If
                End With
            End If
        End If
    Next
  
  If fTipo_Operacion = gcOPERACION_Instruccion Then
    Rem Valida la Restricci�n Porcentual del Perfil de Riesgo de Instrumentos y Productos
'    Set lcRestricc_Rel_Porc = New Class_Restricciones_Rel_Porc
'    With lcRestricc_Rel_Porc
'      If Not .Fnt_Restriccion_Rel_Porc(pId_Cuenta:=fId_Cuenta, _
'                                       pId_Nemotecnico:=lId_Nemotecnico, _
'                                       pMonto:=To_Number(Txt_MontoTotal.Text), _
'                                       pId_Moneda:=lId_Moneda_Pago, _
'                                       pFecha_Operacion:=DTP_Fecha_Operacion.Value) Then
'        GoTo ErrProcedure
'      End If
'    End With
'    Set lcRestricc_Rel_Porc = Nothing
  End If
  
  Rem Si es referenciado al inicio => I (inicio), sino F (final)
    lReferenciado = "F"
  
  Rem Vende todo
  lFlg_Vende_Todo = cFlg_No_Vende_Todo 'IIf(Chk_Vende_Todo.Value, cFlg_Vende_Todo, cFlg_No_Vende_Todo)
  
  Rem Tasa Historica solo para custodia y que sea compra de pacto
'  If fTipo_Operacion = gcOPERACION_Custodia And fOperacion = gcTipoOperacion_Ingreso Then
'    lTasa_Historico = Txt_Tasa_Historica.Text
'  Else
    lTasa_Historico = ""
'  End If
  
    Set lPactos = New Class_Pactos
    For lLinea = 1 To (Grilla.Rows - 1)
        If GetCell(Grilla, lLinea, "dsc_emisor") = "" Then
            Exit For
        Else
            lId_Mov_Activo = GetCell(Grilla, lLinea, "id_mov_activo")
            Rem Agrega el detalle de la operaci�n de pacto
            Call lPactos.Agregar_Operaciones_Detalle(pId_Nemotecnico:=GetCell(Grilla, lLinea, "id_nemotecnico"), _
                                                     pCantidad:=GetCell(Grilla, lLinea, "cantidad"), _
                                                     pTasa:=GetCell(Grilla, lLinea, "precio"), _
                                                     PTasa_Gestion:="", _
                                                     pPlazo:=GetCell(Grilla, lLinea, "plazo"), _
                                                     pBase:=lDias_Base, _
                                                     pFecha_Vencimiento:=GetCell(Grilla, lLinea, "fecha_vencimiento"), _
                                                     pId_Moneda_Pago:=fId_Moneda_Transaccion, _
                                                     pMonto_Pago:=GetCell(Grilla, lLinea, "monto"), _
                                                     pReferenciado:=lReferenciado, _
                                                     pTipo_Deposito:=lTipo_Deposito, _
                                                     pFecha_Valuta:=fFecha_Operacion, _
                                                     pFlg_Vende_Todo:=lFlg_Vende_Todo, _
                                                     pTasa_Historica:=lTasa_Historico, _
                                                     pId_Mov_Activo_Compra:=lId_Mov_Activo)
          End If
    Next

    lId_Contraparte = Fnt_ComboSelected_KEY(Cmb_Contraparte)
    lId_Trader = Fnt_ComboSelected_KEY(Cmb_Traders)
    lFecha_Operacion = fFecha_Operacion 'DTP_Fecha_Operacion.Value  ' Fnt_FechaServidor
    lFecha_Vigencia = lFecha_Operacion
    lFecha_Liquidacion = Dtp_FechaLiquidacion.Value
  'lTipo_Precio = "" 'Flg_Limite_Precio=> no va

    Select Case fTipo_Operacion
      Case gcOPERACION_Directa
        If Not lPactos.Realiza_Operacion_Directa(pId_Operacion:=fId_Operacion, _
                                                 pId_Cuenta:=fId_Cuenta, _
                                                 pDsc_Operacion:="", _
                                                 pTipoOperacion:=fOperacion, _
                                                 pId_Contraparte:=lId_Contraparte, _
                                                 pId_Representante:="", _
                                                 pId_Moneda_Operacion:=fId_Moneda_Transaccion, _
                                                 pFecha_Operacion:=lFecha_Operacion, _
                                                 pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                 pId_Trader:=lId_Trader, _
                                                 pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                 pComision:=Txt_Comision.Text, _
                                                 pDerechos:=Txt_Derechos.Text, _
                                                 pGastos:=Txt_Gastos.Text, _
                                                 pIva:=Txt_Iva.Text, _
                                                 pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                 pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                                 pId_Mov_Activo_Compra:=lId_Mov_Activo, _
                                                 pChkAporteRetiro:=sChkAporteRetiro) Then
          Call Fnt_MsgError(lPactos.SubTipo_LOG, _
                            "Problemas en grabar el Pacto Nacional.", _
                            lPactos.ErrMsg, _
                            pConLog:=True)
          GoTo ErrProcedure
        End If
      Case gcOPERACION_Custodia
        If Not lPactos.Realiza_Operacion_Custodia(pId_Operacion:=fId_Operacion, _
                                                  pId_Cuenta:=fId_Cuenta, _
                                                  pDsc_Operacion:="", _
                                                  pTipoOperacion:=fOperacion, _
                                                  pId_Contraparte:=lId_Contraparte, _
                                                  pId_Representante:="", _
                                                  pId_Moneda_Operacion:=fId_Moneda_Transaccion, _
                                                  pFecha_Operacion:=lFecha_Operacion, _
                                                  pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                  pId_Trader:=lId_Trader, _
                                                  pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                  pComision:=Txt_Comision.Text, _
                                                  pDerechos:=Txt_Derechos.Text, _
                                                  pGastos:=Txt_Gastos.Text, _
                                                  pIva:=Txt_Iva.Text, _
                                                  pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                  pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                                  pId_Mov_Activo_Compra:=lId_Mov_Activo, _
                                                 pChkAporteRetiro:=sChkAporteRetiro) Then
                                                 
          Call Fnt_MsgError(lPactos.SubTipo_LOG, _
                            "Problemas en grabar el Pacto Nacional.", _
                            lPactos.ErrMsg, _
                            pConLog:=True)
          GoTo ErrProcedure
        End If
      Case gcOPERACION_Instruccion
        If Not lPactos.Realiza_Operacion_Instruccion(pId_Operacion:=fId_Operacion, _
                                                     pId_Cuenta:=fId_Cuenta, _
                                                     pDsc_Operacion:="", _
                                                     pTipoOperacion:=fOperacion, _
                                                     pId_Contraparte:=lId_Contraparte, _
                                                     pId_Representante:="", _
                                                     pId_Moneda_Operacion:=fId_Moneda_Transaccion, _
                                                     pFecha_Operacion:=lFecha_Operacion, _
                                                     pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                     pId_Trader:=lId_Trader, _
                                                     pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                     pComision:=Txt_Comision.Text, _
                                                     pDerechos:=Txt_Derechos.Text, _
                                                     pGastos:=Txt_Gastos.Text, _
                                                     pIva:=Txt_Iva.Text, _
                                                     pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                     pId_Mov_Activo_Compra:=lId_Mov_Activo, _
                                                     pChkAporteRetiro:=sChkAporteRetiro) Then
                                                     
          Call Fnt_MsgError(lPactos.SubTipo_LOG, _
                            "Problemas en grabar el Pacto Nacional.", _
                            lPactos.ErrMsg, _
                            pConLog:=True)
          GoTo ErrProcedure
        End If
      Case Else
        MsgBox "Operacion no reconocida para operar.", vbCritical, Me.Caption
        GoTo ErrProcedure
    End Select
    
    lRollback = False

ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    Fnt_GrabarPactos = False
  Else
    gDB.CommitTransaccion
    Fnt_GrabarPactos = True
  
    sCC = Fnt_Lee_Mail_BackOffice(gId_Empresa)
    Call Fnt_EnvioEMAIL_Trader(fId_Operacion, sCC)

    If sChkAporteRetiro = "SI" Then
        Frm_AporteRescate_Fechas_Anteriores.ImprimeDocWord fId_Operacion, pTipo:="O"
    End If
    
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Function

Rem FUNCION QUE ES LLAMADA DESDE LA PANTALLA DE CONFIRMACION DE INSTRUCCIONES
Private Sub Txt_Porcentaje_Derechos_LostFocus()
  If Not fMonto_Operacion = "" Then
    Txt_Derechos.Text = Int((fMonto_Operacion * Txt_Porcentaje_Derechos.Text) / 100)
    Call Txt_Iva_LostFocus
  
    If fOperacion = gcTipoOperacion_Ingreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      Txt_MontoTotal.Text = Int(fMonto_Operacion) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
    End If
  End If
End Sub


Private Sub Sub_ValorizaPapel()
Dim lcDepositos     As Class_Depositos
Dim lcPactos     As Class_Pactos
'-------------------------------
Dim lValorizacion   As Double
Dim lCantidad       As Double
Dim lId_Nemotecnico As String
Dim lTasa           As Double
Dim lMonto_Operacion As Double
Dim sCod_Instrumento As String
Dim lId_Moneda      As String
Dim lId_Moneda_Pago As String


    If GetCell(Grilla, fRowGrilla, "cantidad") <> "" Then
        lCantidad = To_Number(GetCell(Grilla, fRowGrilla, "cantidad"))
        lTasa = To_Number(GetCell(Grilla, fRowGrilla, "precio"))
        lId_Nemotecnico = GetCell(Grilla, fRowGrilla, "dsc_nemotecnico")
        lId_Moneda = To_Number(GetCell(Grilla, fRowGrilla, "id_moneda"))
        lId_Moneda_Pago = To_Number(fId_Moneda_Transaccion)
        
        Select Case fCod_Instrumento
            Case gcINST_PACTOS_NAC
                Set lcPactos = New Class_Pactos
                lValorizacion = lcPactos.ValorizaPapel(lId_Nemotecnico, fFecha_Operacion, lTasa, lCantidad, 1)
                Set lcPactos = Nothing
            Case gcINST_DEPOSITOS_NAC, gcINST_DEPOSITOS_DAP 'JGR 080509
                Set lcDepositos = New Class_Depositos
                lValorizacion = lcDepositos.ValorizaPapel(lId_Nemotecnico, fFecha_Operacion, lTasa, lCantidad, 1, , , , fId_Cuenta, lId_Moneda, lId_Moneda_Pago)
                Set lcDepositos = Nothing
        End Select

        
        If fOperacion = gcTipoOperacion_Ingreso Then
          lMonto_Operacion = lValorizacion + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
        ElseIf fOperacion = gcTipoOperacion_Egreso Then
          lMonto_Operacion = lValorizacion - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
        End If
        
        Call SetCell(Grilla, fRowGrilla, "monto", lMonto_Operacion)
        
        Txt_Comision.Text = Int((lValorizacion * Txt_Porcentaje_Comision.Text) / 100)
        Txt_Derechos.Text = Int((lValorizacion * Txt_Porcentaje_Derechos.Text) / 100)
        Txt_Iva.Text = Int((Int(Txt_Comision.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)) * fValor_Iva)
        Call Sub_Calcula_MontoTotal
    End If
End Sub

Private Sub Sub_Calcula_MontoTotal()
Dim lLinea As Long
Dim lTotal As Double
Dim lPorc_Comision As Double
Dim lPorc_Derechos As Double
Dim lComision As Double
Dim lDerechos As Double
Dim lGastos As Double
Dim lIva As Double
    
        
    
    lTotal = 0
        
    For lLinea = 1 To (Grilla.Rows - 1)
        If GetCell(Grilla, lLinea, "monto") <> "" Then
            lTotal = lTotal + NVL(GetCell(Grilla, lLinea, "monto"), 0)
        End If
    Next
    Txt_TotalDetalle.Text = lTotal
    Txt_MontoNeto.Text = lTotal
    
    lPorc_Comision = To_Number(Txt_Porcentaje_Comision.Text)
    lPorc_Derechos = To_Number(Txt_Porcentaje_Derechos.Text)
    lGastos = To_Number(Txt_Gastos.Text)
    
    If fOperacion = gcTipoOperacion_Ingreso Then
      
      lComision = Fnt_Techo_Numero((lTotal * lPorc_Comision) / 100)
      lDerechos = Fnt_Techo_Numero((lTotal * lPorc_Derechos) / 100)
      lIva = Fnt_Techo_Numero((lComision + lGastos + lDerechos) * fValor_Iva)
    
      Txt_MontoTotal.Text = lTotal + lComision + lIva + lGastos + lDerechos
    
    ElseIf fOperacion = gcTipoOperacion_Egreso Then
      
      lComision = Int((lTotal * lPorc_Comision) / 100)
      lDerechos = Int((lTotal * lPorc_Derechos) / 100)
      lIva = Int((lComision + lGastos + lDerechos) * fValor_Iva)
      
      Txt_MontoTotal.Text = lTotal - (lComision + lIva + lGastos + lDerechos)
    End If
    
    Txt_Comision.Text = lComision
    Txt_Derechos.Text = lDerechos
    Txt_Iva.Text = lIva
   
End Sub

Private Function Fnt_ValidaDatos_ValRF() As Boolean
Dim lId_Moneda_Deposito As String

  Fnt_ValidaDatos_ValRF = True
  
'  lId_Moneda_Deposito = Fnt_ComboSelected_KEY(Cmb_Moneda_Pactos)
'
'  If lId_Moneda_Deposito = "" Then
'    MsgBox "Debe seleccionar una Moneda Dep�sito para poder valorizar.", vbCritical, Me.Caption
'    GoTo ErrProcedure
'  ElseIf Cmb_Plazo.Text = "" Then
'    MsgBox "Debe ingresar D�as al Vencimiento para poder valorizar.", vbCritical, Me.Caption
'    GoTo ErrProcedure
'  ElseIf Not To_Number(Cmb_Plazo.Text) > 0 Then
'    MsgBox "D�as al Vencimiento debe ser un valor mayor a cero.", vbCritical, Me.Caption
'    GoTo ErrProcedure
'  ElseIf Not Dtp_Fecha_Vencimiento.Value > fFecha_Operacion Then
'    MsgBox "La Fecha de Vencimiento debe ser mayor o igual a '" & fFecha_Operacion & "'.", vbCritical, Me.Caption
'    GoTo ErrProcedure
'  ElseIf To_Number(Txt_Cantidad.Text) = 0 Then
'    MsgBox "El campo 'Nominales' no puede vac�o ni cero.", vbCritical, Me.Caption
'    GoTo ErrProcedure
'  End If

  Exit Function
  
ErrProcedure:
  Fnt_ValidaDatos_ValRF = False
  
End Function

Private Sub Sub_CargaContrapartes()
Dim lcRel_Contrapartes_Instrum As Class_Rel_Contrapartes_Instrum
Dim lReg As hFields
Dim lTexto As String

  With Cmb_Contraparte
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    
    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
    End With
    
    Set lcRel_Contrapartes_Instrum = New Class_Rel_Contrapartes_Instrum
    lcRel_Contrapartes_Instrum.Campo("cod_instrumento").Valor = fCod_Instrumento
    If lcRel_Contrapartes_Instrum.BuscarView Then
      For Each lReg In lcRel_Contrapartes_Instrum.Cursor
        lTexto = ""
        
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
          
        lTexto = lReg("DSC_CONTRAPARTE").Value
          
        .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("ID_CONTRAPARTE").Value, lTexto)
          
        Call .AddItem(lTexto)
          
      Next
    End If
    Set lcRel_Contrapartes_Instrum = Nothing
  End With

End Sub

Private Sub Sub_CargaFechaLiquidacion()
    With Cmb_FechaLiquidacion
      Call .AddItem("PH")
      Call .AddItem("PM")
      Call .AddItem("CN")
      
      With .Columns(0).ValueItems
        .Add Fnt_AgregaValueItem("0", "PH")
        .Add Fnt_AgregaValueItem("1", "PM")
        .Add Fnt_AgregaValueItem("2", "CN")
        .Translate = True
      End With
      
    End With
    Call Sub_ComboSelectedItem(Cmb_FechaLiquidacion, 0)
End Sub

Private Sub Sub_CargaPanelEmisores()
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lDSC_Tipo_Emisor As String
Dim lEmisor_Especifico As Class_Emisores_Especifico
Dim sCodSVS As String

    Call Sub_Bloquea_Puntero(Me)
    
    'Pnl_Emisores.Visible = True
    
    Grilla_Emisor.Rows = 1
    
    Set lEmisor_Especifico = New Class_Emisores_Especifico
    With lEmisor_Especifico
        If .Buscar(True) Then
            For Each lReg In .Cursor
                sCodSVS = NVL(lReg("cod_svs_nemotecnico").Value, "")
                If sCodSVS <> "" Then
                    lLinea = Grilla_Emisor.Rows
                    Call Grilla_Emisor.AddItem("")
                    
                    Call SetCell(Grilla_Emisor, lLinea, "id_emisor", lReg("ID_EMISOR_ESPECIFICO").Value, pAutoSize:=False)
                    Call SetCell(Grilla_Emisor, lLinea, "dsc_emisor_especifico", lReg("DSC_EMISOR_ESPECIFICO").Value, pAutoSize:=False)
                    Call SetCell(Grilla_Emisor, lLinea, "cod_SVS_nemotecnico", NVL(lReg("COD_SVS_NEMOTECNICO").Value, ""), pAutoSize:=False)
                End If
            Next
        End If
    End With
    Set lEmisor_Especifico = Nothing
    Call Sub_Desbloquea_Puntero(Me)
    'Pnl_Emisores.Visible = False
End Sub

Private Sub Sub_CargaPanelNemotecnicos()
Dim lReg As hFields
Dim lcSaldos_Activos As Class_Saldo_Activos
Dim lLinea As Integer

    Grilla_Nemo.Rows = 1
    Set lcSaldos_Activos = New Class_Saldo_Activos
    If lcSaldos_Activos.Buscar_Ultimo_CierreCuenta(pId_Cuenta:=fId_Cuenta, pCod_Instrumento:=fCod_Instrumento) Then
      For Each lReg In lcSaldos_Activos.Cursor
          lLinea = Grilla_Nemo.Rows
          Call Grilla_Nemo.AddItem("")
          Call SetCell(Grilla_Nemo, lLinea, "id_nemotecnico", lReg("id_nemotecnico").Value, pAutoSize:=False)
          Call SetCell(Grilla_Nemo, lLinea, "dsc_nemotecnico", lReg("nemotecnico").Value, pAutoSize:=False)
      Next
    Else
      Call Fnt_MsgError(lcSaldos_Activos.SubTipo_LOG, _
                        "Problemas en carga de de Saldos Activos.", _
                        lcSaldos_Activos.ErrMsg, _
                        pConLog:=True)
     End If
    'Pnl_Nemo.Visible = True
End Sub

Private Sub Sub_CargaComboMoneda()
Dim lcMoneda As Object
Dim lReg As hFields
Dim lLinea As Long
Dim sValor As String
    
    sValor = ""
    Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
    With lcMoneda
        If .Buscar Then
            For Each lReg In .Cursor
                If sValor = "" Then
                    sValor = lReg("dsc_moneda").Value
                Else
                    sValor = sValor & Chr$(124) & lReg("dsc_moneda").Value
                End If
                fCmbMoneda = sValor
            Next
        Else
          MsgBox .ErrMsg, vbCritical, Me.Caption
          Set lcMoneda = Nothing
          
        End If
    End With
    Set lcMoneda = Nothing
End Sub
Private Function Fnt_BuscaDatosNemotecnico() As Boolean
Dim lcNemotecnico   As Class_Nemotecnicos
Dim lcPrecio        As Class_Publicadores_Precio
Dim lcDepositos     As Class_Depositos
Dim lcBonos         As Class_Bonos
Dim lcSaldos_Activos As Class_Saldo_Activos
Dim lcMov_Activos   As Class_Mov_Activos
Dim lcOperacion_Detalle As Class_Operaciones_Detalle
Dim lReg            As hFields
Dim lId_Nemotecnico As String
Dim lId_Moneda      As Integer
Dim lId_Moneda_Tr   As Integer
Dim lSaldo_Cantidad As Double
Dim lId_Mov_Activo  As String
Dim lId_Operacion_detalle As String
Dim lresult As Boolean

    lresult = True
    If fDsc_Nemotecnico <> "" Then
        Set lcNemotecnico = New Class_Nemotecnicos
        With lcNemotecnico
            .Campo("nemotecnico").Valor = fDsc_Nemotecnico
            If .BuscarView Then
                If .Cursor.Count = 0 Then
                        MsgBox "Nemot�cnico no existe.", vbInformation, Me.Caption
                        Grilla.Col = c_nemo
                        lresult = False
                Else
                    lId_Moneda = .Cursor(1)("id_moneda").Value
                    lId_Moneda_Tr = .Cursor(1)("id_moneda_transaccion").Value
                    fDsc_Moneda_Transaccion = .Cursor(1)("dsc_moneda_transaccion").Value
                    lId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
                    lId_Mov_Activo = GetCell(Grilla, fRowGrilla, "id_mov_activo")
                    fId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
                    Call SetCell(Grilla, fRowGrilla, "dsc_nemotecnico", .Cursor(1)("nemotecnico").Value, pAutoSize:=False)
                    Call SetCell(Grilla, fRowGrilla, "id_nemotecnico", .Cursor(1)("id_nemotecnico").Value, pAutoSize:=False)
                    Call SetCell(Grilla, fRowGrilla, "dsc_emisor", .Cursor(1)("dsc_emisor_especifico").Value, pAutoSize:=False)
                    Call SetCell(Grilla, fRowGrilla, "id_emisor", .Cursor(1)("id_emisor_especifico").Value, pAutoSize:=False)
                    Call SetCell(Grilla, fRowGrilla, "id_moneda", lId_Moneda, pAutoSize:=False)
                    Call SetCell(Grilla, fRowGrilla, "dsc_moneda", .Cursor(1)("dsc_moneda").Value, pAutoSize:=False)
                    
                    fCod_Instrumento = .Cursor(1)("cod_instrumento").Value
                    fTasa_Emision = NVL(.Cursor(1)("tasa_emision").Value, 0)
                    If fRowGrilla = 0 Then
                        fId_Moneda_Transaccion = lId_Moneda_Tr
                        Call Sub_ComboSelectedItem(Cmb_Moneda_Pago, lId_Moneda_Tr)
                    End If
                    If lId_Moneda_Tr <> fId_Moneda_Transaccion Then
                        MsgBox "La moneda debe ser la misma que la de los nemot�cnicos ya ingresados", vbInformation, Me.Caption
                        lresult = False
                    End If
                End If
            Else
              MsgBox .ErrMsg, vbCritical, Me.Caption
              lresult = False
            End If
        End With
        If fOperacion = gcTipoOperacion_Egreso Then
            Set lcPrecio = New Class_Publicadores_Precio
            With lcPrecio
                .Campo("Id_Nemotecnico").Valor = fId_Nemotecnico
                .Campo("fecha").Valor = fFecha_Operacion
                If .Buscar_Ultimo_Tasa_Cta_Nemo(fId_Cuenta) Then
                    Call SetCell(Grilla, fRowGrilla, "precio", NVL(.Campo("tasa").Valor, 0), pAutoSize:=False)
                Else
                    Call Fnt_MsgError(.SubTipo_LOG, _
                                      "Error en cargar datos de Publicadores Precio.", _
                                      .ErrMsg, _
                                      pConLog:=True)
                    lresult = False
                End If
            End With
            Set lcPrecio = Nothing

            
            Set lcDepositos = New Class_Depositos
            With lcDepositos
                lSaldo_Cantidad = .Saldo_Activo_Cantidad(fId_Cuenta, lId_Nemotecnico, lId_Mov_Activo)
                Call SetCell(Grilla, fRowGrilla, "cantidad_nemo", lSaldo_Cantidad, pAutoSize:=False)
            End With
            Set lcDepositos = Nothing
            '-------------------------------------------------------------------------
            Rem Busca el plazo de la operacion para valorizar
            If lId_Mov_Activo <> "" Then
                Set lcMov_Activos = New Class_Mov_Activos
                With lcMov_Activos
                  .Campo("id_mov_activo").Valor = lId_Mov_Activo
                  If .Buscar Then
                    If .Cursor.Count > 0 Then
                      lId_Operacion_detalle = NVL(.Cursor(1)("id_operacion_detalle").Value, "")
                    End If
                  Else
                    Call Fnt_MsgError(.SubTipo_LOG, _
                                      "Error en cargar el Detalle de la Operaci�n.", _
                                      .ErrMsg, _
                                      pConLog:=True)
                    lId_Operacion_detalle = ""
                    'lResult = False
                  End If
                  Call SetCell(Grilla, fRowGrilla, "id_operacion_detalle", lId_Operacion_detalle, pAutoSize:=False)
                End With
                Set lcMov_Activos = Nothing
            End If
            If Not lId_Operacion_detalle = "" Then
              Set lcOperacion_Detalle = New Class_Operaciones_Detalle
              With lcOperacion_Detalle
                .Campo("id_operacion_detalle").Valor = lId_Operacion_detalle
                If .Buscar Then
                  If .Cursor.Count > 0 Then
                    Call SetCell(Grilla, fRowGrilla, "plazo", .Cursor(1)("plazo").Value, pAutoSize:=False)
                  End If
                Else
                  Call Fnt_MsgError(.SubTipo_LOG, _
                                    "Error en cargar Plazo de la Operaci�n para Valorizar.", _
                                    .ErrMsg, _
                                    pConLog:=True)
                    'lresult=False
                End If
              End With
              Set lcOperacion_Detalle = Nothing
            End If
            '-------------------------------------------------------------------------
            
'            Call Sub_ValorizaPapel
        End If

'        Set lcNemotecnico = Nothing
    End If
ErrProcedure:
    Set lcNemotecnico = Nothing
    Fnt_BuscaDatosNemotecnico = lresult

End Function

Private Sub Sub_GeneraNemotecnicoSVSPactos()
Dim lcPactos As Class_Pactos
Dim lId_Emisor_Especifico As String
Dim lId_Moneda_Deposito As String
Dim lId_Moneda_Pago As String
Dim dFecha As Date
Dim sValor As String

    If Not GetCell(Grilla, fRowGrilla, "dsc_emisor") = "" And _
       Not GetCell(Grilla, fRowGrilla, "dsc_moneda") = "" Then
      
        lId_Emisor_Especifico = GetCell(Grilla, fRowGrilla, "id_emisor")
        lId_Moneda_Deposito = GetCell(Grilla, fRowGrilla, "id_moneda")
        lId_Moneda_Pago = Fnt_ComboSelected_KEY(Cmb_Moneda_Pago)
        dFecha = CDate(GetCell(Grilla, fRowGrilla, "fecha_vencimiento"))
        Set lcPactos = New Class_Pactos
        sValor = lcPactos.Fnt_Genera_Nemo_SVS_Pactos(lId_Emisor_Especifico, _
                                                                   dFecha, _
                                                                   lId_Moneda_Deposito, _
                                                                   lId_Moneda_Pago)
        fDsc_Nemotecnico = sValor
        Call SetCell(Grilla, fRowGrilla, "dsc_nemotecnico", sValor, pAutoSize:=False)
'        Grilla_Bonos.Col = c_cantidad
'        Call Grilla_Bonos_StartEdit(frowgrilla, c_nemo, False)

        Set lcPactos = Nothing
    End If

End Sub
Private Sub Sub_GeneraNemotecnicoSVSDepositos()
Dim lNemotecnico            As Class_Nemotecnicos
Dim lId_Emisor_Especifico   As String
Dim lId_Moneda              As String
Dim lId_Moneda_Pago         As String
Dim sNemotecnico            As String
Dim dFecha                  As Date
Dim lresult             As String
     
    sNemotecnico = ""
    If Not GetCell(Grilla, fRowGrilla, "dsc_emisor") = "" And _
       Not GetCell(Grilla, fRowGrilla, "dsc_moneda") = "" Then
        lId_Emisor_Especifico = GetCell(Grilla, fRowGrilla, "id_emisor")
        lId_Moneda = GetCell(Grilla, fRowGrilla, "id_moneda")
        
        dFecha = CDate(GetCell(Grilla, fRowGrilla, "fecha_vencimiento"))
        lId_Moneda_Pago = ""
        
        Set lNemotecnico = New Class_Nemotecnicos
        With lNemotecnico
          .Campo("Id_Emisor_Especifico").Valor = lId_Emisor_Especifico
          .Campo("Fecha_Vencimiento").Valor = dFecha
          sNemotecnico = .Fnt_Generacion_Nemo_SVS(lId_Moneda, lId_Moneda_Pago)
        End With
        Set lNemotecnico = Nothing
    End If
    Call SetCell(Grilla, fRowGrilla, "dsc_nemotecnico", sNemotecnico, pAutoSize:=False)
'    Set lNemotecnico = New Class_Nemotecnicos
'    With lNemotecnico
'        .Campo("cod_instrumento").Valor = gcINST_DEPOSITOS_NAC
'        .Campo("nemotecnico").Valor = sNemotecnico 'Txt_Nemotecnico.Text
'        If .Buscar_Nemotecnico(gcPROD_RF_NAC) Then
'            fId_Nemotecnico = .Campo("id_nemotecnico").Valor
'            fDsc_Nemotecnico = .Campo("nemotecnico").Valor
'            lresult = Fnt_BuscaDatosNemotecnico
'        End If
'    End With

End Sub


Private Function Fnt_GrabarDepositos() As Boolean
Dim lDepositos          As Class_Depositos
Dim lNemotecnico        As Class_Nemotecnicos
Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc
Dim lId_Nemotecnico     As String
Dim lId_Emisor_Especifico As String
Dim lPlazo              As String
Dim lBase               As String
Dim lId_representante   As String
Dim lReferenciado       As String
Dim lTipo_Deposito      As String
Dim lId_Contraparte     As String
Dim lFecha_Operacion    As Date
Dim lFecha_Vigencia     As Date
Dim lFecha_Liquidacion  As Date
Dim lId_Moneda_Deposito As String
Dim lFlg_Vende_Todo     As String
Dim lTasa_Historico     As String
Dim lId_Trader          As String
Dim lId_Mov_Activo      As String
'---------------------------------
Dim lId_Caja_Cuenta     As Double
Dim lNum_Error          As Double
'---------------------------------
Dim lRollback           As Boolean
'---------------------------------
Dim sChkAporteRetiro    As String
Dim lDias_Base          As Integer
Dim lLinea              As Long
Dim lId_Subfamilia      As String


    sChkAporteRetiro = IIf(chkAporteRetiro.Value = 0, "NO", "SI")


    Call Sub_Bloquea_Puntero(Me)
    Me.Enabled = False

    gDB.IniciarTransaccion

    lRollback = True
    Fnt_GrabarDepositos = False

'  If Not Fnt_ValidarDatos Then
'    GoTo ErrProcedure
'  End If
    
    If fDsc_Moneda_Transaccion = "PESOS" Then
        lDias_Base = 30
    Else
        lDias_Base = 360
    End If
    
    If fOperacion = gcTipoOperacion_Ingreso Then
        Rem Si es una compra
        Select Case fTipo_Operacion
            Case gcOPERACION_Instruccion, gcOPERACION_Directa
                lId_Caja_Cuenta = Fnt_CheckeaFinanciamiento(pId_Cuenta:=fId_Cuenta _
                                                           , pCod_Mercado:=fc_Mercado _
                                                           , pMonto:=To_Number(Txt_MontoTotal.Text) _
                                                           , pId_Moneda:=fId_Moneda_Transaccion _
                                                           , pFecha_Liquidacion:=Dtp_FechaLiquidacion.Value _
                                                           , pNum_Error:=lNum_Error)

                'VERITICA EL RESULTADO DE LA OPERACION
                Select Case lNum_Error
                    Case 0, eFinanciamiento_Caja.eFC_InversionDescubierta
                      'SI SON ESTOS VALORES SIGNIFICA QUE LA OPERACION SE PUEDE REALIZAR
                    Case Else
                      'Si el financiamiento tuvo problemas
                      GoTo ErrProcedure
                End Select
            Case gcOPERACION_Custodia
                lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, fId_Moneda_Transaccion)
                If lId_Caja_Cuenta = cNewEntidad Then
                  'Si el financiamiento tuvo problemas
                  GoTo ErrProcedure
                End If
        End Select
    Else
        Rem Si es una venta
        lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, fId_Moneda_Transaccion)
        If lId_Caja_Cuenta < 0 Then
          Rem Significa que hubo problema con la busqueda de la caja
          GoTo ErrProcedure
        End If
    End If
    
    For lLinea = 1 To (Grilla.Rows - 1)
        If GetCell(Grilla, lLinea, "dsc_emisor") = "" Then
            Exit For
        Else
            If fOperacion = gcTipoOperacion_Ingreso Then
    
                Rem Busca que el nemotecnicos exista en el sistema
                Set lNemotecnico = New Class_Nemotecnicos
                With lNemotecnico
                    .Campo("cod_instrumento").Valor = IIf(fCod_Instrumento = gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_NAC) 'JGR 020509 'gcINST_DEPOSITOS_NAC
                    .Campo("nemotecnico").Valor = GetCell(Grilla, lLinea, "dsc_nemotecnico")
                    If .Buscar_Nemotecnico(gcPROD_RF_NAC) Then
                        lId_Nemotecnico = .Campo("id_nemotecnico").Valor
        
                        Rem Si lId_Nemotecnico = "0" se crea el nemotecnico en el sistema (no existe ;))
                        If lId_Nemotecnico = "0" Then
                            .LimpiaParam
                            .Campo("id_Nemotecnico").Valor = cNewEntidad
                            .Campo("cod_Instrumento").Valor = IIf(fCod_Instrumento = gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_DAP, gcINST_DEPOSITOS_NAC) 'JGR 020509 'gcINST_DEPOSITOS_NAC
                            .Campo("id_subfamilia").Valor = lId_Subfamilia
                            .Campo("nemotecnico").Valor = GetCell(Grilla, lLinea, "dsc_nemotecnico")
                            .Campo("id_Mercado_Transaccion").Valor = fc_Mercado_Transaccion
                            .Campo("Id_Emisor_Especifico").Valor = GetCell(Grilla, lLinea, "Id_Emisor")
                            .Campo("id_Emisor_Especifico_Origen").Valor = GetCell(Grilla, lLinea, "Id_Emisor")
                            .Campo("id_Moneda").Valor = GetCell(Grilla, lLinea, "Id_Moneda")
                            .Campo("id_Moneda_transaccion").Valor = fId_Moneda_Transaccion
                            .Campo("id_Tipo_Estado").Valor = cTEstado_Nemotecnico
                            .Campo("cod_Estado").Valor = cCod_Estado_Vigente
                            .Campo("dsc_Nemotecnico").Valor = GetCell(Grilla, lLinea, "dsc_nemotecnico")
                            .Campo("tasa_Emision").Valor = GetCell(Grilla, lLinea, "precio")
                            .Campo("tipo_Tasa").Valor = ""
                            .Campo("periodicidad").Valor = ""
                            .Campo("fecha_Vencimiento").Valor = GetCell(Grilla, lLinea, "fecha_vencimiento")
                            .Campo("corte_Minimo_Papel").Valor = fc_Corte_Minimo_Dep
                            .Campo("monto_Emision").Valor = ""
                            .Campo("liquidez").Valor = ""
                            .Campo("base").Valor = lDias_Base
                            .Campo("cod_Pais").Valor = gcPais_Chile
                            .Campo("flg_Fungible").Valor = ""
                            .Campo("fecha_emision").Valor = fFecha_Operacion
        
                            If Not .Guardar Then
                                Call Fnt_MsgError(.SubTipo_LOG, _
                                                  "Problemas en grabar el Nemot�cnico.", _
                                                  .ErrMsg, _
                                                  pConLog:=True)
                                GoTo ErrProcedure
                            End If
                            lId_Nemotecnico = .Campo("id_Nemotecnico").Valor
                            
                        End If
                        Call SetCell(Grilla, lLinea, "id_nemotecnico", lId_Nemotecnico, pAutoSize:=False)
                    Else
                        GoTo ErrProcedure
                    End If
                End With
    
    '        Else
    '            lId_Nemotecnico = Cmb_Nemotecnico.Columns(eNem_Id_Nemotecnico).Text
    '            lId_Mov_Activo = Cmb_Nemotecnico.Columns(eNem_Id_Mov_Activo).Text
            End If
        End If
    Next
    
    
    If fTipo_Operacion = gcOPERACION_Instruccion Then
        Rem Valida la Restricci�n Porcentual del Perfil de Riesgo de Instrumentos y Productos
'        Set lcRestricc_Rel_Porc = New Class_Restricciones_Rel_Porc
'        With lcRestricc_Rel_Porc
'            If Not .Fnt_Restriccion_Rel_Porc(pId_Cuenta:=fId_Cuenta, _
'                                             pId_Nemotecnico:=lId_Nemotecnico, _
'                                             pMonto:=To_Number(Txt_MontoOperacion.Text), _
'                                             pId_Moneda:=fid_moneda_transaccion, _
'                                             pFecha_Operacion:=DTP_Fecha_Operacion.Value) Then
'              GoTo ErrProcedure
'            End If
'        End With
'        Set lcRestricc_Rel_Porc = Nothing
    End If

    lReferenciado = "F"

    Rem Vende todo
    lFlg_Vende_Todo = cFlg_No_Vende_Todo 'IIf(Chk_Vende_Todo.Value, cFlg_Vende_Todo, cFlg_No_Vende_Todo)

    Rem Tasa Historica solo para custodia y que sea compra de deposito
'    If fTipo_Operacion = gcOPERACION_Custodia And fOperacion = gcTipoOperacion_Ingreso Then
'        lTasa_Historico = Txt_Tasa_Historica.Text
'    Else
        lTasa_Historico = ""
'    End If

    Set lDepositos = New Class_Depositos
    For lLinea = 1 To (Grilla.Rows - 1)
        If GetCell(Grilla, lLinea, "dsc_emisor") = "" Then
            Exit For
        Else
            lId_Mov_Activo = GetCell(Grilla, lLinea, "id_mov_activo")
            
            Call lDepositos.Agregar_Operaciones_Detalle(pId_Nemotecnico:=GetCell(Grilla, lLinea, "id_nemotecnico"), _
                                                        pCantidad:=GetCell(Grilla, lLinea, "cantidad"), _
                                                        pTasa:=GetCell(Grilla, lLinea, "precio"), _
                                                        PTasa_Gestion:="", _
                                                        pPlazo:=GetCell(Grilla, lLinea, "plazo"), _
                                                        pBase:=lDias_Base, _
                                                        pFecha_Vencimiento:=GetCell(Grilla, lLinea, "fecha_vencimiento"), _
                                                        pId_Moneda_Pago:=fId_Moneda_Transaccion, _
                                                        pMonto_Pago:=GetCell(Grilla, lLinea, "monto"), _
                                                        pReferenciado:=lReferenciado, _
                                                        pTipo_Deposito:=lTipo_Deposito, _
                                                        pFecha_Valuta:=fFecha_Operacion, _
                                                        pFlg_Vende_Todo:=lFlg_Vende_Todo, _
                                                        pTasa_Historico:=lTasa_Historico, _
                                                        pId_Mov_Activo_Compra:=lId_Mov_Activo)
        End If
    Next
    lId_Contraparte = Fnt_ComboSelected_KEY(Cmb_Contraparte)
    lId_representante = Fnt_ComboSelected_KEY(Cmb_Representantes)
    lId_Trader = Fnt_ComboSelected_KEY(Cmb_Traders)
    lFecha_Operacion = fFecha_Operacion 'DTP_Fecha_Operacion.Value  ' Fnt_FechaServidor
    lFecha_Vigencia = lFecha_Operacion
    lFecha_Liquidacion = Dtp_FechaLiquidacion.Value


    Select Case fTipo_Operacion
        Case gcOPERACION_Directa
            If Not lDepositos.Realiza_Operacion_Directa(pId_Operacion:=fId_Operacion, _
                                                        pId_Cuenta:=fId_Cuenta, _
                                                        pDsc_Operacion:="", _
                                                        pTipoOperacion:=fOperacion, _
                                                        pId_Contraparte:=lId_Contraparte, _
                                                        pId_Representante:=lId_representante, _
                                                        pId_Moneda_Operacion:=fId_Moneda_Transaccion, _
                                                        pFecha_Operacion:=lFecha_Operacion, _
                                                        pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                        pId_Trader:=lId_Trader, _
                                                        pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                        pComision:=Txt_Comision.Text, _
                                                        pDerechos_Bolsa:=Txt_Derechos.Text, _
                                                        pGastos:=Txt_Gastos.Text, _
                                                        pIva:=Txt_Iva.Text, _
                                                        pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                        pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                                        pId_Mov_Activo_Compra:=lId_Mov_Activo, _
                                                        pChkAporteRetiro:=sChkAporteRetiro, _
                                                        pCodInstrumento:=fCod_Instrumento) Then   'JGR 080509

                Call Fnt_MsgError(lDepositos.SubTipo_LOG, _
                                  "Problemas en grabar el Dep�sito Nacional.", _
                                  lDepositos.ErrMsg, _
                                  pConLog:=True)
                GoTo ErrProcedure
            End If
        Case gcOPERACION_Custodia, gcOPERACION_Custodia_NoCapital
            sChkAporteRetiro = IIf(chkAporteRetiro.Value = 1, "SI", "NO")
            If Not lDepositos.Realiza_Operacion_Custodia(pId_Operacion:=fId_Operacion, _
                                                        pId_Cuenta:=fId_Cuenta, _
                                                        pDsc_Operacion:="", _
                                                        pTipoOperacion:=fOperacion, _
                                                        pId_Contraparte:=lId_Contraparte, _
                                                        pId_Representante:=lId_representante, _
                                                        pId_Moneda_Operacion:=fId_Moneda_Transaccion, _
                                                        pFecha_Operacion:=lFecha_Operacion, _
                                                        pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                        pId_Trader:=lId_Trader, _
                                                        pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                        pComision:=Txt_Comision.Text, _
                                                        pDerechos_Bolsa:=Txt_Derechos.Text, _
                                                        pGastos:=Txt_Gastos.Text, _
                                                        pIva:=Txt_Iva.Text, _
                                                        pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                        pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                                        pId_Mov_Activo_Compra:=lId_Mov_Activo, _
                                                        pChkAporteRetiro:=sChkAporteRetiro, _
                                                        pCodInstrumento:=fCod_Instrumento) Then   'JGR 080509
                                                        
                Call Fnt_MsgError(lDepositos.SubTipo_LOG, _
                                  "Problemas en grabar el Dep�sito Nacional.", _
                                  lDepositos.ErrMsg, _
                                  pConLog:=True)
                GoTo ErrProcedure
            End If
        Case gcOPERACION_Instruccion
            If Not lDepositos.Realiza_Operacion_Instruccion(pId_Operacion:=fId_Operacion, _
                                                            pId_Cuenta:=fId_Cuenta, _
                                                            pDsc_Operacion:="", _
                                                            pTipoOperacion:=fOperacion, _
                                                            pId_Contraparte:=lId_Contraparte, _
                                                            pId_Representante:=lId_representante, _
                                                            pId_Moneda_Operacion:=fId_Moneda_Transaccion, _
                                                            pFecha_Operacion:=lFecha_Operacion, _
                                                            pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                            pId_Trader:=lId_Trader, _
                                                            pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                            pComision:=Txt_Comision.Text, _
                                                            pDerechos_Bolsa:=Txt_Derechos.Text, _
                                                            pGastos:=Txt_Gastos.Text, _
                                                            pIva:=Txt_Iva.Text, _
                                                            pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                            pId_Mov_Activo_Compra:=lId_Mov_Activo, _
                                                            pChkAporteRetiro:=sChkAporteRetiro, _
                                                            pCodInstrumento:=fCod_Instrumento) Then   'JGR 080509
                                                            
                Call Fnt_MsgError(lDepositos.SubTipo_LOG, _
                                  "Problemas en grabar el Dep�sito Nacional.", _
                                  lDepositos.ErrMsg, _
                                  pConLog:=True)
                GoTo ErrProcedure
            End If
        Case Else
            MsgBox "Operacion no reconocida para operar.", vbCritical, Me.Caption
            GoTo ErrProcedure
    End Select
    lRollback = False
    Fnt_GrabarDepositos = True
ErrProcedure:
    If lRollback Then
        gDB.RollbackTransaccion
        Fnt_GrabarDepositos = False
    Else
        gDB.CommitTransaccion
        Fnt_GrabarDepositos = True

'        Call Fnt_EnvioEMAIL_Trader(fId_Operacion)
        If sChkAporteRetiro = "SI" Then
            Frm_AporteRescate_Fechas_Anteriores.ImprimeDocWord fId_Operacion, pTipo:="O"
        End If
    End If

    Call Sub_Desbloquea_Puntero(Me)
    Me.Enabled = True
End Function


Sub prdValidarColumnas(ByVal Formulario As Form, ByVal vsGrilla As VSFlexGrid, KeyAscii As Integer, ByVal intColumna As Integer, ByVal intTipos As TiposPactos)
    Dim inti As Integer
    If vsGrilla.Col = intColumna Then
        Select Case intTipos
            Case vb_vs_numerico
                If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> vbKeyReturn _
                    And KeyAscii <> vbKeyLeft And KeyAscii <> vbKeyRight And KeyAscii <> vbKeyUp _
                    And KeyAscii <> vbKeyDown Then
                    KeyAscii = False
                End If
            Case vb_vs_alfanumerico
                KeyAscii = Asc(UCase(Chr(KeyAscii)))
            Case vb_vs_decimal
                If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> vbKeyReturn _
                    And KeyAscii <> vbKeyLeft And KeyAscii <> vbKeyRight And KeyAscii <> vbKeyUp _
                    And KeyAscii <> vbKeyDown And KeyAscii <> 46 Then
                    KeyAscii = False
                End If
            Case vb_vs_fecha
'                fcaracterfecha = fcaracterfecha + 1
'                If fcaracterfecha <> 3 And fcaracterfecha <> 6 Then 'slash
'                    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> vbKeyReturn _
'                        And KeyAscii <> vbKeyLeft And KeyAscii <> vbKeyRight And KeyAscii <> vbKeyUp _
'                        And KeyAscii <> vbKeyDown Then
'                        KeyAscii = False
'                    End If
'                Else
'                    If (Chr(KeyAscii) <> "/") And KeyAscii <> vbKeyReturn _
'                        And KeyAscii <> vbKeyLeft And KeyAscii <> vbKeyRight And KeyAscii <> vbKeyUp _
'                        And KeyAscii <> vbKeyDown Then
'                        KeyAscii = False
'                    End If
'                End If
      End Select
   End If
End Sub

Private Function Fnt_BuscaDatosMoneda(sMoneda As String) As Boolean
Dim lcMoneda As Object
Dim lresult As Boolean

    lresult = True
    Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
    With lcMoneda
      .Campo("dsc_moneda").Valor = sMoneda
      If .Buscar Then
        Call SetCell(Grilla, fRowGrilla, "id_moneda", .Cursor(1)("id_moneda").Value)
      Else
        lresult = False
        MsgBox .ErrMsg, vbCritical, Me.Caption
      End If
    End With
    Set lcMoneda = Nothing
    Fnt_BuscaDatosMoneda = lresult
End Function

Private Function Fnt_BuscaDatosEmisor() As Boolean
Dim lcEmisores      As Class_Emisores_Especifico
Dim lresult         As Boolean

    lresult = True
    If fDsc_Emisor <> "" Then
        Set lcEmisores = New Class_Emisores_Especifico
        With lcEmisores
            .Campo("cod_svs_nemotecnico").Valor = fDsc_Emisor
            If .Buscar(False) Then
                If .Cursor.Count = 0 Then
                    MsgBox "Emisor no existe.", vbInformation, Me.Caption
                    lresult = False
                Else
                    Call SetCell(Grilla, fRowGrilla, "id_emisor", .Cursor(1)("id_emisor_especifico").Value, pAutoSize:=False)
                    Call SetCell(Grilla, fRowGrilla, "dsc_emisor", .Cursor(1)("cod_svs_nemotecnico").Value, pAutoSize:=False)
                End If
            Else
                MsgBox .ErrMsg, vbCritical, Me.Caption
                lresult = False
            End If
        End With
    End If
ErrProcedure:
    Set lcEmisores = Nothing
    Fnt_BuscaDatosEmisor = lresult
End Function

Private Function Fnt_ValidaFechaVcto(sFecha As String) As Boolean
Dim lresult As Boolean
Dim cFecha
    lresult = True
    If Not IsDate(sFecha) Then
        lresult = False
        MsgBox "Fecha Invalida.", vbInformation, Me.Caption
        GoTo ErrProcedure
    Else
        If CDate(sFecha) <= fFecha_Operacion Then
            lresult = False
            MsgBox "Fecha de Vencimiento debe ser mayor a la Fecha de Operaci�n.", vbInformation, Me.Caption
        End If
    End If
    
ErrProcedure:
    Fnt_ValidaFechaVcto = lresult
End Function

Private Sub Sub_EliminarLinea()
Dim lResp As VbMsgBoxResult
Dim lCol As Integer

    lResp = MsgBox("Desea eliminar esta l�nea?", vbYesNo, Me.Caption)
    If lResp = vbYes Then
        For lCol = 0 To MAX_COLUMNAS
            Grilla.TextMatrix(fRowGrilla, lCol) = ""
        Next
        Call Sub_Calcula_MontoTotal
        Grilla.Col = c_nemo
        Grilla.Row = IIf(fRowGrilla = 1, 1, fRowGrilla - 1)
    End If
End Sub

