VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Confirmacion_Contraparte_RV 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Confirmaci�n por Contraparte Renta Variable"
   ClientHeight    =   7185
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   11850
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7185
   ScaleWidth      =   11850
   Begin VB.Frame Frm_ 
      Caption         =   "Filtros de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2655
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   11745
      Begin VB.Frame Frm_Mergen 
         Caption         =   "Margen Pr�ximos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1815
         Left            =   6150
         TabIndex        =   5
         Top             =   720
         Width           =   5115
         Begin VB.Frame Frm_Margen_Aporte 
            Caption         =   "Margen Aportes"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   1095
            Left            =   120
            TabIndex        =   10
            Top             =   600
            Width           =   2385
            Begin hControl2.hTextLabel Txt_Margen_Sup_Apo 
               Height          =   315
               Left            =   150
               TabIndex        =   11
               Tag             =   "OBLI"
               Top             =   270
               Width           =   2055
               _ExtentX        =   3625
               _ExtentY        =   556
               LabelWidth      =   700
               TextMinWidth    =   500
               Caption         =   "Superior"
               Text            =   "0"
               Text            =   "0"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Margen_Inf_Apo 
               Height          =   315
               Left            =   150
               TabIndex        =   12
               Tag             =   "OBLI"
               Top             =   660
               Width           =   2055
               _ExtentX        =   3625
               _ExtentY        =   556
               LabelWidth      =   700
               TextMinWidth    =   500
               Caption         =   "Inferior"
               Text            =   "0"
               Text            =   "0"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
         End
         Begin VB.Frame Frm_Margen_Rescate 
            Caption         =   "Margen Rescates"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   1095
            Left            =   2580
            TabIndex        =   7
            Top             =   600
            Width           =   2385
            Begin hControl2.hTextLabel Txt_Margen_Sup_Resc 
               Height          =   315
               Left            =   150
               TabIndex        =   8
               Tag             =   "OBLI"
               Top             =   270
               Width           =   2055
               _ExtentX        =   3625
               _ExtentY        =   556
               LabelWidth      =   700
               TextMinWidth    =   500
               Caption         =   "Superior"
               Text            =   "0"
               Text            =   "0"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Margen_Inf_Resc 
               Height          =   315
               Left            =   150
               TabIndex        =   9
               Tag             =   "OBLI"
               Top             =   660
               Width           =   2055
               _ExtentX        =   3625
               _ExtentY        =   556
               LabelWidth      =   700
               TextMinWidth    =   500
               Caption         =   "Inferior"
               Text            =   "0"
               Text            =   "0"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
         End
         Begin VB.CheckBox Chk_Margen 
            Caption         =   "Aplicar Margenes a Instrucciones Pr�ximas"
            Height          =   375
            Left            =   180
            TabIndex        =   6
            Top             =   210
            Width           =   3975
         End
      End
      Begin VB.Frame Frm_Busca_Archivo 
         Caption         =   "Carga Archivo Plano"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1815
         Left            =   120
         TabIndex        =   1
         Top             =   720
         Width           =   5925
         Begin VB.CommandButton Cmb_BuscaFile 
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   5400
            TabIndex        =   2
            ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
            Top             =   390
            Width           =   375
         End
         Begin hControl2.hTextLabel Txt_ArchivoPlano 
            Height          =   315
            Left            =   120
            TabIndex        =   3
            Tag             =   "OBLI"
            Top             =   390
            Width           =   5205
            _ExtentX        =   9181
            _ExtentY        =   556
            LabelWidth      =   1100
            TextMinWidth    =   1200
            Caption         =   "Archivo Plano"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
         End
         Begin MSComctlLib.Toolbar Toolbar_Carga_Archivo 
            Height          =   330
            Left            =   4110
            TabIndex        =   4
            Top             =   1290
            Width           =   1650
            _ExtentX        =   2910
            _ExtentY        =   582
            ButtonWidth     =   2461
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Parear         "
                  Key             =   "PAREAR"
               EndProperty
            EndProperty
         End
      End
      Begin MSComCtl2.DTPicker DTP_Fecha 
         Height          =   345
         Left            =   1200
         TabIndex        =   13
         Top             =   300
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   609
         _Version        =   393216
         Format          =   66519041
         CurrentDate     =   38938
      End
      Begin VB.Label Lbl_Fecha 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         Height          =   345
         Index           =   1
         Left            =   150
         TabIndex        =   14
         Top             =   300
         Width           =   1005
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   15
      Tag             =   "OBLI"
      Top             =   0
      Width           =   11850
      _ExtentX        =   20902
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Caption         =   "&Parear"
            Key             =   "RESEARCH"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Confirmar"
            Key             =   "CONF"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   16
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin TabDlg.SSTab SSTab 
      Height          =   3975
      Left            =   60
      TabIndex        =   17
      Top             =   3150
      Width           =   11745
      _ExtentX        =   20717
      _ExtentY        =   7011
      _Version        =   393216
      Tabs            =   4
      Tab             =   1
      TabsPerRow      =   6
      TabHeight       =   529
      WordWrap        =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Exactos"
      TabPicture(0)   =   "Frm_Confirmacion_Contraparte_RV.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Toolbar_Exactos"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Grilla_Exactos"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Proximos"
      TabPicture(1)   =   "Frm_Confirmacion_Contraparte_RV.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Toolbar_Proximos"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Grilla_Proximos"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Solo Contraparte"
      TabPicture(2)   =   "Frm_Confirmacion_Contraparte_RV.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Toolbar_Contraparte"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "Grilla_Contraparte"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Solo CSGPI"
      TabPicture(3)   =   "Frm_Confirmacion_Contraparte_RV.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Grilla_CSGPI"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).ControlCount=   1
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Contraparte 
         Height          =   3315
         Left            =   -74850
         TabIndex        =   18
         Top             =   480
         Width           =   10905
         _cx             =   19235
         _cy             =   5847
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   17
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Confirmacion_Contraparte_RV.frx":0070
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_CSGPI 
         Height          =   3315
         Left            =   -74850
         TabIndex        =   19
         Top             =   480
         Width           =   11415
         _cx             =   20135
         _cy             =   5847
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   14
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Confirmacion_Contraparte_RV.frx":0336
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Proximos 
         Height          =   3315
         Left            =   150
         TabIndex        =   20
         Top             =   480
         Width           =   10905
         _cx             =   19235
         _cy             =   5847
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   23
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Confirmacion_Contraparte_RV.frx":05C8
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Exactos 
         Height          =   3315
         Left            =   -74850
         TabIndex        =   21
         Top             =   480
         Width           =   10905
         _cx             =   19235
         _cy             =   5847
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   22
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Confirmacion_Contraparte_RV.frx":09D7
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Exactos 
         Height          =   660
         Left            =   -63840
         TabIndex        =   22
         Top             =   750
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_ALL"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Selecciona todos los items"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_NOTHING"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Deselecciona todos los items"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar Toolbar_Proximos 
         Height          =   660
         Left            =   11160
         TabIndex        =   23
         Top             =   750
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_ALL"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Selecciona todos los items"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_NOTHING"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Deselecciona todos los items"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar Toolbar_Contraparte 
         Height          =   660
         Left            =   -63840
         TabIndex        =   24
         Top             =   750
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_ALL"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Selecciona todos los items"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_NOTHING"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Deselecciona todos los items"
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "Frm_Confirmacion_Contraparte_RV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Dim fConf_Cont_RV As Object

Const fc_Mercado = "N"

Dim fRecord_Confirmar As hRecord

Public Function Fnt_Mostrar(pCod_Proceso_Componente As String)
  Fnt_Mostrar = False

  fKey = pCod_Proceso_Componente
  
  If Not Fnt_CargarDatos Then
    Unload Me
    Exit Function
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
  
  Fnt_Mostrar = True
End Function

Private Function Fnt_CargarDatos() As Boolean
Dim lcProceso_Componente  As Class_Proceso_Componente
Dim lMensaje              As String

On Error GoTo ErrProcedure

  Call Sub_Bloquea_Puntero(Me)
  
  Load Me
  Fnt_CargarDatos = False
  
  Set fConf_Cont_RV = Nothing
  
  'Busca el componente que corresponde al conciliador
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = fKey
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set fConf_Cont_RV = .IniciaClass(lMensaje)
    
    If fConf_Cont_RV Is Nothing Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                      , lMensaje _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  Set lcProceso_Componente = Nothing
  
  With fConf_Cont_RV
    Set .gRelogDB = gRelogDB
    .ID_EMPRESA = Fnt_EmpresaActual
  End With
    
  Fnt_CargarDatos = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la carga del conciliador (" & fKey & ")." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Function

Private Sub Cmb_BuscaFile_Click()
  Call Sub_Busca_Archivo_Plano
End Sub

Private Sub Sub_Busca_Archivo_Plano()
Dim lArchivo As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Not fConf_Cont_RV.Buscar_Contraparte(lArchivo) Then
    GoTo ErrProcedure
  End If

  Txt_ArchivoPlano.Text = lArchivo
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("RESEARCH").Image = "boton_grilla_buscar"
      .Buttons("CONF").Image = cBoton_Grabar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Carga_Archivo
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("PAREAR").Image = cBoton_Modificar
  End With
  
  With Toolbar_Exactos
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With
  
  With Toolbar_Proximos
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With
  
  With Toolbar_Contraparte
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With
  
  Call Sub_Setea_Controles
  
End Sub

Private Sub Sub_Setea_Controles()

  DTP_Fecha.Value = Format(Fnt_FechaServidor, cFormatDate)
  
  Txt_ArchivoPlano.Text = ""
  
  Chk_Margen.Value = 0
  Txt_Margen_Sup_Apo.Text = 0
  Txt_Margen_Inf_Apo.Text = 0
  Txt_Margen_Sup_Resc.Text = 0
  Txt_Margen_Inf_Resc.Text = 0
  
  Grilla_Exactos.Rows = 1
  Grilla_Proximos.Rows = 1
  Grilla_Contraparte.Rows = 1
  Grilla_CSGPI.Rows = 1
  
  SSTab.Tab = 0
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Set fConf_Cont_RV = Nothing
End Sub

Private Sub Grilla_Contraparte_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla_Contraparte.ColIndex("chk") Then
    Cancel = True
  End If
End Sub

Private Sub Grilla_CSGPI_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla_CSGPI.ColIndex("chk") Then
    Cancel = True
  End If
End Sub

Private Sub Grilla_Exactos_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla_Exactos.ColIndex("chk") Then
    Cancel = True
  End If
End Sub

Private Sub Grilla_Proximos_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla_Proximos.ColIndex("chk") Then
    Cancel = True
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
'    Case "RESEARCH"
'      Call Sub_CargarDatos
    Case "CONF"
      Call Sub_Confirmar
    Case "REFRESH"
      Call Sub_Setea_Controles
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Carga_Archivo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "PAREAR"
      Call Sub_Parear_Movimientos
  End Select
End Sub

Private Sub Sub_Parear_Movimientos()
Dim lMargen_Sup_Apo As Double
Dim lMargen_Inf_Apo As Double
Dim lMargen_Sup_Resc As Double
Dim lMargen_Inf_Resc As Double
'-----------------------------
Dim lReg_Exactos As hFields
Dim lReg_Proximos As hFields
Dim lReg_Contraparte As hFields
Dim lReg_CSGPI As hFields
'-----------------------------
Dim lLinea As Long
Dim lTipo_Movimiento As String

  Call Sub_Bloquea_Puntero(Me)
  
  If Chk_Margen.Value Then
    lMargen_Sup_Apo = To_Number(Txt_Margen_Sup_Apo.Text)
    lMargen_Inf_Apo = To_Number(Txt_Margen_Inf_Apo.Text)
    lMargen_Sup_Resc = To_Number(Txt_Margen_Sup_Resc.Text)
    lMargen_Inf_Resc = To_Number(Txt_Margen_Inf_Resc.Text)
  End If
  
  SSTab.Tab = 0
  Grilla_Exactos.Rows = 1
  Grilla_Proximos.Rows = 1
  Grilla_Contraparte.Rows = 1
  Grilla_CSGPI.Rows = 1
  
  With fConf_Cont_RV
  
    Set .gDB = gDB
    .fFechaProceso = DTP_Fecha.Value
    
    If Not .Fnt_Parear_Movimientos(Chk_Margen.Value, _
                                   lMargen_Sup_Apo, _
                                   lMargen_Inf_Apo, _
                                   lMargen_Sup_Resc, _
                                   lMargen_Inf_Resc) Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
  '  Else
  '    MsgBox "Movimientos del Dia Local listo cargado correctamente.", vbInformation
    End If
    
    For Each lReg_Exactos In .fRecord_Exactos
      If lReg_Exactos("flg_tipo_movimiento").Value = gcTipoOperacion_Ingreso Then
        lTipo_Movimiento = "Ingreso"
      ElseIf lReg_Exactos("flg_tipo_movimiento").Value = gcTipoOperacion_Egreso Then
        lTipo_Movimiento = "Egreso"
      End If
      
      lLinea = Grilla_Exactos.Rows
      Call Grilla_Exactos.AddItem("")
      Grilla_Exactos.Cell(flexcpChecked, lLinea, "chk") = flexChecked
      Call SetCell(Grilla_Exactos, lLinea, "RUT_CLIENTE", lReg_Exactos("RUT_CLIENTE").Value)
      Call SetCell(Grilla_Exactos, lLinea, "colum_pk", lReg_Exactos("Id_Operacion").Value)
      Call SetCell(Grilla_Exactos, lLinea, "id_operacion_detalle", lReg_Exactos("id_operacion_detalle").Value)
      Call SetCell(Grilla_Exactos, lLinea, "id_cuenta", lReg_Exactos("Id_cuenta").Value)
      Call SetCell(Grilla_Exactos, lLinea, "num_cuenta", lReg_Exactos("num_cuenta").Value)
      Call SetCell(Grilla_Exactos, lLinea, "flg_tipo_movimiento", lReg_Exactos("flg_tipo_movimiento").Value)
      Call SetCell(Grilla_Exactos, lLinea, "tipo_movimiento", lTipo_Movimiento)
      Call SetCell(Grilla_Exactos, lLinea, "id_nemotecnico", lReg_Exactos("id_nemotecnico_GPI").Value)
      Call SetCell(Grilla_Exactos, lLinea, "nemotecnico", lReg_Exactos("nemotecnico").Value)
      Call SetCell(Grilla_Exactos, lLinea, "CANTIDAD", lReg_Exactos("CANTIDAD").Value)
      Call SetCell(Grilla_Exactos, lLinea, "PRECIO", lReg_Exactos("PRECIO").Value)
      Call SetCell(Grilla_Exactos, lLinea, "monto_detalle", lReg_Exactos("monto").Value)
      Call SetCell(Grilla_Exactos, lLinea, "NRO_orden", lReg_Exactos("NRO_orden").Value)
      Call SetCell(Grilla_Exactos, lLinea, "CANTIDAD_cont", lReg_Exactos("CANTIDAD_cont").Value)
      Call SetCell(Grilla_Exactos, lLinea, "PRECIO_cont", lReg_Exactos("PRECIO_cont").Value)
      Call SetCell(Grilla_Exactos, lLinea, "COMISION", lReg_Exactos("COMISION").Value)
      Call SetCell(Grilla_Exactos, lLinea, "DERECHOS", lReg_Exactos("DERECHOS").Value)
      Call SetCell(Grilla_Exactos, lLinea, "GASTOS", lReg_Exactos("GASTOS").Value)
      Call SetCell(Grilla_Exactos, lLinea, "IVA", lReg_Exactos("IVA").Value)
      Call SetCell(Grilla_Exactos, lLinea, "monto_cont", lReg_Exactos("monto_cont").Value)
      Call SetCell(Grilla_Exactos, lLinea, "diferencia", lReg_Exactos("diferencia").Value)
      
    Next
    
    For Each lReg_Proximos In .fRecord_Proximos
      If lReg_Proximos("flg_tipo_movimiento").Value = gcTipoOperacion_Ingreso Then
        lTipo_Movimiento = "Ingreso"
      ElseIf lReg_Proximos("flg_tipo_movimiento").Value = gcTipoOperacion_Egreso Then
        lTipo_Movimiento = "Egreso"
      End If
      
      lLinea = Grilla_Proximos.Rows
      Call Grilla_Proximos.AddItem("")
      Grilla_Proximos.Cell(flexcpChecked, lLinea, "chk") = flexChecked
      Call SetCell(Grilla_Proximos, lLinea, "RUT_CLIENTE", lReg_Proximos("RUT_CLIENTE").Value)
      Call SetCell(Grilla_Proximos, lLinea, "colum_pk", lReg_Proximos("Id_Operacion").Value)
      Call SetCell(Grilla_Proximos, lLinea, "id_operacion_detalle", lReg_Proximos("id_operacion_detalle").Value)
      Call SetCell(Grilla_Proximos, lLinea, "id_cuenta", lReg_Proximos("Id_cuenta").Value)
      Call SetCell(Grilla_Proximos, lLinea, "num_cuenta", lReg_Proximos("num_cuenta").Value)
      Call SetCell(Grilla_Proximos, lLinea, "flg_tipo_movimiento", lReg_Proximos("flg_tipo_movimiento").Value)
      Call SetCell(Grilla_Proximos, lLinea, "tipo_movimiento", lTipo_Movimiento)
      Call SetCell(Grilla_Proximos, lLinea, "id_nemotecnico", lReg_Proximos("id_nemotecnico_GPI").Value)
      Call SetCell(Grilla_Proximos, lLinea, "nemotecnico", lReg_Proximos("nemotecnico").Value)
      Call SetCell(Grilla_Proximos, lLinea, "CANTIDAD", lReg_Proximos("CANTIDAD").Value)
      Call SetCell(Grilla_Proximos, lLinea, "PRECIO", lReg_Proximos("PRECIO").Value)
      Call SetCell(Grilla_Proximos, lLinea, "monto_detalle", lReg_Proximos("monto").Value)
      Call SetCell(Grilla_Proximos, lLinea, "NRO_orden", lReg_Proximos("NRO_orden").Value)
      Call SetCell(Grilla_Proximos, lLinea, "CANTIDAD_cont", lReg_Proximos("CANTIDAD_cont").Value)
      Call SetCell(Grilla_Proximos, lLinea, "PRECIO_cont", lReg_Proximos("PRECIO_cont").Value)
      Call SetCell(Grilla_Proximos, lLinea, "monto_cont", lReg_Proximos("monto_cont").Value)
      Call SetCell(Grilla_Proximos, lLinea, "COMISION", lReg_Proximos("COMISION").Value)
      Call SetCell(Grilla_Proximos, lLinea, "DERECHOS", lReg_Proximos("DERECHOS").Value)
      Call SetCell(Grilla_Proximos, lLinea, "GASTOS", lReg_Proximos("GASTOS").Value)
      Call SetCell(Grilla_Proximos, lLinea, "IVA", lReg_Proximos("IVA").Value)
      Call SetCell(Grilla_Proximos, lLinea, "diferencia", lReg_Proximos("diferencia").Value)
    Next
    
    For Each lReg_Contraparte In .fRecord_Contraparte
      If lReg_Contraparte("tipo_movimiento").Value = gcTipoOperacion_Ingreso Then
        lTipo_Movimiento = "Ingreso"
      ElseIf lReg_Contraparte("tipo_movimiento").Value = gcTipoOperacion_Egreso Then
        lTipo_Movimiento = "Egreso"
      End If
      
      lLinea = Grilla_Contraparte.Rows
      Call Grilla_Contraparte.AddItem("")
      Grilla_Contraparte.Cell(flexcpChecked, lLinea, "chk") = flexChecked
      Call SetCell(Grilla_Contraparte, lLinea, "colum_pk", lReg_Contraparte("NRO_orden").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "NRO_DOCUMENTO", lReg_Contraparte("NRO_DOCUMENTO").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "rut", lReg_Contraparte("RUT_CLIENTE").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "cuenta", lReg_Contraparte("cuenta").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "NEMOTECNICO", lReg_Contraparte("NEMOTECNICO").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "flg_tipo_movimiento", lReg_Contraparte("tipo_movimiento").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "tipo_movimiento", lTipo_Movimiento)
      Call SetCell(Grilla_Contraparte, lLinea, "id_nemotecnico", lReg_Contraparte("id_nemotecnico_GPI").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "fecha", lReg_Contraparte("fecha").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "CANTIDAD", lReg_Contraparte("CANTIDAD").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "PRECIO", lReg_Contraparte("PRECIO").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "monto_DETALLE", lReg_Contraparte("monto").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "COMISION", lReg_Contraparte("COMISION").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "DERECHOS", lReg_Contraparte("DERECHOS").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "GASTOS", lReg_Contraparte("GASTOS").Value)
      Call SetCell(Grilla_Contraparte, lLinea, "IVA", lReg_Contraparte("IVA").Value)
      
    Next
    
    For Each lReg_CSGPI In .fRecord_CSGPI
      If lReg_CSGPI("flg_tipo_movimiento").Value = gcTipoOperacion_Ingreso Then
        lTipo_Movimiento = "Ingreso"
      ElseIf lReg_CSGPI("flg_tipo_movimiento").Value = gcTipoOperacion_Egreso Then
        lTipo_Movimiento = "Egreso"
      End If
      
      lLinea = Grilla_CSGPI.Rows
      Call Grilla_CSGPI.AddItem("")
      Grilla_CSGPI.Cell(flexcpChecked, lLinea, "chk") = flexUnchecked
      Call SetCell(Grilla_CSGPI, lLinea, "colum_pk", lReg_CSGPI("Id_Operacion").Value)
      Call SetCell(Grilla_CSGPI, lLinea, "id_operacion_detalle", lReg_CSGPI("id_operacion_detalle").Value)
      Call SetCell(Grilla_CSGPI, lLinea, "rut", lReg_CSGPI("RUT_CLIENTE").Value)
      Call SetCell(Grilla_CSGPI, lLinea, "id_cuenta", lReg_CSGPI("Id_cuenta").Value)
      Call SetCell(Grilla_CSGPI, lLinea, "num_cuenta", lReg_CSGPI("num_cuenta").Value)
      Call SetCell(Grilla_CSGPI, lLinea, "flg_tipo_movimiento", lReg_CSGPI("flg_tipo_movimiento").Value)
      Call SetCell(Grilla_CSGPI, lLinea, "tipo_movimiento", lTipo_Movimiento)
      Call SetCell(Grilla_CSGPI, lLinea, "id_nemotecnico", lReg_CSGPI("id_nemotecnico").Value)
      Call SetCell(Grilla_CSGPI, lLinea, "nemotecnico", lReg_CSGPI("nemotecnico").Value)
      Call SetCell(Grilla_CSGPI, lLinea, "cantidad", lReg_CSGPI("cantidad").Value)
      Call SetCell(Grilla_CSGPI, lLinea, "precio", lReg_CSGPI("precio").Value)
      Call SetCell(Grilla_CSGPI, lLinea, "monto_detalle", lReg_CSGPI("monto").Value)
      Call SetCell(Grilla_CSGPI, lLinea, "id_moneda_operacion", lReg_CSGPI("id_moneda").Value)
    Next
    
  End With
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Toolbar_Contraparte_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Contraparte, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Contraparte, False)
  End Select
End Sub

Private Sub Toolbar_Exactos_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Exactos, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Exactos, False)
  End Select
End Sub

Private Sub Toolbar_Proximos_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla_Proximos, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla_Proximos, False)
  End Select
End Sub

Private Sub Txt_Margen_Inf_Apo_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", y "backspace"
  If Not KeyAscii = 8 And Not KeyAscii = 46 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Txt_Margen_Inf_Resc_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", y "backspace"
  If Not KeyAscii = 8 And Not KeyAscii = 46 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Txt_Margen_Sup_Apo_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", y "backspace"
  If Not KeyAscii = 8 And Not KeyAscii = 46 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Txt_Margen_Sup_Resc_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", y "backspace"
  If Not KeyAscii = 8 And Not KeyAscii = 46 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Sub_Confirmar()
Dim lFila_Exacto As Long
Dim lFila_Proximo As Long
Dim lFila_CSGPI As Long
Dim lFila_Contraparte As Long
Dim lConfirmar As Boolean
Dim lFila_Exacto2 As Long
Dim lFila_Proximo2 As Long
Dim lFila_CSGPI2 As Long
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Validacion Then
    GoTo ExitProcedure
  End If
  
  Set fRecord_Confirmar = New hRecord
  With fRecord_Confirmar
    .ClearFields
    .AddField "ID_OPERACION", 0
    .AddField "ID_OPERACION_DETALLE", 0
    .AddField "CANTIDAD", 0
    .AddField "PRECIO", 0
    .AddField "MONTO", 0
    .AddField "COMISION", 0
    .AddField "DERECHOS", 0
    .AddField "GASTOS", 0
    .AddField "IVA", 0
    .AddField "NRO_ORDEN"
    .AddField "Confirmado", False
    .LimpiarRegistros
  End With
  
  For lFila_Exacto = 1 To Grilla_Exactos.Rows - 1
    If Grilla_Exactos.Cell(flexcpChecked, lFila_Exacto, "chk") = flexChecked Then
      Rem Si esta chequeado se incluye en la matriz por confirmar
      Call Sub_Ingr_Reg_Confirmacion(Grilla_Exactos, lFila_Exacto, True)
      
      Rem Busca en la misma grilla Exacto las filas que sean de la misma operacion
      For lFila_Exacto2 = 1 To Grilla_Exactos.Rows - 1
        Rem Si es la misma operacion y est� chequeda la incluye en la matriz por confirmar
        If GetCell(Grilla_Exactos, lFila_Exacto, "colum_pk") = GetCell(Grilla_Exactos, lFila_Exacto2, "colum_pk") Then
          If Grilla_Exactos.Cell(flexcpChecked, lFila_Proximo, "chk") = flexChecked Then
            Call Sub_Ingr_Reg_Confirmacion(Grilla_Exactos, lFila_Exacto2, True)
          End If
        End If
      Next lFila_Exacto2
      
      Rem Busca en la grilla Proximos las filas que sean de la misma operacion
      For lFila_Proximo = 1 To Grilla_Proximos.Rows - 1
        Rem Si es la misma operacion y est� chequeda la incluye en la matriz por confirmar
        If GetCell(Grilla_Exactos, lFila_Exacto, "colum_pk") = GetCell(Grilla_Proximos, lFila_Proximo, "colum_pk") Then
          If Grilla_Proximos.Cell(flexcpChecked, lFila_Proximo, "chk") = flexChecked Then
            Call Sub_Ingr_Reg_Confirmacion(Grilla_Proximos, lFila_Proximo, True)
          End If
        End If
      Next lFila_Proximo
        
      Rem Busca en la grilla CSGPI las filas que sean de la misma operacion
'      For lFila_CSGPI = 1 To Grilla_CSGPI.Rows - 1
'        Rem Si es la misma operacion y est� chequeda la incluye en la matriz por confirmar
'        If GetCell(Grilla_Exactos, lFila_Exacto, "colum_pk") = GetCell(Grilla_CSGPI, lFila_CSGPI, "colum_pk") Then
'          If Grilla_CSGPI.Cell(flexcpChecked, lFila_CSGPI, "chk") = flexChecked Then
'            Call Sub_Ingr_Reg_Confirmacion(Grilla_CSGPI, lFila_CSGPI, False)
'          End If
'        End If
'      Next lFila_CSGPI
      
    End If
  Next lFila_Exacto
    
  For lFila_Proximo = 1 To Grilla_Proximos.Rows - 1
    If Grilla_Proximos.Cell(flexcpChecked, lFila_Proximo, "chk") = flexChecked Then
      Rem Si esta chequeado se incluye en la matriz por confirmar
      Call Sub_Ingr_Reg_Confirmacion(Grilla_Proximos, lFila_Proximo, True)
                      
      Rem Busca en la misma grilla Proximo las filas que sean de la misma operacion
      For lFila_Proximo2 = 1 To Grilla_Proximos.Rows - 1
        Rem Si es la misma operacion y est� chequeda la incluye en la matriz por confirmar
        If GetCell(Grilla_Proximos, lFila_Proximo, "colum_pk") = GetCell(Grilla_Proximos, lFila_Proximo2, "colum_pk") Then
          If Grilla_Proximos.Cell(flexcpChecked, lFila_Proximo2, "chk") = flexChecked Then
            Call Sub_Ingr_Reg_Confirmacion(Grilla_Proximos, lFila_Proximo2, True)
          End If
        End If
      Next lFila_Proximo2
      
      Rem Busca en la grilla CSGPI las filas que sean de la misma operacion
'      For lFila_CSGPI = 1 To Grilla_CSGPI.Rows - 1
'        Rem Si es la misma operacion y est� chequeda la incluye en la matriz por confirmar
'        If GetCell(Grilla_Proximos, lFila_Proximo, "colum_pk") = GetCell(Grilla_CSGPI, lFila_CSGPI, "colum_pk") Then
'          If Grilla_CSGPI.Cell(flexcpChecked, lFila_CSGPI, "chk") = flexChecked Then
'            Call Sub_Ingr_Reg_Confirmacion(Grilla_CSGPI, lFila_CSGPI, False)
'          End If
'        End If
'      Next lFila_CSGPI
      
    End If
  Next lFila_Proximo
  
'  For lFila_CSGPI = 1 To Grilla_CSGPI.Rows - 1
'    Rem Busca las operaciones chequadas
'    If Grilla_CSGPI.Cell(flexcpChecked, lFila_CSGPI, "chk") = flexChecked Then
'
'      Call Sub_Ingr_Reg_Confirmacion(Grilla_CSGPI, lFila_CSGPI, False)
'
'      Rem Busca en la misma grilla CSGPI las filas que sean de la misma operacion
'      For lFila_CSGPI2 = 1 To Grilla_CSGPI.Rows - 1
'        Rem Si es la misma operacion y est� chequeda la incluye en la matriz por confirmar
'        If GetCell(Grilla_CSGPI, lFila_CSGPI, "colum_pk") = GetCell(Grilla_CSGPI, lFila_CSGPI2, "colum_pk") Then
'          If Grilla_CSGPI.Cell(flexcpChecked, lFila_CSGPI2, "chk") = flexChecked Then
'            Call Sub_Ingr_Reg_Confirmacion(Grilla_CSGPI, lFila_CSGPI2, True)
'          End If
'        End If
'      Next lFila_CSGPI2
'
'    End If
'  Next lFila_CSGPI
  
  lConfirmar = True
  
  Rem Aca se confirman las operaciones
  If Not Fnt_Confirmar Then
    lConfirmar = False
    GoTo ErrProcedure
  End If
  
  Rem Aca se realizan las operaciones directas
  If Not Fnt_Directa Then
    lConfirmar = False
    GoTo ErrProcedure
  End If
    
ErrProcedure:
  If lConfirmar Then
    MsgBox "Confirmaci�n de Instrucciones Acciones realizadas correctamente.", vbInformation, Me.Caption
  End If
  
  Call Sub_Setea_Controles
  
ExitProcedure:

  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Fnt_Validacion() As Boolean

  Fnt_Validacion = True
  
  If Grilla_Exactos.Rows = 1 And Grilla_Proximos.Rows = 1 And Grilla_Contraparte.Rows = 1 And Grilla_CSGPI.Rows = 1 Then
    MsgBox "No hay datos en las grillas para confirmar.", vbInformation, Me.Caption
    Fnt_Validacion = False
  End If
  
End Function

Private Sub Sub_Ingr_Reg_Confirmacion(pGrilla As VSFlexGrid, pFila As Long, Optional pNro_Operacion As Boolean = False)
Dim lReg As hFields

  Set lReg = fRecord_Confirmar.Add
  lReg("ID_OPERACION").Value = GetCell(pGrilla, pFila, "colum_pk")
  lReg("ID_OPERACION_DETALLE").Value = GetCell(pGrilla, pFila, "ID_OPERACION_DETALLE")
  lReg("CANTIDAD").Value = GetCell(pGrilla, pFila, "CANTIDAD")
  lReg("PRECIO").Value = GetCell(pGrilla, pFila, "PRECIO")
  lReg("MONTO").Value = GetCell(pGrilla, pFila, "MONTO_CONT")
  lReg("COMISION").Value = GetCell(pGrilla, pFila, "COMISION")
  lReg("DERECHOS").Value = GetCell(pGrilla, pFila, "DERECHOS")
  lReg("GASTOS").Value = GetCell(pGrilla, pFila, "GASTOS")
  lReg("IVA").Value = GetCell(pGrilla, pFila, "IVA")
  lReg("NRO_ORDEN").Value = GetCell(pGrilla, pFila, "NRO_ORDEN")
  
  Rem El folio pertence a la "grilla_exactos" y "grilla_proximos" solamente; para la "grilla_CSGPI" pone null
'  If pNro_Operacion Then
'    lReg("NRO_ORDEN").Value = GetCell(pGrilla, pFila, "NRO_ORDEN")
'  Else
'    lReg("NRO_ORDEN").Value = Null
'  End If
  
  pGrilla.Cell(flexcpChecked, pFila, pGrilla.ColIndex("chk")) = flexUnchecked
  
End Sub

Private Function Fnt_Confirmar() As Boolean
Dim lReg As hFields
Dim lReg2 As hFields
Dim lId_Operacion As String
Dim lId_Operacion_Anterior As String
Dim lMonto_Operacion As Double

  For Each lReg In fRecord_Confirmar
      
    If Not lReg("Confirmado").Value Then
      lMonto_Operacion = 0
      Rem Calcula el monto total de la operacion sumando los detalles
      For Each lReg2 In fRecord_Confirmar
        If lReg("id_operacion").Value = lReg2("id_operacion").Value Then
          lMonto_Operacion = lMonto_Operacion + To_Number(lReg2("MONTO_DETALLE").Value)
        End If
      Next
    
      Rem Realiza la confirmacion de instrucciones
      If Not Fnt_Grabar_Confirmacion(lReg("id_operacion").Value, lMonto_Operacion) Then
        GoTo ErrProcedure
      End If
    
      Rem Todos los detalles con la misma operacion los confirma
      For Each lReg2 In fRecord_Confirmar
        If lReg("id_operacion").Value = lReg2("id_operacion").Value Then
          lReg2("Confirmado").Value = True
        End If
      Next
    
    End If
    
  Next
  
  Fnt_Confirmar = True
  
  Exit Function
  
ErrProcedure:
  Fnt_Confirmar = False
  
End Function

Private Function Fnt_Grabar_Confirmacion(pId_Operacion As String, pMonto_Operacion As Double) As Boolean
Dim lId_Caja_Cuenta As Double
Dim lcOperaciones As Class_Operaciones
Dim lDetalle As Class_Operaciones_Detalle
Dim lReg As hFields
Dim lEsta_Detalle As Boolean
Dim lNro_Orden As Variant
Dim lRollback As Boolean

  lRollback = False
  
  gDB.IniciarTransaccion
  
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Id_Operacion").Valor = pId_Operacion
    
    Rem Busca el detalle de la operacion
    If Not .BuscaConDetalles Then
      lRollback = True
      GoTo ErrProcedure
    End If
    
    Rem Valida la caja
    If .Campo("flg_tipo_movimiento").Valor = gcTipoOperacion_Ingreso Then
      lId_Caja_Cuenta = Fnt_ValirdarFinanciamiento(.Campo("Id_cuenta").Valor, _
                                                   fc_Mercado, _
                                                   pMonto_Operacion, _
                                                   .Campo("Id_Moneda_Operacion").Valor, _
                                                   .Campo("Fecha_Liquidacion").Valor)
      If lId_Caja_Cuenta = -1 Then
        'Si el financiamiento tuvo problemas
        lRollback = True
        GoTo ErrProcedure
      End If
    Else
      'Si es una venta
      lId_Caja_Cuenta = Fnt_ValidarCaja(.Campo("Id_cuenta").Valor, _
                                        fc_Mercado, _
                                        .Campo("Id_Moneda_Operacion").Valor)
      If lId_Caja_Cuenta < 0 Then
        'Significa que hubo problema con la busqueda de la caja
        lRollback = True
        GoTo ErrProcedure
      End If
    End If
    
    Rem Busca en los detalles originales aquellos que no se confirman y los elimina de la colecci�n
    For Each lDetalle In lcOperaciones.Detalles
      lEsta_Detalle = False
      
      For Each lReg In fRecord_Confirmar
        If To_Number(lDetalle.Campo("id_operacion_detalle").Valor) = To_Number(lReg("id_operacion_detalle").Value) Then
          lNro_Orden = lReg("NRO_ORDEN").Value
          lEsta_Detalle = True
          Exit For
        End If
      Next
      
      If Not lEsta_Detalle Then
        Rem Si no esta lo remueve de la colecci�n
        .Remover_Operaciones_Detalle lDetalle.Campo("id_operacion_detalle").Valor
      Else
        Rem Si est� en la colecci�n, busca que el numero documento no est� ya procesado.
        Rem Fnt_Buscar_Rel_Nro_Oper_Detalle = True significa que encontr� la relaci�n "nro_documento-id_operacion_detalle"
        If Fnt_Buscar_Rel_Nro_Oper_Detalle(lNro_Orden) Then
          lRollback = True
          GoTo ErrProcedure
        Else
          Rem Si no est�, guarda el nro_documento del archivo y el id_operacion_detalle en la tabla Rel_Conversion
          If Not Fnt_Guardar_Rel_Nro_Oper_Detalle(lNro_Orden, lDetalle.Campo("id_operacion_detalle").Valor) Then
            lRollback = True
            GoTo ErrProcedure
          End If
        End If
      End If
      
    Next
    
    Rem Confirma la operacion
    If Not .Confirmar(lId_Caja_Cuenta) Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en la Confirmaci�n de la Instrucci�n Acciones.", _
                        .ErrMsg, _
                        pConLog:=True)
      lRollback = True
      GoTo ErrProcedure
    End If
  End With
  Set lcOperaciones = Nothing
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  
  Fnt_Grabar_Confirmacion = Not lRollback
  
End Function

Private Function Fnt_Buscar_Rel_Nro_Oper_Detalle(pNro_Orden As Variant) As Boolean
Dim lcRel_Conversion As Object 'Class_Rel_Conversiones
    
  Fnt_Buscar_Rel_Nro_Oper_Detalle = False
  
  Rem Si el nro de orden es null quiere decir que la operacion de solo de CSGPI (pertenece a la
  Rem grilla_CSGPI), no es necesario verificar el nro de orden (no existe ;))
  If IsNull(pNro_Orden) Then
    Exit Function
  End If
    
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones) 'New Class_Rel_Conversiones
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = Fnt_Busca_Id_Origen(cOrigen_BAC_Fondos_BBVA, False)
    .Campo("Id_Tipo_Conversion").Valor = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle, False)
    .Campo("valor").Valor = pNro_Orden
    If .Buscar Then
      If .Cursor.Count > 0 Then
        MsgBox "Hay un ingreso de Confirmaci�n de Acciones para el N�mero de Orden '" & pNro_Orden & "'. No se puede ingresar la operaci�n nuevamente.", vbInformation, Me.Caption
        Fnt_Buscar_Rel_Nro_Oper_Detalle = True
      End If
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en buscar la relaci�n N�mero Documento y el Detalle de la Operaci�n.", _
                        .ErrMsg, _
                        pConLog:=True)
      Fnt_Buscar_Rel_Nro_Oper_Detalle = True
    End If
  End With
  Set lcRel_Conversion = Nothing
  
End Function

Private Function Fnt_Guardar_Rel_Nro_Oper_Detalle(pNro_Orden As Variant, pId_Operacion_Detalle As String) As Boolean
Dim lcRel_Conversion As Object 'Class_Rel_Conversiones

  Fnt_Guardar_Rel_Nro_Oper_Detalle = True
  
  Rem Si el nro de orden es null quiere decir que la operacion de solo de CSGPI (pertenece a la
  Rem grilla_CSGPI), no es necesario verificar el nro de orden (no existe ;))
  If IsNull(pNro_Orden) Then
    Exit Function
  End If
  
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones) 'New Class_Rel_Conversiones
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = Fnt_Busca_Id_Origen(cOrigen_BAC_Fondos_BBVA, False)
    .Campo("Id_Tipo_Conversion").Valor = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle, False)
    .Campo("valor").Valor = pNro_Orden
    Rem En "id_entidad" se guarda el id_operacion_detalle
    .Campo("id_entidad").Valor = pId_Operacion_Detalle
    If Not .Guardar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en guardar la relaci�n N�mero de Orden y el Detalle de la Operaci�n.", _
                        .ErrMsg, _
                        pConLog:=True)
      Fnt_Guardar_Rel_Nro_Oper_Detalle = False
    End If
  End With
  Set lcRel_Conversion = Nothing
  
End Function

Private Function Fnt_Directa() As Boolean
Dim lFila_Contraparte As Long

  Rem Las operaciones de la grilla Contraparte se realizan como operaciones directas
  For lFila_Contraparte = 1 To Grilla_Contraparte.Rows - 1
    Rem Busca las operaciones chequadas
    If Grilla_Contraparte.Cell(flexcpChecked, lFila_Contraparte, "chk") = flexChecked Then
      
      Rem Fnt_Buscar_Rel_Nro_Oper_Detalle = True significa que encontr� la relaci�n "nro_documento-id_operacion_detalle"
      If Not Fnt_Buscar_Rel_Nro_Oper_Detalle(GetCell(Grilla_Contraparte, lFila_Contraparte, "colum_pk")) Then
        If Not Fnt_Ope_Directa_Cont(lFila_Contraparte) Then
          GoTo ErrProcedure
        End If
      End If
      
    End If
  Next lFila_Contraparte
    
  Fnt_Directa = True
  Exit Function
  
ErrProcedure:
  Fnt_Directa = False
  
End Function

Private Function Fnt_Ope_Directa_Cont(pFila As Long) As Boolean
Dim lcAcciones As Class_Acciones
Dim lId_Caja_Cuenta As Double
Dim lId_Nemotecnico As String
Dim lFecha_Movimiento As Date
Dim lFecha_Liquidacion As Date
'---------------------------------------
Dim lcCliente As Object
Dim lRut_Cliente As String
Dim lId_Cliente As String
'---------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lId_Cuenta As String
'---------------------------------------
'Dim lcAlias As Class_Alias
Dim lId_Moneda As String
'---------------------------------------
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
'---------------------------------------
Dim lId_Operacion As String
Dim lcOperaciones As Class_Operaciones
Dim lDetalle As Class_Operaciones_Detalle
Dim lRollback As Boolean
  
  lRollback = False
  
  gDB.IniciarTransaccion
  
  lId_Nemotecnico = GetCell(Grilla_Contraparte, pFila, "id_nemotecnico")
  
  '-----------------------------------------------------------------------------------------
  Rem Validaciones
  Rem Si el id_nemotecnico y flg_tipo_movimiento son vacios quiere decir que no se encontraron alias para estos items (estos resultados los entrega la dll ;))
  If lId_Nemotecnico = "" Then
    MsgBox "El Nemot�cnico '" & GetCell(Grilla_Contraparte, pFila, "fondo") & "' de la grilla ""Solo Contraparte"" no est� creado en el sistema." & vbCr & "No se puede operar la instrucci�n.", vbInformation, Me.Caption
    GoTo ErrProcedure
  End If
  '-----------------------------------------------------------------------------------------
  
  lRut_Cliente = GetCell(Grilla_Contraparte, pFila, "rut")
  
  Rem Busca el ID_Cliente segun el rut ingresado en el archivo
  Set lcCliente = Fnt_CreateObject(cDLL_Clientes)
  With lcCliente
    Set .gDB = gDB
    .Campo("rut_cliente").Valor = lRut_Cliente
    If .Buscar(True) Then
      Select Case .Cursor.Count
        Case 1
          lId_Cliente = NVL(.Cursor(1)("id_cliente").Value, "")
        Case 0
          MsgBox "No existen clientes en CSGPI con el Rut '" & lRut_Cliente & "'. No se puede ingresar la operaci�n de Contraparte.", vbInformation, Me.Caption
          GoTo ErrProcedure
        Case Is > 1
          MsgBox "Existe m�s de un cliente en CSGPI con el Rut '" & lRut_Cliente & "'. No se puede ingresar la operaci�n de Contraparte.", vbInformation, Me.Caption
          GoTo ErrProcedure
      End Select
    Else
      lRollback = True
      Call Fnt_MsgError(.SubTipo_LOG, _
                         "Problemas en cargar datos de Clientes.", _
                         .ErrMsg, _
                         pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  Set lcCliente = Nothing
  '-----------------------------------------------------------------------------
  
  Rem Busca las cuentas asociadas al cliente
'  Set lcCuenta = New Class_Cuentas
  
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  With lcCuenta
    .Campo("id_cliente").Valor = lId_Cliente
    If .Buscar Then
      If .Cursor.Count > 0 Then
        Rem Toma la primera cuenta encontrada
        lId_Cuenta = .Cursor(1)("id_cuenta").Value
      Else
        MsgBox "El Cliente '" & lRut_Cliente & "' no tiene cuentas asociadas en CSGPI. No se puede ingresar la operaci�n de Contraparte.", vbInformation, Me.Caption
        GoTo ErrProcedure
      End If
    Else
      lRollback = True
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar datos de Cuentas.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  Set lcCuenta = Nothing
  '-----------------------------------------------------------------------------
  
  Rem Busca la moneda del nemotecnico en CSGPI
  If Not Fnt_Buscar_Datos_Nemo(lId_Nemotecnico, lId_Moneda) Then
    lRollback = True
    GoTo ErrProcedure
  End If
  '-----------------------------------------------------------------------------
  
  Rem Busca la caja de la cuenta
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    If .Buscar_Caja_Para_Invertir(lId_Nemotecnico) Then
      If .Cursor.Count > 0 Then
        lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
      End If
    Else
      lRollback = True
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Cajas de la Cuenta.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  '-----------------------------------------------------------------------------
  
  lFecha_Movimiento = Fnt_Dia_Habil_MasProximo(CDate(GetCell(Grilla_Contraparte, pFila, "fecha")))
  lFecha_Liquidacion = lFecha_Movimiento
  
  Rem Realiza la operacion directa
  Set lcAcciones = New Class_Acciones
  With lcAcciones
    Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lId_Nemotecnico, _
                                      pCantidad:=GetCell(Grilla_Contraparte, pFila, "cantidad"), _
                                      pPrecio:=GetCell(Grilla_Contraparte, pFila, "precio"), _
                                      pId_Moneda:=lId_Moneda, _
                                      pMonto:=GetCell(Grilla_Contraparte, pFila, "monto_detalle"), _
                                      pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                      pPrecio_Historico:="")
  
    If Not .Realiza_Operacion_Directa(pId_Operacion:=lId_Operacion, _
                                      pId_Cuenta:=lId_Cuenta, _
                                      pDsc_Operacion:="", _
                                      pTipoOperacion:=GetCell(Grilla_Contraparte, pFila, "flg_tipo_movimiento"), _
                                      pId_Contraparte:="", _
                                      pId_Representante:="", _
                                      pId_Moneda_Operacion:=lId_Moneda, _
                                      pFecha_Operacion:=lFecha_Movimiento, _
                                      pFecha_Vigencia:=lFecha_Movimiento, _
                                      pFecha_Liquidacion:=lFecha_Liquidacion, _
                                      pId_Trader:="", _
                                      pPorc_Comision:=0, _
                                      pComision:=GetCell(Grilla_Contraparte, pFila, "COMISION"), _
                                      pDerechos_Bolsa:=GetCell(Grilla_Contraparte, pFila, "DERECHO"), _
                                      pGastos:=GetCell(Grilla_Contraparte, pFila, "GASTOS"), _
                                      pIva:=GetCell(Grilla_Contraparte, pFila, "IVA"), _
                                      pMonto_Operacion:=GetCell(Grilla_Contraparte, pFila, "monto_detalle"), _
                                      pTipo_Precio:=cTipo_Precio_Mercado, _
                                      pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
      lRollback = True
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al grabar Acciones Nacionales.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  '----------------------------------------------------------------------------
  
  Rem Con el nuevo id_operacion busca los detalles
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Id_Operacion").Valor = lId_Operacion
    
    Rem Busca el detalle de la operacion
    If Not .BuscaConDetalles Then
      lRollback = True
      GoTo ErrProcedure
    End If
      
    Rem Por cada id_operacion_detalle encontrado guarda la relacion
    Rem "nro_orden-id_operacion_detalle" en la tabla rel_conversiones
    For Each lDetalle In lcOperaciones.Detalles
      If Not Fnt_Guardar_Rel_Nro_Oper_Detalle(GetCell(Grilla_Contraparte, pFila, "colum_pk"), _
                                              lDetalle.Campo("id_operacion_detalle").Valor) Then
        lRollback = True
        GoTo ErrProcedure
      End If
    Next
    
  End With
  '----------------------------------------------------------------------------
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
  End If
  
  Fnt_Ope_Directa_Cont = Not lRollback
  
End Function

Private Function Fnt_Buscar_Datos_Nemo(pId_Nemotecnico As String, _
                                       ByRef pId_Moneda_Nemo As String) As Boolean
Dim lcAlias As Class_Alias
Dim lcNemotecnico As Class_Nemotecnicos
  
  Fnt_Buscar_Datos_Nemo = True
  
  Set lcNemotecnico = New Class_Nemotecnicos
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pId_Moneda_Nemo = .Cursor(1)("id_moneda_transaccion").Value
      End If
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar datos de Nemotecnico.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  Set lcNemotecnico = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Buscar_Datos_Nemo = False
End Function
