VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_FlujosResumen 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Resumen Flujos de Caja"
   ClientHeight    =   8760
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6900
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8760
   ScaleWidth      =   6900
   Begin VB.Frame Frm_Fecha_Re_Proceso 
      Caption         =   "Fechas Proceso"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   795
      Left            =   60
      TabIndex        =   16
      Top             =   390
      Width           =   6765
      Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
         Height          =   315
         Left            =   1470
         TabIndex        =   0
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   67502081
         CurrentDate     =   37732
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
         Height          =   315
         Left            =   4500
         TabIndex        =   1
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   67502081
         CurrentDate     =   37732
      End
      Begin VB.Label Lbl_Fecha_Desde 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha_Desde"
         Height          =   345
         Left            =   210
         TabIndex        =   18
         Top             =   270
         Width           =   1215
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Hasta"
         Height          =   345
         Left            =   3240
         TabIndex        =   17
         Top             =   270
         Width           =   1215
      End
   End
   Begin VB.Frame Frm_Filtros 
      Caption         =   "B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7455
      Left            =   60
      TabIndex        =   6
      Top             =   1230
      Width           =   6765
      Begin VB.Frame Frm_Cuentas 
         Caption         =   "Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4815
         Left            =   150
         TabIndex        =   12
         Top             =   2550
         Width           =   6525
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
            Height          =   4425
            Left            =   90
            TabIndex        =   13
            Top             =   270
            Width           =   5745
            _cx             =   10134
            _cy             =   7805
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   5
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Reporte_FlujosResumen.frx":0000
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Chequeo 
            Height          =   660
            Left            =   5940
            TabIndex        =   14
            Top             =   540
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar3 
               Height          =   255
               Left            =   9420
               TabIndex        =   15
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
      End
      Begin VB.Frame Frm_Propiedades_Cuentas 
         Caption         =   "Propiedades de la Cuenta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   2265
         Left            =   150
         TabIndex        =   7
         Top             =   270
         Width           =   6525
         Begin MSComctlLib.Toolbar Toolbar_Chequear_Propietario 
            Height          =   330
            Left            =   5070
            TabIndex        =   8
            Top             =   1830
            Width           =   1320
            _ExtentX        =   2328
            _ExtentY        =   582
            ButtonWidth     =   1958
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Chequear"
                  Key             =   "CHK"
                  Object.ToolTipText     =   "Busca las cuentas seg�n los filtros"
               EndProperty
            EndProperty
         End
         Begin TrueDBList80.TDBCombo Cmb_Asesor 
            Height          =   345
            Left            =   1380
            TabIndex        =   3
            Top             =   630
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_FlujosResumen.frx":00D1
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Clientes 
            Height          =   345
            Left            =   1380
            TabIndex        =   4
            Top             =   1020
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_FlujosResumen.frx":017B
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Grupos_Cuentas 
            Height          =   345
            Left            =   1380
            TabIndex        =   5
            Top             =   1410
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_FlujosResumen.frx":0225
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Perfil 
            Height          =   345
            Left            =   1380
            TabIndex        =   2
            Top             =   240
            Width           =   4605
            _ExtentX        =   8123
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_FlujosResumen.frx":02CF
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Lbl_Perfil 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Perfil de Riesgo"
            Height          =   345
            Left            =   90
            TabIndex        =   21
            Top             =   240
            Width           =   1275
         End
         Begin VB.Label lbl_GruposCuentas 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Grupos Cuentas"
            Height          =   345
            Left            =   90
            TabIndex        =   11
            Top             =   1410
            Width           =   1275
         End
         Begin VB.Label Lbl_Clientes 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Clientes"
            Height          =   345
            Left            =   90
            TabIndex        =   10
            Top             =   1020
            Width           =   1275
         End
         Begin VB.Label Lbl_Asesor 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Asesor"
            Height          =   345
            Left            =   90
            TabIndex        =   9
            Top             =   630
            Width           =   1275
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   6900
      _ExtentX        =   12171
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORT"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Recalcula Comisones"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   20
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_FlujosResumen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Form_Load()
  
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORT").Image = cBoton_Excel
      .Buttons("REFRESH").Image = cBoton_Original
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Chequear_Propietario
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("CHK").Image = cBoton_Agregar_Grilla
  End With
  
  With Toolbar_Chequeo
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
      .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
      .Appearance = ccFlat
  End With
   
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Cuentas_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla_Cuentas.ColIndex("CHK") Then
    Cancel = True
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "REPORT"
      Call Sub_Generar_Reporte
    Case "REFRESH"
      Call Sub_CargaForm
    Case "EXIT"
      Unload Me
  End Select
  
End Sub

Private Sub Sub_CargaForm()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lReg As hFields
Dim lLinea As Long

  Call Sub_FormControl_Color(Me.Controls)

  Grilla_Cuentas.Rows = 1
  
  DTP_Fecha_Hasta.Value = Fnt_FechaServidor
  DTP_Fecha_Desde.Value = Fnt_FechaServidor
  
  Rem Carga los combos con el primer elemento vac�o
  Call Sub_CargaCombo_Perfil_Riesgo(Cmb_Perfil, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Perfil, cCmbKBLANCO)
  
  Call Sub_CargaCombo_Asesor(Cmb_Asesor, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKBLANCO)
  
  Call Sub_CargaCombo_Clientes(Cmb_Clientes, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Clientes, cCmbKBLANCO)
  
  Call Sub_CargaCombo_Grupos_Cuentas(Cmb_Grupos_Cuentas, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Grupos_Cuentas, cCmbKBLANCO)

  Rem Carga las cuentas habilitadas y de la empresa en la grilla
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)

  With lcCuenta
    .Campo("Cod_Estado").Valor = cCod_Estado_Habilitada
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    If .Buscar_Vigentes Then
      Call Sub_hRecord2Grilla(lcCuenta.Cursor, Grilla_Cuentas, "id_cuenta")
    Else
      Call Fnt_MsgError(.SubTipo_LOG _
                      , "Problema con buscar las cuentas vigentes." _
                      , .ErrMsg _
                      , pConLog:=True)
      Err.Clear
    End If
  End With
  Set lcCuenta = Nothing
  
End Sub

Private Sub Toolbar_Chequear_Propietario_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "CHK"
      Call Sub_Busca_Cuentas_Propiedades
  End Select
  
End Sub

Private Sub Toolbar_Chequeo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
       Call Sub_CambiaCheck(True)
    Case "SEL_NOTHING"
       Call Sub_CambiaCheck(False)
  End Select
   
End Sub

Private Sub Sub_CambiaCheck(pValor As Boolean)
Dim lLinea As Long
Dim lCol As Long
  
  lCol = Grilla_Cuentas.ColIndex("CHK")
  If pValor Then
    For lLinea = 1 To Grilla_Cuentas.Rows - 1
      Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexChecked
    Next
  Else
    For lLinea = 1 To Grilla_Cuentas.Rows - 1
      Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexUnchecked
    Next
  End If
  
End Sub

Private Sub Sub_Busca_Cuentas_Propiedades()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lCursor As hCollection.hRecord
Dim lReg As hCollection.hFields
Dim lId_Perfil_Riesgo As String
Dim lId_Asesor As String
Dim lId_Cliente As String
Dim lId_Grupos_Cuentas As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  lId_Perfil_Riesgo = Fnt_ComboSelected_KEY(Cmb_Perfil)
  lId_Perfil_Riesgo = IIf(lId_Perfil_Riesgo = cCmbKBLANCO, "", lId_Perfil_Riesgo)
  
  lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
  lId_Asesor = IIf(lId_Asesor = cCmbKBLANCO, "", lId_Asesor)
  
  lId_Cliente = Fnt_ComboSelected_KEY(Cmb_Clientes)
  lId_Cliente = IIf(lId_Cliente = cCmbKBLANCO, "", lId_Cliente)
  
  lId_Grupos_Cuentas = Fnt_ComboSelected_KEY(Cmb_Grupos_Cuentas)
  lId_Grupos_Cuentas = IIf(lId_Grupos_Cuentas = cCmbKBLANCO, "", lId_Grupos_Cuentas)
  
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_perfil_riesgo").Valor = lId_Perfil_Riesgo
    .Campo("id_asesor").Valor = lId_Asesor
    .Campo("Id_Cliente").Valor = lId_Cliente
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar(pEnVista:=False, pId_Grupo_Cuenta:=lId_Grupos_Cuentas) Then
      For Each lReg In .Cursor
        Call Sub_Llena_Grilla_Cuentas(lReg("id_cuenta").Value)
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With

  Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Sub Sub_Llena_Grilla_Cuentas(pId_Cuenta As String)
Dim lfila As Long
Dim lCol As Long
  
  With Grilla_Cuentas
    If .Rows > 0 Then
      lCol = Grilla_Cuentas.ColIndex("CHK")
      For lfila = 1 To .Rows - 1
        If GetCell(Grilla_Cuentas, lfila, "colum_pk") = pId_Cuenta Then
          .Cell(flexcpChecked, lfila, lCol) = flexChecked
          Exit For
        End If
      Next
    End If
  End With
  
End Sub

Private Sub Sub_Generar_Reporte()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
'------------------------------------------
Dim lLinea As Integer
Dim lFecha_Desde As Date
Dim lFecha_Hasta As Date

  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ExitProcedure
  End If
  
  If DTP_Fecha_Desde.Value > DTP_Fecha_Hasta.Value Then
    MsgBox "La Fecha Desde no puede ser mayor que la Fecha Hasta.", vbInformation, Me.Caption
    GoTo ExitProcedure
  End If
  
  lFecha_Desde = DTP_Fecha_Desde.Value
  lFecha_Hasta = DTP_Fecha_Hasta.Value
  
  For lLinea = 1 To (Grilla_Cuentas.Rows - 1)
    With Grilla_Cuentas
      If .Cell(flexcpChecked, lLinea, .ColIndex("CHK")) = flexChecked Then
        If Not Fnt_Genera_Excel(pId_Cuenta:=GetCell(Grilla_Cuentas, lLinea, "colum_pk") _
                              , pFecha_Desde:=lFecha_Desde _
                              , pFecha_Hasta:=lFecha_Hasta _
                              , pFila:=lLinea) Then
          Exit For
        End If
      End If
    End With
  Next
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function Fnt_Genera_Excel(pId_Cuenta _
                                , pFecha_Desde _
                                , pFecha_Hasta _
                                , pFila) As Boolean
Dim lcMov_Caja  As Class_Mov_Caja

Dim lCursor_MC  As hRecord
Dim lReg_MC     As hFields
'-------------------------------------------------------------
Dim lcExcel As Excel.Application
Dim lcLibro As Excel.Workbook
Dim lcHoja  As Excel.Worksheet
Dim lcHoja_Resumen As Excel.Worksheet

Dim lhFechas   As hRecord
Dim lfFechas   As hFields

Dim lhDescripcion As hRecord
Dim lfDescripcion As hFields

Dim lhFila_Moneda As hRecord
Dim lfFila_Moneda As hFields

'Dim lhFila_Ingreso As hRecord
'Dim lfFila_Ingreso As hFields
'
'Dim lhFila_Egreso As hRecord
'Dim lfFila_Egreso As hFields

Dim lhFila_Total_Ingreso As hRecord
Dim lfFila_Total_Ingreso As hFields

Dim lhFila_Total_Egreso As hRecord
Dim lfFila_Total_Egreso As hFields

Dim lhFila_Saldo As hRecord
Dim lfFila_Saldo As hFields

Dim lNro_Libro As Integer

Dim lLinea_Movs As Long

Dim lLinea_Fechas As Long
Dim lLinea_Saldos As Long
Dim lColumnaFecha As Long

Dim lSaldo_Anterior As Double
Dim lCod_Movimiento As String
Dim lDsc_Mov As String

Dim lId_Moneda
Dim lFormato_Moneda As String

Dim lFilas_Ingreso As Long
Dim lFilas_Egreso As Long
Dim lTotal_Filas_Correr  As Long
Dim lFilas_con_Egreso As Long
Dim lLinea_Egreso As Long
Dim lFilas_con_Ingreso As Long
Dim lFormula_Total_Egresos As String
Dim lFila_Moneda As Long

Dim lLine As Long
Dim lCol As Long
Dim lAcum As Long

Dim lReg As hFields

Const cHoja_Detalle = 1
Const cHoja_Resumen = 2

Const cLineaTitulo = 8
Const cColumnaFecha = 1

  Fnt_Genera_Excel = False
  
  Set lhFechas = New hRecord
  With lhFechas
    .AddField "Fecha"
    .AddField "Columna"
  End With
  
  Set lhDescripcion = New hRecord
  With lhDescripcion
    .AddField "Des_Mov"
    .AddField "Fila"
    .AddField "Tipo_Mov"
  End With
  
  Set lhFila_Moneda = New hRecord
  With lhFila_Moneda
    .AddField "Fila"
  End With
  
'  Set lhFila_Ingreso = New hRecord
'  With lhFila_Ingreso
'    .AddField "id_moneda"
'    .AddField "Fila"
'  End With
'
'  Set lhFila_Egreso = New hRecord
'  With lhFila_Egreso
'    .AddField "id_moneda"
'    .AddField "Fila"
'  End With
  
  Set lhFila_Total_Ingreso = New hRecord
  With lhFila_Total_Ingreso
    .AddField "id_moneda"
    .AddField "Fila"
  End With
  
  Set lhFila_Total_Egreso = New hRecord
  With lhFila_Total_Egreso
    .AddField "id_moneda"
    .AddField "Fila"
  End With
  
  Set lhFila_Saldo = New hRecord
  With lhFila_Saldo
    .AddField "id_moneda"
    .AddField "Fila"
  End With
  
  Set lcExcel = New Excel.Application
  Set lcLibro = lcExcel.Workbooks.Add
  
  lcExcel.ActiveWindow.DisplayGridlines = False
  
  For lNro_Libro = lcLibro.Worksheets.Count To lcLibro.Worksheets.Count Step -1
    lcLibro.Worksheets(lNro_Libro).Delete
  Next lNro_Libro
  
  For lNro_Libro = 2 To 1 Step -1
    lcLibro.Sheets(lNro_Libro).Select
    lcLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresa).ShapeRange.IncrementLeft 39.75
    lcLibro.Worksheets.Item(lNro_Libro).Range("A6").Value = "Banca Patrimonial de Inversiones"
    lcLibro.Worksheets.Item(lNro_Libro).Range("A6").Font.Bold = True
    lcLibro.Worksheets.Item(lNro_Libro).Columns("A:A").ColumnWidth = 25
  Next lNro_Libro
  
  lcLibro.Worksheets.Item(cHoja_Detalle).Name = "Detalle"
  lcLibro.Worksheets.Item(cHoja_Resumen).Name = "Resumen"
  
  Set lcHoja = lcLibro.Sheets(cHoja_Detalle)
  Set lcHoja_Resumen = lcLibro.Sheets(cHoja_Resumen)
  Call lcHoja.Select
  
  'lcExcel.Visible = True

  Set lcMov_Caja = New Class_Mov_Caja
  With lcMov_Caja
    If Not .Buscar_Flujos_Resumen(pId_Cuenta:=pId_Cuenta _
                                , pFecha_Inicio:=pFecha_Desde _
                                , pFecha_Termino:=pFecha_Hasta) Then
      Call Fnt_MsgError(eLS_LogVarios _
                      , "Problemas al buscar los registros." _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
    
    Set lCursor_MC = .Cursor
  End With
  Set lcMov_Caja = Nothing
  
  If lCursor_MC.Count > 0 Then
    BarraProceso.Max = lCursor_MC.Count
  End If
  
  lLinea_Fechas = cLineaTitulo + 4
  lLinea_Saldos = cLineaTitulo + 6
  lColumnaFecha = cColumnaFecha
  
  With lcHoja_Resumen
    .Cells(cLineaTitulo, lColumnaFecha).Value = "Resumen Flujos de Caja"
    .Cells(cLineaTitulo, lColumnaFecha).Font.Bold = True
    
    .Cells(cLineaTitulo + 1, lColumnaFecha).Value = "Cuenta " & GetCell(Grilla_Cuentas, pFila, "num_cuenta") _
                                                    & " " & GetCell(Grilla_Cuentas, pFila, "abr_cuenta") _
                                                    & " " & GetCell(Grilla_Cuentas, pFila, "dsc_cuenta")
    '.Cells(cLineaTitulo, lColumnaFecha + 1).Font.Bold = True
    If lCursor_MC.Count = 0 Then
      .Cells(12, 1).Value = "Sin Datos"
      .Cells(12, 1).Font.Bold = True
    End If
  End With
  
  With lcHoja
    .Cells(cLineaTitulo, lColumnaFecha).Value = "Resumen Flujos de Caja"
    .Cells(cLineaTitulo, lColumnaFecha).Font.Bold = True
    
    .Cells(cLineaTitulo + 1, lColumnaFecha).Value = "Cuenta " & GetCell(Grilla_Cuentas, pFila, "num_cuenta") _
                                                    & " " & GetCell(Grilla_Cuentas, pFila, "abr_cuenta") _
                                                    & " " & GetCell(Grilla_Cuentas, pFila, "dsc_cuenta")
    '.Cells(cLineaTitulo, lColumnaFecha + 1).Font.Bold = True
    
    If lCursor_MC.Count = 0 Then
      .Cells(12, 1).Value = "Sin Datos"
      .Cells(12, 1).Font.Bold = True
    End If
      
    BarraProceso.Value = 0
    For Each lReg_MC In lCursor_MC
      BarraProceso.Value = lReg_MC.Index
      Call Sub_Interactivo(gRelogDB)
      
      If Not lId_Moneda = lReg_MC("id_moneda").Value Then
        
        If Not IsEmpty(lId_Moneda) Then
          lLinea_Fechas = lLinea_Fechas + lhDescripcion.Count + 12
          lLinea_Saldos = lLinea_Saldos + lhDescripcion.Count + 12
          
          lhDescripcion.LimpiarRegistros
          lhFechas.LimpiarRegistros
          lColumnaFecha = cColumnaFecha
          lFilas_Ingreso = 0
          lFilas_Egreso = 0
        End If
        
        lId_Moneda = lReg_MC("id_moneda").Value
        lFormato_Moneda = Fnt_Formato_Moneda(lId_Moneda)
        
        Set lfFila_Moneda = lhFila_Moneda.Add
        lfFila_Moneda("Fila").Value = lLinea_Fechas
        .Cells(lLinea_Fechas, cColumnaFecha).Value = "Moneda " & lReg_MC("DSC_MONEDA").Value
        .Cells(lLinea_Fechas, cColumnaFecha).Font.Bold = True
        
'        Set lfFila_Ingreso = lhFila_Ingreso.Add
'        lfFila_Ingreso("id_moneda").Value = lId_Moneda
'        lfFila_Ingreso("Fila").Value = lLinea_Fechas + 1
        .Cells(lLinea_Fechas + 1, cColumnaFecha).Value = "Ingresos"
        .Cells(lLinea_Fechas + 1, cColumnaFecha).Font.Bold = True
        
        .Cells(lLinea_Saldos, lColumnaFecha).Value = "Saldo Anterior"
        
        Set lfFila_Total_Ingreso = lhFila_Total_Ingreso.Add
        lfFila_Total_Ingreso("id_moneda").Value = lId_Moneda
        lfFila_Total_Ingreso("Fila").Value = lLinea_Saldos + 1
        .Cells(lLinea_Saldos + 1, cColumnaFecha).Value = "Total Ingresos"
        .Cells(lLinea_Saldos + 1, cColumnaFecha).Font.Bold = True
        
'        Set lfFila_Egreso = lhFila_Egreso.Add
'        lfFila_Egreso("id_moneda").Value = lId_Moneda
'        lfFila_Egreso("Fila").Value = lLinea_Saldos + 3
        .Cells(lLinea_Saldos + 3, cColumnaFecha).Value = "Egresos"
        .Cells(lLinea_Saldos + 3, cColumnaFecha).Font.Bold = True
        
        Set lfFila_Total_Egreso = lhFila_Total_Egreso.Add
        lfFila_Total_Egreso("id_moneda").Value = lId_Moneda
        lfFila_Total_Egreso("Fila").Value = lLinea_Saldos + 4
        .Cells(lLinea_Saldos + 4, cColumnaFecha).Value = "Total Egresos"
        .Cells(lLinea_Saldos + 4, cColumnaFecha).Font.Bold = True
        
        Set lfFila_Saldo = lhFila_Saldo.Add
        lfFila_Saldo("id_moneda").Value = lId_Moneda
        lfFila_Saldo("Fila").Value = lLinea_Saldos + 6
        .Cells(lLinea_Saldos + 6, cColumnaFecha).Value = "Saldo Diario"
        .Cells(lLinea_Saldos + 6, cColumnaFecha).Font.Bold = True
      End If
    
      Set lfFechas = lhFechas.Buscar("Fecha", lReg_MC("fecha_liquidacion").Value)
      If lfFechas Is Nothing Then
        lColumnaFecha = lColumnaFecha + 1
        Set lfFechas = lhFechas.Add
        lfFechas("fecha").Value = lReg_MC("fecha_liquidacion").Value
        lfFechas("columna").Value = lColumnaFecha
        
        '.Cells(lLinea_Fechas, lColumnaFecha).Value = "'" & Format(lReg_MC("fecha_liquidacion").Value, cFormatDate)
        .Cells(lLinea_Fechas, lColumnaFecha).NumberFormat = cFormatDate
        .Cells(lLinea_Fechas, lColumnaFecha).Value = lReg_MC("fecha_liquidacion").Value
        
        
        .Cells(lLinea_Saldos, lColumnaFecha).NumberFormat = lFormato_Moneda
        .Cells(lLinea_Saldos, lColumnaFecha).Value = Fnt_Busca_Saldo_Anterior(pId_Cuenta, _
                                                                              lId_Moneda, _
                                                                              lReg_MC("fecha_cierre").Value)
        
        lFilas_con_Egreso = 0
        lFilas_con_Ingreso = 0
        For Each lReg In lhDescripcion
          If lReg("tipo_mov").Value = "Egreso" Then
            lFilas_con_Egreso = lFilas_con_Egreso + 1
          ElseIf lReg("tipo_mov").Value = "Ingreso" Then
            lFilas_con_Ingreso = lFilas_con_Ingreso + 1
          End If
        Next
          
        .Cells(lLinea_Saldos + lFilas_con_Ingreso + 1, lColumnaFecha).NumberFormat = lFormato_Moneda
        .Cells(lLinea_Saldos + lFilas_con_Ingreso + 1, lColumnaFecha).FormulaR1C1 = "0"
        
        lLinea_Egreso = lLinea_Saldos + lFilas_con_Ingreso + 3
        .Cells(lLinea_Egreso + lFilas_con_Egreso + 1, lColumnaFecha).NumberFormat = lFormato_Moneda
        .Cells(lLinea_Egreso + lFilas_con_Egreso + 1, lColumnaFecha).FormulaR1C1 = "0"
      End If
      
      lDsc_Mov = ""
      If lReg_MC("flg_tipo_movimiento").Value = gcTipoOperacion_Abono Then
        Select Case NVL(Trim(lReg_MC("cod_instrumento").Value), "")
          Case gcINST_ACCIONES_NAC
            lDsc_Mov = "Venta Acciones"
          Case gcINST_FFMM_RF_NAC, gcINST_FFMM_RV_NAC, gcINST_FFMM_FIP_NAC, gcINST_FFMM_CA_NAC, gcINST_FFMM_FI_NAC  'JGR 190509
            lDsc_Mov = "Rescate Fondos Mutuos"
          Case gcINST_BONOS_NAC
            lDsc_Mov = "Venta Bonos"
          Case gcINST_PACTOS_NAC
            lDsc_Mov = "Venta Pactos"
          Case gcINST_DEPOSITOS_NAC, gcINST_DEPOSITOS_DAP 'jgr 080509
            lDsc_Mov = "Venta Dep�sitos"
          Case ""
            Select Case Trim(lReg_MC("cod_origen_mov_caja").Value)
              Case gcOrigen_Mov_Caja_CargoAbono
                lDsc_Mov = "Abono"
              Case gcOrigen_Mov_Caja_APO_RES
                lDsc_Mov = "Aporte"
              Case gcOrigen_Mov_Caja_DIVIDENDO
                lDsc_Mov = "Dividendo"
              Case gcOrigen_Mov_Caja_Vencimientos
                lDsc_Mov = "Vencimiento IIF"
              Case gcOrigen_Mov_Caja_CorteCupon
                lDsc_Mov = "Corte de Cup�n"
              Case gcOrigen_Mov_Caja_REMESA
                lDsc_Mov = "Ingreso Remesa"
            End Select
        End Select
        
        Set lfDescripcion = lhDescripcion.Buscar("Des_Mov", lDsc_Mov)
        If lfDescripcion Is Nothing Then
        
          lFilas_Ingreso = lFilas_Ingreso + 1
          Set lfDescripcion = lhDescripcion.Add
          lfDescripcion("Des_Mov").Value = lDsc_Mov
          lfDescripcion("Fila").Value = lLinea_Saldos + lFilas_Ingreso
          lfDescripcion("Tipo_Mov").Value = "Ingreso"
          
          lTotal_Filas_Correr = lLinea_Saldos + lhDescripcion.Count + 5
          
          lFilas_con_Ingreso = 0
          For Each lReg In lhDescripcion
            If lReg("tipo_mov").Value = "Ingreso" Then
              lFilas_con_Ingreso = lFilas_con_Ingreso + 1
            End If
          Next
          
          For lLine = lTotal_Filas_Correr To lLinea_Saldos + lFilas_con_Ingreso Step -1
            Call .Range(.Cells(lLine, 1), .Cells(lLine, lColumnaFecha)).Copy _
                  (Destination:=.Cells(lLine + 1, 1))
            For lCol = 2 To lColumnaFecha
              .Cells(lLine, lCol).Value = ""
            Next
            
            Select Case .Cells(lLine, 1)
'              Case "Ingresos"
'                Set lfFila_Ingreso = lhFila_Ingreso.Buscar("id_moneda", lId_Moneda)
'                If lfFila_Ingreso Is Nothing Then
'                  Set lfFila_Ingreso = lhFila_Ingreso.Add
'                End If
'                lfFila_Ingreso("Fila").Value = lLine + 1
'              Case "Egresos"
'                Set lfFila_Egreso = lhFila_Egreso.Buscar("id_moneda", lId_Moneda)
'                If lfFila_Egreso Is Nothing Then
'                  Set lfFila_Egreso = lhFila_Egreso.Add
'                End If
'                lfFila_Egreso("Fila").Value = lLine + 1
              Case "Total Ingresos"
                Set lfFila_Total_Ingreso = lhFila_Total_Ingreso.Buscar("id_moneda", lId_Moneda)
                If lfFila_Total_Ingreso Is Nothing Then
                  Set lfFila_Total_Ingreso = lhFila_Total_Ingreso.Add
                End If
                lfFila_Total_Ingreso("Fila").Value = lLine + 1
              Case "Total Egresos"
                Set lfFila_Total_Egreso = lhFila_Total_Egreso.Buscar("id_moneda", lId_Moneda)
                If lfFila_Total_Egreso Is Nothing Then
                  Set lfFila_Total_Egreso = lhFila_Total_Egreso.Add
                End If
                lfFila_Total_Egreso("Fila").Value = lLine + 1
              Case "Saldo Diario"
                Set lfFila_Saldo = lhFila_Saldo.Buscar("id_moneda", lId_Moneda)
                If lfFila_Saldo Is Nothing Then
                  Set lfFila_Saldo = lhFila_Saldo.Add
                End If
                lfFila_Saldo("Fila").Value = lLine + 1
             End Select
          Next
          
          .Cells(lLinea_Saldos + lFilas_Ingreso, cColumnaFecha).Value = lDsc_Mov
          .Cells(lLinea_Saldos + lFilas_Ingreso, cColumnaFecha).Font.Bold = False
          
          .Cells(lLinea_Saldos + lFilas_Ingreso, lColumnaFecha).NumberFormat = lFormato_Moneda
          .Cells(lLinea_Saldos + lFilas_Ingreso, lColumnaFecha).Value = lReg_MC("Monto").Value
          
          For Each lReg In lhDescripcion
            If lReg("tipo_mov").Value = "Egreso" Then
              lReg("fila").Value = lReg("fila").Value + 1
            End If
          Next
        Else
          .Cells(lfDescripcion("fila").Value, lColumnaFecha).NumberFormat = lFormato_Moneda
          .Cells(lfDescripcion("fila").Value, lColumnaFecha).Value = .Cells(lfDescripcion("fila").Value, lColumnaFecha).Value + lReg_MC("Monto").Value
        End If
        
        For lCol = 2 To cColumnaFecha + lhFechas.Count
          .Cells(lLinea_Saldos + lFilas_Ingreso + 1, lCol).NumberFormat = lFormato_Moneda
          .Cells(lLinea_Saldos + lFilas_Ingreso + 1, lCol).FormulaR1C1 = _
                                                                "=SUM(R[-" & CStr(lFilas_Ingreso + 1) & "]C:R[-1]C)"
          
          .Cells(lLinea_Saldos + lFilas_Ingreso + lFilas_Egreso + 6, lCol).NumberFormat = lFormato_Moneda
          .Cells(lLinea_Saldos + lFilas_Ingreso + lFilas_Egreso + 6, lCol).FormulaR1C1 = _
                                                                "=R[-" & CStr(lFilas_Egreso + 5) & "]C-R[-2]C"
        Next
        
      ElseIf lReg_MC("flg_tipo_movimiento").Value = gcTipoOperacion_Cargo Then
        Select Case NVL(Trim(lReg_MC("cod_instrumento").Value), "")
          Case gcINST_ACCIONES_NAC
            lDsc_Mov = "Compra Acciones"
          Case gcINST_FFMM_RF_NAC, gcINST_FFMM_RV_NAC, gcINST_FFMM_FIP_NAC, gcINST_FFMM_CA_NAC, gcINST_FFMM_FI_NAC 'JGR 190509
            lDsc_Mov = "Inversi�n Fondos Mutuos"
          Case gcINST_BONOS_NAC
            lDsc_Mov = "Compra Bonos"
          Case gcINST_PACTOS_NAC
            lDsc_Mov = "Compra Pactos"
          Case gcINST_DEPOSITOS_NAC, gcINST_DEPOSITOS_DAP 'jgr 080509
            lDsc_Mov = "Compra Dep�sitos"
          Case ""
            Select Case Trim(lReg_MC("cod_origen_mov_caja").Value)
              Case gcOrigen_Mov_Caja_CargoAbono
                lDsc_Mov = "Cargo"
              Case gcOrigen_Mov_Caja_APO_RES
                lDsc_Mov = "Retiro"
              Case gcOrigen_Mov_Caja_REMESA
                lDsc_Mov = "Egreso Remesa"
            End Select
        End Select
        
        Set lfDescripcion = lhDescripcion.Buscar("Des_Mov", lDsc_Mov)
        If lfDescripcion Is Nothing Then
        
          lFilas_Egreso = lFilas_Egreso + 1
          Set lfDescripcion = lhDescripcion.Add
          lfDescripcion("Des_Mov").Value = lDsc_Mov
          lfDescripcion("Fila").Value = lLinea_Saldos + lFilas_Ingreso + lFilas_Egreso + 3
          lfDescripcion("Tipo_Mov").Value = "Egreso"
          
          lTotal_Filas_Correr = lLinea_Saldos + lhDescripcion.Count + 5
          
          lFilas_con_Egreso = 0
          lFilas_con_Ingreso = 0
          For Each lReg In lhDescripcion
            If lReg("tipo_mov").Value = "Egreso" Then
              lFilas_con_Egreso = lFilas_con_Egreso + 1
            ElseIf lReg("tipo_mov").Value = "Ingreso" Then
              lFilas_con_Ingreso = lFilas_con_Ingreso + 1
            End If
          Next
          
          lLinea_Egreso = lLinea_Saldos + lFilas_con_Ingreso + 3
          
          For lLine = lTotal_Filas_Correr To lLinea_Egreso + lFilas_con_Egreso Step -1
            Call .Range(.Cells(lLine, 1), .Cells(lLine, lColumnaFecha)).Copy _
                  (Destination:=.Cells(lLine + 1, 1))
            For lCol = 2 To lColumnaFecha
              .Cells(lLine, lCol).Value = ""
            Next
            
            Select Case .Cells(lLine, 1)
'              Case "Ingresos"
'                Set lfFila_Ingreso = lhFila_Ingreso.Buscar("id_moneda", lId_Moneda)
'                If lfFila_Ingreso Is Nothing Then
'                  Set lfFila_Ingreso = lhFila_Ingreso.Add
'                End If
'                lfFila_Ingreso("Fila").Value = lLine + 1
'              Case "Egresos"
'                Set lfFila_Egreso = lhFila_Egreso.Buscar("id_moneda", lId_Moneda)
'                If lfFila_Egreso Is Nothing Then
'                  Set lfFila_Egreso = lhFila_Egreso.Add
'                End If
'                lfFila_Egreso("Fila").Value = lLine + 1
              Case "Total Ingresos"
                Set lfFila_Total_Ingreso = lhFila_Total_Ingreso.Buscar("id_moneda", lId_Moneda)
                If lfFila_Total_Ingreso Is Nothing Then
                  Set lfFila_Total_Ingreso = lhFila_Total_Ingreso.Add
                End If
                lfFila_Total_Ingreso("Fila").Value = lLine + 1
              Case "Total Egresos"
                Set lfFila_Total_Egreso = lhFila_Total_Egreso.Buscar("id_moneda", lId_Moneda)
                If lfFila_Total_Egreso Is Nothing Then
                  Set lfFila_Total_Egreso = lhFila_Total_Egreso.Add
                End If
                lfFila_Total_Egreso("Fila").Value = lLine + 1
              Case "Saldo Diario"
                Set lfFila_Saldo = lhFila_Saldo.Buscar("id_moneda", lId_Moneda)
                If lfFila_Saldo Is Nothing Then
                  Set lfFila_Saldo = lhFila_Saldo.Add
                End If
                lfFila_Saldo("Fila").Value = lLine + 1
             End Select
          Next
          
          .Cells(lLinea_Egreso + lFilas_Egreso, cColumnaFecha).Value = lDsc_Mov
          .Cells(lLinea_Egreso + lFilas_Egreso, cColumnaFecha).Font.Bold = False
          
          .Cells(lLinea_Egreso + lFilas_Egreso, lColumnaFecha).NumberFormat = lFormato_Moneda
          .Cells(lLinea_Egreso + lFilas_Egreso, lColumnaFecha).Value = lReg_MC("Monto").Value
                    
        Else
          .Cells(lfDescripcion("fila").Value, lColumnaFecha).NumberFormat = lFormato_Moneda
          .Cells(lfDescripcion("fila").Value, lColumnaFecha).Value = .Cells(lfDescripcion("fila").Value, lColumnaFecha).Value + lReg_MC("Monto").Value
        End If
        
        For lCol = 2 To cColumnaFecha + lhFechas.Count
          .Cells(lLinea_Egreso + lFilas_Egreso + 1, lCol).NumberFormat = lFormato_Moneda
          
          lFormula_Total_Egresos = "=SUM(R[-" & CStr(lFilas_Egreso) & "]C:R[-1]C)"
          .Cells(lLinea_Egreso + lFilas_Egreso + 1, lCol).FormulaR1C1 = lFormula_Total_Egresos

          .Cells(lLinea_Saldos + lFilas_Ingreso + lFilas_Egreso + 6, lCol).NumberFormat = lFormato_Moneda
          .Cells(lLinea_Saldos + lFilas_Ingreso + lFilas_Egreso + 6, lCol).FormulaR1C1 = _
                                                                  "=R[-" & CStr(lFilas_Egreso + 5) & "]C-R[-2]C"
        Next
        
      End If
      
    Next
  End With
  
  '*******************************************************************************
  lAcum = 0
  For Each lfFila_Moneda In lhFila_Moneda
    If lAcum = 0 Then
      lFila_Moneda = lfFila_Moneda("Fila").Value
      Call Sub_Colores(lcExcel, lcLibro, cHoja_Detalle, "A" & CStr(lfFila_Moneda("Fila").Value), 36)
      
      Call lcHoja.Range(lcHoja.Cells(lfFila_Moneda("Fila").Value, 1), lcExcel.Selection.End(xlToRight)).Copy
      Call lcHoja_Resumen.Cells(lfFila_Moneda("Fila").Value, 1).PasteSpecial(Paste:=xlPasteAll)
    Else
      Call Sub_Colores(lcExcel, lcLibro, cHoja_Detalle, "A" & CStr(lfFila_Moneda("Fila").Value), 36)
    
      Call lcHoja.Range(lcHoja.Cells(lfFila_Moneda("Fila").Value, 1), lcExcel.Selection.End(xlToRight)).Copy
      Call lcHoja_Resumen.Cells(lFila_Moneda + lAcum, 1).PasteSpecial(Paste:=xlPasteAll)
    End If
    lAcum = lAcum + 6
  Next

  lFila_Moneda = lFila_Moneda + 1
  lAcum = 0
  For Each lfFila_Total_Ingreso In lhFila_Total_Ingreso
    Call Sub_Colores(lcExcel, lcLibro, cHoja_Detalle, "A" & CStr(lfFila_Total_Ingreso("Fila").Value), 36)
    
    Call lcHoja.Range(lcHoja.Cells(lfFila_Total_Ingreso("Fila").Value, 1), lcExcel.Selection.End(xlToRight)).Copy
    Call lcHoja_Resumen.Cells(lFila_Moneda + lAcum, 1).PasteSpecial(Paste:=xlPasteAll)
    'Call lcHoja_Resumen.Cells(lFila_Moneda + lAcum, 1).PasteSpecial(Paste:=xlPasteValuesAndNumberFormats)
    lAcum = lAcum + 6
  Next
  
  lFila_Moneda = lFila_Moneda + 1
  lAcum = 0
  For Each lfFila_Total_Egreso In lhFila_Total_Egreso
    Call Sub_Colores(lcExcel, lcLibro, cHoja_Detalle, "A" & CStr(lfFila_Total_Egreso("Fila").Value), 36)
    
    Call lcHoja.Range(lcHoja.Cells(lfFila_Total_Egreso("Fila").Value, 1), lcExcel.Selection.End(xlToRight)).Copy
    Call lcHoja_Resumen.Cells(lFila_Moneda + lAcum, 1).PasteSpecial(Paste:=xlPasteAll)
    'Call lcHoja_Resumen.Cells(lFila_Moneda + lAcum, 1).PasteSpecial(Paste:=xlPasteValuesAndNumberFormats)
    lAcum = lAcum + 6
  Next
  
  lFila_Moneda = lFila_Moneda + 1
  lAcum = 0
  For Each lfFila_Saldo In lhFila_Saldo
    Call Sub_Colores(lcExcel, lcLibro, cHoja_Detalle, "A" & CStr(lfFila_Saldo("Fila").Value), 35)
    
    Call lcHoja.Range(lcHoja.Cells(lfFila_Saldo("Fila").Value, 1), lcExcel.Selection.End(xlToRight)).Copy
    Call lcHoja_Resumen.Cells(lFila_Moneda + lAcum, 1).PasteSpecial(Paste:=xlPasteAll)
    'Call lcHoja_Resumen.Cells(lFila_Moneda + lAcum, 1).PasteSpecial(Paste:=xlPasteValuesAndNumberFormats)
    lAcum = lAcum + 6
  Next
  
'  For Each lfFila_Ingreso In lhFila_Ingreso
'    Call Sub_Colores(lcExcel, lcLibro, cHoja_Detalle, "A" & CStr(lfFila_Ingreso("Fila").Value), 35)
'  Next
'  For Each lfFila_Egreso In lhFila_Egreso
'    Call Sub_Colores(lcExcel, lcLibro, cHoja_Detalle, "A" & CStr(lfFila_Egreso("Fila").Value), 35)
'  Next
  '*******************************************************************************
  lcLibro.ActiveSheet.Columns("B:B").Select
  lcExcel.Range(lcExcel.Selection, lcExcel.Selection.End(xlToRight)).Select
  lcExcel.Selection.EntireColumn.AutoFit
  '*******************************************************************************
  lcLibro.Sheets(cHoja_Resumen).Select
  'lcLibro.Sheets(cHoja_Resumen).Activate
  lcExcel.ActiveWindow.DisplayGridlines = False
  'lcExcel.Selection.EntireColumn.AutoFit
  lcLibro.ActiveSheet.Range("A1").Select
  lcLibro.Sheets(cHoja_Detalle).Select
  lcLibro.ActiveSheet.Range("A1").Select
  
  lcExcel.Visible = True
  lcExcel.UserControl = True
  
  Fnt_Genera_Excel = True

ExitProcedure:
  Set lcMov_Caja = Nothing
  
  lcExcel.DisplayAlerts = False
End Function

Private Sub Sub_Colores(pExcel As Excel.Application, _
                        pLibro As Excel.Workbook, _
                        pHoja As Long, _
                        pRango As String, _
                        pColor As Long)
  
  With pLibro
    .Sheets(pHoja).Select
    .Sheets(pHoja).Select
    .Sheets(pHoja).Activate
    
    .ActiveSheet.Range(pRango).Select
    .ActiveSheet.Range(pRango).Activate
  End With
  
  With pExcel
    .Range(.Selection, .Selection.End(xlToRight)).Select
    'lcLibro.ActiveSheet.Range(lcExcel.Selection, lcExcel.Selection.End(xlToRight)).Font.Bold = True
    
    .Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    .Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    .Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeRight).Weight = xlMedium
        
    .Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    .Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    .Selection.Borders(xlInsideVertical).Weight = xlThin
    
    .Selection.Interior.ColorIndex = pColor
    .Selection.Interior.Pattern = xlSolid
  End With
  
End Sub

Private Function Fnt_Busca_Saldo_Anterior(pId_Cuenta, pId_Moneda, pFecha_Desde) As Double
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lcSaldos_Caja As Class_Saldos_Caja
Dim lSaldo_Anterior As Double
Dim lId_Caja_Cuenta As String
  
  lSaldo_Anterior = 0
  
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("id_moneda").Valor = pId_Moneda
    If Not .Buscar Then
      Call Fnt_MsgError(eLS_LogVarios _
                      , "Problemas al buscar los registros." _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ErrProcedure
    End If
    lId_Caja_Cuenta = .Cursor(1)("id_caja_cuenta").Value
  End With
  Set lcCaja_Cuenta = Nothing
  
  Set lcSaldos_Caja = New Class_Saldos_Caja
  With lcSaldos_Caja
    .Campo("Id_Caja_Cuenta").Valor = lId_Caja_Cuenta
    .Campo("fecha_cierre").Valor = pFecha_Desde
    If Not .Buscar_Saldos_Cuenta(lSaldo_Anterior, CStr(pId_Cuenta)) Then
      Call Fnt_MsgError(eLS_LogVarios _
                      , "Problemas al buscar los registros." _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  Set lcSaldos_Caja = Nothing
  
ErrProcedure:
  Fnt_Busca_Saldo_Anterior = lSaldo_Anterior
End Function
