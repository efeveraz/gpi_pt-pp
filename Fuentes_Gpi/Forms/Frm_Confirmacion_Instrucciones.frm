VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Confirmacion_Instrucciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Confirmaci�n de Instrucciones"
   ClientHeight    =   6120
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   9660
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6120
   ScaleWidth      =   9660
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1155
      Left            =   60
      TabIndex        =   2
      Top             =   390
      Width           =   9525
      Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
         Height          =   315
         Left            =   1290
         TabIndex        =   6
         Top             =   240
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   20971521
         CurrentDate     =   37732
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
         Height          =   315
         Left            =   1290
         TabIndex        =   7
         Top             =   690
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   20971521
         CurrentDate     =   37732
      End
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   345
         Left            =   4080
         TabIndex        =   12
         Top             =   240
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Confirmacion_Instrucciones.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Asesores 
         Height          =   345
         Left            =   6630
         TabIndex        =   13
         Top             =   240
         Width           =   2565
         _ExtentX        =   4524
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Confirmacion_Instrucciones.frx":00AA
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Instrumento 
         Height          =   345
         Left            =   4080
         TabIndex        =   14
         Top             =   660
         Width           =   5115
         _ExtentX        =   9022
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Confirmacion_Instrucciones.frx":0154
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label lbl_Fecha_desde 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Desde"
         Height          =   345
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   1125
      End
      Begin VB.Label lbl_fecha_hasta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Hasta"
         Height          =   315
         Left            =   120
         TabIndex        =   8
         Top             =   690
         Width           =   1125
      End
      Begin VB.Label lbl_asesores 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Asesor"
         Height          =   330
         Left            =   5820
         TabIndex        =   5
         Top             =   240
         Width           =   795
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuentas"
         Height          =   330
         Left            =   2940
         TabIndex        =   4
         Top             =   240
         Width           =   1125
      End
      Begin VB.Label Label8 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Instrumento"
         Height          =   345
         Left            =   2940
         TabIndex        =   3
         Top             =   660
         Width           =   1125
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Confirmaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4515
      Left            =   60
      TabIndex        =   0
      Top             =   1530
      Width           =   9525
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   4125
         Left            =   90
         TabIndex        =   1
         Top             =   300
         Width           =   9315
         _cx             =   16431
         _cy             =   7276
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   14
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Confirmacion_Instrucciones.frx":01FE
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9660
      _ExtentX        =   17039
      _ExtentY        =   635
      ButtonWidth     =   2593
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   10
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "RESEARCH"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Busca las instrucciones sin confirmar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Caption         =   "&Confirmar"
            Key             =   "CONF"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Confirma las instrucciones en forma automatizada"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Conf. &Manual"
            Key             =   "MANUAL"
            Object.ToolTipText     =   "Confirma las instrucciones en forma manual"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Anular"
            Key             =   "Anular"
            Description     =   "Anula Instrucciones Pendientes"
            Object.ToolTipText     =   "Anula las Instrucciones Pendientes"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   8010
         TabIndex        =   11
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Confirmacion_Instrucciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const fc_Mercado = "N"

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("RESEARCH").Image = "boton_grilla_buscar"
      .Buttons("CONF").Image = cBoton_Grabar
      .Buttons("MANUAL").Image = cBoton_Grabar
      .Buttons("Anular").Image = cBoton_Cancelar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
      .Buttons("EXIT").Image = cBoton_Salir
  End With

  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  Cancel = True
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "RESEARCH"
      Call Sub_CargarDatos
'    Case "CONF"
'      Call Sub_Confirmacion
    Case "MANUAL"
      Call Sub_Confirmacion_Manual
    Case "Anular"
      If MsgBox("Esta seguro(a) que desea Anular la operaci�n.", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
        Call Sub_Anular
      End If
    Case "REFRESH"
      Call Sub_Limpiar
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Limpiar()

  Grilla.Rows = 1
  
  Call Sub_ComboSelectedItem(Cmb_Cuentas, cCmbKALL)
  Call Sub_ComboSelectedItem(Cmb_Asesores, cCmbKALL)
  Call Sub_ComboSelectedItem(Cmb_Instrumento, cCmbKALL)
  
  DTP_Fecha_Desde.Value = Format(Fnt_FechaServidor, cFormatDate)
  DTP_Fecha_Hasta.Value = Format(Fnt_FechaServidor, cFormatDate)
End Sub

Private Sub Sub_CargarDatos()
Dim lId_Cuenta As String
Dim lId_Asesor As String
Dim lCod_Instrumento As String
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lTipo_Movimiento As String
Dim lcOperaciones As Class_Operaciones
Dim lFecha_Desde As String
Dim lFecha_Hasta As String

  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  If DTP_Fecha_Desde.Value > DTP_Fecha_Hasta.Value Then
    MsgBox "La Fecha Desde no puede ser mayor que la Fecha Hasta", vbExclamation, Me.Caption
    Exit Sub
  End If
  
  Call Sub_Bloquea_Puntero(Me)
  
  Grilla.Rows = 1
  
  lId_Cuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Cuentas))
  lId_Asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesores) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Asesores))
  lCod_Instrumento = IIf(Fnt_ComboSelected_KEY(Cmb_Instrumento) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Instrumento))
  lFecha_Desde = DTP_Fecha_Desde.Value
  lFecha_Hasta = DTP_Fecha_Hasta.Value
  
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("Cod_Instrumento").Valor = lCod_Instrumento
    .Campo("cod_producto").Valor = ""
    .Campo("cod_tipo_operacion").Valor = gcOPERACION_Instruccion
    .Campo("cod_estado").Valor = cCod_Estado_Pendiente
    .Campo("Id_Operacion").Valor = ""
    .Campo("Flg_Tipo_Movimiento").Valor = ""
    If .Fnt_Buscar_Operaciones(lFecha_Desde, lFecha_Hasta, lId_Cuenta, lId_Asesor) Then
      For Each lReg In .Cursor
        Select Case lReg("flg_tipo_movimiento").Value
          Case gcTipoOperacion_Ingreso
            lTipo_Movimiento = "Compra/Ingreso"
          Case gcTipoOperacion_Egreso
            lTipo_Movimiento = "Venta/Egreso"
          Case Else
            lTipo_Movimiento = ""
        End Select
        
        lLinea = Grilla.Rows
        Call Grilla.AddItem("")
        Grilla.Cell(flexcpChecked, lLinea, 0) = flexUnchecked
        Call SetCell(Grilla, lLinea, "colum_pk", lReg("ID_OPERACION").Value)
        Call SetCell(Grilla, lLinea, "id_cuenta", lReg("ID_CUENTA").Value)
        Call SetCell(Grilla, lLinea, "num_cuenta", lReg("NUM_CUENTA").Value)
        Call SetCell(Grilla, lLinea, "cod_instrumento", lReg("COD_INSTRUMENTO").Value)
        Call SetCell(Grilla, lLinea, "dsc_instrumento", NVL(lReg("DSC_INTRUMENTO").Value, ""))
        Call SetCell(Grilla, lLinea, "tipo_movimiento", lTipo_Movimiento)
        Call SetCell(Grilla, lLinea, "monto", NVL(lReg("MONTO_OPERACION").Value, ""))
        Call SetCell(Grilla, lLinea, "id_moneda_operacion", NVL(lReg("ID_MONEDA_OPERACION").Value, ""))
        Call SetCell(Grilla, lLinea, "dsc_moneda_operacion", NVL(lReg("DSC_MONEDA").Value, ""))
        Call SetCell(Grilla, lLinea, "FechaOperacion", lReg("FECHA_OPERACION").Value)
        Call SetCell(Grilla, lLinea, "id_contraparte", NVL(lReg("ID_CONTRAPARTE").Value, ""))
        Call SetCell(Grilla, lLinea, "dsc_contraparte", NVL(lReg("DSC_CONTRAPARTE").Value, ""))
        Call SetCell(Grilla, lLinea, "formulario", NVL(lReg("FORMULARIO").Value, ""))
        
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Operaciones.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  
  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_CargaForm()

  Call Sub_FormControl_Color(Me.Controls)

  Rem Limpia la grilla
  Grilla.Rows = 1
  
  Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, , True)
  Call Sub_CargaCombo_Asesor(Cmb_Asesores, , True)
  Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento, , , True)
  
  Call Sub_Limpiar
  
End Sub

Private Sub Grilla_DblClick()
Dim lKey As String
Dim lCod_Instrumento As String
Dim lFormulario As String
Dim lId_Cuenta As String

  With Grilla
    If .Row > 0 Then
      lKey = GetCell(Grilla, .Row, "colum_pk")
      lCod_Instrumento = GetCell(Grilla, .Row, "cod_instrumento")
      lFormulario = GetCell(Grilla, .Row, "formulario")
      lId_Cuenta = GetCell(Grilla, .Row, "id_cuenta")
      Call Sub_EsperaVentana(lKey, lCod_Instrumento, lFormulario, lId_Cuenta)
    End If
  End With
End Sub

Private Sub Sub_EsperaVentana(pkey As String, _
                              pCod_Instrumento As String, _
                              pFormulario As String, _
                              pId_Cuenta As String)
Dim lClass As Object
Dim lForm As Object
Dim lResult As Boolean
Dim lMonto_Total As Double


  Me.Enabled = False
  
  Set lClass = Nothing
  Select Case UCase(pFormulario)
    Case "CLASS_ACCIONES"
      Set lClass = New Class_Acciones
    Case "CLASS_BONOS"
      Set lClass = New Class_Bonos
    Case "CLASS_FONDOSMUTUOS"
      Set lClass = New Class_FondosMutuos
    Case "CLASS_DEPOSITOS"
      Set lClass = New Class_Depositos
    Case "CLASS_PACTOS"
      Set lClass = New Class_Pactos
'Agregado para modulo internacional
    Case "CLASS_ACCIONES_INT"
      Set lClass = New Class_Acciones_Int
    Case "CLASS_BONOS_INT"
      Set lClass = New Class_Bonos_Int
    Case "CLASS_FONDOSMUTUOS_INT"
      Set lClass = New Class_FondosMutuos_Int
  End Select
  
  If Not lClass Is Nothing Then
    
    Set lForm = Nothing
    Set lForm = lClass.Form_Operacion_Confirmacion
    
    If Not lForm Is Nothing Then
      Call Sub_FormControl_Enabled(Me.Controls, Me, False)
      lResult = lForm.Confirmar(Me, pkey, pId_Cuenta, lMonto_Total, pCod_Instrumento)
      Call Sub_FormControl_Enabled(Me.Controls, Me, True)
      
      If lResult Then
        MsgBox "Confirmaci�n de la Instrucci�n realizada con �xito.", vbInformation
        Call Sub_CargarDatos
      End If
    End If
  End If
  
  Me.Enabled = True
  
End Sub

Private Sub Sub_Confirmacion_Manual()
  Call Grilla_DblClick
End Sub

Private Sub Sub_Anular()
Dim lFila As Long
Dim lRollback As Boolean
Dim Selecciono As Boolean

  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
  
  lRollback = False
  
  gDB.IniciarTransaccion
  With Grilla
    lFila = Grilla.Row
    If .Rows > 1 Then
      If lFila > 0 Then
        If Not Fnt_Anular(GetCell(Grilla, lFila, "colum_pk"), _
                          GetCell(Grilla, lFila, "id_cuenta")) Then
              lRollback = True
              GoTo ErrProcedure
        End If
        Selecciono = True
      Else
        Selecciono = False
      End If
    Else
      GoTo Salir
    End If
  End With
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    MsgBox "Se produjo un Error al Anular operaciones." & vbCr & vbCr & gDB.ErrMsg, vbCritical
  Else
    gDB.CommitTransaccion
    If Selecciono Then
          MsgBox "Las operaciones fueron Anuladas con �xito.", vbInformation
          Call Sub_CargarDatos
    Else
          MsgBox "No se ha seleccionado ninguna operaci�n.", vbInformation + vbOKOnly
    End If
  End If
  
Salir:
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Sub

'Private Sub Sub_Confirmacion()
'Dim lFila As Long
'Dim lRollback As Boolean
'Dim Selecciono As Boolean
'
'  Call Sub_Bloquea_Puntero(Me)
'  Me.Enabled = False
'
'  Selecciono = False
'  lRollback = False
'
'  With Grilla
'    If .Rows > 1 Then
'      gDB.IniciarTransaccion
'      For lFila = 1 To .Rows - 1
'        If Grilla.Cell(flexcpChecked, lFila, 0) = flexChecked Then
'            If Not Fnt_Grabar_Confirmacion(GetCell(Grilla, lFila, "colum_pk"), _
'                                           GetCell(Grilla, lFila, "id_cuenta"), _
'                                           GetCell(Grilla, lFila, "monto"), _
'                                           GetCell(Grilla, lFila, "id_moneda_operacion")) Then
'                  lRollback = True
'                  GoTo ErrProcedure
'            Else
'                  Selecciono = True
'            End If
'        End If
'      Next
'    Else
'      GoTo Salir
'    End If
'  End With
'
'ErrProcedure:
'  If lRollback Then
'    gDB.RollbackTransaccion
'    MsgBox "Error en la confirmaci�n de operaciones." & vbCr & vbCr & gDB.ErrMsg, vbCritical
'  Else
'      gDB.CommitTransaccion
'      If Selecciono Then
'            MsgBox "Las operaciones fueron confirmadas con �xito.", vbInformation
'            Call Sub_CargarDatos
'      Else
'            MsgBox "No se ha seleccionado ninguna operaci�n. Debe seleccionar al menos una para Confirmar.", vbInformation + vbOKOnly
'      End If
'  End If
'
'Salir:
'  Call Sub_Desbloquea_Puntero(Me)
'  Me.Enabled = True
'End Sub
'
'Private Function Fnt_Grabar_Confirmacion(pId_Operacion As String, _
'                                         pId_cuenta As String, _
'                                         pMontoTotal As String, _
'                                         pId_Moneda As String) As Boolean
'Dim lId_Caja_Cuenta As Double
'Dim lRollback As Boolean
'Dim lOperaciones As Class_Operaciones
'Dim lOperacion  As String
'
'  lRollback = False
'
'  Set lOperaciones = New Class_Operaciones
'  lOperaciones.campo("Id_Operacion").Valor = pId_Operacion
'
'  If lOperaciones.BuscaConDetalles Then
'    lOperacion = lOperaciones.campo("Flg_Tipo_Movimiento").Valor
'  End If
'
'  If lOperacion = gcTipoOperacion_Ingreso Then
'    Rem Si es una compra
'    lId_Caja_Cuenta = Fnt_ValirdarFinanciamiento(pId_cuenta, fc_Mercado, pMontoTotal, pId_Moneda)
'    If lId_Caja_Cuenta = -1 Then
'      Rem Si el financiamiento tuvo problemas
'      lRollback = True
'      GoTo ErrProcedure
'    End If
'  Else
'    Rem Si es una venta
'    lId_Caja_Cuenta = Fnt_ValidarCaja(pId_cuenta, fc_Mercado, pId_Moneda)
'    If lId_Caja_Cuenta < 0 Then
'      Rem Significa que hubo problema con la busqueda de la caja
'      lRollback = True
'      GoTo ErrProcedure
'    End If
'  End If
'
'  With lOperaciones
'    If Not .Confirmar(lId_Caja_Cuenta) Then
'      lRollback = True
'      GoTo ErrProcedure
'    End If
'  End With
'
'ErrProcedure:
'  If lRollback Then
'    Fnt_Grabar_Confirmacion = False
'  Else
'    Fnt_Grabar_Confirmacion = True
'  End If
'
'End Function

Private Function Fnt_Anular(pId_Operacion As String, pId_Cuenta As String) As Boolean
Dim lcOperaciones As Class_Operaciones
Dim lRollback As Boolean

  lRollback = False
  
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("id_Operacion").Valor = pId_Operacion
    .Campo("id_Cuenta").Valor = pId_Cuenta
    If Not .Anular Then
      'Si existe un problema el proceso devuelve false
      lRollback = True
    End If
  End With
  
ErrProcedure:
  If lRollback Then
    Fnt_Anular = False
  Else
    Fnt_Anular = True
  End If
  
End Function

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Confirmaci�n de Instrucciones" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub
