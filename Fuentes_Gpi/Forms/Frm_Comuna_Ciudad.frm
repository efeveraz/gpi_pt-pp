VERSION 5.00
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form Frm_Comuna_Ciudad 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Comuna-Ciudad"
   ClientHeight    =   1860
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6015
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1860
   ScaleWidth      =   6015
   Begin VB.Frame Frame1 
      Caption         =   "Comuna-Ciudad"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1155
      Left            =   60
      TabIndex        =   2
      Top             =   390
      Width           =   5895
      Begin hControl2.hTextLabel Txt_ComunaCiudad 
         Height          =   315
         Left            =   120
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   720
         Width           =   5550
         _ExtentX        =   9790
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Comuna/Ciudad"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   50
      End
      Begin hControl2.hTextLabel Txt_Region 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Tag             =   "OBLI"
         Top             =   360
         Width           =   5550
         _ExtentX        =   9790
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Regi�n"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   50
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6015
      _ExtentX        =   10610
      _ExtentY        =   635
      ButtonWidth     =   1931
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Comuna_Ciudad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey                     As String 'Contraparte

Public fId_Comuna_Ciudad        As String
Public fId_Region               As String
Public fDsc_Comuna_Ciudad       As String
Public fDsc_Region              As String
Public fId_Comuna_Ciudad_new    As String
Public fId_Region_new           As String
Public fDsc_Comuna_Ciudad_new   As String
Public fDsc_Region_new          As String

Public fCod_Producto            As String
Public fCod_Instrumento         As String

Public fCod_Producto_new        As String
Public fdsc_producto_new        As String
Public fCod_instrumento_new     As String
Public fDsc_Intrumento_new      As String

Dim fFlg_Tipo_Permiso           As String

Private Sub Form_Load()
    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("SAVE").Image = cBoton_Agregar_Grilla
            .Buttons("EXIT").Image = cBoton_Salir
            .Buttons("REFRESH").Image = cBoton_Original
    End With

    Call Sub_CargaForm

    Me.Top = 1
    Me.Left = 1
End Sub

Public Function Fnt_Modificar(pId_Comuna_Ciudad _
                            , pDsc_Comuna_Ciudad _
                            , pid_region _
                            , pDsc_region _
                            , pId_Comuna_Ciudad_new _
                            , pDsc_Comuna_Ciudad_new _
                            , pCod_Arbol_Sistema)
   
    Dim lName       As String
    
    fKey = pId_Comuna_Ciudad
    fDsc_Comuna_Ciudad = pDsc_Comuna_Ciudad
    fId_Region = pid_region
  
    Me.Top = 1
    Me.Left = 1
  
    fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
    
    Call Form_Resize
    
    Call Sub_CargarDatos
    
    Me.Show
  
    lName = Me.Name
    Do While Fnt_ExisteVentana(lName)
        DoEvents
    Loop
    
    pId_Comuna_Ciudad_new = fKey
    pDsc_Comuna_Ciudad_new = fDsc_Comuna_Ciudad_new

'    pCod_Producto_new = fCod_Producto_new
'    pDsc_Producto_new = fdsc_producto_new
'    pCod_instrumento_new = fCod_instrumento_new
'    pDsc_Intrumento_new = fDsc_Intrumento_new
  
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar() Then
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
    Dim lCod_Instrumento    As String
    Dim lCod_Producto       As String
    
    Dim oComunaCiudad       As Class_Comuna_Ciudad

    Fnt_Grabar = True

    If Not Fnt_ValidarDatos Then
        Fnt_Grabar = False
        Exit Function
    End If
    
    Set oComunaCiudad = New Class_Comuna_Ciudad
    
    With oComunaCiudad
        .Campo("id_comuna_ciudad").Valor = fKey
        .Campo("id_region").Valor = fId_Region
        .Campo("DSC_COMUNA_CIUDAD").Valor = Txt_ComunaCiudad.Text
        
        If .Guardar() Then
            fKey = .Campo("id_comuna_ciudad").Valor
        Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                    "Problemas en cargar datos.", _
                    .ErrMsg, _
                    pConLog:=True)
        End If
        
    End With
    
    Set oComunaCiudad = Nothing
    
End Function

Private Sub Sub_CargaForm()
    Call Sub_FormControl_Color(Me.Controls)
End Sub

Private Sub Sub_CargarDatos()
    Dim lReg    As hCollection.hFields
    Dim oComunasCiudades As Class_Comuna_Ciudad
  
    Load Me
    
    BuscaNombreRegion
  
    Set oComunasCiudades = New Class_Comuna_Ciudad
  
    With oComunasCiudades
        .Campo("ID_COMUNA_CIUDAD").Valor = fKey
        .Campo("ID_REGION").Valor = fId_Region
        
        If .Buscar() Then
            For Each lReg In .Cursor
                Txt_ComunaCiudad.Text = lReg("DSC_COMUNA_CIUDAD").Value
                Txt_Region.Text = lReg("DSC_REGION").Value
            Next
        Else
            Call Fnt_MsgError(.SubTipo_LOG, _
            "Problemas en cargar la comuna/ciudad.", _
            .ErrMsg, _
            pConLog:=True)
        End If
        
    End With

Set oComunasCiudades = Nothing

End Sub
Private Sub BuscaNombreRegion()
    Dim oRegion         As Class_Regiones
    Set oRegion = New Class_Regiones
    
    With oRegion
        ' .Campo("COD_PAIS").Valor = sCodPais
        .Campo("id_region").Valor = fId_Region
        
        If .Buscar Then
            If .Cursor.Count > 0 Then
                Txt_Region.Text = .Cursor(1).Fields("DSC_REGION").Value
            Else
                Txt_Region.Text = ""
            End If
        Else
            Txt_Region.Text = "no tiene"
'            Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Problemas en cargar Regi�n.", _
'                        .ErrMsg, _
'                        pConLog:=True)
        End If
        
    End With
    
    Set oRegion = Nothing
    
End Sub


Private Function Fnt_ValidarDatos() As Boolean
    Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function
