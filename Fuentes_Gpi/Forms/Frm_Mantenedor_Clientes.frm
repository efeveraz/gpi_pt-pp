VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{E2D000D0-2DA1-11D2-B358-00104B59D73D}#1.0#0"; "titext8.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Mantenedor_Clientes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Clientes"
   ClientHeight    =   6885
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   8625
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6885
   ScaleWidth      =   8625
   Begin VB.Frame FraDatos 
      Enabled         =   0   'False
      Height          =   5295
      Left            =   45
      TabIndex        =   5
      Top             =   1560
      Width           =   8535
      Begin C1SizerLibCtl.C1Tab SSTab 
         Height          =   4950
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   8280
         _cx             =   14605
         _cy             =   8731
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         Appearance      =   0
         MousePointer    =   0
         Version         =   801
         BackColor       =   6579300
         ForeColor       =   -2147483634
         FrontTabColor   =   -2147483635
         BackTabColor    =   6579300
         TabOutlineColor =   -2147483632
         FrontTabForeColor=   -2147483634
         Caption         =   $"Frm_Mantenedor_Clientes.frx":0000
         Align           =   0
         CurrTab         =   0
         FirstTab        =   0
         Style           =   6
         Position        =   0
         AutoSwitch      =   -1  'True
         AutoScroll      =   -1  'True
         TabPreview      =   -1  'True
         ShowFocusRect   =   0   'False
         TabsPerPage     =   0
         BorderWidth     =   0
         BoldCurrent     =   -1  'True
         DogEars         =   -1  'True
         MultiRow        =   0   'False
         MultiRowOffset  =   200
         CaptionStyle    =   0
         TabHeight       =   0
         TabCaptionPos   =   4
         TabPicturePos   =   0
         CaptionEmpty    =   ""
         Separators      =   -1  'True
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   37
         Begin VB.PictureBox Picture12 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   12195
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   43
            Top             =   330
            Width           =   8250
            Begin VB.Frame Frame6 
               Height          =   1800
               Left            =   45
               TabIndex        =   104
               Top             =   120
               Width           =   8085
               Begin TrueDBList80.TDBCombo Cmb_InstitucionPrevisional 
                  Height          =   345
                  Left            =   2300
                  TabIndex        =   105
                  Top             =   300
                  Width           =   3500
                  _ExtentX        =   6165
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":00AB
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin MSComCtl2.DTPicker DTP_Fecha_Prevision 
                  Height          =   345
                  Left            =   2300
                  TabIndex        =   106
                  Top             =   800
                  Width           =   2000
                  _ExtentX        =   3519
                  _ExtentY        =   609
                  _Version        =   393216
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  CheckBox        =   -1  'True
                  Format          =   80674817
                  CurrentDate     =   37732
               End
               Begin VB.Label Label15 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Fecha Previsi�n"
                  Height          =   345
                  Left            =   120
                  TabIndex        =   108
                  Top             =   800
                  Width           =   2100
               End
               Begin VB.Label Label17 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Instituci�n Previsional"
                  Height          =   345
                  Left            =   120
                  TabIndex        =   107
                  Top             =   300
                  Width           =   2100
               End
            End
         End
         Begin VB.PictureBox Picture11 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   11895
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   42
            Top             =   330
            Width           =   8250
            Begin VB.Frame Pnl_Empleador_Datos 
               Caption         =   "Identificaci�n del Empleador"
               ForeColor       =   &H000000FF&
               Height          =   1400
               Left            =   45
               TabIndex        =   97
               Top             =   120
               Width           =   8100
               Begin hControl2.hTextLabel Txt_RutEmpleador 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   98
                  Top             =   240
                  Width           =   2820
                  _ExtentX        =   4974
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Rut Empleador"
                  Text            =   ""
                  BackColorTxt    =   12648384
                  BackColorTxt    =   12648384
                  MaxLength       =   19
               End
               Begin hControl2.hTextLabel Txt_RazonSocialEmpleador 
                  Height          =   315
                  Left            =   3000
                  TabIndex        =   99
                  Top             =   240
                  Width           =   5000
                  _ExtentX        =   8811
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Raz�n Social"
                  Text            =   ""
                  BackColorTxt    =   12648384
                  BackColorTxt    =   12648384
                  MaxLength       =   150
               End
               Begin hControl2.hTextLabel Txt_GiroEmpleador 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   100
                  Top             =   600
                  Width           =   3900
                  _ExtentX        =   6879
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Giro"
                  Text            =   ""
                  MaxLength       =   100
               End
               Begin hControl2.hTextLabel Txt_CasillaEmpleador 
                  Height          =   315
                  Left            =   4300
                  TabIndex        =   101
                  Top             =   600
                  Width           =   2820
                  _ExtentX        =   4974
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Casilla"
                  Text            =   ""
                  MaxLength       =   40
               End
               Begin hControl2.hTextLabel Txt_TelefonoEmpleador 
                  Height          =   315
                  Left            =   4300
                  TabIndex        =   102
                  Top             =   950
                  Width           =   3400
                  _ExtentX        =   6006
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Tel�fono"
                  Text            =   ""
                  MaxLength       =   50
               End
               Begin hControl2.hTextLabel Txt_EmailEmpleador 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   103
                  Top             =   950
                  Width           =   4000
                  _ExtentX        =   7064
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "E-Mail "
                  Text            =   ""
                  MaxLength       =   120
               End
            End
            Begin VB.Frame Pnl_Empleador_Direccion 
               Caption         =   "Direcci�n"
               ForeColor       =   &H000000FF&
               Height          =   1500
               Left            =   45
               TabIndex        =   89
               Top             =   1500
               Width           =   8100
               Begin hControl2.hTextLabel Txt_DireccionEmpleador 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   90
                  Top             =   240
                  Width           =   7500
                  _ExtentX        =   13229
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Direcci�n"
                  Text            =   ""
                  MaxLength       =   150
               End
               Begin TrueDBList80.TDBCombo Cmb_PaisEmp 
                  Height          =   345
                  Left            =   1450
                  TabIndex        =   91
                  Tag             =   "CAPTION=Pais"
                  Top             =   650
                  Width           =   2235
                  _ExtentX        =   3942
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":0155
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin TrueDBList80.TDBCombo Cmb_RegionEmp 
                  Height          =   345
                  Left            =   5700
                  TabIndex        =   92
                  Tag             =   "CAPTION=Pais"
                  Top             =   1050
                  Width           =   2235
                  _ExtentX        =   3942
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":01FF
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin TrueDBList80.TDBCombo Cmb_ComunaCiudadEmp 
                  Height          =   345
                  Left            =   1450
                  TabIndex        =   93
                  Tag             =   "CAPTION=Pais"
                  Top             =   1050
                  Width           =   3300
                  _ExtentX        =   5821
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":02A9
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin VB.Label Label14 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Pais"
                  Height          =   345
                  Left            =   120
                  TabIndex        =   96
                  Top             =   650
                  Width           =   1300
               End
               Begin VB.Label Label16 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Regi�n"
                  Height          =   345
                  Left            =   4900
                  TabIndex        =   95
                  Top             =   1050
                  Width           =   800
               End
               Begin VB.Label Label18 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Comuna/Ciudad"
                  Height          =   345
                  Left            =   120
                  TabIndex        =   94
                  Top             =   1050
                  Width           =   1300
               End
            End
            Begin VB.Frame Frame4 
               Caption         =   "Contacto"
               ForeColor       =   &H000000FF&
               Height          =   1200
               Left            =   45
               TabIndex        =   85
               Top             =   2985
               Width           =   8050
               Begin hControl2.hTextLabel Txt_NombreContactoEmp 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   86
                  Top             =   240
                  Width           =   5000
                  _ExtentX        =   8811
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Nombre"
                  Text            =   ""
                  MaxLength       =   100
               End
               Begin hControl2.hTextLabel Txt_EmailContactoEmp 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   87
                  Top             =   650
                  Width           =   4000
                  _ExtentX        =   7064
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "E-Mail "
                  Text            =   ""
                  MaxLength       =   120
               End
               Begin hControl2.hTextLabel Txt_FonoContactoEmp 
                  Height          =   315
                  Left            =   4300
                  TabIndex        =   88
                  Top             =   645
                  Width           =   3400
                  _ExtentX        =   6006
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Tel�fono"
                  Text            =   ""
                  MaxLength       =   50
               End
            End
         End
         Begin VB.PictureBox Picture10 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   11595
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   41
            Top             =   330
            Width           =   8250
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Formas_Operacion 
               Height          =   4335
               Left            =   45
               TabIndex        =   84
               Top             =   120
               Width           =   8145
               _cx             =   14367
               _cy             =   7646
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   3
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Mantenedor_Clientes.frx":0353
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   2
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
         Begin VB.PictureBox Picture9 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   11295
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   40
            Top             =   330
            Width           =   8250
            Begin VB.Frame Pnl_Detalle_Antecedente 
               Caption         =   "Detalle de Antecedentes"
               ForeColor       =   &H000000FF&
               Height          =   2200
               Left            =   0
               TabIndex        =   82
               Top             =   120
               Width           =   8175
               Begin VSFlex8LCtl.VSFlexGrid Grilla_Checklist 
                  Height          =   1900
                  Left            =   45
                  TabIndex        =   83
                  Top             =   250
                  Width           =   8000
                  _cx             =   14111
                  _cy             =   3351
                  Appearance      =   2
                  BorderStyle     =   1
                  Enabled         =   -1  'True
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  MousePointer    =   0
                  BackColor       =   -2147483643
                  ForeColor       =   -2147483640
                  BackColorFixed  =   -2147483633
                  ForeColorFixed  =   -2147483630
                  BackColorSel    =   65535
                  ForeColorSel    =   0
                  BackColorBkg    =   -2147483643
                  BackColorAlternate=   -2147483643
                  GridColor       =   -2147483633
                  GridColorFixed  =   -2147483632
                  TreeColor       =   -2147483632
                  FloodColor      =   192
                  SheetBorder     =   -2147483642
                  FocusRect       =   2
                  HighLight       =   1
                  AllowSelection  =   -1  'True
                  AllowBigSelection=   -1  'True
                  AllowUserResizing=   1
                  SelectionMode   =   3
                  GridLines       =   10
                  GridLinesFixed  =   2
                  GridLineWidth   =   1
                  Rows            =   2
                  Cols            =   7
                  FixedRows       =   1
                  FixedCols       =   0
                  RowHeightMin    =   0
                  RowHeightMax    =   0
                  ColWidthMin     =   0
                  ColWidthMax     =   0
                  ExtendLastCol   =   -1  'True
                  FormatString    =   $"Frm_Mantenedor_Clientes.frx":03EE
                  ScrollTrack     =   -1  'True
                  ScrollBars      =   3
                  ScrollTips      =   -1  'True
                  MergeCells      =   0
                  MergeCompare    =   0
                  AutoResize      =   -1  'True
                  AutoSizeMode    =   0
                  AutoSearch      =   2
                  AutoSearchDelay =   2
                  MultiTotals     =   -1  'True
                  SubtotalPosition=   1
                  OutlineBar      =   0
                  OutlineCol      =   0
                  Ellipsis        =   1
                  ExplorerBar     =   3
                  PicturesOver    =   0   'False
                  FillStyle       =   0
                  RightToLeft     =   0   'False
                  PictureType     =   0
                  TabBehavior     =   0
                  OwnerDraw       =   0
                  Editable        =   2
                  ShowComboButton =   1
                  WordWrap        =   0   'False
                  TextStyle       =   0
                  TextStyleFixed  =   0
                  OleDragMode     =   0
                  OleDropMode     =   0
                  ComboSearch     =   3
                  AutoSizeMouse   =   -1  'True
                  FrozenRows      =   0
                  FrozenCols      =   0
                  AllowUserFreezing=   0
                  BackColorFrozen =   0
                  ForeColorFrozen =   0
                  WallPaperAlignment=   9
                  AccessibleName  =   ""
                  AccessibleDescription=   ""
                  AccessibleValue =   ""
                  AccessibleRole  =   24
               End
            End
            Begin VB.Frame Pnl_Aporte_Inicial 
               Caption         =   "Aporte Inicial"
               ForeColor       =   &H000000FF&
               Height          =   2200
               Left            =   45
               TabIndex        =   80
               Top             =   2320
               Width           =   8175
               Begin VSFlex8LCtl.VSFlexGrid Grilla_Checklist_Aporte_Inicial 
                  Height          =   1900
                  Left            =   100
                  TabIndex        =   81
                  Top             =   250
                  Width           =   8000
                  _cx             =   14111
                  _cy             =   3351
                  Appearance      =   2
                  BorderStyle     =   1
                  Enabled         =   -1  'True
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  MousePointer    =   0
                  BackColor       =   -2147483643
                  ForeColor       =   -2147483640
                  BackColorFixed  =   -2147483633
                  ForeColorFixed  =   -2147483630
                  BackColorSel    =   65535
                  ForeColorSel    =   0
                  BackColorBkg    =   -2147483643
                  BackColorAlternate=   -2147483643
                  GridColor       =   -2147483633
                  GridColorFixed  =   -2147483632
                  TreeColor       =   -2147483632
                  FloodColor      =   192
                  SheetBorder     =   -2147483642
                  FocusRect       =   2
                  HighLight       =   1
                  AllowSelection  =   -1  'True
                  AllowBigSelection=   -1  'True
                  AllowUserResizing=   1
                  SelectionMode   =   3
                  GridLines       =   10
                  GridLinesFixed  =   2
                  GridLineWidth   =   1
                  Rows            =   2
                  Cols            =   7
                  FixedRows       =   1
                  FixedCols       =   0
                  RowHeightMin    =   0
                  RowHeightMax    =   0
                  ColWidthMin     =   0
                  ColWidthMax     =   0
                  ExtendLastCol   =   -1  'True
                  FormatString    =   $"Frm_Mantenedor_Clientes.frx":0530
                  ScrollTrack     =   -1  'True
                  ScrollBars      =   3
                  ScrollTips      =   -1  'True
                  MergeCells      =   0
                  MergeCompare    =   0
                  AutoResize      =   -1  'True
                  AutoSizeMode    =   0
                  AutoSearch      =   2
                  AutoSearchDelay =   2
                  MultiTotals     =   -1  'True
                  SubtotalPosition=   1
                  OutlineBar      =   0
                  OutlineCol      =   0
                  Ellipsis        =   1
                  ExplorerBar     =   3
                  PicturesOver    =   0   'False
                  FillStyle       =   0
                  RightToLeft     =   0   'False
                  PictureType     =   0
                  TabBehavior     =   0
                  OwnerDraw       =   0
                  Editable        =   2
                  ShowComboButton =   1
                  WordWrap        =   0   'False
                  TextStyle       =   0
                  TextStyleFixed  =   0
                  OleDragMode     =   0
                  OleDropMode     =   0
                  ComboSearch     =   3
                  AutoSizeMouse   =   -1  'True
                  FrozenRows      =   0
                  FrozenCols      =   0
                  AllowUserFreezing=   0
                  BackColorFrozen =   0
                  ForeColorFrozen =   0
                  WallPaperAlignment=   9
                  AccessibleName  =   ""
                  AccessibleDescription=   ""
                  AccessibleValue =   ""
                  AccessibleRole  =   24
               End
            End
         End
         Begin VB.PictureBox Picture8 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   10995
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   39
            Top             =   330
            Width           =   8250
            Begin MSComctlLib.Toolbar Toolbar_Observaciones 
               Height          =   360
               Left            =   0
               TabIndex        =   44
               Top             =   0
               Width           =   8115
               _ExtentX        =   14314
               _ExtentY        =   635
               ButtonWidth     =   1852
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Wrappable       =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   3
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Agregar"
                     Key             =   "ADD"
                     Description     =   "Agrega Observaciones"
                     Object.ToolTipText     =   "Agrega Observaciones"
                  EndProperty
                  BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Modificar"
                     Key             =   "UPDATE"
                     Description     =   "Modifica Observaciones"
                     Object.ToolTipText     =   "Modifica Observaciones"
                  EndProperty
                  BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Eliminar"
                     Key             =   "DEL"
                     Description     =   "Elimina datos del representante."
                     Object.ToolTipText     =   "Elimina datos del representante."
                  EndProperty
               EndProperty
               BorderStyle     =   1
            End
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Observaciones 
               Height          =   4035
               Left            =   45
               TabIndex        =   45
               Top             =   420
               Width           =   8115
               _cx             =   14314
               _cy             =   7117
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   2
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Mantenedor_Clientes.frx":0674
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
         Begin VB.PictureBox Picture7 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   10695
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   38
            Top             =   330
            Width           =   8250
            Begin MSComctlLib.Toolbar Toolbar_Cuentas_Corrientes 
               Height          =   360
               Left            =   0
               TabIndex        =   78
               Top             =   0
               Width           =   8145
               _ExtentX        =   14367
               _ExtentY        =   635
               ButtonWidth     =   1852
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Wrappable       =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   3
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Agregar"
                     Key             =   "ADD"
                     Description     =   "Agrega Cta.Cte."
                     Object.ToolTipText     =   "Agrega Cta.Cte."
                  EndProperty
                  BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Modificar"
                     Key             =   "UP"
                     Description     =   "Modifica datos de la Cta.Cte."
                     Object.ToolTipText     =   "Modifica datos de la Cta.Cte."
                  EndProperty
                  BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Eliminar"
                     Key             =   "DEL"
                     Description     =   "Elimina datos de la Cta.Cte."
                     Object.ToolTipText     =   "Elimina datos de la Cta.Cte."
                  EndProperty
               EndProperty
               BorderStyle     =   1
            End
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas_Corrientes 
               Height          =   4035
               Left            =   45
               TabIndex        =   79
               Top             =   420
               Width           =   8145
               _cx             =   14367
               _cy             =   7117
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   16
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Mantenedor_Clientes.frx":06F4
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   2
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
         Begin VB.PictureBox Picture6 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   10395
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   34
            Top             =   330
            Width           =   8250
            Begin MSComctlLib.Toolbar Toolbar_Asesor 
               Height          =   360
               Left            =   0
               TabIndex        =   76
               Top             =   0
               Width           =   8115
               _ExtentX        =   14314
               _ExtentY        =   635
               ButtonWidth     =   1746
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Wrappable       =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   2
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Agregar"
                     Key             =   "ADD"
                     Description     =   "Agrega Asesor"
                     Object.ToolTipText     =   "Agrega Asesor"
                  EndProperty
                  BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Eliminar"
                     Key             =   "DEL"
                     Description     =   "Elimina datos del Asesor"
                     Object.ToolTipText     =   "Elimina datos del Asesor"
                  EndProperty
               EndProperty
               BorderStyle     =   1
            End
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Asesor 
               Height          =   4035
               Left            =   45
               TabIndex        =   77
               Top             =   420
               Width           =   8115
               _cx             =   14314
               _cy             =   7117
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   3
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Mantenedor_Clientes.frx":09AC
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   2
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
         Begin VB.PictureBox TabAlias 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   10095
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   33
            Top             =   330
            Width           =   8250
            Begin MSComctlLib.Toolbar Toolbar_Representante 
               Height          =   360
               Left            =   0
               TabIndex        =   74
               Top             =   0
               Width           =   8115
               _ExtentX        =   14314
               _ExtentY        =   635
               ButtonWidth     =   1852
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Wrappable       =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   3
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Agregar"
                     Key             =   "ADD"
                     Description     =   "Agrega representante."
                     Object.ToolTipText     =   "Agrega representante."
                  EndProperty
                  BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Modificar"
                     Key             =   "UP"
                     Description     =   "Modifica datos del representante."
                     Object.ToolTipText     =   "Modifica datos del representante."
                  EndProperty
                  BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Eliminar"
                     Key             =   "DEL"
                     Description     =   "Elimina datos del representante."
                     Object.ToolTipText     =   "Elimina datos del representante."
                  EndProperty
               EndProperty
               BorderStyle     =   1
            End
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Representantes 
               Height          =   4035
               Left            =   45
               TabIndex        =   75
               Top             =   420
               Width           =   8115
               _cx             =   14314
               _cy             =   7117
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   10
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Mantenedor_Clientes.frx":0A29
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   2
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
         Begin VB.PictureBox Picture5 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   9795
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   32
            Top             =   330
            Width           =   8250
            Begin VB.Frame Frame_TipoReferido 
               Height          =   4395
               Left            =   45
               TabIndex        =   60
               Top             =   120
               Width           =   8085
               Begin VB.CheckBox Chk_Cliente_Bancario 
                  Caption         =   "Es cliente del banco"
                  Height          =   255
                  Left            =   4230
                  TabIndex        =   62
                  Top             =   300
                  Width           =   3615
               End
               Begin VB.CheckBox Chk_ContratoBPI 
                  Caption         =   "Contrato BPI"
                  Height          =   255
                  Left            =   225
                  TabIndex        =   61
                  Top             =   270
                  Width           =   3615
               End
               Begin hControl2.hTextLabel Txt_Origen_Referido 
                  Height          =   345
                  Left            =   210
                  TabIndex        =   63
                  Top             =   2895
                  Width           =   7530
                  _ExtentX        =   13282
                  _ExtentY        =   609
                  LabelWidth      =   1515
                  TextMinWidth    =   1200
                  Caption         =   "Origen Referido"
                  Text            =   ""
                  MaxLength       =   10
               End
               Begin hControl2.hTextLabel Txt_Contato_Referido 
                  Height          =   345
                  Left            =   210
                  TabIndex        =   64
                  Top             =   3300
                  Width           =   7530
                  _ExtentX        =   13282
                  _ExtentY        =   609
                  LabelWidth      =   1515
                  Caption         =   "Contacto"
                  Text            =   ""
                  MaxLength       =   119
               End
               Begin TrueDBList80.TDBCombo Cmb_Tipo_Referido 
                  Height          =   345
                  Left            =   1770
                  TabIndex        =   65
                  Top             =   2490
                  Width           =   2760
                  _ExtentX        =   4868
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":0BDC
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin hControl2.hTextLabel Txt_CtaCte 
                  Height          =   345
                  Left            =   225
                  TabIndex        =   66
                  Top             =   2085
                  Width           =   4290
                  _ExtentX        =   7567
                  _ExtentY        =   609
                  LabelWidth      =   1515
                  TextMinWidth    =   1200
                  Caption         =   "Cuenta Corriente"
                  Text            =   ""
                  MaxLength       =   10
               End
               Begin TrueDBList80.TDBCombo Cmb_Banco 
                  Height          =   345
                  Left            =   1740
                  TabIndex        =   67
                  Top             =   1665
                  Width           =   2805
                  _ExtentX        =   4948
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":0C86
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin TrueDBList80.TDBCombo Cmb_Sucursal 
                  Height          =   345
                  Left            =   1755
                  TabIndex        =   68
                  Tag             =   "CAPTION=Pais"
                  Top             =   810
                  Width           =   2760
                  _ExtentX        =   4868
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":0D30
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin TrueDBList80.TDBCombo Cmb_Ejecutivo 
                  Height          =   345
                  Left            =   1755
                  TabIndex        =   69
                  Tag             =   "CAPTION=Pais"
                  Top             =   1245
                  Width           =   2760
                  _ExtentX        =   4868
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":0DDA
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin VB.Label Label12 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Tipo Referido"
                  Height          =   345
                  Left            =   210
                  TabIndex        =   73
                  Top             =   2490
                  Width           =   1515
               End
               Begin VB.Label Label19 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Banco"
                  Height          =   345
                  Left            =   225
                  TabIndex        =   72
                  Top             =   1665
                  Width           =   1515
               End
               Begin VB.Label Label10 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Sucursal"
                  Height          =   345
                  Left            =   225
                  TabIndex        =   71
                  Top             =   810
                  Width           =   1515
               End
               Begin VB.Label Label11 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Ejecutivo"
                  Height          =   345
                  Left            =   225
                  TabIndex        =   70
                  Top             =   1245
                  Width           =   1515
               End
            End
         End
         Begin VB.PictureBox Picture4 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   9495
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   31
            Top             =   330
            Width           =   8250
            Begin VB.Frame Frame3 
               Height          =   4425
               Left            =   45
               TabIndex        =   48
               Top             =   120
               Width           =   8085
               Begin VB.Frame Frame_Conyuge 
                  Caption         =   "Conyuge"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H000000FF&
                  Height          =   2385
                  Left            =   120
                  TabIndex        =   51
                  Top             =   1950
                  Width           =   7875
                  Begin hControl2.hTextLabel Txt_AP_Paterno_Conyuge 
                     Height          =   315
                     Left            =   180
                     TabIndex        =   52
                     Top             =   1050
                     Width           =   5475
                     _ExtentX        =   9657
                     _ExtentY        =   556
                     LabelWidth      =   1300
                     Caption         =   "Apellido Paterno"
                     Text            =   ""
                     MaxLength       =   49
                  End
                  Begin hControl2.hTextLabel Txt_AP_Materno_Conyuge 
                     Height          =   315
                     Left            =   180
                     TabIndex        =   53
                     Top             =   1410
                     Width           =   5475
                     _ExtentX        =   9657
                     _ExtentY        =   556
                     LabelWidth      =   1300
                     Caption         =   "Apellido Materno"
                     Text            =   ""
                     MaxLength       =   49
                  End
                  Begin hControl2.hTextLabel Txt_Rut_Conyuge 
                     Height          =   315
                     Left            =   180
                     TabIndex        =   54
                     Top             =   300
                     Width           =   2535
                     _ExtentX        =   4471
                     _ExtentY        =   556
                     LabelWidth      =   1300
                     TextMinWidth    =   1200
                     Caption         =   "RUT"
                     Text            =   ""
                     MaxLength       =   10
                  End
                  Begin hControl2.hTextLabel Txt_Nombres_Conyuge 
                     Height          =   315
                     Left            =   180
                     TabIndex        =   55
                     Top             =   690
                     Width           =   5475
                     _ExtentX        =   9657
                     _ExtentY        =   556
                     LabelWidth      =   1300
                     Caption         =   "Nombres"
                     Text            =   ""
                     MaxLength       =   49
                  End
                  Begin TrueDBList80.TDBCombo Cmb_Pais_Conyuge 
                     Height          =   345
                     Left            =   5700
                     TabIndex        =   56
                     Tag             =   "CAPTION=Pais"
                     Top             =   300
                     Width           =   2055
                     _ExtentX        =   3625
                     _ExtentY        =   609
                     _LayoutType     =   0
                     _RowHeight      =   -2147483647
                     _WasPersistedAsPixels=   0
                     _DropdownWidth  =   0
                     _EDITHEIGHT     =   609
                     _GAPHEIGHT      =   53
                     Columns(0)._VlistStyle=   0
                     Columns(0)._MaxComboItems=   5
                     Columns(0).DataField=   "ub_grid1"
                     Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                     Columns(1)._VlistStyle=   0
                     Columns(1)._MaxComboItems=   5
                     Columns(1).DataField=   "ub_grid2"
                     Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                     Columns.Count   =   2
                     Splits(0)._UserFlags=   0
                     Splits(0).ExtendRightColumn=   -1  'True
                     Splits(0).AllowRowSizing=   0   'False
                     Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                     Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                     Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                     Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                     Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                     Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                     Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                     Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                     Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                     Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                     Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                     Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                     Splits.Count    =   1
                     Appearance      =   3
                     BorderStyle     =   1
                     ComboStyle      =   0
                     AutoCompletion  =   -1  'True
                     LimitToList     =   -1  'True
                     ColumnHeaders   =   0   'False
                     ColumnFooters   =   0   'False
                     DataMode        =   5
                     DefColWidth     =   0
                     Enabled         =   -1  'True
                     HeadLines       =   1
                     FootLines       =   1
                     RowDividerStyle =   0
                     Caption         =   ""
                     EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                     LayoutName      =   ""
                     LayoutFileName  =   ""
                     MultipleLines   =   0
                     EmptyRows       =   0   'False
                     CellTips        =   0
                     AutoSize        =   0   'False
                     ListField       =   ""
                     BoundColumn     =   ""
                     IntegralHeight  =   0   'False
                     CellTipsWidth   =   0
                     CellTipsDelay   =   1000
                     AutoDropdown    =   -1  'True
                     RowTracking     =   -1  'True
                     RightToLeft     =   0   'False
                     MouseIcon       =   0
                     MouseIcon.vt    =   3
                     MousePointer    =   0
                     MatchEntryTimeout=   2000
                     OLEDragMode     =   0
                     OLEDropMode     =   0
                     AnimateWindow   =   3
                     AnimateWindowDirection=   5
                     AnimateWindowTime=   200
                     AnimateWindowClose=   1
                     DropdownPosition=   0
                     Locked          =   0   'False
                     ScrollTrack     =   -1  'True
                     ScrollTips      =   -1  'True
                     RowDividerColor =   14215660
                     RowSubDividerColor=   14215660
                     MaxComboItems   =   10
                     AddItemSeparator=   ";"
                     _PropDict       =   $"Frm_Mantenedor_Clientes.frx":0E84
                     _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                     _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                     _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                     _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                     _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                     _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                     _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                     _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                     _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                     _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                     _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                     _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                     _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                     _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                     _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                     _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                     _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                     _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                     _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                     _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                     _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                     _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                     _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                     _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                     _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                     _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                     _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                     _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                     _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                     _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                     _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                     _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                     _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                     _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                     _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                     _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                     _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                     _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                     _StyleDefs(38)  =   "Named:id=33:Normal"
                     _StyleDefs(39)  =   ":id=33,.parent=0"
                     _StyleDefs(40)  =   "Named:id=34:Heading"
                     _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                     _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                     _StyleDefs(43)  =   "Named:id=35:Footing"
                     _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                     _StyleDefs(45)  =   "Named:id=36:Selected"
                     _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                     _StyleDefs(47)  =   "Named:id=37:Caption"
                     _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                     _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                     _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                     _StyleDefs(51)  =   "Named:id=39:EvenRow"
                     _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                     _StyleDefs(53)  =   "Named:id=40:OddRow"
                     _StyleDefs(54)  =   ":id=40,.parent=33"
                     _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                     _StyleDefs(56)  =   ":id=41,.parent=34"
                     _StyleDefs(57)  =   "Named:id=42:FilterBar"
                     _StyleDefs(58)  =   ":id=42,.parent=33"
                  End
                  Begin VB.Label Label9 
                     BorderStyle     =   1  'Fixed Single
                     Caption         =   "Pais"
                     Height          =   345
                     Left            =   4200
                     TabIndex        =   57
                     Top             =   285
                     Width           =   1485
                  End
               End
               Begin VB.CheckBox Chk_Mayor_Edad 
                  Caption         =   "Tiene la mayor�a de edad"
                  Height          =   255
                  Left            =   180
                  TabIndex        =   50
                  Top             =   240
                  Width           =   3615
               End
               Begin TDBText6Ctl.TDBText Txt_Observaci�n 
                  Height          =   1245
                  Left            =   4290
                  TabIndex        =   49
                  Top             =   570
                  Width           =   3675
                  _Version        =   65536
                  _ExtentX        =   6482
                  _ExtentY        =   2196
                  Caption         =   "Frm_Mantenedor_Clientes.frx":0F2E
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DropDown        =   "Frm_Mantenedor_Clientes.frx":0F98
                  Key             =   "Frm_Mantenedor_Clientes.frx":0FB6
                  BackColor       =   -2147483643
                  EditMode        =   0
                  ForeColor       =   -2147483640
                  ReadOnly        =   0
                  ShowContextMenu =   1
                  MarginLeft      =   1
                  MarginRight     =   1
                  MarginTop       =   1
                  MarginBottom    =   1
                  Enabled         =   -1
                  MousePointer    =   0
                  Appearance      =   2
                  BorderStyle     =   1
                  AlignHorizontal =   0
                  AlignVertical   =   0
                  MultiLine       =   -1
                  ScrollBars      =   3
                  PasswordChar    =   ""
                  AllowSpace      =   -1
                  Format          =   ""
                  FormatMode      =   1
                  AutoConvert     =   -1
                  ErrorBeep       =   0
                  MaxLength       =   0
                  LengthAsByte    =   0
                  Text            =   ""
                  Furigana        =   0
                  HighlightText   =   -1
                  IMEMode         =   0
                  IMEStatus       =   0
                  DropWndWidth    =   0
                  DropWndHeight   =   0
                  ScrollBarMode   =   1
                  MoveOnLRKey     =   0
                  OLEDragMode     =   0
                  OLEDropMode     =   0
               End
               Begin hControl2.hTextLabel Txt_SitioWeb 
                  Height          =   345
                  Left            =   150
                  TabIndex        =   58
                  Top             =   570
                  Width           =   4050
                  _ExtentX        =   7144
                  _ExtentY        =   609
                  LabelWidth      =   1300
                  TextMinWidth    =   1200
                  Caption         =   "Sitio Web"
                  Text            =   ""
                  MaxLength       =   50
               End
               Begin VB.Label Label2 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Observaci�n"
                  Height          =   330
                  Left            =   4290
                  TabIndex        =   59
                  Top             =   210
                  Width           =   1485
               End
            End
         End
         Begin VB.PictureBox Picture3 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   9195
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   30
            Top             =   330
            Width           =   8250
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Direcciones 
               Height          =   4095
               Left            =   45
               TabIndex        =   46
               Top             =   420
               Width           =   8145
               _cx             =   14367
               _cy             =   7223
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   13
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Mantenedor_Clientes.frx":0FFA
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   0   'False
               AutoSizeMode    =   1
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   2
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
            Begin MSComctlLib.Toolbar Toolbar_Direccion 
               Height          =   360
               Left            =   0
               TabIndex        =   47
               Top             =   0
               Width           =   8145
               _ExtentX        =   14367
               _ExtentY        =   635
               ButtonWidth     =   1746
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Wrappable       =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   2
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Agregar"
                     Key             =   "ADD"
                     Description     =   "Agrega una direcci�n al cliente."
                     Object.ToolTipText     =   "Agrega una direcci�n al cliente."
                  EndProperty
                  BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Eliminar"
                     Key             =   "DEL"
                     Description     =   "Elimina la direcci�n del cliente."
                     Object.ToolTipText     =   "Elimina la direcci�n del cliente."
                  EndProperty
               EndProperty
               BorderStyle     =   1
            End
         End
         Begin VB.PictureBox Picture2 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   8895
            ScaleHeight     =   4605
            ScaleWidth      =   8250
            TabIndex        =   29
            Top             =   330
            Width           =   8250
            Begin VSFlex8LCtl.VSFlexGrid Grilla 
               Height          =   4395
               Left            =   45
               TabIndex        =   37
               Top             =   120
               Width           =   9825
               _cx             =   17330
               _cy             =   7752
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   14
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Mantenedor_Clientes.frx":123C
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
         Begin VB.PictureBox Picture1 
            BorderStyle     =   0  'None
            Height          =   4605
            Left            =   15
            ScaleHeight     =   4176.871
            ScaleMode       =   0  'User
            ScaleWidth      =   8250
            TabIndex        =   7
            Top             =   330
            Width           =   8250
            Begin VB.Frame Frame_Natural 
               Height          =   2985
               Left            =   45
               TabIndex        =   109
               Top             =   1545
               Width           =   8085
               Begin VB.Frame Pnl_Clientes_TipoTrabajador 
                  Caption         =   "Tipo Trabajador"
                  ForeColor       =   &H000000FF&
                  Height          =   975
                  Left            =   6500
                  TabIndex        =   110
                  Top             =   1600
                  Width           =   1500
                  Begin VB.OptionButton Opt_Dependiente 
                     Caption         =   "Dependiente"
                     Height          =   255
                     Left            =   100
                     TabIndex        =   112
                     Top             =   240
                     Width           =   1215
                  End
                  Begin VB.OptionButton Opt_Independiente 
                     Caption         =   "Independiente"
                     Height          =   255
                     Left            =   100
                     TabIndex        =   111
                     Top             =   600
                     Width           =   1350
                  End
               End
               Begin TrueDBList80.TDBCombo Cmb_Contrato_Nupcial 
                  Height          =   345
                  Left            =   4900
                  TabIndex        =   113
                  Tag             =   "CAPTION=Pais"
                  Top             =   2550
                  Width           =   1455
                  _ExtentX        =   2566
                  _ExtentY        =   609
                  _LayoutType     =   4
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   1
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=1"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":148A
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Named:id=33:Normal"
                  _StyleDefs(35)  =   ":id=33,.parent=0"
                  _StyleDefs(36)  =   "Named:id=34:Heading"
                  _StyleDefs(37)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(38)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(39)  =   "Named:id=35:Footing"
                  _StyleDefs(40)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(41)  =   "Named:id=36:Selected"
                  _StyleDefs(42)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(43)  =   "Named:id=37:Caption"
                  _StyleDefs(44)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(45)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(46)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=39:EvenRow"
                  _StyleDefs(48)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(49)  =   "Named:id=40:OddRow"
                  _StyleDefs(50)  =   ":id=40,.parent=33"
                  _StyleDefs(51)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(52)  =   ":id=41,.parent=34"
                  _StyleDefs(53)  =   "Named:id=42:FilterBar"
                  _StyleDefs(54)  =   ":id=42,.parent=33"
               End
               Begin hControl2.hTextLabel Txt_Ap_Paterno 
                  Height          =   315
                  Left            =   150
                  TabIndex        =   114
                  Top             =   540
                  Width           =   7710
                  _ExtentX        =   13600
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Apellido Paterno"
                  Text            =   ""
                  BackColorTxt    =   12648384
                  BackColorTxt    =   12648384
                  MaxLength       =   49
               End
               Begin hControl2.hTextLabel Txt_Ap_Materno 
                  Height          =   315
                  Left            =   150
                  TabIndex        =   115
                  Top             =   900
                  Width           =   7710
                  _ExtentX        =   13600
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Apellido Materno"
                  Text            =   ""
                  BackColorTxt    =   12648384
                  BackColorTxt    =   12648384
                  MaxLength       =   49
               End
               Begin hControl2.hTextLabel Txt_Profesion 
                  Height          =   315
                  Left            =   150
                  TabIndex        =   116
                  Top             =   1440
                  Width           =   3000
                  _ExtentX        =   5292
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Profesi�n"
                  Text            =   ""
                  MaxLength       =   19
               End
               Begin hControl2.hTextLabel Txt_Celular 
                  Height          =   315
                  Left            =   150
                  TabIndex        =   117
                  Top             =   2190
                  Width           =   3000
                  _ExtentX        =   5292
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Celular"
                  Text            =   ""
                  MaxLength       =   49
               End
               Begin hControl2.hTextLabel Txt_Empleador 
                  Height          =   315
                  Left            =   150
                  TabIndex        =   118
                  Top             =   2550
                  Width           =   3000
                  _ExtentX        =   5292
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Empleador"
                  Text            =   ""
                  MaxLength       =   49
               End
               Begin hControl2.hTextLabel Txt_Cargo 
                  Height          =   315
                  Left            =   150
                  TabIndex        =   119
                  Top             =   1800
                  Width           =   3000
                  _ExtentX        =   5292
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Cargo"
                  Text            =   ""
                  MaxLength       =   49
               End
               Begin MSComCtl2.DTPicker Txt_Fecha_Nacimiento 
                  Height          =   315
                  Left            =   4900
                  TabIndex        =   120
                  Top             =   1830
                  Width           =   1455
                  _ExtentX        =   2566
                  _ExtentY        =   556
                  _Version        =   393216
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  CheckBox        =   -1  'True
                  Format          =   80674817
                  CurrentDate     =   37732
               End
               Begin TrueDBList80.TDBCombo Cmb_Estado_Civil 
                  Height          =   345
                  Left            =   4900
                  TabIndex        =   121
                  Tag             =   "CAPTION=Pais"
                  Top             =   2190
                  Width           =   1455
                  _ExtentX        =   2566
                  _ExtentY        =   609
                  _LayoutType     =   4
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   1
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=1"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":1534
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Named:id=33:Normal"
                  _StyleDefs(35)  =   ":id=33,.parent=0"
                  _StyleDefs(36)  =   "Named:id=34:Heading"
                  _StyleDefs(37)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(38)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(39)  =   "Named:id=35:Footing"
                  _StyleDefs(40)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(41)  =   "Named:id=36:Selected"
                  _StyleDefs(42)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(43)  =   "Named:id=37:Caption"
                  _StyleDefs(44)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(45)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(46)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=39:EvenRow"
                  _StyleDefs(48)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(49)  =   "Named:id=40:OddRow"
                  _StyleDefs(50)  =   ":id=40,.parent=33"
                  _StyleDefs(51)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(52)  =   ":id=41,.parent=34"
                  _StyleDefs(53)  =   "Named:id=42:FilterBar"
                  _StyleDefs(54)  =   ":id=42,.parent=33"
               End
               Begin hControl2.hTextLabel Txt_Nombres 
                  Height          =   315
                  Left            =   150
                  TabIndex        =   122
                  Top             =   180
                  Width           =   7710
                  _ExtentX        =   13600
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Nombres"
                  Text            =   ""
                  BackColorTxt    =   12648384
                  BackColorTxt    =   12648384
                  MaxLength       =   49
               End
               Begin TrueDBList80.TDBCombo Cmb_Sexo 
                  Height          =   345
                  Left            =   4900
                  TabIndex        =   123
                  Tag             =   "CAPTION=Pais"
                  Top             =   1440
                  Width           =   1455
                  _ExtentX        =   2566
                  _ExtentY        =   609
                  _LayoutType     =   4
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid2"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid1"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=1905"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=1826"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   2
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":15DE
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=32,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=30,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=31,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=28,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=26,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=27,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin VB.Label Label4 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Fecha Nacimiento"
                  Height          =   345
                  Left            =   3400
                  TabIndex        =   127
                  Top             =   1830
                  Width           =   1485
               End
               Begin VB.Label Label3 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Sexo"
                  Height          =   345
                  Left            =   3400
                  TabIndex        =   126
                  Top             =   1440
                  Width           =   1485
               End
               Begin VB.Label Label5 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Estado Civil"
                  Height          =   345
                  Left            =   3400
                  TabIndex        =   125
                  Top             =   2190
                  Width           =   1485
               End
               Begin VB.Label Label13 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Contrato Nupcial"
                  Height          =   345
                  Left            =   3400
                  TabIndex        =   124
                  Top             =   2550
                  Width           =   1485
               End
            End
            Begin VB.Frame Frame5 
               Height          =   1425
               Left            =   45
               TabIndex        =   19
               Top             =   132
               Width           =   8085
               Begin hControl2.hTextLabel Txt_Rut 
                  Height          =   345
                  Left            =   120
                  TabIndex        =   20
                  Tag             =   "OBLI"
                  Top             =   180
                  Width           =   3390
                  _ExtentX        =   5980
                  _ExtentY        =   609
                  LabelWidth      =   1300
                  TextMinWidth    =   1200
                  Caption         =   "RUT"
                  Text            =   ""
                  BackColorTxt    =   12648384
                  BackColorTxt    =   12648384
                  MaxLength       =   10
               End
               Begin hControl2.hTextLabel Txt_Email 
                  Height          =   345
                  Left            =   4320
                  TabIndex        =   21
                  Top             =   570
                  Width           =   3540
                  _ExtentX        =   6244
                  _ExtentY        =   609
                  LabelWidth      =   1300
                  Caption         =   "Email"
                  Text            =   ""
                  MaxLength       =   119
               End
               Begin TrueDBList80.TDBCombo Cmb_Estado 
                  Height          =   345
                  Left            =   1440
                  TabIndex        =   22
                  Tag             =   "OBLI=S;CAPTION=Estado del Cliente. General"
                  Top             =   960
                  Width           =   2055
                  _ExtentX        =   3625
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":1688
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin TrueDBList80.TDBCombo Cmb_Pais 
                  Height          =   345
                  Left            =   5640
                  TabIndex        =   23
                  Tag             =   "CAPTION=Pais"
                  Top             =   960
                  Width           =   2235
                  _ExtentX        =   3942
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Mantenedor_Clientes.frx":1732
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin hControl2.hTextLabel Txt_Tipo 
                  Height          =   345
                  Left            =   120
                  TabIndex        =   24
                  Top             =   570
                  Width           =   3390
                  _ExtentX        =   5980
                  _ExtentY        =   609
                  LabelWidth      =   1300
                  Caption         =   "Tipo"
                  Text            =   ""
                  Locked          =   -1  'True
                  BackColorTxt    =   12648447
                  BackColorTxt    =   12648447
               End
               Begin MSComCtl2.DTPicker DTP_FechaCreacion 
                  Height          =   345
                  Left            =   5640
                  TabIndex        =   25
                  Top             =   180
                  Width           =   1455
                  _ExtentX        =   2566
                  _ExtentY        =   609
                  _Version        =   393216
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  CheckBox        =   -1  'True
                  Format          =   80674817
                  CurrentDate     =   37732
               End
               Begin VB.Label Label6 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Fecha Creaci�n"
                  Height          =   345
                  Left            =   4320
                  TabIndex        =   28
                  Top             =   180
                  Width           =   1305
               End
               Begin VB.Label Label8 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Pais"
                  Height          =   345
                  Left            =   4320
                  TabIndex        =   27
                  Top             =   960
                  Width           =   1305
               End
               Begin VB.Label Label1 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Estado"
                  Height          =   345
                  Left            =   120
                  TabIndex        =   26
                  Top             =   960
                  Width           =   1305
               End
            End
            Begin VB.Frame Frame_Juridico 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   2985
               Left            =   45
               TabIndex        =   8
               Top             =   1545
               Visible         =   0   'False
               Width           =   8085
               Begin VB.CheckBox ChkAntecedentes 
                  Caption         =   "Entreg� todos sus antecedentes"
                  Height          =   255
                  Left            =   4770
                  TabIndex        =   9
                  Top             =   2550
                  Width           =   2895
               End
               Begin hControl2.hTextLabel TxtRazonSocial 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   10
                  Top             =   300
                  Width           =   7770
                  _ExtentX        =   13705
                  _ExtentY        =   556
                  LabelWidth      =   1100
                  Caption         =   "Raz�n Social"
                  Text            =   ""
                  BackColorTxt    =   12648384
                  BackColorTxt    =   12648384
                  MaxLength       =   99
               End
               Begin hControl2.hTextLabel TxtGiro 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   11
                  Top             =   660
                  Width           =   7770
                  _ExtentX        =   13705
                  _ExtentY        =   556
                  LabelWidth      =   1100
                  Caption         =   "Giro"
                  Text            =   ""
                  MaxLength       =   99
               End
               Begin MSComCtl2.DTPicker TxtFormacion 
                  Height          =   345
                  Left            =   1230
                  TabIndex        =   12
                  Top             =   1020
                  Width           =   1455
                  _ExtentX        =   2566
                  _ExtentY        =   609
                  _Version        =   393216
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  CheckBox        =   -1  'True
                  Format          =   80674817
                  CurrentDate     =   37732
               End
               Begin hControl2.hTextLabel TxtLugar 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   13
                  Top             =   1410
                  Width           =   5250
                  _ExtentX        =   9260
                  _ExtentY        =   556
                  LabelWidth      =   1100
                  Caption         =   "Lugar"
                  Text            =   ""
                  MaxLength       =   79
               End
               Begin hControl2.hTextLabel TxtNotaria 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   14
                  Top             =   1770
                  Width           =   5250
                  _ExtentX        =   9260
                  _ExtentY        =   556
                  LabelWidth      =   1100
                  Caption         =   "Notaria"
                  Text            =   ""
                  MaxLength       =   99
               End
               Begin hControl2.hTextLabel TxtRegistro 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   15
                  Top             =   2130
                  Width           =   3870
                  _ExtentX        =   6826
                  _ExtentY        =   556
                  LabelWidth      =   1100
                  Caption         =   "Registro"
                  Text            =   ""
                  MaxLength       =   99
               End
               Begin hControl2.hTextLabel TxtNumero 
                  Height          =   315
                  Left            =   4770
                  TabIndex        =   16
                  Top             =   2130
                  Width           =   3120
                  _ExtentX        =   5503
                  _ExtentY        =   556
                  LabelWidth      =   1100
                  Caption         =   "N�mero"
                  Text            =   ""
                  MaxLength       =   99
               End
               Begin hControl2.hTextLabel TxtA�o 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   17
                  Top             =   2490
                  Width           =   2610
                  _ExtentX        =   4604
                  _ExtentY        =   556
                  LabelWidth      =   1100
                  Caption         =   "A�o"
                  Text            =   ""
                  MaxLength       =   99
               End
               Begin VB.Label Label7 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Formaci�n"
                  Height          =   345
                  Left            =   120
                  TabIndex        =   18
                  Top             =   1020
                  Width           =   1095
               End
            End
         End
      End
   End
   Begin VB.Frame FraCodigo 
      Height          =   1215
      Left            =   45
      TabIndex        =   0
      Top             =   360
      Width           =   8535
      Begin VB.CommandButton cmb_buscar_Rut 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3060
         Picture         =   "Frm_Mantenedor_Clientes.frx":17DC
         TabIndex        =   2
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmb_Buscar_NombreCliente 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7560
         Picture         =   "Frm_Mantenedor_Clientes.frx":1AE6
         TabIndex        =   1
         Top             =   600
         Width           =   375
      End
      Begin hControl2.hTextLabel Txt_Identificador_Cliente 
         Height          =   315
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   2910
         _ExtentX        =   5133
         _ExtentY        =   556
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "Identificador"
         Text            =   ""
         MaxLength       =   10
      End
      Begin hControl2.hTextLabel hTextLabel1 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   7410
         _ExtentX        =   13070
         _ExtentY        =   556
         LabelWidth      =   1245
         Caption         =   "Nombre"
         Text            =   ""
         MaxLength       =   99
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   35
      Top             =   0
      Width           =   8625
      _ExtentX        =   15214
      _ExtentY        =   635
      ButtonWidth     =   1931
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agregar Cliente"
            Object.ToolTipText     =   "Agregar Cliente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica Cliente"
            Object.ToolTipText     =   "Modifica Cliente"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Caption         =   "&Eliminar"
            Key             =   "DELETE"
            Description     =   "Eliminar Cliente"
            Object.ToolTipText     =   "Eliminar Cliente"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SEPARATOR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   36
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Mantenedor_Clientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fId_CheckEstado_Default As String
Dim fBorrar_Direccion As hRecord
Dim fBorrar_Representante As hRecord
Dim fBorrar_Asesor As hRecord
Dim fBorrar_Cta_Cte As hRecord
Dim fBorrar_Observacion As hRecord

Dim fFlg_Tipo_Permiso As String

Dim fTipo_Entidad As String

'-----------------------
Dim fIdPais As String
Dim fIdRegion   As String

Const fNatural = "N"
Const fJuridico = "J"
Const fTipo_Natural = "NATURAL"
Const fTipo_Juridico = "JUR�DICO"

Const fAntenedentes = "S"
Const fSin_Antecedentes = "N"

Private Enum eGrilla_Dir
  eGD_Direccion
  eGD_Comuna
  eGD_Cuidad
  eGD_Fono
  eGD_Fax
  eGD_Pais
End Enum

Private Enum eTabs
  eT_General
  eT_Direcciones
  eT_Varios
  eT_PorBanco
  eT_Representante
  eT_Asesores
  eT_CtaCte
  eT_Observaciones
  eT_CheckList
  eT_FormaOperacion
End Enum

'Para APV
Dim fId_Cliente_Empleador As Integer

Private Sub cmb_Buscar_NombreCliente_Click()
  fKey = NVL(Frm_Busca_Clientes.BuscarCliente(pFiltro_NombreCliente:=TxtRazonSocial.Text), 0)
  Call Fnt_Modificar
End Sub

Private Sub cmb_buscar_Rut_Click()
  fKey = NVL(Frm_Busca_Clientes.BuscarCliente(pFiltro_RutCliente:=Txt_Identificador_Cliente.Text), 0)
  Call Fnt_Modificar
End Sub

Private Sub Cmb_Estado_Civil_ItemChange()
  'Si el cliente esta casado activa el control, si no, lo desactiva
  Cmb_Contrato_Nupcial.Enabled = (Fnt_FindValue4Display(Cmb_Estado_Civil, Cmb_Estado_Civil.Text) = "CAS")
  Frame_Conyuge.Enabled = (Fnt_FindValue4Display(Cmb_Estado_Civil, Cmb_Estado_Civil.Text) = "CAS")
End Sub

Private Sub Cmb_Estado_GotFocus()
  SSTab.Tab = eT_General
End Sub

Private Sub Cmb_PaisEmp_Change()
  fIdPais = Fnt_ComboSelected_KEY(Cmb_PaisEmp)
  Call Sub_CargaCombo_RegionesEmp(Cmb_RegionEmp, fIdPais)
End Sub

Private Sub Cmb_PaisEmp_ItemChange()
  fIdPais = Fnt_ComboSelected_KEY(Cmb_PaisEmp)
  Call Sub_CargaCombo_RegionesEmp(Cmb_RegionEmp, fIdPais)
End Sub

Private Sub Cmb_RegionEmp_Change()
  fIdRegion = Fnt_ComboSelected_KEY(Cmb_RegionEmp)
  Call Sub_CargaCombo_ComunaCiudadEmp(Cmb_ComunaCiudadEmp)
End Sub

Private Sub Cmb_RegionEmp_ItemChange()
  fIdRegion = Fnt_ComboSelected_KEY(Cmb_RegionEmp)
  Call Sub_CargaCombo_ComunaCiudadEmp(Cmb_ComunaCiudadEmp)
End Sub

Private Sub Cmb_ComunaCiudadEmp_itemchange()
Dim fIdComunaCiudad As String
Dim fIdRegion As Integer

    fIdComunaCiudad = Fnt_ComboSelected_KEY(Cmb_ComunaCiudadEmp)
    fIdRegion = Fnt_ObtieneNroRegion(Cmb_ComunaCiudadEmp.Text)
    Call Sub_ComboSelectedItem(Cmb_RegionEmp, fIdRegion)
End Sub
Private Sub Cmb_Sucursal_ItemChange()
Dim lSucursal As String

  lSucursal = Fnt_FindValue4Display(Cmb_Sucursal, Cmb_Sucursal.Text)
  Cmb_Ejecutivo.Text = ""
  Call Sub_CargaCombo_EjecutivosSucursal(Cmb_Ejecutivo, pBlanco:=True, pCod_Sucursal:=lSucursal)
End Sub

Private Sub Sub_Tipo_Entidad()
Dim lVisibleNatural As Boolean
Dim lPosicion As Long
Dim lrut As Double
  
  lPosicion = InStr(1, Txt_Rut.Text, "-")
  lrut = CDbl(Left(Txt_Rut.Text, lPosicion - 1))
  
  If lrut >= (5 * (10 ^ 7)) Then
    fTipo_Entidad = fJuridico
    lVisibleNatural = False
    Txt_Tipo.Text = fTipo_Juridico
  Else
    fTipo_Entidad = fNatural
    lVisibleNatural = True
    Txt_Tipo.Text = fTipo_Natural
  End If
    
  Call Sub_Cambia_Frames(lVisibleNatural)
    
End Sub

Private Sub Sub_Cambia_Frames(pVisible_Natural As Boolean)
  'Naturales
  Frame_Natural.Visible = pVisible_Natural
  Frame_Conyuge.Visible = pVisible_Natural
  Chk_Mayor_Edad.Visible = pVisible_Natural
  'Juridicos
  Frame_Juridico.Visible = Not pVisible_Natural
End Sub

Private Sub Form_Load()

  Call Form_Resize
  
  Call Sub_Bloquea_Puntero(Me)
  
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  With Toolbar_Direccion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
  End With
  
  With Toolbar_Representante
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("UP").Image = cBoton_Modificar
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
  End With
  
  With Toolbar_Asesor
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
  End With
  
  With Toolbar_Cuentas_Corrientes
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("UP").Image = cBoton_Modificar
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
  End With
  
  With Toolbar_Observaciones
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
  End With
   
  Set fBorrar_Direccion = New hRecord
  With fBorrar_Direccion
    .AddField "id_direccion"
  End With
  
  Set fBorrar_Representante = New hRecord
  With fBorrar_Representante
    .AddField "id_representante"
  End With
  
  Set fBorrar_Observacion = New hRecord
  With fBorrar_Observacion
    .AddField "id_Observacion_cliente"
  End With
  
  Set fBorrar_Cta_Cte = New hRecord
  With fBorrar_Cta_Cte
    .AddField "id_Cta_Cte_Cliente"
  End With
  
  Set fBorrar_Asesor = New hRecord
  With fBorrar_Asesor
    .AddField "id_Asesor"
  End With
  
  Call Sub_CargaForm

'  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Fnt_Modificar()
  Dim lTipo As String
  
  If fKey = "" Then
    Txt_Identificador_Cliente.SetFocus
    GoTo ExitProcedure
  End If
  
  FraCodigo.Enabled = False
  FraDatos.Enabled = True

  Txt_Rut.Locked = True
  Txt_Rut.BackColorTxt = fColorNoEdit
  
  Call Sub_CargarDatos
  
ExitProcedure:
  
End Sub

Private Sub Fnt_Agregar()
  Dim lTipo As String

  fKey = cNewEntidad
  
  FraCodigo.Enabled = False
  FraDatos.Enabled = True

  Rem Por defecto entra como persona natural
  Call Sub_Cambia_Frames(True)

  Txt_Rut.SetFocus
  
End Sub
Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Asesor_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub Grilla_Asesor_ValidateEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
Dim lRow As Long
  
  With Grilla_Asesor
    For lRow = 1 To .Rows - 1
      If (Not lRow = Row) And (.EditText = .Cell(flexcpTextDisplay, lRow, Col)) Then
        Cancel = True
        MsgBox "Ya est� asignado este asesor al cliente.", vbInformation, Me.Caption
        Exit For
      End If
    Next
  End With
End Sub

Private Sub Grilla_Checklist_Aporte_Inicial_KeyPress(KeyAscii As Integer)
Dim lLargo As Integer

    
End Sub

Private Sub Grilla_Checklist_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Col = Grilla_Checklist.ColIndex("dsc_checklist") Then
    Cancel = True
  End If
  If Col = Grilla_Checklist.ColIndex("observacion") Then
    If GetCell(Grilla_Checklist, Row, "con_observacion") = "NO" Then
       Cancel = True
    End If
  End If
End Sub

Private Sub Grilla_Checklist_Aporte_Inicial_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Col = Grilla_Checklist_Aporte_Inicial.ColIndex("dsc_checklist") Then
       Cancel = True
  End If
  If Col = Grilla_Checklist_Aporte_Inicial.ColIndex("observacion") Then
    If GetCell(Grilla_Checklist_Aporte_Inicial, Row, "con_observacion") = "NO" Then
       Cancel = True
    End If
  End If
End Sub


Private Sub Grilla_Cuentas_Corrientes_AfterEdit(ByVal Row As Long, ByVal Col As Long)
  If Row > 0 Then
    Call SetCell(Grilla_Cuentas_Corrientes, Row, "estado_operacion", "U")
  End If
End Sub

Private Sub Grilla_Cuentas_Corrientes_DblClick()
Dim lKey As String
    With Grilla_Cuentas_Corrientes
        If .Row > 0 Then
            lKey = GetCell(Grilla_Cuentas_Corrientes, .Row, "id_Cta_Cte_Cliente")
            Call Sub_EsperaVentana_CtaCte(lKey)
        End If
    End With
End Sub

Private Sub Grilla_Direcciones_DblClick()
    Dim sId_Direccion As String
    If Grilla_Direcciones.Row > 0 Then
        sId_Direccion = GetCell(Grilla_Direcciones, Grilla_Direcciones.Row, "ID_DIRECCION")
        Call Sub_EsperaVentanaDireccion(sId_Direccion)
    End If
End Sub

Private Sub Grilla_Direcciones_GotFocus()
  SSTab.Tab = eT_Direcciones
End Sub

Private Sub Grilla_Formas_Operacion_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Col = Grilla_Formas_Operacion.ColIndex("dsc_contraparte") Then
    Cancel = True
  End If
End Sub

Private Sub Grilla_Observaciones_DblClick()
Dim lKey As String

  With Grilla_Observaciones
    If .Row > 0 Then
      lKey = GetCell(Grilla_Observaciones, .Row, "id_observacion")
      Call Sub_EsperaVentana_Observacion(lKey)
    End If
  End With
End Sub

Private Sub Grilla_Representantes_AfterEdit(ByVal Row As Long, ByVal Col As Long)
  If Row > 0 Then
    Call SetCell(Grilla_Representantes, Row, "estado_operacion", "U")
  End If
End Sub

Private Sub Grilla_Representantes_DblClick()
Dim lKey As String

  With Grilla_Representantes
    If .Row > 0 Then
      lKey = GetCell(Grilla_Representantes, .Row, "id_representante")
      Call Sub_EsperaVentana(lKey)
    End If
  End With
End Sub


Private Sub Opt_Dependiente_Click()
    If Opt_Dependiente.Value Then
        SSTab.TabEnabled(10) = True
        Call Sub_ComboSelectedItem(Cmb_PaisEmp, "CHI")
    Else
        SSTab.TabEnabled(10) = False
    End If
End Sub

Private Sub Opt_Independiente_Click()
    SSTab.TabEnabled(10) = False
End Sub

Private Sub Toolbar_Asesor_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim lLinea As Long

  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      lLinea = Grilla_Asesor.Rows
      Grilla_Asesor.AddItem ("")
      Call SetCell(Grilla_Asesor, lLinea, "tipo", "I")
    Case "DEL"
      If Grilla_Asesor.Row >= 0 Then
        If Grilla_Asesor.Row = 0 Then
        Else
          If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
            With Grilla_Asesor
              If GetCell(Grilla_Asesor, .Row, "TIPO") = "I" Then
                .RemoveItem (Grilla_Asesor.Row)
              Else
                If Not GetCell(Grilla_Asesor, .Row, "id_asesor") = "" Then
                  fBorrar_Asesor.Add.Fields("id_asesor").Value = GetCell(Grilla_Asesor, .Row, "id_asesor")
                  Call .RemoveItem(Grilla_Asesor.Row)
                End If
              End If
            End With
          End If
        End If
      End If

  End Select
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "ADD"
      Call Fnt_Agregar
    Case "SAVE"
      If Fnt_Grabar Then
        If MsgBox("Cliente guardado correctamente." & vbCr & "�Desea Imprimir el Cliente?.", vbYesNo + vbDefaultButton2 + vbInformation) = vbYes Then
          Call Frm_Reporte_Clientes_C.Sub_Reporte_Cliente_desde_Mantenedor(fKey)
        End If
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargaForm
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
'------------------------------------------------------
Dim lcClientes                As Object
Dim lcDireccion_Cliente       As Object
Dim lcRepresentante           As Object
Dim lcRel_Asesor_Cliente      As Object 'Class_Rel_Asesor_Cliente
Dim lcClientes_Ctas_Ctes      As Object 'Class_Clientes_Ctas_Ctes
Dim lcClientes_Observaciones  As Object 'Class_Clientes_Observaciones
Dim lcRel_Cliente_Checklist   As Object 'Class_Rel_Cliente_Checklist
Dim lcRel_Clte_Contra_FOpera  As Object 'Class_Rel_Clte_Contra_FOpera
'---------------APV
Dim lcClientes_Empleadores    As Object
Dim lcClientes_APV            As Object
'------------------
'------------------------------------------------------
Dim lId_Estado As String
Dim lSexo As String
Dim LEstadoCivil As String
Dim lLinea As Long
Dim lResult As Boolean
Dim lReg As hFields
Dim lAntecedentes As String
Dim lPais         As String
Dim lPais_Conyuge As String
'------------------------------------
Dim lSucursal       As String
Dim lBanco       As String
Dim lEjecutivo      As String
Dim lTipo_Referido  As String
Dim lContrato_BPI   As String
Dim lCliente_Bancario As String
Dim lMayor_Edad     As String
Dim lContrato_Nupcial

Dim ltemp As Long   'valor para nombre en asesor
Dim j As Long
Dim lexiste  As Long

  On Error GoTo ErrProcedure
  
  Call Sub_Bloquea_Puntero(Me)
  
  Call gDB.IniciarTransaccion
           
  lResult = False
  
  If Not Fnt_ValidarDatos Then
     GoTo ErrProcedure
  End If
  

  
    lId_Estado = Fnt_ComboSelected_KEY(Cmb_Estado)
    lSexo = Fnt_ComboSelected_KEY(Cmb_Sexo)
    LEstadoCivil = Fnt_ComboSelected_KEY(Cmb_Estado_Civil)
     
    If Not Fnt_FindValue4Display(Cmb_Estado_Civil, Cmb_Estado_Civil.Text) = "CAS" Then
        Txt_Rut_Conyuge.Text = ""
        Cmb_Pais_Conyuge.Text = ""
        Txt_Nombres_Conyuge.Text = ""
        Txt_AP_Paterno_Conyuge.Text = ""
        Txt_AP_Materno_Conyuge.Text = ""
    End If
  
    lAntecedentes = IIf(ChkAntecedentes.Value = vbChecked, fAntenedentes, fSin_Antecedentes)
    lPais = Fnt_ComboSelected_KEY(Cmb_Pais)
    lPais_Conyuge = Fnt_ComboSelected_KEY(Cmb_Pais_Conyuge)
    lContrato_Nupcial = Fnt_ComboSelected_KEY(Cmb_Contrato_Nupcial)
    
    lContrato_BPI = IIf(Chk_ContratoBPI.Value = vbChecked, "S", "N")
    lCliente_Bancario = IIf(Chk_Cliente_Bancario.Value = vbChecked, "S", "N")
    lMayor_Edad = IIf(Chk_Mayor_Edad.Value = vbChecked, "S", "N")
    
    lSucursal = Fnt_ComboSelected_KEY(Cmb_Sucursal)
    'Verifica que si es cCmbBLANCO lo deje como ""
    If lSucursal = cCmbBLANCO Then
    lSucursal = ""
    End If
    
    lBanco = Fnt_ComboSelected_KEY(Cmb_Banco)
    If lBanco = cCmbBLANCO Then
    lBanco = ""
    End If
    
    lTipo_Referido = Fnt_ComboSelected_KEY(Cmb_Tipo_Referido)
    'Verifica que si es cCmbBLANCO lo deje como ""
    If lTipo_Referido = cCmbBLANCO Then
    lTipo_Referido = ""
    End If
    
    lEjecutivo = Fnt_ComboSelected_KEY(Cmb_Ejecutivo)
    'Verifica que si es cCmbBLANCO lo deje como ""
    If lEjecutivo = cCmbBLANCO Then
    lEjecutivo = ""
    End If
  
  
    'Grabando el cliente
    Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
    With lcClientes
        Set .gDB = gDB
        .Campo("id_Cliente").Valor = fKey
        .Campo("cod_Estado").Valor = lId_Estado
        .Campo("rut_Cliente").Valor = Txt_Rut.Text
        .Campo("observaciones").Valor = Txt_Observaci�n.Text
        .Campo("cod_sucursal").Valor = lSucursal
        .Campo("Cod_Ejecutivo").Valor = lEjecutivo
        .Campo("email").Valor = Txt_Email.Text
        .Campo("cod_Pais").Valor = lPais
        .Campo("segmento_Cliente").Valor = ""
        .Campo("tipo_Entidad").Valor = fTipo_Entidad
     
        .Campo("FLG_MAYOR_EDAD").Valor = lMayor_Edad
        .Campo("FLG_CLIENTE_BANCARIO").Valor = lCliente_Bancario
        .Campo("CONTACTO_REFERIDO").Valor = Txt_Contato_Referido.Text
        .Campo("ORIGEN_REFERIDO").Valor = Txt_Origen_Referido.Text
        .Campo("SITIOWEB").Valor = Txt_SitioWeb.Text
        .Campo("ID_TIPO_REFERIDO").Valor = lTipo_Referido
        .Campo("FLG_CONTRATO_BPI").Valor = lContrato_BPI
        .Campo("ID_CONTRATO_NUPCIAL").Valor = lContrato_Nupcial
        .Campo("FECHA_CREACION").Valor = DTP_FechaCreacion.Value    '==== Agregado por MMA el 24/07/08
     
        If Opt_Dependiente.Value Then
            .Campo("TIPO_TRABAJADOR").Valor = "D"
        Else
            If Opt_Independiente.Value Then
                .Campo("TIPO_TRABAJADOR").Valor = "I"
            Else
                .Campo("TIPO_TRABAJADOR").Valor = ""
            End If
        End If
        
        If Frame_Natural.Visible Then
           '------------------------------ CLIENTE NATURAL
           .Campo("nombres").Valor = Txt_Nombres.Text
           .Campo("paterno").Valor = Txt_Ap_Paterno.Text
           .Campo("materno").Valor = Txt_Ap_Materno.Text
           .Campo("profesion").Valor = Txt_Profesion.Text
           .Campo("sexo").Valor = lSexo
           .Campo("cargo").Valor = Txt_Cargo.Text
           .Campo("fecha_Nacimiento").Valor = Txt_Fecha_Nacimiento.Value
           .Campo("celular").Valor = Txt_Celular.Text
           .Campo("estado_Civil").Valor = LEstadoCivil
           .Campo("empleador").Valor = Txt_Empleador.Text
           .Campo("rut_Conyuge").Valor = Txt_Rut_Conyuge.Text
           .Campo("paterno_Conyuge").Valor = Txt_AP_Paterno_Conyuge.Text
           .Campo("materno_Conyuge").Valor = Txt_AP_Materno_Conyuge.Text
           .Campo("nombres_Conyuge").Valor = Txt_Nombres_Conyuge.Text
           .Campo("cod_Pais_Conyuge").Valor = lPais_Conyuge
        
           .Campo("copia_Escritura").Valor = ""
           .Campo("razon_Social").Valor = ""
           .Campo("id_Tipo_Sociedad").Valor = ""
           .Campo("giro").Valor = ""
           .Campo("fecha_Formacion").Valor = Null
           .Campo("lugar").Valor = ""
           .Campo("notaria").Valor = ""
           .Campo("registro").Valor = ""
           .Campo("numero").Valor = ""
           .Campo("ano").Valor = ""
           .Campo("antecedentes").Valor = ""
        Else
           '-------------------- CLIENTE JURIDICO
           .Campo("copia_Escritura").Valor = ""
           .Campo("razon_Social").Valor = TxtRazonSocial.Text
           .Campo("id_Tipo_Sociedad").Valor = ""
           .Campo("giro").Valor = TxtGiro.Text
           .Campo("fecha_Formacion").Valor = TxtFormacion.Value
           .Campo("lugar").Valor = TxtLugar.Text
           .Campo("notaria").Valor = TxtNotaria.Text
           .Campo("registro").Valor = TxtRegistro.Text
           .Campo("numero").Valor = TxtNumero.Text
           .Campo("ano").Valor = TxtA�o.Text
           .Campo("antecedentes").Valor = lAntecedentes
        
           .Campo("nombres").Valor = ""
           .Campo("paterno").Valor = ""
           .Campo("materno").Valor = ""
           .Campo("profesion").Valor = ""
           .Campo("sexo").Valor = ""
           .Campo("cargo").Valor = ""
           .Campo("fecha_Nacimiento").Valor = Null
           .Campo("celular").Valor = ""
           .Campo("estado_Civil").Valor = ""
           .Campo("empleador").Valor = ""
           .Campo("rut_Conyuge").Valor = ""
           .Campo("paterno_Conyuge").Valor = ""
           .Campo("materno_Conyuge").Valor = ""
           .Campo("nombres_Conyuge").Valor = ""
           .Campo("cod_Pais_Conyuge").Valor = ""
           
        End If
       
        .Campo("ID_BANCO").Valor = lBanco
        .Campo("NUMERO_CTA_CTE").Valor = Txt_CtaCte.Text
   
   
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, "Problemas en grabar el Cliente.", .ErrMsg, pConLog:=True)
          GoTo ErrProcedure
        End If
   
      fKey = .Campo("id_Cliente").Valor
    End With
   
''-------------------------------------------------------------------------------------
'' Grabar Grilla de Direcciones
''-------------------------------------------------------------------------------------
  For Each lReg In fBorrar_Direccion
    Set lcDireccion_Cliente = Fnt_CreateObject(cDLL_Direccion_Cliente)
    With lcDireccion_Cliente
      Set .gDB = gDB
      .Campo("id_Direccion").Valor = lReg("id_direccion").Value
    
      If Not .Borrar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , "Problemas al eliminar la direcci�n del cliente." _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ErrProcedure
      End If
    End With
    Set lcDireccion_Cliente = Nothing
  Next
  
  '-------------------------------------------------------
  '-- REPRESENTANTES
  '-------------------------------------------------------
  For Each lReg In fBorrar_Representante
    Set lcRepresentante = Fnt_CreateObject(cDLL_Representantes)
    With lcRepresentante
      Set .gDB = gDB
      .Campo("id_representante").Valor = lReg("id_representante").Value
    
      If Not .Borrar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , "Problemas al eliminar representante del cliente." _
                          , .ErrMsg _
                          , pConLog:=True)
         GoTo ErrProcedure
      End If
    End With
    Set lcRepresentante = Nothing
  Next
  
  '-------------------------------------------------------
  '-- CUENTAS CORRIENTES
  '-------------------------------------------------------
  For Each lReg In fBorrar_Cta_Cte
    Set lcClientes_Ctas_Ctes = Fnt_CreateObject(cDLL_Clientes_Ctas_Ctes) 'New Class_Clientes_Ctas_Ctes
    With lcClientes_Ctas_Ctes
      Set .gDB = gDB
      .Campo("id_cta_cte_cliente").Valor = lReg("id_cta_cte_cliente").Value
      If Not .Borrar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , "Problemas al eliminar Cuentas Corrientes del cliente." _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ErrProcedure
      End If
    End With
    Set lcClientes_Ctas_Ctes = Nothing
  Next
  
  '-------------------------------------------------------
  '-- ASESORES
  '-------------------------------------------------------
  For lLinea = 1 To Grilla_Asesor.Rows - 1
    If GetCell(Grilla_Asesor, lLinea, "TIPO") = "I" Then
      Set lcRel_Asesor_Cliente = Fnt_CreateObject(cDLL_Rel_Asesor_Cliente) 'New Class_Rel_Asesor_Cliente
      With lcRel_Asesor_Cliente
        Set .gDB = gDB
        .Campo("ID_CLIENTE").Valor = fKey
        .Campo("ID_ASESOR").Valor = GetCell(Grilla_Asesor, lLinea, "nombre")
   
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG _
                            , "Problemas en grabar Asesores." _
                            , .ErrMsg _
                            , pConLog:=True)
           GoTo ErrProcedure
        End If
      End With
      Set lcRel_Asesor_Cliente = Nothing
    End If
  Next
  
  For Each lReg In fBorrar_Asesor
    Set lcRel_Asesor_Cliente = Fnt_CreateObject(cDLL_Rel_Asesor_Cliente) 'New Class_Rel_Asesor_Cliente
    With lcRel_Asesor_Cliente
      Set .gDB = gDB
      .Campo("id_Asesor").Valor = lReg("id_asesor").Value
      .Campo("id_Cliente").Valor = fKey
      
      If Not .Borrar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , "Problemas al eliminar Asesor del cliente." _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ErrProcedure
      End If
    End With
    Set lcRel_Asesor_Cliente = Nothing
  Next

  '-------------------------------------------------------
  '-- OBSERVACION
  '-------------------------------------------------------
  For Each lReg In fBorrar_Observacion
    Set lcClientes_Observaciones = Fnt_CreateObject(cDLL_Clientes_Observaciones) 'New Class_Clientes_Observaciones
    With lcClientes_Observaciones
      Set .gDB = gDB
      .Campo("id_Cliente_observacion").Valor = lReg("id_observacion_cliente").Value
      
      If Not .Borrar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , "Problemas al eliminar la observaci�n del cliente." _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ErrProcedure
      End If
    End With
    Set lcClientes_Observaciones = Nothing
  Next



  '-------------------------------------------------------
  '-- ESTADOS CHECKLIST
  '-------------------------------------------------------
  For lLinea = 1 To Grilla_Checklist.Rows - 1
    Set lcRel_Cliente_Checklist = Fnt_CreateObject(cDLL_Rel_Cliente_Checklist) ' New Class_Rel_Cliente_Checklist
    With lcRel_Cliente_Checklist
       Set .gDB = gDB
       .Campo("ID_CLIENTE").Valor = fKey
       .Campo("ID_Checklist").Valor = GetCell(Grilla_Checklist, lLinea, "ID_Checklist")
       .Campo("ID_Checklist_estado").Valor = GetCell(Grilla_Checklist, lLinea, "CMB_ESTADO")
       .Campo("Observacion").Valor = GetCell(Grilla_Checklist, lLinea, "OBSERVACION")
        
       If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG _
                            , "Problemas en grabar Checklist de Antecedentes del Cliente." _
                            , .ErrMsg _
                            , pConLog:=True)
          GoTo ErrProcedure
       End If
    End With
    Set lcRel_Cliente_Checklist = Nothing
  Next


  '-------------------------------------------------------
  '-- ESTADOS CHECKLIST APORTE
  '-------------------------------------------------------
  For lLinea = 1 To Grilla_Checklist_Aporte_Inicial.Rows - 1
    Set lcRel_Cliente_Checklist = Fnt_CreateObject(cDLL_Rel_Cliente_Checklist) ' New Class_Rel_Cliente_Checklist
    With lcRel_Cliente_Checklist
       Set .gDB = gDB
       .Campo("ID_CLIENTE").Valor = fKey
       .Campo("ID_Checklist").Valor = GetCell(Grilla_Checklist_Aporte_Inicial, lLinea, "ID_Checklist")
       .Campo("ID_Checklist_estado").Valor = GetCell(Grilla_Checklist_Aporte_Inicial, lLinea, "CMB_ESTADO")
       .Campo("Observacion").Valor = GetCell(Grilla_Checklist_Aporte_Inicial, lLinea, "OBSERVACION")
       If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG _
                            , "Problemas en grabar Checklist Aporte Inicial del Cliente." _
                            , .ErrMsg _
                            , pConLog:=True)
          GoTo ErrProcedure
       End If
    End With
    Set lcRel_Cliente_Checklist = Nothing
  Next
  '-------------------------------------------------------
  '-- RELACION DE LAS FORMAS DE OPERACION
  '-------------------------------------------------------
  For lLinea = 1 To Grilla_Formas_Operacion.Rows - 1
    Set lcRel_Clte_Contra_FOpera = Fnt_CreateObject(cDLL_Rel_Clte_Contra_FOpera) 'New Class_Rel_Clte_Contra_FOpera
    With lcRel_Clte_Contra_FOpera
      Set .gDB = gDB
      .Campo("ID_CLIENTE").Valor = fKey
      .Campo("ID_CONTRAPARTE").Valor = GetCell(Grilla_Formas_Operacion, lLinea, "ID_contraparte")
      If Not Trim(GetCell(Grilla_Formas_Operacion, lLinea, "cmb_forma")) = "" Then
        .Campo("ID_FORMA_OPERACION").Valor = GetCell(Grilla_Formas_Operacion, lLinea, "cmb_forma")
      
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG _
                            , "Problemas en grabar la Forma de Operaci�n del Cliente." _
                            , .ErrMsg _
                            , pConLog:=True)
           GoTo ErrProcedure
        End If
      Else
        If Not .Borrar Then
          Call Fnt_MsgError(.SubTipo_LOG _
                            , "Problemas al eliminar la Forma de Operaci�n del Cliente." _
                            , .ErrMsg _
                            , pConLog:=True)
           GoTo ErrProcedure
        End If
      End If
    End With
    Set lcRel_Clte_Contra_FOpera = Nothing
  Next

  '-------------------------------------------------------
  '-- DATOS EMPLEADOR
  '-------------------------------------------------------
  If Opt_Dependiente.Value Then
    Set lcClientes_Empleadores = Fnt_CreateObject(cDLL_Clientes_Empleadores)
    With lcClientes_Empleadores
        Set .gDB = gDB
            .Campo("id_cliente_empleador").Valor = fId_Cliente_Empleador
            .Campo("id_cliente").Valor = fKey
            .Campo("rut_empleador").Valor = Txt_RutEmpleador.Text
            .Campo("razon_social").Valor = Txt_RazonSocialEmpleador.Text
            .Campo("giro").Valor = Txt_GiroEmpleador.Text
            .Campo("casilla").Valor = Txt_CasillaEmpleador.Text
            .Campo("email").Valor = Txt_EmailEmpleador.Text
            .Campo("fono").Valor = Txt_TelefonoEmpleador.Text
            .Campo("direccion").Valor = Txt_DireccionEmpleador.Text
            .Campo("id_comuna_ciudad").Valor = Fnt_ComboSelected_KEY(Cmb_ComunaCiudadEmp)
            .Campo("id_region").Valor = Fnt_ComboSelected_KEY(Cmb_RegionEmp)
            .Campo("cod_pais").Valor = Fnt_ComboSelected_KEY(Cmb_PaisEmp)
            .Campo("contacto").Valor = Txt_NombreContactoEmp.Text
            .Campo("email_contacto").Valor = Txt_EmailContactoEmp.Text
            .Campo("fono_contacto").Valor = Txt_FonoContactoEmp.Text
            .Campo("id_cliente_empleador").Valor = fId_Cliente_Empleador
            If Not .Guardar Then
              Call Fnt_MsgError(.SubTipo_LOG _
                                , "Problemas en grabar datos Empleador." _
                                , .ErrMsg _
                                , pConLog:=True)
               GoTo ErrProcedure
            End If
        Set lcClientes_Empleadores = Nothing
    End With
    
  End If


  '-------------------------------------------------------
  '-- DATOS APV
  '-------------------------------------------------------
    If Cmb_InstitucionPrevisional.Text <> "" Then
        Set lcClientes_APV = Fnt_CreateObject(cDLL_Clientes_APV)
        With lcClientes_APV
            Set .gDB = gDB
            .Campo("id_cliente").Valor = fKey
            .Campo("id_institucion").Valor = Fnt_ComboSelected_KEY(Cmb_InstitucionPrevisional)
            .Campo("fecha_prevision").Valor = DTP_Fecha_Prevision.Value 'IIf(IsNull(lReg("fecha_prevision")), "", lReg("fecha_prevision"))
            If Not .Guardar Then
              Call Fnt_MsgError(.SubTipo_LOG _
                                , "Problemas en grabar datos APV." _
                                , .ErrMsg _
                                , pConLog:=True)
               GoTo ErrProcedure
            End If
            Set lcClientes_APV = Nothing
        End With
    End If

  lResult = True

ErrProcedure:
  If Err Then
    lResult = False
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al grabar el cliente", Err.Description, pConLog:=True)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  If lResult Then
    Call gDB.CommitTransaccion
    'MsgBox "Cliente guardado.", vbInformation, "Guardar Cliente"
  Else
    Call gDB.RollbackTransaccion
  End If
  
  Fnt_Grabar = lResult
  
  Call Sub_Desbloquea_Puntero(Me)
End Function

Private Sub Sub_CargaForm()
Dim lcAsesores          As Class_Asesor
Dim lcPaises            As Class_Paises
Dim lcChecklist_Estado  As Object 'Class_Checklist_Estados
Dim lcContraparte       As Class_Contrapartes
Dim lcForma_Operacion   As Class_Formas_Operacion
'------------------------------------------------------------
Dim lReg As hFields
Dim lComboList As String
Dim lLinea As Long

Rem Agrega CGarcia 23/11/2009
Dim lClass_Entidad As Class_Entidad
Dim pId_Empresa As String
Dim lEncontrado As Boolean

  Call Sub_Bloquea_Puntero(Me)
  
  FraCodigo.Enabled = True
  FraDatos.Enabled = False
  
  'Limpia las direcciones borradas
  Call fBorrar_Direccion.LimpiarRegistros
  Call fBorrar_Representante.LimpiarRegistros
  Call fBorrar_Cta_Cte.LimpiarRegistros
  
  Call Sub_FormControl_Color(Me.Controls)
  DTP_FechaCreacion.Value = ""
  
  Call Sub_CargaCombo_Estados(Cmb_Estado, cTEstado_Cliente)
  Call Sub_Elije_EstadoDefault(Cmb_Estado) 'HAF 20070703
  
  'Call Sub_ComboSelectedItem(Cmb_Estado, cCod_Estado_Vigente) ' Agregado CSM 11/06/2007
  
  Call Sub_CargaCombo_Sucursales(Cmb_Sucursal, pBlanco:=True)
  Call Sub_CargaCombo_Bancos(Cmb_Banco, pBlanco:=True)
  Call Sub_CargaCombo_Tipo_Referido(Cmb_Tipo_Referido)
  Call Sub_ComboSelectedItem(Cmb_Tipo_Referido, 1)
  Call Sub_CargaCombo_Contrato_Nupcial(Cmb_Contrato_Nupcial)
   
  With Cmb_Sexo
    .Clear
    .Text = ""
    .EmptyRows = True
    .Columns.Remove 1
    Call .AddItem("Masculino")
    Call .AddItem("Femenino")
    
    With .Columns(0).ValueItems
      .Clear
      .Add Fnt_AgregaValueItem("M", "Masculino")
      .Add Fnt_AgregaValueItem("F", "Femenino")
    End With
  End With
  Call Sub_ComboSelectedItem(Cmb_Sexo, "M") ' Agregado CSM 11/06/2007
  
  With Cmb_Estado_Civil
    .Clear
    .Text = ""
    .SelectedItem = Null
    Call .AddItem("Soltero")
    Call .AddItem("Casado")
    Call .AddItem("Viudo")
    Call .AddItem("Separado")
     
    With .Columns(0).ValueItems
      .Clear
      .Add Fnt_AgregaValueItem("SOL", "Soltero")
      .Add Fnt_AgregaValueItem("CAS", "Casado")
      .Add Fnt_AgregaValueItem("VIU", "Viudo")
      .Add Fnt_AgregaValueItem("SEP", "Separado")
    End With
  End With
  Call Sub_ComboSelectedItem(Cmb_Estado_Civil, "SOL") ' Agregado CSM 11/06/2007
  Call Cmb_Estado_Civil_ItemChange
  
  Call Sub_CargaCombo_Paises(pCombo:=Cmb_Pais)
  Call Sub_ComboSelectedItem(Cmb_Pais, "CHI") 'gcPais_Chile)                   ' Agregado CSM 11/06/2007
  
  Call Sub_CargaCombo_Paises(pCombo:=Cmb_Pais_Conyuge)
  Call Sub_ComboSelectedItem(Cmb_Pais_Conyuge, "CHI")           ' Agregado CSM 11/06/2007
  
  'Txt_Fecha_Nacimiento.Value = Fnt_FechaServidor    '==== Modificado por JOP el 18/03/07
  Txt_Fecha_Nacimiento.Value = ""
  TxtFormacion.Value = Fnt_FechaServidor            '==== Agregado por JOP el 18/03/07
  DTP_FechaCreacion.Value = Fnt_FechaServidor       '==== Agregado por MMA el 24/07/08
  
  ' Carga asesores
  lComboList = ""
  
  Rem 21/11/2009. Agregado por CGarcia.
  Rem Relaci�n ASESOR/EMPRESA
  pId_Empresa = Fnt_EmpresaActual

  Set lClass_Entidad = New Class_Entidad
  With lClass_Entidad
     
    .AddCampo "id_empresa", ePT_Numero, ePD_Entrada
    .Campo("id_empresa").Valor = pId_Empresa
    .AddCampo "Cod_Estado", ePT_Caracter, ePD_Entrada
    .Campo("Cod_Estado").Valor = cCod_Estado_Vigente
    lEncontrado = .Buscar("PKG_ASESORES.Buscar")
    If lEncontrado Then
      For Each lReg In .Cursor
        If Not lReg("cod_estado").Value = "C" Then
          lComboList = lComboList & "|#" & lReg("id_asesor").Value
          
          If lReg.Index = 1 Then
            lComboList = lComboList & "*1"
          End If
          
          lComboList = lComboList & ";" & lReg("id_asesor").Value & vbTab & lReg("nombre").Value
        End If
      Next
    Else
      MsgBox "Problemas en cargar los asesores." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lClass_Entidad = Nothing
  
  Grilla_Asesor.ColComboList(Grilla_Asesor.ColIndex("nombre")) = lComboList
  
  lComboList = ""
  Set lcChecklist_Estado = Fnt_CreateObject(cDLL_Checklist_Estados) 'New Class_Checklist_Estados
  With lcChecklist_Estado
    Set .gDB = gDB
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar Then
      For Each lReg In .Cursor
        lComboList = lComboList & "|#" & lReg("id_Checklist_Estado").Value
      
        If lReg.Index = 1 Then
           lComboList = lComboList & "*0"
        End If
      
        lComboList = lComboList & ";" & lReg("dsc_Checklist_Estado").Value
        
        If lReg("flg_default").Value = "S" Then
          fId_CheckEstado_Default = lReg("id_Checklist_Estado").Value
        End If
      Next
    Else
      MsgBox "Problemas en cargar los estados del checklist." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Grilla_Checklist.ColComboList(Grilla_Checklist.ColIndex("cmb_estado")) = lComboList
  Grilla_Checklist_Aporte_Inicial.ColComboList(Grilla_Checklist_Aporte_Inicial.ColIndex("cmb_estado")) = lComboList
  Set lcChecklist_Estado = Nothing
  
  
  '-------------------------------------------------------------------------
  '-- COMBO CON FORMAS DE OPERACION.
  '-------------------------------------------------------------------------
  lComboList = ""
  Set lcForma_Operacion = New Class_Formas_Operacion
  With lcForma_Operacion
    If .Buscar Then
      For Each lReg In .Cursor
        lComboList = lComboList & "|#" & lReg("id_forma_operacion").Value
      
        If lReg.Index = 1 Then
           lComboList = lComboList & "*0"
        End If
      
        lComboList = lComboList & ";" & lReg("dsc_forma_operacion").Value
      Next
    Else
      MsgBox "Problemas en cargar las Formas de Pago." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Grilla_Formas_Operacion.ColComboList(Grilla_Formas_Operacion.ColIndex("cmb_forma_pago")) = lComboList
  Set lcForma_Operacion = Nothing
  
  '-------------------------------------------------------------------------
  '-- CONTRAPARTES.
  '-------------------------------------------------------------------------
  Grilla_Formas_Operacion.Rows = 1
  Set lcContraparte = New Class_Contrapartes
  With lcContraparte
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Formas_Operacion.Rows
        Call Grilla_Formas_Operacion.AddItem("")
        
        Call SetCell(Grilla_Formas_Operacion, lLinea, "id_contraparte", "" & lReg("id_contraparte").Value, False)
        Call SetCell(Grilla_Formas_Operacion, lLinea, "dsc_contraparte", "" & lReg("dsc_contraparte").Value, False)
      Next
    Else
       MsgBox "Problemas en cargar las Contrapartes." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcContraparte = Nothing
  
  
  Txt_Origen_Referido.Text = ""
  Txt_Contato_Referido.Text = ""
  Txt_SitioWeb.Text = ""
  Chk_Cliente_Bancario.Value = vbUnchecked
  Cmb_Ejecutivo.SelectedItem = Null
  Cmb_Ejecutivo.Text = ""
  
  
  '-------------------------------------------------------------------------
  '-- DATOS EMPLEADOR.
  '-------------------------------------------------------------------------
  SSTab.TabEnabled(10) = True
  Call Sub_CargaCombo_Paises(Cmb_PaisEmp)
  Call Sub_ComboSelectedItem(Cmb_Pais, "CHI")
  Call Cmb_PaisEmp_ItemChange
  Call Sub_CargaCombo_ComunaCiudadEmp(Cmb_ComunaCiudadEmp)
  SSTab.TabEnabled(10) = False
  
  
  '-------------------------------------------------------------------------
  '-- DATOS APV.
  '-------------------------------------------------------------------------
  
  Call Sub_CargaCombo_InstitucionPrevisional(Cmb_InstitucionPrevisional, pIdGrupoInstitucion:=1)
  
  SSTab.CurrTab = 0
  
  Rem CGarcia 28/11/2009
  Rem Se Inhabilita el Tab Asesor para anular relaci�n Asesor/Cliente
  SSTab.TabEnabled(6) = False
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargarDatos()
    Dim lcClientes  As Object
    '-------------------------------------------
    Dim lReg As hCollection.hFields
    Dim lLinea As Long
    Dim lCod_Sucursal As String
    Dim lCod_Ejecutivo As String
  
    Dim sTipo_Trabajador As String
    
    Call Sub_Bloquea_Puntero(Me)
  
    Load Me
    DoEvents
  
    'Grabando el cliente
    Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
    With lcClientes
        'Set .gDB = gDB
        .Campo("Id_Cliente").Valor = fKey
        
        If .Buscar(True) Then
            For Each lReg In .Cursor
                
                Rem Frame de Codigo
                Txt_Identificador_Cliente.Text = NVL(lReg("rut_cliente").Value, "") 'Por el momento es el Rut
                hTextLabel1.Text = NVL(lReg("nombre_cliente").Value, "")
                
                Txt_Rut.Text = NVL(lReg("rut_cliente").Value, "")
                Txt_Nombres.Text = NVL(lReg("nombres").Value, "")
                Txt_Ap_Paterno.Text = NVL(lReg("paterno").Value, "")
                Txt_Ap_Materno.Text = NVL(lReg("materno").Value, "")
                Txt_Cargo.Text = NVL(lReg("cargo").Value, "")
                Txt_Celular.Text = NVL(lReg("celular").Value, "")
                Txt_Email.Text = NVL(lReg("email").Value, "")
                Txt_Empleador.Text = NVL(lReg("empleador").Value, "")
                Txt_Profesion.Text = NVL(lReg("profesion").Value, "")
                DTP_FechaCreacion.Value = IIf(IsNull(lReg("fecha_creacion")), "", lReg("fecha_creacion"))      '==== Agregado por MMA el 24/07/08
                
                Txt_Observaci�n.Text = NVL(lReg("observaciones").Value, "")
                Txt_Fecha_Nacimiento.Value = lReg("fecha_nacimiento").Value
                'Call Sub_ComboSelectedItem(Cmb_Pais, NVL(lReg("cod_pais").Value, gcPais_Chile))
                Call Sub_ComboSelectedItem(Cmb_Pais, NVL(lReg("cod_pais").Value, ""))
        
                'Call Sub_ComboSelectedItem(Cmb_Estado, NVL(lReg("cod_estado").Value, cCod_Estado_Vigente))
                Call Sub_ComboSelectedItem(Cmb_Estado, NVL(lReg("cod_estado").Value, ""))
        
                'Call Sub_ComboSelectedItem(Cmb_Sexo, NVL(lReg("sexo").Value, "M"))
                Call Sub_ComboSelectedItem(Cmb_Sexo, NVL(lReg("sexo").Value, ""))
        
                'Call Sub_ComboSelectedItem(Cmb_Estado_Civil, NVL(lReg("estado_civil").Value, "SOL"))
                Call Sub_ComboSelectedItem(Cmb_Estado_Civil, NVL(lReg("estado_civil").Value, ""))
        
                Call Sub_ComboSelectedItem(Cmb_Contrato_Nupcial, "" & lReg("id_contrato_nupcial").Value)
        
                'Activa el evento para que se pueda ejecutar
                Call Cmb_Estado_Civil_ItemChange
      
                '---------------------------
                '-- Datos del C�nyuge
                '---------------------------
                'Call Sub_ComboSelectedItem(Cmb_Pais_Conyuge, NVL(lReg("cod_pais_conyuge").Value, gcPais_Chile))
                Call Sub_ComboSelectedItem(Cmb_Pais_Conyuge, NVL(lReg("cod_pais_conyuge").Value, ""))
      
                Txt_Rut_Conyuge.Text = NVL(lReg("rut_conyuge").Value, "")
                Txt_Nombres_Conyuge.Text = NVL(lReg("nombres_conyuge").Value, "")
                Txt_AP_Paterno_Conyuge.Text = NVL(lReg("paterno_conyuge").Value, "")
                Txt_AP_Materno_Conyuge.Text = NVL(lReg("materno_conyuge").Value, "")
                '----------------
                '-- Datos empresas
                '----------------
                fTipo_Entidad = NVL(lReg("tipo_entidad").Value, fNatural)
                
                Select Case fTipo_Entidad
                  Case fNatural
                    Txt_Tipo.Text = fTipo_Natural
                    Call Sub_Cambia_Frames(True)
                  Case fJuridico
                    Txt_Tipo.Text = fTipo_Juridico
                    Call Sub_Cambia_Frames(False)
                End Select
                
                TxtRazonSocial.Text = NVL(lReg("RAZON_SOCIAL").Value, "")
                TxtGiro.Text = NVL(lReg("GIRO").Value, "")
                TxtFormacion.Value = lReg("FECHA_FORMACION").Value
                TxtLugar.Text = NVL(lReg("LUGAR").Value, "")
                TxtNotaria.Text = NVL(lReg("NOTARIA").Value, "")
                TxtRegistro.Text = NVL(lReg("REGISTRO").Value, "")
                TxtNumero.Text = NVL(lReg("NUMERO").Value, "")
                TxtA�o.Text = NVL(lReg("ANO").Value, "")
                ChkAntecedentes.Value = IIf(NVL(lReg("ANTECEDENTES").Value, fSin_Antecedentes) = fSin_Antecedentes, vbUnchecked, vbChecked)
                
                Txt_SitioWeb.Text = "" & lReg("SITIOWEB").Value
                
                '-----------------------------
                '-- Datos Por Banco
                '-------------------------------
                lCod_Sucursal = NVL(lReg("cod_sucursal").Value, "")
                If Not lCod_Sucursal = "" Then
                  '-- Selecciona la sucursal del cliente
                  Call Sub_ComboSelectedItem(Cmb_Sucursal, "" & lCod_Sucursal)
                  
                  '-- Carga los ejecutivos segun la sucursal del cliente
                  Call Sub_CargaCombo_EjecutivosSucursal(Cmb_Ejecutivo, pBlanco:=True, pCod_Sucursal:=lReg("cod_sucursal").Value)
                  
                  '-- Selecciona el ejecutivo del cliente
                  lCod_Ejecutivo = NVL(lReg("cod_ejecutivo").Value, "")
                  If Not lCod_Ejecutivo = "" Then
                    Call Sub_ComboSelectedItem(Cmb_Ejecutivo, "" & lCod_Ejecutivo)
                  End If
                End If
                
                Call Sub_ComboSelectedItem(Cmb_Banco, NVL(lReg("Id_Banco").Value, ""))
                
                Txt_CtaCte.Text = "" & lReg("Numero_Cta_Cte").Value
                
                Call Sub_ComboSelectedItem(Cmb_Tipo_Referido, lReg("Id_Tipo_Referido").Value)
                
                Txt_Origen_Referido.Text = "" & lReg("ORIGEN_REFERIDO").Value
                Txt_Contato_Referido.Text = "" & lReg("CONTACTO_REFERIDO").Value
                
                Chk_Mayor_Edad.Value = IIf(lReg("FLG_MAYOR_EDAD").Value = "S", vbChecked, vbUnchecked)
                Chk_ContratoBPI.Value = IIf(lReg("FLG_CONTRATO_BPI").Value = "S", vbChecked, vbUnchecked)
                Chk_Cliente_Bancario.Value = IIf("" & lReg("FLG_CLIENTE_BANCARIO").Value = "S", vbChecked, vbUnchecked)
        
                'APV. Tipo de Trabajador
                sTipo_Trabajador = NVL(lReg("TIPO_TRABAJADOR").Value, "I")
                If sTipo_Trabajador = "D" Then
                    Opt_Dependiente.Value = True
                    Opt_Independiente.Value = False
                Else
                    Opt_Dependiente.Value = False
                    Opt_Independiente.Value = True
                End If
                
                
            Next
        Else
            MsgBox "Problemas en cargar el Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        End If
    End With
    Set lcClientes = Nothing
  
    Call Sub_carga_DireccionesCliente
      
    Call Sub_Carga_Representantes
    Call Sub_Carga_Asesores
    Call Sub_Carga_CtaCtes
    Call Sub_Carga_Observaciones
    Call Sub_Carga_CheckList
    Call Sub_Carga_FormasPago
    
    'APV
    If sTipo_Trabajador = "D" Then
        SSTab.TabEnabled(10) = True
        Call Sub_Carga_EmpleadorCliente
    End If
    
    Call Sub_Carga_DatosAPV
    
        
    
    SSTab.CurrTab = 0
    
    Call Sub_Desbloquea_Puntero(Me)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Sub_carga_DireccionesCliente
' Author    : jvidal
' Date      : 19/01/2008
' Purpose   : Cargar la Grilla de Direcciones del Cliente
'---------------------------------------------------------------------------------------
Private Sub Sub_carga_DireccionesCliente()
    Dim lcDireccion_Cliente As Object
    Dim lReg                As hCollection.hFields
    Dim lLinea              As Long
    
    Grilla_Direcciones.Rows = 1
    
    Set lcDireccion_Cliente = Fnt_CreateObject(cDLL_Direccion_Cliente)
    
    With lcDireccion_Cliente
        Set .gDB = gDB
        
        .Campo("Id_Cliente").Valor = fKey
        
        If .Buscar Then
            For Each lReg In .Cursor
                lLinea = Grilla_Direcciones.Rows
                Call Grilla_Direcciones.AddItem("")
                
                Call SetCell(Grilla_Direcciones, lLinea, "ID_DIRECCION", NVL(lReg("ID_DIRECCION").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "ID_CLIENTE", NVL(lReg("ID_CLIENTE").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "Direccion", NVL(lReg("Direccion").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "FONO", NVL(lReg("FONO").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "FAX", NVL(lReg("FAX").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "ID_COMUNA_CIUDAD", NVL(lReg("ID_COMUNA_CIUDAD").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "DSC_COMUNA_CIUDAD", NVL(lReg("DSC_COMUNA_CIUDAD").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "COD_TIPO_DIRECCION", NVL(lReg("COD_TIPO_DIRECCION").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "DSC_TIPO_DIRECCION", NVL(lReg("DSC_TIPO_DIRECCION").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "ID_REGION", NVL(lReg("ID_REGION").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "DSC_REGION", NVL(lReg("DSC_REGION").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "Cod_Pais", NVL(lReg("Cod_Pais").Value, ""))
                Call SetCell(Grilla_Direcciones, lLinea, "DSC_PAIS", NVL(lReg("DSC_PAIS").Value, ""))
            Next
        Else
            MsgBox "Problemas en cargar las Direcciones del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        End If
        
    End With
    
    Set lcDireccion_Cliente = Nothing

End Sub

Private Sub Sub_Carga_Representantes()
    Dim lcRepresentante       As Object
    '------------------------------------------------------
    Dim lReg As hCollection.hFields
    Dim lLinea  As Long
  
    Grilla_Representantes.Rows = 1
    Set lcRepresentante = Fnt_CreateObject(cDLL_Representantes)
    
    With lcRepresentante
        Set .gDB = gDB
        .Campo("Id_Cliente").Valor = fKey
        If .Buscar Then
            For Each lReg In .Cursor
                lLinea = Grilla_Representantes.Rows
                Call Grilla_Representantes.AddItem("")
                
                Call SetCell(Grilla_Representantes, lLinea, "id_representante", NVL(lReg("Id_representante").Value, ""))
                Call SetCell(Grilla_Representantes, lLinea, "rut_representante", NVL(lReg("rut_representante").Value, ""))
                Call SetCell(Grilla_Representantes, lLinea, "nombre", NVL(lReg("nombre").Value, ""))
                Call SetCell(Grilla_Representantes, lLinea, "fono", NVL(lReg("fono").Value, ""))
                Call SetCell(Grilla_Representantes, lLinea, "celular", NVL(lReg("celular").Value, ""))
                Call SetCell(Grilla_Representantes, lLinea, "email", NVL(lReg("email").Value, ""))
                Call SetCell(Grilla_Representantes, lLinea, "observaciones", NVL(lReg("observaciones").Value, ""))
                Call SetCell(Grilla_Representantes, lLinea, "cargo", NVL(lReg("cargo").Value, ""))
                Call SetCell(Grilla_Representantes, lLinea, "fecha_nacimiento", NVL(lReg("fecha_nacimiento").Value, ""))
            Next
        Else
            MsgBox "Problemas en cargar los Representantes del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        End If
    End With
    Set lcRepresentante = Nothing
End Sub

Private Sub Sub_Carga_CtaCtes()
Dim lcClientes_Ctas_Ctes As Object 'Class_Clientes_Ctas_Ctes
'----------------------------------------------------------
Dim lReg As hCollection.hFields
Dim lLinea  As Long
  
  Grilla_Cuentas_Corrientes.Rows = 1
  Set lcClientes_Ctas_Ctes = Fnt_CreateObject(cDLL_Clientes_Ctas_Ctes) 'New Class_Clientes_Ctas_Ctes
  With lcClientes_Ctas_Ctes
    Set .gDB = gDB
    .Campo("Id_Cliente").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Cuentas_Corrientes.Rows
        Call Grilla_Cuentas_Corrientes.AddItem("")
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "id_Cta_Cte_Cliente", NVL(lReg("id_Cta_Cte_Cliente").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "ID_CLIENTE", NVL(lReg("ID_CLIENTE").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "ID_MONEDA", NVL(lReg("ID_MONEDA").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "DSC_MONEDA", NVL(lReg("DSC_MONEDA").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "ID_BANCO", NVL(lReg("ID_BANCO").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "DSC_BANCO", NVL(lReg("DSC_BANCO").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "ID_TIPO_ESTADO", NVL(lReg("ID_TIPO_ESTADO").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "COD_ESTADO", NVL(lReg("COD_ESTADO").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "DSC_ESTADO", NVL(lReg("DSC_ESTADO").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "NUMERO_CTA_CTE", NVL(lReg("NUMERO_CTA_CTE").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "SUCURSAL", NVL(lReg("SUCURSAL").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "EJECUTIVO", NVL(lReg("EJECUTIVO").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "FONO", NVL(lReg("FONO").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "FECHA_APERTURA", NVL(lReg("FECHA_APERTURA").Value, ""))
        Call SetCell(Grilla_Cuentas_Corrientes, lLinea, "FECHA_CIERRE", NVL(lReg("FECHA_CIERRE").Value, ""))
      Next
    Else
       MsgBox "Problemas al cargar las Cuentas Corrientes del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcClientes_Ctas_Ctes = Nothing
End Sub

Private Sub Sub_Carga_Asesores()
Dim lcRel_Asesor_Cliente As Object 'Class_Rel_Asesor_Cliente
'---------------------------------------------------
Dim lReg As hCollection.hFields
Dim lLinea  As Long
  
  Grilla_Asesor.Rows = 1
  Set lcRel_Asesor_Cliente = Fnt_CreateObject(cDLL_Rel_Asesor_Cliente) 'New Class_Rel_Asesor_Cliente
  With lcRel_Asesor_Cliente
    Set .gDB = gDB
    .Campo("ID_CLIENTE").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Asesor.Rows
        Call Grilla_Asesor.AddItem("")
        Call SetCell(Grilla_Asesor, lLinea, "id_asesor", NVL(lReg("id_asesor").Value, ""))
        Call SetCell(Grilla_Asesor, lLinea, "nombre", NVL(lReg("id_asesor").Value, ""))
      Next
    Else
       MsgBox "Problemas en cargar Asesores del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcRel_Asesor_Cliente = Nothing
End Sub
Private Function Fnt_ValidarDatos() As Boolean
Dim lLinea_Direciones As Long
Dim lLinea_Asesor As Long
Dim lLineas_Vacias As Long
Dim lcClientes As Object
Dim lMsg As String
Dim lLinea_Grilla As Integer
   
  Fnt_ValidarDatos = True
  lLineas_Vacias = 0
   
  Rem Valida campos obligatorios
  If Not Fnt_Form_Validar(Me.Controls) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
   
  Rem Valida Rut
  If Not Fnt_Valida_Rut(Txt_Rut) Then
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Not Fnt_Valida_Rut(Txt_Rut_Conyuge) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
'  If Not Fnt_VerificaEMAIL(Txt_Email.Text) Then
'    Fnt_ValidarDatos = False
'    Txt_Email.SetFocus
'    Exit Function
'  End If
'
    If Txt_Email.Text <> "" Then
        If Not Validar_Email(Txt_Email.Text) Then
            MsgBox "La direccion de correo no es valida", vbInformation, Me.Caption
            Txt_Email.SetFocus
            Fnt_ValidarDatos = False
            Exit Function
        End If
    End If
    
    If Txt_EmailContactoEmp.Text <> "" Then
        If Not Validar_Email(Txt_EmailContactoEmp.Text) Then
            MsgBox "La direccion de correo no es valida", vbInformation, Me.Caption
            Txt_EmailContactoEmp.SetFocus
            Fnt_ValidarDatos = False
            Exit Function
        End If
    End If
    
    If Txt_EmailEmpleador.Text <> "" Then
        If Not Validar_Email(Txt_EmailEmpleador.Text) Then
            MsgBox "La direccion de correo no es valida", vbInformation, Me.Caption
            Txt_EmailEmpleador.SetFocus
            Fnt_ValidarDatos = False
            Exit Function
        End If
    End If
      
  Select Case fTipo_Entidad
    Case fNatural
      If Trim(Txt_Nombres.Text) = "" Then
        MsgBox "Debe ingresar el nombre del cliente.", vbInformation, Me.Caption
        Fnt_ValidarDatos = False
        Txt_Nombres.SetFocus
        Exit Function
      End If
  
      If Trim(Txt_Ap_Paterno.Text) = "" Then
        MsgBox "Debe ingresar el apellido paterno del cliente.", vbInformation, Me.Caption
        Fnt_ValidarDatos = False
        Txt_Ap_Paterno.SetFocus
        Exit Function
      End If
  
      If Trim(Txt_Ap_Materno.Text) = "" Then
        MsgBox "Debe ingresar el apellido materno del cliente.", vbInformation, Me.Caption
        Fnt_ValidarDatos = False
        Txt_Ap_Materno.SetFocus
        Exit Function
      End If
    Case fJuridico
      If Trim(TxtRazonSocial.Text) = "" Then
        MsgBox "Debe ingresar la raz�n social del cliente.", vbInformation, Me.Caption
        Fnt_ValidarDatos = False
        TxtRazonSocial.SetFocus
        Exit Function
      End If
'==== Modificador por MMA. Ya no es obligatorio 22/07/08
'      If Trim(TxtGiro.Text) = "" Then
'        MsgBox "Debe ingresar el giro del cliente.", vbInformation, Me.Caption
'        Fnt_ValidarDatos = False
'        TxtGiro.SetFocus
'        Exit Function
'      End If
  End Select
  
   
  Rem Valida que el rut no est� en el sistema
  If fKey = cNewEntidad Then
    Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
    With lcClientes
      Set .gDB = gDB
      .Campo("Rut_Cliente").Valor = Txt_Rut.Text
      If .Buscar(True) Then
        If .Cursor.Count > 0 Then
          MsgBox "No se puede ingresar el cliente porque el Rut ya existe en el sistema." & vbCr & vbCr & .ErrMsg, vbExclamation, Me.Caption
          Fnt_ValidarDatos = False
          Exit Function
        End If
      Else
        MsgBox "Problemas en Validaci�n del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        Fnt_ValidarDatos = False
        Exit Function
      End If
    End With
  End If
  
'  Rem Valida campos de Direcciones de Clientes
'  For lLinea_Direciones = 1 To Grilla_Direcciones.Rows - 1
'    lMsg = ""
'    Select Case GetCell(Grilla_Direcciones, lLinea_Direciones, "estado_operacion")
'      Case "I", "U"
'        If GetCell(Grilla_Direcciones, lLinea_Direciones, "direccion") = "" Then
'          lMsg = "Debe ingresar una Direcci�n en la l�nea " & lLinea_Direciones & " en ""Direcciones""."
'        ElseIf Len(GetCell(Grilla_Direcciones, lLinea_Direciones, "direccion")) > 150 Then
'          lMsg = "La Direcci�n en la l�nea " & lLinea_Direciones & " en ""Direcciones"" debe tener m�ximo 150 caracteres."
'        ElseIf GetCell(Grilla_Direcciones, lLinea_Direciones, "comuna") = "" Then
'          lMsg = "Debe ingresar una Comuna en la l�nea " & lLinea_Direciones & " en ""Direcciones""."
'        ElseIf Trim(GetCell(Grilla_Direcciones, lLinea_Direciones, "pais")) = "" Then
'          lMsg = "Debe ingresar un Pais en la l�nea " & lLinea_Direciones & " en ""Direcciones""."
'        ElseIf Len(GetCell(Grilla_Direcciones, lLinea_Direciones, "comuna")) > 50 Then
'          lMsg = "La Comuna en la l�nea " & lLinea_Direciones & " en ""Direcciones"" debe tener m�ximo 50 caracteres."
'        ElseIf GetCell(Grilla_Direcciones, lLinea_Direciones, "cuidad") = "" Then
'          lMsg = "Debe ingresar una Ciudad en la l�nea " & lLinea_Direciones & " en ""Direcciones""."
'        ElseIf Len(GetCell(Grilla_Direcciones, lLinea_Direciones, "cuidad")) > 50 Then
'          lMsg = "La Ciudad en la l�nea " & lLinea_Direciones & " en ""Direcciones"" debe tener m�ximo 50 caracteres."
'        ElseIf Len(GetCell(Grilla_Direcciones, lLinea_Direciones, "fono")) > 50 Then
'          lMsg = "El Fono en la l�nea " & lLinea_Direciones & " en ""Direcciones"" debe tener m�ximo 50 caracteres."
'        ElseIf Len(GetCell(Grilla_Direcciones, lLinea_Direciones, "fax")) > 50 Then
'          lMsg = "El Fax en la l�nea " & lLinea_Direciones & " en ""Direcciones"" debe tener m�ximo 50 caracteres."
'        End If
'    End Select
'
'    If Not lMsg = "" Then
'      MsgBox lMsg, vbExclamation, Me.Caption
'      Fnt_ValidarDatos = False
'      Grilla_Direcciones.SetFocus
'      Exit Function
'    End If
'  Next

'=== Agregado por MMA 23/07/08. Valida largo de Observaciones
   With Grilla_Checklist
      For lLinea_Grilla = 1 To .Rows - 1
        If GetCell(Grilla_Checklist, lLinea_Grilla, "con_observacion") = "SI" Then
           If Len(GetCell(Grilla_Checklist, lLinea_Grilla, "observacion")) > 30 Then
                MsgBox "En Item " & GetCell(Grilla_Checklist, lLinea_Grilla, "dsc_checklist") & _
                       ", Debe ingresar una observaci�n menor a 30 caracteres.", vbExclamation, Me.Caption
                Fnt_ValidarDatos = False
                Exit Function
            End If
        End If
      Next
  End With
  
   With Grilla_Checklist_Aporte_Inicial
      For lLinea_Grilla = 1 To .Rows - 1
        If GetCell(Grilla_Checklist_Aporte_Inicial, lLinea_Grilla, "con_observacion") = "SI" Then
           If Len(GetCell(Grilla_Checklist_Aporte_Inicial, lLinea_Grilla, "observacion")) > 30 Then
                MsgBox "En Item " & GetCell(Grilla_Checklist_Aporte_Inicial, lLinea_Grilla, "dsc_checklist") & _
                       ", Debe ingresar una observaci�n menor a 30 caracteres.", vbExclamation, Me.Caption
                Fnt_ValidarDatos = False
                Exit Function
            End If
        End If
      Next
  End With
  
  For lLinea_Asesor = 1 To Grilla_Asesor.Rows - 1
    If GetCell(Grilla_Asesor, lLinea_Asesor, "TIPO") = "I" Then
      If GetCell(Grilla_Asesor, lLinea_Asesor, "nombre") = "" Then
        lLineas_Vacias = lLineas_Vacias + 1
      End If
    End If
  Next
  
  If lLineas_Vacias = 1 Then
    Fnt_ValidarDatos = False
    MsgBox "Hay una l�nea en blanco en ""Asesores""." & vbCr & "Debe elegir uno para grabar de los datos correctamente.", vbExclamation, Me.Caption
    Exit Function
  ElseIf lLineas_Vacias > 1 Then
    Fnt_ValidarDatos = False
    MsgBox "Hay " & lLineas_Vacias & " l�neas en blanco en ""Asesores""." & vbCr & "Debe seleccionar Asesores o eliminarlos para grabar de los datos correctamente.", vbExclamation, Me.Caption
    Exit Function
  End If
   
   
  If Opt_Dependiente.Value Then
    If Txt_RutEmpleador.Text = "" Then
        MsgBox "Debe ingresar Rut Empleador en ""Datos Empleador""." & vbCr & "Debe seleccionar Datos Empleador para grabar los datos correctamente.", vbExclamation, Me.Caption
        Fnt_ValidarDatos = False
        Exit Function
    End If
    If Not Fnt_Valida_Rut(Txt_RutEmpleador) Then
        Fnt_ValidarDatos = False
        Exit Function
    End If
    If Txt_RazonSocialEmpleador.Text = "" Then
        MsgBox "Debe ingresar Raz�n Social Empleador en ""Datos Empleador""." & vbCr & "Debe seleccionar Datos Empleador para grabar los datos correctamente.", vbExclamation, Me.Caption
        Fnt_ValidarDatos = False
        Exit Function
    End If
  End If
End Function

Private Sub Toolbar_Cuentas_Corrientes_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim llave As String
  
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      If Not fKey = cNewEntidad Then
        Call Sub_EsperaVentana_CtaCte(cNewEntidad)
      Else
        If MsgBox("Para agregar una Cuenta Corriente, debe existir el Cliente." & vbCr & vbCr & "�Desea agregar este Cliente?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
          If Fnt_Grabar Then
            Call Sub_CargarDatos
            Call Sub_EsperaVentana_CtaCte(cNewEntidad)
          End If
        End If
      End If
    Case "UP"
      Call Grilla_Cuentas_Corrientes_DblClick
    Case "DEL"
      If Grilla_Cuentas_Corrientes.Row > 0 Then
        If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
          With Grilla_Cuentas_Corrientes
            If Not GetCell(Grilla_Cuentas_Corrientes, .Row, "Id_Cta_Cte_Cliente") = "" Then
               fBorrar_Cta_Cte.Add.Fields("Id_Cta_Cte_Cliente").Value = GetCell(Grilla_Cuentas_Corrientes, .Row, "Id_Cta_Cte_Cliente")
            End If
          
            Call .RemoveItem(.Row)
          End With
        End If
      End If
  End Select
End Sub

Private Sub Toolbar_Direccion_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim llave As String
  
    Me.SetFocus
    DoEvents

    Select Case Button.Key
        Case "ADD"
            If Not fKey = cNewEntidad Then
                Call Sub_EsperaVentanaDireccion(cNewEntidad)
            Else
                If MsgBox("Para agregar una Direcci�n, debe existir el Cliente." & vbCr & vbCr & "�Desea agregar este Cliente?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
                    If Fnt_Grabar Then
                        Call Sub_CargarDatos
                        Call Sub_EsperaVentanaDireccion(cNewEntidad)
                    End If
                End If
            End If
    Case "UP"
        Call Grilla_Direcciones_DblClick
    Case "DEL"
        If Grilla_Direcciones.Row > 0 Then
            If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
                With Grilla_Direcciones
                    If Sub_BorrarDireccion() Then
                        Call Sub_carga_DireccionesCliente
                    End If
                End With
            End If
        End If
    End Select

'Dim lLinea As Long
'
'  Me.SetFocus
'  DoEvents
'
'  Select Case Button.Key
'    Case "ADD"
'      lLinea = Grilla_Direcciones.Rows
'      Call Grilla_Direcciones.AddItem("I")
'      Call SetCell(Grilla_Direcciones, lLinea, "id_direccion_cliente", cNewEntidad)
'      Call SetCell(Grilla_Direcciones, lLinea, "pais", "CHI")
'    Case "DEL"
'      If Grilla_Direcciones.Row > 0 Then
'        If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
'          With Grilla_Direcciones
'            If Not GetCell(Grilla_Direcciones, .Row, "id_direccion_cliente") = cNewEntidad Then
'              fBorrar_Direccion.Add.Fields("id_direccion").Value = GetCell(Grilla_Direcciones, .Row, "id_direccion_cliente")
'            End If
'            Call .RemoveItem(.Row)
'          End With
'        End If
'      End If
'  End Select
End Sub

Private Function Sub_BorrarDireccion() As Boolean
    Dim sId_Direccion   As String
    Dim lcDireccion     As Object
    
    Set lcDireccion = Fnt_CreateObject(cDLL_Direccion_Cliente)
    
    sId_Direccion = GetCell(Grilla_Direcciones, Grilla_Direcciones.Row, "ID_DIRECCION")
    
    With lcDireccion
        Set .gDB = gDB
        .Campo("ID_DIRECCION").Valor = sId_Direccion
        
        If Not .Borrar Then
            MsgBox "Problemas al borrar la direcci�n." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            Sub_BorrarDireccion = False
        Else
            Sub_BorrarDireccion = True
        End If
        
    End With
    Set lcDireccion = Nothing
End Function

Private Sub Toolbar_Observaciones_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_EsperaVentana_Observacion(cNewEntidad)
    Case "UPDATE"
      Call Grilla_Observaciones_DblClick
    Case "DEL"
      If Grilla_Observaciones.Row > 0 Then
        If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
          With Grilla_Observaciones
            If Not GetCell(Grilla_Observaciones, .Row, "id_observacion") = "" Then
               fBorrar_Observacion.Add.Fields("id_observacion_cliente").Value = GetCell(Grilla_Observaciones, .Row, "id_observacion")
            End If

            Call .RemoveItem(.Row)
          End With
        End If
      End If
  End Select
End Sub

Private Sub Toolbar_Representante_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim llave As String
  
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      If Not fKey = cNewEntidad Then
        Call Sub_EsperaVentana(cNewEntidad)
      Else
        If MsgBox("Para agregar un Representante, debe existir el Cliente." & vbCr & vbCr & "�Desea agregar este Cliente?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
          If Fnt_Grabar Then
            Call Sub_CargarDatos
            Call Sub_EsperaVentana(cNewEntidad)
          End If
        End If
      End If
    Case "UP"
      Call Grilla_Representantes_DblClick
    Case "DEL"
      If Grilla_Representantes.Row > 0 Then
        If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
          With Grilla_Representantes
            If Not GetCell(Grilla_Representantes, .Row, "id_representante") = "" Then
               fBorrar_Representante.Add.Fields("id_representante").Value = GetCell(Grilla_Representantes, .Row, "id_representante")
            End If
          
            Call .RemoveItem(.Row)
          End With
        End If
      End If
  End Select

End Sub
'---------------------------------------------------------------------------------------
' Procedure : Sub_EsperaVentanaDireccion
' Author    : Administrador
' Date      : 18/01/2008
' Purpose   :
'---------------------------------------------------------------------------------------
Private Sub Sub_EsperaVentanaDireccion(ByVal pkey)
    Dim lForm       As Frm_Direccion
    Dim lNom_Form   As String
  
    Me.Enabled = False
    lNom_Form = "Frm_Direccion"
    
    If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
        Set lForm = New Frm_Direccion
        Call lForm.Fnt_Modificar(fKey, pkey)
    
        Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
            DoEvents
        Loop
    
        Call Sub_carga_DireccionesCliente
    End If
  
    Me.Enabled = True
End Sub

Private Sub Sub_EsperaVentana(ByVal pkey)
    Dim lForm As Frm_Representante
    Dim lNom_Form As String
  
    Me.Enabled = False
  
    lNom_Form = "Frm_Representante"
  
    If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
        Set lForm = New Frm_Representante
        Call lForm.Fnt_Modificar(fKey, pkey)
    
        Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
            DoEvents
        Loop
        Call Sub_Carga_Representantes
    End If
  
    Me.Enabled = True
End Sub

Private Sub Sub_EsperaVentana_CtaCte(ByVal pkey)
    Dim lForm As Frm_CuentasCorrientes
    Dim lNom_Form As String
  
    Me.Enabled = False
  
    lNom_Form = "Frm_CuentasCorrientes"
  
    If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
        Set lForm = New Frm_CuentasCorrientes
        Call lForm.Fnt_Modificar(fKey, pkey)
    
        Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
            DoEvents
        Loop
    
        Call Sub_Carga_CtaCtes
    End If
  
    Me.Enabled = True
End Sub

Private Sub Txt_AP_Materno_Conyuge_KeyPress(KeyAscii As Integer)
    KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Ap_Materno_KeyPress(KeyAscii As Integer)
    KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_AP_Paterno_Conyuge_KeyPress(KeyAscii As Integer)
    KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Ap_Paterno_KeyPress(KeyAscii As Integer)
    KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Cargo_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Celular_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Contato_Referido_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_EMail_LostFocus()

'    If Txt_Email.Text <> "" Then
'        If Not Validar_Email(Txt_Email.Text) Then
'            MsgBox "La direccion de correo no es valida", vbInformation, Me.Caption
'            Txt_Email.SetFocus
'            Exit Sub
'        End If
'    End If
End Sub

Private Sub Txt_Empleador_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_NombreContacto_KeyDown(KeyCode As Integer, Shift As Integer)

End Sub

Private Sub Txt_Nombres_Conyuge_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Nombres_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Observaci�n_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Origen_Referido_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Profesion_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Rut_Conyuge_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Rut_Conyuge_LostFocus()
  Call Fnt_Valida_Rut(Txt_Rut_Conyuge)
End Sub

Private Sub Txt_Rut_GotFocus()
  'SSTab.Tab = eT_General
End Sub

Private Sub Txt_Rut_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub txt_rut_LostFocus()
  If Fnt_Valida_Rut(Txt_Rut) Then
    If Not Txt_Rut.Text = "" Then
      Call Sub_Tipo_Entidad
    End If
  End If
End Sub

Private Sub Sub_EsperaVentana_Observacion(ByVal pkey)
Dim lForm As Frm_Observacion_Cliente
Dim lNom_Form As String
  
  Me.Enabled = False
  
  lNom_Form = "Frm_Observacion_Cliente"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    
    Set lForm = New Frm_Observacion_Cliente
    Call lForm.Fnt_Modificar(pkey, fKey)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    Call Sub_Carga_Observaciones
  End If
  
  Me.Enabled = True
End Sub

Private Sub Sub_Carga_Observaciones()
Dim lcClientes_Observaciones As Object 'Class_Clientes_Observaciones
'---------------------------------------------------
Dim lReg As hCollection.hFields
Dim lLinea  As Long
  
  Grilla_Observaciones.Rows = 1
  Set lcClientes_Observaciones = Fnt_CreateObject(cDLL_Clientes_Observaciones) 'New Class_Clientes_Observaciones
  With lcClientes_Observaciones
    Set .gDB = gDB
    .Campo("ID_CLIENTE").Valor = fKey
    .Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Observaciones.Rows
        Call Grilla_Observaciones.AddItem("")
        Call SetCell(Grilla_Observaciones, lLinea, "id_observacion", NVL(lReg("id_cliente_observacion").Value, ""))
        Call SetCell(Grilla_Observaciones, lLinea, "Gls_Observacion_cliente", NVL(lReg("gls_cliente_observacion").Value, ""))
      Next
    Else
       MsgBox "Problemas en cargar las observaciones del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcClientes_Observaciones = Nothing
End Sub

Public Sub Sub_Carga_CheckList()
Dim lcChecklist_Cliente As Object 'Class_Checklist_Cliente
Dim lcRel_Cliente_Checklist As Object 'Class_Rel_Cliente_Checklist
'---------------------------------------------------
Dim lReg As hCollection.hFields
Dim lLinea  As Long
  
'Carga CheckList Detalle De Antecedentes
  'Primero busca todos los registros checklist pertenecientes a la empresa
  Grilla_Checklist.Rows = 1
  Set lcChecklist_Cliente = Fnt_CreateObject(cDLL_Checklist_Cliente) 'New Class_Checklist_Cliente
  With lcChecklist_Cliente
    Set .gDB = gDB
    .Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
    .Campo("TIPO_CHECKLIST").Valor = 1
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Checklist.Rows
        Call Grilla_Checklist.AddItem("")
        Call SetCell(Grilla_Checklist, lLinea, "id_checklist", "" & lReg("id_checklist").Value)
        Call SetCell(Grilla_Checklist, lLinea, "dsc_checklist", "" & lReg("DSC_CHECKLIST").Value)
        Call SetCell(Grilla_Checklist, lLinea, "cmb_estado", fId_CheckEstado_Default, False)
        Call SetCell(Grilla_Checklist, lLinea, "tipo_checklist", lReg("TIPO_CHECKLIST"), False)
        Call SetCell(Grilla_Checklist, lLinea, "orden", lReg("ORDEN"), False)
        Call SetCell(Grilla_Checklist, lLinea, "con_observacion", lReg("CON_OBSERVACION"), False)

      Next
    Else
       MsgBox "Problemas en cargar los Checklist del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcChecklist_Cliente = Nothing
  
  'Enlaza los estados grabados anteriormente
  Set lcRel_Cliente_Checklist = Fnt_CreateObject(cDLL_Rel_Cliente_Checklist)  ' New Class_Rel_Cliente_Checklist
  With lcRel_Cliente_Checklist
    Set .gDB = gDB
    .Campo("ID_CLIENTE").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Checklist.FindRow(Item:=lReg("id_checklist").Value, Col:=Grilla_Checklist.ColIndex("id_checklist"))
        If lLinea > -1 Then
          Call SetCell(Grilla_Checklist, lLinea, "cmb_estado", "" & lReg("ID_CHECKLIST_ESTADO").Value, False)
          Call SetCell(Grilla_Checklist, lLinea, "observacion", "" & lReg("OBSERVACION").Value, False)
        End If
      Next
    Else
       MsgBox "Problemas en cargar los Checklist seleccionados del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcChecklist_Cliente = Nothing
  
'Carga CheckList Aporte Inicial
  'Primero busca todos los registros checklist pertenecientes a la empresa
  Grilla_Checklist_Aporte_Inicial.Rows = 1
  Set lcChecklist_Cliente = Fnt_CreateObject(cDLL_Checklist_Cliente) 'New Class_Checklist_Cliente
  With lcChecklist_Cliente
    Set .gDB = gDB
    .Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
    .Campo("TIPO_CHECKLIST").Valor = 2
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Checklist_Aporte_Inicial.Rows
        Call Grilla_Checklist_Aporte_Inicial.AddItem("")
        Call SetCell(Grilla_Checklist_Aporte_Inicial, lLinea, "id_checklist", "" & lReg("id_checklist").Value)
        Call SetCell(Grilla_Checklist_Aporte_Inicial, lLinea, "dsc_checklist", "" & lReg("DSC_CHECKLIST").Value)
        Call SetCell(Grilla_Checklist_Aporte_Inicial, lLinea, "cmb_estado", fId_CheckEstado_Default, False)
        Call SetCell(Grilla_Checklist_Aporte_Inicial, lLinea, "tipo_checklist", lReg("TIPO_CHECKLIST"), False)
        Call SetCell(Grilla_Checklist_Aporte_Inicial, lLinea, "orden", lReg("ORDEN"), False)
        Call SetCell(Grilla_Checklist_Aporte_Inicial, lLinea, "con_observacion", lReg("CON_OBSERVACION"), False)

      Next
    Else
       MsgBox "Problemas en cargar los Checklist Aporte Inicial del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcChecklist_Cliente = Nothing
  
  'Enlaza los estados grabados anteriormente
  Set lcRel_Cliente_Checklist = Fnt_CreateObject(cDLL_Rel_Cliente_Checklist)  ' New Class_Rel_Cliente_Checklist
  With lcRel_Cliente_Checklist
    Set .gDB = gDB
    .Campo("ID_CLIENTE").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Checklist_Aporte_Inicial.FindRow(Item:=lReg("id_checklist").Value, Col:=Grilla_Checklist_Aporte_Inicial.ColIndex("id_checklist"))
        If lLinea > -1 Then
          Call SetCell(Grilla_Checklist_Aporte_Inicial, lLinea, "cmb_estado", "" & lReg("ID_CHECKLIST_ESTADO").Value, False)
          Call SetCell(Grilla_Checklist_Aporte_Inicial, lLinea, "observacion", "" & lReg("OBSERVACION").Value, False)
        End If
      Next
    Else
       MsgBox "Problemas en cargar los Checklist seleccionados del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcChecklist_Cliente = Nothing
End Sub

Public Sub Sub_Carga_FormasPago()
Dim lcRel_Clte_Contra_FOpera As Object 'Class_Rel_Clte_Contra_FOpera
'---------------------------------------------------
Dim lReg As hCollection.hFields
Dim lLinea  As Long

  'Enlaza las formas de pago con las contrapartes
  Set lcRel_Clte_Contra_FOpera = Fnt_CreateObject(cDLL_Rel_Clte_Contra_FOpera) 'New Class_Rel_Clte_Contra_FOpera
  With lcRel_Clte_Contra_FOpera
    Set .gDB = gDB
    .Campo("ID_CLIENTE").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Formas_Operacion.FindRow(Item:=lReg("id_contraparte").Value, Col:=Grilla_Formas_Operacion.ColIndex("id_contraparte"))
        If lLinea > -1 Then
          Call SetCell(Grilla_Formas_Operacion, lLinea, "cmb_forma", "" & lReg("id_forma_OPERACION").Value, False)
        End If
      Next
    Else
       MsgBox "Problemas en cargar las Formas de Pago seleccionadas para el Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcRel_Clte_Contra_FOpera = Nothing
End Sub

Private Sub TxtA�o_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub TxtGiro_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub TxtLugar_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub TxtNotaria_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub TxtNumero_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub TxtRazonSocial_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub TxtRegistro_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

'Private Sub Grilla_Direcciones_AfterEdit(ByVal Row As Long, ByVal Col As Long)
'  If Row > 0 Then
'    Call SetCell(Grilla_Direcciones, Row, "estado_operacion", "U")
'  End If
'End Sub


'Private Sub Grilla_Direcciones_KeyPress(KeyAscii As Integer)
'  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
'End Sub

'Private Sub Grilla_Direcciones_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
'  If Col = 7 Then
'    KeyAscii = 0
'  End If
'End Sub


'-------------------------------------------------
' Carga Regiones / ComunaCiudad para Empleadores
'-------------------------------------------------
Private Sub Sub_CargaCombo_RegionesEmp(pCombo As TDBCombo, ByVal pIdPais As String)
    Dim oRegion As Class_Regiones
    Dim lCampoPK As String
    Dim lReg            As hCollection.hFields
    Dim lTexto As String

    With pCombo
        .Text = ""
        .ClearFields
        .Clear
        .EmptyRows = True
        .Font.Charset = 0

        Call .Columns.Remove(1)

        .Columns(0).ValueItems.Clear
        .Columns(0).ValueItems.Translate = False

        lCampoPK = "id_Region"

        Set oRegion = New Class_Regiones

        oRegion.Campo("cod_pais").Valor = pIdPais

        If oRegion.Buscar Then
            For Each lReg In oRegion.Cursor
                lTexto = lReg("Dsc_Region").Value

                If Not gRelogDB Is Nothing Then
                    gRelogDB.AvanzaRelog
                End If

                Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(lCampoPK).Value, lTexto))

                Call .AddItem(lTexto)
            Next
        Else
            Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo.", oRegion.ErrMsg, pConLog:=True)
        End If

    End With

    Set oRegion = Nothing
End Sub

Private Sub Sub_CargaCombo_ComunaCiudadEmp(pCombo As TDBCombo)
    Dim oComunaCiudad As Class_ComunasRegiones
    Dim lCampoPK As String
    Dim lReg            As hCollection.hFields
    Dim lTexto As String

    With pCombo
        .Text = ""
        .ClearFields
        .Clear
        .EmptyRows = True
        .Font.Charset = 0

        Call .Columns.Remove(1)

        .Columns(0).ValueItems.Clear
        .Columns(0).ValueItems.Translate = False

        lCampoPK = "id_comuna_Ciudad"

        Set oComunaCiudad = New Class_ComunasRegiones

        If fIdRegion <> "" Then
            oComunaCiudad.Campo("id_region").Valor = fIdRegion
        End If
'        If fIdPais <> "" Then
'            oComunaCiudad.Campo("cod_pais").Valor = fIdPais
'        End If
        If oComunaCiudad.Buscar Then
            For Each lReg In oComunaCiudad.Cursor
                lTexto = lReg("Dsc_Comuna_Ciudad").Value & " (" & Fnt_RegionRomano(lReg("id_region").Value) & ")"

                If Not gRelogDB Is Nothing Then
                    gRelogDB.AvanzaRelog
                End If

                Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(lCampoPK).Value, lTexto))

                Call .AddItem(lTexto)
            Next
        Else
            Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo de Comunas/Ciudades.", oComunaCiudad.ErrMsg, pConLog:=True)
        End If

    End With

    Set oComunaCiudad = Nothing
End Sub

Private Function Fnt_RegionRomano(pid_region As Integer) As String
    Select Case pid_region
        Case 1
            Fnt_RegionRomano = "I"
        Case 2
            Fnt_RegionRomano = "II"
        Case 3
            Fnt_RegionRomano = "III"
        Case 4
            Fnt_RegionRomano = "IV"
        Case 5
            Fnt_RegionRomano = "V"
        Case 6
            Fnt_RegionRomano = "VI"
        Case 7
            Fnt_RegionRomano = "VII"
        Case 8
            Fnt_RegionRomano = "VIII"
        Case 9
            Fnt_RegionRomano = "IX"
        Case 10
            Fnt_RegionRomano = "X"
        Case 11
            Fnt_RegionRomano = "XI"
        Case 12
            Fnt_RegionRomano = "XII"
        Case 13
            Fnt_RegionRomano = "RM"
        Case 14
            Fnt_RegionRomano = "XIV"
        Case 15
            Fnt_RegionRomano = "XV"
    End Select
End Function
Private Sub Sub_Carga_EmpleadorCliente()
Dim lcClientesEmpleador As Object 'Class_Rel_Clte_Contra_FOpera
'---------------------------------------------------
Dim lReg As hCollection.hFields
Dim sCodPais As String
Dim sIdComunaCiudad As String
Dim sIDRegion As String

  'Enlaza las formas de pago con las contrapartess
  Set lcClientesEmpleador = Fnt_CreateObject(cDLL_Clientes_Empleadores)
  With lcClientesEmpleador
    Set .gDB = gDB
    .Campo("ID_CLIENTE").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        fId_Cliente_Empleador = lReg("id_cliente_empleador").Value
        Txt_RutEmpleador.Text = NVL(lReg("rut_empleador").Value, "")
        Txt_RazonSocialEmpleador.Text = NVL(lReg("razon_social").Value, "")
        Txt_GiroEmpleador.Text = NVL(lReg("giro").Value, "")
        Txt_CasillaEmpleador.Text = NVL(lReg("casilla").Value, "")
        Txt_EmailEmpleador.Text = NVL(lReg("email").Value, "")
        Txt_TelefonoEmpleador.Text = NVL(lReg("fono").Value, "")
        Txt_DireccionEmpleador.Text = NVL(lReg("direccion").Value, "")
        sCodPais = NVL(lReg("cod_pais").Value, "")
        sIdComunaCiudad = NVL(lReg("id_comuna_ciudad").Value, "")
        sIDRegion = NVL(lReg("id_region").Value, "")
        Txt_NombreContactoEmp.Text = NVL(lReg("contacto").Value, "")
        Txt_EmailContactoEmp.Text = NVL(lReg("email_contacto").Value, "")
        Txt_FonoContactoEmp.Text = NVL(lReg("fono_contacto").Value, "")
        
        Call Sub_CargaCombo_RegionesEmp(Cmb_RegionEmp, sCodPais)
        Call Sub_CargaCombo_ComunaCiudadEmp(Cmb_ComunaCiudadEmp)
                
        Call Sub_ComboSelectedItem(Cmb_PaisEmp, sCodPais)
        Call Sub_ComboSelectedItem(Cmb_RegionEmp, sIDRegion)
        Call Sub_ComboSelectedItem(Cmb_ComunaCiudadEmp, sIdComunaCiudad)
        
      Next
    Else
       MsgBox "Problemas en cargar datos del Empleador del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcClientesEmpleador = Nothing
End Sub

Private Sub Sub_Carga_DatosAPV()
Dim lcClientesAPV As Object
Dim lcInstitucionPrevisional As Class_InstitucionPrevisional
'---------------------------------------------------
Dim lReg As hCollection.hFields
Dim sId_Institucion As String

    'Enlaza las formas de pago con las contrapartes
    Set lcClientesAPV = Fnt_CreateObject(cDLL_Clientes_APV)
    With lcClientesAPV
      Set .gDB = gDB
      .Campo("ID_CLIENTE").Valor = fKey
      If .Buscar Then
        For Each lReg In .Cursor
          sId_Institucion = NVL(lReg("id_institucion").Value, "")
          Call Sub_ComboSelectedItem(Cmb_InstitucionPrevisional, sId_Institucion)
          DTP_Fecha_Prevision.Value = lReg("fecha_prevision").Value
        Next
      Else
         MsgBox "Problemas en cargar datos de APV del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      End If
    End With
    Set lcClientesAPV = Nothing
  
End Sub

Private Function Fnt_ObtieneNroRegion(sTexto As String) As Integer
Dim sRomano As String
Dim ipos1 As Integer
Dim ipos2 As Integer
    ipos1 = InStr(1, sTexto, "(")
    If ipos1 > 0 Then
        ipos2 = InStr(ipos1, sTexto, ")")
        sRomano = Mid(sTexto, ipos1 + 1, ipos2 - (ipos1 + 1))
        Select Case sRomano
            Case "I"
                Fnt_ObtieneNroRegion = 1
            Case "II"
                Fnt_ObtieneNroRegion = 2
            Case "III"
                Fnt_ObtieneNroRegion = 3
            Case "IV"
                Fnt_ObtieneNroRegion = 4
            Case "V"
                Fnt_ObtieneNroRegion = 5
            Case "VI"
                Fnt_ObtieneNroRegion = 6
            Case "VII"
                Fnt_ObtieneNroRegion = 7
            Case "VIII"
                Fnt_ObtieneNroRegion = 8
            Case "IX"
                Fnt_ObtieneNroRegion = 9
            Case "X"
                Fnt_ObtieneNroRegion = 10
            Case "XI"
                Fnt_ObtieneNroRegion = 11
            Case "XII"
                Fnt_ObtieneNroRegion = 12
            Case "RM"
                Fnt_ObtieneNroRegion = 13
            Case "XIV"
                Fnt_ObtieneNroRegion = 14
            Case "XV"
                Fnt_ObtieneNroRegion = 15
        End Select
    End If
End Function

Public Function Validar_Email(ByVal Email As String) As Boolean
    
    Dim i As Integer, iLen As Integer, caracter As String
    Dim pos As Integer, bp As Boolean, ipos As Integer, ipos2 As Integer

    On Local Error GoTo Err_Sub

    Email = Trim$(Email)

    If Email = vbNullString Then
        Exit Function
    End If

    Email = LCase$(Email)
    iLen = Len(Email)

    
    For i = 1 To iLen
        caracter = Mid(Email, i, 1)

        If (Not (caracter Like "[a-z]")) And (Not (caracter Like "[0-9]")) Then
            
            If InStr(1, "_-" & "." & "@", caracter) > 0 Then
                If bp = True Then
                   Exit Function
                Else
                    bp = True
                   
                    If i = 1 Or i = iLen Then
                        Exit Function
                    End If
                    
                    If caracter = "@" Then
                        If ipos = 0 Then
                            ipos = i
                        Else
                            
                            Exit Function
                        End If
                    End If
                    If caracter = "." Then
                        ipos2 = i
                    End If
                    
                End If
            Else
                
                Exit Function
            End If
        Else
            bp = False
        End If
    Next i
    If ipos = 0 Or ipos2 = 0 Then
        Exit Function
    End If
    
    If ipos2 < ipos Then
        Exit Function
    End If

    
    Validar_Email = True

    Exit Function
Err_Sub:
    On Local Error Resume Next
    
    Validar_Email = False
End Function
