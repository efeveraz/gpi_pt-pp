VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Consulta_AporteRetiro_Capital 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta Aporte Retiro Capital"
   ClientHeight    =   8745
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14625
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8745
   ScaleWidth      =   14625
   Begin TabDlg.SSTab SST_AportesRetiros 
      Height          =   6375
      Left            =   120
      TabIndex        =   3
      Top             =   2280
      Width           =   14415
      _ExtentX        =   25426
      _ExtentY        =   11245
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Detalle"
      TabPicture(0)   =   "Frm_Consulta_AporteRetiro_Capital.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Grilla_Detalle"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Resumen"
      TabPicture(1)   =   "Frm_Consulta_AporteRetiro_Capital.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Grilla_Resumen"
      Tab(1).ControlCount=   1
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Detalle 
         Height          =   5955
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   14145
         _cx             =   24950
         _cy             =   10504
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         SelectionMode   =   1
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   10
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Consulta_AporteRetiro_Capital.frx":0038
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   0
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Resumen 
         Height          =   5955
         Left            =   -74880
         TabIndex        =   5
         Top             =   360
         Width           =   14145
         _cx             =   24950
         _cy             =   10504
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         SelectionMode   =   1
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   5
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Consulta_AporteRetiro_Capital.frx":01E4
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   0
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filtro de B�squeda"
      ForeColor       =   &H000000FF&
      Height          =   1695
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   14400
      Begin VB.Frame Frame5 
         Caption         =   "Moneda de Salida"
         ForeColor       =   &H000000FF&
         Height          =   735
         Left            =   10920
         TabIndex        =   22
         Top             =   240
         Width           =   3375
         Begin TrueDBList80.TDBCombo Cmb_Monedas 
            Height          =   345
            Left            =   120
            TabIndex        =   23
            Tag             =   "OBLI=S;CAPTION=Moneda de Salida"
            Top             =   240
            Width           =   3135
            _ExtentX        =   5530
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_AporteRetiro_Capital.frx":02AF
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
      End
      Begin VB.Frame Frame4 
         Height          =   1335
         Left            =   5000
         TabIndex        =   17
         Top             =   240
         Width           =   5895
         Begin TrueDBList80.TDBCombo Cmb_Asesor 
            Height          =   345
            Left            =   840
            TabIndex        =   18
            Tag             =   "SOLOLECTURA=N"
            Top             =   240
            Width           =   4935
            _ExtentX        =   8705
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_AporteRetiro_Capital.frx":0359
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HFFFFFF&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Cuentas 
            Height          =   315
            Left            =   840
            TabIndex        =   19
            Tag             =   "SOLOLECTURA=N"
            Top             =   840
            Width           =   4935
            _ExtentX        =   8705
            _ExtentY        =   556
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   556
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Consulta_AporteRetiro_Capital.frx":0403
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label lblInstrumento 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Asesor"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   21
            Top             =   240
            Width           =   705
         End
         Begin VB.Label Label1 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Cuentas"
            Height          =   315
            Left            =   120
            TabIndex        =   20
            Top             =   840
            Width           =   705
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Fecha"
         ForeColor       =   &H000000FF&
         Height          =   1335
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   4815
         Begin VB.OptionButton Rdb_Mes 
            Caption         =   "Mes"
            Height          =   255
            Left            =   120
            TabIndex        =   12
            Top             =   720
            Width           =   1275
         End
         Begin VB.OptionButton rdb_RangoFecha 
            Caption         =   "Rango de Fecha"
            Height          =   255
            Left            =   120
            TabIndex        =   11
            Top             =   360
            Width           =   1515
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Inicial 
            Height          =   345
            Left            =   3000
            TabIndex        =   13
            Top             =   360
            Width           =   1695
            _ExtentX        =   2990
            _ExtentY        =   609
            _Version        =   393216
            CustomFormat    =   "MMMM yyyy"
            Format          =   17039361
            CurrentDate     =   39153
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Final 
            Height          =   345
            Left            =   3000
            TabIndex        =   15
            Top             =   720
            Width           =   1695
            _ExtentX        =   2990
            _ExtentY        =   609
            _Version        =   393216
            CustomFormat    =   "MMMM yyyy"
            Format          =   17039363
            CurrentDate     =   39153
         End
         Begin VB.Label Lbl_Fecha_Final 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Final"
            Height          =   345
            Left            =   1800
            TabIndex        =   16
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label Lbl_Fecha_Inicial 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Inicial"
            Height          =   345
            Left            =   1800
            TabIndex        =   14
            Top             =   360
            Width           =   1215
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Tipo Reporte"
         ForeColor       =   &H000000FF&
         Height          =   615
         Left            =   10920
         TabIndex        =   6
         Top             =   960
         Width           =   3375
         Begin VB.OptionButton rdb_Ambos 
            Caption         =   "Ambos"
            Height          =   255
            Left            =   120
            TabIndex        =   9
            Top             =   240
            Width           =   855
         End
         Begin VB.OptionButton rdb_Resumen 
            Caption         =   "Resumen"
            Height          =   255
            Left            =   1080
            TabIndex        =   8
            Top             =   240
            Width           =   1035
         End
         Begin VB.OptionButton rdb_Detalle 
            Caption         =   "Detalle"
            Height          =   255
            Left            =   2280
            TabIndex        =   7
            Top             =   240
            Width           =   1035
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   14625
      _ExtentX        =   25797
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "SEARCH"
            Description     =   "Carga Aportes y Retiros de Capital"
            Object.ToolTipText     =   "Carga Aportes y Retiros de Capital"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORTE"
            Description     =   "Genera reporte de Aportes y Retiros de Capital"
            Object.ToolTipText     =   "Genera reporte de Aportes y Retiros de Capital"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Re&frescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpiar los Controles"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   6960
         TabIndex        =   1
         Top             =   0
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Consulta_AporteRetiro_Capital"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Rem ---------------------------------------------------------------------------
Rem 03/04/2009. Controla fecha de consulta si es el permiso es sololectura
Rem             se considera la fecha de cierre virtual como fecha de consulta
Rem ---------------------------------------------------------------------------

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso   As String 'Flags para el permiqso sobre la ventana
Dim fCod_Arbol_Sistema  As String 'Codigo para el permiso sobre la ventana

'----------------------------------------------------------------------------
Const RColorRelleno = 213   ' R
Const GColorRelleno = 202   ' G
Const BColorRelleno = 195   ' B

Public fApp_Excel           As Excel.Application      ' Excel application
Public fLibro               As Excel.Workbook      ' Excel workbook
Public lForm                   As Frm_Reporte_Generico

Dim sFormato       As String
Dim oCursor_Datos  As hRecord
Dim fFecha_Inicial As Date
Dim fFecha_Final   As Date
Dim sTipoConsulta  As String
Dim sTipoFecha     As String
Public lPeriodo             As String
Public fFechaInicial        As Date
Public fFechaFinal          As Date
Dim lFilaExcel     As Long
Dim iDecimales     As Integer
Dim Iid_moneda As Integer
Dim sTitulo1 As String
Dim lFechaCierreVirtual As Date
Dim bFechaVirtual As Boolean

Public Sub Mostrar(pCod_Arbol_Sistema)
    fCod_Arbol_Sistema = pCod_Arbol_Sistema
'  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
    fFlg_Tipo_Permiso = Fnt_Obtiene_Permiso_Formulario(pCod_Arbol_Sistema)
  
    Call Form_Resize
  
    Load Me
End Sub


Private Sub Cmb_Cuentas_ItemChange()
Dim lId_Cuenta          As String
Dim lFechaCierre        As Date

  lFechaCierre = Fnt_FechaServidor
  If Not Cmb_Cuentas.Text = "Todos" Then
    lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
    If lId_Cuenta <> "" Then
        Call Sub_ComboSelectedItem(Cmb_Cuentas, lId_Cuenta)
        If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
            If Fnt_Obtiene_FechaCierre_Virtual(lId_Cuenta, lFechaCierreVirtual) Then
                bFechaVirtual = True
                If DTP_Fecha_Final.Value > lFechaCierreVirtual Then
                    DTP_Fecha_Final.Value = lFechaCierreVirtual
                    If DTP_Fecha_Final.Value < DTP_Fecha_Inicial.Value Then
                        DTP_Fecha_Inicial.Value = lFechaCierreVirtual
                    End If
                End If
            Else
                bFechaVirtual = False
                DTP_Fecha_Final.Value = lFechaCierre
            End If
        Else
            bFechaVirtual = False
        End If
    End If
  End If
  
End Sub

Private Sub Form_Load()
   
  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEARCH").Image = cBoton_Buscar
    .Buttons("REPORTE").Image = cBoton_Modificar
    .Buttons("REFRESH").Image = cBoton_Original
    .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  Me.Top = 1
  Me.Left = 1
   
  Call Sub_CargaForm
  
End Sub


Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEARCH"
        Call Sub_GeneraConsultaGrilla
    Case "REFRESH"
      Call Sub_Limpiar
    Case "REPORTE"
      Call Sub_GeneraInformePantalla
    Case "EXIT"
      Unload Me
  End Select

End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_GeneraInformePantalla
    Case "EXCEL"
      Call Sub_GeneraInformeExcel
  End Select
End Sub

Private Function ValidaEntradaDatos() As Boolean
Dim bOk As Boolean

    bOk = True
    If Cmb_Monedas.Text = "" Then
        MsgBox "Debe seleccionar Moneda de Salida", vbInformation, Me.Caption
        bOk = False
    End If
    ValidaEntradaDatos = bOk
End Function

Private Sub Form_Resize()
   Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Cmb_Asesor_ItemChange()
Dim sId_Asesor As String

    If Not Cmb_Asesor.Text = "Todos" Then
        sId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
        Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, pTodos:=True, pId_Asesor:=sId_Asesor)
        Call Sub_ComboSelectedItem(Cmb_Cuentas, cCmbKALL)
    End If
End Sub
Private Sub rdb_Ambos_Click()
    SST_AportesRetiros.TabEnabled(0) = True
    SST_AportesRetiros.TabEnabled(1) = True
    Grilla_Detalle.Rows = 1
    Grilla_Resumen.Rows = 1
    
    SST_AportesRetiros.Tab = 0
End Sub

Private Sub rdb_Resumen_Click()
    SST_AportesRetiros.TabEnabled(0) = True
    SST_AportesRetiros.TabEnabled(1) = True
    Grilla_Detalle.Rows = 1
    Grilla_Resumen.Rows = 1
    
    SST_AportesRetiros.TabEnabled(0) = False
    SST_AportesRetiros.TabEnabled(1) = True
    
    SST_AportesRetiros.Tab = 1
End Sub

Private Sub rdb_Detalle_Click()
    SST_AportesRetiros.TabEnabled(0) = True
    SST_AportesRetiros.TabEnabled(1) = True
    Grilla_Detalle.Rows = 1
    Grilla_Resumen.Rows = 1
    
    SST_AportesRetiros.TabEnabled(0) = True
    SST_AportesRetiros.TabEnabled(1) = False
    
    SST_AportesRetiros.Tab = 0
    
End Sub

Private Sub Rdb_Mes_Click()
   DTP_Fecha_Inicial.Format = dtpCustom
   DTP_Fecha_Inicial.CustomFormat = "MMMM yyyy"
   Lbl_Fecha_Inicial.Caption = "Mes Inicial"
   
   DTP_Fecha_Final.Format = dtpCustom
   DTP_Fecha_Final.CustomFormat = "MMMM yyyy"
   Lbl_Fecha_Final.Caption = "Mes Final"
   
   Call Sub_Fechas
   
End Sub

Private Sub rdb_RangoFecha_Click()
   DTP_Fecha_Inicial.Format = dtpShortDate
   Lbl_Fecha_Inicial.Caption = "Fecha Inicial "
   DTP_Fecha_Final.Format = dtpShortDate
   Lbl_Fecha_Final.Caption = "Fecha Final "
   Call Sub_Fechas
End Sub

Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_FormControl_Color(Me.Controls)
    
    Call Sub_CargaCombo_Asesor(Cmb_Asesor, pTodos:=True)
    Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
    
    Call Sub_CargaCombo_Monedas(Cmb_Monedas)
    Call Sub_ComboSelectedItem(Cmb_Monedas, 1)
    
    Cmb_Cuentas.Text = "Todos"
    
    
    Grilla_Detalle.GridLines = flexGridNone
    Grilla_Detalle.Rows = 1
    
    Grilla_Resumen.GridLines = flexGridNone
    Grilla_Resumen.Rows = 1
    
    rdb_Ambos.Value = True
    
    rdb_RangoFecha.Value = True
    Grilla_Resumen.ColHidden(0) = True
    
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Inicial.Value = lCierre.Busca_Ultima_FechaCierre
    DTP_Fecha_Final.Value = Fnt_FechaServidor
    Set lCierre = Nothing
    
    bFechaVirtual = False
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Limpiar()
Dim lCierre As Class_Verificaciones_Cierre
Dim i As Integer

    Cmb_Cuentas.Text = "Todos"
    Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
    Call Sub_ComboSelectedItem(Cmb_Monedas, 1)
    
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Inicial.Value = lCierre.Busca_Ultima_FechaCierre
    DTP_Fecha_Final.Value = Fnt_FechaServidor
    Grilla_Detalle.Rows = 1
    Grilla_Resumen.Rows = 1
    
    rdb_Ambos.Value = True
    rdb_RangoFecha.Value = True
    Grilla_Resumen.ColHidden(0) = True
    
    
End Sub

Private Function Fnt_CargarDatos(pTipoConsulta As String) As Boolean
Dim lId_Asesor As String
Dim lId_Cuenta As String
Dim bCargaExitosa  As Boolean

    
    bCargaExitosa = False
    
    lId_Asesor = IIf(Cmb_Asesor.Text = "Todos", 0, Fnt_ComboSelected_KEY(Cmb_Asesor))
    If lId_Asesor = "" Then
        Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
       lId_Asesor = 0
    End If
    lId_Cuenta = IIf(Cmb_Cuentas.Text = "Todos", 0, Fnt_ComboSelected_KEY(Cmb_Cuentas))
    If lId_Cuenta = "" Then
        Call Sub_ComboSelectedItem(Cmb_Cuentas, cCmbKALL)
       lId_Cuenta = 0
    End If
    Iid_moneda = Fnt_ComboSelected_KEY(Cmb_Monedas)
    iDecimales = TraeDecimalesCuenta(Iid_moneda)
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_APORTE_RESCATE_CTA$BuscarAportesRetirosCapital"
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    gDB.Parametros.Add "pFechaini", ePT_Fecha, fFechaInicial, ePD_Entrada
    gDB.Parametros.Add "pFechafin", ePT_Fecha, fFechaFinal, ePD_Entrada
    gDB.Parametros.Add "pId_Moneda_Salida", ePT_Numero, Iid_moneda, ePD_Entrada
    gDB.Parametros.Add "pTipoConsulta", ePT_Caracter, pTipoConsulta, ePD_Entrada
    
    Rem CSM 03/06/2009 agregue esto a la rapida, despues se puede cambiar a algo mas sofisticado
    If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
      gDB.Parametros.Add "pSoloLectura", ePT_Caracter, gcFlg_SI, ePD_Entrada
    Else
      gDB.Parametros.Add "pSoloLectura", ePT_Caracter, gcFlg_NO, ePD_Entrada
    End If
    
    
    If lId_Asesor > 0 Then
       gDB.Parametros.Add "pId_Asesor", ePT_Numero, lId_Asesor, ePD_Entrada
    End If
    
    If lId_Cuenta > 0 Then
       gDB.Parametros.Add "pId_cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    End If
    
    If gDB.EjecutaSP Then
        Set oCursor_Datos = gDB.Parametros("Pcursor").Valor
        If oCursor_Datos.Count > 0 Then
            bCargaExitosa = True
        Else
            MsgBox "No se registran Aportes y Retiros en este periodo", vbInformation, Me.Caption
        End If
    Else
        MsgBox "Error al cargar datos", vbInformation, Me.Caption
    End If

ExitProcedure:
    Fnt_CargarDatos = bCargaExitosa
End Function

Private Sub Sub_GeneraConsultaGrilla()
Dim bCargarDatos As Boolean
    Call Sub_Bloquea_Puntero(Me)
    If Not ValidaEntradaDatos Then
       GoTo ExitProcedure
    End If

    sTipoConsulta = Fnt_ObtieneTipoConsulta

    sTipoFecha = Fnt_ObtieneFiltroFecha
    
    
    Select Case sTipoConsulta
        Case "A"        'Ambos
            bCargarDatos = Fnt_CargarDatos("D")
            If bCargarDatos Then
               Sub_CargaGrilla_Detalle
            End If
            bCargarDatos = Fnt_CargarDatos(sTipoFecha)
            If bCargarDatos Then
               Sub_CargaGrilla_Resumen
            End If
        Case "D"        'Detalle
            bCargarDatos = Fnt_CargarDatos(sTipoConsulta)
            If bCargarDatos Then
                Sub_CargaGrilla_Detalle
            End If
        Case "R"        'Resumen
            bCargarDatos = Fnt_CargarDatos(sTipoFecha)
            If bCargarDatos Then
                Sub_CargaGrilla_Resumen
            End If
    End Select
ExitProcedure:
    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Sub_CargaGrilla_Detalle()
Dim lReg       As Object
Dim lFila      As Long
Dim dTotalAporte As Double
Dim dTotalRetiro As Double
Dim dTotalNeto        As Double

    dTotalAporte = 0
    dTotalRetiro = 0
    dTotalNeto = 0
    
    Grilla_Detalle.Rows = 1
    For Each lReg In oCursor_Datos
        lFila = Grilla_Detalle.Rows
        Call Grilla_Detalle.AddItem("")
        Call SetCell(Grilla_Detalle, lFila, "colum_pk", NVL(lReg("id_cuenta").Value, ""), pAutoSize:=True)
        Call SetCell(Grilla_Detalle, lFila, "fecha_movimiento", NVL(lReg("fecha_movimiento").Value, ""), pAutoSize:=True)
        Call SetCell(Grilla_Detalle, lFila, "dsc_cuenta", NVL(lReg("num_cuenta").Value, ""), pAutoSize:=True)
        Call SetCell(Grilla_Detalle, lFila, "rut_cliente", NVL(lReg("rut_cliente").Value, ""), pAutoSize:=True)
        Call SetCell(Grilla_Detalle, lFila, "nombre_cliente", NVL(lReg("nombre_cliente").Value, ""), pAutoSize:=True)
        Call SetCell(Grilla_Detalle, lFila, "nombre_asesor", NVL(lReg("nombre_asesor").Value, ""), pAutoSize:=True)
        Call SetCell(Grilla_Detalle, lFila, "monto_aporte", FormatNumber(NVL(lReg("monto_aporte").Value, 0), iDecimales), pAutoSize:=True)
        Call SetCell(Grilla_Detalle, lFila, "monto_retiro", FormatNumber(NVL(lReg("monto_retiro").Value, 0), iDecimales), pAutoSize:=True)
        Call SetCell(Grilla_Detalle, lFila, "observacion", NVL(lReg("observacion").Value, ""), pAutoSize:=True)
        If lReg("flg_primeraporte").Value = 1 Then
            Grilla_Detalle.Cell(flexcpFontBold, lFila, 1, lFila, 9) = True
            Grilla_Detalle.Cell(flexcpChecked, lFila, 1) = flexChecked
            Grilla_Detalle.Cell(flexcpForeColor, lFila, 1, lFila, 9) = vbBlue
        End If
        dTotalAporte = dTotalAporte + CDbl(NVL(lReg("monto_aporte").Value, 0))
        dTotalRetiro = dTotalRetiro + CDbl(NVL(lReg("monto_retiro").Value, 0))
    Next
    dTotalNeto = dTotalAporte - dTotalRetiro
    lFila = Grilla_Detalle.Rows
    Call Grilla_Detalle.AddItem("")
    Call SetCell(Grilla_Detalle, lFila, "nombre_asesor", "Total", pAutoSize:=True)
    Call SetCell(Grilla_Detalle, lFila, "monto_aporte", FormatNumber(dTotalAporte, iDecimales), pAutoSize:=True)
    Call SetCell(Grilla_Detalle, lFila, "monto_retiro", FormatNumber(dTotalRetiro, iDecimales), pAutoSize:=True)
    Grilla_Detalle.Cell(flexcpFontBold, lFila, 6, lFila, 8) = True
    
    lFila = Grilla_Detalle.Rows
    Call Grilla_Detalle.AddItem("")
    Call SetCell(Grilla_Detalle, lFila, "nombre_asesor", "Monto Neto", pAutoSize:=True)
    If dTotalNeto > 0 Then
        Call SetCell(Grilla_Detalle, lFila, "monto_aporte", FormatNumber(dTotalNeto, iDecimales), pAutoSize:=True)
    Else
        Call SetCell(Grilla_Detalle, lFila, "monto_retiro", FormatNumber(dTotalNeto, iDecimales), pAutoSize:=True)
    End If
    Grilla_Detalle.Cell(flexcpFontBold, lFila, 6, lFila, 8) = True
    
End Sub
Private Sub Sub_CargaGrilla_Resumen()
Dim lReg            As Object
Dim lFila           As Long
Dim iContadorMes    As Integer
Dim iMes            As Integer
Dim iMesAnterior    As Integer
Dim sMes            As String
Dim dTotalAporte    As Double
Dim dTotalRetiro    As Double
Dim dTotalNeto      As Double
Dim dParcialAporte  As Double
Dim dParcialRetiro  As Double
Dim dParcialNeto    As Double

    dTotalAporte = 0
    dTotalRetiro = 0
    dTotalNeto = 0
    iMesAnterior = 0
    
    Grilla_Resumen.Rows = 1
    
    If sTipoFecha = "M" Then
        iContadorMes = 0
        For Each lReg In oCursor_Datos
            iMes = lReg("mes").Value
            
            If iMesAnterior <> 0 And iMes <> iMesAnterior Then
                lFila = Grilla_Resumen.Rows
                Call Grilla_Resumen.AddItem("")
                Call SetCell(Grilla_Resumen, lFila, "nombre_asesor", "Total Mensual", pAutoSize:=True)
                Call SetCell(Grilla_Resumen, lFila, "monto_aporte", FormatNumber(dParcialAporte, iDecimales), pAutoSize:=False)
                Call SetCell(Grilla_Resumen, lFila, "monto_retiro", FormatNumber(dParcialRetiro, iDecimales), pAutoSize:=False)
                Call SetCell(Grilla_Resumen, lFila, "monto_neto", FormatNumber(dParcialNeto, iDecimales), pAutoSize:=False)
                Grilla_Resumen.Cell(flexcpFontBold, lFila, 1) = True
                Grilla_Resumen.Cell(flexcpFontBold, lFila, 2) = True
                Grilla_Resumen.Cell(flexcpFontBold, lFila, 3) = True
                Grilla_Resumen.Cell(flexcpFontBold, lFila, 4) = True
                
                iContadorMes = iContadorMes + 1
                
                dTotalAporte = dTotalAporte + dParcialAporte
                dTotalRetiro = dTotalRetiro + dParcialRetiro
                dTotalNeto = dTotalNeto + dParcialNeto
                
                dParcialAporte = 0
                dParcialRetiro = 0
                dParcialNeto = 0
            End If
            
            lFila = Grilla_Resumen.Rows
            sMes = Fnt_ObtieneNombreMes(lReg("mes").Value)
            Call Grilla_Resumen.AddItem("")
            Call SetCell(Grilla_Resumen, lFila, "mes", sMes, pAutoSize:=True)
            Call SetCell(Grilla_Resumen, lFila, "nombre_asesor", NVL(lReg("nombre_asesor").Value, ""), pAutoSize:=False)
            Call SetCell(Grilla_Resumen, lFila, "monto_aporte", FormatNumber(NVL(lReg("monto_aporte").Value, 0), iDecimales), pAutoSize:=False)
            Call SetCell(Grilla_Resumen, lFila, "monto_retiro", FormatNumber(NVL(lReg("monto_retiro").Value, 0), iDecimales), pAutoSize:=False)
            Call SetCell(Grilla_Resumen, lFila, "monto_neto", FormatNumber(NVL(lReg("monto_neto").Value, 0), iDecimales), pAutoSize:=False)
            dParcialAporte = dParcialAporte + CDbl(NVL(lReg("monto_aporte").Value, 0))
            dParcialRetiro = dParcialRetiro + CDbl(NVL(lReg("monto_retiro").Value, 0))
            dParcialNeto = dParcialNeto + CDbl(NVL(lReg("monto_neto").Value, 0))
            iMesAnterior = lReg("mes").Value
        Next
        iContadorMes = iContadorMes + 1
        If iContadorMes > 1 Then
            lFila = Grilla_Resumen.Rows
            Call Grilla_Resumen.AddItem("")
            Call SetCell(Grilla_Resumen, lFila, "nombre_asesor", "Total Mensual", pAutoSize:=True)
            Call SetCell(Grilla_Resumen, lFila, "monto_aporte", FormatNumber(dParcialAporte, iDecimales), pAutoSize:=False)
            Call SetCell(Grilla_Resumen, lFila, "monto_retiro", FormatNumber(dParcialRetiro, iDecimales), pAutoSize:=False)
            Call SetCell(Grilla_Resumen, lFila, "monto_neto", FormatNumber(dParcialNeto, iDecimales), pAutoSize:=False)
            Grilla_Resumen.Cell(flexcpFontBold, lFila, 1) = True
            Grilla_Resumen.Cell(flexcpFontBold, lFila, 2) = True
            Grilla_Resumen.Cell(flexcpFontBold, lFila, 3) = True
            Grilla_Resumen.Cell(flexcpFontBold, lFila, 4) = True
        End If
        dTotalAporte = dTotalAporte + dParcialAporte
        dTotalRetiro = dTotalRetiro + dParcialRetiro
        dTotalNeto = dTotalNeto + dParcialNeto
    
        lFila = Grilla_Resumen.Rows
        Call Grilla_Resumen.AddItem("")
        Call SetCell(Grilla_Resumen, lFila, "nombre_asesor", "Total", pAutoSize:=True)
        Call SetCell(Grilla_Resumen, lFila, "monto_aporte", FormatNumber(dTotalAporte, iDecimales), pAutoSize:=False)
        Call SetCell(Grilla_Resumen, lFila, "monto_retiro", FormatNumber(dTotalRetiro, iDecimales), pAutoSize:=False)
        Call SetCell(Grilla_Resumen, lFila, "monto_neto", FormatNumber(dTotalNeto, iDecimales), pAutoSize:=False)
        Grilla_Resumen.Cell(flexcpFontBold, lFila, 1) = True
        Grilla_Resumen.Cell(flexcpFontBold, lFila, 2) = True
        Grilla_Resumen.Cell(flexcpFontBold, lFila, 3) = True
        Grilla_Resumen.Cell(flexcpFontBold, lFila, 4) = True
        
        Grilla_Resumen.ColHidden(0) = False
    Else
        For Each lReg In oCursor_Datos
            lFila = Grilla_Resumen.Rows
            Call Grilla_Resumen.AddItem("")
            Call SetCell(Grilla_Resumen, lFila, "nombre_asesor", NVL(lReg("nombre_asesor").Value, ""), pAutoSize:=False)
            Call SetCell(Grilla_Resumen, lFila, "monto_aporte", FormatNumber(NVL(lReg("monto_aporte").Value, 0), iDecimales), pAutoSize:=False)
            Call SetCell(Grilla_Resumen, lFila, "monto_retiro", FormatNumber(NVL(lReg("monto_retiro").Value, 0), iDecimales), pAutoSize:=False)
            Call SetCell(Grilla_Resumen, lFila, "monto_neto", FormatNumber(NVL(lReg("monto_neto").Value, 0), iDecimales), pAutoSize:=False)
            dTotalAporte = dTotalAporte + CDbl(NVL(lReg("monto_aporte").Value, 0))
            dTotalRetiro = dTotalRetiro + CDbl(NVL(lReg("monto_retiro").Value, 0))
            dTotalNeto = dTotalNeto + CDbl(NVL(lReg("monto_neto").Value, 0))
        Next
        lFila = Grilla_Resumen.Rows
        Call Grilla_Resumen.AddItem("")
        Call SetCell(Grilla_Resumen, lFila, "nombre_asesor", "Total", pAutoSize:=True)
        Call SetCell(Grilla_Resumen, lFila, "monto_aporte", FormatNumber(dTotalAporte, iDecimales), pAutoSize:=False)
        Call SetCell(Grilla_Resumen, lFila, "monto_retiro", FormatNumber(dTotalRetiro, iDecimales), pAutoSize:=False)
        Call SetCell(Grilla_Resumen, lFila, "monto_neto", FormatNumber(dTotalNeto, iDecimales), pAutoSize:=False)
        Grilla_Resumen.Cell(flexcpFontBold, lFila, 1) = True
        Grilla_Resumen.Cell(flexcpFontBold, lFila, 2) = True
        Grilla_Resumen.Cell(flexcpFontBold, lFila, 3) = True
        Grilla_Resumen.Cell(flexcpFontBold, lFila, 4) = True
        
        Grilla_Resumen.ColHidden(0) = True
    End If
    


End Sub

Private Sub Sub_GeneraInformeExcel()
Dim bCargarDatos As Boolean
    
    Call Sub_Bloquea_Puntero(Me)
    If Not ValidaEntradaDatos Then
       GoTo ExitProcedure
    End If

    sTipoConsulta = Fnt_ObtieneTipoConsulta

    sTipoFecha = Fnt_ObtieneFiltroFecha
    
    Sub_CreaExcel
    Select Case sTipoConsulta
        Case "A"        'Ambos
            bCargarDatos = Fnt_CargarDatos("D")
            If bCargarDatos Then
               Sub_CargaExcel_Detalle
            End If
            bCargarDatos = Fnt_CargarDatos(sTipoFecha)
            If bCargarDatos Then
               Sub_CargaExcel_Resumen
            End If
        Case "D"        'Detalle
            bCargarDatos = Fnt_CargarDatos(sTipoConsulta)
            If bCargarDatos Then
                Sub_CargaExcel_Detalle
            End If
        Case "R"        'Resumen
            bCargarDatos = Fnt_CargarDatos(sTipoFecha)
            If bCargarDatos Then
                Sub_CargaExcel_Resumen
            End If
    End Select
       
    'If bCargarDatos Then
        fApp_Excel.Visible = True
        fApp_Excel.UserControl = True
    'End If
ExitProcedure:
    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Sub_CreaExcel()
Dim hoja As Integer
Dim Index As Integer
Dim i     As Integer
Dim iHoja As Integer

    Call Sub_Bloquea_Puntero(Me)
    If sTipoConsulta = "A" Then
        iHoja = 2
    Else
        iHoja = 1
    End If
    
    Set fApp_Excel = CreateObject("Excel.application")
    fApp_Excel.DisplayAlerts = False
    Set fLibro = fApp_Excel.Workbooks.Add
    
    fApp_Excel.ActiveWindow.DisplayGridlines = False
    
    fLibro.Worksheets.Add
    
    With fLibro
        For i = .Worksheets.Count To iHoja + 1 Step -1
            .Worksheets(i).Delete
        Next i
        
        For i = 1 To iHoja
            .Sheets(i).Select
'            .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).Select
            .ActiveSheet.Range("A5:E6").Font.Bold = True
            .ActiveSheet.Range("A5:E6").HorizontalAlignment = xlLeft
        Next
        Select Case sTipoConsulta
            Case "A"
                .Sheets(1).Select
                .ActiveSheet.Range("A5").Value = "Detalle Aportes y Retiros de Capital"
                .ActiveSheet.Range("A6").Value = "Periodo : " & lPeriodo
                .Worksheets.Item(1).Name = "Detalle"
                
                .Sheets(2).Select
                .ActiveSheet.Range("A5").Value = "Resumen Aportes y Retiros de Capital"
                .ActiveSheet.Range("A6").Value = "Periodo : " & lPeriodo
                .Worksheets.Item(2).Name = "Resumen"
            Case "D"
                .Sheets(1).Select
                .ActiveSheet.Range("A5").Value = "Detalle Aportes y Retiros de Capital"
                .ActiveSheet.Range("A6").Value = "Periodo : " & lPeriodo
                .Worksheets.Item(1).Name = "Detalle"
            Case "R"
                .Sheets(1).Select
                .ActiveSheet.Range("A5").Value = "Resumen Aportes y Retiros de Capital"
                .ActiveSheet.Range("A6").Value = "Periodo : " & lPeriodo
                .Worksheets.Item(1).Name = "Resumen"
        End Select
        
            
    End With
    
End Sub

Private Sub Sub_CargaExcel_Detalle()
Dim lReg       As Object
Dim iCol       As Integer
Dim lxHoja     As Worksheet
Dim dTotalAporte As Double
Dim dTotalRetiro As Double
Dim dTotalNeto        As Double

    dTotalAporte = 0
    dTotalRetiro = 0
    dTotalNeto = 0

    lFilaExcel = 7
    Sub_ExcelEncabezadoDetalle
    Set lxHoja = fLibro.Sheets(1)
    Call lxHoja.Select
    With lxHoja
        For Each lReg In oCursor_Datos
            lFilaExcel = lFilaExcel + 1
            iCol = 1
            If lReg("flg_primeraporte").Value = 1 Then
                .Cells(lFilaExcel, iCol) = "Apo.Inicial"
                .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 8)).BorderAround
                .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, 8)).Font.Bold = True
            End If
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = lReg("fecha_movimiento").Value
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = NVL(lReg("num_cuenta").Value, "")
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = NVL(lReg("rut_cliente").Value, "")
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = NVL(lReg("nombre_cliente").Value, "")
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = NVL(lReg("nombre_asesor").Value, "")
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = NVL(lReg("monto_aporte").Value, 0)
            .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = NVL(lReg("monto_retiro").Value, 0)
            .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = lReg("observacion").Value
            dTotalAporte = dTotalAporte + CDbl(NVL(lReg("monto_aporte").Value, 0))
            dTotalRetiro = dTotalRetiro + CDbl(NVL(lReg("monto_retiro").Value, 0))
            
        Next
        dTotalNeto = dTotalAporte - dTotalRetiro
        
        lFilaExcel = lFilaExcel + 1
        iCol = 6
        .Cells(lFilaExcel, iCol) = "Total"
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol) = dTotalAporte
        .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol) = dTotalRetiro
        .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
        
        lFilaExcel = lFilaExcel + 1
        iCol = 6
        .Cells(lFilaExcel, iCol) = "Total Neto"
        If dTotalNeto > 0 Then
            iCol = iCol + 1
        Else
            iCol = iCol + 2
        End If
        .Cells(lFilaExcel, iCol) = dTotalNeto
        .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
        
        .Range(.Cells(lFilaExcel - 1, 1), .Cells(lFilaExcel, 9)).BorderAround
        .Range(.Cells(lFilaExcel - 1, 1), .Cells(lFilaExcel, 9)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(lFilaExcel - 1, 1), .Cells(lFilaExcel, 9)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
        .Range(.Cells(lFilaExcel - 1, 1), .Cells(lFilaExcel, 9)).Font.Bold = True
        .Range(.Cells(lFilaExcel - 1, 1), .Cells(lFilaExcel, 9)).WrapText = True
        .Range(.Cells(lFilaExcel - 1, 1), .Cells(lFilaExcel, 9)).HorizontalAlignment = xlRight

    End With

End Sub

Private Sub Sub_CargaExcel_Resumen()
Dim lReg            As Object
Dim iCol            As Integer
Dim iMes            As Integer
Dim iMesAnterior    As Integer
Dim iContadorMes    As Integer
Dim sMes            As String
Dim lxHoja          As Worksheet
Dim dTotalAporte    As Double
Dim dTotalRetiro    As Double
Dim dTotalNeto      As Double
Dim dParcialAporte  As Double
Dim dParcialRetiro  As Double
Dim dParcialNeto    As Double

    dTotalAporte = 0
    dTotalRetiro = 0
    dTotalNeto = 0
    
    lFilaExcel = 7
    Sub_ExcelEncabezadoResumen
    iCol = 1
    If sTipoConsulta = "A" Then
        Set lxHoja = fLibro.Sheets(2)
    Else
        Set lxHoja = fLibro.Sheets(1)
    End If
    Call lxHoja.Select

    If sTipoFecha = "M" Then
        iContadorMes = 0

        With lxHoja
            For Each lReg In oCursor_Datos
                iMes = lReg("mes").Value
                If iMesAnterior <> 0 And iMes <> iMesAnterior Then
                    lFilaExcel = lFilaExcel + 1
                    iCol = 2
                    .Cells(lFilaExcel, iCol) = "Total Mensual"
                    iCol = iCol + 1
                    .Cells(lFilaExcel, iCol) = dParcialAporte
                    .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                    iCol = iCol + 1
                    .Cells(lFilaExcel, iCol) = dParcialRetiro
                    .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                    iCol = iCol + 1
                    .Cells(lFilaExcel, iCol) = dParcialNeto
                    .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                    
                    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).BorderAround
                    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Borders.Color = RGB(0, 0, 0)
                    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
                    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Font.Bold = True
                    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).WrapText = True
                    .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).HorizontalAlignment = xlCenter
                    
                    iContadorMes = iContadorMes + 1
                    
                    dTotalAporte = dTotalAporte + dParcialAporte
                    dTotalRetiro = dTotalRetiro + dParcialRetiro
                    dTotalNeto = dTotalNeto + dParcialNeto

                    dParcialAporte = 0
                    dParcialRetiro = 0
                    dParcialNeto = 0
                End If
                    
                lFilaExcel = lFilaExcel + 1
                iCol = 1
                sMes = Fnt_ObtieneNombreMes(lReg("mes").Value)
                .Cells(lFilaExcel, iCol) = sMes
                iCol = iCol + 1
                .Cells(lFilaExcel, iCol) = NVL(lReg("nombre_asesor").Value, "")
                iCol = iCol + 1
                .Cells(lFilaExcel, iCol) = NVL(lReg("monto_aporte").Value, 0)
                .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                iCol = iCol + 1
                .Cells(lFilaExcel, iCol) = NVL(lReg("monto_retiro").Value, 0)
                .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                iCol = iCol + 1
                .Cells(lFilaExcel, iCol) = NVL(lReg("monto_neto").Value, 0)
                .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                
                dParcialAporte = dParcialAporte + CDbl(NVL(lReg("monto_aporte").Value, 0))
                dParcialRetiro = dParcialRetiro + CDbl(NVL(lReg("monto_retiro").Value, 0))
                dParcialNeto = dParcialNeto + CDbl(NVL(lReg("monto_neto").Value, 0))
                
                iMesAnterior = lReg("mes").Value
                
            Next
            iContadorMes = iContadorMes + 1
            
            If iContadorMes > 1 Then
                lFilaExcel = lFilaExcel + 1
                iCol = 2
                .Cells(lFilaExcel, iCol) = "Total Mensual"
                iCol = iCol + 1
                .Cells(lFilaExcel, iCol) = dParcialAporte
                .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                iCol = iCol + 1
                .Cells(lFilaExcel, iCol) = dParcialRetiro
                .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                iCol = iCol + 1
                .Cells(lFilaExcel, iCol) = dParcialNeto
                .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                
                .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).BorderAround
                .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Borders.Color = RGB(0, 0, 0)
                .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
                .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Font.Bold = True
                .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).WrapText = True
                .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).HorizontalAlignment = xlCenter
            End If
            
            dTotalAporte = dTotalAporte + dParcialAporte
            dTotalRetiro = dTotalRetiro + dParcialRetiro
            dTotalNeto = dTotalNeto + dParcialNeto
            
            lFilaExcel = lFilaExcel + 1
            iCol = 2
            .Cells(lFilaExcel, iCol) = "Total"
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = dTotalAporte
            .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = dTotalRetiro
            .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = dTotalNeto
            .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
            
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).BorderAround
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Font.Bold = True
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).WrapText = True
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).HorizontalAlignment = xlCenter
    
        End With
    Else
        With lxHoja
            For Each lReg In oCursor_Datos
                lFilaExcel = lFilaExcel + 1
                iCol = 1
                .Cells(lFilaExcel, iCol) = lReg("nombre_asesor").Value
                iCol = iCol + 1
                .Cells(lFilaExcel, iCol) = NVL(lReg("monto_aporte").Value, 0)
                .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                iCol = iCol + 1
                .Cells(lFilaExcel, iCol) = NVL(lReg("monto_retiro").Value, 0)
                .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                iCol = iCol + 1
                .Cells(lFilaExcel, iCol) = NVL(lReg("monto_neto").Value, 0)
                .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
                
                dTotalAporte = dTotalAporte + CDbl(NVL(lReg("monto_aporte").Value, 0))
                dTotalRetiro = dTotalRetiro + CDbl(NVL(lReg("monto_retiro").Value, 0))
                dTotalNeto = dTotalNeto + CDbl(NVL(lReg("monto_neto").Value, 0))
            Next
            lFilaExcel = lFilaExcel + 1
            iCol = 1
            .Cells(lFilaExcel, iCol) = "Total"
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = dTotalAporte
            .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = dTotalRetiro
            .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
            iCol = iCol + 1
            .Cells(lFilaExcel, iCol) = dTotalNeto
            .Cells(lFilaExcel, iCol).NumberFormat = Fnt_Formato_Moneda(Iid_moneda)
            
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).BorderAround
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Font.Bold = True
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).WrapText = True
            .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).HorizontalAlignment = xlCenter
    
        End With
    
    End If
End Sub



Private Sub Sub_ExcelEncabezadoDetalle()
Dim lxHoja     As Worksheet
Dim iCol       As Integer

    Set lxHoja = fLibro.Sheets(1)
    Call lxHoja.Select

    'Set lxHoja = fLibro.ActiveSheet
    lFilaExcel = lFilaExcel + 1
    iCol = 1
    With lxHoja
        .Cells(lFilaExcel, iCol).Value = ""
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol).Value = "Fecha Movimiento"
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol).Value = "Cuenta"
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol).Value = "Rut Cliente"
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol).Value = "Nombre Cliente"
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol).Value = "Asesor"
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol).Value = "Aporte"
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol).Value = "Retiro"
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol).Value = "Observaci�n"
        
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).BorderAround
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Font.Bold = True
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).WrapText = True
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).HorizontalAlignment = xlCenter

    End With
    
    With fLibro
        .Sheets(1).Select
        .Worksheets.Item(1).Columns("A:A").ColumnWidth = 11
        .Worksheets.Item(1).Columns("B:B").ColumnWidth = 11
        .Worksheets.Item(1).Columns("C:C").ColumnWidth = 11
        .Worksheets.Item(1).Columns("D:D").ColumnWidth = 14
        .Worksheets.Item(1).Columns("E:E").ColumnWidth = 50
        .Worksheets.Item(1).Columns("F:F").ColumnWidth = 50
        .Worksheets.Item(1).Columns("G:G").ColumnWidth = 20
        .Worksheets.Item(1).Columns("H:H").ColumnWidth = 20
        .Worksheets.Item(1).Columns("I:I").ColumnWidth = 70
    End With
End Sub

Private Sub Sub_ExcelEncabezadoResumen()
Dim lxHoja     As Worksheet
Dim iCol       As Integer
Dim iHoja      As Integer

    If sTipoConsulta = "A" Then
        iHoja = 2
    Else
        iHoja = 1
    End If
    
    Set lxHoja = fLibro.Sheets(iHoja)
    Call lxHoja.Select
    
    lFilaExcel = lFilaExcel + 1
    iCol = 1
    With lxHoja
        If sTipoFecha = "M" Then
            .Cells(lFilaExcel, iCol).Value = "Mes"
            iCol = iCol + 1
        End If
        .Cells(lFilaExcel, iCol).Value = "Asesor"
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol).Value = "Aporte"
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol).Value = "Retiro"
        iCol = iCol + 1
        .Cells(lFilaExcel, iCol).Value = "Neto"
        
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).BorderAround
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).Font.Bold = True
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).WrapText = True
        .Range(.Cells(lFilaExcel, 1), .Cells(lFilaExcel, iCol)).HorizontalAlignment = xlCenter

    End With
    With fLibro
        .Sheets(iHoja).Select
        If sTipoFecha = "M" Then
            .Worksheets.Item(iHoja).Columns("A:A").ColumnWidth = 20
            .Worksheets.Item(iHoja).Columns("B:B").ColumnWidth = 50
            .Worksheets.Item(iHoja).Columns("C:C").ColumnWidth = 20
            .Worksheets.Item(iHoja).Columns("D:D").ColumnWidth = 20
            .Worksheets.Item(iHoja).Columns("E:E").ColumnWidth = 20
        Else
            .Worksheets.Item(iHoja).Columns("A:A").ColumnWidth = 50
            .Worksheets.Item(iHoja).Columns("B:B").ColumnWidth = 20
            .Worksheets.Item(iHoja).Columns("C:C").ColumnWidth = 20
            .Worksheets.Item(iHoja).Columns("D:D").ColumnWidth = 20
        End If
    End With
    
End Sub

Private Sub Sub_GeneraInformePantalla()
Dim bCargarDatos As Boolean

    Call Sub_Bloquea_Puntero(Me)
    
    If Not ValidaEntradaDatos Then
       GoTo ExitProcedure
    End If
    
    sTipoConsulta = Fnt_ObtieneTipoConsulta

    sTipoFecha = Fnt_ObtieneFiltroFecha
    
    Select Case sTipoConsulta
        Case "A"
            sTitulo1 = "Detalle Aportes y Retiros de Capital"
        Case "D"
            sTitulo1 = "Detalle Aportes y Retiros de Capital"
        Case "R"
            sTitulo1 = "Resumen Aportes y Retiros de Capital"
    End Select

    Set lForm = New Frm_Reporte_Generico
    Call lForm.Sub_InicarDocumento(pTitulo:=sTitulo1 _
                                   , pTipoSalida:=ePrinter.eP_Pantalla _
                                   , pOrientacion:=orLandscape)


    Select Case sTipoConsulta
        Case "A"        'Ambos
            bCargarDatos = Fnt_CargarDatos("D")
            If bCargarDatos Then
               Sub_CargaInforme_Detalle
            End If
            bCargarDatos = Fnt_CargarDatos(sTipoFecha)
            If bCargarDatos Then
               lForm.VsPrinter.NewPage
               lForm.VsPrinter.FontSize = 16
               lForm.VsPrinter.FontBold = True
               lForm.VsPrinter.Paragraph = sTitulo1
               Sub_CargaInforme_Resumen
            End If
        Case "D"        'Detalle
            bCargarDatos = Fnt_CargarDatos(sTipoConsulta)
            If bCargarDatos Then
                Sub_CargaInforme_Detalle
            End If
        Case "R"        'Resumen
            bCargarDatos = Fnt_CargarDatos(sTipoFecha)
            If bCargarDatos Then
                Sub_CargaInforme_Resumen
            End If
    End Select
    
    lForm.VsPrinter.EndDoc
    
ExitProcedure:
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargaInforme_Detalle()
'------------------------------------------
Const clrHeader = &HD0D0D0
Const sHeader_Det = "|Fecha Movimiento|Cuenta|Rut|Nombre Cliente|Asesor|Aporte|Retiro|Observaci�n"
Const sFormat_Det = "800|900|1000|>1300|3000|2000|>1100|>1100|3000"
'------------------------------------------
Dim sRecord
Dim bAppend
'------------------------------------------
Dim lReg              As Object
Dim sTitulo2          As String
Dim dTotalAporte      As Double
Dim dTotalRetiro      As Double
Dim dTotalNeto        As Double
Dim sPrimerAporte     As String
    dTotalAporte = 0
    dTotalRetiro = 0
    dTotalNeto = 0

    sTitulo2 = "Periodo : " & lPeriodo
    
    With lForm.VsPrinter
        .FontSize = 8
        .FontBold = True
        .Paragraph = sTitulo2
        .Paragraph = "" 'salto de linea
        .FontBold = False
        .StartTable
        .FontSize = 7
        .TableCell(tcAlignCurrency) = False

        For Each lReg In oCursor_Datos
            If lReg("flg_primeraporte").Value = 1 Then
                sPrimerAporte = "Apo.Inicial"
                .FontBold = True
            Else
                sPrimerAporte = ""
                .FontBold = False
            End If
            sRecord = sPrimerAporte & "|" & _
                      NVL(lReg("fecha_movimiento").Value, "") & "|" & _
                      NVL(lReg("num_cuenta").Value, "") & "|" & _
                      NVL(lReg("rut_cliente").Value, "") & "|" & _
                      NVL(lReg("nombre_cliente").Value, "") & "|" & _
                      NVL(lReg("nombre_asesor").Value, "") & "|" & _
                      FormatNumber(NVL(lReg("monto_aporte").Value, 0), iDecimales) & "|" & _
                      FormatNumber(NVL(lReg("monto_retiro").Value, 0), iDecimales) & "|" & _
                      NVL(lReg("observacion").Value, "")
                      
            
            
            .AddTable sFormat_Det, sHeader_Det, sRecord, clrHeader, , bAppend
            
            dTotalAporte = dTotalAporte + NVL(lReg("monto_aporte").Value, 0)
            dTotalRetiro = dTotalRetiro + NVL(lReg("monto_retiro").Value, 0)
        Next
        dTotalNeto = dTotalAporte - dTotalRetiro
        .FontBold = True
        sRecord = "" & "|" & _
                  "" & "|" & _
                  "" & "|" & _
                  "" & "|" & _
                  "" & "|" & _
                  "Total" & "|" & _
                  FormatNumber(NVL(dTotalAporte, 0), iDecimales) & "|" & _
                  FormatNumber(NVL(dTotalRetiro, 0), iDecimales) & "|" & _
                  ""
        .AddTable sFormat_Det, sHeader_Det, sRecord, clrHeader, , bAppend
        If dTotalNeto > 0 Then
            sRecord = "" & "|" & _
                  "" & "|" & _
              "" & "|" & _
              "" & "|" & _
              "" & "|" & _
              "Total Neto" & "|" & _
              FormatNumber(NVL(dTotalNeto, 0), iDecimales) & "|" & _
              "" & "|" & _
              ""
            .AddTable sFormat_Det, sHeader_Det, sRecord, clrHeader, , bAppend
        Else
            sRecord = "" & "|" & _
                  "" & "|" & _
              "" & "|" & _
              "" & "|" & _
              "" & "|" & _
              "Total Neto" & "|" & _
              "" & "|" & _
              FormatNumber(NVL(dTotalNeto, 0), iDecimales) & "|" & _
              ""
            .AddTable sFormat_Det, sHeader_Det, sRecord, clrHeader, , bAppend
        End If
        .FontBold = False
        
        .EndTable
    End With
End Sub

Private Sub Sub_CargaInforme_Resumen()
'------------------------------------------
Const clrHeader = &HD0D0D0
Const sHeader_Res = "Asesor|Aporte|Retiro|Neto"
Const sFormat_Res = "2000|>1500|>1500|>1500"
Const sHeader_ResMes = "Mes|Asesor|Aporte|Retiro|Neto"
Const sFormat_ResMes = "900|2000|>1500|>1500|>1500"
'------------------------------------------
Dim sRecord
Dim bAppend
'------------------------------------------
Dim lReg              As Object
Dim sTitulo2          As String
Dim sMes              As String
Dim iMes              As Integer
Dim iMesAnterior      As Integer
Dim iContadorMes      As Integer
Dim dTotalAporte      As Double
Dim dTotalRetiro      As Double
Dim dTotalNeto        As Double
Dim dParcialAporte    As Double
Dim dParcialRetiro    As Double
Dim dParcialNeto      As Double
Dim iFila             As Integer
            

    dTotalAporte = 0
    dTotalRetiro = 0
    dTotalNeto = 0
    
    sTitulo2 = "Periodo : " & lPeriodo

    If sTipoFecha = "M" Then
        iContadorMes = 0
        With lForm.VsPrinter
            .FontSize = 8
            .FontBold = True
            .Paragraph = sTitulo2
            .Paragraph = "" 'salto de linea
            .FontBold = False
            .StartTable
            .FontSize = 7
            .TableCell(tcAlignCurrency) = False
    
            For Each lReg In oCursor_Datos
                iMes = lReg("mes").Value
                If iMesAnterior <> 0 And iMes <> iMesAnterior Then
                    sRecord = "" & "|" & _
                              "Total Mensual" & "|" & _
                              FormatNumber(NVL(dParcialAporte, 0), iDecimales) & "|" & _
                              FormatNumber(NVL(dParcialRetiro, 0), iDecimales) & "|" & _
                              FormatNumber(NVL(dParcialNeto, 0), iDecimales)
                    .AddTable sFormat_ResMes, sHeader_ResMes, sRecord, clrHeader, , bAppend
                    iFila = .TableCell(tcRows)
                    .TableCell(tcFontSize, iFila) = 9
                    .TableCell(tcFontBold, iFila) = True
                    
                    iContadorMes = iContadorMes + 1
                    dTotalAporte = dTotalAporte + dParcialAporte
                    dTotalRetiro = dTotalRetiro + dParcialRetiro
                    dTotalNeto = dTotalNeto + dParcialNeto
                    
                    dParcialAporte = 0
                    dParcialRetiro = 0
                    dParcialNeto = 0
                End If
                sMes = Fnt_ObtieneNombreMes(lReg("mes").Value)
                sRecord = sMes & "|" & _
                          NVL(lReg("nombre_asesor").Value, "") & "|" & _
                          FormatNumber(NVL(lReg("monto_aporte").Value, 0), iDecimales) & "|" & _
                          FormatNumber(NVL(lReg("monto_retiro").Value, 0), iDecimales) & "|" & _
                          FormatNumber(NVL(lReg("monto_neto").Value, 0), iDecimales)
                          
                .AddTable sFormat_ResMes, sHeader_ResMes, sRecord, clrHeader, , bAppend
                
                dParcialAporte = dParcialAporte + NVL(lReg("monto_aporte").Value, 0)
                dParcialRetiro = dParcialRetiro + NVL(lReg("monto_retiro").Value, 0)
                dParcialNeto = dParcialNeto + NVL(lReg("monto_neto").Value, 0)
                
                iMesAnterior = lReg("mes").Value
            Next
            iContadorMes = iContadorMes + 1
            
            If iContadorMes > 1 Then
                sRecord = "" & "|" & _
                          "Total Mensual" & "|" & _
                          FormatNumber(NVL(dParcialAporte, 0), iDecimales) & "|" & _
                          FormatNumber(NVL(dParcialRetiro, 0), iDecimales) & "|" & _
                          FormatNumber(NVL(dParcialNeto, 0), iDecimales)
                .AddTable sFormat_ResMes, sHeader_ResMes, sRecord, clrHeader, , bAppend
                iFila = .TableCell(tcRows)
                .TableCell(tcFontSize, iFila) = 9
                .TableCell(tcFontBold, iFila) = True
            End If
            dTotalAporte = dTotalAporte + dParcialAporte
            dTotalRetiro = dTotalRetiro + dParcialRetiro
            dTotalNeto = dTotalNeto + dParcialNeto
                    
            sRecord = "" & "|" & _
                      "Total" & "|" & _
                      FormatNumber(NVL(dTotalAporte, 0), iDecimales) & "|" & _
                      FormatNumber(NVL(dTotalRetiro, 0), iDecimales) & "|" & _
                      FormatNumber(NVL(dTotalNeto, 0), iDecimales)
            .AddTable sFormat_ResMes, sHeader_ResMes, sRecord, clrHeader, , bAppend
            iFila = .TableCell(tcRows)
            .TableCell(tcFontSize, iFila) = 9
            .TableCell(tcFontBold, iFila) = True
            
            .EndTable
        End With
    Else
        With lForm.VsPrinter
            .FontSize = 8
            .FontBold = True
            .Paragraph = sTitulo2
            .Paragraph = "" 'salto de linea
            .FontBold = False
            .StartTable
            .FontSize = 7
            .TableCell(tcAlignCurrency) = False
            For Each lReg In oCursor_Datos
                sRecord = NVL(lReg("nombre_asesor").Value, "") & "|" & _
                          FormatNumber(NVL(lReg("monto_aporte").Value, 0), iDecimales) & "|" & _
                          FormatNumber(NVL(lReg("monto_retiro").Value, 0), iDecimales) & "|" & _
                          FormatNumber(NVL(lReg("monto_neto").Value, 0), iDecimales)
                          
                .AddTable sFormat_Res, sHeader_Res, sRecord, clrHeader, , bAppend
                
                dTotalAporte = dTotalAporte + NVL(lReg("monto_aporte").Value, 0)
                dTotalRetiro = dTotalRetiro + NVL(lReg("monto_retiro").Value, 0)
                dTotalNeto = dTotalNeto + NVL(lReg("monto_neto").Value, 0)
            Next
            sRecord = "Total" & "|" & _
                      FormatNumber(NVL(dTotalAporte, 0), iDecimales) & "|" & _
                      FormatNumber(NVL(dTotalRetiro, 0), iDecimales) & "|" & _
                      FormatNumber(NVL(dTotalNeto, 0), iDecimales)
            .AddTable sFormat_Res, sHeader_Res, sRecord, clrHeader, , bAppend
            iFila = .TableCell(tcRows)
            '.TableCell(tcBackColor, iFila) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
            .TableCell(tcFontSize, iFila) = 9
            .TableCell(tcFontBold, iFila) = True
            .EndTable
        End With
    End If
End Sub

Private Function Fnt_ObtieneTipoConsulta() As String
Dim sTipoConsulta As String

    If rdb_Detalle.Value = True Then
       sTipoConsulta = "D"
    End If
    
    If rdb_Resumen.Value = True Then
        sTipoConsulta = "R"
    End If
    
    If rdb_Ambos.Value = True Then
        sTipoConsulta = "A"
    End If
    
    Fnt_ObtieneTipoConsulta = sTipoConsulta
End Function

Private Function Fnt_ObtieneFiltroFecha() As String
Dim sTipoFecha As String
Dim sFecha As String
Dim sFechaMes As String

    If fFlg_Tipo_Permiso = gcFlg_TipoPermiso_SoloLectura Then
        If Cmb_Cuentas.Text <> "Todos" And bFechaVirtual Then
            If DTP_Fecha_Final.Value > lFechaCierreVirtual Then
                DTP_Fecha_Final.Value = lFechaCierreVirtual
                If DTP_Fecha_Final.Value < DTP_Fecha_Inicial.Value Then
                    DTP_Fecha_Inicial.Value = lFechaCierreVirtual
                End If
            End If
        End If
    End If
    If rdb_RangoFecha.Value = True Then
        sTipoFecha = "R"
        fFechaInicial = DTP_Fecha_Inicial.Value
        fFechaFinal = DTP_Fecha_Final.Value
        lPeriodo = CStr(fFechaInicial) & " al " & CStr(fFechaFinal)
    
    End If
    If Rdb_Mes.Value = True Then
        sTipoFecha = "M"
        sFecha = "01/"
        If DTP_Fecha_Inicial.Month < 10 Then
            sFecha = sFecha & "0" & DTP_Fecha_Inicial.Month & "/"
        Else
            sFecha = sFecha & CStr(DTP_Fecha_Inicial.Month) & "/"
        End If
        sFecha = sFecha & CStr(DTP_Fecha_Inicial.Year)
        fFechaInicial = CDate(sFecha)

        fFechaFinal = UltimoDiaDelMesEnCurso(DTP_Fecha_Final.Value)
        sFechaMes = Format(fFechaInicial, "MMMM yyyy")
        lPeriodo = UCase(Mid(sFechaMes, 1, 1)) & Mid(sFechaMes, 2, Len(sFechaMes))
        sFechaMes = Format(fFechaFinal, "MMMM yyyy")
        lPeriodo = lPeriodo & " a " & UCase(Mid(sFechaMes, 1, 1)) & Mid(sFechaMes, 2, Len(sFechaMes))
    
    End If
    
    Fnt_ObtieneFiltroFecha = sTipoFecha
End Function
Private Sub Sub_Fechas()
Dim sFechaMes As String

  If Rdb_Mes.Value Then
    sFechaMes = Format(DTP_Fecha_Inicial, "MMMM yyyy")
    lPeriodo = UCase(Mid(sFechaMes, 1, 1)) & Mid(sFechaMes, 2, Len(sFechaMes))
    sFechaMes = Format(DTP_Fecha_Final, "MMMM yyyy")
    lPeriodo = lPeriodo & " a " & UCase(Mid(sFechaMes, 1, 1)) & Mid(sFechaMes, 2, Len(sFechaMes))
    fFechaInicial = "01/" & CStr(DTP_Fecha_Inicial.Month) & "/" & CStr(DTP_Fecha_Inicial.Year)
    fFechaInicial = "01/" & CStr(DTP_Fecha_Final.Month) & "/" & CStr(DTP_Fecha_Final.Year)
  End If
     
  If rdb_RangoFecha.Value Then
    lPeriodo = CStr(DTP_Fecha_Inicial) & " al " & CStr(DTP_Fecha_Final)
    fFechaInicial = DTP_Fecha_Inicial
    fFechaFinal = DTP_Fecha_Final
  End If
End Sub

Private Sub Sub_HabilitaTabs()
    If rdb_Ambos.Value Then
        SST_AportesRetiros.TabEnabled(0) = True
        SST_AportesRetiros.TabEnabled(1) = True
    End If
    
    If rdb_Detalle.Value Then
        SST_AportesRetiros.TabEnabled(0) = True
        SST_AportesRetiros.TabEnabled(1) = False
    End If
    
    If rdb_Resumen.Value Then
       SST_AportesRetiros.TabEnabled(0) = False
       SST_AportesRetiros.TabEnabled(1) = True
    End If
End Sub

Private Function Fnt_ObtieneNombreMes(pMes As Integer) As String

Select Case pMes
  Case 1
    Fnt_ObtieneNombreMes = "Enero"
  Case 2
    Fnt_ObtieneNombreMes = "Febrero"
  Case 3
    Fnt_ObtieneNombreMes = "Marzo"
  Case 4
    Fnt_ObtieneNombreMes = "Abril"
  Case 5
    Fnt_ObtieneNombreMes = "Mayo"
  Case 6
    Fnt_ObtieneNombreMes = "Junio"
  Case 7
    Fnt_ObtieneNombreMes = "Julio"
  Case 8
    Fnt_ObtieneNombreMes = "Agosto"
  Case 9
    Fnt_ObtieneNombreMes = "Septiembre"
  Case 10
    Fnt_ObtieneNombreMes = "Octubre"
  Case 11
    Fnt_ObtieneNombreMes = "Noviembre"
  Case 12
    Fnt_ObtieneNombreMes = "Diciembre"
  Case Else
    Fnt_ObtieneNombreMes = ""
End Select

End Function

Private Function TraeDecimalesCuenta(lId_Moneda) As Integer
Dim lDecimalesCuenta    As Integer
Dim lcMoneda            As Object
    lDecimalesCuenta = 0
    Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
    With lcMoneda
      .Campo("id_Moneda").Valor = lId_Moneda
      If .Buscar Then
        lDecimalesCuenta = .Cursor(1)("dicimales_mostrar").Value
      End If
    End With
    Set lcMoneda = Nothing
    TraeDecimalesCuenta = lDecimalesCuenta
End Function


Private Function Fnt_Obtiene_FechaCierre_Virtual(ByVal pId_Cuenta As String, ByRef pFecha As Date) As Boolean
Dim lcCuentas As Object
Dim lResult As Boolean

    lResult = True
    Set lcCuentas = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuentas
        .Campo("id_cuenta").Valor = pId_Cuenta
        If .Buscar Then
            If NVL(.Cursor(1)("fecha_cierre_virtual").Value, "") = "" Then
               lResult = False
            Else
                pFecha = .Cursor(1)("fecha_cierre_virtual").Value
            End If
        End If
    End With
    Fnt_Obtiene_FechaCierre_Virtual = lResult
End Function



