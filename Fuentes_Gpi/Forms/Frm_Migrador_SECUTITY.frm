VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Migrador_SECUTITY 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Migraci�n Security"
   ClientHeight    =   6075
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   9285
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6075
   ScaleWidth      =   9285
   Begin VB.Frame Frm_Fecha_Cierre 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2265
      Left            =   60
      TabIndex        =   6
      Top             =   390
      Width           =   9165
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   345
         Left            =   2910
         TabIndex        =   12
         Top             =   1320
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   609
         _Version        =   393216
         Format          =   17170433
         CurrentDate     =   39463
      End
      Begin VB.Frame Fra_Planilla 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   735
         Left            =   150
         TabIndex        =   9
         Top             =   1020
         Width           =   2595
         Begin VB.OptionButton Opt_Accion 
            Caption         =   "Acciones"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   1320
            TabIndex        =   11
            Top             =   360
            Width           =   1215
         End
         Begin VB.OptionButton Opt_FFMM 
            Caption         =   "FFMM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   90
            TabIndex        =   10
            Top             =   360
            Value           =   -1  'True
            Width           =   795
         End
      End
      Begin VB.CheckBox Chk_TodosClientes 
         Caption         =   "Todos las Cuentas Asociadas"
         Height          =   225
         Left            =   5040
         TabIndex        =   7
         Top             =   300
         Width           =   2805
      End
      Begin hControl2.hTextLabel Txt_CuentaPlanes 
         Height          =   315
         Left            =   150
         TabIndex        =   0
         Top             =   270
         Width           =   4755
         _ExtentX        =   8387
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Cuenta Planes"
         Text            =   "AAS000123"
         Text            =   "AAS000123"
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin MSComctlLib.ProgressBar ProgressBar 
         Height          =   285
         Left            =   150
         TabIndex        =   8
         Top             =   660
         Width           =   8880
         _ExtentX        =   15663
         _ExtentY        =   503
         _Version        =   393216
         Appearance      =   0
         Scrolling       =   1
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9285
      _ExtentX        =   16378
      _ExtentY        =   635
      ButtonWidth     =   2275
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Importar"
            Key             =   "IMPORT"
            Description     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Guardar Log"
            Key             =   "SAVELOG"
            Object.ToolTipText     =   "Graba el log de forma permanente en el equipo. El formato del archivo generado es plano."
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      MousePointer    =   4
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   4
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frnm_Grilla_Sucesos 
      Caption         =   "Visor de Sucesos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3255
      Left            =   60
      TabIndex        =   2
      Top             =   2700
      Width           =   9165
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   2865
         Left            =   120
         TabIndex        =   1
         Top             =   270
         Width           =   7965
         _cx             =   14049
         _cy             =   5054
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Migrador_SECUTITY.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Detalle 
         Height          =   330
         Left            =   8160
         TabIndex        =   5
         Top             =   540
         Width           =   900
         _ExtentX        =   1588
         _ExtentY        =   582
         ButtonWidth     =   1588
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Detalle"
               Key             =   "DETALLE"
               Object.ToolTipText     =   "Muestra el detalle de los sucesos del Cierre"
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "Frm_Migrador_SECUTITY"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fArchivoPlano As String

Private Sub Chk_TodosClientes_Click()
  Txt_CuentaPlanes.Enabled = Not (Chk_TodosClientes.Value = vbChecked)
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("IMPORT").Image = cBoton_Aceptar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("SAVELOG").Image = cBoton_Grabar
  End With

  With Toolbar_Detalle
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("DETALLE").Image = cBoton_Agregar_Grilla
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub


Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Call Sub_StatusBar("")
End Sub

Private Sub Grilla_DblClick()
Dim lMensaje As String

  With Grilla
    If .Row > 0 Then
      lMensaje = GetCell(Grilla, .Row, "TEXTO")
      If Not lMensaje = "" Then
        MsgBox lMensaje, vbInformation, Me.Caption
      End If
    End If
  End With
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "IMPORT"
      If Chk_TodosClientes.Value Then
        Grilla.Rows = 1
        Call Fnt_ImportarMasivo
        MsgBox "Exportacion Finalizada", , Me.Caption
      Else
        Grilla.Rows = 1
        Call Fnt_Importar(Txt_CuentaPlanes.Text)
        MsgBox "Exportacion Finalizada", , Me.Caption
      End If
    Case "SAVELOG"
      Call Sub_Exportar_Archivo_Plano(Grilla, fArchivoPlano)
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Detalle_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "DETALLE"
      Call Grilla_DblClick
  End Select
End Sub

Private Sub Sub_CargaForm()
  Rem Limpia la grilla
  Grilla.Rows = 1
End Sub

Private Function Fnt_Importar(pCuentaPlanes) As Boolean
Dim lDB           As DLL_DB.Class_Conect
Dim lcAlias       As Object
Dim lHora             As Double
'------------------------------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lfCuenta As hFields
Dim lId_Cuenta
'------------------------------------------------------------
Const cMsgErr = "Problemas en la lectura hacia planes."

On Error GoTo ErrProcedure

  Call Sub_Bloquea_Puntero(Me)
  BarraProceso.Value = 0
  Me.Enabled = False

  Set lcAlias = Fnt_CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                        , pCodigoCSBPI:=cTabla_Cuentas _
                                        , pValor:=pCuentaPlanes)
  Set lcAlias = Nothing
  
  If IsNull(lId_Cuenta) Then
    MsgBox "No existe la cuenta asociada a CSGPI", vbCritical, Me.Caption
    GoTo ExitProcedure
  End If

  lHora = GetTickCount
  Call Fnt_Escribe_Grilla(Grilla, "N", "Se procesa la cuenta con ID " & lId_Cuenta & " - " & pCuentaPlanes, pConLog:=True)
  
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cuenta").Valor = lId_Cuenta
    If Not .Buscar Then
      Call Fnt_Escribe_Grilla(Grilla, "E", "No se puede leer la cuenta." & vbCr & .ErrMsg, pConLog:=True)
      GoTo ExitProcedure
    End If
    
    Set lfCuenta = .Cursor(1)
  End With
  
  Set lDB = gDB
'  Set lDB = New DLL_DB.Class_Conect
'  With lDB
'    .AppPath = App.Path & "\"
'    .ArchivoConfiguracion = "INVERSIONES.INI"
'    .Conectar
'  End With
    
  lfCuenta("fecha_operativa").Value = DateSerial(2007, 10, 1)
    
'  Call Sub_Importar_Caja(lDB, lId_Cuenta, pCuentaPlanes, lfCuenta)
  If Opt_Accion.Value Then
    Call Sub_Importar_Acciones(lDB, lId_Cuenta, pCuentaPlanes, lfCuenta)
  End If
  If Opt_FFMM.Value Then
    Call Sub_Importar_FFMM(lDB, lId_Cuenta, pCuentaPlanes, lfCuenta)
    'Call Sub_Importar_FFMM_2(lDB, lId_Cuenta, pCuentaPlanes, lfCuenta)
  End If
  'Call Sub_Importar_Bonos(lDB, lId_Cuenta, pCuentaPlanes, lfCuenta)
  'Call Sub_Importar_Depositos(lDB, lId_Cuenta, pCuentaPlanes, lfCuenta)
  'Call Sub_Importar_Pactos(lDB, lId_Cuenta, pCuentaPlanes, lfCuenta)
   
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, cMsgErr, Err.Description, pConLog:=True)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  lHora = GetTickCount - lHora
  Call Fnt_Escribe_Grilla(Grilla, "N", "Importaci�n Finalizada en " & (lHora / 1000) & " segundos.", pConLog:=True)
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
  
  Call Sub_StatusBar("Exportacion Finalizada")
End Function

Private Sub Sub_Importar_Acciones(pDB As DLL_DB.Class_Conect, pId_Cuenta, pCuentaPlanes, pfCuenta)
Dim lcOperacion         As Class_Operaciones
Dim lcOperacion_Detalle As Class_Operaciones_Detalle
Dim lcCaja_Cuenta       As Class_Cajas_Cuenta
Dim lcNemotecnico       As Class_Nemotecnicos
Dim lCursor             As hRecord
Dim lReg                As hFields
'------------------------------------------------------
Dim lTipoOperacion    As String
Dim lId_Moneda        As Double
Dim lId_Caja_Cuenta   As String
Dim lFecha_Operacion  As Date
Dim lId_Operacion     As Double
Dim lId_Nemotecnico   As Double
Dim lFecha_Liquidacion As Date
'------------------------------------------------------
Dim lSql              As String
Dim lMov_Cargados     As Long
'------------------------------------------------------
Const cTipOp_Compra = "C"
Const cTipOp_Venta = "V"
Const cMsgErr = "Problemas en la lectura hacia planes en acciones."
  
On Error GoTo ErrProcedure
  
  BarraProceso.Value = 0
  'lSql = "SELECT cartera, tip_instru, docto, n_bolsa, emisor, fecha, cantidad, precio, valor," & _
         "       Comision , Iva, Gastos, derechobolsa, TIP_OPERA, FECHA_TR" & _
         "  From acciones " & _
         " WHERE cartera = '" & pCuentaPlanes & "' " & _
         "   and FECHA >= convert(datetime, '" & Format(pfCuenta("fecha_operativa").Value, "yyyymmdd") & "', 112)" & _
         "   and FECHA <= convert(datetime, '20080131', 112)" & _
         " ORDER BY FECHA_TR, docto, N_BOLSA"
' SIN FECHA OPERATIVA
         
  lSql = "SELECT cartera, tip_instru, docto, n_bolsa, emisor, fecha, cantidad, precio, valor," & _
         "       Comision , Iva, Gastos, derechobolsa, TIP_OPERA, FECHA_TR" & _
         "  From acciones " & _
         " WHERE cartera = '" & pCuentaPlanes & "' " & _
         "   and FECHA <= convert(datetime, '20080116', 112)" & _
         " ORDER BY FECHA_TR, docto, N_BOLSA"


Print pDB.Parametros.Clear
  If Not pDB.EjecutaSelect(lSql) Then
    Call Fnt_MsgError(eLS_ErrSystem _
                      , cMsgErr _
                      , pDB.ErrMsg _
                      , pConLog:=True)
    GoTo ExitProcedure
  End If
  
  Set lCursor = pDB.Parametros("cursor").Valor
    
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & "Se encontraron " & lCursor.Count & " movimientos de acciones.", pConLog:=True)
  
  lId_Moneda = 1 'peso
  lMov_Cargados = 0
    
  If lCursor.Count > 0 Then
    BarraProceso.Max = lCursor.Count
  End If
  
  Set lcOperacion = Nothing
  For Each lReg In lCursor
    Call gRelogDB.AvanzaRelog
    BarraProceso.Value = BarraProceso.Value + 1
    Call Sub_StatusBar("Cargando movimiento " & Format(BarraProceso.Value, "#,###") & " de " & Format(BarraProceso.Max, "#,###"))
    DoEvents
    
    lTipoOperacion = IIf(lReg("TIP_OPERA").Value = cTipOp_Venta, gcTipoOperacion_Egreso, gcTipoOperacion_Ingreso)
    lFecha_Operacion = lReg("fecha").Value
  
    lFecha_Liquidacion = Fnt_Calcula_Dia_Habil(lFecha_Operacion, 2)
  
    Set lcOperacion = New Class_Operaciones
    With lcOperacion
      .Campo("Id_Operacion").Valor = cNewEntidad
      .Campo("Id_Cuenta").Valor = pId_Cuenta
      .Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Custodia ' Es para ingresar custodia sin mov caja
      '.Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Instruccion ' Es para operaciones
      .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente
      .Campo("Id_Contraparte").Valor = Null
      .Campo("Id_Representante").Valor = Null
      .Campo("Id_Moneda_Operacion").Valor = 1 'peso
      .Campo("Cod_Producto").Valor = gcPROD_RV_NAC 'Esto va en duro
      .Campo("Cod_Instrumento").Valor = gcINST_ACCIONES_NAC 'Esto va en duro
      .Campo("Flg_Tipo_Movimiento").Valor = lTipoOperacion
      .Campo("Fecha_Operacion").Valor = lFecha_Operacion
      .Campo("Fecha_Vigencia").Valor = lFecha_Operacion
      .Campo("Fecha_Liquidacion").Valor = lFecha_Liquidacion
      .Campo("Dsc_Operacion").Valor = "" 'pDsc_Operacion
      .Campo("Id_Trader").Valor = Null
      .Campo("Porc_Comision").Valor = 0 'pPorc_Comision
      .Campo("Comision").Valor = 0 'lReg("Comision").Value
      .Campo("Derechos").Valor = 0 'lReg("derechobolsa").Value
      .Campo("Gastos").Valor = 0 'lReg("Gastos").Value
      .Campo("Iva").Valor = 0 'lReg("Iva").Value
      .Campo("Monto_Operacion").Valor = 0 'pMonto_Operacion
      .Campo("Flg_Limite_Precio").Valor = cTipo_Precio_Mercado
    End With
  
    Rem Busca que el nemotecnicos exista en el sistema
    Set lcNemotecnico = New Class_Nemotecnicos
    With lcNemotecnico
      .Campo("nemotecnico").Valor = Trim(lReg("n_bolsa").Value)
      If Not .Buscar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , cMsgErr _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ExitProcedure
      End If
    
      If .Cursor.Count <= 0 Then
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No esta definido el nemotecnico """ & lReg("n_bolsa").Value & """, favor de crear", pConLog:=True)
        GoTo SiguienteNemo
      End If
    
      lId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
    End With
  
    Set lcOperacion_Detalle = New Class_Operaciones_Detalle
    With lcOperacion_Detalle
      .Campo("Id_Nemotecnico").Valor = lId_Nemotecnico
      .Campo("Cantidad").Valor = lReg("cantidad").Value
      .Campo("Precio").Valor = lReg("precio").Value
      .Campo("Id_Moneda_Pago").Valor = lId_Moneda
      .Campo("Monto_Pago").Valor = lReg("valor").Value
      .Campo("Flg_Vende_Todo").Valor = cFlg_No_Vende_Todo
    End With
    
    With lcOperacion
      .Campo("Comision").Valor = .Campo("Comision").Valor + lReg("Comision").Value
      .Campo("Derechos").Valor = .Campo("Derechos").Valor + lReg("derechobolsa").Value
      .Campo("Gastos").Valor = .Campo("Gastos").Valor + lReg("Gastos").Value
      .Campo("Iva").Valor = .Campo("Iva").Valor + lReg("Iva").Value
      .Campo("Monto_Operacion").Valor = .Campo("Monto_Operacion").Valor + lReg("valor").Value + ((lReg("Iva").Value + lReg("Gastos").Value + lReg("derechobolsa").Value + lReg("Comision").Value) * IIf(lTipoOperacion = gcTipoOperacion_Egreso, -1, 1))
      
      Call .Agregar_Operaciones_Detalle(lcOperacion_Detalle)
    End With
    
    lId_Caja_Cuenta = ""
  
    Set lcCaja_Cuenta = New Class_Cajas_Cuenta
    With lcCaja_Cuenta
      .Campo("id_cuenta").Valor = pId_Cuenta
      .Campo("id_moneda").Valor = lId_Moneda
      If Not .Buscar_Caja_Para_Invertir() Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , cMsgErr _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ExitProcedure
      End If
      
      If .Cursor.Count = 0 Then
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe una caja definida para la moneda PESO, es imposible realizar el movimiento de caja.", pConLog:=True)
        GoTo SiguienteNemo
      End If
      
      lId_Caja_Cuenta = .Cursor(1)("Id_Caja_Cuenta").Value
    End With
  
    If Not lcOperacion.Guardar(lId_Caja_Cuenta) Then
      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
                        , cMsgErr _
                        , lcOperacion.ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
    
'    lId_Operacion = lcOperacion.Campo("Id_Operacion").Valor
'
'    If Not lcOperacion.Confirmar(lId_Caja_Cuenta) Then
'      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
'                        , cMsgErr _
'                        , lcOperacion.ErrMsg _
'                        , pConLog:=True)
'      GoTo ExitProcedure
'    End If
    
    lMov_Cargados = lMov_Cargados + 1
    
SiguienteNemo:

  Next
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, cMsgErr, Err.Description, pConLog:=True)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Set lcOperacion = Nothing
  Set lcCaja_Cuenta = Nothing
  
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & lMov_Cargados & " Movimientos de acciones cargados.", pConLog:=True)
  Beep
End Sub

Private Sub Sub_Importar_FFMM(pDB As DLL_DB.Class_Conect, pId_Cuenta, pCuentaPlanes, pfCuenta)
Dim lcOperacion         As Class_Operaciones
Dim lcOperacion_Detalle As Class_Operaciones_Detalle
Dim lcCaja_Cuenta       As Class_Cajas_Cuenta
Dim lcNemotecnico       As Class_Nemotecnicos
Dim lcAlias             As Object
Dim lCursor             As hRecord
Dim lReg                As hFields
'------------------------------------------------------
Dim lTipoOperacion    As String
Dim lId_Moneda        As Double
Dim lId_Caja_Cuenta
Dim lFecha_Operacion  As Date
Dim lId_Operacion     As Double
Dim lId_Nemotecnico
'------------------------------------------------------
Dim lSql              As String
Dim lMov_Cargados     As Long
'------------------------------------------------------
Const cTipOp_Compra = "C"
Const cTipOp_Venta = "V"
Const cMsgErr = "Problemas en la lectura hacia planes en fondos mutuos."
  
On Error GoTo ErrProcedure

  Set lcAlias = Fnt_CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  
  BarraProceso.Value = 0
'  lSql = "select cartera, tip_opera, fondo, fecha, cantidad, " & _
         " MPRECIO AS precio, MVALOR AS valor, fecha_tr, CUENTA, fondo2 as num_fondo " & _
         " From Fon_mut " & _
         " where cartera = '" & pCuentaPlanes & "' " & _
         "   and FECHA_tr >= convert(datetime, '" & Format(pfCuenta("fecha_operativa").Value, "yyyymmdd") & "', 112)" & _
         "   and FECHA_tr <= convert(datetime, '20080229', 112)" & _
         " order by cartera, fecha_tr, fondo "
         
' SIN TOMAR FECHA OPERATIVA
  lSql = "select cartera, tip_opera, fondo, fecha, cantidad, " & _
         " MPRECIO AS precio, MVALOR AS valor, fecha_tr, CUENTA, fondo2 as num_fondo " & _
         " From Fon_mut " & _
         " where cartera = '" & pCuentaPlanes & "' " & _
         " and FECHA <= '" & Format(DTPicker1.Value, "yyyymmdd") & "'" & _
         " order by cartera, fecha, fondo "

'         "   and FECHA <= convert(datetime, '20080116', 112)" & _
 '        "   and FECHA_tr <= convert(datetime, '20080116', 112)" & _

  pDB.Parametros.Clear
  If Not pDB.EjecutaSelect(lSql) Then
    Call Fnt_MsgError(eLS_ErrSystem _
                      , cMsgErr _
                      , pDB.ErrMsg _
                      , pConLog:=True)
    GoTo ExitProcedure
  End If
  
  Set lCursor = pDB.Parametros("cursor").Valor
    
  Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Informacion, Space(4) & "Se encontraron " & lCursor.Count & " movimientos de fondos mutuos.", pConLog:=True)
  
  If lCursor.Count > 0 Then
    BarraProceso.Max = lCursor.Count
  End If
  
  lId_Moneda = 1 'peso
  lMov_Cargados = 0
  
  Set lcOperacion = Nothing
  For Each lReg In lCursor
    Call gRelogDB.AvanzaRelog
    BarraProceso.Value = BarraProceso.Value + 1
    Call Sub_StatusBar("Cargando movimiento " & Format(BarraProceso.Value, "#,###") & " de " & Format(BarraProceso.Max, "#,###"))
    DoEvents
    
    lId_Caja_Cuenta = ""
    lId_Caja_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                          , pCodigoCSBPI:=cTabla_Cajas_Cuenta _
                                          , pValor:=Trim(lReg("CUENTA").Value))
  
    If IsNull(lId_Caja_Cuenta) Then
      Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe una caja definida para la cuenta """ & Trim(lReg("CUENTA").Value) & """ , es imposible realizar el movimiento de caja.", pConLog:=True)
      GoTo SiguienteNemo
    End If
    
    Set lcCaja_Cuenta = New Class_Cajas_Cuenta
    With lcCaja_Cuenta
      .Campo("Id_Caja_Cuenta").Valor = lId_Caja_Cuenta
      If Not .Buscar() Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , cMsgErr _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ExitProcedure
      End If

      If .Cursor.Count = 0 Then
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe una caja definida para la moneda PESO, es imposible realizar el movimiento de caja.", pConLog:=True)
        GoTo SiguienteNemo
      End If

      lId_Moneda = .Cursor(1)("id_moneda").Value
    End With
    
    lTipoOperacion = IIf(lReg("TIP_OPERA").Value = cTipOp_Venta, gcTipoOperacion_Egreso, gcTipoOperacion_Ingreso)
    lFecha_Operacion = lReg("fecha").Value
  
    Set lcOperacion = New Class_Operaciones
    With lcOperacion
      .Campo("Id_Operacion").Valor = cNewEntidad
      .Campo("Id_Cuenta").Valor = pId_Cuenta
      .Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Custodia
      '.Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Instruccion
      .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente
      .Campo("Id_Contraparte").Valor = Null
      .Campo("Id_Representante").Valor = Null
      .Campo("Id_Moneda_Operacion").Valor = lId_Moneda
      .Campo("Cod_Producto").Valor = gcPROD_FFMM_NAC 'Esto va en duro
      .Campo("Cod_Instrumento").Valor = gcINST_FFMM_RF_NAC  'Esto va en duro
      .Campo("Flg_Tipo_Movimiento").Valor = lTipoOperacion
      .Campo("Fecha_Operacion").Valor = lFecha_Operacion
      .Campo("Fecha_Vigencia").Valor = lFecha_Operacion
      .Campo("Fecha_Liquidacion").Valor = lFecha_Operacion
      .Campo("Dsc_Operacion").Valor = "" 'pDsc_Operacion
      .Campo("Id_Trader").Valor = Null
      .Campo("Porc_Comision").Valor = 0 'pPorc_Comision
      .Campo("Comision").Valor = 0 'lReg("Comision").Value
      .Campo("Derechos").Valor = 0 'lReg("derechobolsa").Value
      .Campo("Gastos").Valor = 0 'lReg("Gastos").Value
      .Campo("Iva").Valor = 0 'lReg("Iva").Value
      .Campo("Monto_Operacion").Valor = 0 'pMonto_Operacion
      .Campo("Flg_Limite_Precio").Valor = cTipo_Precio_Mercado
    End With
  
Select Case Trim(lReg("FONDO").Value)
      Case "SEC. TRADE", "SEC. INCOME", "SEC.  INCOME"
      
        lReg("FONDO").Value = "SEC.   INCOME"
      Case "SEC.  EQUITY FUND", "SEC EUQITY FUND  57B"
        lReg("FONDO").Value = "SEC EUQITY FUND  57BIS-A"
      Case "FM  OPORTUNITY  105"
        lReg("FONDO").Value = "FM  OPORTUNITY  105  GARANTIZADO"
      Case "SEC. EQUITY FUND  SE"
        lReg("FONDO").Value = "SEC. EQUITY FUND  SERIE  B"
      Case "SEC. ACCIONES SERIE", "SECURITY ACCIONES SE", "SEC. ACCIONES 57 BIS", "SEC.  ACCIONES 57 BI"
        lReg("FONDO").Value = "SEC. ACCIONES 57 BIS B"
      Case "SECURITY ACCIONES", "SEC. ACCIONES"
        lReg("FONDO").Value = "SEC.  ACCIONES 57 BIS A"
      Case "SECURITY PLUS", "SEC. PLUS 57 BIS A"
        lReg("FONDO").Value = "SEC. PLUS"
      Case "SECURITY GOLD", "SECURITY GOLD  ART."
        lReg("FONDO").Value = "SEC. GOLD"
      Case "SEC. GOLD  ART. 57 B", "SEC. GOLD 57 BIS B"
        lReg("FONDO").Value = "SEC. GOLD SERIE B"
      Case "SEC. DOLAR BOND SERI", "SECURITY DOLAR BOND", "SEC. DOLAR BOND 57 B"
        lReg("FONDO").Value = "SEC. DOLAR BOND"
      Case "SEC. GLOBAL 57 BIS B", "SEC. GLOBAL INVEST."
        lReg("FONDO").Value = "SEC. GLOBAL SERIE B"
      Case "SEC. EXPLORER SERIE", "SECURITY EXPLORER SE"
        lReg("FONDO").Value = "SEC. EXPLORER 57 BIS-B"
      Case "SEC.  ENERGY  FUND", "SEC. ENRGY FUND"
        lReg("FONDO").Value = "SEC.  ENERGY  FUND  SERI  D"
      Case "SEC. ENRGY FUND"
        lReg("FONDO").Value = "SEC. ENERGY FUND"
      Case "SEC.  INCOME  SERIE-"
        lReg("FONDO").Value = "SEC.  INCOME   57 BIS-B"
      Case "SEC. BALANCE  SERIE"
        lReg("FONDO").Value = "SEC. BALANCE  SERIE B"
      Case "SECURITY PREMIUM"
        lReg("FONDO").Value = "SEC. PREMIUM"
      Case "SECURITY CHECK", "SEC. CHECK 57 BIS A"
        lReg("FONDO").Value = "SEC. CHECK"
      Case "SEC. GLOBAL INVESTME", "SEC. GLOBAL IINVEST.", "SEC. GLOBAL BIS A SE", "SEC. GLOBAL 57 BIS A"
        lReg("FONDO").Value = "SEC. GLOBAL"
      Case "SECURITY FIRST"
        lReg("FONDO").Value = "SEC. FIRST"
      Case "SECURITY EXPLORER"
        lReg("FONDO").Value = "SEC. EXPLORER"
      Case "SEC. VALUE 57 BIS-A"
        lReg("FONDO").Value = "SEC. VALUE"
      Case "SECURITY 9  SERIE B"
        lReg("FONDO").Value = "SECURITY 9 SERIE B"
    End Select
    
    lId_Nemotecnico = ""
    lId_Nemotecnico = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                          , pCodigoCSBPI:=cTabla_Nemotecnicos _
                                          , pValor:=Trim(lReg("fondo").Value))
  
    If IsNull(lId_Nemotecnico) Then
      Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No esta definido o relacionado el nemotecnico """ & Trim(lReg("fondo").Value) & """ (" & Trim(lReg("num_fondo").Value) & "), favor de regularizar.", pConLog:=True)
      GoTo SiguienteNemo
    End If
  
'    Rem Busca que el nemotecnicos exista en el sistema
'    Set lcNemotecnico = New Class_Nemotecnicos
'    With lcNemotecnico
'      .Campo("nemotecnico").Valor = Trim(lReg("fondo").Value)
'      If Not .Buscar Then
'        Call Fnt_MsgError(.SubTipo_LOG _
'                          , cMsgErr _
'                          , .ErrMsg _
'                          , pConLog:=True)
'        GoTo ExitProcedure
'      End If
'
'      If .Cursor.Count <= 0 Then
'        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No esta definido el nemotecnico """ & lReg("fondo").Value & """, favor de crear", pConLog:=True)
'        GoTo SiguienteNemo
'      End If
'
'      lId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
'    End With
  
    Set lcOperacion_Detalle = New Class_Operaciones_Detalle
    With lcOperacion_Detalle
      .Campo("Id_Nemotecnico").Valor = lId_Nemotecnico
      .Campo("Cantidad").Valor = lReg("cantidad").Value
      .Campo("Precio").Valor = lReg("precio").Value
      .Campo("Id_Moneda_Pago").Valor = lId_Moneda
      .Campo("Monto_Pago").Valor = lReg("valor").Value
      .Campo("Flg_Vende_Todo").Valor = cFlg_No_Vende_Todo
    End With
    
    With lcOperacion
      .Campo("Comision").Valor = .Campo("Comision").Valor + 0 'lReg("Comision").Value
      .Campo("Derechos").Valor = .Campo("Derechos").Valor + 0 'lReg("derechobolsa").Value
      .Campo("Gastos").Valor = .Campo("Gastos").Valor + 0 'lReg("Gastos").Value
      .Campo("Iva").Valor = .Campo("Iva").Valor + 0 'lReg("Iva").Value
      .Campo("Monto_Operacion").Valor = .Campo("Monto_Operacion").Valor + lReg("valor").Value + 0 '((lReg("Iva").Value + lReg("Gastos").Value + lReg("derechobolsa").Value + lReg("Comision").Value) * IIf(lTipoOperacion = gcTipoOperacion_Egreso, -1, 1))
      
      Call .Agregar_Operaciones_Detalle(lcOperacion_Detalle)
    End With
    
    
  
    If Not lcOperacion.Guardar(lId_Caja_Cuenta) Then
      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
                        , cMsgErr _
                        , lcOperacion.ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
    
'    lId_Operacion = lcOperacion.Campo("Id_Operacion").Valor
'
'    If Not lcOperacion.Confirmar(lId_Caja_Cuenta) Then
'      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
'                        , cMsgErr _
'                        , lcOperacion.ErrMsg _
'                        , pConLog:=True)
'      GoTo ExitProcedure
'    End If
    
    lMov_Cargados = lMov_Cargados + 1
    
SiguienteNemo:

  Next
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, cMsgErr, Err.Description, pConLog:=True)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Set lcOperacion = Nothing
  Set lcCaja_Cuenta = Nothing
  Set lcAlias = Nothing
  
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & lMov_Cargados & " Movimientos de fondos mutuos cargados.", pConLog:=True)
End Sub

Private Sub Sub_Importar_FFMM_2(pDB As DLL_DB.Class_Conect, pId_Cuenta, pCuentaPlanes, pfCuenta)
Dim lcOperacion         As Class_Operaciones
Dim lcOperacion_Detalle As Class_Operaciones_Detalle
Dim lcCaja_Cuenta       As Class_Cajas_Cuenta
Dim lcNemotecnico       As Class_Nemotecnicos
Dim lcAlias             As Object
Dim lCursor             As hRecord
Dim lReg                As hFields
'------------------------------------------------------
Dim lTipoOperacion    As String
Dim lId_Moneda        As Double
Dim lId_Caja_Cuenta
Dim lFecha_Operacion  As Date
Dim lId_Operacion     As Double
Dim lId_Nemotecnico
'------------------------------------------------------
Dim lSql              As String
Dim lMov_Cargados     As Long
'------------------------------------------------------
Const cTipOp_Compra = "C"
Const cTipOp_Venta = "V"
Const cMsgErr = "Problemas en la lectura hacia planes en fondos mutuos."
  
On Error GoTo ErrProcedure

  Set lcAlias = Fnt_CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  
  BarraProceso.Value = 0
'  lSql = "select cartera, tip_opera, fondo, fecha, cantidad, " & _
         " MPRECIO AS precio, MVALOR AS valor, fecha_tr, CUENTA, fondo2 as num_fondo " & _
         " From Fon_mut " & _
         " where cartera = '" & pCuentaPlanes & "' " & _
         "   and FECHA_tr >= convert(datetime, '" & Format(pfCuenta("fecha_operativa").Value, "yyyymmdd") & "', 112)" & _
         "   and FECHA_tr <= convert(datetime, '20080229', 112)" & _
         " order by cartera, fecha_tr, fondo "
         
' SIN TOMAR FECHA OPERATIVA
  lSql = "select cartera, tip_opera, fondo, fecha, cantidad, " & _
         " MPRECIO AS precio, MVALOR AS valor, fecha_tr, CUENTA, fondo2 as num_fondo " & _
         " From Fon_mut " & _
         " where cartera = '" & pCuentaPlanes & "' " & _
         "   and FECHA <= convert(datetime, '20080117', 112)" & _
         "   and FECHA_tr > convert(datetime, '20080117', 112)" & _
         " order by cartera, fecha, fondo "
         

  pDB.Parametros.Clear
  If Not pDB.EjecutaSelect(lSql) Then
    Call Fnt_MsgError(eLS_ErrSystem _
                      , cMsgErr _
                      , pDB.ErrMsg _
                      , pConLog:=True)
    GoTo ExitProcedure
  End If
  
  Set lCursor = pDB.Parametros("cursor").Valor
    
  Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Informacion, Space(4) & "Se encontraron " & lCursor.Count & " movimientos de fondos mutuos.", pConLog:=True)
  
  If lCursor.Count > 0 Then
    BarraProceso.Max = lCursor.Count
  End If
  
  lId_Moneda = 1 'peso
  lMov_Cargados = 0
  
  Set lcOperacion = Nothing
  For Each lReg In lCursor
    Call gRelogDB.AvanzaRelog
    BarraProceso.Value = BarraProceso.Value + 1
    Call Sub_StatusBar("Cargando movimiento " & Format(BarraProceso.Value, "#,###") & " de " & Format(BarraProceso.Max, "#,###"))
    DoEvents
    
    lId_Caja_Cuenta = ""
    lId_Caja_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                          , pCodigoCSBPI:=cTabla_Cajas_Cuenta _
                                          , pValor:=Trim(lReg("CUENTA").Value))
  
    If IsNull(lId_Caja_Cuenta) Then
      Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe una caja definida para la cuenta """ & Trim(lReg("CUENTA").Value) & """ , es imposible realizar el movimiento de caja.", pConLog:=True)
      GoTo SiguienteNemo
    End If
    
    Set lcCaja_Cuenta = New Class_Cajas_Cuenta
    With lcCaja_Cuenta
      .Campo("Id_Caja_Cuenta").Valor = lId_Caja_Cuenta
      If Not .Buscar() Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , cMsgErr _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ExitProcedure
      End If

      If .Cursor.Count = 0 Then
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe una caja definida para la moneda PESO, es imposible realizar el movimiento de caja.", pConLog:=True)
        GoTo SiguienteNemo
      End If

      lId_Moneda = .Cursor(1)("id_moneda").Value
    End With
    
    lTipoOperacion = IIf(lReg("TIP_OPERA").Value = cTipOp_Venta, gcTipoOperacion_Egreso, gcTipoOperacion_Ingreso)
    lFecha_Operacion = lReg("fecha").Value
  
    Set lcOperacion = New Class_Operaciones
    With lcOperacion
      .Campo("Id_Operacion").Valor = cNewEntidad
      .Campo("Id_Cuenta").Valor = pId_Cuenta
      .Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Custodia
      '.Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Instruccion
      .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente
      .Campo("Id_Contraparte").Valor = Null
      .Campo("Id_Representante").Valor = Null
      .Campo("Id_Moneda_Operacion").Valor = lId_Moneda
      .Campo("Cod_Producto").Valor = gcPROD_FFMM_NAC 'Esto va en duro
      .Campo("Cod_Instrumento").Valor = gcINST_FFMM_RF_NAC  'Esto va en duro
      .Campo("Flg_Tipo_Movimiento").Valor = lTipoOperacion
      .Campo("Fecha_Operacion").Valor = lFecha_Operacion
      .Campo("Fecha_Vigencia").Valor = lFecha_Operacion
      .Campo("Fecha_Liquidacion").Valor = lFecha_Operacion
      .Campo("Dsc_Operacion").Valor = "" 'pDsc_Operacion
      .Campo("Id_Trader").Valor = Null
      .Campo("Porc_Comision").Valor = 0 'pPorc_Comision
      .Campo("Comision").Valor = 0 'lReg("Comision").Value
      .Campo("Derechos").Valor = 0 'lReg("derechobolsa").Value
      .Campo("Gastos").Valor = 0 'lReg("Gastos").Value
      .Campo("Iva").Valor = 0 'lReg("Iva").Value
      .Campo("Monto_Operacion").Valor = 0 'pMonto_Operacion
      .Campo("Flg_Limite_Precio").Valor = cTipo_Precio_Mercado
    End With
  
Select Case Trim(lReg("FONDO").Value)
      Case "SEC. TRADE", "SEC. INCOME", "SEC.  INCOME"
      
        lReg("FONDO").Value = "SEC.   INCOME"
      Case "SEC.  EQUITY FUND", "SEC EUQITY FUND  57B"
        lReg("FONDO").Value = "SEC EUQITY FUND  57BIS-A"
      Case "FM  OPORTUNITY  105"
        lReg("FONDO").Value = "FM  OPORTUNITY  105  GARANTIZADO"
      Case "SEC. EQUITY FUND  SE"
        lReg("FONDO").Value = "SEC. EQUITY FUND  SERIE  B"
      Case "SEC. ACCIONES SERIE", "SECURITY ACCIONES SE", "SEC. ACCIONES 57 BIS", "SEC.  ACCIONES 57 BI"
        lReg("FONDO").Value = "SEC. ACCIONES 57 BIS B"
      Case "SECURITY ACCIONES", "SEC. ACCIONES"
        lReg("FONDO").Value = "SEC.  ACCIONES 57 BIS A"
      Case "SECURITY PLUS", "SEC. PLUS 57 BIS A"
        lReg("FONDO").Value = "SEC. PLUS"
      Case "SECURITY GOLD", "SECURITY GOLD  ART."
        lReg("FONDO").Value = "SEC. GOLD"
      Case "SEC. GOLD  ART. 57 B", "SEC. GOLD 57 BIS B"
        lReg("FONDO").Value = "SEC. GOLD SERIE B"
      Case "SEC. DOLAR BOND SERI", "SECURITY DOLAR BOND", "SEC. DOLAR BOND 57 B"
        lReg("FONDO").Value = "SEC. DOLAR BOND"
      Case "SEC. GLOBAL 57 BIS B", "SEC. GLOBAL INVEST."
        lReg("FONDO").Value = "SEC. GLOBAL SERIE B"
      Case "SEC. EXPLORER SERIE", "SECURITY EXPLORER SE"
        lReg("FONDO").Value = "SEC. EXPLORER 57 BIS-B"
      Case "SEC.  ENERGY  FUND", "SEC. ENRGY FUND"
        lReg("FONDO").Value = "SEC.  ENERGY  FUND  SERI  D"
      Case "SEC. ENRGY FUND"
        lReg("FONDO").Value = "SEC. ENERGY FUND"
      Case "SEC.  INCOME  SERIE-"
        lReg("FONDO").Value = "SEC.  INCOME   57 BIS-B"
      Case "SEC. BALANCE  SERIE"
        lReg("FONDO").Value = "SEC. BALANCE  SERIE B"
      Case "SECURITY PREMIUM"
        lReg("FONDO").Value = "SEC. PREMIUM"
      Case "SECURITY CHECK", "SEC. CHECK 57 BIS A"
        lReg("FONDO").Value = "SEC. CHECK"
      Case "SEC. GLOBAL INVESTME", "SEC. GLOBAL IINVEST.", "SEC. GLOBAL BIS A SE", "SEC. GLOBAL 57 BIS A"
        lReg("FONDO").Value = "SEC. GLOBAL"
      Case "SECURITY FIRST"
        lReg("FONDO").Value = "SEC. FIRST"
      Case "SECURITY EXPLORER"
        lReg("FONDO").Value = "SEC. EXPLORER"
      Case "SEC. VALUE 57 BIS-A"
        lReg("FONDO").Value = "SEC. VALUE"
      Case "SECURITY 9  SERIE B"
        lReg("FONDO").Value = "SECURITY 9 SERIE B"
    End Select
    
    lId_Nemotecnico = ""
    lId_Nemotecnico = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                          , pCodigoCSBPI:=cTabla_Nemotecnicos _
                                          , pValor:=Trim(lReg("fondo").Value))
  
    If IsNull(lId_Nemotecnico) Then
      Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No esta definido o relacionado el nemotecnico """ & Trim(lReg("fondo").Value) & """ (" & Trim(lReg("num_fondo").Value) & "), favor de regularizar.", pConLog:=True)
      GoTo SiguienteNemo
    End If
  
'    Rem Busca que el nemotecnicos exista en el sistema
'    Set lcNemotecnico = New Class_Nemotecnicos
'    With lcNemotecnico
'      .Campo("nemotecnico").Valor = Trim(lReg("fondo").Value)
'      If Not .Buscar Then
'        Call Fnt_MsgError(.SubTipo_LOG _
'                          , cMsgErr _
'                          , .ErrMsg _
'                          , pConLog:=True)
'        GoTo ExitProcedure
'      End If
'
'      If .Cursor.Count <= 0 Then
'        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No esta definido el nemotecnico """ & lReg("fondo").Value & """, favor de crear", pConLog:=True)
'        GoTo SiguienteNemo
'      End If
'
'      lId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
'    End With
  
    Set lcOperacion_Detalle = New Class_Operaciones_Detalle
    With lcOperacion_Detalle
      .Campo("Id_Nemotecnico").Valor = lId_Nemotecnico
      .Campo("Cantidad").Valor = lReg("cantidad").Value
      .Campo("Precio").Valor = lReg("precio").Value
      .Campo("Id_Moneda_Pago").Valor = lId_Moneda
      .Campo("Monto_Pago").Valor = lReg("valor").Value
      .Campo("Flg_Vende_Todo").Valor = cFlg_No_Vende_Todo
    End With
    
    With lcOperacion
      .Campo("Comision").Valor = .Campo("Comision").Valor + 0 'lReg("Comision").Value
      .Campo("Derechos").Valor = .Campo("Derechos").Valor + 0 'lReg("derechobolsa").Value
      .Campo("Gastos").Valor = .Campo("Gastos").Valor + 0 'lReg("Gastos").Value
      .Campo("Iva").Valor = .Campo("Iva").Valor + 0 'lReg("Iva").Value
      .Campo("Monto_Operacion").Valor = .Campo("Monto_Operacion").Valor + lReg("valor").Value + 0 '((lReg("Iva").Value + lReg("Gastos").Value + lReg("derechobolsa").Value + lReg("Comision").Value) * IIf(lTipoOperacion = gcTipoOperacion_Egreso, -1, 1))
      
      Call .Agregar_Operaciones_Detalle(lcOperacion_Detalle)
    End With
    
    
  
    If Not lcOperacion.Guardar(lId_Caja_Cuenta) Then
      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
                        , cMsgErr _
                        , lcOperacion.ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
    
'    lId_Operacion = lcOperacion.Campo("Id_Operacion").Valor
'
'    If Not lcOperacion.Confirmar(lId_Caja_Cuenta) Then
'      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
'                        , cMsgErr _
'                        , lcOperacion.ErrMsg _
'                        , pConLog:=True)
'      GoTo ExitProcedure
'    End If
    
    lMov_Cargados = lMov_Cargados + 1
    
SiguienteNemo:

  Next
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, cMsgErr, Err.Description, pConLog:=True)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Set lcOperacion = Nothing
  Set lcCaja_Cuenta = Nothing
  Set lcAlias = Nothing
  
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & lMov_Cargados & " Movimientos de fondos mutuos cargados.", pConLog:=True)
End Sub


Private Sub Sub_Importar_Caja(pDB As DLL_DB.Class_Conect, pId_Cuenta, pCuentaPlanes, pfCuenta)
Dim lcCaja_Cuenta       As Class_Cajas_Cuenta
Dim lcCargo_Abono       As Class_Cargos_Abonos
Dim lcAporteRescate     As Class_APORTE_RESCATE_CUENTA
Dim lCursor             As hRecord
Dim lReg                As hFields
'------------------------------------------------------
Dim lTipoOperacion    As String
Dim lId_Moneda        As Double
Dim lId_Caja_Cuenta   As String
Dim lFecha_Operacion  As Date
Dim lDsc_Cargo_Abono  As String
Dim lRetencion        As Double
Dim lMonto            As Double
Dim lCod_Origen_Mov_Caja As String
Dim lestado           As String
'------------------------------------------------------
Dim lSql              As String
Dim lMov_Cargados     As Long
'------------------------------------------------------
Const cTipOp_Compra = "2"
Const cTipOp_Venta = "1"
Const cMsgErr = "Problemas en la lectura hacia planes en cajas."
  
On Error GoTo ErrProcedure

  BarraProceso.Value = 0
  
  lSql = "select CARTERA, i_e AS TIPO_MOVIMIENTO " & _
         "      , MONTO, COD_TRASP, TIP_TRAN, TIP_TRAN + ' / ' + GLOSA AS GLOSA " & _
         "      , FECHA_REAL AS FECHA_MOVIMIENTO, FECHA_REAL AS FECHA_LIQUIDACION " & _
         "      , MONEDA_F AS MONEDA, CUENTA " & _
         " From caja " & _
         " where cartera = '" & pCuentaPlanes & "' " & _
         "   and FECHA_REAL >= convert(datetime, '" & Format(pfCuenta("fecha_operativa").Value, "yyyymmdd") & "', 112)" & _
         "   and STATUS = 'REAL'" & _
         " ORDER BY CARTERA, FECHA_REAL "
'         " AND PAIS = 'CL' " & _


  pDB.Parametros.Clear
  If Not pDB.EjecutaSelect(lSql) Then
    Call Fnt_MsgError(eLS_ErrSystem _
                      , cMsgErr _
                      , pDB.ErrMsg _
                      , pConLog:=True)
    GoTo ExitProcedure
  End If
  
  Set lCursor = pDB.Parametros("cursor").Valor
    
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & "Se encontraron " & lCursor.Count & " movimientos de caja.", pConLog:=True)
  
  If lCursor.Count > 0 Then
    BarraProceso.Max = lCursor.Count
  End If
  
  
  lMov_Cargados = 0
  lestado = "L"
  
  For Each lReg In lCursor
    Call gRelogDB.AvanzaRelog
    BarraProceso.Value = BarraProceso.Value + 1
    Call Sub_StatusBar("Cargando movimiento " & Format(BarraProceso.Value, "#,###") & " de " & Format(BarraProceso.Max, "#,###"))
    DoEvents
    
    Select Case lReg("moneda").Value
      Case 2
        lId_Moneda = 2
      Case 4
        lId_Moneda = 1 'peso
      Case Else
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "Moneda """ & lReg("moneda").Value & """ no reconocida, es imposible realizar el movimiento de caja.", pConLog:=True)
        GoTo ExitProcedure
    End Select
    
    lId_Caja_Cuenta = ""
    Set lcCaja_Cuenta = New Class_Cajas_Cuenta
    With lcCaja_Cuenta
      .Campo("id_cuenta").Valor = pId_Cuenta
      .Campo("id_moneda").Valor = lId_Moneda
      .Campo("cod_mercado").Valor = "N" 'Nacional
      If Not .Buscar_Caja_Para_Invertir() Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , cMsgErr _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ExitProcedure
      End If
      
      If .Cursor.Count = 0 Then
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe una caja definida para la moneda PESO, es imposible realizar el movimiento de caja.", pConLog:=True)
        GoTo ExitProcedure
      End If
      
      lId_Caja_Cuenta = .Cursor(1)("Id_Caja_Cuenta").Value
    End With
    
    Select Case Trim(lReg("TIP_TRAN").Value)
      Case "APORTE CAPITAL", "RETIRO  CAPITAL"
        lCod_Origen_Mov_Caja = gcOrigen_Mov_Caja_APO_RES
'      Case "COMPRA", "COMPRA ACCIONES", "COMPRA DE IRF", "COMPRA F.MUT.", "COMPRA F.MUTUO", _
'           "RESCATE F.MUT.", "RESCATE F.MUTUO", "VENTA", "VENTA ACCIONES", "VENTA DE IRF", _
'           "VENTA RENTA FIJ"
'        GoTo SiguienteNemo
      Case Else
        lCod_Origen_Mov_Caja = gcOrigen_Mov_Caja_CargoAbono
    End Select
  
    lFecha_Operacion = lReg("FECHA_MOVIMIENTO").Value
    
    lDsc_Cargo_Abono = lReg("GLOSA").Value
    lRetencion = lReg("FECHA_LIQUIDACION").Value - lFecha_Operacion
    lMonto = Abs(lReg("MONTO").Value)
  
    If lCod_Origen_Mov_Caja = gcOrigen_Mov_Caja_APO_RES Then
      lTipoOperacion = IIf(lReg("TIPO_MOVIMIENTO").Value = cTipOp_Venta, gcTipoOperacion_Rescate, gcTipoOperacion_Aporte)
      Set lcAporteRescate = New Class_APORTE_RESCATE_CUENTA
      With lcAporteRescate
        .Campo("id_Apo_Res_Cuenta").Valor = cNewEntidad
        .Campo("cod_Medio_Pago").Valor = gcMedios_Pago_Cash
        .Campo("id_Banco").Valor = Null
        .Campo("id_Caja_Cuenta").Valor = lId_Caja_Cuenta
        .Campo("id_Cuenta").Valor = pId_Cuenta
        .Campo("dsc_Apo_Res_Cuenta").Valor = lDsc_Cargo_Abono
        .Campo("flg_Tipo_Movimiento").Valor = lTipoOperacion
        .Campo("fecha_Movimiento").Valor = lFecha_Operacion
        .Campo("retencion").Valor = lRetencion
        .Campo("monto").Valor = lMonto
        
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas al grabar el " & IIf(lTipoOperacion = gcTipoOperacion_Rescate, "Rescate", "Aporte") & "." _
                            , .ErrMsg _
                            , True)
          GoTo ExitProcedure
        End If
      End With
      Set lcAporteRescate = Nothing
    Else
      lTipoOperacion = IIf(lReg("TIPO_MOVIMIENTO").Value = cTipOp_Venta, gcTipoOperacion_Cargo, gcTipoOperacion_Abono)
      Set lcCargo_Abono = New Class_Cargos_Abonos
      With lcCargo_Abono
        .Campo("id_cargo_abono").Valor = cNewEntidad
        .Campo("Cod_Origen_Mov_Caja").Valor = lCod_Origen_Mov_Caja
        .Campo("Id_Mov_Caja").Valor = cNewEntidad
        .Campo("id_Caja_Cuenta").Valor = lId_Caja_Cuenta
        .Campo("id_Cuenta").Valor = pId_Cuenta
        .Campo("Dsc_Cargo_Abono").Valor = lDsc_Cargo_Abono
        .Campo("Flg_Tipo_Cargo").Valor = lTipoOperacion
        .Campo("fecha_Movimiento").Valor = lFecha_Operacion
        .Campo("retencion").Valor = lRetencion
        .Campo("monto").Valor = lMonto
        .Campo("Id_Moneda").Valor = lId_Moneda
        .Campo("FLG_TIPO_ORIGEN").Valor = gcFlg_Tipo_Origen_Migracion 'Todos los movimientos son conciderados como migraci�n
        .Campo("cod_estado").Valor = lestado
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG _
                            , cMsgErr _
                            , .ErrMsg _
                            , pConLog:=True)
          GoTo ExitProcedure
        End If
      End With
      Set lcCargo_Abono = Nothing
    End If
    
    lMov_Cargados = lMov_Cargados + 1
    
SiguienteNemo:

  Next
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, cMsgErr, Err.Description, pConLog:=True)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Set lcCaja_Cuenta = Nothing
  Set lcCargo_Abono = Nothing
  
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & lMov_Cargados & " Movimientos de caja cargados.", pConLog:=True)
End Sub

Private Sub Sub_Importar_Bonos(pDB As DLL_DB.Class_Conect, pId_Cuenta, pCuentaPlanes, pfCuenta)
Const cTipOp_Compra = "C"
Const cTipOp_Venta = "V"

  Call Sub_ImportarBonos_x_Operacion(pDB, pId_Cuenta, cTipOp_Compra, pCuentaPlanes, pfCuenta)
  Call Sub_ImportarBonos_x_Operacion(pDB, pId_Cuenta, cTipOp_Venta, pCuentaPlanes, pfCuenta)
End Sub


Private Sub Sub_ImportarBonos_x_Operacion(pDB As DLL_DB.Class_Conect, pId_Cuenta, pTipOp, pCuentaPlanes, pfCuenta)
Dim lcOperacion         As Class_Operaciones
Dim lcOperacion_Detalle As Class_Operaciones_Detalle
Dim lcCaja_Cuenta       As Class_Cajas_Cuenta
Dim lcNemotecnico       As Class_Nemotecnicos
Dim lcRel_Conversion    As Object
Dim lcAlias             As Object
Dim lcMov_Activo        As Class_Mov_Activos
Dim lCursor             As hRecord
Dim lReg                As hFields
'------------------------------------------------------
Dim lTipoOperacion          As String
Dim lId_Moneda              As Double
Dim lId_Caja_Cuenta         As String
Dim lFecha_Operacion        As Date
Dim lId_Operacion           As Double
Dim lId_Nemotecnico         As Double
Dim lId_Moneda_Transaccion  As Double
Dim lFecha_Liquidacion      As Date
Dim lId_Operacion_detalle
Dim lId_Mov_Activo_Compra
'------------------------------------------------------
Dim lAlias_DetOperacion     As String
Dim lId_Origen              As Double
Dim lId_Tipo_Conversion     As Double
'------------------------------------------------------
Dim lSql                    As String
Dim lMov_Cargados           As Long
Dim lMensaje                As String
'------------------------------------------------------
Const cTipOp_Compra = "C"
Const cTipOp_Venta = "V"
Const cMsgErr = "Problemas en la lectura hacia planes en bonos."
  
On Error GoTo ErrProcedure

  BarraProceso.Value = 0
  
  'Buscando alias de los detalle operaciones
  If Not Fnt_BuscaCodigosAlias(pId_Origen:=lId_Origen, pId_Tipo_Conversion:=lId_Tipo_Conversion) Then
    GoTo ExitProcedure
  End If
  
  Set lcAlias = Fnt_CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  
  'lSql = "select cartera, tip_instru, n_bolsa as nemotecnico, TIP_OPERA, emisor, fecha as fecha_operacion " & _
         "   , fecha_p as fecha_liquidacion, fec_ven as fecha_vencimiento, fec_emis as fecha_emision " & _
         "   , comision, iva, derechobolsa, gastos, face_value as nominales, tasa, principal as monto " & _
         "   , moneda, NRO_OPERA, COR_OPERA , NRO_OPERARELA, COR_OPERARELA " & _
         "   From ren_fija " & _
         " where cartera = '" & pCuentaPlanes & "' " & _
         "     and TIP_OPERA = '" & pTipOp & "' " & _
         "    and tip_instru in ('BONO', 'LH', 'BCP', 'BR', 'LH *', 'BCU', 'BT', 'LH&', 'PRC', 'PRD', 'LH*') " & _
         "   and FECHA >= convert(datetime, '" & Format(pfCuenta("fecha_operativa").Value, "yyyymmdd") & "', 112)" & _
         "   and FECHA <= convert(datetime, '20080229', 112)" & _
         " order by cartera, fecha, n_bolsa "
'         "    --AND pais = 'CL' " & _


' SIN FECHA OPERATIVA
  lSql = "select cartera, tip_instru, n_bolsa as nemotecnico, TIP_OPERA, emisor, fecha as fecha_operacion " & _
         "   , fecha_p as fecha_liquidacion, fec_ven as fecha_vencimiento, fec_emis as fecha_emision " & _
         "   , comision, iva, derechobolsa, gastos, face_value as nominales, tasa, principal as monto " & _
         "   , moneda, NRO_OPERA, COR_OPERA , NRO_OPERARELA, COR_OPERARELA " & _
         "   From ren_fija " & _
         " where cartera = '" & pCuentaPlanes & "' " & _
         "     and TIP_OPERA = '" & pTipOp & "' " & _
         "    and tip_instru in ('BONO', 'LH', 'BCP', 'BR', 'LH *', 'BCU', 'BT', 'LH&', 'PRC', 'PRD', 'LH*') " & _
         "   and FECHA <= convert(datetime, '20080116', 112)" & _
         " order by cartera, fecha, n_bolsa "


  pDB.Parametros.Clear
  If Not pDB.EjecutaSelect(lSql) Then
    Call Fnt_MsgError(eLS_ErrSystem _
                      , cMsgErr _
                      , pDB.ErrMsg _
                      , pConLog:=True)
    GoTo ExitProcedure
  End If
  
  Set lCursor = pDB.Parametros("cursor").Valor
    
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & "Se encontraron " & lCursor.Count & " movimientos de bonos de tipo operacion '" & pTipOp & "'.", pConLog:=True)
  
  If lCursor.Count > 0 Then
    BarraProceso.Max = lCursor.Count
  End If
  
  lMov_Cargados = 0
  lId_Moneda_Transaccion = 1 'peso
  
  Set lcOperacion = Nothing
  For Each lReg In lCursor
    Call gRelogDB.AvanzaRelog
    BarraProceso.Value = BarraProceso.Value + 1
    Call Sub_StatusBar("Cargando movimiento " & Format(BarraProceso.Value, "#,###") & " de " & Format(BarraProceso.Max, "#,###"))
    DoEvents
    
    Select Case lReg("moneda").Value
      Case 1 'UF
        lId_Moneda = 4 'uf
      Case 4 'peso
        lId_Moneda = 1 'peso
    End Select
    
    lTipoOperacion = IIf(lReg("TIP_OPERA").Value = cTipOp_Venta, gcTipoOperacion_Egreso, gcTipoOperacion_Ingreso)
    lFecha_Operacion = lReg("fecha_operacion").Value
    lFecha_Liquidacion = lReg("fecha_liquidacion").Value
    lId_Mov_Activo_Compra = Null
  
    If lTipoOperacion = gcTipoOperacion_Egreso Then
      'Registra el numero de operacion con su detallenta
      lAlias_DetOperacion = Trim(pCuentaPlanes) & "-C-" & Trim(lReg("NRO_OPERARELA").Value) & "-" & Trim(lReg("COR_OPERARELA").Value)
      
      lId_Operacion_detalle = ""
      lId_Operacion_detalle = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                                      , pCodigoCSBPI:=cTabla_Operacion_Detalle _
                                                      , pValor:=lAlias_DetOperacion)
          
      If IsNull(lId_Operacion_detalle) Then
'        MsgBox "ID_Operacion_Detalle: " & NVL(lId_Operacion_detalle, "null") & vbCr & _
'               "LIN: " & lReg.Index & _
'               "lAlias_DetOperacion :" & lAlias_DetOperacion
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe el movimiento de compra """ & lAlias_DetOperacion & """.", pConLog:=True)
        GoTo SiguienteNemo
      End If
      
      'Busca el mov_activo que esta relacionado con la operacion de detalle
      Set lcMov_Activo = New Class_Mov_Activos
      With lcMov_Activo
        .Campo("id_operacion_detalle").Valor = lId_Operacion_detalle
        If Not .Buscar Then
          Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No se puede buscar en la MOV_ACTIVO" & vbCr & vbCr & .ErrMsg, pConLog:=True)
          GoTo ExitProcedure
        End If
        
        If .Cursor.Count <= 0 Then
          Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe la operacion detalle relacionada con la operacion """ & lAlias_DetOperacion & """.", pConLog:=True)
          GoTo ExitProcedure
        End If
        
        lId_Mov_Activo_Compra = .Cursor(1)("id_mov_activo").Value
      End With
    End If
  
    Set lcOperacion = New Class_Operaciones
    With lcOperacion
      .Campo("Id_Operacion").Valor = cNewEntidad
      .Campo("Id_Cuenta").Valor = pId_Cuenta
      .Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Custodia
      '.Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Instruccion
      .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente
      .Campo("Id_Contraparte").Valor = Null
      .Campo("Id_Representante").Valor = Null
      .Campo("Id_Moneda_Operacion").Valor = 1 'peso
      .Campo("Cod_Producto").Valor = gcPROD_RF_NAC 'Esto va en duro
      .Campo("Cod_Instrumento").Valor = gcINST_BONOS_NAC  'Esto va en duro
      .Campo("Flg_Tipo_Movimiento").Valor = lTipoOperacion
      .Campo("Fecha_Operacion").Valor = lFecha_Operacion
      .Campo("Fecha_Vigencia").Valor = lFecha_Operacion
      .Campo("Fecha_Liquidacion").Valor = lFecha_Liquidacion
      .Campo("Dsc_Operacion").Valor = "" 'pDsc_Operacion
      .Campo("Id_Trader").Valor = Null
      .Campo("Porc_Comision").Valor = 0 'pPorc_Comision
      .Campo("Comision").Valor = 0
      .Campo("Derechos").Valor = 0
      .Campo("Gastos").Valor = 0
      .Campo("Iva").Valor = 0
      .Campo("Monto_Operacion").Valor = 0
      .Campo("Flg_Limite_Precio").Valor = cTipo_Precio_Mercado
    End With
  
    'Crear el nemotecnico si no existe
    If Not Fnt_CreaNemotecnico_Bonos(pCodigoEmisor:=Trim(lReg("emisor").Value) _
                                  , pFecha_Vencimiento:=lReg("fecha_vencimiento").Value _
                                  , pId_Moneda:=lId_Moneda _
                                  , pId_Moneda_Transaccion:=lId_Moneda_Transaccion _
                                  , pTasa_Emision:=lReg("tasa").Value _
                                  , pFecha_Emision:=NVL(lReg("fecha_emision").Value, lFecha_Operacion) _
                                  , pId_Nemotecnico:=lId_Nemotecnico _
                                  , pNemotecnico:=Trim(lReg("Nemotecnico").Value) _
                                  , pMensaje:=lMensaje _
                                  , pGrilla:=Grilla) Then
      Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No se pudo crear el nemotecnico """ & Trim(lReg("Nemotecnico").Value) & """." & vbLf & vbLf & lMensaje, pConLog:=True)
      GoTo SiguienteNemo
    End If
    
    Rem Busca que el nemotecnicos exista en el sistema
    Set lcNemotecnico = New Class_Nemotecnicos
    With lcNemotecnico
      .Campo("nemotecnico").Valor = Trim(lReg("Nemotecnico").Value)
      If Not .Buscar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , cMsgErr _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ExitProcedure
      End If
    
      If .Cursor.Count <= 0 Then
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No esta definido el nemotecnico """ & Trim(lReg("Nemotecnico").Value) & """, favor de crear", pConLog:=True)
        GoTo SiguienteNemo
      End If
    
      lId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
    End With
  
    Set lcOperacion_Detalle = New Class_Operaciones_Detalle
    With lcOperacion_Detalle
      .Campo("Id_Nemotecnico").Valor = lId_Nemotecnico
      .Campo("Cantidad").Valor = lReg("nominales").Value
      .Campo("Precio").Valor = lReg("tasa").Value
      .Campo("Id_Moneda_Pago").Valor = lId_Moneda_Transaccion
      .Campo("Monto_Pago").Valor = lReg("monto").Value
      .Campo("Flg_Vende_Todo").Valor = cFlg_No_Vende_Todo
      .Campo("id_mov_activo_compra").Valor = lId_Mov_Activo_Compra
    End With
    
    With lcOperacion
      .Campo("Comision").Valor = .Campo("Comision").Valor + lReg("Comision").Value
      .Campo("Derechos").Valor = .Campo("Derechos").Valor + lReg("derechobolsa").Value
      .Campo("Gastos").Valor = .Campo("Gastos").Valor + lReg("Gastos").Value
      .Campo("Iva").Valor = .Campo("Iva").Valor + lReg("Iva").Value
      .Campo("Monto_Operacion").Valor = .Campo("Monto_Operacion").Valor + lReg("monto").Value + ((lReg("Iva").Value + lReg("Gastos").Value + lReg("derechobolsa").Value + lReg("Comision").Value) * IIf(lTipoOperacion = gcTipoOperacion_Egreso, -1, 1))
      
      Call .Agregar_Operaciones_Detalle(lcOperacion_Detalle)
    End With
    
    lId_Caja_Cuenta = ""
  
    Set lcCaja_Cuenta = New Class_Cajas_Cuenta
    With lcCaja_Cuenta
      .Campo("id_cuenta").Valor = pId_Cuenta
      .Campo("id_moneda").Valor = lId_Moneda_Transaccion
      If Not .Buscar_Caja_Para_Invertir() Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , cMsgErr _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ExitProcedure
      End If
      
      If .Cursor.Count = 0 Then
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe una caja definida para la moneda PESO, es imposible realizar el movimiento de caja.", pConLog:=True)
        GoTo SiguienteNemo
      End If
      
      lId_Caja_Cuenta = .Cursor(1)("Id_Caja_Cuenta").Value
    End With
  
    If Not lcOperacion.Guardar(lId_Caja_Cuenta) Then
      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
                        , cMsgErr _
                        , lcOperacion.ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
    
'    lId_Operacion = lcOperacion.Campo("Id_Operacion").Valor
'
'    If Not lcOperacion.Confirmar(lId_Caja_Cuenta) Then
'      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
'                        , cMsgErr _
'                        , lcOperacion.ErrMsg _
'                        , pConLog:=True)
'      GoTo ExitProcedure
'    End If
'
    'Registra el numero de operacion con su detalle
    lAlias_DetOperacion = Trim(pCuentaPlanes) & "-" & Trim(lReg("TIP_OPERA").Value) & "-" & Trim(lReg("NRO_OPERA").Value) & "-" & Trim(lReg("COR_OPERA").Value)
    
    lId_Operacion_detalle = lcOperacion.Detalles(1).Campo("id_operacion_detalle").Valor
    
    Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones) 'New Class_Rel_Conversiones
    With lcRel_Conversion
      Set .gDB = gDB
      .Campo("id_entidad").Valor = lId_Operacion_detalle
      .Campo("id_tipo_conversion").Valor = lId_Tipo_Conversion
      .Campo("id_origen").Valor = lId_Origen
      .Campo("Valor").Valor = lAlias_DetOperacion
      If Not .Guardar Then
        MsgBox "Problemas en el alias de la operacion detalle." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        GoTo ErrProcedure
      End If
    End With
    Set lcRel_Conversion = Nothing
    
    lMov_Cargados = lMov_Cargados + 1
    
SiguienteNemo:

  Next
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, cMsgErr, Err.Description, pConLog:=True)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Set lcOperacion = Nothing
  Set lcCaja_Cuenta = Nothing
  Set lcRel_Conversion = Nothing
  
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & lMov_Cargados & " Movimientos de bonos cargados.", pConLog:=True)
End Sub


Private Sub Sub_Importar_Depositos(pDB As DLL_DB.Class_Conect, pId_Cuenta, pCuentaPlanes, pfCuenta)
Dim lcOperacion         As Class_Operaciones
Dim lcOperacion_Detalle As Class_Operaciones_Detalle
Dim lcCaja_Cuenta       As Class_Cajas_Cuenta
Dim lcNemotecnico       As Class_Nemotecnicos
Dim lcRel_Conversion    As Object
Dim lcAlias             As Object
Dim lcMov_Activo        As Class_Mov_Activos
Dim lCursor             As hRecord
Dim lReg                As hFields
'------------------------------------------------------
Dim lTipoOperacion    As String
Dim lId_Moneda_Pago   As Double
Dim lId_Caja_Cuenta   As String
Dim lFecha_Operacion  As Date
Dim lId_Operacion     As Double
Dim lId_Nemotecnico   As Double
Dim lNemotecnico      As String
Dim lFecha_Liquidacion As Date
'------------------------------------------------------
Dim lId_Moneda_Deposito As Double
Dim lId_Operacion_detalle
Dim lId_Mov_Activo_Compra
'------------------------------------------------------
Dim lAlias_DetOperacion     As String
Dim lId_Origen              As Double
Dim lId_Tipo_Conversion     As Double
'------------------------------------------------------
Dim lSql              As String
Dim lMov_Cargados     As Long
Dim lMensaje          As String
'------------------------------------------------------
Const cTipOp_Compra = "C"
Const cTipOp_Venta = "V"
Const cMsgErr = "Problemas en la lectura hacia planes en depositos."
  
On Error GoTo ErrProcedure

  BarraProceso.Value = 0
  
  'Buscando alias de los detalle operaciones
  If Not Fnt_BuscaCodigosAlias(pId_Origen:=lId_Origen, pId_Tipo_Conversion:=lId_Tipo_Conversion) Then
    GoTo ExitProcedure
  End If
  
  Set lcAlias = Fnt_CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  
'  lSql = "select cartera, tip_instru, n_bolsa as nemotecnico, TIP_OPERA, emisor, fecha as fecha_operacion " & _
         "   , fecha_p as fecha_liquidacion, fec_ven as fecha_vencimiento, fec_emis as fecha_emision " & _
         "   , comision, iva, derechobolsa, gastos, moneda, face_value as nominales, tasa, principal as monto " & _
         "   , NRO_OPERA, COR_OPERA , NRO_OPERARELA, COR_OPERARELA " & _
         "   From ren_fija " & _
         " where cartera = '" & pCuentaPlanes & "' " & _
         "    AND pais = 'CL' " & _
         "    and tip_instru IN ('DPD', 'DPF', 'DPR', 'SN') " & _
         "   and FECHA >= convert(datetime, '" & Format(pfCuenta("fecha_operativa").Value, "yyyymmdd") & "', 112)" & _
         "   and FECHA <= convert(datetime, '20080229', 112)" & _
         " order by cartera, fecha, n_bolsa "

'SIN FECHA OPERATIVA
  lSql = "select cartera, tip_instru, n_bolsa as nemotecnico, TIP_OPERA, emisor, fecha as fecha_operacion " & _
         "   , fecha_p as fecha_liquidacion, fec_ven as fecha_vencimiento, fec_emis as fecha_emision " & _
         "   , comision, iva, derechobolsa, gastos, moneda, face_value as nominales, tasa, principal as monto " & _
         "   , NRO_OPERA, COR_OPERA , NRO_OPERARELA, COR_OPERARELA " & _
         "   From ren_fija " & _
         " where cartera = '" & pCuentaPlanes & "' " & _
         "    AND pais = 'CL' " & _
         "    and tip_instru IN ('DPD', 'DPF', 'DPR', 'SN') " & _
         "   and FECHA <= convert(datetime, '20080116', 112)" & _
         " order by cartera, fecha, n_bolsa "


  pDB.Parametros.Clear
  If Not pDB.EjecutaSelect(lSql) Then
    Call Fnt_MsgError(eLS_ErrSystem _
                      , cMsgErr _
                      , pDB.ErrMsg _
                      , pConLog:=True)
    GoTo ExitProcedure
  End If
  
  Set lCursor = pDB.Parametros("cursor").Valor
    
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & "Se encontraron " & lCursor.Count & " movimientos de depositos.", pConLog:=True)
  
  If lCursor.Count > 0 Then
    BarraProceso.Max = lCursor.Count
  End If
  
  lMov_Cargados = 0
  
  Set lcOperacion = Nothing
  For Each lReg In lCursor
    Call gRelogDB.AvanzaRelog
    BarraProceso.Value = BarraProceso.Value + 1
    Call Sub_StatusBar("Cargando movimiento " & Format(BarraProceso.Value, "#,###") & " de " & Format(BarraProceso.Max, "#,###"))
    DoEvents
    
    'para buscar la moneda del deposito
    Select Case Trim(lReg("tip_instru").Value)
      Case "DPF"
        lId_Moneda_Deposito = 1 'peso
        lId_Moneda_Pago = 1
      Case "DPR"
        lId_Moneda_Deposito = 4 'uf
        lId_Moneda_Pago = 1
      Case "DPD"
        lId_Moneda_Deposito = 2 'dolar
        lId_Moneda_Pago = 2
      Case "SN"
        lId_Moneda_Pago = 1
        
        Select Case lReg("moneda").Value
          Case 1
            lId_Moneda_Deposito = 4 'uf
          Case 4
            lId_Moneda_Deposito = 1 'peso
        End Select
    End Select
    
    lTipoOperacion = IIf(lReg("TIP_OPERA").Value = cTipOp_Venta, gcTipoOperacion_Egreso, gcTipoOperacion_Ingreso)
    lFecha_Operacion = lReg("fecha_operacion").Value
    lId_Mov_Activo_Compra = Null
    lFecha_Liquidacion = lReg("fecha_liquidacion").Value
  
    If lTipoOperacion = gcTipoOperacion_Egreso Then
      'Registra el numero de operacion con su detalle
      lAlias_DetOperacion = Trim(pCuentaPlanes) & "-C-" & Trim(lReg("NRO_OPERARELA").Value) & "-" & Trim(lReg("COR_OPERARELA").Value)
      
      lId_Operacion_detalle = ""
      lId_Operacion_detalle = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                                      , pCodigoCSBPI:=cTabla_Operacion_Detalle _
                                                      , pValor:=lAlias_DetOperacion)
          
      If IsNull(lId_Operacion_detalle) Then
'        MsgBox "ID_Operacion_Detalle: " & NVL(lId_Operacion_detalle, "null") & vbCr & _
'               "LIN: " & lReg.Index & _
'               "lAlias_DetOperacion :" & lAlias_DetOperacion
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe el movimiento de compra """ & lAlias_DetOperacion & """.", pConLog:=True)
        GoTo SiguienteNemo
      End If
      
      'Busca el mov_activo que esta relacionado con la operacion de detalle
      Set lcMov_Activo = New Class_Mov_Activos
      With lcMov_Activo
        .Campo("id_operacion_detalle").Valor = lId_Operacion_detalle
        If Not .Buscar Then
          Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No se puede buscar en la MOV_ACTIVO" & vbCr & vbCr & .ErrMsg, pConLog:=True)
          GoTo ExitProcedure
        End If
        
        If .Cursor.Count <= 0 Then
          Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe la operacion detalle relacionada con la operacion """ & lAlias_DetOperacion & """.", pConLog:=True)
          GoTo ExitProcedure
        End If
        
        lId_Mov_Activo_Compra = .Cursor(1)("id_mov_activo").Value
      End With
    End If
  
    If Not Fnt_CreaNemotecnico_Deposito(pCodigoEmisor:=Trim(lReg("emisor").Value) _
                                  , pFecha_Vencimiento:=lReg("fecha_vencimiento").Value _
                                  , PId_Moneda_Deposito:=lId_Moneda_Deposito _
                                  , pId_Moneda_Pago:=lId_Moneda_Pago _
                                  , pTasa_Emision:=lReg("tasa").Value _
                                  , pFecha_Emision:=NVL(lReg("fecha_emision").Value, lFecha_Operacion) _
                                  , pId_Nemotecnico:=lId_Nemotecnico _
                                  , pNemotecnico:=lNemotecnico _
                                  , pMensaje:=lMensaje _
                                  , pGrilla:=Grilla) Then
      Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No se pudo crear el nemotecnico """ & lNemotecnico & """." & vbLf & vbLf & lMensaje, pConLog:=True)
      GoTo SiguienteNemo
    End If
  
    Set lcOperacion = New Class_Operaciones
    With lcOperacion
      .Campo("Id_Operacion").Valor = cNewEntidad
      .Campo("Id_Cuenta").Valor = pId_Cuenta
      .Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Custodia
      '.Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Instruccion
      .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente
      .Campo("Id_Contraparte").Valor = Null
      .Campo("Id_Representante").Valor = Null
      .Campo("Id_Moneda_Operacion").Valor = 1 'peso
      .Campo("Cod_Producto").Valor = gcPROD_RF_NAC 'Esto va en duro
      .Campo("Cod_Instrumento").Valor = gcINST_DEPOSITOS_NAC  'Esto va en duro
      .Campo("Flg_Tipo_Movimiento").Valor = lTipoOperacion
      .Campo("Fecha_Operacion").Valor = lFecha_Operacion
      .Campo("Fecha_Vigencia").Valor = lFecha_Operacion
      .Campo("Fecha_Liquidacion").Valor = lFecha_Liquidacion
      .Campo("Dsc_Operacion").Valor = "" 'pDsc_Operacion
      .Campo("Id_Trader").Valor = Null
      .Campo("Porc_Comision").Valor = 0 'pPorc_Comision
      .Campo("Comision").Valor = 0
      .Campo("Derechos").Valor = 0
      .Campo("Gastos").Valor = 0
      .Campo("Iva").Valor = 0
      .Campo("Monto_Operacion").Valor = 0
      .Campo("Flg_Limite_Precio").Valor = cTipo_Precio_Mercado
    End With
  
    Rem Busca que el nemotecnicos exista en el sistema
    Set lcNemotecnico = New Class_Nemotecnicos
    With lcNemotecnico
      .Campo("nemotecnico").Valor = Trim(lNemotecnico)
      If Not .Buscar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , cMsgErr _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ExitProcedure
      End If
    
      If .Cursor.Count <= 0 Then
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No esta definido el nemotecnico """ & Trim(lReg("Nemotecnico").Value) & """, favor de crear", pConLog:=True)
        GoTo SiguienteNemo
      End If
    
      lId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
    End With
  
    Set lcOperacion_Detalle = New Class_Operaciones_Detalle
    With lcOperacion_Detalle
      .Campo("Id_Nemotecnico").Valor = lId_Nemotecnico
      .Campo("Cantidad").Valor = lReg("nominales").Value
      .Campo("Precio").Valor = lReg("tasa").Value
      '.Campo("TASA").Valor = lReg("tasa").Value
      .Campo("Id_Moneda_Pago").Valor = lId_Moneda_Pago
      .Campo("Monto_Pago").Valor = lReg("monto").Value
      .Campo("Flg_Vende_Todo").Valor = cFlg_No_Vende_Todo
      .Campo("id_mov_activo_compra").Valor = lId_Mov_Activo_Compra
    End With
    
    With lcOperacion
      .Campo("Comision").Valor = .Campo("Comision").Valor + lReg("Comision").Value
      .Campo("Derechos").Valor = .Campo("Derechos").Valor + lReg("derechobolsa").Value
      .Campo("Gastos").Valor = .Campo("Gastos").Valor + lReg("Gastos").Value
      .Campo("Iva").Valor = .Campo("Iva").Valor + lReg("Iva").Value
      .Campo("Monto_Operacion").Valor = .Campo("Monto_Operacion").Valor + lReg("monto").Value + ((lReg("Iva").Value + lReg("Gastos").Value + lReg("derechobolsa").Value + lReg("Comision").Value) * IIf(lTipoOperacion = gcTipoOperacion_Egreso, -1, 1))
      
      Call .Agregar_Operaciones_Detalle(lcOperacion_Detalle)
    End With
    
    lId_Caja_Cuenta = ""
  
    Set lcCaja_Cuenta = New Class_Cajas_Cuenta
    With lcCaja_Cuenta
      .Campo("id_cuenta").Valor = pId_Cuenta
      .Campo("id_moneda").Valor = lId_Moneda_Pago
      If Not .Buscar_Caja_Para_Invertir() Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , cMsgErr _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ExitProcedure
      End If
      
      If .Cursor.Count = 0 Then
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe una caja definida para la moneda PESO, es imposible realizar el movimiento de caja.", pConLog:=True)
        GoTo SiguienteNemo
      End If
      
      lId_Caja_Cuenta = .Cursor(1)("Id_Caja_Cuenta").Value
    End With
  
    If Not lcOperacion.Guardar(lId_Caja_Cuenta) Then
      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
                        , cMsgErr _
                        , lcOperacion.ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
    
'    lId_Operacion = lcOperacion.Campo("Id_Operacion").Valor
'
'    If Not lcOperacion.Confirmar(lId_Caja_Cuenta) Then
'      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
'                        , cMsgErr _
'                        , lcOperacion.ErrMsg _
'                        , pConLog:=True)
'      GoTo ExitProcedure
'    End If
    
    'Registra el numero de operacion con su detalle
    lAlias_DetOperacion = Trim(pCuentaPlanes) & "-" & Trim(lReg("TIP_OPERA").Value) & "-" & Trim(lReg("NRO_OPERA").Value) & "-" & Trim(lReg("COR_OPERA").Value)
    
    lId_Operacion_detalle = lcOperacion.Detalles(1).Campo("id_operacion_detalle").Valor
    
    Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones) 'New Class_Rel_Conversiones
    With lcRel_Conversion
      Set .gDB = gDB
      .Campo("id_entidad").Valor = lId_Operacion_detalle
      .Campo("id_tipo_conversion").Valor = lId_Tipo_Conversion
      .Campo("id_origen").Valor = lId_Origen
      .Campo("Valor").Valor = lAlias_DetOperacion
      If Not .Guardar Then
        MsgBox "Problemas en el alias de la operacion detalle." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        GoTo ErrProcedure
      End If
    End With
    Set lcRel_Conversion = Nothing
    
    lMov_Cargados = lMov_Cargados + 1
    
SiguienteNemo:

  Next
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, cMsgErr, Err.Description, pConLog:=True)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Set lcOperacion = Nothing
  Set lcCaja_Cuenta = Nothing
  
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & lMov_Cargados & " Movimientos de depositos cargados.", pConLog:=True)
End Sub

Private Sub Sub_Importar_Pactos(pDB As DLL_DB.Class_Conect, pId_Cuenta, pCuentaPlanes, pfCuenta)
Dim lcOperacion         As Class_Operaciones
Dim lcOperacion_Detalle As Class_Operaciones_Detalle
Dim lcCaja_Cuenta       As Class_Cajas_Cuenta
Dim lcNemotecnico       As Class_Nemotecnicos
Dim lcRel_Conversion    As Object
Dim lcAlias             As Object
Dim lcMov_Activo        As Class_Mov_Activos
Dim lCursor             As hRecord
Dim lReg                As hFields
'------------------------------------------------------
Dim lTipoOperacion    As String
Dim lId_Moneda_Pago   As Double
Dim lId_Caja_Cuenta   As String
Dim lFecha_Operacion  As Date
Dim lId_Operacion     As Double
Dim lId_Nemotecnico   As Double
Dim lNemotecnico      As String
Dim lFecha_Liquidacion  As Date
'------------------------------------------------------
Dim lId_Moneda_Deposito As Double
Dim lId_Operacion_detalle
Dim lId_Mov_Activo_Compra
'------------------------------------------------------
Dim lAlias_DetOperacion     As String
Dim lId_Origen              As Double
Dim lId_Tipo_Conversion     As Double
'------------------------------------------------------
Dim lSql              As String
Dim lMov_Cargados     As Long
Dim lMensaje          As String
'------------------------------------------------------
Const cTipOp_Compra = "C"
Const cTipOp_Venta = "V"
Const cMsgErr = "Problemas en la lectura hacia planes en pactos."
  
On Error GoTo ErrProcedure

  BarraProceso.Value = 0
  
  'Buscando alias de los detalle operaciones
  If Not Fnt_BuscaCodigosAlias(pId_Origen:=lId_Origen, pId_Tipo_Conversion:=lId_Tipo_Conversion) Then
    GoTo ExitProcedure
  End If
  
  Set lcAlias = Fnt_CreateObject(cDLL_Alias)
  Set lcAlias.gDB = gDB
  
  'lSql = "select cartera, tip_instru, n_bolsa as nemotecnico, TIP_OPERA, emisor, fecha as fecha_operacion " & _
         "   , fecha_p as fecha_liquidacion, fec_ven as fecha_vencimiento, fec_emis as fecha_emision " & _
         "   , comision, iva, derechobolsa, gastos, face_value as nominales, tasa, principal as monto " & _
         "   , NRO_OPERA, COR_OPERA , NRO_OPERARELA, COR_OPERARELA " & _
         "   From ren_fija " & _
         " where cartera = '" & pCuentaPlanes & "' " & _
         "    AND pais = 'CL' " & _
         "    and tip_instru IN ('PACT') " & _
         "   and FECHA >= convert(datetime, '" & Format(pfCuenta("fecha_operativa").Value, "yyyymmdd") & "', 112)" & _
         "   and FECHA >= convert(datetime, '20080229', 112)" & _
         " order by cartera, fecha, n_bolsa "

'SIN FECHA OPERATIVA
  lSql = "select cartera, tip_instru, n_bolsa as nemotecnico, TIP_OPERA, emisor, fecha as fecha_operacion " & _
         "   , fecha_p as fecha_liquidacion, fec_ven as fecha_vencimiento, fec_emis as fecha_emision " & _
         "   , comision, iva, derechobolsa, gastos, face_value as nominales, tasa, principal as monto " & _
         "   , NRO_OPERA, COR_OPERA , NRO_OPERARELA, COR_OPERARELA " & _
         "   From ren_fija " & _
         " where cartera = '" & pCuentaPlanes & "' " & _
         "    AND pais = 'CL' " & _
         "    and tip_instru IN ('PACT') " & _
         "   and FECHA <= convert(datetime, '20080116', 112)" & _
         " order by cartera, fecha, n_bolsa "


  pDB.Parametros.Clear
  If Not pDB.EjecutaSelect(lSql) Then
    Call Fnt_MsgError(eLS_ErrSystem _
                      , cMsgErr _
                      , pDB.ErrMsg _
                      , pConLog:=True)
    GoTo ExitProcedure
  End If
  
  Set lCursor = pDB.Parametros("cursor").Valor
    
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & "Se encontraron " & lCursor.Count & " movimientos de pactos.", pConLog:=True)
  
  If lCursor.Count > 0 Then
    BarraProceso.Max = lCursor.Count
  End If
  
  lMov_Cargados = 0
  
  Set lcOperacion = Nothing
  For Each lReg In lCursor
    Call gRelogDB.AvanzaRelog
    BarraProceso.Value = BarraProceso.Value + 1
    Call Sub_StatusBar("Cargando movimiento " & Format(BarraProceso.Value, "#,###") & " de " & Format(BarraProceso.Max, "#,###"))
    DoEvents
    
    'para buscar la moneda del deposito
    Select Case Trim(lReg("tip_instru").Value)
      Case "PACT"
        lId_Moneda_Deposito = 1 'peso
        lId_Moneda_Pago = 1
      Case "PRD", "PRC", "PAGU"
        lId_Moneda_Deposito = 4 'uf
        lId_Moneda_Pago = 1
    End Select
    
    lTipoOperacion = IIf(lReg("TIP_OPERA").Value = cTipOp_Venta, gcTipoOperacion_Egreso, gcTipoOperacion_Ingreso)
    lFecha_Operacion = lReg("fecha_operacion").Value
    lFecha_Liquidacion = lReg("fecha_liquidacion").Value
    
    lId_Mov_Activo_Compra = Null
    If lTipoOperacion = gcTipoOperacion_Egreso Then
      'Registra el numero de operacion con su detalle
      lAlias_DetOperacion = Trim(pCuentaPlanes) & "-C-" & Trim(lReg("NRO_OPERARELA").Value) & "-" & Trim(lReg("COR_OPERARELA").Value)
      
      lId_Operacion_detalle = ""
      lId_Operacion_detalle = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                                      , pCodigoCSBPI:=cTabla_Operacion_Detalle _
                                                      , pValor:=lAlias_DetOperacion)
          
      If IsNull(lId_Operacion_detalle) Then
'        MsgBox "ID_Operacion_Detalle: " & NVL(lId_Operacion_detalle, "null") & vbCr & _
'               "LIN: " & lReg.Index & _
'               "lAlias_DetOperacion :" & lAlias_DetOperacion
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe el movimiento de compra """ & lAlias_DetOperacion & """.", pConLog:=True)
        GoTo SiguienteNemo
      End If
      
      'Busca el mov_activo que esta relacionado con la operacion de detalle
      Set lcMov_Activo = New Class_Mov_Activos
      With lcMov_Activo
        .Campo("id_operacion_detalle").Valor = lId_Operacion_detalle
        If Not .Buscar Then
          Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No se puede buscar en la MOV_ACTIVO" & vbCr & vbCr & .ErrMsg, pConLog:=True)
          GoTo ExitProcedure
        End If
        
        If .Cursor.Count <= 0 Then
          Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe la operacion detalle relacionada con la operacion """ & lAlias_DetOperacion & """.", pConLog:=True)
          GoTo ExitProcedure
        End If
        
        lId_Mov_Activo_Compra = .Cursor(1)("id_mov_activo").Value
      End With
    End If
    
    If Not Fnt_CreaNemotecnico_Pacto(pCodigoEmisor:=Trim(lReg("emisor").Value) _
                                  , pFecha_Vencimiento:=lReg("fecha_vencimiento").Value _
                                  , PId_Moneda_Deposito:=lId_Moneda_Deposito _
                                  , pId_Moneda_Pago:=lId_Moneda_Pago _
                                  , pTasa_Emision:=lReg("tasa").Value _
                                  , pFecha_Emision:=NVL(lReg("fecha_emision").Value, lFecha_Operacion) _
                                  , pId_Nemotecnico:=lId_Nemotecnico _
                                  , pNemotecnico:=lNemotecnico _
                                  , pMensaje:=lMensaje _
                                  , pGrilla:=Grilla) Then
      Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No se pudo crear el nemotecnico """ & lNemotecnico & """." & vbLf & vbLf & lMensaje, pConLog:=True)
      GoTo SiguienteNemo
    End If
  
    Set lcOperacion = New Class_Operaciones
    With lcOperacion
      .Campo("Id_Operacion").Valor = cNewEntidad
      .Campo("Id_Cuenta").Valor = pId_Cuenta
      .Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Custodia
      '.Campo("Cod_Tipo_Operacion").Valor = gcOPERACION_Instruccion
      .Campo("Cod_Estado").Valor = cCod_Estado_Pendiente
      .Campo("Id_Contraparte").Valor = Null
      .Campo("Id_Representante").Valor = Null
      .Campo("Id_Moneda_Operacion").Valor = 1 'peso
      .Campo("Cod_Producto").Valor = gcPROD_RF_NAC 'Esto va en duro
      .Campo("Cod_Instrumento").Valor = gcINST_PACTOS_NAC  'Esto va en duro
      .Campo("Flg_Tipo_Movimiento").Valor = lTipoOperacion
      .Campo("Fecha_Operacion").Valor = lFecha_Operacion
      .Campo("Fecha_Vigencia").Valor = lFecha_Operacion
      .Campo("Fecha_Liquidacion").Valor = lFecha_Liquidacion
      .Campo("Dsc_Operacion").Valor = "" 'pDsc_Operacion
      .Campo("Id_Trader").Valor = Null
      .Campo("Porc_Comision").Valor = 0 'pPorc_Comision
      .Campo("Comision").Valor = 0
      .Campo("Derechos").Valor = 0
      .Campo("Gastos").Valor = 0
      .Campo("Iva").Valor = 0
      .Campo("Monto_Operacion").Valor = 0
      .Campo("Flg_Limite_Precio").Valor = cTipo_Precio_Mercado
    End With
  
    Rem Busca que el nemotecnicos exista en el sistema
    Set lcNemotecnico = New Class_Nemotecnicos
    With lcNemotecnico
      .Campo("nemotecnico").Valor = Trim(lNemotecnico)
      If Not .Buscar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , cMsgErr _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ExitProcedure
      End If
    
      If .Cursor.Count <= 0 Then
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No esta definido el nemotecnico """ & Trim(lReg("Nemotecnico").Value) & """, favor de crear", pConLog:=True)
        GoTo SiguienteNemo
      End If
    
      lId_Nemotecnico = .Cursor(1)("id_nemotecnico").Value
    End With
  
    Set lcOperacion_Detalle = New Class_Operaciones_Detalle
    With lcOperacion_Detalle
      .Campo("Id_Nemotecnico").Valor = lId_Nemotecnico
      .Campo("Cantidad").Valor = lReg("nominales").Value
      .Campo("Precio").Valor = lReg("tasa").Value
      .Campo("Id_Moneda_Pago").Valor = lId_Moneda_Pago
      .Campo("Monto_Pago").Valor = lReg("monto").Value
      .Campo("Flg_Vende_Todo").Valor = cFlg_No_Vende_Todo
      .Campo("id_mov_activo_compra").Valor = lId_Mov_Activo_Compra
    End With
    
    With lcOperacion
      .Campo("Comision").Valor = .Campo("Comision").Valor + lReg("Comision").Value
      .Campo("Derechos").Valor = .Campo("Derechos").Valor + lReg("derechobolsa").Value
      .Campo("Gastos").Valor = .Campo("Gastos").Valor + lReg("Gastos").Value
      .Campo("Iva").Valor = .Campo("Iva").Valor + lReg("Iva").Value
      .Campo("Monto_Operacion").Valor = .Campo("Monto_Operacion").Valor + lReg("monto").Value + ((lReg("Iva").Value + lReg("Gastos").Value + lReg("derechobolsa").Value + lReg("Comision").Value) * IIf(lTipoOperacion = gcTipoOperacion_Egreso, -1, 1))
      
      Call .Agregar_Operaciones_Detalle(lcOperacion_Detalle)
    End With
    
    lId_Caja_Cuenta = ""
  
    Set lcCaja_Cuenta = New Class_Cajas_Cuenta
    With lcCaja_Cuenta
      .Campo("id_cuenta").Valor = pId_Cuenta
      .Campo("id_moneda").Valor = lId_Moneda_Pago
      If Not .Buscar_Caja_Para_Invertir() Then
        Call Fnt_MsgError(.SubTipo_LOG _
                          , cMsgErr _
                          , .ErrMsg _
                          , pConLog:=True)
        GoTo ExitProcedure
      End If
      
      If .Cursor.Count = 0 Then
        Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No existe una caja definida para la moneda PESO, es imposible realizar el movimiento de caja.", pConLog:=True)
        GoTo SiguienteNemo
      End If
      
      lId_Caja_Cuenta = .Cursor(1)("Id_Caja_Cuenta").Value
    End With
  
    If Not lcOperacion.Guardar(lId_Caja_Cuenta) Then
      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
                        , cMsgErr _
                        , lcOperacion.ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
    
'    lId_Operacion = lcOperacion.Campo("Id_Operacion").Valor
'
'    If Not lcOperacion.Confirmar(lId_Caja_Cuenta) Then
'      Call Fnt_MsgError(lcOperacion.SubTipo_LOG _
'                        , cMsgErr _
'                        , lcOperacion.ErrMsg _
'                        , pConLog:=True)
'      GoTo ExitProcedure
'    End If
    
    'Registra el numero de operacion con su detalle
    lAlias_DetOperacion = Trim(pCuentaPlanes) & "-" & Trim(lReg("TIP_OPERA").Value) & "-" & Trim(lReg("NRO_OPERA").Value) & "-" & Trim(lReg("COR_OPERA").Value)
    
    lId_Operacion_detalle = lcOperacion.Detalles(1).Campo("id_operacion_detalle").Valor
    
    Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones) 'New Class_Rel_Conversiones
    With lcRel_Conversion
      Set .gDB = gDB
      .Campo("id_entidad").Valor = lId_Operacion_detalle
      .Campo("id_tipo_conversion").Valor = lId_Tipo_Conversion
      .Campo("id_origen").Valor = lId_Origen
      .Campo("Valor").Valor = lAlias_DetOperacion
      If Not .Guardar Then
        MsgBox "Problemas en el alias de la operacion detalle." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        GoTo ErrProcedure
      End If
    End With
    Set lcRel_Conversion = Nothing
    
    lMov_Cargados = lMov_Cargados + 1
    
SiguienteNemo:

  Next
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, cMsgErr, Err.Description, pConLog:=True)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Set lcOperacion = Nothing
  Set lcCaja_Cuenta = Nothing
  
  Call Fnt_Escribe_Grilla(Grilla, "I", Space(4) & lMov_Cargados & " Movimientos de pactos cargados.", pConLog:=True)
End Sub

Private Function Fnt_BuscaCodigosAlias(ByRef pId_Origen, ByRef pId_Tipo_Conversion) As Boolean
Dim lcOrigen As Object
Dim lcTipo_Conversion As Object
'---------------------------------------------------

  Fnt_BuscaCodigosAlias = False
  
  Set lcOrigen = Fnt_CreateObject(cDLL_Origenes)
  With lcOrigen
    Set .gDB = gDB
    .Campo("cod_origen").Valor = cOrigen_Sec_Planes
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas al cargar las contrapartes del alias """ & cOrigen_Sec_Planes & """.", .ErrMsg, pConLog:=True)
      GoTo ExitProcedure
    End If
  
    If .Cursor.Count <= 0 Then
      MsgBox "No esta definido el origen para """ & cOrigen_Sec_Planes & """.", vbInformation, Me.Caption
      GoTo ExitProcedure
    End If
    
    pId_Origen = .Cursor(1)("id_origen").Value
  End With
  Set lcOrigen = Nothing
  
  Set lcTipo_Conversion = Fnt_CreateObject(cDLL_Tipos_Conversion)
  With lcTipo_Conversion
    Set .gDB = gDB
    .Campo("tabla").Valor = cTabla_Operacion_Detalle
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas en la carga de los alias." _
                        , .ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      MsgBox "No esta definido el tipo de conversion para """ & cTabla_Operacion_Detalle & """.", vbInformation, Me.Caption
      GoTo ExitProcedure
    End If
    
    pId_Tipo_Conversion = .Cursor(1)("id_tipo_conversion").Value
  End With
  Set lcTipo_Conversion = Nothing
  
  Fnt_BuscaCodigosAlias = True
  
ExitProcedure:
  Set lcOrigen = Nothing
  Set lcTipo_Conversion = Nothing
End Function

Private Function Fnt_ImportarMasivo() As Boolean
Dim lcRel_Conversiones  As Object
Dim lcTipo_Conversion   As Object
Dim lcOrigenes          As Object
Dim lReg As hFields
'-----------------------------------------
Dim lId_Tipo_Conversion As Double
Dim lId_Origen          As Double

  Fnt_ImportarMasivo = False
  
  ProgressBar.Value = 0
  ProgressBar.Appearance = cc3D
  
  Set lcTipo_Conversion = Fnt_CreateObject(cDLL_Tipos_Conversion)
  With lcTipo_Conversion
    Set .gDB = gDB
    .Campo("tabla").Valor = cTabla_Cuentas
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas en la carga de los alias." _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No esta definido el tipo de conversion para " & cTabla_Cuentas & ".", pConLog:=True)
      GoTo ExitProcedure
    End If
    
    lId_Tipo_Conversion = .Cursor(1)("id_tipo_conversion").Value
  End With
  
  Set lcOrigenes = CreateObject(cDLL_Origenes)
  With lcOrigenes
    Set .gDB = gDB
    .Campo("cod_origen").Valor = cOrigen_Sec_Planes
    If Not .Buscar Then
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas en la lectura de origenes" _
                        , .ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
    
    If .Cursor.Count <= 0 Then
      Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "No esta definido el tipo de conversion para " & cOrigen_Sec_Planes & ".", pConLog:=True)
      GoTo ExitProcedure
    End If
    
    lId_Origen = .Cursor(1)("id_origen").Value
  End With
  
  Set lcRel_Conversiones = Fnt_CreateObject(cDLL_Rel_Conversiones)  ' New Class_Rel_Conversiones
  With lcRel_Conversiones
    Set .gDB = gDB
    Rem Borra todos los registros de Rel_Conversiones seg�n el id
    .Campo("id_tipo_conversion").Valor = lId_Tipo_Conversion
    .Campo("id_origen").Valor = lId_Origen
    If Not .Buscar Then
      Call Fnt_Escribe_Grilla(Grilla, cGrillaLog_Error, Space(8) & "Problemas al buscar." & vbCr & vbCr & .ErrMsg, pConLog:=True)
      GoTo ExitProcedure
    End If
  End With

  If lcRel_Conversiones.Cursor.Count > 0 Then
    ProgressBar.Max = lcRel_Conversiones.Cursor.Count
  End If
  
  For Each lReg In lcRel_Conversiones.Cursor
    ProgressBar.Value = ProgressBar.Value + 1
    Call Fnt_Importar(lReg("valor").Value)
  Next

  Fnt_ImportarMasivo = True
ExitProcedure:

  Set lcRel_Conversiones = Nothing
End Function
