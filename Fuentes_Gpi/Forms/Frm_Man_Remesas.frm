VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Man_Remesas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Remesas"
   ClientHeight    =   7365
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11430
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7365
   ScaleWidth      =   11430
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Remesas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   6855
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   11295
      Begin VB.CheckBox chk_TodasCtas 
         Caption         =   "Todas"
         Height          =   375
         Left            =   4410
         TabIndex        =   7
         ToolTipText     =   "Todas las Cuentas"
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3945
         Picture         =   "Frm_Man_Remesas.frx":0000
         TabIndex        =   4
         Top             =   300
         Width           =   375
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   6015
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   11055
         _cx             =   19500
         _cy             =   10610
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   10
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Remesas.frx":030A
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin hControl2.hTextLabel Txt_Nombres 
         Height          =   315
         Left            =   5550
         TabIndex        =   5
         Top             =   360
         Width           =   5610
         _ExtentX        =   9895
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Nombre"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   135
         TabIndex        =   6
         Top             =   300
         Width           =   3780
         _ExtentX        =   6668
         _ExtentY        =   609
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "N� de Cuenta"
         Text            =   ""
         MaxLength       =   100
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11430
      _ExtentX        =   20161
      _ExtentY        =   635
      ButtonWidth     =   2037
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Remesa"
            Key             =   "ADD"
            Description     =   "Ingresa remesa"
            Object.ToolTipText     =   "Agrega una Remesa"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Anular"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Anula un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un elemento"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   8100
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Remesas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'----------------------------------------------------------------------------
Dim fFlg_Fechas_Anteriores As Boolean
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Public Sub Mostrar(pCod_Arbol_Sistema, Optional pFlg_Fechas_Anteriores As Boolean = False)

  fFlg_Fechas_Anteriores = pFlg_Fechas_Anteriores
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Sub_CargarDatos()
Dim lReg    As hFields
Dim lLinea  As Long
Dim lCuenta As String
Dim lID     As String
Dim lRemesa As Class_Remesas
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object

  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Grilla.Rows = 1
  
  If Trim(Txt_Num_Cuenta.Tag) <> "" And Trim(Txt_Num_Cuenta.Text) <> "" Then
    lCuenta = Txt_Num_Cuenta.Tag
  Else
    lCuenta = ""
  End If
  
  Set lRemesa = New Class_Remesas
  With lRemesa
    If Not lCuenta = "" Then
      .Campo("id_Cuenta").Valor = lCuenta
    Else
      Txt_Nombres.Text = ""
      Txt_Num_Cuenta.Text = ""
    End If
    If Not .Buscar Then
    
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en recuperar.", _
                        .ErrMsg, _
                        pConLog:=True)
    Else
      For Each lReg In .Cursor
        lLinea = Grilla.Rows
        Grilla.AddItem ""
        Call SetCell(Grilla, lLinea, "colum_pk", lReg("Id_Remesa").Value)
        Call SetCell(Grilla, lLinea, "id_cuenta", lReg("id_cuenta").Value)
        Call SetCell(Grilla, lLinea, "Fecha_Remesa", lReg("Fecha_Remesa").Value)
        Call SetCell(Grilla, lLinea, "Caja_Ingreso", lReg("dsc_caja_ingreso").Value)
        Call SetCell(Grilla, lLinea, "monto_ingreso", Format(lReg("monto_ingreso").Value, Fnt_Formato_Moneda(lReg("ID_MONEDA_INGRESO").Value)))
        Call SetCell(Grilla, lLinea, "Caja_Egreso", lReg("dsc_caja_egreso").Value)
        Call SetCell(Grilla, lLinea, "monto_egreso", Format(lReg("monto_egreso").Value, Fnt_Formato_Moneda(lReg("ID_MONEDA_EGRESO").Value)))
        Call SetCell(Grilla, lLinea, "cod_estado_remesa", lReg("cod_estado_remesa").Value)
        Call SetCell(Grilla, lLinea, "estado_remesa", lReg("estado_remesa").Value)
        
'        Set lcCuenta = New Class_Cuentas
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
          .Campo("id_cuenta").Valor = lReg("id_cuenta").Value
          If .Buscar Then
            If .Cursor.Count > 0 Then
              Call SetCell(Grilla, lLinea, "num_cuenta", .Cursor(1)("num_cuenta").Value)
            End If
          End If
        End With
        Set lcCuenta = Nothing
      Next
    End If
  End With
  Set lRemesa = Nothing
  
  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If
End Sub

Private Sub chk_TodasCtas_Click()

If chk_TodasCtas.Value = 1 Then
    Txt_Num_Cuenta.Text = ""
    Txt_Num_Cuenta.Tag = ""
    Txt_Nombres.Text = ""
    Txt_Num_Cuenta.Enabled = False
    cmb_buscar.Enabled = False
    Grilla.Rows = 1
    Call Sub_CargarDatos
Else
    Txt_Num_Cuenta.Enabled = True
    cmb_buscar.Enabled = True
End If

End Sub






Private Sub cmb_buscar_Click()
    Dim lId_Cuenta As String
    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)
    If lId_Cuenta <> 0 Then
        Txt_Num_Cuenta.Text = ""
        Txt_Num_Cuenta.Tag = lId_Cuenta
        Call Lpr_Buscar_Cuenta("", CStr(lId_Cuenta))
    End If
End Sub


Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir

  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agregar
    Case "EXIT"
      Unload Me
    Case "DEL"
      Call Sub_Anular
    Case "REFRESH"
      Call Sub_CargaForm
    Case "UPDATE"
      Call Grilla_DblClick
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Sub_Agregar()
Dim lId_Cuenta As String
  
  If Trim(Txt_Num_Cuenta.Tag) <> "" And Trim(Txt_Num_Cuenta.Text) <> "" Then
    lId_Cuenta = Txt_Num_Cuenta.Tag
  Else
    lId_Cuenta = ""
  End If
  Call Sub_EsperaVentana(cNewEntidad, lId_Cuenta)
  
End Sub

Private Sub Sub_CargaForm()

  Call Sub_FormControl_Color(Me.Controls)

  Txt_Num_Cuenta.Text = ""
  Txt_Num_Cuenta.Tag = ""
  Txt_Nombres.Text = ""
  chk_TodasCtas.Value = 0
  Txt_Num_Cuenta.Enabled = True
  cmb_buscar.Enabled = True
  Grilla.Rows = 1
  
End Sub

Private Sub Grilla_DblClick()
Dim lKey As String
Dim lId_Cuenta As String

  With Grilla
    If .Row > 0 Then
      lKey = GetCell(Grilla, .Row, "colum_pk")
      lId_Cuenta = GetCell(Grilla, .Row, "id_cuenta")
      
      Call Sub_EsperaVentana(lKey, lId_Cuenta)
    End If
  End With
End Sub

Private Sub Sub_EsperaVentana(ByVal pkey, ByVal pId_Cuenta)
Dim lForm_Fechas_Anteriores As Frm_Remesa_Fechas_Anteriores
Dim lNombre As String

  Me.Enabled = False

  If Not pId_Cuenta = "" Then
    If Not Fnt_ExisteVentanaKey("Frm_Remesa_Fechas_Anteriores", pkey) Then
      lNombre = Me.Name
      
      Set lForm_Fechas_Anteriores = New Frm_Remesa_Fechas_Anteriores
      Call lForm_Fechas_Anteriores.Fnt_Modificar(pkey, _
                                                pId_Cuenta, _
                                                pCod_Arbol_Sistema:=fCod_Arbol_Sistema, _
                                                pFlg_Fechas_Anteriores:=fFlg_Fechas_Anteriores)
      
      Do While Fnt_ExisteVentanaKey("Frm_Remesa_Fechas_Anteriores", pkey)
        DoEvents
      Loop
      
      If Fnt_ExisteVentana(lNombre) Then
        Call Sub_CargarDatos
      Else
        Exit Sub
      End If
    End If
  Else
    MsgBox "Primero elija una cuenta.", vbInformation + vbOKOnly, Me.Caption
  End If
  
  Me.Enabled = True
  
End Sub

Private Sub Sub_Anular()
Dim lcRemesa As Class_Remesas
Dim lId_Mov_Caja_Egreso As String
Dim lId_Mov_Caja_Ingreso As String
Dim lId_Remesa As String
Dim lRollback As Boolean
  
  Call Sub_Bloquea_Puntero(Me)
  
  lRollback = False
  gDB.IniciarTransaccion
  
  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea Anular?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      Rem Si la remesa ya esta anulada, no se anula de nuevo el movimiento
      If GetCell(Grilla, Grilla.Row, "cod_estado_remesa") = cCod_Estado_Anulado Then
        lRollback = True
        MsgBox "La Remesa ya est� anulada.", vbInformation, Me.Caption
        GoTo ErrProcedure
      End If
    
      lId_Remesa = GetCell(Grilla, Grilla.Row, "colum_pk")
      If Not lId_Remesa = "" Then
        Set lcRemesa = New Class_Remesas
        With lcRemesa
          .Campo("ID_REMESA").Valor = lId_Remesa
          If .Buscar Then
            If .Cursor.Count > 0 Then
              lId_Mov_Caja_Egreso = NVL(.Cursor(1)("MOV_EGRESO").Value, "")
              lId_Mov_Caja_Ingreso = NVL(.Cursor(1)("MOV_INGRESO").Value, "")
            End If
          Else
            lRollback = True
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en eliminar la Remesa.", _
                              .ErrMsg, _
                              pConLog:=True)
            GoTo ErrProcedure
          End If
          
          If lId_Mov_Caja_Egreso = "" Then
            lRollback = True
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en eliminar la Remesa.", _
                              "ID Caja de Egreso vacia.", _
                              pConLog:=True)
            GoTo ErrProcedure
          ElseIf lId_Mov_Caja_Ingreso = "" Then
            lRollback = True
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en eliminar la Remesa.", _
                              "ID Caja de Ingreso vacia.", _
                              pConLog:=True)
            GoTo ErrProcedure
          End If
          
          Rem Anula el movimiento de caja de Egreso
          If Not Fnt_Anula_Mov_Caja(lId_Mov_Caja_Egreso) Then
            lRollback = True
            GoTo ErrProcedure
          End If
          Rem Anula el movimiento de caja de Ingreso
          If Not Fnt_Anula_Mov_Caja(lId_Mov_Caja_Ingreso) Then
            lRollback = True
            GoTo ErrProcedure
          End If
          
          Rem Anula la remesa
          If Not .Anular(lId_Remesa) Then
            lRollback = True
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en eliminar la Remesa.", _
                        .ErrMsg, _
                        pConLog:=True)
            GoTo ErrProcedure
          End If
          
        End With
        MsgBox "Remesa Anulada correctamente.", vbInformation + vbOKOnly, Me.Caption
      End If
    End If
  End If
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
    If Grilla.Row > 0 Then
        Call Sub_CargarDatos
    End If
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Fnt_Anula_Mov_Caja(Pid_Mov_Caja As String) As Boolean
Dim lcMov_Caja As Class_Mov_Caja

  Fnt_Anula_Mov_Caja = True
  
  Set lcMov_Caja = New Class_Mov_Caja
  With lcMov_Caja
    If Not .Anular(Pid_Mov_Caja) Then
      Fnt_Anula_Mov_Caja = False
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en eliminar la Remesa.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcMov_Caja = Nothing
  
End Function

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
Dim lCuenta As String
  
  Set lForm = New Frm_Reporte_Generico
  lCuenta = Txt_Num_Cuenta.Text
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Listado de Remesas" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Cuenta: " & lCuenta
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub



Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call Lpr_Buscar_Cuenta("TXT")
    End If
End Sub

Private Sub Lpr_Buscar_Cuenta(ByVal Origen As String, Optional p_Id_Cuenta As String)
Dim lcCuenta      As Object

    If Origen = "TXT" Then
        If InStr(1, Txt_Num_Cuenta.Text, "-") > 0 Then
           Txt_Num_Cuenta.Text = Trim(Left(Txt_Num_Cuenta.Text, InStr(1, Txt_Num_Cuenta.Text, "-") - 1))
        End If
        p_Id_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text), 0)
    End If
    If p_Id_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = p_Id_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                    Txt_Nombres.Text = .Cursor(1)("nombre_cliente").Value
                    p_Id_Cuenta = .Cursor(1)("id_cuenta").Value
                    If Not p_Id_Cuenta = "" Then
                       Call Sub_CargarDatos
                    End If
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
            Set lcCuenta = Nothing
    End If
End Sub



