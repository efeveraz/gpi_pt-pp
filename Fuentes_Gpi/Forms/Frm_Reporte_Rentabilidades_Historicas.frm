VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Object = "{1BCC7098-34C1-4749-B1A3-6C109878B38F}#1.0#0"; "vspdf8.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Frm_Reporte_Rentabilidades_Historicas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte de Rentabilidades Historicas"
   ClientHeight    =   8055
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6255
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8055
   ScaleWidth      =   6255
   Begin VB.Frame Frame1 
      Caption         =   "Fecha de Consulta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   675
      Left            =   120
      TabIndex        =   22
      Top             =   480
      Width           =   6015
      Begin MSComCtl2.DTPicker DTP_Fecha_Consulta 
         Height          =   315
         Left            =   1440
         TabIndex        =   23
         Top             =   240
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   17235969
         CurrentDate     =   37732
      End
      Begin VB.Label Lbl_Fecha_Consulta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Consulta"
         Height          =   315
         Left            =   120
         TabIndex        =   24
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.CommandButton Cmb_BuscaFile 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5760
      TabIndex        =   19
      ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
      Top             =   2040
      Width           =   345
   End
   Begin VB.Frame Frm_Filtros 
      Caption         =   "Filtros de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   5535
      Left            =   120
      TabIndex        =   6
      Top             =   2400
      Width           =   6015
      Begin VB.CommandButton cmb_Buscar_NombreCliente 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5520
         Picture         =   "Frm_Reporte_Rentabilidades_Historicas.frx":0000
         TabIndex        =   12
         Top             =   1320
         Width           =   375
      End
      Begin VB.CommandButton cmb_buscar_Rut 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5520
         Picture         =   "Frm_Reporte_Rentabilidades_Historicas.frx":030A
         TabIndex        =   10
         Top             =   960
         Width           =   375
      End
      Begin VB.CommandButton cmb_buscar_Cuenta 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5520
         Picture         =   "Frm_Reporte_Rentabilidades_Historicas.frx":0614
         TabIndex        =   8
         Top             =   600
         Width           =   375
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   315
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   5310
         _ExtentX        =   9366
         _ExtentY        =   556
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "N� de Cuenta"
         Text            =   ""
         MaxLength       =   12
      End
      Begin hControl2.hTextLabel Txt_Rut_Cliente 
         Height          =   315
         Left            =   120
         TabIndex        =   11
         Top             =   960
         Width           =   2910
         _ExtentX        =   5133
         _ExtentY        =   556
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "RUT Cliente"
         Text            =   ""
         MaxLength       =   10
      End
      Begin hControl2.hTextLabel TxtRazonSocial 
         Height          =   315
         Left            =   120
         TabIndex        =   13
         Top             =   1320
         Width           =   5280
         _ExtentX        =   9313
         _ExtentY        =   556
         LabelWidth      =   1245
         Caption         =   "Nombre Cliente"
         Text            =   ""
         MaxLength       =   99
      End
      Begin TrueDBList80.TDBCombo Cmb_Asesor 
         Height          =   315
         Left            =   1440
         TabIndex        =   15
         Top             =   240
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   556
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   556
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Rentabilidades_Historicas.frx":091E
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComctlLib.Toolbar Toolbar_Interna 
         Height          =   360
         Left            =   120
         TabIndex        =   16
         Top             =   1680
         Width           =   5805
         _ExtentX        =   10239
         _ExtentY        =   635
         ButtonWidth     =   1746
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Wrappable       =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Agregar"
               Key             =   "ADD"
               Description     =   "Agrega informaci�n"
               Object.ToolTipText     =   "Agrega informaci�n"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Eliminar"
               Key             =   "DEL"
               Description     =   "Elimina informaci�n"
               Object.ToolTipText     =   "Elimina informaci�n"
            EndProperty
         EndProperty
         BorderStyle     =   1
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   255
            Left            =   9420
            TabIndex        =   17
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
         Height          =   3360
         Left            =   120
         TabIndex        =   18
         Top             =   2040
         Width           =   5805
         _cx             =   10239
         _cy             =   5927
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   7
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Reporte_Rentabilidades_Historicas.frx":09C8
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin VB.Label Lbl_Asesor 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Asesor"
         Height          =   315
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   1245
      End
   End
   Begin VB.Frame Frm_Fecha_Re_Proceso 
      Caption         =   "Per�odo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   675
      Left            =   120
      TabIndex        =   2
      Top             =   1200
      Width           =   6015
      Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
         Height          =   315
         Left            =   1440
         TabIndex        =   0
         Top             =   240
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   17235969
         CurrentDate     =   37732
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
         Height          =   315
         Left            =   4440
         TabIndex        =   1
         Top             =   240
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   17235969
         CurrentDate     =   37732
      End
      Begin VB.Label Lbl_Fecha_Hasta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Hasta"
         Height          =   315
         Left            =   3120
         TabIndex        =   7
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Lbl_Fecha_Desde 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Desde"
         Height          =   315
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   6255
      _ExtentX        =   11033
      _ExtentY        =   635
      ButtonWidth     =   3016
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Generar Informe"
            Key             =   "CIERRE"
            Description     =   "Generar Informe de cuentas"
            Object.ToolTipText     =   "Generar Informe de cuentas"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la ventana perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la ventana perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin hControl2.hTextLabel Txt_Directorio 
      Height          =   315
      Left            =   120
      TabIndex        =   20
      Tag             =   "OBLI"
      Top             =   2040
      Width           =   5580
      _ExtentX        =   9843
      _ExtentY        =   556
      LabelWidth      =   900
      TextMinWidth    =   1200
      Caption         =   "Directorio"
      Text            =   ""
      BackColorTxt    =   12648384
      BackColorTxt    =   12648384
   End
   Begin MSComDlg.CommonDialog Cmd_AbreArchivo 
      Left            =   480
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VSPrinter8LibCtl.VSPrinter vp 
      Height          =   615
      Left            =   1080
      TabIndex        =   21
      Top             =   0
      Visible         =   0   'False
      Width           =   1335
      _cx             =   2355
      _cy             =   1085
      Appearance      =   1
      BorderStyle     =   1
      Enabled         =   -1  'True
      MousePointer    =   0
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoRTF         =   -1  'True
      Preview         =   -1  'True
      DefaultDevice   =   0   'False
      PhysicalPage    =   -1  'True
      AbortWindow     =   -1  'True
      AbortWindowPos  =   0
      AbortCaption    =   "Printing..."
      AbortTextButton =   "Cancel"
      AbortTextDevice =   "on the %s on %s"
      AbortTextPage   =   "Now printing Page %d of"
      FileName        =   ""
      MarginLeft      =   1440
      MarginTop       =   1440
      MarginRight     =   1440
      MarginBottom    =   1440
      MarginHeader    =   0
      MarginFooter    =   0
      IndentLeft      =   0
      IndentRight     =   0
      IndentFirst     =   0
      IndentTab       =   720
      SpaceBefore     =   0
      SpaceAfter      =   0
      LineSpacing     =   100
      Columns         =   1
      ColumnSpacing   =   180
      ShowGuides      =   2
      LargeChangeHorz =   300
      LargeChangeVert =   300
      SmallChangeHorz =   30
      SmallChangeVert =   30
      Track           =   0   'False
      ProportionalBars=   -1  'True
      Zoom            =   -1.23106060606061
      ZoomMode        =   3
      ZoomMax         =   400
      ZoomMin         =   10
      ZoomStep        =   25
      EmptyColor      =   -2147483636
      TextColor       =   0
      HdrColor        =   0
      BrushColor      =   0
      BrushStyle      =   0
      PenColor        =   0
      PenStyle        =   0
      PenWidth        =   0
      PageBorder      =   0
      Header          =   ""
      Footer          =   ""
      TableSep        =   "|;"
      TableBorder     =   7
      TablePen        =   0
      TablePenLR      =   0
      TablePenTB      =   0
      NavBar          =   3
      NavBarColor     =   -2147483633
      ExportFormat    =   0
      URL             =   ""
      Navigation      =   3
      NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
      AutoLinkNavigate=   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   9
   End
   Begin VSPDF8LibCtl.VSPDF8 VSPDF81 
      Left            =   0
      Top             =   120
      Author          =   ""
      Creator         =   ""
      Title           =   ""
      Subject         =   ""
      Keywords        =   ""
      Compress        =   3
   End
End
Attribute VB_Name = "Frm_Reporte_Rentabilidades_Historicas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fHoraInicio As Long
'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso       As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema      As String 'Codigo para el permiso sobre la ventana

Dim App_Excel       As Excel.Application      ' Excel application
Dim lLibro          As Excel.Workbook      ' Excel workbook

Private Type TCuenta
    id_Cuenta       As Integer
    Num_Cuenta      As Long
    rut_cliente     As String
    dsc_cuenta      As String
    asesor_cuenta   As String
End Type

Dim aCuentas() As TCuenta

Dim lId_Cuenta      As String
Dim fId_Cuenta      As Long 'Flag para validar el agregar en grilla
Dim bMuestraCuadro  As Boolean

Dim FilaAux             As Variant
Dim iFilaArreglo  As Integer
Dim oCliente            As New Class_Cartola
Dim sTextoIndicadores As String
Dim stextovalores  As String
Dim sTextoValoresAnt As String
Dim sTextoDosPuntos As String
Dim nInterlineado As Integer
Dim nPage               As Integer
Dim bImprimioHeader     As Boolean

'--------------------------------------------------------------------------
' Constantes
'--------------------------------------------------------------------------
Const RColorRelleno = 38     ' 213   ' R
Const GColorRelleno = 7      ' 202   ' G
Const BColorRelleno = 115    ' 195   ' B

Const RColorRelleno_TITULOS = 242  ' R
Const GColorRelleno_TITULOS = 242   ' G
Const BColorRelleno_TITULOS = 242   ' B

Const glb_tamletra_titulo = 14
Const glb_tamletra_subtitulo = 10
Const glb_tamletra_registros = 8

Const Font_Name = "Times New Roman"
' Const Font_Name = "Gill Sans MT"

Const COLUMNA_INICIO    As String = "10mm"
Const COLUMNA_DOS       As String = "140mm"
Const CONST_ANCHO_GRILLA As String = "255mm"

Const CONS_NUMERO_LINEAS As Integer = 38   ' Numero de Lineas

Const CONS_POR_PANTALLA As Integer = 0
Const CONS_POR_IMPRESORA As Integer = 1
Const CONS_POR_ARCHIVO As Integer = 2

Private Declare Function MakeSureDirectoryPathExists Lib "imagehlp.dll" (ByVal lpPath As String) As Long


Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Cmb_Asesor_ItemChange()
    Txt_Num_Cuenta.Text = ""
    Txt_Rut_Cliente.Text = ""
    TxtRazonSocial.Text = ""
End Sub

Private Sub Cmb_BuscaFile_Click()

    On Error GoTo Cmd_BuscarArchivo_Err
    
    With Cmd_AbreArchivo
        .Flags = cdlOFNPathMustExist
        .Flags = .Flags Or cdlOFNHideReadOnly
        .Flags = .Flags Or cdlOFNNoChangeDir
        .Flags = .Flags Or cdlOFNExplorer
        .Flags = .Flags Or cdlOFNNoValidate
        .Filename = "*.*"
    End With

    Dim X As Integer
    X = 3

    Cmd_AbreArchivo.CancelError = True    'Do not terminate on error

    On Error Resume Next         'I will hande errors

    Cmd_AbreArchivo.Action = 1            'Present "open" dialog

    'If FileTitle is null, user did not override the default (*.*)
    If Cmd_AbreArchivo.FileTitle <> "" Then X = Len(Cmd_AbreArchivo.FileTitle)

    If Err = 0 Then
        ChDrive Cmd_AbreArchivo.Filename
        Txt_Directorio.Text = Left(Cmd_AbreArchivo.Filename, Len(Cmd_AbreArchivo.Filename) - X)
    Else
        'User puso "Cancelar"
    End If
  Exit Sub

Cmd_BuscarArchivo_Err:
  If Err = 32755 Then 'usuario presiona el boton Cancelar del Command Dialog
    Exit Sub
  End If
End Sub

Private Sub cmb_buscar_Cuenta_Click()
    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(pNum_Cuenta:=Txt_Num_Cuenta.Text), 0)
    Call Mostrar_Cuenta
End Sub

Private Sub cmb_Buscar_NombreCliente_Click()
    lId_Cuenta = NVL(Frm_Busca_Cuentas.BuscarCliente(pFiltro_NombreCliente:=TxtRazonSocial.Text, pBusca_Cuenta:=True), 0)
    Call Mostrar_Cuenta
End Sub

Private Sub cmb_buscar_Rut_Click()
    lId_Cuenta = NVL(Frm_Busca_Cuentas.BuscarCliente(pFiltro_RutCliente:=Txt_Rut_Cliente.Text, pBusca_Cuenta:=True), 0)
    Call Mostrar_Cuenta
End Sub

Private Sub Form_Load()
    Dim lReg  As hCollection.hFields
    Dim lLinea As Long

    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("CIERRE").Image = cBoton_Aceptar
            .Buttons("REFRESH").Image = cBoton_Refrescar
            .Buttons("EXIT").Image = cBoton_Salir
    End With
    
    With Toolbar_Interna
    Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("ADD").Image = cBoton_Agregar_Grilla
        .Buttons("DEL").Image = cBoton_Eliminar_Grilla
    End With
  
    Call Sub_CargaForm
    
    Me.Top = 1
    Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
    Dim lCierre As Class_Verificaciones_Cierre
    Dim lcCuenta As Object
    Dim lReg As hFields
    Dim lLinea As Long

    Call Sub_FormControl_Color(Me.Controls)

    Rem Limpia la grilla
    Grilla_Cuentas.Rows = 1
    fId_Cuenta = 0
    
    Txt_Num_Cuenta.Text = ""
    Txt_Rut_Cliente.Text = ""
    TxtRazonSocial.Text = ""
    Me.Txt_Directorio.Text = ""

    Rem La hora que se propone es la del �ltimo dia de cierre
    Set lCierre = New Class_Verificaciones_Cierre
  
    DTP_Fecha_Consulta.Value = Fnt_FechaServidor
    DTP_Fecha_Hasta.Value = Fnt_FechaServidor
    DTP_Fecha_Desde.Value = DTP_Fecha_Hasta.Value
  
    Rem Carga los combos con el primer elemento vac�o
    Call Sub_CargaCombo_Asesor(Cmb_Asesor, pBlanco:=True)
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "CIERRE"
      If Txt_Directorio.Text = "" Then
        MsgBox "No hay directorio.", vbExclamation, Me.Caption
      Else
        If Grilla_Cuentas.Rows <= 1 Then
          MsgBox "No hay cuentas.", vbExclamation, Me.Caption
        Else
          Call Sub_Informe
        End If
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargaForm
      End If
  End Select

End Sub

Private Sub Sub_Agrega()
  Dim lLinea      As Long
  Dim lId_Cuenta  As String
  Dim lId_Asesor  As String
  Dim lCursor     As hCollection.hRecord
  Dim lReg        As hCollection.hFields
  Dim lCuentas    As Class_Re_Proceso_Cierre
  Dim lcCuenta    As Object
  
  lId_Cuenta = Txt_Num_Cuenta.Text
  
  lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
  lId_Asesor = IIf(lId_Asesor = cCmbBLANCO, "", lId_Asesor)
  
  ' Agrega a la grilla cuentas por asesor
  If Not lId_Asesor = "" Then
    'Grilla_Cuentas.Rows = 1
    fId_Cuenta = 1
  
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_CUENTAS$BuscarCuentasPorAsesor"
    gDB.Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pId_Asesor", ePT_Numero, lId_Asesor, ePD_Entrada
    gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    
    If Not gDB.EjecutaSP Then
      MsgBox "No se pudo obtener cuentas del asesor", vbInformation, Me.Caption
      Exit Sub
    End If
    
    Set lCursor = gDB.Parametros("pCursor").Valor
    
    For Each lReg In lCursor
      Grilla_Cuentas.Row = Grilla_Cuentas.FindRow(lReg("id_cuenta").Value, , Grilla_Cuentas.ColIndex("colum_pk"))
      If Not Grilla_Cuentas.Row = cNewEntidad Then
        'MsgBox "Cuenta ya se encuentra asociada.", vbExclamation, Me.Caption
      Else
        lLinea = Grilla_Cuentas.Rows
        Call Grilla_Cuentas.AddItem("")
        Call SetCell(Grilla_Cuentas, lLinea, "colum_pk", lReg("id_cuenta").Value)
        Call SetCell(Grilla_Cuentas, lLinea, "num_cuenta", lReg("num_cuenta").Value)
        Call SetCell(Grilla_Cuentas, lLinea, "rut_cliente", lReg("rut_cliente").Value)
        Call SetCell(Grilla_Cuentas, lLinea, "dsc_cuenta", NVL(lReg("nombre_cliente").Value, ""))
        Call SetCell(Grilla_Cuentas, lLinea, "asesor_cuenta", NVL(lReg("nombre_asesor").Value, ""))
        Call SetCell(Grilla_Cuentas, lLinea, "fecha_cierre", NVL(cierre_cuenta(lReg("id_cuenta").Value), ""))
      End If
    Next
  End If
  
  ' Agrega a la grilla por cuentas
  If Not lId_Cuenta = "" Then
    
    If fId_Cuenta = 1 Then
      'Grilla_Cuentas.Rows = 1
      fId_Cuenta = 0
    End If
    
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    
    With lcCuenta
      .Campo("num_cuenta").Valor = Txt_Num_Cuenta.Text
      .Campo("id_Empresa").Valor = Fnt_EmpresaActual
      
      If .Buscar_Vigentes Then
        If .Cursor.Count > 0 Then
            lId_Asesor = .Cursor(1)("nombre_asesor").Value
            lId_Cuenta = .Cursor(1)("id_cuenta").Value
        End If
      End If
    End With
    
    Grilla_Cuentas.Row = Grilla_Cuentas.FindRow(lId_Cuenta, , Grilla_Cuentas.ColIndex("colum_pk"))
    If Not Grilla_Cuentas.Row = cNewEntidad Then
      'MsgBox "Cuenta ya se encuentra asociada.", vbExclamation, Me.Caption
    Else
      lLinea = Grilla_Cuentas.Rows
      Grilla_Cuentas.AddItem ("")
      SetCell Grilla_Cuentas, lLinea, "colum_pk", lId_Cuenta
      SetCell Grilla_Cuentas, lLinea, "num_cuenta", Txt_Num_Cuenta.Text
      SetCell Grilla_Cuentas, lLinea, "rut_cliente", Txt_Rut_Cliente.Text
      SetCell Grilla_Cuentas, lLinea, "dsc_cuenta", TxtRazonSocial.Text
      SetCell Grilla_Cuentas, lLinea, "asesor_cuenta", lId_Asesor
      Call SetCell(Grilla_Cuentas, lLinea, "fecha_cierre", NVL(cierre_cuenta(lId_Cuenta), ""))
    End If
  End If
End Sub

' Elimina de la grilla
Private Sub Sub_Elimina()
  If Grilla_Cuentas.Row >= 1 Then
    Grilla_Cuentas.RemoveItem (Grilla_Cuentas.Row)
  End If
End Sub

Private Sub Toolbar_Interna_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agrega
    Case "DEL"
      Call Sub_Elimina
  End Select

End Sub

Private Sub Sub_Informe()
    Dim lcComision_Hono_Ase_Cuenta  As Class_Comisiones_Hono_Ase_Cuenta
    Dim lcComi_Fija_Hono_Ase_Cuenta As Class_Comi_Fija_Hono_Ase_Cuenta
    
    Dim oSaldosCaja     As Class_Saldos_Caja
    
    Dim lhCajas         As hFields
    
    Dim lFila           As Long
    Dim lId_Cuenta      As String
    Dim lFecha_Proceso  As Date
    
    Dim nFila   As Integer
    Dim i       As Integer
    
    Dim bPrimera As Boolean
    
    ReDim aCuentas(0)
    
    Call Sub_Bloquea_Puntero(Me)
    Me.Enabled = False
  
    If Grilla_Cuentas.Rows = 1 Then
        GoTo ExitProcedure
    End If
        
    For lFila = 1 To Grilla_Cuentas.Rows - 1
      nFila = UBound(aCuentas) + 1
      
      lId_Cuenta = GetCell(Grilla_Cuentas, lFila, "colum_pk")
      nFila = UBound(aCuentas) + 1
      ReDim Preserve aCuentas(nFila)
      
      aCuentas(nFila).id_Cuenta = GetCell(Grilla_Cuentas, lFila, "colum_pk")
      aCuentas(nFila).Num_Cuenta = GetCell(Grilla_Cuentas, lFila, "num_cuenta")
      aCuentas(nFila).rut_cliente = GetCell(Grilla_Cuentas, lFila, "rut_cliente")
      aCuentas(nFila).dsc_cuenta = GetCell(Grilla_Cuentas, lFila, "dsc_cuenta")
      aCuentas(nFila).asesor_cuenta = GetCell(Grilla_Cuentas, lFila, "asesor_cuenta")
      If GetCell(Grilla_Cuentas, lFila, "fecha_cierre") = "" Then
        Call SetCell(Grilla_Cuentas, lFila, "mensaje", "Cuenta sin cierre")
        Grilla_Cuentas.Cell(flexcpForeColor, lFila, 1, lFila, Grilla_Cuentas.Cols - 1) = vbRed
      Else
        If CDate(GetCell(Grilla_Cuentas, lFila, "fecha_cierre")) < Me.DTP_Fecha_Hasta Then
          Call SetCell(Grilla_Cuentas, lFila, "mensaje", "Rango de consulta superior al cierre")
          Grilla_Cuentas.Cell(flexcpForeColor, lFila, 1, lFila, Grilla_Cuentas.Cols - 1) = vbRed
        Else
          If CDate(GetCell(Grilla_Cuentas, lFila, "fecha_cierre")) < Me.DTP_Fecha_Consulta Then
            Call SetCell(Grilla_Cuentas, lFila, "mensaje", "Fecha de Consulta superior al cierre")
            Grilla_Cuentas.Cell(flexcpForeColor, lFila, 1, lFila, Grilla_Cuentas.Cols - 1) = vbRed
          Else
            Call SetCell(Grilla_Cuentas, lFila, "mensaje", "")
            Call CrearReporte(lId_Cuenta, lFila)
          End If
        End If
      End If
    Next
    
ExitProcedure:
    Set oSaldosCaja = Nothing
    Call Sub_Desbloquea_Puntero(Me)
    MsgBox "Proceso finalizado", vbInformation, Me.Caption
    Me.Enabled = True
End Sub

' Para cada una de las cuentas encontradas en la grilla de la aplicacion.
Private Sub CrearReporte(lId_Cuenta As String, lFila)
    Dim i As Integer
    'Dim hay_cierre As Boolean
On Error GoTo ErrProcedure
    nPage = 0
    'Moneda_Cuentas = lDsc_Moneda
    'oCliente.IdCuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Cuentas))
    'oCliente.Fecha_Cartola = DTP_Fecha_Ter.Value
    
    oCliente.IdCuenta = lId_Cuenta
    oCliente.Fecha_Cartola = CDate(Me.DTP_Fecha_Consulta)
    oCliente.IdMonedaSalida = 1
    oCliente.FlgComision = 0
    
    'GeneraCartola = True
    'hay_cierre = GeneraCartola

    Call DatosDelCliente
    
    FormatoHoja
    vp.StartDoc
    PaginaUno
    vp.EndDoc
    
    Dim Carpeta As String
    Dim Directorio As String
    
    If Txt_Directorio.Text <> "" Then
        Carpeta = MonthName(Month(oCliente.Fecha_Cartola)) & Year(oCliente.Fecha_Cartola)
        
        On Error Resume Next
        MakeSureDirectoryPathExists Txt_Directorio.Text & Carpeta & "\"
        
        Directorio = Me.Txt_Directorio.Text & Carpeta
        ' Dim sFileName As String
        
        oCliente.NombreArchivo = "Reporte_Rentabilidades_Historicas" & _
            "C" & oCliente.Num_Cuenta & _
            "D" & Year(oCliente.Fecha_Cartola) & "-" & String(2 - Len(Month(oCliente.Fecha_Cartola)), "0") & Month(oCliente.Fecha_Cartola) & "-" & String(2 - Len(Day(oCliente.Fecha_Cartola)), "0") & Day(oCliente.Fecha_Cartola) & _
            "R" & Replace(Trim(oCliente.Rut), ".", "") & _
            "N" & Trim(oCliente.Nombre) & ".PDF"
        VSPDF81.ConvertDocument vp, Directorio & "\" & oCliente.NombreArchivo
        Grilla_Cuentas.Cell(flexcpForeColor, lFila, 1, lFila, Grilla_Cuentas.Cols - 1) = vbBlue
    End If
ErrProcedure:
  If Not Err.Number = 0 Then
    Grilla_Cuentas.Cell(flexcpForeColor, lFila, 1, lFila, Grilla_Cuentas.Cols - 1) = vbRed
    Call SetCell(Grilla_Cuentas, lFila, "mensaje", Err.Description)
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al exportar a PDF.", Err.Description, pConLog:=True)
    Err.Clear
    Resume
  End If
End Sub

Sub DatosDelCliente()
    Dim lCursorCuentas
    Dim lReg
    Dim CursorDireccion
    Dim Reg_1
    Dim reg
    Dim Cursor_1
    Dim Cursor

    Dim IDMoneda As Integer
    'Dim Moneda As String

    'Dim Decimales As Integer

    Dim Fecha_Servidor As Date
    'Dim Fecha_Cierre As Date
    'Dim Fecha_Consulta As Date

    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Procedimiento = "PKG_CUENTAS$Buscar"

    If Not gDB.EjecutaSP Then
        GoTo Fin
    End If

    Set lCursorCuentas = gDB.Parametros("Pcursor").Valor
    gDB.Parametros.Clear

'#################### Direcciones Clientes ####################################
     For Each lReg In lCursorCuentas
        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Pid_cliente", ePT_Numero, lReg("id_cliente").Value, ePD_Entrada
        gDB.Procedimiento = "PKG_DIRECCIONES_CLIENTES$Buscar"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set CursorDireccion = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each Reg_1 In CursorDireccion
            oCliente.Direccion = Trim(NVL(Reg_1("direccion").Value, ""))
            oCliente.Comuna = NVL(Reg_1("DSC_COMUNA_CIUDAD").Value, "")
            oCliente.Ciudad = NVL(Reg_1("DSC_REGION").Value, "")
            oCliente.Telefono = NVL(Reg_1("fono").Value, "")
            
            If oCliente.Direccion = "" Then
                oCliente.Direccion = "Sin Direcci�n"
            End If

        Next
'#################### FIN Direcciones Clientes #################################

'#################### Moneda  ####################################
        'IDMoneda = lReg("id_moneda").Value
        IDMoneda = oCliente.IdMonedaSalida

        gDB.Parametros.Clear
        gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "Pid_Moneda", ePT_Numero, IDMoneda, ePD_Entrada
        gDB.Procedimiento = "PKG_MONEDAS$Buscar"

        If Not gDB.EjecutaSP Then
            GoTo Fin
        End If

        Set Cursor_1 = gDB.Parametros("Pcursor").Valor

        gDB.Parametros.Clear

        For Each Reg_1 In Cursor_1
            oCliente.Moneda = Reg_1("dsc_moneda").Value
            oCliente.Decimales = Reg_1("dicimales_mostrar").Value
        Next
        
        
'#################### FIN Moneda #################################

'PKG_CIERRES_CUENTAS$BUSCAR_ultimo
'#################### Moneda  ####################################
        
        oCliente.Fecha_Cierre = cierre_cuenta(oCliente.IdCuenta)
        
'#################### FIN Moneda #################################
        oCliente.Nombre = lReg("nombre_cliente").Value
        
        oCliente.Rut = formato_rut(lReg("rut_cliente").Value)
        
        oCliente.Num_Cuenta = lReg("num_cuenta").Value
        oCliente.Perfil_Riesgo = lReg("dsc_perfil_riesgo").Value
        oCliente.Ejecutivo = NVL(lReg("dsc_asesor").Value, "Sin Asesor")
        oCliente.Fecha_Creacion = lReg("fecha_operativa").Value
        
        oCliente.MailEjecutivo = NVL(lReg("email").Value, "")        ' mail del Asesor.
        oCliente.FonoEjecutivo = NVL(lReg("fono").Value, "")     ' Fono del Asesor.
        
        oCliente.IDEmpresa = lReg("id_empresa").Value
        
        oCliente.NombreArchivo = ""
    Next
    
Fin:

    Set lCursorCuentas = Nothing
    Set lReg = Nothing
    Set CursorDireccion = Nothing
    Set Reg_1 = Nothing
    Set reg = Nothing
    Set Cursor_1 = Nothing
    Set Cursor = Nothing

End Sub

Public Function cierre_cuenta(id_Cuenta) As String
    Dim fecha As String
    Dim reg As hFields
    Dim Fecha_Servidor As Date
    
    Fecha_Servidor = Fnt_FechaServidor()
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pid_cuenta", ePT_Numero, id_Cuenta, ePD_Entrada
    gDB.Parametros.Add "PFECHA_CIERRE", ePT_Fecha, CDate(Fecha_Servidor), ePD_Entrada
    gDB.Procedimiento = "PKG_CIERRES_CUENTAS$BUSCAR_ultimo"

    If Not gDB.EjecutaSP Then
        cierre_cuenta = ""
        Exit Function
    End If

    Set Cursor = gDB.Parametros("Pcursor").Valor

    gDB.Parametros.Clear

    For Each reg In Cursor
        'Fecha_cierre = Reg("fecha_cierre").value
        fecha = reg("fecha_cierre").Value
    Next
    cierre_cuenta = fecha
End Function

Public Sub pon_titulo(pTitulo As String, pAlto As Variant, pfont_name As String, pfont_size As Double)
    Dim ant_font_name   As String
    Dim ant_font_size   As Double
    Dim pY              As Variant
    Dim pX              As Variant


    With vp
        'lee valores anteriores
        ant_font_name = .FontName
        ant_font_size = .FontSize

        'pon valores nuevos
        ' .FontName = "Gill Sans MT"   ' pfont_name
        .FontSize = pfont_size

        ' pon titulo
        pY = .CurrentY
        pX = .CurrentX

        Call haz_linea(pY, True)

        .CalcParagraph = pTitulo
        .Paragraph = " " & pTitulo
        
        Call haz_linea(.CurrentY, True)

        'recupera valores anteriores
        .FontName = ant_font_name
        .FontSize = ant_font_size

        '.CurrentX = pX
        '.CurrentY = pY

    End With
End Sub

Private Sub vp_NewPage()
    nPage = nPage + 1
    
    Call pon_header
    
    bImprimioHeader = True
    
    'Call Indicadores
End Sub

Private Sub haz_linea(pY As Variant, Optional bColor As Boolean = False)
    Dim vColorAnt As Variant
    Dim vColorAnt2 As Variant
    vColorAnt = vp.PenColor
    vColorAnt2 = vp.TextColor
    'If bColor Then
        'vp.PenColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno) 'RGB(0, 62, 134) 'RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        vp.TextColor = 0
    'End If
    vp.DrawLine vp.MarginLeft, pY, vp.PageWidth - vp.MarginRight, pY
    'vp.DrawLine 0, 0, 0, 0
    'vp.DrawLine 0, 0, 0, 0
    vp.PenColor = vColorAnt
    vp.TextColor = vColorAnt2
End Sub

Private Sub PaginaUno()
    vp.CurrentY = vp.CurrentY + rTwips("0mm")
    pon_titulo "Reporte de Rentabilidades Historicas", "5mm", "Times New Roman", glb_tamletra_titulo
    vp.CurrentY = vp.CurrentY + rTwips("0mm")
   
    FilaAux = vp.CurrentY
    
    'CuadroActivos
    
    RentabilidadCartera
    
    'Sub_ComposicionCartera
    
    'Sub_ActivosPorMoneda
    
    'FlujoPatrimonial
    vp.CurrentY = vp.CurrentY + rTwips("1mm")

    FilaAux = vp.CurrentY
    
    Sub_GraficoEvolucionRentabilidad
    
End Sub
Sub FormatoHoja()
    With vp
        .PaperSize = pprLetter
        .PaperWidth = 12240
        .PaperHeight = 15840
        
        .MarginLeft = COLUMNA_INICIO   ' "15mm"
        .MarginRight = "15mm"
        .MarginTop = "45mm"
        .MarginBottom = "20mm"
        .MarginFooter = "15mm"
        .MarginHeader = "0mm"
        
        
        .Font.Name = "Microsoft Sans Serif"
        .Font.Size = glb_tamletra_registros - 1
        
        .LineSpacing = 110
        
        .Orientation = orLandscape
    End With
    
    sTextoIndicadores = ""
    stextovalores = ""
    sTextoValoresAnt = ""
    sTextoDosPuntos = ""
    
End Sub

Sub RentabilidadCartera()
    Dim lCursor As hRecord
    Dim lReg As hFields
    Dim iFila As Integer
    
    Dim Xinicio As Variant
    Dim YInicio As Variant
    
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "Pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_cierre", ePT_Fecha, CDate(oCliente.Fecha_Cartola), ePD_Entrada
    gDB.Parametros.Add "Pfecha_inicio", ePT_Fecha, CDate(Me.DTP_Fecha_Desde.Value), ePD_Entrada
    gDB.Parametros.Add "Pfecha_fin", ePT_Fecha, CDate(Me.DTP_Fecha_Hasta.Value), ePD_Entrada
    gDB.Parametros.Add "pId_Moneda_Salida", ePT_Numero, oCliente.IdMonedaSalida, ePD_Entrada
    gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS$RENTABILIDADES_HISTORICAS"
    
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor = gDB.Parametros("Pcursor").Valor

    gDB.Parametros.Clear

    With vp
        vp.MarginLeft = COLUMNA_INICIO ' + rTwips("150mm")
'        Xinicio = vp.MarginLeft
'        YInicio = FilaAux
        .LineSpacing = nInterlineado
'        .CurrentY = YInicio
'        FilaAux = vp.CurrentY
        .CurrentY = .CurrentY + rTwips("1mm")
        FilaAux = vp.CurrentY

        .StartTable
        .TableBorder = tbBox

        .TableCell(tcCols) = 4

        .TableCell(tcRows) = .TableCell(tcRows) + 1
        
        .TableCell(tcFontSize, 1, 1, 1, 4) = glb_tamletra_subtitulo
        .TableCell(tcBackColor, 1, 1, 1, 4) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcForeColor, 1) = RGB(255, 255, 255)    ' BLANCO
        '.TableCell(tcRowHeight, 1) = rTwips("4.6mm")
        .TableCell(tcText, 1, 1) = " RENTABILIDADES "
        .TableCell(tcText, 1, 2) = "  $     "
        .TableCell(tcText, 1, 3) = " UF   "
        .TableCell(tcText, 1, 4) = "US$ "
        
        .TableCell(tcColWidth, 1, 1) = "165mm"
        .TableCell(tcColWidth, 1, 2) = "30mm"
        .TableCell(tcColWidth, 1, 3) = "30mm"
        .TableCell(tcColWidth, 1, 4) = "30mm"
        
        .TableCell(tcColAlign, 1, 1, 1, 1) = taLeftTop
        .TableCell(tcColAlign, 1, 2, 1, 4) = taCenterMiddle
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        iFila = .TableCell(tcRows)
        For Each lReg In lCursor
            .TableCell(tcText, iFila, 1) = "Rentabilidad Mensual"
            .TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_mensual_$$").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 4) = FormatNumber(lReg("rentabilidad_mensual_DO").Value, 2, True) & "%"
            .TableCell(tcText, iFila, 3) = FormatNumber(lReg("rentabilidad_mensual_UF").Value, 2, True) & "%"
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
            .TableCell(tcText, iFila, 1) = "Rentabilidad Acumulada Anual"
            .TableCell(tcText, iFila, 2) = FormatNumber(lReg("rentabilidad_anual_$$"), 2, True) & "%"  'MMA 09/09/2008
            .TableCell(tcText, iFila, 4) = FormatNumber(lReg("rentabilidad_anual_DO"), 2, True) & "%"
            .TableCell(tcText, iFila, 3) = FormatNumber(lReg("rentabilidad_anual_UF"), 2, True) & "%"

            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
            .TableCell(tcText, iFila, 1) = "Rentabilidad �ltimos 12 meses"
            .TableCell(tcText, iFila, 2) = IIf(lReg("hay_valor_cuota_ult_12_meses") = "SI", FormatNumber(lReg("rentabilidad_ult_12_meses_$$"), 2, True) & "%", "NA")
            .TableCell(tcText, iFila, 4) = IIf(lReg("hay_valor_cuota_ult_12_meses") = "SI", FormatNumber(lReg("rentabilidad_ult_12_meses_DO"), 2, True) & "%", "NA")
            .TableCell(tcText, iFila, 3) = IIf(lReg("hay_valor_cuota_ult_12_meses") = "SI", FormatNumber(lReg("rentabilidad_ult_12_meses_UF"), 2, True) & "%", "NA")
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
            .TableCell(tcText, iFila, 1) = "Rentabilidad Desde Inicio de la Cuenta " & Format(NVL(lReg("fecha_operativa"), ""), "dd/mm/yyyy")
            oCliente.Fecha_Creacion = NVL(lReg("fecha_operativa"), "")
            .TableCell(tcText, iFila, 2) = IIf(lReg("hay_valor_cuota_inicio_cuenta") = "SI", FormatNumber(lReg("rentabilidad_inicio_cuenta_$$"), 2, True) & "%", "NA")
            .TableCell(tcText, iFila, 4) = IIf(lReg("hay_valor_cuota_inicio_cuenta") = "SI", FormatNumber(lReg("rentabilidad_inicio_cuenta_DO"), 2, True) & "%", "NA")
            .TableCell(tcText, iFila, 3) = IIf(lReg("hay_valor_cuota_inicio_cuenta") = "SI", FormatNumber(lReg("rentabilidad_inicio_cuenta_UF"), 2, True) & "%", "NA")
            .TableCell(tcRows) = .TableCell(tcRows) + 1
            iFila = .TableCell(tcRows)
            .TableCell(tcText, iFila, 1) = "Rentabilidad del " & Format(NVL(lReg("fecha_inicio"), ""), "dd/mm/yyyy") & " al " & Me.DTP_Fecha_Hasta.Value
            .TableCell(tcText, iFila, 2) = IIf(lReg("hay_valor_cuota_inicio_periodo") = "SI", FormatNumber(lReg("rentabilidad_inicio_periodo_$$"), 2, True) & "%", "NA")
            .TableCell(tcText, iFila, 4) = IIf(lReg("hay_valor_cuota_inicio_periodo") = "SI", FormatNumber(lReg("rentabilidad_inicio_periodo_DO"), 2, True) & "%", "NA")
            .TableCell(tcText, iFila, 3) = IIf(lReg("hay_valor_cuota_inicio_periodo") = "SI", FormatNumber(lReg("rentabilidad_inicio_periodo_UF"), 2, True) & "%", "NA")
        Next
        
        .TableCell(tcAlign, , 1) = taLeftMiddle
        .TableCell(tcAlign, , 2) = taRightMiddle
        .TableCell(tcAlign, , 3) = taRightMiddle
        .TableCell(tcAlign, , 4) = taRightMiddle
        .TableCell(tcFontSize, 2, 1, 6, 4) = glb_tamletra_registros
        .EndTable
    End With
    
    Set lCursor = Nothing
    Set lReg = Nothing
End Sub

' Header de la hojas de la aplicacion.
Function HeaderXls_Informe_Portada(ByVal hoja As Integer) As Integer
    Dim nFilaInicioDatos As Integer
    
    App_Excel.ActiveWindow.DisplayGridlines = False
    
    With lLibro.ActiveSheet
        'TITULO
        .Range("B5").Value = "Reporte Comisiones Devengadas"
        .Range("B5:H5").Font.Bold = True
        .Range("B5:H5").Font.Size = glb_tamletra_titulo
        .Range("B5:H5").HorizontalAlignment = xlCenter
        .Range("B5:H5").Merge
        
        .Range("B8").Value = "Empresa: " & gDsc_Empresa
        .Range("B8:D8").Merge
        .Range("F8").Value = "Fecha Generaci�n Reporte: " & Fnt_FechaServidor
        .Range("F8:H8").Merge
        
        .Range("B11").Value = "Cuenta"
        .Range("B11:H11").HorizontalAlignment = xlCenter
        .Range("C11").Value = "Rut Cliente"
        .Range("D11").Value = "Nombre Cliente"
        .Range("E11").Value = "Fecha Movimiento"
        .Range("F11").Value = "Monto"
        .Range("G11").Value = "Item / Producto"
        .Range("H11").Value = "Asesor"
        
        .Range("B11:H11").BorderAround
        .Range("B11:H11").Borders.Color = RGB(0, 0, 0)
        .Range("B11:H11").Interior.Color = RGB(255, 255, 0)
        .Range("B11:H11").Font.Bold = True
        
        nFilaInicioDatos = 12
        
    End With
    
    HeaderXls_Informe_Portada = nFilaInicioDatos  ' Columna donde comienzan los datos
    
End Function

Private Sub Sub_GraficoEvolucionRentabilidad()
    Dim sName           As String
    Dim sTitulo
    Dim fechainicrent
    Dim fechabaserent
    Dim lCursor_Cuotas  As hRecord
    Dim lCampo          As hFields
    
    Dim catnom(), Valor()
    Dim catnomc(), valorC()
    Dim catnom3(), valor3()
    
    Dim sFechaMenor
    Dim sFechaMayor
    
    Dim iValorMenor
    Dim iValorMayor
    
    Dim nRentabilidad_diaria As Double
    Dim iBase           As Integer
    
    Dim lResult     As Boolean
    
    Dim nSemestres As Double
    Dim iDiferencia As Double
    Dim bLineaTendencia As Boolean
    Dim lStep     As Long
    Dim iYear   As Double
    Dim iMonth  As Integer
    Dim iDay    As Integer
    Dim dFechaIni As Date
    Dim bFechaCompleta As Boolean
    Dim lSemestres As Integer
    
    dFechaIni = oCliente.Fecha_Creacion

    iDay = DateDiff("d", dFechaIni, Me.DTP_Fecha_Hasta)
    If iDay <= 25 Then
        lStep = 1
        bFechaCompleta = True
    Else
        If iDay > 25 And iDay <= 60 Then
            lStep = 7
            bFechaCompleta = True
        Else
            If iDay > 60 And iDay <= 120 Then
                lStep = 15
                bFechaCompleta = True
            Else
                iMonth = DateDiff("m", dFechaIni, Me.DTP_Fecha_Hasta)
                If iMonth <= 12 Then
                    lStep = 30
                    bFechaCompleta = False
                Else
                    'iYear = DateDiff("y", dFechaIni, oCliente.fFecha_Cartola) / 365
                    iYear = iDay / 365
                    If iYear > 5 Then
                        lStep = 365
                    Else
                        lStep = (Val(iYear)) * 30
                    End If
                    bFechaCompleta = False
                End If
            End If
        End If
    End If
    lResult = False
    iFilaArreglo = 0
    fechainicrent = CDate(Me.DTP_Fecha_Hasta) - 365
    fechabaserent = Me.DTP_Fecha_Hasta
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pid_cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_inicio", ePT_Fecha, CDate(oCliente.Fecha_Creacion), ePD_Entrada
    gDB.Parametros.Add "Pfecha_final", ePT_Fecha, CDate(Me.DTP_Fecha_Hasta), ePD_Entrada
    gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS$RENTABILIDAD_PERIODOS_HISTORICAS"
    
    If Not gDB.EjecutaSP Then
        MsgBox gDB.Errnum & gDB.ErrMsg, vbCritical
    Else
        Set lCursor_Cuotas = gDB.Parametros("pcursor").Valor
        If lCursor_Cuotas.Count > 0 Then
            lResult = True
            iBase = 100
            iFilaArreglo = 0
            For Each lCampo In lCursor_Cuotas
            
                nRentabilidad_diaria = CDbl(lCampo("rentab_acum").Value)
                If nRentabilidad_diaria <> 0 Then
                    ReDim Preserve catnom(iFilaArreglo)
                    ReDim Preserve Valor(iFilaArreglo)
        
                    ReDim Preserve catnomc(iFilaArreglo)
                    ReDim Preserve valorC(iFilaArreglo)
                    ReDim Preserve catnom3(iFilaArreglo)
                    ReDim Preserve valor3(iFilaArreglo)
                    
                    If iFilaArreglo = 0 Then
                        sFechaMenor = lCampo("fecha_cierre").Value
                        sFechaMayor = lCampo("fecha_cierre").Value
        
                        iValorMenor = nRentabilidad_diaria
                        iValorMayor = nRentabilidad_diaria
        
                        catnom(iFilaArreglo) = lCampo("fecha_cierre").Value
                        Valor(iFilaArreglo) = nRentabilidad_diaria
                    Else
                        catnom(iFilaArreglo) = lCampo("fecha_cierre").Value
                        Valor(iFilaArreglo) = nRentabilidad_diaria
                    End If
        
                    If sFechaMenor > lCampo("fecha_cierre").Value Then
                        sFechaMenor = lCampo("fecha_cierre").Value
                    End If
        
                    If sFechaMayor < lCampo("fecha_cierre").Value Then
                        sFechaMayor = lCampo("fecha_cierre").Value
                    End If
        
                    If iValorMenor > Valor(iFilaArreglo) Then
                        iValorMenor = Valor(iFilaArreglo)
                    End If
        
                    If iValorMayor < Valor(iFilaArreglo) Then
                        iValorMayor = Valor(iFilaArreglo)
                    End If
                    
                    If bFechaCompleta Then
                        catnom(iFilaArreglo) = Format(lCampo("fecha_cierre").Value, "dd/mm/yy")
                    Else
                        catnom(iFilaArreglo) = Mid(DameMes(lCampo("fecha_cierre").Value), 1, 3) & "-" & Mid((lCampo("fecha_cierre").Value), 9, 2)
                    End If
                    catnomc(iFilaArreglo) = lCampo("fecha_cierre").Value & "-"
                    valorC(iFilaArreglo) = CDbl(lCampo("valor_cuota_100").Value)
                    
                    iFilaArreglo = iFilaArreglo + 1
                End If
            Next
        End If
    End If
    
    gDB.Parametros.Clear

    sTitulo = ""

    bLineaTendencia = False
    lSemestres = 6 'iFilaArreglo / 182
    
    sName = Fnt_GraficoLinea(catnom, Valor, sTitulo, False, iFilaArreglo - 1, lStep, bLineaTendencia, iValorMenor, iValorMayor)

    With vp
        .CurrentY = FilaAux '+ rTwips("5mm")
        .MarginLeft = COLUMNA_INICIO
        .DrawPicture LoadPicture(sName), vp.MarginLeft, vp.CurrentY, 960 * 15, 500 * 15, , False ' True
    End With
End Sub

Function DameMes(sFecha)
    Dim iMes As Integer
    Dim aMeses
    iMes = Month(sFecha)
    aMeses = Array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
    DameMes = aMeses(iMes)
End Function

'' Realiza informe por cuenta
'Sub Genera_Informe(ByVal hoja As Integer, iFilaExcel As Integer)
'    Dim lReg                As hCollection.hFields
'    Dim oCuentasInforme     As Class_ComisionesCuentas
'
'    Dim lxHoja          As Worksheet
'
'    Dim iFila           As Integer
'    Dim iCol            As Integer
'
'    Dim iColExcel       As Integer
'    Dim lIdCuenta       As String
'
'    Dim sValor              As String
'    Dim nDecimales          As Integer
'
'    Dim dFechaAnterior      As String
'    Dim sTipo               As String
'    Dim xRango              As String
'    Dim sFormato            As String
'
'    On Error GoTo ErrProcedure
'
'    lIdCuenta = aCuentas(hoja).id_cuenta
'    Set lxHoja = lLibro.ActiveSheet
'
'    Set oCuentasInforme = New Class_ComisionesCuentas
'
'    With oCuentasInforme
'
'        If Not .Buscar_Comisiones_Informe(pId_Cuenta:=lIdCuenta, _
'                                        pFecha_ini:=DTP_Fecha_Desde.Value, _
'                                        Pfecha_fin:=DTP_Fecha_Hasta.Value) Then
'
'            GoTo ExitProcedure
'        End If
'
'        dFechaAnterior = ""
'
'        iFila = iFilaExcel - 1
'
'        If oCuentasInforme.Cursor.Count > 0 Then
'
'            lxHoja.Cells(iFila + 1, 2).Value = aCuentas(hoja).Num_Cuenta
'            lxHoja.Cells(iFila + 1, 3).Value = aCuentas(hoja).rut_cliente
'            lxHoja.Cells(iFila + 1, 4).Value = aCuentas(hoja).dsc_cuenta
'            lxHoja.Cells(iFila + 1, 8).Value = aCuentas(hoja).asesor_cuenta
'
'            For Each lReg In oCuentasInforme.Cursor
'                Call Sub_Interactivo(gRelogDB)
'
'                sFormato = "#,##0"
'                If lReg("DICIMALES_MOSTRAR").Value > 0 Then
'                    sFormato = sFormato & "." & String(lReg("DICIMALES_MOSTRAR").Value, "0")
'                End If
'
'                iFila = iFila + 1
'
'                lxHoja.Cells(iFila, 5).Value = lReg("fecha_cierre").Value
'                lxHoja.Cells(iFila, 6).Value = lReg("comision_honorarios").Value
'                lxHoja.Cells(iFila, 7).Value = lReg("DSC_INSTRUMENTO").Value
'
'                lxHoja.Cells(iFila, 6).NumberFormat = sFormato
'            Next
'
'            Set oCuentasInforme = Nothing
'        Else
'            iFila = iFila + 1
'        End If
'
'        iFila = iFila + 1
'
'        lxHoja.Cells(iFila, 4).Value = "Total Moneda"
'        lxHoja.Cells(iFila, 4).HorizontalAlignment = xlLeft
'        lxHoja.Range(lxHoja.Cells(iFila, 2), lxHoja.Cells(iFila, 3)).Merge
'        lxHoja.Range(lxHoja.Cells(iFila, 4), lxHoja.Cells(iFila, 5)).Merge
'        lxHoja.Range(lxHoja.Cells(iFila, 7), lxHoja.Cells(iFila, 8)).Merge
'
'        For iCol = 6 To 6
'            xRango = "=SUM(" & Chr(Asc("A") + iCol - 1) & iFilaExcel & ":" & Chr(Asc("A") + iCol - 1) & iFila - 1 & ")"
'            lxHoja.Cells(iFila, iCol).Formula = xRango
'        Next
'
'        lxHoja.Range(lxHoja.Cells(iFila, 4), lxHoja.Cells(iFila, 6)).Interior.Color = RGB(127, 255, 212)
'        lxHoja.Range(lxHoja.Cells(iFila, 4), lxHoja.Cells(iFila, 6)).Font.Bold = True
'
'    End With
'
'    With lxHoja
'        'Rayas verticales
'        For iCol = 2 To 8
'            .Range(.Cells(iFilaExcel, iCol), .Cells(iFila, iCol)).BorderAround
'            .Range(.Cells(iFilaExcel, iCol), .Cells(iFila, iCol)).Borders.Color = RGB(0, 0, 0)
'            .Range(.Cells(iFilaExcel, iCol), .Cells(iFila, iCol)).Borders(xlInsideHorizontal).LineStyle = xlNone
'        Next
'
'        lxHoja.Range(lxHoja.Cells(iFila, 2), lxHoja.Cells(iFila, 8)).BorderAround
'        lxHoja.Range(lxHoja.Cells(iFila, 2), lxHoja.Cells(iFila, 8)).Borders.Color = RGB(0, 0, 0)
'
'    End With
'
'    bMuestraCuadro = True
'    'Cuadros anexos en la hoja
'    If bMuestraCuadro Then
'        iFila = 13
'        ComisionesPorcentuales hoja, iFila
'        ComisionesFijas hoja, iFila
'    End If
'
'    lxHoja.Range("C1:E1").EntireColumn.AutoFit
'    lxHoja.Range("G1:O1").EntireColumn.AutoFit
'    lxHoja.Cells(1, 1).Select
'
'ErrProcedure:
'    If Not Err.Number = 0 Then
'        Call Fnt_MsgError(eLS_ErrSystem, "Problemas con el informe de Comisiones de Cuentas.", Err.Description, pConLog:=True)
'        Err.Clear
'        GoTo ExitProcedure
'        Resume
'    End If
'
'ExitProcedure:
'
'  Set oCuentasInforme = Nothing
'
'End Sub

' Realiza el cuadro completo de las comisiones porcentuales en la hoja
'Private Sub ComisionesPorcentuales(ByVal hoja As Integer, ByRef Filacomienzo)
'    Dim lReg                As hCollection.hFields
'    Dim oComisionesFijas    As Class_ComisionesCuentas
'
'    Dim lxHoja          As Worksheet
'
'
'    Dim iFila           As Integer
'    Dim iFilaInicial    As Integer
'
'    Dim iColExcel       As Integer
'    Dim lIdCuenta       As String
'
'    Dim sValor              As String
'
'    Dim nDecimales          As Integer
'
'    Dim dFechaAnterior      As String
'    Dim sTipo               As String
'    Dim xRango              As String
'
'    On Error GoTo ErrProcedure
'
'    lIdCuenta = aCuentas(hoja).id_cuenta
'    Set lxHoja = lLibro.ActiveSheet
'
'    Set oComisionesFijas = New Class_ComisionesCuentas
'
'    With oComisionesFijas
'        .Campo("ID_CUENTA").Valor = lIdCuenta
'
'        If Not .Buscar_Comisiones_Porcentuales(lIdCuenta, DTP_Fecha_Desde.Value, DTP_Fecha_Hasta.Value) Then
'            GoTo ExitProcedure
'        End If
'
'' Encabezado de cuadro
'        lxHoja.Range("K11").Value = "Comisiones Porcentuales"
'        lxHoja.Range("K11").Font.Bold = True
'        lxHoja.Range("K11:L11").Merge
'
'        lxHoja.Range("K12").Value = "Fecha Desde"
'        lxHoja.Range("K12:K13").Merge
'
'        lxHoja.Range("L12").Value = "Fecha Hasta"
'        lxHoja.Range("L12:L13").Merge
'
'        lxHoja.Range("M12").Value = "Periodicidad"
'        lxHoja.Range("M12:M13").Merge
'
'        lxHoja.Range("N12").Value = "% Comisi�n ADC"
'        lxHoja.Range("N12:N13").Merge
'
'        lxHoja.Range("O12").Value = "Instrumento"
'        lxHoja.Range("O12:O13").Merge
'
'        lxHoja.Range("K12:O13").WrapText = True
'        lxHoja.Range("K12:O13").HorizontalAlignment = xlCenter
'        lxHoja.Range("K12:O13").VerticalAlignment = xlCenter
'        lxHoja.Range("K12:O13").ColumnWidth = 12.57
'
'        lxHoja.Range("K12:O13").BorderAround
'        lxHoja.Range("K12:O13").Borders.Color = RGB(0, 0, 0)
'        lxHoja.Range("K12:O13").Interior.Color = RGB(255, 255, 0)
'        lxHoja.Range("K12:O13").Font.Bold = True
'
'        iFilaInicial = 14
'        iFila = iFilaInicial
'        iColExcel = 11
'
'        If oComisionesFijas.Cursor.Count > 0 Then
'          For Each lReg In oComisionesFijas.Cursor
'            Call Sub_Interactivo(gRelogDB)
'
'            iColExcel = 11
'            lxHoja.Cells(iFila, iColExcel).Value = lReg("FECHA_INI").Value
'            lxHoja.Cells(iFila, iColExcel + 1).Value = lReg("FECHA_TER").Value
'            lxHoja.Cells(iFila, iColExcel + 2).Value = lReg("DSC_COMISION_FIJA_PERIODICIDAD").Value
'            lxHoja.Cells(iFila, iColExcel + 3).Value = lReg("COMISION_HONORARIOS").Value * 100
'            lxHoja.Cells(iFila, iColExcel + 4).Value = lReg("DSC_INTRUMENTO").Value
'            iFila = iFila + 1
'          Next
'
'          lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).BorderAround
'          lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).Borders.Color = RGB(0, 0, 0)
'          lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).Borders(xlInsideHorizontal).LineStyle = xlNone
'          lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel + 3), lxHoja.Cells(iFila - 1, iColExcel + 4)).NumberFormat = "#,##0.00"
'
'        Else
'          lxHoja.Cells(iFila, iColExcel).Value = "No tiene"
'          lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).BorderAround
'          lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).Borders.Color = RGB(0, 0, 0)
'          lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).Borders(xlInsideVertical).LineStyle = xlNone
'
'          iFila = iFila + 1
'        End If
'
'        Filacomienzo = iFila
'
'    End With
'
'ErrProcedure:
'    If Not Err.Number = 0 Then
'        Call Fnt_MsgError(eLS_ErrSystem, "Problemas con el informe de Comisiones de Cuenta.", Err.Description, pConLog:=True)
'        Err.Clear
'        GoTo ExitProcedure
'        Resume
'    End If
'
'ExitProcedure:
'
'  Set oComisionesFijas = Nothing
'
'End Sub

' Realizar el cuadro completo de las comisiones fijas en la hoja
'Private Sub ComisionesFijas(ByVal hoja As Integer, iFilaComienzo As Integer)
'    Dim lReg                As hCollection.hFields
'    Dim oComisionesFijas    As Class_ComisionesCuentas
'
'    Dim lxHoja          As Worksheet
'
'    Dim iFila           As Integer
'    Dim iFilaInicial    As Integer
'
'    Dim iColExcel       As Integer
'    Dim lIdCuenta       As String
'
'    Dim sValor              As String
'    Dim nDecimales          As Integer
'
'    Dim dFechaAnterior      As String
'    Dim sTipo               As String
'    Dim xRango              As String
'
'    On Error GoTo ErrProcedure
'
'    lIdCuenta = aCuentas(hoja).id_cuenta
'    Set lxHoja = lLibro.ActiveSheet
'
'    Set oComisionesFijas = New Class_ComisionesCuentas
'
'    With oComisionesFijas
'        .Campo("ID_CUENTA").Valor = lIdCuenta
'
'        If Not .Buscar_Comisiones_Fijas(lIdCuenta, DTP_Fecha_Desde.Value, DTP_Fecha_Hasta.Value) Then
'            GoTo ExitProcedure
'        End If
'
'' Encabezado de cuadro
'        iFila = iFilaComienzo + 1
'
'        lxHoja.Range("K" & iFila).Value = "Comisiones Fijas"
'        lxHoja.Range("K" & iFila).Font.Bold = True
'        lxHoja.Range("K" & iFila & ":L" & iFila).Merge
'
'        iFila = iFila + 1
'        lxHoja.Range("K" & iFila).Value = "Fecha Desde"
'        lxHoja.Range("K" & iFila & ":K" & iFila + 1).Merge
'
'        lxHoja.Range("L" & iFila).Value = "Fecha Hasta"
'        lxHoja.Range("L" & iFila & ":L" & iFila + 1).Merge
'
'        lxHoja.Range("M" & iFila).Value = "Periodicidad"
'        lxHoja.Range("M" & iFila & ":M" & iFila + 1).Merge
'
'        lxHoja.Range("N" & iFila).Value = "Comisi�n ADC"
'        lxHoja.Range("N" & iFila & ":N" & iFila + 1).Merge
'
'        lxHoja.Range("O" & iFila).Value = "Instrumento"
'        lxHoja.Range("O" & iFila & ":O" & iFila + 1).Merge
'
'        lxHoja.Range("K" & iFila & ":O" & iFila + 1).WrapText = True
'        lxHoja.Range("K" & iFila & ":O" & iFila + 1).HorizontalAlignment = xlCenter
'        lxHoja.Range("K" & iFila & ":O" & iFila + 1).VerticalAlignment = xlCenter
'        lxHoja.Range("K" & iFila & ":O" & iFila + 1).ColumnWidth = 12.57
'
'        lxHoja.Range("K" & iFila & ":O" & iFila + 1).BorderAround
'        lxHoja.Range("K" & iFila & ":O" & iFila + 1).Borders.Color = RGB(0, 0, 0)
'        lxHoja.Range("K" & iFila & ":O" & iFila + 1).Interior.Color = RGB(255, 255, 0)
'        lxHoja.Range("K" & iFila & ":O" & iFila + 1).Font.Bold = True
'
'        iFilaInicial = iFila + 2
'        iFila = iFilaInicial
'        iColExcel = 11
'
'        If oComisionesFijas.Cursor.Count > 0 Then
'            For Each lReg In oComisionesFijas.Cursor
'              Call Sub_Interactivo(gRelogDB)
'
'                iColExcel = 11
'                lxHoja.Cells(iFila, iColExcel).Value = lReg("FECHA_INI").Value
'                lxHoja.Cells(iFila, iColExcel + 1).Value = lReg("FECHA_TER").Value
'                lxHoja.Cells(iFila, iColExcel + 2).Value = lReg("DSC_COMISION_FIJA_PERIODICIDAD").Value
'                lxHoja.Cells(iFila, iColExcel + 3).Value = lReg("MONTO_COMISION_HONORARIO").Value
'                lxHoja.Cells(iFila, iColExcel + 4).Value = lReg("DSC_INTRUMENTO").Value
'                iFila = iFila + 1
'            Next
'
'            lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).BorderAround
'            lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).Borders.Color = RGB(0, 0, 0)
'            lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).Borders(xlInsideHorizontal).LineStyle = xlNone
'            lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel + 3), lxHoja.Cells(iFila - 1, iColExcel + 4)).NumberFormat = "#,##0.00"
'
'        Else
'            lxHoja.Cells(iFila, iColExcel).Value = "No tiene"
'            lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).BorderAround
'            lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).Borders.Color = RGB(0, 0, 0)
'            lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).Borders(xlInsideVertical).LineStyle = xlNone
'            iFila = iFila + 1
'        End If
'
'    End With
'
'ErrProcedure:
'    If Not Err.Number = 0 Then
'        Call Fnt_MsgError(eLS_ErrSystem, "Problemas con el informe de Comisiones de Cuenta.", Err.Description, pConLog:=True)
'        Err.Clear
'        GoTo ExitProcedure
'        Resume
'    End If
'
'ExitProcedure:
'
'  Set oComisionesFijas = Nothing
'
'End Sub

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call cmb_buscar_Cuenta_Click
    End If
End Sub

Private Sub Txt_Rut_Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call cmb_buscar_Rut_Click
    End If
End Sub

Private Sub TxtRazonSocial_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call cmb_Buscar_NombreCliente_Click
    End If
End Sub

Public Sub Mostrar_Cuenta()
Dim pFecha              As Date
Dim lRutCliente As String
Dim lNombreCliente As String
Dim lNumCuenta As String
Dim lDesError As String
Dim lFecOperativa  As String
Dim lFechaCierre  As Date
Dim lcCuenta As Object

    lDesError = ""
    pFecha = Fnt_FechaServidor
    Call Sub_Entrega_DatosCuenta(lId_Cuenta, pRut_Cliente:=lRutCliente, pNombre_Cliente:=lNombreCliente, pNum_Cuenta:=lNumCuenta, PError:=lDesError)
    If lId_Cuenta <> "" And lId_Cuenta <> "0" Then
        Txt_Rut_Cliente.Text = lRutCliente
        TxtRazonSocial.Text = lNombreCliente
        Txt_Num_Cuenta.Text = lNumCuenta
        Call Sub_CargaCombo_Asesor(Cmb_Asesor, pBlanco:=True)
    End If
End Sub

Function ColorToHex(r, g, b)
    ColorToHex = "&H" & Hex(r) & Hex(g) & Hex(b)
End Function

Private Function Fnt_GraficoLinea(Categories1, Vals1, sTitulo, leyenda, entries, nSemestres, bLineaTendencia, iValorMenor, iValorMayor) As String
Dim cd
Dim c
Dim xColor          As String
Dim xColorTitulo    As String
Dim noOfPoints      As Integer
Dim Path            As String
Dim sFname As String
Dim X As Integer
Dim Y As Integer

    On Local Error Resume Next

    xColorTitulo = ColorToHex(0, 0, 0)
    
    noOfPoints = 1
    
    Set cd = CreateObject("ChartDirector.API")
    
    cd.setLicenseCode ("YT5ZJ59NKAF9ZR3942DAE53E")
    
    X = 960 * 2 '450
    Y = 500 * 2 '320
    
    Set c = cd.XYChart(X, Y)
    
    Call c.setPlotArea(90, 90, X - 100, Y - 180)
    Call c.yaxis().setLabelStyle("arialbd.ttf", 14, &H0&) '&H555555)
    Call c.yaxis().setLabelFormat("{value|2.}%")
    Call c.yaxis().setLinearScale(iValorMenor, iValorMayor)
    
    Call c.xAxis().setLabels(Categories1)
    Call c.xAxis().setLabelStyle("arialbd.ttf", 14, &H0&, 30).setpos(0, -1)
    'Call c.xAxis().setLabelStep(nSemestres) '30)   'Indica el espaciado de las etiquetas.... cada 30 d�as o 180 d�as
    Call c.xAxis().setLabelStep(nSemestres) '30)   'Indica el espaciado de las etiquetas.... cada 30 d�as o 180 d�as
    
    Call c.addLineLayer(Vals1, 0)
    sTitulo = "<*size=14,yoffset=-11*><*block*><*color=" & xColorTitulo & "*>" & sTitulo & "<*br*><*/*>"
    Call c.addTitle(sTitulo, "GILB____.TTF", 14, xColorTitulo)  ' .setBackground(xColor)

    Path = Environ$("TEMP") & "\"

    sFname = c.makeTmpFile(Path & sFname, cd.JPG)
    
    Fnt_GraficoLinea = Path & sFname

End Function

Function formato_rut(rut_bruto)
    Dim rut_salida, dv
    'Dim temp(3)
    Dim Rut
    Dim f
    Dim rut_salida3
    Dim rut_salida2
    Dim rut_salida1
    
    rut_salida = Trim(rut_bruto)
    dv = Right(rut_salida, 1)
    Rut = Left(rut_salida, Len(rut_salida) - 2)
    f = Len(Rut)
    
    If f > 6 Then
        rut_salida3 = Mid(Rut, f - 2, 3)
        f = f - 3
        rut_salida2 = Mid(Rut, f - 2, 3)
        f = f - 3
        If f < 3 Then
            rut_salida1 = Left(Rut, f)
        End If
        formato_rut = rut_salida1 & "." & rut_salida2 & "." & rut_salida3 & "-" & dv
    Else
        formato_rut = rut_bruto
    End If
End Function

Public Sub pon_header()
    Dim xAnt As Variant
    Dim yAnt As Variant
    Dim xtalign As Variant
    Dim xbrushcolor As Variant
    Dim xpencolor As Variant
    Dim xtextcolor As Variant
    Dim xfontsize As Variant
    Dim xmarginleft As Variant
    Dim xAjusteAnt As Variant
    Dim sColorTextoAnt      As Long
    '-------------------------------------------------------------------
    Dim xValTop As Variant
    Dim sRutaImagen As String
    '-------------------------------------------------------------------
    Dim Xinicio As Variant
    Dim YInicio As Variant
    '-------------------------------------------------------------------
    Dim lreg_vtc            As hCollection.hFields
    Dim lValor_Tipo_Cambio  As New Class_Valor_Tipo_Cambio
    '-------------------------------------------------------------------
    
    vp.FontBold = False
    
    vp.MarginLeft = COLUMNA_INICIO
    
    sRutaImagen = App.Path & "\security.jpg"
    
    xValTop = ""
    
    'Inserta Logo
    On Error Resume Next
    vp.DrawPicture LoadPicture(sRutaImagen), "200mm", "7mm", "65mm", "10mm"
    
    On Error GoTo 0
    
    'Guarda valores anteriores
    xAnt = vp.CurrentX
    yAnt = vp.CurrentY
    xtalign = vp.TextAlign
    xbrushcolor = vp.BrushColor
    xpencolor = vp.PenColor
    xtextcolor = vp.TextColor
    xfontsize = vp.FontSize
    xmarginleft = vp.MarginLeft
    sColorTextoAnt = vp.TextColor
    
    vp.CurrentX = vp.MarginLeft
    vp.CurrentY = "20mm"
        
'Asigna nuevos valores
    vp.TextAlign = taCenterTop
    vp.BrushColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = RGB(255, 255, 255) ' RGB(100, 100, 100)
    
    vp.FontSize = glb_tamletra_titulo
    'Llena texto
    vp.TextBox "", vp.MarginLeft, vp.CurrentY, "122.7mm", "7mm", True, False, True
    'Asigna nuevos valores
    vp.TextAlign = taLeftTop
    vp.BrushColor = RGB(242, 242, 242)
    'vp.PenColor = RGB(232, 226, 222)
    vp.TextColor = RGB(0, 62, 134)    ' sColorTextoAnt  ' &H0&
    vp.FontSize = glb_tamletra_registros
    
    'Llena texto
    Xinicio = vp.MarginLeft
    YInicio = vp.CurrentY
    
    Call vp.DrawRectangle(Xinicio, YInicio, Xinicio + rTwips("123mm"), YInicio + rTwips("22mm"))

    ' vp.MarginLeft = "79mm"
    vp.MarginLeft = Xinicio + rTwips("2mm")
    vp.CurrentX = Xinicio + rTwips("2mm")
    vp.CurrentY = YInicio + rTwips("2mm")
    
    Xinicio = vp.CurrentX
    
    'vp.CalcText = UCase("Se�ores" & vbCrLf & _
              oCliente.Nombre & vbCrLf & _
              "RUT N�: " & oCliente.Rut & vbCrLf & _
              oCliente.Direccion & vbCrLf & _
              oCliente.Comuna & " " & oCliente.Ciudad)
              
    vp.Text = UCase("Se�ores" & vbCrLf & _
              oCliente.Nombre & vbCrLf & _
              "RUT N�: " & oCliente.Rut & vbCrLf & _
              oCliente.Direccion & vbCrLf & _
              oCliente.Comuna & " " & oCliente.Ciudad)
              
' ####################################################  Cuadro Dos
    vp.MarginLeft = COLUMNA_DOS ' + rTwips("150mm")
    Xinicio = vp.MarginLeft
    YInicio = "20mm"
                
    'Asigna nuevos valores
    vp.TextAlign = taCenterTop
    vp.BrushColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    'vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = RGB(0, 62, 134)     ' RGB(100, 100, 100)
    
    vp.FontSize = glb_tamletra_titulo
    'Llena texto
    vp.TextBox " ", Xinicio, YInicio, "124.7mm", "7mm", True, False, True
    
    'Asigna nuevos valores
    vp.TextAlign = taLeftTop
    vp.BrushColor = RGB(242, 242, 242)   ' RGB(232, 226, 222)
    'vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = RGB(0, 62, 134)
    vp.FontSize = glb_tamletra_registros
    
    'Llena texto
     vp.MarginLeft = Xinicio ' + rTwips("3mm")
    Xinicio = vp.MarginLeft
    YInicio = vp.CurrentY
    
    vp.DrawRectangle Xinicio, _
                    YInicio, _
                    Xinicio + rTwips("125mm"), _
                    YInicio + rTwips("22mm")

    vp.MarginLeft = Xinicio + rTwips("2mm")
    vp.CurrentX = Xinicio + rTwips("2mm")
    vp.CurrentY = YInicio + rTwips("2mm")
    
    vp.Text = UCase("Informe al " & vbCrLf & _
                    "N� Cuenta " & vbCrLf & _
                    "Asesor" & vbCrLf & _
                    "Email" & vbCrLf & _
                    "Telefono")
    
    
    vp.MarginLeft = Xinicio + rTwips("22mm")
    vp.CurrentX = Xinicio + rTwips("22mm")
    vp.CurrentY = YInicio + rTwips("2mm")
    
    vp.Text = UCase(": " & oCliente.Fecha_Cartola & vbCrLf & _
              ": " & oCliente.Num_Cuenta & vbCrLf & _
              ": " & oCliente.Ejecutivo & vbCrLf & _
              ": " & oCliente.MailEjecutivo & vbCrLf & _
              ": " & oCliente.FonoEjecutivo)
              
'********************************************************************************
' Indicadores
'********************************************************************************
'-----------------------------------------------------------------------------
    xAjusteAnt = vp.TextAlign
    
    If sTextoIndicadores = "" Then    ' Es la primera vez que pasa por ac�
    
        sTextoIndicadores = "Valores al"
        sTextoDosPuntos = ":"
        stextovalores = Me.DTP_Fecha_Consulta
        sTextoValoresAnt = Me.DTP_Fecha_Desde
        
        With lValor_Tipo_Cambio
        
            .Campo("FECHA").Valor = Me.DTP_Fecha_Hasta
            If .BuscarIndicadores Then
                For Each lreg_vtc In .Cursor
                    sTextoIndicadores = sTextoIndicadores & vbCrLf & Trim(lreg_vtc("DSC_MONEDA").Value)
                    sTextoDosPuntos = sTextoDosPuntos & vbCrLf & ":"
                    stextovalores = stextovalores & vbCrLf & FormatNumber(lreg_vtc("VALOR").Value, 2)
                Next
            End If
                    
            '.Campo("FECHA").Valor = Me.DTP_Fecha_Desde
            'If .BuscarIndicadores Then
            '    For Each lreg_vtc In .Cursor
            '        sTextoValoresAnt = sTextoValoresAnt & vbCrLf & FormatNumber(lreg_vtc("VALOR").Value, 2)
            '    Next
            'End If
            
        End With
        
        Set lValor_Tipo_Cambio = Nothing
    End If
    
    vp.MarginLeft = Xinicio + rTwips("66mm")
    vp.CurrentX = vp.MarginLeft
    vp.CurrentY = YInicio + rTwips("3mm")
    
    vp.StartTable
    
    vp.TableCell(tcRows) = 1
    vp.TableCell(tcCols) = 4
    vp.TableBorder = tbNone
    vp.TableCell(tcFontSize) = 7.5
    
    vp.TableCell(tcText, 1, 1) = UCase(sTextoIndicadores)
    vp.TableCell(tcText, 1, 2) = sTextoDosPuntos
    vp.TableCell(tcText, 1, 3) = UCase(stextovalores)
    'vp.TableCell(tcText, 1, 4) = UCase(sTextoValoresAnt)
    
    vp.TableCell(tcColWidth, 1, 1) = rTwips("21mm")
    vp.TableCell(tcColWidth, 1, 2) = rTwips("4mm")
    vp.TableCell(tcColWidth, 1, 3) = rTwips("17mm")
    vp.TableCell(tcColWidth, 1, 4) = rTwips("17mm")
    
    vp.TableCell(tcColAlign, 1, 2) = taLeftMiddle
    vp.TableCell(tcColAlign, 1, 3, 1, 4) = taRightMiddle
    
    vp.EndTable
        
'----------------------------------------------------------------------------
    vp.MarginLeft = COLUMNA_INICIO
    vp.CurrentY = YInicio + rTwips("23mm")
    vp.CurrentX = vp.MarginLeft
    
    
    vp.TextAlign = xtalign
    vp.BrushColor = xbrushcolor
    vp.PenColor = xpencolor
    vp.TextAlign = xAjusteAnt
    vp.TextColor = sColorTextoAnt
End Sub

