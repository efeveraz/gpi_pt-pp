VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Frm_Migracion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Migraci�n de datos"
   ClientHeight    =   6420
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   9345
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6420
   ScaleWidth      =   9345
   Begin VB.Frame Frnm_Grilla_Sucesos 
      Caption         =   "Visor de Sucesos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4515
      Left            =   90
      TabIndex        =   4
      Top             =   1860
      Width           =   9165
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   4095
         Left            =   120
         TabIndex        =   5
         Top             =   270
         Width           =   7965
         _cx             =   14049
         _cy             =   7223
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Migracion.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Detalle 
         Height          =   330
         Left            =   8160
         TabIndex        =   6
         Top             =   540
         Width           =   900
         _ExtentX        =   1588
         _ExtentY        =   582
         ButtonWidth     =   1588
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Detalle"
               Key             =   "DETALLE"
               Object.ToolTipText     =   "Muestra el detalle de los sucesos del Cierre"
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1365
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   9195
      Begin VB.Frame Fra_Planilla 
         Caption         =   "Planilla"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1125
         Left            =   120
         TabIndex        =   8
         Top             =   150
         Width           =   1995
         Begin VB.OptionButton Opt_Apo_Res 
            Caption         =   "Aporte/Rescate"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   90
            TabIndex        =   11
            Top             =   810
            Width           =   1815
         End
         Begin VB.OptionButton Opt_FFMM 
            Caption         =   "FFMM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   90
            TabIndex        =   10
            Top             =   525
            Width           =   795
         End
         Begin VB.OptionButton Opt_Accion 
            Caption         =   "RV-RF"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   90
            TabIndex        =   9
            Top             =   240
            Value           =   -1  'True
            Width           =   885
         End
      End
      Begin hControl2.hTextLabel Txt_ArchivoPlano 
         Height          =   315
         Left            =   2190
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   300
         Width           =   5640
         _ExtentX        =   9948
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "Planilla Excel"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin MSComDlg.CommonDialog Cmd_AbreArchivo 
         Left            =   3240
         Top             =   150
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin MSComctlLib.Toolbar Toolbar_Buscar_Archivo 
         Height          =   330
         Left            =   7920
         TabIndex        =   7
         Top             =   300
         Width           =   1080
         _ExtentX        =   1905
         _ExtentY        =   582
         ButtonWidth     =   1746
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Buscar"
               Key             =   "BUSCAR"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9345
      _ExtentX        =   16484
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Migrar"
            Key             =   "MIGRAR"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Migracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const fc_Mercado = "N"
Const fcDias_Liquid_ACC = 2
Const fcDias_Liquid_FFMM = 1

Private Enum eExcel_Acciones
  eGD_Cliente = 1
  eGD_Secuencia
  eGD_Fecha
  eGD_Informacion
  eGD_Folio
  eGD_Cod_Movimiento
  eGD_Dsc_Movimiento
  eGD_Cod_Concepto
  eGD_Dsc_Concepto
  eGD_Cargo
  eGD_Abono
  eGD_Instrumento
  eGD_Precio
  eGD_Entrada
  eGD_Salida
  eGD_Emisor
  eGD_Fecha_Emision
  eGD_Fecha_Vcto
  eGD_Valor_Rescate
  eGD_Tasa
  eGD_Mercado
End Enum

Public Enum eExcel_FFMM
  eGD_rut_Cliente = 1
  eGD_Nombre_Cliente
  eGD_Codigo_Fondo
  eGD_Nombre_Fondo
  eGD_Fecha_Movimiento
  eGD_Tipo_Operacion
  eGD_Nro_Operacion
  eGD_Monto
  eGD_Valor_Cuota
  eGD_Nro_Cuotas
  eGD_Saldo_Cuotas
End Enum

Public Enum eExcel_Apo_Res
  eGD_Cliente = 1
  eGD_Caja_Cuenta
  eGD_Des_Apo_Res
  eGD_Tipo_Mov
  eGD_Fecha_Mov
  eGD_Monto
End Enum

'Public Enum eExcel_FFMM
'  eGD_Cliente_FFMM = 1
'  eGD_Moneda = 11
'  eGD_Fecha_Emision = 13
'  eGD_Fecha_Desde
'  eGD_Fecha_Hasta
'  eGD_Fecha_Blanco_FFMM = 17
'  eGD_Fondo_Mutuo = 19
'  eGD_Serie = 21
'  eGD_Fecha_Saldo_Inicial = 24
'  eGD_Saldo_Ini
'  eGD_Valor_Cuota_Ini
'  eGD_Monto
'  eGD_Fecha_Mov
'  eGD_Operacion
'  eGD_Folio_FFMM
'  eGD_Monto_Aporte
'  eGD_Monto_Rescate
'  eGD_Valor_Cuota
'  eGD_Nro_Cuotas
'  eGD_Saldo_Cuotas
'  eGD_Fecha_Saldo_Fin
'  eGD_Saldo_Fin
'  eGD_Valor_Cuota_Fin
'  eGD_Monto_Fin
'End Enum

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("MIGRAR").Image = cBoton_Grabar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("EXIT").Image = cBoton_Salir
  End With

  With Toolbar_Buscar_Archivo
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("BUSCAR").Image = "boton_grilla_buscar"
  End With
  
  With Toolbar_Detalle
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("DETALLE").Image = cBoton_Agregar_Grilla
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
Dim lMensaje As String

  With Grilla
    If .Row > 0 Then
      lMensaje = GetCell(Grilla, .Row, "TEXTO")
      If Not lMensaje = "" Then
        MsgBox lMensaje, vbInformation, Me.Caption
      End If
    End If
  End With

End Sub

Private Sub Toolbar_Buscar_Archivo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "BUSCAR"
      Call Sub_Busca_Planilla
  End Select
End Sub

Private Sub Sub_Busca_Planilla()

On Error GoTo Cmd_BuscarArchivo_Err
    
  With Cmd_AbreArchivo
    .CancelError = True
    .InitDir = "C:\Mis documentos\"
    .Filter = "Texto (*.xls)|*.xls"
    .Flags = cdlOFNFileMustExist
  
    .ShowOpen
  End With
  
  Rem Se coloca el path completo, que incluye el nombre de archivo
  Txt_ArchivoPlano.Text = Cmd_AbreArchivo.FileName
  
  Exit Sub

Cmd_BuscarArchivo_Err:
  If Err = 32755 Then 'usuario presiona el boton Cancelar del Command Dialog
    Exit Sub
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "MIGRAR"
      Call Sub_Bloquea_Puntero(Me)
      If Opt_Accion Then
        Call Sub_Migrar_Acciones
      ElseIf Opt_FFMM Then
        Call Sub_Migrar_Fondos
      ElseIf Opt_Apo_Res Then
        Call Sub_Migrar_Aportes_Rescates
      End If
      Call Sub_Desbloquea_Puntero(Me)
    Case "REFRESH"
      Call Sub_CargaForm
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_CargaForm()
  Txt_ArchivoPlano.Text = ""
  Grilla.Rows = 1
  
  Opt_Accion = True
  Opt_FFMM = False
End Sub

Private Sub Sub_Migrar_Acciones()
Dim lExcel As New Excel.Application
Dim lLibro As Excel.Workbook
Dim lHoja As Excel.Worksheet
'-----------------------------------
Dim lFila As Double
Dim lcAlias As Object
Dim lCliente As String
Dim lId_Cuenta As String
Dim lFecha As Date
Dim lFecha_Operativa As Date
Dim lFolio As String
Dim lCod_Mov As String
Dim lDsc_Mov As String
Dim lNemotecnico As String
Dim lInformacion As String
Dim lCod_Concepto As String
Dim lDsc_Concepto As String
Dim lDsc_Cargo_Abono As String
'-----------------------------------
Dim lcNemotecnicos As Class_Nemotecnicos
Dim lId_Nemotecnico As String
Dim lCod_Instrumento As String
'-----------------------------------
Dim lcIva As Class_Iva
Dim lValor_Iva As Double
Dim lcComisiones As Class_Comisiones_Instrumentos
Dim lPorcentaje_Comision As Double
Dim lGastos As Double
Dim lPorcentaje_Derechos As Double
'------------------------------------
Dim lCargo_Abono As Class_Cargos_Abonos
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lId_Moneda As String
Dim lId_Caja_Cuenta As String
Dim lTipo_Cargo As String
Dim lMonto As Double
Dim lCargo As String
Dim lAbono As String
Dim lMonto_Valido As Boolean

On Error GoTo ErrProcedure
  
  Grilla.Rows = 1
  
  If Txt_ArchivoPlano.Text = "" Then
    MsgBox "Debe ingresar la ruta de la planilla Excel.", vbCritical, Me.Caption
    Exit Sub
  End If
  
  '------------------------------------------------
  Rem Valor Iva del Sistema
  Set lcIva = New Class_Iva
  With lcIva
    If .Buscar(True) Then
      lValor_Iva = .Cursor(1)("valor").Value
    End If
  End With
  '------------------------------------------------
        
  lExcel.Visible = False
  Rem Abre el libro excel
  Set lLibro = lExcel.Workbooks.Open(Txt_ArchivoPlano.Text, ReadOnly:=False, Editable:=True)
  Rem abre la hoja del libro a procesar
  Set lHoja = lLibro.Worksheets(1)

  With lHoja
    Rem Setea la linea de inicio de recorrido del excel
    lFila = 2
    Call Fnt_Escribe_Grilla(Grilla, "N", "Inicio Proceso Migraci�n RV-RF: " & Now & ".")
    Do While Not .Cells(lFila, eExcel_Acciones.eGD_Fecha) = ""
      
      lInformacion = Trim(NVL(.Cells(lFila, eExcel_Acciones.eGD_Informacion), ""))
      lFolio = Trim(NVL(.Cells(lFila, eExcel_Acciones.eGD_Folio), ""))
      lCod_Mov = Trim(NVL(.Cells(lFila, eExcel_Acciones.eGD_Cod_Movimiento), ""))
      lDsc_Mov = Trim(NVL(.Cells(lFila, eExcel_Acciones.eGD_Dsc_Movimiento), ""))
      lCod_Concepto = Trim(NVL(.Cells(lFila, eExcel_Acciones.eGD_Cod_Concepto), ""))
      lDsc_Concepto = Trim(NVL(.Cells(lFila, eExcel_Acciones.eGD_Dsc_Concepto), ""))
      lFecha = Trim(NVL(.Cells(lFila, eExcel_Acciones.eGD_Fecha), ""))
      lCliente = Trim(NVL(.Cells(lFila, eExcel_Acciones.eGD_Cliente), ""))
      
      lFecha_Operativa = Fnt_Busca_Fecha_Operativa(lCliente)
        
      lId_Cuenta = ""
      Set lcAlias = Fnt_CreateObject(cDLL_Alias)
      Set lcAlias.gDB = gDB
      lId_Cuenta = NVL(lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                               , pCodigoCSBPI:=cTabla_Cuentas _
                                               , pValor:=lCliente), "")
      Set lcAlias = Nothing
      
      '---------------------------------------------------------------------------------
      Rem Renta Variable y Renta Fija
      Rem "CIRV" = COMPRA RV
      Rem "VIRV" = VENTA RV
      Rem "CIRF" = COMPRA RF
      Rem "VIRF" = VENTA RF
      Rem "IIRV" = INGRESO RV
      Rem "RIRV" = RETIRO RV
      Rem "IIRF" = INGRESO RF
      Rem "RIRF" = RETIRO RF
      '---------------------------------------------------------------------------------
      If lCod_Mov = "CIRV" Or _
         lCod_Mov = "VIRV" Or _
         lCod_Mov = "CIRF" Or _
         lCod_Mov = "VIRF" Or _
         lCod_Mov = "IIRV" Or _
         lCod_Mov = "RIRV" Or _
         lCod_Mov = "IIRF" Or _
         lCod_Mov = "RIRF" Then
        
        If lFecha >= lFecha_Operativa Then
        
          lNemotecnico = Trim(NVL(.Cells(lFila, eExcel_Acciones.eGD_Instrumento), ""))
          
          Set lcNemotecnicos = New Class_Nemotecnicos
          lcNemotecnicos.Campo("nemotecnico").Valor = lNemotecnico
          
          lCod_Instrumento = ""
          Rem Busca el id_nemotecnico del Nemotecnico de la planilla
          If lcNemotecnicos.Buscar_Nemotecnico(pCod_Producto:=Null, pMostrar_Msg:=False) Then
            lId_Nemotecnico = lcNemotecnicos.Campo("id_nemotecnico").Valor
            
            Rem Si encuentra el nemotecnico en el sistema, procede a realizar la operacion
            If Not lId_Nemotecnico = "0" Then
              Rem Busca atributos del Nemotecnico
              If lcNemotecnicos.Buscar Then
                lCod_Instrumento = NVL(lcNemotecnicos.Cursor(1)("cod_instrumento").Value, "")
              Else
                Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & lcNemotecnicos.ErrMsg)
              End If
            Else
              Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": El Nemot�cnico '" & lNemotecnico & "' no se encuentra en el sistema.")
            End If
          Else
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & lcNemotecnicos.ErrMsg)
          End If
          Set lcNemotecnicos = Nothing
          
          Rem Si el Id_Cuenta es vacio hace la operacion
          If Not lId_Cuenta = "" And Not lCod_Instrumento = "" Then
            Rem Comisiones
            Set lcComisiones = New Class_Comisiones_Instrumentos
            With lcComisiones
              .Campo("Id_Cuenta").Valor = lId_Cuenta
              .Campo("COD_INSTRUMENTO").Valor = lCod_Instrumento
              If .Buscar(True) Then
                If .Cursor.Count > 0 Then
                  lPorcentaje_Comision = lcIva.Porcentaje_Iva(.Cursor(1)("COMISION").Value)
                  lGastos = Int(NVL(.Cursor(1)("GASTOS").Value, 0))
                  lPorcentaje_Derechos = lcIva.Porcentaje_Iva(NVL(.Cursor(1)("DERECHOS_BOLSA").Value, 0))
                End If
              End If
            End With
            Set lcComisiones = Nothing
            
            Rem Por cada linea de la planilla Excel agrega una operaci�n cuando corresponda
            If Fnt_Grabar_Operacion(pHoja:=lHoja, _
                                    pFolio:=lFolio, _
                                    pFila:=lFila, _
                                    pId_Cuenta:=lId_Cuenta, _
                                    pCod_Instrumento:=lCod_Instrumento, _
                                    pPorcentaje_Comision:=lPorcentaje_Comision, _
                                    pGastos:=lGastos, _
                                    pPorcentaje_Derechos:=lPorcentaje_Derechos, _
                                    pValor_Iva:=lValor_Iva) Then
    
              Call Fnt_Escribe_Grilla(Grilla, "N", "Fila " & lFila & ": Ingreso correcto de la Operaci�n.")
            End If
          Else
            If lId_Cuenta = "" Then
              Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": El Rut no se encuentra en el sistema.")
            End If
          End If
'        Else
'          Call Fnt_Escribe_Grilla(Grilla, "A", "Fila " & lFila & ": Fecha de Movimiento anterior a la Fecha Operativa de la Cuenta.")
        End If
      '---------------------------------------------------------------------------------
      Rem Intermediacion Financiera
      Rem "CIIF" = COMPRA IF
      Rem "VIIF" = VENTA IF
      Rem "IIIF" = INGRESO IF
      Rem "RIIF" = RETIRO IF
      '---------------------------------------------------------------------------------
      ElseIf lCod_Mov = "CIIF" Or _
             lCod_Mov = "VIIF" Or _
             lCod_Mov = "IIIF" Or _
             lCod_Mov = "RIIF" Then
        
        If lFecha >= lFecha_Operativa Then
          lCod_Instrumento = gcINST_DEPOSITOS_NAC
          
          Rem Si el Id_Cuenta es vacio hace la operacion
          If Not lId_Cuenta = "" Then
            Rem Comisiones
            Set lcComisiones = New Class_Comisiones_Instrumentos
            With lcComisiones
              .Campo("Id_Cuenta").Valor = lId_Cuenta
              .Campo("COD_INSTRUMENTO").Valor = lCod_Instrumento
              If .Buscar(True) Then
                If .Cursor.Count > 0 Then
                  lPorcentaje_Comision = lcIva.Porcentaje_Iva(.Cursor(1)("COMISION").Value)
                  lGastos = Int(NVL(.Cursor(1)("GASTOS").Value, 0))
                  lPorcentaje_Derechos = lcIva.Porcentaje_Iva(NVL(.Cursor(1)("DERECHOS_BOLSA").Value, 0))
                End If
              End If
            End With
            Set lcComisiones = Nothing
            
            Rem Por cada linea de la planilla Excel agrega una operaci�n cuando corresponda
            If Fnt_Grabar_Operacion(pHoja:=lHoja, _
                                    pFolio:=lFolio, _
                                    pFila:=lFila, _
                                    pId_Cuenta:=lId_Cuenta, _
                                    pCod_Instrumento:=lCod_Instrumento, _
                                    pPorcentaje_Comision:=lPorcentaje_Comision, _
                                    pGastos:=lGastos, _
                                    pPorcentaje_Derechos:=lPorcentaje_Derechos, _
                                    pValor_Iva:=lValor_Iva) Then
    
              Call Fnt_Escribe_Grilla(Grilla, "N", "Fila " & lFila & ": Ingreso correcto de la Operaci�n.")
            End If
          Else
            If lId_Cuenta = "" Then
              Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": El Rut no se encuentra en el sistema.")
            End If
          End If
'        Else
'          Call Fnt_Escribe_Grilla(Grilla, "A", "Fila " & lFila & ": Fecha de Movimiento anterior a la Fecha Operativa de la Cuenta.")
        End If
      '---------------------------------------------------------------------------------
      Rem Movimientos de Caja
      Rem Excepto para estos Movimientos de Caja ya procesados en operaciones:
      Rem "FCRV" = FACTURA COMPRA RV
      Rem "FVRV" = FACTURA VENTA RV
      Rem "FCRF" = FACTURA COMPRA RF
      Rem "FVRF" = FACTURA VENTA RF
      Rem "FCIF" = FACTURA COMPRA IF
      Rem "FVIF" = FACTURA VENTA IF
      Rem "ABMN" = ABONO EN DINERO y "CN" = COBRO CLIENTE POR FACT.COMPRA ACCIONES
      Rem "CAMN" = CARGO EN DINERO y "CN" = GIRO A CLIENTE POR FACT.COMPRA ACCIONES
      Rem "CAMN" = CARGO EN DINERO y "C7" = PAGO DE DIVIDENDO
      '---------------------------------------------------------------------------------
      ElseIf lInformacion = "Caja" Then
        If (lCod_Mov = "FCRV") Or _
           (lCod_Mov = "FVRV") Or _
           (lCod_Mov = "FCRF") Or _
           (lCod_Mov = "FVRF") Or _
           (lCod_Mov = "FCIF") Or _
           (lCod_Mov = "FVIF") Or _
           (lCod_Mov = "ABMN" And lCod_Concepto = "CN") Or _
           (lCod_Mov = "CAMN" And lCod_Concepto = "CN") Or _
           (lCod_Mov = "CAMN" And lCod_Concepto = "C7") Then
          Rem NO HACER MOVIMIENTO DE CAJA
          
        Else
          If lFecha >= lFecha_Operativa Then
            If Not lId_Cuenta = "" Then
              Rem Moneda Peso Nacional
              lId_Moneda = 1
              
              Set lcCaja_Cuenta = New Class_Cajas_Cuenta
              With lcCaja_Cuenta
                .Campo("id_cuenta").Valor = lId_Cuenta
                .Campo("id_moneda").Valor = lId_Moneda
                .Campo("cod_mercado").Valor = fc_Mercado
                If .Buscar_Caja_Para_Invertir() Then
                  If .Cursor.Count > 0 Then
                    lId_Caja_Cuenta = NVL(.Cursor(1)("id_caja_cuenta").Value, "")
                  End If
                Else
                  Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & lcCaja_Cuenta.ErrMsg)
                End If
              End With
              Set lcCaja_Cuenta = Nothing
              
              lCargo = Trim(NVL(.Cells(lFila, eExcel_Acciones.eGD_Cargo), ""))
              lAbono = Trim(NVL(.Cells(lFila, eExcel_Acciones.eGD_Abono), ""))
              
              lMonto_Valido = True
              If lCargo = "" And Not lAbono = "" Then
                lTipo_Cargo = gcTipoOperacion_Abono
                lMonto = lAbono
              ElseIf lAbono = "" And Not lCargo = "" Then
                lTipo_Cargo = gcTipoOperacion_Cargo
                lMonto = lCargo
              Else
                Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": No hay montos asociados al Movimiento.")
                lMonto_Valido = False
              End If
    
              If lMonto_Valido Then
                Rem Se realizan Movimientos de Caja
                Set lCargo_Abono = New Class_Cargos_Abonos
                With lCargo_Abono
                  .Campo("id_cargo_abono").Valor = cNewEntidad
                  .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_CargoAbono
                  .Campo("Id_Mov_Caja").Valor = cNewEntidad
                  .Campo("id_Caja_Cuenta").Valor = lId_Caja_Cuenta
                  .Campo("id_Cuenta").Valor = lId_Cuenta
                  If lCod_Mov = "ABMN" Or lCod_Mov = "CAMN" Then
                    lDsc_Cargo_Abono = lDsc_Concepto
                  Else
                    lDsc_Cargo_Abono = lDsc_Mov
                  End If
                  .Campo("Dsc_Cargo_Abono").Valor = lDsc_Cargo_Abono
                  .Campo("Flg_Tipo_Cargo").Valor = lTipo_Cargo
                  .Campo("fecha_Movimiento").Valor = lFecha
                  .Campo("retencion").Valor = 0
                  .Campo("monto").Valor = lMonto
                  .Campo("Id_Moneda").Valor = lId_Moneda
                  .Campo("FLG_TIPO_ORIGEN").Valor = gcFlg_Tipo_Origen_Migracion  'automatica
                  .Campo("cod_estado").Valor = "L"
                  '.Campo("Id_Tipo_Estado").Valor = cTEstado_Mov_Caja        'Tipo de estado Mov_caja = 4
                  If .Guardar Then
                    Call Fnt_Escribe_Grilla(Grilla, "N", "Fila " & lFila & ": Ingreso correcto del Movimiento de Caja. Descripci�n Movimiento: " & lDsc_Cargo_Abono & ".")
                  Else
                    Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & .ErrMsg & ".")
                  End If
                End With
                Set lCargo_Abono = Nothing
              End If
            Else
              Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": El Rut no se encuentra en el sistema.")
            End If
'          Else
'            Call Fnt_Escribe_Grilla(Grilla, "A", "Fila " & lFila & ": Fecha de Movimiento anterior a la Fecha Operativa de la Cuenta.")
          End If
        End If
      End If
      
      lFila = lFila + 1
    Loop
    Call Fnt_Escribe_Grilla(Grilla, "N", "T�rmino Proceso Migraci�n RV-RF: " & Now & ".")
    
  End With
  
  MsgBox "Terminado el proceso de carga de la Planilla Excel", vbInformation, Me.Caption
  
ErrProcedure:
  If Not Err.Number = 0 Then
    If MsgBox("Error en la carga de planilla Excel." & vbCr & vbCr & Err.Description, vbApplicationModal + vbCritical + vbOKCancel, Me.Caption) = vbRetry Then
      Resume
    End If
  End If
  
  lExcel.DisplayAlerts = False
  lExcel.Quit
End Sub

Private Function Fnt_Grabar_Operacion(pHoja As Excel.Worksheet, _
                                      pFolio, _
                                      ByRef pFila, _
                                      pId_Cuenta, _
                                      pCod_Instrumento, _
                                      pPorcentaje_Comision, _
                                      pGastos, _
                                      pPorcentaje_Derechos, _
                                      pValor_Iva) As Boolean
Dim lcClase As Object
Dim lId_Caja_Cuenta As String
Dim lFila_Detalle As Long
Dim lSalir As Boolean
'---------------------------------------
Dim lFecha As String
Dim lCod_Mov As String
Dim lInformacion As String
Dim lFolio As String
Dim lTipo_Mov As String
Dim lCargo As String
Dim lAbono As String
Dim lNemotecnico As String
Dim lPrecio As String
Dim lTasa As Double
Dim lEntrada As String
Dim lSalida As String
Dim lId_Moneda As Double
Dim lCantidad As Double
Dim lMonto As Double
Dim lMonto_Operacion As Double
Dim lMonto_Operacion_Planilla As Double
Dim lFecha_Operacion As Date
Dim lFecha_Liquidacion As Date
Dim lTipo_Movimiento As String
Dim lMonto_Neto As Double
Dim lTipo_Operacion As String
Dim lFecha_Vencimiento As Date
Dim lEmisor As String
Dim lFecha_Emision As Date
Dim lCod_Emisor As String
Dim lValor_Rescate As Double
'----------------------------------------
Dim lcNemotecnicos As Class_Nemotecnicos
Dim lId_Nemotecnico As Double
'----------------------------------------
Dim lComision
Dim lDerechos
Dim lIva
'----------------------------------------
Dim lRecord_Detalle As hRecord
Dim lReg As hFields
'Dim lRecord_Nemotecnicos As hRecord
'Dim lReg_Nemo As hFields
'Dim lReg1 As hFields
'Dim lReg2 As hFields
'Dim lEsta As Boolean
'----------------------------------------
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
'----------------------------------------
Dim lcBonos As Class_Bonos
Dim lcDepositos As Class_Depositos
Dim lMsg_Error As String
'----------------------------------------
Dim lcEmisor_Especifico As Class_Emisores_Especifico
  
'  Set lRecord_Nemotecnicos = New hRecord
'  With lRecord_Nemotecnicos
'    .ClearFields
'    .AddField "ID_NEMOTECNICO", 0
'    .LimpiarRegistros
'  End With
  
  Set lRecord_Detalle = New hRecord
  With lRecord_Detalle
    .ClearFields
    .AddField "ID_NEMOTECNICO", 0
    .AddField "CANTIDAD", 0
    .AddField "PRECIO", 0
    .AddField "MONTO_DETALLE", 0
    .AddField "FECHA_VENCIMIENTO"
    .LimpiarRegistros
  End With
  
  Fnt_Grabar_Operacion = True
  
  Rem La Moneda es Peso Nacional = 1
  lId_Moneda = 1
     
  Select Case pCod_Instrumento
  
Rem ------------------------------------------------------------------------------------------------------
Rem ------------ ACCIONES --------------------------------------------------------------------------------
Rem ------------------------------------------------------------------------------------------------------
    Case gcINST_ACCIONES_NAC
      Set lcClase = New Class_Acciones
      lcClase.fMigracion = True
      
      lFila_Detalle = pFila
      lSalir = False
      
      While Not lSalir
        With pHoja
          lFecha = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Fecha), ""))
          lCod_Mov = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Cod_Movimiento), ""))
          lInformacion = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Informacion), ""))
          lFolio = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Folio), ""))
          lTipo_Mov = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Cod_Movimiento), ""))
          lCargo = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Cargo), ""))
          lAbono = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Abono), ""))
          lNemotecnico = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Instrumento), ""))
          lPrecio = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Precio), ""))
          lEntrada = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Entrada), ""))
          lSalida = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Salida), ""))
        End With
          
        If pFolio = lFolio And _
           (lCod_Mov = "CIRV" Or _
            lCod_Mov = "VIRV" Or _
            lCod_Mov = "IIRV" Or _
            lCod_Mov = "RIRV") Then
          
          Set lcNemotecnicos = New Class_Nemotecnicos
          lcNemotecnicos.Campo("nemotecnico").Valor = lNemotecnico
          Rem Busca el id_nemotecnico del Nemotecnico de la planilla
          If lcNemotecnicos.Buscar_Nemotecnico(pCod_Producto:="", pMostrar_Msg:=False) Then
            lId_Nemotecnico = lcNemotecnicos.Campo("id_nemotecnico").Valor
            
            If lId_Nemotecnico = "0" Then
              Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": Nemot�cnico " & lNemotecnico & " no existe o est� vencido. Operaci�n cancelada.")
              GoTo ErrProcedure
            End If
          Else
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": " & lcNemotecnicos.ErrMsg)
            GoTo ErrProcedure
          End If
          Set lcNemotecnicos = Nothing
          
          If lCod_Mov = "CIRV" Then
            lCantidad = lEntrada
            lTipo_Operacion = gcOPERACION_Directa
          ElseIf lCod_Mov = "VIRV" Then
            lCantidad = lSalida
            lTipo_Operacion = gcOPERACION_Directa
          ElseIf lCod_Mov = "IIRV" Then
            lCantidad = lEntrada
            lTipo_Operacion = gcOPERACION_Custodia
            Rem Fecha de la Operacion y Liquidacion
            lFecha_Operacion = lFecha
            lFecha_Liquidacion = Fnt_Calcula_Dia_Habil(lFecha_Operacion, fcDias_Liquid_ACC)
            Rem Setea la ultima fila procesada, para empezar desde esta fila el ciclo de afuera
            pFila = lFila_Detalle
            Rem Tipo Movimiento Ingreso
            lTipo_Movimiento = gcTipoOperacion_Ingreso
            lSalir = True
          ElseIf lCod_Mov = "RIRV" Then
            lCantidad = lSalida
            lTipo_Operacion = gcOPERACION_Custodia
            Rem Fecha de la Operacion y Liquidacion
            lFecha_Operacion = lFecha
            lFecha_Liquidacion = Fnt_Calcula_Dia_Habil(lFecha_Operacion, fcDias_Liquid_ACC)
            Rem Setea la ultima fila procesada, para empezar desde esta fila el ciclo de afuera
            pFila = lFila_Detalle
            Rem Tipo Movimiento Egreso
            lTipo_Movimiento = gcTipoOperacion_Egreso
            lSalir = True
          End If
          
          lMonto = lCantidad * lPrecio
          
          If Not Fnt_Busca_Nemotecnico(lRecord_Detalle, lId_Nemotecnico, lCantidad, lMonto) Then
            Set lReg = lRecord_Detalle.Add
            lReg("ID_NEMOTECNICO").Value = lId_Nemotecnico
            lReg("CANTIDAD").Value = lCantidad
            lReg("PRECIO").Value = lPrecio
            lReg("MONTO_DETALLE").Value = lMonto
          End If
          
        ElseIf pFolio = lFolio And (Not lCod_Mov = "ABMN") And (Not lCod_Mov = "CAMN") Then
          If lCod_Mov = "FCRV" Then
            lMonto_Operacion_Planilla = lCargo
            lTipo_Movimiento = gcTipoOperacion_Ingreso
          ElseIf lCod_Mov = "FVRV" Then
            lMonto_Operacion_Planilla = lAbono
            lTipo_Movimiento = gcTipoOperacion_Egreso
          End If
          
          Rem Fecha de la Operacion y Liquidacion
          lFecha_Operacion = lFecha
          lFecha_Liquidacion = Fnt_Calcula_Dia_Habil(lFecha_Operacion, fcDias_Liquid_ACC)
          
          Rem Setea la ultima fila procesada, para empezar desde esta fila el ciclo de afuera
          pFila = lFila_Detalle
          
          lSalir = True
        End If
        lFila_Detalle = lFila_Detalle + 1
      Wend
            
'      For Each lReg1 In lRecord_Detalle
'        For Each lReg2 In lRecord_Detalle
'          If Not lReg1("id_nemotecnico").Value = lReg2("id_nemotecnico").Value Then
'            lEsta = False
'            For Each lReg In lRecord_Nemotecnicos
'              If lReg1("id_nemotecnico").Value = lReg("id_nemotecnico").Value Then
'                lEsta = True
'                Exit For
'              End If
'            Next
'            If Not lEsta Then
'              Set lReg_Nemo = lRecord_Nemotecnicos.Add
'              lReg_Nemo("id_nemotecnico").Value = lReg1("id_nemotecnico").Value
'            End If
'          End If
'        Next
'      Next
            
      lId_Caja_Cuenta = ""
      Set lcCaja_Cuenta = New Class_Cajas_Cuenta
      With lcCaja_Cuenta
        .Campo("id_cuenta").Valor = pId_Cuenta
        .Campo("id_moneda").Valor = lId_Moneda
        .Campo("cod_mercado").Valor = fc_Mercado
        If .Buscar_Caja_Para_Invertir(lId_Nemotecnico) Then
          If .Cursor.Count > 0 Then
            lId_Caja_Cuenta = NVL(.Cursor(1)("id_caja_cuenta").Value, "")
          End If
        Else
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lcCaja_Cuenta.ErrMsg)
          GoTo ErrProcedure
        End If
      End With
      Set lcCaja_Cuenta = Nothing
      
      If lId_Caja_Cuenta = "" Then
        Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": No existe una caja para la cuenta.")
        GoTo ErrProcedure
      End If
            
      For Each lReg In lRecord_Detalle
        Call lcClase.Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("id_nemotecnico").Value, _
                                                 pCantidad:=lReg("cantidad").Value, _
                                                 pPrecio:=lReg("precio").Value, _
                                                 pId_Moneda:=lId_Moneda, _
                                                 pMonto:=lReg("monto_detalle").Value, _
                                                 pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                 pPrecio_Historico:="")
        lMonto_Neto = lMonto_Neto + lReg("monto_detalle").Value
      Next
      
      If lTipo_Operacion = gcOPERACION_Directa Then
        
        lComision = Int((lMonto_Neto * pPorcentaje_Comision) / 100)
        lDerechos = Int((lMonto_Neto * pPorcentaje_Derechos) / 100)
        lIva = Int((Int(lComision) + Int(pGastos) + Int(lDerechos)) * pValor_Iva)
         
        If lTipo_Movimiento = gcTipoOperacion_Ingreso Then
          lMonto_Operacion = lMonto_Neto + lComision + lIva + pGastos + lDerechos
        ElseIf lTipo_Movimiento = gcTipoOperacion_Egreso Then
          lMonto_Operacion = lMonto_Neto - (lComision + lIva + pGastos + lDerechos)
        End If
        
        Rem Verifica si se cobraron Gastos
        '-------------------------------------------
        If lMonto_Neto = lMonto_Operacion_Planilla Then
          Rem Si el Monto Neto es igual al Monto Total que aparece en la planilla,
          Rem no se cobraron comisiones por la operacion
          lComision = 0
          lDerechos = 0
          lIva = 0
        ElseIf Not lMonto_Operacion = lMonto_Operacion_Planilla Then
          pGastos = Int(Abs(lMonto_Operacion - lMonto_Operacion_Planilla) / (1 + pValor_Iva))
          lIva = Int((Int(lComision) + Int(pGastos) + Int(lDerechos)) * pValor_Iva)
        End If
        
        If Not lcClase.Realiza_Operacion_Directa(pId_Operacion:=cNewEntidad, _
                                                 pId_Cuenta:=pId_Cuenta, _
                                                 pDsc_Operacion:="", _
                                                 pTipoOperacion:=lTipo_Movimiento, _
                                                 pId_Contraparte:="", _
                                                 pId_Representante:="", _
                                                 pId_Moneda_Operacion:=lId_Moneda, _
                                                 pFecha_Operacion:=lFecha_Operacion, _
                                                 pFecha_Vigencia:=lFecha_Operacion, _
                                                 pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                 pId_Trader:="", _
                                                 pPorc_Comision:=pPorcentaje_Comision / 100, _
                                                 pComision:=lComision, _
                                                 pDerechos_Bolsa:=lDerechos, _
                                                 pGastos:=pGastos, _
                                                 pIva:=lIva, _
                                                 pMonto_Operacion:=lMonto_Operacion_Planilla, _
                                                 pTipo_Precio:=cTipo_Precio_Mercado, _
                                                 pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
          
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lcClase.ErrMsg)
          GoTo ErrProcedure
        End If
      ElseIf lTipo_Operacion = gcOPERACION_Custodia Then
        If Not lcClase.Realiza_Operacion_Custodia(pId_Operacion:=cNewEntidad, _
                                                  pId_Cuenta:=pId_Cuenta, _
                                                  pDsc_Operacion:="", _
                                                  pTipoOperacion:=lTipo_Movimiento, _
                                                  pId_Contraparte:="", _
                                                  pId_Representante:="", _
                                                  pId_Moneda_Operacion:=lId_Moneda, _
                                                  pFecha_Operacion:=lFecha_Operacion, _
                                                  pFecha_Vigencia:=lFecha_Operacion, _
                                                  pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                  pId_Trader:="", _
                                                  pPorc_Comision:=0, _
                                                  pComision:=0, _
                                                  pDerechos_Bolsa:=0, _
                                                  pGastos:=0, _
                                                  pIva:=0, _
                                                  pMonto_Operacion:=lMonto_Neto, _
                                                  pTipo_Precio:=cTipo_Precio_Mercado, _
                                                  pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lcClase.ErrMsg)
          GoTo ErrProcedure
        End If
      End If
Rem ---------------------------------------------------------------------------------------------------
Rem ------------ BONOS --------------------------------------------------------------------------------
Rem ---------------------------------------------------------------------------------------------------
    Case gcINST_BONOS_NAC
      Set lcClase = New Class_Bonos
      lcClase.fMigracion = True
      
      lFila_Detalle = pFila
      lSalir = False

      While Not lSalir
        With pHoja
          lFecha = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Fecha), ""))
          lCod_Mov = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Cod_Movimiento), ""))
          lInformacion = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Informacion), ""))
          lFolio = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Folio), ""))
          lTipo_Mov = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Cod_Movimiento), ""))
          lCargo = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Cargo), ""))
          lAbono = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Abono), ""))
          lNemotecnico = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Instrumento), ""))
          lTasa = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Tasa), ""))
          lEntrada = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Entrada), ""))
          lSalida = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Salida), ""))
        End With

        If pFolio = lFolio And _
           (lCod_Mov = "CIRF" Or _
            lCod_Mov = "VIRF" Or _
            lCod_Mov = "IIRF" Or _
            lCod_Mov = "RIRF") Then

          Set lcNemotecnicos = New Class_Nemotecnicos
          lcNemotecnicos.Campo("nemotecnico").Valor = lNemotecnico
          Rem Busca el id_nemotecnico del Nemotecnico de la planilla
          If lcNemotecnicos.Buscar_Nemotecnico(pCod_Producto:="", pMostrar_Msg:=False) Then
            lId_Nemotecnico = lcNemotecnicos.Campo("id_nemotecnico").Valor

            If lId_Nemotecnico = "0" Then
              Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": Nemot�cnico " & lNemotecnico & " no existe o est� vencido. Operaci�n cancelada.")
              GoTo ErrProcedure
            End If

          End If
          Set lcNemotecnicos = Nothing

          If lCod_Mov = "CIRF" Then
            lCantidad = lEntrada
            lTipo_Operacion = gcOPERACION_Directa
          ElseIf lCod_Mov = "VIRF" Then
            lCantidad = lSalida
            lTipo_Operacion = gcOPERACION_Directa
          ElseIf lCod_Mov = "IIRF" Then
            lCantidad = lEntrada
            lTipo_Operacion = gcOPERACION_Custodia
            Rem Fecha de la Operacion
            lFecha_Operacion = lFecha
            Rem Setea la ultima fila procesada, para empezar desde esta fila el ciclo de afuera
            pFila = lFila_Detalle
            Rem Tipo Movimiento Ingreso
            lTipo_Movimiento = gcTipoOperacion_Ingreso
            lSalir = True
          ElseIf lCod_Mov = "RIRF" Then
            lCantidad = lSalida
            lTipo_Operacion = gcOPERACION_Custodia
            Rem Fecha de la Operacion
            lFecha_Operacion = lFecha
            Rem Setea la ultima fila procesada, para empezar desde esta fila el ciclo de afuera
            pFila = lFila_Detalle
            Rem Tipo Movimiento Egreso
            lTipo_Movimiento = gcTipoOperacion_Egreso
            lSalir = True
          End If

          Rem Valoriza el Nemotecnico
          Set lcBonos = New Class_Bonos
          lMonto = lcBonos.ValorizaPapel(lId_Nemotecnico, lFecha, lTasa, lCantidad, False, lMsg_Error)
          Set lcBonos = Nothing
                 
          If Not lMsg_Error = "" Then
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lMsg_Error)
            GoTo ErrProcedure
          End If
          
          If Not Fnt_Busca_Nemotecnico(lRecord_Detalle, lId_Nemotecnico, lCantidad, lMonto) Then
            Set lReg = lRecord_Detalle.Add
            lReg("ID_NEMOTECNICO").Value = lId_Nemotecnico
            lReg("CANTIDAD").Value = lCantidad
            lReg("PRECIO").Value = lTasa
            lReg("MONTO_DETALLE").Value = lMonto
          End If

        ElseIf pFolio = lFolio And (Not lCod_Mov = "ABMN") And (Not lCod_Mov = "CAMN") Then
          If lCod_Mov = "FCRF" Then
            lMonto_Operacion_Planilla = lCargo
            lTipo_Movimiento = gcTipoOperacion_Ingreso
          ElseIf lCod_Mov = "FVRF" Then
            lMonto_Operacion_Planilla = lAbono
            lTipo_Movimiento = gcTipoOperacion_Egreso
          End If
          
          Rem Fecha de la Operacion
          lFecha_Operacion = lFecha

          Rem Setea la ultima fila procesada, para empezar desde esta fila el ciclo de afuera
          pFila = lFila_Detalle

          lSalir = True
        End If
        lFila_Detalle = lFila_Detalle + 1
      Wend

      lId_Caja_Cuenta = ""

      Set lcCaja_Cuenta = New Class_Cajas_Cuenta
      With lcCaja_Cuenta
        .Campo("id_cuenta").Valor = pId_Cuenta
        .Campo("id_moneda").Valor = lId_Moneda
        .Campo("cod_mercado").Valor = fc_Mercado
        If .Buscar_Caja_Para_Invertir(lId_Nemotecnico) Then
          If .Cursor.Count > 0 Then
            lId_Caja_Cuenta = NVL(.Cursor(1)("id_caja_cuenta").Value, "")
          End If
        Else
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lcCaja_Cuenta.ErrMsg)
          GoTo ErrProcedure
        End If
      End With
      Set lcCaja_Cuenta = Nothing

      If lId_Caja_Cuenta = "" Then
        Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": No existe una caja para la cuenta.")
        GoTo ErrProcedure
      End If

      For Each lReg In lRecord_Detalle
        Call lcClase.Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("Id_Nemotecnico").Value, _
                                                 pCantidad:=lReg("Cantidad").Value, _
                                                 pTasa:=lReg("Precio").Value, _
                                                 PTasa_Gestion:=lReg("Precio").Value, _
                                                 pId_Moneda:=lId_Moneda, _
                                                 pMonto:=lReg("Monto_detalle").Value, _
                                                 pUtilidad:=0, _
                                                 pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                 pTasa_Historico:="", _
                                                 pId_Mov_Activo_Compra:="")
        lMonto_Neto = lMonto_Neto + lReg("monto_detalle").Value
      Next

      If lTipo_Operacion = gcOPERACION_Directa Then
      
        lComision = Int((lMonto_Neto * pPorcentaje_Comision) / 100)
        lDerechos = Int((lMonto_Neto * pPorcentaje_Derechos) / 100)
        lIva = Int((Int(lComision) + Int(pGastos) + Int(lDerechos)) * pValor_Iva)
  
        If lTipo_Movimiento = gcTipoOperacion_Ingreso Then
          lMonto_Operacion = lMonto_Neto + lComision + lIva + pGastos + lDerechos
        ElseIf lTipo_Movimiento = gcTipoOperacion_Egreso Then
          lMonto_Operacion = lMonto_Neto - (lComision + lIva + pGastos + lDerechos)
        End If
        
        Rem Verifica si se cobraron Gastos
        '-------------------------------------------
        If lMonto_Neto = lMonto_Operacion_Planilla Then
          Rem Si el Monto Neto es igual al Monto Total que aparece en la planilla,
          Rem no se cobraron comisiones por la operacion
          lComision = 0
          lDerechos = 0
          lIva = 0
        ElseIf Not lMonto_Operacion = lMonto_Operacion_Planilla Then
          pGastos = Int(Abs(lMonto_Operacion - lMonto_Operacion_Planilla) / (1 + pValor_Iva))
          lIva = Int((Int(lComision) + Int(pGastos) + Int(lDerechos)) * pValor_Iva)
        End If
        
        If Not lcClase.Realiza_Operacion_Directa(pId_Operacion:=cNewEntidad, _
                                                 pId_Cuenta:=pId_Cuenta, _
                                                 pDsc_Operacion:="", _
                                                 pTipoOperacion:=lTipo_Movimiento, _
                                                 pId_Contraparte:="", _
                                                 pId_Representante:="", _
                                                 pId_Moneda_Operacion:=lId_Moneda, _
                                                 pFecha_Operacion:=lFecha_Operacion, _
                                                 pFecha_Liquidacion:=lFecha_Operacion, _
                                                 pId_Trader:="", _
                                                 pPorc_Comision:=pPorcentaje_Comision / 100, _
                                                 pComision:=lComision, _
                                                 pDerechos_Bolsa:=lDerechos, _
                                                 pGastos:=pGastos, _
                                                 pIva:=lIva, _
                                                 pMonto_Operacion:=lMonto_Operacion_Planilla, _
                                                 pTipo_Precio:=cTipo_Precio_Mercado, _
                                                 pId_Caja_Cuenta:=lId_Caja_Cuenta) Then

          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lcClase.ErrMsg)
          GoTo ErrProcedure
        End If
      ElseIf lTipo_Operacion = gcOPERACION_Custodia Then
        If Not lcClase.Realiza_Operacion_Custodia(pId_Operacion:=cNewEntidad, _
                                                  pId_Cuenta:=pId_Cuenta, _
                                                  pDsc_Operacion:="", _
                                                  pTipoOperacion:=lTipo_Movimiento, _
                                                  pId_Contraparte:="", _
                                                  pId_Representante:="", _
                                                  pId_Moneda_Operacion:=lId_Moneda, _
                                                  pFecha_Operacion:=lFecha_Operacion, _
                                                  pFecha_Liquidacion:=lFecha_Operacion, _
                                                  pId_Trader:="", _
                                                  pPorc_Comision:=0, _
                                                  pComision:=0, _
                                                  pDerechos_Bolsa:=0, _
                                                  pGastos:=0, _
                                                  pIva:=0, _
                                                  pMonto_Operacion:=lMonto_Neto, _
                                                  pTipo_Precio:=cTipo_Precio_Mercado, _
                                                  pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lcClase.ErrMsg)
          GoTo ErrProcedure
        End If
      End If
      
Rem ---------------------------------------------------------------------------------------------------
Rem ------------ DEPOSITO --------------------------------------------------------------------------------
Rem ---------------------------------------------------------------------------------------------------
    Case gcINST_DEPOSITOS_NAC
      Set lcClase = New Class_Depositos
      lcClase.fMigracion = True
      
      lFila_Detalle = pFila
      lSalir = False

      While Not lSalir
        With pHoja
          lFecha = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Fecha), ""))
          lCod_Mov = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Cod_Movimiento), ""))
          lInformacion = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Informacion), ""))
          lFolio = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Folio), ""))
          lTipo_Mov = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Cod_Movimiento), ""))
          lCargo = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Cargo), ""))
          lAbono = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Abono), ""))
          lNemotecnico = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Instrumento), ""))
          lTasa = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Tasa), ""))
          'lEntrada = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Entrada), ""))
          'lSalida = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Salida), ""))
          lEmisor = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Emisor), ""))
          lFecha_Emision = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Fecha_Emision), ""))
          lFecha_Vencimiento = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Fecha_Vcto), ""))
          lValor_Rescate = Trim(NVL(.Cells(lFila_Detalle, eExcel_Acciones.eGD_Valor_Rescate), ""))
        End With

        If pFolio = lFolio And _
           (lCod_Mov = "CIIF" Or _
            lCod_Mov = "VIIF" Or _
            lCod_Mov = "IIIF" Or _
            lCod_Mov = "RIIF") Then
          
          Rem Busca el Codigo SVS del Emisor
          lCod_Emisor = ""
          Set lcEmisor_Especifico = New Class_Emisores_Especifico
          With lcEmisor_Especifico
            .Campo("DSC_EMISOR_ESPECIFICO").Valor = lEmisor
            If .Buscar() Then
              If .Cursor.Count > 0 Then
                lCod_Emisor = .Cursor(1)("COD_SVS_NEMOTECNICO").Value
              Else
                Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": Emisor " & lEmisor & " no existe en el sistema.")
                GoTo ErrProcedure
              End If
            Else
              Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & .ErrMsg)
              GoTo ErrProcedure
            End If
          End With
          Set lcEmisor_Especifico = Nothing
          
          lMsg_Error = ""
          If Not Fnt_CreaNemotecnico_Deposito(pCodigoEmisor:=lCod_Emisor, _
                                              pFecha_Vencimiento:=lFecha_Vencimiento, _
                                              PId_Moneda_Deposito:=lId_Moneda, _
                                              pId_Moneda_Pago:=lId_Moneda, _
                                              pTasa_Emision:=lTasa, _
                                              pFecha_Emision:=lFecha_Emision, _
                                              pId_Nemotecnico:=lId_Nemotecnico, _
                                              pNemotecnico:=lNemotecnico, _
                                              pMensaje:=lMsg_Error) Then
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lMsg_Error)
            GoTo ErrProcedure
          End If
          
          If lCod_Mov = "CIIF" Then
            lCantidad = lValor_Rescate
            lTipo_Operacion = gcOPERACION_Directa
          ElseIf lCod_Mov = "VIIF" Then
            lCantidad = lValor_Rescate
            lTipo_Operacion = gcOPERACION_Directa
          ElseIf lCod_Mov = "IIIF" Then
            lCantidad = lValor_Rescate
            lTipo_Operacion = gcOPERACION_Custodia
            Rem Fecha de la Operacion
            lFecha_Operacion = lFecha
            Rem Setea la ultima fila procesada, para empezar desde esta fila el ciclo de afuera
            pFila = lFila_Detalle
            Rem Tipo Movimiento Ingreso
            lTipo_Movimiento = gcTipoOperacion_Ingreso
            lSalir = True
          ElseIf lCod_Mov = "RIIF" Then
            lCantidad = lValor_Rescate
            lTipo_Operacion = gcOPERACION_Custodia
            Rem Fecha de la Operacion
            lFecha_Operacion = lFecha
            Rem Setea la ultima fila procesada, para empezar desde esta fila el ciclo de afuera
            pFila = lFila_Detalle
            Rem Tipo Movimiento Egreso
            lTipo_Movimiento = gcTipoOperacion_Egreso
            lSalir = True
          End If
          
          lMsg_Error = ""
          Rem Valoriza el Nemotecnico
          Set lcDepositos = New Class_Depositos
          lMonto = lcDepositos.ValorizaPapel(lNemotecnico, lFecha, lTasa, lCantidad, "", False, lMsg_Error)
          Set lcDepositos = Nothing
                 
          If Not lMsg_Error = "" Then
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lMsg_Error)
            GoTo ErrProcedure
          End If
          
          If Not Fnt_Busca_Nemotecnico(lRecord_Detalle, lId_Nemotecnico, lCantidad, lMonto) Then
            Set lReg = lRecord_Detalle.Add
            lReg("ID_NEMOTECNICO").Value = lId_Nemotecnico
            lReg("CANTIDAD").Value = lCantidad
            lReg("PRECIO").Value = lTasa
            lReg("MONTO_DETALLE").Value = lMonto
            lReg("FECHA_VENCIMIENTO").Value = lFecha_Vencimiento
          End If

        ElseIf pFolio = lFolio And (Not lCod_Mov = "ABMN") And (Not lCod_Mov = "CAMN") Then
          If lCod_Mov = "FCIF" Then
            lMonto_Operacion_Planilla = lCargo
            lTipo_Movimiento = gcTipoOperacion_Ingreso
          ElseIf lCod_Mov = "FVIF" Then
            lMonto_Operacion_Planilla = lAbono
            lTipo_Movimiento = gcTipoOperacion_Egreso
          End If
          
          Rem Fecha de la Operacion
          lFecha_Operacion = lFecha

          Rem Setea la ultima fila procesada, para empezar desde esta fila el ciclo de afuera
          pFila = lFila_Detalle

          lSalir = True
        End If
        lFila_Detalle = lFila_Detalle + 1
      Wend

      lId_Caja_Cuenta = ""
      Set lcCaja_Cuenta = New Class_Cajas_Cuenta
      With lcCaja_Cuenta
        .Campo("id_cuenta").Valor = pId_Cuenta
        .Campo("id_moneda").Valor = lId_Moneda
        .Campo("cod_mercado").Valor = fc_Mercado
        If .Buscar_Caja_Para_Invertir(lId_Nemotecnico) Then
          If .Cursor.Count > 0 Then
            lId_Caja_Cuenta = NVL(.Cursor(1)("id_caja_cuenta").Value, "")
          End If
        Else
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lcCaja_Cuenta.ErrMsg)
          GoTo ErrProcedure
        End If
      End With
      Set lcCaja_Cuenta = Nothing

      If lId_Caja_Cuenta = "" Then
        Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": No existe una caja para la cuenta.")
        GoTo ErrProcedure
      End If

      For Each lReg In lRecord_Detalle
        Call lcClase.Agregar_Operaciones_Detalle(pId_Nemotecnico:=lReg("Id_Nemotecnico").Value, _
                                                 pCantidad:=lReg("Cantidad").Value, _
                                                 pTasa:=lReg("Precio").Value, _
                                                 PTasa_Gestion:=lReg("Precio").Value, _
                                                 pPlazo:="", _
                                                 pBase:="", _
                                                 pFecha_Vencimiento:=lReg("Fecha_Vencimiento").Value, _
                                                 pId_Moneda_Pago:=lId_Moneda, _
                                                 pMonto_Pago:=lReg("Monto_detalle").Value, _
                                                 pReferenciado:="", _
                                                 pTipo_Deposito:="", _
                                                 pFecha_Valuta:="", _
                                                 pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                 pTasa_Historico:="", _
                                                 pId_Mov_Activo_Compra:="")
        lMonto_Neto = lMonto_Neto + lReg("monto_detalle").Value
      Next

      If lTipo_Operacion = gcOPERACION_Directa Then
      
        lComision = Int((lMonto_Neto * pPorcentaje_Comision) / 100)
        lDerechos = Int((lMonto_Neto * pPorcentaje_Derechos) / 100)
        lIva = Int((Int(lComision) + Int(pGastos) + Int(lDerechos)) * pValor_Iva)
  
        If lTipo_Movimiento = gcTipoOperacion_Ingreso Then
          lMonto_Operacion = lMonto_Neto + lComision + lIva + pGastos + lDerechos
        ElseIf lTipo_Movimiento = gcTipoOperacion_Egreso Then
          lMonto_Operacion = lMonto_Neto - (lComision + lIva + pGastos + lDerechos)
        End If
        
        Rem Verifica si se cobraron Gastos
        '-------------------------------------------
        If lMonto_Neto = lMonto_Operacion_Planilla Then
          Rem Si el Monto Neto es igual al Monto Total que aparece en la planilla,
          Rem no se cobraron comisiones por la operacion
          lComision = 0
          lDerechos = 0
          lIva = 0
        ElseIf Not lMonto_Operacion = lMonto_Operacion_Planilla Then
          pGastos = Int(Abs(lMonto_Operacion - lMonto_Operacion_Planilla) / (1 + pValor_Iva))
          lIva = Int((Int(lComision) + Int(pGastos) + Int(lDerechos)) * pValor_Iva)
        End If
        
        If Not lcClase.Realiza_Operacion_Directa(pId_Operacion:=cNewEntidad, _
                                                 pId_Cuenta:=pId_Cuenta, _
                                                 pDsc_Operacion:="", _
                                                 pTipoOperacion:=lTipo_Movimiento, _
                                                 pId_Contraparte:="", _
                                                 pId_Representante:="", _
                                                 pId_Moneda_Operacion:=lId_Moneda, _
                                                 pFecha_Operacion:=lFecha_Operacion, _
                                                 pFecha_Liquidacion:=lFecha_Operacion, _
                                                 pId_Trader:="", _
                                                 pPorc_Comision:=pPorcentaje_Comision / 100, _
                                                 pComision:=lComision, _
                                                 pDerechos_Bolsa:=lDerechos, _
                                                 pGastos:=pGastos, _
                                                 pIva:=lIva, _
                                                 pMonto_Operacion:=lMonto_Operacion_Planilla, _
                                                 pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lcClase.ErrMsg)
          GoTo ErrProcedure
        End If
      ElseIf lTipo_Operacion = gcOPERACION_Custodia Then
        If Not lcClase.Realiza_Operacion_Custodia(pId_Operacion:=cNewEntidad, _
                                                  pId_Cuenta:=pId_Cuenta, _
                                                  pDsc_Operacion:="", _
                                                  pTipoOperacion:=lTipo_Movimiento, _
                                                  pId_Contraparte:="", _
                                                  pId_Representante:="", _
                                                  pId_Moneda_Operacion:=lId_Moneda, _
                                                  pFecha_Operacion:=lFecha_Operacion, _
                                                  pFecha_Liquidacion:=lFecha_Operacion, _
                                                  pId_Trader:="", _
                                                  pPorc_Comision:=0, _
                                                  pComision:=0, _
                                                  pDerechos_Bolsa:=0, _
                                                  pGastos:=0, _
                                                  pIva:=0, _
                                                  pMonto_Operacion:=lMonto_Neto, _
                                                  pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lcClase.ErrMsg)
          GoTo ErrProcedure
        End If
      End If
      
  End Select
  
  Set lcClase = Nothing
  Exit Function
  
ErrProcedure:
  Fnt_Grabar_Operacion = False
  Set lcClase = Nothing
  
End Function

Private Function Fnt_Busca_Nemotecnico(pRecord_Detalle As hRecord, _
                                       pId_Nemotecnico, _
                                       pCantidad, _
                                       pMonto_Detalle) As Boolean
Dim lReg As hFields
   
  Fnt_Busca_Nemotecnico = False
  
  For Each lReg In pRecord_Detalle
    If pId_Nemotecnico = lReg("ID_NEMOTECNICO").Value Then
      lReg("CANTIDAD").Value = lReg("CANTIDAD").Value + pCantidad
      lReg("MONTO_DETALLE").Value = lReg("MONTO_DETALLE").Value + pMonto_Detalle
      Fnt_Busca_Nemotecnico = True
      Exit For
    End If
  Next
  
End Function

Private Sub Sub_Migrar_Fondos()
Dim lcAlias As Object
'-----------------------------------
Dim lExcel As New Excel.Application
Dim lLibro As Excel.Workbook
Dim lHoja As Excel.Worksheet
'-----------------------------------
Dim lFila As Double
Dim lRut_Cliente As String
Dim lId_Cuenta As String
Dim lNombre_Cliente As String
Dim lCod_Fondo As String
Dim lNombre_Fondo As String
Dim lFecha_Movimiento As Date
Dim lFecha_Liquidacion As Date
Dim lFecha_Operativa As Date
Dim lTipo_Operacion As String
Dim lNro_Operacion As String
Dim lMonto As Double
Dim lValor_Cuota As Double
Dim lNro_Cuotas As Double
Dim lSaldo_Cuotas As Double
Dim lTipo_Mov As String
Dim lId_Moneda As String
'-----------------------------------
Dim lcNemotecnicos As Class_Nemotecnicos
Dim lId_Nemotecnico As String
Dim lCod_Instrumento As String

On Error GoTo ErrProcedure
  
  Grilla.Rows = 1
  
  If Txt_ArchivoPlano.Text = "" Then
    MsgBox "Debe ingresar la ruta de la planilla Excel.", vbCritical, Me.Caption
    Exit Sub
  End If
  
  lExcel.Visible = False
  Rem Abre el libro excel
  Set lLibro = lExcel.Workbooks.Open(Txt_ArchivoPlano.Text, ReadOnly:=False, Editable:=True)
  Rem abre la hoja del libro a procesar
  Set lHoja = lLibro.Worksheets(1)

  With lHoja
    Rem Setea la linea de inicio de recorrido del excel
    lFila = 2
    Call Fnt_Escribe_Grilla(Grilla, "N", "Inicio Proceso Migraci�n FFMM: " & Now & ".")
    Do While Not .Cells(lFila, eExcel_FFMM.eGD_rut_Cliente) = ""
            
      lRut_Cliente = Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_rut_Cliente), ""))
      lCod_Fondo = Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Codigo_Fondo), ""))
      
      Set lcAlias = Fnt_CreateObject(cDLL_Alias)
      Set lcAlias.gDB = gDB
      Rem Busca el alias del Rut del Cliente
      lId_Cuenta = NVL(lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                               , pCodigoCSBPI:=cTabla_Cuentas _
                                               , pValor:=lRut_Cliente), "")
      Rem Busca el alias del C�digo del Fondo Mutuo
      lId_Nemotecnico = NVL(lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                                    , pCodigoCSBPI:=cTabla_Nemotecnicos _
                                                    , pValor:=lCod_Fondo), "")
      Set lcAlias = Nothing
      
      Rem Si el Id_Cuenta o Id_Nemotecnico es vacio no hace la operacion
      If (Not lId_Cuenta = "") And (Not lId_Nemotecnico = "") Then
        
        Rem Rescata los otros datos de la planilla
        lNombre_Cliente = Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Nombre_Cliente), ""))
        lNombre_Fondo = Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Nombre_Fondo), ""))
        lFecha_Movimiento = Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Fecha_Movimiento), ""))
        lTipo_Operacion = Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Tipo_Operacion), ""))
        lNro_Operacion = Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Nro_Operacion), ""))
        lMonto = To_Number(Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Monto), 0)))
        lValor_Cuota = To_Number(Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Valor_Cuota), 0)))
        lNro_Cuotas = To_Number(Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Nro_Cuotas), 0)))
        lSaldo_Cuotas = To_Number(Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Saldo_Cuotas), 0)))
        
        lFecha_Operativa = Fnt_Busca_Fecha_Operativa(lRut_Cliente)
        
        If lFecha_Movimiento >= lFecha_Operativa Then
        
          Rem Busca atributos del Nemotecnico
          Set lcNemotecnicos = New Class_Nemotecnicos
          lcNemotecnicos.Campo("id_nemotecnico").Valor = lId_Nemotecnico
          If lcNemotecnicos.Buscar Then
            lCod_Instrumento = NVL(lcNemotecnicos.Cursor(1)("cod_instrumento").Value, "")
            
            If lTipo_Operacion = "APORTE" Then
              lTipo_Mov = gcTipoOperacion_Ingreso
              lFecha_Liquidacion = lFecha_Movimiento
            ElseIf lTipo_Operacion = "RESCATE" Then
              lTipo_Mov = gcTipoOperacion_Egreso
              lFecha_Liquidacion = Fnt_Calcula_Dia_Habil(lFecha_Movimiento, fcDias_Liquid_FFMM)
            End If
            
            Rem La Moneda es Peso Nacional = 1
            lId_Moneda = 1
            
            Rem Por cada linea de la planilla Excel agrega una operaci�n cuando corresponda
            If Fnt_Grabar_Operacion_FFMM(pFila:=lFila, _
                                         pId_Cuenta:=lId_Cuenta, _
                                         pCod_Instrumento:=lCod_Instrumento, _
                                         pId_Nemotecnico:=lId_Nemotecnico, _
                                         pCantidad:=lNro_Cuotas, _
                                         pPrecio:=lValor_Cuota, _
                                         pId_Moneda:=lId_Moneda, _
                                         pMonto:=lMonto, _
                                         pTipo_Movimiento:=lTipo_Mov, _
                                         pFecha_Operacion:=lFecha_Movimiento, _
                                         pFecha_Liquidacion:=lFecha_Liquidacion) Then
  
              Call Fnt_Escribe_Grilla(Grilla, "N", "Fila " & lFila & ": Ingreso correcto de la Operaci�n.")
            End If
            
          Else
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & lcNemotecnicos.ErrMsg)
          End If
          Set lcNemotecnicos = Nothing
'        Else
'          Call Fnt_Escribe_Grilla(Grilla, "A", "Fila " & lFila & ": Fecha de Movimiento anterior a la Fecha Operativa de la Cuenta.")
        End If
      Else
        If lId_Cuenta = "" Then
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": El Rut " & lRut_Cliente & " no existe en el sistema.")
        ElseIf lId_Nemotecnico = "" Then
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": El Alias del C�digo " & lCod_Fondo & " no est� asociado a ningun Nemot�cnico.")
        End If
      End If

      lFila = lFila + 1
    Loop
    Call Fnt_Escribe_Grilla(Grilla, "N", "T�rmino Proceso Migraci�n FFMM: " & Now & ".")
    
  End With
  
  MsgBox "Terminado el proceso de carga de la Planilla Excel", vbInformation, Me.Caption
  
ErrProcedure:
  If Not Err.Number = 0 Then
    If MsgBox("Error en la carga de planilla Excel." & vbCr & vbCr & Err.Description, vbApplicationModal + vbCritical + vbOKCancel, Me.Caption) = vbRetry Then
      Resume
    End If
  End If
  
  lExcel.DisplayAlerts = False
  lExcel.Quit
End Sub

Private Function Fnt_Grabar_Operacion_FFMM(pFila, _
                                           pId_Cuenta, _
                                           pCod_Instrumento, _
                                           pId_Nemotecnico, _
                                           pCantidad, _
                                           pPrecio, _
                                           pId_Moneda, _
                                           pMonto, _
                                           pTipo_Movimiento, _
                                           pFecha_Operacion, _
                                           pFecha_Liquidacion) As Boolean
Dim lcClase As Object
Dim lId_Caja_Cuenta As String
Dim lFila_Detalle As Long
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
  
  Fnt_Grabar_Operacion_FFMM = True
  
  lId_Caja_Cuenta = ""
  
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("id_moneda").Valor = pId_Moneda
    .Campo("cod_mercado").Valor = fc_Mercado
    If .Buscar_Caja_Para_Invertir(pId_Nemotecnico) Then
      If .Cursor.Count > 0 Then
        lId_Caja_Cuenta = NVL(.Cursor(1)("id_caja_cuenta").Value, "")
      End If
    Else
      Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lcCaja_Cuenta.ErrMsg)
      GoTo ErrProcedure
    End If
  End With
  Set lcCaja_Cuenta = Nothing
  
  If lId_Caja_Cuenta = "" Then
    Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": No existe una caja para la cuenta.")
    GoTo ErrProcedure
  End If
  
  Select Case pCod_Instrumento
Rem ------------ FONDOS MUTUOS --------------------------------------------------------------------------------
    Case gcINST_FFMM_RF_NAC, gcINST_FFMM_RV_NAC
      Set lcClase = New Class_FondosMutuos
      lcClase.fMigracion = True
      
      Call lcClase.Agregar_Operaciones_Detalle(pId_Nemotecnico:=pId_Nemotecnico, _
                                               pcuota:=pCantidad, _
                                               pPrecio:=pPrecio, _
                                               pId_Moneda:=pId_Moneda, _
                                               pMonto:=pMonto, _
                                               pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                               pFecha_Liquidacion:=pFecha_Liquidacion, _
                                               pPrecio_Historico:="")
                                                 
      If Not lcClase.Realiza_Operacion_Directa(pId_Operacion:=cNewEntidad, _
                                               pId_Cuenta:=pId_Cuenta, _
                                               pDsc_Operacion:="", _
                                               pTipoOperacion:=pTipo_Movimiento, _
                                               pId_Contraparte:="", _
                                               pId_Representante:="", _
                                               pId_Moneda_Operacion:=pId_Moneda, _
                                               pFecha_Operacion:=pFecha_Operacion, _
                                               pFecha_Vigencia:=pFecha_Operacion, _
                                               pFecha_Liquidacion:=pFecha_Liquidacion, _
                                               pId_Trader:="", _
                                               pPorc_Comision:=0, _
                                               pComision:=0, _
                                               pDerechos_Bolsa:=0, _
                                               pGastos:=0, _
                                               pIva:=0, _
                                               pMonto_Operacion:=pMonto, _
                                               pTipo_Precio:=cTipo_Precio_Mercado, _
                                               pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                               pCod_Instrumento:=pCod_Instrumento) Then
        
        Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & pFila & ": " & lcClase.ErrMsg)
        GoTo ErrProcedure
      End If
      
  End Select
  
  Exit Function
  
ErrProcedure:
  Fnt_Grabar_Operacion_FFMM = False
End Function

Private Sub Sub_Migrar_Aportes_Rescates()
Dim lExcel As New Excel.Application
Dim lLibro As Excel.Workbook
Dim lHoja As Excel.Worksheet
'------------------------------
Dim lFila As Double
Dim lCliente As String
Dim lCaja_Cuenta As String
Dim lDsc_Apo_Res As String
Dim lTipo_Mov As String
Dim lFecha_Mov As Date
Dim lFecha_Operativa As Date
Dim lMonto As Double
'------------------------------
Dim lId_Cuenta As String
Dim lTipo_Movimiento As String
Dim lId_Moneda As String
Dim lId_Caja_Cuenta As String
'------------------------------
Dim lcApo_Res As Class_APORTE_RESCATE_CUENTA
Dim lcAlias As Object
Dim lcCaja_Cuenta As Class_Cajas_Cuenta

On Error GoTo ErrProcedure
  
  Grilla.Rows = 1
  Rem Moneda Nacional en Pesos
  lId_Moneda = 1
  
  If Txt_ArchivoPlano.Text = "" Then
    MsgBox "Debe ingresar la ruta de la planilla Excel.", vbCritical, Me.Caption
    Exit Sub
  End If
  
  'lFecha_Servidor = Fnt_FechaServidor
  
  lExcel.Visible = False
  Rem Abre el libro excel
  Set lLibro = lExcel.Workbooks.Open(Txt_ArchivoPlano.Text, ReadOnly:=False, Editable:=True)
  Rem abre la hoja del libro a procesar
  Set lHoja = lLibro.Worksheets(1)

  With lHoja
    Rem Setea la linea de inicio de recorrido del excel
    lFila = 2
    
    Call Fnt_Escribe_Grilla(Grilla, "N", "Inicio Proceso Migraci�n Aporte/Rescate: " & Now & ".")
    
    Do While Not .Cells(lFila, eExcel_FFMM.eGD_rut_Cliente) = ""
            
      lCliente = Trim(NVL(.Cells(lFila, eExcel_Apo_Res.eGD_Cliente), ""))
      lTipo_Mov = Trim(NVL(.Cells(lFila, eExcel_Apo_Res.eGD_Tipo_Mov), ""))
      
      Set lcAlias = Fnt_CreateObject(cDLL_Alias)
      Set lcAlias.gDB = gDB
      lId_Cuenta = NVL(lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                               , pCodigoCSBPI:=cTabla_Cuentas _
                                               , pValor:=lCliente), "")
      Set lcAlias = Nothing
      
      lTipo_Movimiento = ""
      If lTipo_Mov = "APO" Then
        lTipo_Movimiento = gcTipoOperacion_Aporte
      ElseIf lTipo_Mov = "RES" Then
        lTipo_Movimiento = gcTipoOperacion_Rescate
      End If
      
      Rem Si el Id_Cuenta o Id_Nemotecnico es vacio no hace la operacion
      If (Not lId_Cuenta = "") And (Not lTipo_Movimiento = "") Then
      
        lCaja_Cuenta = Trim(NVL(.Cells(lFila, eExcel_Apo_Res.eGD_Caja_Cuenta), ""))
        lDsc_Apo_Res = Trim(NVL(.Cells(lFila, eExcel_Apo_Res.eGD_Des_Apo_Res), ""))
        
        lFecha_Mov = Trim(NVL(.Cells(lFila, eExcel_Apo_Res.eGD_Fecha_Mov), ""))
        lMonto = Trim(NVL(.Cells(lFila, eExcel_Apo_Res.eGD_Monto), ""))
        
        lFecha_Operativa = Fnt_Busca_Fecha_Operativa(lCliente)
          
        If lFecha_Mov >= lFecha_Operativa Then
          
          lId_Caja_Cuenta = ""
          
          Set lcCaja_Cuenta = New Class_Cajas_Cuenta
          With lcCaja_Cuenta
            .Campo("id_cuenta").Valor = lId_Cuenta
            .Campo("id_moneda").Valor = lId_Moneda
            .Campo("cod_mercado").Valor = fc_Mercado
            If .Buscar_Caja_Para_Invertir() Then
              If .Cursor.Count > 0 Then
                lId_Caja_Cuenta = NVL(.Cursor(1)("id_caja_cuenta").Value, "")
              End If
            Else
              Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & lcCaja_Cuenta.ErrMsg)
              GoTo ErrProcedure
            End If
          End With
          Set lcCaja_Cuenta = Nothing
          
          If Not lId_Caja_Cuenta = "" Then
          
            Set lcApo_Res = New Class_APORTE_RESCATE_CUENTA
            With lcApo_Res
              .Campo("id_Apo_Res_Cuenta").Valor = cNewEntidad
              .Campo("cod_Medio_Pago").Valor = "CASH"
              '.Campo("id_Banco").Valor = ""
              '.Campo("id_Banco").Valor = Null
              .Campo("id_Caja_Cuenta").Valor = lId_Caja_Cuenta
              .Campo("id_Cuenta").Valor = lId_Cuenta
              .Campo("dsc_Apo_Res_Cuenta").Valor = lDsc_Apo_Res
              .Campo("flg_Tipo_Movimiento").Valor = lTipo_Movimiento
              .Campo("fecha_Movimiento").Valor = lFecha_Mov
              '.Campo("num_Documento").Valor = ""
              .Campo("retencion").Valor = 0
              .Campo("monto").Valor = lMonto
              '.Campo("cta_Cte_Bancaria").Valor = ""
              If .Guardar Then
                Call Fnt_Escribe_Grilla(Grilla, "N", "Fila " & lFila & ": Ingreso correcto de la Operaci�n.")
              Else
                Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & lcApo_Res.ErrMsg)
              End If
            End With
            Set lcApo_Res = Nothing
            
          Else
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": No existe una caja para la cuenta.")
          End If
          
  '      Else
  '        Call Fnt_Escribe_Grilla(Grilla, "A", "Fila " & lFila & ": Fecha de Movimiento anterior a la Fecha Operativa de la Cuenta.")
        End If
      Else
        If lId_Cuenta = "" Then
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": No existe un alias para la cuenta '" & lCliente & "' en el sistema GPI.")
        ElseIf lTipo_Movimiento = "" Then
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": El Tipo Movimiento '" & lTipo_Mov & "' no es v�lido.")
        End If
      End If
      
      lFila = lFila + 1
    Loop
    
    Call Fnt_Escribe_Grilla(Grilla, "N", "T�rmino Proceso Migraci�n Aporte/Rescate: " & Now & ".")
    
  End With
  
  MsgBox "Terminado el proceso de carga de la Planilla Excel", vbInformation, Me.Caption
  
ErrProcedure:
  If Not Err.Number = 0 Then
    If MsgBox("Error en la carga de planilla Excel." & vbCr & vbCr & Err.Description, vbApplicationModal + vbCritical + vbOKCancel, Me.Caption) = vbRetry Then
      Resume
    End If
  End If
  
  lExcel.DisplayAlerts = False
  lExcel.Quit
End Sub

Private Sub Toolbar_Detalle_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "DETALLE"
      Call Grilla_DblClick
  End Select
End Sub

Private Function Fnt_Busca_Fecha_Operativa(pRut_Cliente As String) As Date
  
  With gDB
    .Parametros.Clear
    .Parametros.Add "pRut_Cliente", ePT_Caracter, pRut_Cliente, ePD_Entrada
    .Parametros.Add "PFECHA_OPERATIVA", ePT_Fecha, "", ePD_Salida
    .Procedimiento = "PKG_CLIENTES_FECHA_OP.BUSCAR"
    If .EjecutaSP Then
      Fnt_Busca_Fecha_Operativa = NVL(.Parametros("PFECHA_OPERATIVA").Valor, DateSerial(1994, 1, 1))
    Else
      Fnt_Busca_Fecha_Operativa = DateSerial(1994, 1, 1)
    End If
    .Parametros.Clear
  End With
  
End Function
