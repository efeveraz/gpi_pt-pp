VERSION 5.00
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Checklist_Cliente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Checklist Cliente"
   ClientHeight    =   1725
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6855
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1725
   ScaleWidth      =   6855
   Begin VB.Frame Frame1 
      Height          =   1200
      Left            =   60
      TabIndex        =   2
      Top             =   450
      Width           =   6705
      Begin VB.CheckBox Chk_ConObservacion 
         Caption         =   "Permite Observaci�n"
         Height          =   255
         Left            =   150
         TabIndex        =   4
         Top             =   900
         Width           =   3615
      End
      Begin hControl2.hTextLabel Txt_Descripcion 
         Height          =   315
         Left            =   210
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   390
         Width           =   6285
         _ExtentX        =   11086
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   500
         Caption         =   "Descripci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Checklist_Cliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pId_Checklist, pCod_Arbol_Sistema)
  fKey = pId_Checklist
  
'-----------------------------------------------------------------------
'--Setea los controles dependiendo de los permisos
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
'-----------------------------------------------------------------------
  
  Call Sub_CargarDatos
    
  If Txt_Descripcion.Text = "" Then
    Me.Caption = "Ingreso Checklist Cliente"
  Else
    Me.Caption = "Modificaci�n Checklist Cliente: " & Txt_Descripcion.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcChecklist As Object 'Class_Checklist_Cliente

  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If

  Set lcChecklist = Fnt_CreateObject(cDLL_Checklist_Cliente) 'New Class_Checklist_Cliente
  With lcChecklist
    Set .gDB = gDB
    .Campo("id_checklist").Valor = fKey
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    .Campo("dsc_checklist").Valor = Txt_Descripcion.Text
    .Campo("tipo_checklist").Valor = 1
    .Campo("orden").Valor = Fnt_ObtieneValorOrden
    .Campo("con_observacion").Valor = IIf(Chk_ConObservacion.Value = vbChecked, "SI", "NO")
    If Not .Guardar Then
      MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      Fnt_Grabar = False
    End If
  End With
  Set lcChecklist = Nothing
  
End Function

Private Sub Sub_CargaForm()
  Txt_Descripcion.Text = ""
End Sub

Private Sub Sub_CargarDatos()
Dim lcChecklist As Object 'Class_Checklist_Cliente
Dim lConObs As String
  
  Load Me
  
  Set lcChecklist = Fnt_CreateObject(cDLL_Checklist_Cliente) 'New Class_Checklist_Cliente
  With lcChecklist
    Set .gDB = gDB
    .Campo("Id_Checklist").Valor = fKey
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar Then
      If .Cursor.Count > 0 Then
        Txt_Descripcion.Text = NVL(.Cursor(1)("dsc_checklist").Value, "")
        lConObs = NVL(.Cursor(1)("con_observacion").Value, "NO")
        Chk_ConObservacion.Value = IIf(lConObs = "SI", vbChecked, vbUnchecked)
      End If
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcChecklist = Nothing
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function

Private Function Fnt_ObtieneValorOrden() As Integer
Dim lOrden As Integer
Dim lCursor
Dim lReg

        lOrden = 0
        gDB.Parametros.Clear
        gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
        gDB.Parametros.Add "pOrden", ePT_Numero, "", ePD_Salida
        gDB.Procedimiento = "PKG_CHECKLIST_CLIENTES$TraeOrdenMaximo"

        If gDB.EjecutaSP Then
            lOrden = NVL(gDB.Parametros("porden").Valor, 0)
            lOrden = lOrden + 1
        Else
            GoTo ErrProcedure
        End If

        Call gDB.Parametros.Clear
ErrProcedure:
        Fnt_ObtieneValorOrden = lOrden
End Function
