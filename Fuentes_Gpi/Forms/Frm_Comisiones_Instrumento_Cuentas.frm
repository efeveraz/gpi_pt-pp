VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Comisiones_Instrumento_Cuentas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Comisiones Transaccionales"
   ClientHeight    =   4095
   ClientLeft      =   45
   ClientTop       =   540
   ClientWidth     =   7650
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4095
   ScaleWidth      =   7650
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   9
      Tag             =   "OBLI"
      Top             =   0
      Width           =   7650
      _ExtentX        =   13494
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   10
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin C1SizerLibCtl.C1Tab SSTab 
      Height          =   3615
      Left            =   45
      TabIndex        =   12
      Top             =   420
      Width           =   7545
      _cx             =   13309
      _cy             =   6376
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   6579300
      ForeColor       =   -2147483634
      FrontTabColor   =   -2147483635
      BackTabColor    =   6579300
      TabOutlineColor =   -2147483632
      FrontTabForeColor=   -2147483634
      Caption         =   "&General|&Historico"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   6
      Position        =   0
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   0   'False
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   -1  'True
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   0
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   -1  'True
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.PictureBox Picture4 
         BorderStyle     =   0  'None
         Height          =   3270
         Left            =   8160
         ScaleHeight     =   3270
         ScaleWidth      =   7515
         TabIndex        =   15
         Top             =   330
         Width           =   7515
         Begin VSFlex8LCtl.VSFlexGrid Grilla 
            Height          =   3045
            Left            =   90
            TabIndex        =   8
            Top             =   90
            Width           =   7335
            _cx             =   12938
            _cy             =   5371
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   7
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Comisiones_Instrumento_Cuentas.frx":0000
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   3270
         Left            =   15
         ScaleHeight     =   3270
         ScaleWidth      =   7515
         TabIndex        =   13
         Top             =   330
         Width           =   7515
         Begin VB.Frame Frame1 
            Height          =   2325
            Left            =   90
            TabIndex        =   17
            Top             =   870
            Width           =   7335
            Begin VB.CheckBox Chk_Iva 
               Caption         =   "Valores Afectos a IVA"
               Height          =   255
               Left            =   180
               TabIndex        =   5
               Top             =   1920
               Width           =   1995
            End
            Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
               Height          =   330
               Left            =   5370
               TabIndex        =   7
               Top             =   1485
               Width           =   1710
               _ExtentX        =   3016
               _ExtentY        =   582
               _Version        =   393216
               CheckBox        =   -1  'True
               DateIsNull      =   -1  'True
               Format          =   56688641
               CurrentDate     =   38898.6446875
            End
            Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
               Height          =   330
               Left            =   5370
               TabIndex        =   6
               Tag             =   "OBLI"
               Top             =   1080
               Width           =   1710
               _ExtentX        =   3016
               _ExtentY        =   582
               _Version        =   393216
               Format          =   56688641
               CurrentDate     =   38810
            End
            Begin hControl2.hTextLabel Txt_Comision 
               Height          =   315
               Left            =   150
               TabIndex        =   2
               Tag             =   "OBLI"
               ToolTipText     =   "Comisi�n de Honorarios"
               Top             =   690
               Width           =   2025
               _ExtentX        =   3572
               _ExtentY        =   556
               LabelWidth      =   1400
               TextMinWidth    =   500
               Caption         =   "Comisi�n"
               Text            =   "0,0000"
               Text            =   "0,0000"
               Format          =   "#,##0.0000"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Gastos 
               Height          =   315
               Left            =   150
               TabIndex        =   3
               Tag             =   "OBLI"
               ToolTipText     =   "Comisi�n de Honorarios"
               Top             =   1080
               Width           =   3315
               _ExtentX        =   5847
               _ExtentY        =   556
               LabelWidth      =   1400
               TextMinWidth    =   500
               Caption         =   "Gastos"
               Text            =   "0"
               Text            =   "0"
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Derechos 
               Height          =   315
               Left            =   150
               TabIndex        =   4
               Tag             =   "OBLI"
               ToolTipText     =   "Comisi�n de Honorarios"
               Top             =   1470
               Width           =   2025
               _ExtentX        =   3572
               _ExtentY        =   556
               LabelWidth      =   1400
               TextMinWidth    =   500
               Caption         =   "Derechos Bolsa"
               Text            =   "0,0000"
               Text            =   "0,0000"
               Format          =   "#,##0.0000"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin TrueDBList80.TDBCombo Cmb_Instrumento 
               Height          =   345
               Left            =   1560
               TabIndex        =   1
               Tag             =   "OBLI=S;CAPTION=Instrumento"
               Top             =   210
               Width           =   5535
               _ExtentX        =   9763
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   0   'False
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Comisiones_Instrumento_Cuentas.frx":0160
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin VB.Label Label4 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha T�rmino"
               Height          =   330
               Left            =   3960
               TabIndex        =   11
               Top             =   1485
               UseMnemonic     =   0   'False
               Width           =   1395
            End
            Begin VB.Label Label1 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha Inicio"
               Height          =   330
               Left            =   3960
               TabIndex        =   14
               Top             =   1080
               Width           =   1395
            End
            Begin VB.Label Label2 
               Caption         =   "%"
               Height          =   195
               Left            =   2280
               TabIndex        =   20
               Top             =   750
               Width           =   225
            End
            Begin VB.Label lbl_tipo_cintraparte 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Instrumento"
               Height          =   345
               Left            =   150
               TabIndex        =   19
               Top             =   210
               Width           =   1400
            End
            Begin VB.Label Label5 
               Caption         =   "%"
               Height          =   195
               Left            =   2280
               TabIndex        =   18
               Top             =   1530
               Width           =   225
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Datos de la Cuenta"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   825
            Left            =   90
            TabIndex        =   16
            Top             =   60
            Width           =   7335
            Begin hControl2.hTextLabel Lbl_Cuenta 
               Height          =   315
               Left            =   150
               TabIndex        =   0
               Top             =   330
               Width           =   6915
               _ExtentX        =   12197
               _ExtentY        =   556
               LabelWidth      =   1275
               Enabled         =   0   'False
               Caption         =   "Cuenta"
               Text            =   ""
               Locked          =   -1  'True
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
            End
         End
      End
   End
End
Attribute VB_Name = "Frm_Comisiones_Instrumento_Cuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Rem --------------------------------------------------------
Rem 05/08/2009 MMardones. Los campos Comisi�n y Derecho
Rem            ahora permitir�n un m�ximo de 6 dec. en BD
Rem            por tanto para el ingreso se habilitan 4 dec.
Rem --------------------------------------------------------

Const fAfecta_IVA = "S"
Const fNo_Afecta_IVA = "N"

Public fKey As String
Public lCuenta As String
Public lInstrumento As String
Public fGrilla As VSFlexGrid

Dim fFlg_Tipo_Permiso As String

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm
  Call Sub_CargarDatos
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
'        MsgBox "Comision se ha grabado existosamente.", vbOKOnly + vbQuestion, Me.Caption
        Unload Me
      End If
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lSexo As String
Dim lComision As Double
Dim lCod_Instrumento As String
Dim lcComisiones_Instrumentos As Class_Comisiones_Instrumentos
Dim lDerechos As Double
Dim lResult As Boolean

  Call Sub_Bloquea_Puntero(Me)
  
  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If
  
  lComision = To_Number(Txt_Comision.Text) / 100
  lDerechos = To_Number(Txt_Derechos.Text) / 100
  lCod_Instrumento = Fnt_ComboSelected_KEY(Cmb_Instrumento)
  
  lResult = True
  If fKey = cNewEntidad Then
    If Fnt_ExisteComision(lCod_Instrumento) Then
      If MsgBox("La comisi�n ya existe para este instrumento. Desea continuar?" _
          , vbYesNo + vbQuestion _
          , Me.Caption) = vbYes Then
          lResult = True
      Else
          lResult = False
      End If
    Else
      lResult = True
    End If
  End If
  
  If lResult Then
        Set lcComisiones_Instrumentos = New Class_Comisiones_Instrumentos
        With lcComisiones_Instrumentos
          
          .Campo("Id_Comision_Instrum_Cuenta").Valor = fKey
          .Campo("Id_Cuenta").Valor = lCuenta
          .Campo("Cod_Instrumento").Valor = lCod_Instrumento
          .Campo("Comision").Valor = lComision
          .Campo("Gastos").Valor = Txt_Gastos.Text
          .Campo("Derechos_Bolsa").Valor = lDerechos
          If Chk_Iva.Value = 0 Then
            .Campo("Flg_Afecta_Iva").Valor = fNo_Afecta_IVA
          Else
            .Campo("Flg_Afecta_Iva").Valor = fAfecta_IVA
          End If
          .Campo("Fecha_Desde").Valor = Format(DTP_Fecha_Desde.Value, cFormatDate)
          .Campo("Fecha_Hasta").Valor = NVL(Format(DTP_Fecha_Hasta.Value, cFormatDate), Null)
          
          If Not .Guardar Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en guardar Comisiones.", _
                              .ErrMsg, _
                              pConLog:=True)
            Fnt_Grabar = False
          End If
        End With
        Set lcComisiones_Instrumentos = Nothing
    End If
  
  Call Sub_Desbloquea_Puntero(Me)
End Function

Private Sub Sub_CargaForm()
Dim lReg As hFields

  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_FormControl_Color(Me.Controls)
  Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento, "")
  
  SSTab.CurrTab = 0

  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Function Fnt_ValidarDatos() As Boolean
Dim lValido As Boolean
Dim lFecha_Ini As String
Dim lFecha_Ter As String
Dim lResultado As Integer
Dim lMensaje As String

  Call Sub_Bloquea_Puntero(Me)

  If Not Fnt_Form_Validar(Me.Controls) Then
    lValido = False
    GoTo ErrProcedure
  End If
  
  If Not IsNull(DTP_Fecha_Hasta.Value) Then
    If DTP_Fecha_Hasta.Value < DTP_Fecha_Desde.Value Then
      MsgBox "Fecha Hasta debe ser posterior a Fecha Desde.", vbOKOnly + vbExclamation, Me.Caption
      lValido = False
      DTP_Fecha_Hasta.SetFocus
      GoTo ErrProcedure
    End If
  End If
  
  If To_Number(Txt_Comision.Text) < 0 Or To_Number(Txt_Comision.Text) > 100 Then
    MsgBox "El Porcentaje de Comisi�n debe estar entre 0 y 100%.", vbOKOnly + vbExclamation, Me.Caption
    lValido = False
    GoTo ErrProcedure
  End If
  
  If To_Number(Txt_Derechos.Text) < 0 Or To_Number(Txt_Derechos.Text) > 100 Then
    MsgBox "El Porcentaje de Derechos Bolsa debe estar entre 0 y 100%.", vbOKOnly + vbExclamation, Me.Caption
    lValido = False
    GoTo ErrProcedure
  End If
  
  lValido = True
  
ErrProcedure:
  Fnt_ValidarDatos = lValido
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Function

Private Sub Sub_CargarHistorial()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lcComisiones_Instrumento As Class_Comisiones_Instrumentos
  
  Grilla.Rows = 1
  
  Set lcComisiones_Instrumento = New Class_Comisiones_Instrumentos
  With lcComisiones_Instrumento
    .Campo("id_cuenta").Valor = lCuenta
    .Campo("cod_instrumento").Valor = Fnt_ComboSelected_KEY(Cmb_Instrumento)
    If .BuscarView_Todas(Fnt_EmpresaActual) Then
      For Each lReg In .Cursor
        lLinea = Grilla.Rows
        Call Grilla.AddItem("")
        
        Call SetCell(Grilla, lLinea, "colum_pk", lReg("id_comision_instrumento_cuenta").Value) 'Aqui solo poner en la columna "colum_pk" el id de la entidad
        Call SetCell(Grilla, lLinea, "Fecha_ini", lReg("Fecha_Desde").Value)
        Call SetCell(Grilla, lLinea, "Fecha_ter", NVL(lReg("Fecha_Hasta").Value, ""))
        Call SetCell(Grilla, lLinea, "Comision", To_Number(NVL(lReg("comision").Value, 0)) * 100)
        Call SetCell(Grilla, lLinea, "Gastos", NVL(lReg("Gastos").Value, 0))
        Call SetCell(Grilla, lLinea, "Derechos", To_Number(NVL(lReg("Derechos_Bolsa").Value, 0) * 100))
        If lReg("Flg_Afecta_IVA").Value = "S" Then
          Call SetCell(Grilla, lLinea, "Iva", True)
        Else
          Call SetCell(Grilla, lLinea, "Iva", False)
        End If
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar el Historial.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcComisiones_Instrumento = Nothing
  
End Sub

Public Function Fnt_Modificar(pColum_PK, pInstrumento, pCuenta, pIdCuenta, pCod_Arbol_Sistema, pGrilla As VSFlexGrid)
  fKey = pColum_PK
  Set fGrilla = pGrilla
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  If fKey = cNewEntidad Then
    Me.Caption = "Agregar Comisiones Transaccionales"
  Else
    Me.Caption = "Modificaci�n de Comisiones Transaccionales : " & pInstrumento
  End If
  
  lCuenta = pIdCuenta
  
  Lbl_Cuenta.Text = pCuenta
  lInstrumento = pInstrumento
  
  DTP_Fecha_Desde.Value = Format(Fnt_FechaServidor, cFormatDate)
  DTP_Fecha_Hasta.Value = Format(Fnt_FechaServidor, cFormatDate)
  DTP_Fecha_Hasta.Value = Null
  
  Call Sub_CargarHistorial
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lcComisiones_Instrumento As Class_Comisiones_Instrumentos
  
  Set lcComisiones_Instrumento = New Class_Comisiones_Instrumentos
  With lcComisiones_Instrumento
    .Campo("Id_Comision_Instrum_Cuenta").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        
        Call Sub_ComboSelectedItem(Cmb_Instrumento, lReg("cod_INSTRUMENTO").Value)
        Txt_Comision.Text = To_Number(NVL(lReg("Comision").Value, "0") * 100)
        Txt_Gastos.Text = NVL(lReg("Gastos").Value, "0")
        Txt_Derechos.Text = To_Number(NVL(lReg("Derechos_Bolsa").Value, "0") * 100)
        
        If lReg("Flg_Afecta_Iva").Value = "N" Then
          Chk_Iva.Value = 0
        Else
          Chk_Iva.Value = 1
        End If
        
        DTP_Fecha_Desde.Value = DLL_COMUN.NVL(lReg("Fecha_Desde").Value, "")
        DTP_Fecha_Hasta.Value = DLL_COMUN.NVL(lReg("Fecha_Hasta").Value, "")
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar datos de Comisiones.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcComisiones_Instrumento = Nothing
End Sub


Public Function Fnt_ExisteComision(pCodInstrumento) As Boolean
Dim fila As Long
Dim lexiste As Boolean

    lexiste = False
    For fila = 1 To fGrilla.Rows - 1
        If GetCell(fGrilla, fila, "id_instrumento") = pCodInstrumento Then
            lexiste = True
            Exit For
        End If
    Next
    Fnt_ExisteComision = lexiste
End Function
