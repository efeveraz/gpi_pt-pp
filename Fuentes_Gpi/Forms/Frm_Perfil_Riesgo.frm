VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Perfil_Riesgo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Perfil Riesgo"
   ClientHeight    =   2010
   ClientLeft      =   6180
   ClientTop       =   1635
   ClientWidth     =   7635
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2010
   ScaleWidth      =   7635
   Begin VB.Frame Frame1 
      Caption         =   "Datos de Perfil de Riesgo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1515
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   7515
      Begin VB.CheckBox CHK_perfil 
         Caption         =   "Perfil por defecto"
         Height          =   255
         Left            =   5850
         TabIndex        =   6
         Top             =   1080
         Width           =   1515
      End
      Begin hControl2.hTextLabel txt_saldo 
         Height          =   315
         Left            =   180
         TabIndex        =   5
         Tag             =   "OBLI"
         Top             =   1050
         Width           =   2925
         _ExtentX        =   5159
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Salto Cuota (%)"
         Text            =   "0"
         Text            =   "0"
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
      End
      Begin hControl2.hTextLabel txt_nom_abreviado 
         Height          =   315
         Left            =   180
         TabIndex        =   4
         Tag             =   "OBLI"
         Top             =   690
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Nom abreviado"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   15
      End
      Begin hControl2.hTextLabel txt_nombre 
         Height          =   315
         Left            =   180
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   330
         Width           =   7155
         _ExtentX        =   12621
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Nombre Perfil"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   49
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7635
      _ExtentX        =   13467
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Perfil_Riesgo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With
End Sub

Public Function Fnt_Modificar(pId_Perfil, pCod_Arbol_Sistema)
  fKey = pId_Perfil
  
'-----------------------------------------------------------------------
'--Setea los controles dependiendo de los permisos
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
'-----------------------------------------------------------------------
  
  Call Sub_CargarDatos
    
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de Perfil de Riesgo"
  Else
    Me.Caption = "Modificaci�n de Perfil de Riesgo: " & txt_nombre.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lreg As hCollection.hFields
Dim lcPerfil_Riesgo As Class_Perfiles_Riesgo
  
  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If

  If CHK_perfil.Value = vbChecked Then
    Set lcPerfil_Riesgo = New Class_Perfiles_Riesgo
    With lcPerfil_Riesgo
        .Campo("id_empresa").Valor = Fnt_EmpresaActual
        If .Buscar Then
          For Each lreg In .Cursor
            If NVL(lreg("FLG_PERFIL_DEFAULT").Value, "") = "S" Then
              If Not lreg("id_perfil_riesgo").Value = fKey Then
                MsgBox "No puede existir m�s de un Perfil por defecto.", vbOKOnly + vbCritical, Me.Caption
                Fnt_Grabar = False
                Exit Function
              End If
            End If
          Next
      Else
        MsgBox "No puede existir m�s de un Perfil por defecto.", vbOKOnly + vbCritical, Me.Caption
        Fnt_Grabar = False
        Exit Function
      End If
    End With
    Set lcPerfil_Riesgo = Nothing
  End If
    
  Set lcPerfil_Riesgo = New Class_Perfiles_Riesgo
  With lcPerfil_Riesgo
    .Campo("Id_Perfil_Riesgo").Valor = fKey
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    .Campo("Dsc_Perfil_Riesgo").Valor = txt_nombre.Text
    .Campo("Abr_Perfil_Riesgo").Valor = txt_nom_abreviado.Text
    .Campo("Porc_Salto_Cuota").Valor = txt_saldo.Text
    .Campo("Flg_Perfil_Default").Valor = IIf(CHK_perfil.Value = vbChecked, "S", "N")
    
    If Not .Guardar Then
      MsgBox "Problemas en grabar en Perfiles." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      Fnt_Grabar = False
    End If
  End With
  Set lcPerfil_Riesgo = Nothing
End Function

Private Sub Sub_CargarDatos()
Dim lreg As hCollection.hFields
Dim lcPerfil_Riesgo As Class_Perfiles_Riesgo

  Load Me
  
  Set lcPerfil_Riesgo = New Class_Perfiles_Riesgo
  With lcPerfil_Riesgo
    .Campo("id_perfil_riesgo").Valor = fKey
    If .Buscar Then
      For Each lreg In .Cursor
        txt_nombre.Text = NVL(lreg("DSC_PERFIL_RIESGO").Value, "")
        txt_nom_abreviado.Text = NVL(lreg("ABR_PERFIL_RIESGO").Value, "")
        txt_saldo.Text = NVL(lreg("PORC_SALTO_CUOTA").Value, "")
        CHK_perfil.Value = IIf(lreg("Flg_Perfil_Default").Value = "S", vbChecked, vbUnchecked)
      Next
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcPerfil_Riesgo = Nothing
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = True

  If Fnt_PreguntaIsNull(txt_nombre) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If

  If Fnt_PreguntaIsNull(txt_nom_abreviado) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If

  If To_Number(txt_saldo.Text) < 0 Or To_Number(txt_saldo.Text) > 100 Then
    MsgBox "Salto Cuota debe ser un valor entre 0 y 100.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  End If

End Function

Private Sub txt_saldo_KeyPress(KeyAscii As Integer)
  If Not KeyAscii = 8 And Not KeyAscii = 46 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub
