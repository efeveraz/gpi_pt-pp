VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Man_Restriccion_Compra 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Restricciones de Compra"
   ClientHeight    =   4155
   ClientLeft      =   165
   ClientTop       =   540
   ClientWidth     =   7485
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4155
   ScaleWidth      =   7485
   Begin VB.Frame Frame1 
      Caption         =   "Restricciones a la Cuenta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   75
      TabIndex        =   0
      Top             =   420
      Width           =   7365
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3960
         Picture         =   "Frm_Man_Restriccion_Compra.frx":0000
         TabIndex        =   4
         Top             =   240
         Width           =   375
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   2805
         Left            =   120
         TabIndex        =   1
         Top             =   750
         Width           =   7125
         _cx             =   12568
         _cy             =   4948
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   5
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Restriccion_Compra.frx":030A
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   3780
         _ExtentX        =   6668
         _ExtentY        =   609
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "N� de Cuenta"
         Text            =   ""
         MaxLength       =   100
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   7485
      _ExtentX        =   13203
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Eliminar"
            Key             =   "DEL"
            Object.ToolTipText     =   "Elimina un elemento"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   6600
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Restriccion_Compra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      '.Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With

  Call Sub_CargaForm
  Call Sub_CargaDatos

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_AfterEdit(ByVal Row As Long, ByVal Col As Long)
  Call SetCell(Grilla, Row, "estado", "U") 'Cambia el estado a actualizado
  
  Call Grilla.AutoSize(Col)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "DEL"
      Call Sub_Eliminar
    Case "ADD"
      Call Sub_Agregar
'    Case "UPDATE"
'      Call Grilla_DblClick
    Case "REFRESH"
      Call Sub_CargaDatos
    Case "EXIT"
      Unload Me
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Sub_Eliminar()
Dim lId_Nemotecnico As String

  If Grilla.Row > 0 Then
    If MsgBox("Esta seguro(a) que desea eliminar el registro?.", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
      lId_Nemotecnico = UCase(Trim(GetCell(Grilla, Grilla.Row, "Id_Nemotecnico")))
      If Fnt_Eliminar(Txt_Num_Cuenta.Tag, _
                      GetCell(Grilla, Grilla.Row, "colum_pk"), _
                      IIf(Not lId_Nemotecnico = "", GetCell(Grilla, Grilla.Row, "Id_Nemotecnico"), 0)) Then
        Call Sub_CargaDatos
      End If
    End If
  End If
End Sub

Private Sub Sub_CargaForm()

  'Limpia la grilla
  Grilla.Rows = 1
  
  'Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas)
End Sub

Private Sub Cmb_Cuentas_ItemChange()
  Call Sub_CargaDatos
End Sub

Private Sub Sub_CargaDatos()
Dim lReg As hFields
Dim lLinea As Long
Dim lID
Dim lcRestriccion_Compra As Class_Restricciones_Compra_Cuenta

  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  Grilla.Rows = 1
  
  'If Not IsNull(Cmb_Cuentas.SelectedItem) Then
  
  If Txt_Num_Cuenta.Tag <> "" Then
    Set lcRestriccion_Compra = New Class_Restricciones_Compra_Cuenta
    With lcRestriccion_Compra
      .Campo("id_Cuenta").Valor = Txt_Num_Cuenta.Tag
      If .Buscar(True) Then
        For Each lReg In .Cursor
          Grilla.AddItem ""
          lLinea = Grilla.Rows - 1
          Call SetCell(Grilla, lLinea, "estado", "")
          Call SetCell(Grilla, lLinea, "colum_pk", lReg("Id_Emisor_Especifico").Value)
          Call SetCell(Grilla, lLinea, "Id_Nemotecnico", NVL(lReg("Id_Nemotecnico").Value, ""))
          Call SetCell(Grilla, lLinea, "dsc_Emisor", lReg("Dsc_Emisor_Especifico").Value)
          Call SetCell(Grilla, lLinea, "dsc_Nemotecnico", NVL(lReg("Nemotecnico").Value, "Todos"))
        Next
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Restricciones de Compra.", _
                        .ErrMsg, _
                        pConLog:=True)
      End If
    End With
    Set lcRestriccion_Compra = Nothing
        
    If Not lID = "" Then
      Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
      If Not Grilla.Row = cNewEntidad Then
        Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
      End If
    End If
  End If

End Sub

Private Sub Sub_Agregar()
  'If IsNull(Cmb_Cuentas.SelectedItem) Then
  If Txt_Num_Cuenta.Tag = "" Then
    MsgBox "Elija una cuenta para agregar una Restricci�n de Compra.", vbExclamation, Me.Caption
  Else
    Call Sub_EsperaVentana(Txt_Num_Cuenta.Tag)
  End If
End Sub

'Private Sub Grilla_DblClick()
'  With Grilla
'    If .Row > 0 Then
'      Call Sub_EsperaVentana(Mid(Cmb_Cuentas.SelectedItem.Key, 2), _
'                             GetCell(Grilla, .Row, "colum_pk"), _
'                             GetCell(Grilla, .Row, "dsc_nemotecnico"))
'    End If
'  End With
'End Sub

Private Sub Sub_EsperaVentana(ByVal pkey) ', Optional ByVal pId_Emisor = "", Optional ByVal pNemotecnico = "")
Dim lForm As Frm_AsignaRestriccionCompra
Dim lNombre As String
  
  Me.Enabled = False
  
  If Not Fnt_ExisteVentanaKey("Frm_AsignaRestriccionCompra", pkey) Then
    lNombre = Me.Name
    
    Set lForm = New Frm_AsignaRestriccionCompra
    Call lForm.Fnt_Modificar(pkey, pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
    
    Do While Fnt_ExisteVentanaKey("Frm_AsignaRestriccionCompra", pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      Call Sub_CargaDatos
    Else
      Exit Sub
    End If
  End If
  
  Me.Enabled = True
  
End Sub

Private Function Fnt_Eliminar(lId_Cuenta As Long, lEmisores As Long, lId_Nemotecnico As Long) As Boolean
Dim lcRestriccion_Compra As Class_Restricciones_Compra_Cuenta
  
  Fnt_Eliminar = True
  If lId_Nemotecnico <> 0 Then
  Set lcRestriccion_Compra = New Class_Restricciones_Compra_Cuenta
  With lcRestriccion_Compra
    .Campo("Id_Cuenta").Valor = lId_Cuenta
    .Campo("Id_Emisor_Especifico").Valor = lEmisores
    If Not lId_Nemotecnico = 0 Then
      .Campo("Id_Nemotecnico").Valor = lId_Nemotecnico
    End If
    If .Borrar Then
      MsgBox "Restricci�n de Compra eliminada correctamente.", vbInformation, Me.Caption
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en eliminar la Restricci�n de Compra.", _
                        .ErrMsg, _
                        pConLog:=True)
      Fnt_Eliminar = False
    End If
  End With
  Set lcRestriccion_Compra = Nothing
  Else
      MsgBox "La Restricci�n de Compra no pudo ser eliminada correctamente.", vbInformation, Me.Caption
      Fnt_Eliminar = False
  End If
  
End Function

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
Dim lCuenta As String
  Set lForm = New Frm_Reporte_Generico
  If Txt_Num_Cuenta.Tag <> "" Then
      lCuenta = Txt_Num_Cuenta.Tag
      Rem Comienzo de la generaci�n del reporte
        With lForm
            Call .Sub_InicarDocumento(pTitulo:="Listado de Restricciones de Compra" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
            With .VsPrinter
                .FontSize = 10
                .Paragraph = "Cuenta: " & lCuenta
                .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
                .Paragraph = "" 'salto de linea
                .FontBold = False
                .FontSize = 9
                Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
                .EndTable
                .EndDoc
            End With
        End With
 End If
ExitProcedure:
  Set lForm = Nothing
End Sub
Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents
  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub
Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call cmb_buscar_Click
    End If
End Sub
Private Sub cmb_buscar_Click()
Dim lId_Cuenta    As String
Dim fId_Cuenta    As String
Dim lcCuenta      As Object
Dim ftextCuenta  As String
    ftextCuenta = Txt_Num_Cuenta.Text & "-"
    If Len(ftextCuenta) > 1 Then
        ftextCuenta = Trim(Left(ftextCuenta, InStr(1, ftextCuenta, "-") - 1))
    Else
        ftextCuenta = ""
    End If
    
    fId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(ftextCuenta), 0)
    
    If fId_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = fId_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                    fId_Cuenta = .Cursor(1)("id_cuenta").Value
                Else
                    MsgBox "No hay cuentas", vbCritical
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
    End If
Set lcCuenta = Nothing
End Sub
