VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Cartola_Clientes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cartola de Clientes"
   ClientHeight    =   1800
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   8190
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1800
   ScaleWidth      =   8190
   Begin VB.Frame Frame1 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1275
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   8055
      Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
         Height          =   345
         Left            =   1365
         TabIndex        =   5
         Top             =   270
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         Format          =   67043329
         CurrentDate     =   38938
      End
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   3900
         TabIndex        =   1
         Top             =   480
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2858
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Reporte"
               Key             =   "REPORTE"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               Object.ToolTipText     =   "Genera el Reporte de Instruccones"
            EndProperty
         EndProperty
      End
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   345
         Left            =   1365
         TabIndex        =   7
         Tag             =   "OBLI=S;CAPTION=Cuenta"
         Top             =   720
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Cartola_Clientes.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Lbl_Cuenta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   " Cuenta"
         Height          =   330
         Left            =   150
         TabIndex        =   6
         Top             =   720
         Width           =   1170
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Cartola"
         Height          =   345
         Index           =   0
         Left            =   150
         TabIndex        =   2
         Top             =   270
         Width           =   1185
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   8190
      _ExtentX        =   14446
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   4
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Cartola_Clientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFecha As Date
Dim lForm As Frm_Reporte_Cartola_BBVA
Dim FilaAux As Variant
Dim iFilaTermino As Variant
Dim lId_Cuenta  As String
Dim lFormatoMonedaCta As String

Dim lNum_Cuenta As String
Dim lDsc_Cuenta As String
Dim lNom_Cliente As String
Dim lDsc_Moneda As String
Dim lId_Moneda_Cta As String
Dim nDecimales As Integer

Const RColorRelleno = 213   ' R
Const GColorRelleno = 202   ' G
Const BColorRelleno = 195   ' B
Const glb_tamletra_titulo = 12
Const glb_tamletra_subtitulo1 = 8
Const glb_tamletra_subtitulo2 = 8
Const glb_tamletra_registros = 8
Const COLUMNA_INICIO As String = "150mm"

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
  End With
  
  With Toolbar_Verificacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORTE").Image = cBoton_Modificar
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre

Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, pTodos:=True)
Call Sub_ComboSelectedItem(Cmb_Cuentas, cCmbKALL)
  
Set lCierre = New Class_Verificaciones_Cierre
fFecha = lCierre.Busca_Ultima_FechaCierre
DTP_Fecha_Ter.Value = fFecha
Call Sub_Limpiar
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Call Sub_Limpiar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REPORTE"
      Call Sub_Generar_Reporte
  End Select
End Sub

Private Sub Sub_Limpiar()
  
'  Call Sub_ComboSelectedItem(Cmb_Estado, cCmbKALL)
  'DTP_Fecha_Ini.Value = fFecha
  'DTP_Fecha_Ter.Value = fFecha
  
End Sub
Private Sub Sub_Generar_Reporte()
Dim glb_cuenta As Long
Dim glb_fecha_hoy As String
Dim glb_fecha_inimes As String

'------------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lReg_Cta As hFields

'Dim lcMonedas As Class_Monedas
Dim lcMoneda As Object
'------------------------------------------

Rem Comienzo de la generaci�n del reporte

lNum_Cuenta = ""
lDsc_Cuenta = ""
lNom_Cliente = ""
lDsc_Moneda = ""
lId_Moneda_Cta = ""
nDecimales = 0

lId_Cuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Cuentas))

Rem Busca los datos de la cuenta

'Set lcCuenta = New Class_Cuentas

Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)

With lcCuenta
  .Campo("id_cuenta").Valor = lId_Cuenta
  If .Buscar Then
    For Each lReg_Cta In .Cursor
      lNum_Cuenta = lReg_Cta("Num_Cuenta").Value
      lDsc_Cuenta = lReg_Cta("Dsc_Cuenta").Value
      lNom_Cliente = lReg_Cta("nombre_cliente").Value
      lId_Moneda_Cta = lReg_Cta("id_moneda").Value
    Next
  Else
    MsgBox .ErrMsg, vbCritical, Me.Caption
    GoTo ErrProcedure
  End If
End With

Rem Busca el nombre de la moneda
'Set lcMonedas = New Class_Monedas
Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
With lcMoneda
  .Campo("id_Moneda").Valor = lId_Moneda_Cta
  lFormatoMonedaCta = Fnt_Formato_Moneda(lId_Moneda_Cta)
  If .Buscar Then
    lDsc_Moneda = .Cursor(1)("dsc_moneda").Value
    nDecimales = .Cursor(1)("dicimales_mostrar").Value
  Else
    MsgBox .ErrMsg, vbCritical, Me.Caption
    Set lcMoneda = Nothing
    GoTo ErrProcedure
  End If
End With
Set lcMoneda = Nothing

GeneraCartola lId_Cuenta ', lNum_Cuenta, lDsc_Cuenta, lDsc_Moneda, nDecimales
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Function DameInicioDeMes(sFecha As String) As String
    Dim sFechaTmp As String
    Dim sMes As String
    
    Dim sFormatoFecha As String
    
    'sFormatoFecha = Obtener_Simbolo(LOCALE_SSHORTDATE)
    
    If Month(sFecha) < 10 Then
        sMes = "0" & Month(sFecha)
    Else
        sMes = Month(sFecha)
    End If
    
    sFechaTmp = "01/" & sMes & "/" & Year(sFecha)
    
    DameInicioDeMes = sFechaTmp
    
End Function
Private Function Obtener_Simbolo(Valor As Long) As String
   Dim Simbolo  As String
   Dim r1       As Long
   Dim r2       As Long
   Dim p        As Integer
   Dim Locale   As Long
   
   'Locale = GetUserDefaultLCID()
   r1 = GetLocaleInfo(Locale, Valor, vbNullString, 0)
   
   'buffer
   Simbolo = String$(r1, 0)
   
   'En esta llamada devuelve el s�mbolo en el Buffer
   r2 = GetLocaleInfo(Locale, Valor, Simbolo, r1)
   
   'Localiza el espacio nulo de la cadena para eliminarla
   p = InStr(Simbolo, Chr$(0))
   
   If p > 0 Then
      'Elimina los nulos
      Obtener_Simbolo = Left$(Simbolo, p - 1)
   End If
   
End Function
Function GeneraCartola(lId_Cuenta As String)
    Dim i As Integer
    Dim hay_cierre As Boolean
    GeneraCartola = True
    hay_cierre = GeneraCartola
    Set lForm = New Frm_Reporte_Cartola_BBVA
    lForm.vp.PaperSize = pprLetter
    lForm.vp.MarginLeft = "15mm"
    lForm.vp.MarginRight = "15mm"
    lForm.vp.MarginTop = "45mm"
    lForm.vp.MarginBottom = "25mm"
    lForm.vp.MarginFooter = "15mm"
    lForm.vp.MarginHeader = "0mm"
    Call lForm.trae_datos_cliente(lId_Cuenta, DTP_Fecha_Ter.Value)
    lForm.vp.Font.Name = "Gill Sans MT"
    lForm.vp.Font.Size = glb_tamletra_registros
    lForm.vp.Orientation = orLandscape
    lForm.vp.StartDoc
    Call PaginaUno(hay_cierre)
    If Not hay_cierre Then
        lForm.vp.EndDoc
        lForm.vp.Clear
        Unload lForm
        Set lForm = Nothing
        Exit Function
    End If
    DoEvents
    lForm.vp.NewPage
    Call Sub_Generar_Reporte_2
'Call PaginaDos
'DoEvents
'
'Call PaginaTres
'DoEvents
'
'Call PaginaTresyMedia
'DoEvents
'
'Call PaginaCuatro
'DoEvents
'
'Call PaginaCinco
'DoEvents

'fncDisclaimer
lForm.vp.EndDoc
'********************************************
lForm.vp.Footer = " "
For i = 1 To lForm.vp.PageCount
    lForm.vp.StartOverlay i
    lForm.vp.HdrFontName = "Arial"
    lForm.vp.HdrFontSize = 6
    haz_linea ("205mm")
    lForm.vp.Footer = "|Av. Isidora Goyenechea 3621, Piso 8 - Santiago, Chile - Tel: (56-2) 337 7900 - Fax: (56-2) 337 7999 - e-mail: info@moneda.cl - http://www.moneda.cl" & vbLf & lForm.vp.DocName & "" & vbLf & "Pagina %d /" & CStr(lForm.vp.PageCount)
    lForm.vp.EndOverlay
Next
'**************************************************
'Dim sFileName As String
'sFileName = "Cartola_" & glb_cuenta & "_" & FormatoFechaYYYYMMDD(glb_fecha_hoy) & ".pdf"
'VSPDF81.ConvertDocument vp, oApp.PathPdf & sFileName
Call ColocarEn(lForm, Me, eFP_Abajo)
'lForm.vp.Preview
End Function
Private Sub PaginaUno(ByRef GeneraCartola As Boolean)
    Dim nMarginLeft As Variant
    Dim nColinicio As Variant
    lForm.vp.CurrentY = lForm.vp.CurrentY + rTwips("2mm")
    pon_titulo "CUENTA INVERSI�N", "5mm", "Gill Sans MT", glb_tamletra_titulo
    lForm.vp.CurrentY = lForm.vp.CurrentY + rTwips("2mm")
    FilaAux = lForm.vp.CurrentY
    Call fncCuentaInversion(GeneraCartola)
    If Not GeneraCartola Then Exit Sub
    lForm.vp.CurrentY = iFilaTermino + rTwips("2mm")
    CuadroResumen
    lForm.vp.CurrentY = iFilaTermino + rTwips("1mm")
    FlujoPatrimonial
'    ComposicionCarteraNacional
End Sub
Public Sub pon_titulo(pTitulo As String, pAlto As Variant, pfont_name As String, pfont_size As Double)
    Dim ant_font_name As String
    Dim ant_font_size As Double
    Dim pY As Variant
    
    With lForm.vp
        'lee valores anteriores
        ant_font_name = .FontName
        ant_font_size = .FontSize
        
        'pon valores nuevos
        ' .FontName = "Gill Sans MT"   ' pfont_name
        .FontSize = pfont_size
        
        'pon titulo
        pY = .CurrentY
        haz_linea (pY)
        'lForm.vp.CurrentY = pY + rTwips("0.5mm")
        .CalcParagraph = pTitulo
        .Paragraph = " " & pTitulo
        ' lForm.vp.CurrentY = pY + rTwips(pAlto) + rTwips("2mm")
        'haz_linea (pY + rTwips(pAlto))
        haz_linea (.CurrentY)
        
        'recupera valores anteriores
        .FontName = ant_font_name
        .FontSize = ant_font_size
    End With
    
End Sub
Private Function rTwips(pMM As Variant)
    Dim i As Integer, s As String, d As Double
    If VarType(pMM) <> vbString Then
        rTwips = pMM
    Else
        i = InStr(1, pMM, "mm")
        If i = 0 Then
            MsgBox "Error de conversion de mm a twips"
            End
        End If
        
        s = Mid$(pMM, 1, i - 1)
        d = CDbl(s)
        
        ' rTwips = ((((d * 254) / 1440) + 0.5) * 567) / 100 + 0.5
        rTwips = 56.52 * d
        
    End If
End Function
Public Sub CuadroResumen()
    Dim sName               As String

    Dim xSql                As String
    Dim sCodEmpresa         As String
    Dim sFechaCreacion      As String
    Dim sFechaTope          As String
    Dim nMontoPatrimonio    As Double

    Dim sMonedaFondo        As String
    Dim sTituloMoneda       As String
    Dim Titulo              As String
    Dim fmt                 As String
    Dim cadena              As String
    Dim hdr                 As String
    
    Dim lCursor_Cuotas As hRecord
    Dim lCampo As hFields

    ReDim Campos(0 To 8)
    ReDim glosas(0 To 8)
    ReDim anchos(0 To 8)
    ReDim formatos(0 To 8)

    Dim nro_registros As Long
    sMonedaFondo = "$$"
    sTituloMoneda = "CLP"
    
    lForm.vp.CurrentY = lForm.vp.CurrentY + rTwips("1.5mm")
    pon_titulo "RENTABILIDAD Y FLUJO PATRIMONIAL", "5mm", "Gill Sans MT", glb_tamletra_titulo
    With lForm.vp
        .CurrentY = .CurrentY + rTwips("2mm")
        FilaAux = lForm.vp.CurrentY
        .StartTable
        .TableBorder = tbBox
        cadena = ""
        .TableCell(tcRows) = 5
        .TableCell(tcCols) = 5
        .TableCell(tcFontSize, 1, 1, 2, 5) = glb_tamletra_subtitulo2
        .TableCell(tcBackColor, 1, 1, 2, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcColBorderColor, 0, 1, 3, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcText, 1, 1) = "CUADRO RESUMEN"
        .TableCell(tcColWidth, 1, 1, 7, 1) = "39mm"
        .TableCell(tcAlign, 1, 1) = taLeftMiddle

        .TableCell(tcText, 1, 2) = "RENTABILIDAD"
        .TableCell(tcText, 1, 5) = "VOLAT."

        .TableCell(tcColSpan, 1, 2) = 3
        .TableCell(tcRowBorderColor, 1, , 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)

        .TableCell(tcText, 2, 2) = "CLP"
        .TableCell(tcText, 2, 3) = "UF"
        .TableCell(tcText, 2, 4) = "USD"
        .TableCell(tcText, 2, 5) = sTituloMoneda

        .TableCell(tcAlign, 1, 2, 2, 5) = taCenterMiddle
        .TableCell(tcColWidth, 3, 2, 7, 5) = "15mm"
        .TableCell(tcColNoWrap, 1, 1, 2, 7) = False

        .TableCell(tcAlign, 3, 1, 7, 1) = taLeftMiddle
        .TableCell(tcFontSize) = glb_tamletra_registros
        .TableCell(tcAlign, 3, 2, 7, 5) = taRightMiddle

        .TableCell(tcRowBorderAbove, 3) = 2
        
        gDB.Parametros.Clear
        gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
        gDB.Parametros.Add "pid_cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
        gDB.Parametros.Add "pfecha_cierre", ePT_Fecha, DTP_Fecha_Ter.Value, ePD_Entrada
        gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS$RENTABILIDADES"
        If gDB.EjecutaSP Then
            Set lCursor_Cuotas = gDB.Parametros("pcursor").Valor
            If lCursor_Cuotas.Count > 0 Then
                For Each lCampo In lCursor_Cuotas
                    .TableCell(tcText, 3, 1) = "�ltimos 12 Meses"
                    If lCampo("hay_valor_cuota_ult_12_meses").Value = "SI" Then
                        .TableCell(tcText, 3, 2) = FormatNumber(lCampo("rentabilidad_ult_12_meses_$$").Value, 2, True) & "%"
                        .TableCell(tcText, 3, 3) = FormatNumber(lCampo("rentabilidad_ult_12_meses_UF").Value, 2, True) & "%"
                        .TableCell(tcText, 3, 4) = FormatNumber(lCampo("rentabilidad_ult_12_meses_DO").Value, 2, True) & "%"
                        .TableCell(tcText, 3, 5) = FormatNumber(lCampo("volatilidad").Value, 2, True) & "%"
                    Else
                        .TableCell(tcText, 3, 2) = "?" & "%"
                        .TableCell(tcText, 3, 3) = "?" & "%"
                        .TableCell(tcText, 3, 4) = "?" & "%"
                        .TableCell(tcText, 3, 5) = "?" & "%"
                    End If
                    .TableCell(tcText, 4, 1) = "A�o Calendario"
                    If lCampo("hay_valor_cuota_ano_calendario").Value = "SI" Then
                        .TableCell(tcText, 4, 2) = FormatNumber(lCampo("rentabilidad_anual_$$").Value, 2, True) & "%"
                        .TableCell(tcText, 4, 3) = FormatNumber(lCampo("rentabilidad_anual_UF").Value, 2, True) & "%"
                        .TableCell(tcText, 4, 4) = FormatNumber(lCampo("rentabilidad_anual_DO").Value, 2, True) & "%"
                        .TableCell(tcText, 4, 5) = FormatNumber(lCampo("volatilidad").Value, 2, True) & "%"
                    Else
                        .TableCell(tcText, 4, 2) = "?" & "%"
                        .TableCell(tcText, 4, 3) = "?" & "%"
                        .TableCell(tcText, 4, 4) = "?" & "%"
                        .TableCell(tcText, 4, 5) = "?" & "%"
                    End If
                    .TableCell(tcText, 5, 1) = "Mes"
                    If lCampo("hay_valor_cuota_mes").Value = "SI" Then
                        .TableCell(tcText, 5, 2) = FormatNumber(lCampo("rentabilidad_mensual_$$").Value, 2, True) & "%"
                        .TableCell(tcText, 5, 3) = FormatNumber(lCampo("rentabilidad_mensual_UF").Value, 2, True) & "%"
                        .TableCell(tcText, 5, 4) = FormatNumber(lCampo("rentabilidad_mensual_DO").Value, 2, True) & "%"
                        .TableCell(tcText, 5, 5) = FormatNumber(lCampo("volatilidad").Value, 2, True) & "%"
                    Else
                        .TableCell(tcText, 5, 2) = "?" & "%"
                        .TableCell(tcText, 5, 3) = "?" & "%"
                        .TableCell(tcText, 5, 4) = "?" & "%"
                        .TableCell(tcText, 5, 5) = "?" & "%"
                    End If
                Next
            Else
                .TableCell(tcText, 3, 1) = "�ltimos 12 Meses"
                .TableCell(tcText, 3, 2) = "?" & "%"
                .TableCell(tcText, 3, 3) = "?" & "%"
                .TableCell(tcText, 3, 4) = "?" & "%"
                .TableCell(tcText, 3, 5) = "?" & "%"
                
                .TableCell(tcText, 4, 1) = "A�o Calendario"
                .TableCell(tcText, 4, 2) = "?" & "%"
                .TableCell(tcText, 4, 3) = "?" & "%"
                .TableCell(tcText, 4, 4) = "?" & "%"
                .TableCell(tcText, 4, 5) = "?" & "%"
                
                .TableCell(tcText, 5, 1) = "Mes"
                .TableCell(tcText, 5, 2) = "?" & "%"
                .TableCell(tcText, 5, 3) = "?" & "%"
                .TableCell(tcText, 5, 4) = "?" & "%"
                .TableCell(tcText, 5, 5) = "?" & "%"
            End If
        Else
            .TableCell(tcText, 3, 1) = "�ltimos 12 Meses"
            .TableCell(tcText, 3, 2) = "?" & "%"
            .TableCell(tcText, 3, 3) = "?" & "%"
            .TableCell(tcText, 3, 4) = "?" & "%"
            .TableCell(tcText, 3, 5) = "?" & "%"
            
            .TableCell(tcText, 4, 1) = "A�o Calendario"
            .TableCell(tcText, 4, 2) = "?" & "%"
            .TableCell(tcText, 4, 3) = "?" & "%"
            .TableCell(tcText, 4, 4) = "?" & "%"
            .TableCell(tcText, 4, 5) = "?" & "%"
            
            .TableCell(tcText, 5, 1) = "Mes"
            .TableCell(tcText, 5, 2) = "?" & "%"
            .TableCell(tcText, 5, 3) = "?" & "%"
            .TableCell(tcText, 5, 4) = "?" & "%"
            .TableCell(tcText, 5, 5) = "?" & "%"
        End If
        gDB.Parametros.Clear
        .TableCell(tcColWidth, 1, 5, 7, 5) = "15mm"
        .TableCell(tcFontSize) = glb_tamletra_registros
        .EndTable
    End With

    lForm.vp.CurrentY = lForm.vp.CurrentY + rTwips("1mm")

    Dim nMargenLeft  As Variant
    Dim iFilaActual As Variant

    iFilaActual = lForm.vp.CurrentY

    sName = fncGraficoValorCuota()

    With lForm.vp
        .CurrentY = FilaAux + rTwips("1mm")
        nMargenLeft = .MarginLeft
        .MarginLeft = COLUMNA_INICIO

        .DrawPicture LoadPicture(sName), lForm.vp.MarginLeft, lForm.vp.CurrentY, "90mm", "57mm", , False ' True

        .MarginLeft = nMargenLeft
        .CurrentY = iFilaActual
    End With
    iFilaTermino = iFilaActual
    
End Sub
Sub fncDisclaimer()
    Dim sTxt As String
    lForm.vp.FontSize = 8
    If (lForm.vp.CurrentY + rTwips("30mm")) > (lForm.vp.PageHeight - lForm.vp.MarginBottom - rTwips("10mm")) Then
        lForm.vp.NewPage
    Else
        lForm.vp.CurrentY = (lForm.vp.PageHeight - lForm.vp.MarginBottom - rTwips("30mm"))
    End If
    lForm.vp.BrushColor = vbWhite
    lForm.vp.DrawRectangle lForm.vp.MarginLeft - rTwips("0mm"), lForm.vp.CurrentY, (lForm.vp.PageWidth - lForm.vp.MarginRight) + rTwips("0mm"), lForm.vp.CurrentY + rTwips("30mm")
    sTxt = "AVISO IMPORTANTE"
    lForm.vp.Paragraph = sTxt
    sTxt = "" & vbCrLf
    sTxt = sTxt & "El presente documento ha sido elaborado por Corredora Gesti�n de Inversiones S.A., de conformidad con el Mandato para la Administraci�n de Cartera celebrado entre el Cliente y Moneda Gesti�n de Inversiones S.A. el cual se entiende formar parte del presente instrumento."
    sTxt = sTxt & "Corredora Gesti�n de Inversiones S.A. no asume ninguna responsabilidad de cualquier informaci�n impl�cita o expl�cita que pudiera contener este documento."
    sTxt = sTxt & "La informaci�n contenida en el presente instrumento es confidencial y est� dirigida exclusivamente al destinatario. Cualquier uso, reproducci�n, divulgaci�n o distribuci�n por otras personas se encuentra estrictamente prohibida, acarreando su incumplimiento sanciones penales de conformidad a la ley. "
    sTxt = sTxt & vbCrLf
    lForm.vp.Paragraph = sTxt
End Sub
Private Sub haz_linea(pY As Variant)
    lForm.vp.DrawLine lForm.vp.MarginLeft, pY, lForm.vp.PageWidth - lForm.vp.MarginRight, pY
End Sub
Sub fncCuentaInversion(ByRef GeneraCartola As Boolean)
    Dim sProductoAnterior As String
    Dim modo As String
    Dim Filtro As String
    Dim xSql As String
    Dim nDecimales As Integer
    Dim Mostrado As Integer
    Dim Espacios As String
    Dim suma As Double
    Dim mTitulo As Integer
    Dim iFila As Integer
            
    Dim lcPatrimonio_Cuenta As Class_Patrimonio_Cuentas
    Dim lcSaldos_Activos As Class_Saldo_Activos
    'Dim lcTotal_Activos As Class_Subcuota_Instrumentos
    Dim lCaja_Cuenta As Double
    Dim lTotal_Activos As Double
    Dim lPatrimonio As Double
    Dim lCursor_Activos As hRecord
    Dim lReg As hFields
    Dim Dsc_Instrumento As String
    Dim monto_mon_cta As Double
    Dim Porcentaje As Double
    Dim sNombres()
    Dim sValores()
    Dim i As Integer
    Dim sName As String
    Dim iFilaComienzo As Variant
    Set lcPatrimonio_Cuenta = New Class_Patrimonio_Cuentas
    With lcPatrimonio_Cuenta
        .Campo("id_cuenta").Valor = lId_Cuenta
        .Campo("FECHA_CIERRE").Valor = DTP_Fecha_Ter.Value
        If .Buscar Then
            If .Cursor.Count > 0 Then
                lCaja_Cuenta = (.Cursor(1)("SALDO_CAJA_MON_CUENTA").Value + .Cursor(1)("MONTO_X_COBRAR_MON_CTA").Value) - .Cursor(1)("MONTO_X_PAGAR_MON_CTA").Value
                lPatrimonio = .Cursor(1)("PATRIMONIO_MON_CUENTA").Value
            Else
                MsgBox "No hay Cierres Vigentes", vbInformation, Me.Caption
                GeneraCartola = False
                GoTo ErrProcedure
            End If
            
        End If
    End With
    Set lcPatrimonio_Cuenta = Nothing
    
    Set lcSaldos_Activos = New Class_Saldo_Activos
    With lcSaldos_Activos
        .Campo("id_cuenta").Valor = lId_Cuenta
        .Campo("fecha_cierre").Valor = DTP_Fecha_Ter.Value
        If .Buscar_Activos_Productos Then
            Set lCursor_Activos = .Cursor
        End If
    End With
    Set lcSaldos_Activos = Nothing

    With lForm.vp
        iFilaComienzo = .CurrentY
        .StartTable
        .TableBorder = tbBox
        .TableCell(tcRowHeight, 1, 1, 7, 5) = "2mm"
        .TableCell(tcRows) = 1
        .TableCell(tcCols) = 3
        .TableCell(tcBackColor, 1, 1, 1, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
                
        .TableCell(tcText, 1, 1) = "ACTIVOS POR PRODUCTO"
        .TableCell(tcColWidth, , 1) = "49mm"
        .TableCell(tcText, 1, 2) = "PORCENTAJE"
        .TableCell(tcColWidth, , 2) = "20mm"
        .TableCell(tcColAlign, , 2) = taRightMiddle
        .TableCell(tcText, 1, 3) = "MONTO (" & Monto_Moneda(lDsc_Moneda) & ")"
        .TableCell(tcColWidth, , 3) = "30mm"
        .TableCell(tcColAlign, , 3) = taRightMiddle
        
        .TableCell(tcAlign, 1, 2, 1, 3) = taCenterMiddle
        .TableCell(tcFontSize) = glb_tamletra_registros
        .TableCell(tcRowBorderBelow, 1) = 2
        
        sProductoAnterior = ""
        nDecimales = 0
        
        Mostrado = 0
        Espacios = "  "
        suma = 0
        mTitulo = 1
        iFila = 2
        i = 0
        Dsc_Instrumento = "CAJA"
        monto_mon_cta = lCaja_Cuenta
        If monto_mon_cta <> 0 And lPatrimonio <> 0 Then
            Porcentaje = Round((monto_mon_cta / lPatrimonio) * 100, 2)
        Else
            Porcentaje = 0
        End If
        .TableCell(tcText, iFila, 1) = " "
        '.TableCell(tcFontBold, iFila, 1, iFila, 3) = True
        .TableCell(tcColSpan, iFila, 1) = 3
        .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
        .TableCell(tcRowHeight, iFila, 1, iFila, 3) = "1mm"
        iFila = .TableCell(tcRows) + 1
        .TableCell(tcRows) = iFila
        .TableCell(tcText, iFila, 1) = Dsc_Instrumento
        '.TableCell(tcFontBold, iFila, 1, iFila, 3) = True
        .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
            
        .TableCell(tcText, iFila, 2) = FormatNumber(Porcentaje, 2, True) & "%"
        .TableCell(tcText, iFila, 3) = FormatNumber(monto_mon_cta, 0)
        '.TableCell(tcText, iFila, 3) = FormatNumber(monto_mon_cta, Dame_Decimales(glb_cliente.Moneda_fondo))
            
        iFila = .TableCell(tcRows) + 1
        .TableCell(tcRows) = iFila
        If monto_mon_cta <> 0 Then
            ReDim Preserve sNombres(i)
            ReDim Preserve sValores(i)

            sNombres(i) = Dsc_Instrumento
            sValores(i) = monto_mon_cta
            i = i + 1
        End If
        For Each lReg In lCursor_Activos
            Dsc_Instrumento = lReg("dsc_intrumento").Value
            monto_mon_cta = lReg("monto_mon_cta").Value
            If monto_mon_cta <> 0 And lPatrimonio <> 0 Then
                Porcentaje = Round((monto_mon_cta / lPatrimonio) * 100, 2)
            Else
                Porcentaje = 0
            End If
            .TableCell(tcText, iFila, 1) = " "
            '.TableCell(tcFontBold, iFila, 1, iFila, 3) = True
            .TableCell(tcColSpan, iFila, 1) = 3
            .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
            .TableCell(tcRowHeight, iFila, 1, iFila, 3) = "1mm"
            iFila = .TableCell(tcRows) + 1
            .TableCell(tcRows) = iFila
            .TableCell(tcText, iFila, 1) = Dsc_Instrumento
            '.TableCell(tcFontBold, iFila, 1, iFila, 3) = True
            .TableCell(tcColAlign, iFila, 1) = taLeftMiddle
                
            .TableCell(tcText, iFila, 2) = FormatNumber(Porcentaje, 2, True) & "%"
            .TableCell(tcText, iFila, 3) = FormatNumber(monto_mon_cta, 0)
            '.TableCell(tcText, iFila, 3) = FormatNumber(monto_mon_cta, Dame_Decimales(glb_cliente.Moneda_fondo))
                
            iFila = .TableCell(tcRows) + 1
            .TableCell(tcRows) = iFila
            If monto_mon_cta <> 0 Then
                ReDim Preserve sNombres(i)
                ReDim Preserve sValores(i)

                sNombres(i) = Dsc_Instrumento
                sValores(i) = monto_mon_cta
                i = i + 1
            End If
        Next
        .EndTable
    End With
    Dim nMargenLeft  As Variant
    Dim iFilaActual As Variant
    Dim j As Integer
    Dim aux As Double
    Dim aux1 As String
    
    iFilaActual = lForm.vp.CurrentY
    
'    sName = fncGraficoValorCuota()
    If i > 0 Then
        For i = LBound(sValores) To UBound(sValores)
            For j = i + 1 To UBound(sValores) - 1
                If CDbl(sValores(i)) > CDbl(sValores(j)) Then
                    aux = sValores(j)
                    sValores(j) = sValores(i)
                    sValores(i) = aux
    
                    aux1 = sNombres(j)
                    sNombres(j) = sNombres(i)
                    sNombres(i) = aux1
                End If
            Next
        Next
    
        sName = fncGraficoTorta(sNombres, sValores, "Composici�n Cartera", 11, False)

        With lForm.vp
            .CurrentY = iFila
            nMargenLeft = .MarginLeft
            .MarginLeft = COLUMNA_INICIO
            .DrawPicture LoadPicture(sName), lForm.vp.MarginLeft + rTwips("2mm"), iFilaComienzo, "90mm", "55mm", , False
    
    '        .CurrentY = iFila
    '        nMargenLeft = .MarginLeft
    '        .MarginLeft = COLUMNA_INICIO
    '
    '        .DrawPicture LoadPicture(sGrafRentaFija), vp.MarginLeft + rTwips("2mm"), iFila, "90mm", "55mm", , False
             .MarginLeft = nMargenLeft
    
             .CurrentY = FilaAux
        End With
    End If
    iFilaTermino = iFilaActual
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub
Function Monto_Moneda(ByVal sCodMoneda As String) As String
    Dim s As String
    s = UCase(Trim(sCodMoneda))
    Select Case s
        Case "PESO"
            Monto_Moneda = "$"
        Case "DO"
            Monto_Moneda = "USD"
        Case Else
            Monto_Moneda = s
    End Select
End Function
Function Dame_Decimales(ByVal s As String)
    Select Case s
        Case "PESO"
            Dame_Decimales = 0
        Case Else
            Dame_Decimales = 2
    End Select
End Function

Function fncGraficoTorta(catnom, valores, stitulo, tipo, bLeyenda)
    Dim xColorTitulo As String
    Dim xColor As String
    Dim aColors()
    Dim cd, c
    Dim Path As String
    Dim sFname As String
    
    
    xColorTitulo = ColorToHex(0, 0, 0)
    xColor = ColorToHex(RColorRelleno, GColorRelleno, BColorRelleno)

    aColors = Array(ColorToHex(44, 41, 38), _
                    ColorToHex(18, 17, 15), _
                    ColorToHex(196, 183, 169), _
                    ColorToHex(81, 41, 38), _
                    ColorToHex(248, 245, 241), _
                    ColorToHex(138, 128, 119), _
                    ColorToHex(238, 245, 241), _
                    ColorToHex(189, 184, 179), _
                    ColorToHex(214, 200, 186), _
                    ColorToHex(194, 181, 167), _
                    ColorToHex(196, 183, 169), _
                    ColorToHex(64, 61, 48) _
                    )

    
    Set cd = CreateObject("ChartDirector.API")

    Set c = cd.PieChart(900, 350)

    Call c.setPieSize(450, 140, 250)

    Call c.set3D(40, 75)
    
    'Call c.addLegend(330, 40)
    
    Call c.setLabelFormat("<*size=11*><*block*>{label}<*br*>{percent}%<*/*>")

    Call c.setStartAngle(120, False)

    Call c.SetData(valores, catnom)

    'sTitulo= "<*size=12*><*block*><*color=" & xColorTitulo & "*>" & sTitulo & "<*/*>"

    Call c.addTitle(stitulo, "arial.ttf", 14, xColorTitulo)
    
    Call c.setLabelPos(50, cd.LineColor)

    Call c.setColors2(cd.DataColor, aColors)
    ' Call c.setColors(aColors)

    'Path = App.Path & "\" '    App.Path ' Left(Request.ServerVariables("PATH_INFO"), InStrRev(Request.ServerVariables("PATH_INFO"), "/graficos/"))
    
    'MODIFICADO PARA QUE EL SISTEMA DEJE EN UN DIRECTORIO TEMPORAL EL JPG QUE SE GENERA DEBIDO A QUE ESTABA
    'ENSUCIANDO EL DIRECTORIO DE INSTALACION DEL SISTEMA, LO CUAL, ERA INCORRECTO.�
    'HAF: 20070620.
    Path = Environ$("TEMP") & "\"

    sFname = c.makeTmpFile(Path & sFname, cd.JPG)

    fncGraficoTorta = Path & sFname

End Function
Function ColorToHex(r, g, b)
    ColorToHex = "&H" & Hex(r) & Hex(g) & Hex(b)
End Function
Function fncGraficoValorCuota() As String
    Dim oRs As ADODB.Recordset
    Dim xSql
    Dim sName As String
    Dim stitulo
    Dim fechainicrent
    Dim fechabaserent
    Dim lCursor_Cuotas As hRecord
    Dim lCampo As hFields
    Dim catnom(), Valor()
    Dim catnomc(), valorC()
    Dim catnom3(), valor3()

    Dim sFechaMenor
    Dim sFechaMayor

    Dim iValorMenor
    Dim iValorMayor

    Dim nRentabilidad_diaria
    Dim iBase As Integer
    Dim i As Integer
    
    fechainicrent = "21/10/2006" ' & Year(glb_fecha_hoy)
    fechabaserent = "21/11/2006"
    
    gDB.Parametros.Clear
    gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pid_cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    gDB.Parametros.Add "Pfecha_inicio", ePT_Numero, fechainicrent, ePD_Entrada
    gDB.Parametros.Add "Pfecha_final", ePT_Numero, fechabaserent, ePD_Entrada
    gDB.Procedimiento = "PKG_PATRIMONIO_CUENTAS.RENTABILIDAD_PERIODOS"
    If gDB.EjecutaSP Then
        Set lCursor_Cuotas = gDB.Parametros("pcursor").Valor
        If lCursor_Cuotas.Count > 0 Then
            iBase = 100
            i = 0
            For Each lCampo In lCursor_Cuotas
            
                If NVL(lCampo("rentabilidad_mon_cuenta").Value, 0) = 0 Then
                    nRentabilidad_diaria = 0
                Else
                    nRentabilidad_diaria = CDbl(lCampo("rentabilidad_mon_cuenta").Value)
                End If
                If nRentabilidad_diaria <> 0 Then
                    ReDim Preserve catnom(i)
                    ReDim Preserve Valor(i)
        
                    ReDim Preserve catnomc(i)
                    ReDim Preserve valorC(i)
                    ReDim Preserve catnom3(i)
                    ReDim Preserve valor3(i)
                    If i = 0 Then
                        sFechaMenor = lCampo("fecha_cierre").Value
                        sFechaMayor = lCampo("fecha_cierre").Value
        
                        iValorMenor = iBase
                        iValorMayor = iBase
        
                        catnom(i) = lCampo("fecha_cierre").Value
                        Valor(i) = iBase
                    Else
                        catnom(i) = lCampo("fecha_cierre").Value
                        Valor(i) = Valor(i - 1) * (1 + nRentabilidad_diaria / 100)
                    End If
        
                    If sFechaMenor > lCampo("fecha_cierre").Value Then
                        sFechaMenor = lCampo("fecha_cierre").Value
                    End If
        
                    If sFechaMayor < lCampo("fecha_cierre").Value Then
                        sFechaMayor = lCampo("fecha_cierre").Value
                    End If
        
                    If iValorMenor > Valor(i) Then
                        iValorMenor = Valor(i)
                    End If
        
                    If iValorMayor < Valor(i) Then
                        iValorMayor = Valor(i)
                    End If
                    catnom(i) = Format(lCampo("fecha_cierre").Value, "dd/mm/yy") 'Mid(DameMes(lCampo("fecha_cierre").Value), 1, 3) & "-" & Year(lCampo("fecha_cierre").Value)
                    catnomc(i) = lCampo("fecha_cierre").Value & "-"
                    valorC(i) = CDbl(lCampo("valor_cuota_mon_cuenta").Value)
                    i = i + 1
                End If
            Next
        End If
    Else
        MsgBox gDB.Parametros.Errnum & vbCr & gDB.Parametros.ErrMsg, vbCritical
    End If
    gDB.Parametros.Clear

    ' cantidades = datediff("d", fechainicrent, fechainicrent)

'    Do While Not oRs.EOF
'        If IsNull(oRs("rentabilidad_diaria")) Then
'            nRentabilidad_diaria = 0
'        Else
'            nRentabilidad_diaria = CDbl(oRs("rentabilidad_diaria"))
'        End If
'
'        If nRentabilidad_diaria <> 0 Then
'            ReDim Preserve catnom(i)
'            ReDim Preserve Valor(i)
'
'            ReDim Preserve catnomc(i)
'            ReDim Preserve valorC(i)
'            ReDim Preserve catnom3(i)
'            ReDim Preserve valor3(i)
'
'            If i = 0 Then
'                sFechaMenor = oRs("fecha")
'                sFechaMayor = oRs("fecha")
'
'                iValorMenor = iBase
'                iValorMayor = iBase
'
'                catnom(i) = oRs("fecha")
'                Valor(i) = iBase
'            Else
'                catnom(i) = oRs("fecha")
'                Valor(i) = Valor(i - 1) * (1 + nRentabilidad_diaria / 100)
'            End If
'
'            If sFechaMenor > oRs("fecha") Then
'                sFechaMenor = oRs("fecha")
'            End If
'
'            If sFechaMayor < oRs("fecha") Then
'                sFechaMayor = oRs("fecha")
'            End If
'
'            If iValorMenor > Valor(i) Then
'                iValorMenor = Valor(i)
'            End If
'
'            If iValorMayor < Valor(i) Then
'                iValorMayor = Valor(i)
'            End If
'
'            catnom(i) = Mid(DameMes(oRs("fecha")), 1, 3) & "-" & Year(oRs("fecha"))
'
'            catnomc(i) = oRs("fecha") & "-"
'            valorC(i) = CDbl(oRs("cuota"))
'
'            i = i + 1
'
'        End If
'
'        oRs.MoveNext
'
'    Loop
    
    Dim nSemestres As Double
    Dim iDiferencia As Double
    Dim bLineaTendencia As Boolean

'    nSemestres = Round((DateDiff("d", sFechaMenor, sFechaMayor) / 365) * 2)
'    nSemestres = DateDiff("m", sFechaMenor, sFechaMayor)
'
'    iDiferencia = (iValorMayor - iValorMenor) / 10
'
'    iValorMenor = iValorMenor - iDiferencia
'    iValorMayor = iValorMayor + iDiferencia

    ' sTitulo = "Valor Cuota - " & fechainicrent & " hasta " & fechabaserent & " "
    stitulo = "VALOR CUOTA/BASE 100"

    bLineaTendencia = False
      

'   sName  = GraficoLineas(catnom, valor, sTitulo, false, i-1, nSemestres, bLineaTendencia, Int(iValorMenor/1.1), INT(iValorMayor*1.1) )
    sName = GraficoLineas(catnom, Valor, stitulo, False, i - 1, nSemestres, bLineaTendencia, iValorMenor, iValorMayor)

    fncGraficoValorCuota = sName

End Function

Function GraficoLineas(Categories1, Vals1, stitulo, leyenda, entries, nSemestres, bLineaTendencia, iValorMenor, iValorMayor) As String
    Dim cd
    Dim c
    Dim xColor          As String
    Dim xColorTitulo    As String
    Dim noOfPoints      As Integer
    Dim Path            As String
    Dim sFname As String
    'Dim sTitulo
    Dim x As Integer
    Dim y As Integer
    
    
    
    

    xColorTitulo = ColorToHex(0, 0, 0)
    xColor = ColorToHex(RColorRelleno, GColorRelleno, BColorRelleno)
    'x = 950
    'y = 350

    noOfPoints = 1 ' UBound(Vals1) - 1

    Set cd = CreateObject("ChartDirector.API")
 
    cd.setLicenseCode ("YT5ZJ59NKAF9ZR3942DAE53E")
    
    
    
    x = 350
    y = 220
    Set c = cd.XYChart(x, y) ' , cd.metalColor(&HDEDEDE), -1, 2)

    'Call c.addTitle(stitulo, "arialbd.ttf", 13, &HFFFFFF).setBackground(&HFF0000, &H0, 0)      ' Add a title to the y axis
    Call c.setPlotArea(50, 60, x - 60, y - 90)

'    Set c = cd.XYChart(x, y, &HFFFFFF, 1, 0)
 '   Call c.setPlotArea(100, 60, Int(x * 0.9), Int(y * 0.7), cd.Transparent, cd.Transparent, cd.Transparent, cd.Transparent, cd.Transparent)

    'Call c.yAxis().setLabelStyle("GILB____.TTF", 10)
    'Call c.yAxis().setLabelStyle("GILB____.TTF", 12)
    Call c.yAxis().setLabelStyle("arialbd.ttf", 7, &H555555)
    Call c.yAxis().setLabelFormat("{value|4.}")
    
    Call c.yAxis().setLinearScale(iValorMenor, iValorMayor)

    Call c.xAxis().setLabels(Categories1)

    'Call c.xAxis().setLabelStyle("GILB____.TTF", 12)  ' , 0, 90)
    Call c.xAxis().setLabelStyle("arialbd.ttf", 7, &H555555, 30).setpos(0, -1)
    
    Call c.xAxis().setLabelStep(3)
    
    Call c.addLineLayer(Vals1, 0)

    stitulo = "<*size=14,yoffset=-11*><*block*><*color=" & xColorTitulo & "*>" & stitulo & "<*br*><*/*>"

    Call c.addTitle(stitulo, "GILB____.TTF", 14, xColorTitulo).setBackground(xColor)
    
    'Path = App.Path & "\" '    App.Path ' "." ' Left(Request.ServerVariables("PATH_INFO"), InStrRev(Request.ServerVariables("PATH_INFO"), "/graficos/"))

    'MODIFICADO PARA QUE EL SISTEMA DEJE EN UN DIRECTORIO TEMPORAL EL JPG QUE SE GENERA DEBIDO A QUE ESTABA
    'ENSUCIANDO EL DIRECTORIO DE INSTALACION DEL SISTEMA, LO CUAL, ERA INCORRECTO.�
    'HAF: 20070620.
    Path = Environ$("TEMP") & "\"

    sFname = c.makeTmpFile(Path & sFname, cd.JPG)
    
    GraficoLineas = Path & sFname

End Function
Sub FlujoPatrimonial()
    Dim nColinicio As String
    Dim n As Double
    Dim lReg As hCollection.hFields
    Dim lLinea As Long
    Dim lAporte_Rescate_Cuenta As Class_APORTE_RESCATE_CUENTA
'Dim lcCuenta As Class_Cuentas
'
'  Load Me
'
'  'Se tiene que cargar aca debido a que todavia no se sabe ID que le corresponde
'  Call Sub_CargaCombo_Clientes_Ctas_Ctes_Cta(Cmb_Cta_Cte, fId_Cuenta)
'
'  fEstado = ""

'
'        Call Sub_ComboSelectedItem(Cmb_Medios_Pago, lReg("cod_medio_pago").Value)
'        Call Sub_ComboSelectedItem(Cmb_Bancos, NVL(lReg("id_banco").Value, ""))
'        Call Sub_ComboSelectedItem(Cmb_Cajas, lReg("Id_Caja_Cuenta").Value)
'        Txt_Observaci�n.Text = lReg("dsc_Apo_Res_Cuenta").Value
'        fTipo_Movimiento = lReg("flg_Tipo_Movimiento").Value
'        Txt_Num_Documento.Text = "" & lReg("num_Documento").Value
'        Txt_Dias_Retencion.Text = lReg("retencion").Value
'        Txt_Monto.Text = lReg("monto").Value
'        Cmb_Cta_Cte.Text = "" & lReg("cta_Cte_Bancaria").Value
'        fEstado = lReg("cod_estado").Value
'        Txt_FechaMovimiento.Text = Format(lReg("fecha_movimiento").Value, cFormatDate)
'      Next
'    Else
'      Call Fnt_MsgError(.SubTipo_LOG _
'                        , "Problemas al buscar el Aporte/Rescate." _
'                        , .ErrMsg _
'                        , pConLog:=True)
'    End If
'  End With
'  Set lAporte_Rescate_Cuenta = Nothing
    

    Dim sFechaInicio, sFechaTermino
    Dim nDecimales As Integer
    Dim nMarginLeft As String
    Dim iFila As Variant
    Dim xSql As String
    Dim oRs As New ADODB.Recordset
    Dim nAporte As Double
    Dim nRetiro As Double
    Dim nFlujo As Double
    Dim bHayDatos As Boolean
    Dim i As Integer

    'sFechaInicio = FormatoFecha(DateAdd("yyyy", -1, glb_fecha_hoy))
    'sFechaTermino = glb_fecha_hoy
   
    With lForm.vp
        lForm.vp.StartTable
        lForm.vp.TableBorder = tbBox
        lForm.vp.TableCell(tcRowHeight, 1, 1, 7, 5) = "2mm"
        lForm.vp.TableCell(tcRows) = 12
        lForm.vp.TableCell(tcCols) = 3
        lForm.vp.TableCell(tcBackColor, 1, 1, 1, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        lForm.vp.TableCell(tcText, 1, 1) = "FLUJO PATRIMONIAL ANUAL "
        lForm.vp.TableCell(tcRowBorderBelow, 1) = 2
        lForm.vp.TableCell(tcColSpan, 1, 1) = 3
        
        lForm.vp.TableCell(tcText, 2, 1) = "Fecha"
        lForm.vp.TableCell(tcText, 2, 2) = "Aportes (" & Monto_Moneda(lDsc_Moneda) & ")"
        lForm.vp.TableCell(tcText, 2, 3) = "Retiros (" & Monto_Moneda(lDsc_Moneda) & ")"
        
        lForm.vp.TableCell(tcColWidth, 1, 1, 9, 3) = "33mm"
        lForm.vp.TableCell(tcColAlign, 2, 2, 9, 3) = taRightMiddle
        lForm.vp.TableCell(tcFontSize, 2, 2, 9, 3) = glb_tamletra_registros
        
        iFila = 2
        nAporte = 0
        nRetiro = 0
        
        nFlujo = 0
        n = 0
        
        bHayDatos = False
        nDecimales = 2
        
        Set lAporte_Rescate_Cuenta = New Class_APORTE_RESCATE_CUENTA
        With lAporte_Rescate_Cuenta
            .Campo("Id_Cuenta").Valor = lId_Cuenta
            bHayDatos = False
            If .Buscar_Descendente Then
                For Each lReg In .Cursor
                    If .Cursor.Count > 0 Then
                        bHayDatos = True
                        If iFila <= 9 Then
                            'lForm.vp.CurrentX = rTwips(nColinicio)
                            lForm.vp.TableCell(tcText, iFila + 1, 1) = Format(lReg("fecha_movimiento").Value, cFormatDate)
                            If Trim(lReg("flg_tipo_movimiento").Value) = "A" Then
                                nAporte = Abs(CDbl(lReg("monto").Value))
                                nRetiro = 0
                            Else
                                nAporte = 0
                                nRetiro = Abs(CDbl(lReg("monto").Value))
                            End If
                            If nAporte <> 0 Then
                                lForm.vp.TableCell(tcText, iFila + 1, 2) = FormatNumber(nAporte, nDecimales)
                            Else
                                lForm.vp.TableCell(tcText, iFila + 1, 2) = " "
                            End If
                            If nRetiro <> 0 Then
                                lForm.vp.TableCell(tcText, iFila + 1, 3) = FormatNumber(nRetiro, nDecimales)
                            Else
                                lForm.vp.TableCell(tcText, iFila + 1, 3) = " "
                            End If
                        End If
                        iFila = iFila + 1
                    End If
                Next
            End If
        End With
        Set lAporte_Rescate_Cuenta = Nothing
        For i = iFila To 10
            If i = iFila And Not bHayDatos Then
                lForm.vp.TableCell(tcText, i + 2, 1) = "Sin Movimientos"
            Else
                lForm.vp.TableCell(tcText, i + 2, 1) = " "
            End If

            lForm.vp.TableCell(tcColSpan, i + 2, 1) = 3
            lForm.vp.TableCell(tcColAlign, i + 2, 1) = taCenterTop
            ' iFila = iFila + 1
        Next
        
        ' .TableCell(tcText, i + 1, 1) = "Neto UF Equivalente"
        '.TableCell(tcText, i + 1, 1) = "Aporte/Retiro UF Equivalente"
        '.TableCell(tcAlign, i + 1, 1) = taLeftMiddle
        '.TableCell(tcColSpan, i + 1, 1) = 2
        '.TableCell(tcText, i + 1, 3) = FormatNumber(nFlujo, 2)
        '.TableCell(tcAlign, i + 1, 3) = taCenterMiddle
        
        
        '.TableCell(tcFontSize, 1, 1, i + 3, 3) = glb_tamletra_registros
        '.TableCell(tcRowBorderBelow, i + 1) = 2
        '.TableCell(tcRowBorderAbove, i + 1) = 2
        lForm.vp.EndTable
               
    End With
End Sub
Private Sub Sub_Generar_Reporte_2()
Const clrHeader = &HD0D0D0
'------------------------------------------
Const sHeader_Caja = "Caja|Mercado|Moneda|Monto Moneda Caja|Monto Moneda Cuenta|"
Const sFormat_Caja = "3000|1000|1000|>2500|>2500|>4200"
Const sHeader_SA = "Nemot�cnico|Cantidad|Precio|P.P. Compra|Valor Mercado|Valor Mercado Cuenta|"
Const sFormat_SA = "2900|>1100|>1100|>2500|>2500|>2500|1600"
Const sHeader_Detalle_Caja = "Fecha Mov.|Fecha Liq.|Origen Mov.|Movimientos|Ingreso|Egreso|Saldo"
Const sFormat_Detalle_Caja = "1200|1100|1950|4550|>1800|>1800|>1800"

'------------------------------------------
Dim sRecord
Dim bAppend
Dim lLinea As Integer
'------------------------------------------
Dim lPatrimonio As Class_Patrimonio_Cuentas
Dim lReg_Pat As hFields
Dim lPatrim As String
Dim lRentabilidad As Double
'------------------------------------------
Dim lCajas_Ctas As Class_Cajas_Cuenta
Dim lCursor_Caj As hRecord
Dim lReg_Caj As hFields
Dim lMonto_Mon_Caja As String
'------------------------------------------
Dim lSaldos_Activos As Class_Saldo_Activos
Dim lCursor_SA As hRecord
Dim lReg_SA As hFields
'------------------------------------------
Dim lParidad As Double
Dim lOperacion As String
Dim lMonto_Mon_Cta As String
'------------------------------------------
'Dim lcTipo_Cambio As Class_Tipo_Cambios
Dim lcTipo_Cambio As Object
'------------------------------------------
Dim lInstrumento As Class_Instrumentos
Dim lCursor_Ins As hRecord
Dim lReg_Ins As hFields
Dim lHay_Activos As Boolean
'------------------------------------------
Dim lSaldos_Caja As Class_Saldos_Caja

'--------------------------------------
Dim lReg As hCollection.hFields
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lcMov_Caja As Class_Mov_Caja
Dim lcSaldos_Caja As Class_Saldos_Caja
'--------------------------------------
Dim lFecDesde As Date
Dim lFecHasta As Date
Dim lId_Caja_Cuenta As String
Dim lTotal As Double
Dim lIngreso As Double
Dim lEgreso As Double
Dim lUltimaCaja As Long
Dim nDecimales As Integer

'------------------------------------------
  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  End If
           
  Rem Busca el patrimonio de la cuenta y su rentabilidad
  Set lPatrimonio = New Class_Patrimonio_Cuentas
  With lPatrimonio
    .Campo("id_cuenta").Valor = lId_Cuenta
    .Campo("FECHA_CIERRE").Valor = DTP_Fecha_Ter.Value
    'Valores por defecto
    .Campo("RENTABILIDAD_MON_CUENTA").Valor = Null
    .Campo("APORTE_RETIROS_MON_CUENTA").Valor = Null
    If .Buscar Then
      For Each lReg_Pat In .Cursor
        lPatrim = FormatNumber(lReg_Pat("PATRIMONIO_MON_CUENTA").Value, nDecimales)
        lRentabilidad = NVL(lReg_Pat("RENTABILIDAD_MON_CUENTA").Value, 0)
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  Rem Busca las cajas de la cuenta
  Set lCajas_Ctas = New Class_Cajas_Cuenta
  With lCajas_Ctas
    .Campo("id_cuenta").Valor = lId_Cuenta
    If .Buscar(True) Then
      Set lCursor_Caj = .Cursor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  Rem Busca los Instrumentos del sistema
  Set lInstrumento = New Class_Instrumentos
  With lInstrumento
    If .Buscar Then
      Set lCursor_Ins = .Cursor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  Rem Comienzo de la generaci�n del reporte
    
  With lForm.vp
    Rem Datos de los activos de la cuenta
    .Paragraph = "" 'salto de linea
    .FontSize = glb_tamletra_registros
    .FontBold = True
    .Paragraph = "Datos de Activos de la Cuenta"
    .FontBold = False
    .FontSize = glb_tamletra_registros
    Rem Busca los Activos en cartera de la cuenta
    Rem Por cada instrumento del sistema
    lHay_Activos = False
    For Each lReg_Ins In lCursor_Ins
      Rem Se buscan los activos en cartera para cada instrumento
      Set lSaldos_Activos = New Class_Saldo_Activos
      With lSaldos_Activos
        If .Buscar_saldos_para_cartola(DTP_Fecha_Ter.Value, pId_Cuenta:=lId_Cuenta, pCod_Instrumento:=lReg_Ins("cod_instrumento").Value) Then
          Set lCursor_SA = .Cursor
        Else
          MsgBox .ErrMsg, vbCritical, Me.Caption
          GoTo ErrProcedure
        End If
      End With
      
      Rem Si hay activos para el instrumento consultado procede a la generacion de la tabla
      If lCursor_SA.Count > 0 Then
        lHay_Activos = True
        .FontSize = glb_tamletra_registros
        .FontBold = True
        .Paragraph = lReg_Ins("Dsc_Intrumento").Value
        .FontSize = glb_tamletra_registros
        .FontBold = False
        .StartTable
        .TableCell(tcAlignCurrency) = False
        Rem Se agrupan los activos por instrumentos
        For Each lReg_SA In lCursor_SA
          If Not IsNull(lReg_SA("Fecha_Cierre")) Then
            sRecord = lReg_SA("nemotecnico").Value & "|" & _
                        Format(lReg_SA("cantidad").Value, "#,##0.###0") & "|" & _
                        Format(NVL(lReg_SA("precio").Value, 0), "#,##0.###0") & "|" & _
                        Format(NVL(lReg_SA("precio_promedio_compra").Value, 0), "#,##0.###0") & "|" & _
                        Format(lReg_SA("monto_mon_nemotecnico").Value, Fnt_Formato_Moneda(lReg_SA("id_moneda_nemotecnico"))) & "|" & _
                        Format(lReg_SA("monto_mon_cta").Value, lFormatoMonedaCta) & "|"
            .AddTable sFormat_SA, sHeader_SA, sRecord, clrHeader, , bAppend
           End If
        Next
        '.TableCell(tcBackColor, 1, 1, 1, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcBackColor, 0, 1, 0, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
        .TableCell(tcFontBold, 0) = True
        .EndTable
        .Paragraph = ""
      End If
    Next
    
    If Not lHay_Activos Then
      .Paragraph = "No hay activos para la cuenta"
    End If
    
        Rem Datos de la Caja
    .Paragraph = "" 'salto de linea
    .FontSize = glb_tamletra_registros
    .FontBold = True
    .Paragraph = "Datos de la(s) Caja(s) de la Cuenta"
    .FontBold = False
    .FontSize = glb_tamletra_registros
    .StartTable
    .TableCell(tcAlignCurrency) = False
    For Each lReg_Caj In lCursor_Caj
      lMonto_Mon_Caja = 0
      
      Set lSaldos_Caja = New Class_Saldos_Caja
      With lSaldos_Caja
        .Campo("fecha_cierre").Valor = DTP_Fecha_Ter.Value
        .Campo("id_caja_cuenta").Valor = lReg_Caj("id_caja_cuenta").Value
        If .Buscar Then
          If .Cursor.Count > 0 Then
            lMonto_Mon_Caja = (.Cursor(1)("monto_mon_caja").Value + .Cursor(1)("MONTO_X_COBRAR_MON_CTA").Value) - .Cursor(1)("MONTO_X_PAGAR_MON_CTA").Value
          End If
        End If
      End With
      'Set lSaldos_Caja = Nothing
      
      'lCajas_Ctas.campo("id_caja_cuenta").Valor = lReg_Caj("id_caja_cuenta").Value
      'lMonto_Mon_Caja = Format(CStr(lCajas_Ctas.Saldo_Disponible(DTP_Fecha.Value)), "###,##0.0000")
      
'      Set lcTipo_Cambio = New Class_Tipo_Cambios
      Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
      If lcTipo_Cambio.Busca_Precio_Paridad(lId_Cuenta _
                                       , lReg_Caj("Id_Moneda").Value _
                                       , lId_Moneda_Cta _
                                       , DTP_Fecha_Ter.Value _
                                       , lParidad _
                                       , lOperacion) Then
        If lOperacion = "M" Then
          Rem Multiplicacion
          lMonto_Mon_Cta = lMonto_Mon_Caja * lParidad
        Else
          Rem Division
          lMonto_Mon_Cta = Fnt_Divide(lMonto_Mon_Caja, lParidad)
        End If
      Else
        MsgBox lcTipo_Cambio.ErrMsg, vbCritical, Me.Caption
      End If
      sRecord = lReg_Caj("Dsc_Caja_Cuenta").Value & "|" & _
                lReg_Caj("Desc_Mercado").Value & "|" & _
                lReg_Caj("Dsc_Moneda").Value & "|" & _
                Format(lMonto_Mon_Caja, Fnt_Formato_Moneda(lReg_Caj("Id_Moneda").Value)) & "|" & _
                Format(lMonto_Mon_Cta, lFormatoMonedaCta) & "|"
      .AddTable sFormat_Caja, sHeader_Caja, sRecord, clrHeader, , bAppend
    Next
    .TableCell(tcBackColor, 0, 1, 0, 5) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    .TableCell(tcFontBold, 0) = True
    .EndTable
    
Rem --------------------
    .Paragraph = "" 'salto de linea
    .FontSize = glb_tamletra_registros
    .FontBold = True
    .Paragraph = "Detalle de Cajas"
    .FontBold = False
    .FontSize = glb_tamletra_registros
    .StartTable
    .TableCell(tcAlignCurrency) = False
      Call Sub_Bloquea_Puntero(Me)
      
      If Not Fnt_Form_Validar(Me) Then
        'MsgBox "Debe seleccionar una Cuenta para buscar Movimientos de Caja.", vbInformation, Me.Caption
        GoTo ErrProcedure
      End If
      
      lFecDesde = CDate(DameInicioDeMes(DTP_Fecha_Ter.Value))
      
      lFecHasta = DTP_Fecha_Ter.Value
       
      If lFecDesde > lFecHasta Then
        MsgBox "La Fecha Desde debe ser menor que Fecha Hasta.", vbExclamation, Me.Caption
        GoTo ErrProcedure
      End If
      
      lUltimaCaja = 0
      lIngreso = 0
      lEgreso = 0
      lTotal = 0
      lId_Caja_Cuenta = ""
      Set lcMov_Caja = New Class_Mov_Caja
      With lcMov_Caja
        If Not lId_Caja_Cuenta = "" Then
          .Campo("Id_Caja_Cuenta").Valor = lId_Caja_Cuenta
        End If
        If .Buscar_Movimientos_Cuenta(lId_Cuenta, lFecDesde, lFecHasta) Then
          For Each lReg In .Cursor
            If lUltimaCaja = lReg("Id_Caja_Cuenta").Value Then
              If lReg("FLG_Tipo_Movimiento").Value = gcTipoOperacion_Abono Then
                lTotal = lTotal + lReg("Monto").Value
                lIngreso = lIngreso + lReg("Monto").Value
                sRecord = lReg("Fecha_Movimiento").Value & "|" & _
                        lReg("Fecha_Liquidacion").Value & "|" & _
                        lReg("dsc_Origen_Mov_Caja").Value & "|" & _
                        lReg("Dsc_Mov_Caja").Value & "|" & _
                        FormatNumber(lReg("Monto").Value, Dame_Decimales(lDsc_Moneda)) & "|" & _
                        FormatNumber(0, Dame_Decimales(lDsc_Moneda)) & "|" & _
                        FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
              Else
                lTotal = lTotal - lReg("Monto").Value
                lEgreso = lEgreso + lReg("Monto").Value
                sRecord = lReg("Fecha_Movimiento").Value & "|" & _
                        lReg("Fecha_Liquidacion").Value & "|" & _
                        lReg("dsc_Origen_Mov_Caja").Value & "|" & _
                        lReg("Dsc_Mov_Caja").Value & "|" & _
                        FormatNumber(0, Dame_Decimales(lDsc_Moneda)) & "|" & _
                        FormatNumber(lReg("Monto").Value, Dame_Decimales(lDsc_Moneda)) & "|" & _
                        FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
              End If
              lForm.vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
            Else
              If Not lUltimaCaja = 0 Then
                 sRecord = "|" & _
                         "|" & _
                         "|" & _
                         "SALDO CAJA" & "|" & _
                         FormatNumber(lIngreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
                         FormatNumber(lEgreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
                         FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
                lForm.vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
                lForm.vp.TableCell(tcFontBold, lForm.vp.TableCell(tcRows), 4, lForm.vp.TableCell(tcRows), 4) = True
                lIngreso = 0
                lEgreso = 0
                lTotal = 0
              End If
              sRecord = "|" & _
                        "|" & _
                        "|" & _
                        UCase(lReg("DSC_CAJA_CUENTA").Value) & "|" & _
                        "|" & _
                        "|"
              lForm.vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
              'lForm.vp.TableCell(tcBackColor, 1, 1, 1, 3) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
              lForm.vp.TableCell(tcBackColor, lForm.vp.TableCell(tcRows), 1, lForm.vp.TableCell(tcRows), 7) = RGB(232, 226, 222)
              lForm.vp.TableCell(tcFontBold, lForm.vp.TableCell(tcRows), 4, lForm.vp.TableCell(tcRows), 4) = True
              lForm.vp.TableCell(tcRowBorderBelow, lForm.vp.TableCell(tcRows)) = 2
              lForm.vp.TableCell(tcRowBorderAbove, lForm.vp.TableCell(tcRows)) = 2
              
              lUltimaCaja = lReg("Id_Caja_Cuenta").Value
              'Buscar Saldo Anterior por Caja
              Set lcSaldos_Caja = New Class_Saldos_Caja
              With lcSaldos_Caja
                .Campo("id_Caja_Cuenta").Valor = lUltimaCaja
                .Campo("Fecha_cierre").Valor = lFecDesde
                If .Buscar_Saldos_Cuenta(lTotal, lId_Cuenta) Then
                    sRecord = lFecDesde - 1 & "|" & _
                              "|" & _
                              "SALDO " & "|" & _
                              "Saldo Anterior" & "|" & _
                              FormatNumber(lIngreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
                              FormatNumber(lEgreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
                              FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
                    lForm.vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
                    lForm.vp.TableCell(tcFontBold, lForm.vp.TableCell(tcRows), 4, lForm.vp.TableCell(tcRows), 4) = True
                Else
                  Call Fnt_MsgError(.SubTipo_LOG, _
                                    "Problemas en cargar Valores Cuota.", _
                                    .ErrMsg, _
                                    pConLog:=True)
                  lTotal = 0
                End If
              End With
              Set lcSaldos_Caja = Nothing
              If lReg("FLG_Tipo_Movimiento").Value = gcTipoOperacion_Abono Then
                'Call SetCell(Grilla, lLinea, "Ingresos", lReg("Monto").Value)
                lTotal = lTotal + lReg("Monto").Value
                lIngreso = lIngreso + lReg("Monto").Value
                sRecord = lReg("Fecha_Movimiento").Value & "|" & _
                        lReg("Fecha_Liquidacion").Value & "|" & _
                        lReg("dsc_Origen_Mov_Caja").Value & "|" & _
                        lReg("Dsc_Mov_Caja").Value & "|" & _
                        FormatNumber(lReg("Monto").Value, Dame_Decimales(lDsc_Moneda)) & "|" & _
                        FormatNumber(0, Dame_Decimales(lDsc_Moneda)) & "|" & _
                        FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
              Else
                'Call SetCell(Grilla, lLinea, "Egresos", lReg("Monto").Value)
                lTotal = lTotal - lReg("Monto").Value
                lEgreso = lEgreso + lReg("Monto").Value
                sRecord = lReg("Fecha_Movimiento").Value & "|" & _
                        lReg("Fecha_Liquidacion").Value & "|" & _
                        lReg("dsc_Origen_Mov_Caja").Value & "|" & _
                        lReg("Dsc_Mov_Caja").Value & "|" & _
                        FormatNumber(0, Dame_Decimales(lDsc_Moneda)) & "|" & _
                        FormatNumber(lReg("Monto").Value, Dame_Decimales(lDsc_Moneda)) & "|" & _
                        FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
              End If
              lForm.vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
            End If
          Next
          sRecord = "|" & _
                  "|" & _
                  "|" & _
                  "SALDO CAJA" & "|" & _
                  FormatNumber(lIngreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
                  FormatNumber(lEgreso, Dame_Decimales(lDsc_Moneda)) & "|" & _
                  FormatNumber(lTotal, Dame_Decimales(lDsc_Moneda))
         lForm.vp.AddTable sFormat_Detalle_Caja, sHeader_Detalle_Caja, sRecord, clrHeader, , bAppend
         lForm.vp.TableCell(tcBackColor, 0, 1, 0, 7) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
         lForm.vp.TableCell(tcFontBold, 0) = True
         lForm.vp.EndTable
        Else
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en cargar Valores Cuota.", _
                            .ErrMsg, _
                            pConLog:=True)
        End If
      End With
      Set lcMov_Caja = Nothing
Rem --------------------
    '.EndDoc
  End With
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub
Function FormatNumber(xValor As Variant, iDecimales As Integer, Optional bPorcentaje As Boolean = False) As String
    Dim xFormato        As String
    Dim sDecimalSep     As String
    Dim sMilesSep       As String
        
    sDecimalSep = Obtener_Simbolo(LOCALE_SDECIMAL)
    sMilesSep = Obtener_Simbolo(LOCALE_SMONTHOUSANDSEP)
    
    On Error GoTo PError
        
    If iDecimales = 0 Then
        xFormato = "###,###,##0"
    ElseIf bPorcentaje Then
        xFormato = "##0." & String(iDecimales - 1, "#") & "0"
    Else
        xFormato = "###,###,##0." & String(iDecimales - 1, "#") & "0"
    End If
    
    GoTo Fin
    
PError:
    xValor = "0"
    
Fin:
    FormatNumber = Format(xValor, xFormato)
    On Error GoTo 0
    
End Function

Function DameMes(sFecha)
    Dim iMes
    Dim aMeses
    iMes = Month(sFecha)
    aMeses = Array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
    DameMes = aMeses(iMes)
End Function





