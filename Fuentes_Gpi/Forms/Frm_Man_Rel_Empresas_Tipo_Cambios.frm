VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Man_Rel_Empresas_Tipo_Cambio 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Tipos de Cambios"
   ClientHeight    =   2820
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7200
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2820
   ScaleWidth      =   7200
   Begin VB.Frame Frame1 
      Caption         =   "Tipos de Cambio"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2325
      Left            =   30
      TabIndex        =   2
      Top             =   390
      Width           =   7095
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   1875
         Left            =   120
         TabIndex        =   3
         Top             =   300
         Width           =   6795
         _cx             =   11986
         _cy             =   3307
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   4
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Rel_Empresas_Tipo_Cambios.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7200
      _ExtentX        =   12700
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Eliminar"
            Key             =   "DEL"
            Description     =   "Elimina un elemento"
            Object.ToolTipText     =   "Modifica un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un elemento"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Rel_Empresas_Tipo_Cambio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = "boton_agregar_grilla"
      .Buttons("DEL").Image = "boton_eliminar_grilla"
      .Buttons("EXIT").Image = "boton_salir"
      .Buttons("UPDATE").Image = "boton_modificar"
      .Buttons("REFRESH").Image = "boton_refrescar"
  End With

  Call Sub_CargaForm
  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1

End Sub

Private Sub Sub_CargaForm()
Dim lComboList As String
Dim lReg As hFields




End Sub


Private Sub Sub_CargarDatos()
  Dim lReg    As hCollection.hFields
  Dim lLinea  As Long
  Dim lID     As String
  Grilla.Rows = 1

  If Grilla.Row > 0 Then
      lID = GetCell(Grilla, Grilla.Row, "id_tipo_cambio")
  Else
      lID = ""
  End If
  
  gDB.Tabla = "View_Empresa_Tipo_Cambio"
  gDB.Parametros.Add "id_empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
  
  If gDB.EjecutaSelect Then
      For Each lReg In gDB.Parametros("cursor").valor
          lLinea = Grilla.Rows
          Call Grilla.AddItem("")
          Call SetCell(Grilla, lLinea, "id_tipo_cambio", NVL(lReg("id_tipo_cambio").Value, ""))
          Call SetCell(Grilla, lLinea, "dsc_origen", NVL(lReg("fuente").Value, ""))
          Call SetCell(Grilla, lLinea, "dsc_destino", NVL(lReg("destino").Value, ""))
          Call SetCell(Grilla, lLinea, "dsc_tipo_cambio", NVL(lReg("dsc_tipo_cambio").Value, ""))
      Next
  End If
  gDB.Parametros.Clear
  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("id_banco"))
    Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
  End If
End Sub


Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "ADD"
      Call Sub_Agregar
    'Case "UPDATE"
    '  Call Grilla_DblClick
    Case "REFRESH"
      Grilla.Rows = 1
      Call Sub_CargarDatos
    Case "DEL"
      Call Sub_Eliminar
    Case "EXIT"
      Unload Me
  End Select

End Sub


Private Sub Sub_Agregar()
  Call Sub_EsperaVentana(cNewEntidad)
End Sub


Private Sub Sub_Eliminar()
Dim lId_tipo_cambio As String

  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      lId_tipo_cambio = GetCell(Grilla, Grilla.Row, "id_tipo_cambio")
      If Not lId_tipo_cambio = "" Then
        gDB.Tabla = "rel_empresas_tipo_cambios"
        gDB.Parametros.Add "id_tipo_cambio", ePT_Numero, lId_tipo_cambio, ePD_Entrada
        If gDB.EjecutaTablaDelete Then
          MsgBox "Tipo Cambio eliminado", vbInformation + vbOKOnly, Me.Caption
        Else
          MsgBox gDB.ErrMsg, vbCritical, Me.Caption
        End If
        gDB.Parametros.Clear
      End If

      Call Sub_CargarDatos
    End If
  End If
  
End Sub

Private Sub Sub_EsperaVentana(ByVal pkey)
Dim lForm As Frm_Tipos_Cambio
Dim lNombre As String
Dim lNom_Form As String
  
  lNom_Form = "Frm_Tipos_Cambio"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lNombre = Me.Name
    
    Set lForm = New Frm_Tipos_Cambio
    Call lForm.Fnt_Modificar(pkey)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      Call Sub_CargarDatos
    End If
  End If
End Sub



