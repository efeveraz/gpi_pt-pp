VERSION 5.00
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{1BCC7098-34C1-4749-B1A3-6C109878B38F}#1.0#0"; "vspdf8.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Reporte_Generico 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Impresi�n"
   ClientHeight    =   11160
   ClientLeft      =   180
   ClientTop       =   330
   ClientWidth     =   11385
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   11160
   ScaleWidth      =   11385
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   2
      Top             =   10755
      Width           =   11385
      _ExtentX        =   20082
      _ExtentY        =   714
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11385
      _ExtentX        =   20082
      _ExtentY        =   635
      ButtonWidth     =   1773
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Min/Max"
            Key             =   "MaxMin"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "a PDF"
            Key             =   "APDF"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin VSPDF8LibCtl.VSPDF8 VSPDF8 
         Left            =   2820
         Top             =   90
         Author          =   ""
         Creator         =   "CSGPI"
         Title           =   ""
         Subject         =   ""
         Keywords        =   ""
         Compress        =   3
      End
   End
   Begin C1SizerLibCtl.C1Elastic C1Elastic2 
      Height          =   10410
      Left            =   0
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   360
      Width           =   11385
      _cx             =   20082
      _cy             =   18362
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   4
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FloodColor      =   6553600
      ForeColorDisabled=   -2147483631
      Caption         =   ""
      Align           =   5
      AutoSizeChildren=   8
      BorderWidth     =   6
      ChildSpacing    =   4
      Splitter        =   0   'False
      FloodDirection  =   0
      FloodPercent    =   0
      CaptionPos      =   1
      WordWrap        =   -1  'True
      MaxChildSize    =   0
      MinChildSize    =   0
      TagWidth        =   0
      TagPosition     =   0
      Style           =   0
      TagSplit        =   2
      PicturePos      =   4
      CaptionStyle    =   0
      ResizeFonts     =   0   'False
      GridRows        =   3
      GridCols        =   3
      Frame           =   3
      FrameStyle      =   0
      FrameWidth      =   1
      FrameColor      =   -2147483628
      FrameShadow     =   -2147483632
      FloodStyle      =   1
      _GridInfo       =   $"Frm_Reporte_Generico.frx":0000
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   9
      Begin VSPrinter8LibCtl.VSPrinter VsPrinter 
         Height          =   9960
         Left            =   225
         TabIndex        =   4
         Top             =   225
         Width           =   10935
         _cx             =   19288
         _cy             =   17568
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         MousePointer    =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoRTF         =   -1  'True
         Preview         =   -1  'True
         DefaultDevice   =   0   'False
         PhysicalPage    =   -1  'True
         AbortWindow     =   -1  'True
         AbortWindowPos  =   0
         AbortCaption    =   "Imprimiendo..."
         AbortTextButton =   "Cancelar"
         AbortTextDevice =   "en %s en %s"
         AbortTextPage   =   "Ahora imprimiendo P�gina %d de"
         FileName        =   ""
         MarginLeft      =   1440
         MarginTop       =   1440
         MarginRight     =   1440
         MarginBottom    =   1000
         MarginHeader    =   0
         MarginFooter    =   0
         IndentLeft      =   0
         IndentRight     =   0
         IndentFirst     =   0
         IndentTab       =   720
         SpaceBefore     =   0
         SpaceAfter      =   0
         LineSpacing     =   100
         Columns         =   1
         ColumnSpacing   =   180
         ShowGuides      =   2
         LargeChangeHorz =   300
         LargeChangeVert =   300
         SmallChangeHorz =   30
         SmallChangeVert =   30
         Track           =   0   'False
         ProportionalBars=   -1  'True
         Zoom            =   57.7651515151515
         ZoomMode        =   3
         ZoomMax         =   400
         ZoomMin         =   10
         ZoomStep        =   25
         EmptyColor      =   -2147483636
         TextColor       =   0
         HdrColor        =   0
         BrushColor      =   0
         BrushStyle      =   0
         PenColor        =   0
         PenStyle        =   0
         PenWidth        =   0
         PageBorder      =   0
         Header          =   ""
         Footer          =   ""
         TableSep        =   "|;"
         TableBorder     =   7
         TablePen        =   0
         TablePenLR      =   0
         TablePenTB      =   0
         NavBar          =   3
         NavBarColor     =   -2147483633
         ExportFormat    =   0
         URL             =   ""
         Navigation      =   7
         NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
         AutoLinkNavigate=   0   'False
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Generico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Object_Padre As Object

Public fTipoSalida As ePrinter
Dim fArchivoSalida As String

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("APDF").Image = cBoton_Pdf
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
  Me.Width = 10260
  Me.Height = 8205
  Me.Hide
End Sub

Private Sub Sub_CargaForm()
  Call Sub_FormControl_Color(Me.Controls)
End Sub

Public Sub Sub_InicarDocumento(ByVal pTitulo As String _
                             , ByVal pTipoSalida As ePrinter _
                             , Optional pOrientacion As OrientationSettings = OrientationSettings.orPortrait _
                             , Optional pArchivoSalida As String = "" _
                             , Optional pImprimeSoloEmpresa As String = "")
  
  fTipoSalida = pTipoSalida
  fArchivoSalida = pArchivoSalida
  Set Object_Padre = Nothing
  
  VSPDF8.Title = pTitulo
  VSPDF8.Compress = vspfdCompressAll
  
  With VsPrinter
    .Clear
    .Orientation = pOrientacion
    .StartDoc
    .FontName = "Arial"
    .SpaceBefore = "2pt"
    .SpaceAfter = "2pt"
    '-----------------------------------------------------------------------------------------------------
    Rem Encabezado y Pie de p�gina
    .HdrFontName = "Arial"
    .HdrFontSize = 10
    .HdrFontBold = True
    If pImprimeSoloEmpresa <> "S" Then
       .Footer = gDsc_Empresa & "||P�gina %d" & vbLf & "Impreso el " & Format(Fnt_SYSDATE, cFormatSysdate)
    Else
       .Footer = gDsc_Empresa
    End If
    '-----------------------------------------------------------------------------------------------------
    Rem Titulos
    .FontSize = 14
    .FontBold = True
    .Paragraph = pTitulo
  End With
  
  Me.Hide
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
  With Toolbar
    .Buttons("MaxMin").Image = IIf(Me.WindowState = vbMaximized, cBoton_RestoreWindow, cBoton_MaxWindow)
  End With
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "EXIT"
      Unload Me
    Case "APDF"
      Call Sub_ElijeArchivoPDF
    Case "MaxMin"
      Me.WindowState = IIf(Me.WindowState = vbMaximized, vbNormal, vbMaximized)
  End Select
End Sub

Private Sub VsPrinter_BeforeFooter()
  With VsPrinter
    .HdrFontSize = 9
    .HdrFontBold = False
  End With
End Sub

Private Sub VsPrinter_BeforeHeader()
  With VsPrinter
    .HdrFontSize = 10
    .HdrFontBold = True
  End With
End Sub

Private Sub VsPrinter_EndDoc()
  Select Case fTipoSalida
    Case ePrinter.eP_Pantalla
      Me.Show
    Case ePrinter.eP_Impresora
      Call VsPrinter.PrintDoc(True)
      Unload Me
    Case ePrinter.eP_PDF
      Call Sub_ElijeArchivoPDF
      Unload Me
    Case ePrinter.eP_PDF_Automatico
      Call Fnt_ConvertPDF
      Unload Me
    Case Else
      Unload Me
  End Select
End Sub

Private Sub VsPrinter_EndPage()
  On Error Resume Next
  Call VsPrinter.DrawPicture(LoadPicture(gStrPictureEmpresa), 1500, 350)
End Sub

Private Function Fnt_ConvertPDF() As Boolean
On Error GoTo ErrProcedure
  
  Fnt_ConvertPDF = False

  If fArchivoSalida = "" Then
    fArchivoSalida = VSPDF8.Title & ".pdf"
  End If
  
  Call VSPDF8.ConvertDocument(VsPrinter, fArchivoSalida)
  
  Fnt_ConvertPDF = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al exportar a PDF.", Err.Description, pConLog:=True)
    Err.Clear
    Resume
  End If

End Function


Private Sub Sub_ElijeArchivoPDF()
Dim lArchivo As String

On Error GoTo Cmd_BuscarArchivo_Err
    
  If fArchivoSalida = "" Then
    fArchivoSalida = VSPDF8.Title
  End If
    
  With MDI_Principal.CommonDialog
    
    If .Filename = "" Then
      .Filename = fArchivoSalida
    End If
    
    .CancelError = True
    .Filter = "Archivo PDF (*.pdf)|*.pdf|Todos los Archivos (*.*)|*.*"
    .Flags = cdlOFNExplorer + cdlOFNHideReadOnly + cdlOFNLongNames + cdlOFNNoReadOnlyReturn + cdlOFNOverwritePrompt + cdlOFNPathMustExist
  
    .ShowSave
    
    lArchivo = .Filename
  End With

  If lArchivo = "" Then
    GoTo ExitProcedure
  End If

  fArchivoSalida = lArchivo

  If Fnt_ConvertPDF Then
    If MsgBox("�Desea abrir el documento?", vbYesNo) = vbYes Then
      Call Shell("cmd /c """ & lArchivo & """")
    End If
  End If

Cmd_BuscarArchivo_Err:
  If Err.Number = 32755 Then 'usuario presiona el boton Cancelar del Command Dialog
    Exit Sub
  End If
  
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al exportar a PDF.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If
ExitProcedure:

End Sub

Private Sub VsPrinter_NewPage()
  On Error Resume Next
  Call Object_Padre.VsPrint_NewPage(VsPrinter)
  On Error GoTo 0
End Sub
