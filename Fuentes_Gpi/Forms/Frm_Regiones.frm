VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Regiones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Regi�n"
   ClientHeight    =   4560
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6810
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4560
   ScaleWidth      =   6810
   Begin VB.Frame Frame1 
      Caption         =   "Regi�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4065
      Left            =   60
      TabIndex        =   2
      Top             =   450
      Width           =   6675
      Begin VB.Frame frm_instrumentos 
         Caption         =   "Comuna/Ciudad"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   2520
         Left            =   120
         TabIndex        =   4
         Top             =   1080
         Width           =   6405
         Begin VSFlex8LCtl.VSFlexGrid grdComunaCiudad 
            Height          =   2085
            Left            =   120
            TabIndex        =   5
            Top             =   270
            Width           =   5505
            _cx             =   9710
            _cy             =   3678
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   4
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Regiones.frx":0000
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
         Begin MSComctlLib.Toolbar Toolbar_Instrumento 
            Height          =   660
            Left            =   5790
            TabIndex        =   6
            Top             =   270
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ADD"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Agregar un instrumento a la Contraparte"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "DEL"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Elimina el instrumento seleccionado de la Contraparte"
               EndProperty
            EndProperty
         End
      End
      Begin hControl2.hTextLabel Txt_Region 
         Height          =   315
         Left            =   180
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   660
         Width           =   5550
         _ExtentX        =   9790
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Regi�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   50
      End
      Begin hControl2.hTextLabel txtPais 
         Height          =   315
         Left            =   180
         TabIndex        =   7
         Tag             =   "OBLI"
         Top             =   240
         Width           =   5550
         _ExtentX        =   9790
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Pa�s"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         MaxLength       =   50
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6810
      _ExtentX        =   12012
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Regiones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey                 As String
Public sCodPais             As String

Dim lId_Region_new          As String
Dim lId_Comuna_Ciudad_new   As String
Dim lDsc_Comuna_Ciudad_new  As String
Dim lDsc_Region_new         As String

Dim fFlg_Tipo_Permiso       As String
Dim fCod_Arbol_Sistema      As String

Private Sub Form_Load()
    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("SAVE").Image = cBoton_Grabar
            .Buttons("EXIT").Image = cBoton_Salir
            .Buttons("REFRESH").Image = cBoton_Original
    End With
  
    With Toolbar_Instrumento
    Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("ADD").Image = cBoton_Agregar_Grilla
        .Buttons("DEL").Image = cBoton_Eliminar_Grilla
        .Appearance = ccFlat
    End With
'
'  With Toolbar_Trader
'  Set .ImageList = MDI_Principal.ImageListGlobal16
'      .Buttons("ADD").Image = cBoton_Agregar_Grilla
'      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
'      .Appearance = ccFlat
'  End With
  
  Call Sub_CargaForm

' Me.SSTab.CurrTab = 0
  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pid_region, pCod_Arbol_Sistema, Cod_Pais)
    fKey = pid_region
    sCodPais = Cod_Pais
  
    fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
    fCod_Arbol_Sistema = pCod_Arbol_Sistema
  
    Call Form_Resize
  
    Call Sub_CargarDatos
    
    If fKey = cNewEntidad Then
        Me.Caption = "Ingreso de Regi�n"
    Else
        Me.Caption = "Modificaci�n Regi�n: " & Txt_Region.Text
    End If
    
    Me.Top = 1
    Me.Left = 1
    
    Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Rem Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
    Dim lTipo_Contraparte   As String
    Dim lFila               As Long
    Dim lRollback           As Boolean
    
    Dim oRegiones           As Class_Regiones
    Dim oComunasCiudades    As Class_Comuna_Ciudad

    Call Sub_Bloquea_Puntero(Me)
  
    Fnt_Grabar = True

    If Not Fnt_ValidarDatos Then
        Fnt_Grabar = False
        Call Sub_Desbloquea_Puntero(Me)
        Exit Function
    End If

    lRollback = False
    gDB.IniciarTransaccion
  
    Set oRegiones = New Class_Regiones
    
    With oRegiones
        Rem Graba los datos en la tabla Regiones
        .Campo("id_region").Valor = fKey
        .Campo("dsc_region").Valor = Txt_Region.Text
        .Campo("cod_pais").Valor = sCodPais
        
        If Not .Guardar Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en guardar la Regi�n.", _
                            .ErrMsg, _
                            pConLog:=True)
            lRollback = True
            
            GoTo ErrProcedure
        End If
        
        fKey = .Campo("id_region").Valor
    End With
    
    Set oRegiones = Nothing
  
'  Rem Borra todos los registros de Rel_contrapartes_instrumentos seg�n el id
'  Set oComunasCiudades = New Class_Comuna_Ciudad
'  With oComunasCiudades
'    .Campo("ID_CONTRAPARTE").Valor = fKey
'    If Not .Borrar Then
'      Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Problemas en guardar Regi�n.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      lRollback = True
'      GoTo ErrProcedure
'    End If
'  End With
'  Set oComunasCiudades = Nothing
'
'  Rem Graba los instrumentos de la grdComunaCiudad
'  If grdComunaCiudad.Rows > 1 Then
'    For lFila = 1 To grdComunaCiudad.Rows - 1
'      Set oComunasCiudades = New Class_Comuna_Ciudad
'      With oComunasCiudades
'        .Campo("id_contraparte").Valor = fKey
'        .Campo("cod_instrumento").Valor = GetCell(grdComunaCiudad, lFila, "cod_instrumento")
'        If Not .Guardar Then
'          Call Fnt_MsgError(.SubTipo_LOG, _
'                      "Problemas en guardar Regi�n.", _
'                      .ErrMsg, _
'                      pConLog:=True)
'          lRollback = True
'          Exit For
'        End If
'      End With
'      Set oComunasCiudades = Nothing
'    Next
'  End If
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    Fnt_Grabar = False
  Else
    gDB.CommitTransaccion
    Call Sub_CargarDatos
  End If
  
  Set oRegiones = Nothing
  Set oComunasCiudades = Nothing

  Call Sub_Desbloquea_Puntero(Me)
End Function

Private Sub Sub_CargaForm()

  Call Sub_FormControl_Color(Me.Controls)

  Rem Carga los Tipos de Contrapartes
  ' Call Sub_CargaCombo_Tipos_Contrapartes(Cmb_TipoContraparte)
  
  Txt_Region.Text = ""
  grdComunaCiudad.Rows = 1
  
  ' Grilla_Traders.Rows = 1
    
End Sub

Private Sub Sub_CargarDatos()
    Dim oComunaCiudad   As Class_Comuna_Ciudad
    Dim lReg            As hCollection.hFields
    '-----------------------------------------------------------------
    Dim lLinea  As Long
    Dim lID     As String
  
    Load Me
    
    '-----------------------------------------------------
    ' Setea el txtPais con el extraido de la base de datos
    '-----------------------------------------------------
    Call BuscaNombrePais
    '----------------------------------------------------------------------------------
    '   Busca la region
    '----------------------------------------------------------------------------------
    Call BuscaNombreRegion
    
    '----------------------------------------------------------------------------------
    '   Busca las comunas/ciudades asociadas a la region
    '----------------------------------------------------------------------------------
    Set oComunaCiudad = New Class_Comuna_Ciudad
    
    With oComunaCiudad
        .Campo("ID_REGION").Valor = fKey
        
        If .Buscar Then
            grdComunaCiudad.Rows = 1
            For Each lReg In .Cursor
                lLinea = grdComunaCiudad.Rows
                grdComunaCiudad.AddItem ""
                
                SetCell grdComunaCiudad, lLinea, "colum_pk", lReg("ID_COMUNA_CIUDAD").Value
                SetCell grdComunaCiudad, lLinea, "ID_REGION", lReg("ID_REGION").Value
                SetCell grdComunaCiudad, lLinea, "DSC_COMUNA_CIUDAD", lReg("DSC_COMUNA_CIUDAD").Value
                SetCell grdComunaCiudad, lLinea, "DSC_REGION", lReg("DSC_REGION").Value
            Next
        Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en cargar la relacion Regi�n/Ciudad-Comuna.", _
                              .ErrMsg, _
                              pConLog:=True)
        End If
    End With
    
    Set oComunaCiudad = Nothing
  
    'Carga los traders de la contraparte
    ' Call Sub_CargarTraders
    
    If Not lID = "" Then
        grdComunaCiudad.Row = grdComunaCiudad.FindRow(lID, , grdComunaCiudad.ColIndex("colum_pk"))
        If Not grdComunaCiudad.Row = cNewEntidad Then
            Call grdComunaCiudad.ShowCell(grdComunaCiudad.Row, grdComunaCiudad.ColIndex("colum_pk"))
        End If
    End If
    
End Sub

Private Sub BuscaNombreRegion()
    Dim oRegion         As Class_Regiones
    Set oRegion = New Class_Regiones
    
    With oRegion
        .Campo("COD_PAIS").Valor = sCodPais
        .Campo("id_region").Valor = fKey
        
        If .Buscar Then
            If .Cursor.Count > 0 Then
                Txt_Region.Text = .Cursor(1).Fields("DSC_REGION").Value
            Else
                Txt_Region.Text = ""
            End If
        Else
            Txt_Region.Text = "no tiene"
'            Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Problemas en cargar Regi�n.", _
'                        .ErrMsg, _
'                        pConLog:=True)
        End If
        
    End With
    
    Set oRegion = Nothing
    
End Sub

Private Sub BuscaNombrePais()
    Dim oPais As New Class_Paises
    
    With oPais
    
        .Campo("Cod_Pais").Valor = sCodPais
        
        If .Buscar() Then
            If .Cursor(1).Count > 0 Then
                txtPais.Text = .Cursor(1).Fields("DSC_PAIS").Value
            Else
                txtPais.Text = ""
            End If
        
        Else
            txtPais.Text = "no tiene"
        End If
        
    End With
    
    Set oPais = Nothing
    
End Sub


Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function

Private Sub Toolbar_Instrumento_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents

    Select Case Button.Key
        Case "ADD"
            Call Sub_Agregar_Instrumento
        Case "DEL"
            If grdComunaCiudad.Row > 0 Then
                If MsgBox("�Esta seguro de eliminar la Comuna/Ciudad?", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
                    Sub_Eliminar_ComunaCiudad
                End If
            End If
    End Select
    
End Sub

Private Sub Sub_Eliminar_ComunaCiudad()
    Dim sIdComunaCiudad As String
    
    Dim oComunaCiudad As Class_Comuna_Ciudad
    
    sIdComunaCiudad = GetCell(grdComunaCiudad, grdComunaCiudad.Row, "colum_pk")
    
    Set oComunaCiudad = New Class_Comuna_Ciudad
    
    With oComunaCiudad
        .Campo("ID_COMUNA_CIUDAD").Valor = sIdComunaCiudad
        .Campo("id_region").Valor = fKey
        
        If .Borrar Then
        Else
            Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en borrar la comuna/ciudad.", _
                            .ErrMsg, _
                            pConLog:=True)
        End If
        
        Call Sub_CargarDatos
    End With
    
    Set oComunaCiudad = Nothing
    
End Sub

Private Sub grdComunaCiudad_DblClick()
    Dim sIdComunaCiudad As String
    
    If grdComunaCiudad.Row > 0 Then
        sIdComunaCiudad = GetCell(grdComunaCiudad, grdComunaCiudad.Row, "colum_pk")
        Call Sub_EsperaVentana_ComunaCiudad(fKey, sIdComunaCiudad)
    End If
    
End Sub

Private Sub Sub_Agregar_Instrumento()
    If fKey = "-1" Then
        If MsgBox("La Regi�n no est� Guardada." & vbCrLf & _
                  "Para agregar Comunas/Ciudades debe guardar antes." & vbCrLf & _
                  "� Desea guardar la Regi�n ? ", vbQuestion + vbYesNo, Me.Caption) = vbNo Then
                  
            Exit Sub
        End If
        
        If Fnt_Grabar Then
            ' Call Sub_CargarDatos
        End If
    End If

    Call Sub_EsperaVentana_ComunaCiudad(fKey, cNewEntidad)
    
End Sub

Private Sub Sub_EsperaVentana_ComunaCiudad(ByVal pkey, pIdComunaCiudad As String)
    Dim lForm               As Frm_Comuna_Ciudad
    Dim lNombre             As String
    Dim lNom_Form           As String
    Dim lFila               As Long
    
    Dim sDescComunaCiudad   As String
    Dim idRegion            As String
    
    If pIdComunaCiudad = cNewEntidad Then
        sDescComunaCiudad = ""
    Else
        sDescComunaCiudad = GetCell(grdComunaCiudad, grdComunaCiudad.Row, "DSC_COMUNA_CIUDAD")
    End If
    
    idRegion = pkey
    
    Me.Enabled = False
    
    
    lNom_Form = "Frm_Comuna_Ciudad"
    
    If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
        lNombre = Me.Name
    
        Set lForm = New Frm_Comuna_Ciudad
    
        Call lForm.Fnt_Modificar(pId_Comuna_Ciudad:=pIdComunaCiudad _
                        , pid_region:=idRegion _
                        , pDsc_Comuna_Ciudad:=sDescComunaCiudad _
                        , pDsc_region:=Txt_Region.Text _
                        , pId_Comuna_Ciudad_new:=lId_Comuna_Ciudad_new _
                        , pDsc_Comuna_Ciudad_new:=lDsc_Comuna_Ciudad_new _
                        , pCod_Arbol_Sistema:=fCod_Arbol_Sistema)

        Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
            DoEvents
        Loop
    
        If Fnt_ExisteVentana(lNombre) Then
            Call Sub_CargarDatos
        End If
        
    End If
    
    Me.Enabled = True
End Sub

'Private Sub Sub_Sp_IU_Grilla(lFila As Long, _
'                                pIdComunaCiudad As String, _
'                                pDscComunaCiudad As String, _
'                                pIdRegion As String, _
'                                pDscRegion As String)
'
'  SetCell grdComunaCiudad, lFila, "colum_pk", pIdComunaCiudad
'  SetCell grdComunaCiudad, lFila, "dsc_comuna_ciudad", pDscComunaCiudad
'  SetCell grdComunaCiudad, lFila, "id_region", pIdRegion
'  SetCell grdComunaCiudad, lFila, "dsc_region", pDscRegion
'End Sub
'

'Private Function Fnt_EliminarTrader(pPK) As Boolean
'Dim lcTrader As Class_Traders
'
'On Error GoTo ErrProcedure
'
'  Fnt_EliminarTrader = False
'
'  Set lcTrader = New Class_Traders
'  With lcTrader
'    .Campo("id_trader").Valor = pPK
'    If Not .Borrar Then
'      Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Problemas al eliminar el trader.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      GoTo ExitProcedure
'    End If
'  End With
'
'  Fnt_EliminarTrader = True
'
'ErrProcedure:
'  If Not Err.Number = 0 Then
'    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al eliminar el trader.", Err.Description)
'    GoTo ExitProcedure
'    Resume
'  End If
'
'ExitProcedure:
'  Set lcTrader = Nothing
'End Function

'Private Sub Sub_EsperaVentana_Trader(ByVal pKey)
'Dim lForm As Frm_Trader
'Dim lNombre As String
'Dim lNom_Form As String
'Dim existe_instrumento As Boolean
'Dim lFila As Long
'
'On Error GoTo ErrProcedure
'
'  'primero que nada grabo las contrapartes, si el fkey es igual a sin entidad
'  If fKey = cNewEntidad Then
'    If Not MsgBox("Primero debe grabar antes acceder a esta opci�n." & vbCr & vbCr & _
'                  "�Desea grabar?", vbYesNo, Me.Caption) = vbYes Then
'      GoTo ExitProcedure
'    End If
'
'    If Not Fnt_Grabar Then
'      GoTo ExitProcedure
'    End If
'  End If
'
'
'  Me.Enabled = False
'  existe_instrumento = False
'  lNom_Form = "Frm_Trader"
'
'  If Not Fnt_ExisteVentanaKey(lNom_Form, pKey) Then
'    lNombre = Me.Name
'
'    Set lForm = New Frm_Trader
'    Call lForm.Fnt_Modificar(pId_Trader:=pKey _
'                           , pId_Contraparte:=fKey _
'                           , pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
'
'    Do While Fnt_ExisteVentanaKey(lNom_Form, pKey)
'      DoEvents
'    Loop
'
'    Unload lForm
'
'    Call Sub_CargarTraders
'  End If
'
'ErrProcedure:
'  If Not Err.Number = 0 Then
'    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en manejar el Trader.", Err.Description)
'    GoTo ExitProcedure
'    Resume
'  End If
'
'ExitProcedure:
'  Me.Enabled = True
'End Sub

'Private Sub Sub_CargarTraders()
'    Dim lcTrader  As Class_Traders
'    Dim lReg      As hFields
'    '----------------------------------------------------
'    Dim lLinea    As Long
'    Dim lID       As String
'
'    If Grilla_Traders.Row > 0 Then
'        lID = GetCell(Grilla_Traders, Grilla_Traders.Row, "colum_pk")
'    Else
'        lID = ""
'    End If
'
'    Grilla_Traders.Rows = 1
'
'    Set lcTrader = New Class_Traders
'    With lcTrader
'        .Campo("ID_CONTRAPARTE").Valor = fKey
'        If .Buscar Then
'            For Each lReg In .Cursor
'                lLinea = Grilla_Traders.Rows
'                Grilla_Traders.AddItem ""
'
'                SetCell Grilla_Traders, lLinea, "colum_pk", lReg("ID_TRADER").Value
'                SetCell Grilla_Traders, lLinea, "dsc_trader", lReg("DSC_TRADER").Value
'                SetCell Grilla_Traders, lLinea, "email", lReg("EMAIL_trader").Value
'            Next
'        Else
'            Call Fnt_MsgError(.SubTipo_LOG, _
'                                "Problemas en cargar Traders.", _
'                                .ErrMsg, _
'                                pConLog:=True)
'        End If
'    End With
'
'  With Grilla_Traders
'    If Not lID = "" Then
'      .Row = .FindRow(lID, , .ColIndex("colum_pk"))
'      If Not .Row = cNewEntidad Then
'        Call .ShowCell(.Row, .ColIndex("colum_pk"))
'      End If
'    End If
'  End With
'End Sub
'Private Sub Toolbar_Trader_ButtonClick(ByVal Button As MSComctlLib.Button)
'  Me.SetFocus
'  DoEvents
'
'  Select Case Button.Key
'    Case "ADD"
'      Call Sub_EsperaVentana_Trader(cNewEntidad)
'    Case "DEL"
'      If Grilla_Traders.Row > 0 Then
'        If MsgBox("�Esta seguro de eliminar el Trader?", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
'          If Fnt_EliminarTrader(GetCell(Grilla_Traders, Grilla_Traders.Row, "colum_pk")) Then
'            Call Sub_CargarTraders
'          End If
'        End If
'      End If
'  End Select
'End Sub


'Private Sub Grilla_Traders_AfterEdit(ByVal Row As Long, ByVal Col As Long)
'  Select Case Col
'    Case Grilla_Traders.ColIndex("email")
'      If Not Fnt_VerificaEMAIL(Grilla_Traders.Text) Then
'        With Grilla_Traders
'          .Text = .EditText
'        End With
'      End If
'  End Select
'End Sub

'Private Sub Grilla_Traders_DblClick()
'  If Grilla_Traders.Row > 0 Then
'    Call Sub_EsperaVentana_Trader(GetCell(Grilla_Traders, Grilla_Traders.Row, "colum_pk"))
'  End If
'End Sub
