VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Roles 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Roles"
   ClientHeight    =   4965
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9030
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4965
   ScaleWidth      =   9030
   Begin VB.Frame Frame_Natural 
      Height          =   4455
      Left            =   60
      TabIndex        =   2
      Top             =   390
      Width           =   8955
      Begin hControl2.hTextLabel Txt_Nombres 
         Height          =   315
         Left            =   180
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   300
         Width           =   4680
         _ExtentX        =   8255
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Descripci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   99
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Permisos 
         Height          =   3555
         Left            =   180
         TabIndex        =   4
         Top             =   720
         Width           =   8625
         _cx             =   15214
         _cy             =   6271
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   0
         GridLines       =   2
         GridLinesFixed  =   4
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   6
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Roles.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   1
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   4
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   1
         OwnerDraw       =   6
         Editable        =   2
         ShowComboButton =   2
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   6
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9030
      _ExtentX        =   15928
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n."
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Roles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'=========================================================================================================
'====  Formulario         : Frm_Roles
'====  Descripci�n        : Este formulario permite Agregar, Modificar y Consultar los
'====                       permisos asociados a los distintos roles.
'====  Desarrollado por   : Hans Muller R.
'====  Fecha Creaci�n     : 24/07/2006
'====  Fecha Modificaci�n :
'====  Base de Datos      : Oracle 8i     Usuario : CSBPI
'=========================================================================================================
Option Explicit

Public fKey As String

Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pId_Roles, pCod_Arbol_Sistema)
  fKey = pId_Roles

'-----------------------------------------------------------------------
'--Setea los controles dependiendo de los permisos
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
'-----------------------------------------------------------------------
  
  Call Sub_CargarDatos

  If Trim(Txt_Nombres.Text) = "" Then
    Me.Caption = "Ingreso de Roles"
  Else
    Me.Caption = "Modificaci�n Roles: " & Txt_Nombres.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Permisos_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Col < 4 Then
    Cancel = True
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Txt_Nombres.Text = ""
        Call Sub_CargaForm
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lLinea As Long
Dim lResult As Boolean
Dim lTipo_Permiso As String
Dim lcRoles As Class_Roles
Dim lcRel_Roles_Arbol_Sistema As Class_Rel_Roles_Arbol_Sistema

  Call gDB.IniciarTransaccion

  lResult = True

  If Not Fnt_ValidarDatos Then
    lResult = False
    GoTo ErrProcedure
  End If

  Rem Grabando el Rol
  Set lcRoles = New Class_Roles
  With lcRoles
    .Campo("Id_Rol").Valor = fKey
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    .Campo("Dsc_Rol").Valor = Txt_Nombres.Text
    If .Guardar Then
      ' ID DEL ROL
      fKey = .Campo("Id_Rol").Valor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      lResult = False
      GoTo ErrProcedure
    End If
  End With
  Set lcRoles = Nothing
    
  '------ ELIMINA REGISTROS EXISTENTES DE LA RELACI�N REL_ROLES_ARBOL_SISTEMA --------
  Set lcRel_Roles_Arbol_Sistema = New Class_Rel_Roles_Arbol_Sistema
  With lcRel_Roles_Arbol_Sistema
    .Campo("Id_Rol").Valor = fKey
    If Not .Borrar Then
      MsgBox "Problemas al grabar la relaci�n entre el Rol y el Arbol del Sistema." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      lResult = False
      GoTo ErrProcedure
    End If
  End With
  Set lcRel_Roles_Arbol_Sistema = Nothing
      
  '-----------AGREGA REGISTROS A LA RELACI�N REL_ROLES_ARBOL_SISTEMA -------------------
  For lLinea = 1 To Grilla_Permisos.Rows - 1
    Grilla_Permisos.Row = lLinea
    Grilla_Permisos.Col = 4
    If Grilla_Permisos.Value = vbTrue Then
      Grilla_Permisos.Col = 5
      lTipo_Permiso = UCase(Mid(Grilla_Permisos.Text, 1, 1))
      
      If lTipo_Permiso = "S" Then lTipo_Permiso = "L"
      
      Grilla_Permisos.Col = 1
      
      Set lcRel_Roles_Arbol_Sistema = New Class_Rel_Roles_Arbol_Sistema
      With lcRel_Roles_Arbol_Sistema
        .Campo("Id_Rol").Valor = fKey
        .Campo("Id_Arbol_Sistema").Valor = Grilla_Permisos.Text
        .Campo("Flg_Tipo_Permiso").Valor = lTipo_Permiso
        If Not .Guardar Then
          MsgBox "Problemas al grabar la relaci�n entre el Rol y el Arbol del Sistema." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
          lResult = False
          GoTo ErrProcedure
        End If
      End With
      Set lcRel_Roles_Arbol_Sistema = Nothing
    End If
  Next lLinea
    
ErrProcedure:
  If Err Then
    lResult = False
  End If

  If lResult Then
    Call gDB.CommitTransaccion
  Else
    Call gDB.RollbackTransaccion
  End If
  
  Fnt_Grabar = lResult
  
  Set lcRel_Roles_Arbol_Sistema = Nothing
  Set lcRoles = Nothing
  
End Function

Private Sub Sub_CargaForm()
Dim lcArbol_Sistema As Class_Arbol_Sistema
Dim lPadre As hFields

  Call Sub_FormControl_Color(Me.Controls)

  Rem inicializa la grilla
  With Grilla_Permisos
    Rem Disposici�n
    .Rows = 1
    .FixedCols = 0
    .ExtendLastCol = True
    .ColAlignment(-1) = flexAlignLeftTop
    .Editable = flexEDKbdMouse

    Rem Contorno
    .OutlineCol = 0
    .OutlineBar = flexOutlineBarSimpleLeaf
    .MergeCells = flexMergeOutline

    Rem Otras
    .AllowUserResizing = flexResizeColumns
    .AllowSelection = False
    .GridLines = flexGridFlatVert
  End With

  Grilla_Permisos.Redraw = flexRDNone
  
  Set lcArbol_Sistema = New Class_Arbol_Sistema
  With lcArbol_Sistema
    '------ BUSCA PRIMER NIVEL DEL ARBOL -------------------------
    '------ LLENADO DE ARBOL DE SISTEMA  -------------------------
    If .Buscar_Padres Then
      For Each lPadre In .Cursor
        Grilla_Permisos.AddItem lPadre("Dsc_Arbol_Sistema").Value
        Grilla_Permisos.IsSubtotal(Grilla_Permisos.Rows - 1) = True
        Grilla_Permisos.RowOutlineLevel(Grilla_Permisos.Rows - 1) = 0
        
        If Not Fnt_BuscaHijos(lPadre("Id_Arbol_Sistema").Value, 1) Then
          Exit For
        End If
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcArbol_Sistema = Nothing
    
  Grilla_Permisos.Redraw = flexRDBuffered

End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lId_Arbol As Long
Dim lcRoles As Class_Roles
Dim lcRel_Roles_Arbol_Sistema As Class_Rel_Roles_Arbol_Sistema

  Load Me
  
  Set lcRoles = New Class_Roles
  With lcRoles
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    .Campo("Id_Rol").Valor = fKey
    If .Buscar Then
      If .Cursor.Count > 0 Then
        Txt_Nombres.Text = NVL(.Cursor(1)("Dsc_Rol").Value, "")
      End If
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcRoles = Nothing
  
  Set lcRel_Roles_Arbol_Sistema = New Class_Rel_Roles_Arbol_Sistema
  With lcRel_Roles_Arbol_Sistema
    .Campo("Id_Rol").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        lId_Arbol = lReg("Id_Arbol_Sistema").Value
        For lLinea = 1 To Grilla_Permisos.Rows - 1
          Grilla_Permisos.Row = lLinea
          Grilla_Permisos.Col = 1
          If Not Trim(Grilla_Permisos.Text) = "" Then
            If Grilla_Permisos.Text = lId_Arbol Then
              Grilla_Permisos.Col = 4
              Grilla_Permisos.CellChecked = True
              Grilla_Permisos.Col = 5
              If lReg("Flg_Tipo_Permiso").Value = "C" Then
                Grilla_Permisos.Text = "Control Total"
              Else
                Grilla_Permisos.Text = "Solo Lectura"
              End If
            End If
          End If
        Next lLinea
      Next
    End If
  End With
  Set lcRel_Roles_Arbol_Sistema = Nothing
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
   Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function

Private Function Fnt_BuscaHijos(Id_Padre As Long, Nivel As Integer) As Boolean
Dim lReg As hFields
Dim lcArbol_Sistema As Class_Arbol_Sistema
  
  Fnt_BuscaHijos = True
  
  Set lcArbol_Sistema = New Class_Arbol_Sistema
  With lcArbol_Sistema
    '-------------- BUSCA LOS HIJOS DE UN NODO ---------------
    .Campo("Id_Padre").Valor = Id_Padre
    If .Buscar_Hijos Then
      For Each lReg In .Cursor
        If Trim(lReg("Tipo").Value) = "N" Then
          ' ---- SI ES HOJA TERMINA RECURSION ------
          Grilla_Permisos.AddItem lReg("Dsc_Arbol_Sistema").Value & vbTab & lReg("Id_Arbol_Sistema").Value & vbTab & IIf(lReg("WEB"), "Si", "No") & vbTab & IIf(lReg("EXE"), "Si", "No") & vbTab & "FALSE" & vbTab & "Control Total"
          
          Grilla_Permisos.IsSubtotal(Grilla_Permisos.Rows - 1) = True
          Grilla_Permisos.RowOutlineLevel(Grilla_Permisos.Rows - 1) = Nivel
        Else
          ' ---- SI ES PADRE CONTINUA RECURSION ------
          Grilla_Permisos.AddItem lReg("Dsc_Arbol_Sistema").Value
          
          Grilla_Permisos.IsSubtotal(Grilla_Permisos.Rows - 1) = True
          Grilla_Permisos.RowOutlineLevel(Grilla_Permisos.Rows - 1) = Nivel
          
          If Not Fnt_BuscaHijos(lReg("Id_Arbol_Sistema").Value, Nivel + 1) Then
            Exit For
          End If
        End If
      Next
    Else
      Fnt_BuscaHijos = False
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcArbol_Sistema = Nothing
  
End Function
