VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Direccion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Direcci�n"
   ClientHeight    =   3495
   ClientLeft      =   2100
   ClientTop       =   3045
   ClientWidth     =   6375
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3495
   ScaleWidth      =   6375
   Begin VB.Frame Frame1 
      Caption         =   "Datos Direcci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3015
      Left            =   60
      TabIndex        =   10
      Top             =   420
      Width           =   6225
      Begin hControl2.hTextLabel Txt_Direccion 
         Height          =   315
         Left            =   240
         TabIndex        =   2
         Tag             =   "OBLI"
         Top             =   600
         Width           =   5685
         _ExtentX        =   10028
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Direcci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   149
      End
      Begin hControl2.hTextLabel Txt_Fax 
         Height          =   315
         Left            =   3120
         TabIndex        =   7
         Top             =   2160
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Fax"
         Text            =   ""
         BackColorTxt    =   16777215
         BackColorTxt    =   16777215
         MaxLength       =   49
      End
      Begin hControl2.hTextLabel Txt_Fono 
         Height          =   315
         Left            =   240
         TabIndex        =   6
         Top             =   2160
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Tel�fono"
         Text            =   ""
         BackColorTxt    =   16777215
         BackColorTxt    =   16777215
         MaxLength       =   49
      End
      Begin hControl2.hTextLabel Txt_Nombre 
         Height          =   315
         Left            =   240
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   210
         Width           =   5685
         _ExtentX        =   10028
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Cliente"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         MaxLength       =   149
      End
      Begin TrueDBList80.TDBCombo Cmb_Pais 
         Height          =   345
         Left            =   1545
         TabIndex        =   3
         Tag             =   "OBLI=S;CAPTION=Pa�s"
         Top             =   990
         Width           =   4395
         _ExtentX        =   7752
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Direccion.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo cmb_ComunaCiudad 
         Height          =   345
         Left            =   1545
         TabIndex        =   5
         Tag             =   "OBLI=S;CAPTION=Comuna/Ciudad"
         Top             =   1770
         Width           =   4395
         _ExtentX        =   7752
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Direccion.frx":00AA
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo cmb_TipoDireccion 
         Height          =   345
         Left            =   1545
         TabIndex        =   8
         Tag             =   "OBLI=S;CAPTION=Tipo Direcci�n"
         Top             =   2520
         Width           =   4395
         _ExtentX        =   7752
         _ExtentY        =   609
         _LayoutType     =   4
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=0,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=MS Sans Serif"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Direccion.frx":0154
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo cmb_Region 
         Height          =   345
         Left            =   1545
         TabIndex        =   4
         Tag             =   "OBLI=S;CAPTION=Regi�n"
         Top             =   1375
         Width           =   4395
         _ExtentX        =   7752
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Direccion.frx":01FE
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label4 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Regi�n"
         Height          =   345
         Left            =   240
         TabIndex        =   14
         Top             =   1365
         Width           =   1275
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tipo Direcci�n"
         Height          =   345
         Left            =   240
         TabIndex        =   13
         Top             =   2550
         Width           =   1275
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Comuna/Ciudad"
         Height          =   345
         Left            =   240
         TabIndex        =   12
         Top             =   1785
         Width           =   1275
      End
      Begin VB.Label Label3 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Pa�s"
         Height          =   345
         Left            =   240
         TabIndex        =   11
         Top             =   990
         Width           =   1275
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   9
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Direccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey         As String
Public fId_Cliente  As String

Rem ----------------------------------------------------------------
Rem 30/01/2009. Cambia el orden para traer las comunas. Primero
Rem             comunas luego region.
Rem ----------------------------------------------------------------

Private Sub Cmb_Pais_ItemChange()
    Dim fIdPais As String
    
    fIdPais = Fnt_ComboSelected_KEY(Cmb_Pais)
    
    Call Sub_CargaCombo_ComunaCiudad(cmb_ComunaCiudad, pCodPais:=fIdPais)   'Agregado por MMA 30/01/2009
    Call Sub_CargaCombo_Regiones(cmb_Region, fIdPais)
    
End Sub

Private Sub cmb_Region_itemChange()
    Dim fIdRegion   As String
    fIdRegion = Fnt_ComboSelected_KEY(cmb_Region)
    
    Call Sub_CargaCombo_ComunaCiudad(cmb_ComunaCiudad, pIdRegion:=fIdRegion)    'Modificado por MMA 30/01/2009
End Sub
'Agregado por MMA 30/01/2009
Private Sub cmb_ComunaCiudad_ItemChange()
Dim fIdComunaCiudad As String
Dim fIdRegion As Integer

    fIdComunaCiudad = Fnt_ComboSelected_KEY(cmb_ComunaCiudad)
    fIdRegion = Fnt_ObtieneNroRegion(cmb_ComunaCiudad.Text)
    Call Sub_ComboSelectedItem(cmb_Region, fIdRegion)

End Sub

Private Sub Form_Load()
    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
                .Buttons("SAVE").Image = cBoton_Grabar
                .Buttons("EXIT").Image = cBoton_Salir
                .Buttons("REFRESH").Image = cBoton_Original
    End With
    
    Call Sub_CargaForm
End Sub

Public Function Fnt_Modificar(ByVal pId_Cliente As String, ByVal pId_Direccion As String)
    fKey = pId_Direccion
    fId_Cliente = pId_Cliente
    
    Call Sub_CargarDatos
    
    If fKey = cNewEntidad Then
        Me.Caption = "Ingreso de Direcci�n"
    Else
        Me.Caption = "Modifica Direcci�n: " & Txt_Nombre.Text
    End If
    
    Me.Top = 1
    Me.Left = 1
    
    Me.Show
    
End Function

Private Sub Form_Resize()
    Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents

    Select Case Button.Key
        Case "SAVE"
            If Fnt_Grabar Then
                Unload Me
            End If
        Case "EXIT"
            Unload Me
        Case "REFRESH"
           If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & _
                     "�Volver al Original?" _
                     , vbYesNo + vbQuestion _
                     , Me.Caption) = vbYes Then
                Call Sub_CargarDatos
           End If
    End Select
End Sub

Private Function Fnt_Grabar() As Boolean
    Dim lcDireccion     As Object
    '------------------------------------------------------
    Dim lId_Estado As String
    Dim lSexo As String
    Dim LEstadoCivil As String
    Dim lvar_tmp As String

    Fnt_Grabar = True

    If Not Fnt_ValidarDatos Then
        Fnt_Grabar = False
        Exit Function
    End If

    Set lcDireccion = Fnt_CreateObject(cDLL_Direccion_Cliente)
    
    With lcDireccion
        Set .gDB = gDB
        .Campo("ID_DIRECCION").Valor = fKey
        .Campo("ID_CLIENTE").Valor = fId_Cliente
        .Campo("DIRECCION").Valor = Txt_Direccion.Text
        .Campo("FONO").Valor = Txt_Fono.Text
        .Campo("FAX").Valor = NVL(Txt_Fax.Text, "")
        .Campo("ID_COMUNA_CIUDAD").Valor = Fnt_ComboSelected_KEY(cmb_ComunaCiudad)
        .Campo("COD_TIPO_DIRECCION").Valor = Fnt_ComboSelected_KEY(cmb_TipoDireccion)
        
        If Not .Guardar Then
            MsgBox "Problemas en grabar el Direccion." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            Fnt_Grabar = False
        Else
            fKey = .Campo("ID_DIRECCION").Valor
        End If
        
    End With

    Set lcDireccion = Nothing
End Function

Private Sub Sub_CargaForm()
    Dim lReg As hFields
End Sub

Private Sub Sub_CargarDatos()
    '------------------------------------------------------
    Dim lcDireccion     As Object
    '------------------------------------------------------
    Dim lReg            As hCollection.hFields
    Dim lLinea          As Long
    
    Dim sIdPais         As String
    Dim sIDRegion       As String
    Dim sIdComunaCiudad As String
    Dim sCodTipoDomicilio As String
    
    
    Load Me
    Set lcDireccion = Fnt_CreateObject(cDLL_Direccion_Cliente)

    Call Sub_CargarCliente
    
    Call Sub_CargarTipoDomicilio(cmb_TipoDireccion)
    
    Call Sub_CargaCombo_Paises(Cmb_Pais)
    Call Sub_ComboSelectedItem(Cmb_Pais, gcPais_Chile)  'Agregado por MMA 30/01/2009
    Call Cmb_Pais_ItemChange
   
    
    With lcDireccion
        Set .gDB = gDB
        .Campo("ID_DIRECCION").Valor = fKey
        .Campo("ID_CLIENTE").Valor = fId_Cliente

        If .Buscar Then
            For Each lReg In .Cursor
                Txt_Direccion.Text = DLL_COMUN.NVL(lReg("direccion").Value, "")
                Txt_Fono.Text = DLL_COMUN.NVL(lReg("fono").Value, "")
                Txt_Fax.Text = DLL_COMUN.NVL(lReg("fax").Value, "")
                
                
                sIdPais = DLL_COMUN.NVL(lReg("cod_pais").Value, "")
                sIDRegion = DLL_COMUN.NVL(lReg("id_region").Value, "")
                sIdComunaCiudad = DLL_COMUN.NVL(lReg("ID_COMUNA_CIUDAD").Value, "")
                sCodTipoDomicilio = DLL_COMUN.NVL(lReg("COD_TIPO_DIRECCION").Value, "")
                
                Call Sub_CargaCombo_Regiones(cmb_Region, sIdPais)
                Call Sub_CargaCombo_ComunaCiudad(cmb_ComunaCiudad, sIDRegion)
                
                Call Sub_ComboSelectedItem(Cmb_Pais, sIdPais)
                Call Sub_ComboSelectedItem(cmb_Region, sIDRegion)
                Call Sub_ComboSelectedItem(cmb_ComunaCiudad, sIdComunaCiudad)
                Call Sub_ComboSelectedItem(cmb_TipoDireccion, sCodTipoDomicilio)
                
            Next
        Else
            MsgBox "Problemas en cargar el Cliente." & vbCr & vbCr & _
                    .ErrMsg, _
                    vbCritical, _
                    Me.Caption
        End If
    End With

    Set lcDireccion = Nothing
End Sub

Private Sub Sub_CargarCliente()
    Dim lcClientes      As Object
    '------------------------------------------------------
    Dim lReg            As hCollection.hFields
    Dim lLinea          As Long
    
    Dim sIdPais         As String
    Dim sIDRegion       As String
    Dim sIdComunaCiudad As String
    Dim sCodTipoDomicilio As String
    
    Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
    
    With lcClientes
        Set .gDB = gDB
        .Campo("id_Cliente").Valor = fId_Cliente
        
        If .Buscar(True) Then
            Txt_Nombre.Text = NVL(.Cursor(1)("nombre_cliente").Value, "")
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
        End If
    End With
    Set lcClientes = Nothing
End Sub

Private Function Fnt_ValidarDatos() As Boolean
    Dim lResult As Boolean
    lResult = Fnt_Form_Validar(Me.Controls)
    Fnt_ValidarDatos = lResult
  
End Function

Private Sub Txt_Nombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Direccion_KeyPress(KeyAscii As Integer)
    KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Sub_CargarTipoDomicilio(pCombo As TDBCombo)
    Dim oTipoDireccion  As Class_Tipos_Direcciones
    Dim lCampoPK        As String
    Dim lReg            As hCollection.hFields
    Dim lTexto As String

    With pCombo
        .Text = ""
        .ClearFields
        .Clear
        .EmptyRows = True
        .Font.Charset = 0

        Call .Columns.Remove(1)

        .Columns(0).ValueItems.Clear
        .Columns(0).ValueItems.Translate = False

        lCampoPK = "COD_TIPO_DIRECCION"

        Set oTipoDireccion = New Class_Tipos_Direcciones

        If oTipoDireccion.Buscar Then
            For Each lReg In oTipoDireccion.Cursor
                lTexto = lReg("DSC_TIPO_DIRECCION").Value

                If Not gRelogDB Is Nothing Then
                    gRelogDB.AvanzaRelog
                End If

                Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(lCampoPK).Value, lTexto))

                Call .AddItem(lTexto)
            Next
        Else
            Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo Tipo Direccion.", oTipoDireccion.ErrMsg, pConLog:=True)
        End If

    End With
    Set oTipoDireccion = Nothing
End Sub

Private Sub Sub_CargaCombo_Regiones(pCombo As TDBCombo, ByVal pIdPais As String)
    Dim oRegion As Class_Regiones
    Dim lCampoPK As String
    Dim lReg            As hCollection.hFields
    Dim lTexto As String

    With pCombo
        .Text = ""
        .ClearFields
        .Clear
        .EmptyRows = True
        .Font.Charset = 0

        Call .Columns.Remove(1)

        .Columns(0).ValueItems.Clear
        .Columns(0).ValueItems.Translate = False

        lCampoPK = "id_Region"

        Set oRegion = New Class_Regiones

        oRegion.Campo("cod_pais").Valor = pIdPais

        If oRegion.Buscar Then
            For Each lReg In oRegion.Cursor
                lTexto = lReg("Dsc_Region").Value

                If Not gRelogDB Is Nothing Then
                    gRelogDB.AvanzaRelog
                End If

                Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(lCampoPK).Value, lTexto))

                Call .AddItem(lTexto)
            Next
        Else
            Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo.", oRegion.ErrMsg, pConLog:=True)
        End If

    End With

    Set oRegion = Nothing
End Sub

Private Sub Sub_CargaCombo_ComunaCiudad(pCombo As TDBCombo, Optional pIdRegion As String = "", _
                                        Optional pCodPais As String = "")
    Dim oComunaCiudad As New Class_ComunasRegiones 'Class_Comuna_Ciudad  'Modificado por MMA 30/01/2009
    Dim lCampoPK As String
    Dim lReg            As hCollection.hFields
    Dim lTexto As String

    With pCombo
        .Text = ""
        .ClearFields
        .Clear
        .EmptyRows = True
        .Font.Charset = 0

        Call .Columns.Remove(1)

        .Columns(0).ValueItems.Clear
        .Columns(0).ValueItems.Translate = False

        lCampoPK = "id_comuna_Ciudad"

        'Set oComunaCiudad = New Class_Comuna_Ciudad    'Modificado por MMA 30/01/2009
        Set oComunaCiudad = New Class_ComunasRegiones

'        oComunaCiudad.Campo("id_region").Valor = pIdRegion
        'Modificado por MMA. 30/01/2009
        If pIdRegion <> "" Then
            oComunaCiudad.Campo("id_region").Valor = pIdRegion
        End If
'        If pCodPais <> "" Then
'            oComunaCiudad.Campo("cod_pais").Valor = pCodPais
'        End If

        If oComunaCiudad.Buscar Then
            For Each lReg In oComunaCiudad.Cursor
                'lTexto = lReg("Dsc_Comuna_Ciudad").Value
                lTexto = lReg("Dsc_Comuna_Ciudad").Value & " (" & Fnt_RegionRomano(lReg("id_region").Value) & ")"    'Modificado por MMA 30/01/2009

                If Not gRelogDB Is Nothing Then
                    gRelogDB.AvanzaRelog
                End If

                Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem(lReg(lCampoPK).Value, lTexto))

                Call .AddItem(lTexto)
            Next
        Else
            Call Fnt_MsgError(eLS_ErrSystem, "No se puede llenar la combo de Comunas/Ciudades.", oComunaCiudad.ErrMsg, pConLog:=True)
        End If

    End With

    Set oComunaCiudad = Nothing
End Sub

Private Function Fnt_RegionRomano(pid_region As Integer) As String
    Select Case pid_region
        Case 1
            Fnt_RegionRomano = "I"
        Case 2
            Fnt_RegionRomano = "II"
        Case 3
            Fnt_RegionRomano = "III"
        Case 4
            Fnt_RegionRomano = "IV"
        Case 5
            Fnt_RegionRomano = "V"
        Case 6
            Fnt_RegionRomano = "VI"
        Case 7
            Fnt_RegionRomano = "VII"
        Case 8
            Fnt_RegionRomano = "VIII"
        Case 9
            Fnt_RegionRomano = "IX"
        Case 10
            Fnt_RegionRomano = "X"
        Case 11
            Fnt_RegionRomano = "XI"
        Case 12
            Fnt_RegionRomano = "XII"
        Case 13
            Fnt_RegionRomano = "RM"
        Case 14
            Fnt_RegionRomano = "XIV"
        Case 15
            Fnt_RegionRomano = "XV"
    End Select
End Function

Private Function Fnt_ObtieneNroRegion(sTexto As String) As Integer
Dim sRomano As String
Dim ipos1 As Integer
Dim ipos2 As Integer
    ipos1 = InStr(1, sTexto, "(")
    If ipos1 > 0 Then
        ipos2 = InStr(ipos1, sTexto, ")")
        sRomano = Mid(sTexto, ipos1 + 1, ipos2 - (ipos1 + 1))
    
        Select Case sRomano
            Case "I"
                Fnt_ObtieneNroRegion = 1
            Case "II"
                Fnt_ObtieneNroRegion = 2
            Case "III"
                Fnt_ObtieneNroRegion = 3
            Case "IV"
                Fnt_ObtieneNroRegion = 4
            Case "V"
                Fnt_ObtieneNroRegion = 5
            Case "VI"
                Fnt_ObtieneNroRegion = 6
            Case "VII"
                Fnt_ObtieneNroRegion = 7
            Case "VIII"
                Fnt_ObtieneNroRegion = 8
            Case "IX"
                Fnt_ObtieneNroRegion = 9
            Case "X"
                Fnt_ObtieneNroRegion = 10
            Case "XI"
                Fnt_ObtieneNroRegion = 11
            Case "XII"
                Fnt_ObtieneNroRegion = 12
            Case "RM"
                Fnt_ObtieneNroRegion = 13
            Case "XIV"
                Fnt_ObtieneNroRegion = 14
            Case "XV"
                Fnt_ObtieneNroRegion = 15
        End Select
    End If
End Function

