VERSION 5.00
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Ingresos_Mensual_Conceptos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Conceptos de Ingreso Mensual"
   ClientHeight    =   1500
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5925
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1500
   ScaleWidth      =   5925
   Begin VB.Frame Frame1 
      Height          =   945
      Left            =   90
      TabIndex        =   2
      Top             =   420
      Width           =   5715
      Begin hControl2.hTextLabel Txt_Concepto 
         Height          =   315
         Left            =   210
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   330
         Width           =   5250
         _ExtentX        =   9260
         _ExtentY        =   556
         LabelWidth      =   900
         TextMinWidth    =   1200
         Caption         =   "Concepto"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   59
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5925
      _ExtentX        =   10451
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Ingresos_Mensual_Conceptos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fFlg_Tipo_Permiso As String

Private Sub Form_Load()

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SAVE").Image = cBoton_Grabar
    .Buttons("EXIT").Image = cBoton_Salir
    .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm

End Sub

Public Function Fnt_Modificar(pId_Ing_Msn_C_oncepto, pCod_Arbol_Sistema)
  fKey = pId_Ing_Msn_C_oncepto
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Call Sub_CargarDatos
    
  If fKey = cNewEntidad Then
    Me.Caption = "Agregar Conceptos de Ingreso Mensual"
  Else
    Me.Caption = "Modificaci�n de Conceptos: " & Txt_Concepto.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
  
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lResult As Boolean
Dim lcIngr_Mens_Concepto As Class_Ingresos_Mensual_Concepto

  Call Sub_Bloquea_Puntero(Me)
   
  lResult = True

  If Not Fnt_ValidarDatos Then
    lResult = False
    GoTo ErrProcedure
  End If
  
  Set lcIngr_Mens_Concepto = New Class_Ingresos_Mensual_Concepto
  With lcIngr_Mens_Concepto
    .Campo("ID_ING_MSN_C_ONCEPTO").Valor = fKey
    .Campo("DSC_ING_MSN_C_ONCEPTO").Valor = Txt_Concepto.Text
        
    If Not .Guardar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas de guardar Concepto.", _
                        .ErrMsg, _
                        pConLog:=True)
      lResult = False
    End If
  End With
  
ErrProcedure:
  Fnt_Grabar = lResult
  
  Set lcIngr_Mens_Concepto = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Function

Private Sub Sub_CargaForm()
  Txt_Concepto.Text = ""
End Sub

Private Sub Sub_CargarDatos()
Dim lcIngr_Mens_Concepto As Class_Ingresos_Mensual_Concepto
  
  Txt_Concepto.Text = ""
  
  Set lcIngr_Mens_Concepto = New Class_Ingresos_Mensual_Concepto
  With lcIngr_Mens_Concepto
    .Campo("ID_ING_MSN_C_ONCEPTO").Valor = fKey
    If .Buscar Then
      If .Cursor.Count > 0 Then
        Txt_Concepto.Text = .Cursor(1).Fields("DSC_ING_MSN_C_ONCEPTO").Value
      End If
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Concepto.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcIngr_Mens_Concepto = Nothing
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function
