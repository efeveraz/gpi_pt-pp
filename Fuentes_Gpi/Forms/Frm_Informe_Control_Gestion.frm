VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Informe_Control_Gestion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de Control de Gesti�n"
   ClientHeight    =   8820
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7095
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8820
   ScaleWidth      =   7095
   Begin VB.Frame Frm_Cuentas 
      Caption         =   "Cuentas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2925
      Left            =   60
      TabIndex        =   34
      Top             =   2910
      Width           =   6945
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
         Height          =   2535
         Left            =   90
         TabIndex        =   35
         Top             =   270
         Width           =   6165
         _cx             =   10874
         _cy             =   4471
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   5
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Informe_Control_Gestion.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Chequeo 
         Height          =   660
         Left            =   6360
         TabIndex        =   36
         Top             =   540
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_ALL"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Selecciona todos los items"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_NOTHING"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Deselecciona todos los items"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar3 
            Height          =   255
            Left            =   9420
            TabIndex        =   37
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
   End
   Begin VB.Frame Frm_Propiedades_Cuentas 
      Caption         =   "Propiedades de la Cuenta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1455
      Left            =   60
      TabIndex        =   26
      Top             =   1440
      Width           =   6945
      Begin MSComctlLib.Toolbar Toolbar_Chequear_Propietario 
         Height          =   330
         Left            =   5520
         TabIndex        =   27
         Top             =   1020
         Width           =   1290
         _ExtentX        =   2275
         _ExtentY        =   582
         ButtonWidth     =   1958
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Chequear"
               Key             =   "CHK"
               Object.ToolTipText     =   "Busca las cuentas seg�n los filtros"
            EndProperty
         EndProperty
      End
      Begin TrueDBList80.TDBCombo Cmb_Asesor 
         Height          =   345
         Left            =   1380
         TabIndex        =   28
         Top             =   270
         Width           =   4065
         _ExtentX        =   7170
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Informe_Control_Gestion.frx":00D1
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Clientes 
         Height          =   345
         Left            =   1380
         TabIndex        =   29
         Top             =   630
         Width           =   4065
         _ExtentX        =   7170
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Informe_Control_Gestion.frx":017B
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Grupos_Cuentas 
         Height          =   345
         Left            =   1380
         TabIndex        =   30
         Top             =   990
         Width           =   4065
         _ExtentX        =   7170
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Informe_Control_Gestion.frx":0225
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label lbl_GruposCuentas 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Grupos Cuentas"
         Height          =   345
         Left            =   90
         TabIndex        =   33
         Top             =   990
         Width           =   1275
      End
      Begin VB.Label Lbl_Clientes 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Clientes"
         Height          =   345
         Left            =   90
         TabIndex        =   32
         Top             =   630
         Width           =   1275
      End
      Begin VB.Label Lbl_Asesor 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Asesor"
         Height          =   345
         Left            =   90
         TabIndex        =   31
         Top             =   270
         Width           =   1275
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Reportes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2925
      Left            =   60
      TabIndex        =   2
      Top             =   5820
      Width           =   6945
      Begin VB.CheckBox Chk_Todos 
         Caption         =   "Todos"
         Height          =   255
         Left            =   90
         TabIndex        =   25
         Top             =   270
         Width           =   2295
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Gr�ficos"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   -1  'True
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   195
         Index           =   15
         Left            =   3300
         TabIndex        =   24
         Tag             =   "GRAFICOS"
         Top             =   1845
         Width           =   2880
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Reporte de Flujos Patrimoniales"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   3300
         TabIndex        =   23
         Tag             =   "FUJOS_PATRIMONIALES"
         Top             =   1605
         Width           =   2880
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Gr�fico de Distribuci�n por Tipo de Activo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   3300
         TabIndex        =   22
         Tag             =   "GRAFICO_DISTRIB_TIPO_ACTIVO"
         Top             =   1350
         Width           =   3360
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Detalle de Clientes por Tipo de Instrumento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   3300
         TabIndex        =   21
         Tag             =   "DETALLE_CLIENTE_TIPO_INSTRUMENTO"
         Top             =   1095
         Width           =   3360
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Resumen Gesti�n BPI"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   3300
         TabIndex        =   19
         Tag             =   "GESTION_BPI"
         Top             =   855
         Width           =   2880
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Informe Semanal"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   -1  'True
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   10
         Left            =   3300
         TabIndex        =   18
         Tag             =   "SEMANAL"
         Top             =   570
         Width           =   2880
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Patrimonio Mensual por Cliente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   285
         TabIndex        =   17
         Tag             =   "PATRIMONIO_MES_CLIENTE"
         Top             =   2565
         Width           =   2730
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Totales por Agente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   285
         TabIndex        =   16
         Tag             =   "TOTALES_AGENTE"
         Top             =   2325
         Width           =   2730
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Resumen por Mes por Cliente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   285
         TabIndex        =   15
         Tag             =   "RESUMEN_MES_CLIENTE"
         Top             =   2070
         Width           =   2730
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Cuadro Contribuci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   285
         TabIndex        =   14
         Tag             =   "CUADRO_CONTRIBUCION"
         Top             =   1815
         Width           =   2730
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Individual"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   285
         TabIndex        =   13
         Tag             =   "INDIVIDUAL"
         Top             =   1575
         Width           =   2730
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "BPI"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   270
         TabIndex        =   12
         Tag             =   "BPI"
         Top             =   1320
         Width           =   2730
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Ingresos Acumulados Por Mes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   270
         TabIndex        =   11
         Tag             =   "INGRESOS_ACUMULADOSMES"
         Top             =   1065
         Width           =   2730
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Ingresos Acumulados Por Asesor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   285
         TabIndex        =   10
         Tag             =   "INGRESOS_ACUMULADOS_ASESOR"
         Top             =   825
         Width           =   2730
      End
      Begin VB.CheckBox Chk_Reporte 
         Caption         =   "Portada"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   285
         TabIndex        =   9
         Tag             =   "INFORME_PORTADA"
         Top             =   540
         Width           =   2730
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Fecha Inicial"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1035
      Left            =   60
      TabIndex        =   1
      Top             =   390
      Width           =   6945
      Begin MSComCtl2.DTPicker DTP_Fecha_Inicial 
         Height          =   345
         Left            =   4350
         TabIndex        =   5
         Top             =   240
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   609
         _Version        =   393216
         CustomFormat    =   "MMMM yyyy"
         Format          =   57802753
         CurrentDate     =   39153
      End
      Begin VB.OptionButton Rdb_Mes 
         Caption         =   "Mes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   600
         Width           =   1275
      End
      Begin VB.OptionButton rdb_RangoFecha 
         Caption         =   "Rango de Fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   300
         Width           =   1515
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Final 
         Height          =   345
         Left            =   4350
         TabIndex        =   8
         Top             =   600
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   609
         _Version        =   393216
         CustomFormat    =   "MMMM yyyy"
         Format          =   57802755
         CurrentDate     =   39153
      End
      Begin VB.Label Lbl_Fecha_Final 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Final"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3150
         TabIndex        =   7
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Lbl_Fecha_Inicial 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Inicial"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3150
         TabIndex        =   6
         Top             =   240
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7095
      _ExtentX        =   12515
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORTE"
            Object.ToolTipText     =   "Genera el Informe Control de Gesti�n"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpiar los Controles"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   6960
         TabIndex        =   20
         Top             =   0
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Informe_Control_Gestion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const cfPackage = "PKG_INFORME_CONTROL_GESTION"

Dim fFecha_Inicial As Date
Dim fFecha_Final As Date

Public fTotal_Reportes      As Integer
Public lPeriodo             As String
Public fFechaInicial        As Date
Public fFechaFinal          As Date
Public fCheck_Todos         As Boolean
Public fCheck_Reporte       As Boolean
Public fApp_Excel           As Excel.Application      ' Excel application
Public fLibro               As Excel.Workbook      ' Excel workbook

'-----------------------------------------------------------------
'Public fId_Moneda_Empresa   As String
Public fFormatoMoneda       As String
'-----------------------------------------------------------------

Private Sub Chk_Reporte_Click(Index As Integer)
    Dim lChequeados As Integer

    If Not fCheck_Todos Then
        fCheck_Reporte = True
        lChequeados = Fnt_Contar_ReportesChequeados
        Chk_Todos.Value = IIf(lChequeados = fTotal_Reportes, 1, 0)
        fCheck_Reporte = False
    End If

End Sub

Private Sub Chk_Todos_Click()
Dim lTodos As Boolean
Dim lReporte As Integer

  fCheck_Todos = True
  If Not fCheck_Reporte Then
      lTodos = IIf(Chk_Todos.Value = 0, False, True)
      For lReporte = 1 To fTotal_Reportes
          Chk_Reporte(lReporte).Value = IIf(lTodos, 1, 0)
      Next lReporte
  End If
  fCheck_Todos = False
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORTE").Image = cBoton_Excel
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
  End With
  
  With Toolbar_Chequear_Propietario
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("CHK").Image = cBoton_Agregar_Grilla
  End With
    
  With Toolbar_Chequeo
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
      .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
      .Appearance = ccFlat
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object

  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_FormControl_Color(Me.Controls)

  Grilla_Cuentas.Rows = 1
  
  Set lCierre = New Class_Verificaciones_Cierre
  fFecha_Inicial = Date
  fFecha_Final = Date
  DTP_Fecha_Inicial.Value = fFecha_Inicial
  DTP_Fecha_Final.Value = fFecha_Final
    
  fTotal_Reportes = Chk_Reporte.Count
  
  Rem Carga los combos con el primer elemento vac�o
  Call Sub_CargaCombo_Asesor(Cmb_Asesor, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKBLANCO)
  
  Call Sub_CargaCombo_Clientes(Cmb_Clientes, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Clientes, cCmbKBLANCO)
  
  Call Sub_CargaCombo_Grupos_Cuentas(Cmb_Grupos_Cuentas, pBlanco:=True)
  Call Sub_ComboSelectedItem(Cmb_Grupos_Cuentas, cCmbKBLANCO)

  Rem Carga las cuentas habilitadas y de la empresa en la grilla
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("Cod_Estado").Valor = cCod_Estado_Habilitada
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    If .Buscar_Vigentes Then
      Call Sub_hRecord2Grilla(lcCuenta.Cursor, Grilla_Cuentas, "id_cuenta")
    Else
      Call Fnt_MsgError(.SubTipo_LOG _
                      , "Problema con buscar las cuentas vigentes." _
                      , .ErrMsg _
                      , pConLog:=True)
      Err.Clear
    End If
  End With
  Set lcCuenta = Nothing
  
  ' JOVB
  fFormatoMoneda = Fnt_Busca_FormatoMonedaEmpresa
  
  'HAF
  fFormatoMoneda = fFormatoMoneda & ";[RED]-" & fFormatoMoneda
  
  
  Call Sub_Desbloquea_Puntero(Me)
    
End Sub


Private Function Fnt_Busca_FormatoMonedaEmpresa() As String
  Dim sFormato As String
  Dim lEmpresa As Class_Empresas
  
  Set lEmpresa = New Class_Empresas
  With lEmpresa
      .Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
      If .Buscar Then
          sFormato = Fnt_Formato_Moneda(.Cursor(1).Fields("ID_MONEDA").Value)
      Else
          Call Fnt_MsgError(.SubTipo_LOG _
              , "Error en traer la moneda de la emprsa." _
              , .ErrMsg _
              , pConLog:=True)
          Err.Clear
      End If
  End With
  
  Set lEmpresa = Nothing

  Fnt_Busca_FormatoMonedaEmpresa = sFormato
End Function

Private Sub Form_Resize()
Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Cuentas_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla_Cuentas.ColIndex("CHK") Then
    Cancel = True
  End If
End Sub

Private Sub Rdb_Mes_Click()
   DTP_Fecha_Inicial.Visible = False
   Lbl_Fecha_Inicial.Visible = False
   DTP_Fecha_Final.Format = dtpCustom
   DTP_Fecha_Final.CustomFormat = "MMMM yyyy"
   Lbl_Fecha_Final.Caption = "Mes "
   
   Call Sub_Fechas
   
End Sub

Private Sub rdb_RangoFecha_Click()
   DTP_Fecha_Inicial.Visible = True
   Lbl_Fecha_Inicial.Visible = True
   DTP_Fecha_Final.Format = dtpShortDate
   Lbl_Fecha_Final.Caption = "Fecha Final "
   Call Sub_Fechas
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lMensaje As String

  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REPORTE"
      If Fnt_Seleccionados(lMensaje) Then
        Call Sub_Fechas
        If Fnt_Carga_Cuentas_Grilla Then
          Call Sub_Crea_Excel
        End If
      Else
        MsgBox lMensaje, vbInformation, Me.Caption
      End If
    Case "REFRESH"
      Call Sub_Limpiar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Function Fnt_Carga_Cuentas_Grilla() As Boolean
Dim lcTemp_Tabla As Class_Temp_Tabla
'-----------------------------------
Dim lFila As Long
Dim lCol As Long
  
  Fnt_Carga_Cuentas_Grilla = True
  
  lCol = Grilla_Cuentas.ColIndex("CHK")
  
  Set lcTemp_Tabla = New Class_Temp_Tabla
  Rem Primero limpia la tabla temporal
  lcTemp_Tabla.Limpiar
  Rem Luego, agrega a la tabla temporal solo las cuentas chequeadas
  For lFila = 1 To Grilla_Cuentas.Rows - 1
    If Grilla_Cuentas.Cell(flexcpChecked, lFila, lCol) = flexChecked Then
      With lcTemp_Tabla
        .Campo("VALOR_NUMERICO").Valor = GetCell(Grilla_Cuentas, lFila, "colum_pk")
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problema en la carga de Cuentas. No se puede continuar con la generaci�n de los Reportes.", _
                            .ErrMsg)
          GoTo ErrProcedure
        End If
      End With
    End If
  Next
  Set lcTemp_Tabla = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Carga_Cuentas_Grilla = False
  Set lcTemp_Tabla = Nothing
  
End Function

Private Function Fnt_Seleccionados(ByRef pMensaje As String) As Boolean
Dim lSeleccionados As Boolean
  
  If Fnt_Contar_ReportesChequeados > 0 Then
    lSeleccionados = True
  Else
    lSeleccionados = False
    pMensaje = "Debe Seleccionar al menos un Informe."
    GoTo ErrProcedure
  End If
  
  If Fnt_Contar_CuentasChequedas > 0 Then
    lSeleccionados = True
  Else
    lSeleccionados = False
    pMensaje = "Debe Seleccionar al menos una Cuenta."
    GoTo ErrProcedure
  End If
  
ErrProcedure:
  Fnt_Seleccionados = lSeleccionados
  
End Function

Private Sub Sub_Fechas()
  If Rdb_Mes.Value Then
    lPeriodo = Format(DTP_Fecha_Final, "MMMM yyyy")
    lPeriodo = UCase(Mid(lPeriodo, 1, 1)) & Mid(lPeriodo, 2, Len(lPeriodo))
    fFechaInicial = "01/" & CStr(DTP_Fecha_Final.Month) & "/" & CStr(DTP_Fecha_Final.Year)
    If CStr(DTP_Fecha_Final.Month) = "12" Then
        fFechaFinal = CDate("01/01/" & CStr(DTP_Fecha_Final.Year + 1)) - 1
    Else
        fFechaFinal = CDate("01/" & CStr(DTP_Fecha_Final.Month + 1) & "/" & CStr(DTP_Fecha_Final.Year)) - 1
    End If
  End If
     
  If rdb_RangoFecha.Value Then
    lPeriodo = CStr(DTP_Fecha_Inicial) & " al " & CStr(DTP_Fecha_Final)
    fFechaInicial = DTP_Fecha_Inicial
    fFechaFinal = DTP_Fecha_Final
  End If
End Sub

Private Sub Sub_Limpiar()
  DTP_Fecha_Inicial.Value = fFecha_Inicial
  DTP_Fecha_Final.Value = fFecha_Final
  
  Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKBLANCO)
  Call Sub_ComboSelectedItem(Cmb_Clientes, cCmbKBLANCO)
  Call Sub_ComboSelectedItem(Cmb_Grupos_Cuentas, cCmbKBLANCO)
  
  Call Sub_CambiaCheck(False)
  
End Sub

Private Function Fnt_Contar_ReportesChequeados() As Integer
Dim lChequeados As Integer
Dim lReportes As Integer

  For lReportes = 1 To fTotal_Reportes
    If Chk_Reporte(lReportes).Value = Checked Then
      lChequeados = lChequeados + 1
    End If
  Next lReportes
  Fnt_Contar_ReportesChequeados = lChequeados
    
End Function

Private Function Fnt_Contar_CuentasChequedas() As Long
Dim lFila As Long
Dim lCol As Long
Dim lChequeados As Long
  
  lCol = Grilla_Cuentas.ColIndex("CHK")
  
  For lFila = 1 To Grilla_Cuentas.Rows - 1
    If Grilla_Cuentas.Cell(flexcpChecked, lFila, lCol) = flexChecked Then
      lChequeados = lChequeados + 1
    End If
  Next
  Fnt_Contar_CuentasChequedas = lChequeados
  
End Function

Private Sub Sub_Crea_Excel()
    Dim lHojas As Integer
    Dim lNro_Libro As Integer
    Dim lHoja As Integer
  
    Set fApp_Excel = CreateObject("Excel.application")
    fApp_Excel.DisplayAlerts = False
    Set fLibro = fApp_Excel.Workbooks.Add
    
    lHojas = Fnt_Contar_ReportesChequeados
    
    For lNro_Libro = fLibro.Worksheets.Count To fLibro.Worksheets.Count - 1 Step -1
        fLibro.Worksheets(lNro_Libro).Delete
    Next lNro_Libro

    For lNro_Libro = fLibro.Worksheets.Count To lHojas - 1
        fLibro.Worksheets.Add
    Next lNro_Libro

    For lHoja = 1 To fLibro.Worksheets.Count
        fLibro.Sheets(lHoja).Select
        fLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresa).ShapeRange.IncrementLeft 39.75
        fLibro.Worksheets.Item(lHoja).Range("B6").Value = "Banca Patrimonial de Inversiones"
        fLibro.Worksheets.Item(lHoja).Range("B6").Font.Bold = True
        fLibro.Worksheets.Item(lHoja).Columns("A:A").ColumnWidth = 2
    Next lHoja
   
    Call Sub_Generar_Reportes
    'ActiveWindow.DisplayGridlines = False
    
    'fApp_Excel.ActiveWindow.DisplayGridlines = False
    fApp_Excel.Visible = True
    fApp_Excel.UserControl = True
End Sub

Private Sub Sub_Generar_Reportes()
Dim lIndex As Integer
Dim lHoja As Integer
Dim lcTemp_Tabla As Class_Temp_Tabla

  lHoja = 1
  Call Sub_Bloquea_Puntero(Me)
    
  For lIndex = 1 To fTotal_Reportes
    If Chk_Reporte(lIndex).Value = Checked Then
      Select Case Chk_Reporte(lIndex).Tag
        Case "INFORME_PORTADA"
            Call Sub_HeaderXls_Informe_Portada(lHoja)
            Call Genera_Informe_Portada(lHoja)
        Case "INGRESOS_ACUMULADOS_ASESOR"
            Call HeaderXls_Informe_IngresosAcumulados_x_Asesor(lHoja)
            Call Genera_Informe_IngresosAcumulados_x_Asesor(lHoja)
        Case "INGRESOS_ACUMULADOSMES"
            Call HeaderXls_Informe_IngresosAcumulados_x_Mes(lHoja)
            Call Genera_Informe_IngresosAcumulados_x_Mes(lHoja)
        Case "BPI"
            Call HeaderXls_Informe_BPI(lHoja)
            Call Genera_Informe_BPI(lHoja)
        Case "INDIVIDUAL"
            Call HeaderXls_Informe_Individual(lHoja)
            Call Genera_Informe_Individual(lHoja)
        Case "CUADRO_CONTRIBUCION"
            Call HeaderXls_Informe_CuadroContribucion(lHoja)
            Call Genera_Informe_CuadroContribucion(lHoja)
        Case "RESUMEN_MES_CLIENTE"
            Call HeaderXls_Informe_Resumen_x_Mes_x_Cliente(lHoja)
            Call Genera_Informe_Resumen_x_Mes_x_Cliente(lHoja)
        Case "TOTALES_AGENTE"
            Call HeaderXls_Informe_Totales_x_Agente(lHoja)
            Call Genera_Informe_Totales_x_Agente(lHoja)
        Case "PATRIMONIO_MES_CLIENTE"
            Call HeaderXls_Informe_PatrimonioMensual_x_Cliente(lHoja)
            Call Genera_Informe_PatrimonioMensual_x_Cliente(lHoja)
        Case "SEMANAL"
            Call HeaderXls_Informe_Semanal(lHoja)
            Call Genera_Informe_Semanal(lHoja)
        Case "GESTION_BPI"
            Call HeaderXls_Informe_ResumenGestionBPI(lHoja)
            Call Genera_Informe_ResumenGestionBPI(lHoja)
        Case "DETALLE_CLIENTE_TIPO_INSTRUMENTO"
            Call HeaderXls_Informe_DetalleCliente_x_TipoInstrumento(lHoja)
            Call Genera_Informe_DetalleCliente_x_TipoInstrumento(lHoja)
        Case "GRAFICO_DISTRIB_TIPO_ACTIVO"
            Call HeaderXls_Grafico_Distribucion_x_TipoActivo(lHoja)
            Call Genera_Grafico_Distribucion_x_TipoActivo(lHoja)
        Case "FUJOS_PATRIMONIALES"
            Call HeaderXls_Informe_FlujosPatrimoniales(lHoja)
            Call Genera_Informe_FlujosPatrimoniales(lHoja)
        Case "GRAFICOS"
            Call HeaderXls_Graficos(lHoja)
            Call Genera_Graficos(lHoja)
      End Select
      lHoja = lHoja + 1
    End If
  Next lIndex
  
  fLibro.Sheets(1).Select
  
  Rem Limpia la Temp_Tabla de las cuentas ingresadas
  Set lcTemp_Tabla = New Class_Temp_Tabla
  ' lcTemp_Tabla.Limpiar
  Set lcTemp_Tabla = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Sub Genera_Informe_Portada(ByVal hoja As Integer)
    Dim lcInforme_Control_Gestion  As Class_Informe_Control_Gestion
    Dim lReg            As hCollection.hFields
    Dim lSaldos_Activos As Class_Saldo_Activos
    '----------------------------------------------------
    Dim lxHoja          As Worksheet
    '----------------------------------------------------
    Dim lFila            As Integer
    Dim lColumna         As Integer
    Dim lFechaCierre    As Variant
    Dim lAsesor_Ant     As String

    Dim lTotal_PatrAcc      As Double
    Dim lTotal_PatrAccProm  As Double
    Dim lTotal_FFMM         As Double
    Dim lTotal_FFMMProm     As Double
    Dim lTotal_RF           As Double
    Dim lTotal_RFProm       As Double

    Dim sRango As String

On Error GoTo ErrProcedure

    ' fApp_Excel.Visible = True

  Set lSaldos_Activos = New Class_Saldo_Activos
    
  Set lxHoja = fLibro.Sheets(hoja)
  Call lxHoja.Select

  lxHoja.Cells(10, 2).Value = "Patrimonios Administrados"
  lxHoja.Cells(10, 3).Value = "Totales"

  lColumna = 3
  lAsesor_Ant = "*"

  Set lcInforme_Control_Gestion = New Class_Informe_Control_Gestion
  If Not lcInforme_Control_Gestion.Distribucion_Patrimonios_Administrados(fFechaInicial _
                                                                        , fFechaFinal _
                                                                        , lFechaCierre) Then
    Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas con calcular la distribuci�n de patrimonios administrados.", _
                      lcInforme_Control_Gestion.ErrMsg)
    GoTo ExitProcedure
  End If
    
  lxHoja.Cells(18, 3).FormulaR1C1 = "=R[-3]C + R[-5]C + R[-7]C"
  lxHoja.Cells(18, 3).NumberFormat = fFormatoMoneda             '"#,##0.00"
  lxHoja.Cells(19, 3).FormulaR1C1 = "=R[-3]C + R[-5]C + R[-7]C"
  lxHoja.Cells(19, 3).NumberFormat = fFormatoMoneda             '#,##0.00"
  
  BarraProceso.Value = 0
  If lcInforme_Control_Gestion.Cursor.Count > 0 Then
        BarraProceso.Max = lcInforme_Control_Gestion.Cursor.Count
  End If
  
  For Each lReg In lcInforme_Control_Gestion.Cursor
    BarraProceso.Value = lReg.Index
    If lReg.Index = 1 Then
        lxHoja.Cells(11, 2).Value = "Patr. ACC " & lFechaCierre
        lxHoja.Cells(12, 2).Value = "Patr. ACC Prom."
        lxHoja.Cells(13, 2).Value = "FFMM " & lFechaCierre
        lxHoja.Cells(14, 2).Value = "FFMM Prom."
        lxHoja.Cells(15, 2).Value = "RF " & lFechaCierre
        lxHoja.Cells(16, 2).Value = "RF Promedio"
        lxHoja.Cells(18, 2).Value = "Total Adm. Periodo"
        lxHoja.Cells(19, 2).Value = "Total Patr. Prom."
    End If
    If lReg("Asesor").Value <> lAsesor_Ant Then
       If lColumna = 3 Then
          lColumna = lColumna + 1
       Else
          lColumna = lColumna + 2
       End If
       lFila = 10
       lxHoja.Cells(lFila, lColumna).Value = lReg("Asesor").Value
       lxHoja.Cells(lFila, lColumna + 1).Value = "%Participacion"
       lAsesor_Ant = lReg("Asesor").Value
    End If
    
    lxHoja.Cells(lFila + CInt(lReg("Fila").Value), lColumna).Select
    lxHoja.Cells(lFila + CInt(lReg("Fila").Value), lColumna).Value = lReg("Monto").Value ' Format(lReg("Monto").Value, fFormatoMoneda)    ' "#,##0.000")
    fApp_Excel.Selection.NumberFormat = fFormatoMoneda                  ' "#,##0.00"
    
    If CInt(lReg("Fila").Value) = 1 Or CInt(lReg("Fila").Value) = 3 Or CInt(lReg("Fila").Value) = 5 Then
      lxHoja.Cells(lFila + CInt(lReg("Fila").Value), lColumna + 1).Select
      fApp_Excel.Selection.NumberFormat = "#,##0.00"
      lxHoja.Cells(lFila + CInt(lReg("Fila").Value), lColumna + 1).FormulaR1C1 = "=RC[-1]/R[" & CStr(18 - (lFila + CInt(lReg("Fila").Value))) & "]C[" & CStr(3 - lColumna - 1) & "]*100"
      
    ElseIf CInt(lReg("Fila").Value) = 2 Or CInt(lReg("Fila").Value) = 4 Or CInt(lReg("Fila").Value) = 6 Then
      lxHoja.Cells(lFila + CInt(lReg("Fila").Value), lColumna + 1).Select
      fApp_Excel.Selection.NumberFormat = "#,##0.00"
      lxHoja.Cells(lFila + CInt(lReg("Fila").Value), lColumna + 1).FormulaR1C1 = "=RC[-1]/R[" & CStr(18 - (lFila + CInt(lReg("Fila").Value) - 1)) & "]C[" & CStr(3 - lColumna - 1) & "]*100"
      
    End If
    
    'lColumna TOTALES
    Select Case lReg("Fila").Value
      Case 1
        lTotal_PatrAcc = lTotal_PatrAcc + lReg("Monto").Value
        With lxHoja.Cells(lFila + CInt(lReg("Fila").Value), 3)
          .NumberFormat = fFormatoMoneda   ' "#,##0.00"
          .Value = lTotal_PatrAcc
        End With
      Case 2
        lTotal_PatrAccProm = lTotal_PatrAccProm + lReg("Monto").Value
        With lxHoja.Cells(lFila + CInt(lReg("Fila").Value), 3)
          .NumberFormat = fFormatoMoneda   ' "#,##0.00"
          .Value = lTotal_PatrAccProm
        End With
      Case 3
        lTotal_FFMM = lTotal_FFMM + lReg("Monto").Value
        With lxHoja.Cells(lFila + CInt(lReg("Fila").Value), 3)
          .NumberFormat = fFormatoMoneda   ' "#,##0.00"
          .Value = lTotal_FFMM
        End With
      Case 4
        lTotal_FFMMProm = lTotal_FFMMProm + lReg("Monto").Value
        With lxHoja.Cells(lFila + CInt(lReg("Fila").Value), 3)
          .NumberFormat = fFormatoMoneda   ' "#,##0.00"
          .Value = lTotal_FFMMProm
        End With
      Case 5
        lTotal_RF = lTotal_RF + lReg("Monto").Value
        With lxHoja.Cells(lFila + CInt(lReg("Fila").Value), 3)
          .NumberFormat = fFormatoMoneda   ' "#,##0.00"
          .Value = lTotal_RF
        End With
      Case 6
        lTotal_RFProm = lTotal_RFProm + lReg("Monto").Value
        With lxHoja.Cells(lFila + CInt(lReg("Fila").Value), 3)
          .NumberFormat = fFormatoMoneda   ' "#,##0.00"
          .Value = lTotal_RFProm
        End With
    End Select
    
    With lxHoja.Cells(18, lColumna)
      .NumberFormat = fFormatoMoneda   ' "#,##0.00"
      '.FormulaR1C1 = "=SUM(R[-7]C:R[-2]C)"
      .FormulaR1C1 = "=R[-3]C + R[-5]C + R[-7]C"
    End With
    
    With lxHoja.Cells(18, lColumna + 1)
      .NumberFormat = "#,##0.00"
      .FormulaR1C1 = "=RC[-1]/RC[" & CStr((3 - (lColumna + 1))) & "]*100"
    End With
    
    With lxHoja.Cells(19, lColumna)
      .NumberFormat = fFormatoMoneda   ' "#,##0.00"
      .FormulaR1C1 = "=R[-3]C + R[-5]C + R[-7]C"
    End With
    
    With lxHoja.Cells(19, lColumna + 1)
      .NumberFormat = "#,##0.00"
      .FormulaR1C1 = "=RC[-1]/RC[" & CStr((3 - (lColumna + 1))) & "]*100"
    End With
  Next
  
  If lcInforme_Control_Gestion.Cursor.Count > 0 Then
    Call Formato_Portada(hoja)
    
    fApp_Excel.Charts.Add
    
    fApp_Excel.ActiveChart.ChartType = xl3DPieExploded
    
    sRango = "B11:C11" & gstrSepList & "B13:C13" & gstrSepList & "B15:C15"
    
    fApp_Excel.ActiveChart.SetSourceData fLibro.Sheets("Portada").Range(sRango), xlColumns
    
    fApp_Excel.ActiveChart.Location xlLocationAsObject, "Portada"
  
    With fApp_Excel.ActiveChart
      .HasTitle = True
      .ChartTitle.Characters.Text = "Distribuci�n de Patrimonios Administrados"
      
      '.ApplyDataLabels AutoText:=True, LegendKey:=False, _
            HasLeaderLines:=True, ShowSeriesName:=False, ShowCategoryName:=False, _
            ShowValue:=False, ShowPercentage:=True, ShowBubbleSize:=False
            
      .SeriesCollection(1).DataLabels.Position = xlLabelPositionOutsideEnd
      
      ' .Axes(xlCategory).CategoryType = xlAutomatic
      '.ChartObjects(1).Left = 20
      '.ChartObjects(1).Top = 270
      
    
    End With
    
  Else
  
    With lxHoja.Cells(10, 2)
      .Value = "Sin Datos"
      .Font.Bold = True
    End With
    
    lxHoja.Cells(10, 3).Value = ""
    lxHoja.Cells(18, 3).Value = ""
    lxHoja.Cells(19, 3).Value = ""
    
  End If
  
  fLibro.Sheets("Portada").Cells(1, 1).Select

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas con calcular la distribuci�n de patrimonios administrados.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
End Sub

Sub Genera_Informe_IngresosAcumulados_x_Asesor(ByVal hoja As Integer)
Dim lcInforme_Control_Gestion  As Class_Informe_Control_Gestion
Dim lReg            As hCollection.hFields
Dim lSaldos_Activos As Class_Saldo_Activos
'--------------------------------------------------------------------
Dim lxHoja          As Worksheet
'--------------------------------------------------------------------
Dim lFila            As Integer
Dim lColumna         As Integer
Dim lFechaCierre    As Date
Dim lAsesor_Ant     As String
Dim lIndex          As Integer

On Error GoTo ErrProcedure

  lColumna = 2
  lFila = 12

  Set lxHoja = fLibro.Sheets(hoja)
  Call lxHoja.Activate
  
  Set lcInforme_Control_Gestion = New Class_Informe_Control_Gestion
  If Not lcInforme_Control_Gestion.Ingresos_Acumulados_Asesor(fFechaInicial _
                                                            , fFechaFinal) Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas con calcular los ingresos acumulados por asesor.", lcInforme_Control_Gestion.ErrMsg)
    GoTo ExitProcedure
  End If
  
  BarraProceso.Value = 0
  If lcInforme_Control_Gestion.Cursor.Count > 0 Then BarraProceso.Max = lcInforme_Control_Gestion.Cursor.Count
  For Each lReg In lcInforme_Control_Gestion.Cursor
    BarraProceso.Value = lReg.Index
    
    lxHoja.Cells(lFila, lColumna).Value = lReg("Asesor").Value
    
    With lxHoja.Cells(lFila, lColumna + 1)
      .NumberFormat = "#,##0.00"
      .Value = lReg("Comision_Transaccion").Value
    End With
    
    With lxHoja.Cells(lFila, lColumna + 2)
      .NumberFormat = "#,##0.00"
      .Value = lReg("Derechos_Bolsa").Value
    End With
    
    With lxHoja.Cells(lFila, lColumna + 3)
      .NumberFormat = "#,##0.00"
      .Value = lReg("Compra_Venta_Divisas").Value
    End With
    
    With lxHoja.Cells(lFila, lColumna + 4)
      .NumberFormat = "#,##0.00"
      .Value = lReg("Comision_FFMM").Value
    End With
    
    With lxHoja.Cells(lFila, lColumna + 5)
      .NumberFormat = "#,##0.00"
      .Cells(lFila, lColumna + 5).Value = lReg("Comisiones").Value
    End With
    
    With lxHoja.Cells(lFila, lColumna + 6)
      .NumberFormat = "#,##0.00"
      .Cells(lFila, lColumna + 6).Value = lReg("Honorarios").Value
    End With
    
    With lxHoja.Cells(lFila, lColumna + 7)
      .NumberFormat = "#,##0.00"
      .Cells(lFila, lColumna + 7).Value = lReg("Cuentas_Ext").Value
    End With
    
    With lxHoja.Cells(lFila, lColumna + 8)
      .NumberFormat = "#,##0.00"
      .FormulaR1C1 = "=SUM(RC[-7]:RC[-1])"
    End With
    
    lFila = lFila + 1
  Next
  
  If lcInforme_Control_Gestion.Cursor.Count > 0 Then
    lxHoja.Cells(lFila + 1, 2).Value = "Total"
    For lIndex = 3 To 10
      With lxHoja.Cells(lFila + 1, lIndex)
        .NumberFormat = "#,##0.00"
        .FormulaR1C1 = "=SUM(R[-" & CStr(lcInforme_Control_Gestion.Cursor.Count + 1) & "]C:R[-2]C)"
      End With
    Next lIndex
    Formato_IngresosAcumulados_x_Asesor (hoja)
  Else
     fApp_Excel.Range("B10:J11").Select
     fApp_Excel.Selection.ClearContents
     lxHoja.Cells(10, 2).Value = "Sin Datos"
     fApp_Excel.Selection.Font.Bold = True
  End If
  
  lxHoja.Cells(1, 1).Select

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas con calcular los ingresos acumulados por asesor.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
End Sub

Sub Genera_Informe_IngresosAcumulados_x_Mes(ByVal hoja As Integer)
Dim lcInforme_Control_Gestion  As Class_Informe_Control_Gestion
Dim lReg            As hCollection.hFields
Dim lSaldos_Activos As Class_Saldo_Activos
'--------------------------------------------------------------------
Dim lxHoja          As Worksheet
'--------------------------------------------------------------------
Dim lFila            As Integer
Dim lColumna         As Integer
Dim lFechaCierre    As Date
Dim lAno_Ant        As Integer
Dim lMes_Ant        As Integer
Dim lTipo_Ant       As Integer
Dim lIndex          As Integer
Dim lFechaInicial   As Date

On Error GoTo ErrProcedure

  Set lxHoja = fLibro.Sheets(hoja)
  Call lxHoja.Activate
  

  lColumna = 2
  lFila = 12
  lAno_Ant = 0
  lMes_Ant = 0
  lTipo_Ant = 0
  
  If Rdb_Mes.Value Then
    lFechaInicial = DateAdd("m", -11, fFechaInicial)
    lFechaInicial = CDate("01/" + CStr(Month(lFechaInicial)) + "/" + CStr(Year(lFechaInicial)))
  End If
  
  Set lcInforme_Control_Gestion = New Class_Informe_Control_Gestion
  If Not lcInforme_Control_Gestion.Ingresos_Acumulados_Mes(lFechaInicial _
                                                         , fFechaFinal) Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas con calcular los ingresos acumulados por mes.", lcInforme_Control_Gestion.ErrMsg)
    GoTo ExitProcedure
  End If
  
  BarraProceso.Value = 0
  If lcInforme_Control_Gestion.Cursor.Count > 0 Then BarraProceso.Max = lcInforme_Control_Gestion.Cursor.Count
  
  
  
  For Each lReg In lcInforme_Control_Gestion.Cursor
    BarraProceso.Value = lReg.Index
    If lAno_Ant <> lReg("ano").Value Then
      If lAno_Ant = 0 Then
        lFila = 11
      Else
        With lxHoja.Cells(lFila + 9, lColumna)
          .NumberFormat = "#,##0.00"
          .FormulaR1C1 = "=SUM(R[-8]C:R[-2]C)"
        End With
        
        With lxHoja.Cells(lFila, lColumna + 1)
          .NumberFormat = "#,##0.00"
          .Value = "Total Acumulado"
        End With
        
        With lxHoja.Cells(lFila + 9, lColumna + 1)
          .NumberFormat = "#,##0.00"
          .FormulaR1C1 = "=SUM(RC[" + CStr(2 - lColumna) + "]:RC[-1])" ' total ultimo mes
        End With
        lFila = lFila + 13
      End If
      
      lColumna = 2
      lAno_Ant = lReg("ano").Value
      lxHoja.Cells(lFila, lColumna).Value = "Periodo " + CStr(lReg("ano").Value)
      lxHoja.Cells(lFila + 1, 2).Value = "Comisiones x Transacci�n"
      lxHoja.Cells(lFila + 2, 2).Value = "Derechos Bolsa"
      lxHoja.Cells(lFila + 3, 2).Value = "Asesorias"
      lxHoja.Cells(lFila + 4, 2).Value = "Honorarios"
      lxHoja.Cells(lFila + 5, 2).Value = "Compra Venta Divisas"
      lxHoja.Cells(lFila + 6, 2).Value = "Comisones FFMM"
      lxHoja.Cells(lFila + 7, 2).Value = "Comisiones Houston"
      lxHoja.Cells(lFila + 9, 2).Value = "Totales"
      lMes_Ant = 0
    End If
    
    If lMes_Ant <> lReg("Mes").Value Then
      If lMes_Ant <> 0 Then
        With lxHoja.Cells(lFila + 9, lColumna)
          .NumberFormat = "#,##0.00"
          .FormulaR1C1 = "=SUM(R[-8]C:R[-2]C)"
        End With
      End If
      lMes_Ant = lReg("Mes").Value
      lColumna = lColumna + 1
      lxHoja.Cells(lFila, lColumna).Value = Nombre_Mes(lReg("Mes_Nombre").Value)
    End If
    
    With lxHoja.Cells(lFila + lReg("Nro_tipo").Value, lColumna)

      .NumberFormat = "#,##0.00"
      .Value = lReg("Monto").Value
      
    End With
  Next
  
  If lcInforme_Control_Gestion.Cursor.Count > 0 Then
    With lxHoja.Cells(lFila + 9, lColumna)
      .NumberFormat = "#,##0.00"
      .FormulaR1C1 = "=SUM(R[-8]C:R[-2]C)" ' total ultimo mes
    End With
    
    With lxHoja.Cells(lFila, lColumna + 1)
      .NumberFormat = "#,##0.00"
      .Value = "Total Acumulado"
    End With
    
    With lxHoja.Cells(lFila + 9, lColumna + 1)
      .NumberFormat = "#,##0.00"
      .FormulaR1C1 = "=SUM(RC[" + CStr(2 - lColumna) + "]:RC[-1])" ' total ultimo mes
    End With
    
    Call Formato_IngresosAcumulados_x_Mes(hoja)
  Else
    fApp_Excel.Range("B10:B11").Select
    fApp_Excel.Selection.ClearContents
    lxHoja.Cells(10, 2).Value = "Sin Datos"
  End If

  lxHoja.Cells(1, 1).Select

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas con calcular los ingresos acumulados por mes.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
End Sub

Sub Genera_Informe_BPI(ByVal hoja As Integer)
Dim lcInforme_Control_Gestion  As Class_Informe_Control_Gestion
Dim lReg            As hCollection.hFields
Dim lSaldos_Activos As Class_Saldo_Activos
'--------------------------------------------------------------------
Dim lxHoja          As Worksheet
'--------------------------------------------------------------------
Dim lFila            As Integer
Dim lColumna         As Integer
Dim lFechaCierre    As Date
Dim lAno_Ant        As Integer
Dim lMes_Ant        As Integer
Dim lTipo_Ant       As Integer
Dim lIndex          As Integer
Dim lDifCol          As Integer
Dim Parte_Ingresos  As String
Dim Parte_VolMedio  As String
Dim lMes            As Integer
Dim i               As Long
Dim lFechaInicial   As Date


On Error GoTo ErrProcedure

  Set lxHoja = fLibro.Sheets(hoja)
  Call lxHoja.Activate

  lColumna = 2
  lFila = 12
  lAno_Ant = 0
  lMes_Ant = 0
  lTipo_Ant = 0

  If Rdb_Mes.Value Then
    lFechaInicial = DateAdd("m", -11, fFechaInicial)
    lFechaInicial = CDate("01/" + CStr(Month(lFechaInicial)) + "/" + CStr(Year(lFechaInicial)))
  End If
  
  Set lcInforme_Control_Gestion = New Class_Informe_Control_Gestion
  If Not lcInforme_Control_Gestion.Ingresos_Acumulados_Mes(fFechaInicial _
                                                         , fFechaFinal) Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas con calcular el Informe BPI.", lcInforme_Control_Gestion.ErrMsg)
    GoTo ExitProcedure
  End If
  
  BarraProceso.Value = 0
  If lcInforme_Control_Gestion.Cursor.Count > 0 Then BarraProceso.Max = lcInforme_Control_Gestion.Cursor.Count
  For Each lReg In lcInforme_Control_Gestion.Cursor
    BarraProceso.Value = lReg.Index
  
    If lAno_Ant <> lReg("ano").Value Then
      If lAno_Ant = 0 Then
        lFila = 11
      Else
        lDifCol = 3 - lColumna
        lMes = lColumna - 2
        Parte_Ingresos = "(SUM(R[-2]C:R[-2]C[" & lDifCol & "]))/" & lMes
        Parte_VolMedio = "(SUM(R[-4]C:R[-4]C[" & lDifCol & "]))/" & lMes
        With lxHoja.Cells(lFila + 5, lColumna)
          .NumberFormat = "#,##0.00"
          .FormulaR1C1 = "=((" & Parte_Ingresos & "/" & Parte_VolMedio & ")*12)*100"
        End With
        lFila = lFila + 7
      End If
      lColumna = 2
      lAno_Ant = lReg("ano").Value
      lxHoja.Cells(lFila, 2).Value = "Item"
      lxHoja.Cells(lFila + 1, 2).Value = "Volumen Medio"
      lxHoja.Cells(lFila + 2, 2).Value = "Volumen Total"
      lxHoja.Cells(lFila + 3, 2).Value = "Ingresos"
      lxHoja.Cells(lFila + 5, 2).Value = "Roa Acumulado"
      lMes_Ant = 0
    End If
    
    If lMes_Ant <> lReg("Mes").Value Then
      If lMes_Ant <> 0 Then
         lDifCol = 3 - lColumna
         lMes = lColumna - 2
         Parte_Ingresos = "(SUM(R[-2]C:R[-2]C[" & lDifCol & "]))/" & lMes
         Parte_VolMedio = "(SUM(R[-4]C:R[-4]C[" & lDifCol & "]))/" & lMes
         lxHoja.Cells(lFila + 5, lColumna).Select
         fApp_Excel.Selection.NumberFormat = "#,##0.00"
         lxHoja.Cells(lFila + 5, lColumna).FormulaR1C1 = "=((" & Parte_Ingresos & "/" & Parte_VolMedio & ")*12)*100"
      End If
      lMes_Ant = lReg("Mes").Value
      lColumna = lColumna + 1
      lxHoja.Cells(lFila, lColumna).Value = lReg("Mes_Nombre").Value & " " & CStr(lReg("ano").Value)
    End If
    
    lxHoja.Cells(lFila + lReg("Nro_tipo").Value, lColumna).Select
    fApp_Excel.Selection.NumberFormat = "#,##0.00"
    lxHoja.Cells(lFila + lReg("Nro_tipo").Value, lColumna).Value = lReg("Monto").Value
  Next
  
  If lcInforme_Control_Gestion.Cursor.Count > 0 Then
    lDifCol = 3 - lColumna
    lMes = lColumna - 2
    Parte_Ingresos = "(SUM(R[-2]C:R[-2]C[" & lDifCol & "]))/" & lMes
    Parte_VolMedio = "(SUM(R[-4]C:R[-4]C[" & lDifCol & "]))/" & lMes
    
    With lxHoja.Cells(lFila + 5, lColumna)
      .NumberFormat = "#,##0.00"
      .FormulaR1C1 = "=((" & Parte_Ingresos & "/" & Parte_VolMedio & ")*12)*100"
    End With
    
    Call Formato_BPI(hoja)
  Else
    fApp_Excel.Range("B10:B10").Select
    fApp_Excel.Selection.ClearContents
    lxHoja.Cells(10, 2).Value = "Sin Datos"
    fApp_Excel.Selection.Font.Bold = True
  End If
  
  lxHoja.Cells(1, 1).Select
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas con calcular el Informe BPI.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
End Sub

Sub Genera_Informe_Individual(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate
Dim lCursor As hRecord
Dim lcInforme_Control_Gestion  As Class_Informe_Control_Gestion
Dim lReg            As hCollection.hFields
Dim lSaldos_Activos As Class_Saldo_Activos
Dim lFila            As Integer
Dim lColumna         As Integer
Dim lColumna_Base    As Integer
Dim lFechaCierre    As Date
Dim lAno_Ant        As Integer
Dim lMes_Ant        As Integer
Dim lAsesor_Ant     As String
Dim lTipo_Ant       As Integer
Dim lNRegistros     As Integer
Dim lIndex          As Integer
Dim DifCol          As Integer
Dim Parte_Ingresos  As String
Dim Parte_VolMedio  As String
Dim lMes            As Integer
Dim lFechaInicial   As Date

On Error GoTo ErrProcedure

lColumna = 3
lFila = 12
lNRegistros = 0
lAno_Ant = 0
lMes_Ant = 0
lTipo_Ant = 0
lAsesor_Ant = ""

  If Rdb_Mes.Value Then
     lFechaInicial = DateAdd("m", -11, fFechaInicial)
     lFechaInicial = CDate("01/" + CStr(Month(lFechaInicial)) + "/" + CStr(Year(lFechaInicial)))
  End If

  gDB.Procedimiento = cfPackage & ".Individual"
  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "PFecha_Inicial", ePT_Fecha, fFechaInicial, ePD_Entrada
  gDB.Parametros.Add "PFecha_Final", ePT_Fecha, fFechaFinal, ePD_Entrada
  If gDB.EjecutaSP Then
    Set lCursor = gDB.Parametros("pcursor").Valor
    
    BarraProceso.Value = 0
    If lCursor.Count > 0 Then BarraProceso.Max = lCursor.Count
    
    For Each lReg In lCursor
      BarraProceso.Value = lReg.Index
      
        lNRegistros = lNRegistros + 1
        
        If lAsesor_Ant <> lReg("asesor").Value Then
           
            ' CALCULA ROA
           DifCol = lColumna_Base - lColumna
           lMes = lColumna - (lColumna_Base - 1)
           Parte_Ingresos = "(SUM(R[-1]C:R[-1]C[" & DifCol & "]))/" & lMes
           Parte_VolMedio = "(SUM(R[-2]C:R[-2]C[" & DifCol & "]))/" & lMes
           fLibro.ActiveSheet.Cells(lFila + 3, lColumna).Select
           fApp_Excel.Selection.NumberFormat = "#,##0.00"
           fLibro.ActiveSheet.Cells(lFila + 3, lColumna).FormulaR1C1 = "=((" & Parte_Ingresos & "/" & Parte_VolMedio & ")*12)*100"
        
           If lAsesor_Ant <> "" Then
              lFila = lFila + 5
           End If
           lColumna = 3
           fLibro.ActiveSheet.Cells(lFila, 2).Value = "Item"
           fLibro.ActiveSheet.Cells(lFila, 3).Value = "Asesor"
           
           fLibro.ActiveSheet.Cells(lFila + 1, 2).Value = "Volumen Medio"
           fLibro.ActiveSheet.Cells(lFila + 2, 2).Value = "Ingresos"
           fLibro.ActiveSheet.Cells(lFila + 3, 2).Value = "Roa Acumulado"
           
           fLibro.ActiveSheet.Cells(lFila + 1, 3).Value = lReg("Asesor").Value
           fLibro.ActiveSheet.Cells(lFila + 2, 3).Value = lReg("Asesor").Value
           fLibro.ActiveSheet.Cells(lFila + 3, 3).Value = lReg("Asesor").Value
           
           lAsesor_Ant = lReg("asesor").Value
           lMes_Ant = 0
           lAno_Ant = 0
        End If
        
        If lMes_Ant <> lReg("Mes").Value Then
           lColumna = lColumna + 1
           lMes_Ant = lReg("Mes").Value
           If lAno_Ant <> lReg("ano").Value Then
              lColumna_Base = lColumna
              lAno_Ant = lReg("ano").Value
           End If
           
            ' CALCULA ROA
           If lAno_Ant <> 0 Then
              DifCol = lColumna_Base - lColumna
              lMes = lColumna - (lColumna_Base - 1)
              Parte_Ingresos = "(SUM(R[-1]C:R[-1]C[" & DifCol & "]))/" & lMes
              Parte_VolMedio = "(SUM(R[-2]C:R[-2]C[" & DifCol & "]))/" & lMes
              fLibro.ActiveSheet.Cells(lFila + 3, lColumna).Select
              fApp_Excel.Selection.NumberFormat = "#,##0.00"
              fLibro.ActiveSheet.Cells(lFila + 3, lColumna).FormulaR1C1 = "=((" & Parte_Ingresos & "/" & Parte_VolMedio & ")*12)*100"
           End If
           
           fLibro.ActiveSheet.Cells(lFila, lColumna).Value = lReg("Mes_Nombre").Value & " " & CStr(lReg("ano").Value)
          
        End If
        
        If Not IsNull(lReg("Monto").Value) Then
           fLibro.ActiveSheet.Cells(lFila + lReg("Nro_Tipo").Value, lColumna).Select
           fApp_Excel.Selection.NumberFormat = "#,##0.00"
           fLibro.ActiveSheet.Cells(lFila + lReg("Nro_Tipo").Value, lColumna).Value = lReg("Monto").Value
        End If
        
    
    Next
    Dim lColumna_Final As Integer
    Dim i As Integer
    Dim j As Integer
    Dim Celda As String
    Dim Formula As String
    If lNRegistros > 0 Then
        lColumna_Final = lColumna
        ' CALCULA ROA
         DifCol = lColumna_Base - lColumna
         lMes = lColumna - (lColumna_Base - 1)
         Parte_Ingresos = "(SUM(R[-1]C:R[-1]C[" & DifCol & "])/" & lMes & ")"
         Parte_VolMedio = "(SUM(R[-2]C:R[-2]C[" & DifCol & "])/" & lMes & ")"
         fLibro.ActiveSheet.Cells(lFila + 3, lColumna).Select
         fApp_Excel.Selection.NumberFormat = "#,##0.00"
         fLibro.ActiveSheet.Cells(lFila + 3, lColumna).FormulaR1C1 = "=((" & Parte_Ingresos & "/" & Parte_VolMedio & ")*12)*100"
         
         
         lFila = lFila + 5
         fLibro.ActiveSheet.Cells(lFila, 2).Value = "Totales"
         For i = 4 To lColumna_Final
             Formula = "="
             For j = 13 To lFila
                If fLibro.ActiveSheet.Cells(j, 2).Value = "Volumen Medio" Then
                    Celda = fLibro.ActiveSheet.Cells(j, i).Address
                    Formula = Formula & "+" & Celda
                End If
            Next j
           fLibro.ActiveSheet.Cells(lFila, i).Select
           fApp_Excel.Selection.NumberFormat = "#,##0.00"
           fLibro.ActiveSheet.Cells(lFila, i).Formula = Formula
        
         Next i
    
         lFila = lFila + 3
         fLibro.ActiveSheet.Cells(lFila, 2).Value = "Ingresos Acumulados"
         For i = 4 To lColumna_Final
            fLibro.ActiveSheet.Cells(lFila - 1, i).Value = fLibro.ActiveSheet.Cells(12, i).Value
            
            Formula = "="
            If i <> 4 And fLibro.ActiveSheet.Cells(12, i).Value <> "Enero" Then
                Celda = fLibro.ActiveSheet.Cells(lFila, i - 1).Address
                Formula = Formula & Celda
            End If
             
             For j = 13 To lFila
                If fLibro.ActiveSheet.Cells(j, 2).Value = "Ingresos" Then
                    Celda = fLibro.ActiveSheet.Cells(j, i).Address
                    Formula = Formula & "+" & Celda
                End If
            Next j
            
            fLibro.ActiveSheet.Cells(lFila, i).Select
            fApp_Excel.Selection.NumberFormat = "#,##0.00"
            fLibro.ActiveSheet.Cells(lFila, i).Formula = Formula
         
         Next i
         Formato_Individual (hoja)
    Else
       fApp_Excel.Range("B10:B10").Select
       fApp_Excel.Selection.ClearContents
       fLibro.ActiveSheet.Cells(10, 2).Value = "Sin Datos"
       fApp_Excel.Selection.Font.Bold = True
       fLibro.ActiveSheet.Cells(1, 1).Select
    End If
  Else
    Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas en generar la hoja Indivudual.", _
                      gDB.ErrMsg)
    GoTo ExitProcedure
  End If
  gDB.Parametros.Clear
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en generar la hoja Indivudual.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
  gDB.Parametros.Clear
End Sub

Sub Genera_Informe_CuadroContribucion(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate
Dim lCursor As hRecord
Dim lcInforme_Control_Gestion  As Class_Informe_Control_Gestion
Dim lReg            As hCollection.hFields
Dim lSaldos_Activos As Class_Saldo_Activos
Dim lFila            As Integer
Dim lColumna         As Integer
Dim lColumna_Final   As Integer
Dim lFechaCierre    As Date
Dim lAsesor_Ant     As String
Dim i As Integer
Dim lNRegistros     As Integer

Dim Total_PatrAcc      As Double
Dim Total_PatrAccProm  As Double
Dim Total_FFMM         As Double
Dim Total_FFMMProm     As Double
Dim Total_RF           As Double
Dim Total_RFProm       As Double

On Error GoTo ErrProcedure

  Set lSaldos_Activos = New Class_Saldo_Activos
  fLibro.ActiveSheet.Cells(10, 2).Value = "Patrimonios Administrados"
  
  lColumna = 3
  lAsesor_Ant = "*"
  lNRegistros = 0
  
  fLibro.ActiveSheet.Cells(11, 2).Value = "Patr. ACC"
  fLibro.ActiveSheet.Cells(12, 2).Value = "Patr. ACC Prom."
  fLibro.ActiveSheet.Cells(13, 2).Value = "FFMM"
  fLibro.ActiveSheet.Cells(14, 2).Value = "FFMM Prom."
  fLibro.ActiveSheet.Cells(15, 2).Value = "RF "
  fLibro.ActiveSheet.Cells(16, 2).Value = "RF Promedio"
  fLibro.ActiveSheet.Cells(18, 2).Value = "Total Adm. Periodo"
  
  
  fLibro.ActiveSheet.Cells(18, 3).Select
  fApp_Excel.Selection.NumberFormat = "#,##0.00"
  fLibro.ActiveSheet.Cells(18, 3).FormulaR1C1 = "=R[-3]C + R[-5]C + R[-7]C"

  gDB.Procedimiento = cfPackage & ".Cuadro_Contribucion"
  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "PFecha_Inicial", ePT_Fecha, fFechaInicial, ePD_Entrada
  gDB.Parametros.Add "PFecha_Final", ePT_Fecha, fFechaFinal, ePD_Entrada
  gDB.Parametros.Add "PFecha_MaxCierre", ePT_Fecha, Null, ePD_Salida
  If gDB.EjecutaSP Then
    Set lCursor = gDB.Parametros("pcursor").Valor
    
    BarraProceso.Value = 0
    If lCursor.Count > 0 Then BarraProceso.Max = lCursor.Count
    
    For Each lReg In lCursor
      BarraProceso.Value = lReg.Index
        lNRegistros = lNRegistros + 1
        lFechaCierre = gDB.Parametros("PFecha_MaxCierre").Valor
        If lReg("Asesor").Value <> lAsesor_Ant Then
           lColumna = lColumna + 1
           lFila = 10
           fLibro.ActiveSheet.Cells(lFila, lColumna).Value = lReg("Asesor").Value
           fLibro.ActiveSheet.Cells(lFila, lColumna + 1).Value = "%Contribucion"
           fLibro.ActiveSheet.Cells(lFila, lColumna + 2).Value = "%Participacion"
           lAsesor_Ant = lReg("Asesor").Value
        End If
        fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), lColumna).Select
        fApp_Excel.Selection.NumberFormat = "#,##0.00"
        fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), lColumna).Value = lReg("Monto").Value
        
        
        'lColumna TOTALES
        If CInt(lReg("Fila").Value) = 1 Then
           Total_PatrAcc = Total_PatrAcc + lReg("Monto").Value
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Select
           fApp_Excel.Selection.NumberFormat = "#,##0.00"
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Value = Total_PatrAcc
        End If
        
        If CInt(lReg("Fila").Value) = 2 Then
           Total_PatrAccProm = Total_PatrAccProm + lReg("Monto").Value
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Select
           fApp_Excel.Selection.NumberFormat = "#,##0.00"
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Value = Total_PatrAccProm
        End If
        
        If CInt(lReg("Fila").Value) = 3 Then
           Total_FFMM = Total_FFMM + lReg("Monto").Value
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Select
           fApp_Excel.Selection.NumberFormat = "#,##0.00"
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Value = Total_FFMM
        End If
        
        If CInt(lReg("Fila").Value) = 4 Then
           Total_FFMMProm = Total_FFMMProm + lReg("Monto").Value
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Select
           fApp_Excel.Selection.NumberFormat = "#,##0.00"
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Value = Total_FFMMProm
        End If
        
        If CInt(lReg("Fila").Value) = 5 Then
           Total_RF = Total_RF + lReg("Monto").Value
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Select
           fApp_Excel.Selection.NumberFormat = "#,##0.00"
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Value = Total_RF
        End If
        
        If CInt(lReg("Fila").Value) = 6 Then
           Total_RFProm = Total_RFProm + lReg("Monto").Value
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Select
           fApp_Excel.Selection.NumberFormat = "#,##0.00"
           fLibro.ActiveSheet.Cells(lFila + CInt(lReg("Fila").Value), 3).Value = Total_RFProm
        End If
        fLibro.ActiveSheet.Cells(18, lColumna).Select
        fApp_Excel.Selection.NumberFormat = "#,##0.00"
        fLibro.ActiveSheet.Cells(18, lColumna).FormulaR1C1 = "=R[-3]C + R[-5]C + R[-7]C"
    Next
  Else
    Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas en generar la hoja Cuadro Contribuci�n.", _
                      gDB.ErrMsg)
    GoTo ExitProcedure
  End If
  fLibro.ActiveSheet.Cells(18, 3).Value = 100
  lColumna_Final = lColumna + 2
  For i = 5 To lColumna_Final
      If fLibro.ActiveSheet.Cells(10, i).Value = "%Contribucion" Then
         If fLibro.ActiveSheet.Cells(11, 3).Value <> 0 Then
            fLibro.ActiveSheet.Cells(11, i).Select
            fApp_Excel.Selection.NumberFormat = "#,##0.00"
            fLibro.ActiveSheet.Cells(11, i).Formula = "=(" & fLibro.ActiveSheet.Cells(11, i - 1).Address & "/" & fLibro.ActiveSheet.Cells(11, 3).Address & ")*100"
         End If
         If fLibro.ActiveSheet.Cells(13, 3).Value <> 0 Then
            fLibro.ActiveSheet.Cells(13, i).Select
            fApp_Excel.Selection.NumberFormat = "#,##0.00"
            fLibro.ActiveSheet.Cells(13, i).Formula = "=(" & fLibro.ActiveSheet.Cells(13, i - 1).Address & "/" & fLibro.ActiveSheet.Cells(13, 3).Address & ")*100"
         End If
         If fLibro.ActiveSheet.Cells(15, 3).Value <> 0 Then
            fLibro.ActiveSheet.Cells(15, i).Select
            fApp_Excel.Selection.NumberFormat = "#,##0.00"
            fLibro.ActiveSheet.Cells(15, i).Formula = "=(" & fLibro.ActiveSheet.Cells(15, i - 1).Address & "/" & fLibro.ActiveSheet.Cells(15, 3).Address & ")*100"
         End If
         If fLibro.ActiveSheet.Cells(18, 3).Value <> 0 Then
            fLibro.ActiveSheet.Cells(18, i).Select
            fApp_Excel.Selection.NumberFormat = "#,##0.00"
            fLibro.ActiveSheet.Cells(18, i).Formula = "=(" & fLibro.ActiveSheet.Cells(18, i - 1).Address & "/" & fLibro.ActiveSheet.Cells(18, 3).Address & ")*100"
         End If
      End If
      
      If fLibro.ActiveSheet.Cells(10, i).Value = "%Participacion" Then
         If fLibro.ActiveSheet.Cells(18, 3).Value <> 0 Then
            fLibro.ActiveSheet.Cells(11, i).Select
            fApp_Excel.Selection.NumberFormat = "#,##0.00"
            fLibro.ActiveSheet.Cells(11, i).Formula = "=(" & fLibro.ActiveSheet.Cells(11, i - 2).Address & "/" & fLibro.ActiveSheet.Cells(18, 3).Address & ")*100"
            
            fLibro.ActiveSheet.Cells(13, i).Select
            fApp_Excel.Selection.NumberFormat = "#,##0.00"
            fLibro.ActiveSheet.Cells(13, i).Formula = "=(" & fLibro.ActiveSheet.Cells(13, i - 2).Address & "/" & fLibro.ActiveSheet.Cells(18, 3).Address & ")*100"
            
            fLibro.ActiveSheet.Cells(15, i).Select
            fApp_Excel.Selection.NumberFormat = "#,##0.00"
            fLibro.ActiveSheet.Cells(15, i).Formula = "=(" & fLibro.ActiveSheet.Cells(15, i - 2).Address & "/" & fLibro.ActiveSheet.Cells(18, 3).Address & ")*100"
            
            fLibro.ActiveSheet.Cells(18, i).Select
            fApp_Excel.Selection.NumberFormat = "#,##0.00"
            fLibro.ActiveSheet.Cells(18, i).Formula = "=(" & fLibro.ActiveSheet.Cells(18, i - 2).Address & "/" & fLibro.ActiveSheet.Cells(18, 3).Address & ")*100"
         End If
      End If
  Next i
    If lNRegistros > 0 Then
   
        Formato_CuadroContribucion (hoja)
    Else
       fApp_Excel.Range("B10:C18").Select
       fApp_Excel.Selection.ClearContents
       fLibro.ActiveSheet.Cells(10, 2).Value = "Sin Datos"
       fApp_Excel.Selection.Font.Bold = True
       fLibro.ActiveSheet.Cells(1, 1).Select
    End If
  gDB.Parametros.Clear

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en generar la hoja Cuadro Contribuci�n..", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
  gDB.Parametros.Clear
End Sub

Sub Genera_Informe_Resumen_x_Mes_x_Cliente(ByVal pHoja As Integer)
    Dim lCursor As hRecord
    Dim lcInforme_Control_Gestion  As Class_Informe_Control_Gestion
    Dim lReg            As hCollection.hFields
    Dim lSaldos_Activos As Class_Saldo_Activos
    '---------------------------------------------------------------------------
    Dim lcHoja          As Excel.Worksheet
    '---------------------------------------------------------------------------
    Dim lFila            As Integer
    Dim lColumna         As Integer
    Dim lColumna_Final   As Integer
    Dim lFechaCierre    As Date
    Dim lAsesor_Ant     As String
    Dim lCliente_Ant    As String
    'Dim i               As Integer
    Dim i               As Long
    Dim lNRegistros     As Integer
    Dim Cambia          As Boolean
    
    Const cFila_Inicio = 10
    
    On Error GoTo ErrProcedure

  Set lcHoja = fLibro.Sheets(pHoja)
  With lcHoja
    Call .Activate
    Call .Select
  End With

  Set lSaldos_Activos = New Class_Saldo_Activos
  
  lcHoja.Cells(10, 2).Value = "Resumen X Mes"
  lcHoja.Cells(10, 2).Font.Bold = True
  lcHoja.Cells(10, 4).Value = "Volumen Medio"
  lcHoja.Cells(10, 8).Value = "Volumen Final"
    
  lcHoja.Cells(11, 2).Value = "Cliente"
  lcHoja.Cells(11, 3).Value = "Asesor"
  lcHoja.Cells(11, 4).Value = "Acciones"
  lcHoja.Cells(11, 5).Value = "FFMM"
  lcHoja.Cells(11, 6).Value = "DPF"
  lcHoja.Cells(11, 7).Value = "RF"
  lcHoja.Cells(11, 8).Value = "Acciones"
  lcHoja.Cells(11, 9).Value = "FFMM"
  lcHoja.Cells(11, 10).Value = "Interm. Financ."
  lcHoja.Cells(11, 11).Value = "RF"
  lcHoja.Cells(11, 12).Value = "Comisiones"
  lcHoja.Cells(11, 13).Value = "Honorarios"
  lcHoja.Cells(11, 14).Value = "Transaccion"
  lcHoja.Cells(11, 15).Value = "D� Bolsa"
  lcHoja.Cells(11, 16).Value = "Opt. de Cambio"
  lcHoja.Cells(11, 17).Value = "FFMM"
  lcHoja.Cells(11, 18).Value = "Total"
  lcHoja.Cells(11, 19).Value = "%"

  lColumna = 3
  lAsesor_Ant = "*"
  lCliente_Ant = "*"
  Cambia = False
  lFila = cFila_Inicio
  lNRegistros = 0

  gDB.Procedimiento = cfPackage & ".Resumen_x_Mes_x_Cliente"
  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "PFecha_Inicial", ePT_Fecha, fFechaInicial, ePD_Entrada
  gDB.Parametros.Add "PFecha_Final", ePT_Fecha, fFechaFinal, ePD_Entrada
  gDB.Parametros.Add "PFecha_MaxCierre", ePT_Fecha, Null, ePD_Salida
  
  If gDB.EjecutaSP Then
    Set lCursor = gDB.Parametros("pcursor").Valor
    
    BarraProceso.Value = 0
    If lCursor.Count > 0 Then BarraProceso.Max = lCursor.Count
    
    For Each lReg In lCursor
      BarraProceso.Value = lReg.Index
      Call Sub_Interactivo(gRelogDB)
      
      lNRegistros = lNRegistros + 1
      lFechaCierre = gDB.Parametros("PFecha_MaxCierre").Valor
      If lReg("Cliente").Value <> lCliente_Ant Then
         lFila = lFila + 1
         lCliente_Ant = lReg("cliente").Value
         Cambia = True
      End If

      If lReg("Asesor").Value <> lAsesor_Ant Then
         lFila = lFila + 1
         lAsesor_Ant = lReg("Asesor").Value
         Cambia = True
      End If
      
      'Calcula Total
      If Cambia Then
         lcHoja.Cells(lFila, 18).FormulaR1C1 = "=SUM(RC[-6]:RC[-1])"
      End If
      lcHoja.Cells(lFila, lColumna - 1).Value = lReg("Cliente").Value
      lcHoja.Cells(lFila, lColumna).Value = lReg("Asesor").Value
      
      
      With lcHoja.Cells(lFila, lColumna + lReg("Columna").Value)
        .NumberFormat = "#,##0.00"
        .Value = lReg("Monto").Value
      End With
    Next
  Else
    Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas en generar la hoja Resumen por Mes por Cliente.", _
                      gDB.ErrMsg)
    GoTo ExitProcedure
  End If
  
  If lNRegistros > 0 Then
    lFila = lFila + 2
    ' Calcula Totales
    lcHoja.Cells(lFila, 2).Value = "Totales"
    For i = 4 To 19
      With lcHoja.Cells(lFila, i)
        .NumberFormat = "#,##0.00"
        .FormulaR1C1 = "=SUM(R[" & CStr(12 - lFila) & "]C:R[-2]C)"
      End With
    Next i
  
    If lcHoja.Cells(lFila, 18).Value <> 0 Then
      For i = 12 To lFila - 2
        With lcHoja.Cells(i, 19)
         .NumberFormat = "#,##0.00"
         .Formula = "=(" & lcHoja.Cells(i, 18).Address & "/" & lcHoja.Cells(lFila, 18).Address & ")*100 "
        End With
      Next i
    End If
    
    Call Sub_Formato_Resumen_x_Mes(lcHoja, lFila - 2)
  Else
       fApp_Excel.Range("B10:S11").Select
       fApp_Excel.Selection.ClearContents
       lcHoja.Cells(10, 2).Value = "Sin Datos"
       fApp_Excel.Selection.Font.Bold = True
       lcHoja.Cells(1, 1).Select
  End If

  gDB.Parametros.Clear

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en generar la hoja Resumen por Mes por Cliente.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
  gDB.Parametros.Clear
End Sub

Sub Genera_Informe_Totales_x_Agente(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate
Dim lCursor As hRecord
Dim lcInforme_Control_Gestion  As Class_Informe_Control_Gestion
Dim lReg            As hCollection.hFields
Dim lSaldos_Activos As Class_Saldo_Activos
Dim lFila            As Integer
Dim lColumna         As Integer
Dim lColumna_Final   As Integer
Dim lFechaCierre    As Date
Dim lAsesor_Ant     As String
Dim lCliente_Ant    As String
Dim i               As Integer
Dim Cambia          As Boolean
Dim lNRegistros     As Integer

On Error GoTo ErrProcedure

Set lSaldos_Activos = New Class_Saldo_Activos

fLibro.ActiveSheet.Cells(10, 4).Value = "Volumen Medio"
fLibro.ActiveSheet.Cells(10, 8).Value = "Volumen Final"


fLibro.ActiveSheet.Cells(11, 2).Value = "Asesor"
fLibro.ActiveSheet.Cells(11, 3).Value = "Acciones"
fLibro.ActiveSheet.Cells(11, 4).Value = "FFMM"
fLibro.ActiveSheet.Cells(11, 5).Value = "DPF"
fLibro.ActiveSheet.Cells(11, 6).Value = "RF"
fLibro.ActiveSheet.Cells(11, 7).Value = "Acciones"
fLibro.ActiveSheet.Cells(11, 8).Value = "FFMM"
fLibro.ActiveSheet.Cells(11, 9).Value = "Dolar Houston"
fLibro.ActiveSheet.Cells(11, 10).Value = "RF"
fLibro.ActiveSheet.Cells(11, 11).Value = "Comisiones"
fLibro.ActiveSheet.Cells(11, 12).Value = "Honorarios"
fLibro.ActiveSheet.Cells(11, 13).Value = "Transaccion"
fLibro.ActiveSheet.Cells(11, 14).Value = "D� Bolsa"
fLibro.ActiveSheet.Cells(11, 15).Value = "Opt. de Cambio"
fLibro.ActiveSheet.Cells(11, 16).Value = "FFMM"
fLibro.ActiveSheet.Cells(11, 17).Value = "Total"
fLibro.ActiveSheet.Cells(11, 18).Value = "%"

lColumna = 2
lAsesor_Ant = "*"
lCliente_Ant = "*"
Cambia = False
lFila = 11
lNRegistros = 0
  gDB.Procedimiento = cfPackage & ".Totales_Agente"
  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "PFecha_Inicial", ePT_Fecha, fFechaInicial, ePD_Entrada
  gDB.Parametros.Add "PFecha_Final", ePT_Fecha, fFechaFinal, ePD_Entrada
  gDB.Parametros.Add "PFecha_MaxCierre", ePT_Fecha, Null, ePD_Salida
  If gDB.EjecutaSP Then
    Set lCursor = gDB.Parametros("pcursor").Valor
    
    BarraProceso.Value = 0
    If lCursor.Count > 0 Then BarraProceso.Max = lCursor.Count
    
    For Each lReg In lCursor
      BarraProceso.Value = lReg.Index
        lFechaCierre = gDB.Parametros("PFecha_MaxCierre").Valor

        If lReg("Asesor").Value <> lAsesor_Ant Then
           lFila = lFila + 1
           lAsesor_Ant = lReg("Asesor").Value
           Cambia = True
        End If
        'Calcula Total
        If Cambia Then
           fLibro.ActiveSheet.Cells(lFila, 17).FormulaR1C1 = "=SUM(RC[-6]:RC[-1])"
        End If
        fLibro.ActiveSheet.Cells(lFila, lColumna).Value = lReg("Asesor").Value
        fLibro.ActiveSheet.Cells(lFila, lColumna + lReg("Columna").Value).Select
        fApp_Excel.Selection.NumberFormat = "#,##0.00"
        fLibro.ActiveSheet.Cells(lFila, lColumna + lReg("Columna").Value).Value = lReg("Monto").Value
        lNRegistros = lNRegistros + 1
    Next
  Else
    Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas en generar la hoja Totales por Agente.", _
                      gDB.ErrMsg)
    GoTo ExitProcedure
  End If
  
  If lNRegistros > 0 Then
    lFila = lFila + 2
    ' Calcula Totales
    fLibro.ActiveSheet.Cells(lFila, 2).Value = "Totales"
    For i = 3 To 18
        fLibro.ActiveSheet.Cells(lFila, i).Select
        fApp_Excel.Selection.NumberFormat = "#,##0.00"
        fLibro.ActiveSheet.Cells(lFila, i).FormulaR1C1 = "=SUM(R[" & CStr(12 - lFila) & "]C:R[-2]C)"
    Next i
    
    If fLibro.ActiveSheet.Cells(lFila, 17).Value <> 0 Then
        For i = 12 To lFila - 2
           fLibro.ActiveSheet.Cells(i, 18).Select
           fApp_Excel.Selection.NumberFormat = "#,##0.00"
           fLibro.ActiveSheet.Cells(i, 18).Formula = "=(" & fLibro.ActiveSheet.Cells(i, 17).Address & "/" & fLibro.ActiveSheet.Cells(lFila, 17).Address & ")*100 "
        Next i
    End If
    Formato_Total_x_Agente (hoja)
  Else
       fApp_Excel.Range("B10:R13").Select
       fApp_Excel.Selection.ClearContents
       fLibro.ActiveSheet.Cells(10, 2).Value = "Sin Datos"
       fApp_Excel.Selection.Font.Bold = True
       fLibro.ActiveSheet.Cells(1, 1).Select
    
  End If
  gDB.Parametros.Clear

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas con calcular el Informe BPI.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
  gDB.Parametros.Clear
End Sub

Sub Genera_Informe_PatrimonioMensual_x_Cliente(ByVal pHoja As Integer)
Dim lcHoja                    As Excel.Worksheet
Dim lCursor                   As hRecord
Dim lcInforme_Control_Gestion As Class_Informe_Control_Gestion
Dim lReg                      As hCollection.hFields
Dim lSaldos_Activos           As Class_Saldo_Activos
'------------------------------------------------------------------------
Dim lFila            As Integer
Dim lColumna         As Integer
Dim lColumna_Base    As Integer
Dim lFechaCierre    As Date
Dim lAno_Ant        As Integer
Dim lMes_Ant        As Integer
Dim lAsesor_Ant     As String
Dim lCliente_Ant    As String
Dim lTipo_Ant       As Integer
Dim lNRegistros     As Integer
Dim lIndex          As Integer
Dim DifCol          As Integer
Dim Parte_Ingresos  As String
Dim Parte_VolMedio  As String
Dim lMes            As Integer
Dim Cambia          As Boolean
Dim Vec_Asesores()  As String
Dim Tot_Asesores  As Integer

On Error GoTo ErrProcedure

  Set lcHoja = fLibro.Sheets(pHoja)
  With lcHoja
    Call .Activate
    Call .Select
  End With
  
  lColumna = 3
  lFila = 10
  lNRegistros = 0
  lAno_Ant = 0
  lMes_Ant = 0
  lTipo_Ant = 0
  lAsesor_Ant = ""
  lCliente_Ant = ""
  Cambia = False
  Tot_Asesores = 0

  gDB.Procedimiento = cfPackage & ".Patrimonio_Mensual_Cliente"
  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "PFecha_Inicial", ePT_Fecha, fFechaInicial, ePD_Entrada
  gDB.Parametros.Add "PFecha_Final", ePT_Fecha, fFechaFinal, ePD_Entrada
  gDB.Parametros.Add "PFecha_MaxCierre", ePT_Fecha, Null, ePD_Salida
  If gDB.EjecutaSP Then
    Set lCursor = gDB.Parametros("pcursor").Valor
    
    BarraProceso.Value = 0
    If lCursor.Count > 0 Then BarraProceso.Max = lCursor.Count
    lFila = 11
    For Each lReg In lCursor
      BarraProceso.Value = lReg.Index
      lNRegistros = lNRegistros + 1
      
      If lAsesor_Ant <> lReg("asesor").Value Then
        Tot_Asesores = Tot_Asesores + 1
        ReDim Preserve Vec_Asesores(Tot_Asesores)
        Vec_Asesores(Tot_Asesores) = lReg("asesor").Value
        
        lColumna = 4
        lAsesor_Ant = lReg("asesor").Value
        lMes_Ant = 0
        lAno_Ant = 0
        Cambia = True
      End If
'      lFila = lFila + 1
      If lCliente_Ant <> lReg("Cliente").Value Then
'        'If Not Cambia Then
           lFila = lFila + 1
        'End If
        lColumna = 4
        lCliente_Ant = lReg("Cliente").Value
        lMes_Ant = 0
        lAno_Ant = 0
        Cambia = True
      End If

      If Cambia Then
         lcHoja.Cells(lFila, 2).Value = lReg("Cliente").Value
         lcHoja.Cells(lFila, 3).Value = lReg("Asesor").Value
      End If
      
      lcHoja.Cells(11, lColumna).Value = lReg("Mes_Nombre").Value
      
      With lcHoja.Cells(lFila, lColumna)
        .NumberFormat = fFormatoMoneda
        .Value = lReg("Monto").Value
      End With
      
      lColumna = lColumna + 1
    Next

    Dim lColumna_Final As Integer
    
    Dim i As Integer
    Dim j As Integer
    Dim K As Integer
    Dim Celda As String
    Dim FirstCelda As String
    Dim LastCelda As String
    Dim lFilaTotal As Integer

    lFilaTotal = lFila + 2
    If lNRegistros > 0 Then
      lColumna_Final = lColumna - 1
      lFila = lFila + 1
      For j = 1 To Tot_Asesores
          lFila = lFila + 1
          lcHoja.Cells(lFila, 2).Value = "Total"
          lcHoja.Cells(lFila, 3).Value = Vec_Asesores(j)
          For i = 4 To lColumna_Final
              FirstCelda = "*"
              LastCelda = "*"
              For K = 12 To lFila - 2
                If lcHoja.Cells(K, 3).Value = Vec_Asesores(j) Then
                  If FirstCelda = "*" Then
                      FirstCelda = lcHoja.Cells(K, i).Address
                  End If
                  LastCelda = lcHoja.Cells(K, i).Address
                End If
              Next K
              
              With lcHoja.Cells(lFila, i)
               .NumberFormat = fFormatoMoneda
               .Formula = "=SUM(" & FirstCelda & ":" & LastCelda & ")"
             End With
          Next i
      Next j
        
      lcHoja.Cells(lFila + 1, 2).Value = "Totales"
    
      For i = 4 To lColumna_Final
        FirstCelda = lcHoja.Cells(lFilaTotal, i).Address
        LastCelda = lcHoja.Cells(lFila, i).Address
        
        With lcHoja.Cells(lFila + 1, i)
          .NumberFormat = fFormatoMoneda
          .Formula = "=SUM(" & FirstCelda & ":" & LastCelda & ")"
        End With
      Next i
      
      Call Formato_PatrimonioMensual_x_Cliente(pHoja, UBound(Vec_Asesores))
    Else
      With fApp_Excel.Range("B10:D11")
        .ClearContents
        .Font.Bold = True
      End With
      
      lcHoja.Cells(10, 2).Value = "Sin Datos"
    End If
    
  Else
    Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas en generar la hoja Patrimonio Mensual por Cliente.", _
                      gDB.ErrMsg)
    GoTo ExitProcedure
  End If
  gDB.Parametros.Clear
  
  lcHoja.Cells(1, 1).Select
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en generar la hoja Patrimonio Mensual por Cliente.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
  gDB.Parametros.Clear
End Sub

Sub Genera_Informe_Semanal(ByVal hoja As Integer)

End Sub

Sub Genera_Informe_ResumenGestionBPI(ByVal pHoja As Integer)
Dim lCursor                   As hRecord
Dim lcInforme_Control_Gestion As Class_Informe_Control_Gestion
Dim lReg                      As hCollection.hFields
Dim lcHoja                    As Excel.Worksheet
'------------------------------------------------------------------
Dim lFila           As Integer
Dim lColumna        As Integer
Dim lFechaCierre    As Date
Dim lAno_Ant        As Integer
Dim lMes_Ant        As Integer
Dim lCliente_Ant    As String
Dim lNRegistros     As Integer
Dim lMes            As Integer
Dim Cambia          As Boolean
Dim lFechaInicial   As Date
Dim lUltimoMes      As Date

On Error GoTo ErrProcedure


  Set lcHoja = fLibro.Sheets(pHoja)
  With lcHoja
    Call .Activate
    Call .Select
  End With

  lColumna = 3
  lFila = 11
  lNRegistros = 0
  lAno_Ant = 0
  lMes_Ant = 0
  lCliente_Ant = ""
  Cambia = False
  
  If Rdb_Mes.Value Then
    lFechaInicial = DateAdd("m", -11, fFechaInicial)
    lFechaInicial = DateSerial(1, Month(lFechaInicial), Year(lFechaInicial))
  End If

  gDB.Procedimiento = cfPackage & ".Resumen_Gestion_BPI"
  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "PFecha_Inicial", ePT_Fecha, fFechaInicial, ePD_Entrada
  gDB.Parametros.Add "PFecha_Final", ePT_Fecha, fFechaFinal, ePD_Entrada
  If Not gDB.EjecutaSP Then
    Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas en generar la hoja Resumen Gesti�n BPI.", _
                      gDB.ErrMsg)
    GoTo ExitProcedure
  End If
  Set lCursor = gDB.Parametros("pcursor").Valor
  
  BarraProceso.Value = 0
  If lCursor.Count > 0 Then
    BarraProceso.Max = lCursor.Count
  End If
  
  For Each lReg In lCursor
    BarraProceso.Value = lReg.Index
    lNRegistros = lNRegistros + 1
    
    If lMes_Ant <> lReg("Mes").Value Then
       lFila = lFila + 1
       lMes_Ant = lReg("Mes").Value
       lAno_Ant = lReg("Ano").Value
    Else
       If lAno_Ant <> lReg("Ano").Value Then
          lFila = lFila + 1
          lAno_Ant = lReg("Ano").Value
       End If
    End If

    lcHoja.Cells(lFila, 2).Value = Fnt_MesNombre(lReg("Mes").Value) & " " & lReg("Ano").Value & " (" & lReg("Clientes").Value & ")"
    
    With lcHoja.Cells(lFila, 4)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("Aportes").Value
    End With
    
    With lcHoja.Cells(lFila, 5)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("Retiros").Value
    End With
  Next
  gDB.Parametros.Clear
  
  If lNRegistros > 0 Then
    Call Formato_ResumenGestionBPI(pHoja, 1, 0)
  Else
    With lcHoja.Range("B11:E11")
      .ClearContents
      .Font.Bold = True
    End With
    
    lcHoja.Cells(10, 2).Value = "Sin Datos"
  End If
  
  lNRegistros = 0
  gDB.Procedimiento = cfPackage & ".Resumen_Gestion_BPI_Detalle"
  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "PFecha_Inicial", ePT_Fecha, fFechaInicial, ePD_Entrada
  gDB.Parametros.Add "PFecha_Final", ePT_Fecha, fFechaFinal, ePD_Entrada
  gDB.Parametros.Add "PUltimo_Mes", ePT_Fecha, lUltimoMes, ePD_Salida
  If Not gDB.EjecutaSP Then
    Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas en generar la hoja Resumen Gesti�n BPI.", _
                      gDB.ErrMsg)
    GoTo ExitProcedure
  End If
  
  lFila = 11
  For Each lReg In gDB.Parametros("pcursor").Valor
    lNRegistros = lNRegistros + 1
      
    If Not lCliente_Ant = lReg("CLIENTE").Value Then
      lFila = lFila + 1
      lCliente_Ant = lReg("CLIENTE").Value
    End If
    
    
    If lReg("Tipo").Value = "APORTE" Then
     lColumna = 8
    Else
     lColumna = 10
    End If
    
    lcHoja.Cells(lFila, lColumna).Value = lReg("Cliente").Value
    
    With lcHoja.Cells(lFila, lColumna + 1)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("Monto").Value
    End With
  Next
  
  If lNRegistros > 0 Then
    lUltimoMes = gDB.Parametros("PUltimo_Mes").Valor
    lcHoja.Cells(10, 8).Value = "DETALLE MES DE " & UCase(Fnt_MesNombre(Month(lUltimoMes))) & " " & Year(lUltimoMes)
    
    Call Formato_ResumenGestionBPI(pHoja, 2, lFila)
  Else
    With lcHoja.Range("H10:K11")
      .ClearContents
      .Font.Bold = True
    End With
       
    lcHoja.Cells(10, 8).Value = "Sin Detalle"
  End If
  gDB.Parametros.Clear
  
  lcHoja.Cells(1, 1).Select
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en generar la hoja Resumen Gesti�n BPI.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
  gDB.Parametros.Clear
End Sub

Sub Genera_Informe_DetalleCliente_x_TipoInstrumento(ByVal pHoja As Integer)
Dim lCursor                     As hRecord
Dim lcInforme_Control_Gestion   As Class_Informe_Control_Gestion
Dim lReg                        As hCollection.hFields
Dim lcHoja                      As Excel.Worksheet
'----------------------------------------------------------------
Dim lFila            As Integer
Dim lColumna         As Integer
Dim lFechaCierre    As Date
Dim lAno_Ant        As Integer
Dim lMes_Ant        As Integer
Dim lTipo_Ant       As String
Dim lCliente_Ant    As String
Dim lNRegistros     As Integer
Dim lMes            As Integer
Dim Cambia          As Boolean

On Error GoTo ErrProcedure

  Set lcHoja = fLibro.Sheets(pHoja)
  With lcHoja
    .Select
    .Activate
  End With

  lColumna = 3
  lFila = 13
  lNRegistros = 0
  lAno_Ant = 0
  lMes_Ant = 0
  lTipo_Ant = ""
  lCliente_Ant = ""
  Cambia = False
              
  gDB.Procedimiento = cfPackage & ".Detalles_Cliente_Tipo_Instrumento"
  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "PFecha_Inicial", ePT_Fecha, fFechaInicial, ePD_Entrada
  gDB.Parametros.Add "PFecha_Final", ePT_Fecha, fFechaFinal, ePD_Entrada
  If Not gDB.EjecutaSP Then
    Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas en generar la hoja Detalle de Clientes por Tipo de Instrumento.", _
                      gDB.ErrMsg)
    GoTo ExitProcedure
  End If
    
  Set lCursor = gDB.Parametros("pcursor").Valor
  
  BarraProceso.Value = 0
  If lCursor.Count > 0 Then BarraProceso.Max = lCursor.Count
  
  For Each lReg In lCursor
    BarraProceso.Value = lReg.Index
    lNRegistros = lNRegistros + 1
    lcHoja.Cells(lFila, 2).Value = lReg("Cliente").Value
    
    With lcHoja.Cells(lFila, 3)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("ACCIONES").Value
    End With
    
    With lcHoja.Cells(lFila, 4)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("FM_RF").Value
    End With
    
    With lcHoja.Cells(lFila, 5)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("FM_RV").Value
    End With
    
    With lcHoja.Cells(lFila, 6)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("RF").Value
    End With
    
    With lcHoja.Cells(lFila, 7)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("INTERM_FINAN").Value
    End With
    
    With lcHoja.Cells(lFila, 8)
      .NumberFormat = fFormatoMoneda
      .FormulaR1C1 = "=SUM(RC[-5]:RC[-1])"
    End With
    
    lFila = lFila + 1
  Next
  
  If lNRegistros > 0 Then
    lcHoja.Cells(lFila, 2).Value = "Totales"
    
    With lcHoja.Cells(lFila, 3)
      .NumberFormat = fFormatoMoneda
      .FormulaR1C1 = "=SUM(R[" & CStr(13 - lFila) & "]C:R[-1]C)"
    End With
    
    With lcHoja.Cells(lFila, 4)
      .NumberFormat = fFormatoMoneda
      .FormulaR1C1 = "=SUM(R[" & CStr(13 - lFila) & "]C:R[-1]C)"
    End With
    
    With lcHoja.Cells(lFila, 5)
      .NumberFormat = fFormatoMoneda
      .FormulaR1C1 = "=SUM(R[" & CStr(13 - lFila) & "]C:R[-1]C)"
    End With
    
    With lcHoja.Cells(lFila, 6)
      .NumberFormat = fFormatoMoneda
      .FormulaR1C1 = "=SUM(R[" & CStr(13 - lFila) & "]C:R[-1]C)"
    End With
    
    With lcHoja.Cells(lFila, 7)
      .NumberFormat = fFormatoMoneda
      .FormulaR1C1 = "=SUM(R[" & CStr(13 - lFila) & "]C:R[-1]C)"
    End With
    
    With lcHoja.Cells(lFila, 8)
      .NumberFormat = fFormatoMoneda
      .FormulaR1C1 = "=SUM(R[" & CStr(13 - lFila) & "]C:R[-1]C)"
    End With
    
    Call Formato_DetalleCliente_x_TipoInstrumento(pHoja)
   Else
      With lcHoja.Range("B10:H12")
        .ClearContents
        .Font.Bold = True
      End With
      
      lcHoja.Cells(10, 2).Value = "Sin Datos"
   End If
  gDB.Parametros.Clear

  lcHoja.Cells(1, 1).Select

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en generar la hoja Detalle de Clientes por Tipo de Instrumento.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
  gDB.Parametros.Clear
End Sub

Sub Genera_Grafico_Distribucion_x_TipoActivo(ByVal hoja As Integer)
Dim lcHoja                     As Excel.Worksheet
Dim lcChart                    As Excel.Chart
Dim lCursor                    As hRecord
Dim lcInforme_Control_Gestion  As Class_Informe_Control_Gestion
Dim lReg                       As hCollection.hFields
'--------------------------------------------------------------------
Dim lFila            As Integer
Dim lColumna         As Integer
Dim lFechaCierre    As Date
Dim lNRegistros     As Integer
Dim lMes            As Integer


On Error GoTo ErrProcedure

  Set lcHoja = fLibro.Sheets(hoja)
  With lcHoja
    .Select
    .Activate
  End With

  lColumna = 3
  lFila = 13
  lNRegistros = 0
              
  gDB.Procedimiento = cfPackage & ".Grafico_Distribucion_Tipo_Activo"
  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "PFecha_Inicial", ePT_Fecha, fFechaInicial, ePD_Entrada
  gDB.Parametros.Add "PFecha_Final", ePT_Fecha, fFechaFinal, ePD_Entrada
  If Not gDB.EjecutaSP Then
    Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas en generar la hoja Gr�fico de Distribuci�n por Tipo de Activo.", _
                      gDB.ErrMsg)
    GoTo ExitProcedure
  End If
    
  Set lCursor = gDB.Parametros("pcursor").Valor
  
  BarraProceso.Value = 0
  If lCursor.Count > 0 Then BarraProceso.Max = lCursor.Count
  
  For Each lReg In lCursor
    BarraProceso.Value = lReg.Index
    lNRegistros = lNRegistros + 1
    With lcHoja.Cells(11, 3)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("ACCIONES").Value
    End With
    
    With lcHoja.Cells(12, 3)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("FM_RF").Value
    End With
    
    With lcHoja.Cells(13, 3)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("FM_RV").Value
    End With
    
    With lcHoja.Cells(14, 3)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("RF").Value
    End With
    
    With lcHoja.Cells(15, 3)
      .NumberFormat = fFormatoMoneda
      .Value = lReg("INTERM_FINAN").Value
    End With
    
    With lcHoja.Cells(16, 3)
      .NumberFormat = fFormatoMoneda
      .FormulaR1C1 = "=SUM(R[-5]C:R[-1]C)"
    End With
    
    lFila = lFila + 1
  Next
  
  If lcHoja.Cells(16, 3).Value > 0 Then
    With lcHoja.Cells(11, 4)
      .NumberFormat = "#,##0.00"
      .Formula = "=(" & lcHoja.Cells(11, 3).Address & "/" & lcHoja.Cells(16, 3).Address & ")*100"
    End With
    
    With lcHoja.Cells(12, 4)
      .NumberFormat = "#,##0.00"
      .Value = "=(" & lcHoja.Cells(12, 3).Address & "/" & lcHoja.Cells(16, 3).Address & ")*100"
    End With
    
    With lcHoja.Cells(13, 4)
      .NumberFormat = "#,##0.00"
      .Value = "=(" & lcHoja.Cells(13, 3).Address & "/" & lcHoja.Cells(16, 3).Address & ")*100"
    End With
    
    With lcHoja.Cells(14, 4)
      .NumberFormat = "#,##0.00"
      .Value = "=(" & lcHoja.Cells(14, 3).Address & "/" & lcHoja.Cells(16, 3).Address & ")*100"
    End With
    
    With lcHoja.Cells(15, 4)
      .NumberFormat = "#,##0.00"
      .Value = "=(" & lcHoja.Cells(15, 3).Address & "/" & lcHoja.Cells(16, 3).Address & ")*100"
    End With
    
    With lcHoja.Cells(16, 4)
      .NumberFormat = "#,##0.00"
      .FormulaR1C1 = "=SUM(R[-5]C:R[-1]C)"
    End With
  End If
  
  If lNRegistros > 0 And lcHoja.Cells(16, 3).Value <> 0 Then
    Set lcChart = fApp_Excel.Charts.Add
    With lcChart ' fApp_Excel.ActiveChart
      .ChartType = xl3DColumnClustered
      .SetSourceData lcHoja.Range("B11:B15,D11:D15"), xlRows
      
      .HasTitle = True
      .ChartTitle.Characters.Text = "Distribuci�n de Patrimonios Administrados"
      .Axes(xlCategory).HasTitle = False
      .Axes(xlSeries).HasTitle = False
      .Axes(xlValue).HasTitle = False
      .HasAxis(xlCategory) = False
      .HasAxis(xlSeries) = False
      .HasAxis(xlValue) = True
      
      .Axes(xlCategory).CategoryType = xlAutomatic
    
      .Location xlLocationAsObject, lcHoja.Name
    End With
    
    lcHoja.ChartObjects.Left = 250
    lcHoja.ChartObjects.Top = 120

    Call Formato_Distribucion_x_TipoActivo(hoja)
  Else
    With lcHoja.Range("B10:D16")
      .ClearContents
      .Font.Bold = True
    End With
    
    lcHoja.Cells(10, 2).Value = "Sin Datos"
  End If
  gDB.Parametros.Clear
    
  'Selecciona la primera celda al terminar
  lcHoja.Cells(1, 1).Select

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en generar la hoja Gr�fico de Distribuci�n por Tipo de Activo.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
  gDB.Parametros.Clear
End Sub

Sub Genera_Informe_FlujosPatrimoniales(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate
Dim lCursor As hRecord
Dim lcInforme_Control_Gestion  As Class_Informe_Control_Gestion
Dim lReg            As hCollection.hFields
Dim lFila            As Integer
Dim lColumna         As Integer
Dim lFechaCierre    As Date
Dim lNRegistros     As Integer
Dim lMes            As Integer

On Error GoTo ErrProcedure

  lColumna = 3
  lFila = 12
  lNRegistros = 0
              
  gDB.Procedimiento = cfPackage & ".Flujos_Patrimoniales"
  gDB.Parametros.Clear
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "PFecha_Inicial", ePT_Fecha, fFechaInicial, ePD_Entrada
  gDB.Parametros.Add "PFecha_Final", ePT_Fecha, fFechaFinal, ePD_Entrada
  If gDB.EjecutaSP Then
    Set lCursor = gDB.Parametros("pcursor").Valor
    
    BarraProceso.Value = 0
    If lCursor.Count > 0 Then BarraProceso.Max = lCursor.Count
    
    For Each lReg In lCursor
      Call Sub_Interactivo(gRelogDB)
    
      BarraProceso.Value = lReg.Index
      lNRegistros = lNRegistros + 1
      fLibro.ActiveSheet.Cells(lFila, 2).Value = lReg("Fecha").Value
      fLibro.ActiveSheet.Cells(lFila, 3).Value = lReg("Cliente").Value
      fLibro.ActiveSheet.Cells(lFila, 4).Value = lReg("Asesor").Value
      
      With fLibro.ActiveSheet.Cells(lFila, 5)
        .NumberFormat = fFormatoMoneda
        .Value = lReg("Aportes").Value
      End With
      
      With fLibro.ActiveSheet.Cells(lFila, 6)
        .NumberFormat = fFormatoMoneda
        .Value = lReg("Retiros").Value
      End With
      
      With fLibro.ActiveSheet.Cells(lFila, 7)
        .NumberFormat = fFormatoMoneda
        .FormulaR1C1 = "=RC[-2] - RC[-1]"
      End With
      
      fLibro.ActiveSheet.Cells(lFila, 8).Value = lReg("Obs").Value
      lFila = lFila + 1
    Next
    
    If lNRegistros > 0 Then
      fLibro.ActiveSheet.Cells(lFila + 1, 3).Value = "Total"
      
      With fLibro.ActiveSheet.Cells(lFila + 1, 5)
        .NumberFormat = fFormatoMoneda
        .FormulaR1C1 = "=SUM(R[" & CStr(11 - lFila) & "]C:R[-2]C)"
      End With
      
      With fLibro.ActiveSheet.Cells(lFila + 1, 6)
        .NumberFormat = fFormatoMoneda
        .FormulaR1C1 = "=SUM(R[" & CStr(11 - lFila) & "]C:R[-2]C)"
      End With
      
      With fLibro.ActiveSheet.Cells(lFila + 1, 7)
        .NumberFormat = fFormatoMoneda
        .FormulaR1C1 = "=SUM(R[" & CStr(11 - lFila) & "]C:R[-2]C)"
      End With
    End If
  Else
    Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas en generar la hoja Reporte de Flujos Patrimoniles.", _
                      gDB.ErrMsg)
    GoTo ExitProcedure
  End If
  
  gDB.Parametros.Clear
  
  'RESUMEN POR ASESOR
  If lNRegistros > 0 Then
    Dim lFila_Asesor  As Integer
    lFila = lFila + 4
    lNRegistros = 0
    fLibro.ActiveSheet.Cells(lFila, 3).Value = "Resumen por Asesor"
    lFila = lFila + 1
    
    fLibro.ActiveSheet.Cells(lFila, 5).Value = "Aportes"
    fLibro.ActiveSheet.Cells(lFila, 6).Value = "Retiros"
    fLibro.ActiveSheet.Cells(lFila, 7).Value = "Saldo"
    
    gDB.Procedimiento = cfPackage & ".Flujos_Patrimoniales_ResumenAsesor"
    gDB.Parametros.Clear
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "PFecha_Inicial", ePT_Fecha, fFechaInicial, ePD_Entrada
    gDB.Parametros.Add "PFecha_Final", ePT_Fecha, fFechaFinal, ePD_Entrada
    If gDB.EjecutaSP Then
      lFila_Asesor = lFila
      lFila = lFila + 1
      For Each lReg In gDB.Parametros("pcursor").Valor
        lNRegistros = lNRegistros + 1
        fLibro.ActiveSheet.Cells(lFila, 4).Value = lReg("Asesor").Value
        
        With fLibro.ActiveSheet.Cells(lFila, 5)
          .NumberFormat = fFormatoMoneda
          .Value = lReg("Aportes").Value
        End With
        
        With fLibro.ActiveSheet.Cells(lFila, 6)
          .NumberFormat = fFormatoMoneda
          .Value = lReg("Retiros").Value
        End With
        
        With fLibro.ActiveSheet.Cells(lFila, 7)
          .NumberFormat = fFormatoMoneda
          .FormulaR1C1 = "=RC[-2] - RC[-1]"
        End With
        
        lFila = lFila + 1
      Next
      
      If lNRegistros > 0 Then
        fLibro.ActiveSheet.Cells(lFila + 1, 4).Value = "Total"
        
        With fLibro.ActiveSheet.Cells(lFila + 1, 5)
          .NumberFormat = fFormatoMoneda
          .FormulaR1C1 = "=SUM(R[" & CStr(lFila_Asesor - lFila) & "]C:R[-2]C)"
        End With
        
        With fLibro.ActiveSheet.Cells(lFila + 1, 6)
          .NumberFormat = fFormatoMoneda
          .FormulaR1C1 = "=SUM(R[" & CStr(lFila_Asesor - lFila) & "]C:R[-2]C)"
        End With
        
        With fLibro.ActiveSheet.Cells(lFila + 1, 7)
          .NumberFormat = fFormatoMoneda
          .FormulaR1C1 = "=SUM(R[" & CStr(lFila_Asesor - lFila) & "]C:R[-2]C)"
        End With
      End If
     Else
        Call Fnt_MsgError(eLS_ErrSystem, _
                      "Problemas en generar la hoja Reporte de Flujos Patrimoniles.", _
                      gDB.ErrMsg)
        GoTo ExitProcedure
     End If
     Call Formato_FlujosPatrimoniales(hoja)
     gDB.Parametros.Clear
  Else
       fApp_Excel.Range("B10:H11").Select
       fApp_Excel.Selection.ClearContents
       fLibro.ActiveSheet.Cells(10, 2).Value = "Sin Datos"
       fApp_Excel.Selection.Font.Bold = True
       fLibro.ActiveSheet.Cells(1, 1).Select
  End If

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en generar la hoja Reporte de Flujos Patrimoniles.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  Set lcInforme_Control_Gestion = Nothing
  gDB.Parametros.Clear
End Sub

Sub Genera_Graficos(ByVal hoja As Integer)

End Sub

Private Sub Sub_HeaderXls_Informe_Portada(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

  fLibro.Sheets(hoja).Select
  fApp_Excel.ActiveWindow.DisplayGridlines = False

  'NOMBRE HOJA
  fLibro.Worksheets.Item(hoja).Name = "Portada"

  'TITULO
  fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
  fLibro.ActiveSheet.Range("D8").Value = lPeriodo
  fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
  fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
  fLibro.ActiveSheet.Range("D7:G7").Merge
  fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
  fLibro.ActiveSheet.Range("D8:G8").Merge

  'ENCABEZADO
  lFila = 10
  lColumna = 2
  fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Patrimonios Administrados"
  lColumna = 3
  fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Totales"

End Sub

Private Sub HeaderXls_Informe_IngresosAcumulados_x_Asesor(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Ingresos Acumulados por Asesor"

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO
    lFila = 10
    lColumna = 2
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Ingresos Periodo"
    
    lFila = 11
    lColumna = 2
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Asesor"
    lColumna = lColumna + 1
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Comisi�n por Transacci�n"
    lColumna = lColumna + 1
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Derechos Bolsa"
    lColumna = lColumna + 1
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Compra Venta Divisas"
    lColumna = lColumna + 1
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Comisi�n de FFMM"
    lColumna = lColumna + 1
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Comisiones Periodo"
    lColumna = lColumna + 1
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Honoraros Periodo"
    lColumna = lColumna + 1
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Cuentas en el Ext."
    lColumna = lColumna + 1
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Total"
    

End Sub

Private Sub HeaderXls_Informe_IngresosAcumulados_x_Mes(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Ingresos Acumulados por Mes"

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO
    lFila = 10
    lColumna = 2
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Ingresos Acumulados"
    fLibro.ActiveSheet.Cells(lFila, lColumna).Font.Bold = True
    

End Sub
Private Sub HeaderXls_Informe_BPI(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "BPI"
    
'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = "Acumulado al Per�odo: " & lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO
    lFila = 10
    lColumna = 2
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "BPI"
    fLibro.ActiveSheet.Cells(lFila, lColumna).Font.Bold = True

End Sub
Private Sub HeaderXls_Informe_Individual(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Individual"

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge
'ENCABEZADO
    lFila = 10
    lColumna = 2
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Individual"
    fLibro.ActiveSheet.Cells(lFila, lColumna).Font.Bold = True


End Sub
Private Sub HeaderXls_Informe_CuadroContribucion(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Cuadro Contribuci�n"

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO
    lFila = 10
    lColumna = 2
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Patrimonios Administrados"
    lColumna = 3
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Totales"

End Sub
Private Sub HeaderXls_Informe_Resumen_x_Mes_x_Cliente(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Resumen X Mes por Cliente"

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO
    lFila = 10
    lColumna = 2
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Resumen Por Mes"

End Sub
Private Sub HeaderXls_Informe_Totales_x_Agente(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Totales por Agente"

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO
    lFila = 10
    lColumna = 2
    fLibro.ActiveSheet.Cells(lFila, lColumna).Value = "Totales por Agente"
    

End Sub
Private Sub HeaderXls_Informe_PatrimonioMensual_x_Cliente(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Patrimonio Mensual por Cliente"

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO
    fLibro.ActiveSheet.Cells(10, 2).Value = "Resumen Clientes X Mes"
    fLibro.ActiveSheet.Cells(10, 4).Value = "Periodo"
    fLibro.ActiveSheet.Cells(11, 2).Value = "Cliente"
    fLibro.ActiveSheet.Cells(11, 3).Value = "Asesor"

End Sub
Private Sub HeaderXls_Informe_Semanal(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Informe Semanal"

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True

'ENCABEZADO

End Sub
Private Sub HeaderXls_Informe_ResumenGestionBPI(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Resumen Gesti�n BPI"

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO
    fLibro.ActiveSheet.Cells(11, 2).Value = "CAPTACI�N CLIENTES NUEVOS"
    fLibro.ActiveSheet.Cells(11, 4).Value = "APORTES"
    fLibro.ActiveSheet.Cells(11, 5).Value = "RETIROS"
    fLibro.ActiveSheet.Cells(10, 8).Value = "DETALLE MES DE"
    fLibro.ActiveSheet.Cells(11, 8).Value = "CLIENTE"
    fLibro.ActiveSheet.Cells(11, 9).Value = "APORTE"
    fLibro.ActiveSheet.Cells(11, 10).Value = "CLIENTE"
    fLibro.ActiveSheet.Cells(11, 11).Value = "RETIRO"
    
    
End Sub
Private Sub HeaderXls_Informe_DetalleCliente_x_TipoInstrumento(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Det. Cliente Tipo Inst."

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO
    fLibro.ActiveSheet.Cells(11, 4).Value = "Fondos Mutuos"
    
    fLibro.ActiveSheet.Cells(12, 2).Value = "Cliente"
    fLibro.ActiveSheet.Cells(12, 3).Value = "Acciones"
    fLibro.ActiveSheet.Cells(12, 4).Value = "Renta Fija"
    fLibro.ActiveSheet.Cells(12, 5).Value = "Renta Variable"
    fLibro.ActiveSheet.Cells(12, 6).Value = "Renta Fija Directa"
    fLibro.ActiveSheet.Cells(12, 7).Value = "Intermed. Financ."
    fLibro.ActiveSheet.Cells(12, 8).Value = "Total Cliente"
    
End Sub
Private Sub HeaderXls_Grafico_Distribucion_x_TipoActivo(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Gr�fico de dist. tipo Act."

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO
    fLibro.ActiveSheet.Cells(10, 2).Value = "Resumen"
    fLibro.ActiveSheet.Cells(10, 3).Value = "Totales"
    fLibro.ActiveSheet.Cells(10, 4).Value = "%"
    fLibro.ActiveSheet.Cells(11, 2).Value = "Acciones"
    fLibro.ActiveSheet.Cells(12, 2).Value = "FFMM Renta Fija"
    fLibro.ActiveSheet.Cells(13, 2).Value = "FFMM Renta Variable"
    fLibro.ActiveSheet.Cells(14, 2).Value = "Renta Fija Directa"
    fLibro.ActiveSheet.Cells(15, 2).Value = "Intermed. Financ."
    fLibro.ActiveSheet.Cells(16, 2).Value = "Total Patrimonio Adm. $"

End Sub
Private Sub HeaderXls_Informe_FlujosPatrimoniales(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Reporte de flujos patrimoniales"

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO

fLibro.ActiveSheet.Cells(11, 2).Value = "Fecha"
fLibro.ActiveSheet.Cells(11, 3).Value = "Cliente"
fLibro.ActiveSheet.Cells(11, 4).Value = "Ejecutivo"
fLibro.ActiveSheet.Cells(11, 5).Value = "Aportes"
fLibro.ActiveSheet.Cells(11, 6).Value = "Retiros"
fLibro.ActiveSheet.Cells(11, 7).Value = "Saldo"
fLibro.ActiveSheet.Cells(11, 8).Value = "Obs"


End Sub
Private Sub HeaderXls_Graficos(ByVal hoja As Integer)
Dim lFila    As Integer
Dim lColumna As Integer

fLibro.Sheets(hoja).Select
fApp_Excel.ActiveWindow.DisplayGridlines = False

'NOMBRE HOJA
    fLibro.Worksheets.Item(hoja).Name = "Gr�ficos"

'TITULO
    fLibro.ActiveSheet.Range("D7").Value = "Informe de Control de Gesti�n"
    fLibro.ActiveSheet.Range("D8").Value = lPeriodo
    fLibro.ActiveSheet.Range("D7:D8").Font.Bold = True
    fLibro.ActiveSheet.Range("D7:G7").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D7:G7").Merge
    fLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    fLibro.ActiveSheet.Range("D8:G8").Merge

'ENCABEZADO


End Sub




Sub Formato_Portada(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
'fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate


    fLibro.ActiveSheet.Range("B11").Select
    fLibro.ActiveSheet.Range("B11").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    
   
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
    
    fLibro.ActiveSheet.Range("B11").Select
    fLibro.ActiveSheet.Range("B11").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Activate
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Font.Bold = True
    
    
    fLibro.ActiveSheet.Range("B10").Select
    fLibro.ActiveSheet.Range("B10").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True
    
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Interior.ColorIndex = 36
    fApp_Excel.Selection.Interior.Pattern = xlSolid

    fLibro.ActiveSheet.Range("B18").Select
    fLibro.ActiveSheet.Range("B18").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Font.Bold = True

   
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
    
    
    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55
    
    fLibro.ActiveSheet.Columns("B:B").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fApp_Excel.Selection.EntireColumn.AutoFit
    fLibro.ActiveSheet.Range("A1").Select
    
End Sub
Sub Formato_IngresosAcumulados_x_Asesor(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate


    fLibro.ActiveSheet.Range("B11").Select
    fLibro.ActiveSheet.Range("B11").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    
   
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
    
    fLibro.ActiveSheet.Range("B11").Select
    fLibro.ActiveSheet.Range("B11").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
        
    fApp_Excel.Selection.Interior.ColorIndex = 36
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    
        
    fApp_Excel.Range("B11").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Font.Bold = True

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Range("B11").Select
    fApp_Excel.Selection.End(xlDown).Select
    fApp_Excel.Selection.End(xlDown).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Font.Bold = True

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55
   
    fApp_Excel.Range("B11").Select
    fApp_Excel.Selection.End(xlDown).Select
    fApp_Excel.Selection.End(xlDown).Select
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55
    
    
    fApp_Excel.Range("C11:E19").Select
    fApp_Excel.Selection.NumberFormat = "#,##0.00"

    fLibro.ActiveSheet.Columns("B:B").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fApp_Excel.Selection.EntireColumn.AutoFit
    fLibro.ActiveSheet.Range("A1").Select
    
End Sub
Sub Formato_IngresosAcumulados_x_Mes(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate

Dim lFila As Integer

lFila = 11
While fLibro.ActiveSheet.Range("B" + CStr(lFila)).Value <> ""

    fLibro.ActiveSheet.Range("B" + CStr(lFila + 1)).Select
    fLibro.ActiveSheet.Range("B" + CStr(lFila + 1)).Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    
   
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
    
    fLibro.ActiveSheet.Range("C" + CStr(lFila)).Select
    fLibro.ActiveSheet.Range("C" + CStr(lFila)).Activate
    
    If fLibro.ActiveSheet.Range("D" + CStr(lFila)).Value <> "" Then
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
        fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
        
    Else
        fApp_Excel.Selection.Font.Bold = True
    End If
    fApp_Excel.Selection.Interior.ColorIndex = 36
    fApp_Excel.Selection.Interior.Pattern = xlSolid

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

'
    fApp_Excel.Range("B" + CStr(lFila + 1)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Font.Bold = True
'
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Range("B" + CStr(lFila + 1)).Select
    fApp_Excel.Selection.End(xlDown).Select
    fApp_Excel.Selection.End(xlDown).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select

    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55

    lFila = lFila + 13
Wend
    
    fLibro.ActiveSheet.Columns("B:B").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fApp_Excel.Selection.EntireColumn.AutoFit
    fLibro.ActiveSheet.Range("A1").Select
    
End Sub
Sub Formato_BPI(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate

Dim lFila As Integer

lFila = 11
While fLibro.ActiveSheet.Range("B" + CStr(lFila)).Value <> ""

    fLibro.ActiveSheet.Range("B" + CStr(lFila + 1)).Select
    fLibro.ActiveSheet.Range("B" + CStr(lFila + 1)).Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    
   
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
    
    fLibro.ActiveSheet.Range("C" + CStr(lFila)).Select
    fLibro.ActiveSheet.Range("C" + CStr(lFila)).Activate
    If fLibro.ActiveSheet.Range("D" + CStr(lFila)).Value <> "" Then
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
        fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
    
    Else
        fApp_Excel.Selection.Font.Bold = True
    End If

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    

    fLibro.ActiveSheet.Range("C" + CStr(lFila)).Select
    fLibro.ActiveSheet.Range("C" + CStr(lFila)).Activate
    If fLibro.ActiveSheet.Range("D" + CStr(lFila)).Value <> "" Then
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
        fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    Else
        fApp_Excel.Selection.Font.Bold = True
    End If
    
    fApp_Excel.Selection.Interior.ColorIndex = 36
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Range("B" + CStr(lFila + 1)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Font.Bold = True

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Range("B" + CStr(lFila + 1)).Select
    fApp_Excel.Selection.End(xlDown).Select
    fApp_Excel.Selection.End(xlDown).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select

    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True

    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55
    
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin

    lFila = lFila + 7
Wend
    
    fLibro.ActiveSheet.Columns("B:B").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fApp_Excel.Selection.EntireColumn.AutoFit
    fLibro.ActiveSheet.Range("A1").Select
    
End Sub

Sub Formato_Individual(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate

Dim lFila As Integer

lFila = 12
While fLibro.ActiveSheet.Range("B" + CStr(lFila)).Value = "Item"

    fLibro.ActiveSheet.Range("B" + CStr(lFila + 1)).Select
    fLibro.ActiveSheet.Range("B" + CStr(lFila + 1)).Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    
   
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
    
    fLibro.ActiveSheet.Range("C" + CStr(lFila)).Select
    fLibro.ActiveSheet.Range("C" + CStr(lFila)).Activate
    If fLibro.ActiveSheet.Range("D" + CStr(lFila)).Value <> "" Then
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
        fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
    
    Else
        fApp_Excel.Selection.Font.Bold = True
    End If

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    

    fLibro.ActiveSheet.Range("C" + CStr(lFila)).Select
    fLibro.ActiveSheet.Range("C" + CStr(lFila)).Activate
    If fLibro.ActiveSheet.Range("D" + CStr(lFila)).Value <> "" Then
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
        fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    Else
        fApp_Excel.Selection.Font.Bold = True
    End If
    
    fApp_Excel.Selection.Interior.ColorIndex = 36
    fApp_Excel.Selection.Interior.Pattern = xlSolid

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Range("B" + CStr(lFila + 1)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Font.Bold = True

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    lFila = lFila + 5
Wend
    
    fApp_Excel.Range("B" + CStr(lFila) + ":C" + CStr(lFila)).Select
    fApp_Excel.Range("B" + CStr(lFila) + ":C" + CStr(lFila)).HorizontalAlignment = xlLeft
    fApp_Excel.Range("B" + CStr(lFila) + ":C" + CStr(lFila)).Merge
    fApp_Excel.Selection.Font.Bold = True
    
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55
    
    fApp_Excel.Range("D" + CStr(lFila)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55

    lFila = lFila + 3

    
    fApp_Excel.Range("B" + CStr(lFila) + ":C" + CStr(lFila)).Select
    fApp_Excel.Range("B" + CStr(lFila) + ":C" + CStr(lFila)).HorizontalAlignment = xlLeft
    fApp_Excel.Range("B" + CStr(lFila) + ":C" + CStr(lFila)).Merge
    fApp_Excel.Selection.Font.Bold = True
    
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55
    
    fApp_Excel.Range("D" + CStr(lFila - 1)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Interior.ColorIndex = 36
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    
    fApp_Excel.Range("D" + CStr(lFila)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
   
    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55
    
    fLibro.ActiveSheet.Columns("B:B").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fApp_Excel.Selection.EntireColumn.AutoFit
    fLibro.ActiveSheet.Range("A1").Select
    
End Sub

Sub Formato_CuadroContribucion(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate

Dim lFila As Integer
Dim Ultima_Celda As String

lFila = 10
    fLibro.ActiveSheet.Range("C10").Select
    fLibro.ActiveSheet.Range("C10").Activate
    fApp_Excel.Selection.End(xlToRight).Select
    Ultima_Celda = Mid(fApp_Excel.ActiveCell.Address, 2, 1)
    
    fApp_Excel.Range("C10", Ultima_Celda + "16").Select

    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin

    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin

    fLibro.ActiveSheet.Range("B11").Select
    fLibro.ActiveSheet.Range("B11").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Font.Bold = True

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
    
    fLibro.ActiveSheet.Range("C10").Select
    fLibro.ActiveSheet.Range("C10").Activate
    
     If fLibro.ActiveSheet.Range("D10").Value <> "" Then
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
        fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True
    Else
        fApp_Excel.Selection.Font.Bold = True
    End If

    fApp_Excel.Selection.Interior.ColorIndex = 36
    fApp_Excel.Selection.Interior.Pattern = xlSolid

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fLibro.ActiveSheet.Range("C10:C16").Select
    fLibro.ActiveSheet.Range("C10:C16").Activate
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium


fLibro.ActiveSheet.Range("D10").Select
fLibro.ActiveSheet.Range("D10").Activate

While fApp_Excel.ActiveCell.Value <> ""
    fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Address, fApp_Excel.ActiveCell.Offset(6, 2)).Select
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 3).Address).Select
    fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 3).Address).Activate

Wend
    
    
    fLibro.ActiveSheet.Range("C18").Select
    fLibro.ActiveSheet.Range("C18").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin

    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55

    fLibro.ActiveSheet.Range("B18,C18").Select
    fLibro.ActiveSheet.Range("B18,C18").Activate
    fApp_Excel.Selection.Font.Bold = True

    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55
    
    fLibro.ActiveSheet.Columns("B:B").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fApp_Excel.Selection.EntireColumn.AutoFit
    fLibro.ActiveSheet.Range("A1").Select
    
End Sub

Sub Sub_Formato_Resumen_x_Mes(ByVal pcHoja As Excel.Worksheet _
                        , ByVal pFila_Ultima_Cliente As Long)
    Dim lFila As Integer
    Dim sRango As String

'     sRango = "D10:G10,H10:K10,L10:M10,N10:Q10"

    sRango = "D10:G10" & gstrSepList & "H10:K10" & gstrSepList & "L10:M10" & gstrSepList & "N10:Q10"
    
    lFila = 10
  
  Call pcHoja.Activate

  With pcHoja.Range(sRango)
    .Merge
    .Select
    .Font.Bold = True
    .Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Borders(xlEdgeLeft).Weight = xlMedium
  
    .Borders(xlEdgeTop).LineStyle = xlContinuous
    .Borders(xlEdgeTop).Weight = xlMedium

    .Borders(xlEdgeRight).LineStyle = xlContinuous
    .Borders(xlEdgeRight).Weight = xlMedium

    .Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Borders(xlEdgeBottom).Weight = xlMedium
  End With


  pcHoja.Range("C11").Select
  pcHoja.Range("C11").Activate
  pcHoja.Range(fApp_Excel.Selection, pcHoja.Cells(pFila_Ultima_Cliente, fApp_Excel.Selection.Column)).Select 'fApp_Excel.Selection.End(xlDown)
  pcHoja.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
  With fApp_Excel.Selection
    .Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Borders(xlEdgeLeft).Weight = xlMedium

    .Borders(xlEdgeTop).LineStyle = xlContinuous
    .Borders(xlEdgeTop).Weight = xlMedium

    .Borders(xlEdgeRight).LineStyle = xlContinuous
    .Borders(xlEdgeRight).Weight = xlMedium

    .Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Borders(xlEdgeBottom).Weight = xlMedium
    
    .Borders(xlInsideVertical).LineStyle = xlContinuous
    .Borders(xlInsideVertical).Weight = xlThin
    
    .Borders(xlInsideHorizontal).LineStyle = xlContinuous
    .Borders(xlInsideHorizontal).Weight = xlThin
  End With
    
  pcHoja.Range("B11").Select
  pcHoja.Range("B11").Activate
  pcHoja.Range(fApp_Excel.Selection, pcHoja.Cells(pFila_Ultima_Cliente, fApp_Excel.Selection.Column)).Select
  With fApp_Excel.Selection
    .Font.Bold = True
    
    
    .Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Borders(xlEdgeLeft).Weight = xlMedium

    .Borders(xlEdgeTop).LineStyle = xlContinuous
    .Borders(xlEdgeTop).Weight = xlMedium

    .Borders(xlEdgeRight).LineStyle = xlContinuous
    .Borders(xlEdgeRight).Weight = xlMedium

    .Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Borders(xlEdgeBottom).Weight = xlMedium
        
    .Borders(xlInsideHorizontal).LineStyle = xlContinuous
    .Borders(xlInsideHorizontal).Weight = xlThin
  End With

  pcHoja.Range("B11").Select
  pcHoja.Range("B11").Activate
  fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
  With fApp_Excel.Selection
    .Font.Bold = True
    
    .Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Borders(xlEdgeLeft).Weight = xlMedium

    .Borders(xlEdgeTop).LineStyle = xlContinuous
    .Borders(xlEdgeTop).Weight = xlMedium

    .Borders(xlEdgeRight).LineStyle = xlContinuous
    .Borders(xlEdgeRight).Weight = xlMedium

    .Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Borders(xlEdgeBottom).Weight = xlMedium
    
    .Interior.ColorIndex = 36
    .Interior.Pattern = xlSolid
  End With
  
  pcHoja.Range("C11").Select
  pcHoja.Range("C11").Activate
  pcHoja.Range(fApp_Excel.Selection, pcHoja.Cells(pFila_Ultima_Cliente, fApp_Excel.Selection.Column)).Select
  'fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
  With fApp_Excel.Selection
    .Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Borders(xlEdgeLeft).Weight = xlMedium

    .Borders(xlEdgeTop).LineStyle = xlContinuous
    .Borders(xlEdgeTop).Weight = xlMedium

    .Borders(xlEdgeRight).LineStyle = xlContinuous
    .Borders(xlEdgeRight).Weight = xlMedium

    .Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Borders(xlEdgeBottom).Weight = xlMedium
  End With
    
  'Dim Ultima_Celda As String
  'Dim Ultima_lFila As String
  
  fApp_Excel.Range("B11").Select
  fApp_Excel.Selection.End(xlDown).Select
  'Ultima_Celda = fApp_Excel.ActiveCell.Address
  'Ultima_lFila = Mid(Ultima_Celda, 4)
  
  sRango = "D11:G" & pFila_Ultima_Cliente & gstrSepList & "H11:K" & pFila_Ultima_Cliente & gstrSepList & "L11:M" & pFila_Ultima_Cliente & gstrSepList & "N11:Q" & pFila_Ultima_Cliente & gstrSepList & "R11:R" & pFila_Ultima_Cliente & gstrSepList & "S11:s" & pFila_Ultima_Cliente
  
  ' pcHoja.Range("D11:G" & pFila_Ultima_Cliente & ",H11:K" & pFila_Ultima_Cliente & ",L11:M" & pFila_Ultima_Cliente & ",N11:Q" & pFila_Ultima_Cliente & ",R11:R" & pFila_Ultima_Cliente & ",S11:s" & pFila_Ultima_Cliente).Select
  
  pcHoja.Range(sRango).Select
  
  With fApp_Excel.Selection
    .Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Borders(xlEdgeLeft).Weight = xlMedium

    .Borders(xlEdgeTop).LineStyle = xlContinuous
    .Borders(xlEdgeTop).Weight = xlMedium

    .Borders(xlEdgeRight).LineStyle = xlContinuous
    .Borders(xlEdgeRight).Weight = xlMedium

    .Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Borders(xlEdgeBottom).Weight = xlMedium
  End With
    
    
  fApp_Excel.Range("B11").Select
  fApp_Excel.Selection.End(xlDown).Select
  fApp_Excel.Selection.End(xlDown).Select

  pFila_Ultima_Cliente = pFila_Ultima_Cliente + 2

  With pcHoja.Range(pcHoja.Cells(pFila_Ultima_Cliente, 2), pcHoja.Cells(pFila_Ultima_Cliente, 3))
    .Merge
    .Font.Bold = True

    .Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Borders(xlEdgeLeft).Weight = xlMedium

    .Borders(xlEdgeTop).LineStyle = xlContinuous
    .Borders(xlEdgeTop).Weight = xlMedium

    .Borders(xlEdgeRight).LineStyle = xlContinuous
    .Borders(xlEdgeRight).Weight = xlMedium

    .Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Borders(xlEdgeBottom).Weight = xlMedium

    .Interior.ColorIndex = 35
    .Interior.Pattern = xlSolid
    .Font.ColorIndex = 55
  End With

    'pcHoja.Range(fApp_Excel.ActiveCell.Offset(0, 1).Address).Activate

  pcHoja.Range(pcHoja.Cells(pFila_Ultima_Cliente, 4), pcHoja.Cells(pFila_Ultima_Cliente, 4).Offset(0, 15)).Select
  With fApp_Excel.Selection
    .Font.Bold = True
    .Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Borders(xlEdgeLeft).Weight = xlMedium

    .Borders(xlEdgeTop).LineStyle = xlContinuous
    .Borders(xlEdgeTop).Weight = xlMedium

    .Borders(xlEdgeRight).LineStyle = xlContinuous
    .Borders(xlEdgeRight).Weight = xlMedium

    .Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Borders(xlEdgeBottom).Weight = xlMedium

    .Borders(xlInsideVertical).LineStyle = xlContinuous
    .Borders(xlInsideVertical).Weight = xlThin

    .Interior.ColorIndex = 35
    .Interior.Pattern = xlSolid
    .Font.ColorIndex = 55
  End With
  
  fApp_Excel.Range("B11").Select
  fApp_Excel.Selection.End(xlDown).Select
  fApp_Excel.Selection.End(xlDown).Select
  
  'Ultima_Celda = fApp_Excel.ActiveCell.Address
  'Ultima_lFila = Mid(Ultima_Celda, 4)
    
    
'    pcHoja.Range("D" & pFila_Ultima_Cliente & ":G" & pFila_Ultima_Cliente & ",H" & pFila_Ultima_Cliente & ":K" & pFila_Ultima_Cliente & ",L" & pFila_Ultima_Cliente & ":M" & pFila_Ultima_Cliente & ",N" & pFila_Ultima_Cliente & ":Q" & pFila_Ultima_Cliente & ",R" & pFila_Ultima_Cliente & ":R" & pFila_Ultima_Cliente & ",S" & pFila_Ultima_Cliente & ":S" & pFila_Ultima_Cliente).Select
'    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
'    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
'
'    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
'    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
'
'    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
'    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
'
'    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
'    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    
    pcHoja.Columns("B:B").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fApp_Excel.Selection.EntireColumn.AutoFit
    pcHoja.Range("A1").Select
    
End Sub

Sub Formato_Total_x_Agente(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate

Dim lFila As Integer
Dim Ultima_Celda As String

lFila = 10

    fLibro.ActiveSheet.Range("C10:F10,G10:J10,K10:L10,M10:P10").Merge
    fLibro.ActiveSheet.Range("C10:F10,G10:J10,K10:L10,M10:P10").Select
    fApp_Excel.Selection.Font.Bold = True
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fLibro.ActiveSheet.Range("C11").Select
    fLibro.ActiveSheet.Range("C11").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
    
    fLibro.ActiveSheet.Range("B11").Select
    fLibro.ActiveSheet.Range("B11").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Font.Bold = True
    
    
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin

    fLibro.ActiveSheet.Range("B11").Select
    fLibro.ActiveSheet.Range("B11").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True
    
    
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    

    fApp_Excel.Selection.Interior.ColorIndex = 36
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    
    
    Dim Ultima_lFila As String
    
    fApp_Excel.Range("B11").Select
    fApp_Excel.Selection.End(xlDown).Select
    Ultima_Celda = fApp_Excel.ActiveCell.Address
    Ultima_lFila = Mid(Ultima_Celda, 4)
    
    fLibro.ActiveSheet.Range("C11:F" + Ultima_lFila + ",G11:J" + Ultima_lFila + ",K11:L" + Ultima_lFila + ",M11:P" + Ultima_lFila + ",Q11:Q" + Ultima_lFila + ",R11:R" + Ultima_lFila).Select
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    
    fApp_Excel.Range("B11").Select
    fApp_Excel.Selection.End(xlDown).Select
    fApp_Excel.Selection.End(xlDown).Select
    
    fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Address, fApp_Excel.ActiveCell.Offset(0, 1)).Select
    fApp_Excel.Selection.Font.Bold = True
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55
    
    fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 1).Address).Activate

    fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Address, fApp_Excel.ActiveCell.Offset(0, 15)).Select
    fApp_Excel.Selection.Font.Bold = True
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Interior.ColorIndex = 35
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    fApp_Excel.Selection.Font.ColorIndex = 55
    
    fApp_Excel.Range("B11").Select
    fApp_Excel.Selection.End(xlDown).Select
    fApp_Excel.Selection.End(xlDown).Select
    
    Ultima_Celda = fApp_Excel.ActiveCell.Address
    Ultima_lFila = Mid(Ultima_Celda, 4)
    
    fLibro.ActiveSheet.Range("C" + Ultima_lFila + ":F" + Ultima_lFila + ",G" + Ultima_lFila + ":J" + Ultima_lFila + ",K" + Ultima_lFila + ":L" + Ultima_lFila + ",M" + Ultima_lFila + ":P" + Ultima_lFila + ",Q" + Ultima_lFila + ":Q" + Ultima_lFila + ",R" + Ultima_lFila + ":R" + Ultima_lFila).Select
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fLibro.ActiveSheet.Columns("B:B").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fApp_Excel.Selection.EntireColumn.AutoFit
    fLibro.ActiveSheet.Range("A1").Select
    
End Sub


Private Sub Formato_PatrimonioMensual_x_Cliente(ByVal pHoja As Integer, pCantidad_Asesores)
Dim lcHoja           As Excel.Worksheet
'---------------------------------------------------
Dim lFila As Integer
Dim lFila_Final As Integer
Dim i As Long
Dim Ultima_Celda As String

On Error GoTo ErrProcedure

  Set lcHoja = fLibro.Sheets(pHoja)
  With lcHoja
    .Select
    .Activate
  End With

    lcHoja.Range("D10").Select
    fApp_Excel.Selection.Font.Bold = True
    
    '------------------------------------------------------------------------------------------------------
    '-- caption
    '------------------------------------------------------------------------------------------------------
    lcHoja.Range("B11").Select
    lcHoja.Range("B11").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    lcHoja.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Font.Bold = True
    
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Interior.ColorIndex = 36
    fApp_Excel.Selection.Interior.Pattern = xlSolid
    '------------------------------------------------------------------------------------------------------
    
    lcHoja.Range("C11").Select
    lcHoja.Range("C11").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    
    
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
    
    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
    
    lcHoja.Range("B11").Select
    lcHoja.Range("B11").Activate
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
    lcHoja.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Font.Bold = True
    
    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
    fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
    fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin

'    For i = 2 To pCantidad_Asesores
'      lFila = fApp_Excel.Selection.Rows.Count + fApp_Excel.Selection.Row + 1
'      lFila_Final = lFila
'
'      Do While Not lcHoja.Cells(lFila_Final + 1, 2).Value = ""
'        lFila_Final = lFila_Final + 1
'      Loop
'
'      fApp_Excel.Range("C" & lFila, "C" & lFila_Final).Select
'      lcHoja.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
'
'      fApp_Excel.Selection.Font.Bold = True
'
'      fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
'      fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
'
'      fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
'      fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
'
'      fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
'      fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
'
'      fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
'      fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
'
'      fApp_Excel.Range("B" & lFila, "B" & lFila_Final).Select
'
'      fApp_Excel.Selection.Font.Bold = True
'      fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
'      fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
'
'      fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
'      fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
'
'      fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
'      fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
'
'      fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
'      fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
'    Next


'    fApp_Excel.Selection.End(xlDown).Select
'    lFila = fApp_Excel.Selection.Row
'    lcHoja.Range(fApp_Excel.ActiveCell.Address, fApp_Excel.Selection.End(xlToRight)).Select
'    lcHoja.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
'
'    fApp_Excel.Selection.Font.Bold = True
'    fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
'    fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
'
'    fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
'    fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
'
'    fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
'    fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
'
'    fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
'    fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
'
'    fApp_Excel.Selection.Interior.ColorIndex = 35
'    fApp_Excel.Selection.Interior.Pattern = xlSolid
'    fApp_Excel.Selection.Font.ColorIndex = 55
    
'    lcHoja.Range(fApp_Excel.ActiveCell.Offset(0, 1).Address).Activate
'    If fApp_Excel.ActiveCell.Offset(0, 1).Value <> "" Then
'      lcHoja.Range(fApp_Excel.ActiveCell.Address, fApp_Excel.Selection.End(xlToRight)).Select
'      lcHoja.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown).Offset(1, 0).Address).Select
'      fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
'      fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
'    End If
        
    lcHoja.Columns("B:B").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fApp_Excel.Selection.EntireColumn.AutoFit
    lcHoja.Range("A1").Select
    
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en generar la hoja Patrimonio Mensual por Cliente.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:

End Sub




Sub Formato_ResumenGestionBPI(ByVal pHoja As Integer, pCuadro As Integer, pLargo)
fLibro.Sheets(pHoja).Select
fLibro.Sheets(pHoja).Activate

Dim lFila As Integer
Dim Ultima_Celda As String

lFila = 10

    fLibro.ActiveSheet.Range("D10:K10").Select
    fApp_Excel.Selection.Font.Bold = True
    
    If pCuadro = 1 Then
        fApp_Excel.Range("B11").Select
        fApp_Excel.Selection.End(xlDown).Select
        fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 3).Address).Activate
        fLibro.ActiveSheet.Range("B11", fApp_Excel.ActiveCell.Address).Select
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
        
        If fLibro.ActiveSheet.Range("B11").Value <> "" Then
            fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
            fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
        End If
        
        fLibro.ActiveSheet.Range("B11").Select
        fLibro.ActiveSheet.Range("B11").Activate
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
        fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Font.Bold = True
        
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
            
        
        fApp_Excel.Range("B11").Select
        fApp_Excel.Selection.End(xlDown).Select
        fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 1).Address).Activate
        fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Address, "C12").Select
        
        fApp_Excel.Selection.Font.Bold = True
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    
        fLibro.ActiveSheet.Range("B11:C11").Merge
        fLibro.ActiveSheet.Range("B11:C11").Select
        fApp_Excel.Selection.Font.Bold = True
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        
        fApp_Excel.Range("B11").Select
        fApp_Excel.Selection.End(xlDown).Select
        fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 2).Address).Activate
        fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Address, "D11").Select
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    
        fApp_Excel.Range("B11").Select
        fApp_Excel.Selection.End(xlDown).Select
        fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 3).Address).Activate
        fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Address, "D11").Select
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        
        fLibro.ActiveSheet.Range("B11:C11").Merge
        fLibro.ActiveSheet.Range("B11:C11,D11:E11").Select
        fApp_Excel.Selection.Font.Bold = True
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlDouble
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fApp_Excel.Selection.Interior.ColorIndex = 36
        fApp_Excel.Selection.Interior.Pattern = xlSolid
        
   
    Else
       If pCuadro = 2 Then
          fApp_Excel.Range("H11,I11,J11,K11").Select
          fApp_Excel.Selection.Font.Bold = True
          
          fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlDouble
          fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
      
          fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlDouble
          fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
      
          fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlDouble
          fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
      
          fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlDouble
          fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
          
          fApp_Excel.Selection.Interior.ColorIndex = 36
          fApp_Excel.Selection.Interior.Pattern = xlSolid
          If fApp_Excel.Range("H12").Value <> "" Or fApp_Excel.Range("J12").Value <> "" Then
             Dim lFila1 As Integer
             Dim lFila2 As Integer
             Dim lFila_Final As Integer
             fApp_Excel.Range("H11").Select
             If fApp_Excel.Range("H12").Value <> "" Then
                fApp_Excel.Selection.End(xlDown).Select
             End If
             lFila1 = CInt(Mid(fApp_Excel.ActiveCell.Address, 4))
             fApp_Excel.Range("J11").Select
             If fApp_Excel.Range("J12").Value <> "" Then
                fApp_Excel.Selection.End(xlDown).Select
             End If
             lFila2 = CInt(Mid(fApp_Excel.ActiveCell.Address, 4))
             lFila_Final = IIf(lFila1 >= lFila2, lFila1, lFila2)
             fApp_Excel.Range("H11:K" + CStr(pLargo)).Select

              fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
          
              fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
          
              fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
          
              fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
              
              If pLargo > 12 Then
                  fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
                  fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
              End If
              
              lFila_Final = pLargo + 1
              fLibro.ActiveSheet.Cells(lFila_Final, 8).Value = "Total"
              
              fLibro.ActiveSheet.Cells(lFila_Final, 9).FormulaR1C1 = "=SUM(R[" + CStr(11 - lFila_Final) + "]C:R[-1]C)"
              fLibro.ActiveSheet.Cells(lFila_Final, 9).NumberFormat = fFormatoMoneda
              
              fLibro.ActiveSheet.Cells(lFila_Final, 11).FormulaR1C1 = "=SUM(R[" + CStr(11 - lFila_Final) + "]C:R[-1]C)"
              fLibro.ActiveSheet.Cells(lFila_Final, 11).NumberFormat = fFormatoMoneda
              
              fApp_Excel.Range("H" + CStr(lFila_Final), "K" + CStr(lFila_Final)).Select
              fApp_Excel.Selection.Font.Bold = True
              
              fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
          
              fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
          
              fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
          
              fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
             
              fApp_Excel.Selection.Interior.ColorIndex = 35
              fApp_Excel.Selection.Interior.Pattern = xlSolid
              fApp_Excel.Selection.Font.ColorIndex = 55
              
              
              fApp_Excel.Range("H12:H" + CStr(lFila_Final) + ", I12:I" + CStr(lFila_Final) + ", J12:J" + CStr(lFila_Final) + ", K12:K" + CStr(lFila_Final)).Select
              
              fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
          
              fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
          
              fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
          
              fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlDouble
              fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
             
            End If
       End If
    End If

    fLibro.ActiveSheet.Columns("B:B").Select
    fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
    fApp_Excel.Selection.EntireColumn.AutoFit
    fLibro.ActiveSheet.Range("A1").Select
    
End Sub



Sub Formato_DetalleCliente_x_TipoInstrumento(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate

Dim lFila As Integer
Dim Ultima_Celda As String

lFila = 10

      fLibro.ActiveSheet.Range("B11:H12").Select
      fApp_Excel.Selection.Font.Bold = True
    
      fApp_Excel.Range("B12").Select
      fApp_Excel.Selection.End(xlDown).Select
      fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 6).Address).Activate
      fLibro.ActiveSheet.Range("B12", fApp_Excel.ActiveCell.Address).Select
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
        
        If fLibro.ActiveSheet.Range("B13").Value <> "" Then
            fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
            fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
        End If
        
        fLibro.ActiveSheet.Range("D11:E11").Select
        fLibro.ActiveSheet.Range("D11:E11").Merge
        fLibro.ActiveSheet.Range("D11:E11").HorizontalAlignment = xlCenter
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fLibro.ActiveSheet.Range("B12:H12").Select
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
            
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
            
        fApp_Excel.Selection.Interior.ColorIndex = 36
        fApp_Excel.Selection.Interior.Pattern = xlSolid
            
        
        fLibro.ActiveSheet.Range("B12").Select
        fLibro.ActiveSheet.Range("B12").Activate
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlDown)).Select
        fApp_Excel.Selection.Font.Bold = True
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

        fApp_Excel.Range("B12").Select
        fApp_Excel.Selection.End(xlDown).Select
        fLibro.ActiveSheet.Range(fApp_Excel.Selection, fApp_Excel.ActiveCell.Offset(0, 6).Address).Select
        fApp_Excel.Selection.Font.Bold = True
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fApp_Excel.Selection.Interior.ColorIndex = 35
        fApp_Excel.Selection.Interior.Pattern = xlSolid
        fApp_Excel.Selection.Font.ColorIndex = 55

        fApp_Excel.Range("B12").Select
        fApp_Excel.Selection.End(xlDown).Select
        fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 3).Address).Activate
        fLibro.ActiveSheet.Range("D12", fApp_Excel.ActiveCell.Address).Select
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fApp_Excel.Range("B12").Select
        fApp_Excel.Selection.End(xlDown).Select
        fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 6).Address).Activate
        fLibro.ActiveSheet.Range("H12", fApp_Excel.ActiveCell.Address).Select
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
       fLibro.ActiveSheet.Columns("B:B").Select
       fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
       fApp_Excel.Selection.EntireColumn.AutoFit
       fLibro.ActiveSheet.Range("A1").Select
    
End Sub


Sub Formato_Distribucion_x_TipoActivo(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate

Dim lFila As Integer
Dim Ultima_Celda As String

lFila = 10

      fLibro.ActiveSheet.Range("B10:D10,B10:B16,B16:D16").Select
      fApp_Excel.Selection.Font.Bold = True
    
      fApp_Excel.Range("B10:D16").Select
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
        
        fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
        
        fLibro.ActiveSheet.Range("B10:D10,B16:D16,B10:B16,B10,B16").Select
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fLibro.ActiveSheet.Range("B10:D10").Select
        fApp_Excel.Selection.Interior.ColorIndex = 36
        fApp_Excel.Selection.Interior.Pattern = xlSolid

        fApp_Excel.Range("B16:D16").Select
        fApp_Excel.Selection.Interior.ColorIndex = 35
        fApp_Excel.Selection.Interior.Pattern = xlSolid
        fApp_Excel.Selection.Font.ColorIndex = 55

        
       fLibro.ActiveSheet.Columns("B:B").Select
       fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
       fApp_Excel.Selection.EntireColumn.AutoFit

        fLibro.ActiveSheet.Columns("D:D").ColumnWidth = 5.86
        fApp_Excel.Range("D10").Select
        fApp_Excel.Selection.HorizontalAlignment = xlCenter
        fLibro.ActiveSheet.Range("A1").Select
    
End Sub

Sub Formato_FlujosPatrimoniales(ByVal hoja As Integer)
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Select
fLibro.Sheets(hoja).Activate

Dim lFila As Integer
Dim Ultima_Celda As String

lFila = 10

      fLibro.ActiveSheet.Range("B11:H11").Select
      fApp_Excel.Selection.Font.Bold = True
    
      fApp_Excel.Range("B11").Select
      fApp_Excel.Selection.End(xlDown).Select
      fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 6).Address).Activate
      fLibro.ActiveSheet.Range("B11", fApp_Excel.ActiveCell.Address).Select
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
        
        If fLibro.ActiveSheet.Range("B12").Value <> "" Then
            fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
            fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
        End If

        fLibro.ActiveSheet.Range("B11:H11").Select

        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium

        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium

        fApp_Excel.Selection.Interior.ColorIndex = 36
        fApp_Excel.Selection.Interior.Pattern = xlSolid


        fLibro.ActiveSheet.Range("B11").Select
        fLibro.ActiveSheet.Range("B11").Activate
        fApp_Excel.Selection.End(xlDown).Select
        
        fApp_Excel.Range(fApp_Excel.ActiveCell.Offset(2, 1).Address).Select
        Ultima_Celda = fApp_Excel.ActiveCell.Address
        
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.ActiveCell.Offset(0, 4).Address).Select
        fApp_Excel.Selection.Font.Bold = True

        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin

        fApp_Excel.Selection.Interior.ColorIndex = 35
        fApp_Excel.Selection.Interior.Pattern = xlSolid
        fApp_Excel.Selection.Font.ColorIndex = 55


        fApp_Excel.Range(Ultima_Celda).Select
        fApp_Excel.Range(fApp_Excel.ActiveCell.Offset(3, 0).Address, fApp_Excel.ActiveCell.Offset(4, 4).Address).Select
        fApp_Excel.Selection.Font.Bold = True

        fApp_Excel.Range(Ultima_Celda).Select
        fApp_Excel.Range(fApp_Excel.ActiveCell.Offset(4, 1).Address).Select
        
        fApp_Excel.Range(fApp_Excel.ActiveCell.Offset(0, 1).Address, fApp_Excel.ActiveCell.Offset(0, 3).Address).Select
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
        
        fApp_Excel.Selection.Interior.ColorIndex = 36
        fApp_Excel.Selection.Interior.Pattern = xlSolid
        
        fApp_Excel.Range(Ultima_Celda).Select
        fApp_Excel.Range(fApp_Excel.ActiveCell.Offset(4, 1).Address).Select

        Dim Celda_Asesores As String
        Celda_Asesores = fApp_Excel.ActiveCell.Offset(1, 0).Address
        fApp_Excel.Range(Celda_Asesores).Select
        If fApp_Excel.Range(fApp_Excel.ActiveCell.Offset(1, 0).Address).Value <> "" Then
           fLibro.ActiveSheet.Range(Celda_Asesores).Select
           fLibro.ActiveSheet.Range(Celda_Asesores).Activate
           fApp_Excel.Selection.End(xlDown).Select
           fLibro.ActiveSheet.Range(fApp_Excel.ActiveCell.Offset(0, 3).Address, Celda_Asesores).Select
           fApp_Excel.Selection.Borders(xlInsideHorizontal).LineStyle = xlContinuous
           fApp_Excel.Selection.Borders(xlInsideHorizontal).Weight = xlThin
        Else
           fLibro.ActiveSheet.Range(Celda_Asesores, fApp_Excel.ActiveCell.Offset(0, 3).Address).Select
        End If
            
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
        
        
        
        fApp_Excel.Range(Celda_Asesores).Select
        Ultima_Celda = Celda_Asesores
        If fApp_Excel.Range(fApp_Excel.ActiveCell.Offset(1, 0).Address).Value <> "" Then
        fApp_Excel.Selection.End(xlDown).Select
           Ultima_Celda = fApp_Excel.ActiveCell.Address
           fLibro.ActiveSheet.Range(Celda_Asesores, Ultima_Celda).Select
        End If
            
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        
        fApp_Excel.Range(Ultima_Celda).Select
        fApp_Excel.Range(fApp_Excel.ActiveCell.Offset(2, 0).Address, fApp_Excel.ActiveCell.Offset(2, 3).Address).Select
        fApp_Excel.Selection.Font.Bold = True
        
        fApp_Excel.Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeTop).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeRight).Weight = xlMedium
    
        fApp_Excel.Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlEdgeBottom).Weight = xlMedium
        
        fApp_Excel.Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
        fApp_Excel.Selection.Borders(xlInsideVertical).Weight = xlThin
        
        fApp_Excel.Selection.Interior.ColorIndex = 35
        fApp_Excel.Selection.Interior.Pattern = xlSolid
        fApp_Excel.Selection.Font.ColorIndex = 55
        
        fLibro.ActiveSheet.Columns("B:B").Select
        fApp_Excel.Range(fApp_Excel.Selection, fApp_Excel.Selection.End(xlToRight)).Select
        fApp_Excel.Selection.EntireColumn.AutoFit

        fLibro.ActiveSheet.Columns("B:B").ColumnWidth = 13.86
        fApp_Excel.Range("D10").Select
        fApp_Excel.Selection.HorizontalAlignment = xlCenter
        fLibro.ActiveSheet.Range("A1").Select
    
End Sub

Private Sub Toolbar_Chequear_Propietario_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "CHK"
      Call Sub_Busca_Cuentas_Propiedades
  End Select
End Sub

Private Sub Sub_Busca_Cuentas_Propiedades()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lCursor As hCollection.hRecord
Dim lReg As hCollection.hFields
Dim lId_Asesor As String
Dim lId_Cliente As String
Dim lId_Grupos_Cuentas As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
  lId_Asesor = IIf(lId_Asesor = cCmbKBLANCO, "", lId_Asesor)
  
  lId_Cliente = Fnt_ComboSelected_KEY(Cmb_Clientes)
  lId_Cliente = IIf(lId_Cliente = cCmbKBLANCO, "", lId_Cliente)
  
  lId_Grupos_Cuentas = Fnt_ComboSelected_KEY(Cmb_Grupos_Cuentas)
  lId_Grupos_Cuentas = IIf(lId_Grupos_Cuentas = cCmbKBLANCO, "", lId_Grupos_Cuentas)
  
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_asesor").Valor = lId_Asesor
    .Campo("Id_Cliente").Valor = lId_Cliente
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar(pEnVista:=False, pId_Grupo_Cuenta:=lId_Grupos_Cuentas) Then
      For Each lReg In .Cursor
        Call Sub_Llena_Grilla_Cuentas(lReg("id_cuenta").Value)
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With

  Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Sub Sub_Llena_Grilla_Cuentas(pId_Cuenta As String)
Dim lFila As Long
Dim lCol As Long
  
  With Grilla_Cuentas
    If .Rows > 0 Then
      lCol = Grilla_Cuentas.ColIndex("CHK")
      For lFila = 1 To .Rows - 1
        If GetCell(Grilla_Cuentas, lFila, "colum_pk") = pId_Cuenta Then
          .Cell(flexcpChecked, lFila, lCol) = flexChecked
          Exit For
        End If
      Next
    End If
  End With
  
End Sub

Private Sub Toolbar_Chequeo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
       Call Sub_CambiaCheck(True)
    Case "SEL_NOTHING"
       Call Sub_CambiaCheck(False)
  End Select
  
End Sub

Private Sub Sub_CambiaCheck(pValor As Boolean)
Dim lLinea As Long
Dim lCol As Long
  
  lCol = Grilla_Cuentas.ColIndex("CHK")
  If pValor Then
    For lLinea = 1 To Grilla_Cuentas.Rows - 1
      Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexChecked
    Next
  Else
    For lLinea = 1 To Grilla_Cuentas.Rows - 1
      Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexUnchecked
    Next
  End If
  
End Sub

Private Function Nombre_Mes(Month_Name As String) As String

Select Case Month_Name
  Case "January"
    Nombre_Mes = "Enero"
  Case "February"
    Nombre_Mes = "Febrero"
  Case "March"
    Nombre_Mes = "Marzo"
  Case "April"
    Nombre_Mes = "Abril"
  Case "May"
    Nombre_Mes = "Mayo"
  Case "June"
    Nombre_Mes = "Junio"
  Case "July"
    Nombre_Mes = "Julio"
  Case "August"
    Nombre_Mes = "Agosto"
  Case "September"
    Nombre_Mes = "Septiembre"
  Case "October"
    Nombre_Mes = "Octubre"
  Case "November"
    Nombre_Mes = "Noviembre"
  Case "December"
    Nombre_Mes = "Diciembre"
  Case Else
    Nombre_Mes = Month_Name
End Select

End Function
