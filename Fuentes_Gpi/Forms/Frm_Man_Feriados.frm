VERSION 5.00
Object = "{82392BA0-C18D-11D2-B0EA-00A024695830}#1.0#0"; "ticaldr8.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Man_Feriados 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de D�as Festivos"
   ClientHeight    =   4950
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4965
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4950
   ScaleWidth      =   4965
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4965
      _ExtentX        =   8758
      _ExtentY        =   635
      ButtonWidth     =   1852
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Agregar"
            Key             =   "SAVE"
            Description     =   "Agrega fechas festivas"
            Object.ToolTipText     =   "Agrega un Dia festivo"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Eliminar"
            Key             =   "DEL"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Elimina un Dia Festivo"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Modificar"
            Key             =   "UPD"
            Description     =   "Modifica un d�a Festivo"
            Object.ToolTipText     =   "Modifica un D�a Festivo"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin TDBCalendar6Ctl.TDBCalendar TDBCalendario 
      Height          =   4425
      Left            =   60
      TabIndex        =   2
      Top             =   450
      Width           =   4815
      _Version        =   65536
      _ExtentX        =   8493
      _ExtentY        =   7805
      ShowContextMenu =   0   'False
      Appearance      =   3
      AutoSize        =   0   'False
      BorderStyle     =   1
      BackColor       =   -2147483643
      StartOfMonth    =   0
      EmptyRows       =   0
      Enabled         =   -1  'True
      FirstMonth      =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      LineColors0     =   -2147483632
      LineStyles0     =   0
      LineColors1     =   -2147483632
      LineStyles1     =   0
      LineColors2     =   -2147483632
      LineStyles2     =   0
      LineColors3     =   -2147483632
      LineStyles3     =   0
      LineColors4     =   -2147483632
      LineStyles4     =   0
      LineColors5     =   -2147483632
      LineStyles5     =   0
      LineColors6     =   -2147483632
      LineStyles6     =   0
      MarginBottom    =   0
      MarginTitle     =   0
      MarginTop       =   0
      MarginLeft      =   0
      MarginRight     =   0
      MarginWidth     =   0
      MarginHeight    =   0
      MaxDate         =   5373484
      MinDate         =   2453737
      MousePointer    =   0
      YearType        =   0
      MonthRows       =   1
      MonthCols       =   1
      MultiSelect     =   0
      NavOrientation  =   0
      ScrollRate      =   1
      ScrollTipAlign  =   0
      SelEdgeWidth    =   0
      SelectStyle     =   0
      SelectWhat      =   0
      ShowMenu        =   0   'False
      ShowNavigator   =   3
      ShowScrollTip   =   -1  'True
      ShowTrailing    =   0   'False
      StartOfWeek     =   2
      Templates       =   3
      TipInterval     =   500
      TitleHeight     =   0
      TitleFormat     =   "mmmm yyy"
      ValueIsNull     =   0   'False
      Value           =   2453927
      OverrideTipText =   ""
      TopDate         =   2453918
      AttribStyles    =   "Frm_Man_Feriados.frx":0000
      StyleSets       =   "Frm_Man_Feriados.frx":00C0
      CtrlType        =   8
      CtrlValue       =   "CtrlStyle"
      DayType         =   8
      DayValue        =   "DayStyle"
      TitleType       =   8
      TitleValue      =   "TitleStyle"
      WeekType        =   8
      WeekValue       =   "WeekStyle"
      TrailType       =   8
      TrailValue      =   "TrailAttrib"
      SelType         =   8
      SelValue        =   "SelAttrib"
      WeekRests0      =   127
      WeekReflect0    =   2
      WeekCaption0    =   "Dom"
      WeekAttrib0Type =   8
      WeekAttrib0Value=   "SunAttrib"
      WeekRests1      =   0
      WeekReflect1    =   0
      WeekCaption1    =   "Lun"
      WeekAttrib1Type =   1
      WeekRests2      =   0
      WeekReflect2    =   0
      WeekCaption2    =   "Mar"
      WeekAttrib2Type =   1
      WeekRests3      =   0
      WeekReflect3    =   0
      WeekCaption3    =   "Mi�"
      WeekAttrib3Type =   1
      WeekRests4      =   0
      WeekReflect4    =   0
      WeekCaption4    =   "Jue"
      WeekAttrib4Type =   1
      WeekRests5      =   0
      WeekReflect5    =   0
      WeekCaption5    =   "Vie"
      WeekAttrib5Type =   1
      WeekRests6      =   127
      WeekReflect6    =   2
      WeekCaption6    =   "S�b"
      WeekAttrib6Type =   8
      WeekAttrib6Value=   "SunAttrib"
      HolidayStyles   =   "Frm_Man_Feriados.frx":0244
      UserStyles      =   ""
      Key             =   "Frm_Man_Feriados.frx":0260
      OLEDragMode     =   0
      OLEDropMode     =   0
   End
End
Attribute VB_Name = "Frm_Man_Feriados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("UPD").Image = cBoton_Modificar
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  Call Sub_CargaForm
  Call Sub_CargarDatos
  
  TDBCalendario.Value = Format(Fnt_FechaServidor, cFormatDate)
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub TDBCalendario_DblClick()
    Call Sub_Modificar
End Sub
Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Call Sub_Agregar
    Case "DEL"
      Sub_EliminarItem
    Case "UPD"
      Call Sub_Modificar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_CargarDatos()
Dim Clave As Long
Dim lReg As hFields
'---- Se referencia la clase DIAS_FERIADOS
Dim lDias As Class_Dias_Festivos

  Clave = 1
  
  Call Sub_Bloquea_Puntero(Me)
  
  TDBCalendario.HolidayStyles.Clear
  
  TDBCalendario.Redraw = False
  ' Multiselecci�n
  TDBCalendario.MultiSelect = dbiMultiSelectNone
  '----------- Define el estilo a utilizar ---------
  TDBCalendario.HolidayStyles.Add , "Feriado", "D�as Feriados"
  With TDBCalendario.HolidayStyles("Feriado")
    .Override = dbiOverrideNone
    .BackColor = RGB(255, 255, 200)   'Amarillo
    .ForeColor = RGB(255, 0, 0)    ' Green
    '-------------------------------------------------
    
    'Se buscan los d�as asociados a la empresa con
    'la Clase
    '----------------------------------------------------------------------------------
    Set lDias = New Class_Dias_Festivos
    
    lDias.Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    If lDias.Buscar Then
      For Each lReg In lDias.Cursor
        '.Days.Add "CLAVE", "TOOLTIP", FECHA INICIO, FECHA TERMINO, TIPO
        .Days.Add "Dias" & Clave, NVL(lReg("DSC_DIA_FERIADO").Value, ""), _
                  NVL(lReg("fch_dia_feriado").Value, ""), NVL(lReg("fch_dia_feriado").Value, ""), _
                  dbiTypeForcedHoliday
        
        Clave = Clave + 1
      Next
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & lDias.ErrMsg, vbCritical, Me.Caption
    End If
    '----------------------------------------------------------------------------------
  End With
  ' Usar el estilo
  TDBCalendario.UseStyles = "Feriado"
  
  TDBCalendario.Redraw = True
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargaForm()
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_EliminarItem()
Dim lID As String
'---- Se referencia la clase DIAS_FERIADOS
Dim lDias As Class_Dias_Festivos

  If Not TDBCalendario.Value = "" Then
    If MsgBox("�Seguro que desea eliminar esta fecha?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      Set lDias = New Class_Dias_Festivos
    
      lDias.Campo("Id_Empresa").Valor = Fnt_EmpresaActual
      lDias.Campo("fch_dia_feriado").Valor = TDBCalendario.Value
      If Not lDias.Borrar Then
        MsgBox lDias.ErrMsg, vbCritical, Me.Caption
      End If

      Call Sub_CargarDatos
    End If
  End If
End Sub

Private Sub Sub_Agregar()
  Call Sub_EsperaVentana(cNewEntidad)
End Sub

Private Sub Sub_Modificar()
  Call Sub_EsperaVentana(0)
End Sub

Private Sub Sub_EsperaVentana(ByVal pkey)
Dim lForm As Frm_Feriados
Dim lNombre As String
Dim lNom_Form As String
  
  lNom_Form = "Frm_Feriados"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lNombre = Me.Name
    
    Set lForm = New Frm_Feriados
    Call lForm.Fnt_Modificar(pkey, TDBCalendario.Value)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      Call Sub_CargarDatos
    Else
      Exit Sub
    End If
  End If
End Sub
