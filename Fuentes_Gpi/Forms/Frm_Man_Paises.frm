VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Man_Paises 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mentencion de Paises"
   ClientHeight    =   4200
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   8850
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   8850
   Begin VB.Frame Frame1 
      Caption         =   "Datos de Paises"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   75
      TabIndex        =   0
      Top             =   420
      Width           =   8700
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   120
         TabIndex        =   1
         Top             =   300
         Width           =   8415
         _cx             =   14843
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   4
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Paises.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   8850
      _ExtentX        =   15610
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Eliminar"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Elimina un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Grabar"
            Key             =   "SAVE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Guarda los cambios realizados"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   7560
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Paises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fBorrar_Paises As hRecord

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With

  Set fBorrar_Paises = New hRecord
  With fBorrar_Paises
    .AddField "colum_pk"
  End With

  Call Sub_CargaForm
  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
  
End Sub

Private Sub Sub_CargaForm()
Dim lComboList As String
Dim lReg As hFields
'Dim lcMonedas As Class_Monedas
Dim lcMoneda As Object
   
  Call Sub_Bloquea_Puntero(Me)
   

  Call Sub_FormControl_Color(Me.Controls)

  Rem Limpia la grilla
  Grilla.Rows = 1
  
'  Set lcMonedas = New Class_Monedas
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    If .Buscar Then
      lComboList = ""
      For Each lReg In .Cursor
        lComboList = lComboList & "|#" & lReg("id_moneda").Value
        
        If lReg.Index = 1 Then
          lComboList = lComboList & "*1"
        End If
        
        lComboList = lComboList & ";" & lReg("cod_moneda").Value & vbTab & lReg("dsc_moneda").Value
      Next
    End If
  End With
  Grilla.ColComboList(Grilla.ColIndex("id_moneda")) = lComboList

  Set lcMoneda = Nothing

  Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_AfterEdit(ByVal Row As Long, ByVal Col As Long)
  Call SetCell(Grilla, Row, "estado", "U") 'Cambia el estado a actualizado
  
  Call Grilla.AutoSize(Col)
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If GetCell(Grilla, Row, "estado") = "" Then
    If Col = 1 Then
      Cancel = True
    End If
  End If
End Sub

Private Sub Grilla_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
  If Col = 3 Then
    KeyAscii = 0
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "DEL"
      Call Sub_Eliminar
    Case "ADD"
      Call Sub_Agregar
    Case "SAVE"
      Call Sub_Grabar
    Case "REFRESH"
      Call Sub_CargarDatos
    Case "EXIT"
      Unload Me
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Sub_Agregar()
  Call Grilla.AddItem("I")
  Call Grilla.ShowCell(Grilla.Rows - 1, Grilla.ColIndex("colum_pk"))
End Sub

Private Sub Sub_Eliminar()
  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
      With Grilla
        If Not GetCell(Grilla, .Row, "colum_pk") = "" Then
          fBorrar_Paises.Add.Fields("colum_pk").Value = GetCell(Grilla, Grilla.Row, "colum_pk")
        End If
        Call .RemoveItem(.Row)
      End With
    End If
  End If
End Sub

Private Sub Sub_CargarDatos()
Dim lcPaises As Class_Paises
'----------------------------------------------
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String

  Me.Enabled = False
  Call Sub_Bloquea_Puntero(Me)
  
  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Grilla.Rows = 1
  
  'Carga los mercados de Transaccion
  Set lcPaises = New Class_Paises
  With lcPaises
    If .Buscar Then
      Call Prc_ComponentOne.Sub_hRecord2Grilla(.Cursor, Grilla, "cod_pais")
    Else
      Call Fnt_MsgError(.SubTipo_LOG, "Problemas en la carga de los Paises.", .ErrMsg, pConLog:=True)
    End If
  End With
  Set lcPaises = Nothing

  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If

  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Grabar()
Dim lcPaises As Class_Paises
'----------------------------------------------
Dim lReg    As hCollection.hFields
Dim lFila As Long
Dim lRollback As Boolean
  
  lRollback = False
  gDB.IniciarTransaccion
  For lFila = 1 To (Grilla.Rows - 1)
    Select Case GetCell(Grilla, lFila, "estado")
      Case "U", "I" 'en el caso que sea insert o update
        If Fnt_Valida(lFila) Then
          Set lcPaises = New Class_Paises
          With lcPaises
            .Campo("Cod_Pais").Valor = GetCell(Grilla, lFila, "colum_pk")
            .Campo("Dsc_Pais").Valor = GetCell(Grilla, lFila, "dsc_pais")
            .Campo("id_Moneda").Valor = GetCell(Grilla, lFila, "id_moneda")
            If .Guardar Then
              Rem Actualiza la columna con el ID
              Call SetCell(Grilla, lFila, "colum_pk", .Campo("Cod_Pais").Valor)
            Else
              lRollback = True
              MsgBox .ErrMsg, vbCritical, Me.Caption
              GoTo ErrProcedure
            End If
          End With
        Else
          lRollback = True
          GoTo ErrProcedure
        End If
    End Select
  Next
  
  Set lcPaises = Nothing
  
  For Each lReg In fBorrar_Paises
    Set lcPaises = New Class_Paises
    With lcPaises
      .Campo("COD_PAIS").Valor = lReg("colum_pk").Value
      If Not .Borrar Then
        'MsgBox "Problemas al eliminar el Pais." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        If .Errnum = 440 Then ' Si el Pais tiene hijos asociados
          MsgBox "El Pais no puede ser eliminado, ya que est� asociado a otros componentes.", vbCritical, Me.Caption
        Else
          MsgBox .ErrMsg, vbCritical, Me.Caption
        End If
        lRollback = True
        GoTo ErrProcedure
      End If
    End With
  Next
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
    Call Sub_CargarDatos
  End If
  gDB.Parametros.Clear
  Set lcPaises = Nothing
  
End Sub

Private Function Fnt_Valida(pLineas As Long) As Boolean
Dim lCod_Pais As String

  Fnt_Valida = True
  
  lCod_Pais = Trim(GetCell(Grilla, pLineas, "colum_pk"))
  
  If lCod_Pais = "" Then
    Fnt_Valida = False
    MsgBox "Debe ingresar C�digo corto del Pais en todas las filas de la grilla.", vbExclamation, Me.Caption
    Exit Function
  ElseIf Len(lCod_Pais) > 3 Then
    Fnt_Valida = False
    MsgBox "El C�digo corto del Pais debe tener como m�ximo 3 caracteres.", vbExclamation, Me.Caption
    Exit Function
  ElseIf Trim(GetCell(Grilla, pLineas, "dsc_pais")) = "" Then
    Fnt_Valida = False
    MsgBox "Debe ingresar el Nombre del Pais en todas las filas de la grilla.", vbExclamation, Me.Caption
    Exit Function
  End If
  
End Function

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Listado de Pa�ses" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

