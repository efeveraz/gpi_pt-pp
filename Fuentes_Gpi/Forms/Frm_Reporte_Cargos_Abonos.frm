VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Reporte_Cargos_Abonos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte de Cargos y Abonos"
   ClientHeight    =   1710
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   7095
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1710
   ScaleWidth      =   7095
   Begin VB.Frame Frame1 
      Caption         =   "Cuentas Vigentes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1185
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   6945
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3225
         Picture         =   "Frm_Reporte_Cargos_Abonos.frx":0000
         TabIndex        =   9
         Top             =   300
         Width           =   375
      End
      Begin VB.CheckBox chk_TodasCtas 
         Caption         =   "Todas"
         Height          =   210
         Left            =   3690
         TabIndex        =   8
         ToolTipText     =   "Todas las Cuentas"
         Top             =   390
         Width           =   765
      End
      Begin MSComCtl2.DTPicker DTP_Fecha 
         Height          =   345
         Left            =   3615
         TabIndex        =   1
         Top             =   705
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   17301505
         CurrentDate     =   37732
      End
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   5160
         TabIndex        =   2
         Top             =   270
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2858
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Reporte"
               Key             =   "REPORTE"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               Object.ToolTipText     =   "Genera el Reporte de Cargos y Abonos"
            EndProperty
         EndProperty
      End
      Begin TrueDBList80.TDBCombo Cmb_Estado 
         Height          =   345
         Left            =   930
         TabIndex        =   7
         Tag             =   "OBLI=S;CAPTION=Estado"
         Top             =   690
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Cargos_Abonos.frx":030A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   120
         TabIndex        =   10
         Tag             =   "OBLI=S;CAPTION=Cuenta"
         Top             =   300
         Width           =   3075
         _ExtentX        =   5424
         _ExtentY        =   609
         LabelWidth      =   795
         TextMinWidth    =   1200
         Caption         =   "Cuenta"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   100
      End
      Begin VB.Label lbl_estado 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Estado"
         Height          =   330
         Left            =   120
         TabIndex        =   6
         Top             =   690
         Width           =   795
      End
      Begin VB.Label Lbl_Fecha 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         Height          =   345
         Left            =   2775
         TabIndex        =   3
         Top             =   705
         Width           =   795
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   7095
      _ExtentX        =   12515
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Cargos_Abonos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFecha As Date

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
  End With
  
  With Toolbar_Verificacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORTE").Image = cBoton_Modificar
  End With
  
  Call Sub_CargaForm
  Call Sub_Limpiar
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()

  Call Sub_CargaCombo_Estados(Cmb_Estado, cTEstado_Mov_Caja, pTodos:=True)
  fFecha = Format(Fnt_FechaServidor, cFormatDate)
    
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Call Sub_Limpiar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REPORTE"
      Call Sub_Generar_Reporte
  End Select
End Sub

Private Sub Sub_Limpiar()
  
  Call Sub_ComboSelectedItem(Cmb_Estado, cCmbKALL)
  DTP_Fecha.Value = fFecha
  chk_TodasCtas.Value = 0
  Txt_Num_Cuenta.Text = ""
  Txt_Num_Cuenta.Tag = ""
  Txt_Num_Cuenta.Enabled = True
  cmb_buscar.Enabled = True
  
End Sub

Private Sub Sub_Generar_Reporte()
Const clrHeader = &HD0D0D0
'------------------------------------------
Const sHeader_CA = "Cuenta|Fecha Movimiento|Tipo Cargo|Descripci�n Cargo/Abono|Monto|Moneda|Estado|Origen Movimiento|Caja Cuenta|D�as Retenci�n|Tipo de Origen"
Const sFormat_CA = "800|>1100|800|2500|>1500|800|1000|1700|1600|>1000|1200"
'------------------------------------------
Dim sRecord
Dim bAppend
Dim lLinea As Integer
'------------------------------------------
Dim lId_Cuenta As String
Dim lCod_Estado As String
'------------------------------------------
Dim lCargo_Abono As Class_Cargos_Abonos
Dim lCursor_CA As hRecord
Dim lReg_CA As hFields
'------------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lNum_Cta As String
'------------------------------------------
Dim lForm As Frm_Reporte_Generico

  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  End If
  If Trim(Txt_Num_Cuenta.Text) = "" And chk_TodasCtas.Value = 0 Then
    MsgBox "Debe seleccionar una cuenta o marcar ""Todas"".", vbExclamation, Me.Caption
    GoTo ErrProcedure
  End If
  
  lId_Cuenta = IIf(Trim(Txt_Num_Cuenta.Tag) = "" Or chk_TodasCtas.Value = 1, "", Trim(Txt_Num_Cuenta.Tag))
  lCod_Estado = IIf(Fnt_ComboSelected_KEY(Cmb_Estado) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Estado))
  
  Rem Busca todos productos
'  Set lCargo_Abono = New Class_Cargos_Abonos
'  With lCargo_Abono
    '.Campo("id_cargo_abono").Valor = ""
'    .Campo("id_cuenta").Valor = lId_Cuenta
'    .Campo("fecha_movimiento").Valor = DTP_Fecha.Value
'    .Campo("cod_estado").Valor = lCod_Estado
'    If .BuscarCargosAbonosReporte(lId_Cuenta, DTP_Fecha.Value, lCod_Estado) Then
'      Set lCursor_CA = .Cursor
'    Else
'      MsgBox .ErrMsg, vbCritical, Me.Caption
'      GoTo ErrProcedure
'    End If
'  End With
  
   gDB.Parametros.Clear
  gDB.Procedimiento = "Pkg_Cargos_Abonos.Buscar"
  Call gDB.Parametros.Add("pcursor", ePT_Cursor, "", eParam_Direc.ePD_Ambos)
  Call gDB.Parametros.Add("pid_cuenta", ePT_Numero, lId_Cuenta, eParam_Direc.ePD_Entrada)
  Call gDB.Parametros.Add("pfecha_movimiento", ePT_Fecha, DTP_Fecha.Value, eParam_Direc.ePD_Entrada)
  Call gDB.Parametros.Add("pcod_estado", ePT_Caracter, lCod_Estado, eParam_Direc.ePD_Entrada)
  Call gDB.Parametros.Add("pid_Empresa", ePT_Numero, Fnt_EmpresaActual, eParam_Direc.ePD_Entrada)
  
  If gDB.EjecutaSP Then
    Set lCursor_CA = gDB.Parametros("pcursor").Valor
  Else
    MsgBox gDB.ErrMsg, vbCritical, Me.Caption
    GoTo ErrProcedure
  End If
  gDB.Parametros.Clear
  
  Rem Comienzo de la generaci�n del reporte
  Set lForm = New Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="Reporte de Cargos y Abonos" _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orLandscape)
     
  With lForm.VsPrinter
    .FontSize = 9
    .Paragraph = "Fecha Consulta: " & NVL(DTP_Fecha.Value, "Todos")
    .Paragraph = "Cuenta: " & IIf(Txt_Num_Cuenta.Tag = "", "Todas", Txt_Num_Cuenta.Text)
    .Paragraph = "Estado: " & Cmb_Estado.Text
    
    .FontSize = 8
    .FontBold = False
    .Paragraph = "" 'salto de linea

    If lCursor_CA.Count > 0 Then
      .StartTable
      .TableCell(tcAlignCurrency) = False
    
      For Each lReg_CA In lCursor_CA
        
        Rem Busca el numero de la cuenta
        lNum_Cta = ""
'        Set lcCuenta = New Class_Cuentas
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
          .Campo("id_cuenta").Valor = lReg_CA("id_cuenta").Value
          If .Buscar_Vigentes Then
            lNum_Cta = .Cursor(1)("num_cuenta").Value
          Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            GoTo ErrProcedure
          End If
        End With
        
        sRecord = lNum_Cta & "|" & _
                  lReg_CA("fecha_movimiento").Value & "|" & _
                  lReg_CA("dsc_flg_tipo_cargo").Value & "|" & _
                  lReg_CA("dsc_cargo_abono").Value & "|" & _
                  Format(NVL(lReg_CA("monto").Value, 0), "###,##0.00") & "|" & _
                  lReg_CA("dsc_moneda").Value & "|" & _
                  lReg_CA("dsc_estado").Value & "|" & _
                  lReg_CA("dsc_origen_mov_caja").Value & "|" & _
                  lReg_CA("dsc_caja_cuenta").Value & "|" & _
                  lReg_CA("retencion").Value & "|" & _
                  lReg_CA("dsc_flg_tipo_origen").Value
        .AddTable sFormat_CA, sHeader_CA, sRecord, clrHeader, , bAppend
      Next
      .TableCell(tcFontBold, 0) = True
      .EndTable
    Else
      .Paragraph = ""
      .FontSize = 8
      .FontBold = True
      .Paragraph = "No hay Cargos/Abonos asociados."
    End If
    .EndDoc
  End With
  
  Call ColocarEn(lForm, Me, eFP_Abajo)
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub


Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call Lpr_Buscar_Cuenta("TXT")
    End If
End Sub

Private Sub cmb_buscar_Click()
Dim lId_Cuenta As Integer

    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)
    If lId_Cuenta <> 0 Then
        Txt_Num_Cuenta.Text = ""
        Txt_Num_Cuenta.Tag = lId_Cuenta
        Call Lpr_Buscar_Cuenta("", CStr(lId_Cuenta))
    End If
End Sub

Private Sub Lpr_Buscar_Cuenta(ByVal Origen As String, Optional p_Id_Cuenta As String)
Dim lcCuenta      As Object

    If Origen = "TXT" Then
        If InStr(1, Txt_Num_Cuenta.Text, "-") > 0 Then
           Txt_Num_Cuenta.Text = Trim(Left(Txt_Num_Cuenta.Text, InStr(1, Txt_Num_Cuenta.Text, "-") - 1))
        End If
        p_Id_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text), 0)
    End If
    If p_Id_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = p_Id_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
            Set lcCuenta = Nothing
    End If
End Sub

Private Sub chk_TodasCtas_Click()

If chk_TodasCtas.Value = 1 Then
    Txt_Num_Cuenta.Text = ""
    Txt_Num_Cuenta.Tag = ""
    Txt_Num_Cuenta.Enabled = False
    cmb_buscar.Enabled = False
Else
    Txt_Num_Cuenta.Enabled = True
    cmb_buscar.Enabled = True
End If

End Sub







