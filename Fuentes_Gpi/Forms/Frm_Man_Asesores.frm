VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Man_Asesores 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de Asesores"
   ClientHeight    =   4170
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   9390
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4170
   ScaleWidth      =   9390
   Begin VB.Frame Frame1 
      Caption         =   "Descripci�n de Asesores"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   75
      TabIndex        =   0
      Top             =   420
      Width           =   9285
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   120
         TabIndex        =   1
         Top             =   300
         Width           =   9015
         _cx             =   15901
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   9
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Asesores.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   9390
      _ExtentX        =   16563
      _ExtentY        =   635
      ButtonWidth     =   2037
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Eliminar"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Elimina un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un elemento"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   3
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Excel"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Asesores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
 End With

  Call Sub_CargaForm
  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
Dim lKey As String

  With Grilla
    If .Row > 0 Then
      lKey = GetCell(Grilla, .Row, "colum_pk")
      
      Call Sub_EsperaVentana(lKey)
      
    End If
  End With
End Sub

Private Sub Sub_EsperaVentana(ByVal pkey)
Dim lForm As Frm_Asesores
Dim lNombre As String
Dim lNom_Form As String
  
  Me.Enabled = False
    
  lNom_Form = "Frm_Asesores"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lNombre = Me.Name
    
    Set lForm = New Frm_Asesores
    Call lForm.Fnt_Modificar(pkey, pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      Call Sub_CargarDatos
    Else
      Exit Sub
    End If
  End If
  
  Me.Enabled = True
  
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Call Sub_CargarDatos
    Case "EXIT"
      Unload Me
    Case "ADD"
      Call Sub_Agregar
    Case "UPDATE"
      Call Grilla_DblClick
    Case "REFRESH"
      Call Sub_CargarDatos
    Case "DEL"
      Call Sub_Eliminar
   Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)

  End Select
End Sub

Private Sub Sub_CargarDatos()
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
'======= SE REFERENCIA CLASE ASESORES ============
Dim lAsesores As Class_Asesor

  '======== SE INSTANCIA CLASE ASESORES ===========
  Set lAsesores = New Class_Asesor

  Call Sub_Bloquea_Puntero(Me)

  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Grilla.Rows = 1
  
  '=== BUSCA DATOS DE LOS ASESORES =======
  If lAsesores.Buscar(True) Then
    For Each lReg In lAsesores.Cursor
      Grilla.AddItem ""
      lLinea = Grilla.Rows - 1
      
      Call SetCell(Grilla, lLinea, "colum_pk", lReg("id_asesor").Value)
      
      Call SetCell(Grilla, lLinea, "COD_ESTADO", lReg("COD_ESTADO").Value)
      Call SetCell(Grilla, lLinea, "RUT", NVL(lReg("RUT").Value, ""))
      Call SetCell(Grilla, lLinea, "NOMBRE", NVL(lReg("NOMBRE").Value, ""))
      Call SetCell(Grilla, lLinea, "EMAIL", NVL(lReg("EMAIL").Value, ""))
      
      Call SetCell(Grilla, lLinea, "FONO", NVL(lReg("FONO").Value, ""))
      Call SetCell(Grilla, lLinea, "ABR_NOMBRE", NVL(lReg("ABR_NOMBRE").Value, ""))
      Call SetCell(Grilla, lLinea, "FLG_BLOQUEO", NVL(lReg("FLG_BLOQUEO").Value, ""))
    Next
  Else
    MsgBox lAsesores.ErrMsg, vbCritical, Me.Caption
  End If
  Set lAsesores = Nothing


  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargaForm()

  Call Sub_FormControl_Color(Me.Controls)

  
  'Limpia la grilla
  'Grilla.Rows = 1
End Sub

Private Sub Sub_Agregar()
  Call Sub_EsperaVentana(cNewEntidad)
End Sub

Private Sub Sub_Eliminar()
Dim lID As String
'======= SE REFERENCIA CLASE ASESORES ============
Dim lAsesores As Class_Asesor
Dim lrel_asesor As Class_Rel_Asesor_Empresa


  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      lID = GetCell(Grilla, Grilla.Row, "colum_pk")
      If Not lID = "" Then
        '======== SE INSTANCIA CLASE ASESORES ===========
        Set lAsesores = New Class_Asesor
        lAsesores.Campo("id_asesor").Valor = lID
        '=== ELIMINA DATOS DEL ASESOR =======
        
        Set lrel_asesor = New Class_Rel_Asesor_Empresa
        lrel_asesor.Campo("id_Asesor").Valor = lID
        
        If lrel_asesor.Borrar Then
        
        If lAsesores.Borrar Then
          MsgBox "Asesor Eliminado correctamente.", vbInformation + vbOKOnly, Me.Caption
        Else
          If lAsesores.Errnum = 440 Then
            MsgBox "El Asesor no puede ser eliminado, ya que est� asociado a otros componentes.", vbCritical, Me.Caption
          Else
            MsgBox lAsesores.ErrMsg, vbCritical, Me.Caption
          End If
        End If
        
        Else
          MsgBox lrel_asesor.ErrMsg, vbCritical, Me.Caption
        End If
        
        Set lAsesores = Nothing
        Set lrel_asesor = Nothing
      End If

      Call Sub_CargarDatos
    End If
  End If
End Sub



Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
  
    Call .Sub_InicarDocumento(pTitulo:="Listado de Asesores" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub



Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
    Case "Excel"
      Call Sub_Crea_Excel
  End Select
End Sub

Private Sub Sub_Crea_Excel()
Dim lcExcel As Excel.Application
Dim lcLibro As Excel.Workbook
Dim lcHoja  As Excel.Worksheet
'------------------------------------------
'------------------------------------------
Dim lArchivo
Dim pGrilla As VSFlexGrid
Dim lFilaGrilla As Integer
Dim lColGrilla As Integer

Dim i As Integer
Dim lFilaExcel As Integer
Dim lColExcel As Integer
Dim lFilasTotal As Integer
Dim fImg_Header As String

Call Sub_Bloquea_Puntero(Me)
fImg_Header = App.Path & "\" & "security.jpg" 'busqueda imagen portada
lArchivo = Fnt_GuardarComo(pFileName:="Mantenedor Asesores " & Format(Fnt_SYSDATE, "yyyymmdd"))

 If IsNull(lArchivo) Then
    GoTo ErrProcedure
 End If
            

Set lcExcel = Fnt_CreateObject(cExcel)
            lcExcel.DisplayAlerts = False
            lcExcel.Visible = False
Set lcLibro = lcExcel.Workbooks.Add

'Comienza exportacion de Datos
Set lcHoja = lcLibro.Worksheets(1)
            lcHoja.Name = "Asesores"
            lcHoja.Activate
'----------------------------------------------------------------------------------------------------
'Carga de imagen Reporte, relleno de fondos de celdas, valores en negrita, titulos.
'----------------------------------------------------------------------------------------------------
            With lcLibro
              .ActiveSheet.Cells(1, 1).Select
              .ActiveSheet.Pictures.Insert(fImg_Header).Width = 300 'tama�o de la imagen a insertar(ancho)
              .ActiveSheet.Pictures.ShapeRange.Top = 0
              .ActiveSheet.Pictures.ShapeRange.Left = 0
              
            
              '.Worksheets.Item(1).Range("A1", "Z1000").Font.Name = "Arial"
              .Worksheets.Item(1).Range("D5").Font.Size = 12
              .Worksheets.Item(1).Range("D5").Value = "LISTADO DE ASESORES"
              .Worksheets.Item(1).Range("D5").Font.Bold = True
              .Worksheets.Item(1).Range("A5:D5").Merge
              .Worksheets.Item(1).Range("A5:D5").HorizontalAlignment = xlHAlignLeft
              .Worksheets.Item(1).Range("A6").Font.Size = 12
              .Worksheets.Item(1).Range("A6").Value = "Fecha de Consulta: " & Format(Fnt_SYSDATE, cFormatDate)
              .Worksheets.Item(1).Range("A6").Font.Bold = True
              .Worksheets.Item(1).Range("A6:D6").Merge
              .Worksheets.Item(1).Range("A6:D6").HorizontalAlignment = xlHAlignLeft
              .Worksheets.Item(1).Range("A7:D7").Font.Bold = True
              .Worksheets.Item(1).Columns("A:A").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("B:B").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("C:C").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("D:D").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("E:E").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("F:F").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("G:G").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("H:H").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("I:I").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("J:J").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("K:K").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("L:L").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("M:M").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("N:N").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("O:O").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("P:P").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("Q:Q").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("R:R").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("S:S").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("T:T").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("U:U").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("V:V").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("W:W").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("X:X").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("Y:Y").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("Z:Z").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("AA:AA").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("AB:AB").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("AC:AC").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("AD:AD").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("AE:AE").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("AF:AF").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("AG:AG").Interior.Color = vbWhite
              .Worksheets.Item(1).Columns("A:A").NumberFormat = "@"
              .Worksheets.Item(1).Columns("B:B").NumberFormat = "@"
              .Worksheets.Item(1).Columns("C:C").NumberFormat = "@"
              .Worksheets.Item(1).Columns("D:D").NumberFormat = "@"
                  
                  
          '-----------------------------------------------------------------------------------------------------
            
            End With
        
        With lcHoja
            
              Set pGrilla = Grilla
              
              lFilaExcel = 7
              lColExcel = 1
             
            '----------------------------------------------------------------------------------------------------
            'Carga de Restriccion de Valores y Parametros obtenidos desde el Mantenedor
            '----------------------------------------------------------------------------------------------------
                  For lFilaGrilla = 0 To (pGrilla.Rows - 1)
                  pGrilla.Row = lFilaGrilla
                              'lContador = lContador + 1
                              For lColGrilla = 0 To (pGrilla.Cols - 1)
                                  pGrilla.Col = lColGrilla
                                    'KeyCol = pGrilla.ColKey(lColGrilla)
                                          If Not pGrilla.ColHidden(lColGrilla) Then
                                                    .Cells(lFilaExcel, lColExcel).Value = pGrilla.Text
                                                    lColExcel = lColExcel + 1
                                         End If
                          Next
                              lFilaExcel = lFilaExcel + 1
                              lColExcel = 1
                  Next
            '-----------------------------------------------------------------------------------------------------
            
              lFilasTotal = lFilaExcel
                  
              lFilaExcel = 1
             
            '.Range(.Cells(14, 1), .Cells(14, lColExcel - 1)).AutoFilter
            .Range(.Cells(1, 1), .Cells(lFilasTotal - 1, 4)).Font.Name = "Arial"
            .Range(.Cells(7, 1), .Cells(lFilasTotal - 1, 4)).Font.Size = 10
            .Range(.Cells(7, 1), .Cells(lFilasTotal - 1, 4)).WrapText = False
            .Range("A7:D7").EntireColumn.AutoFit
            .Range("A7:D7").Interior.Color = vbYellow
            '.Range("A14:D14").Interior.Color = vbYellow
            .Range("A7:D7").Borders.LineStyle = xlContinuous
            .Range("A7:D7").HorizontalAlignment = xlHAlignCenter
            .Range(.Cells(8, 1), .Cells(lFilasTotal - 1, 4)).HorizontalAlignment = xlHAlignLeft
            .Range(.Cells(8, 1), .Cells(lFilasTotal - 1, 1)).HorizontalAlignment = xlHAlignRight
            .Range(.Cells(1, 1), .Cells(lFilasTotal - 1, 4)).VerticalAlignment = xlTop
            .Range("A6:D6").Interior.Color = vbWhite
            
            
            
            
           'lcLibro.Worksheets(1).Activate
            Call lcLibro.SaveAs(lArchivo)
        End With
        
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al exportar a Excel.", Err.Description)
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

If Not IsNull(lArchivo) And Err.Number = 0 Then
    If MsgBox("�Desea abrir el documento?", vbYesNo, "Archivo Generado") = vbYes Then
        lcExcel.Visible = True
        lcExcel.DisplayAlerts = True
    Else
        lcExcel.Quit
    End If
End If

  Set lcExcel = Nothing
  Set lcLibro = Nothing
  Set lcHoja = Nothing
  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)

End Sub

Public Function Fnt_GuardarComo(Optional pFileName = "" _
                               , Optional pFilter = "Planilla Excel (*.xls)|*.xls|Todos los Archivos (*.*)|*.*" _
                               , Optional pFlags = cdlOFNExplorer + cdlOFNLongNames + cdlOFNPathMustExist + cdlOFNHideReadOnly + cdlOFNNoReadOnlyReturn + cdlOFNOverwritePrompt)
On Error GoTo ErrProcedure
  
  Fnt_GuardarComo = Null
  
  With MDI_Principal.CommonDialog
    .CancelError = True
    .Filter = pFilter
    .Flags = pFlags
    .Filename = pFileName
    .ShowSave
    
    Rem Se coloca el path completo, que incluye el nombre de archivo
    Fnt_GuardarComo = .Filename
  End With
  
ErrProcedure:
  Rem Usuario presiona el boton Cancelar del Command Dialog
  Rem o presiona No o Cancelar en el dialogo "ya existe archivo...desea reemplazarlo"
  If Err.Number = 32755 Or Err.Number = 1004 Then
    GoTo ExitProcedure
  End If
  
  If Not Err.Number = 0 Then
    MsgBox "Error en la exportaci�n de datos a la planilla Excel.", vbExclamation, "Error en Guarda Como..."
    GoTo ExitProcedure
  End If
  
ExitProcedure:

End Function

