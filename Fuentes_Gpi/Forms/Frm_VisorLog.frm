VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{E2D000D0-2DA1-11D2-B358-00104B59D73D}#1.0#0"; "titext8.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Frm_VisorLog 
   Caption         =   "Visor de Log"
   ClientHeight    =   5700
   ClientLeft      =   180
   ClientTop       =   450
   ClientWidth     =   8775
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5700
   ScaleWidth      =   8775
   Begin C1SizerLibCtl.C1Elastic C1Elastic1 
      Height          =   5340
      Left            =   0
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   360
      Width           =   8775
      _cx             =   15478
      _cy             =   9419
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FloodColor      =   6553600
      ForeColorDisabled=   -2147483631
      Caption         =   ""
      Align           =   5
      AutoSizeChildren=   7
      BorderWidth     =   6
      ChildSpacing    =   4
      Splitter        =   0   'False
      FloodDirection  =   0
      FloodPercent    =   0
      CaptionPos      =   0
      WordWrap        =   -1  'True
      MaxChildSize    =   0
      MinChildSize    =   0
      TagWidth        =   0
      TagPosition     =   0
      Style           =   0
      TagSplit        =   2
      PicturePos      =   4
      CaptionStyle    =   0
      ResizeFonts     =   0   'False
      GridRows        =   0
      GridCols        =   0
      Frame           =   3
      FrameStyle      =   0
      FrameWidth      =   1
      FrameColor      =   -2147483628
      FrameShadow     =   -2147483632
      FloodStyle      =   1
      _GridInfo       =   ""
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   9
      Begin C1SizerLibCtl.C1Elastic C1Elastic2 
         Height          =   4515
         Left            =   90
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   735
         Width           =   8595
         _cx             =   15161
         _cy             =   7964
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         Appearance      =   5
         MousePointer    =   0
         Version         =   801
         BackColor       =   -2147483633
         ForeColor       =   255
         FloodColor      =   6553600
         ForeColorDisabled=   -2147483631
         Caption         =   "Registros Log"
         Align           =   0
         AutoSizeChildren=   7
         BorderWidth     =   6
         ChildSpacing    =   4
         Splitter        =   0   'False
         FloodDirection  =   0
         FloodPercent    =   0
         CaptionPos      =   0
         WordWrap        =   -1  'True
         MaxChildSize    =   0
         MinChildSize    =   0
         TagWidth        =   0
         TagPosition     =   0
         Style           =   0
         TagSplit        =   2
         PicturePos      =   4
         CaptionStyle    =   0
         ResizeFonts     =   0   'False
         GridRows        =   0
         GridCols        =   0
         Frame           =   3
         FrameStyle      =   0
         FrameWidth      =   1
         FrameColor      =   -2147483628
         FrameShadow     =   -2147483632
         FloodStyle      =   1
         _GridInfo       =   ""
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
         Begin VSFlex8LCtl.VSFlexGrid Grilla 
            Height          =   3915
            Left            =   60
            TabIndex        =   4
            Top             =   480
            Width           =   8475
            _cx             =   14949
            _cy             =   6906
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   6
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_VisorLog.frx":0000
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin C1SizerLibCtl.C1Elastic C1Elastic3 
         Height          =   645
         Left            =   90
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   30
         Width           =   8595
         _cx             =   15161
         _cy             =   1138
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         Appearance      =   5
         MousePointer    =   0
         Version         =   801
         BackColor       =   -2147483633
         ForeColor       =   255
         FloodColor      =   6553600
         ForeColorDisabled=   -2147483631
         Caption         =   ""
         Align           =   0
         AutoSizeChildren=   0
         BorderWidth     =   6
         ChildSpacing    =   4
         Splitter        =   0   'False
         FloodDirection  =   0
         FloodPercent    =   0
         CaptionPos      =   0
         WordWrap        =   -1  'True
         MaxChildSize    =   0
         MinChildSize    =   0
         TagWidth        =   0
         TagPosition     =   0
         Style           =   0
         TagSplit        =   2
         PicturePos      =   4
         CaptionStyle    =   0
         ResizeFonts     =   0   'False
         GridRows        =   0
         GridCols        =   0
         Frame           =   3
         FrameStyle      =   0
         FrameWidth      =   1
         FrameColor      =   -2147483628
         FrameShadow     =   -2147483632
         FloodStyle      =   1
         _GridInfo       =   ""
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
         Begin VB.Timer Timer 
            Interval        =   5000
            Left            =   5460
            Top             =   60
         End
         Begin TDBText6Ctl.TDBText Txt_SegRefresh 
            Height          =   285
            Left            =   3570
            TabIndex        =   8
            Top             =   90
            Width           =   825
            _Version        =   65536
            _ExtentX        =   1455
            _ExtentY        =   503
            Caption         =   "Frm_VisorLog.frx":0116
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DropDown        =   "Frm_VisorLog.frx":017A
            Key             =   "Frm_VisorLog.frx":0198
            BackColor       =   -2147483643
            EditMode        =   0
            ForeColor       =   -2147483640
            ReadOnly        =   0
            ShowContextMenu =   -1
            MarginLeft      =   1
            MarginRight     =   1
            MarginTop       =   1
            MarginBottom    =   1
            Enabled         =   -1
            MousePointer    =   0
            Appearance      =   2
            BorderStyle     =   1
            AlignHorizontal =   1
            AlignVertical   =   0
            MultiLine       =   0
            ScrollBars      =   0
            PasswordChar    =   ""
            AllowSpace      =   -1
            Format          =   ""
            FormatMode      =   1
            AutoConvert     =   -1
            ErrorBeep       =   0
            MaxLength       =   0
            LengthAsByte    =   0
            Text            =   "5"
            Furigana        =   0
            HighlightText   =   0
            IMEMode         =   0
            IMEStatus       =   0
            DropWndWidth    =   0
            DropWndHeight   =   0
            ScrollBarMode   =   0
            MoveOnLRKey     =   0
            OLEDragMode     =   0
            OLEDropMode     =   0
         End
         Begin VB.CheckBox Chk_AutoRefresh 
            Caption         =   "AutoRefresco"
            Height          =   255
            Left            =   2160
            TabIndex        =   7
            Top             =   90
            Value           =   1  'Checked
            Width           =   1335
         End
         Begin VB.CheckBox Chk_AutoDesplazamiento 
            Caption         =   "AutoDesplazamiento"
            Height          =   255
            Left            =   120
            TabIndex        =   6
            Top             =   90
            Value           =   1  'Checked
            Width           =   1845
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   7800
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_VisorLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Chk_AutoRefresh_Click()
  Txt_SegRefresh.ReadOnly = Not (Chk_AutoRefresh.Value = vbChecked)
  Timer.Enabled = Not Txt_SegRefresh.ReadOnly
End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With

  Call Sub_CargaForm
  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
  
  
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Sub_EsperaVentana(ByVal pkey)
Dim lForm As Frm_ClasificadorRiesgo
Dim lNombre As String
Dim lNom_Form As String
  
  Me.Enabled = False
  
  lNom_Form = "Frm_ClasificadorRiesgo"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lNombre = Me.Name
    
    Set lForm = New Frm_ClasificadorRiesgo
    Call lForm.Fnt_Modificar(pkey, pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lNombre) Then
      Call Sub_CargarDatos
    Else
      Exit Sub
    End If
  End If
  
  Me.Enabled = True
  
End Sub

Private Sub Timer_Timer()
  Call Sub_CargarDatos
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Call Sub_CargarDatos
    Case "EXIT"
      Unload Me
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Sub_CargarDatos()
Dim lReg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lcLog_Registros As Class_Log_Registros
Dim lPrimera As Boolean

  Call Sub_Bloquea_Puntero(Me)
  
  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  lPrimera = Not (Grilla.Rows > 1)
  
  Grilla.Rows = 1
  
  Set lcLog_Registros = New Class_Log_Registros
  With lcLog_Registros
    .Campo("ID_USUARIO").Valor = gID_Usuario
    If Not .Buscar_Ultimos() Then
      Call Fnt_MsgError(eLS_LogVarios _
                        , "Problemas al buscar los registros." _
                        , .ErrMsg _
                        , pConLog:=True)
      GoTo ExitProcedure
    End If
    
    Call Sub_hRecord2Grilla(.Cursor, Grilla, "ID_LOG_REGISTRO")
      
    If lPrimera Then
      Call Sub_AjustaColumnas_Grilla(Grilla)
    End If
  End With
  Set lcLog_Registros = Nothing
  
  If Chk_AutoDesplazamiento.Value = vbChecked Then
    If Grilla.Rows > 1 Then
      Grilla.Row = 1
    End If
  Else
    If Not lID = "" Then
      Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    End If
  End If

  If Not Grilla.Row = cNewEntidad Then
    Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
  End If

ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargaForm()
  Rem Limpia la grilla
  Grilla.Rows = 1

  Call Sub_FormControl_Color(Me.Controls)

End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generación del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Log Sistemas" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

Private Sub Txt_SegRefresh_Change()
  Timer.Interval = To_Number(Txt_SegRefresh.Text) * 1000
End Sub
