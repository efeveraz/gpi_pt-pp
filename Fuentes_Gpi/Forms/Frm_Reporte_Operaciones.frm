VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Operaciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte de Operaciones"
   ClientHeight    =   1770
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8160
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1770
   ScaleWidth      =   8160
   Begin VB.Frame Frame1 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1275
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   8055
      Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
         Height          =   345
         Left            =   4110
         TabIndex        =   3
         Top             =   270
         Width           =   1605
         _ExtentX        =   2831
         _ExtentY        =   609
         _Version        =   393216
         Format          =   16384001
         CurrentDate     =   38938
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Ini 
         Height          =   345
         Left            =   1170
         TabIndex        =   4
         Top             =   270
         Width           =   1605
         _ExtentX        =   2831
         _ExtentY        =   609
         _Version        =   393216
         Format          =   16384001
         CurrentDate     =   38938
      End
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   5850
         TabIndex        =   5
         Top             =   270
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2858
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Reporte"
               Key             =   "REPORTE"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               Object.ToolTipText     =   "Genera el Reporte de Instruccones"
            EndProperty
         EndProperty
      End
      Begin TrueDBList80.TDBCombo Cmb_Estado 
         Height          =   345
         Left            =   1170
         TabIndex        =   6
         Tag             =   "OBLI=S;CAPTION=Estado"
         Top             =   750
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Operaciones.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha T�rmino"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   0
         Left            =   2850
         TabIndex        =   9
         Top             =   270
         Width           =   1185
      End
      Begin VB.Label lbl_estado 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   8
         Top             =   750
         Width           =   975
      End
      Begin VB.Label Lbl_Fecha_Ini 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Inicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   1
         Left            =   120
         TabIndex        =   7
         Top             =   270
         Width           =   975
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8160
      _ExtentX        =   14393
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Operaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const RColorRelleno = 213   ' R
Const GColorRelleno = 202   ' G
Const BColorRelleno = 195   ' B
Dim fFecha As Date

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
  End With
  
  With Toolbar_Verificacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORTE").Image = cBoton_Modificar
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()


  Call Sub_CargaCombo_Estados(Cmb_Estado, cTEstado_Operaciones, pTodos:=True)
  

  fFecha = Format(Fnt_FechaServidor, cFormatDate)
  Call Sub_Limpiar
    
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Call Sub_Limpiar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REPORTE"
      Call Sub_Generar_Reporte
  End Select
End Sub

Private Sub Sub_Limpiar()
  
  Call Sub_ComboSelectedItem(Cmb_Estado, cCmbKALL)
  DTP_Fecha_Ini.Value = fFecha
  DTP_Fecha_Ter.Value = fFecha
  
End Sub

Private Sub Sub_Generar_Reporte()
Const clrHeader = &HD0D0D0
'------------------------------------------
Const sHeader_Ope = "N� Instrucci�n|Cuenta|Instrumento|Nemot�cnico|Cantidad|Tasa/Precio|Monto|Fecha Operaci�n|Fecha Liquidaci�n|Estado|Tipo Operaci�n"
Const sFormat_Ope = "1500|1000|2500|2500|>1200|1500|1500|1000|1000|1000|1000"
Const sHeader_Det = "Nemot�cnico|Cantidad|Tasa/Precio|Monto Instrucci�n|Cantidad Confirmada|Tasa/Precio Confirmado|Monto Confirmado|Fecha Confirmaci�n"
Const sFormat_Det = "2000|>1200|>1300|>1800|>1800|>2000|>2300|>1700"
'------------------------------------------
Dim sRecord
Dim bAppend
Dim lLinea As Integer
'------------------------------------------
Dim lcOperaciones As Class_Operaciones
Dim lCursor_Ope As hRecord
Dim lReg As hFields
'------------------------------------------
Dim lDetalle As Class_Operaciones_Detalle
Dim lCursor_Det As hRecord
Dim lReg_Det As hFields
'------------------------------------------
Dim lMov_Activo As Class_Mov_Activos
Dim lReg_MA As hFields
'------------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lReg_Cta As hFields
Dim lNum_Cuenta As String
'------------------------------------------
Dim lInstrumento As Class_Instrumentos
Dim lReg_Ins As hFields
Dim lReg_Ope As hFields
Dim lDsc_Instrumento As String
Dim lDsc_Producto As String
'------------------------------------------
Dim lNemotecnico As Class_Nemotecnicos
Dim lReg_Nem As hFields
Dim lNemo As String
'------------------------------------------
Dim lCod_Estado As String
Dim lCantidad_Conf As String
Dim lValor_Mercado_Conf As String
Dim lPrecio_Conf As String
Dim lFecha_Conf As String
Dim lFecha_Desde As String
Dim lFecha_Hasta As String
'------------------------------------------
Dim lForm As Frm_Reporte_Generico
Dim a As Integer
Dim Filas As Integer
Dim Tipo_Movimiento(3, 2) As String
Dim i As Integer
Dim lTipoMovimiento As String

Tipo_Movimiento(1, 1) = "INST"
Tipo_Movimiento(2, 1) = "CUST"
Tipo_Movimiento(3, 1) = "DIREC"

Tipo_Movimiento(1, 2) = "INSTRUCCION"
Tipo_Movimiento(2, 2) = "CUSTODIA"
Tipo_Movimiento(3, 2) = "DIRECTA"

  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  ElseIf DTP_Fecha_Ini.Value > DTP_Fecha_Ter.Value Then
    MsgBox "La fecha de inicio es mayor que la fecha de t�rmino.", vbCritical, Me.Caption
    GoTo ErrProcedure
  End If
  
  lCod_Estado = IIf(Fnt_ComboSelected_KEY(Cmb_Estado) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Estado))
  lFecha_Desde = DTP_Fecha_Ini.Value
  lFecha_Hasta = DTP_Fecha_Ter.Value
  
  
  
  Rem Busca las operaciones seg�n los filtros ingresados
  
  Rem Comienzo de la generaci�n del reporte
  Set lForm = New Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="Reporte de Operaciones" _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orLandscape)
     
  With lForm.VsPrinter
    .FontSize = 9
    .Paragraph = "Fecha Consulta entre " & DTP_Fecha_Ini.Value & " y " & DTP_Fecha_Ter.Value
    .Paragraph = "Estado: " & Cmb_Estado.Text
    
    .Paragraph = "" 'salto de linea
    .FontBold = False
    .FontSize = 8
    

      'BuscarView_2
      Set lDetalle = New Class_Operaciones_Detalle
      Set lcOperaciones = New Class_Operaciones
      
      For i = 1 To 3
            lcOperaciones.Campo("Id_Operacion").Valor = ""
            lcOperaciones.Campo("Cod_Instrumento").Valor = ""
            lcOperaciones.Campo("cod_producto").Valor = ""
            lcOperaciones.Campo("Cod_Tipo_Operacion").Valor = ""
            lcOperaciones.Campo("Flg_Tipo_Movimiento").Valor = ""
            lcOperaciones.Campo("cod_Estado").Valor = lCod_Estado
            lcOperaciones.Campo("COD_TIPO_OPERACION").Valor = Tipo_Movimiento(i, 1)
            'lDetalle.Campo("COD_TIPO_OPERACION").Valor = Tipo_Movimiento(i, 1)
            If lcOperaciones.Fnt_Buscar_Operaciones(lFecha_Desde, lFecha_Hasta, "", "") Then
              Set lCursor_Ope = lcOperaciones.Cursor
            Else
              MsgBox .ErrMsg, vbCritical, Me.Caption
            GoTo ErrProcedure
            End If
            
            .Paragraph = "" 'salto de linea
            .Paragraph = "" 'salto de linea
            .FontBold = True
            .FontSize = 8
            .Paragraph = Tipo_Movimiento(i, 2)
            .MarginLeft = "20mm"
            
            .FontBold = False
        
        
            .StartTable
            .TableCell(tcCols) = 10
            .TableCell(tcRows) = 1
            .TableBorder = tbAll
            .FontSize = 7
            .TableCell(tcColWidth, 1, 1) = "10mm"
            .TableCell(tcColWidth, 1, 2) = "15mm"
            .TableCell(tcColWidth, 1, 3) = "30mm"
            .TableCell(tcColWidth, 1, 4) = "50mm"
            .TableCell(tcColWidth, 1, 5) = "20mm"
            .TableCell(tcColWidth, 1, 6) = "15mm"
            .TableCell(tcColWidth, 1, 7) = "23mm"
            .TableCell(tcColWidth, 1, 8) = "30mm"
            '.TableCell(tcColWidth, 1, 9) = "30mm"
            .TableCell(tcColWidth, 1, 9) = "25mm"
            .TableCell(tcColWidth, 1, 10) = "15mm"
            .TableCell(tcBackColor, 1, 1, 1, 11) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
            
            .TableCell(tcText, 1, 1) = "N� Op."
            .TableCell(tcAlign, 1, 1, 1, 11) = taCenterMiddle
            .TableCell(tcText, 1, 2) = "N� Cuenta"
            .TableCell(tcText, 1, 3) = "Instrumento"
            .TableCell(tcText, 1, 4) = "Nemot�cnico"
            .TableCell(tcText, 1, 5) = "Cantidad"
            .TableCell(tcText, 1, 6) = "Precio"
            .TableCell(tcText, 1, 7) = "Monto"
            .TableCell(tcText, 1, 8) = "Fecha Movimiento"
            '.TableCell(tcText, 1, 9) = "Fecha Liquidaci�n"
            .TableCell(tcText, 1, 9) = "Tipo Movimiento"
            .TableCell(tcText, 1, 10) = "Moneda"
            .EndTable
            
            If lCursor_Ope.Count > 0 Then
                Filas = CInt(lcOperaciones.Cursor.Count)
                .StartTable
                '.TableCell(tcRows) = 4
                .TableCell(tcCols) = 10
                .TableBorder = tbAll
                .TableCell(tcRows) = Filas
                .TableCell(tcColWidth, 1, 1) = "10mm"
                .TableCell(tcColWidth, 1, 2) = "15mm"
                .TableCell(tcColWidth, 1, 3) = "30mm"
                .TableCell(tcColWidth, 1, 4) = "50mm"
                .TableCell(tcColWidth, 1, 5) = "20mm"
                .TableCell(tcColWidth, 1, 6) = "15mm"
                .TableCell(tcColWidth, 1, 7) = "23mm"
                .TableCell(tcColWidth, 1, 8) = "30mm"
                .TableCell(tcColWidth, 1, 9) = "25mm"
                .TableCell(tcColWidth, 1, 10) = "15mm"
                
                a = 1
                For Each lReg_Ope In lCursor_Ope
                    lDetalle.Campo("id_operacion").Valor = lReg_Ope("id_operacion").Value
                    If lDetalle.Buscar Then
                      Set lCursor_Det = lDetalle.Cursor
                    Else
                       MsgBox .ErrMsg, vbCritical, Me.Caption
                       GoTo ErrProcedure
                    End If
                    For Each lReg_Det In lCursor_Det
                      Select Case lReg_Ope("flg_tipo_movimiento").Value
                        Case "I"
                            lTipoMovimiento = "INGRESO"
                        Case "E"
                            lTipoMovimiento = "EGRESO"
                       End Select
        
                      .TableCell(tcText, a, 1) = lReg_Ope("id_operacion").Value
                      .TableCell(tcAlign, a, 1) = taCenterMiddle
                      '.TableCell(tcText, a, 2) = lReg("id_operacion_detalle").Value
                      '.TableCell(tcAlign, a, 2) = taCenterMiddle
                      .TableCell(tcText, a, 2) = lReg_Ope("num_cuenta").Value
                      .TableCell(tcAlign, a, 2) = taCenterMiddle
                      .TableCell(tcText, a, 3) = lReg_Ope("dsc_intrumento").Value
                      .TableCell(tcAlign, a, 3) = taLeftMiddle
                      .TableCell(tcText, a, 4) = Entrega_Nemotecnico(lReg_Det("id_nemotecnico").Value)
                      .TableCell(tcAlign, a, 4) = taLeftMiddle
                      .TableCell(tcText, a, 5) = FormatNumber(lReg_Det("cantidad").Value)
                      .TableCell(tcAlign, a, 5) = taRightMiddle
                      .TableCell(tcText, a, 6) = FormatNumber(lReg_Det("precio").Value)
                      .TableCell(tcAlign, a, 6) = taRightMiddle
                      .TableCell(tcText, a, 7) = FormatNumber(lReg_Det("monto_bruto").Value)
                      .TableCell(tcAlign, a, 7) = taRightMiddle
                      .TableCell(tcText, a, 8) = lReg_Ope("fecha_operacion").Value
                      .TableCell(tcAlign, a, 8) = taCenterMiddle
                      '.TableCell(tcText, a, 9) = lReg("fecha_movimiento").Value
                      .TableCell(tcText, a, 9) = lTipoMovimiento
                      .TableCell(tcAlign, a, 9) = taCenterMiddle
                      .TableCell(tcText, a, 10) = lReg_Ope("dsc_moneda").Value
                      .TableCell(tcAlign, a, 10) = taCenterMiddle
                      ' .TableCell(tcText, a, 10) = Entrega_Estado(lReg("cod_estado").Value)
                      '.TableCell(tcAlign, a, 10) = taCenterMiddle
                      a = a + 1
                    Next
                Next
                .EndTable
            End If
        Next
      
        .StartTable
        '.TableCell(tcRows) = 4
        .TableCell(tcCols) = 9
        .TableBorder = tbAll
        
        .TableCell(tcColWidth, 1, 1) = "10mm"
        .TableCell(tcColWidth, 1, 2) = "15mm"
        .TableCell(tcColWidth, 1, 3) = "30mm"
        .TableCell(tcColWidth, 1, 4) = "40mm"
        .TableCell(tcColWidth, 1, 5) = "15mm"
        .TableCell(tcColWidth, 1, 6) = "15mm"
        .TableCell(tcColWidth, 1, 7) = "23mm"
        .TableCell(tcColWidth, 1, 8) = "15mm"
        '.TableCell(tcColWidth, 1, 10) = "20mm"
        '.TableCell(tcColWidth, 1, 11) = "20mm"
              
      
 '       Set lMov_Activo = New Class_Mov_Activos
        
'        If lMov_Activo.BuscarView_2(lFecha_Desde, lFecha_Hasta) Then
'        'If lcOperaciones.BuscaConDetalles Then
'          Filas = CInt(lMov_Activo.Cursor.Count)
'          .TableCell(tcRows) = Filas
'                .TableCell(tcBackColor, 1, 1, 1, 11) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'
'                .TableCell(tcText, 1, 1) = "N� Operaci�n"
'                .TableCell(tcAlign, 1, 1, 1, 11) = taCenterMiddle
'
'                '.TableCell(tcText, 1, 2) = "N� Detalle"
'
'                .TableCell(tcText, 1, 2) = "N� Cuenta"
'
'                .TableCell(tcText, 1, 3) = "Instrumento"
'
'                .TableCell(tcText, 1, 4) = "Nemot�cnico"
'
'                .TableCell(tcText, 1, 5) = "Cantidad"
'
'                .TableCell(tcText, 1, 6) = "Precio"
'
'                .TableCell(tcText, 1, 7) = "Monto"
'
'                .TableCell(tcText, 1, 8) = "Fecha Movimiento"
'
'                .TableCell(tcText, 1, 9) = "Fecha Liquidaci�n"
'
'                '.TableCell(tcText, 1, 10) = "Estado"
'
'          a = 2
'
'          For Each lReg In lMov_Activo.Cursor
'                .TableCell(tcText, a, 1) = Entrega_Operacion(lReg("id_operacion_detalle").Value)
'                .TableCell(tcAlign, a, 1) = taCenterMiddle
'                '.TableCell(tcText, a, 2) = lReg("id_operacion_detalle").Value
'                '.TableCell(tcAlign, a, 2) = taCenterMiddle
'                .TableCell(tcText, a, 2) = lReg("num_cuenta").Value
'                .TableCell(tcAlign, a, 2) = taCenterMiddle
'                .TableCell(tcText, a, 3) = Entrega_Instrumento(lReg("cod_instrumento").Value)
'                .TableCell(tcAlign, a, 3) = taLeftMiddle
'                .TableCell(tcText, a, 4) = Entrega_Nemotecnico(lReg("id_nemotecnico").Value)
'                .TableCell(tcAlign, a, 4) = taLeftMiddle
'                .TableCell(tcText, a, 5) = FormatNumber(lReg("cantidad").Value)
'                .TableCell(tcAlign, a, 5) = taRightMiddle
'                .TableCell(tcText, a, 6) = FormatNumber(lReg("precio").Value)
'                .TableCell(tcAlign, a, 6) = taRightMiddle
'                .TableCell(tcText, a, 7) = FormatNumber(lReg("monto").Value)
'                .TableCell(tcAlign, a, 7) = taRightMiddle
'                .TableCell(tcText, a, 8) = lReg("fecha_operacion").Value
'                .TableCell(tcAlign, a, 8) = taCenterMiddle
'                .TableCell(tcText, a, 9) = lReg("fecha_movimiento").Value
'                .TableCell(tcAlign, a, 9) = taCenterMiddle
'                ' .TableCell(tcText, a, 10) = Entrega_Estado(lReg("cod_estado").Value)
'                '.TableCell(tcAlign, a, 10) = taCenterMiddle
'
'
'
'
'
'          a = a + 1
'          Next
 '
'        End If
      
    .TableCell(tcFontSize, 1, 1, Filas, 10) = 6
    '.TableCell(tcAlign, 1, 1, Filas, 10) = taCenterMiddle
    .EndTable
    
    
'                  sRecord = lReg_Ope("id_operacion").Value & "|" & _
'                    lNum_Cuenta & "|" & _
'                    lDsc_Instrumento & "|" & _
'                    lNemo & "|" & _
'                    Format(lReg_Det("cantidad").Value, "###,##0.00") & "|" & _
'                    Format(lReg_Det("precio").Value, "###,##0.00") & "|" & _
'                    Format(lReg_Det("monto_pago").Value, "###,##0.00") & "|" & _
'                    lReg_Ope("fecha_operacion").Value & "|" & _
'                    " " & "|" & _
'                    lReg_Ope("dsc_estado").Value & "|" & _
'                    lReg_Ope("dsc_tipo_operacion").Value
'
'          .AddTable sFormat_Det, sHeader_Ope, sRecord, clrHeader, , bAppend
'
'
'        .TableCell(tcFontBold, 0) = True
'        .EndTable
        .Paragraph = ""
    '  Next
    
    .EndDoc
  End With
  
  Call ColocarEn(lForm, Me, eFP_Abajo)
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub


Function Entrega_Nemotecnico(lId_Nemotecnico As String) As String
   Dim lcNemotecnicos As Class_Nemotecnicos
   Dim nemotecnico As String
   
   Set lcNemotecnicos = New Class_Nemotecnicos
    With lcNemotecnicos
      .Campo("id_nemotecnico").Valor = lId_Nemotecnico
      If .Buscar Then
        If .Cursor.Count > 0 Then
          nemotecnico = .Cursor(1).Fields("nemotecnico").Value
        End If
      End If
    End With
    
    Entrega_Nemotecnico = nemotecnico
    Set lcNemotecnicos = Nothing
    
End Function


Function Entrega_Instrumento(lCod_Instrumento As String) As String
   Dim lc_Instrumentos As Class_Instrumentos
   Dim instrumento As String
   
   Set lc_Instrumentos = New Class_Instrumentos
    With lc_Instrumentos
      .Campo("cod_instrumento").Valor = lCod_Instrumento
      If .Buscar Then
        If .Cursor.Count > 0 Then
          instrumento = .Cursor(1).Fields("dsc_intrumento").Value
        End If
      End If
    End With
    
    Entrega_Instrumento = instrumento
    Set lc_Instrumentos = Nothing
    
End Function

Function Entrega_Operacion(lId_Det_Operacion As String) As String
   Dim lcDetalle_Operacion As Class_Operaciones_Detalle
   Dim operacion As String
   
   Set lcDetalle_Operacion = New Class_Operaciones_Detalle
    With lcDetalle_Operacion
      .Campo("id_operacion_detalle").Valor = lId_Det_Operacion
      If .Buscar Then
        If .Cursor.Count > 0 Then
          operacion = .Cursor(1).Fields("id_operacion").Value
        End If
      End If
    End With
    
    Entrega_Operacion = operacion
    Set lcDetalle_Operacion = Nothing
    
End Function

Function Entrega_Estado(lCod_Estado As String) As String
   Dim lcEstado As Class_Estados
   Dim estado As String
   
   Set lcEstado = New Class_Estados
    With lcEstado
      .Campo("cod_estado").Valor = lCod_Estado
      If .Buscar Then
        If .Cursor.Count > 0 Then
          estado = .Cursor(1).Fields("dsc_estado").Value
        End If
      End If
    End With
    
    Entrega_Estado = estado
    Set lcEstado = Nothing
    
End Function
