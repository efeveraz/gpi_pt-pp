VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Cuenta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cuenta"
   ClientHeight    =   5520
   ClientLeft      =   1680
   ClientTop       =   1485
   ClientWidth     =   9135
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5520
   ScaleWidth      =   9135
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   34
      Top             =   0
      Width           =   9135
      _ExtentX        =   16113
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   35
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin C1SizerLibCtl.C1Tab SSTab 
      Height          =   5070
      Left            =   0
      TabIndex        =   36
      Top             =   360
      Width           =   9120
      _cx             =   16087
      _cy             =   8943
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   6579300
      ForeColor       =   -2147483634
      FrontTabColor   =   -2147483635
      BackTabColor    =   6579300
      TabOutlineColor =   -2147483632
      FrontTabForeColor=   -2147483634
      Caption         =   "&General|&Cajas|&Varios|&Tipos de Cambio|&Publicadores|&Alias|Perfil de &Riesgo"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   6
      Position        =   0
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   0   'False
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   -1  'True
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   0
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   -1  'True
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.PictureBox Picture6 
         BorderStyle     =   0  'None
         Height          =   4725
         Left            =   11235
         ScaleHeight     =   4725
         ScaleWidth      =   9090
         TabIndex        =   59
         Top             =   330
         Width           =   9090
         Begin VB.Frame Frame7 
            Height          =   4650
            Left            =   120
            TabIndex        =   60
            Top             =   0
            Width           =   8775
            Begin VB.Frame Frame9 
               Caption         =   "Porcentajes por Arbol Clase Instrumento"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   3015
               Left            =   120
               TabIndex        =   62
               Top             =   1560
               Width           =   8535
               Begin VSFlex8LCtl.VSFlexGrid Grilla_Perfil 
                  Height          =   2700
                  Left            =   120
                  TabIndex        =   32
                  Top             =   240
                  Width           =   8235
                  _cx             =   14526
                  _cy             =   4762
                  Appearance      =   1
                  BorderStyle     =   1
                  Enabled         =   -1  'True
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  MousePointer    =   0
                  BackColor       =   -2147483643
                  ForeColor       =   -2147483640
                  BackColorFixed  =   -2147483633
                  ForeColorFixed  =   -2147483630
                  BackColorSel    =   65535
                  ForeColorSel    =   0
                  BackColorBkg    =   -2147483643
                  BackColorAlternate=   -2147483643
                  GridColor       =   -2147483633
                  GridColorFixed  =   -2147483632
                  TreeColor       =   -2147483632
                  FloodColor      =   192
                  SheetBorder     =   -2147483642
                  FocusRect       =   2
                  HighLight       =   1
                  AllowSelection  =   -1  'True
                  AllowBigSelection=   -1  'True
                  AllowUserResizing=   1
                  SelectionMode   =   0
                  GridLines       =   2
                  GridLinesFixed  =   4
                  GridLineWidth   =   1
                  Rows            =   2
                  Cols            =   5
                  FixedRows       =   1
                  FixedCols       =   0
                  RowHeightMin    =   0
                  RowHeightMax    =   0
                  ColWidthMin     =   0
                  ColWidthMax     =   0
                  ExtendLastCol   =   -1  'True
                  FormatString    =   $"Frm_Cuenta.frx":0000
                  ScrollTrack     =   -1  'True
                  ScrollBars      =   3
                  ScrollTips      =   -1  'True
                  MergeCells      =   0
                  MergeCompare    =   0
                  AutoResize      =   -1  'True
                  AutoSizeMode    =   1
                  AutoSearch      =   0
                  AutoSearchDelay =   2
                  MultiTotals     =   -1  'True
                  SubtotalPosition=   1
                  OutlineBar      =   4
                  OutlineCol      =   0
                  Ellipsis        =   1
                  ExplorerBar     =   3
                  PicturesOver    =   0   'False
                  FillStyle       =   0
                  RightToLeft     =   0   'False
                  PictureType     =   0
                  TabBehavior     =   1
                  OwnerDraw       =   6
                  Editable        =   2
                  ShowComboButton =   2
                  WordWrap        =   0   'False
                  TextStyle       =   0
                  TextStyleFixed  =   0
                  OleDragMode     =   0
                  OleDropMode     =   0
                  ComboSearch     =   3
                  AutoSizeMouse   =   -1  'True
                  FrozenRows      =   0
                  FrozenCols      =   0
                  AllowUserFreezing=   0
                  BackColorFrozen =   0
                  ForeColorFrozen =   0
                  WallPaperAlignment=   6
                  AccessibleName  =   ""
                  AccessibleDescription=   ""
                  AccessibleValue =   ""
                  AccessibleRole  =   24
               End
            End
            Begin VB.Frame Frame8 
               Caption         =   "Datos Perfil"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   1410
               Left            =   120
               TabIndex        =   61
               Top             =   120
               Width           =   8535
               Begin hControl2.hTextLabel Txt_NombrePerfil 
                  Height          =   315
                  Left            =   360
                  TabIndex        =   29
                  Tag             =   "OBLI"
                  Top             =   600
                  Width           =   6885
                  _ExtentX        =   12144
                  _ExtentY        =   556
                  LabelWidth      =   1200
                  Caption         =   "Nombre Perfil"
                  Text            =   ""
                  Locked          =   -1  'True
                  BackColorTxt    =   12648447
                  BackColorTxt    =   12648447
                  MaxLength       =   50
               End
               Begin hControl2.hTextLabel Txt_AbrevPerfil 
                  Height          =   315
                  Left            =   360
                  TabIndex        =   30
                  Tag             =   "OBLI"
                  Top             =   960
                  Width           =   2895
                  _ExtentX        =   5106
                  _ExtentY        =   556
                  LabelWidth      =   1200
                  Caption         =   "Nombre Corto"
                  Text            =   ""
                  Locked          =   -1  'True
                  BackColorTxt    =   12648447
                  BackColorTxt    =   12648447
                  MaxLength       =   15
               End
               Begin hControl2.hTextLabel Txt_SaltoCtaPerfil 
                  Height          =   315
                  Left            =   4320
                  TabIndex        =   31
                  Tag             =   "OBLI"
                  Top             =   960
                  Width           =   2925
                  _ExtentX        =   5159
                  _ExtentY        =   556
                  LabelWidth      =   1200
                  Caption         =   "Salto Cuota (%)"
                  Text            =   "0"
                  Text            =   "0"
                  Locked          =   -1  'True
                  BackColorTxt    =   12648447
                  BackColorTxt    =   12648447
                  Format          =   "#,##0.00"
                  Tipo_TextBox    =   1
               End
               Begin TrueDBList80.TDBCombo Cmb_Perfil 
                  Height          =   335
                  Left            =   1575
                  TabIndex        =   28
                  Top             =   225
                  Width           =   5665
                  _ExtentX        =   10001
                  _ExtentY        =   582
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   582
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   0   'False
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Cuenta.frx":0136
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HFFFFFF&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin VB.Label lbl_moneda 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Perfil"
                  Height          =   315
                  Left            =   360
                  TabIndex        =   63
                  Top             =   245
                  Width           =   1200
               End
            End
         End
      End
      Begin VB.PictureBox TabAlias 
         BorderStyle     =   0  'None
         Height          =   4725
         Left            =   10935
         ScaleHeight     =   4725
         ScaleWidth      =   9090
         TabIndex        =   27
         Top             =   330
         Width           =   9090
      End
      Begin VB.PictureBox Picture5 
         BorderStyle     =   0  'None
         Height          =   4725
         Left            =   10635
         ScaleHeight     =   4725
         ScaleWidth      =   9090
         TabIndex        =   49
         Top             =   330
         Width           =   9090
         Begin VB.Frame Frame5 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   4305
            Left            =   90
            TabIndex        =   51
            Top             =   0
            Width           =   8835
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Publicadores 
               Height          =   3915
               Left            =   120
               TabIndex        =   26
               Top             =   210
               Width           =   8595
               _cx             =   15161
               _cy             =   6906
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   3
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Cuenta.frx":01E0
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   2
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
      End
      Begin VB.PictureBox Picture4 
         BorderStyle     =   0  'None
         Height          =   4725
         Left            =   10335
         ScaleHeight     =   4725
         ScaleWidth      =   9090
         TabIndex        =   48
         Top             =   330
         Width           =   9090
         Begin VB.Frame Frame2 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   4305
            Left            =   90
            TabIndex        =   50
            Top             =   0
            Width           =   8835
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Cambios 
               Height          =   3915
               Left            =   120
               TabIndex        =   25
               Top             =   210
               Width           =   8595
               _cx             =   15161
               _cy             =   6906
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   8
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Cuenta.frx":0288
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   2
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
      End
      Begin VB.PictureBox Picture3 
         BorderStyle     =   0  'None
         Height          =   4725
         Left            =   10035
         ScaleHeight     =   4725
         ScaleWidth      =   9090
         TabIndex        =   39
         Top             =   330
         Width           =   9090
         Begin VB.Frame Frame4 
            Height          =   4320
            Left            =   120
            TabIndex        =   47
            Top             =   150
            Width           =   8835
            Begin VB.TextBox Txt_Observaci�n 
               Height          =   2460
               Left            =   1530
               MaxLength       =   179
               MultiLine       =   -1  'True
               TabIndex        =   24
               Top             =   1620
               Width           =   7035
            End
            Begin VB.Frame Frame11 
               Height          =   1410
               Left            =   4050
               TabIndex        =   67
               Top             =   0
               Width           =   4785
               Begin VB.CheckBox Chk_Imp_Instrucciones 
                  Caption         =   "Impresi�n de Instrucciones"
                  Height          =   255
                  Left            =   135
                  TabIndex        =   22
                  Top             =   450
                  Width           =   4545
               End
               Begin VB.CheckBox Chk_Incorpora_Comi_Deven_VC 
                  Caption         =   "Incorpora Comisiones Devengadas en C�lculo Valor Cuota"
                  Height          =   405
                  Left            =   135
                  TabIndex        =   23
                  Top             =   675
                  Width           =   4575
               End
            End
            Begin VB.Frame Frame10 
               Height          =   1410
               Left            =   0
               TabIndex        =   66
               Top             =   0
               Width           =   4110
               Begin hControl2.hTextLabel txt_Porcen_rf 
                  Height          =   315
                  Left            =   225
                  TabIndex        =   20
                  Tag             =   "OBLI"
                  Top             =   360
                  Width           =   3525
                  _ExtentX        =   6218
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  TextMinWidth    =   1200
                  Caption         =   "Porcentaje RF"
                  Text            =   "0,0000"
                  Text            =   "0,0000"
                  BackColorTxt    =   12648384
                  BackColorTxt    =   12648384
                  Format          =   "#,##0.0000"
                  Tipo_TextBox    =   1
                  MaxLength       =   12
               End
               Begin hControl2.hTextLabel txt_Porcen_RV 
                  Height          =   315
                  Left            =   225
                  TabIndex        =   21
                  Tag             =   "OBLI"
                  Top             =   765
                  Width           =   3525
                  _ExtentX        =   6218
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  TextMinWidth    =   1200
                  Caption         =   "Porcentaje RV"
                  Text            =   "0,0000"
                  Text            =   "0,0000"
                  BackColorTxt    =   12648384
                  BackColorTxt    =   12648384
                  Format          =   "#,##0.0000"
                  Tipo_TextBox    =   1
                  MaxLength       =   12
               End
            End
            Begin VB.Label Label6 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Observaci�n"
               Height          =   330
               Left            =   225
               TabIndex        =   68
               Top             =   1620
               Width           =   1260
            End
         End
      End
      Begin VB.PictureBox Picture2 
         BorderStyle     =   0  'None
         Height          =   4725
         Left            =   9735
         ScaleHeight     =   4725
         ScaleWidth      =   9090
         TabIndex        =   38
         Top             =   330
         Width           =   9090
         Begin VB.Frame Frame3 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   4305
            Left            =   90
            TabIndex        =   45
            Top             =   0
            Width           =   8835
            Begin MSComctlLib.Toolbar Toolbar_Cajas_Cuenta 
               Height          =   360
               Left            =   120
               TabIndex        =   46
               Top             =   210
               Width           =   8595
               _ExtentX        =   15161
               _ExtentY        =   635
               ButtonWidth     =   2037
               ButtonHeight    =   582
               AllowCustomize  =   0   'False
               Wrappable       =   0   'False
               Appearance      =   1
               Style           =   1
               TextAlignment   =   1
               _Version        =   393216
               BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
                  NumButtons      =   3
                  BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Agregar"
                     Key             =   "ADD"
                     Description     =   "Agrega una Caja"
                     Object.ToolTipText     =   "Agrega una Caja"
                  EndProperty
                  BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "&Modificar"
                     Key             =   "UPDATE"
                     Description     =   "Modifica Caja Ingresada"
                     Object.ToolTipText     =   "Modifica Caja Ingresada"
                  EndProperty
                  BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                     Caption         =   "Eliminar"
                     Key             =   "DEL"
                     Description     =   "Elimina Caja"
                     Object.ToolTipText     =   "Elimina Caja"
                  EndProperty
               EndProperty
               BorderStyle     =   1
            End
            Begin VSFlex8LCtl.VSFlexGrid Grilla_Cajas 
               Height          =   3615
               Left            =   120
               TabIndex        =   19
               Top             =   570
               Width           =   8595
               _cx             =   15161
               _cy             =   6376
               Appearance      =   2
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   65535
               ForeColorSel    =   0
               BackColorBkg    =   -2147483643
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   2
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   1
               SelectionMode   =   3
               GridLines       =   10
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   2
               Cols            =   4
               FixedRows       =   1
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   -1  'True
               FormatString    =   $"Frm_Cuenta.frx":041E
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   -1  'True
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   2
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   1
               ExplorerBar     =   3
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
               AccessibleName  =   ""
               AccessibleDescription=   ""
               AccessibleValue =   ""
               AccessibleRole  =   24
            End
         End
      End
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   4725
         Left            =   15
         ScaleHeight     =   4285.714
         ScaleMode       =   0  'User
         ScaleWidth      =   9090
         TabIndex        =   37
         Top             =   330
         Width           =   9090
         Begin VB.Frame Frame1 
            Height          =   4695
            Left            =   120
            TabIndex        =   40
            Top             =   0
            Width           =   8835
            Begin VB.CheckBox Chk_Com_Ase_Hon_AfectaImp 
               Caption         =   "Comisi�n por Ases. y Hono. afecta Impto."
               Height          =   315
               Left            =   1620
               TabIndex        =   8
               Top             =   3120
               Width           =   3270
            End
            Begin VB.Frame Pnl_TipoAhorroAPV 
               Caption         =   "Tipo Ahorro APV"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   675
               Left            =   5040
               TabIndex        =   64
               Top             =   3840
               Width           =   3675
               Begin TrueDBList80.TDBCombo Cmb_TipoAhorroAPV 
                  Height          =   345
                  Left            =   45
                  TabIndex        =   18
                  Top             =   240
                  Width           =   3555
                  _ExtentX        =   6271
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   0   'False
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Cuenta.frx":04DD
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HFFFFFF&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
            End
            Begin VB.Frame Frame6 
               Height          =   1515
               Left            =   120
               TabIndex        =   53
               Top             =   1560
               Width           =   4875
               Begin VB.CheckBox Chk_Mov_Descubierto 
                  Caption         =   "Operar sin caja"
                  Height          =   405
                  Left            =   3720
                  TabIndex        =   6
                  Top             =   360
                  Width           =   915
               End
               Begin TrueDBList80.TDBCombo Cmb_Moneda 
                  Height          =   345
                  Left            =   1440
                  TabIndex        =   3
                  Tag             =   "OBLI=S;CAPTION=Moneda"
                  Top             =   240
                  Width           =   2115
                  _ExtentX        =   3731
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   0   'False
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Cuenta.frx":0587
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin TrueDBList80.TDBCombo Cmb_Estado 
                  Height          =   345
                  Left            =   1440
                  TabIndex        =   4
                  Tag             =   "OBLI=S;CAPTION=Estado"
                  Top             =   600
                  Width           =   2115
                  _ExtentX        =   3731
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   0   'False
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Cuenta.frx":0631
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin TrueDBList80.TDBCombo Cmb_Asesor 
                  Height          =   345
                  Left            =   1440
                  TabIndex        =   5
                  Tag             =   "OBLI=S;CAPTION=Asesor"
                  Top             =   960
                  Width           =   3375
                  _ExtentX        =   5953
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   0   'False
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Cuenta.frx":06DB
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin VB.Label Label5 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Asesor"
                  Height          =   345
                  Left            =   120
                  TabIndex        =   65
                  Top             =   960
                  Width           =   1305
               End
               Begin VB.Label Label1 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Estado"
                  Height          =   345
                  Left            =   120
                  TabIndex        =   55
                  Top             =   600
                  Width           =   1305
               End
               Begin VB.Label Label8 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Moneda"
                  Height          =   345
                  Left            =   120
                  TabIndex        =   54
                  Top             =   240
                  Width           =   1305
               End
            End
            Begin hControl2.hTextLabel Txt_Dsc_Cuenta 
               Height          =   315
               Left            =   5040
               TabIndex        =   12
               Tag             =   "OBLI"
               Top             =   1320
               Width           =   3645
               _ExtentX        =   6429
               _ExtentY        =   556
               LabelWidth      =   1500
               Caption         =   "Descripci�n"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               MaxLength       =   20
            End
            Begin VB.Frame Frame_Cliente 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   1455
               Left            =   120
               TabIndex        =   42
               Top             =   120
               Width           =   4875
               Begin hControl2.hTextLabel Txt_Cliente_Nombre 
                  Height          =   315
                  Left            =   150
                  TabIndex        =   2
                  Top             =   1020
                  Width           =   4605
                  _ExtentX        =   8123
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Nombre"
                  Text            =   ""
                  Locked          =   -1  'True
                  BackColorTxt    =   12648447
                  BackColorTxt    =   12648447
               End
               Begin hControl2.hTextLabel Txt_Cliente_RUT 
                  Height          =   315
                  Left            =   150
                  TabIndex        =   1
                  Top             =   660
                  Width           =   2865
                  _ExtentX        =   5054
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "RUT"
                  Text            =   ""
                  Locked          =   -1  'True
                  BackColorTxt    =   12648447
                  BackColorTxt    =   12648447
               End
               Begin TrueDBList80.TDBCombo Cmb_Cliente 
                  Height          =   345
                  Left            =   1470
                  TabIndex        =   0
                  Tag             =   "OBLI=S;CAPTION=Cliente"
                  Top             =   270
                  Width           =   3285
                  _ExtentX        =   5794
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   0   'False
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Cuenta.frx":0785
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin VB.Label Label3 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Cliente"
                  Height          =   345
                  Left            =   150
                  TabIndex        =   43
                  Top             =   270
                  Width           =   1185
               End
            End
            Begin VB.CheckBox Chk_Bloqueada 
               Caption         =   "Bloqueada"
               Height          =   195
               Left            =   240
               TabIndex        =   7
               Top             =   3165
               Width           =   1095
            End
            Begin MSComCtl2.DTPicker DTP_Fecha_Operativa 
               Height          =   345
               Left            =   6555
               TabIndex        =   13
               Top             =   1800
               Width           =   1635
               _ExtentX        =   2884
               _ExtentY        =   609
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   17039361
               CurrentDate     =   38985
            End
            Begin hControl2.hTextLabel Txt_N_Cuenta 
               Height          =   315
               Left            =   5040
               TabIndex        =   10
               Tag             =   "OBLI"
               Top             =   240
               Width           =   3645
               _ExtentX        =   6429
               _ExtentY        =   556
               LabelWidth      =   1500
               TextMinWidth    =   1200
               Caption         =   "N� de Cuenta"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               MaxLength       =   12
            End
            Begin hControl2.hTextLabel Txt_Abr_Cuenta 
               Height          =   315
               Left            =   5040
               TabIndex        =   33
               Top             =   960
               Width           =   3645
               _ExtentX        =   6429
               _ExtentY        =   556
               LabelWidth      =   1500
               Caption         =   "Abreviatura"
               Text            =   ""
               MaxLength       =   12
            End
            Begin TrueDBList80.TDBCombo Cmb_Tipo_Administracion 
               Height          =   345
               Left            =   6555
               TabIndex        =   16
               Tag             =   "OBLI=S;CAPTION=Tipo Administraci�n"
               Top             =   3100
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   0   'False
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Cuenta.frx":082F
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin VB.Frame Frame_Bloqueo 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   1245
               Left            =   120
               TabIndex        =   41
               Top             =   3330
               Width           =   4875
               Begin VB.TextBox Txt_Obs_Bloqueada 
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   945
                  Left            =   90
                  MaxLength       =   119
                  MultiLine       =   -1  'True
                  TabIndex        =   9
                  Top             =   210
                  Width           =   4665
               End
            End
            Begin MSComCtl2.DTPicker DTP_Fecha_Cierre 
               Height          =   345
               Left            =   6555
               TabIndex        =   15
               Top             =   2520
               Width           =   1635
               _ExtentX        =   2884
               _ExtentY        =   609
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   17039361
               CurrentDate     =   38985
            End
            Begin TrueDBList80.TDBCombo Cmb_Tipo_Cuenta 
               Height          =   345
               Left            =   6555
               TabIndex        =   11
               Tag             =   "OBLI=S;CAPTION=Tipo Cuenta"
               Top             =   600
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   609
               _LayoutType     =   0
               _RowHeight      =   -2147483647
               _WasPersistedAsPixels=   0
               _DropdownWidth  =   0
               _EDITHEIGHT     =   609
               _GAPHEIGHT      =   53
               Columns(0)._VlistStyle=   0
               Columns(0)._MaxComboItems=   5
               Columns(0).DataField=   "ub_grid1"
               Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns(1)._VlistStyle=   0
               Columns(1)._MaxComboItems=   5
               Columns(1).DataField=   "ub_grid2"
               Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
               Columns.Count   =   2
               Splits(0)._UserFlags=   0
               Splits(0).ExtendRightColumn=   -1  'True
               Splits(0).AllowRowSizing=   0   'False
               Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
               Splits(0)._ColumnProps(0)=   "Columns.Count=2"
               Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
               Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
               Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
               Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
               Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
               Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
               Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
               Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
               Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
               Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
               Splits.Count    =   1
               Appearance      =   3
               BorderStyle     =   1
               ComboStyle      =   0
               AutoCompletion  =   -1  'True
               LimitToList     =   0   'False
               ColumnHeaders   =   0   'False
               ColumnFooters   =   0   'False
               DataMode        =   5
               DefColWidth     =   0
               Enabled         =   -1  'True
               HeadLines       =   1
               FootLines       =   1
               RowDividerStyle =   0
               Caption         =   ""
               EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
               LayoutName      =   ""
               LayoutFileName  =   ""
               MultipleLines   =   0
               EmptyRows       =   0   'False
               CellTips        =   0
               AutoSize        =   0   'False
               ListField       =   ""
               BoundColumn     =   ""
               IntegralHeight  =   0   'False
               CellTipsWidth   =   0
               CellTipsDelay   =   1000
               AutoDropdown    =   -1  'True
               RowTracking     =   -1  'True
               RightToLeft     =   0   'False
               MouseIcon       =   0
               MouseIcon.vt    =   3
               MousePointer    =   0
               MatchEntryTimeout=   2000
               OLEDragMode     =   0
               OLEDropMode     =   0
               AnimateWindow   =   3
               AnimateWindowDirection=   5
               AnimateWindowTime=   200
               AnimateWindowClose=   1
               DropdownPosition=   0
               Locked          =   0   'False
               ScrollTrack     =   -1  'True
               ScrollTips      =   -1  'True
               RowDividerColor =   14215660
               RowSubDividerColor=   14215660
               MaxComboItems   =   10
               AddItemSeparator=   ";"
               _PropDict       =   $"Frm_Cuenta.frx":08D9
               _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
               _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
               _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
               _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
               _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
               _StyleDefs(5)   =   ":id=0,.fontname=Arial"
               _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
               _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
               _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
               _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
               _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
               _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
               _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
               _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
               _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
               _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
               _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
               _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
               _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
               _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
               _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
               _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
               _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
               _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
               _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
               _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
               _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
               _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
               _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
               _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
               _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
               _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
               _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
               _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
               _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
               _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
               _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
               _StyleDefs(38)  =   "Named:id=33:Normal"
               _StyleDefs(39)  =   ":id=33,.parent=0"
               _StyleDefs(40)  =   "Named:id=34:Heading"
               _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(42)  =   ":id=34,.wraptext=-1"
               _StyleDefs(43)  =   "Named:id=35:Footing"
               _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
               _StyleDefs(45)  =   "Named:id=36:Selected"
               _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(47)  =   "Named:id=37:Caption"
               _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
               _StyleDefs(49)  =   "Named:id=38:HighlightRow"
               _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
               _StyleDefs(51)  =   "Named:id=39:EvenRow"
               _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
               _StyleDefs(53)  =   "Named:id=40:OddRow"
               _StyleDefs(54)  =   ":id=40,.parent=33"
               _StyleDefs(55)  =   "Named:id=41:RecordSelector"
               _StyleDefs(56)  =   ":id=41,.parent=34"
               _StyleDefs(57)  =   "Named:id=42:FilterBar"
               _StyleDefs(58)  =   ":id=42,.parent=33"
            End
            Begin MSComCtl2.DTPicker DTP_Fecha_Contrato 
               Height          =   345
               Left            =   6555
               TabIndex        =   14
               Top             =   2160
               Width           =   1635
               _ExtentX        =   2884
               _ExtentY        =   609
               _Version        =   393216
               CheckBox        =   -1  'True
               Format          =   17039361
               CurrentDate     =   38985
            End
            Begin hControl2.hTextLabel Txt_Num_Folio 
               Height          =   315
               Left            =   5040
               TabIndex        =   17
               Tag             =   "e"
               Top             =   3480
               Width           =   3645
               _ExtentX        =   6429
               _ExtentY        =   556
               LabelWidth      =   1500
               Caption         =   "N� Folio"
               Text            =   ""
               Locked          =   -1  'True
               BackColorTxt    =   12648447
               BackColorTxt    =   12648447
            End
            Begin VB.Label lbl_Fecha_Cierre 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha de Cierre"
               Height          =   315
               Left            =   5040
               TabIndex        =   58
               Top             =   2520
               Width           =   1485
            End
            Begin VB.Label lbl_Fecha_Operativa 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha Operativa"
               Height          =   315
               Left            =   5040
               TabIndex        =   57
               Top             =   1800
               Width           =   1485
            End
            Begin VB.Label lbl_Fecha_Contrato 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Fecha de Contrato"
               Height          =   315
               Left            =   5040
               TabIndex        =   56
               Top             =   2160
               Width           =   1485
            End
            Begin VB.Label Label9 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Tipo de Cuenta"
               Height          =   345
               Left            =   5040
               TabIndex        =   52
               Top             =   600
               Width           =   1485
            End
            Begin VB.Label Label2 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Tipo Administraci�n"
               Height          =   345
               Left            =   5040
               TabIndex        =   44
               Top             =   3100
               Width           =   1485
            End
         End
      End
   End
End
Attribute VB_Name = "Frm_Cuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Dim fBorrar As hRecord
Dim fFormAlias As Object

'APV
Dim fBorrarRelTA_C As hRecord
Dim fAgregaRelTA_C As hRecord
Dim bEsAPV As Boolean
'--------------------------------
Const c_porcentajeperfil = 1
Const fc_EsMoneda_Pago = "S" ' solo las monedas que sean moneda de pago

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String
Dim fId_Perfil_Riesgo   As Integer
Dim fTipoCuenta As String
Dim PasoCmbCliente As Boolean ' 14/07/2009 EREINOSO (De acuerdo a lo conversado con: JG/DO/MM)
Dim MarcaXCarga As Boolean

Private Enum eNum_Perfil
  Nombre = 0
  NomCorto = 1
  SaltoCuota = 2
  IdPerfil = 3
End Enum

Private Sub Chk_Bloqueada_Click()
   Frame_Bloqueo.Enabled = (Chk_Bloqueada.Value = vbChecked)
End Sub

Private Sub Chk_Com_Ase_Hon_AfectaImp_Click()
    If fTipoCuenta = "APV" Then
        If Chk_Com_Ase_Hon_AfectaImp.Value = 1 And Not MarcaXCarga Then
            If MsgBox("� Est� seguro que las comisiones ser�n afectas a Impuesto ?", vbQuestion + vbYesNo, "Asesor�a y Honorarios") = vbNo Then
                Chk_Com_Ase_Hon_AfectaImp.Value = 0
            End If
        End If
    Else
        If Chk_Com_Ase_Hon_AfectaImp.Value = 0 And Not MarcaXCarga Then
            If MsgBox("� Est� seguro que las comisiones NO ser�n afectas a Impuesto ?", vbQuestion + vbYesNo, "Asesor�a y Honorarios") = vbNo Then
                Chk_Com_Ase_Hon_AfectaImp.Value = 1
            End If
        End If
    
    End If
End Sub

Private Sub Cmb_Cliente_ItemChange()
Dim lcClientes As Object
'--------------------------------------------
Dim lId_Cliente As String
Dim lReg As hFields
Dim lId_Asesor As String
Dim lNro_Cuentas_Cliente As Long
Dim lMensaje As String
Dim lFechaContrato As Date
Dim Estado_Cli As String
  lId_Cliente = Fnt_FindValue4Display(Cmb_Cliente, Cmb_Cliente.Text)

  If Not lId_Cliente = "" Then
    PasoCmbCliente = True
    If fKey = cNewEntidad Then
      If Fnt_Busca_Cuentas_X_Cliente(lId_Cliente, lNro_Cuentas_Cliente, Estado_Cli) Then
      
        ' cliente anulado
        If Estado_Cli = "A" Then
            MsgBox "El cliente """ & Cmb_Cliente.Text & """ Esta anulado" & vbCrLf & "No podra asignarle una cuenta", vbInformation, Me.Caption
            GoTo SetProcedure
        End If
        
        If lNro_Cuentas_Cliente > 1 Then
          lMensaje = "El cliente """ & Cmb_Cliente.Text & """ tiene " & lNro_Cuentas_Cliente & " cuentas ya en el sistema." & vbCr & "�Desea continuar operando con el cliente?"
        Else
          lMensaje = "El cliente """ & Cmb_Cliente.Text & """ tiene ya una cuenta en el sistema." & vbCr & "�Desea continuar operando con el cliente?"
        End If
        
        If MsgBox(lMensaje, vbYesNo + vbInformation + vbDefaultButton2, Me.Caption) = vbNo Then
          GoTo SetProcedure
        End If
'----------------
        lFechaContrato = Fnt_Busca_Cuentas_Fec_Contrato(lId_Cliente)
        DTP_Fecha_Contrato.Value = NVL(lFechaContrato, "")
        DTP_Fecha_Contrato.Enabled = IIf(lFechaContrato = 0, True, False)
'----------------
      End If
    End If
    
    lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
    Call Sub_CargaCombo_Asesor_Cliente(Cmb_Asesor, pBlanco:=True, pId_Cliente:=lId_Cliente)
    Call Sub_ComboSelectedItem(Cmb_Asesor, lId_Asesor)
    
    Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
    With lcClientes
      Set .gDB = gDB 'Se pasa la conexion
      .Campo("Id_Cliente").Valor = lId_Cliente
      If .Buscar(True) Then
        For Each lReg In .Cursor
          Txt_Cliente_RUT.Text = lReg("rut_cliente").Value
          Txt_Cliente_Nombre.Text = lReg("Nombre_cliente").Value
        Next
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al cargar datos del Cliente.", _
                        .ErrMsg, _
                        pConLog:=True)
      End If
    End With
    Set lcClientes = Nothing
  End If
  
  Exit Sub
  
SetProcedure:
  Call Sub_ComboSelectedItem(Cmb_Cliente, "")
  
End Sub

Private Function Fnt_Busca_Cuentas_X_Cliente(pId_Cliente, ByRef pNro_Cuentas_Cliente As Long, ByRef Estado_Cli As String) As Boolean
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lReg As hFields
Dim lpaso As String

'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cliente").Valor = pId_Cliente
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pNro_Cuentas_Cliente = .Cursor.Count
        ' toma el estado del cliente
        For Each lReg In .Cursor
            Estado_Cli = Trim(UCase(lReg("cod_estado_cliente").Value))
        Next
        Fnt_Busca_Cuentas_X_Cliente = True
      Else
        GoTo ErrProcedure
      End If
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  Set lcCuenta = Nothing
  
  Exit Function
  
ErrProcedure:
  Fnt_Busca_Cuentas_X_Cliente = False
  Set lcCuenta = Nothing
  
End Function


Private Function Fnt_Busca_Cuentas_Fec_Contrato(pId_Cliente) As Date
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lReg As hFields
Dim lfecha_contrato As Date

    lfecha_contrato = 0
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cliente").Valor = pId_Cliente
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar Then
'-----
        For Each lReg In .Cursor
          If (Not IsNull(lReg("FECHA_CONTRATO").Value)) Then
            lfecha_contrato = lReg("FECHA_CONTRATO").Value
            Exit For
          End If
        Next
    '-----
        Fnt_Busca_Cuentas_Fec_Contrato = lfecha_contrato
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  Set lcCuenta = Nothing
  
  
  Exit Function
  
ErrProcedure:
  Fnt_Busca_Cuentas_Fec_Contrato = ""
  Set lcCuenta = Nothing
  
End Function

Private Sub Cmb_Cliente_LostFocus()
  If Not PasoCmbCliente Then
    Call Cmb_Cliente_ItemChange
  End If
End Sub

Private Sub Cmb_Perfil_ItemChange()
    Dim lId_Perfil As String
    Dim lcRestricc_Porc_Producto As Class_Restricc_Porc_Producto
    'Dim lcRestriccion As Class_RestriccPorcArbolClase
    Dim lReg As hFields
    Dim lFila As Long
    'Dim lID_PADRE_ARBOL_CLASE_INST As Integer
    Dim lId_Arbol_Clase_Inst As String

    Call Sub_Bloquea_Puntero(Me)
    Me.Enabled = False
    
    If LCase(Left(Me.Caption, 12)) = "modificaci�n" Then
        If MsgBox("Al Cambiar el perfil se cargaran los porcentajes asociados al nuevo perfil." & Chr(13) _
                        & "� Est� seguro de continuar con el cambio ?", vbQuestion + vbYesNo, "Cambio de Perfil") = vbNo Then
            GoTo Salir
        End If
    End If
    
    lId_Perfil = Fnt_FindValue4Display(Cmb_Perfil, Cmb_Perfil.Text, 0)
    
    If Not IsNull(lId_Perfil) Then
        Txt_NombrePerfil.Text = Cmb_Perfil.Columns(eNum_Perfil.Nombre).Value
        Txt_AbrevPerfil.Text = Cmb_Perfil.Columns(eNum_Perfil.NomCorto).Value
        Txt_SaltoCtaPerfil.Text = Cmb_Perfil.Columns(eNum_Perfil.SaltoCuota).Value
    
        For lFila = 1 To Grilla_Perfil.Rows - 1
            Rem  Carga los Porcentajes de Restricciones de Perfiles de Riesgo por Productos
            lId_Arbol_Clase_Inst = GetCell(Grilla_Perfil, lFila, "id_arbol_clase_inst")
            Call SetCell(Grilla_Perfil, lFila, "porcentaje", "")
            
            If Not lId_Arbol_Clase_Inst = "" Then
                Set lcRestricc_Porc_Producto = New Class_Restricc_Porc_Producto
                With lcRestricc_Porc_Producto
                    .Campo("id_arbol_clase_inst").Valor = lId_Arbol_Clase_Inst
                    .Campo("id_perfil_riesgo").Valor = lId_Perfil
                    If .Buscar Then
                        If .Cursor.Count > 0 Then
                            Call SetCell(Grilla_Perfil, lFila, "id_restriccion", .Cursor(1).Fields("id_restricc_porc_prod").Value)
                            Call SetCell(Grilla_Perfil, lFila, "porcentaje", To_Number(.Cursor(1).Fields("porcentaje").Value * 100))
                        End If
                    Else
                        Call Fnt_MsgError(.SubTipo_LOG, _
                        "No se pudo completar la cargar de Restricciones de Porcentaje por por Arbol Clase Instrumento.", _
                        .ErrMsg, _
                        pConLog:=True)
                        Exit For
                    End If
                End With
                Set lcRestricc_Porc_Producto = Nothing
            End If
        Next
    
'''''        For lFila = 1 To Grilla_Perfil.Rows - 1
'''''            Call SetCell(Grilla_Perfil, lFila, "porcentaje", "")
'''''            lID_PADRE_ARBOL_CLASE_INST = GetCell(Grilla_Perfil, lFila, "id_padre_arbol_clase_inst")
'''''            lId_Arbol_Clase_Inst = GetCell(Grilla_Perfil, lFila, "id_arbol_clase_inst")
'''''            If Not lID_PADRE_ARBOL_CLASE_INST = 0 Then
'''''
'''''                Set lcRestriccion = New Class_RestriccPorcArbolClase
'''''
'''''                With lcRestriccion
'''''                  .Campo("id_arbol_clase_inst").Valor = lId_Arbol_Clase_Inst
'''''                  .Campo("id_perfil_riesgo").Valor = lId_Perfil
'''''                  If .Buscar Then
'''''                    If .Cursor.Count > 0 Then
'''''                      Call SetCell(Grilla_Perfil, lFila, "id_restriccion", .Cursor(1).Fields("id_restriccion").Value)
'''''                      Call SetCell(Grilla_Perfil, lFila, "porcentaje", To_Number(.Cursor(1).Fields("porcentaje").Value * 100))
'''''                    End If
'''''                  Else
'''''                    Call Fnt_MsgError(.SubTipo_LOG, _
'''''                                      "No se pudo completar la cargar de Restricciones de Porcentaje por Arbol Clase Instrumento.", _
'''''                                      .ErrMsg, _
'''''                                      pConLog:=True)
'''''                    Exit For
'''''                  End If
'''''                End With
'''''                Set lcRestriccion = Nothing
'''''            End If
'''''        Next
    
    Else
        
        Txt_NombrePerfil.Text = ""
        Txt_AbrevPerfil.Text = ""
        Txt_SaltoCtaPerfil.Text = ""
        For lFila = 1 To Grilla_Perfil.Rows - 1
            Call SetCell(Grilla_Perfil, lFila, "porcentaje", "")
        Next
    End If
    
Salir:
    Cmb_Perfil.Text = Txt_NombrePerfil.Text
    Me.Enabled = True
    Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Sub Cmb_Perfil_Validate(Cancel As Boolean)
    Dim lFila As Long
    
    If Not Cmb_Perfil.Text = "" Then
    
        Rem si el perfil no es NULL
        If Fnt_FindValue4Display(Cmb_Perfil, Cmb_Perfil.Text) = "" Then
            MsgBox "El perfil ingresado no existe.", vbCritical, Me.Caption
            Cancel = True
            Cmb_Perfil.Text = ""
            
            Txt_NombrePerfil.Text = ""
            Txt_AbrevPerfil.Text = ""
            Txt_SaltoCtaPerfil.Text = ""
            For lFila = 1 To Grilla_Perfil.Rows - 1
                Call SetCell(Grilla_Perfil, lFila, "porcentaje", "")
            Next
        End If
  
  End If
End Sub
Private Sub Cmb_Tipo_Cuenta_ItemChange()
    If Cmb_Tipo_Cuenta.Text = cTIPOCUENTA_APV Then
        Cmb_TipoAhorroAPV.Enabled = True
        fTipoCuenta = "APV"
        Chk_Com_Ase_Hon_AfectaImp.Value = 0
    Else
        Cmb_TipoAhorroAPV.Enabled = False
        fTipoCuenta = ""
        Chk_Com_Ase_Hon_AfectaImp.Value = 1
    End If
End Sub
Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  With Toolbar_Cajas_Cuenta
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
  End With


  '-----------------------------------------------------------
  '-- Alias
  '-----------------------------------------------------------
  Set fFormAlias = Proce.CargaFormularioAlias
  fFormAlias.CodigoCSBPI = cTabla_Cuentas
  Call Proce.AnexaFormulario(fFormAlias, Me.TabAlias)
  '-----------------------------------------------------------
    
  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pId_Cuenta, pCod_Arbol_Sistema)
  fKey = pId_Cuenta
  
  Load Me
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  Call Form_Resize
   
  Call Sub_CargaForm
  Call Sub_CargarDatos
   
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de Cuenta"
    'Me.Cmb_Perfil.Enabled = True
  Else
    Me.Caption = "Modificaci�n Cuenta: " & Txt_N_Cuenta.Text & " - " & Txt_Dsc_Cuenta.Text
    'Me.Cmb_Perfil.Enabled = False
  End If
   
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  On Error Resume Next
  Unload fFormAlias
End Sub


Private Sub Grilla_Cajas_DblClick()
  With Grilla_Cajas
    If .Row >= 1 And Not GetCell(Grilla_Cajas, .Row, "Colum_PK") = "" Then
      Call Sub_EsperaVentana_Cajas(GetCell(Grilla_Cajas, .Row, "Colum_PK"))
    End If
  End With
End Sub

Private Sub Grilla_Cambios_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla_Cambios.ColIndex("Seleccion") Then
    Cancel = True
  End If
End Sub

Private Sub Grilla_Perfil_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
Dim lId_Padre As Integer
    lId_Padre = GetCell(Grilla_Perfil, Row, "id_padre_arbol_clase_inst")
    If Col = c_porcentajeperfil Then  'And lId_Padre > 0 Then
        Grilla_Perfil.Editable = flexEDKbdMouse
    Else
        Cancel = True
'        Grilla_Perfil.Editable = flexEDNone
    End If
End Sub

Private Sub Grilla_Perfil_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
Dim lId_Padre As Integer
  If Not KeyAscii = 8 And Not KeyAscii = 46 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
    Exit Sub
  End If
    
    lId_Padre = GetCell(Grilla_Perfil, Row, "id_padre_arbol_clase_inst")
    If Col = c_porcentajeperfil And lId_Padre > 0 Then
        Call Sub_EsASCIINumero(KeyAscii)
    End If
End Sub

Private Sub Grilla_Perfil_ValidateEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Trim(Grilla_Perfil.EditText) = "" Then
    If To_Number(Grilla_Perfil.EditText) > 100 Then
      MsgBox "Los porcentajes no pueden ser mayores a 100", vbCritical, Me.Caption
      Cancel = True
    End If
  End If
    
Rem 10/09/2009 JVIDAL
Rem Est� versi�n no se encuentra en producci�n y de acuerdo a lo conversado con: JG,DO,MM se comenta
Rem para mantener lo que se encuentra disponible en certificaci�n.
'    Dim lngSumaPorcentaje As Double
'    Dim lngIdPadre As Long
'
'    lngIdPadre = GetCell(Grilla_Perfil, Row, "id_padre_arbol_clase_inst")
'
'    If Not Trim(Grilla_Perfil.EditText) = "" Then
'        If lngIdPadre = 0 Then
'            If To_Number(Grilla_Perfil.EditText) > 100 Then
'                MsgBox "Los porcentajes no pueden ser mayores a 100", vbCritical, Me.Caption
'                Cancel = True
'            End If
'        Else
'            lngSumaPorcentaje = validaPorcentaje(Grilla_Perfil, Grilla_Perfil.EditText, lngIdPadre, Row)
'
'            If lngSumaPorcentaje > 100 Then
'                MsgBox "Los porcentajes de las Familias no pueden ser mayores a 100", vbCritical, Me.Caption
'                Cancel = True
'            End If
'        End If
'    End If
End Sub

Private Sub Grilla_Publicadores_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla_Publicadores.ColIndex("DSC_PUBLICADOR") Then
    Cancel = True
  End If
End Sub




Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
    
  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        If MsgBox("Cuenta guardada correctamente." & vbCr & "�Desea Imprimir la Cuenta?.", vbYesNo + vbDefaultButton2 + vbInformation) = vbYes Then
          Call Frm_Reporte_Cuentas_C.Sub_Reporte_Cuenta_desde_Mantenedor(fKey)
        End If
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargaForm
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lcRel_Cuenta_Tipo_Cambio As Class_Rel_Cuenta_Tipo_Cambio
'---------------------------------
Dim lId_Cliente As String
Dim lCod_Estado As String
Dim lcod_Tipo_Administracion As String
Dim lid_Contrato_Cuenta As String
'Dim lid_Grupo_Cuenta As String    ==== Eliminada por HMR el 27/06/06
Dim lid_Empresa As String
Dim lFlg_Bloqueado As String
Dim lflg_Imp_Instrucciones As String
Dim lId_Perfil_Riesgo As Integer
Dim lId_Asesor As String
Dim lId_Moneda As String
Dim lFlg_Mov_Descubiertos As String
Dim lFlg_Com_Ase_Hon_AI As String

Dim lPorcen_rf As String
Dim lPorcen_rv As String

'---------------------------------
Dim lResult  As Boolean
Dim lFila As Long
Dim lReg As hFields
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
'---------------------------------
Dim lc_Rel_Cuenta_Instrum_Public As Class_Rel_Cuenta_Instr_Publicador

'------------<RNMM
Dim lNum_Folio As String
Dim lId_TipoCuenta As String
'------------RNMM>

'APV
Dim sTipoAhorro As String
Dim lFlg_Comision_VC As String

On Error GoTo ErrProcedure

    Fnt_Grabar = True
    
    Call gDB.IniciarTransaccion
    
    lResult = True
    
    If Not Fnt_ValidarDatos Then
      lResult = False
      GoTo ErrProcedure
    End If

    lResult = Fnt_GrabaItemPerfilRiesgo(fKey)
    If Not lResult Then
        GoTo ErrProcedure
    End If
      
    lId_Cliente = Fnt_ComboSelected_KEY(Cmb_Cliente)
    lCod_Estado = Fnt_ComboSelected_KEY(Cmb_Estado)
    lcod_Tipo_Administracion = Fnt_ComboSelected_KEY(Cmb_Tipo_Administracion)
    lid_Contrato_Cuenta = ""
    'lid_Grupo_Cuenta = Fnt_ComboSelected_KEY(Cmb_Grupo_Cuentas) ==== Eliminada por HMR 27/06/06
    lid_Empresa = Fnt_EmpresaActual
    lFlg_Bloqueado = IIf(Chk_Bloqueada.Value = vbChecked, gcFlg_SI, gcFlg_NO)
    lflg_Imp_Instrucciones = IIf(Chk_Imp_Instrucciones.Value = vbChecked, gcFlg_SI, gcFlg_NO)
    lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
    'lId_Perfil_Riesgo = Fnt_ComboSelected_KEY(Cmb_Perfil)
    lId_Moneda = Fnt_ComboSelected_KEY(Cmb_Moneda)
    lFlg_Mov_Descubiertos = IIf(Chk_Mov_Descubierto.Value = vbChecked, gcFlg_SI, gcFlg_NO)
  
    'JOVB
    lPorcen_rf = txt_Porcen_rf.Text
    lPorcen_rv = txt_Porcen_RV.Text
  

  '------------<RNMM
    lNum_Folio = Txt_Num_Folio.Text
    lId_TipoCuenta = Fnt_ComboSelected_KEY(Cmb_Tipo_Cuenta)
  '------------RNMM>
  '------------APV
    If Cmb_Tipo_Cuenta.Text = cTIPOCUENTA_APV Then
        sTipoAhorro = CStr(Fnt_ComboSelected_KEY(Cmb_TipoAhorroAPV))
    Else
        sTipoAhorro = ""
    End If
  '------------
  
  lFlg_Comision_VC = IIf(Chk_Incorpora_Comi_Deven_VC.Value = vbChecked, gcFlg_SI, gcFlg_NO)
  lFlg_Com_Ase_Hon_AI = IIf(Chk_Com_Ase_Hon_AfectaImp.Value = vbChecked, gcFlg_SI, gcFlg_NO)
  
  Rem Se guardan los datos de la Cuenta
'  Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
      .Campo("id_Cuenta").Valor = fKey
      .Campo("id_Cliente").Valor = lId_Cliente
      .Campo("Id_Tipo_Estado").Valor = cTEstado_Cuenta
      .Campo("cod_Estado").Valor = lCod_Estado
      .Campo("cod_Tipo_Administracion").Valor = lcod_Tipo_Administracion
      .Campo("id_Contrato_Cuenta").Valor = lid_Contrato_Cuenta
      'Call .Parametros.Add("Pid_Grupo_Cuenta", ePT_Caracter, lid_Grupo_Cuenta, ePD_Entrada) ==== Eliminada por HMR el 27/06/06
      .Campo("id_Empresa").Valor = lid_Empresa
      .Campo("num_Cuenta").Valor = Txt_N_Cuenta.Text
      .Campo("abr_Cuenta").Valor = Txt_Abr_Cuenta.Text
      .Campo("dsc_Cuenta").Valor = Txt_Dsc_Cuenta.Text
      .Campo("observacion").Valor = Txt_Observaci�n.Text
      .Campo("flg_Bloqueado").Valor = lFlg_Bloqueado
      .Campo("obs_Bloqueo").Valor = Txt_Obs_Bloqueada.Text
      .Campo("flg_Imp_Instrucciones").Valor = lflg_Imp_Instrucciones
      If Not lId_Asesor = cCmbKBLANCO Then
        .Campo("id_Asesor").Valor = lId_Asesor
      End If
      .Campo("id_perfil_riesgo").Valor = fId_Perfil_Riesgo
      .Campo("id_moneda").Valor = lId_Moneda
      .Campo("fecha_operativa").Valor = DTP_Fecha_Operativa.Value
      
      .Campo("fecha_cierre_cuenta").Valor = IIf(IsNull(DTP_Fecha_Cierre.Value), "", DTP_Fecha_Cierre.Value)  '==== Agregado por MMA el 21/07/08
      .Campo("FLG_MOV_DESCUBIERTOS").Valor = lFlg_Mov_Descubiertos
      
      .Campo("PORCEN_RF").Valor = lPorcen_rf
      .Campo("PORCEN_RV").Valor = lPorcen_rv
      
      '------------<RNMM
      .Campo("Fecha_Contrato").Valor = IIf(IsNull(DTP_Fecha_Contrato.Value), "", DTP_Fecha_Contrato.Value)
      .Campo("Id_TipoCuenta").Valor = lId_TipoCuenta
      .Campo("Numero_Folio").Valor = lNum_Folio
      '------------RNMM>
      '------------APV
      .Campo("tipo_ahorro").Valor = sTipoAhorro
      '-------------
      .Campo("Flg_Considera_Com_VC").Valor = lFlg_Comision_VC
      .Campo("flg_comision_afecta_impuesto").Valor = lFlg_Com_Ase_Hon_AI
      
      
      If Not .Guardar Then
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en grabar la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
        lResult = False
        GoTo ErrProcedure
      End If
    
      fKey = .Campo("id_Cuenta").Valor
       
    End With
        
    lResult = Fnt_GrabaItemPerfilRiesgo(fKey)
    If Not lResult Then
        GoTo ErrProcedure
    End If
    
    Set lcCuenta = Nothing
  '----------------------------------------------------------------------------------------------
  
  
    Rem Relacion Cuenta/Tipo Cambio
    Set lcRel_Cuenta_Tipo_Cambio = New Class_Rel_Cuenta_Tipo_Cambio
    With lcRel_Cuenta_Tipo_Cambio
      Rem Se eliminan registros de REL_CUENTA_TIPO_CAMBIO por FKey
      .Campo("id_Cuenta").Valor = fKey
      If Not .Borrar Then
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas al eliminar la relacion Cuenta/Tipo Cambio.", _
                          .ErrMsg, _
                          pConLog:=True)
        lResult = False
        GoTo ErrProcedure
      End If
      
      Rem Guarda la Relacion Cuenta/Tipo Cambio
      For lFila = 1 To (Grilla_Cambios.Rows - 1)
        If Grilla_Cambios.Cell(flexcpChecked, lFila, 2) = flexChecked Then
          .Campo("id_Tipo_Cambio").Valor = GetCell(Grilla_Cambios, lFila, "id_tipo_cambio")
          If Not .Guardar Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en guardar la relaci�n Cuenta/Tipo Cambio.", _
                              .ErrMsg, _
                              pConLog:=True)
            lResult = False
            GoTo ErrProcedure
          End If
        End If
      Next lFila
    End With
    Set lcRel_Cuenta_Tipo_Cambio = Nothing
  '----------------------------------------------------------------------------------------------
  
    Rem Relacion Cuenta e Instrumento/Publicador
    Set lc_Rel_Cuenta_Instrum_Public = New Class_Rel_Cuenta_Instr_Publicador
    With lc_Rel_Cuenta_Instrum_Public
      Rem Se elimina la relacion entre la Cuenta e Instrumento/Publicadores
      .Campo("id_cuenta").Valor = fKey
      If Not .Borrar Then
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en eliminar la relaci�n Cuenta Instrumento/Publicador.", _
                          .ErrMsg, _
                          pConLog:=True)
        lResult = False
        GoTo ErrProcedure
      End If
    
      Rem Se guarda la relacion entre Cuenta e Instrumento/Publicadores
      For lFila = 1 To (Grilla_Publicadores.Rows - 1)
        If Not GetCell(Grilla_Publicadores, lFila, "Dsc_Publicador") = "" Then
          .Campo("Id_Publicador").Valor = GetCell(Grilla_Publicadores, lFila, "Dsc_Publicador")
          .Campo("Cod_Instrumento").Valor = GetCell(Grilla_Publicadores, lFila, "colum_pk")
          If Not .Guardar Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en grabar la relaci�n Cuenta Instrumento/Publicador.", _
                              .ErrMsg, _
                              pConLog:=True)
            lResult = False
            GoTo ErrProcedure
          End If
        End If
      Next
      
    End With
    Set lc_Rel_Cuenta_Instrum_Public = Nothing
  
  '-----------------------------------------------------------
  '-- Alias
  '-----------------------------------------------------------
    fFormAlias.CodigoEntidadCSBPI = fKey
    If Not fFormAlias.Grabar Then
      Call Fnt_MsgError(fFormAlias.SubTipo_LOG, _
                        "Problemas en grabar Alias de Cuenta", _
                        fFormAlias.ErrMsg, _
                        pConLog:=True)
    End If

ErrProcedure:
  If Err Then
    lResult = False
  End If
  
  If lResult Then
    Call gDB.CommitTransaccion
  Else
    Call gDB.RollbackTransaccion
  End If
  
  Set lcCuenta = Nothing
  Set lcCaja_Cuenta = Nothing
  Set lcRel_Cuenta_Tipo_Cambio = Nothing
  Set lc_Rel_Cuenta_Instrum_Public = Nothing
  
  Fnt_Grabar = lResult
  
End Function

Private Sub Sub_CargaForm()
Dim lReg        As hFields
Dim lComboList  As String
Dim lLinea As Long
'Dim lcMoneda As Class_Monedas
Dim lcMoneda As Object
Dim lcMercado As Class_Mercados
'Dim lcTipo_Cambio As Class_Tipo_Cambios
Dim lcTipo_Cambio As Object
'-----------------------
Dim lPublicador As Class_Publicadores
'APV
'Dim lcRelTipoAhorroCuenta As Class_Rel_TipoAhorro_Cuenta
  
  Call Sub_Bloquea_Puntero(Me)

  Call Sub_FormControl_Color(Me.Controls)

  Set fBorrar = New hRecord
  With fBorrar
     .AddField "colum_pk"
  End With

  'Agregado por MMA
  DTP_Fecha_Contrato.Value = Fnt_FechaServidor
  Rem Carga de los Combos
  Call Sub_CargaCombo_Estados(Cmb_Estado, cTEstado_Cuenta)
  Call Sub_Elije_EstadoDefault(Cmb_Estado) 'HAF 20070703
  
  Cmb_Perfil.Text = ""
  Call Sub_CargaCombo_Clientes(Cmb_Cliente)
  Call Sub_CargaCombo_Tipos_Administracion(Cmb_Tipo_Administracion)
  Call Sub_CargaCombo_Monedas(Cmb_Moneda, pFlg_Es_Moneda_Pago:=fc_EsMoneda_Pago)
  Call Sub_CargaCombo_TiposAhorro_APV(Cmb_TipoAhorroAPV)
  
  Call Sub_CargaCombo_Tipo_Cuentas(Cmb_Tipo_Cuenta)
  '---------------------------------------------------------------------------------------------
  
  DTP_Fecha_Operativa.Value = Fnt_FechaServidor   '==== Agregado por JOP el 18/03/07
  'DTP_Fecha_Cierre.Value = Fnt_FechaServidor      '==== Agregado por MMA el 21/07/08
  DTP_Fecha_Cierre.Value = ""
  
  Rem Carga combo Monedas en la grilla cajas
'  Set lcMoneda = New Class_Monedas
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    .Campo("flg_Es_Moneda_Pago").Valor = fc_EsMoneda_Pago
    If .Buscar Then
      lComboList = ""
      For Each lReg In .Cursor
        lComboList = lComboList & "|#" & lReg("id_moneda").Value
      
        If lReg.Index = 1 Then
           lComboList = lComboList & "*1"
        End If
      
        lComboList = lComboList & ";" & lReg("cod_moneda").Value & vbTab & lReg("dsc_moneda").Value
      Next
    End If
  End With
  Grilla_Cajas.ColComboList(Grilla_Cajas.ColIndex("dsc_moneda")) = lComboList
  Set lcMoneda = Nothing
  '---------------------------------------------------------------------------------------------
  
  Rem Carga combo Mercado en la grilla cajas
  Set lcMercado = New Class_Mercados
  With lcMercado
    If .Buscar Then
      lComboList = ""
      For Each lReg In .Cursor
        lComboList = lComboList & "|#" & lReg("cod_mercado").Value
        lComboList = lComboList & ";" & lReg("desc_mercado").Value
      Next
    End If
  End With
  Grilla_Cajas.ColComboList(Grilla_Cajas.ColIndex("dsc_mercado")) = lComboList
  Set lcMercado = Nothing
  '---------------------------------------------------------------------------------------------
  
  Rem Carga combo Tipo Cambio en la grilla Tipos de Cambios
'  Set lcTipo_Cambio = New Class_Tipo_Cambios
  Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
  With lcTipo_Cambio
    If .Buscar Then
      lComboList = ""
      Grilla_Cambios.Rows = 1
      For Each lReg In .Cursor
        Grilla_Cambios.AddItem ""
        lLinea = Grilla_Cambios.Rows - 1
        
        Call SetCell(Grilla_Cambios, lLinea, "Id_Tipo_Cambio", lReg("ID_TIPO_CAMBIO").Value)
        Grilla_Cambios.Cell(flexcpChecked, lLinea, 2) = flexUnchecked
        Call SetCell(Grilla_Cambios, lLinea, "Id_Moneda_Fuente", lReg("Id_Moneda_Fuente").Value)
        Call SetCell(Grilla_Cambios, lLinea, "dsc_Moneda_Fuente", lReg("dsc_Moneda_Fuente").Value)
        Call SetCell(Grilla_Cambios, lLinea, "Id_Moneda_Destino", lReg("Id_Moneda_Destino").Value)
        Call SetCell(Grilla_Cambios, lLinea, "dsc_Moneda_Destino", lReg("dsc_Moneda_Destino").Value)
        Call SetCell(Grilla_Cambios, lLinea, "dsc_Tipo_Cambio", lReg("dsc_Tipo_Cambio").Value)
        Call SetCell(Grilla_Cambios, lLinea, "dsc_Operacion", lReg("Operacion").Value)
      Next
     End If
  End With
  Grilla_Cambios.ColComboList(Grilla_Cambios.ColIndex("dsc_tipo_cambio")) = lComboList
  Set lcTipo_Cambio = Nothing
  '---------------------------------------------------------------------------------------------
  
  Rem Carga el proximo numero de cuenta
  gDB.Parametros.Clear
  gDB.Procedimiento = "Pkg_Cuentas.Proximo_Numero_Cuenta"
  gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
  gDB.Parametros.Add "pCuenta", ePT_Numero, 0, ePD_Ambos
  If gDB.EjecutaSP Then
    Txt_N_Cuenta.Text = gDB.Parametros("pCuenta").Valor
  End If
  gDB.Parametros.Clear
  '---------------------------------------------------------------------------------------------
  
  Rem Carga combo Publicadores en grilla Publicadores
  Set lPublicador = New Class_Publicadores
  With lPublicador
    If .Buscar Then
      lComboList = ""
      For Each lReg In .Cursor
        lComboList = lComboList & "|#" & lReg("Id_publicador").Value
  
        If lReg.Index = 1 Then
          lComboList = lComboList & "*1"
        End If
  
        lComboList = lComboList & ";" & lReg("Id_publicador").Value & vbTab & lReg("dsc_publicador").Value
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Publicadores.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lPublicador = Nothing
  Grilla_Publicadores.ColComboList(Grilla_Publicadores.ColIndex("dsc_moneda")) = lComboList
  '---------------------------------------------------------------------------------------------
  
  'Call Sub_Cargar_Perfil_X_Defecto
    
  
  '-----------------------------------------------------------
  '-- Alias
  '-----------------------------------------------------------
  If Not fFormAlias.CargarSystem Then
    'no se que se hace aqui -.-U
  End If
  '-----------------------------------------------------------
  
  '---------APV
    Cmb_TipoAhorroAPV.Enabled = False
  '------------------------
  
  '---------PERFIL RIESGO POR PRODUCTO
    Sub_CargaItemPerfil (0)
  '-------------------------
    SSTab.CurrTab = 0
  
  fTipoCuenta = ""
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargarDatos()
'Dim lcCuenta      As Class_Cuentas
Dim lcCuenta      As Object
Dim lcRel_Cuenta_Tipo_Cambio As Class_Rel_Cuenta_Tipo_Cambio
'----------------------------------------------
Dim lReg As hCollection.hFields
Dim lLinea As Long
'----------------------------------------------
Dim lcInstrumento As Class_Instrumentos
Dim lc_Rel_Cuenta_Instrum_Public As Class_Rel_Cuenta_Instr_Publicador
Dim lTipoAhorro As Integer
  
'APV
Dim lcRelTipoAhorroCuenta  As Class_Rel_TipoAhorro_Cuenta

  Load Me
    fId_Perfil_Riesgo = 0
  Rem Carga Datos de la cuenta
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_Cuenta").Valor = fKey
    If .Buscar(True) Then
      For Each lReg In .Cursor
        Call Sub_CargaCombo_Asesor_Cliente(Cmb_Asesor, lReg("id_cliente").Value, pBlanco:=True)
        Call Sub_ComboSelectedItem(Cmb_Cliente, lReg("id_cliente").Value)
        Call Sub_ComboSelectedItem(Cmb_Estado, NVL(lReg("cod_estado").Value, cCod_Estado_Habilitada))
        Txt_N_Cuenta.Text = NVL(lReg("num_cuenta").Value, "")
        Txt_Abr_Cuenta.Text = NVL(lReg("Abr_Cuenta").Value, "")
        Txt_Dsc_Cuenta.Text = NVL(lReg("dsc_cuenta").Value, "")
      
        Chk_Bloqueada.Value = IIf(NVL(lReg("Flg_Bloqueado").Value, "") = gcFlg_SI, vbChecked, vbUnchecked)
        Txt_Obs_Bloqueada.Text = NVL(lReg("Obs_Bloqueo").Value, "")
      
        Txt_Observaci�n.Text = NVL(lReg("Observacion").Value, "")
      
        Chk_Imp_Instrucciones.Value = IIf(NVL(lReg("Flg_Imp_Instrucciones").Value, "") = gcFlg_SI, vbChecked, vbUnchecked)
        
        txt_Porcen_rf.Text = NVL(lReg("PORCEN_RF").Value, 0)
        txt_Porcen_RV.Text = NVL(lReg("PORCEN_RV").Value, 0)
      
        Call Sub_ComboSelectedItem(Cmb_Asesor, NVL(lReg("id_asesor").Value, ""))
        Call Sub_ComboSelectedItem(Cmb_Tipo_Administracion, NVL(lReg("cod_Tipo_Administracion").Value, ""))
        'Call Sub_ComboSelectedItem(Cmb_Perfil, NVL(lReg("id_perfil_riesgo").Value, ""))
        Call Sub_ComboSelectedItem(Cmb_Moneda, NVL(lReg("id_moneda").Value, ""))
        
        DTP_Fecha_Operativa.Value = NVL(lReg("fecha_operativa").Value, "")
        DTP_Fecha_Cierre.Value = NVL(lReg("fecha_cierre_cuenta").Value, "")    '==== Agregado por MMA el 21/07/08
        Chk_Mov_Descubierto.Value = IIf(NVL(lReg("FLG_MOV_DESCUBIERTOS").Value, "") = gcFlg_SI, vbChecked, vbUnchecked)
        '------------<RNMM
        DTP_Fecha_Contrato.Value = NVL(lReg("FECHA_CONTRATO").Value, "")
        Call Sub_ComboSelectedItem(Cmb_Tipo_Cuenta, NVL(lReg("ID_TIPOCUENTA").Value, ""))
        If Cmb_Tipo_Cuenta.Text = cTIPOCUENTA_APV Then
            Cmb_TipoAhorroAPV.Enabled = True
            fTipoCuenta = "APV"
        Else
            Cmb_TipoAhorroAPV.Enabled = False
        End If
        Txt_Num_Folio.Text = NVL(lReg("NUMERO_FOLIO").Value, "")
        '------------RNMM>
        
        '-------PERFIL RIESGO
        fId_Perfil_Riesgo = NVL(lReg("id_perfil_riesgo").Value, 0)
        Call Sub_CargaItemPerfil(fId_Perfil_Riesgo)
        '---------
        '-------APV
        lTipoAhorro = CInt(NVL(lReg("tipo_ahorro").Value, 0))
        Call Sub_ComboSelectedItem(Cmb_TipoAhorroAPV, lTipoAhorro)
        '---------
        Chk_Incorpora_Comi_Deven_VC.Value = IIf(NVL(lReg("Flg_Considera_Com_VC").Value, "") = gcFlg_SI, vbChecked, vbUnchecked)
        MarcaXCarga = True
        Chk_Com_Ase_Hon_AfectaImp.Value = IIf(NVL(lReg("flg_comision_afecta_impuesto").Value, "") = gcFlg_SI, vbChecked, vbUnchecked)
        MarcaXCarga = False
      Next
    Else
       Call Fnt_MsgError(.SubTipo_LOG, _
                         "Problemas en cargar datos de la Cuenta.", _
                         .ErrMsg, _
                         pConLog:=True)
    End If
  End With
  Set lcCuenta = Nothing
  '----------------------------------------------------------------------------------------------
  
  Call Sub_Carga_Cajas_Cuentas
  
  '----------------------------------------------------------------------------------------------
  Rem Carga la relacion entre Cuenta y Tipo de Cambio
  Set lcRel_Cuenta_Tipo_Cambio = New Class_Rel_Cuenta_Tipo_Cambio
  lcRel_Cuenta_Tipo_Cambio.Campo("id_Cuenta").Valor = fKey
  If lcRel_Cuenta_Tipo_Cambio.Buscar Then
    With Grilla_Cambios
      For Each lReg In lcRel_Cuenta_Tipo_Cambio.Cursor
        lLinea = .FindRow(lReg("ID_TIPO_CAMBIO").Value, , .ColIndex("Id_Tipo_Cambio"))
        If lLinea > -1 Then
          .Cell(flexcpChecked, lLinea, .ColIndex("Seleccion")) = flexChecked
        End If
      Next
    End With
  Else
    Call Fnt_MsgError(lcRel_Cuenta_Tipo_Cambio.SubTipo_LOG, _
                      "Problemas en cargar la relaci�n Cuenta Instrumento/Publicador.", _
                      lcRel_Cuenta_Tipo_Cambio.ErrMsg, _
                      pConLog:=True)
  End If
  Set lcRel_Cuenta_Tipo_Cambio = Nothing
  '----------------------------------------------------------------------------------------------
  
  Rem Carga Instrumentos en la grilla Publicadores
  Set lcInstrumento = New Class_Instrumentos
  With lcInstrumento
    Grilla_Publicadores.Rows = 1
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Publicadores.Rows
        Call Grilla_Publicadores.AddItem("")
        Call SetCell(Grilla_Publicadores, lLinea, "colum_pk", NVL(lReg("Cod_Instrumento").Value, ""))
        Call SetCell(Grilla_Publicadores, lLinea, "DSC_INSTRUMENTO", NVL(lReg("DSC_INTRUMENTO").Value, ""))
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar Instrumentos.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcInstrumento = Nothing
  
  Rem Carga la relacion entre la Cuenta e Instrumento/Publicador
  Set lc_Rel_Cuenta_Instrum_Public = New Class_Rel_Cuenta_Instr_Publicador
  With lc_Rel_Cuenta_Instrum_Public
    .Campo("id_cuenta").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        Grilla_Publicadores.Row = Grilla_Publicadores.FindRow(lReg("Cod_Instrumento").Value, , Grilla_Publicadores.ColIndex("colum_pk"))
        If Not Grilla_Publicadores.Row = cNewEntidad Then
          Call SetCell(Grilla_Publicadores, Grilla_Publicadores.Row, "DSC_PUBLICADOR", lReg("Id_publicador").Value)
        End If
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar la relaci�n Cuenta Instrumento/Publicador.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lc_Rel_Cuenta_Instrum_Public = Nothing
  '----------------------------------------------------------------------------------------------
  
  '-----------------------------------------------------------
  '-- Alias
  '-----------------------------------------------------------
  fFormAlias.CodigoEntidadCSBPI = fKey
  If Not fFormAlias.CargarCSBPI Then
    'no se que se hace aqui -.-U
  End If
  '-----------------------------------------------------------

  Call Chk_Bloqueada_Click
  Me.Frame_Cliente.Enabled = (fKey = cNewEntidad)
  Call Cmb_Cliente_ItemChange
  PasoCmbCliente = False
End Sub

Private Function Fnt_ValidarDatos() As Boolean
Dim lResult As Boolean

  lResult = Fnt_Form_Validar(Me.Controls)
  
  If lResult Then
    If fKey = cNewEntidad Then
      If Not Fnt_Cuenta_Unica(Txt_N_Cuenta.Text) Then
        lResult = False
        MsgBox "El n�mero de cuenta debe ser �nico.", vbCritical + vbOKOnly, Me.Caption
        
        Txt_N_Cuenta.SetFocus
      End If
    End If
    If Chk_Bloqueada.Value = vbChecked Then
      If Trim(Txt_Obs_Bloqueada.Text) = "" Then
        lResult = False
        MsgBox "Si la cuenta esta bloqueda debe ingresar alguna observaci�n.", vbExclamation + vbOKOnly, Me.Caption
        Txt_Obs_Bloqueada.SetFocus
      End If
    End If
  End If
  
  If fTipoCuenta = "APV" Then
     If Cmb_TipoAhorroAPV.Text = "" Then
        lResult = False
        MsgBox "Debe especificar tipo de ahorro.", vbInformation, Me.Caption
    End If
  End If
  Fnt_ValidarDatos = lResult
End Function

Private Sub Toolbar_Cambios_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

   Select Case Button.Key
      Case "ADD"
         Call Grilla_Cambios.AddItem("I")
      Case "DEL"
         If Grilla_Cambios.Row > 0 Then
            If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
               With Grilla_Cambios
                  If Not GetCell(Grilla_Cambios, .Row, "Colum_PK") = "" Then
                     fBorrar.Add.Fields("Colum_PK").Value = GetCell(Grilla_Cambios, .Row, "colum_pk")
                  End If

                  Call .RemoveItem(.Row)
               End With
            End If
         End If
   End Select
End Sub

Private Sub Toolbar_Cajas_Cuenta_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim lcCaja_Cuenta As Class_Cajas_Cuenta

  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agregar_Caja_Cuenta
    Case "UPDATE"
      Call Grilla_Cajas_DblClick
    Case "DEL"
      If Grilla_Cajas.Row > 0 Then
        If MsgBox("La eliminaci�n de la caja es inmediata." & vbCr & vbCr & "�Seguro que desea eliminar?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
          Rem Se borran las cuentas eliminadas por el usuario
          If Not GetCell(Grilla_Cajas, Grilla_Cajas.Row, "Colum_PK") = "" Then
            Set lcCaja_Cuenta = New Class_Cajas_Cuenta
            With lcCaja_Cuenta
              .Campo("id_caja_cuenta").Valor = GetCell(Grilla_Cajas, Grilla_Cajas.Row, "Colum_PK")
              If Not .Borrar Then
                Call Fnt_MsgError(.SubTipo_LOG, _
                                "Problemas al eliminar la Caja de la Cuenta.", _
                                .ErrMsg, _
                                pConLog:=True)
                GoTo ExitProcedure
              End If
            End With
            Set lcCaja_Cuenta = Nothing
            
            Call Sub_Carga_Cajas_Cuentas
          End If
        End If
      End If
  End Select
  
ExitProcedure:
  Set lcCaja_Cuenta = Nothing
End Sub

Private Sub Sub_EsperaVentana_Cajas(ByVal pkey)
Dim lForm As Frm_Cajas_Cuenta
Dim lNom_Form As String
  
  Me.Enabled = False
  
  lNom_Form = "Frm_Cajas_Cuenta"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    
    Set lForm = New Frm_Cajas_Cuenta
    Call lForm.Fnt_Modificar(pId_Caja_Cuenta:=pkey, pId_Cuenta:=fKey, pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    Call Sub_Carga_Cajas_Cuentas
  End If
  
  Me.Enabled = True
End Sub


Private Function Fnt_Valida(pLineas As Long) As Boolean
Dim lResult As Boolean
  
  Fnt_Valida = True
  
  If Trim(GetCell(Grilla_Cajas, pLineas, "dsc_moneda")) = "" Then
    Fnt_Valida = False
    MsgBox "Debe seleccionar una Moneda para la Caja.", vbExclamation, Me.Caption
  ElseIf Trim(GetCell(Grilla_Cajas, pLineas, "dsc_mercado")) = "" Then
    Fnt_Valida = False
    MsgBox "Debe seleccionar el Mercado para la Caja.", vbExclamation, Me.Caption
  ElseIf Trim(GetCell(Grilla_Cajas, pLineas, "dsc_caja_cuenta")) = "" Then
    Fnt_Valida = False
    MsgBox "Debe ingresar la Descripci�n de la Caja.", vbExclamation, Me.Caption
  ElseIf Len(Trim(GetCell(Grilla_Cajas, pLineas, "dsc_caja_cuenta"))) > 50 Then
    Fnt_Valida = False
    MsgBox "La Descripci�n de la Caja debe tener m�ximo 50 caracteres.", vbExclamation, Me.Caption
  End If
  
End Function

Private Sub Sub_Carga_Cajas_Cuentas()
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
  '----------------------------------------------------------------------------------------------
  Rem Carga datos de Cajas Cuenta
  Grilla_Cajas.Rows = 1
  Set lcCaja_Cuenta = New Class_Cajas_Cuenta
  With lcCaja_Cuenta
    .Campo("id_Cuenta").Valor = fKey
    If Not .Buscar(pEnVista:=True) Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar las Cajas de la Cuenta.", _
                        .ErrMsg, _
                        pConLog:=True)
      
      GoTo ExitProcedure
    End If
    Call Sub_hRecord2Grilla(pCursor:=.Cursor, pGrilla:=Grilla_Cajas, pColum_PK:="id_caja_cuenta", pAutoSize:=True)
  End With
  Set lcCaja_Cuenta = Nothing
  '----------------------------------------------------------------------------------------------

ExitProcedure:
  Set lcCaja_Cuenta = Nothing
End Sub

Private Sub Sub_Agregar_Caja_Cuenta()

  If fKey = cNewEntidad Then
    If Not MsgBox("Para agregar una caja a la cuenta se necesita que primero guarde la informaci�n ya ingresada." & vbCr & vbCr & _
              "�Desea grabar ahora?", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
      GoTo ExitProcedure
    End If
    
    If Not Fnt_Grabar Then
      GoTo ExitProcedure
    End If
  End If

  Call Sub_EsperaVentana_Cajas(cNewEntidad)

ExitProcedure:
  
End Sub


Private Sub Sub_Cargar_Perfil_X_Defecto()
'Dim lcPerfil_Riesgo As Class_Perfiles_Riesgo
'
'  Set lcPerfil_Riesgo = New Class_Perfiles_Riesgo
'  With lcPerfil_Riesgo
'    .Campo("FLG_PERFIL_DEFAULT").Valor = gcFlg_SI
'    .Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
'    If Not .Buscar Then
'      Call Fnt_MsgError(.SubTipo_LOG, _
'                        "Problemas en recuperar.", _
'                        .ErrMsg, _
'                        pConLog:=True)
'      GoTo ExitProcedure
'    End If
'
'    If .Cursor.Count > 0 Then
'      Call Sub_ComboSelectedItem(Cmb_Perfil, .Cursor(1)("ID_PERFIL_RIESGO").Value)
'    End If
'  End With
'
'ExitProcedure:

End Sub


Private Sub Sub_CargaItemPerfil(pId_PerfilRiesgo)
Dim lcPerfil_Riesgo As Class_Perfiles_Riesgo
Dim lcProducto As Class_Productos
Dim lcRestriccion As Class_RestriccPorcArbolClase
Dim lReg As hFields
Dim lFila As Long
Dim lcod_producto As String
Dim lCod_Instrumento As String
Dim lcArbol_Clase_Instrumento As Class_Arbol_Clase_Instrumento
Dim lID_PADRE_ARBOL_CLASE_INST As Integer
Dim lId_Arbol_Clase_Inst As Integer
Dim lIDPerfilDef As Long


    Rem inicializa la Grilla_Perfil
    With Grilla_Perfil
      Rem Disposici�n
      .Rows = 1
      .FixedCols = 0
      .ExtendLastCol = True
      '.ColAlignment(-1) = flexAlignLeftTop
      
      .ColAlignment(0) = flexAlignLeftTop
      .ColAlignment(1) = flexAlignRightTop
      .ColWidth(0) = 6000
      .ColWidth(1) = 1000
      
      .Editable = flexEDKbdMouse
    
      Rem Contorno
      .OutlineCol = 0
      .OutlineBar = flexOutlineBarSimpleLeaf
      '.MergeCells = flexMergeOutline
    
      Rem Otras
      .AllowUserResizing = flexResizeColumns
      .AllowSelection = False
      .GridLines = flexGridFlatVert
      
      .Redraw = flexRDNone
    End With
    
    Set lcArbol_Clase_Instrumento = New Class_Arbol_Clase_Instrumento
    With lcArbol_Clase_Instrumento
        .Campo("id_empresa").Valor = Fnt_EmpresaActual
        If .Buscar Then
        For Each lReg In .Cursor
            If IsNull(lReg("id_padre_arbol_clase_inst").Value) Then
                lFila = Grilla_Perfil.Rows
                Call Grilla_Perfil.AddItem("")
                Call SetCell(Grilla_Perfil, lFila, "dsc_arbol_clase_inst", lReg("dsc_arbol_clase_inst").Value)
                Call SetCell(Grilla_Perfil, lFila, "id_arbol_clase_inst", lReg("id_arbol_clase_inst").Value)
                Call SetCell(Grilla_Perfil, lFila, "id_padre_arbol_clase_inst", NVL(lReg("id_padre_arbol_clase_inst").Value, 0))
                Call SetCell(Grilla_Perfil, lFila, "id_restriccion", cNewEntidad)
                Call SetCell(Grilla_Perfil, lFila, "porcentaje", "")
                Grilla_Perfil.IsSubtotal(lFila) = True
                Grilla_Perfil.RowOutlineLevel(lFila) = 0
                If Not Fnt_BuscaHijos(lReg("id_arbol_clase_inst").Value) Then
                  Exit For
                End If
            End If
        Next
      Else
        MsgBox .ErrMsg, vbCritical, Me.Caption
      End If
    End With
    Set lcArbol_Clase_Instrumento = Nothing
      
    Grilla_Perfil.Redraw = flexRDBuffered

    If pId_PerfilRiesgo > 0 Then
        
        Call Sub_ComboSelectedItem(Cmb_Perfil, pId_PerfilRiesgo)
'        Call Cmb_Perfil_ItemChange
    
        Set lcPerfil_Riesgo = New Class_Perfiles_Riesgo
        With lcPerfil_Riesgo
          .Campo("id_perfil_riesgo").Valor = pId_PerfilRiesgo
          If .Buscar Then
            For Each lReg In .Cursor
              Txt_NombrePerfil.Text = NVL(lReg("DSC_PERFIL_RIESGO").Value, "")
              Txt_AbrevPerfil.Text = NVL(lReg("ABR_PERFIL_RIESGO").Value, "")
              Txt_SaltoCtaPerfil.Text = NVL(lReg("PORC_SALTO_CUOTA").Value, "")
            Next
          Else
            MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
          End If
        End With
        Set lcPerfil_Riesgo = Nothing
        
        For lFila = 1 To Grilla_Perfil.Rows - 1
            lID_PADRE_ARBOL_CLASE_INST = GetCell(Grilla_Perfil, lFila, "id_padre_arbol_clase_inst")
            lId_Arbol_Clase_Inst = GetCell(Grilla_Perfil, lFila, "id_arbol_clase_inst")
 '           If Not lID_PADRE_ARBOL_CLASE_INST = 0 Then
                Set lcRestriccion = New Class_RestriccPorcArbolClase
                With lcRestriccion
                  .Campo("id_arbol_clase_inst").Valor = lId_Arbol_Clase_Inst
                  '.Campo("id_perfil_riesgo").Valor = pId_PerfilRiesgo
                  .Campo("id_cuenta").Valor = fKey
                  If .Buscar Then
                    If .Cursor.Count > 0 Then
                      Call SetCell(Grilla_Perfil, lFila, "id_restriccion", .Cursor(1).Fields("id_restriccion").Value)
                      Call SetCell(Grilla_Perfil, lFila, "porcentaje", To_Number(.Cursor(1).Fields("porcentaje").Value * 100))
                    End If
                  Else
                    Call Fnt_MsgError(.SubTipo_LOG, _
                                      "No se pudo completar la carga de Restricciones de Porcentaje por Arbol Clase Instrumento.", _
                                      .ErrMsg, _
                                      pConLog:=True)
                    Exit For
                  End If
                End With
                Set lcRestriccion = Nothing
'            End If
        Next
    
    Else
        
        Call Sub_LimpiarTDBCombo(Cmb_Perfil)

        Cmb_Perfil.Columns.Add(eNum_Perfil.Nombre).Visible = True
        Cmb_Perfil.Columns.Add(eNum_Perfil.NomCorto).Visible = False
        Cmb_Perfil.Columns.Add(eNum_Perfil.SaltoCuota).Visible = False
        Cmb_Perfil.Columns.Add(eNum_Perfil.IdPerfil).Visible = False
        
        
        Set lcPerfil_Riesgo = New Class_Perfiles_Riesgo
        With lcPerfil_Riesgo
          .Campo("id_empresa").Valor = Fnt_EmpresaActual
          If .Buscar Then
            For Each lReg In .Cursor
            
                Call Cmb_Perfil.AddItem(lReg("DSC_PERFIL_RIESGO").Value & ";" & lReg("ABR_PERFIL_RIESGO").Value & ";" & lReg("PORC_SALTO_CUOTA").Value)
                Cmb_Perfil.Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("ID_PERFIL_RIESGO").Value, lReg("DSC_PERFIL_RIESGO").Value)
                
                If lReg("FLG_PERFIL_DEFAULT").Value = "S" Then
                    lIDPerfilDef = lReg("ID_PERFIL_RIESGO").Value
                End If
                
            Next
            
            Call Sub_ComboSelectedItem(Cmb_Perfil, lIDPerfilDef)
            Call Cmb_Perfil_ItemChange
            
          Else
            MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
          End If
        End With
        Set lcPerfil_Riesgo = Nothing
    
    End If
End Sub


Private Function Fnt_BuscaHijos(Id_Padre As String) As Boolean
Dim lReg As hFields
Dim lcArbol_Clase_Instrumento As Class_Arbol_Clase_Instrumento
Dim lFila  As Long
  
    Fnt_BuscaHijos = True
    
    Set lcArbol_Clase_Instrumento = New Class_Arbol_Clase_Instrumento
    With lcArbol_Clase_Instrumento
        '-------------- BUSCA LOS HIJOS DE UN NODO ---------------
        .Campo("id_padre_arbol_clase_inst").Valor = Id_Padre
        If .Buscar Then
            For Each lReg In .Cursor
        
                lFila = Grilla_Perfil.Rows
                Call Grilla_Perfil.AddItem("")
                Call SetCell(Grilla_Perfil, lFila, "dsc_arbol_clase_inst", lReg("dsc_arbol_clase_inst").Value)
                Call SetCell(Grilla_Perfil, lFila, "id_arbol_clase_inst", lReg("id_arbol_clase_inst").Value)
                Call SetCell(Grilla_Perfil, lFila, "id_padre_arbol_clase_inst", lReg("id_padre_arbol_clase_inst").Value)
                Call SetCell(Grilla_Perfil, lFila, "id_restriccion", cNewEntidad)
                Call SetCell(Grilla_Perfil, lFila, "porcentaje", "")
                
                Grilla_Perfil.IsSubtotal(lFila) = True
                Grilla_Perfil.RowOutlineLevel(lFila) = 1
            Next
        Else
          Fnt_BuscaHijos = False
          MsgBox .ErrMsg, vbCritical, Me.Caption
        End If
    End With
    Set lcArbol_Clase_Instrumento = Nothing
  
End Function


Private Function Fnt_GrabaItemPerfilRiesgo(ByRef pId_Cuenta As String) As Boolean
Dim bResult          As Boolean

'    bResult = Fnt_GrabaPerfilRiesgo(pId_PerfilRiesgo)
'    If bResult Then
        bResult = True
        If pId_Cuenta <> -1 Then bResult = Fnt_GrabaPerfilPorArbolClase(pId_Cuenta)
        If bResult And Cmb_Perfil.Text <> "" Then
            fId_Perfil_Riesgo = Fnt_FindValue4Display(Cmb_Perfil, Cmb_Perfil.Text, 0)
        End If
'    End If
    
    Fnt_GrabaItemPerfilRiesgo = bResult
End Function

Private Function Fnt_GrabaPerfilRiesgo(ByRef pId_PerfilRiesgo As Integer) As Boolean
Dim bResult          As Boolean
Dim lcPerfil_Riesgo As Class_Perfiles_Riesgo
    
    bResult = False
       
    If Fnt_PreguntaIsNull(Txt_NombrePerfil) Then
        GoTo ExitFunction
    End If
    
    If Cmb_Perfil.Text = Txt_NombrePerfil.Text Then
        MsgBox "Debe escribir un Nombre Perfil diferente al seleccionado en Perfil.", vbExclamation, Me.Caption
        GoTo ExitFunction
    End If
    
    If Fnt_PreguntaIsNull(Txt_AbrevPerfil) Then
        GoTo ExitFunction
    End If
    
    If To_Number(Txt_SaltoCtaPerfil.Text) < 0 Or To_Number(Txt_SaltoCtaPerfil.Text) > 100 Then
        MsgBox "En Item Perfil Riesgo, Salto Cuota debe ser un valor entre 0 y 100.", vbExclamation, Me.Caption
        GoTo ExitFunction
    End If
   
   
   
    Set lcPerfil_Riesgo = New Class_Perfiles_Riesgo
    With lcPerfil_Riesgo
        .Campo("Id_Perfil_Riesgo").Valor = fId_Perfil_Riesgo
        .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
        .Campo("Dsc_Perfil_Riesgo").Valor = Txt_NombrePerfil.Text
        .Campo("Abr_Perfil_Riesgo").Valor = Txt_AbrevPerfil.Text
        .Campo("Porc_Salto_Cuota").Valor = Txt_SaltoCtaPerfil.Text
        .Campo("Flg_Perfil_Default").Valor = "S"
      
        If Not .Guardar Then
          MsgBox "Problemas en grabar en Perfiles." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
          bResult = False
        Else
            pId_PerfilRiesgo = .Campo("id_Perfil_Riesgo").Valor
            bResult = True
        End If
    End With
    Set lcPerfil_Riesgo = Nothing

ExitFunction:
    Fnt_GrabaPerfilRiesgo = bResult
    
End Function
Private Function Fnt_GrabaPerfilPorArbolClase(pId_Cuenta As String) As Boolean
Dim lRollback            As Boolean
Dim lcRestriccion        As Class_RestriccPorcArbolClase
Dim lId_Arbol_Clase_Inst As Integer
Dim lId_Perfil_Riesgo    As String
Dim lFila                       As Long
Dim bResult          As Boolean

    bResult = True
    For lFila = 1 To Grilla_Perfil.Rows - 1
              
'    If Not GetCell(Grilla_Perfil, lFila, "id_padre_arbol_clase_inst") = 0 Then
        
        Set lcRestriccion = New Class_RestriccPorcArbolClase
        With lcRestriccion
          
        If lFila = 1 Then
            '.Campo("id_restriccion").Valor = GetCell(Grilla_Perfil, lFila, "id_restriccion")
            .Campo("id_cuenta").Valor = pId_Cuenta
            If Not .Borrar Then
              Call Fnt_MsgError(.SubTipo_LOG, _
                              "Problemas en borrar Restricciones de Porcentaje por Arbol Clase Instrumento.", _
                              .ErrMsg, _
                              pConLog:=True)
              bResult = False
              GoTo ErrProcedure
            End If
        End If
        
        If Not GetCell(Grilla_Perfil, lFila, "porcentaje") = "" Then
            .Campo("id_restriccion").Valor = GetCell(Grilla_Perfil, lFila, "id_restriccion")
            .Campo("id_arbol_clase_inst").Valor = GetCell(Grilla_Perfil, lFila, "id_arbol_clase_inst")
            .Campo("id_cuenta").Valor = pId_Cuenta
            .Campo("porcentaje").Valor = To_Number(GetCell(Grilla_Perfil, lFila, "porcentaje") / 100)
            If Not .Guardar Then
              Call Fnt_MsgError(.SubTipo_LOG, _
                                "Problemas en grabar Restricciones de Porcentaje por Arbol Clase Instrumento.", _
                                .ErrMsg, _
                                pConLog:=True)
              bResult = False
              GoTo ErrProcedure
            End If
'    Else
'        .Campo("id_restriccion").Valor = GetCell(Grilla_Perfil, lFila, "id_restriccion")
'        '.Campo("id_cuenta").Valor = fKey
'        If Not .Borrar Then
'              Call Fnt_MsgError(.SubTipo_LOG, _
'                              "Problemas en borrar Restricciones de Porcentaje por Arbol Clase Instrumento.", _
'                              .ErrMsg, _
'                             pConLog:=True)
'              bResult = False
 '             GoTo ErrProcedure
'        End If
    End If
    End With
    Set lcRestriccion = Nothing
          
        
 '     End If
        
    Next
  
ErrProcedure:

  Set lcRestriccion = Nothing
  
  Fnt_GrabaPerfilPorArbolClase = bResult
      
End Function


