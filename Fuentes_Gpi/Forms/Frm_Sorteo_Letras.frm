VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Sorteo_Letras 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sorteo de Letras"
   ClientHeight    =   4290
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8070
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4290
   ScaleWidth      =   8070
   Begin VB.Frame Frame_Cuenta 
      Height          =   1845
      Left            =   60
      TabIndex        =   8
      Top             =   420
      Width           =   7905
      Begin VB.Frame Frame2 
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1125
         Left            =   120
         TabIndex        =   12
         Top             =   630
         Width           =   7695
         Begin hControl2.hTextLabel Txt_Rut 
            Height          =   315
            Left            =   90
            TabIndex        =   13
            Top             =   300
            Width           =   3660
            _ExtentX        =   6456
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   1200
            Caption         =   "RUT"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Nombres 
            Height          =   315
            Left            =   90
            TabIndex        =   14
            Top             =   660
            Width           =   7485
            _ExtentX        =   13203
            _ExtentY        =   556
            LabelWidth      =   1300
            Caption         =   "Nombres"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
      End
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4020
         Picture         =   "Frm_Sorteo_Letras.frx":0000
         TabIndex        =   9
         Top             =   240
         Visible         =   0   'False
         Width           =   375
      End
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   345
         Left            =   1215
         TabIndex        =   10
         Tag             =   "OBLI=S;CAPTION=Cuentas"
         Top             =   240
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Sorteo_Letras.frx":030A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   1065
      End
   End
   Begin VB.Frame Frame_Datos_Sorteo 
      Height          =   1845
      Left            =   60
      TabIndex        =   2
      Top             =   2370
      Width           =   7905
      Begin hControl2.hTextLabel Txt_Saldo_Activo 
         Height          =   315
         Left            =   3990
         TabIndex        =   3
         Top             =   660
         Width           =   3780
         _ExtentX        =   6668
         _ExtentY        =   556
         LabelWidth      =   1400
         Caption         =   "Saldo Activo"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Nemotecnico 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Top             =   210
         Width           =   3750
         _ExtentX        =   6615
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Nemotécnico"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Monto 
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Tag             =   "OBLI"
         Top             =   1380
         Width           =   3765
         _ExtentX        =   6641
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Monto"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Cantidad 
         Height          =   315
         Left            =   120
         TabIndex        =   6
         Tag             =   "OBLI"
         Top             =   660
         Width           =   3765
         _ExtentX        =   6641
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Cantidad"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Tasa 
         Height          =   315
         Left            =   120
         TabIndex        =   7
         Tag             =   "OBLI"
         Top             =   1020
         Width           =   3765
         _ExtentX        =   6641
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Tasa"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin MSComctlLib.Toolbar Toolbar_Valorizar 
         Height          =   330
         Left            =   3990
         TabIndex        =   15
         Top             =   1050
         Width           =   1110
         _ExtentX        =   1958
         _ExtentY        =   582
         ButtonWidth     =   1799
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Valorizar"
               Key             =   "VALORIZA"
               Description     =   "Valoriza el nemotécnico a la tasa de inversión."
               Object.ToolTipText     =   "Valoriza el nemotécnico a tasa señalada"
            EndProperty
         EndProperty
      End
      Begin hControl2.hTextLabel Txt_Fecha_Movimiento 
         Height          =   315
         Left            =   3990
         TabIndex        =   16
         Top             =   210
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   556
         LabelWidth      =   1400
         Caption         =   "Fecha Movimiento"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8070
      _ExtentX        =   14235
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la información de forma Permanente"
            Object.ToolTipText     =   "Graba la información de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la información perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la información perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Sorteo_Letras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fNemotecnico As String
Dim fId_Nemotecnico As String
Dim fhMatriz As hRecord
Dim fSalir As Boolean

Private Enum eNem_Colum
  eNem_Cuenta
  eNem_Id_Cuenta
End Enum

Public fKey As String

Private Sub Cmb_Buscar_Click()
Dim lId_Cuenta
  
  lId_Cuenta = Frm_Busca_Cuentas.Buscar
  
  If Not IsNull(lId_Cuenta) Then
    Call Sub_Carga_Datos_Clientes(lId_Cuenta)
  End If
  
End Sub

Private Sub Cmb_Cuentas_ItemChange()
Dim lId_Cuenta As String
  
  lId_Cuenta = Fnt_FindValue4Display(Cmb_Cuentas, Cmb_Cuentas.Text)
  
  If Not lId_Cuenta = "" Then
    Call Sub_Carga_Datos_Clientes(lId_Cuenta)
  End If
  
End Sub

Private Sub Cmb_Cuentas_KeyPress(KeyAscii As Integer)
Dim lId_Cuenta As String

  If KeyAscii = vbKeyReturn Then
    lId_Cuenta = Fnt_FindValue4Display(Cmb_Cuentas, Cmb_Cuentas.Text)
    If Not lId_Cuenta = "" Then
      Call Sub_Carga_Datos_Clientes(lId_Cuenta)
    End If
  End If
  
End Sub

Private Sub Sub_Carga_Datos_Clientes(pId_Cuenta)
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lcBonos As New Class_Bonos

  Call Sub_ComboSelectedItem(Cmb_Cuentas, pId_Cuenta)
  
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cuenta").Valor = pId_Cuenta
    If .Buscar(True) Then
      Txt_Rut.Text = .Cursor(1)("rut_cliente").Value
      Txt_Nombres.Text = .Cursor(1)("nombre_cliente").Value
      Txt_Saldo_Activo.Text = lcBonos.Saldo_Activo_Cantidad_SFecha(pId_Cuenta, fId_Nemotecnico, Null)
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcBonos = Nothing
  
End Sub

Private Sub Form_Load()

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SAVE").Image = cBoton_Grabar
    .Buttons("EXIT").Image = cBoton_Salir
    .Buttons("REFRESH").Image = cBoton_Original
  End With
  
  With Toolbar_Valorizar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("VALORIZA").Image = "boton_valorizar"
    .Appearance = ccFlat
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(ByVal pId_Cuenta As String, _
                              ByRef phMatriz As hRecord)
  
  fKey = pId_Cuenta
  
  If fKey = cNewEntidad Then
    Me.Caption = "Agregar Sorteo de Letras"
  Else
    Me.Caption = "Modificación Sorteo de Letras"
  End If
  
  Set fhMatriz = phMatriz
  
  Call Sub_CargarDatos
    
  Me.Top = 1
  Me.Left = 1
  Me.Show
  
  Do While Not fSalir
    DoEvents
  Loop
  
  Set phMatriz = fhMatriz
  
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  fSalir = True
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Set fhMatriz = New hRecord
      fhMatriz.ClearFields
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acción perdera las modificaciones." & vbLf & "¿Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargaForm
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lReg As hFields

On Error GoTo ErrProcedure
  
  Fnt_Grabar = True
  
  If Not Fnt_Validaciones Then
    GoTo ErrProcedure
  End If
  
  Set fhMatriz = New hRecord
  With fhMatriz
    .ClearFields
    .AddField "ID_CUENTA"
    .AddField "CUENTA"
    .AddField "CANTIDAD", 0
    .AddField "SALDO", 0
    .AddField "TASA", 0
    .AddField "MONTO", 0
    .AddField "RUT_CLIENTE"
    .AddField "CLIENTE"
    .AddField "ID_NEMOTECNICO"
    .AddField "NEMOTECNICO"
    
    Set lReg = .Add
    
    lReg("ID_CUENTA").Value = Cmb_Cuentas.Columns(eNem_Id_Cuenta).Text
    lReg("CUENTA").Value = Cmb_Cuentas.Text
    lReg("CANTIDAD").Value = Txt_Cantidad.Text
    lReg("SALDO").Value = Txt_Saldo_Activo.Text
    lReg("MONTO").Value = Txt_Monto.Text
    lReg("RUT_CLIENTE").Value = Txt_Rut.Text
    lReg("CLIENTE").Value = Txt_Nombres.Text
    lReg("TASA").Value = Txt_Tasa.Text
    lReg("ID_NEMOTECNICO").Value = fId_Nemotecnico
    lReg("NEMOTECNICO").Value = Txt_Nemotecnico.Text

  End With

  Exit Function
  
ErrProcedure:
  If Err.Number > 0 Then
    MsgBox Err.Description, vbCritical, Me.Caption
  End If
  
  Fnt_Grabar = False
  
End Function

Private Function Fnt_Validaciones() As Boolean

  Fnt_Validaciones = True
  
  If Not Fnt_Form_Validar(Me.Controls, Frame_Cuenta) Then
    GoTo ErrProcedure
  End If
  
  If To_Number(Txt_Saldo_Activo.Text) = 0 Then
    MsgBox "La Cuenta " & Cmb_Cuentas.Text & " no tiene Saldo para realizar el Sorteo de Letras.", vbCritical, Me.Caption
    GoTo ErrProcedure
  End If
  
  If To_Number(Txt_Cantidad.Text) > To_Number(Txt_Saldo_Activo.Text) Then
    MsgBox "La Cantidad no puede ser mayor que el Saldo que tiene en cartera para realizar el Sorteo de Letras.", vbCritical, Me.Caption
    GoTo ErrProcedure
  End If
  
  Exit Function
  
ErrProcedure:
  Fnt_Validaciones = False
  
End Function

Private Sub Sub_CargaForm()
  'Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas)
  
  Txt_Rut.Text = ""
  Txt_Nombres.Text = ""
  Txt_Nemotecnico.Text = ""
  Txt_Cantidad.Text = ""
  Txt_Tasa.Text = ""
  Txt_Monto.Text = ""
  Txt_Saldo_Activo.Text = ""
    
End Sub

Private Sub Sub_CargaCuentas()
Dim lSaldos_Activos As Class_Saldo_Activos
Dim lReg As hFields
Dim lId_Cuenta As String
Dim lCuenta As String
  
  Call Sub_LimpiarTDBCombo(Cmb_Cuentas)
  With Cmb_Cuentas
    With .Columns.Add(eNem_Colum.eNem_Cuenta)
      .Caption = "Cuenta"
      .Visible = True
      
    End With
    With .Columns.Add(eNem_Colum.eNem_Id_Cuenta)
      .Caption = "id_cuenta"
      .Visible = False
    End With
    
    Set lSaldos_Activos = New Class_Saldo_Activos
    lSaldos_Activos.Campo("id_nemotecnico").Valor = fId_Nemotecnico
    If lSaldos_Activos.Buscar_Cuentas_X_Instrumento(pCod_Instrumento:=gcINST_BONOS_NAC, pId_Empresa:=Fnt_EmpresaActual) Then
      For Each lReg In lSaldos_Activos.Cursor
        lId_Cuenta = "" & lReg("id_cuenta").Value
        lCuenta = "" & lReg("num_cuenta").Value & " - " & lReg("abr_cuenta").Value
        Call .AddItem(lCuenta & ";" & lId_Cuenta)
        
        .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lId_Cuenta, lCuenta)
      Next
    Else
      Call Fnt_MsgError(lSaldos_Activos.SubTipo_LOG, _
                        "Problemas en carga de Cuentas.", _
                        lSaldos_Activos.ErrMsg, _
                        pConLog:=True)
    End If
    Set lSaldos_Activos = Nothing
    
  End With
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hFields
  
  For Each lReg In fhMatriz
    Txt_Rut.Text = lReg("rut_cliente").Value
    Txt_Nombres.Text = lReg("cliente").Value
    Txt_Nemotecnico.Text = lReg("nemotecnico").Value
    Txt_Cantidad.Text = lReg("cantidad").Value
    Txt_Saldo_Activo.Text = lReg("saldo").Value
    Txt_Tasa.Text = lReg("tasa").Value
    Txt_Monto.Text = lReg("monto").Value
    fId_Nemotecnico = lReg("id_nemotecnico").Value
    Txt_Fecha_Movimiento.Text = lReg("fecha_movimiento").Value
  Next
  
  Call Sub_CargaCuentas
  
  Call Sub_ComboSelectedItem(Cmb_Cuentas, fKey)
    
End Sub

Private Sub Toolbar_Valorizar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "VALORIZA"
      Call Sub_ValorizaPapel
  End Select
End Sub

Private Sub Sub_ValorizaPapel()
Dim lcBonos As Class_Bonos
Dim lValorizacion As Double
Dim lCantidad As Double
Dim lTasa As Double
Dim lFecha_Movimiento As Date

  If To_Number(Txt_Cantidad.Text) = 0 Then
    MsgBox "Debe ingresar Cantidad para poder valorizar.", vbCritical, Me.Caption
    Exit Sub
  End If

  lCantidad = Txt_Cantidad.Text
  lTasa = Txt_Tasa.Text
  lFecha_Movimiento = CDate(Txt_Fecha_Movimiento.Text)
  
  Set lcBonos = New Class_Bonos
  lValorizacion = lcBonos.ValorizaPapel(fId_Nemotecnico, lFecha_Movimiento, lTasa, lCantidad)

  Txt_Monto.Text = lValorizacion
  
End Sub
