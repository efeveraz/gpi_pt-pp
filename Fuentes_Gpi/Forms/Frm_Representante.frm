VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Representante 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Representante"
   ClientHeight    =   3660
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6375
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3660
   ScaleWidth      =   6375
   Begin VB.Frame Frame1 
      Caption         =   "Datos Representante"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3135
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   6225
      Begin MSComCtl2.DTPicker DT_FNac 
         Height          =   315
         Left            =   1560
         TabIndex        =   10
         Top             =   2640
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         _Version        =   393216
         Format          =   77791233
         CurrentDate     =   38818
      End
      Begin hControl2.hTextLabel Txt_Cargo 
         Height          =   315
         Left            =   255
         TabIndex        =   9
         Tag             =   "OBLI"
         Top             =   2250
         Width           =   5685
         _ExtentX        =   10028
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Cargo"
         Text            =   ""
         BackColorTxt    =   16777215
         BackColorTxt    =   16777215
         MaxLength       =   49
      End
      Begin hControl2.hTextLabel Txt_Obs 
         Height          =   315
         Left            =   255
         TabIndex        =   8
         Tag             =   "OBLI"
         Top             =   1860
         Width           =   5685
         _ExtentX        =   10028
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Observaciones"
         Text            =   ""
         BackColorTxt    =   16777215
         BackColorTxt    =   16777215
         MaxLength       =   149
      End
      Begin hControl2.hTextLabel Txt_Mail 
         Height          =   315
         Left            =   255
         TabIndex        =   7
         Tag             =   "OBLI"
         Top             =   1470
         Width           =   5685
         _ExtentX        =   10028
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "E-Mail"
         Text            =   ""
         BackColorTxt    =   16777215
         BackColorTxt    =   16777215
         MaxLength       =   49
      End
      Begin hControl2.hTextLabel Txt_Cel 
         Height          =   315
         Left            =   3150
         TabIndex        =   6
         Tag             =   "OBLI"
         Top             =   1080
         Width           =   2790
         _ExtentX        =   4921
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Tel�fono Movil"
         Text            =   ""
         BackColorTxt    =   16777215
         BackColorTxt    =   16777215
         MaxLength       =   49
      End
      Begin hControl2.hTextLabel Txt_Fono 
         Height          =   315
         Left            =   255
         TabIndex        =   5
         Tag             =   "OBLI"
         Top             =   1080
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Tel�fono"
         Text            =   ""
         BackColorTxt    =   16777215
         BackColorTxt    =   16777215
         MaxLength       =   49
      End
      Begin hControl2.hTextLabel Txt_Nombre 
         Height          =   315
         Left            =   255
         TabIndex        =   4
         Tag             =   "OBLI"
         Top             =   690
         Width           =   5685
         _ExtentX        =   10028
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Nombre"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   149
      End
      Begin hControl2.hTextLabel Txt_Rut 
         Height          =   315
         Left            =   255
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   330
         Width           =   2790
         _ExtentX        =   4921
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Rut"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   10
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "F.Nacimiento"
         Height          =   315
         Left            =   255
         TabIndex        =   11
         Top             =   2640
         Width           =   1275
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Representante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Public fId_Cliente As String

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm

End Sub

Public Function Fnt_Modificar(ByVal pId_Cliente As String, ByVal pId_Representante As String)
  fKey = pId_Representante
  fId_Cliente = pId_Cliente
    
  Call Sub_CargarDatos
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de Representante"
  Else
    Me.Caption = "Modifica Representante: " & Txt_Nombre.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcRepresentante     As Object
'------------------------------------------------------
Dim lId_Estado As String
Dim lSexo As String
Dim LEstadoCivil As String
Dim lvar_tmp As String

  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If

  Set lcRepresentante = Fnt_CreateObject(cDLL_Representantes)
  With lcRepresentante
    Set .gDB = gDB
    .Campo("id_representante").Valor = fKey
    .Campo("Id_cliente").Valor = fId_Cliente
    .Campo("Rut_Representante").Valor = Txt_Rut.Text
    .Campo("Nombre").Valor = Txt_Nombre.Text
    .Campo("Fono").Valor = Txt_Fono.Text
    .Campo("Celular").Valor = NVL(Txt_Cel.Text, "")
    .Campo("Email").Valor = Txt_Mail.Text
    .Campo("Observaciones").Valor = Txt_Obs.Text
    .Campo("Cargo").Valor = Txt_Cargo.Text
    .Campo("Fecha_nacimiento").Valor = DT_FNac.Value
    
    If Not .Guardar Then
        MsgBox "Problemas en grabar el Representante." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        Fnt_Grabar = False
    Else
        If Not IsNull(.Campo("id_representante").Valor) Then
            fKey = .Campo("id_representante").Valor
        End If
    End If
    End With
  Set lcRepresentante = Nothing
  
End Function

Private Sub Sub_CargaForm()
Dim lReg As hFields

DT_FNac.Value = Fnt_FechaServidor

End Sub

Private Sub Sub_CargarDatos()
'------------------------------------------------------
Dim lcRepresentante     As Object
'------------------------------------------------------
Dim lReg As hCollection.hFields
Dim lLinea As Long
  
  Load Me
  Set lcRepresentante = Fnt_CreateObject(cDLL_Representantes)
  With lcRepresentante
    Set .gDB = gDB
    .Campo("Id_representante").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
      
        Txt_Rut.Text = DLL_COMUN.NVL(lReg("rut_representante").Value, "")
        Txt_Nombre.Text = DLL_COMUN.NVL(lReg("nombre").Value, "")
        Txt_Fono.Text = DLL_COMUN.NVL(lReg("fono").Value, "")
        Txt_Cel.Text = DLL_COMUN.NVL(lReg("celular").Value, "")
        Txt_Mail.Text = DLL_COMUN.NVL(lReg("email").Value, "")
        Txt_Obs.Text = DLL_COMUN.NVL(lReg("observaciones").Value, "")
        Txt_Cargo.Text = DLL_COMUN.NVL(lReg("cargo").Value, "")
        DT_FNac.Value = DLL_COMUN.NVL(lReg("fecha_nacimiento").Value, Date)
      Next
    Else
      MsgBox "Problemas en cargar el Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcRepresentante = Nothing
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = True
  
  If Fnt_PreguntaIsNull(Txt_Rut) Then
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Not Fnt_Valida_Rut(Txt_Rut) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If Fnt_PreguntaIsNull(Txt_Nombre) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  'If Fnt_PreguntaIsNull(txt_fnac) Then
  'If Fnt_PreguntaIsNull(DT_FNac) Then
  '  Fnt_ValidarDatos = False
  '  Exit Function
  'End If
  
    If Txt_Mail.Text <> "" Then
        If Not Validar_Email(Txt_Mail.Text) Then
            MsgBox "La direccion de correo no es valida", vbInformation, Me.Caption
            Txt_Mail.SetFocus
            Fnt_ValidarDatos = False
            Exit Function
        End If
    End If
  
    If DT_FNac.Value = "" Then
        MsgBox "Debe ingresar fecha", vbInformation
        DT_FNac.SetFocus
        Exit Function
    End If
    
  
    ' valida existe representante
    Dim lcRepresentante       As Object
    Dim lReg As hCollection.hFields

    Set lcRepresentante = Fnt_CreateObject(cDLL_Representantes)

    With lcRepresentante
        Set .gDB = gDB
        .Campo("Id_Cliente").Valor = fId_Cliente
        If .Buscar Then
            For Each lReg In .Cursor
                If Trim(UCase(lReg("rut_representante").Value)) = Trim(UCase(Txt_Rut.Text)) Then
                    If lReg("id_representante") <> fKey Then
                        MsgBox "Ya existe un representante con el rut :" & Trim(UCase(Txt_Rut.Text)), vbInformation, Me.Caption
                        Set lcRepresentante = Nothing
                          Fnt_ValidarDatos = False
                        Exit Function
                    End If
                End If
            Next
        End If
    End With
    Set lcRepresentante = Nothing
    
  
End Function

Private Sub Txt_Cargo_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Mail_LostFocus()
'    If Txt_Mail.Text <> "" Then
'        If Not Validar_Email(Txt_Mail.Text) Then
'            MsgBox "La direccion de correo no es valida", vbInformation, Me.Caption
'            Txt_Mail.SetFocus
'            Exit Sub
'        End If
'    End If
End Sub

Private Sub Txt_Nombre_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Obs_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub Txt_Rut_KeyPress(KeyAscii As Integer)
  KeyAscii = Fnt_LetraMayuscula(KeyAscii)
End Sub

Private Sub txt_rut_LostFocus()
  Call Fnt_Valida_Rut(Txt_Rut)
End Sub

Public Function Validar_Email(ByVal Email As String) As Boolean
    
    Dim i As Integer, iLen As Integer, caracter As String
    Dim pos As Integer, bp As Boolean, ipos As Integer, ipos2 As Integer

    On Local Error GoTo Err_Sub

    Email = Trim$(Email)

    If Email = vbNullString Then
        Exit Function
    End If

    Email = LCase$(Email)
    iLen = Len(Email)

    
    For i = 1 To iLen
        caracter = Mid(Email, i, 1)

        If (Not (caracter Like "[a-z]")) And (Not (caracter Like "[0-9]")) Then
            
            If InStr(1, "_-" & "." & "@", caracter) > 0 Then
                If bp = True Then
                   Exit Function
                Else
                    bp = True
                   
                    If i = 1 Or i = iLen Then
                        Exit Function
                    End If
                    
                    If caracter = "@" Then
                        If ipos = 0 Then
                            ipos = i
                        Else
                            
                            Exit Function
                        End If
                    End If
                    If caracter = "." Then
                        ipos2 = i
                    End If
                    
                End If
            Else
                
                Exit Function
            End If
        Else
            bp = False
        End If
    Next i
    If ipos = 0 Or ipos2 = 0 Then
        Exit Function
    End If
    
    If ipos2 < ipos Then
        Exit Function
    End If

    
    Validar_Email = True

    Exit Function
Err_Sub:
    On Local Error Resume Next
    
    Validar_Email = False
End Function



