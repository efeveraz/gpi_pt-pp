VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Consulta_Busqueda_Por_Activos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta B�squeda por Activos"
   ClientHeight    =   8865
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14730
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8850.793
   ScaleMode       =   0  'User
   ScaleWidth      =   14730
   Begin VB.Frame Frm_Cuentas 
      Caption         =   "Cuentas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7605
      Left            =   0
      TabIndex        =   9
      Top             =   1230
      Width           =   14715
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
         Height          =   7275
         Left            =   30
         TabIndex        =   10
         Top             =   240
         Width           =   14625
         _cx             =   25797
         _cy             =   12832
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         SelectionMode   =   1
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   11
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Consulta_Busqueda_Por_Activos.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   2
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   0
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin VB.Frame Frm_Fecha_Re_Proceso 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   795
      Left            =   0
      TabIndex        =   2
      Top             =   390
      Width           =   14715
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   10830
         Picture         =   "Frm_Consulta_Busqueda_Por_Activos.frx":01E9
         TabIndex        =   8
         Top             =   330
         Width           =   375
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Consulta 
         Height          =   315
         Left            =   12870
         TabIndex        =   3
         Top             =   360
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   56098817
         CurrentDate     =   37732
      End
      Begin TrueDBList80.TDBCombo Cmb_Instrumento 
         Height          =   345
         Left            =   1200
         TabIndex        =   5
         Tag             =   "OBLI=S;CAPTION=Cuentas"
         Top             =   360
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Consulta_Busqueda_Por_Activos.frx":04F3
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin hControl2.hTextLabel Txt_Nemotecnico 
         Height          =   315
         Left            =   5400
         TabIndex        =   7
         Top             =   360
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   1200
         Caption         =   "Nemot�cnico"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin VB.Label lblInstrumento 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Instrumento"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   1065
      End
      Begin VB.Label Lbl_Fecha_Consulta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Consulta"
         Height          =   345
         Left            =   11610
         TabIndex        =   4
         Top             =   360
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   14730
      _ExtentX        =   25982
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "SEARCH"
            Description     =   "Busca Cuentas"
            Object.ToolTipText     =   "Carga en Grilla las Cuentas del Nemot�cnico"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORT"
            Object.ToolTipText     =   "Genera reporte de Rentabilidad por Perfil"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Consulta_Busqueda_Por_Activos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

'----------------------------------------------------------------------------
Rem----------------------------------------------------------------------------
Rem 12/12/2008. En Excel la columna Cuenta s�lo debe ir el n�mero de la cuenta.
Rem                      agrega la columna asesor en el reporte
Rem----------------------------------------------------------------------------

Public fApp_Excel           As Excel.Application      ' Excel application
Public fLibro               As Excel.Workbook      ' Excel workbook

Dim oActivos As New Class_BusquedaPorActivos
Dim iFilaExcel    As Integer
Dim sFormato As String



Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  
  Call Form_Resize
  
  Load Me
End Sub


Private Sub cmb_buscar_Click()
    oActivos.Cod_Producto = Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text)
    If Not oActivos.Cod_Producto = "" Then
       ' If Txt_Nemotecnico.Text <> "" Then
            oActivos.Id_Nemotecnico = NVL(frm_Busca_Nemotecnicos.Buscar(pCod_Instrumento:=oActivos.Cod_Producto, _
                                                          pNemotecnico:=Txt_Nemotecnico.Text, _
                                                          pCodOperacion:=gcTipoOperacion_Egreso, _
                                                          pFecha_Movimiento:=DTP_Fecha_Consulta.Value, _
                                                          pSaldoTodasLasCuentas:=True), 0)
'        Else
'            oActivos.Id_Nemotecnico = NVL(frm_Busca_Nemotecnicos.Buscar(pCod_Instrumento:=oActivos.Cod_Producto, _
'                                                  pCodOperacion:=gcTipoOperacion_Egreso), 0)
'        End If
    End If
    
    If oActivos.Id_Nemotecnico <> 0 Then
        Call Sub_Llena_Nemotecnico
      
    End If

End Sub

Private Sub Cmb_Instrumento_ItemChange()
    Grilla_Cuentas.Rows = 1
    Txt_Nemotecnico.Text = ""
End Sub

Private Sub Form_Load()
   Me.Top = 1
   Me.Left = 1
   With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SEARCH").Image = cBoton_Buscar
        .Buttons("REPORT").Image = cBoton_Excel
        .Buttons("REFRESH").Image = cBoton_Original
        .Buttons("EXIT").Image = cBoton_Salir
   End With

   Call Sub_CargaForm
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEARCH"
        If ValidaEntradaDatos Then
            Call Sub_CargarDatos
        End If
    Case "REFRESH"
      Call Sub_Limpiar
    Case "REPORT"
      Call Sub_Crea_Excel
    Case "EXIT"
      Unload Me
  End Select

End Sub
Private Function ValidaEntradaDatos() As Boolean
Dim bOk As Boolean

    bOk = True
    If Cmb_Instrumento.Text = "" Then
        MsgBox "Debe seleccionar Instrumento", vbInformation, Me.Caption
        bOk = False
    Else
        If Txt_Nemotecnico.Text = "" Then
         MsgBox "Debe seleccionar Nemot�cnico", vbInformation, Me.Caption
         bOk = False
        End If
    End If
    ValidaEntradaDatos = bOk
End Function
Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_FormControl_Color(Me.Controls)
    Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento)
    Call Sub_Desbloquea_Puntero(Me)
    Grilla_Cuentas.GridLines = flexGridNone
    Grilla_Cuentas.Rows = 1
    
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Consulta.Value = lCierre.Busca_Ultima_FechaCierre
    Set lCierre = Nothing
    
    sFormato = "#,##0.0000"

End Sub
Private Sub Form_Resize()
   Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Sub_Llena_Nemotecnico()
Dim lcNemotecnico As Class_Nemotecnicos
Dim lReg As hFields
Dim lBonos As Class_Bonos
Dim lSaldo_Cantidad As Double
Dim lFila As Long


  Set lcNemotecnico = New Class_Nemotecnicos
  
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = oActivos.Id_Nemotecnico
    If .BuscarView Then
      For Each lReg In .Cursor
        Txt_Nemotecnico.Text = Trim(lReg("nemotecnico").Value)
        oActivos.Dsc_Moneda_Nemo = lReg("dsc_Moneda").Value
        oActivos.Id_Moneda_Nemo = lReg("id_moneda").Value
      Next
    End If
  End With

End Sub
Private Sub Sub_Limpiar()
Dim lCierre As Class_Verificaciones_Cierre

    Call Sub_ComboSelectedItem(Cmb_Instrumento, cCmbKALL)
    Txt_Nemotecnico.Text = ""
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Consulta.Value = lCierre.Busca_Ultima_FechaCierre
    Grilla_Cuentas.Rows = 1
End Sub
Private Sub Sub_CargarDatos()
Dim lCursor         As Object
Dim lReg            As Object
Dim lLinea          As Integer
Dim lcBusquedaPorActivos As Class_BusquedaPorActivos
Dim dTotalValor     As Double
Dim dTotalCantidad  As Double
Dim iDecimales      As Integer

    oActivos.Fecha_Consulta = DTP_Fecha_Consulta.Value
    Set lcBusquedaPorActivos = New Class_BusquedaPorActivos
    Grilla_Cuentas.Rows = 1
        
    Set lcBusquedaPorActivos = New Class_BusquedaPorActivos
    
    With lcBusquedaPorActivos
        .Campo("id_nemotecnico").Valor = oActivos.Id_Nemotecnico
        .Campo("fecha_cierre").Valor = oActivos.Fecha_Consulta
        .Campo("id_empresa").Valor = Fnt_EmpresaActual
        If Not .Buscar_Activos Then
            MsgBox "Problemas en buscar Activos." & vbLf & vbLf & .ErrMsg, vbCritical, Me.Caption
            GoTo ExitProcedure
        End If
    End With
        
    If lcBusquedaPorActivos.Cursor.Count > 0 Then
        oActivos.Cod_Arbol_clase = lcBusquedaPorActivos.Cursor(1)("codigo").Value
        Call EncabezadoGrilla
        dTotalValor = 0
        dTotalCantidad = 0
        For Each lReg In lcBusquedaPorActivos.Cursor
            oActivos.Decimales = lReg("decimales_mostrar").Value
            lLinea = Grilla_Cuentas.Rows
            Call Grilla_Cuentas.AddItem("")
            Call SetCell(Grilla_Cuentas, lLinea, "colum_pk", Trim(lReg("id_cuenta").Value), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "id_cliente", lReg("id_cliente").Value, pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "Rut_cliente", lReg("rut_cliente").Value, pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "nombre_cliente", lReg("nombre_cliente").Value, pAutoSize:=False)
            'Call SetCell(Grilla_Cuentas, lLinea, "dsc_cuenta", lReg("num_cuenta").Value & " - " & lReg("dsc_cuenta").Value, pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "dsc_cuenta", lReg("num_cuenta").Value, pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "nombre_asesor", NVL(lReg("dsc_asesor").Value, ""), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "cod_arbol_clase", lReg("codigo").Value, pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "cantidad", FormatNumber(lReg("cantidad").Value, 4), pAutoSize:=False)
            Select Case oActivos.Cod_Arbol_clase
                Case "RV", "RVINT", "RFINT", "FM_INT"
                    Call SetCell(Grilla_Cuentas, lLinea, "precio_compra", FormatNumber(lReg("precio_compra").Value, 4), pAutoSize:=False)
                    Call SetCell(Grilla_Cuentas, lLinea, "precio_mercado", FormatNumber(lReg("precio").Value, 4), pAutoSize:=False)
                Case "RF"
                    Call SetCell(Grilla_Cuentas, lLinea, "precio_compra", FormatNumber(lReg("tasa_compra").Value, 4), pAutoSize:=False)
                    Call SetCell(Grilla_Cuentas, lLinea, "precio_mercado", FormatNumber(lReg("tasa").Value, 4), pAutoSize:=False)
                Case "FFMM"
                    Call SetCell(Grilla_Cuentas, lLinea, "precio_compra", FormatNumber(lReg("precio_compra").Value, 4), pAutoSize:=False)
                    Call SetCell(Grilla_Cuentas, lLinea, "precio_mercado", FormatNumber(lReg("precio").Value, 4), pAutoSize:=False)
            End Select
            Call SetCell(Grilla_Cuentas, lLinea, "valor_mercado", FormatNumber(lReg("monto_mon_cta").Value, oActivos.Decimales), pAutoSize:=False)
            dTotalCantidad = dTotalCantidad + lReg("cantidad").Value
            dTotalValor = dTotalValor + lReg("monto_mon_cta").Value
          
        Next
        lLinea = Grilla_Cuentas.Rows
        Call Grilla_Cuentas.AddItem("")
        Call SetCell(Grilla_Cuentas, lLinea, "nombre_cliente", "TOTAL", pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "cantidad", FormatNumber(dTotalCantidad, 4), pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "valor_mercado", FormatNumber(dTotalValor, oActivos.Decimales), pAutoSize:=False)
        Grilla_Cuentas.Cell(flexcpFontBold, lLinea, 3) = True
        Grilla_Cuentas.Cell(flexcpFontBold, lLinea, 7) = True
        Grilla_Cuentas.Cell(flexcpFontBold, lLinea, 10) = True
    Else
        MsgBox "No Existen Activos para este Nemot�cnico", vbInformation, Me.Caption
    End If
    
ExitProcedure:
    Set lcBusquedaPorActivos = Nothing
End Sub

Private Sub EncabezadoGrilla()
    Call SetCell(Grilla_Cuentas, 0, "cantidad", "Cantidad", pAutoSize:=False)
    Select Case oActivos.Cod_Arbol_clase
        Case "RV"
            Call SetCell(Grilla_Cuentas, 0, "precio_compra", "Precio Compra", pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, 0, "precio_mercado", "Precio Mercado", pAutoSize:=False)
        Case "RF"
            Call SetCell(Grilla_Cuentas, 0, "precio_compra", "Tasa Compra", pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, 0, "precio_mercado", "Tasa Mercado", pAutoSize:=False)
        Case "FFMM"
            Call SetCell(Grilla_Cuentas, 0, "precio_compra", "Valor Cta. Promedio Compra", pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, 0, "precio_mercado", "Valor Cuota Actual", pAutoSize:=False)
    End Select
    Call SetCell(Grilla_Cuentas, 0, "valor_mercado", "Valor Mercado", pAutoSize:=False)
    
End Sub

Private Sub Sub_Crea_Excel()
    Dim nHojas, i As Integer
    Dim hoja As Integer
    Dim Index As Integer
    
    Set fApp_Excel = CreateObject("Excel.application")
    fApp_Excel.DisplayAlerts = False
    Set fLibro = fApp_Excel.Workbooks.Add
    
    fApp_Excel.ActiveWindow.DisplayGridlines = False
  
    ' App_Excel.Visible = True
    Call Sub_Bloquea_Puntero(Me)
    
    fLibro.Worksheets.Add
   
    With fLibro
        For i = .Worksheets.Count To 2 Step -1
            .Worksheets(i).Delete
        Next i
        .Sheets(1).Select
        .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).Select
            
        .ActiveSheet.Range("A5").Value = "Reporte B�squeda por Activos"
        
        .ActiveSheet.Range("A5:E5").Font.Bold = True
        .ActiveSheet.Range("A5:E5").HorizontalAlignment = xlLeft
        '.ActiveSheet.Range("A5:E5").Merge
        
        .ActiveSheet.Range("A8").Value = "Instrumento      " & Cmb_Instrumento.Text
        .ActiveSheet.Range("A9").Value = "Nemot�cnico      " & Txt_Nemotecnico.Text
        .ActiveSheet.Range("A10").Value = "Moneda Papel     " & oActivos.Dsc_Moneda_Nemo
        .ActiveSheet.Range("A12").Value = "Fecha Consulta   " & oActivos.Fecha_Consulta
        
        .ActiveSheet.Range("A8:B12").HorizontalAlignment = xlLeft
        .ActiveSheet.Range("A8:B12").Font.Bold = True

        .Worksheets.Item(1).Columns("A:A").ColumnWidth = 50
        .Worksheets.Item(1).Columns("B:B").ColumnWidth = 15
        .Worksheets.Item(1).Columns("C:C").ColumnWidth = 10
        .Worksheets.Item(1).Columns("D:D").ColumnWidth = 40
        .Worksheets.Item(1).Columns("E:E").ColumnWidth = 20
        .Worksheets.Item(1).Columns("F:F").ColumnWidth = 20
        .Worksheets.Item(1).Columns("G:G").ColumnWidth = 20
        .Worksheets.Item(1).Columns("H:H").ColumnWidth = 20
        .Worksheets.Item(1).Name = "Reporte B�squeda por Activos"
    End With
    
    iFilaExcel = 13
    
    Generar_Listado_Excel
    
    fApp_Excel.Visible = True
    fApp_Excel.UserControl = True
    
    
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub


Private Sub Generar_Listado_Excel()
Dim lReg                  As hCollection.hFields
Dim lcBusquedaPorActivos  As Class_BusquedaPorActivos

Dim lxHoja          As Worksheet
'----------------------------------------------------
Dim bImprimioTitulo As Boolean
Dim iFilaGrilla     As Integer
Dim iFilaInicio     As Integer
Dim iColGrilla      As Integer
Dim iColExcel       As Integer
Dim lIdCuenta       As String
Dim dTotalCantidad  As Double
Dim dTotalValor     As Double

Dim sValor              As String
Dim nDecimales          As Integer
Dim sFormatoValorMercado As String
Dim dblTotal            As Double
    
    'On Error GoTo ErrProcedure
    
    
    bImprimioTitulo = False
    iColExcel = 1
    dTotalCantidad = 0
    dTotalValor = 0
    
    
    Set lxHoja = fLibro.ActiveSheet
    
    If Not bImprimioTitulo Then
        Call ImprimeEncabezado
        bImprimioTitulo = True
        iFilaInicio = iFilaExcel + 1
    End If
    
    Set lcBusquedaPorActivos = New Class_BusquedaPorActivos
    
    With lcBusquedaPorActivos
        .Campo("id_nemotecnico").Valor = oActivos.Id_Nemotecnico
        .Campo("fecha_cierre").Valor = oActivos.Fecha_Consulta
        .Campo("id_empresa").Valor = Fnt_EmpresaActual
        If Not .Buscar_Activos Then
            MsgBox "Problemas en buscar Activos." & vbLf & vbLf & .ErrMsg, vbCritical, Me.Caption
            GoTo ExitProcedure
        End If
    End With
    
    If lcBusquedaPorActivos.Cursor.Count > 0 Then
        With lxHoja
            For Each lReg In lcBusquedaPorActivos.Cursor
                iFilaExcel = iFilaExcel + 1
                .Cells(iFilaExcel, 1).Value = lReg("nombre_cliente").Value
                .Cells(iFilaExcel, 2).Value = lReg("rut_cliente").Value
                .Cells(iFilaExcel, 3).Value = lReg("num_cuenta").Value
                .Cells(iFilaExcel, 4).Value = NVL(lReg("dsc_asesor").Value, "")
                .Cells(iFilaExcel, 5).Value = lReg("cantidad").Value
                .Cells(iFilaExcel, 6).NumberFormat = sFormato
    
                dTotalCantidad = dTotalCantidad + lReg("cantidad").Value
                oActivos.Decimales = lReg("decimales_mostrar").Value
                Select Case lReg("codigo").Value
                    Case "RV"
                        .Cells(iFilaExcel, 6).Value = lReg("precio_compra").Value
                        .Cells(iFilaExcel, 7).Value = lReg("precio").Value
                    Case "RF"
                        .Cells(iFilaExcel, 6).Value = lReg("tasa_compra").Value
                        .Cells(iFilaExcel, 7).Value = lReg("tasa").Value
                    Case "FFMM"
                        .Cells(iFilaExcel, 6).Value = lReg("precio_compra").Value
                        .Cells(iFilaExcel, 7).Value = lReg("precio").Value
                End Select
                .Cells(iFilaExcel, 6).NumberFormat = sFormato
                .Cells(iFilaExcel, 7).NumberFormat = sFormato
                .Cells(iFilaExcel, 8).Value = lReg("monto_mon_cta").Value
                .Cells(iFilaExcel, 8).NumberFormat = sFormato
                dTotalValor = dTotalValor + lReg("monto_mon_cta").Value
            Next
            iFilaExcel = iFilaExcel + 1
            .Cells(iFilaExcel, 5).Value = dTotalCantidad
            .Cells(iFilaExcel, 8).Value = dTotalValor
            .Cells(iFilaExcel, 5).NumberFormat = sFormato
            .Cells(iFilaExcel, 8).NumberFormat = Fnt_Formato_Moneda(oActivos.Decimales)
            .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, 8)).BorderAround
            .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, 8)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, 8)).Interior.Color = RGB(255, 255, 0) 'verde:&H00C0FFC0&  amarillo:RGB(255, 255, 0)
            .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, 8)).Font.Bold = True
            
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaExcel, 1)).HorizontalAlignment = xlLeft
            .Range(.Cells(iFilaInicio, 2), .Cells(iFilaExcel, 3)).HorizontalAlignment = xlRight
            .Range(.Cells(iFilaInicio, 4), .Cells(iFilaExcel, 4)).HorizontalAlignment = xlLeft
            .Range(.Cells(iFilaInicio, 5), .Cells(iFilaExcel, 8)).HorizontalAlignment = xlRight
            
            

        End With
    End If
ExitProcedure:
    Set lcBusquedaPorActivos = Nothing
End Sub

Private Sub ImprimeEncabezado()
Dim lxHoja          As Worksheet

    Set lxHoja = fLibro.ActiveSheet
    iFilaExcel = iFilaExcel + 1
    With lxHoja
        Select Case oActivos.Cod_Arbol_clase
            Case "RV"
                .Cells(iFilaExcel, 1).Value = "Cliente"
                .Cells(iFilaExcel, 2).Value = "Rut"
                .Cells(iFilaExcel, 3).Value = "Cuenta"
                .Cells(iFilaExcel, 4).Value = "Cantidad"
                .Cells(iFilaExcel, 5).Value = "Asesor"
                .Cells(iFilaExcel, 6).Value = "Precio Compra"
                .Cells(iFilaExcel, 7).Value = "Precio Mercado"
                .Cells(iFilaExcel, 8).Value = "Valor Mercado"
            Case "RF"
                .Cells(iFilaExcel, 1).Value = "Cliente"
                .Cells(iFilaExcel, 2).Value = "Rut"
                .Cells(iFilaExcel, 3).Value = "Cuenta"
                .Cells(iFilaExcel, 4).Value = "Asesor"
                .Cells(iFilaExcel, 5).Value = "Cantidad"
                .Cells(iFilaExcel, 6).Value = "Tasa Compra"
                .Cells(iFilaExcel, 7).Value = "Tasa Mercado"
                .Cells(iFilaExcel, 8).Value = "Valor Mercado"
            Case "FFMM"
                .Cells(iFilaExcel, 1).Value = "Cliente"
                .Cells(iFilaExcel, 2).Value = "Rut"
                .Cells(iFilaExcel, 3).Value = "Cuenta"
                .Cells(iFilaExcel, 4).Value = "Asesor"
                .Cells(iFilaExcel, 5).Value = "Cantidad"
                .Cells(iFilaExcel, 6).Value = "Valor Cta. Promedio Compra"
                .Cells(iFilaExcel, 7).Value = "Valor Cuota Actual"
                .Cells(iFilaExcel, 8).Value = "Valor Mercado"
        End Select
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, 8)).BorderAround
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, 8)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, 8)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, 8)).Font.Bold = True
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, 8)).WrapText = True
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, 8)).HorizontalAlignment = xlCenter
    End With
    
End Sub


