VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_DisponibleCaja 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Disponible Caja"
   ClientHeight    =   8940
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14910
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8940
   ScaleWidth      =   14910
   Begin VB.Frame Frm_Cuentas 
      Caption         =   "Flujos de Caja"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7365
      Left            =   0
      TabIndex        =   8
      Top             =   1560
      Width           =   14895
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
         Height          =   6915
         Left            =   180
         TabIndex        =   9
         Top             =   240
         Width           =   14505
         _cx             =   25585
         _cy             =   12197
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         SelectionMode   =   1
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   15
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"FrmDisponibleCaja.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   0
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin VB.Frame Frm_Fecha_Re_Proceso 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1185
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   14895
      Begin VB.OptionButton Opt_Todos 
         Caption         =   "Todos"
         Height          =   345
         Left            =   8400
         TabIndex        =   13
         Top             =   720
         Value           =   -1  'True
         Width           =   765
      End
      Begin VB.OptionButton Opt_Consaldo 
         Caption         =   "Con Saldo"
         Height          =   345
         Left            =   6870
         TabIndex        =   12
         Top             =   750
         Width           =   1035
      End
      Begin VB.CommandButton cmb_buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   14100
         Picture         =   "FrmDisponibleCaja.frx":0258
         TabIndex        =   1
         Top             =   320
         Width           =   375
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Consulta 
         Height          =   315
         Left            =   1350
         TabIndex        =   2
         Top             =   320
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   58261505
         CurrentDate     =   37732
      End
      Begin TrueDBList80.TDBCombo Cmb_Asesor 
         Height          =   345
         Left            =   3960
         TabIndex        =   3
         Tag             =   "SOLOLECTURA=N"
         Top             =   320
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"FrmDisponibleCaja.frx":0562
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   315
         Left            =   9690
         TabIndex        =   4
         Tag             =   "SOLOLECTURA=N"
         Top             =   315
         Width           =   4425
         _ExtentX        =   7805
         _ExtentY        =   556
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   556
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"FrmDisponibleCaja.frx":060C
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Instrumento 
         Height          =   315
         Left            =   1380
         TabIndex        =   10
         Tag             =   "SOLOLECTURA=N"
         Top             =   750
         Width           =   4995
         _ExtentX        =   8811
         _ExtentY        =   556
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   556
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"FrmDisponibleCaja.frx":06B6
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Instrumento"
         Height          =   315
         Left            =   90
         TabIndex        =   11
         Top             =   750
         Width           =   1245
      End
      Begin VB.Label lblInstrumento 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Asesor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2880
         TabIndex        =   7
         Top             =   320
         Width           =   1065
      End
      Begin VB.Label Lbl_Fecha_Consulta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Consulta"
         Height          =   345
         Left            =   90
         TabIndex        =   6
         Top             =   320
         Width           =   1215
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuentas"
         Height          =   315
         Left            =   8400
         TabIndex        =   5
         Top             =   320
         Width           =   1245
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   14910
      _ExtentX        =   26300
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "SEARCH"
            Description     =   "Busca Cuentas"
            Object.ToolTipText     =   "Carga en Grilla las Cuentas del Nemot�cnico"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORT"
            Object.ToolTipText     =   "Genera reporte de Rentabilidad por Perfil"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   15
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_DisponibleCaja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiqso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

'----------------------------------------------------------------------------

Public fApp_Excel           As Excel.Application      ' Excel application
Public fLibro               As Excel.Workbook      ' Excel workbook


Private Type Detalle_Totales
    Decimales       As Integer
    SimboloMoneda   As String
    TotalT0         As Double
    TotalT1         As Double
    TotalT2         As Double
End Type

Dim iFilaExcel    As Integer
Dim sFormato As String
Dim iTotalColExcel As Integer


Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Btn_Buscar_Click()
Dim lId_Moneda_Cuenta As Integer
Dim lId_Cuenta As Integer

    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)
    
    If lId_Cuenta <> 0 Then
        Call Sub_ComboSelectedItem(Cmb_Cuentas, lId_Cuenta)
'        MuestraDatosCuenta
        
        
    End If

End Sub



Private Sub cmb_buscar_Click()
Dim lId_Moneda_Cuenta As Integer
Dim lId_Cuenta As Integer
    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(), 0)
    If lId_Cuenta <> 0 Then
        Call Sub_ComboSelectedItem(Cmb_Cuentas, lId_Cuenta)
    End If
End Sub
Private Sub Form_Load()
   Me.Top = 1
   Me.Left = 1
   With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SEARCH").Image = cBoton_Buscar
        .Buttons("REPORT").Image = cBoton_Modificar
        .Buttons("REFRESH").Image = cBoton_Original
        .Buttons("EXIT").Image = cBoton_Salir
   End With

   Call Sub_CargaForm
End Sub

Private Sub Grilla_Cuentas_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    If Col <> Grilla_Cuentas.ColIndex("CHKINVERSION") Then
        If Col <> Grilla_Cuentas.ColIndex("CHKRESCATE") Then
            Cancel = True
        End If
    End If
End Sub

Private Sub Opt_Consaldo_Click()
    Call Toolbar_ButtonClick(Toolbar.Buttons(1))
End Sub

Private Sub Opt_Todos_Click()
    Call Toolbar_ButtonClick(Toolbar.Buttons(1))
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEARCH"
        If ValidaEntradaDatos Then
            Call Sub_CargarDatos
        End If
    Case "REFRESH"
      Call Sub_Limpiar
    Case "REPORT"
      Call Sub_Crea_Informe
    Case "EXIT"
      Unload Me
  End Select

End Sub
Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Crea_Informe
    Case "EXCEL"
      Call Sub_Crea_Excel
  End Select
End Sub
Private Function ValidaEntradaDatos() As Boolean
Dim bOk As Boolean

    bOk = True
'    If Cmb_Asesor.Text = "" Then
'        MsgBox "Debe seleccionar Asesor", vbInformation, Me.Caption
'        bOk = False
'    Else
'        If Cmb_Cuentas.Text = "" Then
'         MsgBox "Debe seleccionar Cuenta", vbInformation, Me.Caption
'         bOk = False
'        End If
'    End If
    ValidaEntradaDatos = bOk
End Function
Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_FormControl_Color(Me.Controls)
    
    Call Sub_CargaCombo_Asesor(Cmb_Asesor, pTodos:=True)
    Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
    
    Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas, pTodos:=True)
    Call Sub_ComboSelectedItem(Cmb_Cuentas, cCmbKALL)
    
    Call Sub_CargaCombo_Nemotecnicos(Cmb_Instrumento, "FMNAC_RF")
    Call Sub_ComboSelectedItem(Cmb_Instrumento, 1)
    
    Grilla_Cuentas.GridLines = flexGridNone
    Grilla_Cuentas.Rows = 1
    
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Consulta.Value = lCierre.Busca_Ultima_FechaCierre + 1
    Set lCierre = Nothing
    
    sFormato = "#,##0.0000"

    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Form_Resize()
   Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Sub_Limpiar()
Dim lCierre As Class_Verificaciones_Cierre
Dim i As Integer

    Call Sub_ComboSelectedItem(Cmb_Cuentas, cCmbKALL)
    Call Sub_ComboSelectedItem(Cmb_Asesor, cCmbKALL)
    Set lCierre = New Class_Verificaciones_Cierre
    DTP_Fecha_Consulta.Value = lCierre.Busca_Ultima_FechaCierre
    Grilla_Cuentas.Rows = 1
    For i = 10 To 12
        Grilla_Cuentas.ColHidden(i) = False
    Next
    
End Sub
Private Sub Sub_CargarDatos()
Dim lCursor                 As Object
Dim lReg                    As Object
Dim lLinea                  As Integer
Dim dTotalValor             As Double
Dim dTotalCantidad          As Double
Dim iDecimales              As Integer
Dim sFechaDesde             As String
Dim sFechaHasta             As String
Dim lId_Cuenta              As Integer
Dim lId_Asesor              As Integer
Dim lMoneda_Cuenta          As Integer
Dim sTituloT0               As String
Dim sTituloT1               As String
Dim sTituloT2               As String
Dim lDecimalesCuenta        As Integer
Dim dTotalT0                As Double
Dim dTotalT1                As Double
Dim dTotalT2                As Double
'---------------------------------------------
Dim lCursor_FlujoCaja       As hRecord
Dim lCajas_Ctas             As Class_Cajas_Cuenta
Dim lReg_Caj                As hFields
Dim lCursor_Caj             As hRecord
Dim dSaldoCaja              As Double
Dim lCaja                   As Integer
Dim lCol                    As Integer
Dim sNombre_Col             As String
Dim sSimbolo                As String
Dim nDecimalesCaja          As Integer
Dim nombre_col              As String
Dim lOtrasCajasActivas      As Integer
Dim Totales()               As Detalle_Totales
Dim i As Integer
Dim pTodos                  As Integer
'---------------------------------------------

    If Opt_Todos.Value = True Then
        pTodos = 1
    Else
        pTodos = 0
    End If

    Call Sub_Bloquea_Puntero(Me)
    Grilla_Cuentas.Rows = 1
        
    Call Compone_Fechas(sFechaDesde, sFechaHasta, sTituloT0, sTituloT1, sTituloT2)
    
    lId_Cuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Cuentas))
    lId_Asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesor) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Asesor))
    
    ReDim Preserve Totales(0)
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_CAJAS$EntregaFlujosCaja_no_FFMM"
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pFecha_Desde", ePT_Caracter, sFechaDesde, ePD_Entrada
    gDB.Parametros.Add "pFecha_Hasta", ePT_Caracter, sFechaHasta, ePD_Entrada
    gDB.Parametros.Add "pFecha_Consulta", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
    gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    gDB.Parametros.Add "pTodos", ePT_Numero, pTodos, ePD_Entrada
    If lId_Cuenta <> 0 Then
        gDB.Parametros.Add "pId_Cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    End If
    If lId_Asesor <> 0 Then
        gDB.Parametros.Add "pId_asesor", ePT_Numero, lId_Asesor, ePD_Entrada
    End If
    If Not gDB.EjecutaSP Then
        MsgBox "Error " & gDB.ErrMsg
        Exit Sub
    End If
    
    lOtrasCajasActivas = 0
    dTotalT0 = 0
    'dTotalT1 = 0
    'dTotalT2 = 0
    
    Set lCursor_FlujoCaja = gDB.Parametros("Pcursor").Valor
    If lCursor_FlujoCaja.Count > 0 Then
        ReDim Preserve Totales(10)
        For i = 1 To 10
            With Totales(lMoneda_Cuenta)
                .Decimales = 0
                .SimboloMoneda = ""
                .TotalT0 = 0
                .TotalT1 = 0
                .TotalT2 = 0
            End With
        Next
        Call SetCell(Grilla_Cuentas, 0, "T0", sTituloT0, pAutoSize:=True)
        'Call SetCell(Grilla_Cuentas, 0, "T1", sTituloT1, pAutoSize:=True)
        'Call SetCell(Grilla_Cuentas, 0, "T2", sTituloT2, pAutoSize:=True)
                
        For Each lReg In lCursor_FlujoCaja
            Set lCajas_Ctas = New Class_Cajas_Cuenta
            With lCajas_Ctas
                .Campo("id_cuenta").Valor = lReg("id_cuenta").Value
                If .Buscar(False) Then
                    Set lCursor_Caj = .Cursor
                Else
                    MsgBox .ErrMsg, vbCritical, Me.Caption
                    Exit Sub
                End If
            End With
            
            lDecimalesCuenta = TraeDecimalesCuenta(lReg("id_moneda").Value)
            
            lMoneda_Cuenta = lReg("id_moneda").Value
            lLinea = Grilla_Cuentas.Rows
            Call Grilla_Cuentas.AddItem("")
            Call SetCell(Grilla_Cuentas, lLinea, "colum_pk", Trim(lReg("id_cuenta").Value), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "dsc_cuenta", lReg("num_cuenta").Value, pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "id_cliente", lReg("id_cliente").Value, pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "Rut_cliente", lReg("rut_cliente").Value, pAutoSize:=True)
            Call SetCell(Grilla_Cuentas, lLinea, "nombre_cliente", lReg("nombre_cliente").Value, pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "nombre_asesor", NVL(lReg("nombre_asesor").Value, ""), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "moneda", NVL(lReg("Simbolo_Moneda").Value, ""), pAutoSize:=False)
            Call SetCell(Grilla_Cuentas, lLinea, "T0", FormatNumber(NVL(lReg("monto_t0").Value, 0), lDecimalesCuenta), pAutoSize:=False)
            'Call SetCell(Grilla_Cuentas, lLinea, "T1", FormatNumber(NVL(lReg("monto_t1").Value, 0), lDecimalesCuenta), pAutoSize:=False)
            'Call SetCell(Grilla_Cuentas, lLinea, "T2", FormatNumber(NVL(lReg("monto_t2").Value, 0), lDecimalesCuenta), pAutoSize:=False)
            
            dTotalT0 = dTotalT0 + NVL(lReg("monto_t0").Value, 0)
            'dTotalT1 = dTotalT1 + NVL(lReg("monto_t1").Value, 0)
            'dTotalT2 = dTotalT2 + NVL(lReg("monto_t2").Value, 0)
            'ReDim Preserve Totales(10)
            With Totales(lMoneda_Cuenta)
                .Decimales = lDecimalesCuenta
                .SimboloMoneda = lReg("simbolo_moneda").Value
                .TotalT0 = .TotalT0 + NVL(lReg("monto_t0").Value, 0)
                '.TotalT1 = .TotalT1 + NVL(lReg("monto_t1").Value, 0)
                '.TotalT2 = .TotalT2 + NVL(lReg("monto_t2").Value, 0)
            End With
            lCaja = 0
            lCol = 8
            For Each lReg_Caj In lCursor_Caj
                If lReg_Caj("id_moneda").Value <> lMoneda_Cuenta Then
                    lCaja = lCaja + 1
                    lCol = lCol + 1
                    sNombre_Col = "Caja_" & CStr(lCaja)
                    Grilla_Cuentas.ColHidden(lCol) = False
                    nDecimalesCaja = lReg_Caj("Decimales").Value
                    sSimbolo = NVL(lReg_Caj("SIMBOLO").Value, "")
                    dSaldoCaja = SaldoCaja(lReg_Caj("id_caja_cuenta").Value, lReg("id_cuenta").Value)
                    Call SetCell(Grilla_Cuentas, lLinea, sNombre_Col, sSimbolo & FormatNumber(dSaldoCaja, nDecimalesCaja), pAutoSize:=False)
                    If lOtrasCajasActivas = 0 Then
                       lOtrasCajasActivas = lCaja
                    End If
                End If
            Next
        Next
        'Muestra Totales
        For i = 0 To UBound(Totales)
            If Totales(i).SimboloMoneda <> "" Then
                lLinea = Grilla_Cuentas.Rows
                Call Grilla_Cuentas.AddItem("")
                Call SetCell(Grilla_Cuentas, lLinea, "nombre_asesor", "TOTAL EN " & Totales(i).SimboloMoneda, pAutoSize:=False)
                Call SetCell(Grilla_Cuentas, lLinea, "T0", FormatNumber(Totales(i).TotalT0, Totales(i).Decimales), pAutoSize:=False)
                'Call SetCell(Grilla_Cuentas, lLinea, "T1", FormatNumber(Totales(i).TotalT1, Totales(i).Decimales), pAutoSize:=False)
                'Call SetCell(Grilla_Cuentas, lLinea, "T2", FormatNumber(Totales(i).TotalT2, Totales(i).Decimales), pAutoSize:=False)
                Grilla_Cuentas.Cell(flexcpFontBold, lLinea, 5) = True
                Grilla_Cuentas.Cell(flexcpFontBold, lLinea, 7) = True
                'Grilla_Cuentas.Cell(flexcpFontBold, lLinea, 8) = True
                'Grilla_Cuentas.Cell(flexcpFontBold, lLinea, 9) = True
            End If
        Next i
        
        
        'Borra T�tulo de Cajas adicionales que no tiene info
        For i = 2 To lOtrasCajasActivas Step -1
            Grilla_Cuentas.ColHidden(i + 10) = True '10
        Next
    End If
    
ExitProcedure:
    Set lCursor_Caj = Nothing
    Set lCursor_FlujoCaja = Nothing
    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Sub Compone_Fechas(ByRef pFechaDesde As String, ByRef pFechaHasta As String, _
                           ByRef pTituloT0 As String, ByRef pTituloT1 As String, ByRef pTituloT2 As String)
Dim sFecha As String
Dim FechaDate As Date
Dim sFechaDesde As String
Dim sFechaHasta As String

    sFechaDesde = ""
    sFechaHasta = ""
    
'Rango para T0
    FechaDate = Fnt_Calcula_Dia_Habil_Menos(DTP_Fecha_Consulta.Value, 1)
    sFecha = CStr(FechaDate)
    sFechaDesde = Mid(sFecha, 7, 4) & Mid(sFecha, 4, 2) & Mid(sFecha, 1, 2)
    
    sFecha = CStr(DTP_Fecha_Consulta.Value)
    sFechaHasta = Mid(sFecha, 7, 4) & Mid(sFecha, 4, 2) & Mid(sFecha, 1, 2)
    
    pTituloT0 = "Disp. Al " & DTP_Fecha_Consulta.Value
'Rango para T1
    sFecha = CStr(DTP_Fecha_Consulta.Value)
    sFechaDesde = sFechaDesde & ";" & Mid(sFecha, 7, 4) & Mid(sFecha, 4, 2) & Mid(sFecha, 1, 2)
    
    FechaDate = Fnt_Calcula_Proximo_Dia_Habil(DTP_Fecha_Consulta.Value, 1)
    sFecha = CStr(FechaDate)
    sFechaHasta = sFechaHasta & ";" & Mid(sFecha, 7, 4) & Mid(sFecha, 4, 2) & Mid(sFecha, 1, 2)
    
    pTituloT1 = "Al " & FechaDate
    
'Rango para T2
    FechaDate = Fnt_Calcula_Proximo_Dia_Habil(DTP_Fecha_Consulta.Value, 1)
    sFecha = CStr(FechaDate)
    sFechaDesde = sFechaDesde & ";" & Mid(sFecha, 7, 4) & Mid(sFecha, 4, 2) & Mid(sFecha, 1, 2)
    
    FechaDate = Fnt_Calcula_Proximo_Dia_Habil(DTP_Fecha_Consulta.Value, 2)
    sFecha = CStr(FechaDate)
    sFechaHasta = sFechaHasta & ";" & Mid(sFecha, 7, 4) & Mid(sFecha, 4, 2) & Mid(sFecha, 1, 2)
     
    pTituloT2 = "Al " & FechaDate
    
    pFechaDesde = sFechaDesde
    pFechaHasta = sFechaHasta
End Sub


Private Sub Sub_Crea_Excel()
Dim nHojas, i       As Integer
Dim hoja            As Integer
Dim Index           As Integer
    
    Set fApp_Excel = CreateObject("Excel.application")
    fApp_Excel.DisplayAlerts = False
    Set fLibro = fApp_Excel.Workbooks.Add
    
    fApp_Excel.ActiveWindow.DisplayGridlines = False
  
    ' App_Excel.Visible = True
    Call Sub_Bloquea_Puntero(Me)
    
    fLibro.Worksheets.Add
   
    With fLibro
        For i = .Worksheets.Count To 2 Step -1
            .Worksheets(i).Delete
        Next i
        .Sheets(1).Select
        .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).Select
            
        .ActiveSheet.Range("A5").Value = "Reporte Flujo de Caja"
        
        .ActiveSheet.Range("A5:E5").Font.Bold = True
        .ActiveSheet.Range("A5:E5").HorizontalAlignment = xlLeft
        '.ActiveSheet.Range("A5:E5").Merge
        
        .ActiveSheet.Range("A8").Value = "Fecha Consulta   " & DTP_Fecha_Consulta.Value
        
        .ActiveSheet.Range("A8:B8").HorizontalAlignment = xlLeft
        .ActiveSheet.Range("A8:B8").Font.Bold = True

        .Worksheets.Item(1).Columns("A:A").ColumnWidth = 15
        .Worksheets.Item(1).Columns("B:B").ColumnWidth = 15
        .Worksheets.Item(1).Columns("C:C").ColumnWidth = 50
        .Worksheets.Item(1).Columns("D:D").ColumnWidth = 40
        .Worksheets.Item(1).Columns("E:E").ColumnWidth = 10
        .Worksheets.Item(1).Columns("F:F").ColumnWidth = 20
        .Worksheets.Item(1).Columns("G:G").ColumnWidth = 20
        .Worksheets.Item(1).Columns("H:H").ColumnWidth = 20
        .Worksheets.Item(1).Columns("I:I").ColumnWidth = 20
        .Worksheets.Item(1).Columns("J:J").ColumnWidth = 20
        .Worksheets.Item(1).Columns("K:K").ColumnWidth = 20
        .Worksheets.Item(1).Columns("L:L").ColumnWidth = 20
        .Worksheets.Item(1).Name = "Flujo de Caja"
    End With
    
    iFilaExcel = 10
    
    Generar_Listado_Excel
    
    fApp_Excel.Visible = True
    fApp_Excel.UserControl = True
    
    
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub


Private Sub Generar_Listado_Excel()
Dim lReg                 As hCollection.hFields

Dim lxHoja               As Worksheet
'----------------------------------------------------
Dim bImprimioTitulo      As Boolean
Dim lFilaGrilla          As Integer
Dim iFilaInicio          As Integer
Dim iColGrilla           As Integer
Dim iColExcel            As Integer
Dim lIdCuenta            As String
Dim dTotalCantidad       As Double
Dim dTotalValor          As Double
Dim sNombre_Col          As String
Dim iTotalColGrilla      As Integer
Dim sValor               As String
Dim nDecimales           As Integer
Dim sFormatoValorMercado As String
Dim dblTotal             As Double
Dim sFechaDesde          As String
Dim sFechaHasta          As String
Dim lCursor_FlujoCaja    As hRecord
Dim lId_Cuenta           As Integer
Dim lId_Asesor           As Integer
Dim lMoneda_Cuenta       As Integer
Dim iColFinal            As Integer
Dim sFormatoCaja         As String
Dim i                    As Integer
Dim sTituloT0            As String
Dim sTituloT1            As String
Dim sTituloT2            As String
Dim lDecimalesCuenta     As Integer
Dim Totales()            As Detalle_Totales
Dim dTotalT0             As Double
Dim dTotalT1             As Double
Dim dTotalT2             As Double
'---------------------------------------------
Dim lCajas_Ctas          As Class_Cajas_Cuenta
Dim lReg_Caj             As hFields
Dim lCursor_Caj          As hRecord
Dim dSaldoCaja           As Double
Dim lCaja                As Integer
Dim lCol                 As Integer
Dim sSimbolo             As String
Dim nDecimalesCaja       As Integer
Dim nombre_col           As String
Dim lOtrasCajasActivas   As Integer
'---------------------------------------------
    
    'On Error GoTo ErrProcedure
    
    
    bImprimioTitulo = False
    iColExcel = 1
    dTotalT0 = 0
    dTotalT1 = 0
    dTotalT2 = 0
    iTotalColGrilla = Grilla_Cuentas.Cols
    
    Set lxHoja = fLibro.ActiveSheet
    
    Call Compone_Fechas(sFechaDesde, sFechaHasta, sTituloT0, sTituloT1, sTituloT2)
    
    If Not bImprimioTitulo Then
        Call ImprimeEncabezado(sTituloT0, sTituloT1, sTituloT2)
        bImprimioTitulo = True
        iFilaInicio = iFilaExcel + 1
    End If
    
    
    lId_Cuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Cuentas))
    lId_Asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesor) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Asesor))
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_CAJAS$EntregaFlujosCaja"
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pFecha_Desde", ePT_Caracter, sFechaDesde, ePD_Entrada
    gDB.Parametros.Add "pFecha_Hasta", ePT_Caracter, sFechaHasta, ePD_Entrada
    gDB.Parametros.Add "pFecha_Consulta", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
    gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    If lId_Cuenta <> 0 Then
        gDB.Parametros.Add "pId_Cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    End If
    If lId_Asesor <> 0 Then
        gDB.Parametros.Add "pId_asesor", ePT_Numero, lId_Asesor, ePD_Entrada
    End If
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If
    
    lOtrasCajasActivas = 0
    
    iFilaInicio = iFilaExcel
    Set lCursor_FlujoCaja = gDB.Parametros("Pcursor").Valor
    If lCursor_FlujoCaja.Count > 0 Then
        ReDim Preserve Totales(10)
        For i = 1 To 10
            With Totales(lMoneda_Cuenta)
                .Decimales = 0
                .SimboloMoneda = ""
                .TotalT0 = 0
                .TotalT1 = 0
                .TotalT2 = 0
            End With
        Next
        With lxHoja

            For Each lReg In lCursor_FlujoCaja
                lDecimalesCuenta = TraeDecimalesCuenta(lReg("id_moneda").Value)
                iColExcel = 1
                Set lCajas_Ctas = New Class_Cajas_Cuenta
                With lCajas_Ctas
                    .Campo("id_cuenta").Valor = lReg("id_cuenta").Value
                    If .Buscar(False) Then
                        Set lCursor_Caj = .Cursor
                    Else
                        MsgBox .ErrMsg, vbCritical, Me.Caption
                        Exit Sub
                    End If
                End With
                lMoneda_Cuenta = lReg("id_moneda").Value
        
                iFilaExcel = iFilaExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("num_cuenta").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("rut_cliente").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("nombre_cliente").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("nombre_asesor").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = lReg("simbolo_moneda").Value
                
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = NVL(lReg("monto_t0").Value, 0)
                .Cells(iFilaExcel, iColExcel).NumberFormat = Fnt_Formato_Moneda(lReg("id_moneda").Value) 'sformato
        
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = NVL(lReg("monto_t1").Value, 0)
                .Cells(iFilaExcel, iColExcel).NumberFormat = Fnt_Formato_Moneda(lReg("id_moneda").Value)
        
                iColExcel = iColExcel + 1
                .Cells(iFilaExcel, iColExcel).Value = NVL(lReg("monto_t2").Value, 0)
                .Cells(iFilaExcel, iColExcel).NumberFormat = Fnt_Formato_Moneda(lReg("id_moneda").Value)
                lCaja = 0
              
                dTotalT0 = dTotalT0 + NVL(lReg("monto_t0").Value, 0)
                dTotalT1 = dTotalT1 + NVL(lReg("monto_t1").Value, 0)
                dTotalT2 = dTotalT2 + NVL(lReg("monto_t2").Value, 0)
                'ReDim Preserve Totales(10)
                With Totales(lMoneda_Cuenta)
                    .Decimales = lDecimalesCuenta
                    .SimboloMoneda = lReg("simbolo_moneda").Value
                    .TotalT0 = .TotalT0 + NVL(lReg("monto_t0").Value, 0)
                    .TotalT1 = .TotalT1 + NVL(lReg("monto_t1").Value, 0)
                    .TotalT2 = .TotalT2 + NVL(lReg("monto_t2").Value, 0)
                End With

                
                For Each lReg_Caj In lCursor_Caj
                    If lReg_Caj("id_moneda").Value <> lMoneda_Cuenta Then
                        lCaja = lCaja + 1
                        iColExcel = iColExcel + 1
                        sNombre_Col = "Saldo Caja " & CStr(lCaja + 1)
                        Grilla_Cuentas.ColHidden(lCol) = False
                        nDecimalesCaja = lReg_Caj("Decimales").Value
                        sSimbolo = NVL(lReg_Caj("SIMBOLO").Value, "")
                        dSaldoCaja = SaldoCaja(lReg_Caj("id_caja_cuenta").Value, lReg("id_cuenta").Value)
                        For i = 1 To nDecimalesCaja
                            sFormatoCaja = "#,##0." & "0"
                        Next
                        If lOtrasCajasActivas = 0 Then
                           lOtrasCajasActivas = lCaja
                        End If
                        .Cells(iFilaInicio, iColExcel).Value = sNombre_Col
                        .Cells(iFilaExcel, iColExcel).Value = sSimbolo & FormatNumber(dSaldoCaja, nDecimalesCaja)
                        .Cells(iFilaExcel, iColExcel).NumberFormat = Fnt_Formato_Moneda(lReg_Caj("id_moneda").Value)
                        .Cells(iFilaExcel, iColExcel).HorizontalAlignment = xlRight
                        '.Cells(iFilaExcel, icolexcel).NumberFormat = sFormatoCaja
                    End If
                Next

            Next
            'Muestra Totales
            For i = 0 To UBound(Totales)
                If Totales(i).SimboloMoneda <> "" Then
                    iFilaExcel = iFilaExcel + 1
                    .Cells(iFilaExcel, 4).Value = "TOTAL EN " & Totales(i).SimboloMoneda
                    .Cells(iFilaExcel, 6).Value = Totales(i).TotalT0
                    .Cells(iFilaExcel, 7).Value = Totales(i).TotalT1
                    .Cells(iFilaExcel, 8).Value = Totales(i).TotalT2
                    .Cells(iFilaExcel, 6).NumberFormat = Fnt_Formato_Moneda(i)
                    .Cells(iFilaExcel, 7).NumberFormat = Fnt_Formato_Moneda(i)
                    .Cells(iFilaExcel, 8).NumberFormat = Fnt_Formato_Moneda(i)
                    .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).BorderAround
                    .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).Borders.Color = RGB(0, 0, 0)
                    .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).Interior.Color = RGB(255, 255, 0) 'verde:&H00C0FFC0&  amarillo:RGB(255, 255, 0)
                    .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).Font.Bold = True
                End If
            Next
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaExcel, 1)).HorizontalAlignment = xlLeft
            .Range(.Cells(iFilaInicio, 2), .Cells(iFilaExcel, iColExcel)).HorizontalAlignment = xlRight

            
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).BorderAround
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).Font.Bold = True
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).WrapText = True
            .Range(.Cells(iFilaInicio, 1), .Cells(iFilaInicio, iColExcel)).HorizontalAlignment = xlCenter

        End With
    End If
ExitProcedure:

End Sub

Private Sub ImprimeEncabezado(ByVal pTituloT0 As String, ByVal pTituloT1 As String, ByVal pTituloT2 As String)
Dim lxHoja          As Worksheet
Dim iColExcel       As Integer
Dim i               As Integer

    Set lxHoja = fLibro.ActiveSheet
    iFilaExcel = iFilaExcel + 1
    iColExcel = 1
    With lxHoja
        .Cells(iFilaExcel, iColExcel).Value = "Cuenta"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Rut"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Cliente"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Asesor"
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Mda."
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = pTituloT0
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = pTituloT1
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = pTituloT2

        
    End With
    
End Sub

Private Sub Sub_Crea_Informe()
'------------------------------------------
Const clrHeader = &HD0D0D0
'Const sHeader_Clt = "Cuenta|Rut|Nombre Cliente|Disponible T+0|Retenci�n T+1|Retenci�n T+2"
'Const sFormat_Clt = "1000|>1300|3000|>1100|>1100|>1100"
Dim sHeader_Clt             As String
Dim sFormat_Clt             As String
'------------------------------------------
Dim sRecord
Dim sRecordCaja
Dim bAppend
Dim lLinea                  As Integer
'------------------------------------------
Dim lForm                   As Frm_Reporte_Generico
Dim sTitulo                 As String
Dim sFechaDesde             As String
Dim sFechaHasta             As String
Dim lId_Cuenta              As Integer
Dim lId_Asesor              As Integer
Dim lMoneda_Cuenta          As Integer
Dim sNombre_Col             As String
Dim sTituloT0               As String
Dim sTituloT1               As String
Dim sTituloT2               As String
Dim lDecimalesCuenta        As Integer
Dim dTotalT0                As Double
Dim dTotalT1                As Double
Dim dTotalT2                As Double
'---------------------------------------------
Dim lReg                    As hCollection.hFields
Dim lCursor_FlujoCaja       As hRecord
Dim lCajas_Ctas             As Class_Cajas_Cuenta
Dim lReg_Caj                As hFields
Dim lCursor_Caj             As hRecord
Dim dSaldoCaja              As Double
Dim lCaja                   As Integer
Dim lCol                    As Integer
Dim sSimbolo                As String
Dim nDecimalesCaja          As Integer
Dim nombre_col              As String
Dim lOtrasCajasActivas      As Integer
Dim Totales()               As Detalle_Totales
Dim i                       As Integer
'---------------------------------------------


    Call Sub_Bloquea_Puntero(Me)
        
    sTitulo = "Reporte Flujo de Caja"
    If Not Fnt_Form_Validar(Me.Controls) Then
        Exit Sub
    End If

    Call Compone_Fechas(sFechaDesde, sFechaHasta, sTituloT0, sTituloT1, sTituloT2)
    
    lId_Cuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuentas) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Cuentas))
    lId_Asesor = IIf(Fnt_ComboSelected_KEY(Cmb_Asesor) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_Asesor))
    
    dTotalT0 = 0
    dTotalT1 = 0
    dTotalT2 = 0
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_CAJAS$EntregaFlujosCaja"
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pFecha_Desde", ePT_Caracter, sFechaDesde, ePD_Entrada
    gDB.Parametros.Add "pFecha_Hasta", ePT_Caracter, sFechaHasta, ePD_Entrada
    gDB.Parametros.Add "pFecha_Consulta", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
    gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    If lId_Cuenta <> 0 Then
        gDB.Parametros.Add "pId_Cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
    End If
    If lId_Asesor <> 0 Then
        gDB.Parametros.Add "pId_asesor", ePT_Numero, lId_Asesor, ePD_Entrada
    End If
    If Not gDB.EjecutaSP Then
        Exit Sub
    End If

    Set lCursor_FlujoCaja = gDB.Parametros("Pcursor").Valor
    If lCursor_FlujoCaja.Count > 0 Then
        ReDim Preserve Totales(10)
        For i = 1 To 10
            With Totales(lMoneda_Cuenta)
                .Decimales = 0
                .SimboloMoneda = ""
                .TotalT0 = 0
                .TotalT1 = 0
                .TotalT2 = 0
            End With
        Next

        sHeader_Clt = "Cuenta|Rut|Nombre Cliente|Asesor|Moneda|" & sTituloT0 & "|" & sTituloT1 & "|" & sTituloT2
        sFormat_Clt = "900|>1000|4000|2000|700|>1300|>1300|>1300"
        Set lForm = New Frm_Reporte_Generico
        Call lForm.Sub_InicarDocumento(pTitulo:=sTitulo _
                                       , pTipoSalida:=ePrinter.eP_Pantalla _
                                       , pOrientacion:=orLandscape)
        With lForm.VSPrinter
            .FontSize = 8
            .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
            .Paragraph = "" 'salto de linea
            .FontBold = False
            
            .StartTable
            .FontSize = 7
            .TableCell(tcAlignCurrency) = False
            For Each lReg In lCursor_FlujoCaja
                lDecimalesCuenta = TraeDecimalesCuenta(lReg("id_moneda").Value)
                Set lCajas_Ctas = New Class_Cajas_Cuenta
                With lCajas_Ctas
                    .Campo("id_cuenta").Valor = lReg("id_cuenta").Value
                    If .Buscar(False) Then
                        Set lCursor_Caj = .Cursor
                    Else
                        MsgBox .ErrMsg, vbCritical, Me.Caption
                        Exit Sub
                    End If
                End With
                lMoneda_Cuenta = lReg("id_moneda").Value
                Rem Genera el reporte
                sRecord = NVL(lReg("num_cuenta").Value, "") & "|" & _
                          NVL(lReg("rut_cliente").Value, "") & "|" & _
                          NVL(lReg("nombre_cliente").Value, "") & "|" & _
                          NVL(lReg("nombre_asesor").Value, "") & "|" & _
                          NVL(lReg("simbolo_moneda").Value, "") & "|" & _
                          FormatNumber(NVL(lReg("monto_t0").Value, 0), lDecimalesCuenta) & "|" & _
                          FormatNumber(NVL(lReg("monto_t1").Value, 0), lDecimalesCuenta) & "|" & _
                          FormatNumber(NVL(lReg("monto_t2").Value, 0), lDecimalesCuenta)
                
                dTotalT0 = dTotalT0 + NVL(lReg("monto_t0").Value, 0)
                dTotalT1 = dTotalT1 + NVL(lReg("monto_t1").Value, 0)
                dTotalT2 = dTotalT2 + NVL(lReg("monto_t2").Value, 0)
                'ReDim Preserve Totales(10)
                With Totales(lMoneda_Cuenta)
                    .Decimales = lDecimalesCuenta
                    .SimboloMoneda = lReg("simbolo_moneda").Value
                    .TotalT0 = .TotalT0 + NVL(lReg("monto_t0").Value, 0)
                    .TotalT1 = .TotalT1 + NVL(lReg("monto_t1").Value, 0)
                    .TotalT2 = .TotalT2 + NVL(lReg("monto_t2").Value, 0)
                End With
                
                For Each lReg_Caj In lCursor_Caj
                    If lReg_Caj("id_moneda").Value <> lMoneda_Cuenta Then
                        lCaja = lCaja + 1
                        sNombre_Col = "Saldo Caja " & CStr(lCaja + 1)
                        Grilla_Cuentas.ColHidden(lCol) = False
                        nDecimalesCaja = lReg_Caj("Decimales").Value
                        sSimbolo = NVL(lReg_Caj("SIMBOLO").Value, "")
                        dSaldoCaja = SaldoCaja(lReg_Caj("id_caja_cuenta").Value, lReg("id_cuenta").Value)
                        sHeader_Clt = sHeader_Clt & "|" & sNombre_Col
                        sFormat_Clt = sFormat_Clt & "|>1300"
                        sRecord = sRecord & "|" & _
                                  sSimbolo & FormatNumber(dSaldoCaja, nDecimalesCaja)
                        
                    End If
                Next
          
                .AddTable sFormat_Clt, sHeader_Clt, sRecord, clrHeader, , bAppend
            Next
            sHeader_Clt = "Cuenta|Rut|Nombre Cliente|Asesor|Moneda|" & sTituloT0 & "|" & sTituloT1 & "|" & sTituloT2
            sFormat_Clt = "900|>1000|4000|2000|700|>1300|>1300|>1300"
            .Paragraph = ""
            For i = 0 To UBound(Totales)
                If Totales(i).SimboloMoneda <> "" Then
                    sRecord = "|" & _
                              "|" & _
                              "|" & _
                              "TOTAL EN " & Totales(i).SimboloMoneda & "|" & _
                              "|" & _
                              FormatNumber(Totales(i).TotalT0, Totales(i).Decimales) & "|" & _
                              FormatNumber(Totales(i).TotalT1, Totales(i).Decimales) & "|" & _
                              FormatNumber(Totales(i).TotalT2, Totales(i).Decimales)
                    .FontBold = True
                    .AddTable sFormat_Clt, sHeader_Clt, sRecord, clrHeader, , bAppend
                End If
            Next
            .TableCell(tcFontBold, 0) = True
            .EndTable
            .EndDoc
        End With
        
    End If
    Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Function SaldoCaja(ByVal lIdCajaCuenta As Long, ByVal lIdCuenta As Long)
Dim ultimosaldo As Double

    gDB.Parametros.Clear
    gDB.Procedimiento = "Pkg_Saldos_Caja$Buscar_Saldos_Cuenta"
    gDB.Parametros.Add "PID_CAJA_CUENTA", ePT_Numero, lIdCajaCuenta, ePD_Entrada
    gDB.Parametros.Add "PID_CUENTA", ePT_Numero, lIdCuenta, ePD_Entrada
    'gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, CDate(UltimoDiaDelMes(DTP_Fecha_Consulta.Value)) + 1, ePD_Entrada
    gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, DTP_Fecha_Consulta.Value, ePD_Entrada
    gDB.Parametros.Add "PSaldo", ePT_Numero, ultimosaldo, ePD_Salida
    
    If gDB.EjecutaSP Then
        ultimosaldo = gDB.Parametros("Psaldo").Valor
    End If
    
    SaldoCaja = ultimosaldo
End Function


    Public Function Fnt_Calcula_Dia_Habil_Menos(pFecha_Inicio As Date, pDias As Long) As Date
    Dim lDia_Semana As Integer
    Dim lDia As Long
    Dim lDias_Habiles As Long
    Dim lSabado As Boolean
      
        If pDias = 0 Then
          Fnt_Calcula_Dia_Habil_Menos = pFecha_Inicio
          Exit Function
        End If
        
        lDias_Habiles = 0
        lSabado = False
        For lDia = 1 To pDias
          
          lDias_Habiles = lDias_Habiles + 1
          
          Rem Dia de la semana de la fecha inicio + lDia
          lDia_Semana = DateTime.Weekday(pFecha_Inicio - lDias_Habiles)
          
          Rem Verifica que el dia sea sabado
          If lDia_Semana = vbSaturday Then
            lDias_Habiles = lDias_Habiles + 1
            lSabado = True
          Rem Verifica que el dia sea sabado
          ElseIf lDia_Semana = vbSunday Then
            If Not lSabado Then
              lDias_Habiles = lDias_Habiles + 2
            End If
          Rem Si el dia no es sabado ni domingo verifica q no sea feriado
          ElseIf Fnt_Verifica_Feriado(pFecha_Inicio - lDias_Habiles) Then
            lDias_Habiles = lDias_Habiles + 1
          End If
          
        Next
        
        Fnt_Calcula_Dia_Habil_Menos = pFecha_Inicio - lDias_Habiles
      
    End Function


Private Function TraeDecimalesCuenta(lId_Moneda) As Integer
Dim lDecimalesCuenta    As Integer
Dim lcMoneda            As Object
    lDecimalesCuenta = 0
    Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
    With lcMoneda
      .Campo("id_Moneda").Valor = lId_Moneda
      If .Buscar Then
        lDecimalesCuenta = .Cursor(1)("dicimales_mostrar").Value
      End If
    End With
    Set lcMoneda = Nothing
    TraeDecimalesCuenta = lDecimalesCuenta
End Function

Public Function Fnt_Calcula_Proximo_Dia_Habil(pFecha_Inicio As Date, pDias As Long) As Date
'-------------------------
Rem Funcion que entrega cual es el "pDias" h�bil
'--------------------------
Dim lDia As Long
Dim lSabado As Boolean
Dim lFecha As Date
Dim lFechaProxima As Date
  
  If pDias = 0 Then
    lFechaProxima = Fnt_Dia_Habil_MasProximo(pFecha_Inicio)
  Else
    lFecha = pFecha_Inicio
    For lDia = 1 To pDias
      lFechaProxima = Fnt_Dia_Habil_MasProximo(lFecha + 1)
      lFecha = lFechaProxima
    Next
  End If
  Fnt_Calcula_Proximo_Dia_Habil = lFechaProxima
  
End Function



