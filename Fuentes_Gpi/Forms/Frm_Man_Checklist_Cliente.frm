VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Man_Checklist_Cliente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Checklist Cliente"
   ClientHeight    =   4200
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   10155
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   10155
   Begin VB.Frame Frame1 
      Caption         =   "Descripcion de Checklist Cliente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3675
      Left            =   75
      TabIndex        =   0
      Top             =   420
      Width           =   10035
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   120
         TabIndex        =   1
         Top             =   300
         Width           =   9100
         _cx             =   16051
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   4
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Checklist_Cliente.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Flechas 
         Height          =   660
         Left            =   9400
         TabIndex        =   4
         Top             =   400
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "UP"
               Description     =   "Sube de nivel el registro"
               Object.ToolTipText     =   "Sube de nivel el registro."
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "DOWN"
               Description     =   "Baja de nivel el registro"
               Object.ToolTipText     =   "Baja de nivel el registro."
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar2 
            Height          =   255
            Left            =   9420
            TabIndex        =   5
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   635
      ButtonWidth     =   2249
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   11
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "ADD"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Agrega un elemento"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Eliminar"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Elimina un elemento"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un elemento"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reordenar"
            Key             =   "SORT"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Checklist_Cliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("SORT").Image = cBoton_Grabar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With

'==== Agregado por MMA 21/07/08
  With Toolbar_Flechas
  Set .ImageList = MDI_Principal.ImageListGlobal16
       .Buttons("UP").Image = "flecha_up"
       .Buttons("DOWN").Image = "flacha_down"
       .Appearance = ccFlat
  End With
  
  Call Sub_CargaForm
  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
Dim lkey As String

  With Grilla
    If .Row > 0 Then
      lkey = GetCell(Grilla, .Row, "colum_pk")
      
      Call Sub_EsperaVentana(lkey)
      
    End If
  End With
End Sub

Private Sub Sub_EsperaVentana(ByVal pkey)
Dim lForm As Frm_Checklist_Cliente
Dim lnombre As String
Dim lNom_Form As String
  
  lNom_Form = "Frm_Checklist_Cliente"
  
  If Not Fnt_ExisteVentanaKey(lNom_Form, pkey) Then
    lnombre = Me.Name
    
    Set lForm = New Frm_Checklist_Cliente
    Call lForm.Fnt_Modificar(pkey, pCod_Arbol_Sistema:=fCod_Arbol_Sistema)
    
    Do While Fnt_ExisteVentanaKey(lNom_Form, pkey)
      DoEvents
    Loop
    
    If Fnt_ExisteVentana(lnombre) Then
      Call Sub_CargarDatos
    Else
      Exit Sub
    End If
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agregar
    Case "DEL"
      Call Sub_Eliminar
    Case "UPDATE"
      Call Grilla_DblClick
    Case "SORT"
      Call Sub_GrabaOrden
    Case "REFRESH"
      Call Sub_CargarDatos
    Case "EXIT"
      Unload Me
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
  End Select
End Sub

Private Sub Sub_CargarDatos()
Dim lreg    As hCollection.hFields
Dim lLinea  As Long
Dim lID     As String
Dim lcChecklist As Object 'Class_Checklist_Cliente

  If Grilla.Row > 0 Then
    lID = GetCell(Grilla, Grilla.Row, "colum_pk")
  Else
    lID = ""
  End If
  
  Grilla.Rows = 1
  
  Set lcChecklist = Fnt_CreateObject(cDLL_Checklist_Cliente) 'New Class_Checklist_Cliente
  With lcChecklist
    Set .gDB = gDB
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    .Campo("tipo_checklist").Valor = 1
    If .Buscar Then
      Call Prc_ComponentOne.Sub_hRecord2Grilla(.Cursor, Grilla, "id_checklist")
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcChecklist = Nothing

  If Not lID = "" Then
    Grilla.Row = Grilla.FindRow(lID, , Grilla.ColIndex("colum_pk"))
    If Not Grilla.Row = cNewEntidad Then
      Call Grilla.ShowCell(Grilla.Row, Grilla.ColIndex("colum_pk"))
    End If
  End If
  
End Sub

Private Sub Sub_CargaForm()
  Rem Limpia la grilla
  Grilla.Rows = 1

  Call Sub_FormControl_Color(Me.Controls)

  
End Sub

Private Sub Sub_Agregar()
  Call Sub_EsperaVentana(cNewEntidad)
End Sub

Private Sub Sub_Eliminar()
Dim lID As String
Dim lcRel_Cliente_Checklist As Object 'Class_Rel_Cliente_Checklist
Dim lcChecklist As Object 'Class_Checklist_Cliente
Dim lRollback As Boolean

  If Grilla.Row > 0 Then
    If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
      lID = GetCell(Grilla, Grilla.Row, "colum_pk")
      
      gDB.IniciarTransaccion
      lRollback = False
      
      If Not lID = "" Then
      
        Set lcRel_Cliente_Checklist = Fnt_CreateObject(cDLL_Rel_Cliente_Checklist) 'New Class_Rel_Cliente_Checklist
        With lcRel_Cliente_Checklist
          Set .gDB = gDB
          .Campo("id_checklist").Valor = lID
          If Not .Borrar Then
            MsgBox .ErrMsg, vbCritical, Me.Caption
            lRollback = True
            GoTo ErrProcedure
          End If
        End With
          
        Set lcChecklist = Fnt_CreateObject(cDLL_Checklist_Cliente) 'New Class_Checklist_Cliente
        With lcChecklist
          Set .gDB = gDB
          .Campo("id_checklist").Valor = lID
          .Campo("id_empresa").Valor = Fnt_EmpresaActual
          If .Borrar Then
            MsgBox "Checklist eliminado correctamente.", vbInformation + vbOKOnly, Me.Caption
          Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            lRollback = True
            GoTo ErrProcedure
          End If
        End With
        
      End If
      
    End If
  End If
  
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
    Call Sub_CargarDatos
  End If
  
  Set lcRel_Cliente_Checklist = Nothing
  Set lcChecklist = Nothing
  
End Sub
Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="CheckList Clientes" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub



Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

Private Sub Toolbar_Flechas_ButtonClick(ByVal Button As MSComctlLib.Button)
Select Case Button.Key
      Case "UP"
         If Grilla.Row > 0 Then
            Call Sub_Inicio
         End If
      
      Case "DOWN"
         If Grilla.Row > 0 Then
            Call Sub_Final
         End If
   End Select
End Sub

Private Sub Sub_Inicio()
Dim lOrder_Arriba As Double
Dim lOrden_Actual As Double

  If Grilla.Row >= 2 Then
    lOrder_Arriba = GetCell(Grilla, Grilla.Row - 1, "orden")
    lOrden_Actual = GetCell(Grilla, Grilla.Row, "orden")
    
    Call SetCell(Grilla, Grilla.Row - 1, "orden", lOrden_Actual)
    Call SetCell(Grilla, Grilla.Row, "orden", lOrder_Arriba)
   
    Grilla.RowPosition(Grilla.Row) = Grilla.Row - 1
    Grilla.Row = Grilla.Row - 1
  
      
    Call Grilla.ShowCell(Grilla.Row, 0)
  End If
End Sub

Private Sub Sub_Final()
Dim lOrden_Abajo As Double
Dim lOrden_Actual As Double

  If Grilla.Row <= (Grilla.Rows - 2) Then
    lOrden_Abajo = GetCell(Grilla, Grilla.Row + 1, "orden")
    lOrden_Actual = GetCell(Grilla, Grilla.Row, "orden")
    
    Call SetCell(Grilla, Grilla.Row + 1, "orden", lOrden_Actual)
    Call SetCell(Grilla, Grilla.Row, "orden", lOrden_Abajo)
  
    Grilla.RowPosition(Grilla.Row) = Grilla.Row + 1
    Grilla.Row = Grilla.Row + 1
    
    Call Grilla.ShowCell(Grilla.Row, 0)
  End If
End Sub

Private Sub Sub_GrabaOrden()
Dim lFila As Integer

Dim lcChecklist As Object 'Class_Checklist_Cliente

    For lFila = 1 To Grilla.Rows - 1

        Set lcChecklist = Fnt_CreateObject(cDLL_Checklist_Cliente) 'New Class_Checklist_Cliente
        With lcChecklist
          Set .gDB = gDB
          .Campo("id_checklist").Valor = GetCell(Grilla, lFila, "colum_pk")
          .Campo("id_empresa").Valor = Fnt_EmpresaActual
          .Campo("dsc_checklist").Valor = GetCell(Grilla, lFila, "dsc_checklist")
          .Campo("tipo_checklist").Valor = 1
          .Campo("orden").Valor = lFila
          .Campo("con_observacion").Valor = GetCell(Grilla, lFila, "con_observacion")
          If Not .Guardar Then
            MsgBox "Problemas en la reordenaci�n." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            Exit Sub
          End If
        End With
        Set lcChecklist = Nothing
    Next
End Sub
