VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Cuentas_C 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte de Cuentas"
   ClientHeight    =   1470
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6090
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1470
   ScaleWidth      =   6090
   Begin VB.Frame Frame1 
      Caption         =   "Cuentas Vigentes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   915
      Left            =   90
      TabIndex        =   2
      Top             =   450
      Width           =   5865
      Begin TrueDBList80.TDBCombo Cmb_Cuenta 
         Height          =   345
         Left            =   1020
         TabIndex        =   3
         Tag             =   "OBLI=S;CAPTION=Cuentas"
         Top             =   330
         Width           =   4695
         _ExtentX        =   8281
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Cuentas_C.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Lbl_Cuenta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuenta"
         Height          =   330
         Left            =   210
         TabIndex        =   4
         Top             =   330
         Width           =   795
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6090
      _ExtentX        =   10742
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Generar"
            Key             =   "REPORTE"
            Object.ToolTipText     =   "Genera Reporte de la Cuenta"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Cuentas_C"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const RColorRelleno = 213   ' R
Const GColorRelleno = 202   ' G
Const BColorRelleno = 195   ' B

Private Sub Form_Load()
Dim lreg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("REPORTE").Image = cBoton_Modificar
    .Buttons("REFRESH").Image = cBoton_Refrescar
    .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  Call Sub_CargaForm
  Call Sub_Limpiar
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
  Call Sub_CargaCombo_Cuentas(Cmb_Cuenta)
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REPORTE"
      Call Sub_Generar_Reporte
    Case "REFRESH"
      Call Sub_CargaForm
      Call Sub_Limpiar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_Limpiar()
  Call Sub_ComboSelectedItem(Cmb_Cuenta, cCmbKBLANCO)
End Sub

Private Sub Sub_Generar_Reporte()
Dim lId_Cuenta As String
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  End If
  
  lId_Cuenta = IIf(Fnt_ComboSelected_KEY(Cmb_Cuenta) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Cuenta))
  
  Call Sub_Reporte_Cuenta(lId_Cuenta)
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Reporte_Cuenta(pId_Cuenta)
Const clrHeader = &HD0D0D0

Dim sRecord
Dim bAppend
Dim lLinea As Integer
'------------------------------------------
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lCursor_Cta As hRecord
Dim lReg_Cta As hFields
Dim lCajas_Ctas As Class_Cajas_Cuenta
Dim lAlias_Ctas As Class_Alias
Dim lCursor_Alias As hRecord
Dim lReg_Alias As hFields

Dim lCursor_Caj As hRecord
Dim lReg_Caja_Cta As hFields
'------------------------------------------
Dim lCursor_Clt As hRecord
Dim lNom_Clt As String
Dim lEstado_Clt As String
Dim lFlg_Bloqueado As String
Dim lMoneda_Clt As String
Dim iFila As Integer
'------------------------------------------
'Dim lcMoneda As Class_Monedas
Dim lcMoneda As Object
'------------------------------------------
Dim lcClientes  As Object
'------------------------------------------
Dim lForm As Frm_Reporte_Generico
'------------------------------------------
Dim lcAsesor As Class_Asesor
Dim lAsesor As String

  Rem Busca las Cuentas vigentes en el sistema
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_cuenta").Valor = pId_Cuenta
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar Then
      Set lCursor_Cta = .Cursor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
    
  Rem Comienzo de la generación del reporte
  Set lForm = New Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="" _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orPortrait)
     
  With lForm.VsPrinter
      
      '.TableCell(tcRows) = IIf(lCursor_Cta.Count > 0, lCursor_Cta.Count, 0)
      For Each lReg_Cta In lCursor_Cta
        
        .FontSize = "12"
        .FontBold = True
        .Paragraph = UCase("Detalle de la Cuenta")
        .FontSize = "10"
        .FontBold = False
        .DrawLine .MarginLeft, .CurrentY, .PageWidth - .MarginRight, .CurrentY
        .Paragraph = UCase("Información de la Cuenta ")
        .DrawLine .MarginLeft, .CurrentY, .PageWidth - .MarginRight, .CurrentY
        .Paragraph = ""
        .StartTable
        .TableCell(tcRows) = 17
        .TableCell(tcCols) = 3
        .TableCell(tcColWidth, , 1) = "60mm"
        .TableCell(tcColWidth, , 2) = "5mm"
        .TableCell(tcColWidth, , 3) = "120mm"
        
        .TableCell(tcFontSize) = "9"
        .TableBorder = tbNone
        
        'Numero
        .TableCell(tcText, 1, 1) = "Número"
        .TableCell(tcText, 1, 2) = ":"
        .TableCell(tcText, 1, 3) = lReg_Cta("num_cuenta").Value
 
 '==== Modificado por MMA. 21/07/08
        Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
        
          Set lcClientes.gDB = gDB
          lcClientes.Campo("id_Cliente").Valor = lReg_Cta("id_cliente").Value
          If lcClientes.Buscar(True) Then
            'Rut cliente
            .TableCell(tcText, 2, 1) = "Rut Cliente"
            .TableCell(tcText, 2, 2) = ":"
            .TableCell(tcText, 2, 3) = NVL(lcClientes.Cursor(1)("rut_cliente").Value, "")
      
            'Nombre cliente
            .TableCell(tcText, 3, 1) = "Cliente"
            .TableCell(tcText, 3, 2) = ":"
            .TableCell(tcText, 3, 3) = NVL(lcClientes.Cursor(1)("nombre_cliente").Value, "")
          Else
            MsgBox lcClientes.ErrMsg, vbCritical, Me.Caption
            Set lcClientes = Nothing
            GoTo ErrProcedure
          End If
        Set lcClientes = Nothing
        
        'Fecha de Contrato
        .TableCell(tcText, 4, 1) = "Fecha de Contrato"
        .TableCell(tcText, 4, 2) = ":"
        .TableCell(tcText, 4, 3) = "" & lReg_Cta("fecha_contrato").Value
        
        'Fecha Operativa
        .TableCell(tcText, 5, 1) = "Fecha Operativa"
        .TableCell(tcText, 5, 2) = ":"
        .TableCell(tcText, 5, 3) = "" & lReg_Cta("fecha_operativa").Value
        
        'Estado
        .TableCell(tcText, 6, 1) = "Estado"
        .TableCell(tcText, 6, 2) = ":"
        .TableCell(tcText, 6, 3) = lReg_Cta("dsc_estado").Value
        
        'Tipo de Administración
        .TableCell(tcText, 7, 1) = "Tipo Administración"
        .TableCell(tcText, 7, 2) = ":"
        .TableCell(tcText, 7, 3) = lReg_Cta("dsc_tipo_administracion").Value
        
        'Empresa
        .TableCell(tcText, 8, 1) = "Empresa"
        .TableCell(tcText, 8, 2) = ":"
        .TableCell(tcText, 8, 3) = lReg_Cta("dsc_empresa").Value
        
        'Perfil
        .TableCell(tcText, 9, 1) = "Perfil"
        .TableCell(tcText, 9, 2) = ":"
        .TableCell(tcText, 9, 3) = lReg_Cta("dsc_perfil_riesgo").Value
        
        'Porcentaje RF
        .TableCell(tcText, 10, 1) = "Porcentaje RF"
        .TableCell(tcText, 10, 2) = ":"
        .TableCell(tcText, 10, 3) = lReg_Cta("PORCEN_RF").Value
        
        'Porcentaje RV
        .TableCell(tcText, 11, 1) = "Porcentaje RV"
        .TableCell(tcText, 11, 2) = ":"
        .TableCell(tcText, 11, 3) = lReg_Cta("PORCEN_RV").Value
                
       
        Rem Busca el nombre de la moneda
'        Set lcMoneda = New Class_Monedas
        Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
        
          lcMoneda.Campo("id_Moneda").Valor = lReg_Cta("id_moneda").Value
          If lcMoneda.Buscar Then
            'Moneda
            .TableCell(tcText, 12, 1) = "Moneda"
            .TableCell(tcText, 12, 2) = ":"
            .TableCell(tcText, 12, 3) = NVL(lcMoneda.Cursor(1)("dsc_moneda").Value, "")
      
          Else
            MsgBox lcMoneda.ErrMsg, vbCritical, Me.Caption
            Set lcMoneda = Nothing
            GoTo ErrProcedure
          End If
      
        Set lcMoneda = Nothing
        'Abreviatura
        .TableCell(tcText, 13, 1) = "Abreviatura"
        .TableCell(tcText, 13, 2) = ":"
        .TableCell(tcText, 13, 3) = lReg_Cta("abr_cuenta").Value
      
        'Descripción
        .TableCell(tcText, 14, 1) = "Descripción"
        .TableCell(tcText, 14, 2) = ":"
        .TableCell(tcText, 14, 3) = lReg_Cta("dsc_cuenta").Value
        
'==== Agregado por MMA. 21/07/08
        'Fecha de Cierre
        .TableCell(tcText, 15, 1) = "Fecha de Cierre"
        .TableCell(tcText, 15, 2) = ":"
        .TableCell(tcText, 15, 3) = IIf(IsNull(lReg_Cta("fecha_cierre_cuenta").Value), "", lReg_Cta("fecha_cierre_cuenta").Value)
        
        'Tipo de Cuenta
        .TableCell(tcText, 16, 1) = "Tipo de Cuenta"
        .TableCell(tcText, 16, 2) = ":"
        .TableCell(tcText, 16, 3) = IIf(IsNull(lReg_Cta("descripcion_larga").Value), "", lReg_Cta("descripcion_larga").Value)
                       
        'Folio
        .TableCell(tcText, 17, 1) = "Folio"
        .TableCell(tcText, 17, 2) = ":"
        .TableCell(tcText, 17, 3) = IIf(IsNull(lReg_Cta("numero_folio").Value), "", lReg_Cta("numero_folio").Value)
        
        lFlg_Bloqueado = NVL(lReg_Cta("Flg_Bloqueado").Value, "")
        If lFlg_Bloqueado = "N" Then
          lFlg_Bloqueado = "No"
        ElseIf lFlg_Bloqueado = "S" Then
          lFlg_Bloqueado = "Si"
        End If
          
        Set lcAsesor = New Class_Asesor
        
          lcAsesor.Campo("id_Asesor").Valor = NVL(lReg_Cta("id_asesor").Value, "")
          If lcAsesor.Buscar Then
            lAsesor = NVL(lcAsesor.Cursor(1)("nombre").Value, "")
          Else
            MsgBox lcAsesor.ErrMsg, vbCritical, Me.Caption
            Set lcAsesor = Nothing
            GoTo ErrProcedure
          End If
    
        Set lcAsesor = Nothing
                

        
        
       .EndTable
  
        .Paragraph = ""
        .DrawLine .MarginLeft, .CurrentY, .PageWidth - .MarginRight, .CurrentY
        .Paragraph = UCase("Cajas")
        .DrawLine .MarginLeft, .CurrentY, .PageWidth - .MarginRight, .CurrentY
        
        .Paragraph = ""
        .StartTable
              .TableBorder = tbAll
              .TableCell(tcRows) = 1
               .TableCell(tcCols) = 3
              .TableCell(tcText, 1, 1) = "Descripción"
              .TableCell(tcText, 1, 2) = "Moneda"
              .TableCell(tcText, 1, 3) = "Mercado"
              .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
              Rem Busca las cajas de la cuenta
              Set lCajas_Ctas = New Class_Cajas_Cuenta
              
              
              lCajas_Ctas.Campo("id_cuenta").Valor = lReg_Cta("id_cuenta").Value
              If lCajas_Ctas.Buscar(True) Then
                
              Set lCursor_Caj = lCajas_Ctas.Cursor
              .TableCell(tcRows) = .TableCell(tcRows) + lCursor_Caj.Count
             
                iFila = 2
                For Each lReg_Caja_Cta In lCursor_Caj
                      
                    .TableCell(tcText, iFila, 1) = lReg_Caja_Cta("dsc_caja_cuenta").Value
                    .TableCell(tcText, iFila, 2) = lReg_Caja_Cta("dsc_moneda").Value
                    .TableCell(tcText, iFila, 3) = lReg_Caja_Cta("desc_mercado").Value
                    iFila = iFila + 1
                Next
              Else
              MsgBox lCajas_Ctas.ErrMsg, vbCritical, Me.Caption
              GoTo ErrProcedure
              End If
              .TableCell(tcColWidth, , 1) = "73mm"
              .TableCell(tcColWidth, , 2) = "43mm"
              .TableCell(tcColWidth, , 3) = "43mm"
        .EndTable
        
'          .StartTable
'              .TableBorder = tbAll
'              .TableCell(tcRows) = 1
'               .TableCell(tcCols) = 3
'              .TableCell(tcText, 1, 1) = "Descripción"
'              .TableCell(tcText, 1, 2) = "Moneda"
'              .TableCell(tcText, 1, 3) = "Mercado"
'              .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'              Rem Busca las cajas de la cuenta
'              Set lAlias_Ctas = New Class_Alias
'
'
'              lAlias_Ctas.Campo("id_cuenta").Valor = lReg_Cta("id_cuenta").Value
'              If lCajas_Ctas.Buscar(True) Then
'
'              Set lCursor_Alias = lAlias_Ctas.Cursor
'              .TableCell(tcRows) = .TableCell(tcRows) + lCursor_Alias.Count
'
'                iFila = 2
'                For Each lReg_Alias In lCursor_Alias
'
'                    .TableCell(tcText, iFila, 1) = lReg_Alias("dsc_caja_cuenta").Value
'                    .TableCell(tcText, iFila, 2) = lReg_Alias("dsc_moneda").Value
'                    .TableCell(tcText, iFila, 3) = lReg_Alias("desc_mercado").Value
'                    iFila = iFila + 1
'                Next
'              Else
'              MsgBox lAlias_Ctas.ErrMsg, vbCritical, Me.Caption
'              GoTo ErrProcedure
'              End If
'              .TableCell(tcColWidth, , 1) = "60mm"
'              .TableCell(tcColWidth, , 2) = "50mm"
'              .TableCell(tcColWidth, , 3) = "50mm"
'        .EndTable
        
        
      Next
      
      

    'Else
    '  .Paragraph = "No hay Cuentas Vigentes en el sistema."
    'End If
    
    .EndDoc
  End With
  
  Call ColocarEn(lForm, Me, eFP_Abajo)
  
ErrProcedure:
  
End Sub

Public Sub Sub_Reporte_Cuenta_desde_Mantenedor(pId_Cuenta)
  Call Sub_Reporte_Cuenta(pId_Cuenta)
  Unload Me
End Sub
