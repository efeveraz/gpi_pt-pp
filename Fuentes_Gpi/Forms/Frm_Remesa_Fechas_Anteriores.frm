VERSION 5.00
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Remesa_Fechas_Anteriores 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Remesas"
   ClientHeight    =   6945
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8010
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6945
   ScaleWidth      =   8010
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   6435
      Left            =   60
      TabIndex        =   24
      Top             =   420
      Width           =   7845
      Begin hControl2.hTextLabel Txt_FechaRemesa 
         Height          =   315
         Left            =   4290
         TabIndex        =   2
         Top             =   270
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Fecha"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin VB.Frame Frame2 
         Height          =   1545
         Left            =   90
         TabIndex        =   26
         Top             =   1350
         Width           =   7665
         Begin hControl2.hTextLabel Txt_MonedaEgreso 
            Height          =   315
            Left            =   4290
            TabIndex        =   8
            Top             =   270
            Width           =   3030
            _ExtentX        =   5345
            _ExtentY        =   556
            LabelWidth      =   1300
            Caption         =   "Moneda Egreso"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_MonedaIngreso 
            Height          =   315
            Left            =   4290
            TabIndex        =   10
            Top             =   660
            Width           =   3030
            _ExtentX        =   5345
            _ExtentY        =   556
            LabelWidth      =   1300
            Caption         =   "Moneda Ingreso"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
         End
         Begin hControl2.hTextLabel Txt_Dias_Retencion 
            Height          =   315
            Left            =   120
            TabIndex        =   11
            Tag             =   "OBLI"
            Top             =   1050
            Width           =   2010
            _ExtentX        =   3545
            _ExtentY        =   556
            LabelWidth      =   1500
            TextMinWidth    =   500
            Caption         =   "Fecha Liquidaci�n"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin TrueDBList80.TDBCombo Cmb_CajaEgreso 
            Height          =   345
            Left            =   1530
            TabIndex        =   7
            Tag             =   "OBLI=S;CAPTION=Caja Egreso"
            Top             =   270
            Width           =   2685
            _ExtentX        =   4736
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   -1  'True
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Remesa_Fechas_Anteriores.frx":0000
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_CajaIngreso 
            Height          =   345
            Left            =   1530
            TabIndex        =   9
            Tag             =   "OBLI=S;CAPTION=Caja Ingreso"
            Top             =   660
            Width           =   2685
            _ExtentX        =   4736
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   -1  'True
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Remesa_Fechas_Anteriores.frx":00AA
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin MSComCtl2.DTPicker dtp_fecha_liquidacion 
            Height          =   315
            Left            =   2130
            TabIndex        =   12
            Tag             =   "OBLI"
            Top             =   1050
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   57671681
            CurrentDate     =   38768
         End
         Begin VB.Label Label2 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Caja Ingreso"
            Height          =   345
            Left            =   120
            TabIndex        =   28
            Top             =   660
            Width           =   1395
         End
         Begin VB.Label Label1 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Caja Egreso"
            Height          =   345
            Left            =   120
            TabIndex        =   27
            Top             =   270
            Width           =   1395
         End
      End
      Begin VB.Frame Frame3 
         Height          =   2055
         Left            =   90
         TabIndex        =   25
         Top             =   2940
         Width           =   7665
         Begin hControl2.hTextLabel Txt_MontoCuenta 
            Height          =   315
            Left            =   120
            TabIndex        =   13
            Top             =   300
            Width           =   4155
            _ExtentX        =   7329
            _ExtentY        =   556
            LabelWidth      =   1400
            Caption         =   "Saldo Disponible"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_CambioCliente 
            Height          =   315
            Left            =   4350
            TabIndex        =   16
            Tag             =   "OBLI"
            Top             =   690
            Width           =   3225
            _ExtentX        =   5689
            _ExtentY        =   556
            LabelWidth      =   1700
            Caption         =   "Cambio Remesa"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_MontoEgreso 
            Height          =   315
            Left            =   120
            TabIndex        =   15
            Tag             =   "OBLI"
            Top             =   690
            Width           =   4155
            _ExtentX        =   7329
            _ExtentY        =   556
            LabelWidth      =   1400
            Caption         =   "Monto Egreso"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_MontoIngreso 
            Height          =   315
            Left            =   120
            TabIndex        =   17
            Tag             =   "OBLI"
            Top             =   1080
            Width           =   4155
            _ExtentX        =   7329
            _ExtentY        =   556
            LabelWidth      =   1400
            Caption         =   "Monto Ingreso"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_cambioOriginal 
            Height          =   315
            Left            =   4350
            TabIndex        =   14
            Top             =   300
            Width           =   3225
            _ExtentX        =   5689
            _ExtentY        =   556
            LabelWidth      =   1700
            Caption         =   "Cambio Cuenta"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_CambioTransferencia 
            Height          =   315
            Left            =   4350
            TabIndex        =   18
            Tag             =   "OBLI"
            Top             =   1080
            Width           =   3225
            _ExtentX        =   5689
            _ExtentY        =   556
            LabelWidth      =   1700
            Caption         =   "Cambio Transferencia"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Utilidad 
            Height          =   315
            Left            =   120
            TabIndex        =   19
            Tag             =   "OBLI"
            Top             =   1500
            Width           =   4155
            _ExtentX        =   7329
            _ExtentY        =   556
            LabelWidth      =   1400
            Caption         =   "Utilidad"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
      End
      Begin hControl2.hTextLabel Txt_Cuenta 
         Height          =   315
         Left            =   150
         TabIndex        =   1
         Top             =   270
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Cuentas"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Observaci�n 
         Height          =   315
         Left            =   180
         TabIndex        =   22
         Tag             =   "OBLI"
         Top             =   5880
         Width           =   7425
         _ExtentX        =   13097
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Observaci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   499
      End
      Begin hControl2.hTextLabel txt_nombre_cliente 
         Height          =   315
         Left            =   150
         TabIndex        =   6
         Top             =   990
         Width           =   7455
         _ExtentX        =   13150
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Nombre Cliente"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel txt_rut_cliente 
         Height          =   315
         Left            =   150
         TabIndex        =   5
         Top             =   630
         Width           =   3990
         _ExtentX        =   7038
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Rut Cliente"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Alignment       =   1
      End
      Begin TrueDBList80.TDBCombo Cmb_Glosa_Descripcion 
         Height          =   315
         Left            =   1500
         TabIndex        =   21
         Top             =   5490
         Width           =   6075
         _ExtentX        =   10716
         _ExtentY        =   556
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   556
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Remesa_Fechas_Anteriores.frx":0154
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Tipo_Remesa 
         Height          =   315
         Left            =   1500
         TabIndex        =   20
         Tag             =   "OBLI=S;CAPTION=Caja Egreso"
         Top             =   5100
         Width           =   6075
         _ExtentX        =   10716
         _ExtentY        =   556
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   556
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Remesa_Fechas_Anteriores.frx":01FE
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComCtl2.DTPicker Dtp_Fecha 
         Height          =   315
         Left            =   5580
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   270
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   57671681
         CurrentDate     =   38768
      End
      Begin VB.Label lbl_fecha 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4290
         TabIndex        =   31
         Top             =   270
         Width           =   1305
      End
      Begin VB.Label Label4 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tipo de Remesa"
         Height          =   315
         Left            =   150
         TabIndex        =   30
         Top             =   5100
         Width           =   1305
      End
      Begin VB.Label Label3 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Descripci�n"
         Height          =   315
         Left            =   150
         TabIndex        =   29
         Top             =   5490
         Width           =   1305
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8010
      _ExtentX        =   14129
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   4
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin hControl2.hTextLabel hTextLabel1 
      Height          =   315
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   556
      LabelWidth      =   1300
      Caption         =   "Cuentas"
      Text            =   ""
      Locked          =   -1  'True
      BackColorTxt    =   12648447
      BackColorTxt    =   12648447
   End
End
Attribute VB_Name = "Frm_Remesa_Fechas_Anteriores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fId_Cuenta As String
Dim C_Aporte_Rescate As Class_APORTE_RESCATE_CUENTA
Dim C_Cajas_Cuenta As Class_Cajas_Cuenta
Dim fOperacion As String
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fFlg_Fechas_Anteriores      As Boolean
Dim l_caja_ingreso As Double
Dim l_caja_egreso As Double
Dim fFlg_Mov_Descubiertos As String
Dim fFecha_Remesa As Date

Private Function Fnt_BuscaTipoCambio() As Boolean
Dim i As Integer
Dim lcTipo_Cambio       As Object
Dim lId_Moneda_Origen   As String
Dim lId_Moneda_Destino  As String
Dim lFecha              As Date
'---------------------------------------
Dim lParidad As Double
Dim lOperacion As String
Dim caja_egreso As String
Dim caja_ingreso As String

  Fnt_BuscaTipoCambio = True
  
  lId_Moneda_Origen = Txt_MonedaEgreso.Tag
  lId_Moneda_Destino = Txt_MonedaIngreso.Tag
   
  If lId_Moneda_Origen = lId_Moneda_Destino Then
    MsgBox "Caja de Ingreso debe ser diferente a la Caja de Egreso.", vbInformation, Me.Caption
    Fnt_BuscaTipoCambio = False

  Else
    lFecha = Dtp_Fecha.Value
  
    If (Not lId_Moneda_Origen = "") And (Not lId_Moneda_Destino = "") Then
'      Set lcTipo_Cambio = New Class_Tipo_Cambios
      Set lcTipo_Cambio = Fnt_CreateObject(cDLL_Tipo_Cambios)
      With lcTipo_Cambio
        If .Busca_Precio_Paridad(fId_Cuenta _
                               , lId_Moneda_Origen _
                               , lId_Moneda_Destino _
                               , lFecha _
                               , lParidad _
                               , lOperacion) Then
          If fKey = cNewEntidad Then
            Txt_CambioCliente.Text = lParidad
          End If
          txt_cambioOriginal.Text = lParidad
          fOperacion = Trim(lOperacion)
        Else
          MsgBox .ErrMsg, vbCritical, Me.Caption
        End If
      End With
    End If
    
    Call Sub_CalculaTipoCambio
    Call Sub_Calcula_Utilidad
   ' Call Asigna_Glosa
  End If
      
End Function

Private Sub Sub_CalculaTipoCambio()
  If Not fOperacion = "" Then
    If Trim(fOperacion) = "M" Then
      'Realiza la multiplicacion
      Txt_MontoIngreso.Text = To_Number(Txt_MontoEgreso.Text) * To_Number(Txt_CambioCliente.Text)
    Else
      'Realiza la division
      Txt_MontoIngreso.Text = Fnt_Divide(To_Number(Txt_MontoEgreso.Text), To_Number(Txt_CambioCliente.Text))
    End If
  End If
End Sub

Private Sub Cmb_CajaEgreso_ItemChange()
Dim lCaja_Cuenta    As Class_Cajas_Cuenta
Dim lId_Caja_Cuenta As String
Dim lReg            As hFields
Dim i As Integer
   lId_Caja_Cuenta = Fnt_ComboSelected_KEY(Cmb_CajaEgreso)
  
  If Not lId_Caja_Cuenta = "" Then
    Set lCaja_Cuenta = New Class_Cajas_Cuenta
    With lCaja_Cuenta
      .Campo("id_caja_cuenta").Valor = lId_Caja_Cuenta
      
      If .Buscar(True) Then
        For Each lReg In .Cursor
          Txt_MonedaEgreso.Tag = lReg("id_moneda").Value
          Txt_MonedaEgreso.Text = lReg("dsc_moneda").Value
          Txt_Utilidad.Caption = "Utilidad (" & lReg("dsc_moneda").Value & ")"
          Txt_MontoEgreso.Format = Fnt_Formato_Moneda(lReg("ID_MONEDA").Value)
          Txt_MontoCuenta.Format = Txt_MontoEgreso.Format
        Next
      Else
        MsgBox .ErrMsg, vbCritical, Me.Caption
      End If
' *************************************************************************************
      Txt_MontoCuenta.Text = .Saldo_Disponible(Dtp_Fecha.Value)
' *************************************************************************************
      If Not Fnt_BuscaTipoCambio Then
        Cmb_CajaEgreso.Text = ""
        Txt_MonedaEgreso.Text = ""
        Txt_MonedaEgreso.Tag = ""
        Call Sub_CargaCombo_Cajas_Cuenta(Cmb_CajaEgreso, fId_Cuenta)
        'Cmb_CajaEgreso.SelectedItem = ""
        
      End If
    End With
  End If
    
  Call Asigna_Glosa

End Sub

Private Sub Cmb_CajaIngreso_ItemChange()
Dim lCaja_Cuenta    As Class_Cajas_Cuenta
Dim lId_Caja_Cuenta As String
Dim lReg            As hFields
Dim i As Integer
  lId_Caja_Cuenta = Fnt_ComboSelected_KEY(Cmb_CajaIngreso)
  
  If Not lId_Caja_Cuenta = "" Then
    Set lCaja_Cuenta = New Class_Cajas_Cuenta
    With lCaja_Cuenta
      .Campo("id_caja_cuenta").Valor = lId_Caja_Cuenta
      
      If .Buscar(True) Then
        For Each lReg In .Cursor
          Txt_MonedaIngreso.Tag = lReg("id_moneda").Value
          Txt_MonedaIngreso.Text = lReg("dsc_moneda").Value
          Txt_MontoIngreso.Format = Fnt_Formato_Moneda(lReg("ID_MONEDA").Value)
          
        Next
      Else
        MsgBox .ErrMsg, vbCritical, Me.Caption
      End If
    End With
  End If
  If Not Fnt_BuscaTipoCambio Then
    Cmb_CajaIngreso.Text = ""
    Txt_MonedaIngreso.Text = ""
    Txt_MonedaIngreso.Tag = ""
    Call Sub_CargaCombo_Cajas_Cuenta(Cmb_CajaIngreso, fId_Cuenta)
    'Cmb_CajaIngreso.SelectedItem = ""
  End If
  Call Asigna_Glosa
End Sub

Private Sub DTP_Fecha_Change()
Dim lCaja_Cuenta    As Class_Cajas_Cuenta
Dim lId_Caja_Cuenta As String

  'lId_Caja_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cajas)
  dtp_fecha_liquidacion.MinDate = Dtp_Fecha.Value
  dtp_fecha_liquidacion.Value = Dtp_Fecha.Value
  fFecha_Remesa = Dtp_Fecha.Value
  Txt_Dias_Retencion.Text = DateDiff("d", Dtp_Fecha.Value, dtp_fecha_liquidacion.Value)

'  If Not lId_Caja_Cuenta = "" Then
'    Set lCaja_Cuenta = New Class_Cajas_Cuenta
'    lCaja_Cuenta.Campo("id_caja_cuenta").Valor = lId_Caja_Cuenta
'    Txt_SaldoCaja.Text = lCaja_Cuenta.Saldo_Disponible(Dtp_Fecha.Value)
'    Set lCaja_Cuenta = Nothing
'  End If
End Sub

Private Sub dtp_fecha_liquidacion_Change()
'    Txt_Dias_Retencion.Text = DateDiff("d", Dtp_Fecha.Value, dtp_fecha_liquidacion.Value)
  dtp_fecha_liquidacion.Value = Fnt_Dia_Habil_MasProximo(dtp_fecha_liquidacion.Value)
  Txt_Dias_Retencion.Text = Fnt_DiasHabiles_EntreFechas(fFecha_Remesa, dtp_fecha_liquidacion.Value)
End Sub

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
     .Buttons("SAVE").Image = cBoton_Grabar
     .Buttons("EXIT").Image = cBoton_Salir
     .Buttons("REFRESH").Image = cBoton_Original
  End With
  
  Call Sub_CargaForm
  'Call Asigna_Glosa
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
   Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lFila As Long
Dim lId_Remesa As String
Dim lRollback As Boolean
Dim tipoRemesa As String
Dim caja_ingreso As Long
Dim caja_egreso As Long

Dim lcRemesa As Class_Remesas
   
  gDB.IniciarTransaccion
  lRollback = False
  
  If Not Fnt_ValidarDatos Then
    lRollback = True
    GoTo ErrProcedure
  End If
  lId_Remesa = fKey
  
  l_caja_egreso = Fnt_Carga_Egreso(ByVal lId_Remesa)
  l_caja_ingreso = Fnt_Carga_Ingreso(ByVal lId_Remesa)
    
  caja_ingreso = Fnt_ComboSelected_KEY(Cmb_CajaIngreso)
  caja_egreso = Fnt_ComboSelected_KEY(Cmb_CajaEgreso)
  
  Set lcRemesa = New Class_Remesas
  
  Select Case Cmb_Tipo_Remesa.SelectedItem
    Case 1
      tipoRemesa = "REM-SPOT"
    Case Else
      tipoRemesa = "REMESA"
  End Select
  
  With lcRemesa
     .Campo("Id_remesa").Valor = lId_Remesa
     .Campo("ID_MOV_CAJA_EGRESO").Valor = l_caja_egreso
     .Campo("ID_MOV_CAJA_INGRESO").Valor = l_caja_ingreso
     .Campo("id_cuenta").Valor = fId_Cuenta
     .Campo("id_tipo_estado").Valor = cTEstado_Mov_Caja
     .Campo("cod_estado").Valor = cCod_Estado_Pendiente
     .Campo("fecha_remesa").Valor = Dtp_Fecha.Value 'IIf(lId_Remesa = cNewEntidad, Dtp_Fecha.Value, Fnt_String2Date(Dtp_Fecha.Value))
     .Campo("retencion").Valor = Txt_Dias_Retencion.Text
     .Campo("ID_CAJA_CUENTA_EGRESO").Valor = caja_egreso
     .Campo("ID_CAJA_CUENTA_INGRESO").Valor = caja_ingreso
     .Campo("monto_egreso").Valor = Txt_MontoEgreso.Text
     .Campo("monto_ingreso").Valor = Txt_MontoIngreso.Text
     .Campo("factor_conversion_cliente").Valor = Txt_CambioCliente.Text
     .Campo("FACTOR_TRANSFERENCIA").Valor = Txt_CambioTransferencia.Text
     .Campo("DSC_REMESA").Valor = Cmb_Glosa_Descripcion.Text
     .Campo("OBS_REMESA").Valor = Txt_Observaci�n.Text
     .Campo("UTILIDAD").Valor = Txt_Utilidad.Text
     .Campo("COD_ORIGEN_MOV_CAJA").Valor = tipoRemesa
     
     If .Guardar Then
       MsgBox "Remesa grabada correctamente.", vbInformation, Me.Caption
     Else
       MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
       lRollback = True
       GoTo ErrProcedure
     End If
     
     lId_Remesa = .Campo("Id_Remesa").Valor
  End With
    
ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    Fnt_Grabar = False
  Else
    Call gDB.CommitTransaccion
    Fnt_Grabar = True
  End If
  
  Set lcRemesa = Nothing
  
End Function

Private Sub Sub_CargaForm()
   Call Sub_FormControl_Color(Me.Controls)
   Cmb_Glosa_Descripcion.BackColor = fColorOBligatorio
   Call Sub_CargaCombo_Cajas_Cuenta(Cmb_CajaEgreso, fId_Cuenta)
   Call Sub_CargaCombo_Glosa_Descripcion(Cmb_Glosa_Descripcion, 1)
   Call Sub_CargaCombo_Cajas_Cuenta(Cmb_CajaIngreso, fId_Cuenta)
   'Txt_FechaRemesa.Text = Fnt_FechaServidor
   Dtp_Fecha.Value = Fnt_FechaServidor
   Dtp_Fecha.MaxDate = Fnt_FechaServidor
   fFecha_Remesa = Fnt_FechaServidor
   Call Sub_CargarTipoRemesa

   'Call Asigna_Glosa
End Sub

Private Sub Sub_CargarTipoRemesa()
With Cmb_Tipo_Remesa
  .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    '.SelectedItem = Null
    
    Call .Columns.Remove(1)
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
'      .Translate = True
    End With
            
    'lResult = False
    
    On Error Resume Next
    'lResult = pClase.Llena_Combo
'    lNumError = Err.Number
    On Error GoTo 0
      
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
        
        Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem("REMESA", "REMESA"))
        Call .Columns(0).ValueItems.Add(Fnt_AgregaValueItem("REM-SPOT", "REMESA DOLAR SPOT"))
        
        Call .AddItem("REMESA")
        Call .AddItem("REMESA DOLAR SPOT")
    
  End With
End Sub

Private Sub Sub_CargarDatos()
Dim lReg    As hCollection.hFields
Dim lCursor As hRecord
Dim lLinea  As Long
Dim lID     As String
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lRemesa As Class_Remesas

  Load Me
  
'  Set lcCuenta = New Class_Cuentas
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  With lcCuenta
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    .Campo("id_Cuenta").Valor = fId_Cuenta
    If .Buscar_Vigentes Then
       For Each lReg In .Cursor
          Txt_Cuenta.Text = lReg("num_cuenta").Value & " - " & lReg("dsc_cuenta").Value
          txt_nombre_cliente.Text = lReg("nombre_cliente").Value
          txt_rut_cliente.Text = lReg("rut_cliente").Value
          fFlg_Mov_Descubiertos = .Cursor(1)("FLG_MOV_DESCUBIERTOS").Value
       Next
    Else
       MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcCuenta = Nothing
  
  If fKey = cNewEntidad Then
     Exit Sub
  End If
  
  Set lRemesa = New Class_Remesas
  With lRemesa
     .Campo("id_Cuenta").Valor = fId_Cuenta
     .Campo("id_remesa").Valor = fKey
     If Not .Buscar Then
        MsgBox .ErrMsg, vbCritical, Me.Caption
     Else
        For Each lReg In .Cursor
           
           Txt_MontoEgreso.Text = lReg("monto_egreso").Value
           Txt_MontoIngreso.Text = lReg("monto_ingreso").Value
           Txt_CambioCliente.Text = lReg("factor_cliente").Value
           Txt_CambioTransferencia.Text = lReg("factor_transferencia").Value
           Txt_Observaci�n.Text = lReg("observacion").Value
           Txt_Dias_Retencion.Text = lReg("retencion").Value
           l_caja_ingreso = lReg("MOV_INGRESO").Value
           l_caja_egreso = lReg("MOV_EGRESO").Value
           Dtp_Fecha.Value = lReg("fecha_remesa").Value
           'dtp_fecha_liquidacion.Value = DateAdd("d", Txt_Dias_Retencion.Text, Dtp_Fecha.Value)
           dtp_fecha_liquidacion.Value = Fnt_Calcula_Dia_Habil(lReg("fecha_remesa").Value, NVL(.Cursor(1).Fields("retencion").Value, 0))
           
           Call Sub_ComboSelectedItem(Cmb_Tipo_Remesa, lReg("COD_ORIGEN_MOV_CAJA").Value)
           Call Sub_ComboSelectedItem(Cmb_CajaEgreso, lReg("id_caja_egreso").Value)
           Call Sub_ComboSelectedItem(Cmb_CajaIngreso, lReg("id_caja_ingreso").Value)
           
           Call Cmb_CajaEgreso_ItemChange
           Call Cmb_CajaIngreso_ItemChange
           
           Cmb_Glosa_Descripcion.Text = lReg("descripcion").Value
           
           Call Sub_Calcula_Utilidad
           
        Next
     End If
  End With
  Set lRemesa = Nothing
  
End Sub

Private Sub Asigna_Glosa()
  Call Sub_CargaCombo_Glosa_Descripcion(Cmb_Glosa_Descripcion, 1, , Cmb_CajaEgreso.Text, Cmb_CajaIngreso.Text)
End Sub

Private Function Fnt_ValidarDatos() As Boolean
Dim lResult As Boolean
  
  Fnt_ValidarDatos = True
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    lResult = False
    GoTo ErrProcedure
  ElseIf Cmb_Glosa_Descripcion.Text = "" Then
    MsgBox "Debe ingresar ""Descripci�n"".", vbInformation
    lResult = False
    GoTo ErrProcedure
'  ElseIf Fnt_Verifica_Feriado(dtp_fecha_liquidacion.Value) Then
'    MsgBox "Fecha de Liquidaci�n debe ser d�a h�bil.", vbInformation
'    lResult = False
'    GoTo ErrProcedure
  End If
  
  Exit Function
  
ErrProcedure:
  Fnt_ValidarDatos = lResult
  
End Function

Public Function Fnt_Modificar(pkey, pId_Cuenta, pCod_Arbol_Sistema, pFlg_Fechas_Anteriores)
  fId_Cuenta = pId_Cuenta
  fFlg_Fechas_Anteriores = pFlg_Fechas_Anteriores
  
  Load Me
  
  fKey = pkey
  
  '-----------------------------------------------------------------------
  '--Setea los controles dependiendo de los permisos
   fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
   Cmb_Glosa_Descripcion.BackColor = fColorOBligatorio
   Call Form_Resize
  '-----------------------------------------------------------------------
  
  Call Sub_CargarDatos
   'Call Asigna_Glosa
   
  If Not fFlg_Fechas_Anteriores Then
    Dtp_Fecha.Visible = False
    Txt_FechaRemesa.Visible = True
    Txt_FechaRemesa.Text = Dtp_Fecha.Value
  Else
    Dtp_Fecha.Visible = True
    Txt_FechaRemesa.Visible = False
    'DTP_Fecha.MaxDate = DTP_Fecha.Value
  End If
  dtp_fecha_liquidacion.MinDate = Dtp_Fecha.Value
   
  If fKey = cNewEntidad Then
     Me.Caption = "Ingreso de Remesa"
     'Txt_FechaRemesa.Visible = False
  Else
    Me.Caption = "Modicaci�n de Remesa"
    Dtp_Fecha.Visible = True
    Txt_FechaRemesa.Visible = True
    Txt_FechaRemesa.Text = Dtp_Fecha.Value
  End If
   
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Txt_CambioCliente_LostFocus()
  Call Sub_CalculaTipoCambio
  Call Sub_Calcula_Utilidad
End Sub

Private Sub Txt_CambioTransferencia_LostFocus()
  Call Sub_Calcula_Utilidad
End Sub

Private Sub Txt_Dias_Retencion_LostFocus()
'  dtp_fecha_liquidacion.Value = DateAdd("d", Txt_Dias_Retencion.Text, Dtp_Fecha.Value)
  If Not Txt_Dias_Retencion.Text = "" Then
    dtp_fecha_liquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Remesa, Txt_Dias_Retencion.Text)
  End If
End Sub

Private Sub Txt_MontoEgreso_LostFocus()
  Call Sub_CalculaTipoCambio
  Call Sub_Calcula_Utilidad
End Sub

Private Sub Txt_MontoEgreso_Validate(Cancel As Boolean)
  If (To_Number(Txt_MontoCuenta.Text) < To_Number(Txt_MontoEgreso.Text)) And (To_Number(Txt_MontoEgreso.Text) > 0) Then
    If fFlg_Mov_Descubiertos = gcFlg_SI Then
        If MsgBox("El monto ingresado sobrepasa el saldo de la caja." & vbLf & vbLf & _
                    "Saldo : " & Format(Txt_MontoCuenta.Text, "#,##0.00") & vbLf & _
                    "Monto : " & Format(To_Number(Txt_MontoEgreso.Text), "#,##0.00") & vbLf & "�Desea Continuar?" _
                  , vbYesNo + vbQuestion _
                , Me.Caption) = vbNo Then
                Cancel = True
        End If
    Else
        MsgBox "Monto Egreso no puede ser mayor al Saldo Disponible.", vbInformation
        Txt_MontoEgreso.Text = ""
        Cancel = True
    End If
  End If
End Sub

Function Fnt_Carga_Egreso(ByVal lId_Remesa As String) As Double   'pid_mov_caja As Double
  Dim lMov_Caja As Class_Mov_Caja
  Set lMov_Caja = New Class_Mov_Caja
  Dim lReg As hFields
  Dim lCod_Estado As String
  
  '---------------------------------------------
  '-- Agrega o modifica el movimiento de caja
  '---------------------------------------------
  With lMov_Caja
    .Campo("id_mov_caja").Valor = IIf(lId_Remesa = cNewEntidad, cNewEntidad, l_caja_egreso)
    .Campo("Id_Caja_Cuenta").Valor = Fnt_ComboSelected_KEY(Cmb_CajaEgreso)
    .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_REMESA
    .Campo("Fecha_Movimiento").Valor = Dtp_Fecha.Value ' Txt_FechaRemesa.Text
    .Campo("Fecha_Liquidacion").Valor = dtp_fecha_liquidacion.Value
    .Campo("Monto").Valor = Txt_MontoEgreso.Text
    .Campo("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Cargo
    .Campo("Dsc_Mov_Caja").Valor = Cmb_Glosa_Descripcion.Text
    .Campo("FLG_MOV_AUTOMATICO").Valor = gcFlg_NO
  
    If Not .Guardar Then
      MsgBox "Problemas al grabar. " & .ErrMsg, vbCritical, Me.Caption
      gDB.Parametros.Clear
    End If
  
    Fnt_Carga_Egreso = .Campo("id_mov_caja").Valor
  End With
End Function

Function Fnt_Carga_Ingreso(ByVal lId_Remesa As String) As Double
   Dim lMov_Caja As Class_Mov_Caja
   Set lMov_Caja = New Class_Mov_Caja
   Dim lReg As hFields
   Dim lCod_Estado As String
   '---------------------------------------------
   '-- Agrega o modifica el movimiento de caja
   '---------------------------------------------
   With lMov_Caja
      .Campo("id_mov_caja").Valor = IIf(lId_Remesa = cNewEntidad, cNewEntidad, l_caja_ingreso)
      .Campo("Id_Caja_Cuenta").Valor = Fnt_ComboSelected_KEY(Cmb_CajaIngreso)
      .Campo("Cod_Origen_Mov_Caja").Valor = gcOrigen_Mov_Caja_REMESA
      .Campo("Fecha_Movimiento").Valor = Dtp_Fecha.Value 'Txt_FechaRemesa.Text
      .Campo("Fecha_Liquidacion").Valor = dtp_fecha_liquidacion.Value
      .Campo("Monto").Valor = Txt_MontoIngreso.Text
      .Campo("Flg_Tipo_Movimiento").Valor = gcTipoOperacion_Abono
      .Campo("Dsc_Mov_Caja").Valor = Cmb_Glosa_Descripcion.Text
      .Campo("FLG_MOV_AUTOMATICO").Valor = gcFlg_NO
    
      If Not .Guardar Then
         MsgBox "Problemas al grabar. " & .ErrMsg, vbCritical, Me.Caption
         gDB.Parametros.Clear
      End If
    
      Fnt_Carga_Ingreso = .Campo("id_mov_caja").Valor
   End With
End Function

Private Sub Txt_MontoIngreso_LostFocus()
  Call Sub_Calcula_Utilidad
  'Call Asigna_Glosa
End Sub

Private Sub Sub_Calcula_Utilidad()
Dim lMonto_Transferencia As Double
Dim lUtilidad_Moneda_Origen As Double

  If Not fOperacion = "" And To_Number(Txt_CambioTransferencia.Text) > 0 Then
    If Trim(fOperacion) = "M" Then
      'Realiza la multiplicacion
      lMonto_Transferencia = To_Number(Txt_MontoEgreso.Text) * To_Number(Txt_CambioTransferencia.Text)
    Else
      'Realiza la division
      lMonto_Transferencia = Fnt_Divide(To_Number(Txt_MontoEgreso.Text), To_Number(Txt_CambioTransferencia.Text))
    End If
    
    lUtilidad_Moneda_Origen = lMonto_Transferencia - Txt_MontoIngreso.Text
    
    Rem Operacion inversa, para dejarlo en la Moneda Origen
    If Trim(fOperacion) = "M" Then
      'Realiza la division
      Txt_Utilidad.Text = Fnt_Divide(To_Number(lUtilidad_Moneda_Origen), To_Number(Txt_CambioTransferencia.Text))
    Else
      'Realiza la multiplciacion
      Txt_Utilidad.Text = To_Number(lUtilidad_Moneda_Origen) * To_Number(Txt_CambioTransferencia.Text)
    End If
    
  End If
End Sub
